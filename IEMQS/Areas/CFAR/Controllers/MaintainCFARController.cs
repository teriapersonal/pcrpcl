﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Areas.Utility.Models;
using IEMQS.Areas.WQ.Models;
using IEMQS.HEDirectoryService;
using IEMQS.Models;
using IEMQSImplementation;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.CFAR.Controllers
{
    public class MaintainCFARController : clsBase
    {

        // GET: CFAR/MaintainCFAR
        #region Header List
        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        public ActionResult Index(string Project)
        {
            ViewBag.Title = clsImplementationEnum.CFARIndexTitle.maintainCFAR.GetStringValue();
            ViewBag.IndexType = clsImplementationEnum.CFARIndexType.maintain.GetStringValue();
            ViewBag.chkProject = Project;
            return View();
        }

        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        public ActionResult Validate()
        {
            ViewBag.Title = clsImplementationEnum.CFARIndexTitle.ValidateCFAR.GetStringValue();
            ViewBag.IndexType = clsImplementationEnum.CFARIndexType.validate.GetStringValue();
            return View("Index");
        }
        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        public ActionResult Invalidate()
        {
            ViewBag.Title = clsImplementationEnum.CFARIndexTitle.InvalidateCFAR.GetStringValue();
            ViewBag.IndexType = clsImplementationEnum.CFARIndexType.invalidate.GetStringValue();
            return View("Index");
        }
        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        public ActionResult Assign()
        {
            ViewBag.Title = clsImplementationEnum.CFARIndexTitle.AssignCFAR.GetStringValue();
            ViewBag.IndexType = clsImplementationEnum.CFARIndexType.assign.GetStringValue();
            return View("Index");
        }

        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        public ActionResult Approve()
        {
            ViewBag.Title = clsImplementationEnum.CFARIndexTitle.ApproveCFAR.GetStringValue();
            ViewBag.IndexType = clsImplementationEnum.CFARIndexType.approve.GetStringValue();
            return View("Index");
        }

        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        public ActionResult Implement()
        {
            ViewBag.Title = clsImplementationEnum.CFARIndexTitle.ImplementCFAR.GetStringValue();
            ViewBag.IndexType = clsImplementationEnum.CFARIndexType.implement.GetStringValue();
            return View("Index");
        }

        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        public ActionResult Closure()
        {
            ViewBag.Title = clsImplementationEnum.CFARIndexTitle.ClosueCFAR.GetStringValue();
            ViewBag.IndexType = clsImplementationEnum.CFARIndexType.closure.GetStringValue();
            return View("Index");
        }

        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        public ActionResult CustomerClosure()
        {
            ViewBag.Title = clsImplementationEnum.CFARIndexTitle.CustomerClosueCFAR.GetStringValue();
            ViewBag.IndexType = clsImplementationEnum.CFARIndexType.customerclosure.GetStringValue();
            return View("Index");
        }

        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        public ActionResult ClosureApproval()
        {
            ViewBag.Title = clsImplementationEnum.CFARIndexTitle.ClosueApprovalCFAR.GetStringValue();
            ViewBag.IndexType = clsImplementationEnum.CFARIndexType.closureapproval.GetStringValue();
            return View("Index");
        }

        //load tab wise partial
        [HttpPost]
        public ActionResult LoadDataGridPartial(string status, string title, string indextype)
        {
            ViewBag.Status = status;
            ViewBag.GridTitle = title;
            ViewBag.IndexDataFor = indextype;
            return PartialView("_GetIndexGridDataPartial");
        }
        //bind datatable for Data Grid
        [HttpPost]
        public JsonResult LoadDataGridData(JQueryDataTableParamModel param,string Project)
        {
            try
            {
                if (Project != null && Project != "")
                {
                    int StartIndex = param.iDisplayStart + 1;
                    int EndIndex = param.iDisplayStart + param.iDisplayLength;
                    string strWhereCondition = string.Empty;

                    #region Datatable Sorting 
                    var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                    var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                    string strSortOrder = string.Empty;
                    string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                    string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                    if (!string.IsNullOrWhiteSpace(sortColumnName))
                    {
                        strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                    }

                    string _urlform = string.Empty;
                    string indextype = param.Department;
                    #endregion

                    if (string.IsNullOrWhiteSpace(indextype))
                    {
                        indextype = clsImplementationEnum.CFARIndexType.maintain.GetStringValue();
                    }

                    if (param.Status.ToLower() == "pending")
                    {
                        if (indextype == clsImplementationEnum.CFARIndexType.maintain.GetStringValue())
                        {
                            strWhereCondition += "1=1 and Status  in ( '" + clsImplementationEnum.CFARStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.CFARStatus.Returned_By_Validator.GetStringValue() + "') and CreatedBy='" + objClsLoginInfo.UserName.Trim() + "'";
                        }
                        if (indextype == clsImplementationEnum.CFARIndexType.validate.GetStringValue())
                        {
                            strWhereCondition += "1=1 and Status in ( '" + clsImplementationEnum.CFARStatus.SubmittedForValidate.GetStringValue() + "','" + clsImplementationEnum.CFARStatus.Returned_By_Assigner.GetStringValue() + "')  and MarketingManager='" + objClsLoginInfo.UserName.Trim() + "'";
                        }
                        if (indextype == clsImplementationEnum.CFARIndexType.invalidate.GetStringValue())
                        {
                            strWhereCondition += "1=1 and Status in ( '" + clsImplementationEnum.CFARStatus.SubmittedForInvalidation.GetStringValue() + "')  and ClusterHead='" + objClsLoginInfo.UserName.Trim() + "'";
                        }
                        if (indextype == clsImplementationEnum.CFARIndexType.assign.GetStringValue())
                        {
                            strWhereCondition += "1=1 and Status in ( '" + clsImplementationEnum.CFARStatus.SubmittedForAssign.GetStringValue() + "','" + clsImplementationEnum.CFARStatus.Returned_By_Approver.GetStringValue() + "')  and ProjectManager='" + objClsLoginInfo.UserName.Trim() + "'";
                        }
                        if (indextype == clsImplementationEnum.CFARIndexType.approve.GetStringValue())
                        {
                            strWhereCondition += "1=1 and Status in ( '" + clsImplementationEnum.CFARStatus.SubmittedForApproval.GetStringValue() + "','" + clsImplementationEnum.CFARStatus.Returned_By_Implementer.GetStringValue() + "')  and QCManager='" + objClsLoginInfo.UserName.Trim() + "'";
                        }
                        if (indextype == clsImplementationEnum.CFARIndexType.implement.GetStringValue())
                        {
                            strWhereCondition += "1=1 and Status in ( '" + clsImplementationEnum.CFARStatus.SubmittedForImplementation.GetStringValue() + "','" + clsImplementationEnum.CFARStatus.Returned_By_Closer.GetStringValue() + "')  and ProjectManager='" + objClsLoginInfo.UserName.Trim() + "'";
                        }
                        if (indextype == clsImplementationEnum.CFARIndexType.closure.GetStringValue())
                        {
                            strWhereCondition += "1=1 and Status in ( '" + clsImplementationEnum.CFARStatus.SubmittedForClosure.GetStringValue() + "','" + clsImplementationEnum.CFARStatus.Returned_By_Customer.GetStringValue() + "')  and QCManager='" + objClsLoginInfo.UserName.Trim() + "'";
                        }
                        if (indextype == clsImplementationEnum.CFARIndexType.customerclosure.GetStringValue())
                        {
                            strWhereCondition += "1=1 and Status in ( '" + clsImplementationEnum.CFARStatus.SubmittedForCustomerClosure.GetStringValue() + "','" + clsImplementationEnum.CFARStatus.Returned_By_QA_Approver.GetStringValue() + "')  and QCHead='" + objClsLoginInfo.UserName.Trim() + "'";
                        }
                        if (indextype == clsImplementationEnum.CFARIndexType.closureapproval.GetStringValue())
                        {
                            strWhereCondition += "1=1 and Status = '" + clsImplementationEnum.CFARStatus.SubmittedForClosureApproval.GetStringValue() + "' and QAManager='" + objClsLoginInfo.UserName.Trim() + "'";
                        }
                    }
                    else
                    {
                        strWhereCondition += "1=1 ";
                    }                    

                    //search Condition 

                    string[] columnName = { "CFARNumber", "Customer", "BU", "Location", "Contract", "ProjectNo", "Status",
                                        "[dbo].[GET_CUSTOMERNAMEAME_BY_CODE](Customer)" , "[dbo].[GET_ContractName_BY_Code](Contract)", "bu.t_desc", "location.t_desc" };
                    if (!string.IsNullOrEmpty(param.sSearch))
                    {
                        strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                    }

                    else
                    {
                        strWhereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                    }                                       

                    strWhereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "BU", "Location");

                    strWhereCondition += " and ProjectNo='" + Project.ToString() + "'";

                    var lstResult = db.SP_CFAR_INDEX_DATA(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();
                    lstResult = lstResult.Where(x => x.ProjectNo.Contains(Project)).ToList();
                    var data = (from uc in lstResult
                                select new[]
                               {
                            Convert.ToString(uc.HeaderId),
                            Convert.ToString(uc.CFARNumber),
                            Convert.ToString(uc.Customer),
                            Convert.ToString(uc.Contract),
                            Convert.ToString(uc.BU),
                            Convert.ToString(uc.Location),
                            Convert.ToString(uc.ProjectNo),
                            Convert.ToString(uc.Status),
                            Helper.GenerateActionIcon(uc.HeaderId, "View", "View Detail", "fa fa-eye", "",WebsiteURL +"/CFAR/MaintainCFAR/Details/"+uc.HeaderId+"?urlForm="+ indextype +"&Project="+Project,false)
                          }).ToList();

                    return Json(new
                    {
                        sEcho = Convert.ToInt32(param.sEcho),
                        iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                        iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                        aaData = data,
                        whereCondition = strWhereCondition,
                        strSortOrder = strSortOrder
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    int StartIndex = param.iDisplayStart + 1;
                    int EndIndex = param.iDisplayStart + param.iDisplayLength;
                    string strWhereCondition = string.Empty;

                    #region Datatable Sorting 
                    var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                    var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                    string strSortOrder = string.Empty;
                    string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                    string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                    if (!string.IsNullOrWhiteSpace(sortColumnName))
                    {
                        strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                    }

                    string _urlform = string.Empty;
                    string indextype = param.Department;
                    #endregion

                    if (string.IsNullOrWhiteSpace(indextype))
                    {
                        indextype = clsImplementationEnum.CFARIndexType.maintain.GetStringValue();
                    }

                    if (param.Status.ToLower() == "pending")
                    {
                        if (indextype == clsImplementationEnum.CFARIndexType.maintain.GetStringValue())
                        {
                            strWhereCondition += "1=1 and Status  in ( '" + clsImplementationEnum.CFARStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.CFARStatus.Returned_By_Validator.GetStringValue() + "') and CreatedBy='" + objClsLoginInfo.UserName.Trim() + "'";
                        }
                        if (indextype == clsImplementationEnum.CFARIndexType.validate.GetStringValue())
                        {
                            strWhereCondition += "1=1 and Status in ( '" + clsImplementationEnum.CFARStatus.SubmittedForValidate.GetStringValue() + "','" + clsImplementationEnum.CFARStatus.Returned_By_Assigner.GetStringValue() + "')  and MarketingManager='" + objClsLoginInfo.UserName.Trim() + "'";
                        }
                        if (indextype == clsImplementationEnum.CFARIndexType.invalidate.GetStringValue())
                        {
                            strWhereCondition += "1=1 and Status in ( '" + clsImplementationEnum.CFARStatus.SubmittedForInvalidation.GetStringValue() + "')  and ClusterHead='" + objClsLoginInfo.UserName.Trim() + "'";
                        }
                        if (indextype == clsImplementationEnum.CFARIndexType.assign.GetStringValue())
                        {
                            strWhereCondition += "1=1 and Status in ( '" + clsImplementationEnum.CFARStatus.SubmittedForAssign.GetStringValue() + "','" + clsImplementationEnum.CFARStatus.Returned_By_Approver.GetStringValue() + "')  and ProjectManager='" + objClsLoginInfo.UserName.Trim() + "'";
                        }
                        if (indextype == clsImplementationEnum.CFARIndexType.approve.GetStringValue())
                        {
                            strWhereCondition += "1=1 and Status in ( '" + clsImplementationEnum.CFARStatus.SubmittedForApproval.GetStringValue() + "','" + clsImplementationEnum.CFARStatus.Returned_By_Implementer.GetStringValue() + "')  and QCManager='" + objClsLoginInfo.UserName.Trim() + "'";
                        }
                        if (indextype == clsImplementationEnum.CFARIndexType.implement.GetStringValue())
                        {
                            strWhereCondition += "1=1 and Status in ( '" + clsImplementationEnum.CFARStatus.SubmittedForImplementation.GetStringValue() + "','" + clsImplementationEnum.CFARStatus.Returned_By_Closer.GetStringValue() + "')  and ProjectManager='" + objClsLoginInfo.UserName.Trim() + "'";
                        }
                        if (indextype == clsImplementationEnum.CFARIndexType.closure.GetStringValue())
                        {
                            strWhereCondition += "1=1 and Status in ( '" + clsImplementationEnum.CFARStatus.SubmittedForClosure.GetStringValue() + "','" + clsImplementationEnum.CFARStatus.Returned_By_Customer.GetStringValue() + "')  and QCManager='" + objClsLoginInfo.UserName.Trim() + "'";
                        }
                        if (indextype == clsImplementationEnum.CFARIndexType.customerclosure.GetStringValue())
                        {
                            strWhereCondition += "1=1 and Status in ( '" + clsImplementationEnum.CFARStatus.SubmittedForCustomerClosure.GetStringValue() + "','" + clsImplementationEnum.CFARStatus.Returned_By_QA_Approver.GetStringValue() + "')  and QCHead='" + objClsLoginInfo.UserName.Trim() + "'";
                        }
                        if (indextype == clsImplementationEnum.CFARIndexType.closureapproval.GetStringValue())
                        {
                            strWhereCondition += "1=1 and Status = '" + clsImplementationEnum.CFARStatus.SubmittedForClosureApproval.GetStringValue() + "' and QAManager='" + objClsLoginInfo.UserName.Trim() + "'";
                        }
                    }
                    else
                    {
                        strWhereCondition += "1=1 ";
                    }

                    //search Condition 

                    string[] columnName = { "CFARNumber", "Customer", "BU", "Location", "Contract", "ProjectNo", "Status",
                                        "[dbo].[GET_CUSTOMERNAMEAME_BY_CODE](Customer)" , "[dbo].[GET_ContractName_BY_Code](Contract)", "bu.t_desc", "location.t_desc" };
                    if (!string.IsNullOrEmpty(param.sSearch))
                    {
                        strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                    }

                    else
                    {
                        strWhereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                    }
                    strWhereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "BU", "Location");
                    var lstResult = db.SP_CFAR_INDEX_DATA(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();

                    var data = (from uc in lstResult
                                select new[]
                               {
                            Convert.ToString(uc.HeaderId),
                            Convert.ToString(uc.CFARNumber),
                            Convert.ToString(uc.Customer),
                            Convert.ToString(uc.Contract),
                            Convert.ToString(uc.BU),
                            Convert.ToString(uc.Location),
                            Convert.ToString(uc.ProjectNo),
                            Convert.ToString(uc.Status),
                            Helper.GenerateActionIcon(uc.HeaderId, "View", "View Detail", "fa fa-eye", "",WebsiteURL +"/CFAR/MaintainCFAR/Details/"+uc.HeaderId+"?urlForm="+ indextype ,false)
                          }).ToList();

                    return Json(new
                    {
                        sEcho = Convert.ToInt32(param.sEcho),
                        iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                        iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                        aaData = data,
                        whereCondition = strWhereCondition,
                        strSortOrder = strSortOrder
                    }, JsonRequestBehavior.AllowGet);
                }

                  
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Add header
        [SessionExpireFilter]
        public ActionResult Details(int id = 0, string urlForm = "",string Project="")
        {
            CFA001 objCFA001 = new CFA001();
            string Heading = string.Empty;
            List<string> lstrole = objClsLoginInfo.GetUserRoleList();
            List<ReturnedData> lstReturnDetails = new List<ReturnedData>();
            ModelWQRequest objModelWQRequest = new ModelWQRequest();
            if (id > 0)
            {


                objCFA001 = db.CFA001.Where(x => x.HeaderId == id).FirstOrDefault();
                ViewBag.Customer = Manager.GetCustomerName(objCFA001.Customer);
                ViewBag.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objCFA001.BU).Select(a => a.t_desc).FirstOrDefault();
                ViewBag.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objCFA001.Location).Select(a => a.t_desc).FirstOrDefault();
                //ViewBag.Contract = Manager.GetContractorAndDescription(objCFA001.Contract);
                ViewBag.Contract = (from cm004 in db.COM004
                                    join cm005 in db.COM005 on cm004.t_cono equals cm005.t_cono
                                    where cm004.t_ofbp == objCFA001.Customer && (cm004.t_cono == objCFA001.Contract)
                                    select cm004.t_cono + "-" + cm005.t_desc).FirstOrDefault();

                ViewBag.MarketingManager = !string.IsNullOrWhiteSpace(objCFA001.MarketingManager) ? Manager.GetPsidandDescription(objCFA001.MarketingManager) : null;
                ViewBag.ProjectManager = !string.IsNullOrWhiteSpace(objCFA001.ProjectManager) ? Manager.GetPsidandDescription(objCFA001.ProjectManager) : null;
                ViewBag.QCManager = !string.IsNullOrWhiteSpace(objCFA001.QCManager) ? Manager.GetPsidandDescription(objCFA001.QCManager) : null;
                ViewBag.QAManager = !string.IsNullOrWhiteSpace(objCFA001.QAManager) ? Manager.GetPsidandDescription(objCFA001.QAManager) : null;
                ViewBag.QCHead = !string.IsNullOrWhiteSpace(objCFA001.QCHead) ? Manager.GetPsidandDescription(objCFA001.QCHead) : null;
                ViewBag.ClusterHead = !string.IsNullOrWhiteSpace(objCFA001.ClusterHead) ? Manager.GetPsidandDescription(objCFA001.ClusterHead) : null;

                #region Return list

                if (!string.IsNullOrWhiteSpace(objCFA001.ValidateReturnBy))
                {
                    lstReturnDetails.Add(new ReturnedData()
                    {
                        ReturnedRemarks = objCFA001.ValidateReturnRemarks,
                        ReturnedState = clsImplementationEnum.CFARStatus.Returned_By_Validator.GetStringValue(),
                        ReturnedBY = Manager.GetPsidandDescription(objCFA001.ValidateReturnBy),
                        ReturnedOn = objCFA001.ValidateReturnOn
                    });
                }
                if (!string.IsNullOrWhiteSpace(objCFA001.AssignReturnBy))
                {
                    lstReturnDetails.Add(new ReturnedData()
                    {
                        ReturnedRemarks = objCFA001.AssignReturnRemarks,
                        ReturnedState = clsImplementationEnum.CFARStatus.Returned_By_Assigner.GetStringValue(),
                        ReturnedBY = Manager.GetPsidandDescription(objCFA001.AssignReturnBy),
                        ReturnedOn = objCFA001.AssignReturnOn
                    });
                }
                if (!string.IsNullOrWhiteSpace(objCFA001.ApproveReturnBy))
                {
                    lstReturnDetails.Add(new ReturnedData()
                    {
                        ReturnedRemarks = objCFA001.ApproveReturnRemarks,
                        ReturnedState = clsImplementationEnum.CFARStatus.Returned_By_Approver.GetStringValue(),
                        ReturnedBY = Manager.GetPsidandDescription(objCFA001.ApproveReturnBy),
                        ReturnedOn = objCFA001.ApproveReturnOn
                    });
                }
                if (!string.IsNullOrWhiteSpace(objCFA001.ImplementReturnBy))
                {
                    lstReturnDetails.Add(new ReturnedData()
                    {
                        ReturnedRemarks = objCFA001.ImplementReturnRemarks,
                        ReturnedState = clsImplementationEnum.CFARStatus.Returned_By_Implementer.GetStringValue(),
                        ReturnedBY = Manager.GetPsidandDescription(objCFA001.ImplementReturnBy),
                        ReturnedOn = objCFA001.ImplementReturnOn
                    });
                }
                if (!string.IsNullOrWhiteSpace(objCFA001.ClosureReturnBy))
                {
                    lstReturnDetails.Add(new ReturnedData()
                    {
                        ReturnedRemarks = objCFA001.ClosureReturnRemarks,
                        ReturnedState = clsImplementationEnum.CFARStatus.Returned_By_Closer.GetStringValue(),
                        ReturnedBY = Manager.GetPsidandDescription(objCFA001.ClosureReturnBy),
                        ReturnedOn = objCFA001.ClosureReturnOn
                    });
                }
                if (!string.IsNullOrWhiteSpace(objCFA001.CustomerClosureReturnBy))
                {
                    lstReturnDetails.Add(new ReturnedData()
                    {
                        ReturnedRemarks = objCFA001.CustomerClosureReturnRemarks,
                        ReturnedState = clsImplementationEnum.CFARStatus.Returned_By_Customer.GetStringValue(),
                        ReturnedBY = Manager.GetPsidandDescription(objCFA001.CustomerClosureReturnBy),
                        ReturnedOn = objCFA001.CustomerClosureReturnOn
                    });
                }
                if (!string.IsNullOrWhiteSpace(objCFA001.ClosureApprovalReturnBy))
                {
                    lstReturnDetails.Add(new ReturnedData()
                    {
                        ReturnedRemarks = objCFA001.ClosureApprovalReturnRemarks,
                        ReturnedState = clsImplementationEnum.CFARStatus.Returned_By_QA_Approver.GetStringValue(),
                        ReturnedBY = Manager.GetPsidandDescription(objCFA001.ClosureApprovalReturnBy),
                        ReturnedOn = objCFA001.ClosureApprovalReturnOn
                    });
                }
                #endregion

                ViewBag.Action = "edit";
            }
            else
            {
                objCFA001.CFARNumber = GetNextCFARNo();
                objCFA001.Status = clsImplementationEnum.CFARStatus.Draft.GetStringValue();
            }
            ViewBag.yesNo = clsImplementationEnum.getyesno().ToArray();

            #region status wise heading
            string status = objCFA001.Status;
            bool Linkagebutton = true;
            string RCANo = string.Empty;
            string RCAUrl = string.Empty;
            string RCAstatus = string.Empty;
            bool isRCAApplicableRCAstatus = false;
            string isRCAApplicable = Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.IsRCAApplicable.GetStringValue());

            clsHelper.ResponseMsgWithStatus objResponseMsgRCA = new clsHelper.ResponseMsgWithStatus();
            if (Linkagebutton && isRCAApplicable == "true")
            {
                DirectoryService ds = new DirectoryService();
                var upsno = ds.Encrypt(Convert.ToInt64(objClsLoginInfo.UserName).ToString("X"));

                objResponseMsgRCA = (new GeneralController()).RCAStatus("V", "CFAR", objCFA001.HeaderId);
                var host = Convert.ToString(Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.RCA_URL.GetStringValue()));
                RCAUrl = host + "?upsno=" + upsno + "&ReferenceNo=" + objCFA001.HeaderId + "&IncidentType=CFAR";
                RCANo = objResponseMsgRCA.Value;
                RCAstatus = !string.IsNullOrWhiteSpace(objResponseMsgRCA.dataValue) ? (objResponseMsgRCA.dataValue.TrimStart().TrimEnd()) : string.Empty;

                isRCAApplicableRCAstatus = (RCAstatus == clsImplementationEnum.RCAStatus.Completed.GetStringValue());
                ViewBag.RCA_Insert_URL = System.Configuration.ConfigurationManager.AppSettings["RCA_Insert_URL"];
            }
            if (isRCAApplicable == "false")
            { isRCAApplicableRCAstatus = true; }
            //  ViewBag.link = "upsno=" + ds.Encrypt(Convert.ToInt64(objClsLoginInfo).ToString("X"));
            if (status == clsImplementationEnum.CFARStatus.Draft.GetStringValue() || status == clsImplementationEnum.CFARStatus.Returned_By_Validator.GetStringValue())
            {
                ViewBag.buttonname = "submit";
                Linkagebutton = false;
                Heading += "<p><span style='margin-left:2px; font-size: larger;'>1. Kindly fill in the following details to create the CFAR</span></p>"
                        + "<p><span style='margin-left:2px; font-size: larger;'>2. Assign the CFAR to the Marketing Manager (MKT-2 or higher) for validation</span></p>"
                        + "<p><span style='margin-left:2px; font-size: larger;'>3. Do not combine the customer complaints across project. For each equipment within a customer complaint there should be a separate CFAR</span></p>";
            }
            if (objCFA001 != null)
            {
                if (status == clsImplementationEnum.CFARStatus.SubmittedForValidate.GetStringValue() || status == clsImplementationEnum.CFARStatus.Returned_By_Assigner.GetStringValue())
                {
                    if (objCFA001.MarketingManager.Trim() == objClsLoginInfo.UserName.Trim() && urlForm == clsImplementationEnum.CFARIndexType.validate.GetStringValue())
                    {
                        ViewBag.buttonname = "validate";
                    }
                    ViewBag.Linkagebutton = false;
                    Heading += "<p><span style='margin-left:2px; font-size: larger;'>Examine the CFAR for Validation. Criteria for Invalidating:</span></p>"
                            + "<p><span style='margin-left:2px; font-size: larger;'>1. The CFAR is trivial in nature and does not warrant going through the entire process to respond to the complaint</span></p>"
                            + "<p><span style='margin-left:2px; font-size: larger;'>2. The CFAR does not warrant any action at all. The problem can be dealt with by talking to the customer and no action is required</span></p>"
                            + "<p><span style='margin-left:2px; font-size: larger;'>If CFAR is Valid, ensure that the CFAR entry contains the relevant project number and comprehensive description of the customer complaint</span></p>";
                }
                if (status == clsImplementationEnum.CFARStatus.SubmittedForInvalidation.GetStringValue())
                {
                    if (objCFA001.ClusterHead.Trim() == objClsLoginInfo.UserName.Trim() && urlForm == clsImplementationEnum.CFARIndexType.invalidate.GetStringValue())
                    {
                        ViewBag.buttonname = "invalidate";
                    }
                }
                if (status == clsImplementationEnum.CFARStatus.SubmittedForAssign.GetStringValue() || status == clsImplementationEnum.CFARStatus.Returned_By_Approver.GetStringValue())
                {
                    if (isRCAApplicableRCAstatus && objCFA001.ProjectManager.Trim() == objClsLoginInfo.UserName.Trim() && urlForm == clsImplementationEnum.CFARIndexType.assign.GetStringValue())
                    {
                        ViewBag.buttonname = "assign";
                    }
                    Heading += "<p><span style='margin-left:2px; font-size: larger;'>1. Arrange for a meeting of the Cross Functional Team (CFT) to explain the nature of Customer Complaint and to decide the action plan</span></p>"
                            + "<p><span style='margin-left:2px; font-size: larger;'>2. CFT to prepare the Rectification Proposal, RCA and CAPA. A scanned copy of the of each document signed by the CFT should be uploaded in PLM.</span></p>"
                            + "<p><span style='margin-left:2px; font-size: larger;'>3. Validate the Location field</span></p>"
                            + "<p><span style='margin-left:2px; font-size: larger;'>4. The names of the CFT members should be written in full (do not use initials)</span></p>";
                }
                if (status == clsImplementationEnum.CFARStatus.SubmittedForApproval.GetStringValue() || status == clsImplementationEnum.CFARStatus.Returned_By_Implementer.GetStringValue())
                {
                    if (objCFA001.QCManager.Trim() == objClsLoginInfo.UserName.Trim() && lstrole.Any(x => x.ToString() == clsImplementationEnum.UserRoleName.QC2.GetStringValue()) && urlForm == clsImplementationEnum.CFARIndexType.approve.GetStringValue())
                    {
                        ViewBag.buttonname = "qcapprove";
                    }
                    Heading += "<p><span style='margin-left:2px; font-size: larger;'>Check the Rectification, RCA and CAPA and approve if found ok</span></p>";
                }
                if (status == clsImplementationEnum.CFARStatus.SubmittedForImplementation.GetStringValue() || status == clsImplementationEnum.CFARStatus.Returned_By_Closer.GetStringValue())
                {
                    if (objCFA001.ProjectManager.Trim() == objClsLoginInfo.UserName.Trim() && lstrole.Any(x => x.ToString() == clsImplementationEnum.UserRoleName.PMG2.GetStringValue()) && urlForm == clsImplementationEnum.CFARIndexType.implement.GetStringValue())
                    {
                        ViewBag.buttonname = "implement";
                    }
                    Heading += "<p><span style='margin-left:2px; font-size: larger;'>Review rectification once completed. The details of rectification and the inspection reports are to be uploaded.</span></p>";
                }
                if (status == clsImplementationEnum.CFARStatus.SubmittedForClosure.GetStringValue() || status == clsImplementationEnum.CFARStatus.Returned_By_Customer.GetStringValue())
                {
                    if (objCFA001.QCManager.Trim() == objClsLoginInfo.UserName.Trim() && lstrole.Any(x => x.ToString() == clsImplementationEnum.UserRoleName.QC2.GetStringValue()) && urlForm == clsImplementationEnum.CFARIndexType.closure.GetStringValue())
                    {
                        ViewBag.buttonname = "qcclosure";
                    }
                    Heading += "<p><span style='margin-left:2px; font-size: larger;'>1. If the rectification done is not in line with the approved Rectification plan, demote CFAR to PMG with comments </span></p>"
                            + "<p><span style='margin-left:2px; font-size: larger;'>2. If the rectification done is satisfactory, all CFARs categorized as High severity shall be discussed in the Quality Review Meeting (QRM) for validation of RCA and CAPA</span></p>";
                }
                if (status == clsImplementationEnum.CFARStatus.SubmittedForCustomerClosure.GetStringValue() || status == clsImplementationEnum.CFARStatus.Returned_By_QA_Approver.GetStringValue())
                {
                    if (objCFA001.QCHead.Trim() == objClsLoginInfo.UserName.Trim() && lstrole.Any(x => x.ToString() == clsImplementationEnum.UserRoleName.QI1.GetStringValue()) && urlForm == clsImplementationEnum.CFARIndexType.customerclosure.GetStringValue())
                    {
                        ViewBag.buttonname = "closurewithcustomer";
                    }
                    Heading += "<p><span style='margin-left:2px; font-size: larger;'>Discuss the Rectification conducted with the Customer and seek satisfaction confirmation for the same</span></p>";
                }
                if (status == clsImplementationEnum.CFARStatus.SubmittedForClosureApproval.GetStringValue())
                {
                    if (objCFA001.QAManager.Trim() == objClsLoginInfo.UserName.Trim() && lstrole.Any(x => x.ToString() == clsImplementationEnum.UserRoleName.QA2.GetStringValue()) && urlForm == clsImplementationEnum.CFARIndexType.closureapproval.GetStringValue())
                    {
                        ViewBag.buttonname = "qaapproval";
                    }
                    Heading += "<p><span style='margin-left:2px; font-size: larger;'>1. If changes are identified during QRM, the RCA and CAPA shall be corrected before approving in PLM.</span></p>"
                             + "<p><span style='margin-left:2px; font-size: larger;'>2. Validate the RCA and CAPA</span></p>";
                }
            }
            #endregion
            List<CategoryData> CFARCategories = objModelWQRequest.GetSubCatagorywithoutBULocationCode("CFAR Category").ToList();
            ViewBag.Category = CFARCategories;

            if (lstReturnDetails.Count > 0)
            {
                ViewBag.ReturnedData = lstReturnDetails.OrderByDescending(x => x.ReturnedOn).ToList();
            }
            else
            { ViewBag.ReturnedData = null; }

            ViewBag.YesNo = clsImplementationEnum.getyesno().ToArray();
            ViewBag.Heading = Heading;
            ViewBag.formRedirect = urlForm;
            ViewBag.Title = "Customer Feedback Action Report(CFAR)";

            if (string.IsNullOrWhiteSpace(ViewBag.buttonname))
            {
                ViewBag.buttonname = "hideall";
            }
            ViewBag.RCAUrl = RCAUrl;
            ViewBag.RCANo = RCANo;
            ViewBag.RCAstatus = RCAstatus;
            ViewBag.User = objClsLoginInfo.UserName;
            ViewBag.isRCAApplicable = isRCAApplicable;
            ViewBag.chkProject = Project;
            return View(objCFA001);
        }

        [HttpPost]
        public ActionResult SaveHeader(CFA001 cfa001)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                CFA001 objCFA001 = new CFA001();
                if (cfa001.HeaderId > 0)
                {
                    objCFA001 = db.CFA001.Where(o => o.HeaderId == cfa001.HeaderId).FirstOrDefault();
                    objCFA001.Customer = cfa001.Customer;
                    objCFA001.Contract = cfa001.Contract;
                    objCFA001.BU = cfa001.BU.Trim();
                    objCFA001.Location = cfa001.Location.Trim();
                    objCFA001.MarketingManager = cfa001.MarketingManager;
                    objCFA001.ProjectNo = cfa001.ProjectNo;
                    objCFA001.ProjectDescription = cfa001.ProjectDescription;
                    objCFA001.Category = cfa001.Category;
                    objCFA001.CategoryDescription = cfa001.CategoryDescription;
                    objCFA001.Description = cfa001.Description;
                    objCFA001.FeedbackReference = cfa001.FeedbackReference;
                    objCFA001.ProjectManager = cfa001.ProjectManager;
                    objCFA001.Equipmentno = cfa001.Equipmentno;
                    objCFA001.MarketingManagerComments = cfa001.MarketingManagerComments;
                    objCFA001.QCManager = cfa001.QCManager;
                    objCFA001.QAManager = cfa001.QAManager;
                    objCFA001.CFTDetails = cfa001.CFTDetails;
                    objCFA001.RootCause = cfa001.RootCause;
                    objCFA001.ProposedCorrection = cfa001.ProposedCorrection;
                    objCFA001.ProposedCorrectionTargetDate = cfa001.ProposedCorrectionTargetDate;
                    objCFA001.ProposedCorrectionCompletedDate = cfa001.ProposedCorrectionCompletedDate;
                    objCFA001.CorrectiveAction = cfa001.CorrectiveAction;
                    objCFA001.CorrectiveActionTargetDate = cfa001.CorrectiveActionTargetDate;
                    objCFA001.CorrectiveActionCompletedDate = cfa001.CorrectiveActionCompletedDate;
                    objCFA001.PreventiveAction = cfa001.PreventiveAction;
                    objCFA001.PreventiveActionTargetDate = cfa001.PreventiveActionTargetDate;
                    objCFA001.PreventiveActionCompletedDate = cfa001.PreventiveActionCompletedDate;
                    objCFA001.QCApproveComments = cfa001.QCApproveComments;
                    objCFA001.PMGCorrectionComments = cfa001.PMGCorrectionComments;
                    objCFA001.QCClosureComments = cfa001.QCClosureComments;
                    objCFA001.QRMRequired = cfa001.QRMRequired;
                    objCFA001.QCHead = cfa001.QCHead;
                    objCFA001.DiscussedWithCustomer = cfa001.DiscussedWithCustomer;
                    objCFA001.QCHeadComment = cfa001.QCHeadComment;
                    objCFA001.QRMReference = cfa001.QRMReference;
                    objCFA001.QMSReference = cfa001.QMSReference;
                    objCFA001.QAApprovalComments = cfa001.QAApprovalComments;
                    objCFA001.EditedBy = objClsLoginInfo.UserName;
                    objCFA001.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update;
                    objResponseMsg.Status = objCFA001.Status;
                    objResponseMsg.HeaderId = objCFA001.HeaderId;
                }
                else
                {
                    objCFA001.CFARNumber = GetNextCFARNo();
                    objCFA001.Status = clsImplementationEnum.CFARStatus.Draft.GetStringValue();
                    objCFA001.Customer = cfa001.Customer;
                    objCFA001.Contract = cfa001.Contract;
                    objCFA001.BU = cfa001.BU;
                    objCFA001.Location = cfa001.Location;
                    objCFA001.MarketingManager = cfa001.MarketingManager;
                    objCFA001.ProjectNo = cfa001.ProjectNo;
                    objCFA001.ProjectDescription = cfa001.ProjectDescription;
                    objCFA001.Equipmentno = cfa001.Equipmentno;
                    objCFA001.Category = cfa001.Category;
                    objCFA001.CategoryDescription = cfa001.CategoryDescription;
                    objCFA001.Description = cfa001.Description;
                    objCFA001.FeedbackReference = cfa001.FeedbackReference;
                    objCFA001.CreatedBy = objClsLoginInfo.UserName;
                    objCFA001.CreatedOn = DateTime.Now;
                    db.CFA001.Add(objCFA001);
                    db.SaveChanges();
                    CaptureEvent(objCFA001.CFARNumber, "Created", objCFA001.CreatedBy, objCFA001.CreatedOn, "");
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert;
                    objResponseMsg.Status = objCFA001.Status;
                    objResponseMsg.HeaderId = objCFA001.HeaderId;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CaptureEvent(string cfarno, string action, string actionby, DateTime? actionon, string comments)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                CFA002 objCFA002 = new CFA002();
                objCFA002.CFARNo = cfarno;
                objCFA002.Action = action;
                objCFA002.ActionBy = actionby;
                objCFA002.ActionOn = actionon;
                objCFA002.Comments = comments;
                db.CFA002.Add(objCFA002);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Promote header
        [HttpPost]
        public ActionResult SendToNextState(int strHeaderId, string strRemark, string currentstate)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            clsHelper.ResponseMsgWithStatus objResponseMsgRCA = new clsHelper.ResponseMsgWithStatus();
            bool flgRCA = true;
            try
            {
                string nextstatus = string.Empty;
                bool IsUpdate = true;
                string action = string.Empty;
                string actionby = string.Empty;
                DateTime? actionon = DateTime.Now;
                //for email
                string emailTo = "", PromoteTo = "", PromoteBy = "", emailfrom = "";
                string toState = "";
                string indextype = "";

                CFA001 objCFA001 = db.CFA001.Where(u => u.HeaderId == strHeaderId).SingleOrDefault();
                if (objCFA001 != null)
                {
                    List<string> lstrole = objClsLoginInfo.GetUserRoleList();
                    //submit
                    if ((currentstate == clsImplementationEnum.CFARStatus.Draft.GetStringValue() || currentstate == clsImplementationEnum.CFARStatus.Returned_By_Validator.GetStringValue()))
                    {
                        objCFA001.Status = clsImplementationEnum.CFARStatus.SubmittedForValidate.GetStringValue();
                        objCFA001.SubmittedBy = objClsLoginInfo.UserName;
                        objCFA001.SubmittedOn = DateTime.Now;
                        action = clsImplementationEnum.CFARActionType.Submitted.GetStringValue();
                        actionby = objCFA001.SubmittedBy;
                        actionon = objCFA001.SubmittedOn;

                        emailTo = objCFA001.MarketingManager;
                        emailfrom = objCFA001.CreatedBy;
                        PromoteTo = clsImplementationEnum.CFARMailTemplate.Marketing_Manager.GetStringValue();
                        PromoteBy = clsImplementationEnum.CFARMailTemplate.Creater.GetStringValue();
                        toState = clsImplementationEnum.CFARMailTemplate.Validate.GetStringValue();
                        indextype = clsImplementationEnum.CFARIndexType.validate.GetStringValue();
                    }
                    //validate
                    else if ((currentstate == clsImplementationEnum.CFARStatus.SubmittedForValidate.GetStringValue() || currentstate == clsImplementationEnum.CFARStatus.Returned_By_Assigner.GetStringValue()) && lstrole.Any(x => x.ToString() == clsImplementationEnum.UserRoleName.MKT2.GetStringValue()))
                    {
                        objCFA001.Status = clsImplementationEnum.CFARStatus.SubmittedForAssign.GetStringValue();
                        objCFA001.ValidatedBy = objClsLoginInfo.UserName;
                        objCFA001.ValidatedOn = DateTime.Now;
                        action = clsImplementationEnum.CFARActionType.Validated.GetStringValue();
                        actionby = objCFA001.ValidatedBy;
                        actionon = objCFA001.ValidatedOn;

                        emailTo = objCFA001.ProjectManager;
                        emailfrom = objCFA001.MarketingManager;
                        PromoteTo = clsImplementationEnum.CFARMailTemplate.Project_Manager.GetStringValue();
                        PromoteBy = clsImplementationEnum.CFARMailTemplate.Marketing_Manager.GetStringValue();
                        toState = clsImplementationEnum.CFARMailTemplate.Assign.GetStringValue();
                        indextype = clsImplementationEnum.CFARIndexType.assign.GetStringValue();
                    }
                    //assign
                    else if ((currentstate == clsImplementationEnum.CFARStatus.SubmittedForAssign.GetStringValue() || currentstate == clsImplementationEnum.CFARStatus.Returned_By_Approver.GetStringValue()) && lstrole.Any(x => x.ToString() == clsImplementationEnum.UserRoleName.PMG2.GetStringValue()))
                    {
                        objCFA001.Status = clsImplementationEnum.CFARStatus.SubmittedForApproval.GetStringValue();
                        objCFA001.AssignedBy = objClsLoginInfo.UserName;
                        objCFA001.AssignedOn = DateTime.Now;
                        action = clsImplementationEnum.CFARActionType.Assigned.GetStringValue();
                        actionby = objCFA001.AssignedBy;
                        actionon = objCFA001.AssignedOn;

                        emailTo = objCFA001.QCManager;
                        emailfrom = objCFA001.ProjectManager;
                        PromoteTo = clsImplementationEnum.CFARMailTemplate.QC_Manager.GetStringValue();
                        PromoteBy = clsImplementationEnum.CFARMailTemplate.PMG_Manager.GetStringValue();
                        toState = clsImplementationEnum.CFARMailTemplate.Approve.GetStringValue();
                        indextype = clsImplementationEnum.CFARIndexType.approve.GetStringValue();
                    }
                    //approve
                    else if ((currentstate == clsImplementationEnum.CFARStatus.SubmittedForApproval.GetStringValue() || currentstate == clsImplementationEnum.CFARStatus.Returned_By_Implementer.GetStringValue()) && lstrole.Any(x => x.ToString() == clsImplementationEnum.UserRoleName.QC2.GetStringValue()))
                    {
                        nextstatus = clsImplementationEnum.CFARStatus.SubmittedForImplementation.GetStringValue();
                        objCFA001.Status = nextstatus;
                        objCFA001.ApprovedBy = objClsLoginInfo.UserName;
                        objCFA001.ApprovedOn = DateTime.Now;
                        action = clsImplementationEnum.CFARActionType.Approved.GetStringValue();
                        actionby = objCFA001.ApprovedBy;
                        actionon = objCFA001.ApprovedOn;

                        emailTo = objCFA001.ProjectManager;
                        emailfrom = objCFA001.QCManager;
                        PromoteTo = clsImplementationEnum.CFARMailTemplate.PMG_Manager.GetStringValue();
                        PromoteBy = clsImplementationEnum.CFARMailTemplate.QC_Manager.GetStringValue();
                        toState = clsImplementationEnum.CFARMailTemplate.Implement.GetStringValue();
                        indextype = clsImplementationEnum.CFARIndexType.implement.GetStringValue();
                    }
                    //implement
                    else if ((currentstate == clsImplementationEnum.CFARStatus.SubmittedForImplementation.GetStringValue() || currentstate == clsImplementationEnum.CFARStatus.Returned_By_Closer.GetStringValue()) && lstrole.Any(x => x.ToString() == clsImplementationEnum.UserRoleName.PMG2.GetStringValue()))
                    {
                        nextstatus = clsImplementationEnum.CFARStatus.SubmittedForClosure.GetStringValue();
                        objCFA001.Status = nextstatus;
                        objCFA001.ImplementedBy = objClsLoginInfo.UserName;
                        objCFA001.ImplementedOn = DateTime.Now;
                        action = clsImplementationEnum.CFARActionType.Implemented.GetStringValue();
                        actionby = objCFA001.ImplementedBy;
                        actionon = objCFA001.ImplementedOn;

                        emailTo = objCFA001.QCManager;
                        emailfrom = objCFA001.ProjectManager;
                        PromoteTo = clsImplementationEnum.CFARMailTemplate.QC_Manager.GetStringValue();
                        PromoteBy = clsImplementationEnum.CFARMailTemplate.PMG_Manager.GetStringValue();
                        toState = clsImplementationEnum.CFARMailTemplate.QC_Closer.GetStringValue();
                        indextype = clsImplementationEnum.CFARIndexType.closure.GetStringValue();
                    }
                    //Closure
                    else if ((currentstate == clsImplementationEnum.CFARStatus.SubmittedForClosure.GetStringValue() || currentstate == clsImplementationEnum.CFARStatus.Returned_By_Customer.GetStringValue()) && lstrole.Any(x => x.ToString() == clsImplementationEnum.UserRoleName.QC2.GetStringValue()))
                    {
                        nextstatus = clsImplementationEnum.CFARStatus.SubmittedForCustomerClosure.GetStringValue();
                        objCFA001.Status = nextstatus;
                        objCFA001.QCClosureBy = objClsLoginInfo.UserName;
                        objCFA001.QCClosureOn = DateTime.Now;
                        action = clsImplementationEnum.CFARActionType.QC_Closed.GetStringValue();
                        actionby = objCFA001.QCClosureBy;
                        actionon = objCFA001.QCClosureOn;

                        emailTo = objCFA001.QCHead;
                        emailfrom = objCFA001.QCManager;
                        PromoteTo = clsImplementationEnum.CFARMailTemplate.QC_Head.GetStringValue();
                        PromoteBy = clsImplementationEnum.CFARMailTemplate.QC_Manager.GetStringValue();
                        toState = clsImplementationEnum.CFARMailTemplate.Closure_with_Customer.GetStringValue();
                        indextype = clsImplementationEnum.CFARIndexType.customerclosure.GetStringValue();
                    }
                    //custmer close
                    else if ((currentstate == clsImplementationEnum.CFARStatus.SubmittedForCustomerClosure.GetStringValue() || currentstate == clsImplementationEnum.CFARStatus.Returned_By_QA_Approver.GetStringValue()) && lstrole.Any(x => x.ToString() == clsImplementationEnum.UserRoleName.QI1.GetStringValue()))
                    {
                        nextstatus = clsImplementationEnum.CFARStatus.SubmittedForClosureApproval.GetStringValue();
                        objCFA001.Status = nextstatus;
                        objCFA001.CustomerClosureBy = objClsLoginInfo.UserName;
                        objCFA001.CustomerClosureOn = DateTime.Now;
                        action = clsImplementationEnum.CFARActionType.Customer_Closed.GetStringValue();
                        actionby = objCFA001.CustomerClosureBy;
                        actionon = objCFA001.CustomerClosureOn;

                        emailTo = objCFA001.QAManager;
                        emailfrom = objCFA001.QCHead;
                        PromoteTo = clsImplementationEnum.CFARMailTemplate.QA_Manager.GetStringValue();
                        PromoteBy = clsImplementationEnum.CFARMailTemplate.QC_Head.GetStringValue();
                        toState = clsImplementationEnum.CFARMailTemplate.QA_Approval.GetStringValue();
                        indextype = clsImplementationEnum.CFARIndexType.closureapproval.GetStringValue();
                    }
                    //release
                    else if (currentstate == clsImplementationEnum.CFARStatus.SubmittedForClosureApproval.GetStringValue() && lstrole.Any(x => x.ToString() == clsImplementationEnum.UserRoleName.QA2.GetStringValue()))
                    {
                        nextstatus = clsImplementationEnum.CFARStatus.Closed.GetStringValue();
                        objCFA001.Status = nextstatus;
                        objCFA001.ClosureApprovedBy = objClsLoginInfo.UserName;
                        objCFA001.ClosureApprovedOn = DateTime.Now;
                        action = clsImplementationEnum.CFARActionType.Closed.GetStringValue();
                        actionby = objCFA001.ClosureApprovedBy;
                        actionon = objCFA001.ClosureApprovedOn;

                        emailTo = objCFA001.CreatedBy;
                        emailfrom = objCFA001.QAManager;
                        PromoteTo = clsImplementationEnum.CFARMailTemplate.Create.GetStringValue();
                        PromoteBy = clsImplementationEnum.CFARMailTemplate.QA_Manager.GetStringValue();
                        toState = clsImplementationEnum.CFARMailTemplate.Released.GetStringValue();
                        indextype = clsImplementationEnum.CFARIndexType.maintain.GetStringValue();
                    }
                    else
                    {
                        IsUpdate = false;
                    }
                    if (IsUpdate)
                    {
                        string isRCAApplicable = Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.IsRCAApplicable.GetStringValue());
                        if (isRCAApplicable == "true")
                        {
                            if (currentstate == clsImplementationEnum.CFARStatus.SubmittedForValidate.GetStringValue())
                            {
                                var bu = db.COM002.Where(x => x.t_dimx == objCFA001.BU).Select(a => a.t_desc).FirstOrDefault();
                                var deptname = Manager.GetDepartmentByPsno(objCFA001.ProjectManager);
                                objResponseMsgRCA = (new GeneralController()).RCAStatus("I", "CFAR", objCFA001.HeaderId, objCFA001.Location, bu, objCFA001.ProjectNo, objCFA001.ValidatedOn.Value.ToString("yyyy-MM-dd"), objCFA001.Description, objCFA001.ProjectManager, deptname, string.Empty);
                                if (!objResponseMsgRCA.Key)
                                {
                                    flgRCA = false;
                                }
                            }
                        }
                        if (flgRCA)
                        {
                            db.SaveChanges();
                            CaptureEvent(objCFA001.CFARNumber, action, actionby, actionon, "");
                            objResponseMsg.ActionKey = true;
                            if (objCFA001.Status == clsImplementationEnum.CFARStatus.Closed.GetStringValue())
                            {
                                objResponseMsg.ActionValue = "CFAR Closed successfully";
                            }
                            else
                            {
                                objResponseMsg.ActionValue = string.Format(clsImplementationMessage.CommonMessages.SubmitedDetails, objCFA001.CFARNumber, objCFA001.Status);
                            }
                            #region Send Mail

                            string CFARNo = objCFA001.CFARNumber;

                            if (!string.IsNullOrWhiteSpace(emailTo))
                            {
                                Hashtable _ht = new Hashtable();
                                EmailSend _objEmail = new EmailSend();
                                //_ht["[CC]"] = ccTo;
                                _ht["[CFARNo]"] = CFARNo;
                                _ht["[ToPerson]"] = Manager.GetUserNameFromPsNo(emailTo);
                                _ht["[PromoteTo]"] = PromoteTo;
                                _ht["[PromotedBy]"] = PromoteBy;
                                _ht["[State]"] = toState;
                                _ht["[Link]"] = WebsiteURL + "/CFAR/MaintainCFAR/Details/" + objCFA001.HeaderId + "?urlForm=" + indextype;
                                _ht["[FromPerson]"] = Manager.GetUserNameFromPsNo(emailfrom);

                                MAIL001 objTemplateMaster = new MAIL001();
                                if (objCFA001.Status != clsImplementationEnum.CFARStatus.Closed.GetStringValue())
                                {
                                    objTemplateMaster = db.MAIL001.Where(ii => ii.TamplateName == MailTemplates.CFAR.PromoteCFAR).SingleOrDefault();
                                }
                                else
                                {
                                    objTemplateMaster = db.MAIL001.Where(ii => ii.TamplateName == MailTemplates.CFAR.ReleaseCFAR).SingleOrDefault();
                                }
                                _objEmail.MailToAdd = Manager.GetMailIdFromPsNo(emailTo);
                                _objEmail.SendMail(_objEmail, _ht, objTemplateMaster);
                            }
                            #endregion
                        }
                        else
                        {
                            objResponseMsg.ActionKey = false;
                            objResponseMsg.ActionValue = "RCA not generated.Please try again";
                        }
                    }
                    else
                    {
                        objResponseMsg.ActionKey = false;
                        objResponseMsg.ActionValue = "Details not submitted.Please try again";
                    }
                }
                else
                {
                    objResponseMsg.ActionKey = true;
                    objResponseMsg.ActionValue = "Details not available.";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SendToInvalidateState(int strHeaderId, string clusterhead, string currentstate)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                CFA001 objCFA001 = db.CFA001.Where(u => u.HeaderId == strHeaderId).FirstOrDefault();
                if (objCFA001 != null)
                {
                    if (currentstate == clsImplementationEnum.CFARStatus.SubmittedForValidate.GetStringValue() || currentstate == clsImplementationEnum.CFARStatus.Returned_By_Assigner.GetStringValue())
                    {
                        objCFA001.ClusterHead = clusterhead;
                        objCFA001.Status = clsImplementationEnum.CFARStatus.SubmittedForInvalidation.GetStringValue();
                        objCFA001.InvalidatedBy = objClsLoginInfo.UserName;
                        objCFA001.InvalidatedOn = DateTime.Now;
                    }
                    db.SaveChanges();
                    CaptureEvent(objCFA001.CFARNumber, clsImplementationEnum.CFARActionType.Invalidated.GetStringValue(), objCFA001.InvalidatedBy, objCFA001.InvalidatedOn, "");
                    objResponseMsg.ActionKey = true;
                    objResponseMsg.ActionValue = string.Format(clsImplementationMessage.CommonMessages.SubmitedDetails, objCFA001.CFARNumber, objCFA001.Status);
                }
                else
                {
                    objResponseMsg.ActionKey = true;
                    objResponseMsg.ActionValue = "Details not available.";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ApproveInvalidate(int strHeaderId, string remark)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                CFA001 objCFA001 = db.CFA001.Where(u => u.HeaderId == strHeaderId).FirstOrDefault();
                if (objCFA001 != null)
                {
                    objCFA001.InvalidateRejectionComments = remark;
                    objCFA001.Status = clsImplementationEnum.CFARStatus.Closed.GetStringValue();
                    objCFA001.InvalidateApprovedBy = objClsLoginInfo.UserName;
                    objCFA001.InvalidateApprovedOn = DateTime.Now;
                    db.SaveChanges();
                    CaptureEvent(objCFA001.CFARNumber, clsImplementationEnum.CFARActionType.Closed.GetStringValue(), objCFA001.InvalidateApprovedBy, objCFA001.InvalidateApprovedOn, "");

                    objResponseMsg.ActionKey = true;
                    objResponseMsg.ActionValue = "CFAR Closed successfully";
                }
                else
                {
                    objResponseMsg.ActionKey = true;
                    objResponseMsg.ActionValue = "Details not available.";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult RejectInvalidate(int strHeaderId, string remark)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                CFA001 objCFA001 = db.CFA001.Where(u => u.HeaderId == strHeaderId).FirstOrDefault();
                if (objCFA001 != null)
                {
                    objCFA001.InvalidateRejectionComments = remark;
                    objCFA001.Status = clsImplementationEnum.CFARStatus.SubmittedForValidate.GetStringValue();
                    objCFA001.InvalidateRejectedBy = objClsLoginInfo.UserName;
                    objCFA001.InvalidateRejectedOn = DateTime.Now;
                    db.SaveChanges();
                    CaptureEvent(objCFA001.CFARNumber, clsImplementationEnum.CFARStatus.SubmittedForValidate.GetStringValue(), objCFA001.InvalidateRejectedBy, objCFA001.InvalidateRejectedOn, "");

                    objResponseMsg.ActionKey = true;
                    objResponseMsg.ActionValue = string.Format(clsImplementationMessage.CommonMessages.SubmitedDetails, objCFA001.CFARNumber, objCFA001.Status);
                }
                else
                {
                    objResponseMsg.ActionKey = true;
                    objResponseMsg.ActionValue = "Details not available.";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Demote Header
        [HttpPost]
        public ActionResult SendToPreviousState(int strHeaderId, string strRemark, string currentstate)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                string demotestatus = string.Empty;
                bool IsUpdate = true;
                string action = string.Empty;
                string actionby = string.Empty;
                DateTime? actionon = DateTime.Now;

                string emailTo = "", DemoteTo = "", DemotedBy = "", emailfrom = "";
                string toState = "";
                string indextype = "";

                CFA001 objCFA001 = db.CFA001.Where(u => u.HeaderId == strHeaderId).SingleOrDefault();
                if (objCFA001 != null)
                {
                    List<string> lstrole = objClsLoginInfo.GetUserRoleList();
                    //return to draft
                    if ((currentstate == clsImplementationEnum.CFARStatus.SubmittedForValidate.GetStringValue() || currentstate == clsImplementationEnum.CFARStatus.Returned_By_Assigner.GetStringValue()) && lstrole.Any(x => x.ToString() == clsImplementationEnum.UserRoleName.MKT2.GetStringValue()))
                    {
                        objCFA001.Status = clsImplementationEnum.CFARStatus.Returned_By_Validator.GetStringValue();
                        objCFA001.ValidateReturnRemarks = strRemark;
                        objCFA001.ValidateReturnBy = objClsLoginInfo.UserName;
                        objCFA001.ValidateReturnOn = DateTime.Now;
                        action = clsImplementationEnum.CFARActionType.Returned_By_Validator.GetStringValue();
                        actionby = objCFA001.ValidateReturnBy;
                        actionon = objCFA001.ValidateReturnOn;

                        emailTo = objCFA001.CreatedBy;
                        emailfrom = objCFA001.MarketingManager;
                        DemoteTo = clsImplementationEnum.CFARMailTemplate.Creater.GetStringValue();
                        DemotedBy = clsImplementationEnum.CFARMailTemplate.Marketing_Manager.GetStringValue();
                        toState = clsImplementationEnum.CFARMailTemplate.Create.GetStringValue();
                        indextype = clsImplementationEnum.CFARIndexType.maintain.GetStringValue();
                    }
                    //return to validate
                    else if ((currentstate == clsImplementationEnum.CFARStatus.SubmittedForAssign.GetStringValue() || currentstate == clsImplementationEnum.CFARStatus.Returned_By_Approver.GetStringValue()) && lstrole.Any(x => x.ToString() == clsImplementationEnum.UserRoleName.PMG2.GetStringValue()))
                    {
                        objCFA001.Status = clsImplementationEnum.CFARStatus.Returned_By_Assigner.GetStringValue();
                        objCFA001.AssignReturnRemarks = strRemark;
                        objCFA001.AssignReturnBy = objClsLoginInfo.UserName;
                        objCFA001.AssignReturnOn = DateTime.Now;
                        action = clsImplementationEnum.CFARActionType.Returned_By_Assigner.GetStringValue();
                        actionby = objCFA001.AssignReturnBy;
                        actionon = objCFA001.AssignReturnOn;

                        emailTo = objCFA001.MarketingManager;
                        emailfrom = objCFA001.ProjectManager;
                        DemoteTo = clsImplementationEnum.CFARMailTemplate.Marketing_Manager.GetStringValue();
                        DemotedBy = clsImplementationEnum.CFARMailTemplate.Project_Manager.GetStringValue();
                        toState = clsImplementationEnum.CFARMailTemplate.Validate.GetStringValue();
                        indextype = clsImplementationEnum.CFARIndexType.validate.GetStringValue();
                    }
                    //return to assign
                    else if ((currentstate == clsImplementationEnum.CFARStatus.SubmittedForApproval.GetStringValue() || currentstate == clsImplementationEnum.CFARStatus.Returned_By_Implementer.GetStringValue()) && lstrole.Any(x => x.ToString() == clsImplementationEnum.UserRoleName.QC2.GetStringValue()))
                    {
                        demotestatus = clsImplementationEnum.CFARStatus.Returned_By_Approver.GetStringValue();
                        objCFA001.Status = demotestatus;
                        objCFA001.ApproveReturnRemarks = strRemark;
                        objCFA001.ApproveReturnBy = objClsLoginInfo.UserName;
                        objCFA001.ApproveReturnOn = DateTime.Now;
                        action = clsImplementationEnum.CFARActionType.Returned_By_Approver.GetStringValue();
                        actionby = objCFA001.ApproveReturnBy;
                        actionon = objCFA001.ApproveReturnOn;

                        emailTo = objCFA001.ProjectManager;
                        emailfrom = objCFA001.QCManager;
                        DemoteTo = clsImplementationEnum.CFARMailTemplate.Project_Manager.GetStringValue();
                        DemotedBy = clsImplementationEnum.CFARMailTemplate.QC_Manager.GetStringValue();
                        toState = clsImplementationEnum.CFARMailTemplate.Assign.GetStringValue();
                        indextype = clsImplementationEnum.CFARIndexType.assign.GetStringValue();
                    }
                    //return to approve
                    else if ((currentstate == clsImplementationEnum.CFARStatus.SubmittedForImplementation.GetStringValue() || currentstate == clsImplementationEnum.CFARStatus.Returned_By_Closer.GetStringValue()) && lstrole.Any(x => x.ToString() == clsImplementationEnum.UserRoleName.PMG2.GetStringValue()))
                    {
                        demotestatus = clsImplementationEnum.CFARStatus.Returned_By_Implementer.GetStringValue();
                        objCFA001.Status = demotestatus;
                        objCFA001.ImplementReturnRemarks = strRemark;
                        objCFA001.ImplementReturnBy = objClsLoginInfo.UserName;
                        objCFA001.ImplementReturnOn = DateTime.Now;
                        action = clsImplementationEnum.CFARActionType.Returned_By_Implementer.GetStringValue();
                        actionby = objCFA001.ImplementReturnBy;
                        actionon = objCFA001.ImplementReturnOn;

                        emailTo = objCFA001.QCManager;
                        emailfrom = objCFA001.ProjectManager;
                        DemoteTo = clsImplementationEnum.CFARMailTemplate.QC_Manager.GetStringValue();
                        DemotedBy = clsImplementationEnum.CFARMailTemplate.PMG_Manager.GetStringValue();
                        toState = clsImplementationEnum.CFARMailTemplate.Approve.GetStringValue();
                        indextype = clsImplementationEnum.CFARIndexType.approve.GetStringValue();
                    }
                    //return to implement
                    else if ((currentstate == clsImplementationEnum.CFARStatus.SubmittedForClosure.GetStringValue() || currentstate == clsImplementationEnum.CFARStatus.Returned_By_Customer.GetStringValue()) && lstrole.Any(x => x.ToString() == clsImplementationEnum.UserRoleName.QC2.GetStringValue()))
                    {
                        demotestatus = clsImplementationEnum.CFARStatus.Returned_By_Closer.GetStringValue();
                        objCFA001.Status = demotestatus;
                        objCFA001.ClosureReturnRemarks = strRemark;
                        objCFA001.ClosureReturnBy = objClsLoginInfo.UserName;
                        objCFA001.ClosureReturnOn = DateTime.Now;
                        action = clsImplementationEnum.CFARActionType.Returned_By_Closer.GetStringValue();
                        actionby = objCFA001.ClosureReturnBy;
                        actionon = objCFA001.ClosureReturnOn;

                        emailTo = objCFA001.ProjectManager;
                        emailfrom = objCFA001.QCManager;
                        DemoteTo = clsImplementationEnum.CFARMailTemplate.PMG_Manager.GetStringValue();
                        DemotedBy = clsImplementationEnum.CFARMailTemplate.QC_Manager.GetStringValue();
                        toState = clsImplementationEnum.CFARMailTemplate.Implement.GetStringValue();
                        indextype = clsImplementationEnum.CFARIndexType.implement.GetStringValue();
                    }
                    //return to qc closure
                    else if ((currentstate == clsImplementationEnum.CFARStatus.SubmittedForCustomerClosure.GetStringValue() || currentstate == clsImplementationEnum.CFARStatus.Returned_By_QA_Approver.GetStringValue()) && lstrole.Any(x => x.ToString() == clsImplementationEnum.UserRoleName.QI1.GetStringValue()))
                    {
                        demotestatus = clsImplementationEnum.CFARStatus.Returned_By_Customer.GetStringValue();
                        objCFA001.Status = demotestatus;
                        objCFA001.CustomerClosureReturnRemarks = strRemark;
                        objCFA001.CustomerClosureReturnBy = objClsLoginInfo.UserName;
                        objCFA001.CustomerClosureReturnOn = DateTime.Now;
                        action = clsImplementationEnum.CFARActionType.Returned_By_Customer.GetStringValue();
                        actionby = objCFA001.CustomerClosureReturnBy;
                        actionon = objCFA001.CustomerClosureReturnOn;

                        emailTo = objCFA001.QCManager;
                        emailfrom = objCFA001.QCHead;
                        DemoteTo = clsImplementationEnum.CFARMailTemplate.QC_Manager.GetStringValue();
                        DemotedBy = clsImplementationEnum.CFARMailTemplate.QC_Head.GetStringValue();
                        toState = clsImplementationEnum.CFARMailTemplate.QC_Closer.GetStringValue();
                        indextype = clsImplementationEnum.CFARIndexType.closure.GetStringValue();
                    }
                    //return to customer closure
                    else if (currentstate == clsImplementationEnum.CFARStatus.SubmittedForClosureApproval.GetStringValue() && lstrole.Any(x => x.ToString() == clsImplementationEnum.UserRoleName.QA2.GetStringValue()))
                    {
                        demotestatus = clsImplementationEnum.CFARStatus.Returned_By_QA_Approver.GetStringValue();
                        objCFA001.Status = demotestatus;
                        objCFA001.ClosureApprovalReturnRemarks = strRemark;
                        objCFA001.ClosureApprovalReturnBy = objClsLoginInfo.UserName;
                        objCFA001.ClosureApprovalReturnOn = DateTime.Now;
                        action = clsImplementationEnum.CFARActionType.Returned_By_QA_Approver.GetStringValue();
                        actionby = objCFA001.ClosureApprovalReturnBy;
                        actionon = objCFA001.ClosureApprovalReturnOn;

                        emailTo = objCFA001.QCHead;
                        emailfrom = objCFA001.QAManager;
                        DemoteTo = clsImplementationEnum.CFARMailTemplate.QC_Head.GetStringValue();
                        DemotedBy = clsImplementationEnum.CFARMailTemplate.QA_Manager.GetStringValue();
                        toState = clsImplementationEnum.CFARMailTemplate.Closure_with_Customer.GetStringValue();
                        indextype = clsImplementationEnum.CFARIndexType.customerclosure.GetStringValue();
                    }
                    else
                    {
                        IsUpdate = false;
                    }
                    if (IsUpdate)
                    {
                        db.SaveChanges();
                        CaptureEvent(objCFA001.CFARNumber, action, actionby, actionon, strRemark);
                        objResponseMsg.ActionKey = true;
                        objResponseMsg.ActionValue = clsImplementationMessage.CommonMessages.Return;
                        #region Send Mail

                        string CFARNo = objCFA001.CFARNumber;

                        if (!string.IsNullOrWhiteSpace(emailTo))
                        {
                            Hashtable _ht = new Hashtable();
                            EmailSend _objEmail = new EmailSend();
                            //_ht["[CC]"] = ccTo;
                            _ht["[CFARNo]"] = CFARNo;
                            _ht["[ToPerson]"] = Manager.GetUserNameFromPsNo(emailTo);
                            _ht["[DemoteTo]"] = DemoteTo;
                            _ht["[DemotedBy]"] = DemotedBy;
                            _ht["[State]"] = toState;
                            _ht["[Link]"] = WebsiteURL + "/CFAR/MaintainCFAR/Details/" + objCFA001.HeaderId + "?urlForm=" + indextype;
                            _ht["[FromPerson]"] = Manager.GetUserNameFromPsNo(emailfrom);

                            MAIL001 objTemplateMaster = new MAIL001();
                            objTemplateMaster = db.MAIL001.Where(ii => ii.TamplateName == MailTemplates.CFAR.DemoteCFAR).SingleOrDefault();
                            _objEmail.MailToAdd = Manager.GetMailIdFromPsNo(emailTo);
                            _objEmail.SendMail(_objEmail, _ht, objTemplateMaster);
                        }
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.ActionKey = false;
                        objResponseMsg.ActionValue = "Please try again";
                    }
                }
                else
                {
                    objResponseMsg.ActionKey = true;
                    objResponseMsg.ActionValue = "Details not available.";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Reposible Persons
        [HttpPost]
        public ActionResult GetReposiblePersonsHtml(int HeaderId)
        {
            CFA001 objCFA001 = db.CFA001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            return PartialView("_ResponsiblePersonPartial", objCFA001);
        }
        #endregion

        #region Timeline History

        public ActionResult GetTimelineHistoryPopup(string CFARNo)
        {
            ViewBag.CFARNo = CFARNo;
            return PartialView("_GetTimelineGridDataPartial");
        }

        [HttpPost]
        public JsonResult LoadTimelineDataGrid(JQueryDataTableParamModel param)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhereCondition = " 1=1 ";

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                string _urlform = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                //Search Condition 
                if (!string.IsNullOrWhiteSpace(param.Status))
                {
                    strWhereCondition += " AND CFARNo = '" + param.Status + "'";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] columnName = { "Action", "[dbo].[GET_USERNAME_BY_PSNO](ActionBy)" };
                    strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }

                var lstResult = db.SP_CFAR_TIMELINE_DATA(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.Action),
                            Convert.ToString(uc.ActionBy),
                            (uc.ActionOn.HasValue ? uc.ActionOn.Value.ToString("dd/MM/yyyy hh:mm tt") :  "")
                          }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Utility
        public string GetNextCFARNo()
        {
            string strRequestNo = string.Empty;
            int num = 1;
            var CFARNumber = db.CFA001.OrderByDescending(x => x.HeaderId).Select(x => x.CFARNumber).FirstOrDefault();
            if (CFARNumber != null)
            {
                num = Convert.ToInt32(CFARNumber.Replace("CFAR", "")) + 1;
                strRequestNo = "CFAR" + num.ToString("D6");
            }
            else
            {
                strRequestNo = "CFAR" + num.ToString("D6");
            }
            return strRequestNo;
        }

        [HttpPost]
        public ActionResult GetAllCustomer(string term)
        {
            List<CategoryData> lstCustomer = new List<CategoryData>();
            if (!string.IsNullOrWhiteSpace(term))
            {
                lstCustomer = (from cm004 in db.COM004
                               join cm006 in db.COM006 on cm004.t_ofbp equals cm006.t_bpid
                               where cm004.t_ofbp.Contains(term) || cm006.t_nama.Contains(term)
                               select new CategoryData { id = cm004.t_ofbp, text = cm004.t_ofbp + "-" + cm006.t_nama }).Distinct().ToList();
            }
            else
            {
                lstCustomer = (from cm004 in db.COM004
                               join cm006 in db.COM006 on cm004.t_ofbp equals cm006.t_bpid
                               select new CategoryData { id = cm004.t_ofbp, text = cm004.t_ofbp + "-" + cm006.t_nama }).Distinct().Take(10).ToList();
            }
            return Json(lstCustomer, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetAllBU(string term)
        {
            List<CategoryData> lstBU = new List<CategoryData>();
            int BU = Convert.ToInt32(clsImplementationEnum.BLDNumber.BU.GetStringValue());
            //var lstBUs = Manager.getBUsByID(BU);
            if (!string.IsNullOrWhiteSpace(term))
            {
                lstBU = (from ath1 in db.ATH001
                         join com2 in db.COM002 on ath1.BU equals com2.t_dimx
                         where ath1.Employee.Trim() == objClsLoginInfo.UserName.Trim()
                                && ath1.BU != ""
                                && (ath1.BU.Contains(term) || com2.t_desc.ToLower().Contains(term.ToLower()))
                         select new CategoryData { id = ath1.BU, text = com2.t_desc }).Distinct().ToList();
            }
            else
            {
                lstBU = (from ath1 in db.ATH001
                         join com2 in db.COM002 on ath1.BU equals com2.t_dimx
                         where ath1.Employee.Trim() == objClsLoginInfo.UserName.Trim()
                                && ath1.BU != ""
                         select new CategoryData { id = ath1.BU, text = com2.t_desc }).Distinct().ToList();
            }
            return Json(lstBU, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetAllLocation(string term)
        {
            List<CategoryData> lstLOC = new List<CategoryData>();
            int LOC = Convert.ToInt32(clsImplementationEnum.BLDNumber.LOC.GetStringValue());

            var lstLocations = Manager.getBUsByID(LOC);

            lstLOC = (from li in lstLocations
                      where li.t_dimx.ToLower().Contains(term.ToLower()) || li.t_desc.ToLower().Contains(term.ToLower())
                      select new CategoryData
                      {
                          id = li.t_dimx,
                          text = li.t_desc,
                      }).ToList();
            return Json(lstLOC, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetAllContract(string term, string customer)
        {
            List<CategoryData> lstContract = new List<CategoryData>();
            if (!string.IsNullOrWhiteSpace(term))
            {
                lstContract = (from cm004 in db.COM004
                               join cm005 in db.COM005 on cm004.t_cono equals cm005.t_cono
                               where cm004.t_ofbp == customer && (cm004.t_cono.Contains(term) || cm005.t_desc.Contains(term))
                               select new CategoryData { id = cm004.t_cono, text = cm004.t_cono + "-" + cm005.t_desc }).Distinct().ToList();
            }
            else
            {
                lstContract = (from cm004 in db.COM004
                               join cm005 in db.COM005 on cm004.t_cono equals cm005.t_cono
                               where cm004.t_ofbp == customer && cm005.t_desc != ""
                               select new CategoryData { id = cm004.t_cono, text = cm004.t_cono + "-" + cm005.t_desc }).Distinct().Take(10).ToList();
            }

            return Json(lstContract, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetAllProject(string search, string contract, string selectedBU)
        {
            List<CategoryData> lstProject = new List<CategoryData>();
            if (!string.IsNullOrWhiteSpace(search))
            {
                lstProject = (from cm001 in db.COM001
                              join cm005 in db.COM005 on cm001.t_cprj equals cm005.t_sprj
                              where cm005.t_cono == contract && (cm005.t_sprj.Contains(search) || cm005.t_desc.Contains(search))
                                    && cm001.t_entu == selectedBU
                              select new CategoryData { id = cm005.t_sprj, text = cm005.t_sprj, Description = cm005.t_desc }).ToList();
            }
            else
            {
                lstProject = (from cm001 in db.COM001
                              join cm005 in db.COM005 on cm001.t_cprj equals cm005.t_sprj
                              where cm005.t_cono == contract && cm001.t_entu == selectedBU
                              select new CategoryData { id = cm005.t_sprj, text = cm005.t_sprj, Description = cm005.t_desc }).ToList();
            }
            return Json(lstProject, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetProjectValueEdit(string search, string contract, string selectedBU, int headerId)
        {
            List<CategoryData> lstProject = new List<CategoryData>();
            if (!string.IsNullOrWhiteSpace(search))
            {
                lstProject = (from cm001 in db.COM001
                              join cm005 in db.COM005 on cm001.t_cprj equals cm005.t_sprj
                              where cm005.t_cono == contract && (cm005.t_sprj.Contains(search) || cm005.t_desc.Contains(search))
                                    && cm001.t_entu == selectedBU
                              select new CategoryData { id = cm005.t_sprj, text = cm005.t_sprj, Description = cm005.t_desc }).ToList();
            }
            else
            {
                lstProject = (from cm001 in db.COM001
                              join cm005 in db.COM005 on cm001.t_cprj equals cm005.t_sprj
                              where cm005.t_cono == contract && cm001.t_entu == selectedBU
                              select new CategoryData { id = cm005.t_sprj, text = cm005.t_sprj, Description = cm005.t_desc }).ToList();
            }
            string result = db.CFA001.Where(s => s.HeaderId == headerId).Select(s => s.ProjectNo).FirstOrDefault();
            var lstresult = result.Split(',').ToList();
            var lstProjectid = lstProject.Select(x => x.id).ToList();
            foreach (var lst in lstresult)
            {
                if (!lstProjectid.Contains(lst))
                {
                    lstProject.Add(new CategoryData { id = lst, text = lst, Description = "newData" });
                }
            }

            return Json(lstProject, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                #region Maintain header
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_CFAR_INDEX_DATA(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }

                    var newlst = (from uc in lst
                                  select new
                                  {
                                      CFARNumber = Convert.ToString(uc.CFARNumber),
                                      Customer = Convert.ToString(uc.Customer),
                                      Contract = Convert.ToString(uc.Contract),
                                      BU = Convert.ToString(uc.BU),
                                      Location = Convert.ToString(uc.Location),
                                      Status = Convert.ToString(uc.Status),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                #endregion


                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Email Notification For Pending Data

        public ActionResult PendingDataEmailNotification()
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                #region Generate excel from SSRS report
                var viewer = new ReportViewer();
                var ReportParams = new List<ReportParameter>();
                viewer = (new GeneralController()).GetReportServerCreadential(viewer, "/CFAR/CFAR Pending Data Report", ReportParams);
                string fileformat = "Excel";
                byte[] bytes = viewer.ServerReport.Render(fileformat);
                string attachedFile = Path.Combine(Server.MapPath("~/Resources/Download/"), "PendingCFAR_" + DateTime.Today.ToString("dd-MMM-yyyy") + ".xls");
                using (FileStream fs = new FileStream(attachedFile, FileMode.Create))
                {
                    fs.Write(bytes, 0, bytes.Length);
                }
                #endregion

                #region Send Mail Notification
                MAIL001 objTemplateMaster = db.MAIL001.Where(ii => ii.TamplateName == MailTemplates.CFAR.PendingCFAR).SingleOrDefault();
                Hashtable _ht = new Hashtable();
                EmailSend _objEmail = new EmailSend();
                _objEmail.MailToAdd = Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.PendingCFAREmailTo.GetStringValue());
                _objEmail.MailCc = Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.PendingCFAREmailCC.GetStringValue());
                _ht["[ReportLink]"] = Convert.ToString(ConfigurationManager.AppSettings["SSRS_Server_URL"]) + "Pages/ReportViewer.aspx?%2fReports%2fIEMQS%2fCFAR%2fCFAR+Pending+Data+Report&rs:Command=Render";
                _ht["[ReportDatetime]"] = DateTime.Now.ToString();
                _objEmail.MailAttachment = attachedFile;
                _objEmail.IsAttachmentDelete = true;
                _objEmail.SendMail(_objEmail, _ht, objTemplateMaster);
                #endregion

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

    }
}