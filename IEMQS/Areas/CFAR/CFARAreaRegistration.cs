﻿using System.Web.Mvc;

namespace IEMQS.Areas.CFAR
{
    public class CFARAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "CFAR";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "CFAR_default",
                "CFAR/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}