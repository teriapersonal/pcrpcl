﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IEMQS.Areas.IMB.Models
{
    public class HeaderModel
    {
        public string Project { get; set;}
    }
    public class IMBProjectDetail
    {
        public string Document { get; set; }
        public string EquipmentName { get; set; }
        public string Customer { get; set; }
        public string Condition { get; set; }
        public int? HeaderId { get; set; }
        public string Product { get; set; }
        public string ProcessLicensor { get; set; }
        public string ApprovedBy { get; set; }
        public string Status { get; set; }
    }
}