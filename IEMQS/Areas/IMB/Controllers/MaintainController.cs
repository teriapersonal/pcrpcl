﻿using IEMQS.Areas.IMB.Models;
using IEMQS.Models;
using IEMQS.Areas.Utility.Models;
using IEMQSImplementation;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using static IEMQS.Areas.HTC.Controllers.MaintainHTCController;
using System.Globalization;

namespace IEMQS.Areas.IMB.Controllers
{
    public class MaintainController : clsBase
    {
        #region Header

        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult GetProjects(string search)
        {
            try
            {
                var items = from li in db.COM001
                            where li.t_cprj.Contains(search) || li.t_dsca.Contains(search)
                            select new
                            {
                                id = li.t_cprj,
                                text = li.t_cprj + "-" + li.t_dsca
                            };
                return Json(items, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetApprover(string search)
        {
            try
            {
                return Json(Manager.GetApprover(clsImplementationEnum.UserRoleName.PLNG2.GetStringValue() + "," + clsImplementationEnum.UserRoleName.PLNG1.GetStringValue(), objClsLoginInfo.Location, true, objClsLoginInfo.UserName, search), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetProduct()
        {
            List<string> lstproductCategory = clsImplementationEnum.getProductCategory().ToList();
            var items = (from li in lstproductCategory
                         select new
                         {
                             id = li.ToString(),
                             text = li.ToString()
                         });
            return Json(items, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetApproverEdit()
        {
            try
            {
                var PLNG2 = clsImplementationEnum.UserRoleName.PLNG2.GetStringValue();
                List<SP_GET_APPROVERBYROLEANDLOCATION_Result> items = db.SP_GET_APPROVERBYROLEANDLOCATION(PLNG2, objClsLoginInfo.Location, 1, objClsLoginInfo.UserName, "").ToList();
                return Json(items, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult loadHeaderDataTable(JQueryDataTableParamModel param, string status)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";
                //whereCondition += " and CreatedBy = " + objClsLoginInfo.UserName;
                if (status.ToUpper() == "PENDING")
                {
                    whereCondition += " and status in('" + clsImplementationEnum.IMBStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.IMBStatus.Returned.GetStringValue() + "')";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] columnName = { "lcom2.t_desc", "imb1.Project +'-'+ com1.t_dsca", "com6.t_bpid+'-'+com6.t_nama", "Product", "ProcessLicensor", "Status", "Document" };
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstHeader = db.SP_IMB_GETHEADER(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from h in lstHeader
                          select new[] {
                           h.Project,
                                    Convert.ToString(h.HeaderId),
                                    Convert.ToString( h.Document),
                                    Convert.ToString( "R"+h.RevNo),
                                    Convert.ToString( h.Customer),
                                    Convert.ToString( h.Product),
                                    Convert.ToString( h.ProcessLicensor),
                                    Convert.ToString( h.LocationDesc),
                                    Convert.ToString(h.Status),
                                    Convert.ToString(h.SubmittedBy),
                                    h.SubmittedOn == null || h.SubmittedOn.Value == DateTime.MinValue ? "NA" : h.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                    Convert.ToString(h.ApprovedBy),
                                    h.ApprovedOn == null || h.ApprovedOn.Value == DateTime.MinValue ? "NA" : h.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
 //  "<center><a class='iconspace' href='javascript:void(O);' title='View' onclick='EditHeader(" + h.HeaderId + ")'><i class='fa fa-eye'></i></a><i title='Print' style='margin-left:5px;cursor:pointer;' class='fa fa-print' onClick='PrintReport("+h.HeaderId+")'></i>"
                           //+"  "+(h.RevNo>0?"<i title='History' style='margin-left:5px;cursor:pointer;' class='fa fa-history' onClick='GetHistoryDetailsPartial("+h.HeaderId+")'></i> "+"<a title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/IMB/Maintain/ShowTimeline?HeaderID=" + Convert.ToInt32(h.HeaderId) + "')><i class='fa fa-clock-o'></i></a>":"</center>"),//Convert.ToString(h.HeaderId)

                           "<nobr><center>"+
                           "<a class='iconspace' href='javascript:void(O);' title='View' onclick='EditHeader(" + h.HeaderId + ")'><i class='fa fa-eye'></i></a>"+
                           Helper.GenerateActionIcon(h.HeaderId,"Delete","Delete Record","fa fa-trash-o", "DeleteDocument("+ h.HeaderId +",'/IMB/Maintain/DeleteHeader', {headerid:"+h.HeaderId+"}, 'tblHeader')","",  (( h.RevNo >0 && h.Status.ToLower() != clsImplementationEnum.CommonStatus.SendForApprovel.GetStringValue().ToLower()) || ( h.RevNo == 0 && h.Status.ToLower() == clsImplementationEnum.CommonStatus.Approved.GetStringValue().ToLower()) ) ? false:true) +
                             "<i class ='iconspace fa fa-clock-o' title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/IMB/Maintain/ShowTimeline?HeaderID=" + Convert.ToInt32(h.HeaderId) + "')></i>"+
                           (h.RevNo>0?"<i title='History' class='iconspace fa fa-history' onClick='GetHistoryDetailsPartial("+h.HeaderId+")'></i> ":"<i title='History' class='disabledicon fa fa-history' ></i>" )+
                            "<i title='Print'  class='iconspace fa fa-print' onClick='PrintReport("+h.HeaderId+")'></i>"+
                           "</center></nobr>",//Convert.ToString(h.HeaderId)
                           
                    };
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult ShowHistoryTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();

            model.TimelineTitle = "IMB TimeLine";
            model.Title = "PDinDoc";
            if (HeaderId > 0)
            {

                IMB001_Log IMB001_Log = db.IMB001_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                model.CreatedBy = IMB001_Log.CreatedBy != null ? Manager.GetUserNameFromPsNo(IMB001_Log.CreatedBy) : null;
                model.CreatedOn = IMB001_Log.CreatedOn;
                model.EditedBy = IMB001_Log.EditedBy != null ? Manager.GetUserNameFromPsNo(IMB001_Log.EditedBy) : null;
                model.EditedOn = IMB001_Log.EditedOn;
                model.SubmittedBy = IMB001_Log.SubmittedBy != null ? Manager.GetUserNameFromPsNo(IMB001_Log.SubmittedBy) : null;
                model.SubmittedOn = IMB001_Log.SubmittedOn;

                model.ApprovedBy = IMB001_Log.ApprovedBy != null ? Manager.GetUserNameFromPsNo(IMB001_Log.ApprovedBy) : null;
                model.ApprovedOn = IMB001_Log.ApprovedOn;

            }
            else
            {

                IMB001_Log IMB001_Log = db.IMB001_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                model.CreatedBy = IMB001_Log.CreatedBy != null ? Manager.GetUserNameFromPsNo(IMB001_Log.CreatedBy) : null;
                model.CreatedOn = IMB001_Log.CreatedOn;
                model.EditedBy = IMB001_Log.EditedBy != null ? Manager.GetUserNameFromPsNo(IMB001_Log.EditedBy) : null;
                model.EditedOn = IMB001_Log.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
        public ActionResult ShowTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();
            model.Title = "PDinDoc";
            model.TimelineTitle = "IMB Timeline";

            if (HeaderId > 0)
            {
                IMB001 OBJIMB001 = db.IMB001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.CreatedBy = OBJIMB001.CreatedBy != null ? Manager.GetUserNameFromPsNo(OBJIMB001.CreatedBy) : null;
                model.CreatedOn = OBJIMB001.CreatedOn;
                model.EditedBy = OBJIMB001.EditedBy != null ? Manager.GetUserNameFromPsNo(OBJIMB001.EditedBy) : null;
                model.EditedOn = OBJIMB001.EditedOn;

                // model.SubmittedBy = PMB001_Log.SendToCompiledOn != null ? Manager.GetUserNameFromPsNo(PMB001_Log.SendToCompiledOn) : null;
                //model.SubmittedOn = PMB001_Log.SendToCompiledOn;
                model.SubmittedBy = OBJIMB001.SubmittedBy != null ? Manager.GetUserNameFromPsNo(OBJIMB001.SubmittedBy) : null;
                model.SubmittedOn = OBJIMB001.SubmittedOn;
                //model.CompiledBy = PMB001_Log.CompiledBy != null ? Manager.GetUserNameFromPsNo(PMB001_Log.CompiledBy) : null;
                // model.CompiledOn = PMB001_Log.CompiledOn;
                model.ApprovedBy = OBJIMB001.ApprovedBy != null ? Manager.GetUserNameFromPsNo(OBJIMB001.ApprovedBy) : null;
                model.ApprovedOn = OBJIMB001.ApprovedOn;

            }
            else
            {
                IMB001 OBJIMB001 = db.IMB001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.CreatedBy = OBJIMB001.CreatedBy != null ? Manager.GetUserNameFromPsNo(OBJIMB001.CreatedBy) : null;
                model.CreatedOn = OBJIMB001.CreatedOn;
                model.EditedBy = OBJIMB001.EditedBy != null ? Manager.GetUserNameFromPsNo(OBJIMB001.EditedBy) : null;
                model.EditedOn = OBJIMB001.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
        public ActionResult GetHeaderGridDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetHeaderGridDataPartial");
        }
        [SessionExpireFilter]
        public ActionResult AddHeader(int? headerId)
        {
            IMB001 objIMB = new IMB001();
            try
            {
                if (headerId != null && headerId > 0)
                {
                    objIMB = db.IMB001.FirstOrDefault(x => x.HeaderId == headerId);
                    var lstResult = (from a in db.ATH001
                                     join b in db.ATH004 on a.Role equals b.Id
                                     where a.Employee.Trim().Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase)
                                     select new { RoleId = a.Role, RoleDesc = b.Role }).ToList();
                    if (lstResult.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PMG3.GetStringValue(), StringComparison.OrdinalIgnoreCase)
                                     || i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PMG2.GetStringValue(), StringComparison.OrdinalIgnoreCase)
                                     || i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PMG1.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                    {
                        ViewBag.display = "false";
                    }
                    else
                    {
                        ViewBag.display = "true";
                    }
                    //if (objIMB == null)
                    //{
                    //    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                    //}
                    ViewBag.Customer = Manager.GetCustomerCodeAndNameByProject(objIMB.Project);
                    ViewBag.EquipmentName = db.COM001.Where(x => x.t_cprj == objIMB.Project).Select(x => x.t_dsca).FirstOrDefault();
                    ViewBag.ApproverName = objIMB.ApprovedBy + '-' + Manager.GetUserNameFromPsNo(objIMB.ApprovedBy);
                    ViewBag.Action = "edit";

                    var PlanningDinID = db.PDN002.Where(x => x.RefId == headerId && x.DocumentNo == objIMB.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                    ViewBag.PlanningDinID = PlanningDinID;

                    ViewBag.IsReviseBtnEnabled = Manager.IsReviseEnabled(PlanningDinID, objIMB.HeaderId, objIMB.Document);
                    ViewBag.DocMessage = clsImplementationMessage.CommonMessages.DocMessage.ToString();
                }
                else
                {
                    objIMB.HeaderId = 0;
                    objIMB.Status = clsImplementationEnum.QCPStatus.Draft.GetStringValue();
                    objIMB.RevNo = 0;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View(objIMB);
        }
        [HttpPost]
        public async Task<ActionResult> EditHeader(int headerId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                IMB001 objIMB001 = await db.IMB001.FirstOrDefaultAsync(x => x.HeaderId == headerId);
                if (objIMB001 != null)
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = objIMB001.Status;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Document Not Found";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
                //throw;
            }
            return Json(objResponseMsg);
        }
        [HttpPost]
        public ActionResult DeleteHeader(int headerid)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                objResponseMsg = Manager.DeletePDINDocument(headerid, clsImplementationEnum.PlanList.Improvement_Budget_Plan);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public async Task<ActionResult> GetProjectDetail(string project)
        {
            IMBProjectDetail objProjectDetail = new IMBProjectDetail();
            if (!string.IsNullOrWhiteSpace(project))
            {

                IMB001 objHeader = await db.IMB001.Where(i => i.Project == project).FirstOrDefaultAsync();
                try
                {
                    if (objHeader != null && objHeader.CreatedBy == objClsLoginInfo.UserName && objHeader.Status == clsImplementationEnum.IMBStatus.Approved.GetStringValue())
                    {
                        objProjectDetail.Condition = "approved";
                    }
                    if (objHeader != null && objHeader.CreatedBy == objClsLoginInfo.UserName && objHeader.Status == clsImplementationEnum.IMBStatus.SentForApproval.GetStringValue())
                    {
                        objProjectDetail.Condition = "sentforapproval";
                    }
                    else if (objHeader != null && objHeader.CreatedBy == objClsLoginInfo.UserName && (objHeader.Status == clsImplementationEnum.IMBStatus.Draft.GetStringValue() || objHeader.Status == clsImplementationEnum.IMBStatus.Returned.GetStringValue()))
                    {
                        objProjectDetail.Condition = "edit";
                    }
                    else if (objHeader != null && objHeader.CreatedBy != objClsLoginInfo.UserName)
                    {
                        objProjectDetail.Condition = "otheruser";
                    }
                    else
                    {
                        objProjectDetail.Condition = "ok";
                        objProjectDetail.Customer = Manager.GetCustomerCodeAndNameByProject(project);
                        objProjectDetail.EquipmentName = await db.COM001.Where(x => x.t_cprj == project).Select(x => x.t_dsca).FirstOrDefaultAsync();
                        objProjectDetail.Document = "H02_" + project.GetSubstring(4) + "_PL03";
                    }
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    throw;
                }
                if (objProjectDetail.Condition == "approved" || objProjectDetail.Condition == "edit")
                {
                    objProjectDetail.Customer = Manager.GetCustomerCodeAndNameByProject(project);
                    objProjectDetail.EquipmentName = await db.COM001.Where(x => x.t_cprj == project).Select(x => x.t_dsca).FirstOrDefaultAsync();
                    objProjectDetail.Document = "H02_" + project.GetSubstring(4) + "_PL03";
                    objProjectDetail.HeaderId = objHeader.HeaderId;
                    objProjectDetail.Product = objHeader.Product;
                    objProjectDetail.ProcessLicensor = objHeader.ProcessLicensor;
                    objProjectDetail.ApprovedBy = objHeader.ApprovedBy;
                    objProjectDetail.Status = objHeader.Status;
                }
            }
            return Json(objProjectDetail);
        }
        [HttpPost]
        public ActionResult SaveHeader_R0(FormCollection fc)
        {
            string status = clsImplementationEnum.IMBStatus.Draft.GetStringValue();
            int revNo = 0;
            IMB001 objIMB001 = new IMB001();
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (fc != null)
                {
                    int headerId = Convert.ToInt32(fc["HeaderId"]);
                    string LoginUser = objClsLoginInfo.UserName;
                    if (headerId > 0)
                    {
                        objIMB001 = db.IMB001.Where(i => i.HeaderId == headerId).FirstOrDefault();
                        if (objIMB001.Status != clsImplementationEnum.IMBStatus.SentForApproval.GetStringValue())
                        {
                            objIMB001.Document = fc["Document"];
                            objIMB001.Customer = fc["Customer"].Split('-')[0].Trim();
                            objIMB001.Product = fc["Product"];
                            objIMB001.ProcessLicensor = fc["ProcessLicensor"];
                            if (objIMB001.Status == clsImplementationEnum.IMBStatus.Approved.GetStringValue())
                            {
                                objIMB001.RevNo = objIMB001.RevNo + 1;
                                objIMB001.Remarks = null;
                                objIMB001.ApprovedOn = null;
                            }
                            objIMB001.ReviseRemark = fc["ReviseRemark"];
                            objIMB001.Status = clsImplementationEnum.IMBStatus.Draft.GetStringValue();
                            objIMB001.ApprovedBy = fc["ApprovedBy"];
                            objIMB001.EditedBy = LoginUser;
                            objIMB001.EditedOn = DateTime.Now;
                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = objIMB001.HeaderId.ToString();
                            var folderLoadingSketchPath = "IMB001/" + objIMB001.HeaderId + "/R" + objIMB001.RevNo;

                            //Manager.ManageDocuments(folderLoadingSketchPath, fc["hasAttachments"], fc["lsAttach"], objClsLoginInfo.UserName);

                            Manager.UpdatePDN002(objIMB001.HeaderId, objIMB001.Status, objIMB001.RevNo, objIMB001.Project, objIMB001.Document);
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "This Document is sent for approval, You can't edit now";
                        }
                        status = objIMB001.Status;
                        revNo = Convert.ToInt32(objIMB001.RevNo);
                    }
                    else
                    {
                        objIMB001.Project = fc["ddlProject"];
                        objIMB001.Document = fc["Document"];
                        objIMB001.Customer = fc["Customer"].Split('-')[0].Trim();
                        objIMB001.Product = fc["Product"];
                        objIMB001.ApprovedBy = fc["ApprovedBy"];
                        objIMB001.ProcessLicensor = fc["ProcessLicensor"];
                        objIMB001.Status = clsImplementationEnum.IMBStatus.Draft.GetStringValue();
                        objIMB001.Location = db.COM003.Where(x => x.t_psno == LoginUser && x.t_actv == 1).Select(x => x.t_loca).FirstOrDefault();
                        objIMB001.RevNo = 0;
                        objIMB001.CreatedBy = LoginUser;
                        objIMB001.CreatedOn = DateTime.Now;
                        db.IMB001.Add(objIMB001);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = objIMB001.HeaderId.ToString();
                        status = objIMB001.Status;
                        revNo = Convert.ToInt32(objIMB001.RevNo);
                        Manager.UpdatePDN002(objIMB001.HeaderId, objIMB001.Status, objIMB001.RevNo, objIMB001.Project, objIMB001.Document);

                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            var response = new { Key = objResponseMsg.Key, Value = objResponseMsg.Value, Status = status, RevNo = revNo };
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        //public ActionResult SaveHeader(IMB001 imb001, bool hasAttachments, Dictionary<string, string> Attach)
        public ActionResult SaveHeader(IMB001 imb001)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsgWithStatus = new clsHelper.ResponseMsgWithStatus();
            IMB001 objIMB001 = new IMB001();
            try
            {
                int headerId = Convert.ToInt32(imb001.HeaderId);
                string LoginUser = objClsLoginInfo.UserName;
                if (headerId > 0)
                {
                    objIMB001 = db.IMB001.Where(i => i.HeaderId == headerId).FirstOrDefault();
                    if (objIMB001.Status != clsImplementationEnum.IMBStatus.SentForApproval.GetStringValue())
                    {
                        #region Header Update
                        objIMB001.Document = imb001.Document;
                        objIMB001.Customer = imb001.Customer.Split('-')[0].Trim();
                        objIMB001.Product = imb001.Product;
                        objIMB001.ProcessLicensor = imb001.ProcessLicensor;
                        if (objIMB001.Status == clsImplementationEnum.IMBStatus.Approved.GetStringValue())
                        {
                            objIMB001.RevNo = objIMB001.RevNo + 1;
                            objIMB001.Remarks = null;
                            objIMB001.ApprovedOn = null;
                        }
                        objIMB001.ReviseRemark = imb001.ReviseRemark;
                        objIMB001.Status = clsImplementationEnum.IMBStatus.Draft.GetStringValue();
                        objIMB001.ApprovedBy = imb001.ApprovedBy;
                        objIMB001.EditedBy = LoginUser;
                        objIMB001.EditedOn = DateTime.Now;
                        db.SaveChanges();

                       // var folderPath = "IMB001/" + objIMB001.HeaderId + "/R" + objIMB001.RevNo;
                       // Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);

                        #endregion

                        objResponseMsgWithStatus.Key = true;
                        objResponseMsgWithStatus.Value = objIMB001.HeaderId.ToString();
                        objResponseMsgWithStatus.HeaderId = objIMB001.HeaderId;
                        objResponseMsgWithStatus.RevNo = objIMB001.RevNo.ToString();
                        objResponseMsgWithStatus.Status = objIMB001.Status;
                        Manager.UpdatePDN002(objIMB001.HeaderId, objIMB001.Status, objIMB001.RevNo, objIMB001.Project, objIMB001.Document);
                    }
                }
                else
                {
                    #region Header insert
                    objIMB001 = new IMB001();

                    objIMB001.Document = imb001.Document;
                    objIMB001.Customer = imb001.Customer.Split('-')[0].Trim();
                    objIMB001.Product = imb001.Product;
                    objIMB001.ProcessLicensor = imb001.ProcessLicensor;
                    if (objIMB001.Status == clsImplementationEnum.IMBStatus.Approved.GetStringValue())
                    {
                        objIMB001.RevNo = objIMB001.RevNo + 1;
                        objIMB001.Remarks = null;
                        objIMB001.ApprovedOn = null;
                    }
                    objIMB001.ReviseRemark = imb001.ReviseRemark;
                    objIMB001.Status = clsImplementationEnum.IMBStatus.Draft.GetStringValue();
                    objIMB001.ApprovedBy = imb001.ApprovedBy;
                    objIMB001.CreatedBy = LoginUser;
                    objIMB001.CreatedOn = DateTime.Now;
                    db.IMB001.Add(objIMB001);
                    db.SaveChanges();
                    objResponseMsgWithStatus.Key = true;
                    objResponseMsgWithStatus.Value = objIMB001.HeaderId.ToString();
                    objResponseMsgWithStatus.HeaderId = objIMB001.HeaderId;
                    objResponseMsgWithStatus.RevNo = objIMB001.RevNo.ToString();
                    objResponseMsgWithStatus.Status = objIMB001.Status;
                    //var folderPath = "IMB001/" + objIMB001.HeaderId + "/R" + objIMB001.RevNo;
                    // Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);

                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsgWithStatus.Key = false;
                objResponseMsgWithStatus.Value = ex.Message;
            }

            return Json(objResponseMsgWithStatus);
        }
        [HttpPost]
        public ActionResult SendForApprove(int headerId, string Approver)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            IMB001 objIMB001 = new IMB001();
            try
            {
                if (headerId > 0)
                {
                    objIMB001 = db.IMB001.Where(i => i.HeaderId == headerId).FirstOrDefault();
                    List<IMB002> lstIMB002 = db.IMB002.Where(i => i.HeaderId == headerId).ToList();
                    if (lstIMB002 != null && lstIMB002.Count == 0)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Please add improvements in this document";
                    }
                    else if (objIMB001.Status == clsImplementationEnum.IMBStatus.Returned.GetStringValue())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "This document is returned, Please edit this document and then send for approval.";
                    }
                    else if (objIMB001.Status == clsImplementationEnum.IMBStatus.Approved.GetStringValue())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "This document is already approved.";
                    }
                    else
                    {
                        objIMB001.Status = clsImplementationEnum.IMBStatus.SentForApproval.GetStringValue();
                        objIMB001.ApprovedBy = Approver;
                        objIMB001.SubmittedBy = objClsLoginInfo.UserName;
                        objIMB001.SubmittedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = objIMB001.Document + " Successfully Send For Approval";
                        Manager.UpdatePDN002(objIMB001.HeaderId, objIMB001.Status, objIMB001.RevNo, objIMB001.Project, objIMB001.Document);

                        #region Send Mail
                        Hashtable _ht = new Hashtable();
                        EmailSend _objEmail = new EmailSend();
                        _ht["[ApprovarName]"] = Manager.GetUserNameFromPsNo(objIMB001.ApprovedBy);
                        _ht["[Document]"] = objIMB001.Document;
                        _ht["[Name]"] = Manager.GetUserNameFromPsNo(objIMB001.CreatedBy);
                        MAIL001 objTemplateMaster = db.MAIL001.Where(ii => ii.TamplateName == MailTemplates.IMB.SentforApproval).SingleOrDefault();
                        if (!string.IsNullOrWhiteSpace(objTemplateMaster.Body))
                        {
                            _objEmail.MailToAdd = Manager.GetMailIdFromPsNo(objIMB001.ApprovedBy);
                            _ht["[Subject]"] = objIMB001.Document + " Is Sent For Approval";
                            _objEmail.SendMail(_objEmail, _ht, objTemplateMaster);
                        }
                        else
                        {

                        }

                        #endregion

                        #region Send Notification
                        (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG2.GetStringValue() + "," + clsImplementationEnum.UserRoleName.PLNG1.GetStringValue(), objIMB001.Project, "", "", Manager.GetPDINDocumentNotificationMsg(objIMB001.Project, clsImplementationEnum.PlanList.Improvement_Budget_Plan.GetStringValue(), objIMB001.RevNo.Value.ToString(), objIMB001.Status), clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(), Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Improvement_Budget_Plan.GetStringValue(), objIMB001.HeaderId.ToString(), true), objIMB001.ApprovedBy);
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg);
        }

        [HttpPost]
        public ActionResult ReviseHeader(int strHeaderId, string strRemarks)
        {
            CustomResponceMsg objResponseMsg = new CustomResponceMsg();
            try
            {
                IMB001 objIMB001 = db.IMB001.Where(u => u.HeaderId == strHeaderId).SingleOrDefault();
                if (objIMB001 != null)
                {
                    objIMB001.RevNo = Convert.ToInt32(objIMB001.RevNo) + 1;
                    objIMB001.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                    objIMB001.ReviseRemark = strRemarks;
                    objIMB001.EditedBy = objClsLoginInfo.UserName;
                    objIMB001.EditedOn = DateTime.Now;
                    objIMB001.Remarks = null;
                    objIMB001.ApprovedOn = null;
                    objIMB001.SubmittedBy = null;
                    objIMB001.SubmittedOn = null;
                    db.SaveChanges();
                    Manager.UpdatePDN002(objIMB001.HeaderId, objIMB001.Status, objIMB001.RevNo, objIMB001.Project, objIMB001.Document);
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderID = objIMB001.HeaderId;
                    objResponseMsg.Status = objIMB001.Status;
                    objResponseMsg.rev = objIMB001.RevNo;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Revision;
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Lines
        [HttpPost]
        public ActionResult AddLinesPartial(int headerId, int? lineId)
        {
            IMB002 objIMB002 = new IMB002();

            if (lineId != null && lineId > 0)
            {
                objIMB002 = db.IMB002.FirstOrDefault(x => x.HeaderId == headerId && x.LineId == lineId);
                ViewBag.TeamLead = objIMB002.TeamLead + "-" + Manager.GetUserNameFromPsNo(objIMB002.TeamLead);
                ViewBag.Action = "edit";
            }
            else
            {
                objIMB002.HeaderId = headerId;
            }

            return PartialView("_AddLinesPartial", objIMB002);
        }

        [HttpPost]
        public ActionResult AddAttachment(int headerId, int? lineId)
        {
            IMB002 objIMB002 = new IMB002();

            if (lineId != null && lineId > 0)
            {
                objIMB002 = db.IMB002.FirstOrDefault(x => x.HeaderId == headerId && x.LineId == lineId);
                ViewBag.Visibility = "true";
            }
            else
            {
                ViewBag.Visibility = "true";
                objIMB002.HeaderId = headerId;
            }
            return PartialView("~/Areas/IMB/Views/Shared/_LineAttachment.cshtml", objIMB002);
        }

        [HttpPost]
        public ActionResult LoadPlannerLineStatus(int headerId)
        {
            IMB001 objIMB001 = new IMB001();

            if (headerId > 0)
            {
                objIMB001 = db.IMB001.FirstOrDefault(x => x.HeaderId == headerId);
            }

            return PartialView("_LoadPlannerLineStatus", objIMB001);
        }

        public ActionResult loadLinesDataTable(JQueryDataTableParamModel param, int headerId)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string condition = string.Empty;
                condition = "HeaderId = " + headerId;

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    condition += " and ( ImprovementDescription like '%" + param.sSearch + "%' or TeamMember like '%" + param.sSearch + "%' or Comments like '%" + param.sSearch + "%' or TeamFormation like '%" + param.sSearch + "%'  or TeamLead like '%" + param.sSearch + "%')";
                }
                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion


                var lstLines = db.SP_IMB_GETLINES(StartIndex, EndIndex, strSortOrder, condition).ToList();
                int? totalRecords = lstLines.Select(i => i.TotalCount).FirstOrDefault();

                var res = from c in lstLines
                          select new[] {
                                        Convert.ToString(c.ROW_NO),
                                        Convert.ToString(c.HeaderId),
                                        Convert.ToString(c.LineId),
                                        Convert.ToString(c.ImprovementDescription),
                                        Convert.ToString(c.TeamLead),
                                        Convert.ToString(c.TeamLeadName),
                                        Convert.ToString(c.TeamMember),
                                        Convert.ToString(c.TeamMemberName),
                                        Convert.ToDateTime( c.TargetDate).ToString("dd/MM/yyyy"),
                                        Convert.ToString(c.TeamFormation),
                                        Convert.ToString(c.Comments)  ,
                                        generateDropdown(c.LineId,c.Complete,param.IsVisible),
                                        Convert.ToString(c.LineId),
                                        GetAttachment( c.LineId),
                          };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = condition
                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""

                }, JsonRequestBehavior.AllowGet);
            }
        }
        public string GetAttachment(int id)
        {
            string attachment;
            //var folderPath = "IMB002/" + id;
            //var existing = (new clsFileUpload()).GetDocuments(folderPath);

            IEMQS.Areas.Utility.Controllers.FileUploadController _obj = new Utility.Controllers.FileUploadController();
            if (!_obj.CheckAnyDocumentsExits("IMB002", id))
            {
                attachment = "False";
            }
            else
            {
                attachment = "True";
            }
            return attachment;
        }


        public string generateDropdown(int LineId, string complete, bool IsVisible)
        {
            string strDropdown = "";

            List<SelectListItem> lstStatus = new List<SelectListItem>(clsImplementationEnum.getIMBLineStatus().Select(x => new SelectListItem { Text = x.ToString(), Value = x.ToString() }));

            if (IsVisible)
            {

                strDropdown = Helper.GenerateDropdown(LineId, "Complete", new SelectList(lstStatus, "Value", "Text", complete), "Select", "UpdateRecord(this," + LineId + ")", false, "");
            }
            else
            {
                strDropdown = complete;
            }
            return strDropdown;

        }

        [HttpPost]
        public ActionResult UpdateDetails(int lineId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                string tableName = "IMB002";

                if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
                {
                    if (columnValue == "Short")
                    {
                        columnValue = clsImplementationEnum.IMBLineStatus.Short_Closed.GetStringValue();
                    }
                    db.SP_COMMON_LINES_UPDATE(lineId, columnName, columnValue, tableName, objClsLoginInfo.UserName);

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetTeamLeader(string search)
        {
            try
            {
                search = search.ToLower();
                var items = (from li in db.COM003
                             where  li.t_actv == 1 && (li.t_psno.ToLower().Contains(search) || li.t_name.ToLower().Contains(search))
                             select new
                             {
                                 id = li.t_psno,
                                 text = li.t_psno + "-" + li.t_name,
                             }).ToList();
                return Json(items, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetTeamLeaderEdit()
        {
            try
            {
                var items = (from li in db.COM003
                             where //li.t_loca == objClsLoginInfo.Location && 
                             li.t_actv == 1
                             select new
                             {
                                 id = li.t_psno,
                                 text = li.t_psno + "-" + li.t_name,
                             }).ToList();
                return Json(items, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetTeamMember(string search, string param)
        {
            try
            {
                var items = (from li in db.COM003
                             where  li.t_actv == 1 && li.t_psno != param && (li.t_psno.ToLower().Contains(search) || li.t_name.ToLower().Contains(search))
                             select new
                             {
                                 id = li.t_psno,
                                 text = li.t_psno + "-" + li.t_name,
                             }).ToList();

                return Json(items, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetTeamMemberEdit(string param)
        {
            try
            {
                var items = (from li in db.COM003
                             where //li.t_loca == objClsLoginInfo.Location && 
                             li.t_psno != param && li.t_actv == 1
                             select new
                             {
                                 id = li.t_psno,
                                 text = li.t_psno + "-" + li.t_name,
                             }).ToList();
                return Json(items, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetTeamFormation()
        {
            try
            {
                List<string> lstTeamFormation = clsImplementationEnum.getIMBTeamFormation().ToList();
                var items = (from li in lstTeamFormation
                             select new
                             {
                                 id = li.ToString(),
                                 text = li.ToString()
                             });
                return Json(items, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult SaveLines(FormCollection fc)
        {
            string status = clsImplementationEnum.IMBStatus.Draft.GetStringValue();
            int revNo = 0;
            IMB002 objIMB002 = new IMB002();
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {

                if (fc != null)
                {
                    int headerId = Convert.ToInt32(fc["HeaderId"]);
                    IMB001 objIMB001 = new IMB001();
                    if (headerId > 0)
                    {
                        objIMB001 = db.IMB001.Where(i => i.HeaderId == headerId).FirstOrDefault();
                        if (objIMB001.Status != clsImplementationEnum.IMBStatus.SentForApproval.GetStringValue() || objIMB001.ApprovedBy == objClsLoginInfo.UserName)
                        {
                            string LoginUser = objClsLoginInfo.UserName;
                            int lineId = Convert.ToInt32(fc["LineId"]);
                            if (lineId > 0)
                            {
                                objIMB002 = db.IMB002.Where(i => i.HeaderId == headerId && i.LineId == lineId).FirstOrDefault();
                                objIMB002.Project = objIMB001.Project;
                                objIMB002.Document = objIMB001.Document;
                                objIMB002.ImprovementDescription = fc["ImprovementDescription"];
                                objIMB002.TeamMember = fc["TeamMember"];
                                objIMB002.TeamLead = fc["TeamLead"];
                                objIMB002.TargetDate = Convert.ToDateTime(fc["TargetDate"]);
                                objIMB002.TeamFormation = fc["TeamFormation"];
                                objIMB002.Comments = fc["Comments"];
                                objIMB002.EditedBy = LoginUser;
                                objIMB002.EditedOn = DateTime.Now;
                                db.SaveChanges();

                                objResponseMsg.Key = true;
                                objResponseMsg.Value = objIMB001.HeaderId.ToString();
                            }
                            else
                            {
                                objIMB002.HeaderId = objIMB001.HeaderId;
                                objIMB002.Project = objIMB001.Project;
                                objIMB002.Document = objIMB001.Document;
                                objIMB002.ImprovementDescription = fc["ImprovementDescription"];
                                objIMB002.TeamMember = fc["TeamMember"];
                                objIMB002.TeamLead = fc["TeamLead"];
                                objIMB002.TargetDate = Convert.ToDateTime(fc["TargetDate"]);
                                objIMB002.TeamFormation = fc["TeamFormation"];
                                objIMB002.Comments = fc["Comments"];
                                objIMB002.CreatedBy = LoginUser;
                                objIMB002.CreatedOn = DateTime.Now;
                                db.IMB002.Add(objIMB002);
                                db.SaveChanges();
                                objResponseMsg.Key = true;
                                objResponseMsg.Value = objIMB001.HeaderId.ToString();
                            }
                            if (objResponseMsg.Key == true && objIMB001.Status == clsImplementationEnum.IMBStatus.Approved.GetStringValue())
                            {
                                objIMB001.RevNo = objIMB001.RevNo + 1;
                                objIMB001.Remarks = null;
                                objIMB001.ApprovedOn = null;
                            }
                            if (objIMB001.ApprovedBy != objClsLoginInfo.UserName)
                            {
                                objIMB001.Status = clsImplementationEnum.IMBStatus.Draft.GetStringValue();
                            }
                            db.SaveChanges();
                            status = objIMB001.Status;
                            revNo = Convert.ToInt32(objIMB001.RevNo);
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "Document is sent for approval, you can not add lines now";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            var response = new { Key = objResponseMsg.Key, Value = objResponseMsg.Value, Status = status, RevNo = revNo };
            return Json(response, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public async Task<ActionResult> DeleteLines(int headerId, int lineId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            IMB001 objIMB001 = await db.IMB001.FirstOrDefaultAsync(x => x.HeaderId == headerId);
            try
            {
                if (objIMB001.Status != clsImplementationEnum.IMBStatus.SentForApproval.GetStringValue() || objIMB001.ApprovedBy == objClsLoginInfo.UserName)
                {
                    IMB002 objIMB002 = await db.IMB002.Where(x => x.HeaderId == headerId && x.LineId == lineId).FirstOrDefaultAsync();
                    if (objIMB002 != null)
                    {
                        db.IMB002.Remove(objIMB002);
                        await db.SaveChangesAsync();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Line deleted successfully";
                    }
                    if (objResponseMsg.Key == true && objIMB001.Status == clsImplementationEnum.IMBStatus.Approved.GetStringValue())
                    {
                        objIMB001.RevNo = objIMB001.RevNo + 1;
                        objIMB001.Remarks = null;
                        objIMB001.ApprovedOn = null;
                    }
                    if (objIMB001.ApprovedBy != objClsLoginInfo.UserName)
                    {
                        objIMB001.Status = clsImplementationEnum.IMBStatus.Draft.GetStringValue();
                    }
                    db.SaveChanges();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Document is sent for approval, You can't delete now";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            var response = new
            {
                Key = objResponseMsg.Key,
                Value = objResponseMsg.Value,
                RevNo = objIMB001.RevNo,
                Status = objIMB001.Status
            };
            return Json(response);
        }
        public JsonResult GetTeamMemberValue(int headerId, int lineId)
        {
            try
            {
                string teamMember = (db.IMB002.Where(x => x.HeaderId == headerId && x.LineId == lineId).Select(x => x.TeamMember).FirstOrDefault());
                string[] teamMembers = null;

                if (!string.IsNullOrWhiteSpace(teamMember))
                {
                    teamMembers = teamMember.Split(',');
                }
                return Json(teamMembers, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }


        #endregion

        #region History
        [HttpPost]
        public ActionResult GetHistoryDetails(int headerId, string role)
        {
            IMB001 objIMB001 = db.IMB001.Where(x => x.HeaderId == headerId).FirstOrDefault();
            ViewBag.role = role;
            return PartialView("_GetHistoryDetailsPartial", objIMB001);
        }

        [HttpPost]
        public JsonResult LoadHistoryDetailData(JQueryDataTableParamModel param, int headerId, string role)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                whereCondition += " AND imb1.HeaderId=" + headerId;
                //whereCondition += " and (ApprovedBy = '" + objClsLoginInfo.UserName + "' or CreatedBy = '" + objClsLoginInfo.UserName + "' ) ";
                string[] columnName = { "lcom2.t_desc", "imb1.Project +'-'+ com1.t_dsca", "com6.t_bpid+'-'+com6.t_nama", "Product", "ProcessLicensor", "Status", "Document" };
                #region Sorting
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion
                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                var lstResult = db.SP_IMB_GETHEADERHistory(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstResult.Select(i => i.TotalCount).FirstOrDefault();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.Document),
                            Convert.ToString("R" + uc.RevNo),
                            uc.Project,
                            uc.Customer,
                            uc.Product,
                            uc.ProcessLicensor,
                            Convert.ToString(uc.Status),
                            uc.CreatedBy,
                            Convert.ToDateTime( uc.CreatedOn).ToString("dd/MM/yyyy"),
                            Convert.ToString(uc.SubmittedBy),
                            uc.SubmittedOn == null || uc.SubmittedOn.Value == DateTime.MinValue ? "NA" : uc.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                            Convert.ToString(uc.ApprovedBy),
                            uc.ApprovedOn == null || uc.ApprovedOn.Value == DateTime.MinValue ? "NA" : uc.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),

                            "<center><a class ='iconspace' title=\"View\" target=\"_blank\" onclick=\"HistoryView("+uc.id+",'"+role+"')\"><i class=\"fa fa-eye\"></i></a>"+" "+"<i class ='iconspace fa fa-clock-o' title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/IMB/Maintain/ShowHistoryTimeline?HeaderID=" + Convert.ToInt32(uc.id) + "')></i> </center>"
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult GetHeaderDetailsForPrintReport(int HeaderId)
        {
            var objIMB001 = db.IMB001.Where(i => i.HeaderId == HeaderId).Select(i => new { i.HeaderId, i.Project, i.Document, i.RevNo }).FirstOrDefault();
            return Json(objIMB001, JsonRequestBehavior.AllowGet);
        }
        [SessionExpireFilter]
        public ActionResult HistoryView(int id, string role)
        {
            IMB001_Log objIMB001_Log = new IMB001_Log();

            objIMB001_Log = db.IMB001_Log.Where(x => x.Id == id).FirstOrDefault();

            objIMB001_Log.CreatedBy = objIMB001_Log.CreatedBy + "-" + Manager.GetUserNameFromPsNo(objIMB001_Log.CreatedBy);
            objIMB001_Log.EditedBy = objIMB001_Log.EditedBy + "-" + Manager.GetUserNameFromPsNo(objIMB001_Log.EditedBy);
            objIMB001_Log.ApprovedBy = objIMB001_Log.ApprovedBy + "-" + Manager.GetUserNameFromPsNo(objIMB001_Log.ApprovedBy);

            var urlPrefix = Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.LastIndexOf('=') + 1);
            if (!objClsLoginInfo.GetUserRoleList().Contains("SHOP"))
            {
                ViewBag.RevPrev = (db.IMB001_Log.Any(q => (q.HeaderId == objIMB001_Log.HeaderId && q.RevNo == (objIMB001_Log.RevNo - 1))) ? urlPrefix + db.IMB001_Log.Where(q => (q.HeaderId == objIMB001_Log.HeaderId && q.RevNo == (objIMB001_Log.RevNo - 1))).FirstOrDefault().Id : null);
                ViewBag.RevNext = (db.IMB001_Log.Any(q => (q.HeaderId == objIMB001_Log.HeaderId && q.RevNo == (objIMB001_Log.RevNo + 1))) ? urlPrefix + db.IMB001_Log.Where(q => (q.HeaderId == objIMB001_Log.HeaderId && q.RevNo == (objIMB001_Log.RevNo + 1))).FirstOrDefault().Id : null);
            }
            ViewBag.role = role;
            objIMB001_Log.Customer = Manager.GetCustomerCodeAndNameByProject(objIMB001_Log.Project);
            ViewBag.EquipmentName = db.COM001.Where(x => x.t_cprj == objIMB001_Log.Project).Select(x => x.t_dsca).FirstOrDefault();
            objIMB001_Log.Project = db.COM001.Where(i => i.t_cprj.Equals(objIMB001_Log.Project, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_cprj + "-" + i.t_dsca).FirstOrDefault();

            return View(objIMB001_Log);
        }

        [HttpPost]
        public JsonResult LoadLinesHistoryData(JQueryDataTableParamModel param, int refId)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string condition = string.Empty;
                condition = "RefId = " + refId;

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    condition += " and ( ImprovementDescription like '%" + param.sSearch + "%' or TeamMember like '%" + param.sSearch + "%' or Comments like '%" + param.sSearch + "%' or TeamFormation like '%" + param.sSearch + "%'  or TeamLead like '%" + param.sSearch + "%')";
                }
                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstLines = db.SP_IMB_GETLINESHistory(StartIndex, EndIndex, strSortOrder, condition).ToList();
                int? totalRecords = lstLines.Select(i => i.TotalCount).FirstOrDefault();

                var res = from c in lstLines
                          select new[] {
                                        Convert.ToString(c.ROW_NO),
                                        Convert.ToString(c.ImprovementDescription),
                                        Convert.ToString(c.TeamLeadName),
                                        GetAttachment(c.LineId),
                                        Convert.ToString(c.TeamMemberName),
                                        Convert.ToDateTime( c.TargetDate).ToString("dd/MM/yyyy"),
                                        Convert.ToString(c.TeamFormation),
                                        Convert.ToString(c.Comments),
                                        Convert.ToString(c.Complete),
                                        Convert.ToString(c.LineId)
                                      };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = condition

                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Copy
        public ActionResult CopyHeaderPartial(int headerId)
        {
            IMB001 objIMB001 = db.IMB001.FirstOrDefault(x => x.HeaderId == headerId);
            return PartialView("_CopyToDestinationPartial", objIMB001);
        }

        public JsonResult GetCopyToProjects(string search, int? headerId)
        {
            try
            {
                string project = db.IMB001.Where(x => x.HeaderId == headerId).Select(x => x.Project).FirstOrDefault();

                var items = from li in db.COM001
                            where li.t_cprj != project && (li.t_cprj.Contains(search) || li.t_dsca.Contains(search))
                            select new
                            {
                                id = li.t_cprj,
                                text = li.t_cprj + "-" + li.t_dsca
                            };
                return Json(items, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult retrack(string headerid)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int hd = Convert.ToInt32(headerid);
                if (hd > 0)
                {
                    IMB001 objIMB001 = db.IMB001.Where(x => x.HeaderId == hd).FirstOrDefault();
                    objIMB001.SubmittedOn = null;
                    objIMB001.SubmittedBy = null;
                    objIMB001.Status = clsImplementationEnum.CTQStatus.DRAFT.GetStringValue();
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    Manager.UpdatePDN002(objIMB001.HeaderId, objIMB001.Status, objIMB001.RevNo, objIMB001.Project, objIMB001.Document);
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult copyDetails(string destProject, int headerId)
        {
            int newHeaderId = 0;
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }

            var response = new
            {
                HeaderId = newHeaderId,
                Key = objResponseMsg.Key,
                Value = objResponseMsg.Value,
            };
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Export Excel
        //Excel funtionality
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {//header grid data
                    var lst = db.SP_IMB_GETHEADER(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from h in lst
                                  select new
                                  {
                                      Document = Convert.ToString(h.Document),
                                      RevNo = Convert.ToString("R" + h.RevNo),
                                      Customer = Convert.ToString(h.Customer),
                                      Product = Convert.ToString(h.Product),
                                      ProcessLicensor = Convert.ToString(h.ProcessLicensor),
                                      Location = Convert.ToString(h.LocationDesc),
                                      Status = Convert.ToString(h.Status),
                                      SubmittedBy = h.SubmittedBy,
                                      SubmittedOn = h.SubmittedOn == null || h.SubmittedOn.Value == DateTime.MinValue ? "NA" : h.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      ApprovedBy = h.ApprovedBy,
                                      ApprovedOn = h.ApprovedOn == null || h.ApprovedOn.Value == DateTime.MinValue ? "NA" : h.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {  //locking cleat lines
                    var lst = db.SP_IMB_GETLINES(1, int.MaxValue, strSortOrder, whereCondition).ToList();


                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No data available";
                    }

                    var newlst = (from c in lst
                                  select new
                                  {
                                      SrNo = Convert.ToString(c.ROW_NO),
                                      ImprovementDescription = Convert.ToString(c.ImprovementDescription),
                                      TeamLead = Convert.ToString(c.TeamLead),
                                      TeamLeadName = Convert.ToString(c.TeamLeadName),
                                      TeamMember = Convert.ToString(c.TeamMember),
                                      TeamMemberName = Convert.ToString(c.TeamMemberName),
                                      TargetDate = (c.TargetDate == null || c.TargetDate.Value == DateTime.MinValue ? "NA" : c.TargetDate.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)),
                                      TeamFormation = Convert.ToString(c.TeamFormation),
                                      Comments = Convert.ToString(c.Comments),
                                      Status = c.Complete
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.SUB_LINES.GetStringValue())
                {  //locking cleat lines
                    var lst = db.SP_IMB_GETLINES(1, int.MaxValue, strSortOrder, whereCondition).ToList();


                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No data available";
                    }

                    var newlst = (from c in lst
                                  select new
                                  {
                                      SrNo = Convert.ToString(c.ROW_NO),
                                      ImprovementDescription = Convert.ToString(c.ImprovementDescription),
                                      TeamLead = Convert.ToString(c.TeamLead),
                                      TeamLeadName = Convert.ToString(c.TeamLeadName),
                                      TeamMember = Convert.ToString(c.TeamMember),
                                      TeamMemberName = Convert.ToString(c.TeamMemberName),
                                      TargetDate = (c.TargetDate == null || c.TargetDate.Value == DateTime.MinValue ? "NA" : c.TargetDate.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)),
                                      TeamFormation = Convert.ToString(c.TeamFormation),
                                      Status = c.Complete
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                if (gridType == clsImplementationEnum.GridType.HISTORYHEADER.GetStringValue())
                {//header grid data
                    var lst = db.SP_IMB_GETHEADERHistory(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from h in lst
                                  select new
                                  {
                                      Document = Convert.ToString(h.Document),
                                      RevNo = Convert.ToString("R" + h.RevNo),
                                      Customer = Convert.ToString(h.Customer),
                                      Product = Convert.ToString(h.Product),
                                      ProcessLicensor = Convert.ToString(h.ProcessLicensor),
                                      Location = Convert.ToString(h.LocationDesc),
                                      Status = Convert.ToString(h.Status),
                                      SubmittedBy = h.SubmittedBy,
                                      SubmittedOn = h.SubmittedOn == null || h.SubmittedOn.Value == DateTime.MinValue ? "NA" : h.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      ApprovedBy = h.ApprovedBy,
                                      ApprovedOn = h.ApprovedOn == null || h.ApprovedOn.Value == DateTime.MinValue ? "NA" : h.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.HISTORYLINES.GetStringValue())
                {  //locking cleat lines
                    var lst = db.SP_IMB_GETLINESHistory(1, int.MaxValue, strSortOrder, whereCondition).ToList();


                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No data available";
                    }

                    var newlst = (from c in lst
                                  select new
                                  {
                                      SrNo = Convert.ToString(c.ROW_NO),
                                      ImprovementDescription = Convert.ToString(c.ImprovementDescription),
                                      TeamLead = Convert.ToString(c.TeamLead),
                                      TeamLeadName = Convert.ToString(c.TeamLeadName),
                                      TeamMember = Convert.ToString(c.TeamMember),
                                      TeamMemberName = Convert.ToString(c.TeamMemberName),
                                      TargetDate = (c.TargetDate == null || c.TargetDate.Value == DateTime.MinValue ? "NA" : c.TargetDate.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)),
                                      TeamFormation = Convert.ToString(c.TeamFormation),
                                      Comments = Convert.ToString(c.Comments),
                                      Status = c.Complete
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }


                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }

}