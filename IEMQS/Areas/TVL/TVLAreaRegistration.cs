﻿using System.Web.Mvc;

namespace IEMQS.Areas.TVL
{
    public class TVLAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "TVL";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "TVL_default",
                "TVL/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}