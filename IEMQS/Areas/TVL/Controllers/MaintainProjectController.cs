﻿using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.TVL.Controllers
{
    public class MaintainProjectController : clsBase
    {
        // GET: TVL/MaintainProject

        #region Project Listing Page

        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoadDataGridPartial(string status)
        {
            return PartialView("_GetIndexGridDataPartial");
        }

        [HttpPost]
        public JsonResult LoadDataGridData(JQueryDataTableParamModel param)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhereCondition = "1=1";

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                //search Condition 
                string[] columnName = { "ProjectNo", "ProjectDescription", "Customer", "PlanningRemarks", "CreatedBy", "EditedBy" };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                var lstResult = db.SP_TVL_GET_PROJECT_DATA(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.ProjectNo),
                            Convert.ToString(uc.ProjectDescription),
                            Convert.ToString(uc.Customer),
                            Convert.ToString(uc.PlanningRemarks),
                            //Convert.ToString(uc.CreatedByFullname),
                            //uc.CreatedOn == null || uc.CreatedOn.Value==DateTime.MinValue? "" :uc.CreatedOn.Value.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture),
                            Helper.GenerateActionIcon(uc.HeaderId, "View", "View Detail", "fa fa-eye", "", WebsiteURL + "/TVL/MaintainProject/Details/"+uc.HeaderId,false)
                            + Helper.GenerateActionIcon(uc.HeaderId, "Delete", "Delete Project", "fa fa-trash-o", "DeleteProject(" + uc.HeaderId + ");", "", db.TVL002.Any(c=>c.ProjectNo == uc.ProjectNo))
                            + Helper.GenerateActionIcon(uc.HeaderId, "Timeline", "Timeline", "fa fa-clock-o", "ShowTimeline(\"/TVL/MaintainProject/ShowTimeline?HeaderID="+ uc.HeaderId +"\");")
                          }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ShowTimeline(int HeaderId)
        {
            TimelineViewModel model = new TimelineViewModel();

            model.TimelineTitle = "Traveler Project Timeline";

            if (HeaderId > 0)
            {
                model.Title = "Traveler Project";
                TVL001 objTVL001 = db.TVL001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.CreatedBy = objTVL001.CreatedBy != null ? Manager.GetUserNameFromPsNo(objTVL001.CreatedBy) : null;
                model.CreatedOn = objTVL001.CreatedOn;
                model.EditedBy = objTVL001.EditedBy != null ? Manager.GetUserNameFromPsNo(objTVL001.EditedBy) : null;
                model.EditedOn = objTVL001.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }

        [HttpPost]
        public ActionResult DeleteProject(int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                var objTVL001 = db.TVL001.Where(x => x.HeaderId == headerId).FirstOrDefault();
                if (objTVL001 != null)
                {
                    db.TVL001.Remove(objTVL001);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Project deleted sucessfully.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Project not found, please try again";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;

                //Main grid(user detail)
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_TVL_GET_PROJECT_DATA(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      ProjectNo = Convert.ToString(uc.ProjectNo),
                                      Description = Convert.ToString(uc.ProjectDescription),
                                      Customer = Convert.ToString(uc.Customer),
                                      PlanningRemarks = Convert.ToString(uc.PlanningRemarks),
                                      CreatedBy = Convert.ToString(uc.CreatedByFullname),
                                      Date = uc.CreatedOn == null || uc.CreatedOn.Value == DateTime.MinValue ? "" : uc.CreatedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }


                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Project Details Page

        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult Details(int? Id)
        {
            TVL001 objTVL001 = new TVL001();
            ViewBag.IsDeleteApplicable = false;
            ViewBag.IsEditApplicable = true;

            if (Id.HasValue)
            {
                objTVL001 = db.TVL001.FirstOrDefault(c => c.HeaderId == Id.Value);
                if(db.TVL002.Any(c=>c.ProjectNo == objTVL001.ProjectNo))
                {
                    ViewBag.IsEditApplicable = false;
                }
                else
                {
                    ViewBag.IsDeleteApplicable = true;
                }
            }

            return View(objTVL001);
        }

        [HttpPost]
        public ActionResult SaveData(TVL001 model)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {


                if (model.HeaderId > 0)
                {
                    if (isDuplicateProject(model.ProjectNo, model.HeaderId))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Project no already exists. Please use diffrent project no";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }

                    var objTVL001 = db.TVL001.Where(x => x.HeaderId == model.HeaderId).FirstOrDefault();

                    objTVL001.ProjectNo = model.ProjectNo.Trim().ToUpper();
                    objTVL001.ProjectDescription = model.ProjectDescription;
                    objTVL001.Customer = model.Customer;
                    objTVL001.PlanningRemarks = model.PlanningRemarks;
                    objTVL001.I1Intervention = model.I1Intervention;
                    objTVL001.I2Intervention = model.I2Intervention;
                    objTVL001.I3Intervention = model.I3Intervention;
                    objTVL001.I4Intervention = model.I4Intervention;
                    objTVL001.EditedBy = objClsLoginInfo.UserName;
                    objTVL001.EditedOn = DateTime.Now;

                    db.SaveChanges();
                    objResponseMsg.HeaderId = objTVL001.HeaderId;
                    objResponseMsg.Value = "Project saved successfully.";
                }
                else
                {
                    if (isDuplicateProject(model.ProjectNo))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Project no already exists. Please use diffrent project no";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }

                    TVL001 objTVL001 = new TVL001
                    {
                        ProjectNo = model.ProjectNo.Trim().ToUpper(),
                        ProjectDescription = model.ProjectDescription,
                        Customer = model.Customer,
                        PlanningRemarks = model.PlanningRemarks,
                        I1Intervention=model.I1Intervention,
                        I2Intervention=model.I2Intervention,
                        I3Intervention=model.I3Intervention,
                        I4Intervention=model.I4Intervention,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    };
                    db.TVL001.Add(objTVL001);
                    db.SaveChanges();
                    objResponseMsg.HeaderId = objTVL001.HeaderId;
                    objResponseMsg.Value = "Project saved successfully.";
                }

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        private bool isDuplicateProject(string projectNo, int? headerId = null)
        {
            if (headerId.HasValue)
            {
                return db.TVL001.Any(c => c.HeaderId != headerId.Value && c.ProjectNo == projectNo);
            }
            else
            {
                return db.TVL001.Any(c => c.ProjectNo == projectNo);
            }
        }

        #endregion

    }
}