﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQSImplementation;
using IEMQS.Models;
using IEMQS.Areas.Utility.Models;
using System.IO;
using OfficeOpenXml;
using System.Data;
using System.Data.Entity.SqlServer;
using IEMQS.Areas.NDE.Models;
using System.Configuration;
using System.Text;
using System;
using System.Net;

namespace IEMQS.Areas.TVL.Controllers
{
    public class CompleteTravelerController : clsBase
    {
        // GET: TVL/CompleteTraveler
        #region Index Page
        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GetIndexCompleteGridDataPartial(string status)
        {
            ViewBag.status = status;
            return PartialView("_GetIndexCompleteGridDataPartial");
        }
        public ActionResult LoadCompleteTravelerIndexDataTable(JQueryDataTableParamModel param, string status)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "tvl40.BU", "tvl40.Location");
                if (status.ToUpper() == "PENDING")
                {
                    whereCondition += " and  ISNULL(IsCompleted,'') != 1  AND dbo.FN_TVL_IS_ALL_OPERATIONS_CLEARED(tvl40.ProjectNo,tvl40.BU,tvl40.Location,tvl40.AssemblyNo,tvl40.TravelerNo,tvl40.SubAssemblyNo)=1";
                }
                else if (status.ToUpper() == "ALL")
                {
                    whereCondition += " and  (ISNULL(IsCompleted,'') = 1 or ISNULL(IsCompleted,'') != 1 )  AND dbo.FN_TVL_IS_ALL_OPERATIONS_CLEARED(tvl40.ProjectNo,tvl40.BU,tvl40.Location,tvl40.AssemblyNo,tvl40.TravelerNo,tvl40.SubAssemblyNo)=1";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (tvl40.ProjectNo like '%" + param.sSearch
                                    + "%' or tvl40.TravelerNo like '%" + param.sSearch
                                    + "%' or tvl40.BU like '%" + param.sSearch
                                    + "%' or tvl40.Location like '%" + param.sSearch
                                    + "%' or tvl40.AssemblyNo like '%" + param.sSearch
                                    + "%' or tvl40.Customer like '%" + param.sSearch
                                    + "%' or tvl40.TravelerRevNo like '%" + param.sSearch
                                    + "%')";
                }
               
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_TVL_OFFER_INDEX_GETALLDATA(startIndex, endIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstResult.Select(i => i.TotalCount).FirstOrDefault();

                var res = from uc in lstResult
                          select new[] {

                            Convert.ToString(uc.Project),
                            Convert.ToString(uc.TravelerNo),
                            Convert.ToString(uc.BU),
                            Convert.ToString(uc.Location),
                            Convert.ToString(uc.AssemblyNo),
                            Convert.ToString(uc.SubAssemblyNo),
                            Helper.GenerateActionIcon(Convert.ToInt32(uc.HeaderId), "View", "View Details", "fa fa-eye", "", WebsiteURL+"/TVL/CompleteTraveler/Details/" + Convert.ToString(uc.HeaderId))
                          + Helper.GenerateActionIcon(uc.HeaderId, "Print", "Print Traveler", "fa fa-print", "PrintTVLReport('"+uc.Project+"','"+uc.TravelerNo+"',"+uc.TravelerRevNo+",'"+uc.AssemblyNo+"','"+uc.SubAssemblyNo+"',true)","",false)
                          };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }
        #endregion

        #region Header Details Page
        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult Details(int? id)
        {
            TVL040 objTVL040 = new TVL040();
            try
            {
                if (id > 0)
                {
                    objTVL040 = db.TVL040.Where(i => i.HeaderId == id).FirstOrDefault();
                    ViewBag.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objTVL040.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                    ViewBag.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objTVL040.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                    ViewBag.Completedby = objTVL040.CompletedBy != null ? (objTVL040.CompletedBy + " - " + Manager.GetUserNameFromPsNo(objTVL040.CompletedBy)) : "";
                    ViewBag.Completedon = (objTVL040.CompletedOn != null ? objTVL040.CompletedOn.Value.ToString("dd/MM/yyyy hh:mm tt") : "");
                }
                //else
                //{
                //    ViewBag.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objClsLoginInfo.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                //    objTVL040.Location = objClsLoginInfo.Location;
                //    ViewBag.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == "02").Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                //}
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return View(objTVL040);
        }

        public ActionResult GetCompleteTravelerHeaderDataPartial(string Status, TVL040 objTVL040)
        {
            ViewBag.Status = Status;
            objTVL040.BU = objTVL040.BU.Split('-')[0];
            objTVL040.Location = objTVL040.Location.Split('-')[0];
            return PartialView("_GetCompleteHeaderDataPartial", objTVL040);
        }
        [HttpPost]
        public ActionResult GetCompleteTravelerHeaderData(JQueryDataTableParamModel param)
        {
            try
            {

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";
                string strSortOrder = string.Empty;


                whereCondition += " and TravelerNo='" + param.TravelerNo + "' and ProjectNo='" + param.Project + "'"
                                    + "and AssemblyNo='" + param.IdentifictionNo + "' and Customer='" + param.Customer + "' "
                                    + "and BU='" + param.BU + "' and Location='" + param.Location + "'"
                                    + "and SubAssemblyNo=" + Convert.ToInt32(param.subassembly) + " ";
                bool ISAllClear = db.Database.SqlQuery<bool>("SELECT DBO.FN_TVL_IS_ALL_OPERATIONS_CLEARED('" + param.Project + "','" + param.BU + "','" + param.Location + "','" + param.IdentifictionNo + "','" + param.TravelerNo + "'," + param.subassembly + ")").FirstOrDefault();
                int headerid = Convert.ToInt32(param.Headerid);
                bool IsComplete = false;
                bool ispending = false;
                var objTVL040 = db.TVL040.Where(x => x.HeaderId == headerid).FirstOrDefault();
                if (objTVL040 != null)
                {
                    var lstCompleted = db.TVL040.Where(x => x.TravelerNo == objTVL040.TravelerNo
                                              && x.TravelerRevNo == objTVL040.TravelerRevNo
                                              && x.ProjectNo == objTVL040.ProjectNo
                                              && x.BU == objTVL040.BU
                                              && x.Location == objTVL040.Location
                                              && x.AssemblyNo == objTVL040.AssemblyNo
                                              && x.SubAssemblyNo == objTVL040.SubAssemblyNo
                                              )
                                              .ToList();
                    if (lstCompleted.All(x => x.IsCompleted == true))
                    {
                        IsComplete = true;
                    }

                }
                if (ISAllClear && !IsComplete)
                {
                    ispending = true;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "tvl040.BU", "tvl040.Location");
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (ProjectNo like '%" + param.sSearch
                                    + "%' or TravelerNo like '%" + param.sSearch
                                    + "%' or BU like '%" + param.sSearch
                                    + "%' or Location like '%" + param.sSearch
                                    + "%' or AssemblyNo like '%" + param.sSearch
                                    + "%' or Customer like '%" + param.sSearch
                                    + "%' or TravelerRevNo like '%" + param.sSearch
                                    + "%')";
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                var lstResult = db.SP_TVL_GET_OFFERDATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                var data = (from uc in lstResult
                            select new[]
                            {
                            Convert.ToString(uc.HeaderId),
                            Helper.GenerateHidden(Convert.ToInt32(uc.HeaderId), "HeaderId", Convert.ToString(uc.HeaderId)),
                            uc.OperationNo.Value.ToString("G29"),
                            Convert.ToString("R"+uc.OperationRevisionNo),
                            Convert.ToString(uc.Activity),
                            Convert.ToBoolean(uc.IsHeaderLine) ? "" : Convert.ToString(uc.RefDocument) != null ? "* "+ Convert.ToString(uc.RefDocument).Replace(",","<br/> * ") : "--",
                            Convert.ToBoolean(uc.IsHeaderLine) ? "" : Convert.ToString(uc.RefDocRevNo) != null ? "* "+ Convert.ToString(uc.RefDocRevNo).Replace(",","<br/> * ") : "--",
                            Convert.ToBoolean(uc.IsHeaderLine) ? "" : Convert.ToString(uc.itenrationNo),
                            Convert.ToBoolean(uc.IsHeaderLine) ? "" : Convert.ToString(uc.SubAssemblyNo),
                            Convert.ToBoolean(uc.IsHeaderLine) ? "" : Convert.ToString(uc.InspectionStatus),
                            Convert.ToBoolean(uc.IsHeaderLine) ? "" : Convert.ToString(uc.LnTInspectorResult),
                            "<nobr><center>"
                            +"<i title='View Timeline' href='javascript:void(0)' onclick=ShowTimeline('/TVL/OfferInspection/ShowTimelineOfferInspectionPartial?headerId=" + Convert.ToInt32(uc.HeaderId) + "'"+"); class='iconspace fa fa-clock-o'></i>"
                            + Manager.generateTVLGridButtons(uc.ProjectNo,uc.Customer,uc.TravelerNo,uc.TravelerRevNo,uc.AssemblyNo,uc.BU,uc.Location,uc.SubAssemblyNo,uc.OperationNo.Value.ToString("G29"),uc.Activity,uc.HeaderId,"Traveler Details",40,"Traveler Details","Request Details","Traveler Request Details")
                            +"</center></nobr>"
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition,
                    ispending = ispending,
                    IsComplete = IsComplete
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult UpdateDSCSrNo(int strHeader, string DSCSrNo)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            List<TVL010> lstTVL010s = new List<TVL010>();
            try
            {
                var objTVL040 = db.TVL040.Where(x => x.HeaderId == strHeader).FirstOrDefault();
                if (objTVL040 != null)
                {
                    var lst = db.TVL040.Where(x => x.TravelerNo == objTVL040.TravelerNo
                                              && x.TravelerRevNo == objTVL040.TravelerRevNo
                                              && x.ProjectNo == objTVL040.ProjectNo
                                              && x.BU == objTVL040.BU
                                              && x.Location == objTVL040.Location
                                              && x.AssemblyNo == objTVL040.AssemblyNo
                                              && x.SubAssemblyNo == objTVL040.SubAssemblyNo)
                                              .ToList();
                    if (lst.Count > 0)
                    {
                        lst.ForEach(x =>
                        {
                            x.DSCSrNo = DSCSrNo;
                            //x.EditedBy = objClsLoginInfo.UserName;
                            //x.EditedOn = DateTime.Now;
                        });

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "DSC Sr. No. has been sucessfully updated";
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Operations does not exists";
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CompleteTravelerHeader(int strHeader)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            List<TVL010> lstTVL010s = new List<TVL010>();
            try
            {
                var objTVL040 = db.TVL040.Where(x => x.HeaderId == strHeader).FirstOrDefault();
                if (objTVL040 != null)
                {
                    var lstCompleted = db.TVL040.Where(x => x.TravelerNo == objTVL040.TravelerNo
                                              && x.TravelerRevNo == objTVL040.TravelerRevNo
                                              && x.ProjectNo == objTVL040.ProjectNo
                                              && x.BU == objTVL040.BU
                                              && x.Location == objTVL040.Location
                                              && x.AssemblyNo == objTVL040.AssemblyNo
                                              && x.SubAssemblyNo == objTVL040.SubAssemblyNo)
                                              .ToList();
                    if (lstCompleted.Count > 0)
                    {
                        lstCompleted.ForEach(x =>
                        {
                            x.IsCompleted = true;
                            x.CompletedBy = objClsLoginInfo.UserName;
                            x.CompletedOn = DateTime.Now;
                        });

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Operation(s) has been sucessfully completed";
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Operations does not exists";
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        private bool CompleteTraveler(int headerId)
        {
            TVL040 objTVL040 = db.TVL040.FirstOrDefault(i => i.HeaderId == headerId);

            if (objTVL040 != null)
            {
                objTVL040.IsCompleted = true;
                db.SaveChanges();

                #region Send Notification

                #endregion

                return true;
            }
            //if (objTVL010 != null)
            //{
            //    objTVL010.Status = clsImplementationEnum.TravelerHeaderStatus.Completed.GetStringValue();
            //    objTVL010.CompletedBy = objClsLoginInfo.UserName;
            //    objTVL010.CompletedOn = DateTime.Now;
            //    db.SaveChanges();

            //    #region Send Notification

            //    #endregion

            //    return true;
            //}
            return false;
        }



        #endregion

        #region Lines Details
        [HttpPost]
        public ActionResult GetCompleteTravelerLinesPartial(int headerId)
        {
            TVL010 objTVL010 = new TVL010();

            if (headerId > 0)
                objTVL010 = db.TVL010.Where(i => i.HeaderId == headerId).FirstOrDefault();

            if (objTVL010.QIReturnedBy != null)
            {

                ViewBag.ReturnByqi = objTVL010.QIReturnedBy + " - " + Manager.GetUserNameFromPsNo(objTVL010.QIReturnedBy);
                ViewBag.ReturnDateqi = objTVL010.QIReturnedOn.Value.ToString("dd/MM/yyyy hh:mm tt");
                ViewBag.Remarksqi = objTVL010.QIRemarks;
            }
            if (objTVL010.WEReturnedBy != null)
            {

                ViewBag.ReturnBywe = objTVL010.WEReturnedBy + " - " + Manager.GetUserNameFromPsNo(objTVL010.WEReturnedBy);
                ViewBag.ReturnDatewe = objTVL010.WEReturnedOn.Value.ToString("dd/MM/yyyy hh:mm tt");
                ViewBag.Remarkswe = objTVL010.WERemarks;
            }
            if (objTVL010.PlanningReturnedBy != null)
            {

                ViewBag.ReturnByplng = objTVL010.PlanningReturnedBy + " - " + Manager.GetUserNameFromPsNo(objTVL010.PlanningReturnedBy);
                ViewBag.ReturnDateplng = objTVL010.PlanningReturnedOn.Value.ToString("dd/MM/yyyy hh:mm tt");
                ViewBag.Remarksplng = objTVL010.PlanningRemarks;
            }
            bool isEditable = ((objTVL010.Status == clsImplementationEnum.TravelerHeaderStatus.ReturnedbyQI.GetStringValue() || objTVL010.Status == clsImplementationEnum.TravelerHeaderStatus.ReturnedbyWE.GetStringValue() || objTVL010.Status == clsImplementationEnum.TravelerHeaderStatus.Returnedbyplanning.GetStringValue()));
            ViewBag.Headerstatus = isEditable;



            ViewBag.project = objTVL010.ProjectNo;

            ViewBag.Status = objTVL010.Status;

            return PartialView("_GetICompleteLinesDataPartial", objTVL010);
        }

        [HttpPost]
        public JsonResult LoadTravelerLinesData(JQueryDataTableParamModel param)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhereCondition = string.Empty;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                strWhereCondition += " 1=1 and TravelerNo='" + param.TravelerNo + "' and ProjectNo='" + param.Project + "'"
                                    + " and AssemblyNo='" + param.IdentifictionNo + "' "
                                    + " and BU='" + param.BU + "' and Location='" + param.Location + "'  and SubAssemblyNo=" + param.subassembly;

                //search Condition 
                string[] columnName = { "OperationNo", "OperationRevisionNo", "Activity", "RefDocument", "RefDocRevNo", "itenrationNo", "SubAssemblyNo", "InspectionStatus" };
                strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                var lstResult = db.SP_TVL_GET_OFFERDATA(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.ColorFlag),
                            uc.OperationNo.Value.ToString("G29"),
                            Convert.ToString("R"+uc.OperationRevisionNo),
                            Convert.ToString(uc.Activity),
                             Convert.ToString(uc.RefDocument) != null ? "* "+ Convert.ToString(uc.RefDocument).Replace(",","<br/> * ") : "--",
                            Convert.ToString(uc.RefDocRevNo) != null ? "* "+ Convert.ToString(uc.RefDocRevNo).Replace(",","<br/> * ") : "--",
                            Convert.ToString(uc.itenrationNo),
                            Convert.ToString(uc.InspectionStatus),
                            Convert.ToString(uc.LnTInspectorResult),
                           "<nobr><center>" +
                            Manager.generateTVLGridButtons(uc.ProjectNo,uc.Customer,uc.TravelerNo,uc.TravelerRevNo,uc.AssemblyNo,uc.BU,uc.Location,uc.SubAssemblyNo,uc.OperationNo.Value.ToString("G29"),uc.Activity,uc.HeaderId,"Traveler Details",40,"true","Traveler Details","Traveler Request Details")
                            +"<i title='View Timeline' href='javascript:void(0)' onclick=ShowTimeline('/TVL/OfferInspection/ShowTimelineOfferInspectionPartial?headerId=" + Convert.ToInt32(uc.HeaderId) + "'"+"); class='iconspace fa fa-clock-o'></i>"
                             +"</center></nobr>"
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetTravelerLinesData(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND tvl11.HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (tvl11.OperationNo like '%" + param.sSearch
                        + "%' or tvl11.OperationNo like '%" + param.sSearch
                        + "%' or tvl11.OperationRevisionNo like '%" + param.sSearch
                        + "%' or tvl11.Activity like '%" + param.sSearch
                        + "%' or tvl11.RefDocument like '%" + param.sSearch
                        + "%' or tvl11.RefDocRevNo like '%" + param.sSearch
                        + "%' or tvl11.LTIntervention like '%" + param.sSearch
                        + "%' or tvl11.I1Intervention like '%" + param.sSearch
                        + "%' or tvl11.I2Intervention like '%" + param.sSearch
                        + "%' or tvl11.I3Intervention like '%" + param.sSearch
                         + "%' or tvl11.I4Intervention like '%" + param.sSearch
                        + "%' or tvl11.InspectionRecordRequired like '%" + param.sSearch
                        + "%')";
                }

                var lstResult = db.SP_TVL_GET_LINES_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                var data = (from uc in lstResult
                            select new[]
                            {
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                               uc.OperationNo.Value.ToString("G29"),
                               "R"+Convert.ToString(uc.OperationRevisionNo),
                               Convert.ToString(uc.Activity),
                               Convert.ToString(uc.RefDocument) != null ? "* "+ Convert.ToString(uc.RefDocument).Replace(",","<br/> * ") : "--",
                               Convert.ToString(uc.RefDocRevNo) != null ? "* "+ Convert.ToString(uc.RefDocRevNo).Replace(",","<br/> * ") : "--",
                               Convert.ToString(Manager.TVLIntervention(uc.LTIntervention)),
                               Convert.ToString(uc.I1Intervention),
                               Convert.ToString(uc.I2Intervention),
                               Convert.ToString(uc.I3Intervention),
                               Convert.ToString(uc.I4Intervention),
                               Convert.ToString(uc.InspectionRecordRequired),
                               Convert.ToString(uc.ReturnRemarks),
                               Convert.ToString(uc.LineStatus),
                               Helper.GenerateActionIcon(uc.LineId,"Timeline","Timeline","fa fa-clock-o", "ShowTimeline(\"/TVL/MaintainTraveler/ShowTimeline?HeaderID="+ uc.HeaderId +"&LineId="+ uc.LineId +"\");"),
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetPDFReportFilePath(string projectno, string travelerno, string identificationNo, int subassemblyno)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            //var refdocvno = db.TVL002.Where(m => m.ProjectNo == project && lstPosition.Contains(m.RefDocument)).ToList().Max(m=>m.RefDocRevNo);
            TVL040 objTVL040 = db.TVL040.Where(m => m.ProjectNo == projectno && m.TravelerNo == travelerno && m.AssemblyNo == identificationNo && m.SubAssemblyNo == subassemblyno).FirstOrDefault();
            if (objTVL040 != null)
            {
                if (objTVL040.IsDigitalSignature == true)
                {
                    string path = "/SignedDoc/Traveler/";
                    objResponseMsg.Key = true;
                    string filename = objTVL040.ProjectNo + "_" + objTVL040.TravelerNo + "_R" + objTVL040.TravelerRevNo + "_" + objTVL040.AssemblyNo + "_" + objTVL040.SubAssemblyNo + ".pdf";

                    objResponseMsg.folderpath = "/SignedDoc/Traveler/";
                    objResponseMsg.dataValue = filename;
                    objResponseMsg.Value = ConfigurationManager.AppSettings["File_Upload_URL"] + "/SignedDoc/Traveler/" + objTVL040.ProjectNo + "_" + objTVL040.TravelerNo + "_R" + objTVL040.TravelerRevNo + "_" + objTVL040.AssemblyNo + "_" + objTVL040.SubAssemblyNo + ".pdf";
                    Utility.Controllers.FileUploadController _objFUC = new Utility.Controllers.FileUploadController();
                    //_objFUC.CheckAnyDocumentsExitsByFileName()
                    bool Flag = (new clsFileUpload()).GetDocuments(path, true).Any(x => x.Name == filename);
                    if (!Flag)
                    {
                        #region generate pdf

                        string filenameformat = objTVL040.ProjectNo + "_" + objTVL040.TravelerNo + "_R" + objTVL040.TravelerRevNo + "_" + objTVL040.AssemblyNo + "_" + objTVL040.SubAssemblyNo;
                        // string downloadpath = "~/Resources/TRAVELER/CompleteTraveler/";
                        string downloadpath = "~/Resources/TRAVELER/CompleteTraveler/";
                        string Reporturl = "/TVL/Traveler";

                        List<SSRSParam> reportParams = new List<SSRSParam>();
                        reportParams.Add(new SSRSParam { Param = "Project", Value = objTVL040.ProjectNo });
                        reportParams.Add(new SSRSParam { Param = "TravelerNo", Value = objTVL040.TravelerNo });
                        reportParams.Add(new SSRSParam { Param = "TravelerRevNo", Value = objTVL040.TravelerRevNo.ToString() });
                        reportParams.Add(new SSRSParam { Param = "AssemblyNo", Value = objTVL040.AssemblyNo });
                        reportParams.Add(new SSRSParam { Param = "SubAssemblyNo", Value = objTVL040.SubAssemblyNo.ToString() });

                        string file = (new Utility.Controllers.GeneralController()).GeneratePDFfromSSRSreport(Server.MapPath(downloadpath + filenameformat + ".pdf"), Reporturl, reportParams);
                        #endregion
                        objResponseMsg.Key = false;
                    }

                }
                else
                {
                    objResponseMsg.Key = false;
                }
            }
            else
            {
                objResponseMsg.Key = false;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }



        #endregion
    }
}