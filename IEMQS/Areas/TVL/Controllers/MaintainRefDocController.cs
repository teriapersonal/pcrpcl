﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Data;
using System.IO;
using OfficeOpenXml;
using System.Drawing;

namespace IEMQS.Areas.TVL.Controllers
{
    public class MaintainRefDocController : clsBase
    {
        // GET: TVL/MaintainRefDoc
        #region Project Listing Page

        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoadDataGridPartial(string status)
        {
            return PartialView("_GetIndexGridDataPartial");
        }

        [HttpPost]
        public JsonResult LoadDataGridData(JQueryDataTableParamModel param)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                //string strWhereCondition = "1=1 AND (tvl002.HeaderId IN (SELECT (Select HeaderId FROM TVL002 WHERE RefDocument=TVL1.RefDocument AND ProjectNo=TVL1.ProjectNo AND RefDocRevNo=MAX(TVL1.RefDocRevNo)) AS HeaderId FROM TVL002 TVL1 GROUP BY ProjectNo, RefDocument)) ";
                string strWhereCondition = "1=1";
                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                //search Condition 
                string[] columnName = { "ProjectNo", "RefDocument", "PlanningRemarks", "CreatedBy", "EditedBy" };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                var lstResult = db.SP_TVL_GET_REFERENCE_DOCUMENTS_DATA(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                           uc.ProjectNo,
                            Helper.GenerateActionIcon(uc.HeaderId, "View", "View Detail", "fa fa-eye", "", WebsiteURL + "/TVL/MaintainRefDoc/Details/"+uc.HeaderId,false)
                            //+ Helper.GenerateActionIcon(uc.HeaderId, "Delete", "Delete Reference Document", "fa fa-trash-o", "DeleteRefDoc(" + uc.HeaderId + ");", "", UsedInTravelerOperation(uc.HeaderId))
                            //+ Helper.GenerateActionIcon(uc.HeaderId, "Revision", "View Previous Revisions", "fa fa-history", "RefDocumentRevisions(" + uc.HeaderId + ");", "", !db.TVL002.Any(c=> c.ProjectNo == uc.ProjectNo && c.RefDocument == uc.RefDocument && c.HeaderId != uc.HeaderId))
                            //+ Helper.GenerateActionIcon(uc.HeaderId, "Timeline", "Timeline", "fa fa-clock-o", "ShowTimeline(\"/TVL/MaintainRefDoc/ShowTimeline?HeaderID="+ uc.HeaderId +"\");")
                          }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        private bool UsedInTravelerOperation(int headerId)
        {
            try
            {
                TVL002 objTVL002 = db.TVL002.FirstOrDefault(c => c.HeaderId == headerId);

                List<RefDocuments> refDocuments = db.TVL011.Where(c => c.TVL010.ProjectNo == objTVL002.ProjectNo && c.RefDocument.Contains(objTVL002.RefDocument)).Select(x => new RefDocuments { ReferenceDocument = x.RefDocument, ReferenceDocumentRevNo = x.RefDocRevNo }).ToList();
                foreach (RefDocuments doc in refDocuments)
                {
                    string[] refDocs = doc.ReferenceDocument.Split(',');
                    string[] refDocRevs = doc.ReferenceDocumentRevNo.Split(',');
                    for (int i = 0; i < refDocs.Length; i++)
                    {
                        string refDoc = refDocs[i].Trim();
                        int refDocRev;
                        if (!int.TryParse(Convert.ToString(refDocRevs[i].Trim()), out refDocRev))
                        {
                            continue;
                        }
                        if (db.TVL002.Any(c => c.HeaderId == headerId && c.RefDocument == refDoc.Trim() && c.RefDocRevNo == refDocRev))
                        {
                            return true;
                        }
                    }
                }
            }
            catch
            {

            }
            return false;
        }

        public ActionResult ShowTimeline(int HeaderId)
        {
            TimelineViewModel model = new TimelineViewModel();

            model.TimelineTitle = "Traveler Reference Document Timeline";

            if (HeaderId > 0)
            {
                model.Title = "Traveler RefDoc";
                TVL002 objTVL002 = db.TVL002.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.CreatedBy = objTVL002.CreatedBy != null ? Manager.GetUserNameFromPsNo(objTVL002.CreatedBy) : null;
                model.CreatedOn = objTVL002.CreatedOn;
                model.EditedBy = objTVL002.EditedBy != null ? Manager.GetUserNameFromPsNo(objTVL002.EditedBy) : null;
                model.EditedOn = objTVL002.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }

        [HttpPost]
        public ActionResult DeleteRefDoc(int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                var objTVL002 = db.TVL002.Where(x => x.HeaderId == headerId).FirstOrDefault();
                if (objTVL002 != null)
                {
                    db.TVL002.Remove(objTVL002);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Reference document deleted sucessfully.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Reference document not found, please try again";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                String Descrption = string.Empty;

                //Main grid(user detail)
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_TVL_GET_REFDOCUMENT_HEADER_DATA(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      ProjectNo = Convert.ToString(uc.ProjectNo),
                                      RefDocuments = Convert.ToString(uc.RefDocument),
                                      RefDocRevisionNo = Convert.ToString(uc.RefDocRevNo),
                                      Date = uc.IssueDate == null || uc.IssueDate.Value == DateTime.MinValue ? "" : uc.IssueDate.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      Description = Convert.ToString(uc.PlanningRemarks),
                                      DocType = Convert.ToString(uc.DocType),
                                      CreatedBy = Convert.ToString(Manager.GetUserNameFromPsNo(uc.CreatedBy)),
                                      CreatedOn = uc.CreatedOn == null || uc.CreatedOn.Value == DateTime.MinValue ? "" : uc.CreatedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }


                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        #region Revisions of Ref Documents

        [HttpPost]
        public ActionResult RevisionHistoryGridDataPartial(int headerId)
        {
            TVL002 objTVL002 = db.TVL002.FirstOrDefault(c => c.HeaderId == headerId);
            ViewBag.RefDocument = objTVL002.RefDocument;
            ViewBag.ProjectNo = objTVL002.ProjectNo;

            return PartialView("_GetRevisionHistoryGridDataPartial");
        }

        [HttpPost]
        public JsonResult LoadDataGridDataForRevision(JQueryDataTableParamModel param)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhereCondition = "1=1 AND ProjectNo='" + param.Project + "' AND RefDocument='" + param.RefDocument + "'";

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                else
                {
                    strSortOrder = " Order By RefDocRevNo DESC ";
                }
                #endregion

                //search Condition 
                string[] columnName = { "RefDocument", "RefDocRevNo", "CreatedBy", "CreatedOn" };
                strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                var lstResult = db.SP_TVL_GET_REFDOCUMENT_HEADER_DATA(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            //Convert.ToString(uc.ProjectNo),
                            Convert.ToString(uc.RefDocument),
                            "R"+Convert.ToString(uc.RefDocRevNo),
                            //Convert.ToString(uc.PlanningRemarks),
                           Convert.ToString(Manager.GetUserNameFromPsNo(uc.CreatedBy)),
                            uc.CreatedOn == null || uc.CreatedOn.Value==DateTime.MinValue? "" :uc.CreatedOn.Value.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture),
                            Helper.GenerateActionIcon(uc.HeaderId, "View", "View Detail", "fa fa-eye", "", WebsiteURL + "/TVL/MaintainRefDoc/RevisionDetails/"+uc.HeaderId,false, true)
                            + Helper.GenerateActionIcon(uc.HeaderId, "Timeline", "Timeline", "fa fa-clock-o", "ShowTimeline(\"/TVL/MaintainRefDoc/ShowTimeline?HeaderID="+ uc.HeaderId +"\");")
                          }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter]
        public ActionResult RevisionDetails(int? Id)
        {
            TVL002 objTVL002 = new TVL002();

            if (Id.HasValue)
            {
                objTVL002 = db.TVL002.FirstOrDefault(c => c.HeaderId == Id.Value);
            }

            return View(objTVL002);
        }

        #endregion

        #endregion

        #region Project Details Page

        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        public ActionResult Details(int? Id)
        {
            TVL002 objTVL002 = new TVL002();
            ViewBag.IsDeleteApplicable = false;
            ViewBag.IsEditApplicable = true;
            ViewBag.lntintervention = Manager.GetSubCatagories("Traveler Doc Type", "02", objClsLoginInfo.Location).Select(i => new CategoryData { Value = i.Description, Code = i.Code, CategoryDescription = i.Description }).ToList();

            if (Id.HasValue)
            {
                objTVL002 = db.TVL002.FirstOrDefault(c => c.HeaderId == Id.Value);
                if (UsedInTravelerOperation(objTVL002.HeaderId))
                {
                    ViewBag.IsEditApplicable = false;
                }
                else
                {
                    ViewBag.IsDeleteApplicable = true;
                }
            }

            return View(objTVL002);
        }

        //[HttpPost]
        //public ActionResult SaveData(TVL002 model)
        //{
        //    clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
        //    try
        //    {
        //        if (model.HeaderId > 0)
        //        {
        //            if (isDuplicateRefDoc(model.ProjectNo, model.RefDocument, model.RefDocRevNo.Value, model.HeaderId))
        //            {
        //                objResponseMsg.Key = false;
        //                objResponseMsg.Value = "Reference document already exists. Please use diffrent reference document.";
        //                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        //            }

        //            var objTVL002 = db.TVL002.Where(x => x.HeaderId == model.HeaderId).FirstOrDefault();

        //            objTVL002.ProjectNo = model.ProjectNo.Trim().ToUpper();
        //            objTVL002.RefDocument = model.RefDocument;
        //            objTVL002.RefDocRevNo = model.RefDocRevNo;
        //            objTVL002.PlanningRemarks = model.PlanningRemarks;
        //            objTVL002.DocType = model.DocType;
        //            objTVL002.EditedBy = objClsLoginInfo.UserName;
        //            objTVL002.EditedOn = DateTime.Now;

        //            db.SaveChanges();
        //            objResponseMsg.HeaderId = objTVL002.HeaderId;
        //            objResponseMsg.Value = "Reference document saved successfully.";
        //        }
        //        else
        //        {
        //            if (isDuplicateRefDoc(model.ProjectNo, model.RefDocument, model.RefDocRevNo.Value))
        //            {
        //                objResponseMsg.Key = false;
        //                objResponseMsg.Value = "Reference document already exists. Please use diffrent reference document.";
        //                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        //            }

        //            TVL002 objTVL002 = new TVL002
        //            {
        //                ProjectNo = model.ProjectNo.Trim().ToUpper(),
        //                RefDocument = model.RefDocument,
        //                RefDocRevNo = model.RefDocRevNo,
        //                PlanningRemarks = model.PlanningRemarks,
        //                DocType = model.DocType,
        //                CreatedBy = objClsLoginInfo.UserName,
        //                CreatedOn = DateTime.Now
        //            };
        //            db.TVL002.Add(objTVL002);
        //            db.SaveChanges();
        //            objResponseMsg.HeaderId = objTVL002.HeaderId;
        //            objResponseMsg.Value = "Reference document saved successfully.";
        //        }

        //        objResponseMsg.Key = true;
        //    }
        //    catch (Exception ex)
        //    {
        //        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        //        objResponseMsg.Key = false;
        //        objResponseMsg.Value = ex.Message;
        //    }
        //    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        //}
        [HttpPost]
        public ActionResult SaveData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int newRowIndex = 0;
                DateTime? IssuerefDate;
                int Headerid = Convert.ToInt32(fc["HeaderId"]);
                string Project = Convert.ToString(fc["ProjectNo"]);
                string RefDocument = !string.IsNullOrEmpty(fc["RefDocument" + newRowIndex]) ? fc["RefDocument" + newRowIndex].Trim() : string.Empty;
                int RefDocRevNo = Convert.ToInt32(fc["RefDocRevNo" + newRowIndex]);
                string Doctype = !string.IsNullOrEmpty(fc["TestDocType" + newRowIndex]) ? fc["TestDocType" + newRowIndex] : string.Empty;
                string PlanningRemarks = !string.IsNullOrEmpty(fc["PlanningRemarks" + newRowIndex]) ? fc["PlanningRemarks" + newRowIndex] : string.Empty;
                if (fc["IssueDate" + newRowIndex] != null && CheckDate(fc["IssueDate" + newRowIndex]))
                {
                    IssuerefDate = Convert.ToDateTime(fc["IssueDate" + newRowIndex]);
                }
                else
                {
                    IssuerefDate = null;
                }
                if (isDuplicateRefDoc(Project, RefDocument, RefDocRevNo))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Reference document already exists. Please use diffrent reference document.";

                }
                else
                {
                    TVL002 objTVL002 = new TVL002
                    {
                        ProjectNo = Project.Trim().ToUpper(),
                        RefDocument = RefDocument.Trim(),
                        RefDocRevNo = RefDocRevNo,
                        IssueDate = IssuerefDate,
                        PlanningRemarks = PlanningRemarks,
                        DocType = Doctype,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    };
                    db.TVL002.Add(objTVL002);
                    db.SaveChanges();
                    objResponseMsg.HeaderId = objTVL002.HeaderId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Reference document saved successfully.";
                }
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult UpdateRefDocumentHeader(int headerId, string columnName, string columnValue)
        {
            bool isValid = false;
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            TVL002 objtvl002 = new TVL002();
            try
            {

                if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
                {
                    columnValue = columnValue.Trim();
                    if (columnName == "IssueDate")
                    {
                        if (CheckDate(columnValue))
                        {
                            isValid = true;
                        }
                    }
                    else
                    {
                        isValid = true;
                    }
                    if (isValid)
                    {
                        db.SP_TVL_UPDATE_REFDOCUMENTHEADER_COLUMN(headerId, columnName, columnValue);
                        if (headerId > 0)
                        {
                            objtvl002 = db.TVL002.Where(i => i.HeaderId == headerId).FirstOrDefault();
                            objtvl002.EditedBy = objClsLoginInfo.UserName;
                            objtvl002.EditedOn = DateTime.Now;
                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = "Updated Successfully";
                        }
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Invalid Date";
                    }

                }


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        private bool isDuplicateRefDoc(string projectNo, string refDocument, int revNo, int? headerId = null)
        {
            if (headerId.HasValue)
            {
                return db.TVL002.Any(c => c.HeaderId != headerId.Value && c.ProjectNo == projectNo && c.RefDocument == refDocument && c.RefDocRevNo == revNo);
            }
            else
            {
                return db.TVL002.Any(c => c.ProjectNo == projectNo && c.RefDocument == refDocument && c.RefDocRevNo == revNo);
            }
        }

        #endregion
        #region Ref Document Inline Headerdata
        public ActionResult GetTravelerRefDataPartial(string Status, string Projectno)
        {
            ViewBag.status = Status;
            ViewBag.Project = Projectno;
            ViewBag.lntintervention = Manager.GetSubCatagories("Traveler Doc Type", "02", objClsLoginInfo.Location).Select(i => new CategoryData { Value = i.Description, Code = i.Code, CategoryDescription = i.Description }).ToList();
            return PartialView("_GetHeaderRefDocumentPartial");
        }
        [HttpPost]
        public ActionResult GetTravelerRefHeaderData(JQueryDataTableParamModel param)
        {
            try
            {

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                //string whereCondition = "1=1";
                string whereCondition = "1=1 AND (HeaderId IN (SELECT (Select top 1  HeaderId FROM TVL002 WHERE RefDocument=TVL1.RefDocument AND ProjectNo='" + param.Project.Trim() + "'  ORDER BY RefDocRevNo desc, HeaderId desc ) AS HeaderId FROM TVL002 TVL1 GROUP BY ProjectNo, RefDocument)) ";
                // whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "tvl010.BU", "tvl010.Location");
                string strSortOrder = string.Empty;
                if (!string.IsNullOrEmpty(param.Project))
                {
                    whereCondition += " AND UPPER(ProjectNo) = '" + param.Project.Trim().ToUpper() + "'";
                }
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (ProjectNo like '%" + param.sSearch
                         + "%' or RefDocument like '%" + param.sSearch
                          + "%' or RefDocRevNo like '%" + param.sSearch
                           + "%' or PlanningRemarks  like '%" + param.sSearch
                            + "%' or DocType like '%" + param.sSearch
                        + "%')";
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                var lstResult = db.SP_TVL_GET_REFDOCUMENT_HEADER_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                var lstResultFinal = (from uc in lstResult
                                      select new
                                      {
                                          ROW_NO = uc.ROW_NO,
                                          TotalCount = uc.TotalCount,
                                          HeaderId = uc.HeaderId,
                                          ProjectNo = uc.ProjectNo,
                                          RefDocument = uc.RefDocument,
                                          RefDocRevNo = uc.RefDocRevNo,
                                          PlanningRemarks = uc.PlanningRemarks,
                                          CreatedBy = uc.CreatedBy,
                                          CreatedOn = uc.CreatedOn,
                                          EditedBy = uc.EditedBy,
                                          EditedOn = uc.EditedOn,
                                          DocType = uc.DocType,
                                          IsInUsed = UsedInTravelerOperation(uc.HeaderId),
                                          IssueDate = uc.IssueDate

                                      }).ToList();
                List<SelectListItem> BoolenList = new List<SelectListItem>() { new SelectListItem { Text = "Yes", Value = "Yes" }, new SelectListItem { Text = "No", Value = "No" } };

                int newRecordId = 0;
                var newRecord = new[] {
                                    Helper.GenerateHidden(newRecordId, "HeaderId", ""),
                                    Helper.GenerateTextbox(newRecordId, "RefDocument", "", "", false, "", "100"),
                                    Helper.GenerateTextbox(newRecordId, "RefDocRevNo", "", "", false, "", "4"),
                                    Helper.GenerateTextbox(newRecordId, "IssueDate"),
                                    Helper.GenerateTextbox(newRecordId, "PlanningRemarks", "", "", false, "", "300"),
                                     Helper.HTMLAutoComplete(newRecordId,"txtdoctype","","",false,"","TestDocType")+""+Helper.GenerateHidden(newRecordId,"TestDocType"),
                                    "",
                                    Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveRefDocumentHeader();")
                                };

                var data = (from uc in lstResultFinal
                            select new[]
                            {
                               Convert.ToString(uc.HeaderId),
                             !uc.IsInUsed?Helper.GenerateTextbox(uc.HeaderId, "RefDocument", uc.RefDocument, "UpdateRefDocumentHeader(this, "+ uc.HeaderId +")", false, "", "100") : uc.RefDocument,
                             !uc.IsInUsed?Helper.GenerateTextbox(uc.HeaderId, "RefDocRevNo", Convert.ToString(uc.RefDocRevNo), "UpdateRefDocumentHeader(this, "+ uc.HeaderId +")", false, "", "4") : Convert.ToString(uc.RefDocRevNo),
                            !uc.IsInUsed? Helper.GenerateTextbox(uc.HeaderId, "IssueDate",uc.IssueDate==null || uc.IssueDate.Value==DateTime.MinValue ? "":Convert.ToDateTime(uc.IssueDate).ToString("yyyy-MM-dd") , "UpdateRefDocumentHeader(this, "+ uc.HeaderId +");",false, "", "") :uc.IssueDate!=null? Convert.ToDateTime(uc.IssueDate).ToString("yyyy-MM-dd"):null,
                             !uc.IsInUsed?Helper.GenerateTextbox(uc.HeaderId, "PlanningRemarks", uc.PlanningRemarks, "UpdateRefDocumentHeader(this, "+ uc.HeaderId +")", false, "", "300") : uc.PlanningRemarks,
                             !uc.IsInUsed?Helper.HTMLAutoComplete(uc.HeaderId, "txtdoctype", uc.DocType, "UpdateRefDocumentHeader(this, "+ uc.HeaderId +")", false, "","TestDocType",false, "50") : uc.DocType,
                             Helper.GenerateActionIcon(uc.HeaderId, "Attachment", "Attach Refrence Document", "fa fa-paperclip", "OpenRefDocument("+ uc.HeaderId +","+(uc.IsInUsed?0:1)+")", "" ),
                               "<nobr>"+
                                Helper.GenerateActionIcon(uc.HeaderId, "Delete", "Delete Refrence Document", "fa fa-trash-o", "DeleteRefDoc("+ uc.HeaderId +")", "",UsedInTravelerOperation(uc.HeaderId) )
                                + Helper.GenerateActionIcon(uc.HeaderId, "Revision", "View Previous Revisions", "fa fa-history", "RefDocumentRevisions(" + uc.HeaderId + ");", "", !db.TVL002.Any(c=> c.ProjectNo == uc.ProjectNo && c.RefDocument == uc.RefDocument && c.HeaderId != uc.HeaderId))
                               //+ Helper.GenerateActionIcon(uc.HeaderId,"Timeline","Timeline","fa fa-clock-o", "ShowTimeline(\"/TVL/MaintainRefDoc/ShowTimeline?HeaderID="+ uc.HeaderId +"\");") +
                                 +"<i title='View Timeline' href='javascript:void(0)' onclick=ShowTimeline('/TVL/MaintainRefDoc/ShowTimeline?HeaderID=" + Convert.ToInt32(uc.HeaderId) + "'"+"); class='iconspace fa fa-clock-o'></i>" +
                               //+ Helper.GenerateActionIcon(uc.HeaderId, "Timeline", "Timeline", "fa fa-clock-o", "ShowTimeline(\"/TVL/MaintainRefDoc/ShowTimeline?HeaderID="+ uc.HeaderId +"\");") +
                               "</nobr>"
                           }).ToList();


                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        #region Copy Ref Documents
        [HttpPost]
        public ActionResult LoadCopyRefDocPartial()
        {
            return PartialView("_CopyRefDocPartial");
        }

        [HttpPost]
        public ActionResult CopyAllRefDoc(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                string sourceProject = Convert.ToString(fc["txtProjectNo"]).Trim();
                string[] destProjects = Convert.ToString(fc["txtDesitinationProjectNo"]).Split(',');

                List<TVL002> newLstTVL002 = new List<TVL002>();

                foreach (string dProj in destProjects)
                {
                    if (!string.IsNullOrWhiteSpace(dProj.Trim()))
                    {
                        List<TVL002> lstTVL002 = db.TVL002.Where(c => c.ProjectNo == sourceProject).ToList();
                        foreach (TVL002 objTVL002 in lstTVL002)
                        {
                            if (!db.TVL002.Any(c => c.ProjectNo == dProj && c.RefDocument == objTVL002.RefDocument))
                            {
                                newLstTVL002.Add(new TVL002
                                {
                                    ProjectNo = dProj.Trim(),
                                    RefDocument = objTVL002.RefDocument.Trim(),
                                    RefDocRevNo = objTVL002.RefDocRevNo,
                                    PlanningRemarks = objTVL002.PlanningRemarks,
                                    DocType = objTVL002.DocType,
                                    CreatedBy = objClsLoginInfo.UserName,
                                    CreatedOn = DateTime.Now,
                                });
                            }
                        }
                    }
                }
                db.TVL002.AddRange(newLstTVL002);
                db.SaveChanges();
                #region Copy Reference document
                foreach (var dProj in newLstTVL002)
                {
                    if (!string.IsNullOrWhiteSpace(dProj.ProjectNo.Trim()))
                    {
                        List<TVL002> lstTVL002 = db.TVL002.Where(c => c.ProjectNo == sourceProject).ToList();
                        foreach (TVL002 objTVL002 in lstTVL002)
                        {
                            string souceDocument = "TVL002//" + objTVL002.HeaderId;
                            string destDocument = "TVL002//" + dProj.HeaderId;
                            //(new clsFileUpload()).CopyFolderContentsAsync(souceDocument, destDocument);
                            Utility.Controllers.FileUploadController _objFUC = new Utility.Controllers.FileUploadController();
                            _objFUC.CopyDataOnFCSServerAsync(souceDocument, destDocument, "TVL002", objTVL002.HeaderId, "TVL002", dProj.HeaderId, DESServices.CommonService.GetUseIPConfig, DESServices.CommonService.objClsLoginInfo.UserName, 0, true);
                        }
                    }
                }
                #endregion
                objResponseMsg.Key = true;
                objResponseMsg.Value = "All reference documents copy successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAllProjects(string sProj)
        {
            try
            {
                var items = (from li in db.TVL001
                             where li.ProjectNo != sProj
                             select new
                             {
                                 id = li.ProjectNo,
                                 text = li.ProjectNo,
                             }).ToList();

                return Json(items, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region ExcelHelper

        [HttpPost]
        [SessionExpireFilter]
        public ActionResult ImportExcel(HttpPostedFileBase upload, FormCollection fc)
        {

            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            //Traveler - Template Ref Document with example
            if (Path.GetExtension(upload.FileName) == ".xlsx" || Path.GetExtension(upload.FileName) == ".xls")
            {
                ExcelPackage package = new ExcelPackage(upload.InputStream);

                bool isError;
                string project = fc["ProjectNo"];
                DataSet ds = ToAddRefDoc(package, project, out isError);
                if (!isError)
                {
                    try
                    {
                        //import Line Data
                        List<TVL002> lstTVL002 = new List<TVL002>();
                        foreach (DataRow item in ds.Tables[0].Rows)
                        {
                            TVL002 obj = new TVL002();
                            string proj = item.Field<string>("Project No").ToString();
                            obj.ProjectNo = db.TVL001.Where(x => x.ProjectNo.Trim().ToLower() == proj.Trim().ToLower()).Select(x => x.ProjectNo).FirstOrDefault();
                            obj.RefDocument = item.Field<string>("Ref Document").ToString().Trim();
                            obj.RefDocRevNo = Convert.ToInt32(item.Field<string>("Rev No"));
                            obj.PlanningRemarks = item.Field<string>("Description").ToString();
                            obj.DocType = getcategoryData("Traveler Doc Type", item.Field<string>("Doc Type"));
                            obj.IssueDate = Convert.ToDateTime(item.Field<string>("Date"));
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;

                            lstTVL002.Add(obj);
                        }
                        db.TVL002.AddRange(lstTVL002);
                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Ref Doc imported successfully";
                    }
                    catch (Exception ex)
                    {
                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    }
                }
                else
                {
                    objResponseMsg = ExportToExcel(ds);
                }
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Excel file is not valid";
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public clsHelper.ResponceMsgWithFileName ExportToExcel(DataSet ds)
        {
            clsHelper.ResponceMsgWithFileName objResponseMsg = new clsHelper.ResponceMsgWithFileName();
            try
            {
                MemoryStream ms = new MemoryStream();
                using (FileStream fs = System.IO.File.OpenRead(Server.MapPath("~/Resources/TRAVELER/Traveler - Error Template Ref Document.xlsx")))
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(fs))
                    {
                        ExcelWorkbook excelWorkBook = excelPackage.Workbook;
                        ExcelWorksheet excelWorksheet = excelWorkBook.Worksheets.First();
                        int i = 2;
                        foreach (DataRow item in ds.Tables[0].Rows)
                        {
                            //operation no.
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(6)))
                            {
                                excelWorksheet.Cells[i, 1].Value = item.Field<string>(0) + " (Note: " + item.Field<string>(6).ToString() + " )";
                                excelWorksheet.Cells[i, 1].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 1].Value = item.Field<string>(0); }

                            //activity
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(7)))
                            {
                                excelWorksheet.Cells[i, 2].Value = item.Field<string>(1) + " (Note: " + item.Field<string>(7).ToString() + " )";
                                excelWorksheet.Cells[i, 2].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 2].Value = item.Field<string>(1); }

                            //ref document
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(8)))
                            {
                                excelWorksheet.Cells[i, 3].Value = item.Field<string>(2) + " (Note: " + item.Field<string>(8).ToString() + " )";
                                excelWorksheet.Cells[i, 3].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 3].Value = item.Field<string>(2); }

                            //rev no
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(9)))
                            {
                                excelWorksheet.Cells[i, 4].Value = item.Field<string>(3) + " (Note: " + item.Field<string>(9).ToString() + " )";
                                excelWorksheet.Cells[i, 4].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 4].Value = item.Field<string>(3); }

                            //date
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(10)))
                            {
                                excelWorksheet.Cells[i, 5].Value = item.Field<string>(4) + " (Note: " + item.Field<string>(10).ToString() + " )";
                                excelWorksheet.Cells[i, 5].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 5].Value = item.Field<string>(4); }

                            //description
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(11)))
                            {
                                excelWorksheet.Cells[i, 6].Value = item.Field<string>(5) + " (Note: " + item.Field<string>(11).ToString() + " )";
                                excelWorksheet.Cells[i, 6].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 6].Value = item.Field<string>(5); }

                            i++;
                        }

                        excelPackage.SaveAs(ms);
                    }
                }

                ms.Position = 0;

                string fileName;
                string excelFilePath = Helper.ExcelFilePath("ErrorList", out fileName);

                Byte[] bin = ms.ToArray();
                System.IO.File.WriteAllBytes(excelFilePath, bin);

                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error";
                objResponseMsg.FileName = fileName;
                return objResponseMsg;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
        }

        public string getcategoryData(string category, string data)
        {
            string intervention = (from glb2 in db.GLB002
                                   join glb1 in db.GLB001 on glb2.Category equals glb1.Id
                                   where glb1.Category == category && glb2.Description == data
                                   select glb2.Description).FirstOrDefault();
            return intervention;
        }

        public DataSet ToAddRefDoc(ExcelPackage package,string project, out bool isError)
        {
            ExcelWorksheet excelWorksheet = package.Workbook.Worksheets.First();
            DataTable dt = new DataTable();
            isError = false;
            try
            {
                #region Validation for Header
                #endregion

                #region Read Lines data from excel 
                foreach (var firstRowCell in excelWorksheet.Cells[1, 1, 1, excelWorksheet.Dimension.End.Column])
                {
                    dt.Columns.Add(firstRowCell.Text);
                }
                for (var rowNumber = 2; rowNumber <= excelWorksheet.Dimension.End.Row; rowNumber++)
                {
                    var row = excelWorksheet.Cells[rowNumber, 1, rowNumber, excelWorksheet.Dimension.End.Column];
                    var newRow = dt.NewRow();
                    if (excelWorksheet.Cells[rowNumber, 1].Value != null
                       || excelWorksheet.Cells[rowNumber, 2].Value != null
                       || excelWorksheet.Cells[rowNumber, 3].Value != null
                       )
                    {
                        foreach (var cell in row)
                        {
                            newRow[cell.Start.Column - 1] = cell.Text;
                        }
                    }
                    else
                    { break; }
                    dt.Rows.Add(newRow);
                }
                #endregion

                #region Validation for Doc
                dt.Columns.Add("ProjectNoErrorMsg", typeof(string));
                dt.Columns.Add("DocTypeErrorMsg", typeof(string));
                dt.Columns.Add("RefDocumentErrorMsg", typeof(string));
                dt.Columns.Add("RefDocRevErrorMsg", typeof(string));
                dt.Columns.Add("DateErrorMsg", typeof(string));
                dt.Columns.Add("DescErrorMsg", typeof(string));

                IQueryable<CategoryData> lstDocType = (from glb2 in db.GLB002
                                                       join glb1 in db.GLB001 on glb2.Category equals glb1.Id
                                                       where glb1.Category == "Traveler Doc Type"
                                                       select glb2).Select(i => new { i.Description }).Distinct().Select(i => new CategoryData { Description = i.Description }).Distinct();

                foreach (DataRow item in dt.Rows)
                {
                    string errorMessage = string.Empty;
                    int RevNo;
                    DateTime IssueDate;
                    //project no
                    string projectno = item.Field<string>("Project No");
                    if (string.IsNullOrWhiteSpace(projectno))
                    {
                        item["ProjectNoErrorMsg"] = "Project No is Required";
                        isError = true;
                    }
                    else
                    {
                        if (projectno.Trim() != project.Trim())
                        {
                            item["ProjectNoErrorMsg"] = "Please enter valid Project";
                            isError = true;
                        }
                        //if (projectno.Trim().Length > 12)
                        //{
                        //    item["ProjectNoErrorMsg"] = "Project No have maximum limit of 12 characters";
                        //    isError = true;
                        //}
                        //else
                        //{
                        //    bool isProjectExist = db.TVL001.Any(x => x.ProjectNo.Trim() == projectno.Trim());
                        //    if (!isProjectExist)
                        //    {
                        //        item["ProjectNoErrorMsg"] = "Project No  is Invalid";
                        //        isError = true;
                        //    }
                        //}
                    }

                    //doc type
                    string doctype = item.Field<string>("Doc Type");
                    if (string.IsNullOrWhiteSpace(doctype))
                    {
                        item["DocTypeErrorMsg"] = "Doc Type is Required";
                        isError = true;
                    }
                    else
                    {
                        if (!lstDocType.Any(x => x.Description.ToLower() == doctype.ToLower()))
                        {
                            item["DocTypeErrorMsg"] = "Doc Type is Invalid"; isError = true;
                        }
                    }

                    //ref doc
                    string Refdoc = item.Field<string>("Ref Document");
                    int? RefRev = Convert.ToInt32(item.Field<string>("Rev No"));
                    bool isExist = false; 
                   
                    if (string.IsNullOrWhiteSpace(Refdoc))
                    {
                        item["RefDocumentErrorMsg"] = "Ref Document is Required";
                        isError = true;
                    }
                    else
                    {
                        isExist = db.TVL002.Where(x => x.RefDocument.Trim() == Refdoc.Trim() && x.RefDocRevNo == RefRev).Any();
                        if (Refdoc.Trim().Length > 100)
                        {
                            item["RefDocumentErrorMsg"] = "Ref Doc have maximum limit of 100 characters";
                            isError = true;
                        }
                        if (isExist)
                        {
                            item["RefDocumentErrorMsg"] += "Ref Doc is already exists";
                            isError = true;
                        }
                    }

                    //ref doc rev
                    if (!int.TryParse(item.Field<string>("Rev No"), out RevNo))
                    {
                        item["RefDocRevErrorMsg"] = item.Field<string>("Rev No") + " is Not Valid, Please Enter Only Numeric Value";
                        isError = true;
                    }

                    //issue date
                    if (!DateTime.TryParse(item.Field<string>("Date"), out IssueDate))
                    {
                        item["DateErrorMsg"] = item.Field<string>("Date") + " is Not Valid, Please Enter Proper Date(dd/mm/yyyy).";
                        isError = true;
                    }

                    if (string.IsNullOrWhiteSpace(item.Field<string>("Description")))
                    {
                        item["DescErrorMsg"] = "Description is Required";
                        isError = true;
                    }
                    else
                    {
                        if (item.Field<string>("Description").Trim().Length > 300)
                        {
                            item["DescErrorMsg"] = "Description have maximum limit of 300 characters";
                            isError = true;
                        }
                    }
                }
                #endregion

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            DataSet ds = new DataSet();
            ds.Tables.Add(dt);

            return ds;
        }

        #endregion
        protected bool CheckDate(String date)
        {
            try
            {
                DateTime dt = DateTime.Parse(date);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }

    public class RefDocuments
    {
        public string ReferenceDocument { get; set; }
        public string ReferenceDocumentRevNo { get; set; }
    }

}