﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQSImplementation;
using IEMQS.Models;
using IEMQS.Areas.Utility.Models;
using System.IO;
using OfficeOpenXml;
using System.Data;
using System.Data.Entity.SqlServer;
using IEMQS.Areas.NDE.Models;
using System.Configuration;
using System.Text;
using System;
using System.Net;

namespace IEMQS.Areas.TVL.Controllers
{
    public class DigitalSignatureController : clsBase
    {
        // GET: TVL/DigitalSignature
        #region Index Page
        [SessionExpireFilter]//, UserPermissions, AllowAnonymous]
        public ActionResult Index()
        {
            ViewBag.Title = "Digital Signature";
            return View();
        }
        public ActionResult GetIndexGridDataPartial(string status)
        {
            ViewBag.status = status;
            return PartialView("_GetIndexGridDataPartial");
        }
        public ActionResult LoadeTravelerIndexDataTable(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "tvl40.BU", "tvl40.Location");
                whereCondition += " and  (ISNULL(IsCompleted,'') = 1 or ISNULL(IsCompleted,'') != 1 )  AND dbo.FN_TVL_IS_ALL_OPERATIONS_CLEARED(tvl40.ProjectNo,tvl40.BU,tvl40.Location,tvl40.AssemblyNo,tvl40.TravelerNo,tvl40.SubAssemblyNo)=1";

                if (param.Status.ToUpper() == "PENDING")
                {
                    whereCondition += " and  ISNULL(IsDigitalSignature,'') != 1";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (tvl40.ProjectNo like '%" + param.sSearch
                                    + "%' or tvl40.TravelerNo like '%" + param.sSearch
                                    + "%' or tvl40.BU like '%" + param.sSearch
                                    + "%' or tvl40.Location like '%" + param.sSearch
                                    + "%' or tvl40.AssemblyNo like '%" + param.sSearch
                                    + "%' or tvl40.Customer like '%" + param.sSearch
                                    + "%' or tvl40.TravelerRevNo like '%" + param.sSearch
                                    + "%')";
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_TVL_OFFER_INDEX_GETALLDATA(startIndex, endIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstResult.Select(i => i.TotalCount).FirstOrDefault();

                var res = from uc in lstResult
                          select new[] {
                            Convert.ToString(uc.HeaderId),
                            Convert.ToString(uc.Project),
                            Convert.ToString(uc.TravelerNo),
                            Convert.ToString(uc.BU),
                            Convert.ToString(uc.Location),
                            Convert.ToString(uc.AssemblyNo),
                            Convert.ToString(uc.SubAssemblyNo),
                            Helper.GenerateActionIcon(Convert.ToInt32(uc.HeaderId), "View", "View Details", "fa fa-eye", "", WebsiteURL+"/TVL/DigitalSignature/Details/" + Convert.ToString(uc.HeaderId))
                          + Helper.GenerateActionIcon(uc.HeaderId, "Print", "Print Traveler", "fa fa-print", "PrintTVLReport('"+uc.Project+"','"+uc.TravelerNo+"',"+uc.TravelerRevNo+",'"+uc.AssemblyNo+"','"+uc.SubAssemblyNo+"',true)","",false)
                          };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }
        #endregion

        #region Header Details Page
        [SessionExpireFilter]//, UserPermissions, AllowAnonymous]
        public ActionResult Details(int? id)
        {
            TVL040 objTVL040 = new TVL040();
            ViewBag.Title = "Digital Signature";
            try
            {
                if (id > 0)
                {
                    objTVL040 = db.TVL040.Where(i => i.HeaderId == id).FirstOrDefault();
                    ViewBag.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objTVL040.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                    ViewBag.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objTVL040.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                    ViewBag.Completedby = objTVL040.CompletedBy != null ? (objTVL040.CompletedBy + " - " + Manager.GetUserNameFromPsNo(objTVL040.CompletedBy)) : "";
                    ViewBag.Completedon = (objTVL040.CompletedOn != null ? objTVL040.CompletedOn.Value.ToString("dd/MM/yyyy hh:mm tt") : "");
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return View(objTVL040);
        }

        public ActionResult GetHeaderDataPartial(string Status, TVL040 objTVL040)
        {
            ViewBag.Status = Status;
            objTVL040.BU = objTVL040.BU.Split('-')[0];
            objTVL040.Location = objTVL040.Location.Split('-')[0];
            return PartialView("_GetHeaderDataPartial", objTVL040);
        }
        [HttpPost]
        public ActionResult LoadHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";
                string strSortOrder = string.Empty;


                whereCondition += " and TravelerNo='" + param.TravelerNo + "' and ProjectNo='" + param.Project + "'"
                                    + "and AssemblyNo='" + param.IdentifictionNo + "' and Customer='" + param.Customer + "' "
                                    + "and BU='" + param.BU + "' and Location='" + param.Location + "'"
                                    + "and SubAssemblyNo=" + Convert.ToInt32(param.subassembly) + " ";
                bool ISAllClear = db.Database.SqlQuery<bool>("SELECT DBO.FN_TVL_IS_ALL_OPERATIONS_CLEARED('" + param.Project + "','" + param.BU + "','" + param.Location + "','" + param.IdentifictionNo + "','" + param.TravelerNo + "'," + param.subassembly + ")").FirstOrDefault();
                int headerid = Convert.ToInt32(param.Headerid);
                bool IsDigitalSignature = false; bool IsCompleted = false;
                bool ispending = false;
                var objTVL040 = db.TVL040.Where(x => x.HeaderId == headerid).FirstOrDefault();
                if (objTVL040 != null)
                {
                    var lstDigitalSignature = db.TVL040.Where(x => x.TravelerNo == objTVL040.TravelerNo
                                              && x.TravelerRevNo == objTVL040.TravelerRevNo
                                              && x.ProjectNo == objTVL040.ProjectNo
                                              && x.BU == objTVL040.BU
                                              && x.Location == objTVL040.Location
                                              && x.AssemblyNo == objTVL040.AssemblyNo
                                              && x.SubAssemblyNo == objTVL040.SubAssemblyNo
                                              )
                                              .ToList();
                    if (lstDigitalSignature.All(x => x.IsDigitalSignature == true))
                    {
                        IsDigitalSignature = true;
                    }
                    if (lstDigitalSignature.All(x => x.IsCompleted == true))
                    {
                        IsCompleted = true;
                    }

                }
                if (ISAllClear && IsCompleted && !IsDigitalSignature)
                {
                    ispending = true;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "tvl040.BU", "tvl040.Location");
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (ProjectNo like '%" + param.sSearch
                                    + "%' or TravelerNo like '%" + param.sSearch
                                    + "%' or BU like '%" + param.sSearch
                                    + "%' or Location like '%" + param.sSearch
                                    + "%' or AssemblyNo like '%" + param.sSearch
                                    + "%' or Customer like '%" + param.sSearch
                                    + "%' or TravelerRevNo like '%" + param.sSearch
                                    + "%')";
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                var lstResult = db.SP_TVL_GET_OFFERDATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                var data = (from uc in lstResult
                            select new[]
                            {
                            Convert.ToString(uc.HeaderId),
                            Helper.GenerateHidden(Convert.ToInt32(uc.HeaderId), "HeaderId", Convert.ToString(uc.HeaderId)),
                            uc.OperationNo.Value.ToString("G29"),
                            Convert.ToString("R"+uc.OperationRevisionNo),
                            Convert.ToString(uc.Activity),
                            Convert.ToBoolean(uc.IsHeaderLine) ? "" : Convert.ToString(uc.RefDocument) != null ? "* "+ Convert.ToString(uc.RefDocument).Replace(",","<br/> * ") : "--",
                            Convert.ToBoolean(uc.IsHeaderLine) ? "" : Convert.ToString(uc.RefDocRevNo) != null ? "* "+ Convert.ToString(uc.RefDocRevNo).Replace(",","<br/> * ") : "--",
                            Convert.ToBoolean(uc.IsHeaderLine) ? "" : Convert.ToString(uc.itenrationNo),
                            Convert.ToBoolean(uc.IsHeaderLine) ? "" : Convert.ToString(uc.SubAssemblyNo),
                            Convert.ToBoolean(uc.IsHeaderLine) ? "" : Convert.ToString(uc.InspectionStatus),
                            Convert.ToBoolean(uc.IsHeaderLine) ? "" : Convert.ToString(uc.LnTInspectorResult),
                            "<nobr><center>"
                            +"<i title='View Timeline' href='javascript:void(0)' onclick=ShowTimeline('/TVL/OfferInspection/ShowTimelineOfferInspectionPartial?headerId=" + Convert.ToInt32(uc.HeaderId) + "'"+"); class='iconspace fa fa-clock-o'></i>"
                            + Manager.generateTVLGridButtons(uc.ProjectNo,uc.Customer,uc.TravelerNo,uc.TravelerRevNo,uc.AssemblyNo,uc.BU,uc.Location,uc.SubAssemblyNo,uc.OperationNo.Value.ToString("G29"),uc.Activity,uc.HeaderId,"Traveler Details",40,"Traveler Details","Request Details","Traveler Request Details")
                            +"</center></nobr>"
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition,
                    ispending = ispending,
                    IsDigitalSignature = IsDigitalSignature,
                    IsCompleted = IsCompleted
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AddDigitalSignature(string strHeaderIds, bool isDetailPage = false)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                string Message = string.Empty;
                List<string> lstSuccess = new List<string>();
                string[] arrayHeaderIds = strHeaderIds.Split(',').ToArray();
                for (int i = 0; i < arrayHeaderIds.Length; i++)
                {
                    int HeaderId = Convert.ToInt32(arrayHeaderIds[i]);
                    var objTVL040 = db.TVL040.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    if (objTVL040 != null)
                    {
                        var lstCompleted = db.TVL040.Where(x => x.TravelerNo == objTVL040.TravelerNo
                                                  && x.TravelerRevNo == objTVL040.TravelerRevNo
                                                  && x.ProjectNo == objTVL040.ProjectNo
                                                  && x.BU == objTVL040.BU
                                                  && x.Location == objTVL040.Location
                                                  && x.AssemblyNo == objTVL040.AssemblyNo
                                                  && x.SubAssemblyNo == objTVL040.SubAssemblyNo)
                                                  .ToList();
                        if (lstCompleted.Count > 0)
                        {
                            if (lstCompleted.All(x => x.IsCompleted == true))
                            {
                                lstCompleted.ForEach(x =>
                                {
                                    x.IsDigitalSignature = true;
                                    x.DigitalSignatureBy = objClsLoginInfo.UserName;
                                    x.DigitalSignatureOn = DateTime.Now;
                                });

                                db.SaveChanges();

                                #region generate pdf

                                string filenameformat = objTVL040.ProjectNo + "_" + objTVL040.TravelerNo + "_R" + objTVL040.TravelerRevNo + "_" + objTVL040.AssemblyNo + "_" + objTVL040.SubAssemblyNo;
                                string downloadpath = "~/Resources/TRAVELER/CompleteTraveler/";
                                string Reporturl = "/TVL/Traveler";

                                List<SSRSParam> reportParams = new List<SSRSParam>();
                                reportParams.Add(new SSRSParam { Param = "Project", Value = objTVL040.ProjectNo });
                                reportParams.Add(new SSRSParam { Param = "TravelerNo", Value = objTVL040.TravelerNo });
                                reportParams.Add(new SSRSParam { Param = "TravelerRevNo", Value = objTVL040.TravelerRevNo.ToString() });
                                reportParams.Add(new SSRSParam { Param = "AssemblyNo", Value = objTVL040.AssemblyNo });
                                reportParams.Add(new SSRSParam { Param = "SubAssemblyNo", Value = objTVL040.SubAssemblyNo.ToString() });

                                string file = (new Utility.Controllers.GeneralController()).GeneratePDFfromSSRSreport(Server.MapPath(downloadpath + filenameformat + ".pdf"), Reporturl, reportParams);
                                #endregion
                                lstSuccess.Add(HeaderId.ToString());
                            }
                        }
                    }
                }

                objResponseMsg.Key = true;
                if (isDetailPage)
                {
                    objResponseMsg.Value = "Traveler has been Signed successfully";
                }
                else
                {
                    objResponseMsg.Value = string.Format("{0} Traveler(s) has been Signed successfully", lstSuccess.Count());
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}