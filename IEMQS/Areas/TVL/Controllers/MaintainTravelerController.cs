﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQSImplementation;
using IEMQS.Models;
using IEMQS.Areas.Utility.Models;
using System.IO;
using OfficeOpenXml;
using System.Data;
using System.Data.Entity.SqlServer;
using IEMQS.Areas.NDE.Models;
using System.Configuration;
using System.Text;
using IEMQS.Areas.TVL.Models;
using System.Drawing;

namespace IEMQS.Areas.TVL.Controllers
{
    public class MaintainTravelerController : clsBase
    {
        // GET: TVL/MaintainTraveler
        // [SessionExpireFilter, UserPermissions, AllowAnonymous]


        #region Index Page
        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult Index()
        {

            UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();

            if (objUserRoleAccessDetails.UserDesignation == clsImplementationEnum.UserAccessRole.Releaser.GetStringValue())
                ViewBag.Title = "Release Traveler";
            else if (objUserRoleAccessDetails.UserDesignation == clsImplementationEnum.UserAccessRole.Approver.GetStringValue())
                ViewBag.Title = "Approve Traveler";
            else
                ViewBag.Title = "Maintain Traveler";

            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            return View();
        }
        public ActionResult GetIndexGridDataPartial(string status)
        {
            ViewBag.status = status;
            return PartialView("_GetIndexGridDataPartial");
        }

        public ActionResult LoadTravelerIndexDataTable(JQueryDataTableParamModel param, string status)
        {
            try

            {
                UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "tvl010.BU", "tvl010.Location");

                if (status.ToUpper() == "PENDING")
                {


                    if ((objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QI1.GetStringValue() || objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QI2.GetStringValue() || objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QI3.GetStringValue()) && (objUserRoleAccessDetails.UserDesignation == clsImplementationEnum.UserAccessRole.Approver.GetStringValue()))
                    {
                        whereCondition += " and tvl010.PlanningSubmittedBy is not null and  tvl010.Status in('" + clsImplementationEnum.TravelerHeaderStatus.SubmittedToQI.GetStringValue() + "')";
                    }
                    else if ((objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.WE1.GetStringValue() || objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.WE2.GetStringValue() || objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.WE3.GetStringValue()) && (objUserRoleAccessDetails.UserDesignation == clsImplementationEnum.UserAccessRole.Approver.GetStringValue()))
                    {
                        whereCondition += " and tvl010.QIApprovedBy is not null and tvl010.Status in('" + clsImplementationEnum.TravelerHeaderStatus.SubmittedToWE.GetStringValue() + "' )";
                    }

                    else if ((objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.PLNG1.GetStringValue() || objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.PLNG2.GetStringValue() || objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.PLNG3.GetStringValue()) && (objUserRoleAccessDetails.UserDesignation == clsImplementationEnum.UserAccessRole.Initiator.GetStringValue()))
                    {
                        // whereCondition += " and  tvl010.WEApprovedBy is not null and   tvl010.Status in('" + clsImplementationEnum.TravelerHeaderStatus.SubmittedToPLANNING.GetStringValue() + "','" + clsImplementationEnum.TravelerHeaderStatus.ApprovedbyPlanning.GetStringValue() + "' )";
                        whereCondition += " and  (tvl010.Status in('" + clsImplementationEnum.TravelerHeaderStatus.SubmittedToPLANNING.GetStringValue() + "' )  or tvl010.Status in('" + clsImplementationEnum.TravelerHeaderStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.TravelerHeaderStatus.ReturnedbyQI.GetStringValue() + "','" + clsImplementationEnum.TravelerHeaderStatus.ReturnedbyWE.GetStringValue() + "','" + clsImplementationEnum.TravelerHeaderStatus.Returnedbyplanning.GetStringValue() + "'))";
                    }

                    else
                    {
                        whereCondition += " and tvl010.Status in('" + clsImplementationEnum.TravelerHeaderStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.TravelerHeaderStatus.ReturnedbyQI.GetStringValue() + "','" + clsImplementationEnum.TravelerHeaderStatus.ReturnedbyWE.GetStringValue() + "','" + clsImplementationEnum.TravelerHeaderStatus.Returnedbyplanning.GetStringValue() + "')";
                    }
                }

                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    whereCondition += " and (tvl010.ProjectNo like '%" + param.sSearch + "%' or (tvl010.Customer) like '%" + param.sSearch + "%' or (LTRIM(RTRIM(tvl010.Location))+'-'+LTRIM(RTRIM(lcom2.t_desc))) like '%" + param.sSearch + "%' or (LTRIM(RTRIM(tvl010.BU))+'-'+LTRIM(RTRIM(bcom2.t_desc))) like '%" + param.sSearch + "%')";
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstHeader = db.SP_TVL_GET_TRAVELER_INDEX_DATA(startIndex, endIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from a in lstHeader
                          select new[] {
                        a.Project,
                        a.ProjectDescription,
                       a.Customer,
                        a.BU,
                        a.Location,
                        Helper.GenerateActionIcon(Convert.ToInt32(a.HeaderId), "View", "View Details", "fa fa-eye", "", WebsiteURL+"/TVL/MaintainTraveler/Details/" + Convert.ToString(a.HeaderId))
                    };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }

        #endregion

        #region Header Details Page
        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult Details(int? id)
        {
            TVL010 objTVL010 = new TVL010();
            try
            {


                if (id > 0)
                {
                    objTVL010 = db.TVL010.Where(i => i.HeaderId == id).FirstOrDefault();
                    ViewBag.Project = objTVL010.ProjectDescription;
                    ViewBag.Customer = objTVL010.Customer;
                    ViewBag.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objTVL010.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                    ViewBag.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objTVL010.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                }
                else
                {
                    ViewBag.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objClsLoginInfo.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                    objTVL010.Location = objClsLoginInfo.Location;
                    ViewBag.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == "02").Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                }

                UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();
                ViewBag.UserRole = objUserRoleAccessDetails.UserRole;
                if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QI1.GetStringValue() || objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QI2.GetStringValue() || objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QI3.GetStringValue())
                {
                    ViewBag.Returnremarkssize = "500";
                }
                else
                {
                    ViewBag.Returnremarkssize = "100";
                }

                if (objUserRoleAccessDetails.UserDesignation == clsImplementationEnum.UserAccessRole.Releaser.GetStringValue())
                    ViewBag.Title = "Release Traveller";
                else if (objUserRoleAccessDetails.UserDesignation == clsImplementationEnum.UserAccessRole.Approver.GetStringValue())
                    ViewBag.Title = "Approve Traveler";
                else
                    ViewBag.Title = "Maintain Traveler";
            }

            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return View(objTVL010);
        }

        public ActionResult GetTravelerHeaderDataPartial(string Status, string Projectno, string Location, string BU, string Customer)
        {
            ViewBag.status = Status;
            ViewBag.Project = Projectno;
            ViewBag.location = Location;
            ViewBag.bu = BU;
            ViewBag.Customer = Customer;
            string status = clsImplementationEnum.TravelerHeaderStatus.Draft.GetStringValue();

            UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;
            List<string> lstProCat = clsImplementationEnum.getTravelerIdentification().ToList();
            ViewBag.Identification = lstProCat.Select(i => new CategoryData { Value = i.ToString(), Code = i.ToString(), CategoryDescription = i.ToString() }).ToList();
            // ViewBag.Identification = new List<CategoryData> { new CategoryData { Value = "Assembly No", Code = "Assembly No", CategoryDescription = "Assembly No" }, new CategoryData { Value = "Sub Assembly No", Code = "Sub Assembly No", CategoryDescription = "Sub Assembly No" }, new CategoryData { Value = "Component No", Code = "Component No", CategoryDescription = "Component No" }, new CategoryData { Value = "Standard Traveler", Code = "Standard Traveler", CategoryDescription = "Standard Traveler" }  };
            return PartialView("_GetHeaderDataPartial");
        }

        [HttpPost]
        public ActionResult GetTravelerHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";
                // whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "tvl010.BU", "tvl010.Location");
                string strSortOrder = string.Empty;
                var qry = db.TVL010.Where(m => m.ProjectNo == param.Project).Select(m => m.Status).ToList();
                if (!string.IsNullOrEmpty(param.Project))
                {
                    whereCondition += " AND UPPER(tvl010.ProjectNo) = '" + param.Project.Trim().ToUpper() + "' AND UPPER(tvl010.Location) = '" + param.Location.Trim().ToUpper() + "'  AND UPPER(tvl010.BU) = '" + param.BU.Trim().ToUpper() + "'";
                }

                if (param.Status.ToUpper() == "PENDING")
                {
                    if ((objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QI1.GetStringValue() || objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QI2.GetStringValue() || objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QI3.GetStringValue()) && (objUserRoleAccessDetails.UserDesignation == clsImplementationEnum.UserAccessRole.Approver.GetStringValue()))
                    {
                        whereCondition += " and tvl010.PlanningSubmittedBy is not null and  tvl010.Status in('" + clsImplementationEnum.TravelerHeaderStatus.SubmittedToQI.GetStringValue() + "')";
                    }
                    else if ((objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.WE1.GetStringValue() || objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.WE2.GetStringValue() || objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.WE3.GetStringValue()) && (objUserRoleAccessDetails.UserDesignation == clsImplementationEnum.UserAccessRole.Approver.GetStringValue()))
                    {
                        whereCondition += " and tvl010.QIApprovedBy is not null and tvl010.Status in('" + clsImplementationEnum.TravelerHeaderStatus.SubmittedToWE.GetStringValue() + "' )";
                    }
                    else if ((objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.PLNG1.GetStringValue() || objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.PLNG2.GetStringValue() || objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.PLNG3.GetStringValue()) && (objUserRoleAccessDetails.UserDesignation == clsImplementationEnum.UserAccessRole.Initiator.GetStringValue()))
                    {
                        // whereCondition += " and  tvl010.WEApprovedBy is not null and   tvl010.Status in('" + clsImplementationEnum.TravelerHeaderStatus.SubmittedToPLANNING.GetStringValue() + "','" + clsImplementationEnum.TravelerHeaderStatus.ApprovedbyPlanning.GetStringValue() + "' )";
                        whereCondition += " and  (tvl010.Status in('" + clsImplementationEnum.TravelerHeaderStatus.SubmittedToPLANNING.GetStringValue() + "' )  or tvl010.Status in('" + clsImplementationEnum.TravelerHeaderStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.TravelerHeaderStatus.ReturnedbyQI.GetStringValue() + "','" + clsImplementationEnum.TravelerHeaderStatus.ReturnedbyWE.GetStringValue() + "','" + clsImplementationEnum.TravelerHeaderStatus.Returnedbyplanning.GetStringValue() + "'))";
                    }

                    else
                    {
                        whereCondition += " and tvl010.Status in('" + clsImplementationEnum.TravelerHeaderStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.TravelerHeaderStatus.ReturnedbyQI.GetStringValue() + "','" + clsImplementationEnum.TravelerHeaderStatus.ReturnedbyWE.GetStringValue() + "','" + clsImplementationEnum.TravelerHeaderStatus.Returnedbyplanning.GetStringValue() + "')";
                    }

                }
                else if (param.Status.ToUpper() == "RELEASE")
                {
                    if ((objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.PLNG1.GetStringValue() || objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.PLNG2.GetStringValue() || objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.PLNG3.GetStringValue()))
                    {
                        whereCondition += " and  tvl010.WEApprovedBy is not null and   tvl010.Status in('" + clsImplementationEnum.TravelerHeaderStatus.ApprovedbyPlanning.GetStringValue() + "' )";
                    }
                    EndIndex = 1000;
                }


                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (tvl010.IdentificationNo like '%" + param.sSearch
                         + "%' or tvl010.TravelerNo like '%" + param.sSearch
                          + "%' or tvl010.Identification like '%" + param.sSearch
                           + "%' or tvl010.TravelerRevisionDetails like '%" + param.sSearch
                            + "%' or tvl010.TravelerDescription like '%" + param.sSearch
                        + "%' or tvl010.TravelerRevNo like '%" + param.sSearch
                        + "%' or tvl010.Status like '%" + param.sSearch
                        + "%')";
                }
                //else
                //{
                //    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                //}
                var lstResult = db.SP_TVL_GET_Traveler_HEADER_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                List<SelectListItem> BoolenList = new List<SelectListItem>() { new SelectListItem { Text = "Yes", Value = "Yes" }, new SelectListItem { Text = "No", Value = "No" } };

                int newRecordId = 0;
                var newRecord = new[] {
                                    "",
                                    Helper.GenerateHidden(newRecordId, "HeaderId", ""),
                                    //GenerateAutoCompleteOnBlur(newRecordId,"txtidentification","","UpdateLTFPSFirstLine(this,"+ newRecordId +")","",false,"","Testidentification")+""+Helper.GenerateHidden(newRecordId,"Testidentification"),
                                     Helper.HTMLAutoComplete(newRecordId,"txtidentification","","",false,"","Testidentification")+""+Helper.GenerateHidden(newRecordId,"Testidentification"),
                                    Helper.GenerateTextbox(newRecordId, "IdentificationNo", "", "", false, "", "50"),
                                    Helper.GenerateTextbox(newRecordId, "TravelerNo", "", "", false, "", "50"),
                                    "R0",
                                    Helper.GenerateTextbox(newRecordId, "TravelerRevisionDetails", "First Revision", "", false, "", "100"),
                                    Helper.GenerateTextbox(newRecordId, "TravelerDescription", "", "", false, "", "300"),
                                    clsImplementationEnum.LTFPSHeaderStatus.Draft.GetStringValue(),
                                    Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveTravellerHeader();")
                                };

                var data = (from uc in lstResult
                            select new[]
                            {
                               Convert.ToString(uc.HeaderId),
                               Helper.GenerateHidden(uc.HeaderId, "HeaderId", Convert.ToString(uc.HeaderId)),                             
                               //((uc.Status == clsImplementationEnum.TravelerHeaderStatus.Draft.GetStringValue() || uc.Status == clsImplementationEnum.TravelerHeaderStatus.Returned.GetStringValue()) && objUserRoleAccessDetails.UserDesignation == clsImplementationEnum.UserAccessRole.Initiator.GetStringValue() ) ? GenerateAutoCompleteOnBlur(uc.HeaderId, "txtidentification", uc.Identification, "UpdateTravellerHeader(this, "+ uc.HeaderId +")","", false, "", "50") : uc.Identification,
                               ((uc.Status == clsImplementationEnum.TravelerHeaderStatus.Draft.GetStringValue() || uc.Status == clsImplementationEnum.TravelerHeaderStatus.ReturnedbyQI.GetStringValue() || uc.Status==clsImplementationEnum.TravelerHeaderStatus.ReturnedbyWE.GetStringValue() || uc.Status==clsImplementationEnum.TravelerHeaderStatus.Returnedbyplanning.GetStringValue()) && uc.TravelerRevNo==0 &&  objUserRoleAccessDetails.UserDesignation == clsImplementationEnum.UserAccessRole.Initiator.GetStringValue() ) ?  Helper.HTMLAutoComplete(uc.HeaderId, "txtidentification", uc.Identification, "UpdateTravellerHeader(this, "+ uc.HeaderId +")", false, "","Testidentification",false, "50") : uc.Identification,
                               ((uc.Status == clsImplementationEnum.TravelerHeaderStatus.Draft.GetStringValue() || uc.Status == clsImplementationEnum.TravelerHeaderStatus.ReturnedbyQI.GetStringValue() || uc.Status==clsImplementationEnum.TravelerHeaderStatus.ReturnedbyWE.GetStringValue() || uc.Status==clsImplementationEnum.TravelerHeaderStatus.Returnedbyplanning.GetStringValue())  && uc.TravelerRevNo==0 && objUserRoleAccessDetails.UserDesignation == clsImplementationEnum.UserAccessRole.Initiator.GetStringValue() && uc.Identification!=clsImplementationEnum.TravelerIdentificationDropdown.StandardTraveler.GetStringValue() ) ? Helper.GenerateTextbox(uc.HeaderId, "IdentificationNo", uc.IdentificationNo, "UpdateTravellerHeader(this, "+ uc.HeaderId +")", false, "", "50") : uc.IdentificationNo,
                                ((uc.Status == clsImplementationEnum.TravelerHeaderStatus.Draft.GetStringValue() || uc.Status == clsImplementationEnum.TravelerHeaderStatus.ReturnedbyQI.GetStringValue() || uc.Status==clsImplementationEnum.TravelerHeaderStatus.ReturnedbyWE.GetStringValue() || uc.Status==clsImplementationEnum.TravelerHeaderStatus.Returnedbyplanning.GetStringValue())  && uc.TravelerRevNo==0 && objUserRoleAccessDetails.UserDesignation == clsImplementationEnum.UserAccessRole.Initiator.GetStringValue() ) ? Helper.GenerateTextbox(uc.HeaderId, "TravelerNo", uc.TravelerNo, "UpdateTravellerHeader(this, "+ uc.HeaderId +")", false, "", "50") : uc.TravelerNo,
                               "R" + Convert.ToString(uc.TravelerRevNo),
                                   ((uc.Status == clsImplementationEnum.TravelerHeaderStatus.Draft.GetStringValue() || uc.Status == clsImplementationEnum.TravelerHeaderStatus.ReturnedbyQI.GetStringValue() || uc.Status==clsImplementationEnum.TravelerHeaderStatus.ReturnedbyWE.GetStringValue() || uc.Status==clsImplementationEnum.TravelerHeaderStatus.Returnedbyplanning.GetStringValue())  && uc.TravelerRevNo==0 && objUserRoleAccessDetails.UserDesignation == clsImplementationEnum.UserAccessRole.Initiator.GetStringValue() ) ? Helper.GenerateTextbox(uc.HeaderId, "TravelerRevisionDetails", uc.TravelerRevisionDetails, "UpdateTravellerHeader(this, "+ uc.HeaderId +")", false, "", "100") : uc.TravelerRevisionDetails,
                                 ((uc.Status == clsImplementationEnum.TravelerHeaderStatus.Draft.GetStringValue() || uc.Status == clsImplementationEnum.TravelerHeaderStatus.ReturnedbyQI.GetStringValue() || uc.Status==clsImplementationEnum.TravelerHeaderStatus.ReturnedbyWE.GetStringValue() || uc.Status==clsImplementationEnum.TravelerHeaderStatus.Returnedbyplanning.GetStringValue())  && uc.TravelerRevNo==0 && objUserRoleAccessDetails.UserDesignation == clsImplementationEnum.UserAccessRole.Initiator.GetStringValue() ) ? Helper.GenerateTextbox(uc.HeaderId, "TravelerDescription", uc.TravelerDescription, "UpdateTravellerHeader(this, "+ uc.HeaderId +")", false, "", "200") : uc.TravelerDescription,
                               Convert.ToString(uc.Status),

                               "<nobr>" + Helper.GenerateActionIcon(uc.HeaderId, "Delete", "Delete Traveler", "fa fa-trash-o", "DeleteTravelerHeaderWithConfirmation("+ uc.HeaderId +")", "",  !(objUserRoleAccessDetails.UserDesignation == clsImplementationEnum.UserAccessRole.Initiator.GetStringValue()  && (uc.Status == clsImplementationEnum.TravelerHeaderStatus.Draft.GetStringValue() || uc.Status == clsImplementationEnum.TravelerHeaderStatus.ReturnedbyQI.GetStringValue() || uc.Status == clsImplementationEnum.TravelerHeaderStatus.ReturnedbyWE.GetStringValue() || uc.Status == clsImplementationEnum.TravelerHeaderStatus.Returnedbyplanning.GetStringValue()))) +
                               "<i title='View Timeline' href='javascript:void(0)' onclick=ShowTimeline('/TVL/MaintainTraveler/ShowTimeline?HeaderID=" + Convert.ToInt32(uc.HeaderId) + "'"+"); class='iconspace fa fa-clock-o'></i>" +
                               //Helper.GenerateActionIcon(uc.HeaderId,"Timeline","Timeline","fa fa-clock-o", "ShowTimeline(\"/TVL/MaintainTraveler/ShowTimeline?HeaderID="+ uc.HeaderId +"\");") +
                               Helper.GenerateActionIcon(uc.HeaderId, "History", "History", "fa fa-history", "HistoryTraveler("+ uc.HeaderId +")", "", uc.TravelerRevNo > 0 || uc.Status == clsImplementationEnum.TravelerHeaderStatus.Released.GetStringValue() ? false : true) +
                               Helper.GenerateActionIcon(uc.HeaderId, "Revise", "Revise", "fa fa-retweet", "ReviseTravelerHeaderWithConfirmation("+ uc.HeaderId +")", "", uc.Status == clsImplementationEnum.LTFPSHeaderStatus.Released.GetStringValue() && objUserRoleAccessDetails.UserDesignation == clsImplementationEnum.UserAccessRole.Initiator.GetStringValue() ? false : true)+
                               Helper.GenerateActionIcon(uc.HeaderId, "Print", "Print Traveler", "fa fa-print", "PrintTVLReport('"+uc.ProjectNo+"','"+uc.TravelerNo+"',"+uc.TravelerRevNo+",'"+uc.IdentificationNo+"',null)","",false)
                              + "</nobr>"

                           }).ToList();


                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SubmiTravelerHeader(string strHeader)
        {
            //clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            ResponceMsgWithMultiValue objResponseMsg = new ResponceMsgWithMultiValue();
            List<TVL010> lstTVL010s = new List<TVL010>();
            try
            {
                if (!string.IsNullOrEmpty(strHeader))
                {
                    List<string> submittedtraveler = new List<string>();
                    List<string> pendingtraveler = new List<string>();
                    int[] headerIds = Array.ConvertAll(strHeader.Split(','), s => int.Parse(s));
                    foreach (int headerId in headerIds)
                    {
                        var tvl010 = db.TVL010.Where(x => x.HeaderId == headerId).FirstOrDefault();
                        var tvl011 = db.TVL011.Where(m => m.HeaderId == headerId).FirstOrDefault();
                        if (tvl011 != null)
                        {
                            if (tvl010.Status == clsImplementationEnum.TravelerHeaderStatus.Draft.GetStringValue() || tvl010.Status == clsImplementationEnum.TravelerHeaderStatus.ReturnedbyQI.GetStringValue() || tvl010.Status == clsImplementationEnum.TravelerHeaderStatus.ReturnedbyWE.GetStringValue() || tvl010.Status == clsImplementationEnum.TravelerHeaderStatus.Returnedbyplanning.GetStringValue())
                            {
                                if (Validtetraveler(headerId))
                                {
                                    var result = SendForApproval(headerId);
                                    if (result)
                                        submittedtraveler.Add(tvl010.TravelerNo);
                                    else
                                        pendingtraveler.Add(tvl010.TravelerNo);

                                    if (submittedtraveler.Count > 0)
                                    {
                                        objResponseMsg.submittedTraveler = "Traveler " + String.Join(",", submittedtraveler) + " submitted successfully";

                                    }


                                    objResponseMsg.Key = true;
                                    objResponseMsg.Value = "Traveler(s) has been sucessfully submitted for approval";
                                }
                                else
                                {
                                    objResponseMsg.Key = false;
                                    objResponseMsg.Value = "Please enter all mendatory field of operation line for Traveler no:-" + tvl010.TravelerNo;
                                }
                            }
                            else
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = "Only Draft and Return status can be submitted for approval";
                            }
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "Please Enter Atleast OneLine in Operation Grid";
                        }
                    }

                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please select atleast one record sending for approval";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        private bool SendForApproval(int headerId)
        {
            TVL010 objtvl010 = db.TVL010.FirstOrDefault(i => i.HeaderId == headerId);
            if (objtvl010 != null)
            {

                objtvl010.Status = clsImplementationEnum.TravelerHeaderStatus.SubmittedToQI.GetStringValue();
                objtvl010.PlanningSubmittedBy = objClsLoginInfo.UserName;
                objtvl010.PlanningSubmittedOn = DateTime.Now;

                db.SaveChanges();

                var objtvl011 = db.TVL011.Where(m => m.HeaderId == headerId).ToList();
                foreach (var tvl11 in objtvl011)
                {
                    TVL011 objtvl11 = db.TVL011.Where(m => m.LineId == tvl11.LineId).FirstOrDefault();
                    objtvl11.ReturnedBy = null;
                    objtvl11.ReturnedOn = null;
                    objtvl11.ReturnRole = null;
                    objtvl11.ReturnRemarks = null;
                    db.SaveChanges();

                }

                #region Send Notification
                (new clsManager()).SendNotificationBULocationWise((clsImplementationEnum.UserRoleName.QI1.GetStringValue() + ',' + clsImplementationEnum.UserRoleName.QI2.GetStringValue() + ',' + clsImplementationEnum.UserRoleName.QI3.GetStringValue()), objtvl010.BU, objtvl010.Location, "Traveler: " + objtvl010.TravelerNo + " has been submitted for Review", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/TVL/MaintainTraveler/Details/" + objtvl010.HeaderId);
                #endregion

                return true;

            }
            return false;
        }
        public ActionResult ReturnTravelerHeader(string strHeader, string strReturnRemarks, string department)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            List<TVL010> lstTVL010s = new List<TVL010>();
            try
            {
                if (!string.IsNullOrEmpty(strHeader))
                {
                    int[] headerIds = Array.ConvertAll(strHeader.Split(','), s => int.Parse(s));
                    string[] returnRemarks = strReturnRemarks.Split(',');

                    for (int i = 0; i < headerIds.Length; i++)
                    {
                        int headerid = Convert.ToInt32(headerIds[i]);
                        TVL010 objtvl010 = db.TVL010.Where(m => m.HeaderId == headerid).FirstOrDefault();
                        if (objtvl010.Status != clsImplementationEnum.TravelerHeaderStatus.Draft.GetStringValue() && objtvl010.Status != clsImplementationEnum.TravelerHeaderStatus.ReturnedbyQI.GetStringValue() && objtvl010.Status != clsImplementationEnum.TravelerHeaderStatus.ReturnedbyWE.GetStringValue() && objtvl010.Status != clsImplementationEnum.TravelerHeaderStatus.Returnedbyplanning.GetStringValue())
                        {
                            if (department == clsImplementationEnum.UserRoleName.QI1.GetStringValue() || department == clsImplementationEnum.UserRoleName.QI2.GetStringValue() || department == clsImplementationEnum.UserRoleName.QI3.GetStringValue())
                                ReturnByQI(headerIds[i], returnRemarks[i]);
                            else if (department == clsImplementationEnum.UserRoleName.WE1.GetStringValue() || department == clsImplementationEnum.UserRoleName.WE2.GetStringValue() || department == clsImplementationEnum.UserRoleName.WE3.GetStringValue())
                                ReturnByWE(headerIds[i], returnRemarks[i]);
                            else if (department == clsImplementationEnum.UserRoleName.PLNG1.GetStringValue() || department == clsImplementationEnum.UserRoleName.PLNG2.GetStringValue())
                                ReturnByPlanning(headerIds[i], returnRemarks[i]);
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = "Traveler(s) has been sucessfully returned";
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "Firstly Please Select Travelerno:-" + objtvl010.TravelerNo + " For Send For Approval";
                        }

                    }

                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please select atleast one record for return";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult ApproveTravelerHeader(string strHeader, string department)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            List<TVL010> lstTVL010s = new List<TVL010>();
            try
            {
                if (!string.IsNullOrEmpty(strHeader))
                {
                    int[] headerIds = Array.ConvertAll(strHeader.Split(','), s => int.Parse(s));
                    foreach (int headerId in headerIds)
                    {
                        TVL010 objtvl010 = db.TVL010.Where(m => m.HeaderId == headerId).FirstOrDefault();
                        if (objtvl010.Status == clsImplementationEnum.TravelerHeaderStatus.SubmittedToQI.GetStringValue() || objtvl010.Status == clsImplementationEnum.TravelerHeaderStatus.SubmittedToWE.GetStringValue() || objtvl010.Status == clsImplementationEnum.TravelerHeaderStatus.SubmittedToPLANNING.GetStringValue())
                        {
                            if (department == clsImplementationEnum.UserRoleName.QI1.GetStringValue() || department == clsImplementationEnum.UserRoleName.QI2.GetStringValue() || department == clsImplementationEnum.UserRoleName.QI3.GetStringValue())
                                ApproveByQI(headerId);
                            else if (department == clsImplementationEnum.UserRoleName.WE1.GetStringValue() || department == clsImplementationEnum.UserRoleName.WE2.GetStringValue() || department == clsImplementationEnum.UserRoleName.WE3.GetStringValue())
                                ApproveByWE(headerId);
                            else if (department == clsImplementationEnum.UserRoleName.PLNG1.GetStringValue() || department == clsImplementationEnum.UserRoleName.PLNG2.GetStringValue())
                            {
                                ApproveByPlanning(headerId);
                            }
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = "Traveler(s) has been sucessfully approved";
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "Firstly Please Select Travelerno:-" + objtvl010.TravelerNo + " For Send For Approval";
                        }
                        //ApproveByCustomer(headerId);
                    }

                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please select atleast one record for approve";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        private bool ApproveByWE(int headerId)
        {
            TVL010 objTVL010 = db.TVL010.FirstOrDefault(i => i.HeaderId == headerId);
            if (objTVL010 != null)
            {
                objTVL010.Status = clsImplementationEnum.TravelerHeaderStatus.SubmittedToPLANNING.GetStringValue();
                objTVL010.WEApprovedBy = objClsLoginInfo.UserName;
                objTVL010.WEApprovedOn = DateTime.Now;
                db.SaveChanges();

                #region Send Notification
                (new clsManager()).SendNotificationBULocationWise((clsImplementationEnum.UserRoleName.PLNG2.GetStringValue() + ',' + clsImplementationEnum.UserRoleName.PLNG1.GetStringValue()), objTVL010.BU, objTVL010.Location, "Traveler: " + objTVL010.TravelerNo + " has been submitted for Approval", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/TVL/MaintainTraveler/Details/" + objTVL010.HeaderId);
                #endregion


                return true;
            }
            return false;
        }
        private bool ReturnByWE(int headerId, string returnRemark)
        {
            TVL010 objTVL010 = db.TVL010.FirstOrDefault(i => i.HeaderId == headerId);
            if (objTVL010 != null)
            {
                objTVL010.Status = clsImplementationEnum.TravelerHeaderStatus.ReturnedbyWE.GetStringValue();
                objTVL010.WERemarks = returnRemark;
                objTVL010.WEReturnedBy = objClsLoginInfo.UserName;
                objTVL010.WEReturnedOn = DateTime.Now;
                db.SaveChanges();
                UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();
                var objtvl011 = db.TVL011.Where(m => m.HeaderId == headerId && m.ReturnRemarks != null).ToList();
                if (objtvl011.Count > 0)
                {
                    TVL012 objtvl012 = new TVL012();
                    foreach (var tvldata in objtvl011)
                    {
                        TVL011 tvl11 = db.TVL011.Where(m => m.LineId == tvldata.LineId).FirstOrDefault();
                        tvl11.ReturnedBy = objClsLoginInfo.UserName;
                        tvl11.ReturnedOn = DateTime.Now;
                        tvl11.ReturnRole = objUserRoleAccessDetails.UserRole;
                        db.SaveChanges();

                        objtvl012.HeaderId = tvldata.HeaderId;
                        objtvl012.OperationNo = tvldata.OperationNo;
                        objtvl012.OperationRevisionNo = tvldata.OperationRevisionNo;
                        objtvl012.ReturnRemarks = tvldata.ReturnRemarks;
                        objtvl012.Role = objUserRoleAccessDetails.UserRole;
                        objtvl012.CreatedBy = objClsLoginInfo.UserName;
                        objtvl012.CreatedOn = DateTime.Now;
                        objtvl012.ReturnedBy = objClsLoginInfo.UserName;
                        objtvl012.ReturnedOn = DateTime.Now;
                        db.TVL012.Add(objtvl012);
                        db.SaveChanges();
                    }
                }
                #region Send Notification
                (new clsManager()).SendNotificationBULocationWise(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue(), objTVL010.BU, objTVL010.Location, "Traveler: " + objTVL010.TravelerNo + " has been Returned", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/TVL/MaintainTraveler/Details/" + objTVL010.HeaderId, objTVL010.PlanningSubmittedBy);
                #endregion


                return true;
            }
            return false;
        }
        private bool ApproveByQI(int headerId)
        {
            TVL010 objTVL010 = db.TVL010.FirstOrDefault(i => i.HeaderId == headerId);
            if (objTVL010 != null)
            {
                objTVL010.Status = clsImplementationEnum.TravelerHeaderStatus.SubmittedToWE.GetStringValue();
                objTVL010.QIApprovedBy = objClsLoginInfo.UserName;
                objTVL010.QIApprovedOn = DateTime.Now;
                db.SaveChanges();


                #region Send Notification
                (new clsManager()).SendNotificationBULocationWise((clsImplementationEnum.UserRoleName.WE1.GetStringValue() + ',' + clsImplementationEnum.UserRoleName.WE2.GetStringValue() + ',' + clsImplementationEnum.UserRoleName.WE3.GetStringValue()), objTVL010.BU, objTVL010.Location, "Traveler: " + objTVL010.TravelerNo + " has been submitted for Review", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/TVL/MaintainTraveler/Details/" + objTVL010.HeaderId);
                #endregion


                return true;
            }
            return false;
        }
        private bool ReturnByQI(int headerId, string returnRemark)
        {
            TVL010 objTVL010 = db.TVL010.FirstOrDefault(i => i.HeaderId == headerId);
            if (objTVL010 != null)
            {
                objTVL010.Status = clsImplementationEnum.TravelerHeaderStatus.ReturnedbyQI.GetStringValue();
                objTVL010.QIRemarks = returnRemark;
                objTVL010.QIReturnedBy = objClsLoginInfo.UserName;
                objTVL010.QIReturnedOn = DateTime.Now;
                db.SaveChanges();
                UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();
                var objtvl011 = db.TVL011.Where(m => m.HeaderId == headerId && m.ReturnRemarks != null).ToList();
                if (objtvl011.Count > 0)
                {
                    TVL012 objtvl012 = new TVL012();
                    foreach (var tvldata in objtvl011)
                    {
                        TVL011 tvl11 = db.TVL011.Where(m => m.LineId == tvldata.LineId).FirstOrDefault();
                        tvl11.ReturnedBy = objClsLoginInfo.UserName;
                        tvl11.ReturnedOn = DateTime.Now;
                        tvl11.ReturnRole = objUserRoleAccessDetails.UserRole;
                        db.SaveChanges();

                        objtvl012.HeaderId = tvldata.HeaderId;
                        objtvl012.OperationNo = tvldata.OperationNo;
                        objtvl012.OperationRevisionNo = tvldata.OperationRevisionNo;
                        objtvl012.ReturnRemarks = tvldata.ReturnRemarks;
                        objtvl012.Role = objUserRoleAccessDetails.UserRole;
                        objtvl012.CreatedBy = objClsLoginInfo.UserName;
                        objtvl012.CreatedOn = DateTime.Now;
                        objtvl012.ReturnedBy = objClsLoginInfo.UserName;
                        objtvl012.ReturnedOn = DateTime.Now;
                        db.TVL012.Add(objtvl012);
                        db.SaveChanges();
                    }
                }


                #region Send Notification
                (new clsManager()).SendNotificationBULocationWise(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue(), objTVL010.BU, objTVL010.Location, "Traveler: " + objTVL010.TravelerNo + " has been Returned", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/TVL/MaintainTraveler/Details/" + objTVL010.HeaderId, objTVL010.PlanningSubmittedBy);
                #endregion

                return true;
            }
            return false;
        }
        private bool ApproveByPlanning(int headerId)
        {
            TVL010 objTVL010 = db.TVL010.FirstOrDefault(i => i.HeaderId == headerId);
            if (objTVL010 != null)
            {
                objTVL010.Status = clsImplementationEnum.TravelerHeaderStatus.ApprovedbyPlanning.GetStringValue();
                objTVL010.PlanningApprovedBy = objClsLoginInfo.UserName;
                objTVL010.PlanningApprovedOn = DateTime.Now;
                db.SaveChanges();

                #region Send Notification
                (new clsManager()).SendNotificationBULocationWise((clsImplementationEnum.UserRoleName.PLNG1.GetStringValue() + ',' + clsImplementationEnum.UserRoleName.PLNG2.GetStringValue() + ',' + clsImplementationEnum.UserRoleName.PLNG3.GetStringValue()), objTVL010.BU, objTVL010.Location, "Traveler: " + objTVL010.TravelerNo + " has been submitted for Release", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/TVL/MaintainTraveler/Details/" + objTVL010.HeaderId);
                #endregion
                return true;
            }
            return false;
        }

        private bool ReturnByPlanning(int headerId, string returnRemark)
        {
            TVL010 objTVL010 = db.TVL010.FirstOrDefault(i => i.HeaderId == headerId);
            if (objTVL010 != null)
            {
                objTVL010.Status = clsImplementationEnum.TravelerHeaderStatus.Returnedbyplanning.GetStringValue();
                objTVL010.PlanningRemarks = returnRemark;
                objTVL010.PlanningReturnedBy = objClsLoginInfo.UserName;
                objTVL010.PlanningReturnedOn = DateTime.Now;
                db.SaveChanges();

                UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();
                var objtvl011 = db.TVL011.Where(m => m.HeaderId == headerId && m.ReturnRemarks != null).ToList();
                if (objtvl011.Count > 0)
                {
                    TVL012 objtvl012 = new TVL012();
                    foreach (var tvldata in objtvl011)
                    {
                        TVL011 tvl11 = db.TVL011.Where(m => m.LineId == tvldata.LineId).FirstOrDefault();
                        tvl11.ReturnedBy = objClsLoginInfo.UserName;
                        tvl11.ReturnedOn = DateTime.Now;
                        tvl11.ReturnRole = objUserRoleAccessDetails.UserRole;
                        db.SaveChanges();

                        objtvl012.HeaderId = tvldata.HeaderId;
                        objtvl012.OperationNo = tvldata.OperationNo;
                        objtvl012.OperationRevisionNo = tvldata.OperationRevisionNo;
                        objtvl012.ReturnRemarks = tvldata.ReturnRemarks;
                        objtvl012.Role = objUserRoleAccessDetails.UserRole;
                        objtvl012.CreatedBy = objClsLoginInfo.UserName;
                        objtvl012.CreatedOn = DateTime.Now;
                        objtvl012.ReturnedBy = objClsLoginInfo.UserName;
                        objtvl012.ReturnedOn = DateTime.Now;
                        db.TVL012.Add(objtvl012);
                        db.SaveChanges();
                    }
                }

                #region Send Notification
                (new clsManager()).SendNotificationBULocationWise(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue(), objTVL010.BU, objTVL010.Location, "Traveler: " + objTVL010.TravelerNo + " has been Returned", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/TVL/MaintainTraveler/Details/" + objTVL010.HeaderId, objTVL010.PlanningSubmittedBy);
                #endregion


                return true;
            }
            return false;
        }


        [HttpPost]
        public ActionResult ReleaseTravelerHeader(string strHeader)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            List<TVL010> lstTVL010s = new List<TVL010>();
            try
            {
                if (!string.IsNullOrEmpty(strHeader))
                {
                    int[] headerIds = Array.ConvertAll(strHeader.Split(','), s => int.Parse(s));
                    string ProjectNumber = string.Empty;
                    string Location = string.Empty;
                    foreach (int headerId in headerIds)
                    {
                        TVL010 tvl010 = db.TVL010.Where(m => m.HeaderId == headerId).FirstOrDefault();
                        if (tvl010.Status == clsImplementationEnum.TravelerHeaderStatus.ApprovedbyPlanning.GetStringValue())
                        {
                            ReleaseByPLNG(headerId);
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = "Traveler(s) has been sucessfully released";
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "Firstly Please Select Travelerno:-" + tvl010.TravelerNo + " For Approval";
                        }

                        if (string.IsNullOrWhiteSpace(ProjectNumber))
                        {
                            ProjectNumber = tvl010.ProjectNo;
                            Location = tvl010.Location;
                        }
                    }

                    int Maxissue = 1;
                    TVL015 objtvl015 = db.TVL015.Where(m => m.ProjectNo == ProjectNumber && m.Location == Location).FirstOrDefault();
                    if (objtvl015 != null)
                    {
                        Maxissue = Convert.ToInt32(db.TVL016.Where(m => m.HeaderId == objtvl015.HeaderId).Max(m => m.IssueNo));
                        Maxissue = Maxissue + 1;
                    }
                    foreach (int headerId in headerIds)
                    {
                        GenerateDIN(headerId, Maxissue);
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please select atleast one record for release";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        private bool ReleaseByPLNG(int headerId)
        {
            TVL010 objTVL010 = db.TVL010.FirstOrDefault(i => i.HeaderId == headerId);
            if (objTVL010 != null)
            {
                objTVL010.Status = clsImplementationEnum.TravelerHeaderStatus.Released.GetStringValue();
                objTVL010.PlanningReleasedBy = objClsLoginInfo.UserName;
                objTVL010.PlanningReleasedOn = DateTime.Now;
                db.SaveChanges();

                #region Insertupdate operation

                InsertUpdateOperation(headerId);

                #endregion

                #region Maintain Log Details
                var lstTVL010_Log = db.TVL010_Log.Where(x => x.HeaderId == headerId).ToList();
                if (lstTVL010_Log.Count() > 0)
                {
                    lstTVL010_Log.ForEach(i => { i.Status = clsImplementationEnum.PlanStatus.Superseded.GetStringValue(); });
                }
                TVL010_Log objTVL010_Log = new TVL010_Log();
                objTVL010_Log.HeaderId = objTVL010.HeaderId;
                objTVL010_Log.ProjectNo = objTVL010.ProjectNo;
                objTVL010_Log.ProjectDescription = objTVL010.ProjectDescription;
                objTVL010_Log.Customer = objTVL010.Customer;
                objTVL010_Log.Identification = objTVL010.Identification;
                objTVL010_Log.IdentificationNo = objTVL010.IdentificationNo;
                objTVL010_Log.TravelerNo = objTVL010.TravelerNo;
                objTVL010_Log.TravelerRevNo = objTVL010.TravelerRevNo;
                objTVL010_Log.TravelerRevisionDetails = objTVL010.TravelerRevisionDetails;
                objTVL010_Log.TravelerDescription = objTVL010.TravelerDescription;
                objTVL010_Log.BU = objTVL010.BU;
                objTVL010_Log.Location = objTVL010.Location;
                objTVL010_Log.Status = objTVL010.Status;
                objTVL010_Log.InitiatorRemarks = objTVL010.InitiatorRemarks;
                objTVL010_Log.PlanningSubmittedBy = objTVL010.PlanningSubmittedBy;
                objTVL010_Log.PlanningSubmittedOn = objTVL010.PlanningSubmittedOn;
                objTVL010_Log.QIApprovedBy = objTVL010.QIApprovedBy;
                objTVL010_Log.QIApprovedOn = objTVL010.QIApprovedOn;
                objTVL010_Log.QIReturnedBy = objTVL010.QIReturnedBy;
                objTVL010_Log.QIReturnedOn = objTVL010.QIReturnedOn;
                objTVL010_Log.QIRemarks = objTVL010.QIRemarks;
                objTVL010_Log.WEApprovedBy = objTVL010.WEApprovedBy;
                objTVL010_Log.WEApprovedOn = objTVL010.WEApprovedOn;
                objTVL010_Log.WEReturnedBy = objTVL010.WEReturnedBy;
                objTVL010_Log.WEReturnedOn = objTVL010.WEReturnedOn;
                objTVL010_Log.WERemarks = objTVL010.WERemarks;
                objTVL010_Log.PlanningApprovedBy = objTVL010.PlanningApprovedBy;
                objTVL010_Log.PlanningApprovedOn = objTVL010.PlanningApprovedOn;
                objTVL010_Log.PlanningReturnedBy = objTVL010.PlanningReturnedBy;
                objTVL010_Log.PlanningReturnedOn = objTVL010.PlanningReturnedOn;
                objTVL010_Log.PlanningRemarks = objTVL010.PlanningRemarks;
                objTVL010_Log.PlanningReleasedBy = objTVL010.PlanningReleasedBy;
                objTVL010_Log.PlanningReleasedOn = objTVL010.PlanningReleasedOn;
                objTVL010_Log.Note1 = objTVL010.Note1;
                objTVL010_Log.Note2 = objTVL010.Note2;
                objTVL010_Log.Note3 = objTVL010.Note3;
                objTVL010_Log.Note4 = objTVL010.Note4;
                objTVL010_Log.Note5 = objTVL010.Note5;
                objTVL010_Log.Note6 = objTVL010.Note6;
                objTVL010_Log.Note7 = objTVL010.Note7;
                objTVL010_Log.Note8 = objTVL010.Note8;
                objTVL010_Log.Note9 = objTVL010.Note9;
                objTVL010_Log.Note10 = objTVL010.Note10;
                objTVL010_Log.Note11 = objTVL010.Note11;
                objTVL010_Log.Note12 = objTVL010.Note12;
                objTVL010_Log.Note13 = objTVL010.Note13;
                objTVL010_Log.Note14 = objTVL010.Note14;
                objTVL010_Log.Note15 = objTVL010.Note15;
                objTVL010_Log.CreatedBy = objTVL010.CreatedBy;
                objTVL010_Log.CreatedOn = objTVL010.CreatedOn;
                objTVL010_Log.EditedBy = objTVL010.EditedBy;
                objTVL010_Log.EditedOn = objTVL010.EditedOn;
                db.TVL010_Log.Add(objTVL010_Log);
                db.SaveChanges();

                var lstTVL011_Log = db.TVL011_Log.Where(x => x.HeaderId == headerId).ToList();

                if (lstTVL011_Log.Count > 0)
                {
                    lstTVL011_Log.ForEach(i => { i.LineStatus = clsImplementationEnum.TravelerLineStatus.Superseded.GetStringValue(); });
                }
                List<TVL011> deletedLines = new List<TVL011>();
                foreach (TVL011 objtvl011 in objTVL010.TVL011)
                {
                    if (objtvl011.LineStatus == clsImplementationEnum.TravelerLineStatus.Deleted.GetStringValue())
                    {
                        deletedLines.Add(objtvl011);
                        continue;
                    }

                    TVL011_Log objTVL011_Log = new TVL011_Log();
                    objTVL011_Log.RefId = objTVL010_Log.Id;
                    objTVL011_Log.LineId = objtvl011.LineId;
                    objTVL011_Log.HeaderId = objtvl011.HeaderId;
                    objTVL011_Log.OperationNo = objtvl011.OperationNo;
                    objTVL011_Log.OperationRevisionNo = objtvl011.OperationRevisionNo;
                    objTVL011_Log.Activity = objtvl011.Activity;
                    objTVL011_Log.RefDocument = objtvl011.RefDocument;
                    objTVL011_Log.RefDocRevNo = objtvl011.RefDocRevNo;
                    objTVL011_Log.LTIntervention = objtvl011.LTIntervention;
                    objTVL011_Log.I1Intervention = objtvl011.I1Intervention;
                    objTVL011_Log.I2Intervention = objtvl011.I2Intervention;
                    objTVL011_Log.I3Intervention = objtvl011.I3Intervention;
                    objTVL011_Log.I4Intervention = objtvl011.I4Intervention;
                    objTVL011_Log.InspectionRecordRequired = objtvl011.InspectionRecordRequired;
                    objTVL011_Log.CreatedBy = objtvl011.CreatedBy;
                    objTVL011_Log.CreatedOn = objtvl011.CreatedOn;
                    objTVL011_Log.EditedBy = objtvl011.EditedBy;
                    objTVL011_Log.EditedOn = objtvl011.EditedOn;
                    objTVL011_Log.LineStatus = clsImplementationEnum.TravelerLineStatus.Released.GetStringValue();
                    objTVL011_Log.ReturnRemarks = objtvl011.ReturnRemarks;
                    objTVL011_Log.ReturnRole = objtvl011.ReturnRole;
                    objTVL011_Log.ReturnedBy = objtvl011.ReturnedBy;
                    objTVL011_Log.ReturnedOn = objtvl011.ReturnedOn;
                    db.TVL011_Log.Add(objTVL011_Log);

                }
                db.TVL011.RemoveRange(deletedLines);
                db.SaveChanges();

                var objTVL012 = db.TVL012.Where(i => i.HeaderId == headerId).ToList();
                if (objTVL012.Count > 0)
                {
                    TVL012_Log objtvl12 = new TVL012_Log();
                    foreach (var item in objTVL012)
                    {
                        objtvl12.RefId = objTVL010_Log.Id;
                        objtvl12.LineId = item.LineId;
                        objtvl12.HeaderId = item.HeaderId;
                        objtvl12.OperationNo = item.OperationNo;
                        objtvl12.OperationRevisionNo = item.OperationRevisionNo;
                        objtvl12.ReturnRemarks = item.ReturnRemarks;
                        objtvl12.Role = item.Role;
                        objtvl12.CreatedBy = objClsLoginInfo.UserName;
                        objtvl12.CreatedOn = DateTime.Now;
                        objtvl12.ReturnedBy = item.ReturnedBy;
                        objtvl12.ReturnedOn = item.ReturnedOn;
                        db.TVL012_Log.Add(objtvl12);
                        db.SaveChanges();

                    }
                }

                #endregion

                objTVL010.TVL011.ToList().ForEach(c => { c.LineStatus = clsImplementationEnum.TravelerLineStatus.Released.GetStringValue(); });
                db.SaveChanges();

                #region Send Notification
                (new clsManager()).SendNotificationBULocationWise(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue(), objTVL010.BU, objTVL010.Location, "Traveler: " + objTVL010.TravelerNo + " has been Released", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/TVL/MaintainTraveler/Details/" + objTVL010.HeaderId, objTVL010.PlanningSubmittedBy);
                #endregion

                return true;
            }
            return false;
        }

        private void GenerateDIN(int headerId, int IssueNo)
        {
            #region Generate Din NO
            TVL010 objTVL010 = db.TVL010.FirstOrDefault(c => c.HeaderId == headerId);

            UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();
            TVL015 objtvl015 = db.TVL015.Where(m => m.ProjectNo == objTVL010.ProjectNo && m.Location == objTVL010.Location).FirstOrDefault();
            if (objtvl015 == null)
            {
                TVL015 objtvl15 = new TVL015();
                objtvl15.ProjectNo = objTVL010.ProjectNo;
                objtvl15.ProjectDescription = objTVL010.ProjectDescription;
                objtvl15.Customer = objTVL010.Customer;
                objtvl15.Location = objTVL010.Location;
                objtvl15.DINNo = objTVL010.ProjectNo + '-' + objTVL010.Location + '-' + "Traveler";
                objtvl15.CreatedBy = objClsLoginInfo.UserName;
                objtvl15.CreatedOn = DateTime.Now;
                db.TVL015.Add(objtvl15);
                db.SaveChanges();
                var Dinheaderid = objtvl15.HeaderId;
                TVL016 objtvl016 = new TVL016();
                objtvl016.HeaderId = Dinheaderid;
                objtvl016.TravelerNo = objTVL010.TravelerNo;
                objtvl016.TravelerDescription = objTVL010.TravelerDescription;
                objtvl016.TravelerRevNo = objTVL010.TravelerRevNo;
                objtvl016.Role = objUserRoleAccessDetails.UserRole;
                objtvl016.ReleasedBy = objClsLoginInfo.UserName;
                objtvl016.ReleasedOn = DateTime.Now;
                objtvl016.CreatedBy = objClsLoginInfo.UserName;
                objtvl016.CreatedOn = DateTime.Now;
                objtvl016.IssueNo = IssueNo;
                db.TVL016.Add(objtvl016);
                db.SaveChanges();
            }
            else
            {
                //int Maxissue = Convert.ToInt32(db.TVL016.Where(m => m.HeaderId == objtvl015.HeaderId).Max(m => m.IssueNo));
                TVL016 objtvl16 = new TVL016();
                objtvl16.HeaderId = objtvl015.HeaderId;
                objtvl16.TravelerNo = objTVL010.TravelerNo;
                objtvl16.TravelerDescription = objTVL010.TravelerDescription;
                objtvl16.TravelerRevNo = objTVL010.TravelerRevNo;
                objtvl16.Role = objUserRoleAccessDetails.UserRole;
                objtvl16.ReleasedBy = objClsLoginInfo.UserName;
                objtvl16.ReleasedOn = DateTime.Now;
                objtvl16.CreatedBy = objClsLoginInfo.UserName;
                objtvl16.CreatedOn = DateTime.Now;
                objtvl16.IssueNo = IssueNo;
                db.TVL016.Add(objtvl16);
                db.SaveChanges();


            }
            #endregion
        }

        [HttpPost]
        public ActionResult ReviseTravelerHeader(string headerId, string strremarks)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            int intheaderId = 0;
            try
            {
                if (!string.IsNullOrEmpty(headerId))
                {
                    intheaderId = Convert.ToInt32(headerId);
                    TVL010 objTVL010 = db.TVL010.FirstOrDefault(c => c.HeaderId == intheaderId);

                    if (objTVL010 != null)
                    {
                        objTVL010.Status = clsImplementationEnum.LTFPSHeaderStatus.Draft.GetStringValue();
                        objTVL010.TravelerRevisionDetails = strremarks;
                        objTVL010.TravelerRevNo = objTVL010.TravelerRevNo + 1;
                        objTVL010.CreatedBy = objClsLoginInfo.UserName;
                        objTVL010.CreatedOn = DateTime.Now;

                        objTVL010.QIApprovedBy = null;
                        objTVL010.QIApprovedOn = null;
                        objTVL010.QIReturnedBy = null;
                        objTVL010.QIReturnedOn = null;
                        objTVL010.WEApprovedBy = null;
                        objTVL010.WEApprovedOn = null;
                        objTVL010.WEReturnedBy = null;
                        objTVL010.WEReturnedOn = null;
                        objTVL010.PlanningApprovedBy = null;
                        objTVL010.PlanningApprovedOn = null;
                        objTVL010.PlanningReturnedBy = null;
                        objTVL010.PlanningReturnedOn = null;
                        objTVL010.PlanningReleasedBy = null;
                        objTVL010.PlanningReleasedOn = null;
                        objTVL010.QIRemarks = null;
                        objTVL010.WERemarks = null;
                        objTVL010.PlanningRemarks = null;


                        var objtvl011 = db.TVL011.Where(m => m.HeaderId == objTVL010.HeaderId).ToList();
                        foreach (var tvl11 in objtvl011)
                        {
                            TVL011 objtvl11 = db.TVL011.Where(m => m.LineId == tvl11.LineId).FirstOrDefault();
                            objtvl11.ReturnedBy = null;
                            objtvl11.ReturnedOn = null;
                            objtvl11.ReturnRole = null;
                            objtvl11.ReturnRemarks = null;
                            db.SaveChanges();
                            #region Copy Reference document
                            if (!string.IsNullOrEmpty(objtvl11.RefDocument))
                            {
                                string referenceDoc = "TVL011//" + objtvl11.LineId + "//R" + (objtvl11.OperationRevisionNo - 1);
                                string newfolderPath = "TVL011//" + tvl11.LineId + "//R" + tvl11.OperationRevisionNo;
                                //(new clsFileUpload()).CopyFolderContentsAsync(referenceDoc, newfolderPath);
                                Utility.Controllers.FileUploadController _objFUC = new Utility.Controllers.FileUploadController();
                                _objFUC.CopyDataOnFCSServerAsync(referenceDoc, newfolderPath, referenceDoc, objtvl11.LineId, newfolderPath, tvl11.LineId, DESServices.CommonService.GetUseIPConfig, DESServices.CommonService.objClsLoginInfo.UserName, 0, false);
                            }
                            #endregion
                        }

                        //objTVL010.TVL011.ToList().ForEach(x =>
                        //{
                        //    x.OperationRevisionNo = objTVL010.TravelerRevNo.Value;
                        //});
                        //db.SaveChanges();
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Traveler(s) Revised successfully";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }


            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion



        #region  Data Grid (Data Table) – Inline Editing and Save Record for Header Grid
        [HttpPost]
        public ActionResult SaveTravellerHeader(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            TVL010 objTVL010 = new TVL010();
            int newRowIndex = 0;
            try
            {
                int Headerid = Convert.ToInt32(fc["HeaderId"]);
                string Project = Convert.ToString(fc["ProjectNo"]);
                string Identification = !string.IsNullOrEmpty(fc["Testidentification" + newRowIndex]) ? fc["Testidentification" + newRowIndex] : string.Empty;
                string Identificationno = !string.IsNullOrEmpty(fc["IdentificationNo" + newRowIndex]) ? fc["IdentificationNo" + newRowIndex] : string.Empty;
                string Travelerno = !string.IsNullOrEmpty(fc["TravelerNo" + newRowIndex]) ? fc["TravelerNo" + newRowIndex] : string.Empty;
                if (fc != null)
                {
                    #region Add New traveler Header

                    if (isTRAVELERSHeaderExist(Project, Identification, Identificationno, Travelerno))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Traveler " + Travelerno + " for Projectno" + Project + " is already exist";
                    }
                    else
                    {

                        objTVL010.ProjectNo = Convert.ToString(fc["ProjectNo"]);
                        objTVL010.ProjectDescription = Convert.ToString(fc["ProjectDescription"]);
                        objTVL010.Customer = Convert.ToString(fc["Customer"]);
                        objTVL010.BU = Convert.ToString(fc["BU"]).Split('-')[0].Trim();
                        objTVL010.Location = Convert.ToString(fc["Location"]).Split('-')[0].Trim();
                        objTVL010.Identification = !string.IsNullOrEmpty(fc["Testidentification" + newRowIndex]) ? fc["Testidentification" + newRowIndex] : string.Empty;
                        objTVL010.IdentificationNo = !string.IsNullOrEmpty(fc["IdentificationNo" + newRowIndex]) ? fc["IdentificationNo" + newRowIndex] : string.Empty;
                        objTVL010.TravelerNo = !string.IsNullOrEmpty(fc["TravelerNo" + newRowIndex]) ? fc["TravelerNo" + newRowIndex] : string.Empty;
                        objTVL010.TravelerRevNo = 0;
                        objTVL010.TravelerRevisionDetails = !string.IsNullOrEmpty(fc["TravelerRevisionDetails" + newRowIndex]) ? fc["TravelerRevisionDetails" + newRowIndex] : string.Empty;
                        objTVL010.TravelerDescription = !string.IsNullOrEmpty(fc["TravelerDescription" + newRowIndex]) ? fc["TravelerDescription" + newRowIndex] : string.Empty;
                        objTVL010.Status = clsImplementationEnum.LTFPSHeaderStatus.Draft.GetStringValue();
                        objTVL010.CreatedBy = objClsLoginInfo.UserName;
                        objTVL010.CreatedOn = DateTime.Now;
                        db.TVL010.Add(objTVL010);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Traveler Added successfully";
                    }
                    objResponseMsg.HeaderId = objTVL010.HeaderId;
                    //objResponseMsg.HeaderStatus = objLTF001.Status;
                    // objResponseMsg.RevNo = Convert.ToString(objLTF001.RevNo);
                    #endregion
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult UpdateTravellerHeader(int headerId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            TVL010 objtvl010 = new TVL010();
            try
            {

                if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
                {
                    db.SP_TVL_UPDATE_HEADER_COLUMN(headerId, columnName, columnValue);
                }

                if (headerId > 0)
                {
                    objtvl010 = db.TVL010.Where(i => i.HeaderId == headerId).FirstOrDefault();
                    if (objtvl010.Identification == clsImplementationEnum.TravelerIdentificationDropdown.StandardTraveler.GetStringValue())
                    {
                        objtvl010.IdentificationNo = "";
                    }
                    objtvl010.EditedBy = objClsLoginInfo.UserName;
                    objtvl010.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Updated Successfully";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteTravelerHeader(string headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            int intheaderId = 0;
            try
            {
                if (!string.IsNullOrEmpty(headerId))
                {
                    intheaderId = Convert.ToInt32(headerId);
                    TVL010 objTVL010 = db.TVL010.FirstOrDefault(c => c.HeaderId == intheaderId);

                    if (objTVL010 != null)
                    {
                        if (objTVL010.TravelerRevNo == 0)
                        {
                            db.TVL011.RemoveRange(objTVL010.TVL011);



                            db.TVL010.Remove(objTVL010);

                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = "Traveler deleted successfully";
                        }
                        else
                        {
                            var objtraveler011 = db.TVL011.Where(m => m.HeaderId == objTVL010.HeaderId).ToList();




                            int travelerrevno = Convert.ToInt32(objTVL010.TravelerRevNo - 1);
                            string status = clsImplementationEnum.TravelerHeaderStatus.Released.GetStringValue();
                            TVL010_Log objtvl010_log = db.TVL010_Log.Where(m => m.ProjectNo == objTVL010.ProjectNo && m.HeaderId == objTVL010.HeaderId && m.TravelerRevNo == travelerrevno && m.Status == status).FirstOrDefault();
                            if (objtvl010_log != null)
                            {
                                objTVL010.HeaderId = objtvl010_log.HeaderId;
                                objTVL010.Identification = objtvl010_log.Identification;
                                objTVL010.IdentificationNo = objtvl010_log.IdentificationNo;
                                objTVL010.TravelerNo = objtvl010_log.TravelerNo;
                                objTVL010.TravelerRevNo = objtvl010_log.TravelerRevNo;
                                objTVL010.TravelerDescription = objtvl010_log.TravelerDescription;
                                objTVL010.TravelerRevisionDetails = objtvl010_log.TravelerRevisionDetails;
                                objTVL010.Status = objtvl010_log.Status;
                                objTVL010.BU = objtvl010_log.BU;
                                objTVL010.Location = objtvl010_log.Location;
                                objTVL010.Customer = objtvl010_log.Customer;
                                objTVL010.Note1 = objtvl010_log.Note1;
                                objTVL010.Note2 = objtvl010_log.Note2;
                                objTVL010.Note3 = objtvl010_log.Note3;
                                objTVL010.Note4 = objtvl010_log.Note4;
                                objTVL010.Note5 = objtvl010_log.Note5;
                                objTVL010.Note6 = objtvl010_log.Note6;
                                objTVL010.Note7 = objtvl010_log.Note7;
                                objTVL010.Note8 = objtvl010_log.Note8;
                                objTVL010.Note9 = objtvl010_log.Note9;
                                objTVL010.Note10 = objtvl010_log.Note10;
                                objTVL010.Note11 = objtvl010_log.Note11;
                                objTVL010.Note12 = objtvl010_log.Note12;
                                objTVL010.Note13 = objtvl010_log.Note13;
                                objTVL010.Note14 = objtvl010_log.Note14;
                                objTVL010.Note15 = objtvl010_log.Note15;
                                objTVL010.InitiatorRemarks = objtvl010_log.InitiatorRemarks;
                                objTVL010.PlanningApprovedBy = objtvl010_log.PlanningApprovedBy;
                                objTVL010.PlanningSubmittedBy = objtvl010_log.PlanningSubmittedBy;
                                objTVL010.PlanningSubmittedOn = objtvl010_log.PlanningSubmittedOn;
                                objTVL010.QIApprovedBy = objtvl010_log.QIApprovedBy;
                                objTVL010.QIApprovedOn = objtvl010_log.QIApprovedOn;
                                objTVL010.QIReturnedBy = objtvl010_log.QIReturnedBy;
                                objTVL010.QIReturnedOn = objtvl010_log.QIReturnedOn;
                                objTVL010.QIRemarks = objtvl010_log.QIRemarks;
                                objTVL010.WEApprovedBy = objtvl010_log.WEApprovedBy;
                                objTVL010.WEApprovedOn = objtvl010_log.WEApprovedOn;
                                objTVL010.WEReturnedBy = objtvl010_log.WEReturnedBy;
                                objTVL010.WEReturnedOn = objtvl010_log.WEReturnedOn;
                                objTVL010.WERemarks = objtvl010_log.WERemarks;
                                objTVL010.PlanningApprovedBy = objtvl010_log.PlanningApprovedBy;
                                objTVL010.PlanningApprovedOn = objtvl010_log.PlanningApprovedOn;
                                objTVL010.PlanningReturnedBy = objtvl010_log.PlanningReturnedBy;
                                objTVL010.PlanningReturnedOn = objtvl010_log.PlanningReturnedOn;
                                objTVL010.PlanningRemarks = objtvl010_log.PlanningRemarks;
                                objTVL010.PlanningReleasedBy = objtvl010_log.PlanningReleasedBy;
                                objTVL010.PlanningReleasedOn = objtvl010_log.PlanningReleasedOn;
                                objTVL010.CompletedBy = objtvl010_log.CompletedBy;
                                objTVL010.CompletedOn = objtvl010_log.CompletedOn;
                                objTVL010.CreatedBy = objtvl010_log.CreatedBy;
                                objTVL010.CreatedOn = objtvl010_log.CreatedOn;
                                db.SaveChanges();
                                if (objTVL010.TVL011.Count > 0)
                                {
                                    foreach (var item in objTVL010.TVL011.ToList())
                                    {
                                        var objTVL011_Log = db.TVL011_Log.Where(m => m.HeaderId == objTVL010.HeaderId && m.LineStatus == status && m.OperationNo == item.OperationNo).FirstOrDefault();
                                        if (objTVL011_Log != null)
                                        {
                                            TVL011 objtvl011 = db.TVL011.Where(m => m.HeaderId == objTVL010.HeaderId && m.OperationNo == item.OperationNo).FirstOrDefault();
                                            int? oldrev = objTVL011_Log.OperationRevisionNo;
                                            int? newrev = objtvl011.OperationRevisionNo;
                                            objtvl011.LineId = objTVL011_Log.LineId;
                                            objtvl011.HeaderId = objTVL011_Log.HeaderId;
                                            objtvl011.OperationNo = objTVL011_Log.OperationNo;
                                            objtvl011.OperationRevisionNo = objTVL011_Log.OperationRevisionNo;
                                            objtvl011.Activity = objTVL011_Log.Activity;
                                            objtvl011.RefDocument = objTVL011_Log.RefDocument;
                                            objtvl011.RefDocRevNo = objTVL011_Log.RefDocRevNo;
                                            objtvl011.LTIntervention = objTVL011_Log.LTIntervention;
                                            objtvl011.I1Intervention = objTVL011_Log.I1Intervention;
                                            objtvl011.I2Intervention = objTVL011_Log.I2Intervention;
                                            objtvl011.I3Intervention = objTVL011_Log.I3Intervention;
                                            objtvl011.I4Intervention = objTVL011_Log.I4Intervention;
                                            objtvl011.InspectionRecordRequired = objTVL011_Log.InspectionRecordRequired;
                                            objtvl011.CreatedBy = objTVL011_Log.CreatedBy;
                                            objtvl011.CreatedOn = objTVL011_Log.CreatedOn;
                                            objtvl011.EditedBy = objTVL011_Log.EditedBy;
                                            objtvl011.EditedOn = objTVL011_Log.EditedOn;
                                            objtvl011.LineStatus = objTVL011_Log.LineStatus;
                                            objtvl011.ReturnRemarks = objTVL011_Log.ReturnRemarks;
                                            objtvl011.ReturnRole = objTVL011_Log.ReturnRole;
                                            objtvl011.ReturnedBy = objTVL011_Log.ReturnedBy;
                                            objtvl011.ReturnedOn = objTVL011_Log.ReturnedOn;
                                            db.SaveChanges();
                                            if (!string.IsNullOrEmpty(objtvl011.RefDocument))
                                            {
                                                if (newrev != oldrev)
                                                {
                                                    string deletefolderPath = "TVL011//" + objtvl011.LineId + "//R" + newrev;
                                                    //var lstTVL011RefDoc = (new clsFileUpload()).GetDocuments(deletefolderPath, true).Select(x => x.Name).ToList();
                                                    //foreach (var filename in lstTVL011RefDoc)
                                                    //{
                                                    //    (new clsFileUpload()).DeleteFileAsync(deletefolderPath, filename.ToString());
                                                    //}

                                                    Utility.Controllers.FileUploadController _objFUC = new Utility.Controllers.FileUploadController();
                                                    _objFUC.DeleteAllFileOnFCSServerAsync(objtvl011.LineId, deletefolderPath, deletefolderPath, DESServices.CommonService.GetUseIPConfig);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (!string.IsNullOrEmpty(item.RefDocument))
                                            {
                                                string deletefolderPath = "TVL011//" + item.LineId + "//R" + item.OperationRevisionNo;
                                                //var lstTVL011RefDoc = (new clsFileUpload()).GetDocuments(deletefolderPath, true).Select(x => x.Name).ToList();
                                                //foreach (var filename in lstTVL011RefDoc)
                                                //{
                                                //    //(new clsFileUpload()).DeleteFileAsync(deletefolderPath, filename.ToString());
                                                    
                                                //    //_obj.DeleteFileOnFCSServerAsync(itm.DocumentMappingId, CommonService.GetUseIPConfig);
                                                //}
                                                IEMQS.Areas.Utility.Controllers.FileUploadController _obj = new IEMQS.Areas.Utility.Controllers.FileUploadController();
                                                _obj.DeleteAllFileOnFCSServerAsync(item.LineId, deletefolderPath, deletefolderPath, IEMQS.DESServices.CommonService.GetUseIPConfig);
                                            }
                                            db.TVL011.Remove(item);
                                            db.SaveChanges();
                                        }
                                    }
                                }
                                objResponseMsg.Key = true;
                                objResponseMsg.Value = "Traveler deleted successfully";
                            }
                            else
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = "Traveler deleted Not successfully";
                            }
                        }
                    }




                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }


            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Lines Details
        [HttpPost]
        public ActionResult GetTravelerLinesPartial(int headerId)
        {
            TVL010 objTVL010 = new TVL010();

            if (headerId > 0)
                objTVL010 = db.TVL010.Where(i => i.HeaderId == headerId).FirstOrDefault();

            UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;



            if (objTVL010.QIReturnedBy != null)
            {

                ViewBag.ReturnByqi = objTVL010.QIReturnedBy + " - " + Manager.GetUserNameFromPsNo(objTVL010.QIReturnedBy);
                ViewBag.ReturnDateqi = objTVL010.QIReturnedOn.Value.ToString("dd/MM/yyyy hh:mm tt");
                ViewBag.Remarksqi = objTVL010.QIRemarks;
            }
            if (objTVL010.WEReturnedBy != null)
            {

                ViewBag.ReturnBywe = objTVL010.WEReturnedBy + " - " + Manager.GetUserNameFromPsNo(objTVL010.WEReturnedBy);
                ViewBag.ReturnDatewe = objTVL010.WEReturnedOn.Value.ToString("dd/MM/yyyy hh:mm tt");
                ViewBag.Remarkswe = objTVL010.WERemarks;
            }
            if (objTVL010.PlanningReturnedBy != null)
            {

                ViewBag.ReturnByplng = objTVL010.PlanningReturnedBy + " - " + Manager.GetUserNameFromPsNo(objTVL010.PlanningReturnedBy);
                ViewBag.ReturnDateplng = objTVL010.PlanningReturnedOn.Value.ToString("dd/MM/yyyy hh:mm tt");
                ViewBag.Remarksplng = objTVL010.PlanningRemarks;
            }

            bool isEditable = ((objTVL010.Status == clsImplementationEnum.TravelerHeaderStatus.ReturnedbyQI.GetStringValue() || objTVL010.Status == clsImplementationEnum.TravelerHeaderStatus.ReturnedbyWE.GetStringValue() || objTVL010.Status == clsImplementationEnum.TravelerHeaderStatus.Returnedbyplanning.GetStringValue()));
            ViewBag.Headerstatus = isEditable;

            bool isstatus = ((objTVL010.Status == clsImplementationEnum.TravelerHeaderStatus.Draft.GetStringValue() || objTVL010.Status == clsImplementationEnum.TravelerHeaderStatus.ReturnedbyQI.GetStringValue() || objTVL010.Status == clsImplementationEnum.TravelerHeaderStatus.ReturnedbyWE.GetStringValue() || objTVL010.Status == clsImplementationEnum.TravelerHeaderStatus.Returnedbyplanning.GetStringValue()));
            ViewBag.isstatus = isstatus;

            ViewBag.InspectionRecordRequired = new List<CategoryData> { new CategoryData { Value = "Yes", Code = "1", CategoryDescription = "Yes" }, new CategoryData { Value = "No", Code = "0", CategoryDescription = "No" } };
            ViewBag.project = objTVL010.ProjectNo;
            ViewBag.lntintervention = Manager.GetSubCatagories("Traveler Intervention", objTVL010.BU, objTVL010.Location).Select(i => new CategoryData { Value = i.Description, Code = i.Code, CategoryDescription = i.Description }).ToList();
            ViewBag.IIntervention = Manager.GetSubCatagories("Traveler TPI Intervention", objTVL010.BU, objTVL010.Location).Select(i => new CategoryData { Value = i.Description, Code = i.Code, CategoryDescription = i.Description }).ToList();
            ViewBag.Status = objTVL010.Status;
            ViewBag.Inspectionrequired = new List<CategoryData> { new CategoryData { Value = "Yes", Code = "Yes", CategoryDescription = "Yes" }, new CategoryData { Value = "No", Code = "No", CategoryDescription = "No" } };
            return PartialView("_GetILinesDataPartial", objTVL010);
        }

        [HttpPost]
        public ActionResult GetTravelerLinesData(JQueryDataTableParamModel param, string headerId)
        {
            try
            {

                UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();

                int intHeaderId = Convert.ToInt32(headerId);

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND tvl11.HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (tvl11.OperationNo like '%" + param.sSearch
                        + "%' or tvl11.OperationNo like '%" + param.sSearch
                        + "%' or tvl11.OperationRevisionNo like '%" + param.sSearch
                        + "%' or tvl11.Activity like '%" + param.sSearch
                        + "%' or tvl11.RefDocument like '%" + param.sSearch
                        + "%' or tvl11.RefDocRevNo like '%" + param.sSearch
                        + "%' or tvl11.LTIntervention like '%" + param.sSearch
                        + "%' or tvl11.I1Intervention like '%" + param.sSearch
                        + "%' or tvl11.I2Intervention like '%" + param.sSearch
                        + "%' or tvl11.I3Intervention like '%" + param.sSearch
                         + "%' or tvl11.I4Intervention like '%" + param.sSearch
                        + "%' or tvl11.InspectionRecordRequired like '%" + param.sSearch
                        + "%')";
                }

                var lstResult = db.SP_TVL_GET_LINES_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                TVL010 objTVL010 = db.TVL010.Where(c => c.HeaderId == intHeaderId).FirstOrDefault();
                var itemsrefdocument = (from li in db.TVL002.Where(m => m.ProjectNo == objTVL010.ProjectNo)
                                        select new SelectItemList
                                        {
                                            id = li.RefDocument,
                                            text = li.RefDocument,
                                        }).Distinct().ToList();
                if (itemsrefdocument.Count == 0)
                {
                    itemsrefdocument = null;
                }


                bool isEditable = ((objTVL010.Status == clsImplementationEnum.TravelerHeaderStatus.Draft.GetStringValue() || objTVL010.Status == clsImplementationEnum.TravelerHeaderStatus.ReturnedbyQI.GetStringValue() || objTVL010.Status == clsImplementationEnum.TravelerHeaderStatus.ReturnedbyWE.GetStringValue() || objTVL010.Status == clsImplementationEnum.TravelerHeaderStatus.Returnedbyplanning.GetStringValue()) && objUserRoleAccessDetails.UserDesignation == clsImplementationEnum.UserAccessRole.Initiator.GetStringValue());

                bool ischeckstatus = Checkstatus(intHeaderId);
                List<SelectItemList> lst = new List<SelectItemList>();


                int newRecordId = 0;
                var newRecord = new[] {
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateNumericTextbox(newRecordId, "OperationNo", "", "", false, "", false, "10"),
                                   "R"+objTVL010.TravelerRevNo,
                                   //Helper.GenerateTextArea(newRecordId, "Activity", "", "", false, "", false, "1000"),
                                  // Helper.GenerateTextArea(newRecordId, "Activity",  "", "", false, "", "1000"),
                                   Helper.GenerateTextArea(newRecordId, "Activity", "", "", false, "width: 300px !important; height: 100px !important;", "1000"),
                                   itemsrefdocument!=null?Helper.MultiSelectDropdown(itemsrefdocument,newRecordId,"RefDocument",false,"Getrevno(0)","","RefDocument"):Helper.MultiSelectDropdown(lst,newRecordId,"RefDocument",false,"","","RefDocument"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "RefDocRevNo", "", "", true, "", false, "100"),
                                   Helper.HTMLAutoComplete(newRecordId,"txtintervention","","",false,"","LTIntervention")+""+Helper.GenerateHidden(newRecordId,"LTIntervention"),
                                   Helper.HTMLAutoComplete(newRecordId,"txtI1Intervention","","",false,"","I1Intervention")+""+Helper.GenerateHidden(newRecordId,"I1Intervention"),
                                   Helper.HTMLAutoComplete(newRecordId,"txtI2Intervention","","",false,"","I2Intervention")+""+Helper.GenerateHidden(newRecordId,"I2Intervention"),
                                   Helper.HTMLAutoComplete(newRecordId,"txtI3Intervention","","",false,"","I3Intervention")+""+Helper.GenerateHidden(newRecordId,"I3Intervention"),
                                   Helper.HTMLAutoComplete(newRecordId,"txtI4Intervention","","",false,"","I4Intervention")+""+Helper.GenerateHidden(newRecordId,"I4Intervention"),
                                   Helper.HTMLAutoComplete(newRecordId,"txtInspectionRecord","No","",false,"","InspectionRecordRequired")+""+Helper.GenerateHidden(newRecordId,"InspectionRecordRequired"),
                                    Helper.GenerateHTMLTextbox(newRecordId, "ReturnRemarks", "", "", false, "", true, "100"),
                                   "",


                                    Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveTravelerLine();")
                                };

                var data = (from uc in lstResult
                            select new[]
                            {

                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                               uc.OperationNo.Value.ToString("G29"),
                               "R" + Convert.ToString(uc.OperationRevisionNo),
                                isEditable ? uc.LineStatus!=clsImplementationEnum.TravelerLineStatus.Deleted.GetStringValue()? Helper.GenerateTextArea(uc.LineId, "Activity", Convert.ToString(uc.Activity), "UpdateTravelerLine(this,"+ uc.LineId +","+ uc.HeaderId +")", false,"width: 300px !important; height: 100px !important;", "1000")  : Convert.ToString(uc.Activity):Convert.ToString(uc.Activity),
                               isEditable  ? uc.LineStatus!=clsImplementationEnum.TravelerLineStatus.Deleted.GetStringValue()? Helper.MultiSelectDropdown(itemsrefdocument,uc.LineId, Convert.ToString(uc.RefDocument),false,"UpdateTravelerLine(this,"+ uc.LineId +","+ uc.HeaderId +")","", "RefDocument"): Convert.ToString(uc.RefDocument):Convert.ToString(uc.RefDocument),
                              //isEditable  ? Helper.MultiSelectDropdown(itemsrefdocument,uc.LineId, Convert.ToString(uc.RefDocument),false, "UpdateTravelerLine(this,"+ uc.LineId +","+ uc.HeaderId +")","", "RefDocument"): Convert.ToString(uc.RefDocument),

                               isEditable ? uc.LineStatus!=clsImplementationEnum.TravelerLineStatus.Deleted.GetStringValue()? Helper.GenerateHTMLTextboxOnChanged(uc.LineId, "RefDocRevNo", uc.RefDocRevNo, "UpdateTravelerLine(this,"+ uc.LineId +","+ uc.HeaderId +")", true, "")  : Convert.ToString(uc.RefDocRevNo):Convert.ToString(uc.RefDocRevNo),
                               isEditable ?uc.LineStatus!=clsImplementationEnum.TravelerLineStatus.Deleted.GetStringValue()? Helper.HTMLAutoComplete(uc.LineId,"txtintervention",uc.LTIntervention,"UpdateTravelerLine(this,"+ uc.LineId +","+ uc.HeaderId +")", false, "","LTIntervention",false)+""+Helper.GenerateHidden(uc.LineId,"LTIntervention",uc.LTIntervention) : Manager.TVLIntervention(uc.LTIntervention):Manager.TVLIntervention(uc.LTIntervention),
                               isEditable ? uc.LineStatus!=clsImplementationEnum.TravelerLineStatus.Deleted.GetStringValue()? Helper.HTMLAutoComplete(uc.LineId,"txtI1Intervention",uc.I1Intervention,"UpdateTravelerLine(this,"+ uc.LineId +","+ uc.HeaderId +")", false, "","I1Intervention",false)+""+Helper.GenerateHidden(uc.LineId,"I1Intervention",uc.I1Intervention) : uc.I1Intervention:uc.I1Intervention,
                               isEditable ? uc.LineStatus!=clsImplementationEnum.TravelerLineStatus.Deleted.GetStringValue()? Helper.HTMLAutoComplete(uc.LineId,"txtI2Intervention",uc.I2Intervention,"UpdateTravelerLine(this,"+ uc.LineId +","+ uc.HeaderId +")", false, "","I2Intervention",false)+""+Helper.GenerateHidden(uc.LineId,"I2Intervention",uc.I2Intervention) : uc.I2Intervention:uc.I2Intervention,
                               isEditable ? uc.LineStatus!=clsImplementationEnum.TravelerLineStatus.Deleted.GetStringValue()?Helper.HTMLAutoComplete(uc.LineId,"txtI3Intervention",uc.I3Intervention,"UpdateTravelerLine(this,"+ uc.LineId +","+ uc.HeaderId +")", false, "","I3Intervention",false)+""+Helper.GenerateHidden(uc.LineId,"I3Intervention",uc.I3Intervention) : uc.I3Intervention:uc.I3Intervention,
                               isEditable ? uc.LineStatus!=clsImplementationEnum.TravelerLineStatus.Deleted.GetStringValue()?Helper.HTMLAutoComplete(uc.LineId,"txtI4Intervention",uc.I4Intervention,"UpdateTravelerLine(this,"+ uc.LineId +","+ uc.HeaderId +")", false, "","I4Intervention",false)+""+Helper.GenerateHidden(uc.LineId,"I4Intervention",uc.I4Intervention) : uc.I4Intervention:uc.I4Intervention,
                                isEditable ? uc.LineStatus!=clsImplementationEnum.TravelerLineStatus.Deleted.GetStringValue()?Helper.HTMLAutoComplete(uc.LineId,"txtInspectionRecord",uc.InspectionRecordRequired,"UpdateTravelerLine(this,"+ uc.LineId +","+ uc.HeaderId +")", false, "","InspectionRecordRequired",false)+""+Helper.GenerateHidden(uc.LineId,"InspectionRecordRequired",uc.InspectionRecordRequired) : uc.InspectionRecordRequired:uc.InspectionRecordRequired,
                                //Helper.GenerateHTMLTextboxOnChanged(uc.LineId, "ReturnRemarks", Convert.ToString(uc.ReturnRemarks), "UpdateTravelerLine(this,"+ uc.LineId +","+ uc.HeaderId +")", (objTVL010.Status==clsImplementationEnum.TravelerHeaderStatus.ReturnedbyQI.GetStringValue() || objTVL010.Status==clsImplementationEnum.TravelerHeaderStatus.ReturnedbyWE.GetStringValue() || objTVL010.Status==clsImplementationEnum.TravelerHeaderStatus.Returnedbyplanning.GetStringValue() || objTVL010.Status==clsImplementationEnum.TravelerHeaderStatus.Draft.GetStringValue() || objTVL010.Status==clsImplementationEnum.TravelerHeaderStatus.Released.GetStringValue() || objTVL010.Status==clsImplementationEnum.TravelerHeaderStatus.Completed.GetStringValue() || objTVL010.Status==clsImplementationEnum.TravelerHeaderStatus.ApprovedbyPlanning.GetStringValue() ||objUserRoleAccessDetails.UserRole==clsImplementationEnum.UserRoleName.PLNG3.GetStringValue()), "100"),
                                Helper.GenerateHTMLTextboxOnChanged(uc.LineId, "ReturnRemarks", Convert.ToString(uc.ReturnRemarks), "UpdateTravelerLine(this,"+ uc.LineId +","+ uc.HeaderId +")", (!ischeckstatus),"100"),
                                 Convert.ToString(uc.LineStatus),
                               "<nobr>" +  Helper.GenerateActionIcon(uc.LineId, "Delete", "Delete Operation", "fa fa-trash-o", "DeleteTravelerLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +")", "",!(objUserRoleAccessDetails.UserDesignation == clsImplementationEnum.UserAccessRole.Initiator.GetStringValue() && uc.LineStatus != clsImplementationEnum.TravelerLineStatus.Deleted.GetStringValue() && (objTVL010.Status == clsImplementationEnum.TravelerHeaderStatus.Draft.GetStringValue() || objTVL010.Status == clsImplementationEnum.TravelerHeaderStatus.ReturnedbyQI.GetStringValue() || objTVL010.Status == clsImplementationEnum.TravelerHeaderStatus.ReturnedbyWE.GetStringValue() || objTVL010.Status == clsImplementationEnum.TravelerHeaderStatus.Returnedbyplanning.GetStringValue()))) +
                                Helper.GenerateActionIcon(uc.HeaderId, "History", "View Return Remarks History", "fa fa-history", "HistoryRemark("+uc.HeaderId+","+uc.OperationNo+")", "", false) +
                               "<i title='View Timeline' href='javascript:void(0)' onclick=ShowTimeline('/TVL/MaintainTraveler/ShowTimeline?HeaderID="+ uc.HeaderId +"&LineId="+ uc.LineId + "'"+"); class='iconspace fa fa-clock-o'></i>" +
                                //Helper.GenerateActionIcon(uc.LineId,"Timeline","Timeline","fa fa-clock-o", "ShowTimeline(\"/TVL/MaintainTraveler/ShowTimeline?HeaderID="+ uc.HeaderId +"&LineId="+ uc.LineId +"\");") +
                                Helper.GenerateActionIcon(uc.LineId,"Attachment","Reference Document","fa fa-paperclip", "ViewAttachment("+uc.LineId+","+uc.OperationRevisionNo+");") +
                                "</nobr>"

                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public ActionResult SaveTravelerLine(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            TVL011 objTVL011 = new TVL011();
            int newRowIndex = 0;
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                if (refHeaderId > 0)
                {
                    TVL010 objTVL010 = db.TVL010.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    string s = (fc["RefDocRevNo" + newRowIndex]);
                    decimal intOprNo = !string.IsNullOrEmpty(fc["OperationNo" + newRowIndex]) ? Convert.ToDecimal(fc["OperationNo" + newRowIndex]) : 0;
                    string strActivity = !string.IsNullOrEmpty(fc["Activity" + newRowIndex]) ? Convert.ToString(fc["Activity" + newRowIndex]).Trim() : string.Empty;
                    string strRefDocument = !string.IsNullOrEmpty(fc["RefDocument" + newRowIndex]) ? Convert.ToString(fc["RefDocument" + newRowIndex]).Trim() : string.Empty;
                    string strRefdocRevNo = !string.IsNullOrEmpty(fc["RefDocRevNo" + newRowIndex]) ? Convert.ToString(fc["RefDocRevNo" + newRowIndex]).Trim() : string.Empty;
                    string strLTIntervention = !string.IsNullOrEmpty(fc["LTIntervention" + newRowIndex]) ? Convert.ToString(fc["LTIntervention" + newRowIndex]).Trim() : string.Empty;
                    string strI1Intervention = !string.IsNullOrEmpty(fc["I1Intervention" + newRowIndex]) ? Convert.ToString(fc["I1Intervention" + newRowIndex]).Trim() : string.Empty;
                    string strI2Intervention = !string.IsNullOrEmpty(fc["I2Intervention" + newRowIndex]) ? Convert.ToString(fc["I2Intervention" + newRowIndex]).Trim() : string.Empty;
                    string strI3Intervention = !string.IsNullOrEmpty(fc["I3Intervention" + newRowIndex]) ? Convert.ToString(fc["I3Intervention" + newRowIndex]).Trim() : string.Empty;
                    string strI4Intervention = !string.IsNullOrEmpty(fc["I4Intervention" + newRowIndex]) ? Convert.ToString(fc["I4Intervention" + newRowIndex]).Trim() : string.Empty;
                    string strInspectionRecordRequired = !string.IsNullOrEmpty(fc["InspectionRecordRequired" + newRowIndex]) ? Convert.ToString(fc["InspectionRecordRequired" + newRowIndex]).Trim() : string.Empty;
                    if (strInspectionRecordRequired == "")
                    {
                        strInspectionRecordRequired = "No";
                    }


                    string readyToOfferStatus = clsImplementationEnum.SeamListInspectionStatus.ReadyToOffer.GetStringValue();

                    if (isTravelerLIneExist(objTVL010.HeaderId, intOprNo))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Record is already exist";
                    }
                    else
                    {
                        #region Add New LTFPS Line
                        objTVL011.OperationNo = intOprNo;
                        objTVL011.HeaderId = refHeaderId;
                        objTVL011.OperationRevisionNo = objTVL010.TravelerRevNo;
                        objTVL011.Activity = strActivity;
                        objTVL011.RefDocument = strRefDocument;
                        objTVL011.RefDocRevNo = strRefdocRevNo;
                        objTVL011.LTIntervention = strLTIntervention;
                        objTVL011.I1Intervention = strI1Intervention;
                        objTVL011.I2Intervention = strI2Intervention;
                        objTVL011.I3Intervention = strI3Intervention;
                        objTVL011.I4Intervention = strI4Intervention;
                        objTVL011.InspectionRecordRequired = strInspectionRecordRequired;

                        objTVL011.CreatedBy = objClsLoginInfo.UserName;
                        objTVL011.CreatedOn = DateTime.Now;
                        objTVL011.LineStatus = clsImplementationEnum.TravelerLineStatus.Added.GetStringValue();

                        db.TVL011.Add(objTVL011);
                        db.SaveChanges();

                        #region Copy Reference document
                        if (!string.IsNullOrEmpty(strRefDocument))
                        {
                            string[] document = strRefDocument.Split(',');
                            string[] strRevNo = strRefdocRevNo.Split(',');
                            int i = 0;
                            foreach (var doc in document)
                            {
                                if (strRevNo.Length == 0)
                                {
                                    break;
                                }
                                int revno = Convert.ToInt32(strRevNo[i]);
                                var refdocument = db.TVL002.Where(x => x.RefDocument == doc && x.ProjectNo == objTVL010.ProjectNo && x.RefDocRevNo == revno).FirstOrDefault();
                                if (refdocument != null)
                                {
                                    string referenceDoc = "TVL002//" + refdocument.HeaderId;
                                    string newfolderPath = "TVL011//" + objTVL011.LineId + "//R" + objTVL011.OperationRevisionNo;
                                    //(new clsFileUpload()).CopyFolderContentsAsync(referenceDoc, newfolderPath);
                                    Utility.Controllers.FileUploadController _objFUC = new Utility.Controllers.FileUploadController();
                                    _objFUC.CopyDataOnFCSServerAsync(referenceDoc, newfolderPath, "TVL002", refdocument.HeaderId, newfolderPath, objTVL011.LineId, DESServices.CommonService.GetUseIPConfig, DESServices.CommonService.objClsLoginInfo.UserName, 0, false);
                                }
                                i++;
                            }
                        }
                        #endregion
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Operation added successfully";
                        objResponseMsg.HeaderId = objTVL011.HeaderId;

                        #endregion
                    }
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateTravelerLine(int lineId, int headerId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            TVL011 objTVL011 = new TVL011();
            objTVL011 = db.TVL011.Where(m => m.LineId == lineId).FirstOrDefault();
            int? oldrev = objTVL011.OperationRevisionNo;
            int? newrev = objTVL011.TVL010.TravelerRevNo;
            try
            {
                string tableName = "TVL011";
                UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();
                if (!string.IsNullOrEmpty(columnName))
                {
                    if (columnName == "RefDocument")
                    {

                        string existingRefDoc = objTVL011.RefDocument;
                        string existingRevNo = objTVL011.RefDocRevNo;
                        if (!string.IsNullOrEmpty(columnValue))
                        {
                            var project = objTVL011.TVL010.ProjectNo;
                            var lstPosition = columnValue.Split(',').ToList();
                            var refdocvno = lstPosition;

                            StringBuilder builder = new StringBuilder();
                            foreach (var item in refdocvno) // Loop through all strings
                            {
                                var refno = db.TVL002.Where(m => m.ProjectNo == project && m.RefDocument.Trim() == item.Trim()).ToList().Max(m => m.RefDocRevNo);
                                builder.Append(refno).Append(","); // Append string to StringBuilder
                            }
                            string Revno = builder.ToString(); // Get string from 
                            var Refrevno = Revno.Substring(0, Revno.Length - 1);
                            objTVL011.RefDocument = columnValue;
                            objTVL011.RefDocRevNo = Refrevno;
                            objTVL011.LineStatus = clsImplementationEnum.TravelerLineStatus.Modified.GetStringValue();
                            objTVL011.OperationRevisionNo = objTVL011.TVL010.TravelerRevNo;
                            db.SaveChanges();
                            #region Copy Reference document
                            if (!string.IsNullOrEmpty(objTVL011.RefDocument) && !string.IsNullOrEmpty(objTVL011.RefDocRevNo))
                            {
                                if (oldrev < newrev)
                                { //copy files after revision
                                    string oldDocuments = "TVL011//" + objTVL011.LineId + "//R" + oldrev;
                                    string revisedDocuments = "TVL011//" + objTVL011.LineId + "//R" + objTVL011.OperationRevisionNo;
                                    //(new clsFileUpload()).CopyFolderContentsAsync(oldDocuments, revisedDocuments);
                                    Utility.Controllers.FileUploadController _objFUC = new Utility.Controllers.FileUploadController();
                                    _objFUC.CopyDataOnFCSServerAsync(oldDocuments, revisedDocuments, oldDocuments, objTVL011.LineId, revisedDocuments, objTVL011.LineId, DESServices.CommonService.GetUseIPConfig, DESServices.CommonService.objClsLoginInfo.UserName, 0, false);
                                }
                                var newRefDoc = objTVL011.RefDocument.Split(',').ToList();
                                var newRevNo = objTVL011.RefDocRevNo.Split(',');
                                var oldRefDoc = (existingRefDoc == null ? null : existingRefDoc.Split(',').ToList());
                                var oldRevNo = (existingRevNo == null ? null : existingRevNo.Split(',').ToList());
                                List<CategoryData> lstDeleteExistingDoc = new List<CategoryData>();
                                List<CategoryData> lstAddNewDoc = new List<CategoryData>();

                                // ref document removed for old reocrd
                                int o = 0;
                                if (oldRefDoc != null)
                                {
                                    foreach (var oldDoc in oldRefDoc)
                                    {
                                        int revno = Convert.ToInt32(oldRevNo[o]);
                                        if (!newRefDoc.Contains(oldDoc.ToString()))
                                        {
                                            lstDeleteExistingDoc.Add(new CategoryData { Description = oldDoc.ToString(), Id = revno });
                                        }
                                        o++;
                                    }
                                }

                                // ref document added for new record  
                                int n = 0;
                                foreach (var newDoc in newRefDoc)
                                {
                                    int revno = Convert.ToInt32(newRevNo[n]);
                                    if (existingRefDoc != null)
                                    {
                                        if (!existingRefDoc.Trim().Contains(newDoc.Trim().ToString()))
                                        {
                                            lstAddNewDoc.Add(new CategoryData { Description = newDoc.ToString(), Id = revno });
                                        }
                                    }
                                    else
                                    {
                                        lstAddNewDoc.Add(new CategoryData { Description = newDoc.ToString(), Id = revno });
                                    }
                                    n++;
                                }

                                //delete file from deleted ref doc
                                foreach (var deleted in lstDeleteExistingDoc)
                                {
                                    var refdocument = db.TVL002.Where(x => x.RefDocument == deleted.Description && x.ProjectNo == objTVL011.TVL010.ProjectNo && x.RefDocRevNo == deleted.Id).FirstOrDefault();
                                    string refdocfolderPath = "TVL002//" + refdocument.HeaderId;
                                    string deletefolderPath = "TVL011//" + objTVL011.LineId + "//R" + objTVL011.OperationRevisionNo;

                                    //var lstTVL002RefDoc = (new clsFileUpload()).GetDocuments(refdocfolderPath, true).Select(x => x.Name).ToList();
                                    //var lstTVL011RefDoc = (new clsFileUpload()).GetDocuments(deletefolderPath, true).Select(x => x.Name).ToList();

                                    IEMQS.Areas.Utility.Controllers.FileUploadController _obj = new IEMQS.Areas.Utility.Controllers.FileUploadController();

                                    var lstTVL002RefDoc = _obj.GetAllDocumentsByTableNameTableId(refdocfolderPath, refdocument.HeaderId, 1).ToList();
                                    var lstTVL011RefDoc = _obj.GetAllDocumentsByTableNameTableId(deletefolderPath, objTVL011.LineId, 1).ToList();

                                    foreach (var filename in lstTVL011RefDoc)
                                    {
                                        if (lstTVL002RefDoc.Contains(filename))
                                        {
                                            //(new clsFileUpload()).DeleteFileAsync(deletefolderPath, filename.ToString());
                                            _obj.DeleteFileOnFCSServerAsync(filename.DocumentMappingId, DESServices.CommonService.GetUseIPConfig);
                                        }
                                    }

                                }

                                //for new added file
                                foreach (var doc in lstAddNewDoc)
                                {
                                    var refdocument = db.TVL002.Where(x => x.RefDocument == doc.Description && x.ProjectNo == objTVL011.TVL010.ProjectNo && x.RefDocRevNo == doc.Id).FirstOrDefault();
                                    string refDocPath = "TVL002//" + refdocument.HeaderId;
                                    string newrefDocPath = "TVL011//" + objTVL011.LineId + "//R" + objTVL011.OperationRevisionNo;
                                    //(new clsFileUpload()).CopyFolderContentsAsync(refDocPath, newrefDocPath);
                                    Utility.Controllers.FileUploadController _objFUC = new Utility.Controllers.FileUploadController();
                                    _objFUC.CopyDataOnFCSServerAsync(refDocPath, newrefDocPath, "TVL002", refdocument.HeaderId, newrefDocPath, objTVL011.LineId, DESServices.CommonService.GetUseIPConfig, DESServices.CommonService.objClsLoginInfo.UserName, 0, false);
                                }
                            }
                            #endregion

                        }
                        else
                        {
                            objTVL011.RefDocument = null;
                            objTVL011.RefDocRevNo = null;
                            objTVL011.OperationRevisionNo = objTVL011.TVL010.TravelerRevNo;
                            objTVL011.LineStatus = clsImplementationEnum.TravelerLineStatus.Modified.GetStringValue();
                            db.SaveChanges();

                            int rev = Convert.ToInt32(existingRevNo);
                            var refdocument = db.TVL002.Where(x => x.RefDocument == existingRefDoc && x.ProjectNo == objTVL011.TVL010.ProjectNo && x.RefDocRevNo == rev).FirstOrDefault();
                            string refdocfolderPath = "TVL002/" + refdocument.HeaderId;
                            string deletefolderPath = "TVL011/" + objTVL011.LineId + "/R" + objTVL011.OperationRevisionNo;

                            //var lstTVL002RefDoc = (new clsFileUpload()).GetDocuments(refdocfolderPath, true).Select(x => x.Name).ToList();
                            //var lstTVL011RefDoc = (new clsFileUpload()).GetDocuments(deletefolderPath, true).Select(x => x.Name).ToList();

                            IEMQS.Areas.Utility.Controllers.FileUploadController _obj = new IEMQS.Areas.Utility.Controllers.FileUploadController();

                            var lstTVL002RefDoc = _obj.GetAllDocumentsByTableNameTableId(refdocfolderPath, refdocument.HeaderId, 1).ToList();
                            var lstTVL011RefDoc = _obj.GetAllDocumentsByTableNameTableId(deletefolderPath, objTVL011.LineId, 1).ToList();

                            foreach (var filename in lstTVL011RefDoc)
                            {
                                if (lstTVL002RefDoc.Contains(filename))
                                {
                                    //(new clsFileUpload()).DeleteFileAsync(deletefolderPath, filename.ToString());
                                    _obj.DeleteFileOnFCSServerAsync(filename.DocumentMappingId, DESServices.CommonService.GetUseIPConfig);
                                }
                            }
                        }
                    }
                    else
                    {
                        db.SP_COMMON_LINES_UPDATE(lineId, columnName, columnValue, tableName, objClsLoginInfo.UserName);
                        objTVL011 = db.TVL011.Where(i => i.LineId == lineId).FirstOrDefault();
                        objTVL011.LineStatus = clsImplementationEnum.TravelerLineStatus.Modified.GetStringValue();
                        objTVL011.OperationRevisionNo = objTVL011.TVL010.TravelerRevNo;
                        db.SaveChanges();
                        if (oldrev < newrev)
                        { //copy files after revision
                            string oldDocuments = "TVL011//" + objTVL011.LineId + "//R" + oldrev;
                            string revisedDocuments = "TVL011//" + objTVL011.LineId + "//R" + objTVL011.OperationRevisionNo;
                            //(new clsFileUpload()).CopyFolderContentsAsync(oldDocuments, revisedDocuments);
                            Utility.Controllers.FileUploadController _objFUC = new Utility.Controllers.FileUploadController();
                            _objFUC.CopyDataOnFCSServerAsync(oldDocuments, revisedDocuments, oldDocuments, objTVL011.LineId, revisedDocuments, objTVL011.LineId, DESServices.CommonService.GetUseIPConfig, DESServices.CommonService.objClsLoginInfo.UserName, 0, false);
                        }
                    }


                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Saved Successfully";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult UpdateTravelerDetails(int headerId, string Notedetails, string Element)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            TVL010 objTVL010 = new TVL010();
            try
            {
                objTVL010 = db.TVL010.Where(m => m.HeaderId == headerId).FirstOrDefault();
                if (Element == "Note1")
                {
                    objTVL010.Note1 = Notedetails;
                    db.SaveChanges();
                }

                if (Element == "Note2")
                {
                    objTVL010.Note2 = Notedetails;
                    db.SaveChanges();
                }
                if (Element == "Note3")
                {
                    objTVL010.Note3 = Notedetails;
                    db.SaveChanges();
                }
                if (Element == "Note4")
                {
                    objTVL010.Note4 = Notedetails;
                    db.SaveChanges();
                }
                if (Element == "Note5")
                {
                    objTVL010.Note5 = Notedetails;
                    db.SaveChanges();
                }
                if (Element == "Note6")
                {
                    objTVL010.Note6 = Notedetails;
                    db.SaveChanges();
                }
                if (Element == "Note7")
                {
                    objTVL010.Note7 = Notedetails;
                    db.SaveChanges();
                }
                if (Element == "Note8")
                {
                    objTVL010.Note8 = Notedetails;
                    db.SaveChanges();
                }
                if (Element == "Note9")
                {
                    objTVL010.Note9 = Notedetails;
                    db.SaveChanges();
                }
                if (Element == "Note10")
                {
                    objTVL010.Note10 = Notedetails;
                    db.SaveChanges();
                }
                if (Element == "Note11")
                {
                    objTVL010.Note11 = Notedetails;
                    db.SaveChanges();
                }
                if (Element == "Note12")
                {
                    objTVL010.Note12 = Notedetails;
                    db.SaveChanges();
                }
                if (Element == "Note13")
                {
                    objTVL010.Note13 = Notedetails;
                    db.SaveChanges();
                }
                if (Element == "Note14")
                {
                    objTVL010.Note14 = Notedetails;
                    db.SaveChanges();
                }

                if (Element == "Note15")
                {
                    objTVL010.Note15 = Notedetails;
                    db.SaveChanges();
                }
                if (Element == "InitiatorRemarks")
                {
                    objTVL010.InitiatorRemarks = Notedetails;
                    db.SaveChanges();
                }



                objResponseMsg.Key = true;
                objResponseMsg.Value = "Saved Successfully";


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteTravelerLine(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                TVL011 objTVL011 = db.TVL011.FirstOrDefault(c => c.LineId == lineId);
                TVL010 objtvl010 = db.TVL010.Where(m => m.HeaderId == headerId).FirstOrDefault();
                if (objTVL011 != null)
                {
                    if (objTVL011.TVL010.TravelerRevNo == 0)
                    {
                        db.TVL011.Remove(objTVL011);
                        db.SaveChanges();
                    }
                    else
                    {
                        objTVL011.LineStatus = clsImplementationEnum.TravelerLineStatus.Deleted.GetStringValue();
                        objTVL011.OperationRevisionNo = objtvl010.TravelerRevNo;
                        db.SaveChanges();
                    }
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Operation deleted successfully";

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }


            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public bool isTravelerLIneExist(int headerId, decimal oprNo)
        {
            return db.TVL011.Any(i => i.HeaderId == headerId && i.OperationNo == oprNo);
        }
        public JsonResult GetRefrevnodetails(string project, string position)
        {
            var lstPosition = position.Split(',').ToList();
            lstPosition = lstPosition.Select(x => x.Trim()).ToList();
            //var refdocvno = db.TVL002.Where(m => m.ProjectNo == project && lstPosition.Contains(m.RefDocument)).ToList().Max(m=>m.RefDocRevNo);
            var refdocvno = db.TVL002.Where(m => m.ProjectNo == project && lstPosition.Contains(m.RefDocument.Trim())).Select(m => m.RefDocument).Distinct();

            StringBuilder builder = new StringBuilder();
            foreach (var item in refdocvno) // Loop through all strings
            {
                var refno = db.TVL002.Where(m => m.ProjectNo == project && m.RefDocument.Trim() == item.Trim()).ToList().Max(m => m.RefDocRevNo);
                builder.Append(refno).Append(","); // Append string to StringBuilder
            }
            string Revno = builder.ToString(); // Get string from 
            var Refrevno = Revno.Substring(0, Revno.Length - 1);

            var data = new
            {
                Refrevno = Refrevno,

            };

            return Json(data, JsonRequestBehavior.AllowGet);

        }
        public JsonResult GetTravelerno(string project, string Identificationno)
        {
            TVL010 objtvl010 = db.TVL010.Where(m => m.ProjectNo == project && m.IdentificationNo == Identificationno).FirstOrDefault();

            string Travelerno = string.Empty;
            string Travelerdetail = string.Empty;
            if (objtvl010 != null)
            {
                Travelerdetail = objtvl010.TravelerNo;
            }

            var data = new
            {
                Travelerno = Travelerdetail

            };

            return Json(data, JsonRequestBehavior.AllowGet);

        }
        public ActionResult TVLAttachmentPartial(int? id)
        {
            TVL011 objTVL011 = db.TVL011.Where(x => x.LineId == id).FirstOrDefault();
            return PartialView("_TVLAttachmentPartial", objTVL011);
        }
        #endregion

        #region Histroy details
        [HttpPost]
        public ActionResult GetHistoryDataPartial(int HeaderId)
        {
            TVL010_Log objTVL010_Log = new TVL010_Log();
            objTVL010_Log = db.TVL010_Log.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            return PartialView("_GetTravelerHistoryDataPartial", objTVL010_Log);
        }

        [HttpPost]
        public ActionResult GetTravelerHistoryHeaderData(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND tvl010.HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (tvl010.Identification like '%" + param.sSearch
                        + "%' or tvl010.IdentificationNo like '%" + param.sSearch
                        + "%' or tvl010.TravelerNo like '%" + param.sSearch
                        + "%' or tvl010.TravelerRevNo like '%" + param.sSearch
                        + "%' or tvl010.TravelerRevisionDetails like '%" + param.sSearch
                        + "%' or tvl010.TravelerDescription like '%" + param.sSearch
                        + "%' or tvl010.Status like '%" + param.sSearch
                        + "%')";
                }
                var lstResult = db.SP_TVL_GET_HISTORY_HEADER_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                var data = (from uc in lstResult
                            select new[]
                            {
                               Convert.ToString(uc.Identification),
                              Convert.ToString(uc.IdentificationNo),
                               Convert.ToString(uc.TravelerNo),
                                Convert.ToString(uc.TravelerRevNo),
                                 Convert.ToString(uc.TravelerRevisionDetails),
                                 Convert.ToString(uc.TravelerDescription),
                               Convert.ToString(uc.Status),
                               "<a target='_blank' href='"+WebsiteURL+"/TVL/MaintainTraveler/HistoryDetails?id=" + uc.Id + "'><i style='margin-right:10px;' class='fa fa-eye'></i></a>" +
                               Helper.GenerateActionIcon(uc.HeaderId,"Timeline","Timeline","fa fa-clock-o", "ShowTimeline(\"/TVL/MaintainTraveler/ShowHistoryTimeline?HeaderID="+ uc.Id +"\");"),
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult HistoryDetails(int? id)
        {
            TVL010_Log objTVL010_Log = new TVL010_Log();

            if (id > 0)
            {
                objTVL010_Log = db.TVL010_Log.Where(i => i.Id == id).FirstOrDefault();
                objTVL010_Log.ProjectNo = objTVL010_Log.ProjectNo;
                objTVL010_Log.Customer = objTVL010_Log.Customer;
                objTVL010_Log.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objTVL010_Log.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objTVL010_Log.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objTVL010_Log.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                ViewBag.RevNo = "R" + Convert.ToString(objTVL010_Log.TravelerRevNo);

                ViewBag.isReturned = false;
                if (objTVL010_Log.QIReturnedBy != null)
                {
                    ViewBag.isReturned = true;
                    ViewBag.ReturnBy = objTVL010_Log.QIReturnedBy + " - " + Manager.GetUserNameFromPsNo(objTVL010_Log.QIReturnedBy);
                    ViewBag.ReturnDate = objTVL010_Log.QIReturnedOn.Value.ToString("dd/MM/yyyy hh:mm tt");
                    ViewBag.Remarks = objTVL010_Log.QIRemarks;
                }
                else if (objTVL010_Log.WEReturnedBy != null)
                {
                    ViewBag.isReturned = true;
                    ViewBag.ReturnBy = objTVL010_Log.WEReturnedBy + " - " + Manager.GetUserNameFromPsNo(objTVL010_Log.WEReturnedBy);
                    ViewBag.ReturnDate = objTVL010_Log.WEReturnedOn.Value.ToString("dd/MM/yyyy hh:mm tt");
                    ViewBag.Remarks = objTVL010_Log.WERemarks;
                }
                else if (objTVL010_Log.PlanningReturnedBy != null)
                {
                    ViewBag.isReturned = true;
                    ViewBag.ReturnBy = objTVL010_Log.PlanningReturnedBy + " - " + Manager.GetUserNameFromPsNo(objTVL010_Log.PlanningReturnedBy);
                    ViewBag.ReturnDate = objTVL010_Log.PlanningReturnedOn.Value.ToString("dd/MM/yyyy hh:mm tt");
                    ViewBag.Remarks = objTVL010_Log.PlanningRemarks;
                }

                ViewBag.isReleased = false;
                if (objTVL010_Log.PlanningReleasedBy != null)
                {
                    ViewBag.isReleased = true;
                    ViewBag.ReleaseBy = objTVL010_Log.PlanningReleasedBy + " - " + Manager.GetUserNameFromPsNo(objTVL010_Log.PlanningReleasedBy);
                    ViewBag.ReleaseDate = objTVL010_Log.PlanningReleasedOn.Value.ToString("dd/MM/yyyy hh:mm tt");
                }

            }
            ViewBag.ID = objTVL010_Log.Id;
            return View(objTVL010_Log);
        }
        public ActionResult GetTravelerHistoryLinesData(JQueryDataTableParamModel param, string RefId)
        {
            try
            {
                int intRefId = Convert.ToInt32(RefId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(RefId))
                {
                    whereCondition += " AND tvl011.RefId = " + RefId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (tvl011.OperationNo like '%" + param.sSearch
                        + "%' or tvl011.OperationRevisionNo like '%" + param.sSearch
                        + "%' or tvl011.Activity like '%" + param.sSearch
                        + "%' or tvl011.RefDocument like '%" + param.sSearch
                        + "%' or tvl011.RefDocRevNo like '%" + param.sSearch
                        + "%' or tvl011.LTIntervention like '%" + param.sSearch
                        + "%' or tvl011.I1Intervention like '%" + param.sSearch
                        + "%' or tvl011.I2Intervention like '%" + param.sSearch
                        + "%' or tvl011.I3Intervention like '%" + param.sSearch
                        + "%' or tvl011.I4Intervention like '%" + param.sSearch
                        + "%' or tvl011.InspectionRecordRequired like '%" + param.sSearch
                        + "%')";
                }
                var lstResult = db.SP_TVL_GET_HISTORY_LINES_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                var data = (from uc in lstResult
                            select new[]
                            {
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                                uc.OperationNo.Value.ToString("G29"),
                               "R"+Convert.ToString(uc.OperationRevisionNo),
                               Convert.ToString(uc.Activity),
                               Convert.ToString(uc.RefDocument),
                               Convert.ToString(uc.RefDocRevNo),
                               Convert.ToString(Manager.TVLIntervention(uc.LTIntervention)),
                               Convert.ToString(uc.I1Intervention),
                               Convert.ToString(uc.I2Intervention),
                               Convert.ToString(uc.I3Intervention),
                               Convert.ToString(uc.I4Intervention),
                               Convert.ToString(uc.InspectionRecordRequired),
                               Convert.ToString(uc.LineStatus),
                               Helper.GenerateActionIcon(uc.LineId,"Timeline","Timeline","fa fa-clock-o", "ShowTimeline(\"/TVL/MaintainTraveler/ShowHistoryTimeline?HeaderID="+ uc.HeaderId +"&LineId="+ uc.Id +"\");"),
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult GetReturnRemarkHistoryDataPartial(int HeaderId, decimal OperationNo)
        {
            TVL012 objtvl012 = new TVL012();
            objtvl012 = db.TVL012.Where(x => x.HeaderId == HeaderId && x.OperationNo == OperationNo).FirstOrDefault();
            if (objtvl012 != null)
            {
                ViewBag.OperationNo = objtvl012.OperationNo;
                ViewBag.HeaderId = objtvl012.HeaderId;
            }
            else
            {
                ViewBag.OperationNo = null;
                ViewBag.HeaderId = null;
            }
            return PartialView("_GetReturnRemarksHistoryDataPartial", objtvl012);
        }
        [HttpPost]
        public ActionResult GetTravelerTeturnRemarksHistoryHeaderData(JQueryDataTableParamModel param, string Operationno, string HeaderId)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(Operationno))
                {
                    whereCondition += " AND tvl012.OperationNo = " + Operationno + "and tvl012.HeaderId=" + HeaderId;
                }
                else
                {
                    whereCondition += " AND tvl012.OperationNo is null and tvl012.HeaderId is null";
                }
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (tvl012.ReturnRemarks like '%" + param.sSearch
                        + "%' or tvl012.Role like '%" + param.sSearch
                        + "%')";
                }
                var lstResult = db.SP_TVL_GET_HISTORY_ReturnRemarks_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                var data = (from uc in lstResult
                            select new[]
                            {
                               Convert.ToString(uc.LineId),
                              Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.OperationNo),
                                Convert.ToString(uc.ReturnRemarks),
                                Convert.ToString(uc.Role),
                                 Convert.ToString(uc.ReturnedBy),
                               Convert.ToString(Convert.ToDateTime(uc.ReturnedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.ReturnedOn).ToString("dd/MM/yyyy"))

                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Copy

        [HttpPost]
        public ActionResult GetCopyDetails(string Projectno)
        {
            string location = objClsLoginInfo.Location;
            ViewBag.Projectno = Projectno;
            ViewBag.Location = location;
            ViewBag.Travelerno = db.TVL010.Where(i => i.ProjectNo.Equals(Projectno, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(location, StringComparison.OrdinalIgnoreCase)).OrderBy(i => i.TravelerNo).Select(i => new { id = i.TravelerNo }).ToList();
            ViewBag.IdentificationNo = db.TVL010.Where(i => i.ProjectNo.Equals(Projectno, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(location, StringComparison.OrdinalIgnoreCase)).OrderBy(i => i.IdentificationNo).Select(i => new { id = i.IdentificationNo }).ToList();
            return PartialView("_LoadTravelerCopyPartial");
        }

        [HttpPost]
        public ActionResult CopyTravelrwithinProject(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {



                string[] destTraveler = Convert.ToString(fc["txtDesitinationTravelerno"]).Split(',');
                string sourceTraveler = Convert.ToString(fc["ddlTravelerNo"]).Trim();
                string Project = Convert.ToString(fc["ProjectNo"]).Trim();
                List<TVL011> newLstTVL011 = new List<TVL011>();

                foreach (string dProj in destTraveler)
                {
                    if (!string.IsNullOrWhiteSpace(dProj.Trim()))
                    {
                        TVL010 objtvl010 = db.TVL010.Where(m => m.ProjectNo == Project && m.TravelerNo == sourceTraveler).FirstOrDefault();
                        int Headerid = objtvl010.HeaderId;


                        List<TVL011> lstTVL011 = db.TVL011.Where(c => c.HeaderId == Headerid).ToList();
                        TVL010 objtvl = db.TVL010.Where(m => m.ProjectNo == Project && m.TravelerNo == dProj).FirstOrDefault();
                        objtvl.Note1 = objtvl010.Note1;
                        objtvl.Note2 = objtvl010.Note2;
                        objtvl.Note3 = objtvl010.Note3;
                        objtvl.Note4 = objtvl010.Note4;
                        objtvl.Note5 = objtvl010.Note5;
                        objtvl.Note6 = objtvl010.Note6;
                        objtvl.Note7 = objtvl010.Note7;
                        objtvl.Note8 = objtvl010.Note8;
                        objtvl.Note9 = objtvl010.Note9;
                        objtvl.Note10 = objtvl010.Note10;
                        objtvl.Note11 = objtvl010.Note11;
                        objtvl.Note12 = objtvl010.Note12;
                        objtvl.Note13 = objtvl010.Note13;
                        objtvl.Note14 = objtvl010.Note14;
                        objtvl.Note15 = objtvl010.Note15;
                        objtvl.InitiatorRemarks = objtvl010.InitiatorRemarks;
                        db.SaveChanges();


                        TVL011 objline = db.TVL011.Where(m => m.HeaderId == objtvl.HeaderId).FirstOrDefault();
                        if (objline == null)
                        {
                            foreach (TVL011 objTVL011 in lstTVL011)
                            {
                                TVL011 tvl11 = new TVL011();
                                tvl11.HeaderId = objtvl.HeaderId;
                                tvl11.OperationNo = objTVL011.OperationNo;
                                tvl11.OperationRevisionNo = 0;
                                tvl11.Activity = objTVL011.Activity;
                                tvl11.RefDocument = objTVL011.RefDocument;
                                tvl11.RefDocRevNo = objTVL011.RefDocRevNo;
                                tvl11.LTIntervention = objTVL011.LTIntervention;
                                tvl11.I1Intervention = objTVL011.I1Intervention;
                                tvl11.I2Intervention = objTVL011.I2Intervention;
                                tvl11.I3Intervention = objTVL011.I3Intervention;
                                tvl11.I4Intervention = objTVL011.I4Intervention;
                                tvl11.InspectionRecordRequired = objTVL011.InspectionRecordRequired;
                                tvl11.CreatedBy = objClsLoginInfo.UserName;
                                tvl11.CreatedOn = DateTime.Now;
                                tvl11.LineStatus = clsImplementationEnum.TravelerLineStatus.Added.GetStringValue();

                                db.TVL011.Add(tvl11);
                                db.SaveChanges();
                                #region Copy Reference document
                                if (!string.IsNullOrEmpty(tvl11.RefDocument))
                                {
                                    string referenceDoc = "TVL011//" + objTVL011.LineId + "//R" + objTVL011.OperationRevisionNo;
                                    string newfolderPath = "TVL011//" + tvl11.LineId + "//R" + tvl11.OperationRevisionNo;
                                    //(new clsFileUpload()).CopyFolderContentsAsync(referenceDoc, newfolderPath);
                                    Utility.Controllers.FileUploadController _objFUC = new Utility.Controllers.FileUploadController();
                                    _objFUC.CopyDataOnFCSServerAsync(referenceDoc, newfolderPath, referenceDoc, objTVL011.LineId, newfolderPath, tvl11.LineId, DESServices.CommonService.GetUseIPConfig, DESServices.CommonService.objClsLoginInfo.UserName, 0, false);
                                }
                                #endregion
                            }

                        }
                    }
                }


                objResponseMsg.Key = true;
                objResponseMsg.Value = "All Traveler(s) Data copy successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }



        public ActionResult CopyTravelrwithProject(FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string[] DestProject = Convert.ToString(fc["txtDesitinationProjectNo"]).Split(',');
                string SourceProject = Convert.ToString(fc["ProjectSourceNo"]).Trim();
                string Identificationno = Convert.ToString(fc["ddlidentificationno"]).Trim();
                string Travelerno = Convert.ToString(fc["ddlProjectTravelerNo"]).Trim();
                foreach (string dProj in DestProject)
                {
                    if (!string.IsNullOrWhiteSpace(dProj.Trim()))
                    {
                        TVL010 objsourcetvl010 = db.TVL010.Where(m => m.ProjectNo == SourceProject && m.TravelerNo == Travelerno).FirstOrDefault();
                        TVL010 objtvl010 = db.TVL010.Where(m => m.ProjectNo == dProj && m.TravelerNo == Travelerno).FirstOrDefault();
                        TVL001 objtvl001 = db.TVL001.Where(m => m.ProjectNo == dProj).FirstOrDefault();
                        if (objtvl010 == null)
                        {
                            TVL010 tvl010 = new TVL010();
                            tvl010.ProjectNo = dProj;
                            tvl010.BU = objsourcetvl010.BU;
                            tvl010.ProjectDescription = objtvl001.ProjectDescription;
                            tvl010.Customer = objtvl001.Customer;

                            tvl010.Location = objsourcetvl010.Location;
                            tvl010.TravelerRevNo = 0;
                            tvl010.TravelerNo = objsourcetvl010.TravelerNo;
                            tvl010.Identification = objsourcetvl010.Identification;
                            tvl010.IdentificationNo = objsourcetvl010.IdentificationNo;
                            tvl010.TravelerRevisionDetails = objsourcetvl010.TravelerRevisionDetails;
                            tvl010.TravelerDescription = objsourcetvl010.TravelerDescription;
                            tvl010.Status = clsImplementationEnum.TravelerHeaderStatus.Draft.GetStringValue();
                            tvl010.CreatedBy = objClsLoginInfo.UserName;
                            tvl010.CreatedOn = DateTime.Now;
                            tvl010.Note1 = objsourcetvl010.Note1;
                            tvl010.Note2 = objsourcetvl010.Note2;
                            tvl010.Note3 = objsourcetvl010.Note3;
                            tvl010.Note4 = objsourcetvl010.Note4;
                            tvl010.Note5 = objsourcetvl010.Note5;
                            tvl010.Note6 = objsourcetvl010.Note6;
                            tvl010.Note7 = objsourcetvl010.Note7;
                            tvl010.Note8 = objsourcetvl010.Note8;
                            tvl010.Note9 = objsourcetvl010.Note9;
                            tvl010.Note10 = objsourcetvl010.Note10;
                            tvl010.Note11 = objsourcetvl010.Note11;
                            tvl010.Note12 = objsourcetvl010.Note12;
                            tvl010.Note13 = objsourcetvl010.Note13;
                            tvl010.Note14 = objsourcetvl010.Note14;
                            tvl010.Note15 = objsourcetvl010.Note15;

                            db.TVL010.Add(tvl010);
                            db.SaveChanges();
                            var headerid = tvl010.HeaderId;

                            List<TVL011> lsttvl011 = new List<TVL011>();

                            foreach (var tvl011 in objsourcetvl010.TVL011)
                            {
                                lsttvl011.Add(new TVL011
                                {
                                    HeaderId = headerid,
                                    OperationNo = tvl011.OperationNo,
                                    OperationRevisionNo = 0,
                                    Activity = tvl011.Activity,
                                    RefDocument = null,
                                    RefDocRevNo = null,
                                    LTIntervention = null,
                                    I1Intervention = null,
                                    I2Intervention = null,
                                    I3Intervention = null,
                                    I4Intervention = null,
                                    InspectionRecordRequired = tvl011.InspectionRecordRequired,
                                    LineStatus = clsImplementationEnum.TravelerLineStatus.Added.GetStringValue(),
                                    CreatedBy = objClsLoginInfo.UserName,
                                    CreatedOn = DateTime.Now,
                                });
                            }

                            db.TVL011.AddRange(lsttvl011);
                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = "Data copied successfully";
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "Record Already Exist";
                        }
                    }
                }




            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #region Timeline
        public ActionResult ShowTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();

            model.TimelineTitle = "Traveler Timeline";

            if (LineId > 0)
            {
                model.Title = "Traveler Lines";
                TVL011 objTVL011 = db.TVL011.Where(x => x.LineId == LineId).FirstOrDefault();
                model.CreatedBy = objTVL011.CreatedBy != null ? Manager.GetUserNameFromPsNo(objTVL011.CreatedBy) : null;
                model.CreatedOn = objTVL011.CreatedOn;
                model.EditedBy = objTVL011.EditedBy != null ? Manager.GetUserNameFromPsNo(objTVL011.EditedBy) : null;
                model.EditedOn = objTVL011.EditedOn;
            }
            else
            {
                model.Title = "Traveler Header";
                TVL010 objTVL010 = db.TVL010.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.CreatedBy = objTVL010.CreatedBy != null ? Manager.GetUserNameFromPsNo(objTVL010.CreatedBy) : null;
                model.CreatedOn = objTVL010.CreatedOn;
                model.EditedBy = objTVL010.EditedBy != null ? Manager.GetUserNameFromPsNo(objTVL010.EditedBy) : null;
                model.EditedOn = objTVL010.EditedOn;
                model.SubmittedBy = objTVL010.PlanningSubmittedBy != null ? Manager.GetUserNameFromPsNo(objTVL010.PlanningSubmittedBy) : null;
                model.SubmittedOn = objTVL010.PlanningSubmittedOn;
                if (objTVL010.QIReturnedBy != null)
                {
                    model.ReturnedBy = Manager.GetUserNameFromPsNo(objTVL010.QIReturnedBy);
                    model.ReturnedOn = objTVL010.QIReturnedOn;
                }
                else if (objTVL010.WEReturnedBy != null)
                {
                    model.ReturnedBy = Manager.GetUserNameFromPsNo(objTVL010.WEReturnedBy);
                    model.ReturnedOn = objTVL010.WEReturnedOn;
                }
                else if (objTVL010.PlanningReturnedBy != null)
                {
                    model.ReturnedBy = Manager.GetUserNameFromPsNo(objTVL010.PlanningReturnedBy);
                    model.ReturnedOn = objTVL010.PlanningReturnedOn;
                }

                else
                {
                    model.ReturnedBy = null;
                    model.ReturnedOn = null;
                }
                model.WEApprovedBy = objTVL010.WEApprovedBy != null ? Manager.GetUserNameFromPsNo(objTVL010.WEApprovedBy) : null;
                model.WEApprovedOn = objTVL010.WEApprovedOn;
                model.QIApprovedBy = objTVL010.QIApprovedBy != null ? Manager.GetUserNameFromPsNo(objTVL010.QIApprovedBy) : null;
                model.QIApprovedOn = objTVL010.QIApprovedOn;
                model.PlanningApprovedBy = objTVL010.PlanningApprovedBy != null ? Manager.GetUserNameFromPsNo(objTVL010.PlanningApprovedBy) : null;
                model.PlanningApprovedOn = objTVL010.PlanningApprovedOn;
                model.PlanningReleasedBy = objTVL010.PlanningReleasedBy != null ? Manager.GetUserNameFromPsNo(objTVL010.PlanningReleasedBy) : null;
                model.PlanningReleasedOn = objTVL010.PlanningReleasedOn;
                model.CompletedBy = objTVL010.CompletedBy != null ? Manager.GetUserNameFromPsNo(objTVL010.CompletedBy) : null;
                model.CompletedOn = objTVL010.CompletedOn;


            }

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }

        public ActionResult ShowHistoryTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();

            model.TimelineTitle = "Traveler Timeline";

            if (LineId > 0)
            {
                model.Title = "Traveler Lines";
                TVL011_Log objTVL011 = db.TVL011_Log.Where(x => x.Id == LineId).FirstOrDefault();
                model.CreatedBy = objTVL011.CreatedBy != null ? Manager.GetUserNameFromPsNo(objTVL011.CreatedBy) : null;
                model.CreatedOn = objTVL011.CreatedOn;
                model.EditedBy = objTVL011.EditedBy != null ? Manager.GetUserNameFromPsNo(objTVL011.EditedBy) : null;
                model.EditedOn = objTVL011.EditedOn;
            }
            else
            {
                model.Title = "Traveler Header";
                TVL010_Log objTVL010 = db.TVL010_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                model.CreatedBy = objTVL010.CreatedBy != null ? Manager.GetUserNameFromPsNo(objTVL010.CreatedBy) : null;
                model.CreatedOn = objTVL010.CreatedOn;
                model.EditedBy = objTVL010.EditedBy != null ? Manager.GetUserNameFromPsNo(objTVL010.EditedBy) : null;
                model.EditedOn = objTVL010.EditedOn;
                model.SubmittedBy = objTVL010.PlanningSubmittedBy != null ? Manager.GetUserNameFromPsNo(objTVL010.PlanningSubmittedBy) : null;
                model.SubmittedOn = objTVL010.PlanningSubmittedOn;
                if (objTVL010.QIReturnedBy != null)
                {
                    model.ReturnedBy = Manager.GetUserNameFromPsNo(objTVL010.QIReturnedBy);
                    model.ReturnedOn = objTVL010.WEReturnedOn;
                }
                else if (objTVL010.WEReturnedBy != null)
                {
                    model.ReturnedBy = Manager.GetUserNameFromPsNo(objTVL010.WEReturnedBy);
                    model.ReturnedOn = objTVL010.WEReturnedOn;
                }
                else if (objTVL010.PlanningReturnedBy != null)
                {
                    model.ReturnedBy = Manager.GetUserNameFromPsNo(objTVL010.PlanningReturnedBy);
                    model.ReturnedOn = objTVL010.PlanningReturnedOn;
                }

                else
                {
                    model.ReturnedBy = null;
                    model.ReturnedOn = null;
                }
                model.WEApprovedBy = objTVL010.WEApprovedBy != null ? Manager.GetUserNameFromPsNo(objTVL010.WEApprovedBy) : null;
                model.WEApprovedOn = objTVL010.WEApprovedOn;
                model.QIApprovedBy = objTVL010.QIApprovedBy != null ? Manager.GetUserNameFromPsNo(objTVL010.QIApprovedBy) : null;
                model.QIApprovedOn = objTVL010.QIApprovedOn;
                model.PlanningApprovedBy = objTVL010.PlanningApprovedBy != null ? Manager.GetUserNameFromPsNo(objTVL010.PlanningApprovedBy) : null;
                model.PlanningApprovedOn = objTVL010.PlanningApprovedOn;
                model.PlanningReleasedBy = objTVL010.PlanningReleasedBy != null ? Manager.GetUserNameFromPsNo(objTVL010.PlanningReleasedBy) : null;
                model.PlanningReleasedOn = objTVL010.PlanningReleasedOn;
                model.CompletedBy = objTVL010.CompletedBy != null ? Manager.GetUserNameFromPsNo(objTVL010.CompletedBy) : null;
                model.CompletedOn = objTVL010.CompletedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
        #endregion

        #region Common Methods

        [HttpPost]
        public ActionResult GetProjects(string term)
        {

            List<Projects> lstQualityProject = new List<Projects>();
            if (!string.IsNullOrEmpty(term))
            {
                lstQualityProject = db.TVL001.Where(i => i.ProjectNo.ToLower().Contains(term.ToLower())).Select(i => new Projects { Value = i.ProjectNo, projectCode = i.ProjectNo }).Distinct().ToList();
            }
            else
            {
                //lstQualityProject = db.TVL001.Where(i => i.ProjectDescription.ToLower().Select(i => new Projects { Value = i.ProjectNo, projectCode = i.ProjectDescription }).Take(10).Distinct().ToList();
                lstQualityProject = db.TVL001.Select(i => new Projects { Value = i.ProjectNo, projectCode = i.ProjectNo }).Take(10).Distinct().ToList();

            }
            return Json(lstQualityProject, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetTravellerProjectWiseProjectBUDetail(string Prjno)
        {
            TravellerProjectsDetails objProjectsDetails = new TravellerProjectsDetails();
            var project = db.TVL001.Where(i => i.ProjectNo == Prjno).FirstOrDefault();

            objProjectsDetails.ProjectCode = project.ProjectNo;
            objProjectsDetails.projectDescription = project.ProjectDescription;
            //string Travelerbucode = ConfigurationManager.AppSettings["TravelerBUCode"];
            string Travelerbucode = "02";
            var BUDescription = (from a in db.COM002
                                 where a.t_dtyp == 2 && a.t_dimx == Travelerbucode
                                 select new { BUDesc = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();

            objProjectsDetails.BUCode = BUDescription.BUDesc.Split('-')[0].Trim();
            objProjectsDetails.BUDescription = BUDescription.BUDesc;

            objProjectsDetails.LocationCode = objClsLoginInfo.Location;
            objProjectsDetails.LocationDescription = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objClsLoginInfo.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();

            objProjectsDetails.Customer = project.Customer;

            return Json(objProjectsDetails, JsonRequestBehavior.AllowGet);
        }
        public bool isTRAVELERSHeaderExist(string Project, string Identification, string Identificationno, string Traveler)
        {
            return db.TVL010.Any(i => i.Identification.Equals(Identification, StringComparison.OrdinalIgnoreCase) && i.IdentificationNo.Equals(Identificationno, StringComparison.OrdinalIgnoreCase) && i.TravelerNo.Equals(Traveler, StringComparison.OrdinalIgnoreCase) && i.ProjectNo.Equals(Project, StringComparison.OrdinalIgnoreCase));
        }
        public bool Checkstatus(int headerid)
        {
            bool Status = false;
            TVL010 objtvl010 = new TVL010();
            UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();
            objtvl010 = db.TVL010.Where(m => m.HeaderId == headerid).FirstOrDefault();
            if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QI1.GetStringValue() || objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QI2.GetStringValue() || objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QI3.GetStringValue())
            {
                if (objtvl010.Status == clsImplementationEnum.TravelerHeaderStatus.SubmittedToQI.GetStringValue())
                {
                    Status = true;
                }
            }
            else if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.WE1.GetStringValue() || objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.WE2.GetStringValue() || objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.WE3.GetStringValue())
            {
                if (objtvl010.Status == clsImplementationEnum.TravelerHeaderStatus.SubmittedToWE.GetStringValue())
                {
                    Status = true;
                }
            }
            else if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.PLNG1.GetStringValue() || objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.PLNG2.GetStringValue())
            {
                if (objtvl010.Status == clsImplementationEnum.TravelerHeaderStatus.SubmittedToPLANNING.GetStringValue())
                {
                    Status = true;
                }
            }
            else
            {
                Status = false;
            }
            return Status;
        }
        public bool CheckRemarks(int LineId)
        {
            bool Status = false;
            TVL012 objtvl012 = new TVL012();

            TVL011 objtvl011 = db.TVL011.Where(m => m.LineId == LineId).FirstOrDefault();
            if (objtvl011 != null)
            {
                objtvl012 = db.TVL012.Where(m => m.HeaderId == objtvl011.HeaderId && m.OperationNo == objtvl011.OperationNo).FirstOrDefault();
                if (objtvl012 != null)
                {
                    Status = true;
                }
                else
                {
                    Status = false;
                }
            }


            return Status;
        }
        public bool Validtetraveler(int headerid)
        {

            bool Status = false;


            var objtvl011 = db.TVL011.Where(m => m.HeaderId == headerid && (string.IsNullOrEmpty(m.InspectionRecordRequired) || string.IsNullOrEmpty(m.LTIntervention))).ToList();
            if (objtvl011.Count > 0)
            {
                foreach (var item in objtvl011)
                {
                    if (item.LineStatus != clsImplementationEnum.TravelerLineStatus.Deleted.GetStringValue())
                    {
                        if (string.IsNullOrEmpty(item.InspectionRecordRequired) || string.IsNullOrEmpty(item.LTIntervention))
                        {
                            Status = false;
                            break;
                        }
                        else
                        {
                            Status = true;
                        }
                    }
                    else
                    {
                        Status = true;
                    }
                }
            }
            else
            {
                Status = true;
            }



            return Status;
        }
        public UserRoleAccessDetails GetUserAccessRights()
        {
            UserRoleAccessDetails objUserRoleAccessDetails = new UserRoleAccessDetails();

            try
            {
                var role = (from a in db.ATH001
                            join b in db.ATH004 on a.Role equals b.Id
                            where a.Employee.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase)
                            select new { RoleId = a.Role, RoleDesc = b.Role }).ToList();

                if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PLNG1.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.PLNG1.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Initiator.GetStringValue();
                }
                else if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PLNG2.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.PLNG2.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Initiator.GetStringValue();
                }
                else if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.PLNG3.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Initiator.GetStringValue();
                }
                else if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.QI1.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.QI1.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Approver.GetStringValue();
                }

                else if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.QI2.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.QI2.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Approver.GetStringValue();
                }
                else if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.QI3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.QI3.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Approver.GetStringValue();
                }
                else if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.WE1.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.WE1.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Approver.GetStringValue();
                }
                else if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.WE2.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.WE2.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Approver.GetStringValue();
                }
                else if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.WE3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.WE3.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Approver.GetStringValue();
                }

            }
            catch
            {

            }
            return objUserRoleAccessDetails;
        }
        public JsonResult GetTraveler(string sProj, string Travelerno)
        {
            try
            {
                var items = (from li in db.TVL010
                             where (li.ProjectNo == sProj && li.TravelerNo != Travelerno)
                             select new
                             {
                                 id = li.TravelerNo,
                                 text = li.TravelerNo,
                             }).ToList();

                return Json(items, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetAllProjects(string sProj)
        {
            try
            {
                var items = (from li in db.TVL001
                             where li.ProjectNo != sProj
                             select new
                             {
                                 id = li.ProjectNo,
                                 text = li.ProjectNo,
                             }).ToList();

                return Json(items, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        public string GetDecimalFormat(string Operationno)
        {
            string operno = Operationno.Substring(Operationno.Length - 3);
            string operno1 = Operationno.Substring(Operationno.Length - 1);
            string stroperation = string.Empty;
            if (operno == ".00")
            {
                stroperation = Operationno.Replace(operno, "");
            }
            else
            {
                if (operno1 == "0")
                {
                    stroperation = Operationno.Remove(Operationno.Length - 1);
                }
                else
                {
                    stroperation = Operationno;
                }

            }
            return stroperation;
        }
        public string HTMLActionString(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", string onHrefURL = "", bool isDisable = false)
        {
            string htmlControl = "";
            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";
            string onHref = !string.IsNullOrEmpty(onHrefURL) ? "href='" + onHrefURL + "'" : "";

            if (isDisable)
            {
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer; opacity:0.3; margin-right:10px;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";
            }
            else
            {
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer; margin-right:10px;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";



            }

            if (!string.IsNullOrEmpty(onHref))
            {
                htmlControl = "<a " + onHref + " >" + htmlControl + "</a>";
            }

            return htmlControl;
        }
        public string GenerateAutoCompleteOnBlur(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", string onClickMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", string oldValue = "", bool disabled = false)
        {
            string strAutoComplete = string.Empty;
            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "autocomplete form-control";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick=" + onClickMethod + "" : "";
            strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' data-lineid='" + rowId + "' hdElement='" + hdElementId + "' value='" + inputValue + "' data-oldvalue='" + oldValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + onClickEvent + " " + (disabled ? "disabled" : "") + " />";

            return strAutoComplete;
        }
        public string GenerateRemark(int headerId, string columnName, string userAccessRole, string userRole)
        {
            LTF001 objLTF001 = db.LTF001.FirstOrDefault(c => c.HeaderId == headerId);
            bool isEditable = false;
            if (objLTF001 != null)
            {
                if (objLTF001.Status == clsImplementationEnum.LTFPSHeaderStatus.SentForApproval.GetStringValue() && userAccessRole == clsImplementationEnum.UserAccessRole.Approver.GetStringValue() && userRole == clsImplementationEnum.UserRoleName.WE3.GetStringValue())
                    isEditable = true;
                else if (objLTF001.Status == clsImplementationEnum.LTFPSHeaderStatus.Approved.GetStringValue() && objLTF001.WEApprovedBy != null && objLTF001.QCApprovedBy == null && userAccessRole == clsImplementationEnum.UserAccessRole.Approver.GetStringValue() && userRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    isEditable = true;
                else if (objLTF001.Status == clsImplementationEnum.LTFPSHeaderStatus.Approved.GetStringValue() && objLTF001.WEApprovedBy != null && objLTF001.QCApprovedBy != null && objLTF001.CustomerApprovedBy == null && userAccessRole == clsImplementationEnum.UserAccessRole.Approver.GetStringValue() && userRole == clsImplementationEnum.UserRoleName.CR1.GetStringValue())
                    isEditable = true;
                else if (objLTF001.Status == clsImplementationEnum.LTFPSHeaderStatus.Approved.GetStringValue() && objLTF001.WEApprovedBy != null && objLTF001.QCApprovedBy != null && objLTF001.CustomerApprovedBy != null && objLTF001.PMGReleaseBy == null && userAccessRole == clsImplementationEnum.UserAccessRole.Releaser.GetStringValue() && userRole == clsImplementationEnum.UserRoleName.PMG2.GetStringValue())
                    isEditable = true;
                else
                    isEditable = false;
            }

            string remarkValue = (!string.IsNullOrWhiteSpace(objLTF001.WEReturnedBy) ? objLTF001.WEReturnRemarks : (!string.IsNullOrWhiteSpace(objLTF001.QCReturnedBy) ? objLTF001.QCReturnRemarks : (!string.IsNullOrWhiteSpace(objLTF001.CustomerReturnedBy) ? objLTF001.CustomerReturnRemarks : (!string.IsNullOrWhiteSpace(objLTF001.PMGReturnedBy) ? objLTF001.PMGReturnRemarks : ""))));

            if (isEditable)
            {
                return Helper.GenerateTextbox(objLTF001.HeaderId, columnName, remarkValue, "", false, "", "200");
            }
            else
            {
                return remarkValue;
            }

        }

        public class ResponceMsgWithMultiValue : clsHelper.ResponseMsg
        {
            public string pendingTraveler { get; set; }
            public string submittedTraveler { get; set; }
            public bool isIdenticalProjectEditable { get; set; }
        }
        #endregion

        public clsHelper.ResponseMsg InsertUpdateOperation(int headerid)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();

            TVL040 objTVL040 = new TVL040();
            var objTVL010 = db.TVL010.Where(x => x.HeaderId == headerid).FirstOrDefault();
            if (objTVL010 != null)
            {
                List<TVL040> lstAllOperations = db.TVL040.Where(i => i.ProjectNo.Trim() == objTVL010.ProjectNo.Trim()
                                                     && i.BU.Trim() == objTVL010.BU.Trim()
                                                     && i.Location.Trim() == objTVL010.Location.Trim()
                                                     && i.Customer.Trim() == objTVL010.Customer.Trim()
                                                     && i.AssemblyNo.Trim() == objTVL010.IdentificationNo.Trim()
                                                     && i.TravelerNo.Trim() == objTVL010.TravelerNo.Trim()
                                                     && i.IsCompleted != true).ToList();

                if (lstAllOperations.Count > 0)
                {
                    lstAllOperations.ForEach(x =>
                    {
                        x.TravelerRevNo = objTVL010.TravelerRevNo;
                        x.TVL050.ToList().ForEach(y => { y.TravelerRevNo = objTVL010.TravelerRevNo; });
                    });
                    db.SaveChanges();

                }

                string releaseStatus = clsImplementationEnum.TravelerLineStatus.Released.GetStringValue();
                List<TVL040> newObjList = new List<TVL040>();
                List<TVL040> deleteObjList = new List<TVL040>();


                List<int?> lstAssemblyNos = db.TVL040.Where(i => i.ProjectNo.Trim() == objTVL010.ProjectNo.Trim()
                                                 && i.BU.Trim() == objTVL010.BU.Trim()
                                                 && i.Location.Trim() == objTVL010.Location.Trim()
                                                 && i.Customer.Trim() == objTVL010.Customer.Trim()
                                                 && i.AssemblyNo.Trim() == objTVL010.IdentificationNo.Trim()
                                                 && i.TravelerNo.Trim() == objTVL010.TravelerNo.Trim()
                                                 && i.TravelerRevNo == objTVL010.TravelerRevNo
                                                 && i.IsCompleted != true).Select(s => s.SubAssemblyNo).Distinct().ToList();

                foreach (var AssemblyNo in lstAssemblyNos)
                {

                    List<TVL040> lstOperations = db.TVL040.Where(i => i.ProjectNo.Trim() == objTVL010.ProjectNo.Trim()
                                                 && i.BU.Trim() == objTVL010.BU.Trim()
                                                 && i.Location.Trim() == objTVL010.Location.Trim()
                                                 && i.Customer.Trim() == objTVL010.Customer.Trim()
                                                 && i.AssemblyNo.Trim() == objTVL010.IdentificationNo.Trim()
                                                 && i.TravelerNo.Trim() == objTVL010.TravelerNo.Trim()
                                                 && i.TravelerRevNo == objTVL010.TravelerRevNo
                                                 && i.SubAssemblyNo == AssemblyNo).ToList();

                    foreach (var item in objTVL010.TVL011.Where(c => c.LineStatus != releaseStatus).ToList())
                    {
                        // Get latets offered operation
                        //TVL040 objTmTVL040 = lstOperations.Where(c => (c.InspectionStatus != clsImplementationEnum.TravelerInspectionStatus.ReadyToOffer.GetStringValue() && c.InspectionStatus != clsImplementationEnum.TravelerInspectionStatus.Ready_To_Process.GetStringValue() && c.InspectionStatus != clsImplementationEnum.TravelerInspectionStatus.Not_Applicatble.GetStringValue())).OrderByDescending(o => o.OperationNo).FirstOrDefault();
                        //decimal LastOfferedOperation = 0;
                        //if (objTmTVL040 != null)
                        //{
                        //    LastOfferedOperation = objTmTVL040.OperationNo.Value;
                        //}
                        //if (item.OperationNo > LastOfferedOperation)
                        //{
                        TVL040 objTVL040item = lstOperations.Where(c => c.OperationNo == item.OperationNo).FirstOrDefault();
                        if (objTVL040item != null)
                        {
                            if (objTVL040item.InspectionStatus == clsImplementationEnum.TravelerInspectionStatus.ReadyToOffer.GetStringValue() || objTVL040item.InspectionStatus == clsImplementationEnum.TravelerInspectionStatus.Ready_To_Process.GetStringValue() || objTVL040item.InspectionStatus == clsImplementationEnum.TravelerInspectionStatus.Returned.GetStringValue())
                            {
                                if (item.LineStatus == clsImplementationEnum.TravelerLineStatus.Deleted.GetStringValue() && objTVL040item.LnTInspectorResult != clsImplementationEnum.TravelerInspectionStatus.Returned.GetStringValue())
                                {
                                    #region Delete Operation
                                    deleteObjList.Add(objTVL040item);
                                    #endregion
                                }
                                else
                                {
                                    #region Update Operation
                                    objTVL040item.OperationRevisionNo = item.OperationRevisionNo;
                                    objTVL040item.Activity = item.Activity;
                                    objTVL040item.RefDocument = item.RefDocument;
                                    objTVL040item.RefDocRevNo = item.RefDocRevNo;
                                    objTVL040item.LTIntervention = item.LTIntervention;
                                    objTVL040item.I1Intervention = item.I1Intervention;
                                    objTVL040item.I2Intervention = item.I2Intervention;
                                    objTVL040item.I3Intervention = item.I3Intervention;
                                    objTVL040item.I4Intervention = item.I4Intervention;
                                    objTVL040item.InspectionRecordRequired = item.InspectionRecordRequired;
                                    if (item.LTIntervention.Trim().ToLower() == "s")
                                    {
                                        objTVL040item.InspectionStatus = clsImplementationEnum.TravelerInspectionStatus.Ready_To_Process.GetStringValue();
                                    }
                                    objTVL040item.EditedBy = objClsLoginInfo.UserName;
                                    objTVL040item.EditedOn = DateTime.Now;
                                    db.SaveChanges();
                                    #endregion
                                }
                            }
                        }
                        else
                        {
                            if (item.LineStatus != clsImplementationEnum.TravelerLineStatus.Deleted.GetStringValue())
                            {
                                #region Add Operation

                                TVL040 AddobjTVL040 = new TVL040();
                                AddobjTVL040.ProjectNo = objTVL010.ProjectNo;
                                AddobjTVL040.Customer = objTVL010.Customer;
                                AddobjTVL040.TravelerRevNo = objTVL010.TravelerRevNo;
                                AddobjTVL040.TravelerNo = objTVL010.TravelerNo;
                                AddobjTVL040.AssemblyNo = objTVL010.IdentificationNo;
                                AddobjTVL040.SubAssemblyNo = AssemblyNo;
                                AddobjTVL040.BU = objTVL010.BU;
                                AddobjTVL040.Identification = objTVL010.Identification;
                                AddobjTVL040.Location = objTVL010.Location;
                                AddobjTVL040.OperationNo = item.OperationNo;
                                AddobjTVL040.OperationRevisionNo = item.OperationRevisionNo;
                                AddobjTVL040.Activity = item.Activity;
                                AddobjTVL040.RefDocument = item.RefDocument;
                                AddobjTVL040.RefDocRevNo = item.RefDocRevNo;
                                AddobjTVL040.LTIntervention = item.LTIntervention;
                                AddobjTVL040.I1Intervention = item.I1Intervention;
                                AddobjTVL040.I2Intervention = item.I2Intervention;
                                AddobjTVL040.I3Intervention = item.I3Intervention;
                                AddobjTVL040.I4Intervention = item.I4Intervention;
                                AddobjTVL040.InspectionRecordRequired = item.InspectionRecordRequired;
                                if (item.LTIntervention.Trim().ToLower() == "s")
                                {
                                    AddobjTVL040.InspectionStatus = clsImplementationEnum.TravelerInspectionStatus.Ready_To_Process.GetStringValue();
                                }
                                else
                                {
                                    AddobjTVL040.InspectionStatus = clsImplementationEnum.TravelerInspectionStatus.ReadyToOffer.GetStringValue();
                                }
                                AddobjTVL040.itenrationNo = 1;
                                AddobjTVL040.CreatedBy = objClsLoginInfo.UserName;
                                AddobjTVL040.CreatedOn = DateTime.Now;
                                newObjList.Add(AddobjTVL040);
                                #endregion
                            }
                        }
                        //}

                        //foreach (TVL040 Opr in lstOperations)
                        //{

                        //    if (Opr.InspectionStatus == clsImplementationEnum.TravelerInspectionStatus.ReadyToOffer.GetStringValue() || Opr.InspectionStatus == clsImplementationEnum.TravelerInspectionStatus.Ready_To_Process.GetStringValue() || Opr.InspectionStatus == clsImplementationEnum.TravelerInspectionStatus.Returned.GetStringValue())
                        //    {
                        //        if (Opr.OperationNo == item.OperationNo)
                        //        {
                        //            objTVL040 = Opr;
                        //            if (item.LineStatus == clsImplementationEnum.TravelerLineStatus.Deleted.GetStringValue() && Opr.InspectionStatus != clsImplementationEnum.TravelerInspectionStatus.Returned.GetStringValue())
                        //            {
                        //                #region Delete Operation
                        //                db.TVL040.Remove(objTVL040);
                        //                db.SaveChanges();
                        //                #endregion
                        //            }
                        //            else
                        //            {
                        //                #region Update Operation
                        //                objTVL040.OperationRevisionNo = item.OperationRevisionNo;
                        //                objTVL040.Activity = item.Activity;
                        //                objTVL040.RefDocument = item.RefDocument;
                        //                objTVL040.RefDocRevNo = item.RefDocRevNo;
                        //                objTVL040.LTIntervention = item.LTIntervention;
                        //                objTVL040.I1Intervention = item.I1Intervention;
                        //                objTVL040.I2Intervention = item.I2Intervention;
                        //                objTVL040.I3Intervention = item.I3Intervention;
                        //                objTVL040.I4Intervention = item.I4Intervention;
                        //                objTVL040.InspectionRecordRequired = item.InspectionRecordRequired;
                        //                if (item.LTIntervention.Trim().ToLower() == "s")
                        //                {
                        //                    objTVL040.InspectionStatus = clsImplementationEnum.TravelerInspectionStatus.Ready_To_Process.GetStringValue();
                        //                }

                        //                objTVL040.EditedBy = objClsLoginInfo.UserName;
                        //                objTVL040.EditedOn = DateTime.Now;
                        //                db.SaveChanges();
                        //                #endregion
                        //            }
                        //        }
                        //        else
                        //        {
                        //            if (item.LineStatus != clsImplementationEnum.TravelerLineStatus.Deleted.GetStringValue())
                        //            {
                        //                #region Add Operation

                        //                objTVL040 = new TVL040();
                        //                objTVL040.ProjectNo = objTVL010.ProjectNo;
                        //                objTVL040.Customer = objTVL010.Customer;
                        //                objTVL040.TravelerRevNo = objTVL010.TravelerRevNo;
                        //                objTVL040.TravelerNo = objTVL010.TravelerNo;
                        //                objTVL040.AssemblyNo = objTVL010.IdentificationNo;
                        //                objTVL040.SubAssemblyNo = AssemblyNo;
                        //                objTVL040.BU = objTVL010.BU;
                        //                objTVL040.Location = objTVL010.Location;
                        //                objTVL040.OperationNo = item.OperationNo;
                        //                objTVL040.OperationRevisionNo = item.OperationRevisionNo;
                        //                objTVL040.Activity = item.Activity;
                        //                objTVL040.RefDocument = item.RefDocument;
                        //                objTVL040.RefDocRevNo = item.RefDocRevNo;
                        //                objTVL040.LTIntervention = item.LTIntervention;
                        //                objTVL040.I1Intervention = item.I1Intervention;
                        //                objTVL040.I2Intervention = item.I2Intervention;
                        //                objTVL040.I3Intervention = item.I3Intervention;
                        //                objTVL040.I4Intervention = item.I4Intervention;
                        //                objTVL040.InspectionRecordRequired = item.InspectionRecordRequired;
                        //                if (item.LTIntervention.Trim().ToLower() == "s")
                        //                {
                        //                    objTVL040.InspectionStatus = clsImplementationEnum.TravelerInspectionStatus.Ready_To_Process.GetStringValue();
                        //                }
                        //                else
                        //                {
                        //                    objTVL040.InspectionStatus = clsImplementationEnum.TravelerInspectionStatus.ReadyToOffer.GetStringValue();
                        //                }
                        //                objTVL040.itenrationNo = 1;
                        //                objTVL040.CreatedBy = objClsLoginInfo.UserName;
                        //                objTVL040.CreatedOn = DateTime.Now;
                        //                newObjList.Add(objTVL040);
                        //                #endregion
                        //            }
                        //        }
                        //    }
                        //}
                    }
                }
                if (newObjList.Count > 0)
                {
                    db.TVL040.AddRange(newObjList);
                    db.SaveChanges();
                }
                if (deleteObjList.Count > 0)
                {
                    db.TVL040.RemoveRange(deleteObjList);
                    db.SaveChanges();
                }
            }
            return objResponseMsg;
        }

        #region ExcelHelper

        [HttpPost]
        [SessionExpireFilter]
        public ActionResult ImportExcel(HttpPostedFileBase upload, string aProjectDescription, string aProject, string aBU, string aLocation, string aCustomer)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            FilterTravelerModel model = new FilterTravelerModel();

            if (upload != null && !string.IsNullOrWhiteSpace(aProject))
            {
                if (Path.GetExtension(upload.FileName).ToLower() == ".xlsx" || Path.GetExtension(upload.FileName).ToLower() == ".xls")
                {
                    ExcelPackage package = new ExcelPackage(upload.InputStream);
                    ExcelWorksheet excelWorksheet = package.Workbook.Worksheets.First();
                    int rowcount = 1;
                    model.BU = aBU.Split('-')[0].Trim();
                    model.Location = aLocation.Split('-')[0].Trim();
                    DataTable dtHeaderExcel = new DataTable();
                    List<CategoryData> errors = new List<CategoryData>();
                    bool IsErrorInHeader = false;
                    foreach (var firstRowCell in excelWorksheet.Cells[1, 1, 4, 1])
                    {
                        switch (rowcount)
                        {
                            case 1:
                                model.ProjectNo = firstRowCell.Text;
                                break;
                            case 2:
                                model.identification = firstRowCell.Text.Replace("\n", "").Trim();
                                model.identificationno = excelWorksheet.Cells[rowcount, 2, rowcount, 2].Text.Replace("\n", "").Trim();
                                break;
                            case 3:
                                model.TravelerNo = firstRowCell.Text.Replace("\n", "").Trim();
                                model.TravelerRevDetails = excelWorksheet.Cells[rowcount, 2, rowcount, 2].Text.Replace("\n", "").Trim();
                                break;
                            case 4:
                                model.TravelerDetails = firstRowCell.Text;
                                break;
                            default:
                                break;
                        }
                        rowcount++;
                    }
                    #region Validation
                    var lstIdentification = clsImplementationEnum.getTravelerIdentification().ToList();
                    //1. project no
                    if (aProject != model.ProjectNo)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Please enter valid Project No in sheet";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        if (!string.IsNullOrWhiteSpace(model.ProjectNo) && model.ProjectNo.Trim().Length > 12)
                        {
                            errors.Add(new CategoryData
                            {
                                Id = 1,
                                text = "Project No have maximum limit of 12 characters"
                            });
                            IsErrorInHeader = true;
                        }
                    }
                    //2. identification
                    if (!string.IsNullOrWhiteSpace(model.identification))
                    {
                        if (!lstIdentification.Contains(model.identification))
                        {
                            IsErrorInHeader = true;
                            errors.Add(new CategoryData { Id = 2, text = "Identification is not valid" });
                        }
                    }
                    else
                    {
                        IsErrorInHeader = true;
                        errors.Add(new CategoryData { Id = 2, text = "Identification is required" });
                    }

                    //3. identification no
                    if (!string.IsNullOrWhiteSpace(model.identificationno) && model.identification.Length > 50)
                    {
                        IsErrorInHeader = true;
                        errors.Add(new CategoryData { Id = 3, text = "Identification is  have maximum limit of 50 characters" });
                    }
                    if (model.identification != clsImplementationEnum.TravelerIdentificationDropdown.StandardTraveler.GetStringValue())
                    {
                        if (string.IsNullOrWhiteSpace(model.identificationno))
                        {
                            IsErrorInHeader = true;
                            errors.Add(new CategoryData { Id = 3, text = "Identification no is required" });
                        }
                    }
                    //4.traveler no
                    if (string.IsNullOrWhiteSpace(model.TravelerNo))
                    {
                        IsErrorInHeader = true;
                        errors.Add(new CategoryData { Id = 4, text = "Traveler No is required" });
                    }
                    else if (!string.IsNullOrWhiteSpace(model.TravelerNo) && model.TravelerNo.Length > 50)
                    {
                        IsErrorInHeader = true;
                        errors.Add(new CategoryData { Id = 4, text = "Travel no have maximum limit of 50 characters" });
                    }

                    var checkExistingTravelerNo = db.TVL010.FirstOrDefault(x => x.BU == model.BU && x.Location == model.Location && x.Customer == aCustomer && x.ProjectNo == aProject && x.TravelerNo.ToUpper() == model.TravelerNo.ToUpper());
                    if (checkExistingTravelerNo != null)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Traveler No " + model.TravelerNo + " is already exists.";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    //5. Traveler Rev Details
                    if (string.IsNullOrWhiteSpace(model.TravelerRevDetails))
                    {
                        IsErrorInHeader = true;
                        errors.Add(new CategoryData { Id = 5, text = "Traveler Revision No.Details is required" });
                    }
                    else if (!string.IsNullOrWhiteSpace(model.TravelerRevDetails) && model.TravelerRevDetails.Length > 100)
                    {
                        IsErrorInHeader = true;
                        errors.Add(new CategoryData { Id = 5, text = "Travel no have maximum limit of 100 characters" });
                    }
                    //6. Traveler Details
                    if (string.IsNullOrWhiteSpace(model.TravelerRevDetails))
                    {
                        IsErrorInHeader = true;
                        errors.Add(new CategoryData { Id = 6, text = "Traveler Details is required" });
                    }
                    else if (!string.IsNullOrWhiteSpace(model.TravelerRevDetails) && model.TravelerRevDetails.Length > 300)
                    {
                        IsErrorInHeader = true;
                        errors.Add(new CategoryData { Id = 6, text = "Travel no have maximum limit of 300 characters" });
                    }
                    #endregion

                    bool isError;
                    DataSet ds = ToAddLinesandNotes(package, model, out isError);
                    if (!isError && !IsErrorInHeader)
                    {
                        try
                        {
                            //header data
                            TVL010 objTVL010 = new TVL010();
                            objTVL010.ProjectNo = model.ProjectNo;
                            objTVL010.ProjectDescription = aProjectDescription.Trim();
                            objTVL010.Customer = aCustomer;
                            objTVL010.Identification = model.identification;
                            objTVL010.IdentificationNo = model.identificationno;
                            objTVL010.TravelerNo = model.TravelerNo;
                            objTVL010.TravelerRevNo = 0;
                            objTVL010.TravelerRevisionDetails = model.TravelerRevDetails;
                            objTVL010.TravelerDescription = model.TravelerDetails;
                            objTVL010.BU = aBU.Split('-')[0];
                            objTVL010.Location = aLocation.Split('-')[0];
                            objTVL010.Status = clsImplementationEnum.TravelerHeaderStatus.Draft.GetStringValue();
                            objTVL010.CreatedBy = objClsLoginInfo.UserName;
                            objTVL010.CreatedOn = DateTime.Now;
                            db.TVL010.Add(objTVL010);
                            db.SaveChanges();
                            //import Line Data
                            string[] refno = null;
                            List<string> refDoc = new List<string>();
                            List<string> refDocrev = new List<string>();
                            List<TVL011> lstTVL011 = new List<TVL011>();
                            foreach (DataRow item in ds.Tables[0].Rows)
                            {
                                TVL011 objTVL011 = new TVL011();
                                objTVL011.HeaderId = objTVL010.HeaderId;
                                objTVL011.OperationNo = Convert.ToDecimal(item.Field<string>("OPN. NO."));
                                objTVL011.OperationRevisionNo = 0;
                                objTVL011.Activity = item.Field<string>("ACTIVITY");
                                //refDoc = null;
                                //refDocrev = null;
                                if (!string.IsNullOrEmpty(item.Field<string>("REFERENCE DOCUMENT")))
                                {
                                    refno = item.Field<string>("REFERENCE DOCUMENT").ToLower().Split(',').ToArray();


                                    //List<string> refDoc = new List<string>();
                                    // List<string> refDocrev = new List<string>();

                                    var lstexistingRefno = db.TVL002.Where(x => x.ProjectNo == model.ProjectNo && refno.Contains(x.RefDocument)).Distinct().OrderByDescending(x => x.RefDocRevNo).ToList();
                                    foreach (var ite in refno)
                                    {
                                        if (!string.IsNullOrWhiteSpace(ite))
                                        {
                                            var lst = (from i in lstexistingRefno
                                                       where i.RefDocument.ToLower().Trim() == ite.ToLower().Trim()
                                                       orderby i.RefDocRevNo descending
                                                       select new CategoryData { catId = i.RefDocument, CategoryDescription = i.RefDocRevNo.ToString() }).FirstOrDefault();
                                            refDoc.Add(lst.catId);
                                            refDocrev.Add(lst.CategoryDescription);
                                        }
                                    }
                                    if (refDoc.Count() > 0)
                                    {
                                        objTVL011.RefDocument = string.Join(",", refDoc);
                                        objTVL011.RefDocRevNo = string.Join(",", refDocrev);
                                    }
                                }
                                else
                                {
                                    objTVL011.RefDocument = null;
                                    objTVL011.RefDocRevNo = null;
                                }
                                if (string.IsNullOrWhiteSpace(item.Field<string>("L&T INTERVENTION")) || item.Field<string>("L&T INTERVENTION").ToLower() == "empty")
                                {
                                    objTVL011.LTIntervention = "E";
                                }
                                else
                                {
                                    objTVL011.LTIntervention = getIntervention("Traveler Intervention", model.BU, model.Location, item.Field<string>("L&T INTERVENTION"));
                                }
                                //objTVL011.LTIntervention = (!string.IsNullOrWhiteSpace(item.Field<string>("L&T INTERVENTION").ToString()) || item.Field<string>("L&T INTERVENTION").ToLower().ToString() != "empty") ? getIntervention("Traveler Intervention", model.BU, model.Location, item.Field<string>("L&T INTERVENTION")) : getIntervention("Traveler Intervention", model.BU, model.Location, "E");
                                objTVL011.I1Intervention = getIntervention("Traveler TPI Intervention", model.BU, model.Location, item.Field<string>("I1 INTERVENTION"));
                                objTVL011.I2Intervention = getIntervention("Traveler TPI Intervention", model.BU, model.Location, item.Field<string>("I2 INTERVENTION"));
                                objTVL011.I3Intervention = getIntervention("Traveler TPI Intervention", model.BU, model.Location, item.Field<string>("I3 INTERVENTION"));
                                objTVL011.I4Intervention = getIntervention("Traveler TPI Intervention", model.BU, model.Location, item.Field<string>("I4 INTERVENTION"));
                                string yn = item.Field<string>("INSPECTION RECORD REQUIRED").ToLower().Trim();
                                var lstYN = clsImplementationEnum.getyesno().ToList();
                                var inspectionrecord = (from lst in lstYN
                                                        where lst.ToLower().Trim() == yn
                                                        select lst).FirstOrDefault();

                                objTVL011.InspectionRecordRequired = inspectionrecord;
                                objTVL011.LineStatus = clsImplementationEnum.TravelerLineStatus.Added.GetStringValue();
                                objTVL011.CreatedBy = objClsLoginInfo.UserName;
                                objTVL011.CreatedOn = DateTime.Now;
                                lstTVL011.Add(objTVL011);
                                if (!string.IsNullOrEmpty(item.Field<string>("REFERENCE DOCUMENT")))
                                {
                                    if (refDocrev.Count() > 0)
                                    {
                                        refDocrev = new List<string>();
                                    }
                                    if (refDoc.Count() > 0)
                                    {
                                        refDoc = new List<string>();
                                    }
                                }

                            }
                            db.TVL011.AddRange(lstTVL011);
                            db.SaveChanges();

                            #region Copy Reference document
                            foreach (var item in lstTVL011)
                            {
                                if (!string.IsNullOrWhiteSpace(item.RefDocument))
                                {
                                    string[] document = item.RefDocument.Split(',');
                                    string[] refdocrevno = item.RefDocRevNo.Split(',');
                                    int r = 0;
                                    foreach (var doc in document)
                                    {
                                        int revno = Convert.ToInt32(refdocrevno[r]);
                                        var refdocument = db.TVL002.Where(x => x.RefDocument == doc && x.ProjectNo == objTVL010.ProjectNo && x.RefDocRevNo == revno).FirstOrDefault();
                                        string referenceDoc = "TVL002//" + refdocument.HeaderId;
                                        string newfolderPath = "TVL011//" + item.LineId + "//R" + item.OperationRevisionNo;
                                        //(new clsFileUpload()).CopyFolderContentsAsync(referenceDoc, newfolderPath);
                                        Utility.Controllers.FileUploadController _objFUC = new Utility.Controllers.FileUploadController();
                                        _objFUC.CopyDataOnFCSServerAsync(referenceDoc, newfolderPath, referenceDoc, refdocument.HeaderId, newfolderPath, item.LineId, DESServices.CommonService.GetUseIPConfig, DESServices.CommonService.objClsLoginInfo.UserName, 0, false);
                                        r++;
                                    }
                                }
                            }
                            #endregion

                            //import Notes Data
                            int notesCount = 1;
                            TVL010 objUpdateTVL010 = db.TVL010.Where(x => x.HeaderId == objTVL010.HeaderId).FirstOrDefault();
                            foreach (DataRow item in ds.Tables[1].Rows)
                            {
                                string notes = item.Field<string>("NOTES");
                                if (!string.IsNullOrWhiteSpace(notes))
                                {
                                    switch (notesCount)
                                    {
                                        case 1:
                                            objUpdateTVL010.Note1 = notes;
                                            break;
                                        case 2:
                                            objUpdateTVL010.Note2 = notes;
                                            break;
                                        case 3:
                                            objUpdateTVL010.Note3 = notes;
                                            break;
                                        case 4:
                                            objUpdateTVL010.Note4 = notes;
                                            break;
                                        case 5:
                                            objUpdateTVL010.Note5 = notes;
                                            break;
                                        case 6:
                                            objUpdateTVL010.Note6 = notes;
                                            break;
                                        case 7:
                                            objUpdateTVL010.Note7 = notes;
                                            break;
                                        case 8:
                                            objUpdateTVL010.Note8 = notes;
                                            break;
                                        case 9:
                                            objUpdateTVL010.Note9 = notes;
                                            break;
                                        case 10:
                                            objUpdateTVL010.Note10 = notes;
                                            break;
                                        case 11:
                                            objUpdateTVL010.Note11 = notes;
                                            break;
                                        case 12:
                                            objUpdateTVL010.Note12 = notes;
                                            break;
                                        case 13:
                                            objUpdateTVL010.Note13 = notes;
                                            break;
                                        case 14:
                                            objUpdateTVL010.Note14 = notes;
                                            break;
                                        case 15:
                                            objUpdateTVL010.Note15 = notes;
                                            break;
                                        default:
                                            break;
                                    }
                                    notesCount++;
                                }
                                else
                                {
                                    break;
                                }
                            }
                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = "Traveler imported successfully";
                        }
                        catch (Exception ex)
                        {
                            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                        }
                    }
                    else
                    {
                        objResponseMsg = ExportToExcel(ds, model, errors);
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Excel file is not valid";
                }
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please enter valid Project no.";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public clsHelper.ResponceMsgWithFileName ExportToExcel(DataSet ds, FilterTravelerModel model, List<CategoryData> headerError)
        {
            clsHelper.ResponceMsgWithFileName objResponseMsg = new clsHelper.ResponceMsgWithFileName();
            try
            {
                MemoryStream ms = new MemoryStream();
                using (FileStream fs = System.IO.File.OpenRead(Server.MapPath("~/Resources/TRAVELER/Traveler -Error Template.xlsx")))
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(fs))
                    {
                        ExcelWorkbook excelWorkBook = excelPackage.Workbook;
                        ExcelWorksheet excelWorksheet = excelWorkBook.Worksheets.First();
                        excelWorksheet.Cells[1, 1].Value = model.ProjectNo;
                        excelWorksheet.Cells[2, 1].Value = model.identification;
                        excelWorksheet.Cells[2, 2].Value = model.identificationno;
                        excelWorksheet.Cells[3, 1].Value = model.TravelerNo;
                        excelWorksheet.Cells[3, 2].Value = model.TravelerRevDetails;
                        excelWorksheet.Cells[4, 1].Value = model.TravelerDetails;

                        foreach (var header in headerError)
                        {
                            switch (header.Id)
                            {
                                case 1:
                                    excelWorksheet.Cells[1, 2].Value = header.text;
                                    excelWorksheet.Cells[1, 2].Style.Font.Color.SetColor(Color.Red);
                                    break;
                                case 2:
                                    excelWorksheet.Cells[2, 3].Value = header.text;
                                    excelWorksheet.Cells[2, 3].Style.Font.Color.SetColor(Color.Red);
                                    break;
                                case 3:
                                    excelWorksheet.Cells[2, 4].Value = header.text;
                                    excelWorksheet.Cells[2, 4].Style.Font.Color.SetColor(Color.Red);
                                    break;
                                case 4:
                                    excelWorksheet.Cells[3, 3].Value = header.text;
                                    excelWorksheet.Cells[3, 3].Style.Font.Color.SetColor(Color.Red);
                                    break;
                                case 5:
                                    excelWorksheet.Cells[3, 4].Value = header.text;
                                    excelWorksheet.Cells[3, 4].Style.Font.Color.SetColor(Color.Red);
                                    break;
                                case 6:
                                    excelWorksheet.Cells[4, 2].Value = header.text;
                                    excelWorksheet.Cells[4, 2].Style.Font.Color.SetColor(Color.Red);
                                    break;
                                default:
                                    break;
                            }
                        }

                        int i = 6;
                        int notes = 0;

                        foreach (DataRow item in ds.Tables[0].Rows)
                        {
                            //operation no.
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(9)))
                            {
                                excelWorksheet.Cells[i, 1].Value = item.Field<string>(0) + " (Note: " + item.Field<string>(9).ToString() + " )";
                                excelWorksheet.Cells[i, 1].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 1].Value = item.Field<string>(0); }

                            //activity
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(10)))
                            {
                                excelWorksheet.Cells[i, 2].Value = item.Field<string>(1) + " (Note: " + item.Field<string>(10).ToString() + " )";
                                excelWorksheet.Cells[i, 2].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 2].Value = item.Field<string>(1); }

                            //ref document
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(11)))
                            {
                                excelWorksheet.Cells[i, 3].Value = item.Field<string>(2) + " (Note: " + item.Field<string>(11).ToString() + " )";
                                excelWorksheet.Cells[i, 3].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 3].Value = item.Field<string>(2); }

                            //LT interevntion
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(12)))
                            {
                                excelWorksheet.Cells[i, 4].Value = item.Field<string>(3) + " (Note: " + item.Field<string>(12).ToString() + " )";
                                excelWorksheet.Cells[i, 4].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 4].Value = item.Field<string>(3); }

                            //I1 interevntion
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(13)))
                            {
                                excelWorksheet.Cells[i, 5].Value = item.Field<string>(4) + " (Note: " + item.Field<string>(13).ToString() + " )";
                                excelWorksheet.Cells[i, 5].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 5].Value = item.Field<string>(4); }

                            //I2 interevntion
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(14)))
                            {
                                excelWorksheet.Cells[i, 6].Value = item.Field<string>(5) + " (Note: " + item.Field<string>(14).ToString() + " )";
                                excelWorksheet.Cells[i, 6].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 6].Value = item.Field<string>(5); }

                            //I3 interevntion
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(15)))
                            {
                                excelWorksheet.Cells[i, 7].Value = item.Field<string>(6) + " (Note: " + item.Field<string>(15).ToString() + " )";
                                excelWorksheet.Cells[i, 7].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 7].Value = item.Field<string>(6); }

                            //I4 interevntion
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(16)))
                            {
                                excelWorksheet.Cells[i, 8].Value = item.Field<string>(8) + " (Note: " + item.Field<string>(16).ToString() + " )";
                                excelWorksheet.Cells[i, 8].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 8].Value = item.Field<string>(7); }

                            //inspection record
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(17)))
                            {
                                excelWorksheet.Cells[i, 9].Value = item.Field<string>(8) + " (Note: " + item.Field<string>(17).ToString() + " )";
                                excelWorksheet.Cells[i, 9].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 9].Value = item.Field<string>(8); }

                            notes = i + 2;
                            i++;
                        }

                        using (ExcelRange Rng = excelWorksheet.Cells[notes, 1, notes, 1])
                        {
                            Rng.Value = "NOTES";
                            Rng.Style.Font.Bold = true;
                        }

                        notes = notes + 1;
                        foreach (DataRow item in ds.Tables[1].Rows)
                        {
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(1)))
                            {
                                excelWorksheet.Cells[notes, 1].Value = item.Field<string>(0) + " (Note: " + item.Field<string>(1) + " )";
                                excelWorksheet.Cells[notes, 1].Style.Font.Color.SetColor(Color.Red);
                            }
                            else
                            { excelWorksheet.Cells[notes, 1].Value = item.Field<string>(0); }
                            notes++;
                        }

                        excelPackage.SaveAs(ms);
                    }
                }

                ms.Position = 0;

                string fileName;
                string excelFilePath = Helper.ExcelFilePath("ErrorList", out fileName);

                Byte[] bin = ms.ToArray();
                System.IO.File.WriteAllBytes(excelFilePath, bin);

                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error";
                objResponseMsg.FileName = fileName;
                return objResponseMsg;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
        }

        public string getIntervention(string category, string bu, string Location, string code)
        {
            string intervention = (from glb2 in db.GLB002
                                   join glb1 in db.GLB001 on glb2.Category equals glb1.Id
                                   where glb1.Category == category && glb2.BU.Trim() == bu.Trim() && glb2.Location.Trim() == Location.Trim() && glb2.Code.ToLower().Trim() == code.ToLower().Trim()
                                   select glb2.Code).FirstOrDefault();
            return intervention;
        }

        public DataSet ToAddLinesandNotes(ExcelPackage package, FilterTravelerModel model, out bool isError)
        {
            ExcelWorksheet excelWorksheet = package.Workbook.Worksheets.First();
            DataTable dtLinesExcel = new DataTable();
            DataTable dtNotesExcel = new DataTable();
            DataTable dtHeaderExcel = new DataTable();
            isError = false;
            int notesIndex = 0;
            try
            {
                #region Validation for Header
                #endregion

                #region Read Lines data from excel 
                foreach (var firstRowCell in excelWorksheet.Cells[5, 1, 5, excelWorksheet.Dimension.End.Column])
                {
                    dtLinesExcel.Columns.Add(firstRowCell.Text);
                }

                for (var rowNumber = 6; rowNumber <= excelWorksheet.Dimension.End.Row; rowNumber++)
                {
                    var row = excelWorksheet.Cells[rowNumber, 1, rowNumber, excelWorksheet.Dimension.End.Column];
                    var newRow = dtLinesExcel.NewRow();
                    if (excelWorksheet.Cells[rowNumber, 1].Value != null
                       || excelWorksheet.Cells[rowNumber, 2].Value != null
                       || excelWorksheet.Cells[rowNumber, 3].Value != null
                       )
                    {
                        foreach (var cell in row)
                        {
                            newRow[cell.Start.Column - 1] = cell.Text;
                        }
                    }
                    else
                    {
                        notesIndex = rowNumber + 1;
                        break;
                    }

                    dtLinesExcel.Rows.Add(newRow);
                }
                #endregion

                #region Validation for Lines
                dtLinesExcel.Columns.Add("OperationNoErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("ActivityErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("RefDocumentErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("LTInterventionErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("I1InterventionErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("I2InterventionErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("I3InterventionErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("I4InterventionErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("InspectionRecordRequiredErrorMsg", typeof(string));

                List<string> lstInterventionTypeValues = (from glb2 in db.GLB002
                                                          join glb1 in db.GLB001 on glb2.Category equals glb1.Id
                                                          where glb1.Category == "Traveler Intervention" && glb2.BU.Trim() == model.BU.Trim() && glb2.Location.Trim() == model.Location.Trim()
                                                          select glb2.Code.ToLower()).ToList();
                List<string> lstTPIInterventionTypeValues = (from glb2 in db.GLB002
                                                             join glb1 in db.GLB001 on glb2.Category equals glb1.Id
                                                             where glb1.Category == "Traveler TPI Intervention" && glb2.BU.Trim() == model.BU.Trim() && glb2.Location.Trim() == model.Location.Trim()
                                                             select glb2.Code.ToLower()).ToList();
                var lstYN = clsImplementationEnum.getyesno().ToList();
                List<string> lstYesNo = (from lst in lstYN
                                         select lst.ToString().ToLower()).ToList();

                foreach (DataRow item in dtLinesExcel.Rows)
                {
                    string errorMessage = string.Empty;
                    decimal opNo;
                    if (!decimal.TryParse(item.Field<string>("OPN. NO."), out opNo))
                    {
                        item["OperationNoErrorMsg"] = item.Field<string>("OPN. NO.") + " is Not Valid, Please Enter Only Numeric Value";
                        isError = true;
                    }
                    if (string.IsNullOrWhiteSpace(item.Field<string>("ACTIVITY")))
                    {
                        item["ActivityErrorMsg"] = "Activity is Required";
                        isError = true;
                    }
                    else
                    {
                        if (item.Field<string>("ACTIVITY").Trim().Length > 1000)
                        {
                            item["ActivityErrorMsg"] = "Activity have maximum limit of 1000 characters";
                            isError = true;
                        }
                    }

                    //  string refno = item.Field<string>("REFERENCE DOCUMENT");
                    if (!string.IsNullOrEmpty(item.Field<string>("REFERENCE DOCUMENT")))
                    {
                        string[] refno = item.Field<string>("REFERENCE DOCUMENT").ToLower().Split(',').ToArray();

                        var lstexistingRefno = db.TVL002.Where(x => x.ProjectNo == model.ProjectNo).Select(x => x.RefDocument.ToLower()).ToList();
                        List<string> lstInvalidRef = new List<string>();
                        foreach (var ite in refno)
                        {
                            if (!string.IsNullOrWhiteSpace(ite))
                            {
                                if (!lstexistingRefno.Contains(ite.ToLower()))
                                {
                                    lstInvalidRef.Add(ite);
                                    isError = true;
                                }
                            }
                        }
                        if (lstInvalidRef.Count() > 0)
                        {
                            isError = true;
                            item["RefDocumentErrorMsg"] = string.Join(",", lstInvalidRef) + " does not belong to the project";

                        }
                        if (item.Field<string>("REFERENCE DOCUMENT").Trim().Length > 1000)
                        {
                            if (lstInvalidRef.Count() > 0)
                            {
                                item["RefDocumentErrorMsg"] = "Reference Document maximum limit of 1000 characters" + " & " + item["RefDocumentErrorMsg"];
                            }
                            else
                            {
                                item["RefDocumentErrorMsg"] = "Reference Document maximum limit of 1000 characters";
                            }
                            isError = true;
                        }
                    }
                    string lntintervention = string.Empty;
                    if (string.IsNullOrWhiteSpace(item.Field<string>("L&T INTERVENTION")) || item.Field<string>("L&T INTERVENTION").ToLower() == "empty")
                    {
                        lntintervention = "E";
                    }
                    else
                    {
                        lntintervention = item.Field<string>("L&T INTERVENTION");
                    }
                    if (!string.IsNullOrWhiteSpace(lntintervention) && !lstInterventionTypeValues.Contains(lntintervention.ToLower()))
                    {
                        item["LTInterventionErrorMsg"] = lntintervention + " is not valid";
                        isError = true;
                    }

                    if (!string.IsNullOrWhiteSpace(item.Field<string>("I1 INTERVENTION")) && !lstTPIInterventionTypeValues.Contains(item.Field<string>("I1 INTERVENTION").ToLower().Trim()))
                    {
                        item["I1InterventionErrorMsg"] = item.Field<string>("I1 INTERVENTION") + " is not valid";
                        isError = true;
                    }
                    if (!string.IsNullOrWhiteSpace(item.Field<string>("I2 INTERVENTION")) && !lstTPIInterventionTypeValues.Contains(item.Field<string>("I2 INTERVENTION").ToLower().Trim()))
                    {
                        item["I2InterventionErrorMsg"] = item.Field<string>("I2 INTERVENTION") + " is not valid";
                        isError = true;
                    }
                    if (!string.IsNullOrWhiteSpace(item.Field<string>("I3 INTERVENTION")) && !lstTPIInterventionTypeValues.Contains(item.Field<string>("I3 INTERVENTION").ToLower().Trim()))
                    {
                        item["I3InterventionErrorMsg"] = item.Field<string>("I3 INTERVENTION") + " is not valid";
                        isError = true;
                    }
                    if (!string.IsNullOrWhiteSpace(item.Field<string>("I4 INTERVENTION")) && !lstTPIInterventionTypeValues.Contains(item.Field<string>("I4 INTERVENTION").ToLower().Trim()))
                    {
                        item["I4InterventionErrorMsg"] = item.Field<string>("I4 INTERVENTION") + " is not valid";
                        isError = true;
                    }
                    if (string.IsNullOrWhiteSpace(item.Field<string>("INSPECTION RECORD REQUIRED")) && string.IsNullOrWhiteSpace(item.Field<string>("INSPECTION RECORD REQUIRED")))
                    {
                        item["InspectionRecordRequiredErrorMsg"] = "Inspection Record Required is Required";
                        isError = true;
                    }
                    else
                    {
                        if (!lstYesNo.Contains(item.Field<string>("INSPECTION RECORD REQUIRED").ToLower().Trim()))
                        {
                            item["InspectionRecordRequiredErrorMsg"] = item.Field<string>("INSPECTION RECORD REQUIRED") + " is not valid";
                            isError = true;
                        }
                    }
                }
                #endregion

                #region Read notes from excel
                foreach (var firstRowCell in excelWorksheet.Cells[notesIndex, 1, notesIndex, 1])
                {
                    dtNotesExcel.Columns.Add(firstRowCell.Text);
                }
                var notesrow = notesIndex + 1;

                for (var rowNumber = notesrow; rowNumber <= excelWorksheet.Dimension.End.Row; rowNumber++)
                {
                    var row = excelWorksheet.Cells[rowNumber, 1, rowNumber, 1];
                    var notesRow = dtNotesExcel.NewRow();
                    bool isBlankNotes = false;
                    foreach (var cell in row)
                    {
                        if (!string.IsNullOrWhiteSpace(cell.Text))
                        {
                            notesRow[cell.Start.Column - 1] = cell.Text;
                        }
                        else
                        { isBlankNotes = true; }
                    }
                    if (isBlankNotes)
                    { break; }
                    dtNotesExcel.Rows.Add(notesRow);
                }
                #endregion

                #region Validation for Lines
                dtNotesExcel.Columns.Add("NotesErrorMsg", typeof(string));
                foreach (DataRow item in dtNotesExcel.Rows)
                {
                    if (!string.IsNullOrWhiteSpace(item.Field<string>("NOTES")) && item.Field<string>("NOTES").Length > 300)
                    {
                        item["NotesErrorMsg"] = "Notes having maximum limit of 300 characters";
                        isError = true;
                    }
                }
                #endregion

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            DataSet ds = new DataSet();
            ds.Tables.Add(dtLinesExcel);
            ds.Tables.Add(dtNotesExcel);

            return ds;
        }

        #endregion
    }
}