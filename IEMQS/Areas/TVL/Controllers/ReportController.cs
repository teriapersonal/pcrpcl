﻿using IEMQS.Areas.IPI.Models;
using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.TVL.Controllers
{
    public class ReportController : clsBase
    {
        // GET: TVL/Report
        public ActionResult Index()
        {
            ViewBag.Title = "Traveler Printout";
            return View();
        }

        [HttpPost]
        public JsonResult GetAllProjectNo(string term)
        {
            try
            {
                List<CategoryData> lstProject = new List<CategoryData>();
                if (!string.IsNullOrWhiteSpace(term))
                {
                    lstProject = (from lst in db.TVL001
                                  where lst.ProjectNo.Trim().Contains(term.Trim())
                                  select new CategoryData { id = lst.ProjectNo, text = lst.ProjectNo }).Distinct().ToList();
                }
                else
                {
                    lstProject = (from lst in db.TVL001
                                  select new CategoryData { id = lst.ProjectNo, text = lst.ProjectNo }).Distinct().Take(10).ToList();
                }
                return Json(lstProject, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult GetAllTravelerNo(string term, string projectno)
        {
            try
            {
                List<CategoryData> lstTravelerNo = new List<CategoryData>();
                var lstlocation = Manager.GetUserwiseLocation(objClsLoginInfo.UserName);
                var lstBu = Manager.GetUserwiseBU(objClsLoginInfo.UserName);
                if (!string.IsNullOrWhiteSpace(term))
                {
                    lstTravelerNo = (from lst in db.TVL010_Log
                                     where lst.ProjectNo.Trim() == projectno.Trim() && lst.TravelerNo.Trim().Contains(term.Trim()) && lstlocation.Contains(lst.Location) && lstBu.Contains(lst.BU)
                                     select new CategoryData { id = lst.TravelerNo, text = lst.TravelerNo }).Distinct().ToList();
                }
                else
                {
                    lstTravelerNo = (from lst in db.TVL010_Log
                                     where lst.ProjectNo.Trim() == projectno.Trim() && lstlocation.Contains(lst.Location) && lstBu.Contains(lst.BU)
                                     select new CategoryData { id = lst.TravelerNo, text = lst.TravelerNo }).Distinct().ToList();
                }
                return Json(lstTravelerNo, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetAllTravelerRevNo(string term, string projectno, string travelerno)
        {
            try
            {
                List<CategoryData> lstTravelerRevNo = new List<CategoryData>();
                if (term.ToLower() != "r" && !string.IsNullOrWhiteSpace(term))
                {
                    term = !term.ToLower().Contains("r") ? term : term.ToLower().Replace("r", "");
                    if (!string.IsNullOrWhiteSpace(term))
                    {
                        lstTravelerRevNo = (from lst in db.TVL010_Log
                                            where lst.ProjectNo.Trim() == projectno.Trim() && lst.TravelerNo.Trim() == travelerno.Trim()
                                                      && (lst.TravelerRevNo.ToString().Contains(term) || lst.TravelerRevNo.ToString().Contains(term))
                                            select new CategoryData { id = lst.TravelerRevNo.ToString(), text = "R" + lst.TravelerRevNo.ToString() }).Distinct().ToList();
                    }
                }
                else
                {
                    lstTravelerRevNo = (from lst in db.TVL010_Log
                                        where lst.ProjectNo.Trim() == projectno.Trim() && lst.TravelerNo.Trim() == travelerno.Trim()
                                        select new CategoryData { id = lst.TravelerRevNo.ToString(), text = "R" + lst.TravelerRevNo.ToString() }).Distinct().ToList();
                }
                return Json(lstTravelerRevNo, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetAllIdentification(string term, string projectno, string travelerno, int rev)
        {
            try
            {
                List<CategoryData> lstTravelerRevNo = new List<CategoryData>();
                if (!string.IsNullOrWhiteSpace(term))
                {
                    lstTravelerRevNo = (from lst in db.TVL040
                                        where lst.ProjectNo.Trim() == projectno.Trim() && lst.TravelerNo.Trim() == travelerno.Trim() && lst.TravelerRevNo == rev
                                        select new CategoryData { id =lst.AssemblyNo, text = lst.AssemblyNo }).Distinct().ToList();
                }
                else
                {
                    lstTravelerRevNo = (from lst in db.TVL040
                                        where lst.ProjectNo.Trim() == projectno.Trim() && lst.TravelerNo.Trim() == travelerno.Trim() && lst.TravelerRevNo == rev
                                        select new CategoryData { id = lst.AssemblyNo, text = lst.AssemblyNo }).Distinct().ToList();
                }
                return Json(lstTravelerRevNo, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetAllSubassembly(string term, string projectno, string travelerno, int rev, string identification)
        {
            try
            {
                List<CategoryData> lstTravelerRevNo = new List<CategoryData>();
                if (!string.IsNullOrWhiteSpace(term))
                {
                    lstTravelerRevNo = (from lst in db.TVL040
                                        where lst.ProjectNo.Trim() == projectno.Trim()
                                                && lst.TravelerNo.Trim() == travelerno.Trim()
                                                && lst.TravelerRevNo == rev
                                                && lst.AssemblyNo == identification
                                                && lst.SubAssemblyNo.ToString().Contains(term)
                                        select new CategoryData { id = lst.SubAssemblyNo.ToString(), text = lst.SubAssemblyNo.ToString() }).Distinct().ToList();
                }
                else
                {
                    lstTravelerRevNo = (from lst in db.TVL040
                                        where lst.ProjectNo.Trim() == projectno.Trim()
                                                && lst.TravelerNo.Trim() == travelerno.Trim()
                                                && lst.TravelerRevNo == rev
                                                && lst.AssemblyNo == identification
                                        select new CategoryData { id = lst.SubAssemblyNo.ToString(), text = lst.SubAssemblyNo.ToString() }).Distinct().ToList();
                }
                return Json(lstTravelerRevNo, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
    }
}