﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Web;
using System.Web.Mvc;


namespace IEMQS.Areas.FKMS.Controllers
{
    public class FKMSIntegrationController : clsBase
    {
        [SessionExpireFilter]
        public ActionResult Login(string RedirectTo)
        {

            ViewBag.CurUser = objClsLoginInfo.UserName.ToString();
            Session["Psno"] = ViewBag.CurUser;
            ViewBag.CurUser = "http://phzpdshpdev:8085/FKMS_IEMQS/Login.aspx?CurUser=" + ViewBag.CurUser;
            ViewBag.RedirectTo = "/FKMS/FKMSIntegration/"+RedirectTo;

            return View();
        }
        // GET: FKMS/FKMSIntegration
        [SessionExpireFilter]
        public ActionResult Index()
        {
            if (Session["Psno"] == null)
            {
                return RedirectToAction("Login", new { RedirectTo = "Index" });
            }
            return View();
        }
        [SessionExpireFilter]
        public ActionResult Create()
        {
            if (Session["Psno"] == null)
            {
                return RedirectToAction("Login", new { RedirectTo = "Create" });
            }
            return View();
        }

        [SessionExpireFilter]
        public ActionResult CreateProj()
        {
            if (Session["Psno"] == null)
            {
                return RedirectToAction("Login", new { RedirectTo = "CreateProj" });
            }
            return View();
        }

        [SessionExpireFilter]
        public ActionResult Role()
        {
            if (Session["Psno"] == null)
            {
                return RedirectToAction("Login", new { RedirectTo = "Role" });
            }
            return View();
        }

        [SessionExpireFilter]
        public ActionResult EMMaster()
        {
            if (Session["Psno"] == null)
            {
                return RedirectToAction("Login", new { RedirectTo = "EMMaster" });
            }
            return View();
        }

        [SessionExpireFilter]
        public ActionResult OpenChildKit(string FKNo = "")
        {
           
            ViewBag.FKNo = "http://phzpdshpdev:8085/FKMS_IEMQS/CreateFullKit.aspx?FKNo=" + FKNo;
            return View();
        }

        [SessionExpireFilter]
        public ActionResult CurrentRole()
        {
            if (Session["Psno"] == null)
            {
                return RedirectToAction("Login", new { RedirectTo = "CurrentRole" });
            }
            return View();
        }

        [SessionExpireFilter]
        public ActionResult FetchPCL()
        {
            if (Session["Psno"] == null)
            {
                return RedirectToAction("Login", new { RedirectTo = "FetchPCL" });
            }
            return View();
        }

    }
}