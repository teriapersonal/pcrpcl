﻿using IEMQS.Areas.NDE.Models;
using IEMQS.FKMSAllocationService;
using IEMQS.Models;
using IEMQS.PLMBOMService;
using IEMQSImplementation;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationEnum;

namespace IEMQS.Areas.FKMS.Controllers
{
    public class MaintainFRController : clsBase
    {
        int StartFindNo = 9599;
        HedPLMBaaNProjectCreationWebServiceService plmWebService = new HedPLMBaaNProjectCreationWebServiceService();
        string allocateInsert = "Insert";
        string allocateDelete = "Delete";

        #region Utility
        public bool IsFindNoReachLimit(int FindNo)
        {
            if (FindNo >= 9301) { return true; } else { return false; }
        }

        [HttpPost]
        public ActionResult GetProjectList(string term)
        {
            var Projects = db.FKM101.ToList();
            if (Projects != null && Projects.Count > 0)
            {
                var lstProjects = Projects.Select(x => new BULocWiseCategoryModel { CatDesc = Manager.GetProjectAndDescription(x.Project), CatID = x.Project }).Distinct().ToList();
                lstProjects = lstProjects.Where(x => x.CatDesc != null && x.CatDesc.ToLower().Contains(term.ToLower())).ToList();
                return Json(lstProjects, JsonRequestBehavior.AllowGet);
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetProjectDetails(string project)
        {
            projectDetails prjDetails = new projectDetails();

            try
            {
                //check project exist in PDIN
                var productType = db.PDN001.Where(x => x.Project == project).GroupBy(u => u.Product).ToDictionary(g => g.Key, g => g.Max(item => item.IssueNo)).FirstOrDefault();
                if (productType.Key != null)
                {
                    //check project exist in FKMS
                    var objFKM101 = db.FKM101.Where(x => x.Project == project).FirstOrDefault();
                    if (objFKM101 != null)
                    {
                        prjDetails.Customer = objFKM101.Customer;
                        prjDetails.CustomerDescription = Manager.GetCustomerName(objFKM101.Customer);
                        prjDetails.CDD = objFKM101.CDD.HasValue ? objFKM101.CDD.Value.ToShortDateString() : null;
                        prjDetails.ZeroDate = objFKM101.ZeroDate.HasValue ? objFKM101.ZeroDate.Value.ToShortDateString() : null;
                        prjDetails.ProductType = objFKM101.ProductType;
                        prjDetails.Key = true;
                    }
                    else
                    {
                        prjDetails.Key = true;
                        prjDetails.Value = string.Format(clsImplementationMessage.CommonMessages.ProjectNotExistInFKMS.ToString(), project);
                    }
                }
                else
                {
                    prjDetails.Key = false;
                    prjDetails.Value = string.Format(clsImplementationMessage.CommonMessages.ProjectNotExistInPDN.ToString(), project);
                }
                return Json(prjDetails, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }
        public void InsertLinesEntry(FKM114 objFKM114)
        {
            List<FXR004> objFXR004 = db.FXR004.Where(x => x.Product == objFKM114.ProductType).ToList();
            List<FKM111> objFKM111 = new List<FKM111>();

            if (objFXR004 != null && objFXR004.Count > 0)
            {
                int fxrSrNo = 1;
                foreach (var item in objFXR004)
                {
                    objFKM111.Add(new FKM111
                    {
                        RefHeaderId = objFKM114.HeaderId,
                        Project = objFKM114.Project,
                        RevNo = objFKM114.RevNo,
                        FixtureName = item.FixtureName,
                        FXRSrNo = fxrSrNo,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now,
                        ParentId = 0,
                        IsManual = false,
                        IsSubcontractFormSubmit = false,
                        Status = clsImplementationEnum.FRStatus.Draft.GetStringValue()
                    });
                    fxrSrNo += 1;
                }
                db.FKM111.AddRange(objFKM111);
                db.SaveChanges();
            }
        }
        public UserRoleAccessDetails GetUserAccessRights()
        {
            UserRoleAccessDetails objUserRoleAccessDetails = new UserRoleAccessDetails();

            try
            {
                var role = (from a in db.ATH001
                            join b in db.ATH004 on a.Role equals b.Id
                            where a.Employee.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase)
                            select new { RoleId = a.Role, RoleDesc = b.Role }).ToList();

                if (role.Where(i => (i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue(), StringComparison.OrdinalIgnoreCase) || i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PMG3.GetStringValue(), StringComparison.OrdinalIgnoreCase))).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.PLNG3.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Initiator.GetStringValue();
                }
                else if (role.Where(i => (i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PLNG1.GetStringValue(), StringComparison.OrdinalIgnoreCase) || i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PLNG2.GetStringValue(), StringComparison.OrdinalIgnoreCase) || i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PMG1.GetStringValue(), StringComparison.OrdinalIgnoreCase) || i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PMG2.GetStringValue(), StringComparison.OrdinalIgnoreCase))).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.PLNG2.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Approver.GetStringValue();
                }
                else if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.FMG3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.FMG3.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Releaser.GetStringValue();
                }
                else if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.SHOP.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.SHOP.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Initiator.GetStringValue();
                }
                else if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PROD3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.PROD3.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Initiator.GetStringValue();
                }
                else if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PMG3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.PMG3.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Initiator.GetStringValue();
                }
            }
            catch
            {

            }
            return objUserRoleAccessDetails;
        }
        public int FindChildCount(List<FKM111> objFKM111List, int RefLineId)
        {
            return objFKM111List.Where(x => x.ParentId == RefLineId).Count();
        }
        public int FindChildCountFMG(List<FKM111_Log> objFKM111List, int RefLineId)
        {
            return objFKM111List.Where(x => x.ParentId == RefLineId).Count();
        }

        public int FixtureAllocateCount(List<FKM119> objFKM119List, int RefLineId)
        {
            return objFKM119List.Where(x => x.RefLineId == RefLineId).Count();
        }

        public string getFXRData(string headerID, string lineID)
        {
            string val = "";
            int LineID = Int32.Parse(lineID);
            int HeaderID = Int32.Parse(headerID);
            FKM111 objFKM111 = db.FKM111.Where(x => x.LineId == LineID && x.RefHeaderId == HeaderID).FirstOrDefault();
            if (objFKM111 != null)
            {
                string fixtureName = objFKM111.FixtureName;
                string project = objFKM111.Project;
                FKM111 obj1FKM111 = db.FKM111.Where(x => x.FixtureName == fixtureName && x.Project == project && x.QtyofFixture != null).FirstOrDefault();
                if (obj1FKM111 != null)
                {
                    val = obj1FKM111.QtyofFixture.ToString();
                }
                else
                {
                    val = "";
                }
            }
            else
            {
                val = "";
            }

            return val;
        }

        public decimal CalculateWeight(FKM111 objFKM111, decimal Density, decimal CalculateWt)
        {
            decimal calculatedWeight = 0;

            if (objFKM111.Category.ToLower() == clsImplementationEnum.FXRCategory.Sheet.GetStringValue().ToLower())
            {
                calculatedWeight = Convert.ToDecimal((((((objFKM111.LengthOD * objFKM111.WidthOD) * objFKM111.Thickness) * objFKM111.Qty) / 1000000) * Density));
            }
            else if (objFKM111.Category.ToLower() == clsImplementationEnum.FXRCategory.Structural.GetStringValue().ToLower() || objFKM111.Category.ToLower() == clsImplementationEnum.FXRCategory.Pipe_Tube.GetStringValue().ToLower())
            {
                //calculatedWeight = Convert.ToDecimal(((((objFKM111.LengthOD / 1000) * objFKM111.Qty) * CalculateWt) * QTyFXR));
                calculatedWeight = Convert.ToDecimal((((objFKM111.LengthOD / 1000) * objFKM111.Qty) * CalculateWt));
            }
            else if (objFKM111.Category.ToLower() == clsImplementationEnum.FXRCategory.Bar.GetStringValue().ToLower())
            {
                //calculatedWeight = Convert.ToDecimal((((((((Convert.ToDecimal(3.141592654) * (objFKM111.LengthOD - objFKM111.Thickness)) * objFKM111.WidthOD) * objFKM111.Thickness) * objFKM111.Qty) / 1000000) * Density) * QTyFXR));
                calculatedWeight = Convert.ToDecimal(((((((Convert.ToDecimal(3.141592654) * (objFKM111.LengthOD - objFKM111.Thickness)) * objFKM111.WidthOD) * objFKM111.Thickness) * objFKM111.Qty) / 1000000) * Density));
            }
            else if (objFKM111.Category.ToLower() == clsImplementationEnum.FXRCategory.Plate.GetStringValue().ToLower())
            {
                calculatedWeight = Convert.ToDecimal((((((objFKM111.LengthOD != null ? objFKM111.LengthOD.Value : 0) * (objFKM111.WidthOD != null ? objFKM111.WidthOD.Value : 0) * (objFKM111.Thickness != null ? objFKM111.Thickness.Value : 0)) / 1000000) * Density) * (objFKM111.Qty != null ? objFKM111.Qty.Value : 0)));
            }
            else if (objFKM111.Category.ToLower() == clsImplementationEnum.FXRCategory.Rod.GetStringValue().ToLower())
            {
                //calculatedWeight = Convert.ToDecimal((Convert.ToDecimal((3.141592654 / 4)) * (objFKM111.Thickness * objFKM111.Thickness * objFKM111.LengthOD * objFKM111.Qty * Density * QTyFXR)) / (1000000));
                calculatedWeight = Convert.ToDecimal((Convert.ToDecimal((3.141592654 / 4)) * (objFKM111.Thickness * objFKM111.Thickness * objFKM111.LengthOD * objFKM111.Qty * Density)) / (1000000));
            }
            else if (objFKM111.Category.ToLower() == clsImplementationEnum.FXRCategory.Wire_Rod.GetStringValue().ToLower()) //same formula as area formula
            {
                //calculatedArea = Convert.ToDecimal((((((((Convert.ToDecimal(3.141592654 / 4) * objFKM111.Thickness) * objFKM111.Thickness) * objFKM111.LengthOD) * objFKM111.Qty) * Density) / 1000000) * QTyFXR));
                calculatedWeight = Convert.ToDecimal(((((((Convert.ToDecimal(3.141592654 / 4) * objFKM111.Thickness) * objFKM111.Thickness) * objFKM111.LengthOD) * objFKM111.Qty) * Density) / 1000000));
            }

            return Math.Round(calculatedWeight, 2);
        }

        public decimal CalculateArea(FKM111 objFKM111, decimal Density, decimal CalculateWt)
        {
            decimal calculatedArea = 0;

            if (objFKM111.Category.ToLower() == clsImplementationEnum.FXRCategory.Structural.GetStringValue().ToLower() || objFKM111.Category.ToLower() == clsImplementationEnum.FXRCategory.Pipe_Tube.GetStringValue().ToLower() || objFKM111.Category.ToLower() == clsImplementationEnum.FXRCategory.Rod.GetStringValue().ToLower() || objFKM111.Category.ToLower() == clsImplementationEnum.FXRCategory.Bar.GetStringValue().ToLower())
            {
                //calculatedArea = Convert.ToDecimal((((objFKM111.Qty * objFKM111.LengthOD) * QTyFXR) / 1000));
                calculatedArea = Convert.ToDecimal(((objFKM111.Qty * objFKM111.LengthOD) / 1000));
            }
            else if (objFKM111.Category.ToLower() == clsImplementationEnum.FXRCategory.Sheet.GetStringValue().ToLower())
            {
                calculatedArea = Convert.ToDecimal((((objFKM111.LengthOD * objFKM111.WidthOD) * objFKM111.Qty) / 1000000));
            }
            else if (objFKM111.Category.ToLower() == clsImplementationEnum.FXRCategory.Plate.GetStringValue().ToLower())
            {
                //calculatedArea = Convert.ToDecimal(((((objFKM111.LengthOD * objFKM111.WidthOD) * objFKM111.Qty) * QTyFXR) / 1000000));
                calculatedArea = Convert.ToDecimal((((objFKM111.LengthOD * objFKM111.WidthOD) * objFKM111.Qty) / 1000000));
            }
            else if (objFKM111.Category.ToLower() == clsImplementationEnum.FXRCategory.Wire_Rod.GetStringValue().ToLower())
            {
                //calculatedArea = Convert.ToDecimal((((((((Convert.ToDecimal(3.141592654 / 4) * objFKM111.Thickness) * objFKM111.Thickness) * objFKM111.LengthOD) * objFKM111.Qty) * Density) / 1000000) * QTyFXR));
                calculatedArea = Convert.ToDecimal(((((((Convert.ToDecimal(3.141592654 / 4) * objFKM111.Thickness) * objFKM111.Thickness) * objFKM111.LengthOD) * objFKM111.Qty) * Density) / 1000000));
            }
            else if (objFKM111.Category.ToLower() == clsImplementationEnum.FXRCategory.Fastner.GetStringValue().ToLower())
            {
                //calculatedArea = Convert.ToDecimal((objFKM111.Qty * QTyFXR));
                calculatedArea = Convert.ToDecimal((objFKM111.Qty));
            }

            return Math.Round(calculatedArea, 2);
        }

        public void MaterialCalculationForSingleQuantity(FKM111 objFKM111, List<FKM115> lstFKM115, decimal Density, decimal CalculateWt, string action)
        {
            int QTyFXR = 1;
            if (action.ToLower() == clsImplementationEnum.Actions.add.GetStringValue().ToLower())
            {
                int maxItemNo = 0;
                var lstData = db.FKM115.Where(x => x.ParentId == objFKM111.ParentId && x.RefHeaderId == objFKM111.RefHeaderId).ToList();
                if (lstData != null && lstData.Count > 0)
                {
                    maxItemNo = lstData.Count;
                }
                for (int i = 1; i <= objFKM111.QtyofFixture; i++)
                {
                    FKM115 objFKM115 = new FKM115();

                    #region fill FKM115 object
                    objFKM115.LineId = objFKM111.LineId;
                    objFKM115.QtyofFixture = QTyFXR;
                    objFKM115.DescriptionofItem = objFKM111.DescriptionofItem;
                    objFKM115.ItemCode = objFKM111.ItemCode;
                    objFKM115.ItemCategory = objFKM111.ItemCategory;
                    objFKM115.Category = objFKM111.Category;
                    objFKM115.Material = objFKM111.Material;
                    objFKM115.MaterialType = objFKM111.MaterialType;
                    objFKM115.LengthOD = objFKM111.LengthOD;
                    objFKM115.WidthOD = objFKM111.WidthOD;
                    objFKM115.Thickness = objFKM111.Thickness;
                    objFKM115.Qty = objFKM111.Qty;
                    objFKM115.Wt = CalculateWeight(objFKM111, Density, CalculateWt);
                    objFKM115.Area = CalculateArea(objFKM111, Density, CalculateWt);
                    objFKM115.Unit = objFKM111.Unit;
                    objFKM115.ReUse = objFKM111.ReUse;
                    objFKM115.Subcontracting = objFKM111.Subcontracting;
                    objFKM115.ReqWt = objFKM111.ReUse ? 0 : objFKM115.Wt;
                    objFKM115.ReqArea = objFKM111.ReUse ? 0 : objFKM115.Area;
                    objFKM115.Unit2 = objFKM111.Unit2;
                    objFKM115.FixRequiredDate = objFKM111.FixRequiredDate;
                    objFKM115.MaterialReqDate = objFKM111.MaterialReqDate;
                    objFKM115.TotalMaterialRequirement = objFKM111.TotalMaterialRequirement;
                    objFKM115.FreshMaterialRequirement = objFKM111.FreshMaterialRequirement;
                    objFKM115.ReUsePercent = objFKM111.ReUsePercent;
                    objFKM115.BudgetedMaterial = objFKM111.BudgetedMaterial;
                    objFKM115.RequiredMaterial = objFKM111.RequiredMaterial;
                    objFKM115.ReuseofMaterial = objFKM111.ReuseofMaterial;
                    objFKM115.EstimatedMaterial = objFKM111.EstimatedMaterial;
                    objFKM115.StructuralType = objFKM111.StructuralType;
                    objFKM115.Size = objFKM111.Size;
                    objFKM115.PipeNormalBore = objFKM111.PipeNormalBore;
                    objFKM115.PipeSchedule = objFKM111.PipeSchedule;
                    objFKM115.CreatedBy = objFKM111.CreatedBy;
                    objFKM115.CreatedOn = objFKM111.CreatedOn;
                    objFKM115.EditedBy = objFKM111.EditedBy;
                    objFKM115.EditedOn = objFKM111.EditedOn;
                    objFKM115.ParentId = objFKM111.ParentId;
                    objFKM115.Project = objFKM111.Project;
                    objFKM115.FXRSrNo = objFKM111.FXRSrNo;
                    objFKM115.DocNo = objFKM111.DocNo;
                    objFKM115.FixtureName = objFKM111.FixtureName;
                    objFKM115.RevNo = objFKM111.RevNo;
                    objFKM115.IsManual = objFKM111.IsManual;
                    objFKM115.RefHeaderId = objFKM111.RefHeaderId;
                    #endregion

                    lstFKM115.Add(objFKM115);
                }
            }
            else
            {
                if (lstFKM115 != null && lstFKM115.Count > 0)
                {
                    foreach (var item in lstFKM115)
                    {
                        #region edit FKM115 object
                        item.LineId = objFKM111.LineId;
                        item.DescriptionofItem = objFKM111.DescriptionofItem;
                        item.ItemCode = objFKM111.ItemCode;
                        item.ItemCategory = objFKM111.ItemCategory;
                        item.Category = objFKM111.Category;
                        item.Material = objFKM111.Material;
                        item.MaterialType = objFKM111.MaterialType;
                        item.LengthOD = objFKM111.LengthOD;
                        item.WidthOD = objFKM111.WidthOD;
                        item.Thickness = objFKM111.Thickness;
                        item.Qty = objFKM111.Qty;
                        item.Wt = CalculateWeight(objFKM111, Density, CalculateWt);
                        item.Area = CalculateArea(objFKM111, Density, CalculateWt);
                        item.Unit = objFKM111.Unit;
                        item.ReUse = objFKM111.ReUse;
                        item.Subcontracting = objFKM111.Subcontracting;
                        item.ReqWt = objFKM111.ReUse ? 0 : item.Wt;
                        item.ReqArea = objFKM111.ReUse ? 0 : item.Area;
                        item.Unit2 = objFKM111.Unit2;
                        item.FixRequiredDate = objFKM111.FixRequiredDate;
                        item.MaterialReqDate = objFKM111.MaterialReqDate;
                        item.TotalMaterialRequirement = objFKM111.TotalMaterialRequirement;
                        item.FreshMaterialRequirement = objFKM111.FreshMaterialRequirement;
                        item.ReUsePercent = objFKM111.ReUsePercent;
                        item.BudgetedMaterial = objFKM111.BudgetedMaterial;
                        item.RequiredMaterial = objFKM111.RequiredMaterial;
                        item.ReuseofMaterial = objFKM111.ReuseofMaterial;
                        item.EstimatedMaterial = objFKM111.EstimatedMaterial;
                        item.StructuralType = objFKM111.StructuralType;
                        item.Size = objFKM111.Size;
                        item.PipeNormalBore = objFKM111.PipeNormalBore;
                        item.PipeSchedule = objFKM111.PipeSchedule;
                        item.EditedBy = objFKM111.EditedBy;
                        item.EditedOn = objFKM111.EditedOn;
                        #endregion
                    }
                }
            }
        }

        [NonAction]
        public static string GenerateHTMLCheckboxWithEvent(int rowId, string columnName, bool columnValue = false, string onClickMethod = "", bool isEnabled = true, string ClassName = "", bool isDisabled = false, string check = "")
        {
            string htmlControl = "";
            string inputID = columnName + "" + rowId.ToString();
            string inputValue = columnValue ? "Checked='Checked'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";
            string cssClass = !string.IsNullOrEmpty(ClassName) ? "class='" + ClassName + "'" : "";

            if (isDisabled)
            {
                if (check != string.Empty && check != null)
                {
                    htmlControl = "<input type='checkbox' id='" + inputID + "' " + inputValue + " name='" + inputID + "' colname='" + columnName + "' disabled='disabled " + cssClass + " />";
                }
                else
                {
                    htmlControl = "<input type='checkbox' id='" + inputID + "' name='" + inputID + "' colname='" + columnName + "' disabled='disabled " + cssClass + " />";
                }
            }
            else
            {
                if (isEnabled)
                {
                    htmlControl = "<input type='checkbox' id='" + inputID + "' " + inputValue + " name='" + inputID + "' colname='" + columnName + "' " + onClickEvent + " " + (!isEnabled ? "disabled='disabled'" : "") + " " + cssClass + " />";
                }
                else
                {
                    htmlControl = columnValue ? "" : "";
                }
            }
            return htmlControl;
        }

        [HttpPost]
        public JsonResult GetFixturesItems(int HeaderId, string term = "")
        {
            List<AutoCompleteModel> lstFixtureNo = new List<AutoCompleteModel>();
            var lstFKM111 = db.FKM111.ToList();

            string approved = clsImplementationEnum.FRStatus.Approved.GetStringValue();

            clsManager objManager = new clsManager();
            lstFixtureNo = lstFKM111.Where(x => x.RefHeaderId != HeaderId && x.ParentId == 0 && x.Status == approved && !x.ReUse).Select(x => new AutoCompleteModel { Text = objManager.GetFixtureNo(x.Project, x.FXRSrNo) + " - " + x.FixtureName, Value = x.LineId.ToString() }).ToList();
            if (!string.IsNullOrWhiteSpace(term))
            {
                lstFixtureNo = (from u in lstFixtureNo
                                where u.Text.Trim().ToLower().Contains(term.Trim().ToLower())
                                select u).ToList();
            }
            return Json(lstFixtureNo, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult CheckSubContractingForm(int headerid)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            bool isAllFormSubmit = db.FKM111.Where(x => x.RefHeaderId == headerid && x.ParentId == 0 && x.IsSubcontractFormSubmit == false).Any();
            if (isAllFormSubmit)
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.SubContarctingFormError;
            }
            else
            {
                objResponseMsg.Key = true;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetItemsList(string term = "")
        {
            //var lstItems = (List<LNItems>)TempData["lstItems"];
            //dynamic filterLstItem = lstItems.Take(10).ToList();
            //if (!string.IsNullOrWhiteSpace(term))
            //{
            //    filterLstItem = lstItems.Where(x => x.Item.Contains(term) || x.Description.Contains(term)).Take(10).ToList();
            //}
            //TempData["lstItems"] = lstItems;

            var filterLstItem = db.SP_FKMS_FR_GET_LN_ITEM_LIST(term).ToList();
            return Json(filterLstItem, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Header

        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult Index()
        {
            ViewBag.Title = "Maintain Fixture Requirement";
            ViewBag.IndexType = WPPIndexType.maintain.GetStringValue();
            ViewBag.UserRole = UserRoleName.PLNG3.GetStringValue();  //ViewBag.UserRole = GetUserRole();
            return View();
        }

        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult ApproverIndex()
        {
            ViewBag.Title = "Approve Fixture Requirement";
            ViewBag.IndexType = WPPIndexType.approve.GetStringValue();
            ViewBag.UserRole = UserRoleName.PLNG2.GetStringValue();//ViewBag.UserRole = GetUserRole();            
            return View("Index");
        }

        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult ShopIndex()
        {
            ViewBag.Title = "Execute Fixture Requirement";
            ViewBag.IndexType = WPPIndexType.release.GetStringValue();
            ViewBag.UserRole = UserRoleName.SHOP.GetStringValue();
            return View("Index");
        }

        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult FKMIndex()
        {
            ViewBag.Title = "Execute Fixture Requirement";
            ViewBag.IndexType = WPPIndexType.releaseFKM.GetStringValue();
            ViewBag.UserRole = UserRoleName.FMG3.GetStringValue();
            return View("Index");
        }

        [HttpPost]
        public ActionResult GetHeaderGridDataPartial(string status, string title, string indextype, bool IsDepartment = false)
        {
            ViewBag.Status = status;
            ViewBag.GridTitle = title;
            ViewBag.IndexDataFor = indextype;
            ViewBag.isRoleFKM3 = objClsLoginInfo.ListRoles.Contains(clsImplementationEnum.UserRoleName.FMG3.GetStringValue());

            //ViewBag.UserRole = GetUserRole();
            if (indextype.ToLower() == WPPIndexType.release.GetStringValue().ToLower())
            {
                ViewBag.UserRole = UserRoleName.SHOP.GetStringValue();
                ViewBag.urlForm = WPPIndexType.release.GetStringValue();
                return PartialView("_GetFixturelineGridDataPartial");
            }
            if (indextype.ToLower() == WPPIndexType.releaseFKM.GetStringValue().ToLower())
            {
                ViewBag.UserRole = UserRoleName.FMG3.GetStringValue();
                ViewBag.urlForm = WPPIndexType.release.GetStringValue();
                if (IsDepartment)
                    return RedirectToAction("GetHeaderGridDataPartialExecuteFixture", "MaintainDeptFR", new { status = status, title = title, indextype = indextype });
                else
                    return PartialView("_GetFixturelineGridDataPartial");
             
            }
            else
                return PartialView("_GetHeaderGridDataPartial");
        }

        public ActionResult LoadFRHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string status = param.Status;

                string whereCondition = "1=1";

                string indextype = param.Department;
                if (string.IsNullOrWhiteSpace(indextype))
                {
                    indextype = clsImplementationEnum.WPPIndexType.maintain.GetStringValue();
                }

                string role = param.Roles; //string role = GetUserRole();

                // Changes as per Obs#16641

                if (status.ToLower() == "pending")
                {
                    if (role == UserRoleName.PLNG3.GetStringValue() || role == UserRoleName.PMG3.GetStringValue())
                    {
                        whereCondition += " and (LineStatus in ('" + FRStatus.Returned.GetStringValue() + "','" + FRStatus.Draft.GetStringValue() + "'))";
                    }
                    else if (role == UserRoleName.PLNG1.GetStringValue() || role == UserRoleName.PLNG2.GetStringValue() || role == UserRoleName.PMG1.GetStringValue() || role == UserRoleName.PMG2.GetStringValue())
                    {
                        whereCondition += " and LineStatus in ('" + FRStatus.SentForApproval.GetStringValue() + "')";
                    }
                }
                //else
                //{
                //    if (role == UserRoleName.PLNG1.GetStringValue() || role == UserRoleName.PLNG2.GetStringValue() || role == UserRoleName.PMG1.GetStringValue() || role == UserRoleName.PMG2.GetStringValue())
                //    {
                //        whereCondition += " and ApprovedBy='" + objClsLoginInfo.UserName + "'";
                //    }
                //}

                if (role == UserRoleName.FMG3.GetStringValue())
                {
                    whereCondition += " and FixMfg='" + objClsLoginInfo.UserName + "' and LineStatus in ('" + FRStatus.Approved.GetStringValue() + "')";
                }

                string[] columnName = { "Project", "Customer", "ZeroDate", "CDD", "Status", "RevNo", "CreatedBy", "CONVERT(nvarchar(20),CreatedOn,103)" };

                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstPam = db.SP_FKMS_FR_GETHEADERDETAILS(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                var res = (from h in lstPam
                           select new[] {
                               Convert.ToString(h.HeaderId),
                               Convert.ToString(h.Project),
                               Convert.ToString(h.Customer),
                               Convert.ToString(h.ZeroDate),
                               Convert.ToString(h.CDD),
                               Convert.ToString("R"+h.RevNo),
                               //Convert.ToString(h.Status),
                               Convert.ToString(h.CreatedBy),
                               Convert.ToString(h.CreatedOn),
                               Convert.ToString(CheckDateFilledByFixMfg(h.HeaderId,0,GridType.HEADER.GetStringValue(),role)),
                               "<center>"+ Helper.GenerateActionIcon(h.HeaderId, "View", "View Detail", "fa fa-eye", "", WebsiteURL + "/FKMS/MaintainFR/FRDetails/"+h.HeaderId +"?urlForm="+indextype,false) +"</center>"
                    }).ToList();
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    whereCondition = whereCondition,
                    strSortOrder = strSortOrder,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public bool CheckDateFilledByFixMfg(int HeaderId, int RefLineId, string GridType, string UserRole)
        {
            bool result = true;
            if (UserRole == UserRoleName.FMG3.GetStringValue())
            {
                string approvedStatus = clsImplementationEnum.FRStatus.Approved.GetStringValue();
                string completed = clsImplementationEnum.FRMaterialDeliveryStatus.Completed.GetStringValue();

                var completedFixtureList = (from a in db.FKM111
                                            join b in db.FKM118 on a.LineId equals b.RefLineId
                                            where (HeaderId > 0 ? a.RefHeaderId == HeaderId : a.RefHeaderId == a.RefHeaderId) && a.ParentId == 0 && a.FixMfg == objClsLoginInfo.UserName && a.Status == approvedStatus && !a.ReUse && b.DeliverStatus == completed
                                            select a.LineId).Distinct().ToList();

                var objFKM116List = (from a in db.FKM111
                                     join b in db.FKM116 on a.LineId equals b.RefLineId
                                     join c in db.FKM118 on a.LineId equals c.RefLineId
                                     where (HeaderId > 0 ? a.RefHeaderId == HeaderId : a.RefHeaderId == a.RefHeaderId) && a.FixMfg == objClsLoginInfo.UserName && a.Status == approvedStatus && !a.ReUse && !completedFixtureList.Contains(a.LineId)
                                     select b).Distinct().ToList();

                if (objFKM116List.Count > 0)
                {
                    int count = 0;
                    if (GridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                    {
                        count = (from a in objFKM116List
                                 where string.IsNullOrEmpty(a.AllocatedContractor) || a.TentativeJobStartDate == null || a.WorkCompletioncommittedbyLEMF == null
                                 select a).Count();

                        if (count <= 0)
                        {
                            count = (from a in objFKM116List
                                     where a.IsViewByFixMFg == null || a.IsViewByFixMFg == false
                                     select a).Count();
                        }
                    }
                    else
                    {
                        count = (from a in objFKM116List
                                 where (string.IsNullOrEmpty(a.AllocatedContractor) || a.TentativeJobStartDate == null || a.WorkCompletioncommittedbyLEMF == null) && a.RefLineId == RefLineId
                                 select a).Count();

                        if (count <= 0)
                        {
                            count = (from a in objFKM116List
                                     where (a.IsViewByFixMFg == null || a.IsViewByFixMFg == false) && a.RefLineId == RefLineId
                                     select a).Count();
                        }
                    }

                    if (count > 0)
                    {
                        result = false;
                    }
                }
            }
            return result;
        }

        [SessionExpireFilter, AllowAnonymous]
        public ActionResult FRDetails(int? id, string urlForm = "")
        {
            FKM114 objFKM114 = null;
            int? HeaderId = id;
            string rolePLNG1 = UserRoleName.PLNG1.GetStringValue();
            string rolePLNG2 = UserRoleName.PLNG2.GetStringValue();
            string rolePLNG3 = UserRoleName.PLNG3.GetStringValue();
            string roleSHOP = UserRoleName.SHOP.GetStringValue();
            string roleFKM = UserRoleName.FMG3.GetStringValue();
            string rolePMG3 = UserRoleName.PMG3.GetStringValue();
            string rolePMG1 = UserRoleName.PMG1.GetStringValue();
            string rolePMG2 = UserRoleName.PMG2.GetStringValue();

            ViewBag.QRCodeURL = db.CONFIG.Where(w => w.Key == "QRCodeURL").Select(s => s.Value).FirstOrDefault();
            var lstFKM101 = db.FKM101.ToList();
            //var lstObjApprover = Manager.GetApproverList(rolePLNG2, objClsLoginInfo.Location, objClsLoginInfo.UserName, string.Empty).ToList();

            //UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();
            //ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            //ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            if (lstFKM101 != null && lstFKM101.Count > 0)
            {
                ViewBag.lstProjects = lstFKM101.Select(x => new BULocWiseCategoryModel { CatDesc = Manager.GetProjectAndDescription(x.Project), CatID = x.Project }).Distinct().ToList();
            }

            List<ApproverModel> listApprovers = new List<ApproverModel>();
            string[] approverRoles = new string[] { rolePLNG1, rolePLNG2, rolePMG1, rolePMG2 };
            for (int i = 0; i < approverRoles.Length; i++)
            {
                List<ApproverModel> list = Manager.GetApproverList(approverRoles[i].ToString(), objClsLoginInfo.Location, objClsLoginInfo.UserName, string.Empty).ToList();
                if (list.Count > 0)
                    listApprovers.AddRange(list);
            }
            ViewBag.lstApprovers = (from a in listApprovers
                                    group a by new
                                    {
                                        a.Code,
                                        a.Name
                                    } into b
                                    select new BULocWiseCategoryModel()
                                    {
                                        CatDesc = b.Key.Name,
                                        CatID = b.Key.Code
                                    }).ToList();

            //if (lstObjApprover != null && lstObjApprover.Count > 0)
            //{
            //    ViewBag.lstApprovers = Manager.GetApproverList(rolePLNG2, objClsLoginInfo.Location, objClsLoginInfo.UserName, string.Empty).Select(x => new BULocWiseCategoryModel { CatDesc = x.Name, CatID = x.Code }).ToList();
            //}

            string Heading = string.Empty;

            if (HeaderId > 0)
            {
                objFKM114 = db.FKM114.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                ViewBag.Project = Manager.GetProjectAndDescription(objFKM114.Project);
                ViewBag.Customer = Manager.GetCustomerName(objFKM114.Customer);
                ViewBag.Approver = Manager.GetPsidandDescription(objFKM114.ApprovedBy);
                ViewBag.ZeroDate = objFKM114.ZeroDate.HasValue ? objFKM114.ZeroDate.Value.ToString("dd/MM/yyyy") : string.Empty;
                ViewBag.CDD = objFKM114.CDD.HasValue ? objFKM114.CDD.Value.ToString("dd/MM/yyyy") : string.Empty;
            }
            else
            {
                objFKM114 = new FKM114();
                objFKM114.RevNo = 0;
                objFKM114.Status = FRStatus.Draft.GetStringValue();
            }

            var roleList = (from a in db.ATH001
                            join b in db.ATH004 on a.Role equals b.Id
                            where a.Employee.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase)
                            select new { RoleId = a.Role, RoleDesc = b.Role }).ToList();

            ViewBag.urlForm = urlForm;
            if (urlForm == WPPIndexType.approve.GetStringValue())
            {
                if (roleList.Any(x => x.RoleDesc == rolePLNG1 || x.RoleDesc == rolePLNG2 || x.RoleDesc == rolePMG1 || x.RoleDesc == rolePMG2))
                {
                    ViewBag.UserRole = rolePLNG2;
                    ViewBag.AccessRole = UserAccessRole.Approver.GetStringValue();
                }
                else
                {
                    return new RedirectResult("~/Authentication/Authenticate/AccessDenied");
                }
            }
            else if (urlForm == WPPIndexType.maintain.GetStringValue())
            {
                if (roleList.Any(x => (x.RoleDesc == rolePLNG3 || x.RoleDesc == rolePMG3)))
                {
                    ViewBag.UserRole = rolePLNG3;
                    ViewBag.AccessRole = UserAccessRole.Initiator.GetStringValue();
                }
                else
                {
                    return new RedirectResult("~/Authentication/Authenticate/AccessDenied");
                }
            }
            else if (urlForm == WPPIndexType.releaseFKM.GetStringValue())
            {
                ViewBag.UserRole = roleFKM;
                ViewBag.AccessRole = UserAccessRole.Initiator.GetStringValue();
            }
            else
            {
                ViewBag.UserRole = roleSHOP;
                ViewBag.AccessRole = UserAccessRole.Initiator.GetStringValue();
            }

            ViewBag.Heading = Heading;
            return View(objFKM114);
        }

        [HttpPost]
        public ActionResult SaveHeader(FKM101 model)
        {
            FKM114 objFKM114 = null;
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (model.HeaderId > 0)
                {
                    if (!db.FKM114.Any(u => u.HeaderId != model.HeaderId && u.Project == model.Project))
                    {
                        objFKM114 = db.FKM114.Where(x => x.HeaderId == model.HeaderId).FirstOrDefault();

                        bool IsApproverUpdated = false;
                        if (objFKM114.ApprovedBy != model.ApprovedBy)
                            IsApproverUpdated = true;

                        objFKM114.Project = model.Project;
                        objFKM114.Customer = model.Customer;
                        objFKM114.ZeroDate = model.ZeroDate;
                        objFKM114.CDD = model.CDD;
                        objFKM114.Status = model.Status;
                        objFKM114.ProductType = model.ProductType;
                        objFKM114.RevNo = model.RevNo;
                        if (!string.IsNullOrWhiteSpace(model.ApprovedBy))
                            objFKM114.ApprovedBy = model.ApprovedBy.Split('-')[0].Trim();
                        objFKM114.EditedBy = objClsLoginInfo.UserName;
                        objFKM114.EditedOn = DateTime.Now;

                        List<FKM111> objFKM111 = db.FKM111.Where(x => x.RefHeaderId == objFKM114.HeaderId && x.Project == objFKM114.Project).ToList();

                        if (objFKM111.Count == 0)
                        {
                            InsertLinesEntry(objFKM114);
                        }

                        if (IsApproverUpdated)
                        {
                            string sentforapproval = clsImplementationEnum.FRStatus.SentForApproval.GetStringValue();
                            var list = db.FKM111.Where(x => x.RefHeaderId == objFKM114.HeaderId && x.Status == sentforapproval).ToList();
                            foreach (var item in list)
                            {
                                if (!string.IsNullOrWhiteSpace(model.ApprovedBy))
                                    item.ApprovedBy = model.ApprovedBy.Split('-')[0].Trim();
                            }
                        }

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                        objResponseMsg.HeaderId = objFKM114.HeaderId;
                        objResponseMsg.RevNo = objFKM114.RevNo.ToString();
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                    }
                }
                else
                {
                    if (!db.FKM114.Any(u => u.Project == model.Project))
                    {
                        objFKM114 = new FKM114();
                        objFKM114.Project = model.Project;
                        objFKM114.Customer = model.Customer;
                        objFKM114.ZeroDate = model.ZeroDate;
                        objFKM114.CDD = model.CDD;
                        objFKM114.RevNo = 0;
                        objFKM114.ProductType = model.ProductType;
                        if (!string.IsNullOrWhiteSpace(model.ApprovedBy))
                            objFKM114.ApprovedBy = model.ApprovedBy.Split('-')[0].Trim();
                        objFKM114.Status = clsImplementationEnum.FRStatus.Draft.GetStringValue();
                        objFKM114.CreatedBy = objClsLoginInfo.UserName;
                        objFKM114.CreatedOn = DateTime.Now;

                        db.FKM114.Add(objFKM114);
                        db.SaveChanges();

                        InsertLinesEntry(objFKM114);

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                        objResponseMsg.HeaderId = objFKM114.HeaderId;
                        objResponseMsg.RevNo = objFKM114.RevNo.ToString();
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddNewFixture(int strHeaderId, string fixtureName)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                bool result = db.FKM111.Where(x => x.RefHeaderId == strHeaderId && x.FixtureName.ToLower().Trim() == fixtureName.ToLower().Trim()).Any();

                if (!result)
                {
                    var fxrSrNo = db.FKM111.Where(x => x.RefHeaderId == strHeaderId && x.ParentId == 0).Max(x => x.FXRSrNo);
                    FKM114 objFKM114 = db.FKM114.Where(x => x.HeaderId == strHeaderId).FirstOrDefault();
                    db.FKM111.Add(new FKM111
                    {
                        RefHeaderId = objFKM114.HeaderId,
                        Project = objFKM114.Project,
                        //DocNo = objFKM114.Document,
                        RevNo = 0,
                        FixtureName = fixtureName,
                        FXRSrNo = (Convert.ToInt32(fxrSrNo) + 1),
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now,
                        ParentId = 0,
                        IsManual = true,
                        IsSubcontractFormSubmit = false,
                        Status = clsImplementationEnum.FRStatus.Draft.GetStringValue()
                    });

                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult HeaderActions(int HeaderId, string actionType)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                FKM114 objFKM114 = db.FKM114.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                if (objFKM114 != null)
                {
                    if (actionType.ToLower() == clsImplementationEnum.FRStatus.SentForApproval.GetStringValue().ToLower())
                    {
                        objFKM114.Status = clsImplementationEnum.FRStatus.SentForApproval.GetStringValue();
                        objFKM114.SubmittedBy = objClsLoginInfo.UserName;
                        objFKM114.SubmittedOn = DateTime.Now;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.sentforApprove;
                    }
                    else if (actionType.ToLower() == clsImplementationEnum.FRStatus.Approved.GetStringValue().ToLower())
                    {
                        objFKM114.Status = clsImplementationEnum.FRStatus.Approved.GetStringValue();
                        objFKM114.ApprovedBy = objClsLoginInfo.UserName;
                        objFKM114.ApprovedOn = DateTime.Now;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Approve;
                    }
                    else if (actionType.ToLower() == clsImplementationEnum.FRStatus.Retract.GetStringValue().ToLower())
                    {
                        objFKM114.Status = clsImplementationEnum.FRStatus.Draft.GetStringValue();
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Retract;
                    }
                    else if (actionType.ToLower() == clsImplementationEnum.FRStatus.Revise.GetStringValue().ToLower())
                    {
                        objFKM114.Status = clsImplementationEnum.FRStatus.Draft.GetStringValue();
                        objFKM114.RevNo += 1;
                        objFKM114.SubmittedBy = null;
                        objFKM114.SubmittedOn = null;
                        objFKM114.ApprovedBy = null;
                        objFKM114.ApprovedOn = null;
                        objFKM114.EditedBy = null;
                        objFKM114.EditedOn = null;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Revision;
                    }
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReturnHeader(int HeaderId, string strRemarks)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                FKM114 objFKM114 = db.FKM114.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                if (objFKM114 != null)
                {
                    objFKM114.Status = clsImplementationEnum.FRStatus.Returned.GetStringValue();
                    objFKM114.ReturnRemark = strRemarks;
                    objFKM114.ReturnBy = objClsLoginInfo.UserName;
                    objFKM114.ReturnOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Return;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ApproveHeader(int HeaderId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                FKM114 objFKM114 = db.FKM114.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                if (objFKM114 != null)
                {
                    objFKM114.Status = clsImplementationEnum.FRStatus.Approved.GetStringValue();
                    objFKM114.ApprovedBy = objClsLoginInfo.UserName;
                    objFKM114.ApprovedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Approve;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Lines

        [HttpPost]
        public ActionResult LoadFixtureListData(JQueryDataTableParamModel param, bool SearchReUse, bool SearchSubContracting, string SearchStatus)
        {
            try
            {
                clsManager objManager = new clsManager();

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = string.Empty;
                int HeaderId = Convert.ToInt32(param.CTQHeaderId);
                string isApprove = param.Department;
                string Status = param.Status;
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                FKM114 objFKM114 = null;

                if (HeaderId > 0)
                {
                    objFKM114 = db.FKM114.Where(u => u.HeaderId == HeaderId).FirstOrDefault();

                    whereCondition = "1=1 and RefHeaderId =" + HeaderId + " AND ParentID = 0 ";
                }
                else
                {
                    if (string.IsNullOrEmpty(param.Department)) // for execute fixture
                    {
                        whereCondition = "1=1 AND ParentID = 0 ";
                    }
                    else
                    {
                        whereCondition = "1=1 and RefHeaderId =" + HeaderId + " AND ParentID = 0 ";
                    }
                }

                if (Status != null)
                {
                    if (string.IsNullOrEmpty(param.Department)) // for execute fixture
                    {
                        if (Status.ToLower() == "all")
                            whereCondition += " and isnull(DeliverStatus,'') ='CO' and lower([Status])='approved' ";
                        else if (Status.ToLower() == "superseded")
                            whereCondition += " and lower([Status])='superseded'";
                        else
                            whereCondition += " and ((isnull(DeliverStatus,'') != 'CO' and lower([Status])='approved')) ";
                    }
                    else
                    {
                        if (Status.ToLower() == "all")
                            whereCondition += " and isnull(DeliverStatus,'') ='CO' ";
                        else
                            whereCondition += " and ((isnull(DeliverStatus,'') != 'CO' and Status='Approved' and RevNo = 0 ) or (RevNo > 0 and isnull(DeliverStatus,'') != 'CO') ) ";
                    }
                }

                if (SearchReUse)
                    whereCondition += " and SearchReUse='" + SearchReUse + "' ";

                if (SearchSubContracting)
                    whereCondition += " and SearchSubcontracting='" + SearchSubContracting + "' ";

                if (!string.IsNullOrWhiteSpace(SearchStatus))
                    whereCondition += " and Status='" + SearchStatus + "' ";

                string role = param.Roles; //string role = GetUserRole();
                var isPLNG3 = (role == UserRoleName.PLNG3.GetStringValue() || role == UserRoleName.PMG3.GetStringValue());
                if (role == UserRoleName.PLNG1.GetStringValue() || role == UserRoleName.PLNG2.GetStringValue() || role == UserRoleName.PMG1.GetStringValue() || role == UserRoleName.PMG2.GetStringValue())
                {
                    if (HeaderId > 0)
                    {
                        if (objFKM114.ApprovedBy == objClsLoginInfo.UserName)
                            whereCondition += " and Status in ('" + FRStatus.SentForApproval.GetStringValue() + "','" + FRStatus.Approved.GetStringValue() + "')";
                        else
                            whereCondition = "1=1";
                    }
                    else
                    {
                        if (db.FKM114.Any(x => x.ApprovedBy == objClsLoginInfo.UserName))
                            whereCondition += " and Status in ('" + FRStatus.SentForApproval.GetStringValue() + "','" + FRStatus.Approved.GetStringValue() + "')";
                        else
                            whereCondition = "1=1";
                    }
                }
                /*else if (role == UserRoleName.SHOP.GetStringValue() || role == UserRoleName.FKM3.GetStringValue() || role == UserRoleName.PROD3.GetStringValue() || role == UserRoleName.PMG3.GetStringValue())
                {
                    //whereCondition += " and (SUBSTRING(FixMfg, 0 ,CHARINDEX('-', FixMfg))='" + objClsLoginInfo.UserName + "') and Status in ('" + clsImplementationEnum.FRStatus.Approved.GetStringValue() + "')";
                }*/

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] columnName = { "Project", "FixtureName", "DescriptionofItem", "Category", "Material", "MaterialType", "StructuralType", "PipeNormalBore", "Status", "FixMfg", "FindNo", "ReturnRemark", "ReviseRemark", "FXRSrNo", "RevNo", "Typeoffixture", "QtyofFixture", "ItemType", "[Weight]", "DeliveryDateRequired", "WCC", "AllocatedContractor", "ISNULL(DeliverStatus,'NS')" };

                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                if (string.IsNullOrEmpty(param.Department)) // for execute fixture
                {
                    #region Execute Fixture
                    var lstResult = db.SP_FKMS_GET_FR_LINES_FMG(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                    if (lstResult.Count == 0)
                    {
                        return Json(new { sEcho = param.sEcho, iTotalRecords = "0", iTotalDisplayRecords = "0", aaData = new string[] { } }, JsonRequestBehavior.AllowGet);
                    }

                    string co = clsImplementationEnum.COP_CommonStatus.Co.GetStringValue();
                    var objFKM111List = db.FKM111_Log.Where(x => x.RefHeaderId == (HeaderId > 0 ? HeaderId : x.RefHeaderId)).ToList();
                    //var objFKM118List = db.FKM118.ToList();
                    var objFKM119List = db.FKM119.ToList();

                    //string whereCondition1 = " RefHeaderId=" + HeaderId + " and ReUse=0 and Subcontracting=0 and ((Status='" + clsImplementationEnum.FRStatus.Approved.GetStringValue() + "' or RevNo>0) and (IsInsertedInPLM=0 or IsInsertedInPLM is null))";
                    //var listNotLinkInPLN = db.SP_FKMS_FR_GET_FIXTURE_NOT_INSERT_IN_PLM(0, int.MaxValue, "", whereCondition1).ToList();

                    var data = (from fx in lstResult
                                select new[]
                                {
                                "",//0
                                "",//1
                                Convert.ToString(fx.ROW_NO),//2
                                "<nobr><img onclick='ExpandCollapsChild("+ fx.LineId +", this)' src='"+WebsiteURL+"/Images/details_close.png' />"+Convert.ToString(fx.GROUP_NO) + "</nobr>",
                                objManager.GetFixtureNo(fx.Project,fx.FXRSrNo),  //4
                                Convert.ToString(fx.RevNo),//fixture rev no 5
                                (isPLNG3 && fx.Status!= clsImplementationEnum.FRStatus.SentForApproval.GetStringValue() ? Helper.GenerateTextbox(fx.LineId,"FixtureName",Convert.ToString(fx.FixtureName),onchange: "UpdateData(this,"+fx.LineId+",true)") :fx.FixtureName), // 6
                                Convert.ToString(fx.ReUse),//fixture resuse   //7                            
                                (fx.RefFixtureReuse != null ? GeRefFixtureReuseFMG(objFKM111List,fx.RefFixtureReuse.Value) : ""), //reference fixture reuse name  //8
                                fx.SCRNo, //9
                                fx.Typeoffixture, // 10
                                fx.DeliverStatus, //11
                                Convert.ToString(fx.Subcontracting),//fixture subcontracting  //12
                                (isPLNG3 && fx.Status!= clsImplementationEnum.FRStatus.SentForApproval.GetStringValue() ?  Helper.GenerateTextbox(fx.LineId,"QtyofFixture",Convert.ToString(fx.QtyofFixture),onchange: fx.Status != clsImplementationEnum.FRStatus.Returned.GetStringValue()? "UpdateQtyofFixture("+fx.LineId+",this)":"UpdateReturnedQtyofFixture("+fx.LineId+",this)", isReadOnly: ((fx.QtyofFixture > 0 || fx.ReUse) && fx.Status != clsImplementationEnum.FRStatus.Returned.GetStringValue() && fx.Status != clsImplementationEnum.FRStatus.Draft.GetStringValue()), classname: "numeric") + Helper.GenerateHidden(fx.LineId, "hidQtyfixture", Convert.ToString(fx.QtyofFixture > 0 ? fx.QtyofFixture : 0)) : Convert.ToString(fx.QtyofFixture)),   //13
                                Convert.ToString(fx.ItemCode),                  //14
                                Convert.ToString(fx.DescriptionofItem),         //15
                                fx.ParentId == 0 ? "":fx.ItemType,              //16                      
                                Convert.ToString(fx.Category),                  //17
                                Convert.ToString(fx.Material),                  //18
                                Convert.ToString(fx.MaterialType),              //19
                                Convert.ToString(fx.LengthOD),                  //20
                                Convert.ToString(fx.WidthOD),                   //21
                                Convert.ToString(fx.Thickness),                 //22
                                Convert.ToString(fx.Qty),                       //23
                                Convert.ToString(fx.Wt),                        //24
                                Convert.ToString(fx.Area),                      //25
                                Convert.ToString(fx.Unit),                      //26
                                Convert.ToString(fx.ReqWt),                     //27
                                Convert.ToString(fx.ReqArea),                   //28
                                Convert.ToString(fx.Unit2),                     //29
                                fx.ParentId == 0 ? Convert.ToString(fx.MaterialReqDate.HasValue ? fx.MaterialReqDate.Value.ToShortDateString() : "") : "",         //30
                                fx.ParentId == 0 ? Convert.ToString(fx.FixRequiredDate.HasValue ? fx.FixRequiredDate.Value.ToShortDateString() : "") : "",         //31
                                Convert.ToString(fx.LineId),                    //32
                                Convert.ToString(fx.RefHeaderId),               //33
                                Convert.ToString(fx.ParentId),                  //34
                                Convert.ToString(fx.FixtureName),               //35
                                Convert.ToString(fx.IsManual),                  //36
                                fx.ParentId == 0 ? FindChildCountFMG(objFKM111List, fx.LineId) > 0 ? "true" : "false" : "false", //37
                                fx.IsSubcontractFormSubmit != null ? Convert.ToBoolean(fx.IsSubcontractFormSubmit) ? "true" : "false" : "false",   //38
                                fx.ParentId == 0 ? Convert.ToString(fx.Status):"",                    //39
                                Convert.ToString(fx.Status),                                          //40
                                fx.ParentId == 0 ? FixtureAllocateCount(objFKM119List,fx.LineId).ToString() : "0",  //41
                                fx.ParentId == 0 ? fx.FixMfg:"",                                      //42                 
                                fx.ParentId == 0 ? "0" : Convert.ToString(GetTotalAllocatedQty(fx.ParentId.Value,fx.LineId,0,fx.ItemType)),//material allocated qty 43
                                fx.ParentId == 0 ? Convert.ToString(GetFixtureTotalAllocatedQty(fx.LineId,0)):"0",//fixture total allocated qty 44
                                fx.ParentId == 0 ? Convert.ToString(GetFixtureTotalReqQtyFMG(objFKM111List,fx.LineId,0)):"0",//fixture total req qty 45
                                fx.ParentId == 0 ? Convert.ToString( (objFKM111List.Where(x=>x.Id == fx.LineId).Select(x=>x.DeliverStatus).FirstOrDefault() == co) ? "true" : "false"):Convert.ToString( (objFKM111List.Where(x=>x.LineId == fx.ParentId).Select(x=>x.DeliverStatus).FirstOrDefault() == co) ? "true" : "false"),//IsFixtureCompleted(objFKM118List,fx.LineId):IsFixtureCompleted(objFKM118List,fx.ParentId.Value),//fixture completed flag 46
                                Convert.ToString(fx.ReUse),//resue to handle show/hide item allocation to LN  //47
                                fx.ParentId == 0 ? (Helper.CheckAttachmentUpload("FKM111/"+fx.RefHeaderId+"/"+fx.LineId+"/R"+fx.RevNo.ToString())==true?"true":"false") :"false", // check whether fixture has attachment 48
                                fx.IsInsertedInPLM != null ? Convert.ToString(fx.IsInsertedInPLM) : "",  // IsInsertedInPLM 49
                                fx.FindNo,   // Find No 50
                                fx.ReturnRemark, // Return Remark 51
                                fx.ReviseRemark, // Revise Remark 52
                                Convert.ToString(fx.QtyofFixture),              //53
                                fx.ParentId == 0 ? Convert.ToString(CheckDateFilledByFixMfg((HeaderId> 0 ? HeaderId: 0),fx.LineId,GridType.LINES.GetStringValue(),role)) : "true", //54
                                fx.Weight, //55
                                Convert.ToString(fx.DeliveryDateRequired.HasValue ? fx.DeliveryDateRequired.Value.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture) : ""), //56
                                Convert.ToString(fx.WCC.HasValue ? fx.WCC.Value.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture) : ""), //57
                                fx.AllocatedContractor, //58
                                Convert.ToString(fx.RefFixtureReuse.HasValue?fx.RefFixtureReuse:0),//59
                                "0",//60
                                "0",//61
                                "",//62
                                "",//63
                                "",//64
                                db.FKM111_Log.Where(x=>x.Id == fx.LineId).FirstOrDefault() != null ?  Convert.ToString(db.FKM111_Log.Where(x=>x.Id == fx.LineId).FirstOrDefault().LineId) : "",//65
                                db.FKM111_Log.Where(x=>x.Id == fx.LineId).FirstOrDefault() != null ? Convert.ToString(db.FKM111_Log.Where(x=>x.Id == fx.LineId).FirstOrDefault().RefHeaderId) : ""//66
                            }).ToList();

                    return Json(new
                    {
                        sEcho = Convert.ToInt32(param.sEcho),
                        iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                        iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                        aaData = data,
                        strSortOrder = strSortOrder,
                        whereCondition = whereCondition
                    }, JsonRequestBehavior.AllowGet);
                    #endregion
                }
                else
                {
                    #region Maintain and Approve Fixture
                    var lstResult = db.SP_FKMS_GET_FR_LINES(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                    if (lstResult.Count == 0)
                    {
                        return Json(new { sEcho = param.sEcho, iTotalRecords = "0", iTotalDisplayRecords = "0", aaData = new string[] { } }, JsonRequestBehavior.AllowGet);
                    }

                    string co = clsImplementationEnum.COP_CommonStatus.Co.GetStringValue();
                    var objFKM111List = db.FKM111.Where(x => x.RefHeaderId == (HeaderId > 0 ? HeaderId : x.RefHeaderId)).ToList();
                    //var objFKM118List = db.FKM118.ToList();
                    var objFKM119List = db.FKM119.ToList();

                    //string whereCondition1 = " RefHeaderId=" + HeaderId + " and ReUse=0 and Subcontracting=0 and ((Status='" + clsImplementationEnum.FRStatus.Approved.GetStringValue() + "' or RevNo>0) and (IsInsertedInPLM=0 or IsInsertedInPLM is null))";
                    //var listNotLinkInPLN = db.SP_FKMS_FR_GET_FIXTURE_NOT_INSERT_IN_PLM(0, int.MaxValue, "", whereCondition1).ToList();

                    var data = (from fx in lstResult
                                select new[]
                                {
                                "",//0
                                "",//1
                                Convert.ToString(fx.ROW_NO),//2
                                fx.ParentId == 0 ? "<nobr><img onclick='ExpandCollapsChild("+ fx.LineId +", this)' src='"+WebsiteURL+"/Images/details_close.png' />"+Convert.ToString(fx.GROUP_NO) + "</nobr>" : "<span class='"+ fx.ParentId +" child' ></span>",
                                fx.ParentId == 0 ? objManager.GetFixtureNo(fx.Project,fx.FXRSrNo) : objManager.GetItemNo(fx.Project,fx.FXRSrNo),  //4
                                fx.ParentId == 0 ? Convert.ToString(fx.RevNo):GetFixtureRevNo(objFKM111List,fx.ParentId.Value),//fixture rev no 5
                                fx.ParentId == 0 ? (isPLNG3 && fx.Status!= clsImplementationEnum.FRStatus.SentForApproval.GetStringValue() ? Helper.GenerateTextbox(fx.LineId,"FixtureName",Convert.ToString(fx.FixtureName),onchange: "UpdateData(this,"+fx.LineId+",true)") :fx.FixtureName) : "", // 6
                                fx.ParentId == 0 ? Convert.ToString(fx.ReUse) : Convert.ToString(IsFixtureReUse(objFKM111List,fx.ParentId.Value)),//fixture resuse   //7                            
                                fx.ParentId == 0 ? (fx.RefFixtureReuse!=null ? GeRefFixtureReuse(objFKM111List,fx.RefFixtureReuse.Value) : "") : "", //reference fixture reuse name  //8
                                fx.SCRNo, //9
                                fx.Typeoffixture, // 10
                                fx.DeliverStatus, //11
                                fx.ParentId == 0 ? Convert.ToString(fx.Subcontracting) : Convert.ToString(IsFixtureSubcontracting(objFKM111List,fx.ParentId.Value)),//fixture subcontracting  //12
                                fx.ParentId == 0 ?(isPLNG3 && fx.Status!= clsImplementationEnum.FRStatus.SentForApproval.GetStringValue() ?  Helper.GenerateTextbox(fx.LineId,"QtyofFixture",Convert.ToString(fx.QtyofFixture),onchange: fx.Status != clsImplementationEnum.FRStatus.Returned.GetStringValue()? "UpdateQtyofFixture("+fx.LineId+",this)":"UpdateReturnedQtyofFixture("+fx.LineId+",this)", isReadOnly: ((fx.QtyofFixture > 0 || fx.ReUse) && fx.Status != clsImplementationEnum.FRStatus.Returned.GetStringValue() && fx.Status != clsImplementationEnum.FRStatus.Draft.GetStringValue()), classname: "numeric") + Helper.GenerateHidden(fx.LineId, "hidQtyfixture", Convert.ToString(fx.QtyofFixture > 0 ? fx.QtyofFixture : 0)) : Convert.ToString(fx.QtyofFixture)) :"",   //13
                                Convert.ToString(fx.ItemCode),                  //14
                                Convert.ToString(fx.DescriptionofItem),         //15
                                fx.ParentId == 0 ? "":fx.ItemType,              //16                      
                                Convert.ToString(fx.Category),                  //17
                                Convert.ToString(fx.Material),                  //18
                                Convert.ToString(fx.MaterialType),              //19
                                Convert.ToString(fx.LengthOD),                  //20
                                Convert.ToString(fx.WidthOD),                   //21
                                Convert.ToString(fx.Thickness),                 //22
                                Convert.ToString(fx.Qty),                       //23
                                Convert.ToString(fx.Wt),                        //24
                                Convert.ToString(fx.Area),                      //25
                                Convert.ToString(fx.Unit),                      //26
                                Convert.ToString(fx.ReqWt),                     //27
                                Convert.ToString(fx.ReqArea),                   //28
                                Convert.ToString(fx.Unit2),                     //29
                                fx.ParentId == 0 ? Convert.ToString(fx.MaterialReqDate.HasValue ? fx.MaterialReqDate.Value.ToShortDateString() : "") : "",         //30
                                fx.ParentId == 0 ? Convert.ToString(fx.FixRequiredDate.HasValue ? fx.FixRequiredDate.Value.ToShortDateString() : "") : "",         //31
                                Convert.ToString(fx.LineId),                    //32
                                Convert.ToString(fx.RefHeaderId),               //33
                                Convert.ToString(fx.ParentId),                  //34
                                Convert.ToString(fx.FixtureName),               //35
                                Convert.ToString(fx.IsManual),                  //36
                                fx.ParentId == 0 ? FindChildCount(objFKM111List, fx.LineId) > 0 ? "true" : "false" : "false", //37
                                fx.IsSubcontractFormSubmit != null ? Convert.ToBoolean(fx.IsSubcontractFormSubmit) ? "true" : "false" : "false",   //38
                                fx.ParentId == 0 ? Convert.ToString(fx.Status):"",                    //39
                                Convert.ToString(fx.Status),                                          //40
                                fx.ParentId == 0 ? FixtureAllocateCount(objFKM119List,fx.LineId).ToString() : "0",  //41
                                fx.ParentId == 0 ? fx.FixMfg:"",                                      //42                 
                                fx.ParentId == 0 ? "0" : Convert.ToString(GetTotalAllocatedQty(fx.ParentId.Value,fx.LineId,0,fx.ItemType)),//material allocated qty 43
                                fx.ParentId == 0 ? Convert.ToString(GetFixtureTotalAllocatedQty(fx.LineId,0)):"0",//fixture total allocated qty 44
                                fx.ParentId == 0 ? Convert.ToString(GetFixtureTotalReqQty(objFKM111List,fx.LineId,0)):"0",//fixture total req qty 45
                                fx.ParentId == 0 ? Convert.ToString( (objFKM111List.Where(x=>x.LineId == fx.LineId).Select(x=>x.DeliverStatus).FirstOrDefault() == co) ? "true" : "false"):Convert.ToString( (objFKM111List.Where(x=>x.LineId == fx.ParentId).Select(x=>x.DeliverStatus).FirstOrDefault() == co) ? "true" : "false"),//IsFixtureCompleted(objFKM118List,fx.LineId):IsFixtureCompleted(objFKM118List,fx.ParentId.Value),//fixture completed flag 46
                                Convert.ToString(fx.ReUse),//resue to handle show/hide item allocation to LN  //47
                                fx.ParentId == 0 ? (Helper.CheckAttachmentUpload("FKM111/"+fx.RefHeaderId+"/"+fx.LineId+"/R"+fx.RevNo.ToString())==true?"true":"false") :"false", // check whether fixture has attachment 48
                                fx.IsInsertedInPLM != null ? Convert.ToString(fx.IsInsertedInPLM) : "",  // IsInsertedInPLM 49
                                fx.FindNo,   // Find No 50
                                fx.ReturnRemark, // Return Remark 51
                                fx.ReviseRemark, // Revise Remark 52
                                Convert.ToString(fx.QtyofFixture),              //53
                                fx.ParentId == 0 ? Convert.ToString(CheckDateFilledByFixMfg((HeaderId> 0 ? HeaderId: 0),fx.LineId,GridType.LINES.GetStringValue(),role)) : "true", //54
                                fx.Weight, //55
                                Convert.ToString(fx.DeliveryDateRequired.HasValue ? fx.DeliveryDateRequired.Value.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture) : ""), //56
                                Convert.ToString(fx.WCC.HasValue ? fx.WCC.Value.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture) : ""), //57
                                fx.AllocatedContractor, //58
                                Convert.ToString(fx.RefFixtureReuse.HasValue?fx.RefFixtureReuse:0),//59
                                Convert.ToString(GetReuseFixtureRefHeaderId(objFKM111List,fx.RefFixtureReuse)),//60
                                Convert.ToString(GetReuseFixtureRefRevNo(objFKM111List,fx.RefFixtureReuse)),//61
                                Convert.ToString(GetReuseFixtureProject(objFKM111List,fx.RefFixtureReuse)),//62
                                Convert.ToString(GetReuseFixtureFixtureNoName(objFKM111List,fx.RefFixtureReuse)),//63
                                Convert.ToString(GetReuseFixtureFindNo(objFKM111List,fx.RefFixtureReuse)),//64
                                "",
                                ""
                            }).ToList();

                    return Json(new
                    {
                        sEcho = Convert.ToInt32(param.sEcho),
                        iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                        iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                        aaData = data,
                        strSortOrder = strSortOrder,
                        whereCondition = whereCondition
                    }, JsonRequestBehavior.AllowGet);
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public int GetReuseFixtureRefHeaderId(List<FKM111> objFKM111List, int? RefFixtureReuse)
        {
            int refHeaderId = 0;
            if (RefFixtureReuse.HasValue)
            {
                var objFKM111 = objFKM111List.Where(x => x.LineId == RefFixtureReuse).FirstOrDefault();
                if (objFKM111 != null)
                    refHeaderId = objFKM111.RefHeaderId.HasValue ? objFKM111.RefHeaderId.Value : 0;
            }

            return refHeaderId;
        }
        public int GetReuseFixtureRefRevNo(List<FKM111> objFKM111List, int? RefFixtureReuse)
        {
            int revNo = 0;
            if (RefFixtureReuse.HasValue)
            {
                var objFKM111 = objFKM111List.Where(x => x.LineId == RefFixtureReuse).FirstOrDefault();
                if (objFKM111 != null)
                    revNo = objFKM111.RevNo.HasValue ? objFKM111.RevNo.Value : 0;
            }

            return revNo;
        }
        public string GetReuseFixtureProject(List<FKM111> objFKM111List, int? RefFixtureReuse)
        {
            string Project = string.Empty;
            if (RefFixtureReuse.HasValue)
            {
                var objFKM111 = objFKM111List.Where(x => x.LineId == RefFixtureReuse).FirstOrDefault();
                if (objFKM111 != null)
                    Project = objFKM111.Project;
            }
            return Project;
        }
        public string GetReuseFixtureFindNo(List<FKM111> objFKM111List, int? RefFixtureReuse)
        {
            string FindNo = string.Empty;
            if (RefFixtureReuse.HasValue)
            {
                var objFKM111 = objFKM111List.Where(x => x.LineId == RefFixtureReuse).FirstOrDefault();
                if (objFKM111 != null)
                    FindNo = objFKM111.FindNo;
            }
            return FindNo;
        }
        public string GetReuseFixtureFixtureNoName(List<FKM111> objFKM111List, int? RefFixtureReuse)
        {
            string Name = string.Empty;
            if (RefFixtureReuse.HasValue)
            {
                clsManager objManager = new clsManager();
                var objFKM111 = objFKM111List.Where(x => x.LineId == RefFixtureReuse).FirstOrDefault();
                if (objFKM111 != null)
                    Name = objFKM111.ParentId == 0 ? objManager.GetFixtureNo(objFKM111.Project, objFKM111.FXRSrNo) : objManager.GetItemNo(objFKM111.Project, objFKM111.FXRSrNo);
            }
            return Name;
        }
        public string GetFixtureRevNo(List<FKM111> objFKM111List, int RefLineId)
        {
            string RevNo = "0";
            var objFKM111 = objFKM111List.Where(x => x.LineId == RefLineId).FirstOrDefault();
            if (objFKM111 != null)
                RevNo = objFKM111.RevNo != null ? objFKM111.RevNo.ToString() : "0";
            return RevNo;
        }

        public string GeRefFixtureReuse(List<FKM111> objFKM111List, int RefFixtureReuse)
        {
            string result = "";
            clsManager objManager = new clsManager();
            var objFKM111Ref = objFKM111List.Where(x => x.LineId == RefFixtureReuse).FirstOrDefault();
            if (objFKM111Ref != null)
                result = objManager.GetFixtureNo(objFKM111Ref.Project, objFKM111Ref.FXRSrNo) + " - " + objFKM111Ref.FixtureName;
            return result;
        }
        public string GeRefFixtureReuseFMG(List<FKM111_Log> objFKM111List, int RefFixtureReuse)
        {
            string result = "";
            clsManager objManager = new clsManager();
            var objFKM111Ref = objFKM111List.Where(x => x.Id == RefFixtureReuse).FirstOrDefault();
            if (objFKM111Ref != null)
                result = objManager.GetFixtureNo(objFKM111Ref.Project, objFKM111Ref.FXRSrNo) + " - " + objFKM111Ref.FixtureName;
            return result;
        }

        public bool IsFixtureReUse(List<FKM111> objFKM111List, int RefLineId)
        {
            bool ReUse = false;
            var objFKM111 = objFKM111List.Where(x => x.LineId == RefLineId).FirstOrDefault();
            if (objFKM111 != null)
                ReUse = objFKM111.ReUse;
            return ReUse;
        }
       
        public bool IsFixtureSubcontracting(List<FKM111> objFKM111List, int RefLineId)
        {
            bool Subcontracting = false;
            var objFKM111 = objFKM111List.Where(x => x.LineId == RefLineId).FirstOrDefault();
            if (objFKM111 != null)
                Subcontracting = objFKM111.Subcontracting;
            return Subcontracting;
        }
        
        public string IsFixtureCompleted(List<FKM118> objFKM118List, int RefLineId)
        {
            string completed = clsImplementationEnum.FRMaterialDeliveryStatus.Completed.GetStringValue();
            if (objFKM118List.Any(x => x.RefLineId == RefLineId && x.DeliverStatus == completed))
                return "true";
            else
                return "false";
        }

        [HttpPost]
        public ActionResult AddOrUpdateFixtureItemsDetails(int lineId, int headerId, string action)
        {
            ViewBag.IsFixtureApproved = false;
            int RefLineId = 0;
            FKM111 objFKM111 = null;
            if (lineId > 0 && headerId > 0)
            {
                ViewBag.StrAction = action;
                if (action == "add")
                {
                    objFKM111 = new FKM111();
                    FKM111 masterLine = db.FKM111.Where(x => x.LineId == lineId && x.RefHeaderId == headerId).FirstOrDefault();
                    if (masterLine != null)
                    {
                        RefLineId = masterLine.LineId;
                        objFKM111.LineId = masterLine.LineId;
                        objFKM111.RefHeaderId = masterLine.RefHeaderId;
                        objFKM111.Project = masterLine.Project;
                        //objFKM111.Document = masterLine.Document;
                        objFKM111.RevNo = masterLine.RevNo;
                        objFKM111.FixtureName = masterLine.FixtureName;
                        ViewBag.RevNo = masterLine.RevNo != null ? Convert.ToString(masterLine.RevNo) : "0";
                    }

                    var objFixItem = db.FKM111.Where(x => x.ParentId == lineId && x.RefHeaderId == headerId).FirstOrDefault();
                    if (objFixItem != null)
                    {
                        objFKM111.FixRequiredDate = objFixItem.FixRequiredDate;
                        objFKM111.MaterialReqDate = objFixItem.MaterialReqDate;
                        objFKM111.FixMfg = objFixItem.FixMfg;
                    }
                }
                else if (action == "edit")
                {
                    objFKM111 = db.FKM111.Where(x => x.LineId == lineId && x.RefHeaderId == headerId).FirstOrDefault();
                    ViewBag.Category = objFKM111.Category;
                    ViewBag.Material = objFKM111.Material;
                    ViewBag.MaterialType = objFKM111.MaterialType;
                    ViewBag.Unit = objFKM111.Unit;
                    ViewBag.Unit2 = objFKM111.Unit2;
                    ViewBag.StructuralType = objFKM111.StructuralType;
                    ViewBag.PipeNormalBore = objFKM111.PipeNormalBore;
                    ViewBag.PipeSchedule = objFKM111.PipeSchedule;
                    ViewBag.ItemCode = objFKM111.ItemCode;
                    if (objFKM111.FXRSrNo > 0)
                    {
                        clsManager objManager = new clsManager();
                        ViewBag.AutoPartNo = objManager.GetItemNo(objFKM111.Project, objFKM111.FXRSrNo);
                    }

                    RefLineId = objFKM111.ParentId.Value;
                    FKM111 masterLine = db.FKM111.Where(x => x.LineId == RefLineId && x.RefHeaderId == headerId).FirstOrDefault();
                    if (masterLine != null)
                        ViewBag.RevNo = masterLine.RevNo != null ? Convert.ToString(masterLine.RevNo) : "0";

                    objFKM111.Wt = objFKM111.Wt.HasValue ? Math.Round(objFKM111.Wt.Value, 2) : 0;
                    objFKM111.Area = objFKM111.Area.HasValue ? Math.Round(objFKM111.Area.Value, 2) : 0;
                }

                if (db.FKM111.Any(x => x.ParentId == RefLineId))
                {
                    var qty = db.FKM111.Where(x => x.ParentId == RefLineId).FirstOrDefault().QtyofFixture;
                    objFKM111.QtyofFixture = Convert.ToInt32(qty);
                    ViewBag.IsFirstEntry = "false";
                }
                else
                {
                    ViewBag.IsFirstEntry = "true";
                }

                List<string> lstFXRStatus = clsImplementationEnum.GetFixtureStatus().ToList();
                ViewBag.lstFXRStatus = lstFXRStatus.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();

                List<string> lstFXRCategory = clsImplementationEnum.GetFXRCategory().ToList();
                ViewBag.lstFXRCategory = lstFXRCategory.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();

                List<string> lstFXRMaterialType = clsImplementationEnum.GetFXRMaterialType().ToList();
                ViewBag.lstFXRMaterialType = lstFXRMaterialType.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();

                //List<string> lstFXRUnit = clsImplementationEnum.GetFXRUnit().ToList();
                //ViewBag.lstFXRUnit = lstFXRUnit.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();

                //List<string> lstFXRUnit2 = clsImplementationEnum.GetFXRUnit2().ToList();
                //ViewBag.lstFXRUnit2 = lstFXRUnit2.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();

                List<string> lstFXRStructuralType = clsImplementationEnum.GetFXRStructuralType().ToList();
                ViewBag.lstFXRStructuralType = lstFXRStructuralType.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();

                List<string> lstBore = db.FXR005.Select(x => x.Bore).Distinct().ToList();
                ViewBag.lstBore = lstBore.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();

                List<string> lstSchedule = db.FXR005.Select(x => x.Schedule).Distinct().ToList();
                ViewBag.lstSchedule = lstSchedule.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();

                List<ApproverModel> listMfg = new List<ApproverModel>();
                string[] mfgRoles = new string[] { UserRoleName.FMG3.GetStringValue() };
                for (int i = 0; i < mfgRoles.Length; i++)
                {
                    List<ApproverModel> list = Manager.GetApproverList(mfgRoles[i].ToString(), objClsLoginInfo.Location, objClsLoginInfo.UserName, string.Empty).ToList();
                    if (list.Count > 0)
                        listMfg.AddRange(list);
                }
                ViewBag.lstFixMfg = (from a in listMfg
                                     group a by new
                                     {
                                         a.Code,
                                         a.Name
                                     } into b
                                     select new BULocWiseCategoryModel()
                                     {
                                         CatDesc = b.Key.Name,
                                         CatID = b.Key.Code
                                     }).ToList();

                if (!string.IsNullOrWhiteSpace(objFKM111.FixMfg))
                    ViewBag.FixMfg = Manager.GetPsidandDescription(objFKM111.FixMfg);

                //Once fixture approved, do not allow to edit item code. Obs Id#17340                
                if (objFKM111.ParentId != null && objFKM111.ParentId > 0)
                {
                    var objFixture = db.FKM111.Where(x => x.LineId == objFKM111.ParentId).FirstOrDefault();
                    if (objFixture != null)
                    {
                        if ((objFixture.RevNo != null && objFixture.RevNo > 0) || objFixture.Status == clsImplementationEnum.FRStatus.Approved.GetStringValue())
                            ViewBag.IsFixtureApproved = true;
                    }
                }
            }

            return PartialView("_FixtureItemDetailsPartial", objFKM111);
        }

        [HttpPost]
        public ActionResult UpdateQtyofFixture(int lineId, int headerId, int QtyofFixture, string project)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsgWithStatus = new clsHelper.ResponseMsgWithStatus();
            List<FKM118> lstFKM118 = new List<FKM118>();

            try
            {
                FKM114 objFKM114 = db.FKM114.Where(x => x.HeaderId == headerId && x.Project == project).FirstOrDefault();
                if (objFKM114.Status == clsImplementationEnum.FRStatus.Approved.GetStringValue())
                {
                    objFKM114.RevNo = Convert.ToInt32(objFKM114.RevNo) + 1;
                    objFKM114.Status = clsImplementationEnum.FRStatus.Draft.GetStringValue();
                    objFKM114.ReturnRemark = null;
                    objFKM114.ApprovedOn = null;
                }
                int parentFXRId = 0;
                int? oldqtyfixture = 0;
                FKM111 objFKM111 = db.FKM111.Where(x => x.LineId == lineId && x.RefHeaderId == headerId).FirstOrDefault();
                parentFXRId = objFKM111.ParentId.Value;
                oldqtyfixture = (objFKM111.QtyofFixture > 0 ? objFKM111.QtyofFixture : 0);
                objFKM111.QtyofFixture = QtyofFixture < 1 ? 1 : QtyofFixture;
                objFKM111.EditedBy = objClsLoginInfo.UserName;
                objFKM111.EditedOn = DateTime.Now;
                objResponseMsgWithStatus.Value = "Item updated successfully";

                var isFirstEntry = !db.FKM111.Any(x => x.ParentId == objFKM111.ParentId);//  db.FKM111.Where(x => x.LineId == parentFXRId && x.RefHeaderId == headerId) ;
                //if (isFirstEntry && objFKM111.QtyofFixture > 0)
                {
                    var objFKM118List = objFKM111.FKM118.ToList();
                    //var objFKM118List = db.FKM118.ToList();
                    clsManager objManager = new clsManager();
                    var fxno = objFKM111.FXRSrNo;

                    var lastseqno = "0";
                    if (objFKM118List.Count > 0)
                    {
                        lastseqno = objFKM118List.LastOrDefault().FixtureNo.Split('-')[3];
                    }

                    for (int i = (Convert.ToInt32(lastseqno) + 1); i <= Convert.ToInt32(lastseqno) + ( objFKM111.QtyofFixture - oldqtyfixture); i++)
                    {
                        FKM118 objFKM118 = new FKM118();
                        objFKM118.RefLineId = Convert.ToInt32(objFKM111.LineId);
                        objFKM118.FixtureNo = objManager.GetFixtureNo(objFKM111.Project, fxno) + "-" + i;
                        objFKM118.CreatedBy = objClsLoginInfo.UserName;
                        objFKM118.CreatedOn = DateTime.Now;
                        //objFKM118.FixReqDate = objFixFKM111.FixRequiredDate;

                        if (!objFKM118List.Any(x => x.RefLineId == objFKM118.RefLineId && x.FixtureNo == objFKM118.FixtureNo))
                            lstFKM118.Add(objFKM118);
                    }

                    if (lstFKM118.Count > 0)
                        db.FKM118.AddRange(lstFKM118);
                }
                db.SaveChanges();
                StartFindNo += 1;

                decimal weight = 0, density = 0;
                if (!string.IsNullOrWhiteSpace(objFKM111.MaterialType))
                    density = db.FXR003.FirstOrDefault(x => x.MaterialType == objFKM111.MaterialType).Density;

                if (!string.IsNullOrWhiteSpace(objFKM111.Category))
                {
                    var mType = objFKM111.Category.Split('-')[0];
                    var mSize = objFKM111.Category.Split('-')[1];
                    if (objFKM111.MaterialType.ToLower() == "structural")
                        weight = Convert.ToDecimal(db.FXR006.Where(x => x.Type == mType && x.Size == mSize).FirstOrDefault().Weight);
                    else
                        weight = Convert.ToDecimal(db.FXR005.Where(x => x.Bore == mType && x.Schedule == mSize).FirstOrDefault().Weight);
                }

                var lstFKM115 = db.FKM115.Where(x => x.LineId == lineId).ToList();
                MaterialCalculationForSingleQuantity(objFKM111, lstFKM115, density, weight, "edit");

                //update weight in sub contracting form
                var objFKM116 = db.FKM116.Where(x => x.RefLineId == parentFXRId).FirstOrDefault();
                if (objFKM116 != null)
                {
                    var wt = db.FKM111.Where(x => x.ParentId == parentFXRId).Sum(x => x.Wt);
                    objFKM116.Weight = wt != null ? wt.ToString() : "";
                }

                db.SaveChanges();
               
                objFKM111.DeliverStatus = GetCombineDeliveryStatus(objFKM111.LineId, false);
                db.SaveChanges();

                objResponseMsgWithStatus.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsgWithStatus.Key = false;
                objResponseMsgWithStatus.Value = ex.Message;
            }
            return Json(objResponseMsgWithStatus);
        }

        /*[HttpPost]
        public ActionResult UpdateStatus(int Id, string Status)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsgWithStatus = new clsHelper.ResponseMsgWithStatus();
            try
            {
                var objFKM118 = db.FKM118.FirstOrDefault(x => x.Id == Id);
                if (objFKM118 != null)
                {
                    objFKM118.DeliverStatus = Status;
                    db.SaveChanges();
                    var statusCO = COP_CommonStatus.Co.GetStringValue();
                    var statusIP = COP_CommonStatus.IP.GetStringValue();
                    var statusNS = COP_CommonStatus.NS.GetStringValue();
                    var objFKM111 = db.FKM111.FirstOrDefault(x => x.LineId == objFKM118.RefLineId);
                    if (Status == statusIP)
                        objFKM111.DeliverStatus = Status;
                    else
                    {
                         if (!db.FKM118.Any(x => x.RefLineId == objFKM111.LineId && (x.DeliverStatus == statusIP || x.DeliverStatus == statusNS || x.DeliverStatus == null)))
                        {
                            objFKM111.DeliverStatus = COP_CommonStatus.Co.GetStringValue();
                            #region Add Inventory
                            var objFKMControl = new MaintainFKMSController();
                            foreach (var item in objFKM111.FKM118)
                            {
                                var objFKM130 = new FKM130();
                                objFKM130.Project = objFKM111.Project;
                                objFKM130.Department = objClsLoginInfo.Department;
                                objFKM130.ItemId = "";
                                objFKM130.PosNo_FixNo = item.FixtureNo;
                                objFKM130.MaterialLocation = objClsLoginInfo.Department;// "FMG";
                                objFKM130.MaterialSubLocation = "";
                                objFKM130.MaterialOwner = MaterialOwner.FMG.GetStringValue();
                                objFKM130.MaterialType = MaterialType.FXR.GetStringValue();
                                objFKM130.PCLNo = "";
                                objFKM130.Qty = 1;
                                objFKM130.Stage = StageType.FixtureCompleted.GetStringValue();
                                objFKM130.TransactionType = TransactionType.Received.GetStringValue();
                                objFKMControl.InsertToInventory(objFKM130);
                            }
                            #endregion

                        }
                        else if (!db.FKM118.Any(x => x.RefLineId == objFKM111.LineId && (x.DeliverStatus == statusIP || x.DeliverStatus == statusCO)))
                            objFKM111.DeliverStatus = statusNS;
                        else
                            objFKM111.DeliverStatus = statusIP;
                    }
                    db.SaveChanges();
                    objResponseMsgWithStatus.Key = true;
                }
                else
                {
                    objResponseMsgWithStatus.Key = false;
                    objResponseMsgWithStatus.Value = clsImplementationMessage.CommonMessages.Notavailable;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsgWithStatus.Key = false;
                objResponseMsgWithStatus.Value = ex.Message;
            }
            return Json(objResponseMsgWithStatus, JsonRequestBehavior.AllowGet);
        }*/

        //update existing column
        [HttpPost]
        public JsonResult UpdateDetails(int id, string columnName, string columnValue)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                bool isValid = true;
                if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
                {
                    if (columnName == "FixtureName" && string.IsNullOrWhiteSpace(columnValue))
                    {
                        isValid = false;
                    }
                    if (isValid)
                    {
                        db.SP_COMMON_LINES_UPDATE(id, columnName, columnValue, "FKM111", objClsLoginInfo.UserName);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Invalid Birthdate";
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveItemDetails(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsgWithStatus = new clsHelper.ResponseMsgWithStatus();
            List<FKM115> lstFKM115 = null;
            List<FKM118> lstFKM118 = new List<FKM118>();

            string ASM = clsImplementationEnum.NodeTypes.ASM.GetStringValue().ToLower();
            string TJF = clsImplementationEnum.NodeTypes.TJF.GetStringValue().ToLower();
            FKM111 objTemp = null;
            decimal OldQty = 0;
            decimal NewQty = 0;

            try
            {
                if (fc != null)
                {
                    int headerId = Convert.ToInt32(fc["RefHeaderId"]);
                    string project = fc["Project"];
                    string fixturename = fc["FixtureName"];

                    if (fc["strAction"] == "add")
                    {
                        //if fixture is reuse/subcontracting , do not allow to add items.
                        var objFixture = db.FKM111.Where(x => x.RefHeaderId == headerId && x.FixtureName == fixturename && x.ParentId == 0).FirstOrDefault();
                        if (objFixture.ReUse)
                        {
                            objResponseMsgWithStatus.Key = false;
                            objResponseMsgWithStatus.Value = objFixture.ReUse ? "Fixture reuse value has been already updated. Please refresh the page." : "Fixture subcontracting value has been already updated. Please refresh the page.";
                            return Json(objResponseMsgWithStatus);
                        }
                    }

                    //Once SOB key generated, do not allow to descrease item qty. Obs Id#17340
                    if (fc["strAction"] != "add")
                    {
                        int lineId = Convert.ToInt32(fc["LineId"]);

                        objTemp = db.FKM111.Where(x => x.LineId == lineId && x.RefHeaderId == headerId).FirstOrDefault();

                        if (!string.IsNullOrWhiteSpace(objTemp.ItemType) && objTemp.ItemType.ToLower() != ASM && objTemp.ItemType.ToLower() != TJF)
                        {
                            OldQty = objTemp.Qty != null ? objTemp.Qty.Value : 0;
                            NewQty = fc["Qty"] == "" ? Decimal.Parse("0") : Convert.ToDecimal(fc["Qty"], CultureInfo.InvariantCulture);

                            if (db.FKM123.Any(x => x.RefLineId == objTemp.ParentId && !string.IsNullOrEmpty(x.SOBKey)))
                            {
                                if (NewQty < OldQty)
                                {
                                    objResponseMsgWithStatus.Key = false;
                                    objResponseMsgWithStatus.Value = "SOB Key generated. you can not descrease qty.";
                                    return Json(objResponseMsgWithStatus);
                                }
                            }
                        }
                    }

                    var dencity = string.IsNullOrEmpty(fc["Density"]) ? "0.0" : fc["Density"];
                    var calculateWt = string.IsNullOrEmpty(fc["CalculatedWeight"]) ? "0.0" : fc["CalculatedWeight"];

                    FKM114 objFKM114 = db.FKM114.Where(x => x.HeaderId == headerId && x.Project == project).FirstOrDefault();
                    if (objFKM114.Status == clsImplementationEnum.FRStatus.Approved.GetStringValue())
                    {
                        objFKM114.RevNo = Convert.ToInt32(objFKM114.RevNo) + 1;
                        objFKM114.Status = clsImplementationEnum.FRStatus.Draft.GetStringValue();
                        objFKM114.ReturnRemark = null;
                        objFKM114.ApprovedOn = null;
                    }
                    int parentFXRId = 0;
                    FKM111 objFKM111 = null;
                    if (fc["strAction"] == "add")
                    {
                        parentFXRId = db.FKM111.Where(x => x.RefHeaderId == headerId && x.FixtureName == fixturename && x.ParentId == 0).Select(x => x.LineId).FirstOrDefault();
                        objFKM111 = new FKM111();
                        objFKM111.ParentId = parentFXRId;
                        var MaxFxrNo = db.FKM111.Where(x => x.Project == project).OrderByDescending(x => x.FXRSrNo).Select(x => x.FXRSrNo).FirstOrDefault();
                        objFKM111.FXRSrNo = Convert.ToInt32(MaxFxrNo) + 1;
                        lstFKM115 = new List<FKM115>();

                        //Obs Id#18961. As per this observation, FindNo will be allocated after successful allocation to PLM. Discussion on 29-12-2018. Reason : FindNo should be unique in PLM
                        // add FindNo to Parent FKM111 when first child item adding
                        //if (!db.FKM111.Any(a => a.ParentId == parentFXRId))
                        //{
                        //    var objFXRFKM111 = db.FKM111.FirstOrDefault(x => x.LineId == parentFXRId);

                        //    if (string.IsNullOrWhiteSpace(objFXRFKM111.FindNo)) // if parent all ready has FindNo
                        //    {
                        //        string MinFindNo = db.FKM111.Where(x => x.Project == project).Min(x => x.FindNo) == null ? Convert.ToString(StartFindNo) : Convert.ToString(Convert.ToInt32(db.FKM111.Where(x => x.Project == project).Min(x => x.FindNo)) - 1);
                        //        if (IsFindNoReachLimit(Convert.ToInt32(MinFindNo)))
                        //        {
                        //            objFXRFKM111.FindNo = MinFindNo;
                        //        }
                        //        else
                        //        {
                        //            objResponseMsgWithStatus.Key = false;
                        //            objResponseMsgWithStatus.Value = "Find no limit exceed...!";
                        //            return Json(objResponseMsgWithStatus);
                        //        }
                        //        db.SaveChanges();
                        //    }
                        //}
                    }
                    else
                    {
                        int lineId = Convert.ToInt32(fc["LineId"]);
                        objFKM111 = db.FKM111.Where(x => x.LineId == lineId && x.RefHeaderId == headerId).FirstOrDefault();
                        parentFXRId = objFKM111.ParentId.Value;
                        lstFKM115 = db.FKM115.Where(x => x.LineId == lineId).ToList();
                    }
                    var str = fc["RevNo"].ToString();
                    int revNo = Convert.ToInt32(str);

                    objFKM111.RefHeaderId = Convert.ToInt32(fc["RefHeaderId"]);
                    objFKM111.Project = fc["Project"];
                    //objFKM111.Document = fc["Document"];
                    objFKM111.RevNo = revNo;
                    objFKM111.FixtureName = fc["FixtureName"];
                    objFKM111.QtyofFixture = Convert.ToInt32(fc["QtyofFixture"]);
                    objFKM111.ItemCode = fc["ItemCode"];
                    objFKM111.ItemCategory = fc["ItemCategory"];
                    objFKM111.DescriptionofItem = fc["DescriptionofItem"];
                    objFKM111.Category = fc["Category"];
                    objFKM111.Material = fc["Material"];
                    objFKM111.MaterialType = fc["MaterialType"];
                    objFKM111.LengthOD = fc["LengthOD"] == "" ? Decimal.Parse("0") : Convert.ToDecimal(fc["LengthOD"], CultureInfo.InvariantCulture);
                    objFKM111.WidthOD = fc["WidthOD"] == "" ? Decimal.Parse("0") : Convert.ToDecimal(fc["WidthOD"], CultureInfo.InvariantCulture);
                    objFKM111.Thickness = fc["Thickness"] == "" ? Decimal.Parse("0") : Convert.ToDecimal(fc["Thickness"], CultureInfo.InvariantCulture);
                    objFKM111.Qty = fc["Qty"] == "" ? Decimal.Parse("0") : Convert.ToDecimal(fc["Qty"], CultureInfo.InvariantCulture);
                    objFKM111.Wt = fc["Wt"] == "" ? Decimal.Parse("0") : Convert.ToDecimal(fc["Wt"], CultureInfo.InvariantCulture);
                    objFKM111.Area = fc["Area"] == "" ? Decimal.Parse("0") : Convert.ToDecimal(fc["Area"], CultureInfo.InvariantCulture);
                    objFKM111.Unit = fc["Unit"];
                    objFKM111.ReqWt = fc["ReqWt"] == "" ? Decimal.Parse("0") : Convert.ToDecimal(fc["ReqWt"], CultureInfo.InvariantCulture);
                    objFKM111.Unit2 = fc["Unit2"];
                    string FxrReqDt = fc["FixRequiredDate"] != null ? fc["FixRequiredDate"].ToString() : "";
                    objFKM111.FixRequiredDate = DateTime.ParseExact(FxrReqDt, @"d/M/yyyy", CultureInfo.InvariantCulture);
                    string FxrMatDt = fc["MaterialReqDate"] != null ? fc["MaterialReqDate"].ToString() : "";
                    objFKM111.MaterialReqDate = DateTime.ParseExact(FxrMatDt, @"d/M/yyyy", CultureInfo.InvariantCulture);
                    objFKM111.StructuralType = fc["StructuralType"] == "" ? null : fc["StructuralType"];
                    objFKM111.Size = Convert.ToString(fc["Size"]);
                    objFKM111.PipeNormalBore = fc["PipeNormalBore"] == "" ? null : fc["PipeNormalBore"];
                    objFKM111.PipeSchedule = fc["PipeSchedule"] == "" ? null : fc["PipeSchedule"];
                    objFKM111.ItemType = fc["ItemType"] == "" ? null : fc["ItemType"];
                    objFKM111.FixMfg = fc["FixMfg"] != null ? fc["FixMfg"].ToString() : "";
                    objFKM111.ReqArea = (fc["ReqArea"] == "" || fc["ReqArea"] == null) ? Decimal.Parse("0") : Convert.ToDecimal(fc["ReqArea"], CultureInfo.InvariantCulture);

                    var objFixFKM111 = db.FKM111.Where(x => x.LineId == parentFXRId && x.RefHeaderId == headerId).FirstOrDefault();
                    objFixFKM111.QtyofFixture = Convert.ToInt32(fc["QtyofFixture"]);
                    objFixFKM111.FixRequiredDate = DateTime.ParseExact(fc["FixRequiredDate"].ToString(), @"d/M/yyyy", CultureInfo.InvariantCulture);
                    objFixFKM111.MaterialReqDate = DateTime.ParseExact(fc["MaterialReqDate"].ToString(), @"d/M/yyyy", CultureInfo.InvariantCulture);
                    objFixFKM111.FixMfg = objFKM111.FixMfg;

                    var tempReUse = Convert.ToString(fc["txtReUse"]);
                    if (tempReUse == "True")
                    {
                        objFKM111.ReUse = true;
                    }
                    else
                    {
                        objFKM111.ReUse = false;
                    }

                    if (fc["strAction"] == "add")
                    {
                        objFKM111.CreatedBy = objClsLoginInfo.UserName;
                        objFKM111.CreatedOn = DateTime.Now;

                        #region Generate FindNo

                        //Obs Id#18961. As per this observation, FindNo will be allocated after successful allocation to PLM. Discussion on 29-12-2018. Reason : FindNo should be unique in PLM
                        //string MinFindNo = db.FKM111.Where(x => x.Project == project).Min(x => x.FindNo) == null ? Convert.ToString(StartFindNo) : Convert.ToString(Convert.ToInt32(db.FKM111.Where(x => x.Project == project).Min(x => x.FindNo)) - 1);
                        //if (IsFindNoReachLimit(Convert.ToInt32(MinFindNo)))
                        //{
                        //    objFKM111.FindNo = MinFindNo;
                        //}
                        //else
                        //{
                        //    objResponseMsgWithStatus.Key = false;
                        //    objResponseMsgWithStatus.Value = "Find no limit exceed...!";
                        //    return Json(objResponseMsgWithStatus);
                        //}

                        #endregion

                        db.FKM111.Add(objFKM111);
                        objResponseMsgWithStatus.Value = "Item saved successfully";
                    }
                    else
                    {
                        int linesId = Convert.ToInt32(fc["LineId"]);
                        string name = fc["FixtureName"];
                        string proj = fc["Project"];

                        objFKM111.EditedBy = objClsLoginInfo.UserName;
                        objFKM111.EditedOn = DateTime.Now;
                        objResponseMsgWithStatus.Value = "Item updated successfully";
                    }

                    var isFirstEntry = fc["isFirstEntry"];
                    if (isFirstEntry == "true" && objFKM111.QtyofFixture > 0)
                    {
                        var objFKM118List = db.FKM118.ToList();
                        clsManager objManager = new clsManager();
                        var fxno = db.FKM111.Where(x => x.LineId == objFKM111.ParentId).Select(x => x.FXRSrNo).FirstOrDefault();
                        for (int i = 1; i <= objFKM111.QtyofFixture; i++)
                        {
                            FKM118 objFKM118 = new FKM118();
                            objFKM118.RefLineId = Convert.ToInt32(objFKM111.ParentId);
                            objFKM118.FixtureNo = objManager.GetFixtureNo(objFKM111.Project, fxno) + "-" + i;
                            objFKM118.CreatedBy = objClsLoginInfo.UserName;
                            objFKM118.CreatedOn = DateTime.Now;
                            objFKM118.FixReqDate = objFixFKM111.FixRequiredDate;

                            if (!objFKM118List.Any(x => x.RefLineId == objFKM118.RefLineId && x.FixtureNo == objFKM118.FixtureNo))
                                lstFKM118.Add(objFKM118);
                        }

                        if (lstFKM118.Count > 0)
                            db.FKM118.AddRange(lstFKM118);
                    }
                    db.SaveChanges();
                    StartFindNo += 1;

                    MaterialCalculationForSingleQuantity(objFKM111, lstFKM115, Convert.ToDecimal(dencity), Convert.ToDecimal(calculateWt), fc["strAction"]);
                    if (fc["strAction"] == "add")
                    {
                        db.FKM115.AddRange(lstFKM115);
                    }

                    //update weight in sub contracting form
                    var objFKM116 = db.FKM116.Where(x => x.RefLineId == parentFXRId).FirstOrDefault();
                    if (objFKM116 != null)
                    {
                        var wt = db.FKM111.Where(x => x.ParentId == parentFXRId).Sum(x => x.Wt);
                        objFKM116.Weight = wt != null ? wt.ToString() : "";
                    }

                    db.SaveChanges();
                    objResponseMsgWithStatus.HeaderId = Convert.ToInt32(fc["HeaderId"]);

                    objResponseMsgWithStatus.Key = true;
                    objResponseMsgWithStatus.RevNo = Convert.ToString(objFKM114.RevNo);
                    objResponseMsgWithStatus.HeaderStatus = objFKM114.Status;

                    // If item qty updated, then deallocate existing item. Obs Id#17340
                    #region Item DeAllocation 

                    bool IsDeAllocation = false;
                    if (fc["strAction"] != "add" && objTemp != null)
                    {
                        if (NewQty > OldQty)
                        {
                            IsDeAllocation = true;

                            var objFKM120List = db.FKM120.Where(u => u.LineId == objTemp.LineId).ToList();
                            if (objFKM120List.Count() > 0)
                            {
                                db.FKM120.RemoveRange(objFKM120List);
                                db.SaveChanges();
                            }

                            var objFKM121List = db.FKM121.Where(u => u.LineId == objTemp.LineId).ToList();
                            if (objFKM121List.Count() > 0)
                            {
                                foreach (var objFKM121 in objFKM121List)
                                {
                                    string errorMsg = string.Empty;
                                    bool IsSucess = NPLTDeallocation(objFKM121, objClsLoginInfo.UserName, objClsLoginInfo.Location, db, ref errorMsg);
                                    if (IsSucess)
                                    {
                                        if (db.FKM121.Any(u => u.Id == objFKM121.Id))
                                        {
                                            db.FKM121.Remove(objFKM121);
                                            db.SaveChanges();
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (fc["strAction"] == "add" || IsDeAllocation == true)
                    {
                        if (objTemp != null && !string.IsNullOrWhiteSpace(objTemp.ItemType) && objTemp.ItemType.ToLower() != ASM && objTemp.ItemType.ToLower() != TJF)
                        {
                            var objFKM118List = db.FKM118.Where(x => x.RefLineId == parentFXRId).ToList();
                            objFKM118List.ForEach(i =>
                            {
                                i.DeliverStatus = string.Empty;
                                i.MaterialStatus = string.Empty;
                                i.KitLocation = string.Empty;
                                i.EditedBy = objClsLoginInfo.UserName;
                                i.EditedOn = DateTime.Now;
                            });
                            db.SaveChanges();
                        }
                    }

                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsgWithStatus.Key = false;
                objResponseMsgWithStatus.Value = ex.Message;
            }
            return Json(objResponseMsgWithStatus);
        }

        [HttpPost]
        public ActionResult ItemsDetails(int parentId)
        {
            ViewBag.ParentId = parentId;
            return PartialView("_GetItemGridPartial");
        }

        [HttpPost]
        public ActionResult GetPartDetails(int lineId)
        {
            PLTPartDetail objPartDetails = new PLTPartDetail();
            bool IsPLT = false;
            var objFKM111 = db.FKM111.Where(x => x.LineId == lineId).FirstOrDefault();
            if (objFKM111 != null)
            {
                if (!string.IsNullOrWhiteSpace(objFKM111.ItemType) && objFKM111.ItemType.Trim().ToLower() == clsImplementationEnum.NodeTypes.PLT.GetStringValue().ToLower())
                {
                    IsPLT = true;
                }

                objPartDetails.Project = objFKM111.Project;
                objPartDetails.Key = objFKM111.ItemCode.Trim();
                objPartDetails.Findnumber = objFKM111.FindNo;
            }

            ViewBag.IsPLT = IsPLT;
            ViewBag.LineId = lineId;
            return PartialView("_GetPartDetails", objPartDetails);
        }

        [HttpPost]
        public ActionResult LoadFKMSData(JQueryDataTableParamModel param)
        {
            try
            {
                double QtyOnHand = 0.00;
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                int lineId = Convert.ToInt32(param.Headerid);

                var objFKM111 = db.FKM111.Where(x => x.LineId == lineId).FirstOrDefault();
                if (objFKM111 != null)
                {
                    string whereCondition = "1=1 and PPOQty > 0"; // Observation 15517 on 04-07-2018

                    //string[] columnName = { "PPONumber", "PPOLine", "PPOLinesequence", "PPOQty", "PONumber", "POLine", "POlineSequence", "POOrderQty", "qtyonhand" };
                    string[] columnName = { "PPONumber", "PPOQty", "PONumber", "POOrderQty" };

                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                    string strSortOrder = string.Empty;
                    if (!string.IsNullOrWhiteSpace(sortColumnName))
                    {
                        strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                    }

                    var lstPam = db.SP_FKMS_GETPARTDETAILS(objFKM111.Project, objFKM111.ItemCode.Trim(), StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                    if (lstPam != null && lstPam.Count > 0)
                    {
                        QtyOnHand = Convert.ToDouble(lstPam.Select(x => x.qtyonhand).FirstOrDefault());
                    }
                    int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                    var res = (from h in lstPam
                               select new[] {
                               Convert.ToString(h.PPONumber),
                               Convert.ToString(h.PPOQty),
                               Convert.ToString(h.PONumber),
                               Convert.ToString(h.POOrderQty),
                               Convert.ToString(h.qtyonhand)
                    }).ToList();

                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        aaData = res,
                        QtyonHand = QtyOnHand,
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = "0",
                        iTotalRecords = "0",
                        aaData = new string[0]
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = string.Empty,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = new string[0]
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult LoadFKMSPLTData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                int lineId = Convert.ToInt32(param.Headerid);

                var objFKM111 = db.FKM111.Where(x => x.LineId == lineId).FirstOrDefault();
                if (objFKM111 != null)
                {
                    string whereCondition = "1=1";

                    string[] columnName = { "PCRNumber", "PCRPosition", "PCRRevision", "PCRStenum", "PCRStatus", "PCLNumber", "PCLStEnum", "PCLStatus", "NumberofPieces" };

                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                    string strSortOrder = string.Empty;
                    if (!string.IsNullOrWhiteSpace(sortColumnName))
                    {
                        strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                    }

                    var lstPam = db.SP_FKMS_GETPLTDETAILS(objFKM111.Project, objFKM111.FindNo, StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                    int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                    var res = (from h in lstPam
                               select new[] {
                               Convert.ToString(h.PCRNumber),
                               Convert.ToString(h.PCRPosition),
                               Convert.ToString(h.PCRRevision),
                               //Convert.ToString(h.PCRStenum ),
                               Convert.ToString(h.PCRStatus),
                               Convert.ToString(h.PCLNumber),
                               //Convert.ToString(h.PCLStEnum),
                               Convert.ToString(h.PCLStatus),
                               Convert.ToString(h.NumberofPieces)
                    }).ToList();

                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        aaData = res,
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = "0",
                        iTotalRecords = "0",
                        aaData = new string[0]
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = string.Empty,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = new string[0]
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult LoadItemsListData(JQueryDataTableParamModel param)
        {
            try
            {
                clsManager objManager = new clsManager();

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                int ParentId = Convert.ToInt32(param.CTQLineHeaderId);
                int RevNo = Convert.ToInt32(param.RevNo);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                strWhere = "1=1 and ParentId = " + ParentId + "";

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (FixtureName like '%" + param.sSearch +
                               "%' or DescriptionofItem like '%" + param.sSearch +
                               "%' or Category like '%" + param.sSearch +
                               "%' or Material like '%" + param.sSearch +
                               "%' or MaterialType like '%" + param.sSearch +
                               "%' or StructuralType like '%" + param.sSearch +
                               "%' or PipeNormalBore like '%" + param.sSearch + "%')";
                }
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_FKMS_GET_FR_ITEMS
                    (
                        StartIndex,
                        EndIndex,
                        strSortOrder,
                        strWhere
                    ).ToList();

                var data = (from fx in lstResult
                            select new[]
                            {
                                Convert.ToString(fx.ROW_NO),
                                Convert.ToString(fx.FixtureName),
                                Convert.ToInt32(fx.FXRSrNo) > 0 ? objManager.GetItemNo(fx.Project,fx.FXRSrNo) : "",
                                Convert.ToString(fx.QtyofFixture),
                                Convert.ToString(fx.DescriptionofItem),
                                Convert.ToString(fx.Category),
                                Convert.ToString(fx.Material),
                                Convert.ToString(fx.MaterialType),
                                Convert.ToString(fx.LengthOD),
                                Convert.ToString(fx.WidthOD),
                                Convert.ToString(fx.Thickness),
                                Convert.ToString(fx.Qty),
                                Convert.ToString(fx.Wt),
                                Convert.ToString(fx.Area),
                                Convert.ToString(fx.Unit),
                                Convert.ToString(fx.ReUse ? "Yes" : "No"),
                                Convert.ToString(fx.ReqWt),
                                Convert.ToString(fx.ReqArea),
                                Convert.ToString(fx.Unit2),
                                Convert.ToString(fx.FixRequiredDate.HasValue ? fx.FixRequiredDate.Value.ToShortDateString() : ""),
                                Convert.ToString(fx.MaterialReqDate.HasValue ? fx.MaterialReqDate.Value.ToShortDateString() : ""),
                                Convert.ToString(fx.SubLineId),
                                Convert.ToString(fx.RefHeaderId),
                            }).ToList();
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult FixtureAttachment(int lineId, int qty = 1, string urlForm = "")
        {
            ViewBag.LineId = lineId;
            ViewBag.Quantity = qty;
            if (urlForm == clsImplementationEnum.WPPIndexType.release.GetStringValue())
            {
                var objFKM111 = db.FKM111_Log.Where(x => x.Id == lineId).FirstOrDefault();
                ViewBag.HeaderId = Convert.ToInt32(objFKM111.RefHeaderId);
                ViewBag.Deliverystatus = Convert.ToString(objFKM111.DeliverStatus);
                ViewBag.Status = Convert.ToString(objFKM111.Status);
                ViewBag.FilderLineId = objFKM111.LineId;
            }
            else
            {
                var objFKM111 = db.FKM111.Where(x => x.LineId == lineId).FirstOrDefault();
                ViewBag.HeaderId = Convert.ToInt32(objFKM111.RefHeaderId);
                ViewBag.Deliverystatus = Convert.ToString(objFKM111.DeliverStatus);
                ViewBag.Status = Convert.ToString(objFKM111.Status);
                ViewBag.FilderLineId = objFKM111.LineId;
            }
            ViewBag.isRoleFKM3 = objClsLoginInfo.ListRoles.Contains(clsImplementationEnum.UserRoleName.FMG3.GetStringValue());
            return PartialView("_FixtureAttachmentsPartial");
        }

        [HttpPost]
        public ActionResult LoadFixtureAttachments(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = string.Empty;


                int RefLineId = Convert.ToInt32(param.CTQLineHeaderId);
                var objFKM111 = db.FKM111_Log.Where(x => x.Id == RefLineId).FirstOrDefault();
                int HeaderId = Convert.ToInt32(objFKM111.RefHeaderId);
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                whereCondition = "1=1 and RefId = " + RefLineId;

                string[] columnName = { "FixtureNo", "CONVERT(nvarchar(20),FixReqDate,103)", "DeliverStatus", "MaterialStatus", "FullkitArea", "KitLocation", "FullkitAreaStatus" };

                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_FKMS_GET_FR_FILE_ATTACHMENT(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                // string ASM = clsImplementationEnum.NodeTypes.ASM.GetStringValue().ToLower();
                // string TJF = clsImplementationEnum.NodeTypes.TJF.GetStringValue().ToLower();
                // string PLT = clsImplementationEnum.NodeTypes.PLT.GetStringValue().ToLower();

                //bool IsNPLT = false;
                //if (db.FKM111_Log.Any(i => i.ItemType.Trim().ToLower() != ASM && i.ItemType.Trim().ToLower() != TJF && i.ItemType.Trim().ToLower() != PLT))
                //    IsNPLT = true;

                List<FCS001> _lstFCS = new List<FCS001>();
                _lstFCS = db.FCS001.Where(w => w.TableName.Contains("FKM118")).ToList();

                string role = param.Roles; //string role = GetUserRole();
                var data = new List<string[]>();
                foreach (var uc in lstResult)
                {
                    var objFKM118Log = db.FKM118_Log.Where(x => x.LogId == uc.Id).FirstOrDefault();
                    string FixReqDate = uc.FixReqDate != null ? Convert.ToDateTime(uc.FixReqDate).ToString("dd/MM/yyyy") : "";
                    string FixReqDateHtml = FixReqDate;
                    string ColorFlag = "";
                    string action = "<center>";
                    string attachment = "<center>";

                   // string rowno = GenerateFixtureRowNo(uc.RefLineId, uc.Id, Convert.ToString(uc.ROW_NO), ref ColorFlag);
                    string rowno = ColorFlag += "<div>&nbsp;" + uc.ROW_NO + "</div></div>";

                    if (role == UserRoleName.FMG3.GetStringValue())
                    {
                        if(_lstFCS.Count(w=>w.TableName == "FKM118//" + HeaderId + "//" + objFKM118Log.RefLineId + "//" + objFKM118Log.Id && w.TableId == objFKM118Log.Id && w.ViewerId == 1) > 0)
                            attachment += Helper.HTMLActionString(uc.Id, "ReportAttchment", "Add/Update Attachment", "fa fa-paperclip", "FixtureDocumentAttachment(" + HeaderId + "," + objFKM118Log.RefLineId + ", " + objFKM118Log.Id + ",'" + uc.DeliverStatus + "')", "", false, "cursor:pointer;color:#32CD32;");
                        else
                            attachment += Helper.HTMLActionString(uc.Id, "ReportAttchment", "Add/Update Attachment", "fa fa-paperclip", "FixtureDocumentAttachment(" + HeaderId + "," + objFKM118Log.RefLineId + ", " + objFKM118Log.Id + ",'" + uc.DeliverStatus + "')", "", false, "cursor:pointer;");
                        //if (Helper.CheckAttachmentUpload("FKM118/" + HeaderId + "/" + objFKM118Log.RefLineId + "/" + objFKM118Log.Id))
                        //{
                        //    attachment += Helper.HTMLActionString(uc.Id, "ReportAttchment", "Add/Update Attachment", "fa fa-paperclip", "FixtureDocumentAttachment(" + HeaderId + "," + objFKM118Log.RefLineId + ", " + objFKM118Log.Id + ",'" + uc.DeliverStatus + "')", "", false, "cursor:pointer;color:#32CD32;");
                        //}
                        //else
                        //{
                        //    attachment += Helper.HTMLActionString(uc.Id, "ReportAttchment", "Add/Update Attachment", "fa fa-paperclip", "FixtureDocumentAttachment(" + HeaderId + "," + objFKM118Log.RefLineId + ", " + objFKM118Log.Id + ",'" + uc.DeliverStatus + "')", "", false, "cursor:pointer;");
                        //}

                        /*if (IsNPLT && ColorFlag == "green" && string.IsNullOrWhiteSpace(uc.MaterialStatus))
                            action += GeneratePartButtonNew(uc.Id, "GenerateSOBKey", "Generate SOB Key", "btn save custompad", "GenerateSOBKey(" + uc.Id + ");");

                        if (uc.MaterialStatus == FRMaterialDeliveryStatus.Request.GetStringValue())
                            action += GeneratePartButtonNew(uc.Id, "UpdateSOBStatus", "Update SOB Status", "btn blue custompad", "UpdateSOBStatus(" + uc.Id + ");");*/

                        //if fixture is subcontracting, it can be complete directly.
                        if (objFKM111.Subcontracting)
                        {
                            if (uc.DeliverStatus != FRMaterialDeliveryStatus.Completed.GetStringValue())
                                action += GeneratePartButtonNew(uc.Id, "Complete", "Complete", "btn submit custompad", "CompleteMaterialStatus(" + HeaderId + "," + objFKM118Log.RefLineId + ", " + objFKM118Log.Id + ");");
                        }
                        else
                        {
                            if (uc.DeliverStatus == FRMaterialDeliveryStatus.Delivered.GetStringValue())
                                action += GeneratePartButtonNew(uc.Id, "Complete", "Complete", "btn submit custompad", "CompleteMaterialStatus(" + HeaderId + "," + objFKM118Log.RefLineId + ", " + objFKM118Log.Id + ");");
                        }
                    }
                    else if (role == UserRoleName.PLNG3.GetStringValue() || role == UserRoleName.PMG3.GetStringValue())
                    {
                        if (uc.DeliverStatus != FRMaterialDeliveryStatus.Completed.GetStringValue())
                        {
                            if (objFKM111 != null && (objFKM111.Status.ToLower() == FRStatus.Draft.GetStringValue().ToLower() || objFKM111.Status.ToLower() == FRStatus.Returned.GetStringValue().ToLower()))
                                FixReqDateHtml = GenerateDateTextboxFor(uc.Id, "", "FixReqDate", !string.IsNullOrWhiteSpace(FixReqDate) ? Convert.ToDateTime(FixReqDate).ToString("yyyy-MM-dd") : "", "UpdateData(this, " + uc.Id + ",'" + (!string.IsNullOrWhiteSpace(FixReqDate) ? Convert.ToDateTime(FixReqDate).ToString("yyyy-MM-dd") : "") + "');");
                        }
                    }

                    //action += GeneratePartButtonNew(uc.Id, "GenerateSOBKey", "Generate SOB Key", "btn save custompad", "GenerateSOBKey(" + uc.Id + ");");
                    //action += GeneratePartButtonNew(uc.Id, "UpdateSOBStatus", "Update SOB Status", "btn blue custompad", "UpdateSOBStatus(" + uc.Id + ");");
                    //action += GeneratePartButtonNew(uc.Id, "Complete", "Complete", "btn submit custompad", "CompleteMaterialStatus(" + HeaderId + "," + uc.RefLineId + ", " + uc.Id + ");");

                    //if (!objFKM111.Subcontracting)
                    //    action += GeneratePartButtonNew(uc.Id, "ViewSOBKey", "View SOB Key", "btn purple paddingp", "ViewSOBKey(" + uc.Id + ");");

                    action += "</center>";
                    attachment += "</center>";

                    data.Add(new[] {
                                Convert.ToString(uc.Id),
                                Convert.ToString(uc.RefLineId),
                                rowno,
                                Convert.ToString(uc.FixtureNo),
                                FixReqDateHtml,
                                Convert.ToString(string.IsNullOrWhiteSpace(uc.DeliverStatus) ? COP_CommonStatus.NS.GetStringValue() : uc.DeliverStatus),
                                Convert.ToString(uc.MaterialStatus),
                                attachment,
                                Convert.ToString(uc.FullkitArea),
                                Convert.ToString(uc.KitLocation),
                                uc.FullkitAreaStatus,
                                action
                    });
                }

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public static string GenerateTextboxFor(int rowId, string status, string columnName, string columnValue = "", string onBlurMethod = "", string onClickMethod = "", bool isReadOnly = false, string inputStyle = "")
        {
            string htmlControl = "";

            string inputID = columnName + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "form-control";

            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";
            if (string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue()))
            {
                htmlControl = "<input disabled type='text' id='" + inputID + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + "  />";
            }
            else
            {
                htmlControl = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + onClickEvent + "  />";
            }

            return htmlControl;
        }

        public static string GenerateDateTextboxFor(int rowId, string status, string columnName, string columnValue = "", string onBlurMethod = "", string onClickMethod = "", bool isReadOnly = false, string inputStyle = "")
        {
            string htmlControl = "";

            string inputID = columnName + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "form-control";

            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur=\"" + onBlurMethod + "\"" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";
            if (string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue()))
            {
                htmlControl = "<input disabled type='date' id='" + inputID + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + "  />";
            }
            else
            {
                htmlControl = "<input type='date' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + onClickEvent + "  />";
            }

            return htmlControl;
        }

        public string GenerateFixtureRowNo(int RefLineId, int RefId, string rowNo, ref string Color)
        {
            decimal totalAllocatedQty = GetFixtureTotalAllocatedQty(RefLineId, RefId);
            decimal totalReqQty = GetFixtureTotalReqQty(null, RefLineId, RefId);

            string ColorFlag = "<div style='display:inline-flex;'>";

            if (totalAllocatedQty > 0)//0
            {
                if (totalAllocatedQty < totalReqQty)//2<5
                {
                    ColorFlag += "<div class='borderleftwarning'></div>";
                }
                else
                {
                    ColorFlag += "<div class='borderleftsuccess'></div>";
                    Color = "green";
                }
            }
            else
            {
                ColorFlag += "<div class='borderleftdanger'></div>";
            }
            ColorFlag += "<div>&nbsp;" + rowNo + "</div></div>";
            return ColorFlag;
        }

        [HttpPost]
        public ActionResult SendFixtureForAprrove(int HeaderId, string strLineIds)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                FKM114 objFKM114 = db.FKM114.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                if (objFKM114 != null)
                {
                    bool IsSentForApproval = false;
                    var list = strLineIds.Split(',').ToList();
                    foreach (var item in list)
                    {
                        int LineId = Convert.ToInt32(item);
                        FKM111 objFKM111 = db.FKM111.Where(x => x.LineId == LineId).FirstOrDefault();
                        if (objFKM111.Status == clsImplementationEnum.FRStatus.Draft.GetStringValue() || objFKM111.Status == clsImplementationEnum.FRStatus.Returned.GetStringValue())
                        {
                            var folderPath = "FKM111//" + HeaderId + "//" + objFKM111.LineId + "//R" + objFKM111.RevNo;
                            var Files = (new clsFileUpload()).GetDocuments(folderPath);

                            Utility.Controllers.FileUploadController _objFUC = new Utility.Controllers.FileUploadController();

                            //if (Files.Count() > 0)
                            if(_objFUC.CheckAnyDocumentsExits(folderPath,objFKM111.LineId))
                            {
                                objFKM111.Status = clsImplementationEnum.FRStatus.SentForApproval.GetStringValue();
                                objFKM111.ApprovedBy = objFKM114.ApprovedBy;

                                objFKM111.ApprovedOn = null;
                                objFKM111.ReturnBy = string.Empty;
                                objFKM111.ReturnOn = null;
                                objFKM111.ReturnRemark = string.Empty;
                                objFKM111.ReviseRemark = string.Empty;
                                if (objFKM111.QtyofFixture < 1 || objFKM111.QtyofFixture == null)
                                {
                                    objFKM111.QtyofFixture = 1;
                                    List<FKM118> lstFKM118 = new List<FKM118>();
                                    var objFKM118List = db.FKM118.ToList();
                                    clsManager objManager = new clsManager();
                                    var fxno = objFKM111.FXRSrNo;

                                    FKM118 objFKM118 = new FKM118();
                                    objFKM118.RefLineId = Convert.ToInt32(objFKM111.LineId);
                                    objFKM118.FixtureNo = objManager.GetFixtureNo(objFKM111.Project, fxno) + "-" + 1;
                                    objFKM118.CreatedBy = objClsLoginInfo.UserName;
                                    objFKM118.CreatedOn = DateTime.Now;
                                    //objFKM118.FixReqDate = objFixFKM111.FixRequiredDate;

                                    if (!objFKM118List.Any(x => x.RefLineId == objFKM118.RefLineId && x.FixtureNo == objFKM118.FixtureNo))
                                        lstFKM118.Add(objFKM118);


                                    if (lstFKM118.Count > 0)
                                        db.FKM118.AddRange(lstFKM118);
                                }
                                db.SaveChanges();

                                #region Send Notification
                                var ApprovedBy = db.FKM116.Where(x => x.RefLineId == objFKM111.LineId).FirstOrDefault();
                                (new clsManager()).SendNotification((clsImplementationEnum.UserRoleName.PLNG1.GetStringValue() + "," + clsImplementationEnum.UserRoleName.PLNG2.GetStringValue() + "," + clsImplementationEnum.UserRoleName.PMG1.GetStringValue() + "," + clsImplementationEnum.UserRoleName.PMG2.GetStringValue()), objFKM111.FKM114.Project, "", "", "Fixture : " + objFKM111.FixtureName + " of Project: " + objFKM111.FKM114.Project + "  has been submitted for your Approval", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/FKMS/MaintainFR/FRDetails/" + HeaderId + "?urlForm=a", (ApprovedBy != null ? ApprovedBy.PlanningLead : ""));
                                #endregion

                                IsSentForApproval = true;
                            }
                            else
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = "Attachment is required";
                                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }

                    if (IsSentForApproval)
                    {
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.FRMessage.SentForApprove;
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Fixture details has been already sent for approval. Please refresh the page.";
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public void InsertPartAndBOMInPLM(int RefLineId, int LineId, bool IsPushFixture, bool IsPushItem, List<string> UsedFindNoList)
        {
            clsManager objManager = new clsManager();
            var objFKM111 = db.FKM111.FirstOrDefault(f => f.LineId == RefLineId);

            HedPLMBaaNProjectCreationWebServiceService plmWebService = new HedPLMBaaNProjectCreationWebServiceService();
            hedplmwebServicesBOMCreationServiceBaaNLNbeanhedPLMBOMData plmBOM = new hedplmwebServicesBOMCreationServiceBaaNLNbeanhedPLMBOMData();
            hedplmwebServicesBOMCreationServiceBaaNLNbeanhedPLMPartData plmPart = new hedplmwebServicesBOMCreationServiceBaaNLNbeanhedPLMPartData();
            //FKM102 parentNode = null;
            int GenerateNo = 0;
            string _defaultPolicy = "Part-CRR";
            string _defaultItemGroup = "MMFG00";
            string _defaultPartType = "Part";
            string _defaultParentPartType = "Part";
            string _defaultChildPartType = "Part";
            //string _defaultPartAction = "ADD";
            string _defaultIsTopPart = "AutoRevise";
            //string _defaultpartQuantity = "1";
            string _defaultLength = "";
            string _defaultWidth = "";
            string _defaultNumberOfPieces = "1";

            //Default values
            plmBOM.childPartType = _defaultChildPartType;
            plmBOM.length = _defaultLength;
            plmBOM.numberOfPieces = _defaultNumberOfPieces;
            plmBOM.owner = objClsLoginInfo.UserName;
            plmBOM.parentPartType = _defaultParentPartType;
            plmBOM.width = _defaultWidth;

            plmPart.isTopPart = _defaultIsTopPart;
            plmPart.itemGroup = _defaultItemGroup;
            plmPart.owner = objClsLoginInfo.UserName;
            plmPart.partType = _defaultPartType;
            plmPart.policy = _defaultPolicy;

            var bomEnt = new FKMSBOMEnt();
            //bomEnt.part.partName = (objFKM111.Project.Length > 7 ? objFKM111.Project.Substring(0, 7) : objFKM111.Project) + "-" + (objFKM111.FXRSrNo.Value).ToString("0000");// GenerateNodeKey(objFKM111.FXRSrNo, "", clsImplementationEnum.FKMSNodeType.Assembly);
            bomEnt.part.partName = objManager.GetFixtureNo(objFKM111.Project, objFKM111.FXRSrNo.Value);
            bomEnt.parentPartName = bomEnt.part.partName;
            //bomEnt.part.IsPWHT = false;
            plmPart.description = objFKM111.FixtureName; //plmPart.description = objFKM111.DescriptionofItem;
            plmPart.partName = bomEnt.part.partName;//objFKM102.NodeKey;
            plmPart.relatedProjectName = objFKM111.Project;

            try
            {
                if (IsPushFixture && !objFKM111.ReUse && !objFKM111.Subcontracting)
                {
                    var partResult = plmWebService.createPart(plmPart);
                    if (partResult.key == 1)
                    {
                        bomEnt.part.IsInsertedInPLM = false;
                        bomEnt.part.error = partResult.value;
                    }
                    else
                    {
                        bomEnt.part.IsInsertedInPLM = true;
                    }
                }
            }
            catch (Exception ex)
            {
                bomEnt.part.IsInsertedInPLM = false;
                bomEnt.part.error = ex.Message.ToString();
            }

            plmBOM.childPartName = bomEnt.part.partName;// objFKM102.NodeKey;
                                                        //plmBOM.findNumber = objFKM111.FindNo;
            if (string.IsNullOrWhiteSpace(objFKM111.FindNo))
                plmBOM.findNumber = GetUniqueFindNo(objFKM111.Project, UsedFindNoList);
            else
                plmBOM.findNumber = objFKM111.FindNo;

            //fetch JIGFIX item name            
            string query = "select top(1) ltrim(case a.t_item when '' then a.t_nitm else a.t_item end) t_item " +
                              "from " + LNLinkedServer + ".dbo.tltpdm105175 a where a.t_cprj = '" + objFKM111.Project + "' and a.t_item like '%JIGFIX%'";

            plmBOM.parentPartName = db.Database.SqlQuery<string>(query).FirstOrDefault();
            plmBOM.action = objFKM111.IsInsertedInPLM == true ? "UPDATE" : "ADD";
            var objSubFKM111 = db.FKM111.Where(w => w.ParentId == RefLineId).ToList();

            var objFixtureQty = objSubFKM111.Select(s => s.QtyofFixture).FirstOrDefault();
            plmBOM.quantity = (objFixtureQty != null ? objFixtureQty.Value : 0) + "";

            try
            {
                if (IsPushFixture && !objFKM111.ReUse && !objFKM111.Subcontracting)
                {
                    var partResult = plmWebService.createBOMAutoRevise(plmBOM);
                    if (partResult.key == 1)
                    {
                        bomEnt.IsInsertedInPLM = false;
                        bomEnt.error = partResult.value;
                    }
                    else
                    {
                        bomEnt.IsInsertedInPLM = true;
                    }
                }
            }
            catch (Exception ex)
            {
                bomEnt.IsInsertedInPLM = false;
                bomEnt.error = ex.Message.ToString();
            }

            if (IsPushFixture && !objFKM111.ReUse && !objFKM111.Subcontracting)
            {
                if (bomEnt.IsInsertedInPLM && bomEnt.part.IsInsertedInPLM)
                {
                    objFKM111.IsInsertedInPLM = true;
                    objFKM111.FindNo = plmBOM.findNumber;
                }
                else
                {
                    objFKM111.IsInsertedInPLM = false;
                }
                objFKM111.PLMError = (bomEnt.part.error != null && bomEnt.part.error.Length > 0 ? "Part Error : " + bomEnt.part.error : "") + (bomEnt.error != null && bomEnt.error.Length > 0 ? ",BOM Error : " + bomEnt.error : "");
                db.SaveChanges();
            }

            #region Push Item 

            if (IsPushItem)
            {
                if (LineId != 0 && objSubFKM111.Count() > 0)
                    objSubFKM111 = objSubFKM111.Where(x => x.LineId == LineId).ToList();

                // create BOM For items
                foreach (var item in objSubFKM111)
                {
                    if (!item.ReUse && !item.Subcontracting)//Obs Id#17375
                    {
                        //as per observation id 19563
                        //bomEnt.part.partName = (item.Project.Length > 7 ? item.Project.Substring(0, 7) : item.Project) + "-" + (item.FXRSrNo.Value).ToString("0000");//GenerateNodeKey(++GenerateNo, "" ,clsImplementationEnum.FKMSNodeType.Self);
                        bomEnt.part.partName = item.ItemCode.Trim();

                        plmBOM.childPartName = bomEnt.part.partName;
                        //plmBOM.findNumber = item.FindNo;                       
                        if (string.IsNullOrWhiteSpace(item.FindNo))
                            plmBOM.findNumber = GetUniqueFindNo(item.Project, UsedFindNoList);
                        else
                            plmBOM.findNumber = item.FindNo;

                        plmBOM.parentPartName = bomEnt.parentPartName;
                        plmBOM.quantity = (item.Qty != null ? item.Qty.Value : 0) + "";
                        plmBOM.action = item.IsInsertedInPLM == true ? "UPDATE" : "ADD";
                        plmBOM.length = item.LengthOD != null ? Convert.ToString(item.LengthOD) : "";
                        plmBOM.width = item.WidthOD != null ? Convert.ToString(item.WidthOD) : "";
                        plmBOM.numberOfPieces = plmBOM.quantity;
                        try
                        {
                            var partResult = plmWebService.createBOMAutoRevise(plmBOM);
                            if (partResult.key == 1)
                            {
                                item.IsInsertedInPLM = false;
                                item.PLMError = partResult.value;
                            }
                            else
                            {
                                item.IsInsertedInPLM = true;
                                item.FindNo = plmBOM.findNumber;
                            }
                        }
                        catch (Exception ex)
                        {
                            item.IsInsertedInPLM = false;
                            item.PLMError = ex.Message.ToString();
                        }
                        db.SaveChanges();
                    }
                }
            }

            #endregion
        }

        public clsHelper.ResponseMsg DeletePartAndBOMFromPLM(int LineId, bool IsDeleteFixture, bool IsDeleteItem)
        {
            clsManager objManager = new clsManager();
            clsHelper.ResponseMsg objResponse = new clsHelper.ResponseMsg();
            objResponse.Key = false;

            var objFKM111 = db.FKM111.FirstOrDefault(f => f.LineId == LineId);

            HedPLMBaaNProjectCreationWebServiceService plmWebService = new HedPLMBaaNProjectCreationWebServiceService();
            hedplmwebServicesBOMCreationServiceBaaNLNbeanhedPLMBOMData plmBOM = new hedplmwebServicesBOMCreationServiceBaaNLNbeanhedPLMBOMData();
            hedplmwebServicesBOMCreationServiceBaaNLNbeanhedPLMPartData plmPart = new hedplmwebServicesBOMCreationServiceBaaNLNbeanhedPLMPartData();
            //FKM102 parentNode = null;
            int GenerateNo = 0;
            string _defaultPolicy = "Part-CRR";
            string _defaultItemGroup = "MMFG00";
            string _defaultPartType = "Part";
            string _defaultParentPartType = "Part";
            string _defaultChildPartType = "Part";
            //string _defaultPartAction = "ADD";
            string _defaultIsTopPart = "AutoRevise";
            //string _defaultpartQuantity = "1";
            string _defaultLength = "";
            string _defaultWidth = "";
            string _defaultNumberOfPieces = "1";

            //Default values
            plmBOM.childPartType = _defaultChildPartType;
            plmBOM.length = _defaultLength;
            plmBOM.numberOfPieces = _defaultNumberOfPieces;
            plmBOM.owner = objClsLoginInfo.UserName;
            plmBOM.parentPartType = _defaultParentPartType;
            plmBOM.width = _defaultWidth;

            plmPart.isTopPart = _defaultIsTopPart;
            plmPart.itemGroup = _defaultItemGroup;
            plmPart.owner = objClsLoginInfo.UserName;
            plmPart.partType = _defaultPartType;
            plmPart.policy = _defaultPolicy;

            var bomEnt = new FKMSBOMEnt();
            //bomEnt.part.partName = (objFKM111.Project.Length > 7 ? objFKM111.Project.Substring(0, 7) : objFKM111.Project) + "-" + (objFKM111.FXRSrNo.Value).ToString("0000");// GenerateNodeKey(objFKM111.FXRSrNo, "", clsImplementationEnum.FKMSNodeType.Assembly);
            bomEnt.part.partName = objManager.GetFixtureNo(objFKM111.Project, objFKM111.FXRSrNo.Value);
            bomEnt.parentPartName = bomEnt.part.partName;
            //bomEnt.part.IsPWHT = false;
            plmPart.description = objFKM111.FixtureName; //plmPart.description = objFKM111.DescriptionofItem;
            plmPart.partName = bomEnt.part.partName;//objFKM102.NodeKey;
            plmPart.relatedProjectName = objFKM111.Project;

            //try
            //{
            //    if (objFKM111.IsInsertedInPLM.Value == true && IsDeleteFixture)
            //    {
            //        var partResult = plmWebService.createPart(plmPart);
            //        if (partResult.key == 1)
            //        {
            //            bomEnt.part.IsInsertedInPLM = false;
            //            bomEnt.part.error = partResult.value;
            //        }
            //        else
            //        {
            //            bomEnt.part.IsInsertedInPLM = true;
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    bomEnt.part.IsInsertedInPLM = false;
            //    bomEnt.part.error = ex.Message.ToString();
            //}

            plmBOM.childPartName = bomEnt.part.partName;
            plmBOM.findNumber = objFKM111.FindNo;

            //fetch JIGFIX item name            
            string query = "select top(1) ltrim(case a.t_item when '' then a.t_nitm else a.t_item end) t_item " +
                              "from " + LNLinkedServer + ".dbo.tltpdm105175 a where a.t_cprj = '" + objFKM111.Project + "' and a.t_item like '%JIGFIX%'";

            plmBOM.parentPartName = db.Database.SqlQuery<string>(query).FirstOrDefault();
            plmBOM.action = "DELETE";
            plmBOM.quantity = (objFKM111.QtyofFixture != null ? objFKM111.QtyofFixture : 0) + "";
            try
            {
                if (objFKM111.IsInsertedInPLM != null && objFKM111.IsInsertedInPLM.Value == true && IsDeleteFixture)
                {
                    var partResult = plmWebService.createBOMAutoRevise(plmBOM);
                    if (partResult.key == 1)
                    {
                        bomEnt.error = partResult.value;
                        objResponse.Key = false;
                        objResponse.Value = partResult.value;
                    }
                    else
                    {
                        objResponse.Key = true;
                    }
                }
            }
            catch (Exception ex)
            {
                bomEnt.error = ex.Message.ToString();
                objResponse.Key = false;
                objResponse.Value = ex.Message.ToString();
            }

            if (objFKM111.IsInsertedInPLM != null && objFKM111.IsInsertedInPLM.Value == true && IsDeleteFixture)
            {
                if (!objResponse.Key)
                    objFKM111.PLMError = (bomEnt.part.error != null && bomEnt.part.error.Length > 0 ? "Delete Part Error : " + bomEnt.part.error : "") + (bomEnt.error != null && bomEnt.error.Length > 0 ? ",Delete BOM Error : " + bomEnt.error : "");

                db.SaveChanges();
            }

            #region Delete Item 

            if (objFKM111.IsInsertedInPLM != null && objFKM111.IsInsertedInPLM.Value == true && IsDeleteItem)
            {
                //bomEnt.part.partName = (objFKM111.Project.Length > 7 ? objFKM111.Project.Substring(0, 7) : objFKM111.Project) + "-" + (objFKM111.FXRSrNo.Value).ToString("0000");//GenerateNodeKey(++GenerateNo, "" ,clsImplementationEnum.FKMSNodeType.Self);
                bomEnt.part.partName = objFKM111.ItemCode.Trim();

                plmBOM.childPartName = bomEnt.part.partName;
                plmBOM.findNumber = objFKM111.FindNo;
                plmBOM.parentPartName = bomEnt.parentPartName;
                plmBOM.quantity = (objFKM111.Qty != null ? objFKM111.Qty.Value : 0) + "";
                plmBOM.action = "DELETE";
                plmBOM.length = objFKM111.LengthOD != null ? Convert.ToString(objFKM111.LengthOD) : "";
                plmBOM.width = objFKM111.WidthOD != null ? Convert.ToString(objFKM111.WidthOD) : "";
                plmBOM.numberOfPieces = plmBOM.quantity;
                try
                {
                    var partResult = plmWebService.createBOMAutoRevise(plmBOM);
                    if (partResult.key == 1)
                    {
                        objFKM111.PLMError = partResult.value;
                        objResponse.Key = false;
                        objResponse.Value = objFKM111.PLMError;
                    }
                    else
                    {
                        objResponse.Key = true;
                    }
                }
                catch (Exception ex)
                {
                    objFKM111.PLMError = ex.Message.ToString();
                    objResponse.Key = false;
                    objResponse.Value = objFKM111.PLMError;
                }
                db.SaveChanges();
            }

            #endregion

            return objResponse;
        }

        public string GetUniqueFindNo(string Project, List<string> UsedFindNoList)
        {
            string result = StartFindNo.ToString();

            string MinFindNo = db.FKM111.Where(x => x.Project == Project).Min(x => x.FindNo);
            if (!string.IsNullOrWhiteSpace(MinFindNo))
            {
                result = Convert.ToString(Convert.ToInt32(MinFindNo) - 1);
            }

            bool IsOk = false;
            while (!IsOk)
            {
                if (UsedFindNoList.Count() > 0)
                {
                    //ensure find no does not duplicate
                    if (UsedFindNoList.Any(x => x.ToString() == result))
                    {
                        result = Convert.ToString(Convert.ToInt32(result) - 1);
                    }
                    else
                    {
                        IsOk = true;
                        break;
                    }
                }
                else
                {
                    IsOk = true;
                    break;
                }
            }

            return result;
        }

        public List<string> GetUsedFindNoList(string Project)
        {
            //Query to check find no uniqueness given by saajan sir on 29/12/2018.
            //SELECT DISTINCT FindNo FROM VW_IPI_GETHBOMLIST where Project = '0017012A' and ParentPart LIKE '%JIGFI%'

            List<string> UsedFindNoList = db.VW_IPI_GETHBOMLIST.Where(x => x.Project == Project && x.ParentPart.Contains("JIGFIX")).Select(x => x.FindNo).Distinct().ToList();
            return UsedFindNoList;
        }

        public string GenerateNodeKey(int GenerateNo, string NodeName, clsImplementationEnum.FKMSNodeType Type)
        {
            string NodeKeyupper = string.Empty;
            NodeKeyupper = (NodeName.Length > 3 ? NodeName.Substring(0, 3) : NodeName);
            string NodeKey = NodeKeyupper.ToUpper();
            if (Type == clsImplementationEnum.FKMSNodeType.Assembly)
            {
                NodeKey += (++GenerateNo).ToString("0000") + "ASM";
            }
            else if (Type == clsImplementationEnum.FKMSNodeType.Self)
            {
                NodeKey += (++GenerateNo).ToString("0000") + "SUB-ASM";
            }
            else if (Type == clsImplementationEnum.FKMSNodeType.InternalAssembly)
            {
                NodeKey += (++GenerateNo).ToString("0000") + "ASM-INT-ASM";
            }
            else if (Type == clsImplementationEnum.FKMSNodeType.ExternalAssembly)
            {
                NodeKey += (++GenerateNo).ToString("0000") + "ASM-EXT-ASM";
            }
            else if (Type == clsImplementationEnum.FKMSNodeType.NozzelAssembly)
            {
                NodeKey += (++GenerateNo).ToString("0000") + "ASM-NOZ-ASM";
            }
            return NodeKey;
        }

        [HttpPost]
        public ActionResult ApproveFixture(string Project, string strLineIds)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            clsManager objManager = new clsManager();
            try
            {
                //List<string> UsedFindNoList = GetUsedFindNoList(Project);

                bool IsApproved = false;
                bool IsQtyInValid = false;
                var list = strLineIds.Split(',').ToList();
                string FixtureList = string.Empty;
                string CompleteEnum = clsImplementationEnum.FRMaterialDeliveryStatus.Completed.GetStringValue();

                foreach (var item in list)
                {
                    int RefLineId = Convert.ToInt32(item);
                    var objFKM111 = db.FKM111.Where(x => x.LineId == RefLineId).FirstOrDefault();
                    if (objFKM111 != null && objFKM111.Status == clsImplementationEnum.FRStatus.SentForApproval.GetStringValue())
                    {
                        var obj111Log = db.FKM111_Log.Where(x => x.LineId == RefLineId && x.RevNo == (objFKM111.RevNo - 1)).FirstOrDefault();
                        if (obj111Log != null)
                        {
                            var obj118Log = db.FKM118_Log.Where(x => x.RefLineId == objFKM111.LineId && x.RefId == obj111Log.Id && x.DeliverStatus == CompleteEnum).ToList();
                            if (!(objFKM111.QtyofFixture >= obj118Log.Count()))
                            {
                                IsQtyInValid = true;
                                if (string.IsNullOrEmpty(FixtureList))
                                    FixtureList = objManager.GetFixtureNo(objFKM111.Project, objFKM111.FXRSrNo);
                                else
                                    FixtureList = FixtureList + ", " + objManager.GetFixtureNo(objFKM111.Project, objFKM111.FXRSrNo);
                            }
                        }
                    }
                }

                if (IsQtyInValid && (!string.IsNullOrEmpty(FixtureList)))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = ("Can not approve fixture(s) " + FixtureList + " as quantity is less than completed quantity");
                }
                else
                {
                    var listEmployeeFMG1 = Manager.GetDepartmentRoleWiseEmployee(objClsLoginInfo.Location, "", UserRoleName.FMG1.GetStringValue());
                    var listEmployeeFMG2 = Manager.GetDepartmentRoleWiseEmployee(objClsLoginInfo.Location, "", UserRoleName.FMG2.GetStringValue());
                   
                    string FMG1CC = string.Empty;
                    string FMG2CC = string.Empty;

                    foreach (var FMG1 in listEmployeeFMG1)
                    {
                        if (string.IsNullOrEmpty(FMG1CC))
                            FMG1CC = Manager.GetMailIdFromPsNo(FMG1.psno);
                        else
                            FMG1CC = FMG1CC + ";" + Manager.GetMailIdFromPsNo(FMG1.psno);
                    }

                    foreach (var FMG2 in listEmployeeFMG2)
                    {
                        if (string.IsNullOrEmpty(FMG2CC))
                            FMG2CC = Manager.GetMailIdFromPsNo(FMG2.psno);
                        else
                            FMG2CC = FMG2CC + ";" + Manager.GetMailIdFromPsNo(FMG2.psno);
                    }

                    foreach (var item in list)
                    {
                        int RefLineId = Convert.ToInt32(item);
                        var objFKM111 = db.FKM111.Where(x => x.LineId == RefLineId).FirstOrDefault();
                        if (objFKM111 != null && objFKM111.Status == clsImplementationEnum.FRStatus.SentForApproval.GetStringValue())
                        {
                                objFKM111.Status = clsImplementationEnum.FRStatus.Approved.GetStringValue();
                                objFKM111.ApprovedOn = DateTime.Now;

                                var objFKM116 = db.FKM116.Where(x => x.RefLineId == RefLineId).FirstOrDefault();
                                if (objFKM116 != null)
                                    objFKM116.Date = objFKM111.ApprovedOn;
                            objFKM111.IsInsertedInPLM = true;
                            objFKM111.PLMError = "";

                            db.SaveChanges();

                            #region Send Notification
                            (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.FMG3.GetStringValue(), objFKM111.FKM114.Project, "", "", "Fixture : " + objFKM111.FixtureName + " of Project: " + objFKM111.FKM114.Project + "  has been Approved", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/FKMS/MaintainFR/FKMIndex", objFKM116 != null ? objFKM116.Fixturemanufacturer : "");
                            #endregion


                            #region Send E-Mail
                            string emailTo = Manager.GetMailIdFromPsNo(objFKM116.Fixturemanufacturer);
                            if (!string.IsNullOrWhiteSpace(emailTo))
                            {
                                Hashtable _ht = new Hashtable();
                                EmailSend _objEmail = new EmailSend();
                                string cc = (!string.IsNullOrEmpty(FMG1CC) ? ";" + FMG1CC : "");
                                if (!string.IsNullOrEmpty(cc))
                                    cc = cc + (!string.IsNullOrEmpty(FMG2CC) ? ";" + FMG2CC : "");
                                else
                                    cc = (!string.IsNullOrEmpty(FMG2CC) ? ";" + FMG2CC : "");

                                _objEmail.MailCc = Manager.GetMailIdFromPsNo(objFKM116.Initiator) + cc;
                                _ht["[FMG]"] = objFKM116.Fixturemanufacturer + " - " + Manager.GetUserNameFromPsNo(objFKM116.Fixturemanufacturer);
                                _ht["[SCR_Number]"] = objFKM116.FRNo + " - " + objFKM111.FixtureName;
                                _ht["[Initiator]"] = objFKM116.Initiator;
                                MAIL001 objTemplateMaster = db.MAIL001.Where(ii => ii.TamplateName == MailTemplates.SCR.SCRInitiator).SingleOrDefault();
                                _objEmail.MailToAdd = emailTo;
                                _objEmail.SendMail(_objEmail, _ht, objTemplateMaster);
                            }
                            #endregion
                            IsApproved = true;
                                /* Obs. 27067: as per SOR for FKMS System*/
                                //if (!objFKM111.ReUse && !objFKM111.Subcontracting)
                                //    InsertPartAndBOMInPLM(RefLineId, 0, true, true, UsedFindNoList);
                        }
                    }

                    if (IsApproved)
                    {
                        InsertFixtureLogDetail("FKM114", Project, 0);
                        foreach (var item in list)
                        {
                            int lineId = Convert.ToInt32(item);
                            InsertFixtureLogDetail("FKM111", Project, lineId);
                            InsertFixtureLogDetail("FKM116", Project, lineId);
                            InsertFixtureLogDetail("FKM118", Project, lineId);
                        }
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Approve.ToString();
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Status has been already updated. Please refresh the page.";
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public void InsertFixtureLogDetail(string type, string project, int lineId)
        {
            try
            {
                if (type == "FKM114")
                {
                    #region FKM114

                    var GetLogRevNo = db.FKM114.Where(x => x.Project.ToLower() == project.ToLower()).FirstOrDefault().RevNo ?? 0;

                    if (!db.FKM114_Log.Any(x => x.Project.ToLower() == project.ToLower() && x.RevNo == GetLogRevNo))
                    {
                        var objData = db.FKM114.Where(x => x.Project.ToLower() == project.ToLower()).FirstOrDefault();
                        FKM114_Log objLog = new FKM114_Log();

                        objLog.HeaderId = objData.HeaderId;
                        objLog.Project = objData.Project;
                        objLog.Customer = objData.Customer;
                        objLog.ZeroDate = objData.ZeroDate;
                        objLog.CDD = objData.CDD;
                        objLog.RevNo = objData.RevNo;
                        objLog.Status = clsImplementationEnum.CommonStatus.Approved.GetStringValue();
                        objLog.CreatedBy = objData.CreatedBy;
                        objLog.CreatedOn = objData.CreatedOn;
                        objLog.EditedBy = objData.EditedBy;
                        objLog.EditedOn = objData.EditedOn;
                        objLog.ApprovedBy = objData.ApprovedBy;
                        objLog.ApprovedOn = objData.ApprovedOn;
                        objLog.ReturnRemark = objData.ReturnRemark;
                        objLog.ReviseRemark = objData.ReviseRemark;
                        objLog.SubmittedBy = objData.SubmittedBy;
                        objLog.SubmittedOn = objData.SubmittedOn;
                        objLog.ProductType = objData.ProductType;
                        objLog.ReturnBy = objData.ReturnBy;
                        objLog.ReturnOn = objData.ReturnOn;

                        db.FKM114_Log.Add(objLog);
                        db.SaveChanges();

                        var RevList114 = db.FKM114_Log.Where(x => x.Project.ToLower() == project.ToLower() && x.RevNo < GetLogRevNo).ToList();
                        RevList114.ForEach(a => a.Status = clsImplementationEnum.CommonStatus.Superseded.GetStringValue());
                        db.SaveChanges();
                    }
                    #endregion
                }
                else if (type == "FKM111")
                {
                    #region FKM111

                    var GetLogRevNo = db.FKM111.Where(x => x.LineId == lineId).FirstOrDefault().RevNo ?? 0;

                    if (!db.FKM111_Log.Any(x => x.LineId == lineId && x.RevNo == GetLogRevNo))
                    {
                        var objData = db.FKM111.Where(x => x.LineId == lineId).FirstOrDefault();
                        FKM111_Log objLog = new FKM111_Log();

                        objLog.LineId = objData.LineId;
                        objLog.QtyofFixture = objData.QtyofFixture;
                        objLog.DescriptionofItem = objData.DescriptionofItem;
                        objLog.Category = objData.Category;
                        objLog.Material = objData.Material;
                        objLog.MaterialType = objData.MaterialType;
                        objLog.LengthOD = objData.LengthOD;
                        objLog.WidthOD = objData.WidthOD;
                        objLog.Thickness = objData.Thickness;
                        objLog.Qty = objData.Qty;
                        objLog.Wt = objData.Wt;
                        objLog.Area = objData.Area;
                        objLog.Unit = objData.Unit;
                        objLog.ReUse = objData.ReUse;
                        objLog.ReqWt = objData.ReqWt;
                        objLog.Unit2 = objData.Unit2;
                        objLog.FixRequiredDate = objData.FixRequiredDate;
                        objLog.MaterialReqDate = objData.MaterialReqDate;
                        objLog.TotalMaterialRequirement = objData.TotalMaterialRequirement;
                        objLog.FreshMaterialRequirement = objData.FreshMaterialRequirement;
                        objLog.ReUsePercent = objData.ReUsePercent;
                        objLog.BudgetedMaterial = objData.BudgetedMaterial;
                        objLog.RequiredMaterial = objData.RequiredMaterial;
                        objLog.ReuseofMaterial = objData.ReuseofMaterial;
                        objLog.EstimatedMaterial = objData.EstimatedMaterial;
                        objLog.StructuralType = objData.StructuralType;
                        objLog.Size = objData.Size;
                        objLog.PipeNormalBore = objData.PipeNormalBore;
                        objLog.PipeSchedule = objData.PipeSchedule;
                        objLog.CreatedBy = objData.CreatedBy;
                        objLog.CreatedOn = objData.CreatedOn;
                        objLog.EditedBy = objData.EditedBy;
                        objLog.EditedOn = objData.EditedOn;
                        objLog.ParentId = objData.ParentId;
                        objLog.Project = objData.Project;
                        objLog.FXRSrNo = objData.FXRSrNo;
                        objLog.DocNo = objData.DocNo;
                        objLog.FixtureName = objData.FixtureName;
                        objLog.RevNo = objData.RevNo;
                        objLog.IsManual = objData.IsManual;
                        objLog.RefHeaderId = objData.RefHeaderId;
                        objLog.FindNo = objData.FindNo;
                        objLog.IsInsertedInPLM = objData.IsInsertedInPLM;
                        objLog.PLMError = objData.PLMError;
                        objLog.IsSubcontractFormSubmit = objData.IsSubcontractFormSubmit;
                        objLog.RefFixtureReuse = objData.RefFixtureReuse;
                        objLog.Status = objData.Status;
                        objLog.ItemCode = objData.ItemCode;
                        objLog.ItemCategory = objData.ItemCategory;
                        objLog.ApprovedBy = objData.ApprovedBy;
                        objLog.ApprovedOn = objData.ApprovedOn;
                        objLog.ReturnRemark = objData.ReturnRemark;
                        objLog.ReviseRemark = objData.ReviseRemark;
                        objLog.ReturnBy = objData.ReturnBy;
                        objLog.ReturnOn = objData.ReturnOn;
                        objLog.FixMfg = objData.FixMfg;
                        objLog.ItemType = objData.ItemType;
                        objLog.DeliverStatus = objData.DeliverStatus;
                        objLog.MaterialStatus = objData.MaterialStatus;
                        objLog.KitLocation = objData.KitLocation;
                        objLog.FixtureRefId = objData.FixtureRefId;
                        objLog.Subcontracting = objData.Subcontracting;
                        objLog.ReqArea = objData.ReqArea;
                        objLog.SubLocation = objData.SubLocation;

                        var objData114 = db.FKM114_Log.Where(x => x.Project.ToLower() == project.ToLower() && x.RevNo == GetLogRevNo).FirstOrDefault();
                        objLog.RefId = objData114.Id;
                        db.FKM111_Log.Add(objLog);
                        db.SaveChanges();

                        var RevList111 = db.FKM111_Log.Where(x => x.LineId == lineId && x.RevNo < GetLogRevNo).ToList();
                        RevList111.ForEach(a => a.Status = clsImplementationEnum.CommonStatus.Superseded.GetStringValue());
                        db.SaveChanges();
                    }
                    #endregion
                }
                else if (type == "FKM116")
                {
                    #region FKM116

                    var GetLogRevNo = db.FKM116.Where(x => x.RefLineId == lineId).FirstOrDefault().RevNo ?? 0;
                    if (!db.FKM116_Log.Any(x => x.RefLineId == lineId && x.RevNo == GetLogRevNo))
                    {
                        var objData = db.FKM116.Where(x => x.RefLineId == lineId).FirstOrDefault();
                        var objDataLog = db.FKM116_Log.Where(x => x.RefLineId == lineId && x.RevNo < GetLogRevNo).ToList().OrderByDescending(x => x.RevNo).FirstOrDefault();
                        if (objDataLog != null)
                        {
                            objData.WorkCompletioncommittedbyLEMF = objDataLog.WorkCompletioncommittedbyLEMF;
                            objData.PCLNoOutbound = objDataLog.PCLNoOutbound;
                            objData.AllocatedContractor = objDataLog.AllocatedContractor;
                            db.SaveChanges();
                        }

                        FKM116_Log objLog = new FKM116_Log();
                        objLog.LineId = objData.LineId;
                        objLog.RefLineId = objData.RefLineId;
                        objLog.FRNo = objData.FRNo;
                        objLog.RevNo = objData.RevNo;
                        objLog.Date = objData.Date;
                        objLog.InitiatorDept = objData.InitiatorDept;
                        objLog.ProjectNo = objData.ProjectNo;
                        objLog.TypeOfWork = objData.TypeOfWork;
                        objLog.BriefScopeOfWork = objData.BriefScopeOfWork;
                        objLog.RefDrgNo = objData.RefDrgNo;
                        objLog.UnitOfMeasurement = objData.UnitOfMeasurement;
                        objLog.MaterialAvailablityDate = objData.MaterialAvailablityDate;
                        objLog.Weight = objData.Weight;
                        objLog.TentativeJobStartDate = objData.TentativeJobStartDate;
                        objLog.DeliveryDateRequired = objData.DeliveryDateRequired;
                        objLog.WorkCompletioncommittedbyLEMF = objData.WorkCompletioncommittedbyLEMF;
                        objLog.PreferredContractor = objData.PreferredContractor;
                        objLog.Initiator = objData.Initiator;
                        objLog.PlanningLead = objData.PlanningLead;
                        objLog.CreatedBy = objData.CreatedBy;
                        objLog.CreatedOn = objData.CreatedOn;
                        objLog.EditedBy = objData.EditedBy;
                        objLog.EditedOn = objData.EditedOn;
                        objLog.RefHeaderId = objData.RefHeaderId;
                        objLog.AllocatedContractor = objData.AllocatedContractor;
                        objLog.IsViewByFixMFg = objData.IsViewByFixMFg;
                        objLog.IsMaterialAvailable = objData.IsMaterialAvailable;
                        objLog.DispatchlocationContactperson = objData.DispatchlocationContactperson;
                        objLog.NDTRequirement = objData.NDTRequirement;
                        objLog.Subcontractingtype = objData.Subcontractingtype;
                        objLog.TypeofSaddle = objData.TypeofSaddle;
                        objLog.Typeoffixture = objData.Typeoffixture;
                        objLog.Fixturemanufacturer = objData.Fixturemanufacturer;
                        objLog.SCRNo = objData.SCRNo;
                        objLog.PCLNoOutbound = objData.PCLNoOutbound;
                        objLog.SrNo = objData.SrNo;

                        var objData111 = db.FKM111_Log.Where(x => x.LineId == lineId && x.RevNo == GetLogRevNo).FirstOrDefault();
                        objLog.RefId = objData111.Id;
                        db.FKM116_Log.Add(objLog);
                        db.SaveChanges();
                    }
                    #endregion
                }
                else if (type == "FKM118")
                {
                    #region FKM118

                    var lstData = db.FKM118.Where(x => x.RefLineId == lineId).ToList();
                    var lst111LogData = db.FKM111_Log.Where(x => x.LineId == lineId).ToList();
                    foreach (var item111 in lst111LogData)
                    {
                        if (!db.FKM118_Log.Any(x => x.RefId == item111.Id))
                        {
                            foreach (var item in lstData)
                            {
                                if (!db.FKM118_Log.Any(x => x.Id == item.RefLineId))
                                {
                                    var objData = db.FKM118.Where(x => x.Id == item.Id).FirstOrDefault();

                                    var objDataLog = db.FKM118_Log.Where(x => x.RefLineId == lineId && x.FixtureNo == objData.FixtureNo).ToList().OrderByDescending(x => x.LogId).FirstOrDefault();
                                    if (objDataLog != null)
                                    {
                                        objData.DeliverStatus = objDataLog.DeliverStatus;
                                        db.SaveChanges();
                                    }
                                    FKM118_Log objLog = new FKM118_Log();

                                    objLog.Id = objData.Id;
                                    objLog.RefLineId = objData.RefLineId;
                                    objLog.FixtureNo = objData.FixtureNo;
                                    objLog.CreatedBy = objData.CreatedBy;
                                    objLog.CreatedOn = objData.CreatedOn;
                                    objLog.EditedBy = objData.EditedBy;
                                    objLog.EditedOn = objData.EditedOn;
                                    objLog.DeliverStatus = objData.DeliverStatus;
                                    objLog.MaterialStatus = objData.MaterialStatus;
                                    objLog.KitLocation = objData.KitLocation;
                                    objLog.FixReqDate = objData.FixReqDate;
                                    objLog.FullkitArea = objData.FullkitArea;
                                    objLog.FullkitAreaStatus = objData.FullkitAreaStatus;
                                    objLog.RequestedBy = objData.RequestedBy;
                                    objLog.RequestedOn = objData.RequestedOn;
                                    objLog.SubLocation = objData.SubLocation;

                                    objLog.RefId = item111.Id;
                                    db.FKM118_Log.Add(objLog);
                                    db.SaveChanges();

                                    //Update RefLineId and RefId of FKM119 because only approved quantity will attach with kit.-Satish
                                    var objFKM119 = db.FKM119.Where(x => x.FixtureNo == item.FixtureNo).FirstOrDefault();
                                    if (objFKM119 != null)
                                    {
                                        var objDataLogI = db.FKM118_Log.Where(x => x.RefLineId == lineId && x.FixtureNo == objFKM119.FixtureNo).ToList().OrderByDescending(x => x.LogId).FirstOrDefault();
                                        objFKM119.RefLineId = objDataLogI.RefId;
                                        objFKM119.RefId = objDataLogI.LogId;
                                    }

                                }
                            }
                        }
                    }

                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }

        [HttpPost]
        public ActionResult ReturnFixture(string strLineIds, string returnRemark = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                bool IsReturned = false;
                var list = strLineIds.Split(',').ToList();
                foreach (var item in list)
                {
                    int LineId = Convert.ToInt32(item);
                    var objFKM111 = db.FKM111.Where(x => x.LineId == LineId).FirstOrDefault();
                    if (objFKM111.Status == clsImplementationEnum.FRStatus.SentForApproval.GetStringValue())
                    {
                        objFKM111.Status = clsImplementationEnum.FRStatus.Returned.GetStringValue();
                        objFKM111.ReturnBy = objClsLoginInfo.UserName;
                        objFKM111.ReturnOn = DateTime.Now;
                        objFKM111.ReturnRemark = returnRemark;
                        db.SaveChanges();

                        #region Send Notification
                        (new clsManager()).SendNotification((clsImplementationEnum.UserRoleName.PLNG3.GetStringValue() + "," + clsImplementationEnum.UserRoleName.PMG3.GetStringValue()), objFKM111.FKM114.Project, "", "", "Fixture : " + objFKM111.FixtureName + " of Project: " + objFKM111.FKM114.Project + "  has been Returned", clsImplementationEnum.NotificationType.Information.GetStringValue(), "", objFKM111.CreatedBy);
                        #endregion

                        IsReturned = true;
                    }
                }

                if (IsReturned)
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Return;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Status has been already updated. Please refresh the page.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReviseFixture(int HeaderId, int LineId, string reviseRemark = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                var objFKM111 = db.FKM111.Where(x => x.LineId == LineId).FirstOrDefault();
                if (objFKM111.Status == clsImplementationEnum.FRStatus.Approved.GetStringValue())
                {
                    objFKM111.Status = clsImplementationEnum.FRStatus.Draft.GetStringValue();
                    objFKM111.ReviseRemark = reviseRemark;

                    int oldrev = objFKM111.RevNo != null ? Convert.ToInt32(objFKM111.RevNo) : 0;
                    int newrev = oldrev + 1;

                    objFKM111.RevNo = newrev;

                    var objFKM116 = db.FKM116.Where(x => x.RefLineId == LineId).FirstOrDefault();
                    if (objFKM116 != null)
                    {
                        objFKM116.IsViewByFixMFg = false;
                        objFKM116.RevNo = newrev;
                        objFKM116.SCRNo = objFKM116.ProjectNo + "-###-R" + newrev;
                    }

                    db.SaveChanges();

                    var MaxRevNo = db.FKM111.Where(x => x.RefHeaderId == HeaderId).OrderByDescending(u => u.RevNo).FirstOrDefault().RevNo;
                    var objFKM114 = db.FKM114.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    if (objFKM114 != null)
                        objFKM114.RevNo = MaxRevNo != null ? Convert.ToInt32(MaxRevNo) : 1;

                    db.SaveChanges();

                    var objFkm111Log = db.FKM111_Log.Where(x => x.LineId == LineId && x.RevNo == oldrev).FirstOrDefault();
                    if (objFkm111Log != null)
                    {
                        var lstFKM118Log = db.FKM118_Log.Where(x => x.RefId == objFkm111Log.Id).ToList();
                        foreach (var item in lstFKM118Log)
                        {
                            var objFkm118 = db.FKM118.Where(x => x.Id == item.Id).FirstOrDefault();
                            objFkm118.DeliverStatus = item.DeliverStatus;
                            objFkm118.EditedBy = item.EditedBy;
                            objFkm118.EditedOn = item.EditedOn;
                            db.SaveChanges();
                        }
                    }
                                        
                    objFKM111.DeliverStatus = GetCombineDeliveryStatus(objFKM111.LineId, false);
                    db.SaveChanges();
                    

                    var folderPath = "FKM111//" + HeaderId + "//" + objFKM111.LineId + "//R" + newrev;
                    var oldFolderPath = "FKM111//" + HeaderId + "//" + objFKM111.LineId + "//R" + oldrev;
                    Utility.Controllers.FileUploadController _objFUC = new Utility.Controllers.FileUploadController();
                    _objFUC.CopyDataOnFCSServerAsync(oldFolderPath,folderPath,oldFolderPath,objFKM111.LineId,folderPath,objFKM111.LineId,DESServices.CommonService.GetUseIPConfig,DESServices.CommonService.objClsLoginInfo.UserName,0,false);
                    //(new clsFileUpload()).CopyFolderContentsAsync(oldFolderPath, folderPath);

                    System.Threading.Thread.Sleep(3000);
                    string fixtureNoName = Manager.GetFixtureNo(objFKM111.Project, objFKM111.FXRSrNo);
                    //var Documents = (new clsFileUpload()).GetDocuments(folderPath).Select(s => s.Name);
                    //Utility.Controllers.FileUploadController.FileUpload_WriteContentOnPDF(String.Join(",", Documents), folderPath, fixtureNoName + " (R" + newrev + ")", 0, 0, 90, 0, "bottomleft");
                    //Utility.Controllers.FileUploadController.FileUpload_WriteContentOnPDF(String.Join(",", Documents), folderPath, fixtureNoName + " (R" + newrev + ")", 0, 0, 0, 0, "topright");

                    _objFUC.FileUpload_WriteContentOnPDF_FCS(folderPath, objFKM111.LineId, DESServices.CommonService.GetUseIPConfig, fixtureNoName + " (R" + newrev + ")", 0, 0, 90, 0, "bottomleft");
                    _objFUC.FileUpload_WriteContentOnPDF_FCS(folderPath, objFKM111.LineId, DESServices.CommonService.GetUseIPConfig, fixtureNoName + " (R" + newrev + ")", 0, 0, 0, 0, "topright");

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Revision;
                    objResponseMsg.Revision = Convert.ToInt32(objFKM114.RevNo);
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Status has been already updated. Please refresh the page.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult RetractFixture(int LineId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                var objFKM111 = db.FKM111.Where(x => x.LineId == LineId).FirstOrDefault();
                if (objFKM111.Status == clsImplementationEnum.FRStatus.SentForApproval.GetStringValue())
                {
                    objFKM111.Status = clsImplementationEnum.FRStatus.Draft.GetStringValue();

                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Retract.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Status has been already updated. Please refresh the page.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReuseFixture(int LineId, int RefFixtureReuse, bool ReUse)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                var objFKM111 = db.FKM111.Where(x => x.LineId == LineId).FirstOrDefault();
                var objFKM111ReUse = db.FKM111.Where(x => x.LineId == RefFixtureReuse).FirstOrDefault();
                objFKM111.ReUse = ReUse;
                if (ReUse)
                {
                    if (objFKM111.Subcontracting)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Fixture subcontracting value has been already updated. Please refresh the page.";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    else if (db.FKM111.Any(x => x.ParentId == LineId))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Item already added in fixture. Please refresh the page.";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    objFKM111.RefFixtureReuse = RefFixtureReuse;
                    objFKM111.DeliverStatus = objFKM111ReUse.DeliverStatus;
                    //objFKM111.Status = objFKM111ReUse.Status;
                    //objFKM111.Qty = objFKM111ReUse.Qty;
                    //objFKM111.QtyofFixture = objFKM111ReUse.QtyofFixture;
                    //objFKM111.FixRequiredDate = objFKM111ReUse.FixRequiredDate;
                    //objFKM111.Area = objFKM111ReUse.Area;
                    //objFKM111.Category = objFKM111ReUse.Category;
                    //objFKM111.LengthOD = objFKM111ReUse.LengthOD;
                    //objFKM111.Material = objFKM111ReUse.Material;
                    //objFKM111.MaterialReqDate = objFKM111ReUse.MaterialReqDate;
                    //objFKM111.MaterialStatus = objFKM111ReUse.MaterialStatus;
                    //objFKM111.MaterialType = objFKM111ReUse.MaterialType;
                    //objFKM111.ReqArea = objFKM111ReUse.ReqArea;
                    //objFKM111.RequiredMaterial = objFKM111ReUse.RequiredMaterial;
                    //objFKM111.ReqWt = objFKM111ReUse.ReqWt;
                    //objFKM111.Size = objFKM111ReUse.Size;
                    //objFKM111.StructuralType = objFKM111ReUse.StructuralType;
                    //objFKM111.Subcontracting = objFKM111ReUse.Subcontracting;
                    //objFKM111.Thickness = objFKM111ReUse.Thickness;
                    //objFKM111.TotalMaterialRequirement = objFKM111ReUse.TotalMaterialRequirement;
                    //objFKM111.Unit = objFKM111ReUse.Unit;
                    //objFKM111.Unit2 = objFKM111ReUse.Unit2;
                    //objFKM111.WidthOD = objFKM111ReUse.WidthOD;
                    //objFKM111.Wt = objFKM111ReUse.Wt;
                }
                else
                {
                    objFKM111.RefFixtureReuse = null;
                    objFKM111.DeliverStatus = null;
                    //objFKM111.Status = null;
                    //objFKM111.Qty = null;                         
                    //objFKM111.QtyofFixture = null;
                    //objFKM111.FixRequiredDate = null;
                    //objFKM111.Area = null;
                    //objFKM111.Category = null;
                    //objFKM111.LengthOD = null;
                    //objFKM111.Material = null;
                    //objFKM111.MaterialReqDate = null;
                    //objFKM111.MaterialStatus = null;
                    //objFKM111.MaterialType = null;
                    //objFKM111.ReqArea = null;
                    //objFKM111.RequiredMaterial = null;
                    //objFKM111.ReqWt = null;
                    //objFKM111.Size = null;
                    //objFKM111.StructuralType = null;
                    //objFKM111.Subcontracting = false;
                    //objFKM111.Thickness = null;
                    //objFKM111.TotalMaterialRequirement = null;
                    //objFKM111.Unit = null;
                    //objFKM111.Unit2 = null;
                    //objFKM111.WidthOD = null;
                    //objFKM111.Wt = null;

                }

                db.SaveChanges();

                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveSubcontractingFixture(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int LineId = Convert.ToInt32(fc["LineId"]);
                bool Subcontracting = Convert.ToBoolean(fc["Subcontracting"]);
                //int QtyofFixture = (fc["QtyofFixture"] != null && fc["QtyofFixture"] != "") ? Convert.ToInt32(fc["QtyofFixture"]) : 0;
                //string FxrReqDt = fc["FixRequiredDate"] != null ? fc["FixRequiredDate"].ToString() : "";
                //string MaterialReqDate = fc["MaterialReqDate"] != null ? fc["MaterialReqDate"].ToString() : "";
                //string FixMfg = fc["FixMfg"] != null ? fc["FixMfg"].ToString() : "";

                clsManager objManager = new clsManager();

                var objFKM111 = db.FKM111.Where(x => x.LineId == LineId).FirstOrDefault();
                objFKM111.Subcontracting = Subcontracting;
                //if (Subcontracting)
                //{
                //    if (objFKM111.ReUse)
                //    {
                //        objResponseMsg.Key = false;
                //        objResponseMsg.Value = "Fixture reuse value has been already updated. Please refresh the page.";
                //        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                //    }
                //    else if (db.FKM111.Any(x => x.ParentId == LineId))
                //    {
                //        objResponseMsg.Key = false;
                //        objResponseMsg.Value = "Item already added in fixture. Please refresh the page.";
                //        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                //    }

                //    objFKM111.QtyofFixture = QtyofFixture;
                //    objFKM111.FixRequiredDate = DateTime.ParseExact(FxrReqDt, @"d/M/yyyy", CultureInfo.InvariantCulture);
                //    objFKM111.MaterialReqDate = DateTime.ParseExact(MaterialReqDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                //    objFKM111.FixMfg = FixMfg;

                //    List<FKM118> objFKM118List = new List<FKM118>();
                //    for (int i = 1; i <= QtyofFixture; i++)
                //    {
                //        FKM118 objFKM118 = new FKM118();
                //        objFKM118.RefLineId = LineId;
                //        objFKM118.FixtureNo = objManager.GetFixtureNo(objFKM111.Project, objFKM111.FXRSrNo) + "-" + i;
                //        objFKM118.CreatedBy = objClsLoginInfo.UserName;
                //        objFKM118.CreatedOn = DateTime.Now;
                //        objFKM118List.Add(objFKM118);
                //    }
                //    db.FKM118.AddRange(objFKM118List);
                //}
                //else
                //{
                //    objFKM111.QtyofFixture = null;
                //    objFKM111.FixRequiredDate = null;
                //    objFKM111.MaterialReqDate = null;
                //    objFKM111.FixMfg = "";

                //    var objFKM118List = db.FKM118.Where(x => x.RefLineId == LineId).ToList();
                //    if (objFKM118List.Count > 0)
                //        db.FKM118.RemoveRange(objFKM118List);
                //}

                db.SaveChanges();

                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteFixture(int LineId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (!db.FKM119.Any(x => x.RefLineId == LineId))
                {
                    var objFKM111 = db.FKM111.Where(x => x.LineId == LineId).FirstOrDefault();
                    if (objFKM111 != null)
                    {
                        clsHelper.ResponseMsg objResponsePLM = new clsHelper.ResponseMsg();
                        objResponsePLM.Key = true;

                        //if (objFKM111.IsInsertedInPLM != null && objFKM111.IsInsertedInPLM == true)
                        //{
                        //    objResponsePLM = DeletePartAndBOMFromPLM(LineId, true, false);
                        //}

                        if (objResponsePLM.Key)
                        {
                            var objFKM111ChildList = db.FKM111.Where(x => x.ParentId == LineId).ToList();
                            if (objFKM111ChildList.Count > 0)
                                db.FKM111.RemoveRange(objFKM111ChildList);

                            var objFKM115List = db.FKM115.Where(x => x.ParentId == LineId).ToList();
                            if (objFKM115List.Count > 0)
                                db.FKM115.RemoveRange(objFKM115List);

                            var objFKM118List = db.FKM118.Where(x => x.RefLineId == LineId).ToList();
                            if (objFKM118List.Count > 0)
                                db.FKM118.RemoveRange(objFKM118List);

                            var objFKM116List = db.FKM116.Where(x => x.RefLineId == LineId).ToList();
                            if (objFKM116List.Count > 0)
                                db.FKM116.RemoveRange(objFKM116List);

                            db.FKM111.Remove(objFKM111);

                            db.SaveChanges();

                            objResponseMsg.Key = true;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete;
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "Deallocate from PLM Error : " + objResponsePLM.Value;
                        }
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Fixture attached to Kit in FKMS. You can't delete it.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteFixtureMaterial(int LineId, int ParentId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                bool CanDelete = true;
                var objFKM111 = db.FKM111.Where(u => u.LineId == ParentId).FirstOrDefault();
                if (objFKM111 != null)
                {
                    if (objFKM111.RevNo > 0 || objFKM111.Status == clsImplementationEnum.FRStatus.SentForApproval.GetStringValue() || objFKM111.Status == clsImplementationEnum.FRStatus.Approved.GetStringValue())
                    {
                        if (!db.FKM111.Any(x => x.LineId != LineId && x.ParentId == ParentId))
                            CanDelete = false;
                    }

                    if (CanDelete)
                    {
                        var objFKM111Child = db.FKM111.Where(x => x.LineId == LineId).FirstOrDefault();
                        if (objFKM111Child != null)
                        {
                            clsHelper.ResponseMsg objResponsePLM = new clsHelper.ResponseMsg();
                            objResponsePLM.Key = true;

                            if (objFKM111Child.IsInsertedInPLM != null && objFKM111Child.IsInsertedInPLM == true)
                            {
                                objResponsePLM = DeletePartAndBOMFromPLM(LineId, false, true);
                            }

                            if (objResponsePLM.Key)
                            {
                                //update weight in sub contracting form
                                var objFKM116 = db.FKM116.Where(x => x.RefLineId == ParentId).FirstOrDefault();
                                if (objFKM116 != null)
                                {
                                    var wt = db.FKM111.Where(x => x.LineId != LineId && x.ParentId == ParentId).Sum(x => x.Wt);
                                    objFKM116.Weight = wt != null ? wt.ToString() : "";
                                }

                                var objFKM115List = db.FKM115.Where(x => x.LineId == LineId).ToList();
                                if (objFKM115List.Count > 0)
                                    db.FKM115.RemoveRange(objFKM115List);

                                if (!db.FKM111.Any(x => x.LineId != LineId && x.ParentId == ParentId))
                                {
                                    var objFKM118List = db.FKM118.Where(x => x.RefLineId == ParentId).ToList();
                                    if (objFKM118List.Count > 0)
                                        db.FKM118.RemoveRange(objFKM118List);

                                    objFKM111.QtyofFixture = null;
                                    objFKM111.FixRequiredDate = null;
                                    objFKM111.MaterialReqDate = null;
                                    objFKM111.FixMfg = "";
                                }

                                db.FKM111.Remove(objFKM111Child);

                                db.SaveChanges();

                                objResponseMsg.Key = true;
                                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete;
                            }
                            else
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = "Deallocate from PLM Error : " + objResponsePLM.Value;
                            }
                        }
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Fixture should have at least one item. You can not delete item";
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No record found";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateFixtureReqDate(int LineId, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var objFKM118 = db.FKM118.Where(x => x.Id == LineId).FirstOrDefault();
                if (!string.IsNullOrWhiteSpace(columnValue))
                    objFKM118.FixReqDate = Convert.ToDateTime(columnValue);
                else
                    objFKM118.FixReqDate = null;

                db.SaveChanges();

                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult LoadFixtureSubcontractingPartial(int LineId)
        {
            FKM111 objFKM111 = db.FKM111.Where(x => x.LineId == LineId).FirstOrDefault();

            List<ApproverModel> listMfg = new List<ApproverModel>();
            string[] mfgRoles = new string[] { clsImplementationEnum.UserRoleName.FMG3.GetStringValue() };
            for (int i = 0; i < mfgRoles.Length; i++)
            {
                List<ApproverModel> list = Manager.GetApproverList(mfgRoles[i].ToString(), objClsLoginInfo.Location, objClsLoginInfo.UserName, string.Empty).ToList();
                if (list.Count > 0)
                    listMfg.AddRange(list);
            }
            ViewBag.lstFixMfg = listMfg.Select(x => new BULocWiseCategoryModel { CatDesc = x.Name, CatID = x.Code }).Distinct().ToList();

            return PartialView("_FixtureSubcontractingPartial", objFKM111);
        }

        #endregion

        #region Subcontracting Form

        public void MapFKM116(FKM116_Log Log, ref FKM116 objFKM116)
        {
            objFKM116.LineId = Log.Id;
            objFKM116.RefLineId = Log.RefId;
            objFKM116.FRNo = Log.FRNo;
            objFKM116.RevNo = Log.RevNo;
            objFKM116.Date = Log.Date;
            objFKM116.InitiatorDept = Log.InitiatorDept;
            objFKM116.ProjectNo = Log.ProjectNo;
            objFKM116.TypeOfWork = Log.TypeOfWork;
            objFKM116.BriefScopeOfWork = Log.BriefScopeOfWork;
            objFKM116.RefDrgNo = Log.RefDrgNo;
            objFKM116.UnitOfMeasurement = Log.UnitOfMeasurement;
            objFKM116.MaterialAvailablityDate = Log.MaterialAvailablityDate;
            objFKM116.Weight = Log.Weight;
            objFKM116.TentativeJobStartDate = Log.TentativeJobStartDate;
            objFKM116.DeliveryDateRequired = Log.DeliveryDateRequired;
            objFKM116.WorkCompletioncommittedbyLEMF = Log.WorkCompletioncommittedbyLEMF;
            objFKM116.PreferredContractor = Log.PreferredContractor;
            objFKM116.Initiator = Log.Initiator;
            objFKM116.PlanningLead = Log.PlanningLead;
            objFKM116.CreatedBy = Log.CreatedBy;
            objFKM116.CreatedOn = Log.CreatedOn;
            objFKM116.EditedBy = Log.EditedBy;
            objFKM116.EditedOn = Log.EditedOn;
            objFKM116.RefHeaderId = Log.RefHeaderId;
            objFKM116.AllocatedContractor = Log.AllocatedContractor;
            objFKM116.IsViewByFixMFg = Log.IsViewByFixMFg;
            objFKM116.IsMaterialAvailable = Log.IsMaterialAvailable;
            objFKM116.DispatchlocationContactperson = Log.DispatchlocationContactperson;
            objFKM116.NDTRequirement = Log.NDTRequirement;
            objFKM116.Subcontractingtype = Log.Subcontractingtype;
            objFKM116.TypeofSaddle = Log.TypeofSaddle;
            objFKM116.Typeoffixture = Log.Typeoffixture;
            objFKM116.Fixturemanufacturer = Log.Fixturemanufacturer;
            objFKM116.SCRNo = Log.SCRNo;
            objFKM116.PCLNoOutbound = Log.PCLNoOutbound;
            objFKM116.SrNo = Log.SrNo;                     
        }

        [HttpPost]
        public ActionResult SubcontractingForm(int lineId, string urlForm, string UserRole, string IsFixtureCompleted)
        {
            string rolePLNG1 = UserRoleName.PLNG1.GetStringValue();
            string rolePLNG2 = UserRoleName.PLNG2.GetStringValue();
            string rolePMG1 = UserRoleName.PMG1.GetStringValue();
            string rolePMG2 = UserRoleName.PMG2.GetStringValue();
            List<ApproverModel> listApprovers = new List<ApproverModel>();
            string[] approverRoles = new string[] { rolePLNG1, rolePLNG2, rolePMG1, rolePMG2 };
            for (int i = 0; i < approverRoles.Length; i++)
            {
                List<ApproverModel> list = Manager.GetApproverList(approverRoles[i].ToString(), objClsLoginInfo.Location, objClsLoginInfo.UserName, string.Empty).ToList();
                if (list.Count > 0)
                    listApprovers.AddRange(list);
            }
            ViewBag.lstApprovers = (from a in listApprovers
                                    group a by new
                                    {
                                        a.Code,
                                        a.Name
                                    } into b
                                    select new BULocWiseCategoryModel()
                                    {
                                        CatDesc = b.Key.Name,
                                        CatID = b.Key.Code
                                    }).ToList();

            if (urlForm != clsImplementationEnum.WPPIndexType.release.GetStringValue())
            {
                #region Maintain & Approve Fixture
                FKM116 objFKM116 = db.FKM116.Where(x => x.RefLineId == lineId).FirstOrDefault();
                FKM111 objFKM111 = db.FKM111.Where(x => x.LineId == lineId).FirstOrDefault();
                var objFixItem = db.FKM111.Where(x => x.ParentId == lineId).FirstOrDefault();
                clsManager objManager = new clsManager();
                if (objFKM116 == null)
                {
                    objFKM116 = new FKM116();
                    objFKM116.FRNo = objManager.GetFixtureNo(objFKM111.Project, objFKM111.FXRSrNo);
                    objFKM116.RevNo = objFKM111.RevNo;
                    objFKM116.InitiatorDept = db.COM003.Where(x => x.t_psno == objFKM111.CreatedBy).Select(x => x.t_depc).FirstOrDefault();
                    objFKM116.ProjectNo = objFKM111.Project;
                    objFKM116.Initiator = objClsLoginInfo.UserName;
                    objFKM116.PlanningLead = objFKM111.FKM114.ApprovedBy;

                    if (objFixItem != null)
                    {
                        objFKM116.DeliveryDateRequired = objFixItem.FixRequiredDate;
                        objFKM116.MaterialAvailablityDate = objFixItem.MaterialReqDate;
                    }
                    objFKM116.SCRNo = objFKM116.ProjectNo + "-###-R" + objFKM116.RevNo;
                }
                else
                {
                    if (objFixItem != null)
                    {
                        if (objFKM116.DeliveryDateRequired == null)
                            objFKM116.DeliveryDateRequired = objFixItem.FixRequiredDate;

                        if (objFKM116.MaterialAvailablityDate == null)
                            objFKM116.MaterialAvailablityDate = objFixItem.MaterialReqDate;
                    }
                    objFKM116.SCRNo = objFKM116.ProjectNo + "-###-R" + objFKM116.RevNo;
                }

                if (string.IsNullOrWhiteSpace(objFKM116.Weight))
                {
                    /*var wt = db.FKM111.Where(x => x.ParentId == lineId).Sum(x => x.Wt);
                    if (wt != null)
                        wt = Math.Round(Convert.ToDecimal(wt), 2);

                    objFKM116.Weight = wt != null ? wt.ToString() : "";*/
                }
                else
                {
                    decimal val = 0;
                    if (Decimal.TryParse(objFKM116.Weight, out val))
                        objFKM116.Weight = Math.Round(Convert.ToDecimal(objFKM116.Weight), 2).ToString();
                }

                objFKM116.RefLineId = lineId;
                objFKM116.RefHeaderId = objFKM111.RefHeaderId;

                ViewBag.Department = Manager.GetDepartmentByPsno(objFKM111.CreatedBy);
                ViewBag.Project = Manager.GetProjectAndDescription(objFKM111.Project);
                ViewBag.Initiator = Manager.GetPsidandDescription(objFKM116.Initiator);
                ViewBag.PlanningLead = Manager.GetPsidandDescription(objFKM116.PlanningLead);

                ViewBag.urlForm = urlForm;
                ViewBag.UserRole = UserRole;
                ViewBag.isRoleFKM3 = objClsLoginInfo.ListRoles.Contains(UserRoleName.FMG3.GetStringValue());
                ViewBag.Status = objFKM111.Status;
                ViewBag.IsFixtureCompleted = IsFixtureCompleted;
                ViewBag.FixtureStatus = objFKM111.Status;

                List<ApproverModel> listMfg = new List<ApproverModel>();
                string[] mfgRoles = new string[] { UserRoleName.FMG3.GetStringValue() };
                foreach (var role in mfgRoles)
                {
                    List<ApproverModel> list = Manager.GetApproverList(role, objClsLoginInfo.Location, objClsLoginInfo.UserName, string.Empty);
                    if (list.Count > 0) listMfg.AddRange(list);
                }
                if (objFKM116.Fixturemanufacturer != null)
                {
                    ViewBag.Fixturemanufacturer = new List<BULocWiseCategoryModel>() { new BULocWiseCategoryModel() { CatDesc = Manager.GetPsidandDescription(objFKM116.Fixturemanufacturer), CatID = objFKM116.Fixturemanufacturer } }.FirstOrDefault().CatDesc;
                    ViewBag.lstFixMfg = listMfg.Select(s => new BULocWiseCategoryModel() { CatDesc = s.Name, CatID = s.Code }).ToList();
                }
                else
                {
                    ViewBag.Fixturemanufacturer = "";
                    ViewBag.lstFixMfg = listMfg.Select(s => new BULocWiseCategoryModel() { CatDesc = s.Name, CatID = s.Code }).ToList();
                }
                //if (!string.IsNullOrWhiteSpace(objFKM116.Fixturemanufacturer))
                //    ViewBag.Fixturemanufacturer = Manager.GetPsidandDescription(objFKM116.Fixturemanufacturer);
                //ViewBag.FixtureApprovedOn = objFKM111.ApprovedOn != null ? Convert.ToDateTime(objFKM111.ApprovedOn).ToString("dd/MM/yyyy") : "";

                if (objFKM116 != null && objFKM116.LineId > 0)
                {
                    objFKM116.EditedBy = objClsLoginInfo.UserName;
                    objFKM116.EditedOn = DateTime.Now;
                    objFKM116.IsViewByFixMFg = true;
                    db.SaveChanges();
                }
                else
                {
                    objFKM116.CreatedBy = objClsLoginInfo.UserName;
                    objFKM116.CreatedOn = DateTime.Now;
                    objFKM116.IsViewByFixMFg = true;
                    db.FKM116.Add(objFKM116);
                    db.SaveChanges();
                }
                              
                return PartialView("_SubcontractingForm", objFKM116);
                #endregion
            }
            else
            {
                #region Execute Fixture
                FKM116_Log objFKM116 = db.FKM116_Log.Where(x => x.RefId == lineId).FirstOrDefault();
                FKM111_Log objFKM111 = db.FKM111_Log.Where(x => x.Id == lineId).FirstOrDefault();
                var objFixItem = db.FKM111_Log.Where(x => x.ParentId == lineId).FirstOrDefault();
                clsManager objManager = new clsManager();

                if (objFixItem != null)
                {
                    if (objFKM116.DeliveryDateRequired == null)
                        objFKM116.DeliveryDateRequired = objFixItem.FixRequiredDate;

                    if (objFKM116.MaterialAvailablityDate == null)
                        objFKM116.MaterialAvailablityDate = objFixItem.MaterialReqDate;
                }
                objFKM116.SCRNo = objFKM116.ProjectNo + "-###-R" + objFKM116.RevNo;

                if (!string.IsNullOrWhiteSpace(objFKM116.Weight))
                {
                    decimal val = 0;
                    if (Decimal.TryParse(objFKM116.Weight, out val))
                        objFKM116.Weight = Math.Round(Convert.ToDecimal(objFKM116.Weight), 2).ToString();
                }

                //objFKM116.RefLineId = lineId;
                //objFKM116.RefHeaderId = objFKM111.RefHeaderId;

                //objFKM116.RefId = lineId;
                //objFKM116.RefHeaderId = objFKM111.RefId;

                ViewBag.Department = Manager.GetDepartmentByPsno(objFKM111.CreatedBy);
                ViewBag.Project = Manager.GetProjectAndDescription(objFKM111.Project);
                ViewBag.Initiator = Manager.GetPsidandDescription(objFKM116.Initiator);
                ViewBag.PlanningLead = Manager.GetPsidandDescription(objFKM116.PlanningLead);

                ViewBag.urlForm = urlForm;
                ViewBag.UserRole = UserRole;
                ViewBag.isRoleFKM3 = objClsLoginInfo.ListRoles.Contains(UserRoleName.FMG3.GetStringValue());
                ViewBag.Status = objFKM111.Status;
                ViewBag.IsFixtureCompleted = IsFixtureCompleted;
                ViewBag.FixtureStatus = objFKM111.Status;

                List<ApproverModel> listMfg = new List<ApproverModel>();
                string[] mfgRoles = new string[] { UserRoleName.FMG3.GetStringValue() };
                foreach (var role in mfgRoles)
                {
                    List<ApproverModel> list = Manager.GetApproverList(role, objClsLoginInfo.Location, objClsLoginInfo.UserName, string.Empty);
                    if (list.Count > 0) listMfg.AddRange(list);
                }
                if (objFKM116.Fixturemanufacturer != null)
                {
                    ViewBag.Fixturemanufacturer = new List<BULocWiseCategoryModel>() { new BULocWiseCategoryModel() { CatDesc = Manager.GetPsidandDescription(objFKM116.Fixturemanufacturer), CatID = objFKM116.Fixturemanufacturer } }.FirstOrDefault().CatDesc;
                    ViewBag.lstFixMfg = listMfg.Select(s => new BULocWiseCategoryModel() { CatDesc = s.Name, CatID = s.Code }).ToList();
                }
                else
                {
                    ViewBag.Fixturemanufacturer = "";
                    ViewBag.lstFixMfg = listMfg.Select(s => new BULocWiseCategoryModel() { CatDesc = s.Name, CatID = s.Code }).ToList();
                }

                if (objFKM116 != null && objFKM116.Id > 0)
                {
                    objFKM116.IsViewByFixMFg = true;
                    db.SaveChanges();
                }

                FKM116 objFKM116Send = new FKM116();
                MapFKM116(objFKM116, ref objFKM116Send);
                return PartialView("_SubcontractingForm", objFKM116Send);
                #endregion
            }
        }

        [HttpPost]
        public ActionResult SaveSubcontractingDetails(FKM116 model, string urlForm)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            if (urlForm == clsImplementationEnum.WPPIndexType.release.GetStringValue())
            {
                #region Execute Fixture
                FKM116_Log objFKM116 = null;
              
                try
                {
                    objFKM116 = db.FKM116_Log.Where(x => x.Id == model.LineId).FirstOrDefault();
                    if (objFKM116 != null)
                    {
                        var objFKM116M = db.FKM116.Where(x => x.LineId == objFKM116.LineId).FirstOrDefault();

                        objFKM116.EditedBy = objClsLoginInfo.UserName;
                        objFKM116.EditedOn = DateTime.Now;

                        objFKM116.PCLNoOutbound = model.PCLNoOutbound;

                        objFKM116M.EditedBy = objClsLoginInfo.UserName;
                        objFKM116M.EditedOn = DateTime.Now;

                        objFKM116M.PCLNoOutbound = model.PCLNoOutbound;
                        if (objClsLoginInfo.ListRoles.Contains(clsImplementationEnum.UserRoleName.FMG3.GetStringValue()))
                        {
                            objFKM116.WorkCompletioncommittedbyLEMF = model.WorkCompletioncommittedbyLEMF;
                            objFKM116.AllocatedContractor = model.AllocatedContractor;

                            objFKM116M.WorkCompletioncommittedbyLEMF = model.WorkCompletioncommittedbyLEMF;
                            objFKM116M.AllocatedContractor = model.AllocatedContractor;
                        }

                        var objFKM111 = db.FKM111_Log.Where(x => x.Id == model.RefLineId).FirstOrDefault();
                        objFKM111.IsSubcontractFormSubmit = true;

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update;
                        db.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
                }
                #endregion
            }
            else
            {
                #region Maintain and Approve Fixture

                FKM116 objFKM116 = null;
                string actiontype = string.Empty;
                try
                {
                    objFKM116 = db.FKM116.Where(x => x.RefLineId == model.RefLineId).FirstOrDefault();
                    if (objFKM116 == null)
                    {
                        objFKM116 = new FKM116();
                        objFKM116.CreatedBy = objClsLoginInfo.UserName;
                        objFKM116.CreatedOn = DateTime.Now;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert;
                        actiontype = clsImplementationEnum.Actions.add.GetStringValue();
                        objFKM116.IsViewByFixMFg = false;
                    }
                    else
                    {
                        objFKM116.EditedBy = objClsLoginInfo.UserName;
                        objFKM116.EditedOn = DateTime.Now;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update;
                        actiontype = clsImplementationEnum.Actions.edit.GetStringValue();
                    }
                    objFKM116.PCLNoOutbound = model.PCLNoOutbound;
                    if (objClsLoginInfo.ListRoles.Contains(clsImplementationEnum.UserRoleName.FMG3.GetStringValue()))
                    {
                        objFKM116.WorkCompletioncommittedbyLEMF = model.WorkCompletioncommittedbyLEMF;
                        objFKM116.AllocatedContractor = model.AllocatedContractor;
                    }
                    else
                    {
                        objFKM116.RefHeaderId = model.RefHeaderId;
                        objFKM116.RefLineId = model.RefLineId;
                        objFKM116.FRNo = model.FRNo;
                        objFKM116.RevNo = model.RevNo;
                        objFKM116.Date = model.Date;
                        objFKM116.InitiatorDept = model.InitiatorDept;
                        objFKM116.ProjectNo = model.ProjectNo;
                        objFKM116.TypeOfWork = model.TypeOfWork;
                        objFKM116.BriefScopeOfWork = model.BriefScopeOfWork;
                        objFKM116.RefDrgNo = model.RefDrgNo;
                        //objFKM116.UnitOfMeasurement = model.UnitOfMeasurement;
                        objFKM116.MaterialAvailablityDate = model.MaterialAvailablityDate;
                        objFKM116.Weight = model.Weight;
                        //objFKM116.TentativeJobStartDate = model.TentativeJobStartDate;
                        objFKM116.DeliveryDateRequired = model.DeliveryDateRequired;
                        objFKM116.PreferredContractor = model.PreferredContractor;
                        objFKM116.Initiator = model.Initiator;
                        objFKM116.PlanningLead = model.PlanningLead;
                        objFKM116.IsMaterialAvailable = model.IsMaterialAvailable;
                        objFKM116.DispatchlocationContactperson = model.DispatchlocationContactperson;
                        objFKM116.NDTRequirement = model.NDTRequirement;
                        objFKM116.Subcontractingtype = model.Subcontractingtype;
                        objFKM116.TypeofSaddle = model.TypeofSaddle;
                        objFKM116.Typeoffixture = model.Typeoffixture;
                        objFKM116.Fixturemanufacturer = model.Fixturemanufacturer;
                    }

                    if (actiontype == clsImplementationEnum.Actions.add.GetStringValue())
                    {
                        int SrNo = db.FKM116.Where(w => w.ProjectNo == objFKM116.ProjectNo).Select(s => s.SrNo).OrderByDescending(o => o).FirstOrDefault().GetValueOrDefault() + 1;
                        objFKM116.SCRNo = objFKM116.ProjectNo + "-" + (SrNo.ToString("000")) + "-R" + objFKM116.RevNo;
                        db.FKM116.Add(objFKM116);
                    }

                    var objFKM111 = db.FKM111.Where(x => x.LineId == model.RefLineId).FirstOrDefault();
                    objFKM111.IsSubcontractFormSubmit = true;

                    objResponseMsg.Key = true;
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
                }
                #endregion
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Export Grid Data

        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "", int? HeaderId = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                clsManager objManager = new clsManager();
                string strFileName = string.Empty;

                if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {
                    var lst = db.SP_FKMS_GET_FR_LINES(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from fx in lst
                                  select new
                                  {
                                      FixtureNoOrItemNo = fx.ParentId == 0 ? objManager.GetFixtureNo(fx.Project, fx.FXRSrNo) : objManager.GetItemNo(fx.Project, fx.FXRSrNo),
                                      RevNo = fx.ParentId == 0 ? "R" + Convert.ToString(fx.RevNo) : "",//fixture rev no 
                                      NameOfFixture = fx.ParentId == 0 ? Convert.ToString(fx.FixtureName) : "",
                                      ///ReUse = fx.ParentId == 0 ? (fx.ReUse ? "Yes" : "No") : "",
                                      //ReuseFixtureName = fx.ParentId == 0 ? (fx.RefFixtureReuse != null ? GeRefFixtureReuse(fx.RefFixtureReuse.Value) : "") : "",

                                      FixtureType = fx.ParentId == 0 ? "" : fx.ItemType,
                                      DeliverStatus = fx.ParentId == 0 ? Convert.ToString(fx.DeliverStatus) : "",

                                      SubcontractingOutsidewithMaterial = fx.ParentId == 0 ? (fx.Subcontracting ? "Yes" : "No") : "",

                                      QtyOfFixture = fx.ParentId == 0 ? Convert.ToString(fx.QtyofFixture) : "",

                                      FixtureRequestReviseRemark = fx.ReviseRemark,
                                      Weight = Convert.ToString(fx.Weight),
                                      DeliveryDateRequired = fx.ParentId == 0 ? Convert.ToString(fx.DeliveryDateRequired.HasValue ? fx.DeliveryDateRequired.Value.ToShortDateString() : "") : "",
                                      WCC = fx.ParentId == 0 ? Convert.ToString(fx.WCC.HasValue ? fx.WCC.Value.ToShortDateString() : "") : "",
                                      AllocatatedContractor = Convert.ToString(fx.AllocatedContractor),

                                      //ItemCode = !string.IsNullOrWhiteSpace(fx.ItemCode) ? Convert.ToString(fx.ItemCode).Trim() : "",
                                      //DescriptionOfItem = Convert.ToString(fx.DescriptionofItem),

                                      //Category = Convert.ToString(fx.Category),
                                      //Material = Convert.ToString(fx.Material),
                                      //MaterialType = Convert.ToString(fx.MaterialType),
                                      //LengthOD = Convert.ToString(fx.LengthOD),
                                      //WidthOD = Convert.ToString(fx.WidthOD),
                                      //Thickness = Convert.ToString(fx.Thickness),
                                      //Qty = Convert.ToString(fx.Qty),
                                      //Weight = Convert.ToString(fx.Wt),
                                      //Area = Convert.ToString(fx.Area),
                                      //Unit = Convert.ToString(fx.Unit),
                                      //ReqWeight = Convert.ToString(fx.ReqWt),
                                      //ReqArea = Convert.ToString(fx.ReqArea),
                                      //Unit2 = Convert.ToString(fx.Unit2),
                                      //FixtureRequiredDate = fx.ParentId == 0 ? Convert.ToString(fx.MaterialReqDate.HasValue ? fx.MaterialReqDate.Value.ToShortDateString() : "") : "",
                                      //MaterialRequiredDate = fx.ParentId == 0 ? Convert.ToString(fx.FixRequiredDate.HasValue ? fx.FixRequiredDate.Value.ToShortDateString() : "") : "",

                                      //FixtureManufacturer = fx.ParentId == 0 ? fx.FixMfg : "",

                                      // FindNo = fx.FindNo,
                                      //FixtureRequestReturnRemark = fx.ReturnRemark,

                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);

                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GenerateExcelFMG(string whereCondition, string strSortOrder, string gridType = "", int? HeaderId = 0)
        {
            string urlForm = Request.QueryString["urlForm"];
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                clsManager objManager = new clsManager();
                string strFileName = string.Empty;

                if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {
                    var lst = db.SP_FKMS_GET_FR_LINES_FMG(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from fx in lst
                                  select new
                                  {
                                      FixtureNoOrItemNo = fx.ParentId == 0 ? objManager.GetFixtureNo(fx.Project, fx.FXRSrNo) : objManager.GetItemNo(fx.Project, fx.FXRSrNo),
                                      RevNo = fx.ParentId == 0 ? "R" + Convert.ToString(fx.RevNo) : "",//fixture rev no 
                                      NameOfFixture = fx.ParentId == 0 ? Convert.ToString(fx.FixtureName) : "",
                                      
                                      FixtureType = fx.ParentId == 0 ? "" : fx.ItemType,
                                      DeliverStatus = fx.ParentId == 0 ? Convert.ToString(fx.DeliverStatus) : "",

                                      SubcontractingOutsidewithMaterial = fx.ParentId == 0 ? (fx.Subcontracting ? "Yes" : "No") : "",

                                      QtyOfFixture = fx.ParentId == 0 ? Convert.ToString(fx.QtyofFixture) : "",

                                      FixtureRequestReviseRemark = fx.ReviseRemark,
                                      Weight = Convert.ToString(fx.Weight),
                                      DeliveryDateRequired = fx.ParentId == 0 ? Convert.ToString(fx.DeliveryDateRequired.HasValue ? fx.DeliveryDateRequired.Value.ToShortDateString() : "") : "",
                                      WCC = fx.ParentId == 0 ? Convert.ToString(fx.WCC.HasValue ? fx.WCC.Value.ToShortDateString() : "") : "",
                                      AllocatatedContractor = Convert.ToString(fx.AllocatedContractor),

                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);

                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        [HttpPost]
        public ActionResult GetProjectSummary(int HeaderId, string UserRole)
        {
            var objFKM114 = db.FKM114.Where(u => u.HeaderId == HeaderId).FirstOrDefault();
            string WhereCondition = " and RefHeaderId =" + HeaderId + "";

            //string role = GetUserRole();
            if (UserRole == UserRoleName.PLNG1.GetStringValue() || UserRole == UserRoleName.PLNG2.GetStringValue() || UserRole == UserRoleName.PMG1.GetStringValue() || UserRole == UserRoleName.PMG2.GetStringValue())
            {
                if (objFKM114.ApprovedBy == objClsLoginInfo.UserName)
                    WhereCondition += " and Status in ('" + FRStatus.SentForApproval.GetStringValue() + "','" + FRStatus.Approved.GetStringValue() + "')";
                else
                    WhereCondition = " and 1=0";
            }
            else if (UserRole == UserRoleName.FMG3.GetStringValue())
            {
                WhereCondition += " and (FixMfg='" + objClsLoginInfo.UserName + "') and Status in ('" + FRStatus.Approved.GetStringValue() + "')";
            }
            //var lstResult = db.SP_FKMS_GET_FR_LINES(0, int.MaxValue, "", WhereCondition).ToList();

            ////List<FKM111> objFKM111 = db.FKM111.Where(x => x.RefHeaderId == HeaderId && x.Category != null).OrderBy(x => x.Category).ToList();
            //lstResult = lstResult.Where(x => x.Category != null).OrderBy(x => x.Category).ToList();
            //List<FKM111> model = null;
            //if (lstResult.Count > 0)
            //{
            //    model = (from c in lstResult
            //             group c by new
            //             {
            //                 c.Category,
            //                 c.Material,
            //                 c.MaterialType,
            //                 c.Thickness,
            //                 c.LengthOD,
            //                 c.WidthOD,
            //                 HeaderId
            //             } into gcs
            //             select new FKM111()
            //             {
            //                 Category = gcs.Key.Category,
            //                 Material = gcs.Key.Material,
            //                 MaterialType = gcs.Key.MaterialType,
            //                 Thickness = gcs.Key.Thickness,
            //                 LengthOD = gcs.Key.LengthOD,
            //                 WidthOD = gcs.Key.WidthOD,
            //                 Wt = gcs.Sum(x => x.Wt),
            //                 Qty = gcs.Sum(x => x.Qty),
            //                 RefHeaderId = HeaderId
            //             }).ToList();

            //    ViewBag.TotalWeight = lstResult.Sum(x => x.Wt);
            //}

            var list = db.SP_FKMS_FR_GET_FIXTURE_SUMMARY_SHEET(WhereCondition).ToList();
            if (list.Count > 0)
            {
                ViewBag.TotalWeight = list.Sum(x => x.Weight);
            }
            ViewBag.HeaderId = HeaderId;
            return PartialView("_ProjectSummary", list);
        }

        #region  Allocation
        [SessionExpireFilter, AllowAnonymous]
        public ActionResult AllocationDetails(int RefLineId, int LineId)
        {
            ViewBag.RefLineId = RefLineId;
            ViewBag.LineId = LineId;
            return View();
        }

        [HttpPost]
        public ActionResult GetAllocationDetails(int RefLineId, int LineId)
        {
            ViewBag.UserRole = GetUserRole();
            var objFKM111 = db.FKM111.Where(x => x.LineId == LineId).FirstOrDefault();
            string PLT = clsImplementationEnum.NodeTypes.PLT.GetStringValue().ToLower();
            decimal usedQty = 0;
            decimal avlQty = 0;
            if (objFKM111 != null)
            {
                if (objFKM111.ItemType.ToLower().ToString() != PLT)
                {
                    List<SP_FKMS_GET_NON_PLT_WAREHOUSE_LIST_Result> componentWarehouselist = GetWarehouseList(objFKM111.Project.Trim(), objFKM111.ItemCode.Trim(), objClsLoginInfo.Location.Trim(), db);
                    if (componentWarehouselist != null)
                        avlQty = Convert.ToDecimal(componentWarehouselist.Sum(u => u.finqhnd));
                }
                else
                {
                    var objPLTAllocationList = db.SP_FKMS_GETPLTALLOCATEDETAILS(objFKM111.Project).ToList();
                    if (objPLTAllocationList.Any(x => x.Partno == objFKM111.FindNo))
                    {
                        usedQty = Convert.ToDecimal(db.FKM120.Where(x => x.FindNo == objFKM111.FindNo).ToList().Sum(x => x.AllocatedQty));
                        avlQty = Convert.ToDecimal(objPLTAllocationList.Where(x => x.Partno == objFKM111.FindNo).ToList().Sum(i => i.Qty));
                        avlQty = avlQty - usedQty;
                    }
                }
            }

            ViewBag.RefLineId = RefLineId;
            ViewBag.LineId = LineId;
            ViewBag.AvlQty = avlQty;
            return PartialView("_GetAllocationDetailsPartial");
        }

        [HttpPost]
        public ActionResult LoadFRAllocationData(JQueryDataTableParamModel param, int RefLineId, int LineId)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string PLT = clsImplementationEnum.NodeTypes.PLT.GetStringValue().ToLower();

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                var objFKM111 = db.FKM111.Where(x => x.LineId == LineId).FirstOrDefault();
                if (objFKM111 != null)
                {
                    string whereCondition = "1=1 and RefLineId=" + RefLineId + " and LineId=" + LineId + " and ItemType='" + objFKM111.ItemType + "'";

                    string[] columnName = { "FindNo", "FixtureNo", "ItemCode", "DescriptionofItem", "Qty", "PLTQty", "NPLTQty", "ItemType", "ErrorMsg" };

                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                    string strSortOrder = string.Empty;
                    if (!string.IsNullOrWhiteSpace(sortColumnName))
                    {
                        strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                    }

                    var lstresult = db.SP_FKMS_FR_GET_LN_ALLOCATION_DETAILS(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                    int? totalRecords = lstresult.Select(i => i.TotalCount).FirstOrDefault();

                    var objFKM118List = db.FKM118.ToList();

                    var res = (from h in lstresult
                               select new[] {
                               Convert.ToString(h.ROW_NO),
                               Convert.ToString(h.FindNo),
                               Convert.ToString(h.FixtureNo),
                               Convert.ToString(h.ItemCode),
                               Convert.ToString(h.DescriptionofItem),
                               Convert.ToString(h.Qty != null ? h.Qty: 0),
                               h.ItemType.ToLower().ToString() != PLT ? Convert.ToString(h.NPLTQty != null ? h.NPLTQty: 0): Convert.ToString(h.PLTQty != null ? h.PLTQty: 0),
                               Convert.ToString(h.ItemType),
                               Convert.ToString(h.ErrorMsg != null? h.ErrorMsg: "-"),
                               Convert.ToString(h.RefLineId),
                               Convert.ToString(h.LineId),
                               Convert.ToString(h.RefId),
                               Convert.ToString(h.PLTId),
                               Convert.ToString(h.NPLTId),
                               Convert.ToString(GetTotalAllocatedQty(h.RefLineId.Value,h.LineId,h.RefId,h.ItemType)),
                               Convert.ToString(IsFixtureCompleted(objFKM118List,h.RefLineId.Value)),
                               ""
                                }).ToList();

                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        aaData = res,
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = "0",
                        iTotalRecords = "0",
                        aaData = new string[0]
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = string.Empty,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = new string[0]
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public JsonResult AllocateItem(int RefLineId, int LineId, int RefId, string ItemType)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();

            var objFixture = db.FKM111.Where(x => x.LineId == RefLineId).FirstOrDefault();
            var objFixtureItem = db.FKM111.Where(x => x.LineId == LineId).FirstOrDefault();

            if (objFixture != null && objFixtureItem != null)
            {
                if (!objFixture.ReUse)
                {
                    if (!objFixtureItem.ReUse)
                    {
                        if (!objFixture.Subcontracting)
                        {
                            if (!objFixtureItem.Subcontracting)
                            {
                                if (objFixture.IsInsertedInPLM != null && objFixtureItem.IsInsertedInPLM != null && objFixture.IsInsertedInPLM.Value == true && objFixtureItem.IsInsertedInPLM.Value == true)
                                {
                                    if (!db.FKM122.Any(i => i.Project == objFixture.Project && i.RefLineId == RefLineId && i.LineId == LineId))
                                    {
                                        if (ItemType.ToLower() == clsImplementationEnum.NodeTypes.PLT.GetStringValue().ToLower())
                                        {
                                            objResponseMsg = IndividualPLTAllocation(objFixture, objFixtureItem, true, RefId, db);
                                        }
                                        else
                                        {
                                            objResponseMsg = IndividualNPLTAllocation(objFixture, objFixtureItem, true, RefId, db);
                                        }
                                    }
                                    else
                                    {
                                        objResponseMsg.Key = false;
                                        objResponseMsg.Value = "Allocation already is in progress. Please Try Again After Sometime.";
                                    }
                                }
                                else
                                {
                                    objResponseMsg.Key = false;
                                    objResponseMsg.Value = "Fixture item is not inserted in PLM. it can not be allocated to LN.";
                                }
                            }
                            else
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = "Subcontracting Fixture item can not be allocated to LN.";
                            }
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "Subcontracting Fixture can not be allocated to LN.";
                        }
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "ReUse Fixture item can not be allocated to LN.";
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "ReUse Fixture can not be allocated to LN.";
                }
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Fixture details not found";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeAllocateItem(int AllocatedId, string ItemType)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            objResponseMsg.Key = false;
            objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message;

            if (ItemType.ToLower() == clsImplementationEnum.NodeTypes.PLT.GetStringValue().ToLower())
            {
                var objFKM120 = db.FKM120.Where(u => u.Id == AllocatedId).FirstOrDefault();
                if (objFKM120 != null)
                {
                    db.FKM120.Remove(objFKM120);
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Item deallocated successfully";
                }
            }
            else
            {
                var objFKM121 = db.FKM121.Where(u => u.Id == AllocatedId).FirstOrDefault();
                if (objFKM121 != null)
                {
                    string errorMsg = string.Empty;
                    bool IsSucess = NPLTDeallocation(objFKM121, objClsLoginInfo.UserName, objClsLoginInfo.Location, db, ref errorMsg);
                    if (IsSucess)
                    {
                        if (db.FKM121.Any(u => u.Id == AllocatedId))
                        {
                            db.FKM121.Remove(objFKM121);
                            db.SaveChanges();
                        }
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Item deallocated successfully";
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = !string.IsNullOrWhiteSpace(errorMsg) ? errorMsg : "Some error occured in deallocation. Please try again.";
                    }
                }
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ReAllocateItem(int RefLineId, int LineId, int RefId, int AllocatedId, string ItemType)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            objResponseMsg.Key = false;
            objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message;

            var objFixture = db.FKM111.Where(x => x.LineId == RefLineId).FirstOrDefault();
            var objFixtureItem = db.FKM111.Where(x => x.LineId == LineId).FirstOrDefault();

            if (!objFixture.ReUse)
            {
                if (!objFixtureItem.ReUse)
                {
                    if (!objFixture.Subcontracting)
                    {
                        if (!objFixtureItem.Subcontracting)
                        {
                            if (ItemType.ToLower() == clsImplementationEnum.NodeTypes.PLT.GetStringValue().ToLower())
                            {
                                var objFKM120 = db.FKM120.Where(u => u.Id == AllocatedId).FirstOrDefault();
                                if (objFKM120 != null)
                                {
                                    db.FKM120.Remove(objFKM120);
                                    db.SaveChanges();

                                    objResponseMsg = IndividualPLTAllocation(objFixture, objFixtureItem, true, RefId, db);
                                    if (objResponseMsg.Key)
                                    {
                                        objResponseMsg.Key = true;
                                        objResponseMsg.Value = "Item reallocated successfully";
                                    }
                                }
                            }
                            else
                            {
                                var objFKM121 = db.FKM121.Where(u => u.Id == AllocatedId).FirstOrDefault();
                                if (objFKM121 != null)
                                {
                                    string errorMsg = string.Empty;
                                    bool IsSucess = NPLTDeallocation(objFKM121, objClsLoginInfo.UserName, objClsLoginInfo.Location, db, ref errorMsg);
                                    if (IsSucess)
                                    {
                                        if (db.FKM121.Any(u => u.Id == AllocatedId))
                                        {
                                            db.FKM121.Remove(objFKM121);
                                            db.SaveChanges();
                                        }
                                        objResponseMsg = IndividualNPLTAllocation(objFixture, objFixtureItem, true, RefId, db);
                                        if (objResponseMsg.Key)
                                        {
                                            objResponseMsg.Key = true;
                                            objResponseMsg.Value = "Item reallocated successfully";
                                        }
                                    }
                                    else
                                    {
                                        objResponseMsg.Key = false;
                                        objResponseMsg.Value = !string.IsNullOrWhiteSpace(errorMsg) ? errorMsg : "Some error occured in reallocation. Please try again.";
                                    }
                                }
                            }
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "Subcontracting Fixture can not be re-allocated to LN.";
                        }
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Subcontracting Fixture can not be re-allocated to LN.";
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "ReUse Fixture can not be re-allocated to LN.";
                }
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "ReUse Fixture can not be re-allocated to LN.";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public clsHelper.ResponseMsgWithStatus IndividualPLTAllocation(FKM111 objApprovedFixture, FKM111 objFixtureItem, bool IsManual, int RefId, IEMQSEntitiesContext dbCustom)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            objResponseMsg.Key = true;

            string ASM = clsImplementationEnum.NodeTypes.ASM.GetStringValue().ToLower();
            string TJF = clsImplementationEnum.NodeTypes.TJF.GetStringValue().ToLower();
            string PLT = clsImplementationEnum.NodeTypes.PLT.GetStringValue().ToLower();
            string PLT_TYPE = clsImplementationEnum.AllocateNodeTypes.PLT.ToString();
            string PROJ_PLT = clsImplementationEnum.AllocateNodeTypes.PROJ_PLT.ToString();
            string project = objApprovedFixture.Project;

            var objPLTAllocationList = dbCustom.SP_FKMS_GETPLTALLOCATEDETAILS(objApprovedFixture.Project).ToList();

            var objFKM118List = dbCustom.FKM118.ToList();

            #region INSERT RECORD INTO FKM122

            ManageAllocationLock(project, objFixtureItem.LineId, objApprovedFixture.LineId, PLT_TYPE, allocateInsert, db);

            #endregion

            try
            {
                #region ALLOCATION PROCESS

                string findno = objFixtureItem.FindNo;
                decimal itemQty = objFixtureItem.Qty != null ? Convert.ToDecimal(objFixtureItem.Qty) : 0;

                if (itemQty > 0)
                {
                    bool IsPLTItem = false;

                    //check item type
                    if (!string.IsNullOrWhiteSpace(objFixtureItem.ItemType))
                    {
                        if (objFixtureItem.ItemType.Trim().ToLower() == PLT)
                        {
                            IsPLTItem = true;
                        }
                    }

                    if (IsPLTItem)
                    {
                        var objFixtureList = (from u in objFKM118List
                                              where u.RefLineId == objApprovedFixture.LineId
                                              orderby u.FixtureNo ascending
                                              select u).ToList();

                        if (RefId != 0)
                        {
                            objFixtureList = objFixtureList.Where(u => u.Id == RefId).ToList();
                        }

                        foreach (var objFixture in objFixtureList)//F1, F2, F3
                        {
                            decimal requiredQty = itemQty;
                            decimal usedQty = 0;
                            decimal avlQty = 0;

                            int LineId = objFixtureItem.LineId;
                            int RefLineId = objFixture.RefLineId;
                            RefId = objFixture.Id;

                            #region CHECK ALLOCATION DONE

                            if (dbCustom.FKM120.Any(u => u.RefLineId == RefLineId && u.RefId == RefId && u.LineId == LineId))
                            {
                                string strTotalAllocatedQty = dbCustom.FKM120.Where(u => u.RefLineId == RefLineId && u.RefId == RefId && u.LineId == LineId).Sum(u => u.AllocatedQty).ToString();
                                decimal totalAllocatedQty = strTotalAllocatedQty != "" ? Convert.ToDecimal(strTotalAllocatedQty) : 0;
                                if (requiredQty < totalAllocatedQty)//2<5
                                {
                                    #region DEALLOCATE LOGIC HERE

                                    //deallocate totalAllocatedQty one by one from warehouse
                                    var allocatedFKM120List = dbCustom.FKM120.Where(u => u.RefLineId == RefLineId && u.LineId == LineId && u.RefId == RefId).ToList();
                                    if (allocatedFKM120List != null)
                                    {
                                        dbCustom.FKM120.RemoveRange(allocatedFKM120List);
                                        dbCustom.SaveChanges();
                                    }

                                    #endregion
                                }
                                else if (requiredQty > totalAllocatedQty)//5>2
                                {
                                    requiredQty = requiredQty - totalAllocatedQty;
                                }
                                else
                                {
                                    requiredQty = 0;
                                }
                            }

                            #endregion

                            if (requiredQty > 0)
                            {
                                if (objPLTAllocationList.Any(x => x.Partno == findno))
                                {
                                    usedQty = Convert.ToDecimal(dbCustom.FKM120.Where(x => x.FindNo == findno).ToList().Sum(x => x.AllocatedQty));
                                    avlQty = Convert.ToDecimal(objPLTAllocationList.Where(x => x.Partno == findno).ToList().Sum(i => i.Qty));
                                    avlQty = avlQty - usedQty;
                                }

                                if (avlQty >= requiredQty)
                                {
                                    List<FKM120> objFKM120List = new List<FKM120>();
                                    var filteredlist = objPLTAllocationList.Where(x => x.Partno == findno && x.Qty > 0).OrderBy(x => x.PCRLineno).ToList();
                                    foreach (var pclitem in filteredlist)
                                    {
                                        decimal allocatedQty = 0;
                                        decimal t_qhnd = pclitem.Qty != null ? Convert.ToDecimal(pclitem.Qty) : 0;

                                        decimal remainQty = Convert.ToDecimal(requiredQty - t_qhnd); //1-2
                                        if (remainQty >= 0)//1=1
                                        {
                                            allocatedQty = t_qhnd;
                                            requiredQty = remainQty;
                                        }
                                        else
                                        {
                                            allocatedQty = requiredQty;
                                            requiredQty = 0;
                                        }

                                        //Add Entry in allocation table
                                        FKM120 objFKM120 = new FKM120();
                                        objFKM120.Project = project;
                                        objFKM120.FindNo = findno;
                                        objFKM120.PCRNo = pclitem.PCRNo;
                                        objFKM120.PCRLineNo = pclitem.PCRLineno;
                                        objFKM120.PCRLineRevNo = pclitem.PCRlinerev;
                                        objFKM120.PCLNo = pclitem.PCLNo;
                                        objFKM120.AllocatedQty = allocatedQty;
                                        objFKM120.CreatedBy = objClsLoginInfo.UserName;
                                        objFKM120.CreatedOn = DateTime.Now;
                                        objFKM120.LineId = objFixtureItem.LineId;
                                        objFKM120.RefLineId = objFixtureItem.ParentId;
                                        objFKM120.RefId = objFixture.Id;
                                        objFKM120.FixtureNo = objFixture.FixtureNo;
                                        objFKM120.ItemCode = objFixtureItem.ItemCode;
                                        objFKM120List.Add(objFKM120);

                                        if (requiredQty == 0)
                                            break;
                                    }

                                    if (objFKM120List.Count > 0)
                                    {
                                        dbCustom.FKM120.AddRange(objFKM120List);
                                        dbCustom.SaveChanges();

                                        UpdatePLTDeliveryStatus(RefLineId, objFixture.Id, dbCustom);
                                    }
                                }
                                else
                                {
                                    objResponseMsg.Key = false;
                                    objResponseMsg.Value = "Qty not available";

                                    if (IsManual)
                                        break;
                                }
                            }
                            else
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = "Req Qty should be greater than 0";

                                if (IsManual)
                                    break;
                            }
                        }
                    }
                }

                #endregion

                if (objResponseMsg.Key)
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Item allocated successfully";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message.ToString();
            }
            finally
            {
                #region DELETE RECORD FROM FKM122

                ManageAllocationLock(project, objFixtureItem.LineId, objApprovedFixture.LineId, PLT_TYPE, allocateDelete, db);

                #endregion
            }
            return objResponseMsg;
        }

        public clsHelper.ResponseMsgWithStatus IndividualNPLTAllocation(FKM111 objApprovedFixture, FKM111 objFixtureItem, bool IsManual, int RefId, IEMQSEntitiesContext dbCustom)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            objResponseMsg.Key = true;

            string ASM = clsImplementationEnum.NodeTypes.ASM.GetStringValue().ToLower();
            string TJF = clsImplementationEnum.NodeTypes.TJF.GetStringValue().ToLower();
            string PLT = clsImplementationEnum.NodeTypes.PLT.GetStringValue().ToLower();
            string NPLT_TYPE = clsImplementationEnum.AllocateNodeTypes.NPLT.ToString();
            string project = objApprovedFixture.Project;
            string psno = objClsLoginInfo.UserName;
            string location = objClsLoginInfo.Location;
            string element = "";
            string workCenter = string.Empty;
            string errorMsg = string.Empty;

            if (!string.IsNullOrWhiteSpace(objApprovedFixture.CreatedBy))
                workCenter = dbCustom.COM003.Where(u => u.t_psno == objApprovedFixture.CreatedBy).FirstOrDefault().t_depc;

            var objFKM118List = dbCustom.FKM118.ToList();

            #region INSERT RECORD INTO FKM122

            ManageAllocationLock(project, objFixtureItem.LineId, objApprovedFixture.LineId, NPLT_TYPE, allocateInsert, dbCustom);

            #endregion

            try
            {
                #region ALLOCATION PROCESS

                string item = objFixtureItem.ItemCode.Trim();
                string findno = objFixtureItem.FindNo;
                decimal itemQty = objFixtureItem.Qty != null ? Convert.ToDecimal(objFixtureItem.Qty) : 0;

                if (itemQty > 0)
                {
                    bool IsNonPLTItem = false;
                    if (!string.IsNullOrWhiteSpace(objFixtureItem.ItemType))
                    {
                        if (objFixtureItem.ItemType.Trim().ToLower() != ASM && objFixtureItem.ItemType.Trim().ToLower() != TJF && objFixtureItem.ItemType.Trim().ToLower() != PLT)
                        {
                            IsNonPLTItem = true;
                        }
                    }

                    if (IsNonPLTItem)
                    {
                        var objFixtureList = (from u in objFKM118List
                                              where u.RefLineId == objApprovedFixture.LineId
                                              orderby u.FixtureNo ascending
                                              select u).ToList();

                        if (RefId != 0)
                        {
                            objFixtureList = objFixtureList.Where(u => u.Id == RefId).ToList();
                        }

                        foreach (var objFixture in objFixtureList)//F1-1, F1-2, F1-3
                        {
                            string FixtureNo = objFixture.FixtureNo;
                            decimal requiredQty = itemQty;
                            decimal avlQty = 0;

                            int LineId = objFixtureItem.LineId;
                            int RefLineId = objFixture.RefLineId;
                            RefId = objFixture.Id;

                            #region CHECK ALLOCATION DONE ALREADY IF YES THEN UPDATE QTY ACCORDINGLY

                            bool IsDeallocationSuccess = true;
                            if (dbCustom.FKM121.Any(u => u.RefLineId == RefLineId && u.LineId == LineId && u.RefId == RefId))
                            {
                                string strTotalAllocatedQty = dbCustom.FKM121.Where(u => u.RefLineId == RefLineId && u.LineId == LineId && u.RefId == RefId).Sum(u => u.AllocatedQty).ToString();
                                decimal totalAllocatedQty = strTotalAllocatedQty != "" ? Convert.ToDecimal(strTotalAllocatedQty) : 0;
                                if (requiredQty < totalAllocatedQty)//2<5
                                {
                                    #region DEALLOCATE LOGIC HERE

                                    //deallocate totalAllocatedQty one by one from warehouse
                                    var allocatedFKM121List = dbCustom.FKM121.Where(u => u.RefLineId == RefLineId && u.LineId == LineId && u.RefId == RefId).ToList();
                                    if (allocatedFKM121List != null)
                                    {
                                        foreach (var objAllocatedFKM121 in allocatedFKM121List)
                                        {
                                            if (objAllocatedFKM121.AllocatedQty > 0)
                                            {
                                                IsDeallocationSuccess = NPLTDeallocation(objAllocatedFKM121, psno, location, dbCustom, ref errorMsg);
                                                if (IsDeallocationSuccess)
                                                {
                                                    dbCustom.FKM121.Remove(objAllocatedFKM121);
                                                    dbCustom.SaveChanges();
                                                }
                                                else
                                                {
                                                    break;
                                                }
                                            }
                                            else
                                            {
                                                dbCustom.FKM121.Remove(objAllocatedFKM121);
                                                dbCustom.SaveChanges();
                                            }
                                        }
                                    }

                                    #endregion
                                }
                                else if (requiredQty > totalAllocatedQty)//5>2
                                {
                                    requiredQty = requiredQty - totalAllocatedQty;
                                }
                                else
                                {
                                    requiredQty = 0;
                                }
                            }

                            #endregion

                            if (requiredQty > 0 && IsDeallocationSuccess)
                            {
                                //get warehouse list and sum qty and check if qty available                                                 
                                List<SP_FKMS_GET_NON_PLT_WAREHOUSE_LIST_Result> componentWarehouselist = GetWarehouseList(project.Trim(), item, location.Trim(), dbCustom);
                                componentWarehouselist = componentWarehouselist.Where(u => u.finqhnd > 0).ToList();
                                if (componentWarehouselist != null)
                                    avlQty = Convert.ToDecimal(componentWarehouselist.Sum(u => u.finqhnd));

                                if (avlQty >= requiredQty)
                                {
                                    List<FKM121> objFKM121List = new List<FKM121>();

                                    #region WAREHOUSE LIST

                                    foreach (var wrh in componentWarehouselist)
                                    {
                                        bool IsError = false;
                                        decimal allocatedQty = 0;
                                        string warehouse = wrh.t_cwar;
                                        decimal t_qhnd = Convert.ToDecimal(wrh.finqhnd);

                                        decimal remainQty = Convert.ToDecimal(requiredQty - t_qhnd); //2 - 5
                                        if (remainQty >= 0)//1=1
                                        {
                                            allocatedQty = t_qhnd;
                                            requiredQty = remainQty;
                                        }
                                        else
                                        {
                                            allocatedQty = requiredQty;
                                            requiredQty = 0;
                                        }

                                        #region WEB SERVICE CALL

                                        frallocationservice objService = new frallocationservice();
                                        objService.quantitySpecified = true;
                                        objService.element = element;
                                        objService.location = location;
                                        objService.project = project;
                                        objService.quantity = allocatedQty;
                                        objService.item = new string(' ', 9) + item;
                                        objService.warehouse = warehouse;
                                        objService.workcenter = workCenter;
                                        objService.fullkitNo = FixtureNo;
                                        objService.logname = psno;
                                        objService.budgetLine = (long)Convert.ToDouble(findno);
                                        objService.budgetLineSpecified = true;
                                        objService.lineid = objFixtureItem.LineId;
                                        objService.reflineid = objFixtureItem.ParentId.Value;
                                        objService.refid = objFixture.Id;
                                        objService.fixtureno = objFixture.FixtureNo;
                                        objService.psno = psno;
                                        objService.findno = findno;

                                        InvokeAllocationService(objService, ref objFKM121List, ref IsError, ref errorMsg);

                                        if (IsManual && IsError)
                                        {
                                            objResponseMsg.Key = false;
                                            objResponseMsg.Value = !string.IsNullOrWhiteSpace(errorMsg) ? errorMsg : "Some error occured in allocation. Please try again.";
                                            break;
                                        }

                                        #endregion

                                        if (requiredQty == 0)
                                            break;
                                    }

                                    #endregion

                                    if (objFKM121List.Count > 0)
                                    {
                                        dbCustom.FKM121.AddRange(objFKM121List);
                                        dbCustom.SaveChanges();

                                        //if all qty allocated then delete error message records from FKM121.
                                        DeleteErrorMessageRecords(project, objFixtureItem.ParentId.Value, objFixture.Id, objFixtureItem.LineId, objFixtureItem.ItemType, dbCustom);
                                    }
                                }
                                else
                                {
                                    objResponseMsg.Key = false;
                                    objResponseMsg.Value = "Qty not available";

                                    if (IsManual)
                                        break;
                                }
                            }
                            else
                            {
                                objResponseMsg.Key = false;
                                if (!IsDeallocationSuccess)
                                    objResponseMsg.Value = !string.IsNullOrWhiteSpace(errorMsg) ? errorMsg : "Some error occured in deallocation. Please try again.";
                                else
                                    objResponseMsg.Value = "Req Qty should be greater than 0";

                                if (IsManual)
                                    break;
                            }
                        }
                    }
                }

                #endregion

                if (objResponseMsg.Key)
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Item allocated successfully";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message.ToString();
            }
            finally
            {
                #region DELETE RECORD FROM FKM122

                ManageAllocationLock(project, objFixtureItem.LineId, objApprovedFixture.LineId, NPLT_TYPE, allocateDelete, dbCustom);

                #endregion
            }
            return objResponseMsg;
        }

        //if all qty allocated then delete error message records from FKM121.
        public void DeleteErrorMessageRecords(string Project, int RefLineId, int RefId, int LineId, string ItemType, IEMQSEntitiesContext dbCustom)
        {
            try
            {
                decimal allocatedQty = GetTotalAllocatedQty(RefLineId, LineId, RefId, ItemType);
                decimal requiredQty = GetFixtureTotalReqQty(null, RefLineId, RefId, LineId);
                if (allocatedQty >= requiredQty)//4>=4
                {
                    //delete logic
                    var objFKM121List = db.FKM121.Where(x => x.Project == Project && x.RefLineId == RefLineId && x.RefId == RefId && x.LineId == LineId && !string.IsNullOrEmpty(x.ErrorMsg) && x.AllocatedQty <= 0).ToList();
                    if (objFKM121List.Count > 0)
                    {
                        dbCustom.FKM121.RemoveRange(objFKM121List);
                        dbCustom.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }

        public ActionResult CheckProjectAllocationInProgress(string sourceProject = "", int ParentNodeId = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                objResponseMsg.Key = true;
                if (sourceProject != "")
                {
                    if (db.FKM122.Any(i => i.Project == sourceProject))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Allocation already is in progress. Please Try Again After Sometime.";
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult FRAllocation(string sourceProject = "", int RefLineId = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                FRAllocationExecute(sourceProject, RefLineId);

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Allocation process is started. it will take time.";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public void FRAllocationExecute(string sourceProject = "", int RefLineId = 0)
        {
            var psno = objClsLoginInfo != null && !string.IsNullOrEmpty(objClsLoginInfo.UserName) ? objClsLoginInfo.UserName : "";
            var location = objClsLoginInfo != null && !string.IsNullOrEmpty(objClsLoginInfo.Location) ? objClsLoginInfo.Location : "";

            Task<int> task1 = LongRunningNPLTAllocationAsync(sourceProject, RefLineId, psno, location);
            Task<int> task2 = LongRunningPLTAllocationAsync(sourceProject, RefLineId, psno);
        }

        public async Task<int> LongRunningNPLTAllocationAsync(string sourceProject, int RefLineId, string psno, string location)
        {
            await Task.Run(() => NPLTAllocationAsync(sourceProject, RefLineId, psno, location));
            return 1;
        }

        public async Task<int> LongRunningPLTAllocationAsync(string sourceProject, int RefLineId, string psno)
        {
            await Task.Run(() => PLTAllocationAsync(sourceProject, RefLineId, psno));
            return 1;
        }

        public void PLTAllocationAsync(string sourceProject, int RefLineId, string psno)
        {
            IEMQSEntitiesContext db1 = new IEMQSEntitiesContext();
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();

            string result = string.Empty;
            string project = string.Empty;
            string fullKitNo = string.Empty;
            string ASM = clsImplementationEnum.NodeTypes.ASM.GetStringValue().ToLower();
            string TJF = clsImplementationEnum.NodeTypes.TJF.GetStringValue().ToLower();
            string PLT = clsImplementationEnum.NodeTypes.PLT.GetStringValue().ToLower();
            string PLT_TYPE = clsImplementationEnum.AllocateNodeTypes.PLT.ToString();
            string PROJ_PLT = clsImplementationEnum.AllocateNodeTypes.PROJ_PLT.ToString();

            string ApprovedStatus = clsImplementationEnum.FRStatus.Approved.GetStringValue().ToLower();

            clsManager objManager = new clsManager();

            try
            {
                //all fkm111 data
                var objFKM111List = db1.FKM111.ToList();
                var objFKM111ApprovedList = db1.FKM111.Where(u => u.Status == ApprovedStatus && u.ParentId == 0 && !u.ReUse && !u.Subcontracting && u.IsInsertedInPLM != null && u.IsInsertedInPLM.Value == true).ToList();
                var objFKM118List = db1.FKM118.ToList();

                if (sourceProject != string.Empty)
                {
                    objFKM111ApprovedList = (from u in objFKM111ApprovedList where u.Project.Trim() == sourceProject.Trim() select u).ToList();
                }

                if (RefLineId != 0)
                {
                    objFKM111ApprovedList = (from u in objFKM111ApprovedList where u.LineId == RefLineId select u).ToList();
                }

                var distinctProjectList = objFKM111ApprovedList.Select(u => u.Project).Distinct().ToList();
                foreach (var projitem in distinctProjectList)
                {
                    project = projitem;

                    ManageAllocationLock(project, 0, 0, PROJ_PLT, allocateInsert, db1);

                    var objPLTAllocationList = db1.SP_FKMS_GETPLTALLOCATEDETAILS(project).ToList();

                    #region Fixture Allocation

                    foreach (var objApprovedFixture in objFKM111ApprovedList)
                    {
                        string MainFixtureNo = objManager.GetFixtureNo(objApprovedFixture.Project, objApprovedFixture.FXRSrNo);

                        if (objPLTAllocationList.Count > 0)
                        {
                            try
                            {
                                //find all items for each fixture and allocate to LN.
                                var objFixtureItemList = (from u in objFKM111List
                                                          where u.ParentId == objApprovedFixture.LineId && u.Project == objApprovedFixture.Project && !u.ReUse && !u.Subcontracting
                                                          && u.IsInsertedInPLM != null && u.IsInsertedInPLM.Value == true
                                                          select u).ToList();

                                if (objFixtureItemList != null && objFixtureItemList.Count > 0)
                                {
                                    decimal FixtureQty = objFixtureItemList[0].QtyofFixture != null ? Convert.ToDecimal(objFixtureItemList[0].QtyofFixture) : 0;

                                    foreach (var objFixtureItem in objFixtureItemList)
                                    {
                                        IndividualPLTAllocation(objApprovedFixture, objFixtureItem, false, 0, db1);

                                        #region TEMP

                                        //#region INSERT RECORD INTO FKM122

                                        ////ManageAllocationLock(sourceProject, fullKitNo, NPLT, allocateInsert, db1);

                                        //#endregion

                                        //#region ALLOCATION PROCESS

                                        //string findno = objFixtureItem.FindNo;
                                        //decimal itemQty = objFixtureItem.Qty != null ? Convert.ToDecimal(objFixtureItem.Qty) : 0;

                                        //if (itemQty > 0)
                                        //{
                                        //    bool IsPLTItem = false;

                                        //    //check item type
                                        //    if (!string.IsNullOrWhiteSpace(objFixtureItem.ItemType))
                                        //    {
                                        //        if (objFixtureItem.ItemType.Trim().ToLower() == PLT)
                                        //        {
                                        //            IsPLTItem = true;
                                        //        }
                                        //    }

                                        //    if (IsPLTItem)
                                        //    {
                                        //        var objFixtureList = (from u in objFKM118List
                                        //                              where u.RefLineId == objApprovedFixture.LineId
                                        //                              orderby u.FixtureNo ascending
                                        //                              select u).ToList();

                                        //        foreach (var objFixture in objFixtureList)//F1, F2, F3
                                        //        {
                                        //            decimal requiredQty = itemQty;
                                        //            decimal usedQty = 0;
                                        //            decimal avlQty = 0;

                                        //            int LineId = objFixtureItem.LineId;
                                        //            RefLineId = objFixture.RefLineId;
                                        //            int RefId = objFixture.Id;

                                        //            #region CHECK ALLOCATION DONE

                                        //            if (db1.FKM120.Any(u => u.RefLineId == RefLineId && u.RefId == RefId && u.LineId == LineId))
                                        //            {
                                        //                string strTotalAllocatedQty = db1.FKM120.Where(u => u.RefLineId == RefLineId && u.RefId == RefId && u.LineId == LineId).Sum(u => u.AllocatedQty).ToString();
                                        //                decimal totalAllocatedQty = strTotalAllocatedQty != "" ? Convert.ToDecimal(strTotalAllocatedQty) : 0;
                                        //                if (requiredQty < totalAllocatedQty)//2<5
                                        //                {
                                        //                    #region DEALLOCATE LOGIC HERE

                                        //                    //deallocate totalAllocatedQty one by one from warehouse
                                        //                    var allocatedFKM120List = db1.FKM120.Where(u => u.RefLineId == RefLineId && u.LineId == LineId && u.RefId == RefId).ToList();
                                        //                    if (allocatedFKM120List != null)
                                        //                    {
                                        //                        db1.FKM120.RemoveRange(allocatedFKM120List);
                                        //                        db1.SaveChanges();
                                        //                    }

                                        //                    #endregion
                                        //                }
                                        //                else if (requiredQty > totalAllocatedQty)//5>2
                                        //                {
                                        //                    requiredQty = requiredQty - totalAllocatedQty;
                                        //                }
                                        //                else
                                        //                {
                                        //                    requiredQty = 0;
                                        //                }
                                        //            }

                                        //            #endregion

                                        //            if (requiredQty > 0)
                                        //            {
                                        //                if (objPLTAllocationList.Any(x => x.Partno == findno))
                                        //                {
                                        //                    usedQty = Convert.ToDecimal(db.FKM120.Where(x => x.FindNo == findno).ToList().Sum(x => x.AllocatedQty));
                                        //                    avlQty = Convert.ToDecimal(objPLTAllocationList.Where(x => x.Partno == findno).ToList().Sum(i => i.Qty));
                                        //                    if (usedQty > 0)
                                        //                        avlQty = avlQty - usedQty;
                                        //                }

                                        //                if (avlQty >= requiredQty)
                                        //                {
                                        //                    List<FKM120> objFKM120List = new List<FKM120>();
                                        //                    var filteredlist = objPLTAllocationList.Where(x => x.Partno == findno).OrderBy(x => x.PCRLineno).ToList();
                                        //                    foreach (var pclitem in filteredlist)
                                        //                    {
                                        //                        decimal allocatedQty = 0;
                                        //                        decimal t_qhnd = pclitem.Qty != null ? Convert.ToDecimal(pclitem.Qty) : 0;

                                        //                        decimal remainQty = Convert.ToDecimal(requiredQty - t_qhnd); //1-2
                                        //                        if (remainQty >= 0)//1=1
                                        //                        {
                                        //                            allocatedQty = t_qhnd;
                                        //                            requiredQty = remainQty;
                                        //                        }
                                        //                        else
                                        //                        {
                                        //                            allocatedQty = requiredQty;
                                        //                            requiredQty = 0;
                                        //                        }

                                        //                        //Add Entry in allocation table
                                        //                        FKM120 objFKM120 = new FKM120();
                                        //                        objFKM120.Project = project;
                                        //                        objFKM120.FindNo = findno;
                                        //                        objFKM120.PCRNo = pclitem.PCRNo;
                                        //                        objFKM120.PCRLineNo = pclitem.PCRLineno;
                                        //                        objFKM120.PCRLineRevNo = pclitem.PCRlinerev;
                                        //                        objFKM120.PCLNo = pclitem.PCLNo;
                                        //                        objFKM120.AllocatedQty = allocatedQty;
                                        //                        objFKM120.CreatedBy = psno;
                                        //                        objFKM120.CreatedOn = DateTime.Now;
                                        //                        objFKM120.LineId = objFixtureItem.LineId;
                                        //                        objFKM120.RefLineId = objFixtureItem.ParentId;
                                        //                        objFKM120.RefId = objFixture.Id;
                                        //                        objFKM120.FixtureNo = objFixture.FixtureNo;
                                        //                        objFKM120.ItemCode = objFixtureItem.ItemCode;
                                        //                        objFKM120List.Add(objFKM120);

                                        //                        if (requiredQty == 0)
                                        //                            break;
                                        //                    }

                                        //                    if (objFKM120List.Count > 0)
                                        //                    {
                                        //                        db1.FKM120.AddRange(objFKM120List);
                                        //                        db1.SaveChanges();
                                        //                    }
                                        //                }
                                        //            }
                                        //        }
                                        //    }
                                        //}

                                        //#endregion

                                        //#region DELETE RECORD FROM FKM122

                                        ////ManageAllocationLock(sourceProject, fullKitNo, NPLT, allocateDelete, db1);

                                        //#endregion

                                        #endregion
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
                            }
                        }
                    }

                    #endregion

                    ManageAllocationLock(project, 0, 0, PROJ_PLT, allocateDelete, db1);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Fixture items allocated successfully";
            }
            catch (Exception ex)
            {
                ManageAllocationLock(project, 0, 0, PROJ_PLT, allocateDelete, db1);

                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            finally
            {
                db1 = null;
            }
        }

        public void NPLTAllocationAsync(string sourceProject, int RefLineId, string psno, string location)
        {
            IEMQSEntitiesContext db1 = new IEMQSEntitiesContext();
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            FKSOBService serviceObj = new FKSOBService();
            string result = string.Empty;
            string project = string.Empty;
            string fullKitNo = string.Empty;
            string ASM = clsImplementationEnum.NodeTypes.ASM.GetStringValue().ToLower();
            string TJF = clsImplementationEnum.NodeTypes.TJF.GetStringValue().ToLower();
            string PLT = clsImplementationEnum.NodeTypes.PLT.GetStringValue().ToLower();
            string NPLT = clsImplementationEnum.AllocateNodeTypes.NPLT.ToString();
            string PROJ_NPLT = clsImplementationEnum.AllocateNodeTypes.PROJ_NPLT.ToString();

            string ApprovedStatus = clsImplementationEnum.FRStatus.Approved.GetStringValue().ToLower();

            clsManager objManager = new clsManager();

            try
            {
                //all fkm111 data
                var objFKM111List = db1.FKM111.ToList();
                var objFKM111ApprovedList = db1.FKM111.Where(u => u.Status == ApprovedStatus && u.ParentId == 0 && !u.ReUse && !u.Subcontracting && u.IsInsertedInPLM != null && u.IsInsertedInPLM.Value == true).ToList();

                if (!string.IsNullOrWhiteSpace(sourceProject))
                {
                    objFKM111ApprovedList = (from u in objFKM111ApprovedList where u.Project.Trim() == sourceProject.Trim() select u).ToList();
                }

                if (RefLineId != 0)
                {
                    objFKM111ApprovedList = (from u in objFKM111ApprovedList where u.LineId == RefLineId select u).ToList();
                }

                var distinctProjectList = objFKM111ApprovedList.Select(u => u.Project).Distinct().ToList();
                foreach (var projitem in distinctProjectList)
                {
                    project = projitem;

                    //get fixture list again if allocation for all projects
                    if (string.IsNullOrWhiteSpace(sourceProject))
                    {
                        objFKM111ApprovedList = (from u in objFKM111ApprovedList where u.Project.Trim() == project.Trim() select u).ToList();

                        if (RefLineId != 0)
                        {
                            objFKM111ApprovedList = (from u in objFKM111ApprovedList where u.LineId == RefLineId select u).ToList();
                        }
                    }

                    ManageAllocationLock(project, 0, 0, PROJ_NPLT, allocateInsert, db1);

                    #region Allocation

                    foreach (var objApprovedFixture in objFKM111ApprovedList)
                    {
                        try
                        {
                            //find all items for each fixture and allocate to LN.
                            var objFixtureItemList = (from u in objFKM111List
                                                      where u.ParentId == objApprovedFixture.LineId && u.Project == objApprovedFixture.Project && !u.ReUse && !u.Subcontracting
                                                       && u.IsInsertedInPLM != null && u.IsInsertedInPLM.Value == true
                                                      select u).ToList();

                            if (objFixtureItemList != null && objFixtureItemList.Count > 0)
                            {
                                foreach (var objFixtureItem in objFixtureItemList)
                                {
                                    IndividualNPLTAllocation(objApprovedFixture, objFixtureItem, false, 0, db1);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
                        }
                    }

                    #endregion

                    ManageAllocationLock(project, 0, 0, PROJ_NPLT, allocateDelete, db1);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Fixture items allocated successfully";
            }
            catch (Exception ex)
            {
                ManageAllocationLock(project, 0, 0, PROJ_NPLT, allocateDelete, db1);

                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            finally
            {
                db1 = null;
                serviceObj = null;
            }
        }

        public List<SP_FKMS_GET_NON_PLT_WAREHOUSE_LIST_Result> GetWarehouseList(string project, string item, string location, IEMQSEntitiesContext db1)
        {
            List<SP_FKMS_GET_NON_PLT_WAREHOUSE_LIST_Result> list = new List<SP_FKMS_GET_NON_PLT_WAREHOUSE_LIST_Result>();
            try
            {
                string query1 = "select t_cono from " + LNLinkedServer + ".dbo.ttpctm110175 where t_cprj= '" + project + "'";
                string contract = db1.Database.SqlQuery<string>(query1).FirstOrDefault();
                list = db1.SP_FKMS_GET_NON_PLT_WAREHOUSE_LIST(contract, item, location).ToList();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return list;
        }

        public bool NPLTDeallocation(FKM121 objFKM121, string psno, string location, IEMQSEntitiesContext dbCustom, ref string errorMsg)
        {
            errorMsg = "";
            bool IsSuccess = true;
            string workCenter = "";

            try
            {
                if (objFKM121 != null)
                {
                    var objFixture = dbCustom.FKM111.Where(x => x.LineId == objFKM121.RefLineId).FirstOrDefault();
                    if (objFixture != null && !string.IsNullOrWhiteSpace(objFixture.CreatedBy))
                        workCenter = dbCustom.COM003.Where(u => u.t_psno == objFixture.CreatedBy).FirstOrDefault().t_depc;
                }

                frdeallocationservice objDeallocation = new frdeallocationservice();
                objDeallocation.quantitySpecified = true;
                objDeallocation.project = objFKM121.Project;
                objDeallocation.location = location;
                objDeallocation.item = objFKM121.ItemCode.Trim();
                objDeallocation.quantity = objFKM121.AllocatedQty != null ? Convert.ToDecimal(objFKM121.AllocatedQty) : 0;
                objDeallocation.warehouse = objFKM121.Warehouse;
                objDeallocation.workcenter = workCenter;
                objDeallocation.fullkitNo = objFKM121.FixtureNo;
                objDeallocation.logname = psno;

                IsSuccess = InvokeDeallocationService(objDeallocation, objFKM121.Id, dbCustom, ref errorMsg);
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                errorMsg = ex.Message.ToString();
            }
            return IsSuccess;
        }

        public void InvokeAllocationService(frallocationservice obj, ref List<FKM121> objFKM121List, ref bool IsError, ref string errorMsg)
        {
            errorMsg = "";
            IsError = false;
            FKM121 objFKM121 = new FKM121();
            try
            {
                FKSOBService serviceObj = new FKSOBService();

                planner1sendrequestResponseType P1createResponse = new planner1sendrequestResponseType();
                planner1sendrequestRequestType P1createRequest = new planner1sendrequestRequestType();
                planner1sendrequestRequestTypeControlArea P1controlArea = new planner1sendrequestRequestTypeControlArea();
                planner1sendrequestRequestTypeFKSOB P1dataArea = new planner1sendrequestRequestTypeFKSOB();

                P1controlArea.processingScope = processingScope.request;
                P1dataArea.quantitySpecified = obj.quantitySpecified;
                P1dataArea.element = obj.element;
                P1dataArea.location = obj.location;
                P1dataArea.project = obj.project;
                P1dataArea.quantity = obj.quantity;
                P1dataArea.item = obj.item;
                P1dataArea.warehouse = obj.warehouse;
                P1dataArea.workcenter = obj.workcenter;
                P1dataArea.fullkitNo = obj.fullkitNo;
                P1dataArea.logname = obj.psno;
                P1dataArea.budgetLine = obj.budgetLine;
                P1dataArea.budgetLineSpecified = obj.budgetLineSpecified;

                P1createRequest.ControlArea = P1controlArea;
                P1createRequest.DataArea = new planner1sendrequestRequestTypeFKSOB[1];
                P1createRequest.DataArea[0] = P1dataArea;

                P1createResponse = serviceObj.planner1sendrequest(P1createRequest);
                if (P1createResponse.InformationArea == null)
                {
                    objFKM121.AllocatedQty = obj.quantity;
                }
                else
                {
                    objFKM121.AllocatedQty = 0;
                    objFKM121.ErrorMsg = P1createResponse.InformationArea[0].messageText.ToString();
                    errorMsg = objFKM121.ErrorMsg;
                }
            }
            catch (Exception ex)
            {
                IsError = true;
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                objFKM121.AllocatedQty = 0;
                objFKM121.ErrorMsg = ex.Message.ToString();
                errorMsg = objFKM121.ErrorMsg;
            }
            finally
            {
                objFKM121.Project = obj.project;
                objFKM121.FindNo = obj.findno;
                objFKM121.FixtureNo = obj.fixtureno;
                objFKM121.ItemCode = obj.item;
                objFKM121.Warehouse = obj.warehouse;
                objFKM121.CreatedBy = obj.psno;
                objFKM121.CreatedOn = DateTime.Now;
                objFKM121.LineId = obj.lineid;
                objFKM121.RefLineId = obj.reflineid;
                objFKM121.RefId = obj.refid;
                objFKM121List.Add(objFKM121);
            }
        }

        public bool InvokeDeallocationService(frdeallocationservice obj, int Id, IEMQSEntitiesContext dbCustom, ref string errorMsg)
        {
            errorMsg = "";
            bool IsSuccess = true;

            try
            {
                FKSOBService serviceObj = new FKSOBService();

                sfcReturnInventoryResponseType sfcCreateResponse = new sfcReturnInventoryResponseType();
                sfcReturnInventoryRequestType sfcCreateRequest = new sfcReturnInventoryRequestType();
                sfcReturnInventoryRequestTypeControlArea sfcControlArea = new sfcReturnInventoryRequestTypeControlArea();
                sfcReturnInventoryRequestTypeFKSOB sfcDataArea = new sfcReturnInventoryRequestTypeFKSOB();

                sfcControlArea.processingScope = processingScope.request;
                sfcDataArea.quantitySpecified = obj.quantitySpecified;
                sfcDataArea.project = obj.project;
                sfcDataArea.location = obj.location;
                sfcDataArea.item = new string(' ', 9) + obj.item;
                sfcDataArea.quantity = obj.quantity;
                sfcDataArea.warehouse = obj.warehouse;
                sfcDataArea.workcenter = obj.workcenter;
                sfcDataArea.fullkitNo = obj.fullkitNo;
                sfcDataArea.logname = obj.logname;

                sfcCreateRequest.ControlArea = sfcControlArea;
                sfcCreateRequest.DataArea = new sfcReturnInventoryRequestTypeFKSOB[1];
                sfcCreateRequest.DataArea[0] = sfcDataArea;

                sfcCreateResponse = serviceObj.sfcReturnInventory(sfcCreateRequest);

                if (sfcCreateResponse.InformationArea == null)
                {
                    IsSuccess = true;
                }
                else
                {
                    IsSuccess = false;
                    errorMsg = sfcCreateResponse.InformationArea[0].messageText.ToString();
                }
            }
            catch (Exception ex)
            {
                var RejectByShopMessage = "Record not found to process Return Inventory.";
                if (ex.Message.ToLower() == RejectByShopMessage.ToLower())
                {
                    var objFKM121 = dbCustom.FKM121.Where(i => i.Id == Id).FirstOrDefault();
                    if (objFKM121 != null)
                    {
                        dbCustom.FKM121.Remove(objFKM121);
                        dbCustom.SaveChanges();
                    }
                }
                IsSuccess = false;
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                errorMsg = ex.Message;
            }
            return IsSuccess;
        }

        public void ManageAllocationLock(string project, int lineId, int refLineId, string allocationType, string actionType, IEMQSEntitiesContext db1)
        {
            try
            {
                if (actionType == allocateInsert)
                {
                    if (refLineId != 0 && lineId != 0)
                    {
                        //fixture item level lock
                        if (!db1.FKM122.Any(x => x.Project == project && x.LineId == lineId && x.RefLineId == refLineId && x.AllocationType == allocationType))
                        {
                            FKM122 objFKM122 = new FKM122();
                            objFKM122.Project = project;
                            objFKM122.RefLineId = refLineId;
                            objFKM122.LineId = lineId;
                            objFKM122.AllocationType = allocationType;
                            db1.FKM122.Add(objFKM122);
                        }
                    }
                    else
                    {
                        //project level lock                        
                        if (!db1.FKM122.Any(x => x.Project == project && x.AllocationType == allocationType))
                        {
                            FKM122 objFKM122 = new FKM122();
                            objFKM122.Project = project;
                            objFKM122.AllocationType = allocationType;
                            db1.FKM122.Add(objFKM122);
                        }
                    }
                }
                else if (actionType == allocateDelete)
                {
                    if (refLineId != 0 && lineId != 0)
                    {
                        //fixture item level lock
                        if (db1.FKM122.Any(x => x.Project == project && x.LineId == lineId && x.RefLineId == refLineId && x.AllocationType == allocationType))
                        {
                            var objFKM122List = db1.FKM122.Where(x => x.Project == project && x.LineId == lineId && x.RefLineId == refLineId && x.AllocationType == allocationType).ToList();
                            db1.FKM122.RemoveRange(objFKM122List);
                        }
                    }
                    else
                    {
                        if (db1.FKM122.Any(x => x.Project == project && x.AllocationType == allocationType))
                        {
                            var objFKM122List = db1.FKM122.Where(x => x.Project == project && x.AllocationType == allocationType).ToList();
                            db1.FKM122.RemoveRange(objFKM122List);
                        }
                    }
                }
                db1.SaveChanges();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }

        public decimal GetTotalAllocatedQty(int RefLineId, int LineId, int RefId, string ItemType)
        {
            decimal totalQty = 0;
            if (!string.IsNullOrWhiteSpace(ItemType))
            {
                if (ItemType.Trim().ToLower() == clsImplementationEnum.NodeTypes.PLT.GetStringValue().ToLower())
                {
                    var objFKM120 = db.FKM120.Where(u => u.RefLineId == RefLineId).ToList();
                    if (LineId != 0 && objFKM120.Count > 0)
                    {
                        objFKM120 = objFKM120.Where(u => u.LineId == LineId).ToList();
                    }
                    if (RefId != 0 && objFKM120.Count > 0)
                    {
                        objFKM120 = objFKM120.Where(u => u.RefId == RefId).ToList();
                    }
                    if (objFKM120 != null)
                    {
                        var obj = objFKM120.Sum(x => x.AllocatedQty);
                        totalQty = obj != null ? obj.Value : 0;
                    }
                }
                else
                {
                    var objFKM121 = db.FKM121.Where(u => u.RefLineId == RefLineId).ToList();
                    if (LineId != 0 && objFKM121.Count > 0)
                    {
                        objFKM121 = objFKM121.Where(u => u.LineId == LineId).ToList();
                    }
                    if (RefId != 0 && objFKM121.Count > 0)
                    {
                        objFKM121 = objFKM121.Where(u => u.RefId == RefId).ToList();
                    }
                    if (objFKM121 != null)
                    {
                        var obj = objFKM121.Sum(x => x.AllocatedQty);
                        totalQty = obj != null ? obj.Value : 0;
                    }
                }
            }
            return totalQty;
        }

        public decimal GetFixtureTotalAllocatedQty(int RefLineId, int RefId)
        {
            decimal totalQty = 0;

            var objFKM120 = db.FKM120.Where(u => u.RefLineId == RefLineId).ToList();
            if (RefId != 0)
                objFKM120 = objFKM120.Where(u => u.RefId == RefId).ToList();

            var obj = objFKM120.Sum(x => x.AllocatedQty);
            totalQty += obj != null ? obj.Value : 0;

            var objFKM121 = db.FKM121.Where(u => u.RefLineId == RefLineId).ToList();
            if (RefId != 0)
                objFKM121 = objFKM121.Where(u => u.RefId == RefId).ToList();

            var obj1 = objFKM121.Sum(x => x.AllocatedQty);
            totalQty += obj1 != null ? obj1.Value : 0;

            return totalQty;
        }

        public decimal GetFixtureTotalReqQty(List<FKM111> objFKM111List, int RefLineId, int RefId, int LineId = 0)
        {
            decimal totalQty = 0;

            string ASM = clsImplementationEnum.NodeTypes.ASM.GetStringValue().ToLower();
            string TJF = clsImplementationEnum.NodeTypes.TJF.GetStringValue().ToLower();

            if (objFKM111List == null || objFKM111List.Count == 0)
                objFKM111List = db.FKM111.ToList();

            var itemList = objFKM111List.Where(u => u.ParentId == RefLineId && !string.IsNullOrEmpty(u.ItemType) && u.ItemType.Trim().ToLower() != ASM && u.ItemType.Trim().ToLower() != TJF && !u.ReUse && !u.Subcontracting).ToList();
            if (LineId != 0)
            {
                if (itemList.Count > 0)
                {
                    itemList = itemList.Where(u => u.LineId == LineId).ToList();
                }
            }
            foreach (var item in itemList)
            {
                if (RefId != 0)
                    totalQty += item.Qty != null ? item.Qty.Value : 0;
                else
                    totalQty += (item.QtyofFixture != null ? item.QtyofFixture.Value : 0) * (item.Qty != null ? item.Qty.Value : 0);
            }
            return totalQty;
        }

        public decimal GetFixtureTotalReqQtyFMG(List<FKM111_Log> objFKM111List, int RefLineId, int RefId, int LineId = 0)
        {
            decimal totalQty = 0;

            string ASM = clsImplementationEnum.NodeTypes.ASM.GetStringValue().ToLower();
            string TJF = clsImplementationEnum.NodeTypes.TJF.GetStringValue().ToLower();

            if (objFKM111List == null || objFKM111List.Count == 0)
                objFKM111List = db.FKM111_Log.ToList();

            var itemList = objFKM111List.Where(u => u.ParentId == RefLineId && !string.IsNullOrEmpty(u.ItemType) && u.ItemType.Trim().ToLower() != ASM && u.ItemType.Trim().ToLower() != TJF && !u.ReUse && !u.Subcontracting).ToList();
            if (LineId != 0)
            {
                if (itemList.Count > 0)
                {
                    itemList = itemList.Where(u => u.Id == LineId).ToList();
                }
            }
            foreach (var item in itemList)
            {
                if (RefId != 0)
                    totalQty += item.Qty != null ? item.Qty.Value : 0;
                else
                    totalQty += (item.QtyofFixture != null ? item.QtyofFixture.Value : 0) * (item.Qty != null ? item.Qty.Value : 0);
            }
            return totalQty;
        }
        #endregion

        #region Generate SOB Key

        [HttpPost]
        public ActionResult GenerateSOBKey(int RefId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                string ASM = clsImplementationEnum.NodeTypes.ASM.GetStringValue().ToLower();
                string TJF = clsImplementationEnum.NodeTypes.TJF.GetStringValue().ToLower();
                string PLT = clsImplementationEnum.NodeTypes.PLT.GetStringValue().ToLower();

                var objFKM118 = db.FKM118.Where(u => u.Id == RefId).FirstOrDefault();
                if (objFKM118 != null)
                {
                    if (string.IsNullOrWhiteSpace(objFKM118.MaterialStatus))
                    {
                        var objFKM111 = db.FKM111.Where(u => u.LineId == objFKM118.RefLineId).FirstOrDefault();

                        //If any non plate then generate SOB Key
                        if (db.FKM111.Any(i => i.ParentId == objFKM118.RefLineId && i.ItemType.Trim().ToLower() != ASM && i.ItemType.Trim().ToLower() != TJF && i.ItemType.Trim().ToLower() != PLT))
                        {
                            FKM123 objFKM123 = new FKM123();
                            objFKM123.Project = objFKM111.Project;
                            objFKM123.RefLineId = objFKM118.RefLineId;
                            objFKM123.RefId = RefId;
                            objFKM123.FixtureNo = objFKM118.FixtureNo;
                            objFKM123.CreatedBy = objClsLoginInfo.UserName;
                            objFKM123.CreatedOn = DateTime.Now;

                            FKSOBService serviceObj = new FKSOBService();

                            sfcoffertostoreResponseType P3createResponse = new sfcoffertostoreResponseType();
                            sfcoffertostoreRequestType P3createRequest = new sfcoffertostoreRequestType();
                            sfcoffertostoreRequestTypeControlArea P3controlArea = new sfcoffertostoreRequestTypeControlArea();
                            sfcoffertostoreRequestTypeFKSOB P3dataArea = new sfcoffertostoreRequestTypeFKSOB();

                            P3controlArea.processingScope = processingScope.request;
                            P3dataArea.quantitySpecified = true;

                            P3dataArea.project = "";
                            P3dataArea.location = "";
                            P3dataArea.quantity = 0;
                            P3dataArea.item = "";
                            P3dataArea.element = "";
                            P3dataArea.warehouse = "";
                            P3dataArea.workcenter = "";
                            P3dataArea.fullkitNo = objFKM123.FixtureNo;
                            P3dataArea.sobkey = "";
                            P3dataArea.logname = objClsLoginInfo.UserName;

                            try
                            {
                                P3createRequest.ControlArea = P3controlArea;
                                P3createRequest.DataArea = new sfcoffertostoreRequestTypeFKSOB[1];
                                P3createRequest.DataArea[0] = P3dataArea;

                                P3createResponse = serviceObj.sfcoffertostore(P3createRequest);

                                if (P3createResponse.InformationArea == null)
                                {
                                    objFKM123.SOBKey = P3createResponse.DataArea[0].sobkey;
                                    if (objFKM123.SOBKey != null)
                                    {
                                        string sobstatus = CheckSOBStatus(objFKM123.SOBKey);
                                        objFKM123.Status = sobstatus;

                                        if (sobstatus != string.Empty)
                                        {
                                            objFKM118.MaterialStatus = clsImplementationEnum.FRMaterialDeliveryStatus.Request.GetStringValue();
                                            db.SaveChanges();

                                            objResponseMsg.Key = true;
                                            objResponseMsg.Value = "Request generated successfully";
                                        }
                                        else
                                        {
                                            objResponseMsg.Key = false;
                                            objResponseMsg.Value = "SOB Status is empty";
                                        }
                                    }
                                    else
                                    {
                                        objResponseMsg.Key = false;
                                        objResponseMsg.Value = "SOB Key is null";
                                    }
                                }
                                else
                                {
                                    objFKM123.ErrorMsg = P3createResponse.InformationArea[0].messageText.ToString();
                                }
                            }
                            catch (Exception ex)
                            {
                                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                                objFKM123.ErrorMsg = ex.Message.ToString();
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = ex.Message.ToString();
                            }
                            finally
                            {
                                db.FKM123.Add(objFKM123);
                                db.SaveChanges();
                            }
                        }
                        else
                        {
                            objFKM118.KitLocation = "";
                            if (!string.IsNullOrWhiteSpace(objFKM111.FixMfg))
                            {
                                string workCenter = db.COM003.Where(u => u.t_psno == objFKM111.FixMfg).FirstOrDefault().t_depc;
                                var objDepartment = db.COM002.Where(i => i.t_dimx == workCenter && i.t_dtyp == 3).FirstOrDefault();
                                objFKM118.KitLocation = objDepartment != null ? objDepartment.t_dimx + "-" + objDepartment.t_desc : "";
                            }
                            objFKM118.DeliverStatus = clsImplementationEnum.FRMaterialDeliveryStatus.Delivered.GetStringValue();
                            objFKM118.MaterialStatus = clsImplementationEnum.FRMaterialDeliveryStatus.MaterialReceived.GetStringValue();
                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = "Request generated successfully";
                        }
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Status has been already updated. Please refresh the page.";
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Request not generated";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateSOBStatus(int RefId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (db.FKM123.Any(i => i.RefId == RefId && !string.IsNullOrEmpty(i.SOBKey) && i.Status != "Issued By Store"))
                {
                    var lstSOBkeys = db.FKM123.Where(i => i.RefId == RefId && !string.IsNullOrEmpty(i.SOBKey)).ToList();
                    foreach (var key in lstSOBkeys)
                    {
                        if (key.Status != "Issued By Store")
                        {
                            key.Status = CheckSOBStatus(key.SOBKey);
                        }
                    }
                    var objFKM118 = db.FKM118.Where(i => i.Id == RefId).FirstOrDefault();
                    if (lstSOBkeys.Count == lstSOBkeys.Where(i => i.RefId == RefId && !string.IsNullOrEmpty(i.SOBKey) && i.Status == "Issued By Store").Count())
                    {
                        var objFKM111 = db.FKM111.Where(x => x.LineId == objFKM118.RefLineId).FirstOrDefault();

                        string workCenter = db.COM003.Where(u => u.t_psno == objFKM111.FixMfg).FirstOrDefault().t_depc;
                        var objDepartment = db.COM002.Where(i => i.t_dimx == workCenter && i.t_dtyp == 3).FirstOrDefault();
                        objFKM118.KitLocation = objDepartment != null ? objDepartment.t_dimx + "-" + objDepartment.t_desc : "";

                        objFKM118.DeliverStatus = clsImplementationEnum.FRMaterialDeliveryStatus.Delivered.GetStringValue();
                        objFKM118.MaterialStatus = clsImplementationEnum.FRMaterialDeliveryStatus.MaterialReceived.GetStringValue();
                        objFKM118.EditedBy = objClsLoginInfo.UserName;
                        objFKM118.EditedOn = DateTime.Now;
                    }

                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "SOB Status updated successfully";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No record found";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public string GetCombineDeliveryStatus(int LineId,bool IsLog)
        {
            string CO = clsImplementationEnum.COP_CommonStatus.Co.GetStringValue();
            string IP = clsImplementationEnum.COP_CommonStatus.IP.GetStringValue();
            string NS = clsImplementationEnum.COP_CommonStatus.NS.GetStringValue();

            string DeliverStatus = string.Empty;
            if (IsLog)
            {
                var GetTotalCount = db.FKM118_Log.Where(x => x.RefId == LineId).Count();
                if (GetTotalCount == db.FKM118_Log.Where(x => x.RefId == LineId && x.DeliverStatus == CO).Count())
                    DeliverStatus = CO;
                else if (GetTotalCount == db.FKM118_Log.Where(x => x.RefId == LineId && (x.DeliverStatus == NS || string.IsNullOrEmpty( x.DeliverStatus))).Count())
                    DeliverStatus = NS;
                else
                    DeliverStatus = IP;
            }
            else
            {
                var GetTotalCount = db.FKM118.Where(x => x.RefLineId == LineId).Count();
                if (GetTotalCount == db.FKM118.Where(x => x.RefLineId == LineId && x.DeliverStatus == CO).Count())
                    DeliverStatus = CO;
                else if (GetTotalCount == db.FKM118.Where(x => x.RefLineId == LineId && (x.DeliverStatus == NS || string.IsNullOrEmpty( x.DeliverStatus))).Count())
                    DeliverStatus = NS;
                else
                    DeliverStatus = IP;
            }
            return DeliverStatus;
        }

        [HttpPost]
        public JsonResult UpdateDeliveryStatus(int LineId, int Qty, string DeliveryStatus, int totalqty)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                List<int> lstLineId = new List<int>();
                string CO = clsImplementationEnum.COP_CommonStatus.Co.GetStringValue();
                string IP = clsImplementationEnum.COP_CommonStatus.IP.GetStringValue();
                string NS = clsImplementationEnum.COP_CommonStatus.NS.GetStringValue();
                List<FKM118_Log> lstFKM118 = db.FKM118_Log.Where(i => i.RefId == LineId && i.DeliverStatus != CO).OrderBy(x => x.Id).ToList();
                var COBeforeChange = db.FKM118_Log.Count(i => i.RefId == LineId && i.DeliverStatus == CO);
                if (totalqty >= Qty)
                {
                    #region fetch only IP

                    List<FKM118_Log> lstFKM118Ip = new List<FKM118_Log>();
                    if (DeliveryStatus == NS)
                    { lstFKM118Ip = lstFKM118.Where(x => x.DeliverStatus == IP).OrderByDescending(x => x.LogId).Take(Qty).ToList(); }
                    if (DeliveryStatus == CO)
                    {
                        lstFKM118Ip = lstFKM118.Where(x => x.DeliverStatus == IP).OrderBy(x => x.LogId).Take(Qty).ToList();
                    }
                    foreach (var objFKM118 in lstFKM118Ip)
                    {
                        objFKM118.DeliverStatus = DeliveryStatus;
                        objFKM118.EditedBy = objClsLoginInfo.UserName;
                        objFKM118.EditedOn = DateTime.Now;
                        lstLineId.Add(objFKM118.Id);
                    }
                    #endregion

                    #region fetch NS
                    List<FKM118_Log> lstFKM118Ns = new List<FKM118_Log>();
                    var ipqty = Qty - lstFKM118Ip.Count();
                    if (ipqty > 0)
                    {
                        if (DeliveryStatus != NS)
                        {
                            lstFKM118Ns = lstFKM118.Where(x => (x.DeliverStatus == null || x.DeliverStatus == "" || x.DeliverStatus == NS)).OrderBy(x => x.LogId).Take(Qty).ToList();
                            foreach (var objFKM118 in lstFKM118Ns)
                            {
                                objFKM118.DeliverStatus = DeliveryStatus;
                                objFKM118.EditedBy = objClsLoginInfo.UserName;
                                objFKM118.EditedOn = DateTime.Now;
                                lstLineId.Add(objFKM118.Id);
                            }
                        }
                    }
                    #endregion
                
                    db.SaveChanges();
                    var objFKM111 = db.FKM111_Log.FirstOrDefault(x => x.Id == LineId);
                    if (objFKM111 != null)
                    {
                        if (lstLineId.Count() > 0)
                        {
                            if (DeliveryStatus == IP)
                                objFKM111.DeliverStatus = DeliveryStatus;
                            else
                            {

                                objFKM111.DeliverStatus = GetCombineDeliveryStatus(objFKM111.Id, true);
                            }
                            db.SaveChanges();
                        }
                    }

                    #region Add Inventory
                    var updatedCO = db.FKM118_Log.Count(w => w.RefId == LineId && w.DeliverStatus == CO) - COBeforeChange;
                    if (updatedCO > 0)
                    {
                        var objFKMControl = new MaintainFKMSController();
                        var objFKM130 = new FKM130();
                        objFKM130.Project = objFKM111.Project;
                        objFKM130.Department = objClsLoginInfo.Department;
                        objFKM130.ItemId = "";
                        objFKM130.PosNo_FixNo = objFKM111.FixtureName;
                        objFKM130.MaterialLocation = objClsLoginInfo.Department;// "FMG";
                        objFKM130.MaterialSubLocation = "";
                        objFKM130.MaterialOwner = MaterialOwner.FMG.GetStringValue();
                        objFKM130.MaterialType = MaterialType.FXR.GetStringValue();
                        objFKM130.PCLNo = "";
                        objFKM130.Qty = updatedCO;
                        objFKM130.Stage = StageType.FixtureCompleted.GetStringValue();
                        objFKM130.TransactionType = TransactionType.Received.GetStringValue();
                        objFKMControl.InsertToInventory(objFKM130);
                    }
                    #endregion

                    #region Update Main Table Deliver Status
                    if (objFKM111.DeliverStatus == CO)
                    {
                        string Approved = clsImplementationEnum.FRStatus.Approved.GetStringValue().ToLower();

                        var objMainFKM111 = db.FKM111.FirstOrDefault(x => x.LineId == objFKM111.LineId);
                        if (objMainFKM111 != null)
                        {
                            if (objFKM111.RevNo == objMainFKM111.RevNo && objMainFKM111.Status.ToLower() == Approved)
                            {
                                var LogFKM118 = db.FKM118_Log.Where(x => x.RefId == LineId).ToList();
                                foreach (var Log118 in LogFKM118)
                                {
                                    var FKM118 = db.FKM118.Where(x => x.Id == Log118.Id).FirstOrDefault();
                                    FKM118.DeliverStatus = DeliveryStatus;
                                    FKM118.EditedBy = objClsLoginInfo.UserName;
                                    FKM118.EditedOn = DateTime.Now;
                                }

                                objMainFKM111.DeliverStatus = CO;
                                db.SaveChanges();
                            }
                        }
                    }
                    #endregion

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Delivery Status updated successfully";
                    objResponseMsg.ActionValue = string.Join(",", lstLineId.ToList());
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Quantity should be less than Total Quantiy";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CompleteMaterialStatus(int RefId, string FullkitArea)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                var objFKM118 = db.FKM118.Where(u => u.Id == RefId).FirstOrDefault();

                objFKM118.DeliverStatus = clsImplementationEnum.FRMaterialDeliveryStatus.Completed.GetStringValue();
                objFKM118.FullkitAreaStatus = clsImplementationEnum.FullkitAreaStatus.PendingConfirmation.GetStringValue();
                objFKM118.FullkitArea = FullkitArea;
                objFKM118.KitLocation = string.Empty;
                objFKM118.RequestedBy = objClsLoginInfo.UserName;
                objFKM118.RequestedOn = DateTime.Now;
                objFKM118.EditedBy = objClsLoginInfo.UserName;
                objFKM118.EditedOn = DateTime.Now;
                db.SaveChanges();

                SendNotificationToFKM3ForFR(objFKM118.FixtureNo);
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Completed successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        private void SendNotificationToFKM3ForFR(string fixtureNo)
        {
            var listEmployeeForNotification = Manager.GetDepartmentRoleWiseEmployee(objClsLoginInfo.Location, "", UserRoleName.FMG3.GetStringValue());

            if (listEmployeeForNotification != null && listEmployeeForNotification.Count > 0)
            {
                var FKM3Emp = listEmployeeForNotification.Select(i => i.psno).ToList();
                var psno = string.Join(",", FKM3Emp);

                string message = $"Fixture No : {fixtureNo} has been submitted to Fullkit Area for your confirmation";

                (new clsManager()).SendNotification(UserRoleName.FMG3.GetStringValue(), "", "", objClsLoginInfo.Location, message, NotificationType.ActionRequired.GetStringValue(), "/FKMS/MaintainFKMS/PendingRequest", psno);
            }
        }

        public string CheckSOBStatus(string sobkey)
        {
            string result = string.Empty;
            try
            {
                if (sobkey != null)
                {
                    string query = "select b.t_desc " +
                                    "from " + LNLinkedServer + ".dbo.tltlnt505175 a with(nolock)join" +
                                    " " + LNLinkedServer + ".dbo.uvwGetEnumDesc b with(nolock) on a.t_trst = b.t_cnst and b.tableno = 'ltlnt505' and b.domain = 'ltlnt.trst'" +
                                    "where a.t_okey = '" + sobkey + "' group by a.t_okey, b.t_desc";
                    var avlQty = db.Database.SqlQuery<string>(query).FirstOrDefault();
                    if (avlQty != null)
                    {
                        result = avlQty;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //result = ex.InnerException.Message;
            }
            return result;
        }

        public void UpdatePLTDeliveryStatus(int RefLineId, int RefId, IEMQSEntitiesContext dbCustom)
        {
            string ASM = clsImplementationEnum.NodeTypes.ASM.GetStringValue().ToLower();
            string TJF = clsImplementationEnum.NodeTypes.TJF.GetStringValue().ToLower();
            string PLT = clsImplementationEnum.NodeTypes.PLT.GetStringValue().ToLower();

            var objFKM111 = dbCustom.FKM111.Where(u => u.LineId == RefLineId).FirstOrDefault();
            if (objFKM111 != null)
            {
                //if all items are plates in fixture, then update status as delivered
                var objFKM111List = dbCustom.FKM111.Where(u => u.ParentId == RefLineId && u.ItemType.Trim().ToLower() != ASM && u.ItemType.Trim().ToLower() != TJF).ToList();
                if (objFKM111List.All(u => u.ItemType.Trim().ToLower() == PLT))
                {
                    decimal totalReqQty = 0;
                    decimal totalAllocatedQty = 0;

                    var obj = objFKM111List.Sum(x => x.Qty);
                    totalReqQty = obj != null ? obj.Value : 0;

                    if (totalReqQty > 0)
                    {
                        var objFKM120 = dbCustom.FKM120.Where(u => u.RefLineId == RefLineId && u.RefId == RefId).Sum(x => x.AllocatedQty);
                        totalAllocatedQty = objFKM120 != null ? objFKM120.Value : 0;

                        if (totalAllocatedQty >= totalReqQty)
                        {
                            var objFKM118 = dbCustom.FKM118.Where(u => u.Id == RefId).FirstOrDefault();
                            objFKM118.KitLocation = "";
                            if (!string.IsNullOrWhiteSpace(objFKM111.FixMfg))
                            {
                                string workCenter = dbCustom.COM003.Where(u => u.t_psno == objFKM111.FixMfg).FirstOrDefault().t_depc;
                                var objDepartment = dbCustom.COM002.Where(i => i.t_dimx == workCenter && i.t_dtyp == 3).FirstOrDefault();
                                objFKM118.KitLocation = objDepartment != null ? objDepartment.t_dimx + "-" + objDepartment.t_desc : "";
                            }

                            objFKM118.DeliverStatus = clsImplementationEnum.FRMaterialDeliveryStatus.Delivered.GetStringValue();
                            objFKM118.MaterialStatus = clsImplementationEnum.FRMaterialDeliveryStatus.MaterialReceived.GetStringValue();

                            objFKM118.EditedBy = objClsLoginInfo.UserName;
                            objFKM118.EditedOn = DateTime.Now;
                            dbCustom.SaveChanges();
                        }
                    }
                }
            }
        }

        [HttpPost]
        public ActionResult CheckAttachment(int HeaderId, int RefLineId, int RefId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                var folderPath1 = "FKM118//" + HeaderId + "//" + RefLineId + "//" + RefId;
                //var existing1 = (new clsFileUpload()).GetDocuments(folderPath1);
                Utility.Controllers.FileUploadController _objFUC = new Utility.Controllers.FileUploadController();
                if (_objFUC.CheckAnyDocumentsExits(folderPath1,RefId))
                {
                    objResponseMsg.Key = true;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Attachment is required";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Resend to PLM

        public ActionResult GetFixtureNotExistInPLMGridDataPartial()
        {
            return PartialView("_GetFixtureNotExistInPLMGridDataPartial");
        }

        public ActionResult LoadFixtureNotLinkInPLM(JQueryDataTableParamModel param, int HeaderId)
        {
            try
            {

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = " RefHeaderId=" + HeaderId + " and ReUse=0 and Subcontracting=0 and ((Status='" + clsImplementationEnum.FRStatus.Approved.GetStringValue() + "' or RevNo>0) and (IsInsertedInPLM=0 or IsInsertedInPLM is null))";

                string[] columnName = { "ItemNo", "FixtureName", "DescriptionofItem", "PLMError" };

                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstPam = db.SP_FKMS_FR_GET_FIXTURE_NOT_INSERT_IN_PLM(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                var res = (from h in lstPam
                           select new[] {
                               h.ItemNo,
                               h.ParentId == 0 ? Convert.ToString(h.FixtureName):"",
                               Convert.ToString(h.DescriptionofItem),
                               Convert.ToString(h.PLMError)
                    }).ToList();
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    whereCondition = whereCondition,
                    strSortOrder = strSortOrder,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ResendFixturesForSaveInPLM(int HeaderId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                List<string> UsedFindNoList = new List<string>();

                string whereCondition = " RefHeaderId=" + HeaderId + " and ReUse=0 and Subcontracting=0 and ((Status='" + clsImplementationEnum.FRStatus.Approved.GetStringValue() + "' or RevNo>0) and (IsInsertedInPLM=0 or IsInsertedInPLM is null))";
                var listNotLinkInPLN = db.SP_FKMS_FR_GET_FIXTURE_NOT_INSERT_IN_PLM(0, int.MaxValue, "", whereCondition).ToList();

                if (listNotLinkInPLN.Count() > 0)
                {
                    string Project = listNotLinkInPLN.FirstOrDefault().Project;
                    UsedFindNoList = GetUsedFindNoList(Project);
                }

                foreach (var objItem in listNotLinkInPLN)
                {
                    if (!objItem.ReUse && !objItem.Subcontracting)
                    {
                        /* Obs. 27067: as per SOR for FKMS System*/
                        var objFKM111 = db.FKM111.FirstOrDefault(f => f.LineId == objItem.LineId);
                        objFKM111.IsInsertedInPLM = true;
                        objFKM111.PLMError = "";
                        db.SaveChanges();
                        /* if (objItem.ParentId == 0)
                             InsertPartAndBOMInPLM(objItem.LineId, 0, true, false, UsedFindNoList);
                         else
                             InsertPartAndBOMInPLM(objItem.ParentId.Value, objItem.LineId, false, true, UsedFindNoList);*/
                    }
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Process completed successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region View SOB Key Details

        [HttpPost]
        public ActionResult GetSOBKeyDetails(int RefId)
        {
            ViewBag.RefId = RefId;
            return PartialView("_SOBKeyDetails");
        }

        [HttpPost]
        public ActionResult LoadSOBKeyListData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                int RefId = Convert.ToInt32(param.CTQHeaderId);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                strWhere = "1=1 and RefId = " + RefId;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (SOBKey like '%" + param.sSearch +
                               "%' or Status like '%" + param.sSearch +
                               "%' or ErrorMsg like '%" + param.sSearch + "%')";
                }
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_FKMS_FR_GET_SOB_KEY_LIST(StartIndex, EndIndex, strSortOrder, strWhere).ToList();

                var data = (from fx in lstResult
                            select new[]
                            {
                                Convert.ToString(fx.ROW_NO),
                                Convert.ToString(fx.SOBKey),
                                Convert.ToString(fx.Status),
                                Convert.ToString(fx.ErrorMsg),
                                Convert.ToString(fx.Id),
                            }).ToList();
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }
        #endregion

        #region fixtureQTY

        [HttpPost]
        public ActionResult GetQtyFixtureSummary(int lineId)
        {
            string completed = clsImplementationEnum.FRMaterialDeliveryStatus.Completed.GetStringValue();
            ViewBag.lineid = lineId;
            ViewBag.CompleteQty = db.FKM118.Where(x => x.RefLineId == lineId && x.DeliverStatus == completed).Count();
            return PartialView("_GetQtyFixtureSummary");
        }

        [HttpPost]
        public ActionResult LoadFixtureSummaryGridData(JQueryDataTableParamModel param, string lineid)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = string.Empty;
                string PDINCondition = string.Empty;

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();
                var UserRole = objUserRoleAccessDetails.UserRole;

                whereCondition = "1=1 ";

                if (!string.IsNullOrWhiteSpace(lineid))
                    whereCondition += " and RefLineId= " + Convert.ToInt32(lineid);

                string[] columnName = { "Project", "NodeName", "FindNo", "FixtureNo" };

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }


                var lstResult = db.SP_FKMS_GET_FR_QTY_SUMMARY(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                var data = (from u in lstResult
                            select new[]
                            {
                                Convert.ToString(u.TempGroupNo),
                                Convert.ToString(u.ROW_NO), //0
                                Convert.ToString(u.TempGroupNo), //1
                                Convert.ToString(u.Id), //2
                                Convert.ToString(u.RefLineId), //3
                                u.TempGroupNo.Value == 1 ? u.FixtureNo : "", //4
                                u.Project, //5
                                Convert.ToString(u.FindNo), //6
                                u.NodeName, //7
                                //("<span class='editable'><a id='Delete' name='Delete' title='Delete' class='blue action' onclick='DeleteRecord("+ Convert.ToString(u.Id) +")'><i class='fa fa-trash'></i></a></span>&nbsp;&nbsp;")
                            }).ToList();

                int TotalCount = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount.Value > 0 ? lstResult.FirstOrDefault().TotalCount.Value : 0);

                TempData["SearchData"] = lstResult;

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = TotalCount,
                    iTotalDisplayRecords = TotalCount,
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult ReviseFixtureQty(int LineId, string strLineIds, int QtyofFixture)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                var objfkm111 = db.FKM111.Where(x => x.LineId == LineId).FirstOrDefault();
                if (objfkm111 != null)
                {
                    var list = strLineIds.Split(',').ToList();
                    if ((objfkm111.QtyofFixture - QtyofFixture) == list.Count())
                    {
                        List<FKM118> lstFKM118 = new List<FKM118>();
                        List<FKM119> lstFKM119 = new List<FKM119>();

                        foreach (var item in list)
                        {
                            int Id = Convert.ToInt32(item);
                            var objFKM118 = db.FKM118.Where(x => x.Id == Id).FirstOrDefault();

                            if (objFKM118 != null)
                            {
                                lstFKM118.Add(objFKM118);
                            }

                            var lstFKM118Log = db.FKM118_Log.Where(x => x.Id == Id).Select(x => x.LogId).Distinct().ToList();

                            foreach (var item1 in lstFKM118Log)
                            {
                                var lstFKM119Get = db.FKM119.Where(x => x.RefId == item1).ToList();
                                if (lstFKM119Get.Count > 0)
                                    db.FKM119.RemoveRange(lstFKM119Get);
                            }

                            //var objFKM119 = db.FKM119.Where(x => x.RefId == Id).ToList();
                            //if (objFKM119.Count > 0)
                            //{
                            //    lstFKM119.AddRange(objFKM119);
                            //}
                        }

                        if (lstFKM118.Count > 0)
                            db.FKM118.RemoveRange(lstFKM118);

                        //if (lstFKM119.Count > 0)
                        //    db.FKM119.RemoveRange(lstFKM119);

                        objfkm111.QtyofFixture = (objfkm111.QtyofFixture - lstFKM118.Count);

                        //string CO = clsImplementationEnum.COP_CommonStatus.Co.GetStringValue();

                        //if (objfkm111.DeliverStatus != CO)
                        //{
                        //    if (objfkm111.FKM118.Where(x => x.DeliverStatus == CO).Count() == objfkm111.FKM118.Count())
                        //    {
                        //        objfkm111.DeliverStatus = CO;
                        //    }
                        //}
                        db.SaveChanges();

                        objfkm111.DeliverStatus = GetCombineDeliveryStatus(objfkm111.LineId, false);
                        db.SaveChanges();
                        
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.FRMessage.ReviseSuccess;
                    }
                    else
                    {
                        if ((objfkm111.QtyofFixture - QtyofFixture) > list.Count())
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = clsImplementationMessage.FRMessage.ReviseMismatch;
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = clsImplementationMessage.FRMessage.ReviseMismatch;
                        }
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Fixture is not Available";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        [HttpPost]
        public JsonResult GetFullkitArea(string term = "")
        {
            List<AutoCompleteModel> lstFullkitArea = new List<AutoCompleteModel>();

            string location = objClsLoginInfo.Location;
            var lstFKM117 = db.FKM117.Where(x => x.Location == location && x.IsActive).ToList();

            if (!string.IsNullOrWhiteSpace(term))
            {
                lstFKM117 = (from u in lstFKM117
                             where u.FullKitArea.Trim().ToLower().Contains(term.Trim().ToLower())
                             select u).ToList();
            }

            lstFullkitArea = lstFKM117.Select(x => new AutoCompleteModel { Text = x.FullKitArea, Value = x.FullKitArea }).ToList();
            return Json(lstFullkitArea, JsonRequestBehavior.AllowGet);
        }

        public string GetUserRole()
        {
            UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();
            return objUserRoleAccessDetails.UserRole;
        }

        [NonAction]
        public static string GeneratePartButtonNew(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";


            htmlControl = "<a id='" + inputID + "' name='" + inputID + "' class='" + className + "' " + onClickEvent + " > " + buttonTooltip + "</a>";

            //htmlControl = "<i  data-modal='' id='" + inputID + "' name='" + inputID + "' style='cursor:Pointer;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";

            return htmlControl;
        }
 
        [SessionExpireFilter]
        public ActionResult ReWritePDFDocument(string LineIds)
        {
            var objResponseMsg = new clsHelper.ResponseMsgWithStatus();

            /*if (!objClsLoginInfo.GetUserRoleList().Contains(UserRoleName.PLNG2.GetStringValue()))
            {
                objResponseMsg.Value = "Access Denied!";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }

            if (string.IsNullOrWhiteSpace(LineIds))
            {
                objResponseMsg.Value = "Please select any LineId";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }*/

            var FailList = new List<int>();
            List<FKM111> lstFKM111 = null;
            if (string.IsNullOrWhiteSpace(LineIds))
            {
                lstFKM111 = db.FKM111.ToList();
            }
            else
            {
                var lstLineIds = LineIds.Split(',').Select(s => Convert.ToInt32(s));
                lstFKM111 = db.FKM111.Where(w => lstLineIds.Contains(w.LineId)).ToList();
            }
                
            foreach (var item in lstFKM111)
            {
                try
                {
                    var objFKM111List = db.FKM111.Where(x => x.RefHeaderId == (item.RefHeaderId > 0 ? item.RefHeaderId : x.RefHeaderId)).ToList();
                    var folderPath = "";
                    string RevNo = "0";
                    /*if (item.RefFixtureReuse > 0)
                    {
                        RevNo = GetReuseFixtureRefRevNo(objFKM111List, item.RefFixtureReuse)+"";
                        folderPath = "FKM111/" + GetReuseFixtureRefHeaderId(objFKM111List, item.RefFixtureReuse) + "/" + item.RefFixtureReuse + "/R" + RevNo;
                    }
                    else
                    {*/
                        RevNo = item.ParentId == 0 ? Convert.ToString(item.RevNo) : GetFixtureRevNo(objFKM111List, item.ParentId.Value);
                        folderPath = "FKM111/" + item.RefHeaderId + "/" + item.LineId + "/R" + RevNo;
                    //}
                    var fixtureNoName = item.ParentId == 0 ? Manager.GetFixtureNo(item.Project, item.FXRSrNo) : Manager.GetItemNo(item.Project, item.FXRSrNo);
                    var Documents = (new clsFileUpload()).GetDocuments(folderPath).Select(s => s.Name);
                    //Utility.Controllers.FileUploadController.FileUpload_WriteContentOnPDF(String.Join(",", Documents), folderPath, fixtureNoName + " (R" + RevNo + ")", 0, 0, 90, 0, "bottomleft");
                    //Utility.Controllers.FileUploadController.FileUpload_WriteContentOnPDF(String.Join(",", Documents), folderPath, fixtureNoName + " (R" + RevNo + ")", 0, 0, 0, 0, "topright");
                    Utility.Controllers.FileUploadController _objFUC = new Utility.Controllers.FileUploadController();
                    _objFUC.FileUpload_WriteContentOnPDF_FCS(folderPath, item.LineId, DESServices.CommonService.GetUseIPConfig, fixtureNoName + " (R" + RevNo + ")", 0, 0, 90, 0, "bottomleft");
                    _objFUC.FileUpload_WriteContentOnPDF_FCS(folderPath, item.LineId, DESServices.CommonService.GetUseIPConfig, fixtureNoName + " (R" + RevNo + ")", 0, 0, 0, 0, "topright");
                }
                catch (Exception ex)
                {
                    FailList.Add(item.LineId);
                }
            }
            if (FailList.Any())
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Line Id [" + string.Join(", ", FailList) + "] : are fail re-write document.";
            }
            else
            {
                objResponseMsg.Key = true;
                objResponseMsg.Value = "succesfully re-write document.";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        } 
    }

    public class LNItems
    {
        public string Item { get; set; }
        public string Description { get; set; }
        public string ItemCategory { get; set; }
        public string ItemType { get; set; }
    }

    public class frallocationservice : planner1sendrequestRequestTypeFKSOB
    {
        public string psno { get; set; }
        public string findno { get; set; }
        public int lineid { get; set; }
        public int reflineid { get; set; }
        public int refid { get; set; }
        public string fixtureno { get; set; }
    }
    public class frdeallocationservice : sfcReturnInventoryRequestTypeFKSOB
    {
        public string psno { get; set; }
        public string findno { get; set; }
        public int lineid { get; set; }
        public int reflineid { get; set; }
        public int refid { get; set; }
        public string fixtureno { get; set; }
    }
}
