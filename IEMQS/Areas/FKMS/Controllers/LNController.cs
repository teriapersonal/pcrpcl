﻿using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;

namespace IEMQS.Areas.FKMS.Controllers
{
    [Models.CustomHandleError]
    public class LNController : ApiController
    {
        // GET: api/FKMS/
        [Route("api/FKMS/Test")]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        [Route("api/FKMS/TestPost")]
        [HttpPost]
        public IEnumerable<string> Post()
        {
            return new string[] { "Postvalue1", "Postvalue2" };
        }

        [Route("api/FKMS/InsertToInventory")]
        [HttpPost]
        public ResultClass InsertToInventory([FromBody]RequestInventory model)
        {
            if (ModelState.IsValid)
            {
                var objResponseData = new ResultClass();
                try
                {
                    var ReqFields = new List<string>();
                    if (!clsImplementationEnum.getMaterialOwners().Contains(model.MaterialOwner))
                    {
                        ReqFields.Add("MaterialOwner");
                    }
                    if (!clsImplementationEnum.getMaterialTypes().Contains(model.MaterialType))
                    {
                        ReqFields.Add("MaterialType");
                    }
                    if (String.IsNullOrWhiteSpace(model.MaterialLocation) && model.TransactionType == clsImplementationEnum.TransactionType.Received.GetStringValue())
                    {
                        ReqFields.Add("MaterialLocation");
                    }
                    if (!clsImplementationEnum.getStageTypes().Contains(model.Stage))
                    {
                        ReqFields.Add("Stage");
                    }
                    if (!clsImplementationEnum.getTransactionTypes().Contains(model.TransactionType))
                    {
                        ReqFields.Add("TransactionType");
                    }
                    if (model.Qty < 0)
                    {
                        ReqFields.Add("Qty");
                    }
                    if (ReqFields.Any())
                    {
                        objResponseData.Status = false;
                        objResponseData.Message = "[" + string.Join(",", ReqFields) + "] value is invalid!";
                        return objResponseData;
                    }
                    using (var db = new IEMQSEntitiesContext())
                    {
                        if (model.MaterialType == clsImplementationEnum.MaterialType.FXR.GetStringValue() || model.MaterialType == clsImplementationEnum.MaterialType.KIT.GetStringValue())
                        {
                            var objFKM130 = new FKM130();
                            objFKM130.Project = model.Project;
                            objFKM130.MaterialType = model.MaterialType;
                            objFKM130.MaterialOwner = model.MaterialOwner;
                            objFKM130.Department = model.Department;
                            objFKM130.PosNo_FixNo = string.IsNullOrWhiteSpace(model.FindNo) ? model.PosNo_FixNo : model.FindNo;
                            objFKM130.ItemId = model.ItemId;
                            objFKM130.PCLNo = model.PCLNo;
                            objFKM130.Stage = model.Stage;
                            objFKM130.MaterialLocation = model.MaterialLocation;
                            objFKM130.MaterialSubLocation = model.MaterialSubLocation;
                            objFKM130.Qty = model.Qty;
                            objFKM130.TransactionType = model.TransactionType;
                            objFKM130.ReceivedfromInKit = model.ReceivedfromInKit;
                            objFKM130.KitNo = model.KitNo;
                            objFKM130.CreatedBy = model.UserPSNo;
                            objFKM130.CreatedOn = DateTime.Now;
                            db.FKM130.Add(objFKM130);
                            db.SaveChanges();
                            objResponseData.Status = true;
                            objResponseData.Message = "success";
                        }
                        else
                        {
                            var objPCLData = db.SP_FKMS_GET_PCR_PCL_DETAILS(model.Location, 0, 1, "", " Project = '" + model.Project + "' AND PCLNumber = '" + model.PCLNumber + "' AND PCLRevision = '" + model.PCLRevision + "' AND t_prtn = '" + model.FindNo + "' ").FirstOrDefault();
                            if (objPCLData != null)
                            {
                                var objFKM130 = new FKM130();
                                objFKM130.Project = objPCLData.Project.Trim();
                                objFKM130.MaterialType = model.MaterialType;
                                objFKM130.MaterialOwner = model.MaterialOwner;
                                objFKM130.Department = objPCLData.SFCDept.Trim();
                                objFKM130.PosNo_FixNo = objPCLData.FindNo;
                                objFKM130.ItemId = objPCLData.Item.Trim();
                                objFKM130.PCLNo = objPCLData.PCLNumber.Trim();
                                objFKM130.Stage = model.Stage;
                                objFKM130.MaterialLocation = model.MaterialLocation;
                                objFKM130.MaterialSubLocation = model.MaterialSubLocation;
                                objFKM130.Qty = model.Qty;
                                objFKM130.TransactionType = model.TransactionType;
                                objFKM130.ReceivedfromInKit = model.ReceivedfromInKit;
                                objFKM130.KitNo = model.KitNo;
                                objFKM130.CreatedBy = model.UserPSNo;
                                objFKM130.CreatedOn = DateTime.Now;
                                db.FKM130.Add(objFKM130);
                                db.SaveChanges();
                                objResponseData.Status = true;
                                objResponseData.Message = "success";
                            }
                            else
                            {
                                objResponseData.Status = false;
                                objResponseData.Message = "Record not found!!";
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    objResponseData.Status = false;
                    objResponseData.Message = ex.Message.ToString();
                    return objResponseData;
                }
                return objResponseData;
            }
            else
            {
                var errors = new List<string>();
                if (ModelState != null)
                {
                    foreach (var state in ModelState)
                    {
                        foreach (var error in state.Value.Errors)
                        {
                            errors.Add(error.ErrorMessage);
                        }
                    }
                }
                return new ResultClass { Status = false, Message = string.Join(Environment.NewLine, errors) };
            }
        }

        [Route("api/FKMS/ReleasedOutbound")]
        [HttpPost]
        public ResultClass ReleasedOutbound([FromBody]RequestIssueNonPlate model)
        {
            if (ModelState.IsValid)
            {
                var objResponseData = new ResultClass();
                try
                {
                    using (var db = new IEMQSEntitiesContext())
                    {
                        var objFKM112 = db.FKM112.FirstOrDefault(w => w.Project == model.Project && w.NodeKey == model.ItemId && w.SOBKey == model.SOBKey && w.FullKitNo == model.FullKitNo);
                        if (objFKM112 != null)
                        {
                            objFKM112.Status = "Issued by Store";
                            objFKM112.EditedBy = model.UserPSNo;
                            objFKM112.EditedOn = DateTime.Now;
                            db.SaveChanges();
                            objResponseData.Status = true;
                            objResponseData.Message = "success";
                        }
                        else
                        {
                            FKM112 FKM112 = new FKM112();
                            string[] str = model.FullKitNo.Split('-');
                            if (str.Length == 3)
                            {
                                int nodeid = Convert.ToInt32(str[2]);
                                var GetFindNo = db.FKM102.Where(x => x.NodeId == nodeid).FirstOrDefault().FindNo;

                                FKM112.Project = model.Project;
                                FKM112.NodeId = nodeid;
                                FKM112.FindNo = GetFindNo;
                                FKM112.NodeKey = model.ItemId;
                                FKM112.FullKitNo = model.FullKitNo;
                                FKM112.SOBKey = model.SOBKey;
                                FKM112.Status = "Issued by Store";
                                FKM112.CreatedBy = model.UserPSNo;
                                FKM112.CreatedOn = DateTime.Now;

                                db.FKM112.Add(FKM112);
                                db.SaveChanges();

                                objResponseData.Status = true;
                                objResponseData.Message = "success";
                            }
                            else
                            {
                                objResponseData.Status = false;
                                objResponseData.Message = "Record not found!!";
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    objResponseData.Status = false;
                    objResponseData.Message = ex.Message.ToString();
                    return objResponseData;
                }
                return objResponseData;
            }
            else
            {
                var errors = new List<string>();
                if (ModelState != null)
                {
                    foreach (var state in ModelState)
                    {
                        foreach (var error in state.Value.Errors)
                        {
                            errors.Add(error.ErrorMessage);
                        }
                    }
                }
                return new ResultClass { Status = false, Message = string.Join(Environment.NewLine, errors) };
            }
        }

        public class ResultClass
        {
            public bool Status { get; set; }
            public string Message { get; set; }
        }
          
        public class RequestInventory
        {
            public int Id { get; set; }
            [Required(ErrorMessage = "Project is required")]
            public string Project { get; set; }
            [Required(ErrorMessage = "MaterialType is required")]
            public string MaterialType { get; set; }
            [Required(ErrorMessage = "MaterialOwner is required")]
            public string MaterialOwner { get; set; }
            public string Department { get; set; }
            public string PosNo_FixNo { get; set; }
            public string ItemId { get; set; }
            public string PCLNo { get; set; }
            [Required(ErrorMessage = "Stage is required")]
            public string Stage { get; set; }
            public string MaterialLocation { get; set; }
            public string MaterialSubLocation { get; set; }
            [Required(ErrorMessage = "Qty is required")]
            public int Qty { get; set; }
            [Required(ErrorMessage = "TransactionType is required")]
            public string TransactionType { get; set; }
            public string ReceivedfromInKit { get; set; }
            public string CreatedBy { get; set; }
            public DateTime CreatedOn { get; set; }
            public string KitNo { get; set; }
            public bool IsAcknowledged { get; set; }
            [Required(ErrorMessage = "UserPSNo is required")]
            public string UserPSNo { get; set; }
            [Required(ErrorMessage = "Location is required")]
            public string Location { get; set; }
            [Required(ErrorMessage = "FindNo is required")]
            public string FindNo { get; set; }
            [Required(ErrorMessage = "PCLNumber is required")]
            public string PCLNumber { get; set; }
            [Required(ErrorMessage = "PCLRevision is required")]
            public string PCLRevision { get; set; }
        }
     
        public class RequestIssueNonPlate
        {
            [Required(ErrorMessage = "Project is required")]
            public string Project { get; set; }
            [Required(ErrorMessage = "FullKitNo is required")]
            public string FullKitNo { get; set; }
            [Required(ErrorMessage = "ItemId is required")]
            public string ItemId { get; set; }
            [Required(ErrorMessage = "SOBKey is required")]
            public string SOBKey { get; set; }
            [Required(ErrorMessage = "UserPSNo is required")]
            public string UserPSNo { get; set; }
        }
    }
  
}
