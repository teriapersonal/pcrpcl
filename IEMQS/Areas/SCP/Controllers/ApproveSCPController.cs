﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.SCP.Controllers
{
    public class ApproveSCPController : clsBase
    {
        // GET: SCP/ApproveSCP
        SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["LNConcerto"].ToString());


        #region Utility
        public string HTMLActionString(int rowId, string status, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            //if (!string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue()))
            //{
            //    htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
            //}
            htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";

            return htmlControl;
        }

        public static string GenerateTextboxFor(int rowId, string status, string columnName, string columnValue = "", string onBlurMethod = "", string onClickMethod = "", bool isReadOnly = false, string inputStyle = "")
        {
            string htmlControl = "";

            string inputID = columnName + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "form-control";

            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";
            if (string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue()))
            {
                htmlControl = "<input disabled type='text' id='" + inputID + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + "  />";
            }
            else
            {
                htmlControl = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + onClickEvent + "  />";
            }

            return htmlControl;
        }
        //public static string GenerateDropdown(int rowId, string columnName, SelectList itemList, string defaultSelectionText = "", string onChangeMethod = "", bool isReadOnly = false, string inputStyle = "")
        //{
        //    string selectOptions = "";
        //    string inputID = columnName + "" + rowId.ToString();
        //    string inputName = columnName;
        //    string className = "form-control col-md-3";
        //    string onChangedEvent = !string.IsNullOrEmpty(onChangeMethod) ? "onchange='" + onChangeMethod + "'" : "";

        //    if (defaultSelectionText.Length > 0)
        //    {
        //        selectOptions += "<option value=''>" + defaultSelectionText + "</option>";
        //    }

        //    foreach (var item in itemList)
        //    {
        //        if (item.Selected)
        //        {
        //            selectOptions += "<option selected value=" + item.Value + ">" + item.Text + "</option>";
        //        }
        //        else
        //        {
        //            selectOptions += "<option value=" + item.Value + ">" + item.Text + "</option>";
        //        }
        //    }

        //    return "<select id='" + inputID + "' name='" + inputID + "' " + onChangedEvent + " colname='" + columnName + "' class='" + className + "' style='" + inputStyle + "' >" + selectOptions + "</select>";

        //}

        public static string GenerateDropdown(int rowId, string status, string columnName, SelectList itemList, string defaultSelectionText = "", string onChangeMethod = "", string OnClickMethod = "", bool isReadOnly = false, string inputStyle = "")
        {
            string selectOptions = "";
            string inputID = columnName + "" + rowId.ToString();
            string inputName = columnName;
            string className = "form-control col-md-3";
            string onChangedEvent = !string.IsNullOrEmpty(onChangeMethod) ? "onchange='" + onChangeMethod + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(OnClickMethod) ? "onclick='" + OnClickMethod + "'" : "";
            if (defaultSelectionText.Length > 0)
            {
                selectOptions += "<option value=''>" + defaultSelectionText + "</option>";
            }

            foreach (var item in itemList)
            {
                if (item.Selected)
                {
                    selectOptions += "<option selected value=" + item.Value + ">" + item.Text + "</option>";
                }
                else
                {
                    selectOptions += "<option value=" + item.Value + ">" + item.Text + "</option>";
                }
            }
            if (string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue()))
            {
                return "<select id='" + inputID + "' disabled name='" + inputID + "' " + onChangedEvent + " colname='" + columnName + "' class='" + className + "' style='" + inputStyle + "' >" + selectOptions + "</select>";
            }
            else
            {
                return "<select id='" + inputID + "' name='" + inputID + "' " + onChangedEvent + " colname='" + columnName + "' class='" + className + "' style='" + inputStyle + "'  " + onClickEvent + " >" + selectOptions + "</select>";
            }
        }

        public string HTMLAutoComplete(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false, string maxlength = "", string validate = "")
        {
            string strAutoComplete = string.Empty;

            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "autocomplete form-control";
            string MaxCharcters = !string.IsNullOrEmpty(maxlength) ? "maxlength='" + maxlength + "'" : "";
            string FieldValidate = !string.IsNullOrEmpty(validate) ? "validate='" + validate + "'" : "";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";

            strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' hdElement='" + hdElementId + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + (disabled ? "disabled" : "") + " " + MaxCharcters + " " + FieldValidate + " />";

            return strAutoComplete;
        }

        #endregion

        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult GetHeaderGridDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("SCPApproveGridPartial");
        }

        [HttpPost]
        public JsonResult LoadSCPHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;

                var user = objClsLoginInfo.UserName;

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and status in('" + clsImplementationEnum.CTQStatus.SendForApprovel.GetStringValue() + "') and ApprovedBy=" + user+" ";
                }
                else
                {
                    strWhere += "1=1";
                }
               // strWhere += " and ApprovedBy=" + user;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += "and (Project like '%" + param.sSearch
                       + "%' or Document like '%" + param.sSearch
                       + "%' or Customer like '%" + param.sSearch
                       + "%' or RevNo like '%" + param.sSearch
                       + "%' or Product like '%" + param.sSearch
                       + "%' or ProcessLicensor like '%" + param.sSearch
                       + "%' or Status like '%" + param.sSearch + "%')";
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_SCP_GET_HEADERDETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(GetCodeAndNameByProject(uc.Project)),
                               Convert.ToString(uc.Document),
                               Convert.ToString(Manager.GetCustomerCodeAndNameByProject(uc.Project)),
                               Convert.ToString(uc.Product),
                               Convert.ToString(uc.ProcessLicensor),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.Status),
                                Convert.ToString(uc.SubmittedBy),
                                Convert.ToString(Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy")),
                                Convert.ToString(uc.ApprovedBy),
                                Convert.ToString(Convert.ToDateTime(uc.ApprovedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.ApprovedOn).ToString("dd/MM/yyyy")),

                               Convert.ToString(uc.HeaderId),
                              // Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter]
        public ActionResult ApproveSCPDetails(int Id = 0)
        {
            SCP001 objSCP001 = new SCP001();
            var user = objClsLoginInfo.UserName;

            var lstProjectDesc = (from a in db.COM001
                                  select new Projects { projectCode = a.t_cprj, projectDescription = a.t_cprj + " - " + a.t_dsca }).ToList();
            ViewBag.Project = new SelectList(lstProjectDesc, "projectCode", "projectDescription");

            List<string> lstProCat = clsImplementationEnum.getProductCategory().ToList();
            ViewBag.lstProCat = lstProCat.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

            var approver = (from t1 in db.ATH001
                            join t2 in db.COM003 on t1.Employee equals t2.t_psno
                            where t2.t_actv == 1
                            select new { t1.Employee, Desc = t2.t_psno + " - " + t2.t_name }).Distinct().ToList();
            ViewBag.approver = new SelectList(approver, "Employee", "Desc");
            if (Id > 0)
            {
                var project = (from a in db.SCP001
                               where a.HeaderId == Id
                               select a.Project).FirstOrDefault();
                ViewBag.Customer = Manager.GetCustomerCodeAndNameByProject(project);

                List<string> lstTestCategory = (from t1 in db.SCP003
                                                select t1.TaskCategory).Distinct().ToList();
                ViewBag.lstTestCategory = lstTestCategory.Select(i => new CategoryData { Value = i.ToString(), Code = i.ToString(), CategoryDescription = i.ToString() }).ToList();

                List<clsTask> list = new List<clsTask>();
                connection.Open();
                string query = "select  DISTINCT  CONTACT as 'Task Manager' " +
                                " from [dbo].[PROJ_TASK] " +
                                " where PROJECTNAME = @proj and CONTACT is not null " +
                                " ORDER BY CONTACT";
                SqlCommand cmd = new SqlCommand(query, connection);
                cmd.Parameters.AddWithValue("@proj", project);
                SqlDataReader reader = cmd.ExecuteReader();
                List<KeyValuePair<string, string>> lst = new List<KeyValuePair<string, string>>();
                while (reader.Read())
                {
                    list.Add(new clsTask { id = reader.GetString(0).ToString(), text = reader.GetString(0).ToString() });
                }

                reader.Close();
                connection.Close();
                ViewBag.lstTask = list.Select(i => new CategoryData { Value = i.text.ToString(), Code = i.text.ToString(), CategoryDescription = i.text.ToString() }).ToList();

                objSCP001 = db.SCP001.Where(x => x.HeaderId == Id).FirstOrDefault();
                if (objSCP001.ApprovedBy != null)
                {
                    if (objSCP001.ApprovedBy.Trim() != objClsLoginInfo.UserName.Trim())
                    {
                        ViewBag.IsDisplayMode = true;
                    }
                    else
                    {
                        ViewBag.IsDisplayMode = false;
                    }
                }
                else
                {
                    ViewBag.IsDisplayMode = true;
                }

                var PlanningDinID = db.PDN002.Where(x => x.RefId == Id && x.DocumentNo == objSCP001.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                ViewBag.PlanningDinID = PlanningDinID;
                ViewBag.Revoke = Manager.IsRevokeApplicable(PlanningDinID, Id, clsImplementationEnum.PlanList.Sub_Contracting_Plan.GetStringValue());
            }
            return View(objSCP001);

        }
        public ActionResult ReturnSelectedSCP(string strHeaderIds, string remarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string[] arrayHeaderIds = strHeaderIds.Split(',').ToArray();
                for (int i = 0; i < arrayHeaderIds.Length; i++)
                {
                    int HeaderId = Convert.ToInt32(arrayHeaderIds[i]);
                    SCP001 objSCP001 = db.SCP001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    objSCP001.Status = clsImplementationEnum.CTQStatus.Returned.GetStringValue();
                    objSCP001.ApprovedBy = objClsLoginInfo.UserName;
                    objSCP001.ApprovedOn = DateTime.Now;
                    objSCP001.ReturnRemark = remarks;
                    db.SaveChanges();
                    Manager.UpdatePDN002(objSCP001.HeaderId, objSCP001.Status, objSCP001.RevNo, objSCP001.Project, objSCP001.Document);

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue(),
                                                        objSCP001.Project,
                                                        "",
                                                        "",
                                                        Manager.GetPDINDocumentNotificationMsg(objSCP001.Project, clsImplementationEnum.PlanList.Sub_Contracting_Plan.GetStringValue(), objSCP001.RevNo.Value.ToString(), objSCP001.Status),
                                                        clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(),
                                                        Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Sub_Contracting_Plan.GetStringValue(), objSCP001.HeaderId.ToString(), false),
                                                        objSCP001.CreatedBy);
                    #endregion
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.QCPMessages.Return.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ApproveSelectedSCP(string strHeaderIds)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string[] arrayHeaderIds = strHeaderIds.Split(',').ToArray();
                for (int i = 0; i < arrayHeaderIds.Length; i++)
                {
                    int HeaderId = Convert.ToInt32(arrayHeaderIds[i]);

                    db.SP_SCP_APPROVE(HeaderId, objClsLoginInfo.UserName);
                    SCP001 objSCP001 = db.SCP001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    int newId = db.SCP001_Log.Where(q => q.HeaderId == HeaderId).OrderByDescending(q => q.ApprovedOn).FirstOrDefault().Id;
                    Manager.UpdatePDN002(objSCP001.HeaderId, objSCP001.Status, objSCP001.RevNo, objSCP001.Project, objSCP001.Document, newId);

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG3.ToString(),
                                                        objSCP001.Project,
                                                        "",
                                                        "",
                                                        Manager.GetPDINDocumentNotificationMsg(objSCP001.Project, clsImplementationEnum.PlanList.Sub_Contracting_Plan.GetStringValue(), objSCP001.RevNo.Value.ToString(), objSCP001.Status),
                                                        clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(),
                                                        Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Sub_Contracting_Plan.GetStringValue(), objSCP001.HeaderId.ToString(), false),
                                                        objSCP001.CreatedBy);
                    #endregion
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.QCPMessages.Approve.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        //public ActionResult LoadSCPLineData(JQueryDataTableParamModel param)
        //{
        //    try
        //    {
        //        var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
        //        var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
        //        int StartIndex = param.iDisplayStart + 1;
        //        int EndIndex = param.iDisplayStart + param.iDisplayLength;
        //        int headerid = Convert.ToInt32(param.Headerid);

        //        string strWhere = string.Empty;
        //        if (!string.IsNullOrWhiteSpace(param.sSearch))
        //        {
        //            strWhere = " (ReferenceDrawing like '%" + param.sSearch
        //                + "%' or ScopeOfWork like '%" + param.sSearch
        //                + "%' or TaskManager like '%" + param.sSearch
        //                + "%' or TaskDescription like '%" + param.sSearch

        //                + "%' or EstFor like '%" + param.sSearch
        //                + "%' or Weight like '%" + param.sSearch
        //                + "%' or RCCHours like '%" + param.sSearch
        //                + "%' or ExpMatDelDate like '%" + param.sSearch
        //                + "%' or ReqComDate like '%" + param.sSearch
        //                + "%' or SubConTo like '%" + param.sSearch
        //                + "%' or DeliverTo like '%" + param.sSearch
        //                + "%')";
        //        }
        //        else
        //        {
        //            strWhere = "1=1";
        //        }
        //        var lstResult = db.SP_SCP_GET_LINEDETAILS
        //                        (
        //                        StartIndex, EndIndex, "", headerid, strWhere
        //                        ).ToList();

        //        var data = (from uc in lstResult
        //                    select new[]
        //                    {
        //                   Convert.ToString(uc.ROW_NO),
        //                   Convert.ToString(uc.ReferenceDrawing).Replace(",","<br>"),
        //                   Convert.ToString(uc.ScopeOfWork).Replace(",","<br>"),
        //                   Convert.ToString(uc.TaskManager),
        //                   Convert.ToString(uc.TaskDescription),
        //                   Convert.ToString(uc.EstFor),
        //                   Convert.ToString(uc.Weight),
        //                   Convert.ToString(uc.RCCHours),
        //                   Convert.ToString(Convert.ToDateTime(uc.ExpMatDelDate).ToShortDateString()),
        //                   Convert.ToString(Convert.ToDateTime(uc.ReqComDate).ToShortDateString()),
        //                   Convert.ToString(uc.SubConTo),
        //                   Convert.ToString(uc.DeliverTo),

        //                   Convert.ToString(uc.HeaderId),
        //                   Convert.ToString(uc.LineId),
        //                   Convert.ToString(uc.IsLNLine)
        //                   }).ToList();


        //        return Json(new
        //        {
        //            sEcho = Convert.ToInt32(param.sEcho),
        //            iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
        //            iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
        //            aaData = data
        //        }, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        //        return Json(new
        //        {
        //            sEcho = param.sEcho,
        //            iTotalRecords = "0",
        //            iTotalDisplayRecords = "0",
        //            aaData = ""
        //        }, JsonRequestBehavior.AllowGet);
        //    }

        //}
        protected bool CheckDate(String date)
        {
            try
            {
                DateTime dt = DateTime.Parse(date);
                return true;
            }
            catch
            {
                return false;
            }
        }
        [HttpPost]
        public ActionResult LoadSCPLineData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                int headerid = Convert.ToInt32(param.Headerid);

                Session["Headerid"] = Convert.ToInt32(headerid);
                var isEditable = false;
                SCP001 ObjSCP001 = db.SCP001.Where(m => m.HeaderId == headerid).FirstOrDefault();
                if (ObjSCP001.Status.ToLower() == clsImplementationEnum.CTQStatus.SendForApprovel.GetStringValue().ToLower() && ObjSCP001.ApprovedBy==objClsLoginInfo.UserName)
                {
                    isEditable = true;
                }
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;

                string strWhere = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere = " (ReferenceDrawing like '%" + param.sSearch
                        + "%' or ScopeOfWork like '%" + param.sSearch
                        + "%' or TaskManager like '%" + param.sSearch
                        + "%' or TaskDescription like '%" + param.sSearch
                        + "%' or EstFor like '%" + param.sSearch
                        + "%' or Weight like '%" + param.sSearch
                        + "%' or RCCHours like '%" + param.sSearch
                        + "%' or ExpMatDelDate like '%" + param.sSearch
                        + "%' or ReqComDate like '%" + param.sSearch
                        + "%' or SubConTo like '%" + param.sSearch
                        + "%' or DeliverTo like '%" + param.sSearch
                        + "%')";
                }
                else
                {
                    strWhere = "1=1";
                }
                List<clsTask> list = new List<clsTask>();

                var lstResult = db.SP_SCP_GET_LINEDETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, headerid, strWhere
                                ).ToList();

                List<string> lstTestCategory = (from t1 in db.SCP003
                                                select t1.TaskCategory).Distinct().ToList();

                // List<clsTask> list = new List<clsTask>();
                //var scpproject = db.SCP002.Where(m => m.HeaderId == headerid).FirstOrDefault();
                //if (scpproject != null)
                //{
                //    connection.Open();
                //    string query = "select  DISTINCT  B.CONTACT as 'TaskManager' " +
                //                    " from[dbo].[VR_PROJECT_TASK_CHECKLIST]  a " +
                //                    " LEFT  join[dbo].[PROJ_TASK] b on a.PROJECTID = b.PROJECTID and a.TASKUNIQUEID = b.TASKUNIQUEID " +
                //                    " where a.CLITEMTEXT IS NOT NULL  AND B.CONTACT IS NOT NULL" +
                //                    " ORDER BY B.CONTACT";
                //    SqlCommand cmd = new SqlCommand(query, connection);
                //    cmd.Parameters.AddWithValue("@proj", scpproject.Project);
                //    SqlDataReader reader = cmd.ExecuteReader();
                //    while (reader.Read())
                //    {
                //        list.Add(new clsTask { id = reader.GetString(0).ToString(), text = reader.GetString(0).ToString() });
                //    }
                //    reader.Close();
                //    connection.Close();
                //}
                //else
                //{
                //    list.Add(new clsTask { id = "", text = "" });
                //}

                var taskcategory = db.SCP003.Select(i => new CategoryData { Value = i.TaskCategory, Code = i.TaskCategory, CategoryDescription = i.TaskCategory }).ToList();
                //  SelectList taskcategory = new SelectList(lstTestCategory, "TaskCategory", "TaskCategory");
                SelectList taskmanager = new SelectList(list, "text", "text");
                int newRecordId = 0;
                var newRecord = new[] {
                                        "",
                                        Helper.GenerateTextbox(newRecordId, "TaskDescription"),
                                       (new MaintainSCPController()).GenerateTextArea(newRecordId, "ScopeOfWork","","","",false,"width:200px;","1000"),
                                         (new MaintainSCPController()).GenerateTextArea(newRecordId, "ReferenceDrawing","","","",false,"width:200px;","1000"),
                                        HTMLAutoComplete(newRecordId, "TaskCategory","","",false,"","hdTaskCategory")+""+Helper.GenerateHidden(newRecordId,"hdTaskCategory"),
                                        HTMLAutoComplete(newRecordId, "TaskManager","","",false,"","hdTaskManager")+""+Helper.GenerateHidden(newRecordId,"hdTaskManager"),
                                        Helper.GenerateTextbox(newRecordId, "EstFor"),
                                        Helper.GenerateTextbox(newRecordId, "Weight"),
                                        Helper.GenerateTextbox(newRecordId, "ExpMatDelDate"),
                                        Helper.GenerateTextbox(newRecordId, "ReqComDate"),
                                        Helper.GenerateTextbox(newRecordId, "SubConTo"),
                                        Helper.GenerateTextbox(newRecordId, "RCCHours"),
                                        Helper.GenerateTextbox(newRecordId, "DeliverTo"),
                                        Helper.GenerateGridButton(newRecordId, "Save", "Add Rocord", "fa fa-plus", "SaveNewRecord();" ),
                                        "",
                                        Helper.GenerateHidden(newRecordId, "HeaderId", param.Headerid),
                                        ""
                                    };

                var data = (from uc in lstResult
                            select new[]
                            {
                               Convert.ToString(uc.ROW_NO),
                               uc.IsLNLine == true ? Convert.ToString(uc.TaskDescription) : isEditable == true ? GenerateTextboxFor(uc.LineId,"", "TaskDescription", uc.TaskDescription, "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", false) : Convert.ToString(uc.TaskDescription),
                               uc.IsLNLine == true ? Convert.ToString(uc.ScopeOfWork) == "" ?     Convert.ToString(uc.ScopeOfWork) :  "<ul><li>" + Convert.ToString(uc.ScopeOfWork).Replace("|", "</li><li>").ToString()+"</li></ul>": isEditable == true ? (new MaintainSCPController()).GenerateTextArea(uc.LineId, "ScopeOfWork", uc.ScopeOfWork.Replace("|","\n"), "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", false,"width:200px;","1000")  : Convert.ToString(uc.ScopeOfWork) == "" ?     Convert.ToString(uc.ScopeOfWork) :  "<ul><li>" + Convert.ToString(uc.ScopeOfWork).Replace("|", "</li><li>").ToString()+"</li></ul>",
                               uc.IsLNLine == true ? Convert.ToString(uc.ReferenceDrawing) == "" ?    Convert.ToString(uc.ReferenceDrawing) :   "<ul><li>"  +  Convert.ToString(uc.ReferenceDrawing).Replace("|", "</li><li>").ToString()+"</li></ul>" : isEditable == true ? (new MaintainSCPController()).GenerateTextArea(uc.LineId, "ReferenceDrawing",    Convert.ToString(uc.ReferenceDrawing.Replace("|","\n")),"UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");",false,"width:200px;","1000") : Convert.ToString(uc.ReferenceDrawing) == "" ?    Convert.ToString(uc.ReferenceDrawing) :  "<ul><li>" + Convert.ToString(uc.ReferenceDrawing).Replace("|", "</li><li>").ToString()+"</li></ul>",

                           // isEditable == false ? GenerateDropdown(uc.LineId,"", "TaskCategory", new SelectList(taskcategory, "Value", "Text", Convert.ToString(uc.TaskCategory)),"", "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", true) : uc.IsLNLine == true ? GenerateDropdown(uc.LineId,"", "TaskCategory",new SelectList(taskcategory, "Value", "Text", Convert.ToString(uc.TaskCategory)),"", "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", false) : GenerateDropdown(uc.LineId,"", "TaskCategory",new SelectList(taskcategory, "Value", "Text", Convert.ToString(uc.TaskCategory)), "","UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", false),
                           // isEditable == false ? GenerateDropdown(uc.LineId,"", "TaskCategory", new SelectList(taskcategory, "Value", "Text", Convert.ToString(uc.TaskCategory)),"", "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", true) : uc.IsLNLine == true ? GenerateDropdown(uc.LineId,"", "TaskCategory",new SelectList(taskcategory, "Value", "Text", Convert.ToString(uc.TaskCategory)),"", "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", false) : GenerateDropdown(uc.LineId,"", "TaskCategory",new SelectList(taskcategory, "Value", "Text", Convert.ToString(uc.TaskCategory)), "","UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", false),
                             isEditable == true ? HTMLAutoComplete(uc.LineId, "TaskCategory",uc.TaskCategory,"UpdateData(this, "+ uc.LineId +");",false,"","hdTaskCategory")+""+Helper.GenerateHidden(uc.LineId, "hdTaskCategory") : HTMLAutoComplete(uc.LineId, "TaskCategory",uc.TaskCategory,"UpdateData(this, "+ uc.LineId +");",true,"","hdTaskCategory")+""+Helper.GenerateHidden(uc.LineId, "hdTaskCategory"),
                             uc.IsLNLine == true ? HTMLAutoComplete(uc.LineId, "TaskManager",uc.TaskManager,"UpdateData(this, "+ uc.LineId +");",true,"","hdTaskManager")+""+Helper.GenerateHidden(uc.LineId, "hdTaskManager") : isEditable == true ? HTMLAutoComplete(uc.LineId, "TaskManager",uc.TaskManager,"UpdateData(this, "+ uc.LineId +");",false,"","hdTaskManager")+""+Helper.GenerateHidden(uc.LineId, "hdTaskManager") : HTMLAutoComplete(uc.LineId, "TaskManager",uc.TaskManager,"UpdateData(this, "+ uc.LineId +");",true,"","hdTaskManager")+""+Helper.GenerateHidden(uc.LineId, "hdTaskManager"),
                


                           //// isEditable == false ? GenerateDropdown(uc.LineId,"", "TaskManager", new SelectList(taskmanager, "text", "text", Convert.ToString(uc.TaskManager)),"", "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", true) : uc.IsLNLine == true ? GenerateDropdown(uc.LineId,"", "TaskManager",new SelectList(taskmanager, "text", "text", Convert.ToString(uc.TaskManager)),"", "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", false) : GenerateDropdown(uc.LineId,"", "TaskManager",new SelectList(taskmanager, "text", "text", Convert.ToString(uc.TaskManager)), "","UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", false),
                           
                           //  isEditable == false ? GenerateTextboxFor(uc.LineId,"", "TaskDescription", uc.TaskDescription, "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", true) : uc.IsLNLine == true ? GenerateTextboxFor(uc.LineId,"", "TaskDescription", uc.TaskDescription, "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", false) : GenerateTextboxFor(uc.LineId,"", "TaskDescription", uc.TaskDescription, "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", false),
                             isEditable == false ? GenerateTextboxFor(uc.LineId,"", "EstFor", uc.EstFor, "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", true) : uc.IsLNLine == true ? GenerateTextboxFor(uc.LineId,"", "EstFor", uc.EstFor, "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", false) : GenerateTextboxFor(uc.LineId,"", "EstFor", uc.EstFor, "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", false),
                             isEditable == false ? GenerateTextboxFor(uc.LineId,"", "Weight", Convert.ToString(uc.Weight), "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", true) : uc.IsLNLine == true ? GenerateTextboxFor(uc.LineId,"", "Weight", Convert.ToString(uc.Weight), "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", false) : GenerateTextboxFor(uc.LineId,"", "Weight", Convert.ToString(uc.Weight), "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", false),

                             isEditable == false ? GenerateTextboxFor(uc.LineId,"", "ExpMatDelDate", Convert.ToDateTime(uc.ExpMatDelDate).ToString("yyyy-MM-dd"), "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", true) : uc.IsLNLine == true ? GenerateTextboxFor(uc.LineId,"", "ExpMatDelDate", Convert.ToDateTime(uc.ExpMatDelDate).ToString("yyyy-MM-dd"), "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", false) : GenerateTextboxFor(uc.LineId,"", "ExpMatDelDate", Convert.ToDateTime(uc.ExpMatDelDate).ToString("yyyy-MM-dd"), "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", false),
                             isEditable == false ? GenerateTextboxFor(uc.LineId,"", "ReqComDate", Convert.ToDateTime(uc.ReqComDate).ToString("yyyy-MM-dd"), "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", true) : uc.IsLNLine == true ? GenerateTextboxFor(uc.LineId,"", "ReqComDate", Convert.ToDateTime(uc.ReqComDate).ToString("yyyy-MM-dd"), "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", false) : GenerateTextboxFor(uc.LineId,"", "ReqComDate", Convert.ToDateTime(uc.ReqComDate).ToString("yyyy-MM-dd"), "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", false),
                             isEditable == false ? GenerateTextboxFor(uc.LineId,"", "SubConTo", uc.SubConTo, "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", true) : uc.IsLNLine == true ? GenerateTextboxFor(uc.LineId,"", "SubConTo", uc.SubConTo, "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", false) : GenerateTextboxFor(uc.LineId,"", "SubConTo", uc.SubConTo, "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", false),
                             isEditable == false ? GenerateTextboxFor(uc.LineId,"", "RCCHours", Convert.ToString(uc.RCCHours), "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", true) : uc.IsLNLine == true ? GenerateTextboxFor(uc.LineId,"", "RCCHours", Convert.ToString(uc.RCCHours), "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", false) : GenerateTextboxFor(uc.LineId,"", "RCCHours", Convert.ToString(uc.RCCHours), "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", false),
                             isEditable == false ? GenerateTextboxFor(uc.LineId,"", "DeliverTo", uc.DeliverTo, "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", true) : uc.IsLNLine == true ? GenerateTextboxFor(uc.LineId,"", "DeliverTo", uc.DeliverTo, "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", false) : GenerateTextboxFor(uc.LineId,"", "DeliverTo", uc.DeliverTo, "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", false),
                             isEditable == false ? null : uc.IsLNLine == false ? HTMLActionString(uc.HeaderId,"","Delete","Delete Record","fa fa-trash-o","DeleteRecord("+ uc.LineId +");") : null,
                             Helper.GenerateHidden(uc.LineId, "LineId", uc.LineId.ToString()),
                             Helper.GenerateHidden(uc.HeaderId, "HeaderId", uc.HeaderId.ToString()),
                             Convert.ToString(uc.IsLNLine)
                           }).ToList();

                data.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);




            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }
        [HttpPost]
        public ActionResult SaveSCPHeader(SCP001 scp001, FormCollection fc)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                if (!string.IsNullOrWhiteSpace(fc["hfHeaderId"].ToString()) && Convert.ToInt32(fc["hfHeaderId"].ToString()) > 0)
                {
                    int hid = Convert.ToInt32(fc["hfHeaderId"].ToString());

                    SCP001 objSCP001 = db.SCP001.Where(x => x.HeaderId == hid).FirstOrDefault();
                    string rev = Regex.Replace(fc["txtRev"].ToString(), "[^0-9]+", string.Empty);
                    objSCP001.Project = fc["hfProject"].ToString();
                    objSCP001.Document = fc["txtdoc"].ToString();
                    objSCP001.Customer = fc["txtCust"].ToString().Split('-')[0];
                    if (objSCP001.Status == clsImplementationEnum.PTMTCTQStatus.Approved.GetStringValue())
                    {
                        objSCP001.RevNo = Convert.ToInt32(objSCP001.RevNo) + 1;
                        objSCP001.Status = clsImplementationEnum.PTMTCTQStatus.DRAFT.GetStringValue();
                    }
                    if (string.IsNullOrWhiteSpace(fc["txtCdd"].ToString()) || fc["txtCdd"].ToString() == "01-01-0001" || fc["txtCdd"].ToString() == "01/01/0001")
                    {
                        objSCP001.CDD = null;
                    }
                    else
                    {
                        objSCP001.CDD = Convert.ToDateTime(fc["txtCdd"].ToString());
                    }
                    //objSCP001.Product = fc["ddlCat"].ToString();
                    objSCP001.ProcessLicensor = fc["txtlicence"].ToString();
                    objSCP001.TaskManager = string.IsNullOrEmpty(fc["ddlTask"]) ? "" : fc["ddlTask"].ToString();

                    objSCP001.EditedBy = objClsLoginInfo.UserName;
                    objSCP001.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Revision = "R" + objSCP001.RevNo.ToString();
                    objResponseMsg.status = objSCP001.Status;
                    Manager.UpdatePDN002(objSCP001.HeaderId, objSCP001.Status, objSCP001.RevNo, objSCP001.Project, objSCP001.Document);
                }
                else
                {
                    string project = fc["ddlProject"].ToString();
                    var isvalid = db.SCP001.Any(x => x.Project == project);
                    if (isvalid == false)
                    {
                        string rev = Regex.Replace(fc["txtRev"].ToString(), "[^0-9]+", string.Empty);
                        SCP001 objSCP001 = new SCP001();
                        objSCP001.Project = fc["ddlProject"].ToString();
                        objSCP001.Document = fc["txtdoc"].ToString();
                        objSCP001.Customer = fc["txtCust"].ToString().Split('-')[0];
                        objSCP001.RevNo = Convert.ToInt32(rev);
                        objSCP001.Status = clsImplementationEnum.CTQStatus.DRAFT.GetStringValue();
                        if (string.IsNullOrWhiteSpace(fc["txtCdd"].ToString()) || fc["txtCdd"].ToString() == "01-01-0001" || fc["txtCdd"].ToString() == "01/01/0001")
                        {
                            objSCP001.CDD = null;
                        }
                        else
                        {
                            objSCP001.CDD = Convert.ToDateTime(fc["txtCdd"].ToString());
                        }

                        objSCP001.Product = fc["ddlCat"].ToString();
                        objSCP001.ProcessLicensor = fc["txtlicence"].ToString();
                        objSCP001.TaskManager = fc["ddlTask"].ToString();
                        objSCP001.CreatedBy = objClsLoginInfo.UserName;
                        objSCP001.CreatedOn = DateTime.Now;
                        db.SCP001.Add(objSCP001);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Revision = "R" + objSCP001.RevNo.ToString();
                        objResponseMsg.status = objSCP001.Status;
                        Manager.UpdatePDN002(objSCP001.HeaderId, objSCP001.Status, objSCP001.RevNo, objSCP001.Project, objSCP001.Document);
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.Message.ToString();
                    }

                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult RevokeDocument(int HeaderId, int pdinId, string Remarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (Manager.IsRevokeApplicable(pdinId, HeaderId, clsImplementationEnum.PlanList.Sub_Contracting_Plan.GetStringValue()))
                {
                    objResponseMsg.Key = Manager.RevokePDINDocument(HeaderId, clsImplementationEnum.PlanList.Sub_Contracting_Plan, Remarks);
                    if (objResponseMsg.Key)
                    {
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Revoke;
                    }
                    else
                    {
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.UnSuccess;
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.pdnRevokeMessage;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult GetInsertView(string project, string document, string RevNo)
        {
            project = project.Split('-')[0].Trim();
            int Headerid = (from a in db.SCP001
                            where a.Project == project
                            select a.HeaderId).FirstOrDefault();
            List<string> lstTestCategory = (from t1 in db.SCP003
                                            select t1.TaskCategory).Distinct().ToList();

            ViewBag.lstTestCategory = lstTestCategory.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();
            ViewBag.project = project;
            ViewBag.document = document;
            ViewBag.RevNo = RevNo;
            ViewBag.Headerid = Headerid;

            List<clsTask> list = new List<clsTask>();
            connection.Open();
            string query = "select  DISTINCT  B.CONTACT as 'Task Manager' " +
                            " from[dbo].[VR_PROJECT_TASK_CHECKLIST]  a " +
                            " LEFT  join[dbo].[PROJ_TASK] b on a.PROJECTID = b.PROJECTID and a.TASKUNIQUEID = b.TASKUNIQUEID " +
                            " where a.PROJECTNAME = @proj and a.CLITEMTEXT IS NOT NULL " +
                            " ORDER BY B.CONTACT";
            SqlCommand cmd = new SqlCommand(query, connection);
            cmd.Parameters.AddWithValue("@proj", project);
            SqlDataReader reader = cmd.ExecuteReader();
            List<KeyValuePair<string, string>> lst = new List<KeyValuePair<string, string>>();
            while (reader.Read())
            {
                list.Add(new clsTask { id = reader.GetString(0).ToString(), text = reader.GetString(0).ToString() });
            }

            reader.Close();
            connection.Close();
            ViewBag.lstTask = new SelectList(list, "id", "text");

            SCP002 objSCP002 = new SCP002();
            return PartialView("SCPApproveLines", objSCP002);
        }
        [HttpPost]
        public ActionResult GetUpdateView(string project, string document, string RevNo, int lineid, string isln)
        {
            project = project.Split('-')[0].Trim();
            SCP002 objSCP002 = new SCP002();
            var isvalid = db.SCP002.Any(x => x.LineId == lineid);
            if (isvalid == true)
            {
                objSCP002 = db.SCP002.Where(x => x.LineId == lineid).FirstOrDefault();
                if (objSCP002 != null)
                {
                    int Headerid = (from a in db.SCP001
                                    where a.Project == project
                                    select a.HeaderId).FirstOrDefault();
                    ViewBag.project = project;
                    ViewBag.document = document;
                    ViewBag.RevNo = RevNo;
                    ViewBag.Headerid = Headerid;
                    ViewBag.Lineid = lineid;
                    ViewBag.isln = isln;
                    List<string> lstTestCategory = (from t1 in db.SCP003
                                                    select t1.TaskCategory).Distinct().ToList();

                    ViewBag.lstTestCategory = lstTestCategory.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();
                    List<clsTask> list = new List<clsTask>();
                    connection.Open();
                    string query = "select  DISTINCT  B.CONTACT as 'Task Manager' " +
                                    " from[dbo].[VR_PROJECT_TASK_CHECKLIST]  a " +
                                    " LEFT  join[dbo].[PROJ_TASK] b on a.PROJECTID = b.PROJECTID and a.TASKUNIQUEID = b.TASKUNIQUEID " +
                                    " where a.PROJECTNAME = @proj and a.CLITEMTEXT IS NOT NULL " +
                                    " ORDER BY B.CONTACT";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    cmd.Parameters.AddWithValue("@proj", project);
                    SqlDataReader reader = cmd.ExecuteReader();
                    List<KeyValuePair<string, string>> lst = new List<KeyValuePair<string, string>>();
                    while (reader.Read())
                    {
                        list.Add(new clsTask { id = reader.GetString(0).ToString(), text = reader.GetString(0).ToString() });
                    }

                    reader.Close();
                    connection.Close();
                    ViewBag.lstTask = new SelectList(list, "id", "text");
                }
            }
            return PartialView("SCPApproveLines", objSCP002);
        }
        //[HttpPost]
        //public ActionResult SaveSCPLines(SCP002 scp002, FormCollection fc)
        //{
        //    clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
        //    try
        //    {

        //        SCP002 objSCP002 = new SCP002();
        //        objSCP002.HeaderId = Convert.ToInt32(fc["hfHeaderId"].ToString());
        //        objSCP002.Project = fc["hfProject"].ToString();
        //        objSCP002.Document = fc["hfdoc"].ToString();
        //        objSCP002.TaskCategory = fc["ddlCat"].ToString();
        //        objSCP002.RevNo = Convert.ToInt32(Regex.Replace(fc["hfRev"].ToString(), "[^0-9]+", string.Empty));
        //        objSCP002.ReferenceDrawing = fc["txtRef"].ToString();
        //        objSCP002.ScopeOfWork = fc["txtScope"].ToString();
        //        objSCP002.TaskManager = fc["ddlTask1"].ToString();
        //        objSCP002.TaskDescription = fc["txtTaskDesc"].ToString();
        //        objSCP002.EstFor = fc["txtEst"].ToString();
        //        objSCP002.Weight = string.IsNullOrWhiteSpace(fc["txtWeight"].ToString()) ? 0 : Convert.ToInt32(fc["txtWeight"]); // Convert.ToInt32(fc["txtWeight"].ToString());
        //        objSCP002.RCCHours = string.IsNullOrWhiteSpace(fc["txtRccp"].ToString()) ? 0 : Convert.ToInt32(fc["txtRccp"]);  //Convert.ToInt32(fc["txtRccp"].ToString());
        //        if (string.IsNullOrWhiteSpace(fc["hfExp"].ToString()) || fc["hfExp"].ToString() == "01/01/0001")
        //        {
        //            objSCP002.ExpMatDelDate = null;
        //        }
        //        else
        //        {
        //            objSCP002.ExpMatDelDate = Convert.ToDateTime(fc["hfExp"].ToString());
        //        }

        //        if (string.IsNullOrWhiteSpace(fc["hfReq"].ToString()) || fc["hfReq"].ToString() == "01/01/0001")
        //        {
        //            objSCP002.ReqComDate = null;
        //        }
        //        else
        //        {
        //            objSCP002.ReqComDate = Convert.ToDateTime(fc["hfReq"].ToString());
        //        }
        //        objSCP002.SubConTo = fc["txtSub"].ToString();
        //        objSCP002.DeliverTo = fc["txtDel"].ToString();
        //        objSCP002.IsLNLine = false;
        //        objSCP002.CreatedBy = objClsLoginInfo.UserName;
        //        objSCP002.CreatedOn = DateTime.Now;
        //        db.SCP002.Add(objSCP002);
        //        db.SaveChanges();
        //        objResponseMsg.Key = true;
        //        objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();
        //    }
        //    catch (Exception ex)
        //    {
        //        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        //        objResponseMsg.Key = false;
        //        objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
        //    }
        //    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        //}
        [HttpPost]
        public ActionResult SaveSCPLines(SCP002 scp002, FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (CheckDate(fc["ExpMatDelDate0"].ToString()) || CheckDate(fc["ReqComDate0"].ToString()))
                {
                    int newRowIndex = 0;
                    int headerId = Convert.ToInt32(fc["HeaderId" + newRowIndex]);
                    SCP001 objscpoo1 = db.SCP001.Where(x => x.HeaderId == headerId).FirstOrDefault();

                    SCP002 objSCP002 = new SCP002();
                    objSCP002.HeaderId = Convert.ToInt32(objscpoo1.HeaderId);
                    objSCP002.Project = objscpoo1.Project;
                    objSCP002.Document = objscpoo1.Document;
                    //objSCP002.TaskCategory = fc["ddlCat"].ToString();
                    //objSCP002.RevNo = Convert.ToInt32(Regex.Replace(fc["hfRev"].ToString(), "[^0-9]+", string.Empty));
                    //objSCP002.ReferenceDrawing = fc["txtRef"].ToString();
                    //objSCP002.ScopeOfWork = fc["txtScope"].ToString();
                    //objSCP002.TaskManager = fc["ddlTask1"].ToString();
                    //objSCP002.TaskDescription = fc["txtTaskDesc"].ToString();
                    //objSCP002.EstFor = fc["txtEst"].ToString();
                    //objSCP002.Weight = string.IsNullOrWhiteSpace(fc["txtWeight"].ToString()) ? 0 : Convert.ToInt32(fc["txtWeight"]); // Convert.ToInt32(fc["txtWeight"].ToString());
                    //objSCP002.RCCHours = string.IsNullOrWhiteSpace(fc["txtRccp"].ToString()) ? 0 : Convert.ToInt32(fc["txtRccp"]);  //Convert.ToInt32(fc["txtRccp"].ToString());
                    //if (string.IsNullOrWhiteSpace(fc["hfExp"].ToString()) || fc["hfExp"].ToString() == "01/01/0001")
                    //{
                    //    objSCP002.ExpMatDelDate = null;
                    //}
                    //else
                    //{
                    //    objSCP002.ExpMatDelDate = Convert.ToDateTime(fc["hfExp"].ToString());
                    //}

                    //if (string.IsNullOrWhiteSpace(fc["hfReq"].ToString()) || fc["hfReq"].ToString() == "01/01/0001")
                    //{
                    //    objSCP002.ReqComDate = null;
                    //}
                    //else
                    //{
                    //    objSCP002.ReqComDate = Convert.ToDateTime(fc["hfReq"].ToString());
                    //}
                    //objSCP002.SubConTo = fc["txtSub"].ToString();
                    //objSCP002.DeliverTo = fc["txtDel"].ToString();
                    //objSCP002.IsLNLine = false;
                    //objSCP002.CreatedBy = objClsLoginInfo.UserName;
                    //objSCP002.CreatedOn = DateTime.Now;
                    //db.SCP002.Add(objSCP002);
                    //db.SaveChanges();
                    objSCP002.TaskCategory = fc["TaskCategory0"].ToString();
                    //  objSCP002.RevNo = Convert.ToInt32(Regex.Replace(fc["RevNo0"].ToString(), "[^0-9]+", string.Empty));
                    objSCP002.ReferenceDrawing = fc["ReferenceDrawing0"].ToString() != "" ? fc["ReferenceDrawing0"].ToString().Replace("\r\n", "|").Replace("\n", "|") : "";
                    objSCP002.ScopeOfWork = fc["ScopeOfWork0"].ToString() != "" ? fc["ScopeOfWork0"].ToString().Replace("\r\n", "|").Replace("\n", "|") : "";

                    objSCP002.TaskManager = fc["TaskManager0"].ToString();
                    objSCP002.TaskDescription = fc["TaskDescription0"].ToString();
                    objSCP002.EstFor = fc["EstFor0"].ToString();
                    objSCP002.Weight = string.IsNullOrWhiteSpace(fc["Weight0"].ToString()) ? 0 : Convert.ToInt32(fc["txtWeight"]); // Convert.ToInt32(fc["txtWeight"].ToString());
                    objSCP002.RCCHours = string.IsNullOrWhiteSpace(fc["RCCHours0"].ToString()) ? 0 : Convert.ToInt32(fc["txtRccp"]);  //Convert.ToInt32(fc["txtRccp"].ToString());
                    if (string.IsNullOrWhiteSpace(fc["ExpMatDelDate0"].ToString()) || fc["ExpMatDelDate0"].ToString() == "01/01/0001")
                    {
                        objSCP002.ExpMatDelDate = null;
                    }
                    else
                    {
                        objSCP002.ExpMatDelDate = Convert.ToDateTime(fc["ExpMatDelDate0"].ToString());
                    }

                    if (string.IsNullOrWhiteSpace(fc["ReqComDate0"].ToString()) || fc["ReqComDate0"].ToString() == "01/01/0001")
                    {
                        objSCP002.ReqComDate = null;
                    }
                    else
                    {
                        objSCP002.ReqComDate = Convert.ToDateTime(fc["ReqComDate0"].ToString());
                    }
                    objSCP002.SubConTo = fc["SubConTo0"].ToString();
                    objSCP002.DeliverTo = fc["DeliverTo0"].ToString();
                    objSCP002.IsLNLine = false;
                    objSCP002.CreatedBy = objClsLoginInfo.UserName;
                    objSCP002.CreatedOn = DateTime.Now;
                    db.SCP002.Add(objSCP002);
                    db.SaveChanges();


                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Invalid Date";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteSCPLine(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                SCP002 objSCP002 = db.SCP002.Where(x => x.LineId == Id).FirstOrDefault();
                db.SCP002.Remove(objSCP002);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Delete.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult UpdateSCPLines(SCP002 scp002, FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string a = fc["lineid"].ToString();
                int lineid = Convert.ToInt32(fc["lineid"].ToString());
                if (lineid > 0)
                {
                    SCP002 objSCP002 = db.SCP002.Where(x => x.LineId == lineid).FirstOrDefault();
                    objSCP002.HeaderId = Convert.ToInt32(fc["hfHeaderId"].ToString());
                    objSCP002.Project = fc["hfProject"].ToString();
                    objSCP002.Document = fc["hfdoc"].ToString();
                    objSCP002.TaskCategory = fc["ddlCat"].ToString();
                    objSCP002.RevNo = Convert.ToInt32(Regex.Replace(fc["hfRev"].ToString(), "[^0-9]+", string.Empty));
                    objSCP002.ReferenceDrawing = fc["txtRef"].ToString().Replace("\r\n", ",");
                    objSCP002.ScopeOfWork = fc["txtScope"].ToString().Replace("\r\n", ",");
                    objSCP002.TaskManager = fc["ddlTask1"].ToString();
                    objSCP002.TaskDescription = fc["txtTaskDesc"].ToString();
                    objSCP002.EstFor = fc["txtEst"].ToString();
                    objSCP002.Weight = string.IsNullOrWhiteSpace(fc["txtWeight"].ToString()) ? 0 : Convert.ToInt32(fc["txtWeight"]); // Convert.ToInt32(fc["txtWeight"].ToString());
                    objSCP002.RCCHours = string.IsNullOrWhiteSpace(fc["txtRccp"].ToString()) ? 0 : Convert.ToInt32(fc["txtRccp"]);  //Convert.ToInt32(fc["txtRccp"].ToString());
                    if (string.IsNullOrWhiteSpace(fc["hfExp"].ToString()) || fc["hfExp"].ToString() == "01/01/0001")
                    {
                        objSCP002.ExpMatDelDate = null;
                    }
                    else
                    {
                        objSCP002.ExpMatDelDate = Convert.ToDateTime(fc["hfExp"].ToString());
                    }

                    if (string.IsNullOrWhiteSpace(fc["hfReq"].ToString()) || fc["hfReq"].ToString() == "01/01/0001")
                    {
                        objSCP002.ReqComDate = null;
                    }
                    else
                    {
                        objSCP002.ReqComDate = Convert.ToDateTime(fc["hfReq"].ToString());
                    }
                    objSCP002.SubConTo = fc["txtSub"].ToString();
                    objSCP002.DeliverTo = fc["txtDel"].ToString();
                    objSCP002.IsLNLine = Convert.ToBoolean(fc["hfisln"].ToString());
                    objSCP002.EditedBy = objClsLoginInfo.UserName;
                    objSCP002.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult getCodeValue(string project, string approver)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                project = project.Split('-')[0].Trim();
                objResponseMsg.projdesc = db.COM001.Where(i => i.t_cprj == project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objResponseMsg.appdesc = db.COM003.Where(i => i.t_psno == approver && i.t_actv == 1).Select(i => i.t_psno + " - " + i.t_name).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult verifyApprover(string approver)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                approver = approver.Split('-')[0].Trim();
                var Exists = db.COM003.Any(x => x.t_psno == approver && x.t_actv == 1);
                if (Exists == false)
                {
                    objResponseMsg.Key = false;
                }
                else
                {
                    objResponseMsg.Key = true;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public string GetCodeAndNameByProject(string project)
        {
            var projdesc = db.COM001.Where(i => i.t_cprj == project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
            return projdesc;
        }

        [HttpPost]
        public ActionResult UpdateData(int LineId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string Table_name = "SCP002";
                if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
                {
                    if (columnName == "ReferenceDrawing" || columnName == "ScopeOfWork")
                    {
                        columnValue = !string.IsNullOrWhiteSpace(columnValue) ? columnValue.Replace("\r\n", "|").Replace("\n", "|").Trim() : "";
                    }

                    db.SP_COMMON_LINES_UPDATE(LineId, columnName, columnValue, Table_name, objClsLoginInfo.UserName);
                    //db.SP_COMMON_LINES_UPDATE(Table_name,LineId, "EditedBy", objClsLoginInfo.UserName);
                    //ReviseData(headerId);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.LineUpdate.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult isLineAvailable(string project)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                project = project.Split('-')[0].Trim();
                var Exists = db.SCP002.Any(x => x.Project == project);
                if (Exists == false)
                {
                    objResponseMsg.Key = false;
                }
                else
                {
                    objResponseMsg.Key = true;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetHistoryView(int headerid = 0)
        {
            SCP001_Log objSCP001 = new SCP001_Log();
            Session["Headerid"] = headerid;
            return PartialView("getHistoryPartial", objSCP001);
        }

        [HttpPost]
        public JsonResult LoadSCPHeaderHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var user = objClsLoginInfo.UserName;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                string strWhere = string.Empty;

                strWhere += " 1 = 1 and HeaderId=" + Convert.ToInt32(Session["Headerid"]);
                strWhere += " and ApprovedBy=" + user;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " Project like '%" + param.sSearch
                       + "%' or Document like '%" + param.sSearch
                       + "%' or Customer like '%" + param.sSearch
                       + "%' or RevNo like '%" + param.sSearch
                       + "%' or Product like '%" + param.sSearch
                       + "%' or ProcessLicensor like '%" + param.sSearch
                       + "%' or Status like '%" + param.sSearch + "%'";
                }
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_SCP_GET_HISTORY_HEADERDETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {  Convert.ToString(uc.HeaderId),
                               Convert.ToString(GetCodeAndNameByProject(uc.Project)),
                               Convert.ToString(uc.Document),
                               Convert.ToString(Manager.GetCustomerCodeAndNameByProject(uc.Project)),
                               Convert.ToString(uc.Product),
                               Convert.ToString(uc.ProcessLicensor),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.Status),
                               Convert.ToString(uc.SubmittedBy),
                               Convert.ToString(Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy")),
                               Convert.ToString(uc.ApprovedBy),
                               Convert.ToString(Convert.ToDateTime(uc.ApprovedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.ApprovedOn).ToString("dd/MM/yyyy")),

                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(db.SCP001_Log.FirstOrDefault(q => (q.HeaderId == uc.HeaderId && q.RevNo == uc.RevNo)).Id)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult LoadSCPLineHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                int headerid = Convert.ToInt32(param.Headerid);
                var user = objClsLoginInfo.UserName;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                string strWhere = string.Empty;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere = " (ReferenceDrawing like '%" + param.sSearch
                       + "%' or ScopeOfWork like '%" + param.sSearch
                       + "%' or TaskManager like '%" + param.sSearch

                       + "%' or EstFor like '%" + param.sSearch
                       + "%' or Weight like '%" + param.sSearch
                       + "%' or RCCHours like '%" + param.sSearch
                       + "%' or ExpMatDelDate like '%" + param.sSearch
                       + "%' or ReqComDate like '%" + param.sSearch
                       + "%' or SubConTo like '%" + param.sSearch
                       + "%' or DeliverTo like '%" + param.sSearch
                       + "%' or TaskDescription like '%" + param.sSearch
                       + "%')";
                }
                else
                {
                    strWhere = "1=1";
                }
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_SCP_GET_HISTORY_LINEDETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, headerid, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                            {
                           Convert.ToString(uc.ROW_NO),
                           Convert.ToString(uc.TaskDescription),
                           Convert.ToString(uc.ScopeOfWork),
                           Convert.ToString(uc.ReferenceDrawing),
                           Convert.ToString(uc.TaskManager),
                           Convert.ToString(uc.EstFor),
                           Convert.ToString(uc.Weight),
                           Convert.ToString(uc.RCCHours),
                          Convert.ToString(Convert.ToDateTime(uc.ExpMatDelDate).ToShortDateString()),
                           Convert.ToString(Convert.ToDateTime(uc.ReqComDate).ToShortDateString()),
                           Convert.ToString(uc.SubConTo),
                           Convert.ToString(uc.DeliverTo),

                           Convert.ToString(uc.HeaderId),
                           Convert.ToString(uc.LineId),
                           Convert.ToString(uc.IsLNLine)
                           }).ToList();
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }
        [SessionExpireFilter]
        public ActionResult SCPHistoryDetails(int Id = 0)
        {
            SCP001_Log objSCP001 = new SCP001_Log();
            objSCP001 = db.SCP001_Log.Where(x => x.Id == Id).FirstOrDefault();
            var user = objClsLoginInfo.UserName;
            if (objSCP001 != null)
            {
                var lstProjectDesc = (from a in db.COM001
                                      select new Projects { projectCode = a.t_cprj, projectDescription = a.t_cprj + " - " + a.t_dsca }).ToList();
                ViewBag.Project = new SelectList(lstProjectDesc, "projectCode", "projectDescription");

                List<string> lstProCat = clsImplementationEnum.getProductCategory().ToList();
                ViewBag.lstProCat = lstProCat.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

                var approver = (from t1 in db.ATH001
                                join t2 in db.COM003 on t1.Employee equals t2.t_psno
                                where t2.t_actv == 1
                                select new { t1.Employee, Desc = t2.t_psno + " - " + t2.t_name }).Distinct().ToList();
                ViewBag.approver = new SelectList(approver, "Employee", "Desc");
                if (Id > 0)
                {
                    var project = (from a in db.SCP001_Log
                                   where a.Id == Id
                                   select a.Project).FirstOrDefault();
                    ViewBag.Customer = Manager.GetCustomerCodeAndNameByProject(project);

                }
                var urlPrefix = Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.LastIndexOf('=') + 1);
                if (!objClsLoginInfo.GetUserRoleList().Contains("SHOP"))
                {
                    ViewBag.RevPrev = (db.SCP001_Log.Any(q => (q.HeaderId == objSCP001.HeaderId && q.RevNo == (objSCP001.RevNo - 1))) ? urlPrefix + db.SCP001_Log.Where(q => (q.HeaderId == objSCP001.HeaderId && q.RevNo == (objSCP001.RevNo - 1))).FirstOrDefault().Id : null);
                    ViewBag.RevNext = (db.SCP001_Log.Any(q => (q.HeaderId == objSCP001.HeaderId && q.RevNo == (objSCP001.RevNo + 1))) ? urlPrefix + db.SCP001_Log.Where(q => (q.HeaderId == objSCP001.HeaderId && q.RevNo == (objSCP001.RevNo + 1))).FirstOrDefault().Id : null);
                }
            }
            return View(objSCP001);
        }

        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_SCP_GET_HEADERDETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = GetCodeAndNameByProject(li.Project),
                                      Document = li.Document,
                                      Customer = Convert.ToString(Manager.GetCustomerCodeAndNameByProject(li.Project)),
                                      Product = Convert.ToString(li.Product),
                                      ProcessLicensor = Convert.ToString(li.ProcessLicensor),
                                      RevNo = Convert.ToString("R" + li.RevNo),
                                      Status = Convert.ToString(li.Status),
                                      SubmittedBy = Convert.ToString(li.SubmittedBy),
                                      SubmittedOn = Convert.ToString(Convert.ToDateTime(li.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA" : Convert.ToDateTime(li.SubmittedOn).ToString("dd/MM/yyyy")),
                                      ApprovedBy = Convert.ToString(li.ApprovedBy),
                                      ApprovedOn = Convert.ToString(Convert.ToDateTime(li.ApprovedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA" : Convert.ToDateTime(li.ApprovedOn).ToString("dd/MM/yyyy")),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                else
                {
                    int headerid = Convert.ToInt32(Session["Headerid"]);
                    var lst = db.SP_SCP_GET_LINEDETAILS(1, int.MaxValue, strSortOrder, headerid, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      ReferenceDrawing = li.ReferenceDrawing,
                                      TaskDescription = li.TaskDescription,
                                      ScopeOfWork = li.ScopeOfWork,
                                      TaskCategory = li.TaskCategory,
                                      TaskManager = li.TaskManager,
                                      EstFor = Convert.ToString(li.EstFor),
                                      Weight = Convert.ToString(li.Weight),
                                      SubmittedOn = Convert.ToString(Convert.ToDateTime(li.ExpMatDelDate).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA" : Convert.ToDateTime(li.ExpMatDelDate).ToString("dd/MM/yyyy")),
                                      ReqComDate = Convert.ToString(Convert.ToDateTime(li.ReqComDate).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA" : Convert.ToDateTime(li.ReqComDate).ToString("dd/MM/yyyy")),
                                      SubConTo = Convert.ToString(li.SubConTo),
                                      RCCHours = Convert.ToString(li.RCCHours),
                                      DeliverTo = Convert.ToString(li.DeliverTo),



                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GenerateExcelHistroy(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_SCP_GET_HISTORY_HEADERDETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = GetCodeAndNameByProject(li.Project),
                                      Document = li.Document,
                                      Customer = Convert.ToString(Manager.GetCustomerCodeAndNameByProject(li.Project)),
                                      Product = Convert.ToString(li.Product),
                                      ProcessLicensor = Convert.ToString(li.ProcessLicensor),
                                      RevNo = Convert.ToString("R" + li.RevNo),
                                      Status = Convert.ToString(li.Status),
                                      SubmittedBy = Convert.ToString(li.SubmittedBy),
                                      SubmittedOn = Convert.ToString(Convert.ToDateTime(li.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA" : Convert.ToDateTime(li.SubmittedOn).ToString("dd/MM/yyyy")),
                                      ApprovedBy = Convert.ToString(li.ApprovedBy),
                                      ApprovedOn = Convert.ToString(Convert.ToDateTime(li.ApprovedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA" : Convert.ToDateTime(li.ApprovedOn).ToString("dd/MM/yyyy")),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                else
                {
                    int headerid = Convert.ToInt32(Session["Headerid"]);
                    var lst = db.SP_SCP_GET_HISTORY_LINEDETAILS(1, int.MaxValue, strSortOrder, headerid, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      ReferenceDrawing = li.ReferenceDrawing,
                                      TaskDescription = li.TaskDescription,
                                      ScopeOfWork = li.ScopeOfWork,
                                      TaskManager = li.TaskManager,
                                      EstFor = Convert.ToString(li.EstFor),
                                      Weight = Convert.ToString(li.Weight),
                                      SubmittedOn = Convert.ToString(Convert.ToDateTime(li.ExpMatDelDate).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA" : Convert.ToDateTime(li.ExpMatDelDate).ToString("dd/MM/yyyy")),
                                      ReqComDate = Convert.ToString(Convert.ToDateTime(li.ReqComDate).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA" : Convert.ToDateTime(li.ReqComDate).ToString("dd/MM/yyyy")),
                                      SubConTo = Convert.ToString(li.SubConTo),
                                      RCCHours = Convert.ToString(li.RCCHours),
                                      DeliverTo = Convert.ToString(li.DeliverTo),



                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
    }

}