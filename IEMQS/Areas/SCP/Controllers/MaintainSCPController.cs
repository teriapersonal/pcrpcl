﻿using IEMQS.Areas.MSW.Controllers;
using IEMQS.Areas.SCP.Models;
using IEMQS.Models;
using IEMQSImplementation;
using IEMQS.Areas.Utility.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace IEMQS.Areas.SCP.Controllers
{
    public class MaintainSCPController : clsBase
    {
        // GET: SCP/MaintainSCP
        SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["LNConcerto"].ToString());

        #region Utility
        public string HTMLActionString(int rowId, string status, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";

            return htmlControl;
        }

        public static string GenerateTextboxFor(int rowId, string status, string columnName, string columnValue = "", string onBlurMethod = "", string onClickMethod = "", bool isReadOnly = false, string inputStyle = "")
        {
            string htmlControl = "";

            string inputID = columnName + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "form-control";

            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";
            if (string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue()))
            {
                htmlControl = "<input disabled type='text' id='" + inputID + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + "  />";
            }
            else
            {
                htmlControl = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + onClickEvent + "  />";
            }

            return htmlControl;
        }


        public static string GenerateDropdown(int rowId, string status, string columnName, SelectList itemList, string defaultSelectionText = "", string onChangeMethod = "", string OnClickMethod = "", bool isReadOnly = false, string inputStyle = "")
        {
            string selectOptions = "";
            string inputID = columnName + "" + rowId.ToString();
            string inputName = columnName;
            string className = "form-control col-md-3";
            string onChangedEvent = !string.IsNullOrEmpty(onChangeMethod) ? "onchange='" + onChangeMethod + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(OnClickMethod) ? "onclick='" + OnClickMethod + "'" : "";
            if (defaultSelectionText.Length > 0)
            {
                selectOptions += "<option value=''>" + defaultSelectionText + "</option>";
            }

            foreach (var item in itemList)
            {
                if (item.Selected)
                {
                    selectOptions += "<option selected value=" + item.Value + ">" + item.Text + "</option>";
                }
                else
                {
                    selectOptions += "<option value=" + item.Value + ">" + item.Text + "</option>";
                }
            }
            if (string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue()))
            {
                return "<select id='" + inputID + "' disabled name='" + inputID + "' " + onChangedEvent + " colname='" + columnName + "' class='" + className + "' style='" + inputStyle + "' >" + selectOptions + "</select>";
            }
            else
            {
                return "<select id='" + inputID + "' name='" + inputID + "' " + onChangedEvent + " colname='" + columnName + "' class='" + className + "' style='" + inputStyle + "'  " + onClickEvent + " >" + selectOptions + "</select>";
            }
        }
        public string GenerateAutoComplete(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false)
        {
            string strAutoComplete = string.Empty;

            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "autocomplete form-control";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";

            strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' hdElement='" + hdElementId + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + (disabled ? "disabled" : "") + " />";

            return strAutoComplete;
        }
        public string HTMLAutoComplete(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false, string maxlength = "", string validate = "")
        {
            string strAutoComplete = string.Empty;

            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "autocomplete form-control";
            string MaxCharcters = !string.IsNullOrEmpty(maxlength) ? "maxlength='" + maxlength + "'" : "";
            string FieldValidate = !string.IsNullOrEmpty(validate) ? "validate='" + validate + "'" : "";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";

            strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' hdElement='" + hdElementId + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + (disabled ? "disabled" : "") + " " + MaxCharcters + " " + FieldValidate + " />";

            return strAutoComplete;
        }


        #endregion
        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }
        [SessionExpireFilter]
        public ActionResult MaintainSCPDetails(int Id = 0)
        {
            SCP001 objSCP001 = new SCP001();
            var user = objClsLoginInfo.UserName;

            var lstProjectDesc = (from a in db.COM001
                                  select new Projects { projectCode = a.t_cprj, projectDescription = a.t_cprj + " - " + a.t_dsca }).ToList();
            ViewBag.Project = new SelectList(lstProjectDesc, "projectCode", "projectDescription");

            List<string> lstProCat = clsImplementationEnum.getProductCategory().ToList();
            ViewBag.lstProCat = lstProCat.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();


            var approver = (from t1 in db.ATH001
                            join t2 in db.COM003 on t1.Employee equals t2.t_psno
                            where t2.t_actv == 1
                            select new { t1.Employee, Desc = t2.t_psno + " - " + t2.t_name }).Distinct().ToList();
            ViewBag.approver = new SelectList(approver, "Employee", "Desc");


            if (Id > 0)
            {
                var project = (from a in db.SCP001
                               where a.HeaderId == Id
                               select a.Project).FirstOrDefault();
                ViewBag.Customer = Manager.GetCustomerCodeAndNameByProject(project);

                objSCP001 = db.SCP001.Where(x => x.HeaderId == Id).FirstOrDefault();
                List<string> lstTestCategory = (from t1 in db.SCP003
                                                select t1.TaskCategory).Distinct().ToList();

                ViewBag.lstTestCategory = lstTestCategory.Select(i => new CategoryData { Value = i.ToString(), Code = i.ToString(), CategoryDescription = i.ToString() }).ToList();

                List<clsTask> list = new List<clsTask>();
                connection.Open();
                string query = "select  DISTINCT  CONTACT as 'Task Manager' " +
                                " from [dbo].[PROJ_TASK] " +
                                " where PROJECTNAME = @proj and CONTACT is not null " +
                                " ORDER BY CONTACT";
                SqlCommand cmd = new SqlCommand(query, connection);
                cmd.Parameters.AddWithValue("@proj", project);
                SqlDataReader reader = cmd.ExecuteReader();
                List<KeyValuePair<string, string>> lst = new List<KeyValuePair<string, string>>();
                while (reader.Read())
                {
                    list.Add(new clsTask { id = reader.GetString(0).ToString(), text = reader.GetString(0).ToString() });
                }

                reader.Close();
                connection.Close();
                ViewBag.lstTask = list.Select(i => new CategoryData { Value = i.text.ToString(), Code = i.text.ToString(), CategoryDescription = i.text.ToString() }).ToList();


                var PlanningDinID = db.PDN002.Where(x => x.RefId == Id && x.DocumentNo == objSCP001.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                ViewBag.PlanningDinID = PlanningDinID;

                ViewBag.IsReviseBtnEnabled = Manager.IsReviseEnabled(PlanningDinID, objSCP001.HeaderId, objSCP001.Document);
                ViewBag.DocMessage = clsImplementationMessage.CommonMessages.DocMessage.ToString();

            }
            return View(objSCP001);
        }
        public JsonResult getTaskManager(string project)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                project = project.Split('-')[0].Trim();
                List<clsTask> list = new List<clsTask>();
                connection.Open();
                string query = "select  DISTINCT  CONTACT as 'Task Manager' " +
                                " from [dbo].[PROJ_TASK] " +
                                " where PROJECTNAME = @proj and CONTACT is not null " +
                                " ORDER BY CONTACT";
                SqlCommand cmd = new SqlCommand(query, connection);
                cmd.Parameters.AddWithValue("@proj", project);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    list.Add(new clsTask { id = reader.GetString(0).ToString(), text = reader.GetString(0).ToString() });
                }
                reader.Close();
                connection.Close();
                objResponseMsg.lsttask = list;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetHeaderData(string project)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                project = project.Split('-')[0].Trim();
                int RevNum;
                var ExistsPrject = db.COM001.Any(x => x.t_cprj == project);
                if (ExistsPrject == false)
                {
                    objResponseMsg.Key = false;
                }
                else
                {
                    string customer = Manager.GetCustomerCodeAndNameByProject(project);
                    var Exists = db.SCP001.Any(x => x.RevNo == null && x.Project == project);
                    if (Exists == false)
                    {
                        RevNum = 0;
                    }
                    else
                    {
                        int? revNo = (from rev in db.SCP001
                                      where rev.Project == project
                                      select rev.RevNo).FirstOrDefault();
                        RevNum = Convert.ToInt32(revNo) + 1;
                    }

                    project = project.Split('-')[0].ToString().Trim();
                    var ccdd = Manager.GetContractWiseCdd(project);

                    List<clsTask> list = new List<clsTask>();
                    connection.Open();
                    string query = "select  DISTINCT  B.CONTACT as 'Task Manager' " +
                                    " from[dbo].[VR_PROJECT_TASK_CHECKLIST]  a " +
                                    " LEFT  join[dbo].[PROJ_TASK] b on a.PROJECTID = b.PROJECTID and a.TASKUNIQUEID = b.TASKUNIQUEID " +
                                    " where a.CLITEMTEXT IS NOT NULL  AND B.CONTACT IS NOT NULL" +
                                    " ORDER BY B.CONTACT";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    //cmd.Parameters.AddWithValue("@proj", project);
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        list.Add(new clsTask { id = reader.GetString(0).ToString(), text = reader.GetString(0).ToString() });
                    }
                    reader.Close();
                    connection.Close();

                    // ViewBag.lstTask = list.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

                    objResponseMsg.Value = customer + "|" + RevNum + "|" + ccdd;
                    objResponseMsg.lsttask = list;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult getHeaderId(string project)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                project = project.Split('-')[0].Trim();
                int Headerid = (from a in db.SCP001
                                where a.Project == project
                                select a.HeaderId).FirstOrDefault();
                string status = (from a in db.SCP001
                                 where a.Project == project
                                 select a.Status).FirstOrDefault();
                objResponseMsg.Key = true;
                objResponseMsg.Value = Headerid.ToString() + "|" + status;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveSCPHeader(SCP001 scp001, FormCollection fc)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                int headerid = !string.IsNullOrWhiteSpace(fc["hfHeaderId"].ToString()) ? Convert.ToInt32(fc["hfHeaderId"]) : 0;

                string project = fc["hfProject"].Split('-')[0].ToString().Trim();
                if (headerid > 0)
                {
                    int hid = Convert.ToInt32(fc["hfHeaderId"].ToString());

                    SCP001 objSCP001 = db.SCP001.Where(x => x.HeaderId == hid).FirstOrDefault();
                    string rev = Regex.Replace(fc["txtRev"].ToString(), "[^0-9]+", string.Empty);
                    objSCP001.Project = project;
                    objSCP001.Document = fc["txtdoc"].ToString();
                    objSCP001.Customer = fc["txtCust"].ToString().Split('-')[0];
                    objSCP001.ReviseRemark = fc["ReviseRemark"].ToString();
                    if (objSCP001.Status == clsImplementationEnum.PTMTCTQStatus.Approved.GetStringValue())
                    {
                        objSCP001.RevNo = Convert.ToInt32(objSCP001.RevNo) + 1;
                        objSCP001.Status = clsImplementationEnum.PTMTCTQStatus.DRAFT.GetStringValue();
                        objSCP001.ReturnRemark = null;
                        objSCP001.ApprovedOn = null;
                    }
                    if (string.IsNullOrWhiteSpace(fc["txtCdd"].ToString()) || fc["txtCdd"].ToString() == "01-01-0001" || fc["txtCdd"].ToString() == "01/01/0001")
                    {
                        objSCP001.CDD = null;
                    }
                    else
                    {
                        objSCP001.CDD = Convert.ToDateTime(fc["txtCdd"].ToString());
                    }
                    //objSCP001.Product = fc["ddlCat"].ToString();
                    objSCP001.ProcessLicensor = fc["txtlicence"].ToString();
                    if (fc["ddlTask"] != null)
                    {
                        objSCP001.TaskManager = fc["ddlTask"].ToString();
                    }
                    else { objSCP001.TaskManager = ""; }
                    objSCP001.EditedBy = objClsLoginInfo.UserName;
                    objSCP001.EditedOn = DateTime.Now;
                    objSCP001.ApprovedBy = fc["ddlApprover"].ToString().Split('-')[0].Trim();
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                    objResponseMsg.Revision = "R" + objSCP001.RevNo.ToString();
                    objResponseMsg.status = objSCP001.Status;
                    Manager.UpdatePDN002(objSCP001.HeaderId, objSCP001.Status, objSCP001.RevNo, objSCP001.Project, objSCP001.Document);
                }
                else
                {

                    var isvalid = db.SCP001.Any(x => x.Project == project);
                    if (isvalid == false)
                    {
                        string rev = Regex.Replace(fc["txtRev"].ToString(), "[^0-9]+", string.Empty);
                        SCP001 objSCP001 = new SCP001();
                        objSCP001.Project = project;
                        objSCP001.Document = fc["txtdoc"].ToString();
                        objSCP001.Customer = fc["txtCust"].ToString().Split('-')[0];
                        objSCP001.RevNo = Convert.ToInt32(rev);
                        objSCP001.ApprovedBy = fc["ddlApprover"].ToString().Split('-')[0].Trim(); ;
                        objSCP001.Status = clsImplementationEnum.CTQStatus.DRAFT.GetStringValue();
                        if (string.IsNullOrWhiteSpace(fc["txtCdd"].ToString()) || fc["txtCdd"].ToString() == "01-01-0001" || fc["txtCdd"].ToString() == "01/01/0001")
                        {
                            objSCP001.CDD = null;
                        }
                        else
                        {
                            objSCP001.CDD = Convert.ToDateTime(fc["txtCdd"].ToString());
                        }

                        objSCP001.Product = fc["ddlCat"].ToString();
                        objSCP001.ProcessLicensor = fc["txtlicence"].ToString();
                        if (fc["ddlTask"] != null)
                        {
                            objSCP001.TaskManager = fc["ddlTask"].ToString();
                        }
                        objSCP001.ReviseRemark = fc["ReviseRemark"].ToString();
                        objSCP001.CreatedBy = objClsLoginInfo.UserName;
                        objSCP001.CreatedOn = DateTime.Now;
                        db.SCP001.Add(objSCP001);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Revision = "R" + objSCP001.RevNo.ToString();
                        objResponseMsg.status = objSCP001.Status;
                        Manager.UpdatePDN002(objSCP001.HeaderId, objSCP001.Status, objSCP001.RevNo, objSCP001.Project, objSCP001.Document);
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.Message.ToString();
                    }

                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public ActionResult getData(string project, string document, string RevNo, string TaskManager, string result)
        {

            string[] values = TaskManager.Split(',').Select(sValue => sValue.Trim()).ToArray();
            string TaskManager1 = String.Join(", ", values);
            TaskManager1 = TaskManager1.Replace('[', ' ');
            TaskManager1 = TaskManager1.Replace(']', ' ');
            TaskManager1 = TaskManager1.Replace('"', '\'');
            project = project.Split('-')[0].ToString().Trim();
            int rev = Convert.ToInt32(Regex.Replace(RevNo.ToString(), "[^0-9]+", string.Empty));
            var isvalid = db.SCP002.Any(x => x.Project == project && x.IsLNLine == true && x.Document == document && x.RevNo == rev);

            int Headerid = (from a in db.SCP001
                            where a.Project == project
                            select a.HeaderId).FirstOrDefault();


            List<SCP_LNData> lstSCP_LNData = getDataLN(project, document, RevNo, TaskManager1);
            if (result.ToLower() == "true")
            {
                var deletelndata = db.SCP002.Where(m => m.HeaderId == Headerid && m.IsLNLine == true).ToList();
                db.SCP002.RemoveRange(deletelndata);
                db.SaveChanges();


            }
            foreach (var listln in lstSCP_LNData.ToList())
            {
                // var qry = db.SCP002.Where(m => m.ReferenceDrawing == listln.ReferenceDrawing && m.ScopeOfWork == listln.ScopeOfWork && m.TaskManager == listln.TaskManager && m.TaskDescription==listln.TaskDecription).FirstOrDefault();
                var qry = db.SCP002.Where(m => m.TASKUNIQUEID == listln.TASKUNIQUEID && m.Project == listln.Project).FirstOrDefault();
                if (qry == null)
                {
                    SCP002 scp = new SCP002();
                    scp.HeaderId = listln.HeaderId;
                    scp.Project = listln.Project;
                    scp.Document = listln.Document;
                    scp.RevNo = Convert.ToInt32(Regex.Replace(listln.RevNo.ToString(), "[^0-9]+", string.Empty));
                    scp.ReferenceDrawing = listln.ReferenceDrawing;
                    scp.TaskDescription = listln.TaskDecription;
                    scp.ScopeOfWork = listln.ScopeOfWork;

                    scp.EstFor = listln.EstFor;
                    scp.TASKUNIQUEID = Convert.ToInt32(listln.TASKUNIQUEID);
                    scp.Weight = (!string.IsNullOrWhiteSpace(listln.Weight) ? Convert.ToInt32(listln.Weight) : 0);
                    scp.RCCHours = (!string.IsNullOrWhiteSpace(listln.RCCHours) ? Convert.ToInt32(listln.RCCHours) : 0);
                    scp.ExpMatDelDate = Convert.ToDateTime(listln.ExpMatDelDate);
                    scp.ReqComDate = Convert.ToDateTime(listln.ReqComDate);
                    scp.SubConTo = listln.SubConTo;
                    scp.DeliverTo = listln.DeliverTo;
                    scp.TaskManager = listln.TaskManager;
                    scp.IsLNLine = listln.IsLNLine;
                    scp.CreatedBy = listln.CreatedBy;
                    scp.CreatedOn = Convert.ToDateTime(listln.CreatedOn);
                    db.SCP002.Add(scp);
                    db.SaveChanges();
                }
                else
                {
                    if (result.ToLower().ToString() == "true")
                    {
                        SCP002 sc = new SCP002();
                        sc = db.SCP002.Where(m => m.TASKUNIQUEID == listln.TASKUNIQUEID && m.Project == listln.Project).FirstOrDefault();
                        sc.Project = listln.Project;
                        sc.Document = listln.Document;
                        sc.RevNo = Convert.ToInt32(Regex.Replace(listln.RevNo.ToString(), "[^0-9]+", string.Empty));
                        sc.EstFor = listln.EstFor;
                        sc.ReferenceDrawing = listln.ReferenceDrawing;
                        sc.Weight = (!string.IsNullOrWhiteSpace(listln.Weight) ? Convert.ToInt32(listln.Weight) : 0);
                        sc.RCCHours = (!string.IsNullOrWhiteSpace(listln.RCCHours) ? Convert.ToInt32(listln.RCCHours) : 0);
                        sc.ExpMatDelDate = Convert.ToDateTime(listln.ExpMatDelDate);
                        sc.ReqComDate = Convert.ToDateTime(listln.ReqComDate);
                        sc.TASKUNIQUEID = Convert.ToInt32(listln.TASKUNIQUEID);
                        sc.SubConTo = listln.SubConTo;
                        sc.DeliverTo = listln.DeliverTo;
                        sc.TaskDescription = listln.TaskDecription;
                        sc.IsLNLine = listln.IsLNLine;
                        sc.EditedBy = objClsLoginInfo.UserName;
                        sc.EditedOn = DateTime.Now;
                        db.SaveChanges();
                    }
                }

            }
            return Json(lstSCP_LNData, JsonRequestBehavior.AllowGet);



        }
        public List<SCP_LNData> getDataLN(string project, string document, string RevNo, string TaskManager)
        {
            List<SCP_LNData> data = new List<SCP_LNData>();
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                project = project.Split('-')[0].ToString().Trim();
                connection.Open();
                int Headerid = (from a in db.SCP001
                                where a.Project == project
                                select a.HeaderId).FirstOrDefault();

                string query =

                "SELECT " +
                       "c.TASKUNIQUEID as 'TASKUNIQUEID' " +
                       ",Isnull( " +
                       "Stuff((SELECT '|' + cc.CLITEMTEXT " +
                       "FROM dbo.VR_PROJECT_TASK_CHECKLIST  cc " +
                       "Join " +
                       "(SELECT DISTINCT TASKUNIQUEID, CONTACT, EXPECTED_START_DATE, EXPECTED_END_DATE FROM " +
                       " dbo.PROJ_TASK WHERE  PROJECTNAME = @proj AND CONTACT in (" + TaskManager + ") " +
                       ") pp on pp.TASKUNIQUEID = cc.TASKUNIQUEID " +
                       "WHERE cc.TASKUNIQUEID = c.TASKUNIQUEID " +
                       "and cc.PROJECTNAME = @proj " +
                       "FOR XML PATH('')), 1, 1, ''),'') as 'Scope of Work' " +
                       ",Isnull( " +
                       "Stuff((SELECT Distinct '|' + cc.TEXT2 " +
                       "FROM dbo.VR_PROJECT_TASK_CHECKLIST  cc " +
                       "Join " +
                       "(SELECT DISTINCT TASKUNIQUEID, CONTACT, EXPECTED_START_DATE, EXPECTED_END_DATE FROM " +
                       " dbo.PROJ_TASK WHERE  PROJECTNAME = @proj AND CONTACT in (" + TaskManager + ") " +
                       ") pp on pp.TASKUNIQUEID = cc.TASKUNIQUEID " +
                       "WHERE cc.TASKUNIQUEID = c.TASKUNIQUEID " +
                       "and cc.PROJECTNAME = @proj " +
                       "FOR XML PATH('')), 1, 1, ''),'') as  'Ref Drawing' " +
                       ",p.CONTACT as 'Task Manager' " +
                       ",c.NAME as 'Task Description' " +
                       ",p.EXPECTED_START_DATE as 'Exp Mat Del Date' " +
                       ",p.EXPECTED_END_DATE as 'Req Completion Date' " +
                "FROM dbo.VR_PROJECT_TASK_CHECKLIST  c " +
                "Join " +
                "(SELECT DISTINCT TASKUNIQUEID, CONTACT, EXPECTED_START_DATE, EXPECTED_END_DATE FROM " +
                " dbo.PROJ_TASK WHERE  PROJECTNAME = @proj AND CONTACT in (" + TaskManager + ") " +
                ") p on p.TASKUNIQUEID = c.TASKUNIQUEID " +
                "where c.PROJECTNAME = @proj " +
                " AND p.CONTACT in (" + TaskManager + ") " +
                "GROUP BY c.TASKUNIQUEID ,p.CONTACT ,c.NAME ,p.EXPECTED_START_DATE ,p.EXPECTED_END_DATE ";

                SqlCommand cmd = new SqlCommand(query, connection);
                cmd.Parameters.AddWithValue("@proj", project);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    SCP_LNData f = new SCP_LNData();

                    f.HeaderId = Headerid;
                    f.Project = project;
                    f.Document = document;
                    f.RevNo = RevNo;
                    f.ReferenceDrawing = Convert.ToString(reader["Ref Drawing"]);
                    f.ScopeOfWork = Convert.ToString(reader["Scope of Work"]);
                    f.TaskDecription = Convert.ToString(reader["Task Description"]);
                    f.TASKUNIQUEID = Convert.ToInt32(reader["TASKUNIQUEID"]);
                    f.EstFor = "";
                    f.Weight = "";
                    f.RCCHours = "";
                    f.ExpMatDelDate = Convert.ToDateTime(reader["Exp Mat Del Date"]);
                    f.ReqComDate = Convert.ToDateTime(reader["Req Completion Date"]);
                    f.SubConTo = "";
                    f.DeliverTo = "";
                    f.TaskManager = Convert.ToString(reader["Task Manager"]);
                    f.IsLNLine = true;
                    f.CreatedBy = objClsLoginInfo.UserName;
                    f.CreatedOn = DateTime.Now;

                    data.Add(f);
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            finally
            {
                connection.Close();
            }
            return data;
        }
        public List<SCP_LNData> getDatascp002(string project, string document, string RevNo, string TaskManager, int Headerid)
        {
            List<SCP_LNData> data1 = new List<SCP_LNData>();
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                project = project.Split('-')[0].Trim();
                //int Headerid = (from a in db.SCP001
                //                where a.Project == project
                //                select a.HeaderId).FirstOrDefault();


                var location = objClsLoginInfo.Location;



                var qry = db.SCP002.Where(m => m.HeaderId == Headerid).ToList();

                foreach (var itm in qry)
                {
                    SCP_LNData f = new SCP_LNData();
                    f.HeaderId = Headerid;
                    f.Project = project;
                    f.Document = document;
                    f.RevNo = RevNo;
                    f.ReferenceDrawing = itm.ReferenceDrawing;
                    f.ScopeOfWork = itm.ScopeOfWork;
                    f.TaskDecription = itm.TaskDescription;
                    f.TASKUNIQUEID = Convert.ToInt32(itm.TASKUNIQUEID);
                    f.EstFor = "";
                    f.Weight = "";
                    f.RCCHours = "";
                    f.ExpMatDelDate = Convert.ToDateTime(itm.ExpMatDelDate);
                    f.ReqComDate = Convert.ToDateTime(itm.ReqComDate);
                    f.SubConTo = "";
                    f.DeliverTo = "";
                    f.TaskManager = itm.TaskManager;
                    f.IsLNLine = true;
                    f.CreatedBy = objClsLoginInfo.UserName;
                    f.CreatedOn = DateTime.Now;
                    data1.Add(f);

                }


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            finally
            {
                connection.Close();
            }
            return data1;
        }


        protected bool CheckDate(String date)
        {
            try
            {
                DateTime dt = DateTime.Parse(date);
                return true;
            }
            catch
            {
                return false;
            }
        }
        [HttpPost]
        public ActionResult LoadSCPLineData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                int headerid = Convert.ToInt32(param.Headerid);
                Session["Headerid"] = Convert.ToInt32(param.Headerid);
                var isEditable = false;
                var status = db.SCP001.Where(m => m.HeaderId == headerid).FirstOrDefault();
                if (status.Status.ToLower() == clsImplementationEnum.CTQStatus.DRAFT.GetStringValue().ToLower() ||
                     status.Status.ToLower() == clsImplementationEnum.CTQStatus.Returned.GetStringValue().ToLower())
                {
                    isEditable = true;
                }
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;

                string strWhere = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere = " (ReferenceDrawing like '%" + param.sSearch
                        + "%' or ScopeOfWork like '%" + param.sSearch
                        + "%' or TaskManager like '%" + param.sSearch
                        + "%' or TaskDescription like '%" + param.sSearch
                        + "%' or EstFor like '%" + param.sSearch
                        + "%' or Weight like '%" + param.sSearch
                        + "%' or RCCHours like '%" + param.sSearch
                        + "%' or ExpMatDelDate like '%" + param.sSearch
                        + "%' or ReqComDate like '%" + param.sSearch
                        + "%' or SubConTo like '%" + param.sSearch
                        + "%' or DeliverTo like '%" + param.sSearch
                        + "%')";
                }
                else
                {
                    strWhere = "1=1";
                }
                List<clsTask> list = new List<clsTask>();

                var lstResult = db.SP_SCP_GET_LINEDETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, headerid, strWhere
                                ).ToList();

                List<string> lstTestCategory = (from t1 in db.SCP003
                                                select t1.TaskCategory).Distinct().ToList();


                var taskcategory = db.SCP003.Select(i => new CategoryData { Value = i.TaskCategory, Code = i.TaskCategory, CategoryDescription = i.TaskCategory }).ToList();
                //  SelectList taskcategory = new SelectList(lstTestCategory, "TaskCategory", "TaskCategory");
                SelectList taskmanager = new SelectList(list, "text", "text");
                int newRecordId = 0;
                var newRecord = new[] {
                                        "",
                                        Helper.GenerateTextbox(newRecordId, "TaskDescription"),
                                        //Helper.GenerateTextbox(newRecordId, "ScopeOfWork"),
                                          GenerateTextArea(newRecordId, "ScopeOfWork","","","",false,"width:200px;","1000"),
                                        GenerateTextArea(newRecordId, "ReferenceDrawing","","","",false,"width:200px;","1000"),
                                        //Helper.GenerateTextbox(newRecordId, "ReferenceDrawing"),
                                        GenerateAutoComplete(newRecordId, "TaskCategory","","",false,"","hdTaskCategory")+""+Helper.GenerateHidden(newRecordId,"hdTaskCategory"),
                                        HTMLAutoComplete(newRecordId, "TaskManager","","",false,"","hdTaskManager")+""+Helper.GenerateHidden(newRecordId,"hdTaskManager"),
                                        Helper.GenerateTextbox(newRecordId, "EstFor"),
                                        Helper.GenerateTextbox(newRecordId, "Weight"),
                                        Helper.GenerateTextbox(newRecordId, "ExpMatDelDate"),
                                        Helper.GenerateTextbox(newRecordId, "ReqComDate"),
                                        Helper.GenerateTextbox(newRecordId, "SubConTo"),
                                        Helper.GenerateTextbox(newRecordId, "RCCHours"),
                                        Helper.GenerateTextbox(newRecordId, "DeliverTo"),
                                        Helper.GenerateGridButton(newRecordId, "Save", "Add Rocord", "fa fa-plus", "SaveNewRecord();" ),
                                        "",
                                        Helper.GenerateHidden(newRecordId, "HeaderId", param.Headerid),
                                        ""
                                    };

                var data = (from uc in lstResult
                            select new[]
                            {
                                Convert.ToString(uc.ROW_NO),
                                //Convert.ToString(uc.ReferenceDrawing) == "" ?    Convert.ToString(uc.ReferenceDrawing) :  "<ul><li>" + Convert.ToString(uc.ReferenceDrawing).Replace("|", "</li><li>").ToString()+"</li></ul>",
                                //Convert.ToString(uc.TaskDescription),
                                //Convert.ToString(uc.ScopeOfWork) == "" ?     Convert.ToString(uc.ScopeOfWork) :  "<ul><li>" + Convert.ToString(uc.ScopeOfWork).Replace("|", "</li><li>").ToString()+"</li></ul>",
                                uc.IsLNLine == true ? Convert.ToString(uc.TaskDescription) : isEditable == true ? GenerateTextboxFor(uc.LineId,"", "TaskDescription", uc.TaskDescription, "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", false) : Convert.ToString(uc.TaskDescription),
                                uc.IsLNLine == true ? Convert.ToString(uc.ScopeOfWork) == "" ?     Convert.ToString(uc.ScopeOfWork) :  "<ul><li>" + Convert.ToString(uc.ScopeOfWork).Replace("|", "</li><li>").ToString()+"</li></ul>" : isEditable == true ? GenerateTextArea(uc.LineId, "ScopeOfWork", uc.ScopeOfWork.Replace("|","\n"), "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", false,"width:200px;","1000") : Convert.ToString(uc.ScopeOfWork) == "" ?     Convert.ToString(uc.ScopeOfWork) :  "<ul><li>" + Convert.ToString(uc.ScopeOfWork).Replace("|", "</li><li>").ToString()+"</li></ul>",
                                uc.IsLNLine == true ? Convert.ToString(uc.ReferenceDrawing) == "" ?    Convert.ToString(uc.ReferenceDrawing) :  "<ul><li>" + Convert.ToString(uc.ReferenceDrawing).Replace("|", "</li><li>").ToString()+"</li></ul>" : isEditable == true ?
                                  GenerateTextArea(uc.LineId, "ReferenceDrawing",    Convert.ToString(uc.ReferenceDrawing.Replace("|","\n")),"UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");",false,"width:200px;","1000")

                                //GenerateTextboxFor(uc.LineId,"", "ReferenceDrawing", uc.ReferenceDrawing, "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", false)
                                : Convert.ToString(uc.ReferenceDrawing) == "" ?    Convert.ToString(uc.ReferenceDrawing) :  "<ul><li>" + Convert.ToString(uc.ReferenceDrawing).Replace("|", "</li><li>").ToString()+"</li></ul>",

                                isEditable == true ? HTMLAutoComplete(uc.LineId, "TaskCategory",uc.TaskCategory,"UpdateData(this, "+ uc.LineId +");",false,"","hdTaskCategory")+""+Helper.GenerateHidden(uc.LineId, "hdTaskCategory") : HTMLAutoComplete(uc.LineId, "TaskCategory",uc.TaskCategory,"UpdateData(this, "+ uc.LineId +");",true,"","hdTaskCategory")+""+Helper.GenerateHidden(uc.LineId, "hdTaskCategory"),
                                uc.IsLNLine == true ?  HTMLAutoComplete(uc.LineId, "TaskManager",uc.TaskManager,"UpdateData(this, "+ uc.LineId +");",true,"","hdTaskManager")+""+Helper.GenerateHidden(uc.LineId, "hdTaskManager") : isEditable == true ? HTMLAutoComplete(uc.LineId, "TaskManager",uc.TaskManager,"UpdateData(this, "+ uc.LineId +");",false,"","hdTaskManager")+""+Helper.GenerateHidden(uc.LineId, "hdTaskManager") : HTMLAutoComplete(uc.LineId, "TaskManager",uc.TaskManager,"UpdateData(this, "+ uc.LineId +");",true,"","hdTaskManager")+""+Helper.GenerateHidden(uc.LineId, "hdTaskManager"),


                                isEditable == false ? GenerateTextboxFor(uc.LineId,"", "EstFor", uc.EstFor, "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", true) : uc.IsLNLine == true ? GenerateTextboxFor(uc.LineId,"", "EstFor", uc.EstFor, "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", false) : GenerateTextboxFor(uc.LineId,"", "EstFor", uc.EstFor, "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", false),
                                isEditable == false ? GenerateTextboxFor(uc.LineId,"", "Weight", Convert.ToString(uc.Weight), "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", true) : uc.IsLNLine == true ? GenerateTextboxFor(uc.LineId,"", "Weight", Convert.ToString(uc.Weight), "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", false) : GenerateTextboxFor(uc.LineId,"", "Weight", Convert.ToString(uc.Weight), "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", false),

                                isEditable == false ? GenerateTextboxFor(uc.LineId,"", "ExpMatDelDate", Convert.ToDateTime(uc.ExpMatDelDate).ToString("yyyy-MM-dd"), "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", true) : uc.IsLNLine == true ? GenerateTextboxFor(uc.LineId,"", "ExpMatDelDate", Convert.ToDateTime(uc.ExpMatDelDate).ToString("yyyy-MM-dd"), "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", false) : GenerateTextboxFor(uc.LineId,"", "ExpMatDelDate", Convert.ToDateTime(uc.ExpMatDelDate).ToString("yyyy-MM-dd"), "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", false),
                                isEditable == false ? GenerateTextboxFor(uc.LineId,"", "ReqComDate", Convert.ToDateTime(uc.ReqComDate).ToString("yyyy-MM-dd"), "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", true) : uc.IsLNLine == true ? GenerateTextboxFor(uc.LineId,"", "ReqComDate", Convert.ToDateTime(uc.ReqComDate).ToString("yyyy-MM-dd"), "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", false) : GenerateTextboxFor(uc.LineId,"", "ReqComDate", Convert.ToDateTime(uc.ReqComDate).ToString("yyyy-MM-dd"), "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", false),
                                isEditable == false ? GenerateTextboxFor(uc.LineId,"", "SubConTo", uc.SubConTo, "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", true) : uc.IsLNLine == true ? GenerateTextboxFor(uc.LineId,"", "SubConTo", uc.SubConTo, "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", false) : GenerateTextboxFor(uc.LineId,"", "SubConTo", uc.SubConTo, "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", false),
                                isEditable == false ? GenerateTextboxFor(uc.LineId,"", "RCCHours", Convert.ToString(uc.RCCHours), "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", true) : uc.IsLNLine == true ? GenerateTextboxFor(uc.LineId,"", "RCCHours", Convert.ToString(uc.RCCHours), "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", false) : GenerateTextboxFor(uc.LineId,"", "RCCHours", Convert.ToString(uc.RCCHours), "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", false),
                                isEditable == false ? GenerateTextboxFor(uc.LineId,"", "DeliverTo", uc.DeliverTo, "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", true) : uc.IsLNLine == true ? GenerateTextboxFor(uc.LineId,"", "DeliverTo", uc.DeliverTo, "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", false) : GenerateTextboxFor(uc.LineId,"", "DeliverTo", uc.DeliverTo, "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", false),
                                //isEditable == false ? null : uc.IsLNLine == false ? HTMLActionString(uc.HeaderId,"","Delete","Delete Record","fa fa-trash-o","DeleteRecord("+ uc.LineId +");") : null,
                                isEditable == false ? null :  HTMLActionString(uc.HeaderId,"","Delete","Delete Record","fa fa-trash-o","DeleteRecord("+ uc.LineId +");"),
                                Helper.GenerateHidden(uc.LineId, "LineId", uc.LineId.ToString()),
                                Helper.GenerateHidden(uc.LineId, "HeaderId", uc.HeaderId.ToString()),
                                Convert.ToString(uc.IsLNLine)
                           }).ToList();

                data.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }
        [NonAction]
        public  string GenerateTextArea(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", string onClickMethod = "", bool isReadOnly = false, string inputStyle = "", string maxlength = "", string role = "")
        {
            string htmlControl = "";

            string inputID = columnName + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "form-control";
            string MaxCharcters = !string.IsNullOrEmpty(maxlength) ? "maxlength='" + maxlength + "'" : "";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onchange='" + onBlurMethod + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";
            string userRole = role != "" && !string.IsNullOrEmpty(role) ? "role = '" + role + "'" : "";
            htmlControl = "<textarea " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' " + MaxCharcters + " colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + userRole + " spara='" + rowId + "'  " + onClickEvent + ">" + inputValue + "</textarea>";

            return htmlControl;
        }


        [HttpPost]
        public ActionResult GetInsertView(string project, string document, string RevNo)
        {
            project = project.Split('-')[0].Trim();

            int Headerid = (from a in db.SCP001
                            where a.Project == project
                            select a.HeaderId).FirstOrDefault();
            List<string> lstTestCategory = (from t1 in db.SCP003
                                            select t1.TaskCategory).Distinct().ToList();

            ViewBag.lstTestCategory = lstTestCategory.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

            ViewBag.project = project;
            ViewBag.document = document;
            ViewBag.RevNo = RevNo;
            ViewBag.Headerid = Headerid;

            List<clsTask> list = new List<clsTask>();
            connection.Open();
            string query = "select  DISTINCT  B.CONTACT as 'Task Manager' " +
                            " from[dbo].[VR_PROJECT_TASK_CHECKLIST]  a " +
                            " LEFT  join[dbo].[PROJ_TASK] b on a.PROJECTID = b.PROJECTID and a.TASKUNIQUEID = b.TASKUNIQUEID " +
                            " where a.PROJECTNAME = @proj and a.CLITEMTEXT IS NOT NULL " +
                            " ORDER BY B.CONTACT";
            SqlCommand cmd = new SqlCommand(query, connection);
            cmd.Parameters.AddWithValue("@proj", project);
            SqlDataReader reader = cmd.ExecuteReader();
            List<KeyValuePair<string, string>> lst = new List<KeyValuePair<string, string>>();
            while (reader.Read())
            {
                list.Add(new clsTask { id = reader.GetString(0).ToString(), text = reader.GetString(0).ToString() });
            }

            reader.Close();
            connection.Close();
            ViewBag.lstTask = new SelectList(list, "task", "taskt");

            SCP002 objSCP002 = new SCP002();
            return PartialView("SCPLines", objSCP002);
        }
        [HttpPost]
        public ActionResult GetUpdateView(string project, string document, string RevNo, int lineid, string isln)
        {
            project = project.Split('-')[0].Trim();
            SCP002 objSCP002 = new SCP002();
            var isvalid = db.SCP002.Any(x => x.LineId == lineid);
            if (isvalid == true)
            {
                objSCP002 = db.SCP002.Where(x => x.LineId == lineid).FirstOrDefault();
                if (objSCP002 != null)
                {
                    int Headerid = (from a in db.SCP001
                                    where a.Project == project
                                    select a.HeaderId).FirstOrDefault();

                    List<string> lstTestCategory = (from t1 in db.SCP003
                                                    select t1.TaskCategory).Distinct().ToList();

                    ViewBag.lstTestCategory = lstTestCategory.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

                    ViewBag.project = project;
                    ViewBag.document = document;
                    ViewBag.RevNo = RevNo;
                    ViewBag.Headerid = Headerid;
                    ViewBag.Lineid = lineid;
                    ViewBag.isln = isln;
                    ViewBag.refdrawing = objSCP002.ReferenceDrawing.ToString().Replace(",", Environment.NewLine);
                    ViewBag.scopeofwork = objSCP002.ScopeOfWork.ToString().Replace(",", "</br>");
                    List<clsTask> list = new List<clsTask>();
                    connection.Open();
                    string query = "select  DISTINCT  B.CONTACT as 'Task Manager' " +
                                    " from[dbo].[VR_PROJECT_TASK_CHECKLIST]  a " +
                                    " LEFT  join[dbo].[PROJ_TASK] b on a.PROJECTID = b.PROJECTID and a.TASKUNIQUEID = b.TASKUNIQUEID " +
                                    " where a.PROJECTNAME = @proj and a.CLITEMTEXT IS NOT NULL " +
                                    " ORDER BY B.CONTACT";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    cmd.Parameters.AddWithValue("@proj", project);
                    SqlDataReader reader = cmd.ExecuteReader();
                    List<KeyValuePair<string, string>> lst = new List<KeyValuePair<string, string>>();
                    while (reader.Read())
                    {
                        list.Add(new clsTask { id = reader.GetString(0).ToString(), text = reader.GetString(0).ToString() });
                    }

                    reader.Close();
                    connection.Close();
                    ViewBag.lstTask = new SelectList(list, "task", "taskt");
                }
            }
            return PartialView("SCPLines", objSCP002);
        }
        [HttpPost]
        public ActionResult SaveSCPLines(SCP002 scp002, FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (CheckDate(fc["ExpMatDelDate0"].ToString()) || CheckDate(fc["ReqComDate0"].ToString()))
                {
                    int newRowIndex = 0;
                    int headerId = Convert.ToInt32(fc["HeaderId" + newRowIndex]);
                    SCP001 objscpoo1 = db.SCP001.Where(x => x.HeaderId == headerId).FirstOrDefault();

                    SCP002 objSCP002 = new SCP002();
                    objSCP002.HeaderId = Convert.ToInt32(objscpoo1.HeaderId);
                    objSCP002.Project = objscpoo1.Project;
                    objSCP002.Document = objscpoo1.Document;

                    objSCP002.TaskCategory = fc["TaskCategory0"].ToString();
                    //  objSCP002.RevNo = Convert.ToInt32(Regex.Replace(fc["RevNo0"].ToString(), "[^0-9]+", string.Empty));
                    objSCP002.ReferenceDrawing = fc["ReferenceDrawing0"].ToString() != "" ? fc["ReferenceDrawing0"].ToString().Replace("\r\n", "|").Replace("\n","|"):"";
                    objSCP002.ScopeOfWork = fc["ScopeOfWork0"].ToString() !=""? fc["ScopeOfWork0"].ToString().Replace("\r\n", "|").Replace("\n", "|"):"";
                    objSCP002.TaskManager = fc["TaskManager0"].ToString();
                    objSCP002.TaskDescription = fc["TaskDescription0"].ToString();
                    objSCP002.EstFor = fc["EstFor0"].ToString();
                    objSCP002.Weight = string.IsNullOrWhiteSpace(fc["Weight0"].ToString()) ? 0 : Convert.ToInt32(fc["Weight0"]); // Convert.ToInt32(fc["txtWeight"].ToString());
                    objSCP002.RCCHours = string.IsNullOrWhiteSpace(fc["RCCHours0"].ToString()) ? 0 : Convert.ToInt32(fc["RCCHours0"]);  //Convert.ToInt32(fc["txtRccp"].ToString());
                    if (string.IsNullOrWhiteSpace(fc["ExpMatDelDate0"].ToString()) || fc["ExpMatDelDate0"].ToString() == "01/01/0001")
                    {
                        objSCP002.ExpMatDelDate = null;
                    }
                    else
                    {
                        objSCP002.ExpMatDelDate = Convert.ToDateTime(fc["ExpMatDelDate0"].ToString());
                    }

                    if (string.IsNullOrWhiteSpace(fc["ReqComDate0"].ToString()) || fc["ReqComDate0"].ToString() == "01/01/0001")
                    {
                        objSCP002.ReqComDate = null;
                    }
                    else
                    {
                        objSCP002.ReqComDate = Convert.ToDateTime(fc["ReqComDate0"].ToString());
                    }
                    objSCP002.SubConTo = fc["SubConTo0"].ToString();
                    objSCP002.DeliverTo = fc["DeliverTo0"].ToString();
                    objSCP002.IsLNLine = false;
                    objSCP002.CreatedBy = objClsLoginInfo.UserName;
                    objSCP002.CreatedOn = DateTime.Now;
                    db.SCP002.Add(objSCP002);
                    db.SaveChanges();


                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Invalid Date";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteSCPLine(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                SCP002 objSCP002 = db.SCP002.Where(x => x.LineId == Id).FirstOrDefault();
                db.SCP002.Remove(objSCP002);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Delete.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult LoadSCPHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;


                var user = objClsLoginInfo.UserName;

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and status in('" + clsImplementationEnum.CTQStatus.DRAFT.GetStringValue() + "','" + clsImplementationEnum.CTQStatus.Returned.GetStringValue() + "')";
                }
                else
                {
                    strWhere += "1=1";
                }
                //strWhere += " and CreatedBy=" + user;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += "and (Project like '%" + param.sSearch
                       + "%' or Document like '%" + param.sSearch
                       + "%' or Customer like '%" + param.sSearch
                       + "%' or RevNo like '%" + param.sSearch
                       + "%' or Product like '%" + param.sSearch
                       + "%' or ProcessLicensor like '%" + param.sSearch
                       + "%' or Status like '%" + param.sSearch + "%')";
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }


                var lstResult = db.SP_SCP_GET_HEADERDETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(GetCodeAndNameByProject(uc.Project)),
                               Convert.ToString(uc.Document),
                               Convert.ToString(Manager.GetCustomerCodeAndNameByProject(uc.Project)),
                               Convert.ToString(uc.Product),
                               Convert.ToString(uc.ProcessLicensor),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.Status),
                               Convert.ToString(uc.SubmittedBy),
                               Convert.ToString(Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy")),
                               Convert.ToString(uc.ApprovedBy),
                               Convert.ToString(Convert.ToDateTime(uc.ApprovedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.ApprovedOn).ToString("dd/MM/yyyy")),

                               "<nobr><center>" +"<a title='View' href='"+WebsiteURL+"/SCP/MaintainSCP/MaintainSCPDetails?Id="+Convert.ToInt32(uc.HeaderId)+"'><i style='' class='iconspace fa fa-eye'></i></a>"+
                               Helper.GenerateActionIcon(uc.HeaderId,"Delete","Delete Record","fa fa-trash-o", "DeleteDocument("+ uc.HeaderId +",'/SCP/MaintainSCP/DeleteHeader', {headerid:"+uc.HeaderId+"}, 'tblHTCHeader')","",  (( uc.RevNo >0 && uc.Status != clsImplementationEnum.CommonStatus.SendForApprovel.GetStringValue()) || ( uc.RevNo == 0 && uc.Status == clsImplementationEnum.CommonStatus.Approved.GetStringValue()) ) ? false:true) +
                               (uc.RevNo>0 ?"<a title=\"History\" onclick=\"history('"+uc.HeaderId+"')\"><i style='' class='iconspace fa fa-history'></i></a>":"<a title=\"History\"><i style='' class='disabledicon fa fa-history'></i></a>")+"<a title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/SCP/MaintainSCP/ShowTimeline?HeaderID=" + Convert.ToInt32(uc.HeaderId) + "')><i style='' class='iconspace fa fa-clock-o'></i></a>"
                               + "</center></nobr>",
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult DeleteHeader(int headerid)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                objResponseMsg = Manager.DeletePDINDocument(headerid, clsImplementationEnum.PlanList.Sub_Contracting_Plan);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ShowHistoryTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();

            model.TimelineTitle = "SCP TimeLine";
            model.Title = "PDinDoc";
            if (HeaderId > 0)
            {

                SCP001_Log objSCP001_Log = db.SCP001_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                model.CreatedBy = objSCP001_Log.CreatedBy != null ? Manager.GetUserNameFromPsNo(objSCP001_Log.CreatedBy) : null;
                model.CreatedOn = objSCP001_Log.CreatedOn;
                model.EditedBy = objSCP001_Log.EditedBy != null ? Manager.GetUserNameFromPsNo(objSCP001_Log.EditedBy) : null;
                model.EditedOn = objSCP001_Log.EditedOn;
                model.SubmittedBy = objSCP001_Log.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objSCP001_Log.SubmittedBy) : null;
                model.SubmittedOn = objSCP001_Log.SubmittedOn;

                model.ApprovedBy = objSCP001_Log.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objSCP001_Log.ApprovedBy) : null;
                model.ApprovedOn = objSCP001_Log.ApprovedOn;

            }
            else
            {

                SCP001_Log objSCP001_Log = db.SCP001_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                model.CreatedBy = objSCP001_Log.CreatedBy != null ? Manager.GetUserNameFromPsNo(objSCP001_Log.CreatedBy) : null;
                model.CreatedOn = objSCP001_Log.CreatedOn;
                model.EditedBy = objSCP001_Log.EditedBy != null ? Manager.GetUserNameFromPsNo(objSCP001_Log.EditedBy) : null;
                model.EditedOn = objSCP001_Log.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
        public ActionResult ShowTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();
            model.Title = "PDinDoc";
            model.TimelineTitle = "SCP Timeline";

            if (HeaderId > 0)
            {
                SCP001 objSCP001 = db.SCP001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.CreatedBy = objSCP001.CreatedBy != null ? Manager.GetUserNameFromPsNo(objSCP001.CreatedBy) : null;
                model.CreatedOn = objSCP001.CreatedOn;
                model.EditedBy = objSCP001.EditedBy != null ? Manager.GetUserNameFromPsNo(objSCP001.EditedBy) : null;
                model.EditedOn = objSCP001.EditedOn;

                // model.SubmittedBy = PMB001_Log.SendToCompiledOn != null ? Manager.GetUserNameFromPsNo(PMB001_Log.SendToCompiledOn) : null;
                //model.SubmittedOn = PMB001_Log.SendToCompiledOn;
                model.SubmittedBy = objSCP001.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objSCP001.SubmittedBy) : null;
                model.SubmittedOn = objSCP001.SubmittedOn;
                //model.CompiledBy = PMB001_Log.CompiledBy != null ? Manager.GetUserNameFromPsNo(PMB001_Log.CompiledBy) : null;
                // model.CompiledOn = PMB001_Log.CompiledOn;
                model.ApprovedBy = objSCP001.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objSCP001.ApprovedBy) : null;
                model.ApprovedOn = objSCP001.ApprovedOn;

            }
            else
            {
                SCP001 objSCP001 = db.SCP001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.CreatedBy = objSCP001.CreatedBy != null ? Manager.GetUserNameFromPsNo(objSCP001.CreatedBy) : null;
                model.CreatedOn = objSCP001.CreatedOn;
                model.EditedBy = objSCP001.EditedBy != null ? Manager.GetUserNameFromPsNo(objSCP001.EditedBy) : null;
                model.EditedOn = objSCP001.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
        public string GetCodeAndNameByProject(string project)
        {
            var projdesc = db.COM001.Where(i => i.t_cprj == project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
            return projdesc;
        }
        [HttpPost]
        public ActionResult GetHeaderGridDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("GetSCPGridPartial");
        }
        [HttpPost]
        public JsonResult sentForApproval(string Approver, int headerid)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (headerid > 0)
                {
                    if (Approver.Split('-')[0].ToString().Trim() == objClsLoginInfo.UserName)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.SMessage.ToString();
                    }
                    else
                    {
                        SCP001 objSCP001 = db.SCP001.Where(x => x.HeaderId == headerid).FirstOrDefault();
                        objSCP001.Status = clsImplementationEnum.CTQStatus.SendForApprovel.GetStringValue();
                        objSCP001.ApprovedBy = Approver.Split('-')[0].ToString().Trim();
                        objSCP001.SubmittedBy = objClsLoginInfo.UserName;
                        objSCP001.SubmittedOn = DateTime.Now;

                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.AMessage.ToString();
                        Manager.UpdatePDN002(objSCP001.HeaderId, objSCP001.Status, objSCP001.RevNo, objSCP001.Project, objSCP001.Document);

                        #region Send Notification
                        (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG2.GetStringValue() + "," + clsImplementationEnum.UserRoleName.PLNG1.GetStringValue(),
                                                            objSCP001.Project,
                                                            "",
                                                            "",
                                                            Manager.GetPDINDocumentNotificationMsg(objSCP001.Project, clsImplementationEnum.PlanList.Sub_Contracting_Plan.GetStringValue(), objSCP001.RevNo.Value.ToString(), objSCP001.Status),
                                                            clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(),
                                                            Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Sub_Contracting_Plan.GetStringValue(), objSCP001.HeaderId.ToString(), true),
                                                            objSCP001.ApprovedBy);
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateSCPLines(SCP002 scp002, FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string a = fc["lineid"].ToString();
                int lineid = Convert.ToInt32(fc["lineid"].ToString());
                if (lineid > 0)
                {
                    SCP002 objSCP002 = db.SCP002.Where(x => x.LineId == lineid).FirstOrDefault();
                    objSCP002.HeaderId = Convert.ToInt32(fc["hfHeaderId"].ToString());
                    objSCP002.Project = fc["hfProject"].ToString();
                    objSCP002.Document = fc["hfdoc"].ToString();
                    objSCP002.TaskCategory = fc["ddlCat"].ToString();
                    objSCP002.RevNo = Convert.ToInt32(Regex.Replace(fc["hfRev"].ToString(), "[^0-9]+", string.Empty));
                    objSCP002.ReferenceDrawing = fc["txtRef"].ToString().Replace("\r\n", "|");
                    objSCP002.ScopeOfWork = fc["txtScope"].ToString().Replace("\r\n", "|").Trim();
                    objSCP002.TaskManager = fc["ddlTask1"].ToString();
                    objSCP002.TaskDescription = fc["txtTaskDesc"].ToString();
                    objSCP002.EstFor = fc["txtEst"].ToString();
                    objSCP002.Weight = string.IsNullOrWhiteSpace(fc["txtWeight"].ToString()) ? 0 : Convert.ToInt32(fc["txtWeight"]); // Convert.ToInt32(fc["txtWeight"].ToString());
                    objSCP002.RCCHours = string.IsNullOrWhiteSpace(fc["txtRccp"].ToString()) ? 0 : Convert.ToInt32(fc["txtRccp"]);  //Convert.ToInt32(fc["txtRccp"].ToString());
                    if (string.IsNullOrWhiteSpace(fc["hfExp"].ToString()) || fc["hfExp"].ToString() == "01/01/0001")
                    {
                        objSCP002.ExpMatDelDate = null;
                    }
                    else
                    {
                        objSCP002.ExpMatDelDate = Convert.ToDateTime(fc["hfExp"].ToString());
                    }

                    if (string.IsNullOrWhiteSpace(fc["hfReq"].ToString()) || fc["hfReq"].ToString() == "01/01/0001")
                    {
                        objSCP002.ReqComDate = null;
                    }
                    else
                    {
                        objSCP002.ReqComDate = Convert.ToDateTime(fc["hfReq"].ToString());
                    }
                    objSCP002.SubConTo = fc["txtSub"].ToString();
                    objSCP002.DeliverTo = fc["txtDel"].ToString();
                    objSCP002.IsLNLine = Convert.ToBoolean(fc["hfisln"].ToString());
                    objSCP002.EditedBy = objClsLoginInfo.UserName;
                    objSCP002.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetHistoryView(int headerid = 0)
        {
            SCP001_Log objSCP001 = new SCP001_Log();
            Session["headerid"] = Convert.ToInt32(headerid);
            return PartialView("getHistoryPartial", objSCP001);
        }

        [HttpPost]
        public JsonResult LoadSCPHeaderHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var user = objClsLoginInfo.UserName;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                string strWhere = string.Empty;

                strWhere += " 1 = 1 and HeaderId=" + Convert.ToInt32(Session["headerid"]);
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " Project like '%" + param.sSearch
                       + "%' or Document like '%" + param.sSearch
                       + "%' or Customer like '%" + param.sSearch
                       + "%' or RevNo like '%" + param.sSearch
                       + "%' or Product like '%" + param.sSearch
                       + "%' or ProcessLicensor like '%" + param.sSearch
                       + "%' or Status like '%" + param.sSearch + "%'";
                }
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_SCP_GET_HISTORY_HEADERDETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {  Convert.ToString(uc.HeaderId),
                               Convert.ToString(GetCodeAndNameByProject(uc.Project)),
                               Convert.ToString(uc.Document),
                               Convert.ToString(Manager.GetCustomerCodeAndNameByProject(uc.Project)),
                               Convert.ToString(uc.Product),
                               Convert.ToString(uc.ProcessLicensor),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.Status),
                                Convert.ToString(uc.SubmittedBy),
                                Convert.ToString(Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy")),
                                Convert.ToString(uc.ApprovedBy),
                                Convert.ToString(Convert.ToDateTime(uc.ApprovedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.ApprovedOn).ToString("dd/MM/yyyy")),

                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(db.SCP001_Log.FirstOrDefault(q => (q.HeaderId == uc.HeaderId && q.RevNo == uc.RevNo)).Id)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult LoadSCPLineHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                int headerid = Convert.ToInt32(param.Headerid);
                Session["Headerid"] = Convert.ToInt32(headerid);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                string strWhere = string.Empty;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere = " (ReferenceDrawing like '%" + param.sSearch
                       + "%' or ScopeOfWork like '%" + param.sSearch
                       + "%' or TaskManager like '%" + param.sSearch

                       + "%' or EstFor like '%" + param.sSearch
                       + "%' or Weight like '%" + param.sSearch
                       + "%' or RCCHours like '%" + param.sSearch
                       + "%' or ExpMatDelDate like '%" + param.sSearch
                       + "%' or ReqComDate like '%" + param.sSearch
                       + "%' or SubConTo like '%" + param.sSearch
                       + "%' or DeliverTo like '%" + param.sSearch
                       + "%' or TaskDescription like '%" + param.sSearch
                       + "%')";
                }
                else
                {
                    strWhere = "1=1";
                }
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_SCP_GET_HISTORY_LINEDETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, headerid, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                            {
                           Convert.ToString(uc.ROW_NO),
                           Convert.ToString(uc.TaskDescription),
                           Convert.ToString(uc.ScopeOfWork),
                           Convert.ToString(uc.ReferenceDrawing),
                           Convert.ToString(uc.TaskManager),
                           Convert.ToString(uc.EstFor),
                           Convert.ToString(uc.Weight),
                           Convert.ToString(uc.RCCHours),
                          Convert.ToString(Convert.ToDateTime(uc.ExpMatDelDate).ToShortDateString()),
                           Convert.ToString(Convert.ToDateTime(uc.ReqComDate).ToShortDateString()),
                           Convert.ToString(uc.SubConTo),
                           Convert.ToString(uc.DeliverTo),

                           Convert.ToString(uc.HeaderId),
                           Convert.ToString(uc.LineId),
                           Convert.ToString(uc.IsLNLine)
                           }).ToList();
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }
        [SessionExpireFilter]
        public ActionResult SCPHistoryDetails(int Id = 0)
        {
            SCP001_Log objSCP001 = new SCP001_Log();
            objSCP001 = db.SCP001_Log.Where(x => x.Id == Id).FirstOrDefault();
            var user = objClsLoginInfo.UserName;
            if (objSCP001 != null)
            {
                var lstProjectDesc = (from a in db.COM001
                                      select new Projects { projectCode = a.t_cprj, projectDescription = a.t_cprj + " - " + a.t_dsca }).ToList();
                ViewBag.Project = new SelectList(lstProjectDesc, "projectCode", "projectDescription");

                List<string> lstProCat = clsImplementationEnum.getProductCategory().ToList();
                ViewBag.lstProCat = lstProCat.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

                var approver = (from t1 in db.ATH001
                                join t2 in db.COM003 on t1.Employee equals t2.t_psno
                                where t2.t_actv == 1
                                select new { t1.Employee, Desc = t2.t_psno + " - " + t2.t_name }).Distinct().ToList();
                ViewBag.approver = new SelectList(approver, "Employee", "Desc");
                if (Id > 0)
                {
                    var project = (from a in db.SCP001_Log
                                   where a.Id == Id
                                   select a.Project).FirstOrDefault();
                    ViewBag.Customer = Manager.GetCustomerCodeAndNameByProject(project);

                }
                var urlPrefix = Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.LastIndexOf('=') + 1);
                if (!objClsLoginInfo.GetUserRoleList().Contains("SHOP"))
                {
                    ViewBag.RevPrev = (db.SCP001_Log.Any(q => (q.HeaderId == objSCP001.HeaderId && q.RevNo == (objSCP001.RevNo - 1))) ? urlPrefix + db.SCP001_Log.Where(q => (q.HeaderId == objSCP001.HeaderId && q.RevNo == (objSCP001.RevNo - 1))).FirstOrDefault().Id : null);
                    ViewBag.RevNext = (db.SCP001_Log.Any(q => (q.HeaderId == objSCP001.HeaderId && q.RevNo == (objSCP001.RevNo + 1))) ? urlPrefix + db.SCP001_Log.Where(q => (q.HeaderId == objSCP001.HeaderId && q.RevNo == (objSCP001.RevNo + 1))).FirstOrDefault().Id : null);
                }
            }
            return View(objSCP001);
        }
        public JsonResult createRevison(int headerid)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int? revison = (from a in db.SCP001
                                where a.HeaderId == headerid
                                select a.RevNo).FirstOrDefault();
                revison = revison + 1;
                SCP001 objSCP001 = db.SCP001.Where(x => x.HeaderId == headerid).FirstOrDefault();
                objSCP001.Status = clsImplementationEnum.CTQStatus.DRAFT.GetStringValue();
                objSCP001.RevNo = revison;
                objSCP001.EditedBy = objClsLoginInfo.UserName;
                objSCP001.EditedOn = DateTime.Now;
                objSCP001.ReturnRemark = null;
                objSCP001.ApprovedOn = null;
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = revison.ToString();
                Manager.UpdatePDN002(objSCP001.HeaderId, objSCP001.Status, objSCP001.RevNo, objSCP001.Project, objSCP001.Document);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult getCodeValue(string project, string approver)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                project = project.Split('-')[0].Trim();
                objResponseMsg.projdesc = db.COM001.Where(i => i.t_cprj == project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objResponseMsg.appdesc = db.COM003.Where(i => i.t_psno == approver && i.t_actv == 1).Select(i => i.t_psno + " - " + i.t_name).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult verifyApprover(string approver)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                approver = approver.Split('-')[0].Trim();
                var Exists = db.COM003.Any(x => x.t_psno == approver && x.t_actv == 1);
                if (Exists == false)
                {
                    objResponseMsg.Key = false;
                }
                else
                {
                    objResponseMsg.Key = true;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult isLineAvailable(string project)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                project = project.Split('-')[0].Trim();
                var Exists = db.SCP002.Any(x => x.Project == project);
                if (Exists == false)
                {
                    objResponseMsg.Key = false;
                }
                else
                {
                    objResponseMsg.Key = true;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult retrack(string headerid)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                int hd = Convert.ToInt32(headerid);
                if (hd > 0)
                {
                    SCP001 objSCP001 = db.SCP001.Where(x => x.HeaderId == hd).FirstOrDefault();
                    objSCP001.SubmittedOn = null;
                    objSCP001.SubmittedBy = null;

                    objSCP001.Status = clsImplementationEnum.CTQStatus.DRAFT.GetStringValue();
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = objSCP001.Status;
                    Manager.UpdatePDN002(objSCP001.HeaderId, objSCP001.Status, objSCP001.RevNo, objSCP001.Project, objSCP001.Document);
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        //// Modification in popup to inlinr grid
        [HttpPost]
        public ActionResult ReviseHeader(int strHeaderId, string strRemarks)
        {
            CustomResponceMsg objResponseMsg = new CustomResponceMsg();
            try
            {
                SCP001 objSCP001 = db.SCP001.Where(u => u.HeaderId == strHeaderId).SingleOrDefault();
                if (objSCP001 != null)
                {
                    objSCP001.RevNo = Convert.ToInt32(objSCP001.RevNo) + 1;
                    objSCP001.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                    objSCP001.ReviseRemark = strRemarks;
                    objSCP001.EditedBy = objClsLoginInfo.UserName;
                    objSCP001.EditedOn = DateTime.Now;
                    objSCP001.ReturnRemark = null;
                    objSCP001.ApprovedOn = null;
                    objSCP001.SubmittedBy = null;
                    objSCP001.SubmittedOn = null;
                    db.SaveChanges();
                    Manager.UpdatePDN002(objSCP001.HeaderId, objSCP001.Status, objSCP001.RevNo, objSCP001.Project, objSCP001.Document);
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderID = objSCP001.HeaderId;
                    objResponseMsg.Status = objSCP001.Status;
                    objResponseMsg.rev = objSCP001.RevNo;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Revision;
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult UpdateData(int LineId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string Table_name = "SCP002";
                if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
                {
                    if (columnName == "ReferenceDrawing" || columnName == "ScopeOfWork")
                    {
                        columnValue = !string.IsNullOrWhiteSpace(columnValue) ? columnValue.Replace("\r\n", "|").Replace("\n", "|").Trim() : "";
                    }

                    db.SP_COMMON_LINES_UPDATE(LineId, columnName, columnValue, Table_name, objClsLoginInfo.UserName);
                    //db.SP_COMMON_LINES_UPDATE(Table_name,LineId, "EditedBy", objClsLoginInfo.UserName);
                    //ReviseData(headerId);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.LineUpdate.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_SCP_GET_HEADERDETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = GetCodeAndNameByProject(li.Project),
                                      Document = li.Document,
                                      Customer = Convert.ToString(Manager.GetCustomerCodeAndNameByProject(li.Project)),
                                      Product = Convert.ToString(li.Product),
                                      ProcessLicensor = Convert.ToString(li.ProcessLicensor),
                                      RevNo = Convert.ToString("R" + li.RevNo),
                                      Status = Convert.ToString(li.Status),
                                      SubmittedBy = Convert.ToString(li.SubmittedBy),
                                      SubmittedOn = Convert.ToString(Convert.ToDateTime(li.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA" : Convert.ToDateTime(li.SubmittedOn).ToString("dd/MM/yyyy")),
                                      ApprovedBy = Convert.ToString(li.ApprovedBy),
                                      ApprovedOn = Convert.ToString(Convert.ToDateTime(li.ApprovedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA" : Convert.ToDateTime(li.ApprovedOn).ToString("dd/MM/yyyy")),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                else
                {
                    int headerid = Convert.ToInt32(Session["Headerid"]);
                    var lst = db.SP_SCP_GET_LINEDETAILS(1, int.MaxValue, strSortOrder, headerid, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      ReferenceDrawing = li.ReferenceDrawing,
                                      TaskDescription = li.TaskDescription,
                                      ScopeOfWork = li.ScopeOfWork,
                                      TaskCategory = li.TaskCategory,
                                      TaskManager = li.TaskManager,
                                      EstFor = Convert.ToString(li.EstFor),
                                      Weight = Convert.ToString(li.Weight),
                                      SubmittedOn = Convert.ToString(Convert.ToDateTime(li.ExpMatDelDate).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA" : Convert.ToDateTime(li.ExpMatDelDate).ToString("dd/MM/yyyy")),
                                      ReqComDate = Convert.ToString(Convert.ToDateTime(li.ReqComDate).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA" : Convert.ToDateTime(li.ReqComDate).ToString("dd/MM/yyyy")),
                                      SubConTo = Convert.ToString(li.SubConTo),
                                      RCCHours = Convert.ToString(li.RCCHours),
                                      DeliverTo = Convert.ToString(li.DeliverTo)
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GenerateExcelHistroy(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_SCP_GET_HISTORY_HEADERDETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = GetCodeAndNameByProject(li.Project),
                                      Document = li.Document,
                                      Customer = Convert.ToString(Manager.GetCustomerCodeAndNameByProject(li.Project)),
                                      Product = Convert.ToString(li.Product),
                                      ProcessLicensor = Convert.ToString(li.ProcessLicensor),
                                      RevNo = Convert.ToString("R" + li.RevNo),
                                      Status = Convert.ToString(li.Status),
                                      SubmittedBy = Convert.ToString(li.SubmittedBy),
                                      SubmittedOn = Convert.ToString(Convert.ToDateTime(li.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA" : Convert.ToDateTime(li.SubmittedOn).ToString("dd/MM/yyyy")),
                                      ApprovedBy = Convert.ToString(li.ApprovedBy),
                                      ApprovedOn = Convert.ToString(Convert.ToDateTime(li.ApprovedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA" : Convert.ToDateTime(li.ApprovedOn).ToString("dd/MM/yyyy")),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                else
                {
                    int headerid = Convert.ToInt32(Session["Headerid"]);
                    var lst = db.SP_SCP_GET_HISTORY_LINEDETAILS(1, int.MaxValue, strSortOrder, headerid, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      ReferenceDrawing = li.ReferenceDrawing,
                                      TaskDescription = li.TaskDescription,
                                      ScopeOfWork = li.ScopeOfWork,
                                      TaskManager = li.TaskManager,
                                      EstFor = Convert.ToString(li.EstFor),
                                      Weight = Convert.ToString(li.Weight),
                                      SubmittedOn = Convert.ToString(Convert.ToDateTime(li.ExpMatDelDate).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA" : Convert.ToDateTime(li.ExpMatDelDate).ToString("dd/MM/yyyy")),
                                      ReqComDate = Convert.ToString(Convert.ToDateTime(li.ReqComDate).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA" : Convert.ToDateTime(li.ReqComDate).ToString("dd/MM/yyyy")),
                                      SubConTo = Convert.ToString(li.SubConTo),
                                      RCCHours = Convert.ToString(li.RCCHours),
                                      DeliverTo = Convert.ToString(li.DeliverTo)
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
    }
    public class ResponceMsgWithStatus : clsHelper.ResponseMsg
    {
        public string status;
        public string Revision;
        public string projdesc;
        public string appdesc;
        public List<clsTask> lsttask { get; set; }
    }
    public class clsTask
    {
        public string id { get; set; }
        public string text { get; set; }
    }

    //// Modification in popup to inlinr grid






}