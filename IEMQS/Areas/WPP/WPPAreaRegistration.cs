﻿using System.Web.Mvc;

namespace IEMQS.Areas.WPP
{
    public class WPPAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "WPP";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "WPP_default",
                "WPP/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}