﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Models;
using IEMQS.Areas.WPS.Models;
using IEMQS.Areas.WQ.Models;
using IEMQS.Models;
using IEMQSImplementation;
using OfficeOpenXml;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.WPP.Controllers
{
    public class MaintainWPPController : clsBase
    {
        // GET: WPP/MaintainWPP
        #region index page
        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        public ActionResult Index()
        {
            ViewBag.Title = "Maintain WPP";
            ViewBag.IndexType = clsImplementationEnum.WPPIndexType.maintain.GetStringValue();
            return View();
        }

        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        public ActionResult Approve()
        {
            ViewBag.Title = "Approve WPP";
            ViewBag.IndexType = clsImplementationEnum.WPPIndexType.approve.GetStringValue();
            return View("Index");
        }

        [SessionExpireFilter]
        //[AllowAnonymous]
        //[UserPermissions]
        public ActionResult Release()
        {
            ViewBag.Title = "Release WPP";
            ViewBag.IndexType = clsImplementationEnum.WPPIndexType.release.GetStringValue();
            return View("Index");
        }
        //load tab wise partial
        [HttpPost]
        public ActionResult LoadDataGridPartial(string status, string title, string indextype)
        {
            ViewBag.Status = status;
            ViewBag.GridTitle = title;
            ViewBag.IndexDataFor = indextype;

            return PartialView("_GetIndexGridDataPartial");
        }

        [HttpPost]
        public JsonResult LoadDataGridData(JQueryDataTableParamModel param)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhereCondition = string.Empty;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                string indextype = param.Department;
                if (string.IsNullOrWhiteSpace(indextype))
                {
                    indextype = clsImplementationEnum.WPPIndexType.maintain.GetStringValue();
                }
                strWhereCondition += " 1=1 ";
                if (param.Status.ToLower() == "pending")
                {
                    if (indextype == clsImplementationEnum.WPPIndexType.maintain.GetStringValue())
                    {
                        strWhereCondition += " and  1=1 and Status  in ( '" + clsImplementationEnum.WPPStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.WPPStatus.Returned.GetStringValue() + "')";
                    }
                    if (indextype == clsImplementationEnum.WPPIndexType.approve.GetStringValue())
                    {
                        strWhereCondition += " and  1=1 and Status  in ( '" + clsImplementationEnum.WPPStatus.SendForApprovel.GetStringValue() + "') ";
                    }
                    if (indextype == clsImplementationEnum.WPPIndexType.release.GetStringValue())
                    {
                        strWhereCondition += " and  1=1 and Status  in ( '" + clsImplementationEnum.WPPStatus.Approved.GetStringValue() + "') ";
                    }
                }
                #endregion


                //search Condition 
                string[] columnName = { "WPPNumber", "WPPRevNo", "Location", "Status", "location.t_desc", "CreatedOn", "SubmittedBy", "CreatedBy", "SubmittedOn" };
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                var lstLoc = string.Join(",", db.ATH001.Where(x => x.Employee.Trim() == objClsLoginInfo.UserName.Trim()).Select(x => x.Location).Distinct().ToList());

                strWhereCondition += Manager.MakeStringInCondition("Location", lstLoc);
                var lstResult = db.SP_WPP_HEADER_INDEX_DATA(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.HeaderId),
                            Convert.ToString(uc.WPPNumber),
                            Convert.ToString("R"+uc.WPPRevNo),
                            Convert.ToString(uc.Status),
                            Convert.ToString(uc.CreatedBy),
                            uc.CreatedOn == null || uc.CreatedOn.Value==DateTime.MinValue? " " :uc.CreatedOn.Value.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture),
                            uc.SubmittedBy,
                            uc.SubmittedOn == null || uc.SubmittedOn.Value==DateTime.MinValue? " " :uc.SubmittedOn.Value.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture),
                            Convert.ToString(uc.Location),
                            Helper.GenerateActionIcon(uc.HeaderId, "View", "View Detail", "fa fa-eye", "",WebsiteURL +"/WPP/MaintainWPP/Details/"+uc.HeaderId+"?urlForm="+ indextype ,false)
                          + (indextype == clsImplementationEnum.WPPIndexType.maintain.GetStringValue() ? Helper.GenerateActionIcon(uc.HeaderId,"Delete","Delete Record","fa fa-trash-o", "DeleteHeader("+ uc.HeaderId +");","", ( uc.WPPRevNo == 0 && (uc.Status==clsImplementationEnum.WPPStatus.Draft.GetStringValue() || uc.Status==clsImplementationEnum.WPPStatus.Returned.GetStringValue())? false : true)) : "")
                          + Helper.GenerateActionIcon(uc.HeaderId, "Print", "Print Report", "fa fa-print", (uc.JointType.ToLower() =="t#ts" || uc.JointType.ToLower() =="t#t/s")?  "PrintGeneralReport("+uc.HeaderId+",'"+ "/WPP/WPP_Report_TTS" + "')" :"PrintGeneralReport("+uc.HeaderId+",'"+ "/WPP/WPP_Report" + "')" ,"" ,false)
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult DeleteWPPHeader(int headerId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            WPP010 objWPP010 = new WPP010();
            try
            {
                if (headerId > 0)
                {
                    objWPP010 = db.WPP010.Where(i => i.HeaderId == headerId).FirstOrDefault();
                    if (objWPP010 != null)
                    {
                        int headerid = objWPP010.HeaderId;
                        int? rev = objWPP010.WPPRevNo;
                        if (string.Equals(objWPP010.Status, clsImplementationEnum.WPSHeaderStatus.Draft.GetStringValue(), StringComparison.OrdinalIgnoreCase) || string.Equals(objWPP010.Status, clsImplementationEnum.WPPStatus.Returned.GetStringValue(), StringComparison.OrdinalIgnoreCase) && objWPP010.WPPRevNo == 0)
                        {
                            if (db.WPP012.Where(i => i.HeaderId == headerId).Any())
                            {
                                db.WPP012.RemoveRange(db.WPP012.Where(i => i.HeaderId == headerId).ToList());
                            }
                            if (db.WPP013.Where(i => i.HeaderId == headerId).Any())
                            {
                                db.WPP013.RemoveRange(db.WPP013.Where(i => i.HeaderId == headerId).ToList());
                            }
                            if (db.WPP014.Where(i => i.HeaderId == headerId).Any())
                            {
                                db.WPP014.RemoveRange(db.WPP014.Where(i => i.HeaderId == headerId).ToList());
                            }
                            if (db.WPP011.Where(i => i.HeaderId == headerId).Any())
                            {
                                db.WPP011.RemoveRange(db.WPP011.Where(i => i.HeaderId == headerId).ToList());
                            }
                            db.WPP010.Remove(objWPP010);
                            db.SaveChanges();
                            string deletefolderPath = "WPP010//" + headerid + "//R" + rev;
                            //var lstDoc = (new clsFileUpload()).GetDocuments(deletefolderPath, true).Select(x => x.Name).ToList();
                            //foreach (var filename in lstDoc)
                            //{
                            //    (new clsFileUpload()).DeleteFileAsync(deletefolderPath, filename.ToString());
                            //}

                            Utility.Controllers.FileUploadController _objFUC = new Utility.Controllers.FileUploadController();
                            _objFUC.DeleteAllFileOnFCSServerAsync(headerid, deletefolderPath, deletefolderPath, DESServices.CommonService.GetUseIPConfig);

                            objResponseMsg.Key = true;
                            objResponseMsg.Value = string.Format(clsImplementationMessage.WPPHeader.Delete.ToString(), objWPP010.WPPNumber);
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = string.Format("WPP:{0} cannot be deleted.", objWPP010.WPPNumber);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region header
        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        public ActionResult Details(int? id, string urlForm = "")
        {
            WPP010 objWPP010 = new WPP010();
            NDEModels objNDEModels = new NDEModels();
            if (id > 0)
            {
                objWPP010 = db.WPP010.Where(x => x.HeaderId == id).FirstOrDefault();
                ViewBag.Location = db.COM002.Where(a => a.t_dimx == objWPP010.Location).Select(a => a.t_dimx + "-" + a.t_desc).FirstOrDefault();
                //var Files = (new clsFileUpload()).GetDocuments("WPP010/" + objWPP010.HeaderId + "/R" + objWPP010.WPPRevNo);
                Utility.Controllers.FileUploadController _objFUC = new Utility.Controllers.FileUploadController();
                var Files = _objFUC.GetAllDocumentsByTableNameTableId("WPP010//" + objWPP010.HeaderId + "//R" + objWPP010.WPPRevNo, objWPP010.HeaderId, 1);
                if (Files.Count() > 0)
                {
                    ViewBag.image = Files.FirstOrDefault().MainDocumentPath;
                }
            }
            else
            {
                objWPP010.Location = objClsLoginInfo.Location;
                objWPP010.Status = clsImplementationEnum.WPPStatus.Draft.GetStringValue();
                objWPP010.WPPRevNo = 0;
                ViewBag.image = "";

                ViewBag.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objClsLoginInfo.Location).Select(a => a.t_dimx + "-" + a.t_desc).FirstOrDefault();
            }

            ViewBag.WeldingProcess = GetSubCatagory("Welding Process", objWPP010.Location).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new { Value = i.Code, CategoryDescription = i.Code }).Distinct().ToList();
            ViewBag.WeldingProcess1Selected = objNDEModels.GetCategory(objWPP010.WeldingProcess1).Code;
            ViewBag.WeldingProcess2Selected = objNDEModels.GetCategory(objWPP010.WeldingProcess2).Code;
            ViewBag.WeldingProcess3Selected = objNDEModels.GetCategory(objWPP010.WeldingProcess3).Code;
            ViewBag.JointTypeSelected = objNDEModels.GetCategory(objWPP010.JointType).Code;

            ViewBag.ParentMetal = GetParentMaterial().Select(i => new { i.PNO1, i.DESCRIPTION }).Distinct().Select(i => new CategoryData { Value = i.PNO1, CategoryDescription = i.PNO1 + "-" + i.DESCRIPTION });
            ViewBag.TypeOfWeld = clsImplementationEnum.getTypeofweld().ToList();
            ViewBag.WEPBy = clsImplementationEnum.getWEPBy().ToList();
            var jointtype = GetSubCatagory("Joint Type", objWPP010.Location).Select(i => new { i.Code, i.Description }).Distinct();
            ViewBag.JointType = jointtype.Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + "-" + i.Description });

            string Heading = string.Empty;
            if (urlForm == clsImplementationEnum.WPPIndexType.approve.GetStringValue())
            {
                ViewBag.buttonname = "approve";
                Heading = "Approve Welding Procedure Plan";
            }
            else if (urlForm == clsImplementationEnum.WPPIndexType.release.GetStringValue())
            {
                ViewBag.buttonname = "release";
                Heading = "Release Welding Procedure Plan";
            }
            else
            {
                ViewBag.buttonname = "maintain";
                Heading = "Maintain Welding Procedure Plan";
            }
            ViewBag.Title = Heading;
            ViewBag.formRedirect = urlForm;
            return View(objWPP010);
        }
        [HttpPost]
        public ActionResult SaveHeader(WPP010 obj, FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                WPP010 objWPP010 = new WPP010();
                if (obj.HeaderId > 0)
                {
                    objWPP010 = db.WPP010.Where(o => o.HeaderId == obj.HeaderId).FirstOrDefault();
                    objWPP010.WeldDesignation = obj.WeldDesignation;
                    objWPP010.DocumentNo = obj.DocumentNo;
                    objWPP010.WeldingProcess1 = obj.WeldingProcess1;
                    objWPP010.WeldingProcess2 = obj.WeldingProcess2;
                    objWPP010.WeldingProcess3 = obj.WeldingProcess3;
                    objWPP010.WeldingType1 = obj.WeldingType1;
                    objWPP010.WeldingType2 = obj.WeldingType2;
                    objWPP010.WeldingType3 = obj.WeldingType3;
                    objWPP010.JointType = obj.JointType;
                    objWPP010.DryingofElectrode = obj.DryingofElectrode;
                    objWPP010.DryingofFlux = obj.DryingofFlux;
                    objWPP010.ItemNumber1 = obj.ItemNumber1;
                    objWPP010.ItemNumber2 = obj.ItemNumber2;
                    objWPP010.ItemNumber3 = obj.ItemNumber3;
                    objWPP010.PNoMaterial1 = obj.PNoMaterial1;
                    objWPP010.PNoMaterial2 = obj.PNoMaterial2;
                    objWPP010.PNoMaterial3 = obj.PNoMaterial3;
                    objWPP010.Specification1 = obj.Specification1;
                    objWPP010.Specification2 = obj.Specification2;
                    objWPP010.Specification3 = obj.Specification3;
                    objWPP010.Remarks1 = obj.Remarks1;
                    objWPP010.Remarks2 = obj.Remarks2;
                    objWPP010.Remarks3 = obj.Remarks3;
                    objWPP010.WEPBy = obj.WEPBy;
                    objWPP010.NatureofRevision = obj.NatureofRevision;
                    objWPP010.Remarks = obj.Remarks;
                    objWPP010.QualityProjects = obj.QualityProjects;
                    objWPP010.Notes = obj.Notes;
                    objWPP010.Status = clsImplementationEnum.WPPStatus.Draft.GetStringValue();
                    objWPP010.EditedBy = objClsLoginInfo.UserName;
                    objWPP010.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    #region code for Linked Project (removed on 24/10/2017 - as per requirements [observation no. 13507])
                    WPP013 objWPP013 = new WPP013();
                    List<WPP013> lstDesWPP013 = db.WPP013.Where(x => x.HeaderId == objWPP010.HeaderId).ToList();
                    if (lstDesWPP013 != null && lstDesWPP013.Count > 0)
                    {
                        db.WPP013.RemoveRange(lstDesWPP013);
                        db.SaveChanges();
                    }
                    string SeamNo = fc["SeamNo"];

                    string[] arraySeamNo = SeamNo.Split(',').ToArray();
                    WPP013 newobjWPP013 = db.WPP013.Where(x => x.HeaderId == objWPP010.HeaderId).FirstOrDefault();
                    List<WPP013> lstWPP013 = new List<WPP013>();
                    for (int i = 0; i < arraySeamNo.Length; i++)
                    {
                        string seam = arraySeamNo[i].Split('#')[1];
                        string proj = arraySeamNo[i].Split('#')[0];
                        WPP013 objWPP013Add = new WPP013();
                        objWPP013Add.HeaderId = objWPP010.HeaderId;
                        objWPP013Add.QualityProject = proj;
                        objWPP013Add.Seam = seam;
                        objWPP013Add.Project = new NDEModels().GetQMSProject(proj).Project;
                        objWPP013Add.BU = db.COM001.Where(o => o.t_cprj == objWPP013Add.Project).FirstOrDefault().t_entu;
                        objWPP013Add.CreatedBy = objClsLoginInfo.UserName;
                        objWPP013Add.CreatedOn = DateTime.Now;
                        objWPP013Add.Location = objWPP010.Location;
                        lstWPP013.Add(objWPP013Add);

                    }
                    if (lstWPP013 != null && lstWPP013.Count > 0)
                    {
                        db.WPP013.AddRange(lstWPP013);
                    }
                    db.SaveChanges();

                    #endregion

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update;
                    objResponseMsg.Status = objWPP010.Status;
                    objResponseMsg.HeaderId = objWPP010.HeaderId;
                    objResponseMsg.Revision = Convert.ToInt32(objWPP010.WPPRevNo);
                }
                else
                {
                    objWPP010.WPPNumber = wppnumber(obj.QualityProject, obj.Location);
                    objWPP010.Location = obj.Location;
                    objWPP010.DocumentNo = obj.DocumentNo;
                    objWPP010.WPPRevNo = 0;
                    objWPP010.WeldDesignation = obj.WeldDesignation;
                    objWPP010.WeldingProcess1 = obj.WeldingProcess1;
                    objWPP010.WeldingProcess2 = obj.WeldingProcess2;
                    objWPP010.WeldingProcess3 = obj.WeldingProcess3;
                    objWPP010.WeldingType1 = obj.WeldingType1;
                    objWPP010.WeldingType2 = obj.WeldingType2;
                    objWPP010.WeldingType3 = obj.WeldingType3;
                    objWPP010.JointType = obj.JointType;
                    objWPP010.DryingofElectrode = obj.DryingofElectrode;
                    objWPP010.DryingofFlux = obj.DryingofFlux;
                    objWPP010.ItemNumber1 = obj.ItemNumber1;
                    objWPP010.ItemNumber2 = obj.ItemNumber2;
                    objWPP010.ItemNumber3 = obj.ItemNumber3;
                    objWPP010.PNoMaterial1 = obj.PNoMaterial1;
                    objWPP010.PNoMaterial2 = obj.PNoMaterial2;
                    objWPP010.PNoMaterial3 = obj.PNoMaterial3;
                    objWPP010.Specification1 = obj.Specification1;
                    objWPP010.Specification2 = obj.Specification2;
                    objWPP010.Specification3 = obj.Specification3;
                    objWPP010.Remarks1 = obj.Remarks1;
                    objWPP010.Remarks2 = obj.Remarks2;
                    objWPP010.Remarks3 = obj.Remarks3;
                    objWPP010.WEPBy = obj.WEPBy;
                    objWPP010.NatureofRevision = obj.NatureofRevision;
                    objWPP010.Remarks = obj.Remarks;
                    objWPP010.Notes = obj.Notes;
                    objWPP010.DocNo = Convert.ToInt32(obj.WPPNumber.Split('-')[1]);
                    objWPP010.QualityProjects = obj.QualityProjects;
                    objWPP010.QualityProject = obj.QualityProject;
                    objWPP010.Status = clsImplementationEnum.WPPStatus.Draft.GetStringValue();
                    objWPP010.CreatedBy = objClsLoginInfo.UserName;
                    objWPP010.CreatedOn = DateTime.Now;
                    db.WPP010.Add(objWPP010);
                    db.SaveChanges();
                    #region code for Linked Project (removed on 24/10/2017 - as per requirements [observation no. 13507])
                    WPP013 objWPP013 = new WPP013();

                    string SeamNo = fc["SeamNo"];
                    string[] arraySeamNo = SeamNo.Split(',').ToArray();
                    WPP013 newobjWPP013 = db.WPP013.Where(x => x.HeaderId == objWPP010.HeaderId).FirstOrDefault();
                    List<WPP013> lstWPP013 = new List<WPP013>();
                    for (int i = 0; i < arraySeamNo.Length; i++)
                    {
                        string seam = arraySeamNo[i].Split('#')[1];
                        string proj = arraySeamNo[i].Split('#')[0];
                        WPP013 objWPP013Add = new WPP013();
                        objWPP013Add.HeaderId = objWPP010.HeaderId;
                        objWPP013Add.QualityProject = proj;
                        objWPP013Add.Project = new NDEModels().GetQMSProject(proj).Project;
                        objWPP013Add.BU = db.COM001.Where(o => o.t_cprj == objWPP013Add.Project).FirstOrDefault().t_entu;

                        objWPP013Add.Location = objWPP010.Location;
                        objWPP013Add.Seam = seam;
                        objWPP013Add.CreatedBy = objClsLoginInfo.UserName;
                        objWPP013Add.CreatedOn = DateTime.Now;
                        lstWPP013.Add(objWPP013Add);

                    }
                    if (lstWPP013 != null && lstWPP013.Count > 0)
                    {
                        db.WPP013.AddRange(lstWPP013);
                    }
                    db.SaveChanges();
                    //attachments

                    #endregion
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert;
                    objResponseMsg.Status = objWPP010.Status;
                    objResponseMsg.Revision = Convert.ToInt32(objWPP010.WPPRevNo);
                    objResponseMsg.HeaderId = objWPP010.HeaderId;
                }
                bool isDeleteEnabled = Convert.ToBoolean(fc["isDeleteEnabled"]);
                //NOT USED IN FCS
                //if (objResponseMsg.Key && isDeleteEnabled)
                //{
                //    //var Files = (new clsFileUpload()).GetDocuments("WPP010/" + objWPP010.HeaderId + "/R" + objWPP010.WPPRevNo);
                //    //foreach (var item in Files)
                //    //{
                //    //    (new clsFileUpload()).DeleteFile("WPP010/" + objWPP010.HeaderId + "/R" + objWPP010.WPPRevNo, item.Name);
                //    //}
                //    string deletefolderPath = "WPP010//" + objWPP010.HeaderId + "//R" + objWPP010.WPPRevNo;
                //    Utility.Controllers.FileUploadController _objFUC = new Utility.Controllers.FileUploadController();
                //    _objFUC.DeleteAllFileOnFCSServerAsync(objWPP010.HeaderId, deletefolderPath, deletefolderPath, DESServices.CommonService.GetUseIPConfig);
                //}
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        //revise header
        [HttpPost]
        public ActionResult ReviseHeader(int strHeaderId, string Remarks)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                string approved = clsImplementationEnum.WPPStatus.Approved.GetStringValue();
                string released = clsImplementationEnum.WPPStatus.Released.GetStringValue();
                WPP010 objWPP010 = db.WPP010.Where(u => u.HeaderId == strHeaderId).FirstOrDefault();
                if (objWPP010 != null)
                {
                    if (objWPP010.Status == approved || objWPP010.Status == released)
                    {
                        int? oldrev = objWPP010.WPPRevNo;

                        int? newrev = Convert.ToInt32(objWPP010.WPPRevNo) + 1;
                        objWPP010.WPPRevNo = Convert.ToInt32(objWPP010.WPPRevNo) + 1;
                        objWPP010.NatureofRevision = Remarks;
                        objWPP010.Status = clsImplementationEnum.PTMTCTQStatus.DRAFT.GetStringValue();
                        objWPP010.ApprovedOn = null;
                        objWPP010.EditedBy = objClsLoginInfo.UserName;
                        objWPP010.EditedOn = DateTime.Now;
                        objWPP010.SubmittedBy = null;
                        objWPP010.SubmittedOn = null;
                        if (db.WPP011.Where(i => i.HeaderId == objWPP010.HeaderId).Any())
                        {
                            db.WPP011.Where(i => i.HeaderId == objWPP010.HeaderId).ToList().ForEach(i => i.WPPRevNo = objWPP010.WPPRevNo);
                        }
                        db.SaveChanges();
                        #region copy documents
                        string oldfolderPath = "WPP010//" + objWPP010.HeaderId + "//R" + oldrev;
                        string newfolderPath = "WPP010//" + objWPP010.HeaderId + "//R" + newrev;
                        //(new clsFileUpload()).CopyFolderContentsAsync(oldfolderPath, newfolderPath);

                        Utility.Controllers.FileUploadController _objFUC = new Utility.Controllers.FileUploadController();
                        _objFUC.CopyDataOnFCSServerAsync(oldfolderPath, newfolderPath, oldfolderPath, objWPP010.HeaderId, newfolderPath, objWPP010.HeaderId, IEMQS.DESServices.CommonService.GetUseIPConfig, IEMQS.DESServices.CommonService.objClsLoginInfo.UserName, 0);
                        #endregion
                        objResponseMsg.Key = true;
                        objResponseMsg.HeaderId = objWPP010.HeaderId;
                        objResponseMsg.Status = objWPP010.Status;
                        objResponseMsg.Revision = Convert.ToInt32(objWPP010.WPPRevNo);
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Revision;
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "WPP Document is in '" + objWPP010.Status + "' ,You cannot revise now!";
                    }
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult showAttahcment(int headerid, int rev)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                //var Files = (new clsFileUpload()).GetDocuments("WPP010/" + headerid + "/R" + rev);
                Utility.Controllers.FileUploadController _objFUC = new Utility.Controllers.FileUploadController();
                var Files = _objFUC.GetAllDocumentsByTableNameTableId("WPP010//" + headerid + "//R" + rev, headerid, 1);
                if (Files.Count() > 0)
                {
                    ViewBag.image = Files.FirstOrDefault().MainDocumentPath;

                    objResponseMsg.dataKey = true;
                    objResponseMsg.folderpath = Files.FirstOrDefault().MainDocumentPath;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SendForApproval(int HeaderId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                WPP010 objWPP010 = db.WPP010.Where(u => u.HeaderId == HeaderId).SingleOrDefault();
                if (objWPP010 != null)
                {
                    objWPP010.Status = clsImplementationEnum.WPPStatus.SendForApprovel.GetStringValue();
                    objWPP010.SubmittedBy = objClsLoginInfo.UserName;
                    objWPP010.SubmittedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.ActionKey = true;
                    objResponseMsg.ActionValue = clsImplementationMessage.CommonMessages.sentforApprove.ToString();

                    #region Send Notification

                    #endregion
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.PLCMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        //[HttpPost]
        //public ActionResult ApproveHeader(int headerId)
        //{
        //    clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
        //    try
        //    {
        //        db.SP_WPP_HEADR_APPROVE(headerId, null, objClsLoginInfo.UserName);
        //        objResponseMsg.Key = true;
        //        objResponseMsg.Value = clsImplementationMessage.PLCMessages.Approve.ToString();
        //    }
        //    catch (Exception ex)
        //    {
        //        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        //        objResponseMsg.Key = false;
        //        objResponseMsg.Value = clsImplementationMessage.PLCMessages.Error.ToString();
        //    }
        //    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        //}

        [HttpPost]
        public ActionResult ApproveByApproverSelected(string strHeaderIds)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            string[] arrayHeaderIds = strHeaderIds.Split(',').ToArray();
            string WPPNo = string.Empty;
            try
            {
                for (int i = 0; i < arrayHeaderIds.Length; i++)
                {
                    int HeaderId = Convert.ToInt32(arrayHeaderIds[i]);
                    var objwpp010 = db.WPP010.Where(o => o.HeaderId == HeaderId).FirstOrDefault();
                    if (WPPNo.Length == 0) { WPPNo = objwpp010.WPPNumber.ToString(); }
                    else { WPPNo += "," + objwpp010.WPPNumber.ToString(); }

                    db.SP_WPP_HEADR_APPROVE(HeaderId, null, objClsLoginInfo.UserName);
                    WPP010 objPLC001 = db.WPP010.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    objResponseMsg.Key = true;
                }
                if (strHeaderIds.Count() > 1)
                {
                    objResponseMsg.Value = string.Format(clsImplementationMessage.WPPHeader.ApprovedMultiple.ToString(), WPPNo);
                }
                else
                {
                    objResponseMsg.Value = string.Format(clsImplementationMessage.WPPHeader.Approved.ToString(), WPPNo);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.PLCMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult ReleaseHeader(int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            string approved = clsImplementationEnum.WPPStatus.Approved.GetStringValue();
            WPP010 objWPP010 = db.WPP010.Where(u => u.HeaderId == headerId).SingleOrDefault();
            WPP010_Log objWPP010Log = db.WPP010_Log.Where(u => u.HeaderId == headerId && u.Status == approved).OrderByDescending(x => x.Id).FirstOrDefault();
            try
            {
                if (objWPP010 != null)
                {
                    objWPP010.Status = clsImplementationEnum.WPPStatus.Released.GetStringValue();
                    objWPP010.ReleasedBy = objClsLoginInfo.UserName;
                    objWPP010.ReleasedOn = DateTime.Now;
                    if (objWPP010Log != null)
                    {
                        objWPP010Log.Status = objWPP010.Status;
                        objWPP010Log.ReleasedBy = objWPP010.ReleasedBy;
                        objWPP010Log.ReleasedOn = objWPP010.ReleasedOn;
                    }
                    db.SaveChanges();
                    objResponseMsg.ActionKey = true;
                    objResponseMsg.ActionValue = clsImplementationMessage.WPPHeader.Released.ToString();

                    #region Send Notification

                    #endregion
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.PLCMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReleaseSelected(string strHeaderIds)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            string[] arrayHeaderIds = strHeaderIds.Split(',').ToArray();
            string WPPNo = string.Empty;
            try
            {
                for (int i = 0; i < arrayHeaderIds.Length; i++)
                {
                    int HeaderId = Convert.ToInt32(arrayHeaderIds[i]);
                    var objwpp010 = db.WPP010.Where(o => o.HeaderId == HeaderId).FirstOrDefault();
                    if (WPPNo.Length == 0) { WPPNo = objwpp010.WPPNumber.ToString(); }
                    else { WPPNo += "," + objwpp010.WPPNumber.ToString(); }
                    string approved = clsImplementationEnum.WPPStatus.Approved.GetStringValue();
                    WPP010 objWPP010 = db.WPP010.Where(u => u.HeaderId == HeaderId).SingleOrDefault();
                    WPP010_Log objWPP010Log = db.WPP010_Log.Where(u => u.HeaderId == HeaderId && u.Status == approved).OrderByDescending(x => x.Id).FirstOrDefault();

                    if (objWPP010 != null)
                    {
                        objWPP010.Status = clsImplementationEnum.WPPStatus.Released.GetStringValue();
                        objWPP010.ReleasedBy = objClsLoginInfo.UserName;
                        objWPP010.ReleasedOn = DateTime.Now;
                        if (objWPP010Log != null)
                        {
                            objWPP010Log.Status = objWPP010.Status;
                            objWPP010Log.ReleasedBy = objWPP010.ReleasedBy;
                            objWPP010Log.ReleasedOn = objWPP010.ReleasedOn;
                        }
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                    }
                    else
                    {

                    }

                }
                if (objResponseMsg.Key)
                {
                    if (strHeaderIds.Count() > 1)
                    {
                        objResponseMsg.Value = string.Format(clsImplementationMessage.WPPHeader.ReleasedMultiple.ToString(), WPPNo);
                    }
                    else
                    {
                        objResponseMsg.Value = string.Format(clsImplementationMessage.WPPHeader.Released.ToString(), WPPNo);
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.PLCMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReturnHeader(string Remarks, int? headerid)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            string returnStatus = clsImplementationEnum.WPPStatus.Returned.GetStringValue();
            try
            {
                WPP010 objWPP010 = db.WPP010.Where(u => u.HeaderId == headerid).SingleOrDefault();
                if (objWPP010 != null)
                {
                    objWPP010.Status = returnStatus;
                    objWPP010.Remarks = Remarks;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Returned.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Error.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.PLCMessages.Error.ToString();
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Lines

        [SessionExpireFilter]
        [HttpPost]
        public ActionResult GetWPPLines(int headerid, int lineId, string page)
        {
            WPP011 objWPP011 = new WPP011();
            NDEModels objNDEModels = new NDEModels();
            string BU = string.Empty;
            List<CategoryData> lstWeldingProc = new List<CategoryData>();
            WPP010 objWPP010 = db.WPP010.Where(x => x.HeaderId == headerid).FirstOrDefault();
            ViewBag.Joint = objWPP010.JointType;
            if (objWPP010 != null)
            {
                if (!string.IsNullOrWhiteSpace(objWPP010.WeldingProcess1))
                {
                    lstWeldingProc.Add(new CategoryData()
                    {
                        Value = objWPP010.WeldingProcess1,
                        CategoryDescription = objNDEModels.GetCategory(objWPP010.WeldingProcess1).Code,
                    });
                }
                if (!string.IsNullOrWhiteSpace(objWPP010.WeldingProcess2) && objWPP010.WeldingProcess1 != objWPP010.WeldingProcess2)
                {
                    lstWeldingProc.Add(new CategoryData()
                    {
                        Value = objWPP010.WeldingProcess2,
                        CategoryDescription = objNDEModels.GetCategory(objWPP010.WeldingProcess2).Code,
                    });
                }
                if (!string.IsNullOrWhiteSpace(objWPP010.WeldingProcess3) && objWPP010.WeldingProcess1 != objWPP010.WeldingProcess3 && objWPP010.WeldingProcess2 != objWPP010.WeldingProcess3)
                {
                    lstWeldingProc.Add(new CategoryData()
                    {
                        Value = objWPP010.WeldingProcess3,
                        CategoryDescription = objNDEModels.GetCategory(objWPP010.WeldingProcess3).Code,
                    });
                }
                lstWeldingProc.Add(new CategoryData()
                {
                    Value = "Back Gouging",
                    CategoryDescription = "Back Gouging",
                });

            }
            ViewBag.AWSClass = db.WPS002.Where(i => i.Location.Equals(objWPP010.Location, StringComparison.OrdinalIgnoreCase)).Select(i => new CategoryData { Value = i.Id.ToString(), CategoryDescription = i.AWSClass + "|" + i.FNumber + "|" + i.SFANo + "|" + i.ANumber + "|" + i.ConsumableSize }).ToList();
            ViewBag.WeldingPosition = GetSubCatagory("Welding Position", objWPP010.Location).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new { Value = i.Code, CategoryDescription = i.Code + "-" + i.Description }).Distinct().ToList();
            ViewBag.ShieldingGas = GetSubCatagory("Shielding Gas", objWPP010.Location).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new { Value = i.Code, CategoryDescription = i.Code + "-" + i.Description }).Distinct().ToList();
            ViewBag.page = page;
            if (lineId > 0)
            {
                objWPP011 = db.WPP011.Where(x => x.LineId == lineId).FirstOrDefault();
                ViewBag.WeldingProcessSelected = objNDEModels.GetCategory(objWPP011.WeldingProcess).Code;
                ViewBag.WeldingPositionSelected = objNDEModels.GetCategory(objWPP011.WeldingPosition).CategoryDescription;
                ViewBag.ShieldingGasSelected = objNDEModels.GetCategory(objWPP011.ShieldingGas).CategoryDescription;
                ViewBag.Action = "line edit";
            }
            else
            {
                objWPP011.HeaderId = headerid;
                ViewBag.Action = "";
            }
            if (lstWeldingProc.Count() > 0)
            {
                ViewBag.WeldProcess = lstWeldingProc.Distinct().ToList();
            }
            else
            {
                ViewBag.WeldProcess = lstWeldingProc.ToList();
            }

            ViewBag.Current = clsImplementationEnum.getCurrent().ToList();
            ViewBag.Polarity = clsImplementationEnum.getPolarity().ToList();

            return PartialView("_AddWPPLinePartial", objWPP011);
        }


        [HttpPost]
        public JsonResult LoadLinesDataGridData(JQueryDataTableParamModel param)

        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhereCondition = string.Empty;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                string indextype = param.Department;
                if (string.IsNullOrWhiteSpace(indextype))
                {
                    indextype = clsImplementationEnum.WPPIndexType.maintain.GetStringValue();
                }

                #endregion

                strWhereCondition += " 1=1 and HeaderId=" + param.Headerid;
                string page = param.Filterstatus;
                if (string.IsNullOrWhiteSpace(page))
                {
                    page = clsImplementationEnum.WPPIndexType.maintain.GetStringValue();
                }
                //search Condition 
                string[] columnName = { "WSNo", "WeldingProcess", "PQRNo", "WeldingPosition" };
                strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                var lstResult = db.SP_WPP_GET_LINES_DATA(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.ROW_NO),
                            Convert.ToString(uc.WSNo),
                            Convert.ToString(uc.WeldingProcess),
                            Convert.ToString(uc.PQRNo),
                            Convert.ToString(uc.AWSClass),
                              "<center><nobr>"
                                +(page==clsImplementationEnum.WPPIndexType.maintain.GetStringValue() ? Helper.GenerateActionIcon(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o",  "GetWPPLines("+uc.HeaderId+","+ uc.LineId +",'"+page+"');","", (param.MTStatus == clsImplementationEnum.WPPStatus.SendForApprovel.GetStringValue()||param.MTStatus == clsImplementationEnum.WPPStatus.Released.GetStringValue()  || param.MTStatus == clsImplementationEnum.WPPStatus.Approved.GetStringValue()  ? true : false)) : Helper.GenerateActionIcon(uc.LineId,"View","View Record","fa fa-pencil-square-o",  "GetWPPLines("+uc.HeaderId+","+ uc.LineId +",'"+page+"');","", ((page==clsImplementationEnum.WPPIndexType.approve.GetStringValue() && param.MTStatus != clsImplementationEnum.WPPStatus.SendForApprovel.GetStringValue()) || (page==clsImplementationEnum.WPPIndexType.release.GetStringValue() && param.MTStatus != clsImplementationEnum.WPPStatus.Approved.GetStringValue()))  ? true : false))
                                +(page==clsImplementationEnum.WPPIndexType.maintain.GetStringValue() ? Helper.GenerateActionIcon(uc.LineId,"Delete","Delete Record","fa fa-trash-o", "DeleteRecord("+ uc.LineId +");","", (param.MTStatus == clsImplementationEnum.WPPStatus.SendForApprovel.GetStringValue() ||param.MTStatus == clsImplementationEnum.WPPStatus.Released.GetStringValue()  ||  param.MTStatus == clsImplementationEnum.WPPStatus.Approved.GetStringValue() ? true : false)) : " ")
                                + "</nobr></center>"
                          }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveWPPLines(WPP011 obj, FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                WPP011 objWPP011 = new WPP011();
                var objHeader = db.WPP010.Where(o => o.HeaderId == obj.HeaderId).FirstOrDefault();
                if (obj.LineId > 0)
                {
                    objWPP011 = db.WPP011.Where(o => o.LineId == obj.LineId).FirstOrDefault();
                    objWPP011.WPPRevNo = objHeader.WPPRevNo;
                    objWPP011.WSNo = obj.WSNo;
                    objWPP011.WeldingProcess = obj.WeldingProcess;
                    objWPP011.PQRNo = obj.PQRNo;
                    objWPP011.WeldingPosition = obj.WeldingPosition;
                    objWPP011.AWSClass = obj.AWSClass;
                    objWPP011.SizeofFillerMetal = obj.SizeofFillerMetal;
                    objWPP011.BatchNo = obj.BatchNo;
                    objWPP011.WMTestNo = obj.WMTestNo;
                    objWPP011.FNumber = obj.FNumber;
                    objWPP011.ANumber = obj.ANumber;
                    objWPP011.FluxName = obj.FluxName;
                    objWPP011.Current = string.IsNullOrWhiteSpace(fc["Current1"]) ? obj.Current : (fc["Current1"]);
                    objWPP011.Polarity = string.IsNullOrWhiteSpace(fc["Polarity1"]) ? obj.Polarity : (fc["Polarity1"]);
                    objWPP011.Amperes = string.IsNullOrWhiteSpace(fc["Amperes1"]) ? obj.Amperes : (fc["Amperes1"]);
                    objWPP011.Voltage = string.IsNullOrWhiteSpace(fc["Voltage1"]) ? obj.Voltage : (fc["Voltage1"]);
                    objWPP011.ShieldingGas = obj.ShieldingGas;
                    objWPP011.FlowRate = obj.FlowRate;
                    objWPP011.TravelSpeed = string.IsNullOrWhiteSpace(fc["TravelSpeed1"]) ? obj.TravelSpeed : (fc["TravelSpeed1"]);
                    objWPP011.BeadLength = obj.BeadLength;
                    objWPP011.PreheatMin = string.IsNullOrWhiteSpace(fc["PreheatMin1"]) ? obj.PreheatMin : (fc["PreheatMin1"]);
                    objWPP011.Interpass = obj.Interpass;
                    objWPP011.NoofLayers = string.IsNullOrWhiteSpace(fc["NoofLayers1"]) ? obj.NoofLayers : (fc["NoofLayers1"]);
                    objWPP011.Electrode = obj.Electrode;
                    objWPP011.Dia = obj.Dia;
                    objWPP011.PeakCurrent = obj.PeakCurrent;
                    objWPP011.BackCurrent = obj.BackCurrent;
                    objWPP011.PrePurgeTime = obj.PrePurgeTime;
                    objWPP011.PreFusionTimeDelay = obj.PreFusionTimeDelay;
                    objWPP011.UpSlopeTime = obj.UpSlopeTime;
                    objWPP011.PeakCurrentTime = obj.PeakCurrentTime;
                    objWPP011.BackGroundCurrentTime = obj.BackGroundCurrentTime;
                    objWPP011.DownSlopeTime = obj.DownSlopeTime;
                    objWPP011.DecayCurrentTime = obj.DecayCurrentTime;
                    objWPP011.OverlapTime = obj.OverlapTime;
                    objWPP011.PostPurgeTime = obj.PostPurgeTime;
                    objWPP011.WireFeedSpeed = obj.WireFeedSpeed;
                    objWPP011.ArcLength = obj.ArcLength;
                    objWPP011.Size = obj.Size;
                    objWPP011.ArcRadius = obj.ArcRadius;
                    objWPP011.FluxBatchNo = obj.FluxBatchNo;
                    objWPP011.ManufChem = obj.ManufChem;
                    objWPP011.HeatNo = obj.HeatNo;
                    objWPP011.EditedBy = objClsLoginInfo.UserName;
                    objWPP011.EditedOn = DateTime.Now;
                    db.SaveChanges();


                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update;
                    objResponseMsg.Status = objHeader.Status;
                    objResponseMsg.Revision = Convert.ToInt32(objHeader.WPPRevNo);
                    objResponseMsg.HeaderId = objWPP011.HeaderId;
                }
                else
                {
                    objWPP011.HeaderId = objHeader.HeaderId;
                    objWPP011.Location = objHeader.Location;
                    objWPP011.WPPNumber = objHeader.WPPNumber;
                    objWPP011.WPPRevNo = objHeader.WPPRevNo;
                    objWPP011.WSNo = obj.WSNo;
                    objWPP011.FluxBatchNo = obj.FluxBatchNo;
                    objWPP011.WeldingProcess = obj.WeldingProcess;
                    objWPP011.PQRNo = obj.PQRNo;
                    objWPP011.WeldingPosition = obj.WeldingPosition;
                    objWPP011.AWSClass = obj.AWSClass;
                    objWPP011.SizeofFillerMetal = obj.SizeofFillerMetal;
                    objWPP011.BatchNo = obj.BatchNo;
                    objWPP011.WMTestNo = obj.WMTestNo;
                    objWPP011.FNumber = obj.FNumber;
                    objWPP011.ANumber = obj.ANumber;
                    objWPP011.FluxName = obj.FluxName;
                    objWPP011.Current = string.IsNullOrWhiteSpace(fc["Current1"]) ? obj.Current : (fc["Current1"]);
                    objWPP011.Polarity = string.IsNullOrWhiteSpace(fc["Polarity1"]) ? obj.Polarity : (fc["Polarity1"]);
                    objWPP011.Amperes = string.IsNullOrWhiteSpace(fc["Amperes1"]) ? obj.Amperes : (fc["Amperes1"]);
                    objWPP011.Voltage = string.IsNullOrWhiteSpace(fc["Voltage1"]) ? obj.Voltage : (fc["Voltage1"]);
                    objWPP011.ShieldingGas = obj.ShieldingGas;
                    objWPP011.FlowRate = obj.FlowRate;
                    objWPP011.TravelSpeed = string.IsNullOrWhiteSpace(fc["TravelSpeed1"]) ? obj.TravelSpeed : (fc["TravelSpeed1"]);
                    objWPP011.BeadLength = obj.BeadLength;
                    objWPP011.PreheatMin = string.IsNullOrWhiteSpace(fc["PreheatMin1"]) ? obj.PreheatMin : (fc["PreheatMin1"]);
                    objWPP011.Interpass = obj.Interpass;
                    objWPP011.NoofLayers = string.IsNullOrWhiteSpace(fc["NoofLayers1"]) ? obj.NoofLayers : (fc["NoofLayers1"]);
                    objWPP011.Electrode = obj.Electrode;
                    objWPP011.Dia = obj.Dia;
                    objWPP011.PeakCurrent = obj.PeakCurrent;
                    objWPP011.BackCurrent = obj.BackCurrent;
                    objWPP011.PrePurgeTime = obj.PrePurgeTime;
                    objWPP011.PreFusionTimeDelay = obj.PreFusionTimeDelay;
                    objWPP011.UpSlopeTime = obj.UpSlopeTime;
                    objWPP011.PeakCurrentTime = obj.PeakCurrentTime;
                    objWPP011.BackGroundCurrentTime = obj.BackGroundCurrentTime;
                    objWPP011.DownSlopeTime = obj.DownSlopeTime;
                    objWPP011.DecayCurrentTime = obj.DecayCurrentTime;
                    objWPP011.OverlapTime = obj.OverlapTime;
                    objWPP011.PostPurgeTime = obj.PostPurgeTime;
                    objWPP011.WireFeedSpeed = obj.WireFeedSpeed;
                    objWPP011.ArcLength = obj.ArcLength;
                    objWPP011.Size = obj.Size;
                    objWPP011.ArcRadius = obj.ArcRadius;
                    objWPP011.FluxBatchNo = obj.FluxBatchNo;
                    objWPP011.ManufChem = obj.ManufChem;
                    objWPP011.HeatNo = obj.HeatNo;
                    objWPP011.CreatedBy = objClsLoginInfo.UserName;
                    objWPP011.CreatedOn = DateTime.Now;
                    db.WPP011.Add(objWPP011);
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert;
                    objResponseMsg.Status = objHeader.Status;
                    objResponseMsg.Revision = Convert.ToInt32(objHeader.WPPRevNo);
                    objResponseMsg.HeaderId = objHeader.HeaderId;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteLine(int lineid)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {

                WPP011 objWPP011 = db.WPP011.Where(x => x.LineId == lineid).FirstOrDefault();
                if (objWPP011 != null)
                {
                    db.WPP011.Remove(objWPP011);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Details successfully deleted.";
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Details not available for delete.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region History
        [HttpPost]
        public ActionResult GetHistoryDetails(int Id, string type) //partial for CTQ History Lines Details
        {
            WPP010_Log objLog = new WPP010_Log();
            ViewBag.formRedirect = type;
            objLog = db.WPP010_Log.Where(x => x.HeaderId == Id).FirstOrDefault();
            return PartialView("_GetHistoryGridDataPartial", objLog);
        }

        public ActionResult ViewLogDetails(int? Id, string type)
        {
            WPP010_Log objWPP010 = new WPP010_Log();
            NDEModels objNDEModels = new NDEModels();
            if (Id > 0)
            {
                objWPP010 = db.WPP010_Log.Where(i => i.Id == Id).FirstOrDefault();
                //var Files = (new clsFileUpload()).GetDocuments("WPP010/" + objWPP010.HeaderId + "/R" + objWPP010.WPPRevNo);
                Utility.Controllers.FileUploadController _objFUC = new Utility.Controllers.FileUploadController();
                var Files = _objFUC.GetAllDocumentsByTableNameTableId("WPP010//" + objWPP010.HeaderId + "//R" + objWPP010.WPPRevNo, objWPP010.HeaderId, 1);
                if (Files.Count() > 0)
                {
                    ViewBag.image = Files.FirstOrDefault().MainDocumentPath;
                }
                ViewBag.Location = db.COM002.Where(a => a.t_dimx == objWPP010.Location).Select(a => a.t_dimx + "-" + a.t_desc).FirstOrDefault();
                ViewBag.WeldingProcess1Selected = objNDEModels.GetCategory(objWPP010.WeldingProcess1).Code;
                ViewBag.WeldingProcess2Selected = objNDEModels.GetCategory(objWPP010.WeldingProcess2).Code;
                ViewBag.WeldingProcess3Selected = objNDEModels.GetCategory(objWPP010.WeldingProcess3).Code;
                ViewBag.JointTypeSelected = objNDEModels.GetCategory(objWPP010.JointType).Code;
                ViewBag.Action = "edit";
                ViewBag.type = type;
            }
            return View(objWPP010);
        }

        [SessionExpireFilter]
        [HttpPost]
        public ActionResult GetWPPLogLines(int headerid, int lineId, string page)
        {
            WPP011_Log objWPP011 = new WPP011_Log();
            NDEModels objNDEModels = new NDEModels();
            string BU = string.Empty;
            List<CategoryData> lstWeldingProc = new List<CategoryData>();
            WPP010_Log objWPP010 = db.WPP010_Log.Where(x => x.Id == headerid).FirstOrDefault();
            ViewBag.Joint = objWPP010.JointType;
            if (objWPP010 != null)
            {
                if (!string.IsNullOrWhiteSpace(objWPP010.WeldingProcess1))
                {
                    lstWeldingProc.Add(new CategoryData()
                    {
                        Value = objWPP010.WeldingProcess1,
                        CategoryDescription = objNDEModels.GetCategory(objWPP010.WeldingProcess1).Code,
                    });
                }
                if (!string.IsNullOrWhiteSpace(objWPP010.WeldingProcess2) && objWPP010.WeldingProcess1 != objWPP010.WeldingProcess2)
                {
                    lstWeldingProc.Add(new CategoryData()
                    {
                        Value = objWPP010.WeldingProcess2,
                        CategoryDescription = objNDEModels.GetCategory(objWPP010.WeldingProcess2).Code,
                    });
                }
                if (!string.IsNullOrWhiteSpace(objWPP010.WeldingProcess3) && objWPP010.WeldingProcess1 != objWPP010.WeldingProcess3 && objWPP010.WeldingProcess2 != objWPP010.WeldingProcess3)
                {
                    lstWeldingProc.Add(new CategoryData()
                    {
                        Value = objWPP010.WeldingProcess3,
                        CategoryDescription = objNDEModels.GetCategory(objWPP010.WeldingProcess3).Code,
                    });
                }
            }
            ViewBag.AWSClass = db.WPS002.Where(i => i.Location.Equals(objWPP010.Location, StringComparison.OrdinalIgnoreCase)).Select(i => new CategoryData { Value = i.Id.ToString(), CategoryDescription = i.AWSClass + "|" + i.FNumber + "|" + i.SFANo + "|" + i.ANumber + "|" + i.ConsumableSize }).ToList();
            ViewBag.WeldingPosition = GetSubCatagory("Welding Position", objWPP010.Location).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new { Value = i.Code, CategoryDescription = i.Code + "-" + i.Description }).Distinct().ToList();
            ViewBag.ShieldingGas = GetSubCatagory("Shielding Gas", objWPP010.Location).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new { Value = i.Code, CategoryDescription = i.Code + "-" + i.Description }).Distinct().ToList();
            ViewBag.page = page;
            if (lineId > 0)
            {
                objWPP011 = db.WPP011_Log.Where(x => x.Id == lineId).FirstOrDefault();
                ViewBag.WeldingProcessSelected = objNDEModels.GetCategory(objWPP011.WeldingProcess).Code;
                ViewBag.WeldingPositionSelected = objNDEModels.GetCategory(objWPP011.WeldingPosition).CategoryDescription;
                ViewBag.ShieldingGasSelected = objNDEModels.GetCategory(objWPP011.ShieldingGas).CategoryDescription;
                ViewBag.Action = "line edit";
            }
            else
            {
                objWPP011.HeaderId = headerid;
                ViewBag.Action = "";
            }
            if (lstWeldingProc.Count() > 0)
            {
                ViewBag.WeldProcess = lstWeldingProc.Distinct().ToList();
            }
            else
            {
                ViewBag.WeldProcess = lstWeldingProc.ToList();
            }

            ViewBag.Current = clsImplementationEnum.getCurrent().ToList();
            ViewBag.Polarity = clsImplementationEnum.getPolarity().ToList();

            return PartialView("_GetLineGridDataPartial", objWPP011);
        }

        [HttpPost]
        public JsonResult LoadHistoryHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhereCondition = string.Empty;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                string indextype = param.Department;
                strWhereCondition += " 1=1 and HeaderId=" + param.Headerid;
                #endregion


                //search Condition 
                string[] columnName = { "WPPNumber", "WPPRevNo", "Location", "Status", "location.t_desc" };
                strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                var lstLoc = string.Join(",", db.ATH001.Where(x => x.Employee.Trim() == objClsLoginInfo.UserName.Trim()).Select(x => x.Location).Distinct().ToList());

                strWhereCondition += Manager.MakeStringInCondition("Location", lstLoc);
                var lstResult = db.SP_WPP_HEADER_HISTORY_INDEX_DATA(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.WPPNumber),
                            Convert.ToString("R"+uc.WPPRevNo),
                            Convert.ToString(uc.Status),
                            Convert.ToString(uc.CreatedBy),
                            uc.CreatedOn == null || uc.CreatedOn.Value==DateTime.MinValue? " " :uc.CreatedOn.Value.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture),
                            uc.SubmittedBy,
                            uc.SubmittedOn == null || uc.SubmittedOn.Value==DateTime.MinValue? " " :uc.SubmittedOn.Value.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture),
                            Convert.ToString(uc.Location),
                            Helper.GenerateActionIcon(uc.HeaderId, "View", "View Detail", "fa fa-eye", "",WebsiteURL +"/WPP/MaintainWPP/ViewLogDetails/"+uc.Id ,false)
                          }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult LoadHistoryLinesDataGridData(JQueryDataTableParamModel param)

        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhereCondition = string.Empty;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                string indextype = param.Department;
                if (string.IsNullOrWhiteSpace(indextype))
                {
                    indextype = clsImplementationEnum.WPPIndexType.maintain.GetStringValue();
                }

                #endregion

                strWhereCondition += " 1=1 and RefId = " + param.Headerid;
                //search Condition 
                string[] columnName = { "WSNo", "WeldingProcess", "PQRNo", "WeldingPosition" };
                strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                var lstResult = db.SP_WPP_HISTORY_LINES_DATA(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.ROW_NO),
                            Convert.ToString(uc.WSNo),
                            Convert.ToString(uc.WeldingProcess),
                            Convert.ToString(uc.PQRNo),
                            Convert.ToString(uc.AWSClass),
                              "<center><nobr>"
                                + Helper.GenerateActionIcon(uc.LineId,"View","View Record","fa fa-pencil-square-o",  "GetWPPLogLines("+uc.Id+","+ uc.RefId +",'approve');","", false )
                                + "</nobr></center>"
                          }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetLogWeldposition(string param)
        {
            try
            {
                int HeaderId = Convert.ToInt32(param);
                WPP010_Log objWPP010 = db.WPP010_Log.Where(x => x.Id == HeaderId).FirstOrDefault();

                var WeldingPosition = GetSubCatagory("Welding Position", objWPP010.Location).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new { Value = i.Code, CategoryDescription = i.Code + "-" + i.Description }).Distinct().ToList();

                var lstWeldingPosition = (from li in WeldingPosition
                                          select new { id = li.Value, text = li.CategoryDescription }).ToList();

                return Json(lstWeldingPosition, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetLogSelectedSeamValue(int headerId)
        {
            var result = db.WPP013_Log.Where(s => s.RefId == headerId).Select(s => s.QualityProject + "#" + s.Seam).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetLogSelectedNotesValue(int headerId)
        {
            var result = db.WPP014_Log.Where(s => s.RefId == headerId).Select(s => s.QualityProject + "#" + s.NoteId).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Copy WPS Number
        [HttpPost]
        public ActionResult CopyWPPNumberPartial(string location, int HeaderId, string wppnumber)
        {
            ViewBag.WPPNumber = wppnumber;
            return PartialView("_CopyWPPNumberPartial");
        }

        [HttpPost]
        public ActionResult CopyToDestination(int headerId, string destProject)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            WPP010 objSrcWPP010 = new WPP010();
            WPP010 objDestWPP010 = new WPP010();

            try
            {
                string currentUser = objClsLoginInfo.UserName;
                int? rev = 0;
                var currentLoc = objClsLoginInfo.Location;
                if (headerId > 0)
                {
                    objSrcWPP010 = db.WPP010.Where(i => i.HeaderId == headerId).FirstOrDefault();
                    rev = objSrcWPP010.WPPRevNo;
                }

                if (!string.IsNullOrEmpty(destProject))
                {
                    #region Copy Header
                    objDestWPP010 = objSrcWPP010;
                    objDestWPP010.Location = currentLoc;
                    objDestWPP010.WPPNumber = wppnumber(destProject, currentLoc);
                    objDestWPP010.DocNo = Convert.ToInt32(objDestWPP010.WPPNumber.Split('-')[1]);
                    objDestWPP010.WPPRevNo = 0;
                    if (!string.IsNullOrWhiteSpace(objDestWPP010.QualityProjects))
                    {
                        string qprojects = objDestWPP010.QualityProjects;
                        var lstQualityProjects = objDestWPP010.QualityProjects.Split(',').ToList();
                        if (!lstQualityProjects.Any(i => i.ToString() == destProject))
                        {
                            objDestWPP010.QualityProjects = destProject + "," + qprojects;
                        }
                    }
                    objDestWPP010.QualityProject = destProject;
                    objDestWPP010.Status = clsImplementationEnum.WPSHeaderStatus.Draft.GetStringValue();
                    objDestWPP010.CreatedBy = objClsLoginInfo.UserName;
                    objDestWPP010.CreatedOn = DateTime.Now;
                    objDestWPP010.ApprovedBy = null;
                    objDestWPP010.ApprovedOn = null;
                    objDestWPP010.SubmittedBy = null;
                    objDestWPP010.SubmittedOn = null;
                    objDestWPP010.EditedBy = null;
                    objDestWPP010.EditedOn = null;
                    db.WPP010.Add(objDestWPP010);
                    db.SaveChanges();
                    #region copy documents
                    string srcfolderPath = "WPP010//" + headerId + "//R" + rev;
                    string destfolderPath = "WPP010//" + objDestWPP010.HeaderId + "//R" + objDestWPP010.WPPRevNo;
                    //(new clsFileUpload()).CopyFolderContentsAsync(srcfolderPath, destfolderPath);
                    Utility.Controllers.FileUploadController _objFUC = new Utility.Controllers.FileUploadController();
                    _objFUC.CopyDataOnFCSServerAsync(srcfolderPath, destfolderPath, srcfolderPath, headerId, destfolderPath, objDestWPP010.HeaderId, IEMQS.DESServices.CommonService.GetUseIPConfig, IEMQS.DESServices.CommonService.objClsLoginInfo.UserName, 0);
                    #endregion
                    #endregion

                    objResponseMsg = InsertOrUpdateWPPNumber(headerId, objDestWPP010, false);
                    objResponseMsg.HeaderId = objDestWPP010.HeaderId;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public clsHelper.ResponseMsgWithStatus InsertOrUpdateWPPNumber(int srcheaderid, WPP010 destWPP010, bool isUpdate)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                #region Lines
                List<WPP011> lstSrcWPP011 = db.WPP011.Where(x => x.HeaderId == srcheaderid).ToList();
                List<WPP011> lstDestWPP011 = new List<WPP011>();

                foreach (var obj in lstSrcWPP011)
                {
                    WPP011 objWPP011Add = db.WPP011.Where(x => x.LineId == obj.LineId).FirstOrDefault();
                    objWPP011Add.HeaderId = destWPP010.HeaderId;
                    objWPP011Add.Location = destWPP010.Location;
                    objWPP011Add.WPPNumber = destWPP010.WPPNumber;
                    objWPP011Add.WPPRevNo = destWPP010.WPPRevNo;
                    objWPP011Add.CreatedBy = objClsLoginInfo.UserName;
                    objWPP011Add.CreatedOn = DateTime.Now;
                    objWPP011Add.EditedBy = null;
                    objWPP011Add.EditedOn = null;
                    lstDestWPP011.Add(objWPP011Add);
                }
                if (lstDestWPP011 != null && lstDestWPP011.Count > 0)
                {
                    db.WPP011.AddRange(lstDestWPP011);
                }
                db.SaveChanges();
                #endregion

                #region copy code for SeamNo and notes
                List<WPP013> lstSrcWPP013 = db.WPP013.Where(x => x.HeaderId == srcheaderid).ToList();
                List<WPP013> lstDestWPP013 = new List<WPP013>();
                foreach (var obj in lstSrcWPP013)
                {
                    WPP013 objWPP013Add = db.WPP013.Where(x => x.LineId == obj.LineId).FirstOrDefault();
                    objWPP013Add.HeaderId = destWPP010.HeaderId;
                    objWPP013Add.Location = destWPP010.Location;
                    objWPP013Add.CreatedBy = objClsLoginInfo.UserName;
                    objWPP013Add.CreatedOn = DateTime.Now;
                    lstDestWPP013.Add(objWPP013Add);
                }
                if (lstDestWPP013 != null && lstDestWPP013.Count > 0)
                {
                    db.WPP013.AddRange(lstDestWPP013);
                }

                List<WPP014> lstSrcWPP014 = db.WPP014.Where(x => x.HeaderId == srcheaderid).ToList();
                List<WPP014> lstDestWPP014 = new List<WPP014>();
                foreach (var obj in lstSrcWPP014)
                {
                    WPP014 objWPP014Add = db.WPP014.Where(x => x.LineId == obj.LineId).FirstOrDefault();
                    objWPP014Add.HeaderId = destWPP010.HeaderId;
                    objWPP014Add.Location = destWPP010.Location;
                    objWPP014Add.CreatedBy = objClsLoginInfo.UserName;
                    objWPP014Add.CreatedOn = DateTime.Now;
                    lstDestWPP014.Add(objWPP014Add);
                }
                if (lstDestWPP014 != null && lstDestWPP014.Count > 0)
                {
                    db.WPP014.AddRange(lstDestWPP014);
                }
                db.SaveChanges();
                #endregion

                objResponseMsg.Key = true;
                objResponseMsg.Value = string.Format("Data Copied to WPP Number :{0} Successfully", destWPP010.WPPNumber);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return objResponseMsg;
        }
        #endregion

        #region Common Utility
        //common functions
        [HttpPost]
        public ActionResult GenerateWPPNumber(string qproject, string location)
        {
            ModelWPS objModelWPP = new ModelWPS();
            NDEModels objNDEModels = new NDEModels();

            if (!string.IsNullOrEmpty(qproject))
            {
                objModelWPP.ProjectNo = wppnumber(qproject, location);
            }

            return Json(objModelWPP, JsonRequestBehavior.AllowGet);
        }
        public string wppnumber(string qproject, string location)
        {
            ModelWPS objModelWPP = new ModelWPS();
            string wppnum = string.Empty;
            int doc = 1;
            if (!string.IsNullOrEmpty(qproject))
            {
                var maxDoc = db.WPP010.Where(i => i.Location.Trim().Equals(location.Trim(), StringComparison.OrdinalIgnoreCase)
                                                     && i.QualityProject.Trim().ToLower().Contains(qproject.Trim().ToLower())).OrderByDescending(x => x.DocNo).FirstOrDefault();
                if (maxDoc != null)
                {
                    doc = Convert.ToInt32(maxDoc.DocNo) + 1;
                    wppnum = qproject + "-" + doc.ToString("D3");
                }
                else
                {
                    wppnum = qproject + "-" + doc.ToString("D3");
                }
            }
            return wppnum;
        }
        //fetch values from seam list
        [HttpPost]
        public ActionResult FetchSeamListData(string seamno)
        {
            clsHelper.ResponseMsgWithStatus objModelWPP = new clsHelper.ResponseMsgWithStatus();
            string project = seamno.Split('#')[0].Trim();
            string seam = seamno.Split('#')[1].Trim();
            objModelWPP = SeamData(project, seam);
            //var objSeamData = (from q12 in db.QMS012_Log
            //                   join q11 in db.QMS011_Log on q12.HeaderId equals q11.HeaderId
            //                   where q11.QualityProject == project.Trim() && q12.SeamNo == seam
            //                   select q12).FirstOrDefault();
            //if (objSeamData != null)
            //{
            //    objModelWPP.WeldDesignation = objSeamData.WeldType;
            //    if (!string.IsNullOrWhiteSpace(objSeamData.Position))
            //    {
            //        var lstPosition = objSeamData.Position.Split(',');
            //        objModelWPP.ItemNumber1 = (lstPosition.Count() > 0) ? objSeamData.Position.Split(',')[0] : "";
            //        objModelWPP.ItemNumber2 = (lstPosition.Count() > 1) ? objSeamData.Position.Split(',')[1] : "";
            //        objModelWPP.ItemNumber3 = (lstPosition.Count() > 2) ? objSeamData.Position.Split(',')[2] : "";
            //    }
            //    if (!string.IsNullOrWhiteSpace(objSeamData.Material))
            //    {
            //        var lstMaterial = objSeamData.Material.Split(',');
            //        objModelWPP.Specification1 = (lstMaterial.Count() > 0) ? objSeamData.Material.Split(',')[0] : "";
            //        objModelWPP.Specification2 = (lstMaterial.Count() > 1) ? objSeamData.Material.Split(',')[1] : "";
            //        objModelWPP.Specification3 = (lstMaterial.Count() > 2) ? objSeamData.Material.Split(',')[2] : "";
            //        if (lstMaterial.Count() > 0)
            //        {
            //            string pno = @"select top 1 PARTDETAILS.t_dscb  from " + LNLinkedServer + ".dbo.ttcibd001175 as PARTDETAILS INNER JOIN " + LNLinkedServer + ".dbo.tltibd901175 as MATERIAL ON MATERIAL.t_mtrl = PARTDETAILS.t_dscb  where MATERIAL.t_desc  ='" + objModelWPP.Specification1 + "'";
            //            objModelWPP.PNumber1 = !string.IsNullOrWhiteSpace(objModelWPP.Specification1) ? db.Database.SqlQuery<string>(@"select top 1 PARTDETAILS.t_dscb  from " + LNLinkedServer + ".dbo.ttcibd001175 as PARTDETAILS INNER JOIN " + LNLinkedServer + ".dbo.tltibd901175 as MATERIAL ON MATERIAL.t_mtrl = PARTDETAILS.t_dscb  where MATERIAL.t_desc  ='" + objModelWPP.Specification1 + "'").FirstOrDefault() : "";
            //            objModelWPP.PNumber2 = !string.IsNullOrWhiteSpace(objModelWPP.Specification2) ? db.Database.SqlQuery<string>(@"select top 1 PARTDETAILS.t_dscb  from " + LNLinkedServer + ".dbo.ttcibd001175 as PARTDETAILS INNER JOIN " + LNLinkedServer + ".dbo.tltibd901175 as MATERIAL ON MATERIAL.t_mtrl = PARTDETAILS.t_dscb  where MATERIAL.t_desc  ='" + objModelWPP.Specification2 + "'").FirstOrDefault() : "";
            //            objModelWPP.PNumber3 = !string.IsNullOrWhiteSpace(objModelWPP.Specification3) ? db.Database.SqlQuery<string>(@"select top 1 PARTDETAILS.t_dscb  from " + LNLinkedServer + ".dbo.ttcibd001175 as PARTDETAILS INNER JOIN " + LNLinkedServer + ".dbo.tltibd901175 as MATERIAL ON MATERIAL.t_mtrl = PARTDETAILS.t_dscb  where MATERIAL.t_desc  ='" + objModelWPP.Specification3 + "'").FirstOrDefault() : "";
            //        }
            //    }
            //}
            return Json(objModelWPP, JsonRequestBehavior.AllowGet);
        }
        //fetch values from filler metal master
        [HttpPost]
        public ActionResult GetAWSClassDetail(string id, string location)
        {
            WPS002 objWPS002 = new WPS002();
            ModelWPSLines objModelWPSLines = new ModelWPSLines();

            if (!string.IsNullOrEmpty(id) && !string.IsNullOrEmpty(location))
            {

                int AWSClassId = Convert.ToInt32(id);
                objWPS002 = db.WPS002.Where(i => i.Id == AWSClassId).FirstOrDefault();
                if (objWPS002 != null)
                {
                    objModelWPSLines.ANo = objWPS002.ANumber;
                    objModelWPSLines.SizeOfFillerMetal = objWPS002.ConsumableSize;
                    objModelWPSLines.SFANo = objWPS002.SFANo;
                    objModelWPSLines.FNo = objWPS002.FNumber;
                    objModelWPSLines.Current = objWPS002.CurrentAMP;
                    objModelWPSLines.TravelSpeed = objWPS002.TravelSpeed;
                    objModelWPSLines.Voltage = objWPS002.Voltage;
                    objModelWPSLines.BeadLength = objWPS002.BeadLength;
                    objModelWPSLines.HeatInput = objWPS002.HeatInput;
                }

                var lstFillerMetalTradeName = db.WPS001.Where(i => i.AWSClass.ToLower().Equals(objWPS002.AWSClass.ToLower()) && i.Location.Equals(location)).Select(i => new CategoryData { Value = i.BrandName, CategoryDescription = i.BrandName }).ToList();
                if (lstFillerMetalTradeName.Any())
                    objModelWPSLines.FillerMetalTradeName = lstFillerMetalTradeName;

                var lstFluxTradeName = db.WPS001.Where(i => i.Location.Equals(location) && i.Producttype.ToLower().Equals("flux")).Select(i => new CategoryData { Value = i.BrandName, CategoryDescription = i.BrandName }).ToList();
                if (lstFluxTradeName.Any())
                    objModelWPSLines.FluxTradeName = lstFluxTradeName;

            }

            return Json(objModelWPSLines, JsonRequestBehavior.AllowGet);
        }
        //not in use
        public List<WPS005> GetParentMaterial()
        {
            List<WPS005> lstWPS005 = (from wps in db.WPS005
                                      select wps).ToList();
            return lstWPS005;
        }
        //not in use
        public string GetParentMaterialDesc(string code)
        {
            string parentmaterial = (from wps in db.WPS005
                                     where wps.PNO1.Trim() == code.Trim()
                                     select wps.PNO1 + "-" + wps.DESCRIPTION).FirstOrDefault();
            if (string.IsNullOrWhiteSpace(parentmaterial))
            {
                parentmaterial = code;
            }
            return parentmaterial;
        }

        //select 2 functions
        public JsonResult GetProjects(string search, string location)
        {
            try
            {
                var lstProjectByUser = Manager.getProjectsByUser(objClsLoginInfo.UserName.Trim()).Distinct().ToList();
                var item = (from lst in lstProjectByUser
                            join qms10 in db.QMS010 on lst.projectCode equals qms10.Project
                            join qms01 in db.QMS001 on lst.projectCode equals qms01.Project
                            where qms01.Location.Trim().ToLower() == location.Trim().ToLower()
                                    && qms01.IsLTFPS == true
                            select new CategoryData { id = qms10.QualityProject, text = qms10.QualityProject }).ToList().Select(i => new { id = i.id, text = i.text }).Distinct().ToList();

                return Json(item, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetSeamNo(string search, string qualityproj)
        {
            try
            {
                string[] arr = qualityproj.Split(',').ToArray();
                var item = (from q12 in db.QMS012_Log
                            join q11 in db.QMS011_Log on q12.HeaderId equals q11.HeaderId
                            where arr.Contains(q11.QualityProject.Trim())
                            select new { id = q11.QualityProject + "#" + q12.SeamNo, text = q11.QualityProject + "-" + q12.SeamNo, project = q11.QualityProject }).Distinct().ToList();

                return Json(item, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetSWPNotes(string search, string qualityproj, string location)
        {
            try
            {
                string[] arr = qualityproj.Split(',').ToArray();
                var item = (from a in db.SWP002
                            where arr.Contains(a.QualityProject.Trim()) && a.Location.Trim() == location.Trim()
                            select new
                            {
                                id = a.QualityProject + "#" + a.LineId.ToString(),
                                text = a.QualityProject + "-" + a.NoteDescription,
                                project = a.QualityProject
                            }).Distinct().ToList();

                return Json(item, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }
        public List<GLB002> GetSubCatagory(string Key, string strLoc)
        {
            List<GLB002> lstGLB002 = (from glb002 in db.GLB002
                                      join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                                      where glb001.Category.Equals(Key, StringComparison.OrdinalIgnoreCase) && glb002.IsActive == true && strLoc.Equals(glb002.Location)
                                      select glb002).ToList();
            //var category = lstGLB002.Select(i => new { i.Code, i.Description }).Distinct().ToList();
            return lstGLB002;
        }

        //get selected values for select 2
        public JsonResult GetWeldposition(string param)
        {
            try
            {
                int HeaderId = Convert.ToInt32(param);
                WPP010 objWPP010 = db.WPP010.Where(x => x.HeaderId == HeaderId).FirstOrDefault();

                var WeldingPosition = GetSubCatagory("Welding Position", objWPP010.Location).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new { Value = i.Code, CategoryDescription = i.Code + "-" + i.Description }).Distinct().ToList();

                var lstWeldingPosition = (from li in WeldingPosition
                                          select new { id = li.Value, text = li.Value }).ToList();

                return Json(lstWeldingPosition, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetSelectedSeamValue(int headerId)
        {
            var result = db.WPP013.Where(s => s.HeaderId == headerId).Select(s => s.QualityProject + "#" + s.Seam).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetSelectedNotesValue(int headerId)
        {
            var result = db.WPP014.Where(s => s.HeaderId == headerId).Select(s => s.QualityProject + "#" + s.NoteId).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public clsHelper.ResponseMsgWithStatus SeamData(string project, string seamno)
        {
            clsHelper.ResponseMsgWithStatus objModelWPP = new clsHelper.ResponseMsgWithStatus();

            var objSeamData = (from q12 in db.QMS012_Log
                               join q11 in db.QMS011_Log on q12.HeaderId equals q11.HeaderId
                               where q11.QualityProject == project.Trim() && q12.SeamNo == seamno
                               select q12).FirstOrDefault();
            if (objSeamData != null)
            {
                objModelWPP.WeldDesignation = objSeamData.WeldType;
                if (!string.IsNullOrWhiteSpace(objSeamData.Position))
                {
                    var lstPosition = objSeamData.Position.Split(',');
                    objModelWPP.ItemNumber1 = (lstPosition.Count() > 0) ? objSeamData.Position.Split(',')[0] : "";
                    objModelWPP.ItemNumber2 = (lstPosition.Count() > 1) ? objSeamData.Position.Split(',')[1] : "";
                    objModelWPP.ItemNumber3 = (lstPosition.Count() > 2) ? objSeamData.Position.Split(',')[2] : "";
                }
                if (!string.IsNullOrWhiteSpace(objSeamData.Material))
                {
                    var lstMaterial = objSeamData.Material.Split(',');
                    objModelWPP.Specification1 = (lstMaterial.Count() > 0) ? objSeamData.Material.Split(',')[0] : "";
                    objModelWPP.Specification2 = (lstMaterial.Count() > 1) ? objSeamData.Material.Split(',')[1] : "";
                    objModelWPP.Specification3 = (lstMaterial.Count() > 2) ? objSeamData.Material.Split(',')[2] : "";
                    //if (lstMaterial.Count() > 0)
                    //{
                    //    string pno = @"select top 1 PARTDETAILS.t_dscb  from " + LNLinkedServer + ".dbo.ttcibd001175 as PARTDETAILS INNER JOIN " + LNLinkedServer + ".dbo.tltibd901175 as MATERIAL ON MATERIAL.t_mtrl = PARTDETAILS.t_dscb  where MATERIAL.t_desc  ='" + objModelWPP.Specification1 + "'";
                    //    objModelWPP.PNumber1 = !string.IsNullOrWhiteSpace(objModelWPP.Specification1) ? db.Database.SqlQuery<string>(@"select top 1 PARTDETAILS.t_dscb  from " + LNLinkedServer + ".dbo.ttcibd001175 as PARTDETAILS INNER JOIN " + LNLinkedServer + ".dbo.tltibd901175 as MATERIAL ON MATERIAL.t_mtrl = PARTDETAILS.t_dscb  where MATERIAL.t_desc  ='" + objModelWPP.Specification1 + "'").FirstOrDefault() : "";
                    //    objModelWPP.PNumber2 = !string.IsNullOrWhiteSpace(objModelWPP.Specification2) ? db.Database.SqlQuery<string>(@"select top 1 PARTDETAILS.t_dscb  from " + LNLinkedServer + ".dbo.ttcibd001175 as PARTDETAILS INNER JOIN " + LNLinkedServer + ".dbo.tltibd901175 as MATERIAL ON MATERIAL.t_mtrl = PARTDETAILS.t_dscb  where MATERIAL.t_desc  ='" + objModelWPP.Specification2 + "'").FirstOrDefault() : "";
                    //    objModelWPP.PNumber3 = !string.IsNullOrWhiteSpace(objModelWPP.Specification3) ? db.Database.SqlQuery<string>(@"select top 1 PARTDETAILS.t_dscb  from " + LNLinkedServer + ".dbo.ttcibd001175 as PARTDETAILS INNER JOIN " + LNLinkedServer + ".dbo.tltibd901175 as MATERIAL ON MATERIAL.t_mtrl = PARTDETAILS.t_dscb  where MATERIAL.t_desc  ='" + objModelWPP.Specification3 + "'").FirstOrDefault() : "";
                    //}
                }
            }
            return objModelWPP;
        }

        #endregion

        #region Export Grid Data
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_WPP_HEADER_INDEX_DATA(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      WPPNumber = Convert.ToString(uc.WPPNumber),
                                      WPPRevNo = Convert.ToString("R" + uc.WPPRevNo),
                                      Status = Convert.ToString(uc.Status),
                                      CreatedBy = Convert.ToString(uc.CreatedBy),
                                      CreatedOn = uc.CreatedOn == null || uc.CreatedOn.Value == DateTime.MinValue ? " " : uc.CreatedOn.Value.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture),
                                      SubmittedBy = uc.SubmittedBy,
                                      SubmittedOn = uc.SubmittedOn == null || uc.SubmittedOn.Value == DateTime.MinValue ? " " : uc.SubmittedOn.Value.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture),
                                      Location = uc.Location
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                if (gridType == clsImplementationEnum.GridType.HISTORYHEADER.GetStringValue())
                {
                    var lst = db.SP_WPP_HEADER_HISTORY_INDEX_DATA(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      WPPNumber = Convert.ToString(uc.WPPNumber),
                                      WPPRevNo = Convert.ToString("R" + uc.WPPRevNo),
                                      Status = Convert.ToString(uc.Status),
                                      CreatedBy = Convert.ToString(uc.CreatedBy),
                                      CreatedOn = uc.CreatedOn == null || uc.CreatedOn.Value == DateTime.MinValue ? " " : uc.CreatedOn.Value.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture),
                                      SubmittedBy = uc.SubmittedBy,
                                      SubmittedOn = uc.SubmittedOn == null || uc.SubmittedOn.Value == DateTime.MinValue ? " " : uc.SubmittedOn.Value.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture),
                                      Location = uc.Location
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {
                    var lst = db.SP_WPP_GET_LINES_DATA(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      srno = Convert.ToString(uc.ROW_NO),
                                      WSNo = Convert.ToString(uc.WSNo),
                                      WeldingProcess = Convert.ToString(uc.WeldingProcess),
                                      PQRNo = Convert.ToString(uc.PQRNo),
                                      AWSClass = Convert.ToString(uc.AWSClass),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                if (gridType == clsImplementationEnum.GridType.HISTORYLINES.GetStringValue())
                {
                    var lst = db.SP_WPP_HISTORY_LINES_DATA(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      srno = Convert.ToString(uc.ROW_NO),
                                      WSNo = Convert.ToString(uc.WSNo),
                                      WeldingProcess = Convert.ToString(uc.WeldingProcess),
                                      PQRNo = Convert.ToString(uc.PQRNo),
                                      AWSClass = Convert.ToString(uc.AWSClass),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }


                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region ExcelHelper
        [HttpPost]
        public ActionResult ImportWPPHeaderPartial()
        {
            ViewBag.location = objClsLoginInfo.Location;
            var jointtype = GetSubCatagory("Joint Type", objClsLoginInfo.Location.Trim()).Select(i => new { i.Code, i.Description }).Distinct();
            ViewBag.JointType = jointtype.Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + "-" + i.Description });

            return PartialView("_UploadWPPHeaderPartial");
        }


        [HttpPost]
        public ActionResult WPPTemplatePartial()
        {
            ViewBag.location = objClsLoginInfo.Location;
            var jointtype = GetSubCatagory("Joint Type", objClsLoginInfo.Location.Trim()).Select(i => new { i.Code, i.Description }).Distinct();
            ViewBag.JointType = jointtype.Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + "-" + i.Description });

            return PartialView("_WPPTemplatePartial");
        }

        [HttpPost]
        [SessionExpireFilter]
        public ActionResult ImportExcel(HttpPostedFileBase upload, FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();

            if (Path.GetExtension(upload.FileName) == ".xlsx" || Path.GetExtension(upload.FileName) == ".xls")
            {
                ExcelPackage package = new ExcelPackage(upload.InputStream);
                string Location = objClsLoginInfo.Location;
                string jointtype = fc["JointType"];
                string qProject = fc["txtQualityProject"];

                bool isError;
                DataSet ds = ToAddWPPHeader(package, qProject, jointtype, out isError);
                if (!isError)
                {
                    try
                    {
                        //header data
                        List<int> lstHeaderId = new List<int>();
                        List<string> lstWPPNos = new List<string>();
                        List<WPP011> lstAddLines = new List<WPP011>();
                        int wppNewHeaderid = 0;
                        string wppNumber = string.Empty;
                        foreach (DataRow item in ds.Tables[0].Rows)
                        {
                            if ((!string.IsNullOrWhiteSpace(item.Field<string>("Document No.")) || !string.IsNullOrWhiteSpace(item.Field<string>("Seam No."))) && item.Field<string>("WS No.") != "" && item.Field<string>("WS No.") != null)
                            {
                                #region Header 
                                clsHelper.ResponseMsgWithStatus objModelWPP = new clsHelper.ResponseMsgWithStatus();
                                string FirstSeam = string.Empty;
                                string Seamno = item.Field<string>("Seam No.");
                                string[] seamArr = Seamno.Split(',');
                                FirstSeam = seamArr[0];
                                objModelWPP = SeamData(qProject.Trim(), FirstSeam);
                                wppNumber = item.Field<string>("WPP No.");
                                WPP010 objWPP010 = new WPP010();
                                if (string.IsNullOrWhiteSpace(wppNumber))
                                {
                                    wppNumber = wppnumber(qProject, objClsLoginInfo.Location);
                                    objWPP010.WPPNumber = wppNumber;
                                    objWPP010.DocNo = Convert.ToInt32(objWPP010.WPPNumber.Split('-')[1]);
                                }
                                else
                                {
                                    objWPP010.WPPNumber = wppNumber;
                                    objWPP010.DocNo = 0;
                                }
                                objWPP010.Location = objClsLoginInfo.Location.Trim();
                                objWPP010.DocumentNo = item.Field<string>("Document No.");
                                objWPP010.WPPRevNo = 0;
                                objWPP010.WeldDesignation = objModelWPP.WeldDesignation;
                                objWPP010.WeldingProcess1 = getCategoryCode("Welding Process", "", objClsLoginInfo.Location.Trim(), item.Field<string>("Welding Process 1"));
                                objWPP010.WeldingProcess2 = getCategoryCode("Welding Process", "", objClsLoginInfo.Location.Trim(), item.Field<string>("Welding Process 2"));
                                objWPP010.WeldingProcess3 = getCategoryCode("Welding Process", "", objClsLoginInfo.Location.Trim(), item.Field<string>("Welding Process 3"));
                                objWPP010.WeldingType1 = item.Field<string>("Type of Weld 1");
                                objWPP010.WeldingType2 = item.Field<string>("Type of Weld 2");
                                objWPP010.WeldingType3 = item.Field<string>("Type of Weld 3");
                                objWPP010.JointType = jointtype;
                                if (objWPP010.WeldingProcess1 == "SMAW" || objWPP010.WeldingProcess2 == "SMAW" || objWPP010.WeldingProcess3 == "SMAW")
                                {
                                    objWPP010.DryingofElectrode = item.Field<string>("Drying of Electrode");
                                }
                                else
                                {
                                    objWPP010.DryingofElectrode = "NA";
                                }
                                if ((objWPP010.WeldingProcess1 == "SAW" || objWPP010.WeldingProcess2 == "SAW" || objWPP010.WeldingProcess3 == "SAW") || (objWPP010.WeldingProcess1 == "ESW" || objWPP010.WeldingProcess2 == "ESW" || objWPP010.WeldingProcess3 == "ESW"))
                                {
                                    objWPP010.DryingofFlux = item.Field<string>("Drying of Flux");
                                }
                                else
                                {
                                    objWPP010.DryingofFlux = "NA";
                                }
                                objWPP010.ItemNumber1 = objModelWPP.ItemNumber1;
                                objWPP010.ItemNumber2 = objModelWPP.ItemNumber2;
                                objWPP010.ItemNumber3 = objModelWPP.ItemNumber3;
                                objWPP010.PNoMaterial1 = !string.IsNullOrWhiteSpace(item.Field<string>("P No. [Material 1]")) ? item.Field<string>("P No. [Material 1]").Replace("\"", "").ToLower() : "";   //objModelWPP.PNumber1;
                                objWPP010.PNoMaterial2 = !string.IsNullOrWhiteSpace(item.Field<string>("P No. [Material 2]")) ? item.Field<string>("P No. [Material 2]").Replace("\"", "").ToLower() : "";   //objModelWPP.PNumber2;
                                objWPP010.PNoMaterial3 = !string.IsNullOrWhiteSpace(item.Field<string>("P No. [Material 3]")) ? item.Field<string>("P No. [Material 3]").Replace("\"", "").ToLower() : "";   //objModelWPP.PNumber3;
                                objWPP010.Specification1 = objModelWPP.Specification1;
                                objWPP010.Specification2 = objModelWPP.Specification2;
                                objWPP010.Specification3 = objModelWPP.Specification3;
                                objWPP010.Remarks1 = item.Field<string>("Remarks 1");
                                objWPP010.Remarks2 = item.Field<string>("Remarks 2");
                                objWPP010.Remarks3 = item.Field<string>("Remarks 3");
                                objWPP010.WEPBy = item.Field<string>("WEP By");
                                objWPP010.NatureofRevision = item.Field<string>("Nature of Revision");
                                objWPP010.Remarks = item.Field<string>("Remarks");
                                objWPP010.Notes = item.Field<string>("Notes");

                                objWPP010.QualityProjects = qProject;
                                objWPP010.QualityProject = qProject;
                                objWPP010.Status = clsImplementationEnum.WPPStatus.Draft.GetStringValue();
                                objWPP010.CreatedBy = objClsLoginInfo.UserName;
                                objWPP010.CreatedOn = DateTime.Now;
                                db.WPP010.Add(objWPP010);
                                db.SaveChanges();
                                WPP013 objWPP013 = new WPP013();
                                string SeamNo = item.Field<string>("Seam No.");
                                string[] arraySeamNo = SeamNo.Split(',').ToArray();
                                List<WPP013> lstWPP013 = new List<WPP013>();
                                for (int i = 0; i < arraySeamNo.Length; i++)
                                {
                                    string seam = arraySeamNo[i];
                                    string proj = qProject;
                                    WPP013 objWPP013Add = new WPP013();
                                    objWPP013Add.HeaderId = objWPP010.HeaderId;
                                    objWPP013Add.QualityProject = proj;
                                    objWPP013Add.Seam = seam;
                                    objWPP013Add.Project = new NDEModels().GetQMSProject(proj).Project;
                                    objWPP013Add.BU = db.COM001.Where(o => o.t_cprj == objWPP013Add.Project).FirstOrDefault().t_entu;
                                    objWPP013Add.CreatedBy = objClsLoginInfo.UserName;
                                    objWPP013Add.CreatedOn = DateTime.Now;
                                    objWPP013Add.Location = objWPP010.Location;
                                    lstWPP013.Add(objWPP013Add);
                                }
                                if (lstWPP013 != null && lstWPP013.Count > 0)
                                {
                                    db.WPP013.AddRange(lstWPP013);
                                }
                                db.SaveChanges();
                                wppNewHeaderid = objWPP010.HeaderId;
                                lstHeaderId.Add(wppNewHeaderid);
                                lstWPPNos.Add(objWPP010.WPPNumber);
                                #endregion
                            }

                            if (item.Field<string>("WS No.") != "" && item.Field<string>("WS No.") != null)
                            {
                                #region Lines
                                WPP011 objWPP011 = new WPP011();
                                objWPP011.HeaderId = wppNewHeaderid;
                                objWPP011.Location = objClsLoginInfo.Location;
                                objWPP011.WPPNumber = wppNumber;
                                objWPP011.WPPRevNo = 0;
                                objWPP011.WSNo = item.Field<string>("WS No.");
                                if (item.Field<string>("Welding Process").ToLower().Trim() == "back gouging")
                                {
                                    objWPP011.WeldingProcess = "Back Gouging";
                                }
                                else
                                {
                                    objWPP011.WeldingProcess = getCategoryCode("Welding Process", "", objClsLoginInfo.Location.Trim(), item.Field<string>("Welding Process"));
                                    objWPP011.PQRNo = item.Field<string>("PQR No.");
                                    string[] WeldingPositionArray = item.Field<string>("Welding Position").ToLower().Split(',').ToArray();

                                    List<string> lstWeldingPosition = new List<string>();
                                    foreach (var ite in WeldingPositionArray)
                                    {
                                        if (!string.IsNullOrWhiteSpace(ite))
                                        {
                                            if (!string.IsNullOrWhiteSpace(getCategoryCode("Welding Position", "", objClsLoginInfo.Location.Trim(), ite, false)))
                                            {
                                                lstWeldingPosition.Add(getCategoryCode("Welding Position", "", objClsLoginInfo.Location.Trim(), ite, false));
                                            }
                                        }
                                    }
                                    if (lstWeldingPosition.Count() > 0)
                                    {
                                        objWPP011.WeldingPosition = string.Join(",", lstWeldingPosition);
                                    }
                                    string awsclass = item.Field<string>("AWS Class").Trim().ToLower();
                                    string Sizeoffillermetal = item.Field<string>("Size of Filler Metal(mm)").ToLower().Trim();
                                    WPS002 objWPS002 = db.WPS002.Where(x => x.AWSClass.Trim().ToLower() == awsclass && x.Location.ToLower().Trim() == objClsLoginInfo.Location.ToLower().Trim() && x.ConsumableSize.ToLower().Trim() == Sizeoffillermetal).FirstOrDefault();
                                    //objWPP011.WeldingPosition = getCategoryCode("Welding Position", "", objClsLoginInfo.Location.Trim(), item.Field<string>("Welding Position"));
                                    objWPP011.AWSClass = objWPS002 != null ? objWPS002.AWSClass : "";
                                    objWPP011.SizeofFillerMetal = item.Field<string>("Size of Filler Metal(mm)");
                                    objWPP011.BatchNo = item.Field<string>("Batch No");
                                    objWPP011.WMTestNo = item.Field<string>("WM Test No");
                                    objWPP011.FNumber = objWPS002 != null ? objWPS002.FNumber : ""; //item.Field<string>("F. No."); //
                                    objWPP011.ANumber = objWPS002 != null ? objWPS002.ANumber : "";// item.Field<string>("A. No.");// 

                                    if (objWPP011.WeldingProcess == "SAW" || objWPP011.WeldingProcess == "ESW" || objWPP011.WeldingProcess == "SASC")
                                    {
                                        objWPP011.FluxName = item.Field<string>("Flux Name");
                                        objWPP011.FluxBatchNo = item.Field<string>("Flux Batch No.");
                                    }
                                    else
                                    {
                                        objWPP011.FluxName = "NA";
                                        objWPP011.FluxBatchNo = "NA";
                                    }
                                    objWPP011.Current = item.Field<string>("Current");
                                    objWPP011.Polarity = item.Field<string>("Polarity");
                                    objWPP011.Amperes = item.Field<string>("Amperes (Amp.)");
                                    objWPP011.Voltage = item.Field<string>("Voltage (V)");
                                    if (jointtype.ToLower() != "t#ts" && jointtype.ToLower() != "t#t/s")
                                    {
                                        if (objWPP011.WeldingProcess != "GTAW" && objWPP011.WeldingProcess != "FCAW" && objWPP011.WeldingProcess != "GMAW")
                                        {
                                            objWPP011.ShieldingGas = "NA";
                                        }
                                        else { objWPP011.ShieldingGas = getCategoryCode("Shielding Gas", "", objClsLoginInfo.Location.Trim(), item.Field<string>("Shielding Gas"), true); }
                                        if (objWPP011.WeldingProcess != "SMAW" && objWPP011.WeldingProcess != "SAW")
                                        {
                                            objWPP011.FlowRate = item.Field<string>("Flow Rate [LPM]");
                                        }
                                        else
                                        {
                                            objWPP011.FlowRate = "NA";
                                        }
                                        objWPP011.BeadLength = item.Field<string>("Min. Bead Length[mm/min]"); //objWPS002.BeadLength ;
                                        objWPP011.Interpass = item.Field<string>("Interpass [Max.] °C");
                                    }
                                    objWPP011.TravelSpeed = item.Field<string>("Travel Speed(mm/min)"); //objWPS002.TravelSpeed;
                                    objWPP011.PreheatMin = item.Field<string>("Preheat [min.] °C");
                                    objWPP011.NoofLayers = item.Field<string>("No. of Layers");
                                    if (jointtype.ToLower() == "t#ts" || jointtype.ToLower() == "t#t/s")
                                    {
                                        objWPP011.Electrode = item.Field<string>("Electrode");
                                        objWPP011.Dia = item.Field<string>("Dia");
                                        objWPP011.PeakCurrent = item.Field<string>("Peak Current [Amp]");
                                        objWPP011.BackCurrent = item.Field<string>("Back Current [Amp]");
                                        objWPP011.PrePurgeTime = item.Field<string>("Pre Purge Time");
                                        objWPP011.PreFusionTimeDelay = item.Field<string>("Pre Fusion Time Delay");
                                        objWPP011.UpSlopeTime = item.Field<string>("Up Slope Time");
                                        objWPP011.PeakCurrentTime = item.Field<string>("Peak Current Time");
                                        objWPP011.BackGroundCurrentTime = item.Field<string>("Back Ground Current Time");
                                        objWPP011.DownSlopeTime = item.Field<string>("Down Slope Time");
                                        objWPP011.DecayCurrentTime = item.Field<string>("Decay Current Time");
                                        objWPP011.OverlapTime = item.Field<string>("Overlap Time");
                                        objWPP011.PostPurgeTime = item.Field<string>("Post Purge Time");
                                        objWPP011.WireFeedSpeed = item.Field<string>("Wire Feed Speed [mm/min]");
                                        objWPP011.ArcLength = item.Field<string>("Arc Length (Gap)(mm)");
                                        //objWPP011.Size = item.Field<string>("Size (mm)");
                                        objWPP011.ArcRadius = item.Field<string>("Arc Radius(mm)");
                                        objWPP011.ManufChem = item.Field<string>("Manuf. / Chem.");
                                        objWPP011.HeatNo = item.Field<string>("Heat No.");
                                    }
                                }
                                objWPP011.CreatedBy = objClsLoginInfo.UserName;
                                objWPP011.CreatedOn = DateTime.Now;
                                lstAddLines.Add(objWPP011);
                                #endregion
                            }
                        }
                        db.WPP011.AddRange(lstAddLines);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        if (lstWPPNos.Count() > 0)
                        {
                            objResponseMsg.Value = "WPP numbers :" + string.Join(",", lstWPPNos) + " imported successfully";
                        }
                        else
                        {
                            objResponseMsg.Value = "WPP Header imported successfully";
                        }
                    }
                    catch (Exception ex)
                    {
                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    }
                }
                else
                {
                    if (jointtype.ToLower() == "t#ts" || jointtype.ToLower() == "t#t/s")
                    {
                        objResponseMsg = ExportToTTSExcel(ds);
                    }
                    else
                    { objResponseMsg = ExportToOtherExcel(ds); }
                }
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Excel file is not valid";
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public string ReplaceNewLine(string oldString)
        {
            string ReplaceString = Regex.Replace(oldString, @"\t|\n|\r", string.Empty);
            return ReplaceString;
        }
        public clsHelper.ResponceMsgWithFileName ExportToTTSExcel(DataSet ds)
        {
            clsHelper.ResponceMsgWithFileName objResponseMsg = new clsHelper.ResponceMsgWithFileName();
            try
            {
                MemoryStream ms = new MemoryStream();
                using (FileStream fs = System.IO.File.OpenRead(Server.MapPath("~/Resources/WPP/WPP-TTS Error Template.xlsx")))
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(fs))
                    {
                        ExcelWorkbook excelWorkBook = excelPackage.Workbook;
                        ExcelWorksheet excelWorksheet = excelWorkBook.Worksheets.First();

                        int i = 2;
                        foreach (DataRow item in ds.Tables[0].Rows)
                        {
                            #region Generate for HEader
                            if ((!string.IsNullOrWhiteSpace(item.Field<string>("Document No.")) || !string.IsNullOrWhiteSpace(item.Field<string>("Seam No."))) && item.Field<string>("WS No.") != "" && item.Field<string>("WS No.") != null)
                            {
                                ///WPP No.
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(65))) { excelWorksheet.Cells[i, 1].Value = item.Field<string>(0) + " (Note: " + item.Field<string>(65).ToString() + " )"; excelWorksheet.Cells[i, 1].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 1].Value = item.Field<string>(0); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(66))) { excelWorksheet.Cells[i, 2].Value = item.Field<string>(1) + " (Note: " + item.Field<string>(66).ToString() + " )"; excelWorksheet.Cells[i, 2].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 2].Value = item.Field<string>(1); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(67))) { excelWorksheet.Cells[i, 3].Value = item.Field<string>(2) + " (Note: " + item.Field<string>(67).ToString() + " )"; excelWorksheet.Cells[i, 3].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 3].Value = item.Field<string>(2); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(68))) { excelWorksheet.Cells[i, 4].Value = item.Field<string>(3) + " (Note: " + item.Field<string>(68).ToString() + " )"; excelWorksheet.Cells[i, 4].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 4].Value = item.Field<string>(3); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(69))) { excelWorksheet.Cells[i, 5].Value = item.Field<string>(4) + " (Note: " + item.Field<string>(69).ToString() + " )"; excelWorksheet.Cells[i, 5].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 5].Value = item.Field<string>(4); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(70))) { excelWorksheet.Cells[i, 6].Value = item.Field<string>(5) + " (Note: " + item.Field<string>(70).ToString() + " )"; excelWorksheet.Cells[i, 6].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 6].Value = item.Field<string>(5); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(71))) { excelWorksheet.Cells[i, 7].Value = item.Field<string>(6) + " (Note: " + item.Field<string>(71).ToString() + " )"; excelWorksheet.Cells[i, 7].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 7].Value = item.Field<string>(6); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(72))) { excelWorksheet.Cells[i, 8].Value = item.Field<string>(7) + " (Note: " + item.Field<string>(72).ToString() + " )"; excelWorksheet.Cells[i, 8].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 8].Value = item.Field<string>(7); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(73))) { excelWorksheet.Cells[i, 9].Value = item.Field<string>(8) + " (Note: " + item.Field<string>(73).ToString() + " )"; excelWorksheet.Cells[i, 9].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 9].Value = item.Field<string>(8); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(74))) { excelWorksheet.Cells[i, 10].Value = item.Field<string>(9) + " (Note: " + item.Field<string>(74).ToString() + " )"; excelWorksheet.Cells[i, 10].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 10].Value = item.Field<string>(9); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(75))) { excelWorksheet.Cells[i, 11].Value = item.Field<string>(10) + " (Note: " + item.Field<string>(75).ToString() + " )"; excelWorksheet.Cells[i, 11].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 11].Value = item.Field<string>(10); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(76))) { excelWorksheet.Cells[i, 12].Value = item.Field<string>(11) + " (Note: " + item.Field<string>(76).ToString() + " )"; excelWorksheet.Cells[i, 12].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 12].Value = item.Field<string>(11); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(77))) { excelWorksheet.Cells[i, 13].Value = item.Field<string>(12) + " (Note: " + item.Field<string>(77).ToString() + " )"; excelWorksheet.Cells[i, 13].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 13].Value = item.Field<string>(12); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(78))) { excelWorksheet.Cells[i, 14].Value = item.Field<string>(13) + " (Note: " + item.Field<string>(78).ToString() + " )"; excelWorksheet.Cells[i, 14].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 14].Value = item.Field<string>(13); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(79))) { excelWorksheet.Cells[i, 15].Value = item.Field<string>(14) + " (Note: " + item.Field<string>(79).ToString() + " )"; excelWorksheet.Cells[i, 15].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 15].Value = item.Field<string>(14); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(80))) { excelWorksheet.Cells[i, 16].Value = item.Field<string>(15) + " (Note: " + item.Field<string>(80).ToString() + " )"; excelWorksheet.Cells[i, 16].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 16].Value = item.Field<string>(15); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(81))) { excelWorksheet.Cells[i, 17].Value = item.Field<string>(16) + " (Note: " + item.Field<string>(81).ToString() + " )"; excelWorksheet.Cells[i, 17].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 17].Value = item.Field<string>(16); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(82))) { excelWorksheet.Cells[i, 18].Value = item.Field<string>(17) + " (Note: " + item.Field<string>(82).ToString() + " )"; excelWorksheet.Cells[i, 18].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 18].Value = item.Field<string>(17); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(83))) { excelWorksheet.Cells[i, 19].Value = item.Field<string>(18) + " (Note: " + item.Field<string>(83).ToString() + " )"; excelWorksheet.Cells[i, 19].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 19].Value = item.Field<string>(18); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(84))) { excelWorksheet.Cells[i, 20].Value = item.Field<string>(19) + " (Note: " + item.Field<string>(84).ToString() + " )"; excelWorksheet.Cells[i, 20].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 20].Value = item.Field<string>(19); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(85))) { excelWorksheet.Cells[i, 21].Value = item.Field<string>(20) + " (Note: " + item.Field<string>(85).ToString() + " )"; excelWorksheet.Cells[i, 21].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 21].Value = item.Field<string>(20); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(86))) { excelWorksheet.Cells[i, 22].Value = item.Field<string>(21) + " (Note: " + item.Field<string>(86).ToString() + " )"; excelWorksheet.Cells[i, 22].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 22].Value = item.Field<string>(21); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(87))) { excelWorksheet.Cells[i, 23].Value = item.Field<string>(22) + " (Note: " + item.Field<string>(87).ToString() + " )"; excelWorksheet.Cells[i, 23].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 23].Value = item.Field<string>(22); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(88))) { excelWorksheet.Cells[i, 24].Value = item.Field<string>(23) + " (Note: " + item.Field<string>(88).ToString() + " )"; excelWorksheet.Cells[i, 24].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 24].Value = item.Field<string>(23); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(89))) { excelWorksheet.Cells[i, 25].Value = item.Field<string>(24) + " (Note: " + item.Field<string>(89).ToString() + " )"; excelWorksheet.Cells[i, 25].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 25].Value = item.Field<string>(24); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(90))) { excelWorksheet.Cells[i, 26].Value = item.Field<string>(25) + " (Note: " + item.Field<string>(90).ToString() + " )"; excelWorksheet.Cells[i, 26].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 26].Value = item.Field<string>(25); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(91))) { excelWorksheet.Cells[i, 27].Value = item.Field<string>(26) + " (Note: " + item.Field<string>(91).ToString() + " )"; excelWorksheet.Cells[i, 27].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 27].Value = item.Field<string>(26); }

                            }
                            #endregion

                            #region Common Columns for Lines
                            ///lines
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(92))) { excelWorksheet.Cells[i, 28].Value = item.Field<string>(27) + " (Note: " + item.Field<string>(92).ToString() + " )"; excelWorksheet.Cells[i, 28].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 28].Value = item.Field<string>(27); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(93))) { excelWorksheet.Cells[i, 29].Value = item.Field<string>(28) + " (Note: " + item.Field<string>(93).ToString() + " )"; excelWorksheet.Cells[i, 29].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 29].Value = item.Field<string>(28); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(94))) { excelWorksheet.Cells[i, 30].Value = item.Field<string>(29) + " (Note: " + item.Field<string>(94).ToString() + " )"; excelWorksheet.Cells[i, 30].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 30].Value = item.Field<string>(29); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(95))) { excelWorksheet.Cells[i, 31].Value = item.Field<string>(30) + " (Note: " + item.Field<string>(95).ToString() + " )"; excelWorksheet.Cells[i, 31].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 31].Value = item.Field<string>(30); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(96))) { excelWorksheet.Cells[i, 32].Value = item.Field<string>(31) + " (Note: " + item.Field<string>(96).ToString() + " )"; excelWorksheet.Cells[i, 32].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 32].Value = item.Field<string>(31); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(97))) { excelWorksheet.Cells[i, 33].Value = item.Field<string>(32) + " (Note: " + item.Field<string>(97).ToString() + " )"; excelWorksheet.Cells[i, 33].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 33].Value = item.Field<string>(32); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(98))) { excelWorksheet.Cells[i, 34].Value = item.Field<string>(33) + " (Note: " + item.Field<string>(98).ToString() + " )"; excelWorksheet.Cells[i, 34].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 34].Value = item.Field<string>(33); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(99))) { excelWorksheet.Cells[i, 35].Value = item.Field<string>(34) + " (Note: " + item.Field<string>(99).ToString() + " )"; excelWorksheet.Cells[i, 35].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 35].Value = item.Field<string>(34); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(100))) { excelWorksheet.Cells[i, 36].Value = item.Field<string>(35) + " (Note: " + item.Field<string>(100).ToString() + " )"; excelWorksheet.Cells[i, 36].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 36].Value = item.Field<string>(35); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(101))) { excelWorksheet.Cells[i, 37].Value = item.Field<string>(36) + " (Note: " + item.Field<string>(101).ToString() + " )"; excelWorksheet.Cells[i, 37].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 37].Value = item.Field<string>(36); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(102))) { excelWorksheet.Cells[i, 38].Value = item.Field<string>(37) + " (Note: " + item.Field<string>(102).ToString() + " )"; excelWorksheet.Cells[i, 38].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 38].Value = item.Field<string>(37); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(103))) { excelWorksheet.Cells[i, 39].Value = item.Field<string>(38) + " (Note: " + item.Field<string>(103).ToString() + " )"; excelWorksheet.Cells[i, 39].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 39].Value = item.Field<string>(38); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(104))) { excelWorksheet.Cells[i, 40].Value = item.Field<string>(39) + " (Note: " + item.Field<string>(104).ToString() + " )"; excelWorksheet.Cells[i, 40].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 40].Value = item.Field<string>(39); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(105))) { excelWorksheet.Cells[i, 41].Value = item.Field<string>(40) + " (Note: " + item.Field<string>(105).ToString() + " )"; excelWorksheet.Cells[i, 41].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 41].Value = item.Field<string>(40); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(106))) { excelWorksheet.Cells[i, 42].Value = item.Field<string>(41) + " (Note: " + item.Field<string>(106).ToString() + " )"; excelWorksheet.Cells[i, 42].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 42].Value = item.Field<string>(41); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(107))) { excelWorksheet.Cells[i, 43].Value = item.Field<string>(42) + " (Note: " + item.Field<string>(107).ToString() + " )"; excelWorksheet.Cells[i, 43].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 43].Value = item.Field<string>(42); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(108))) { excelWorksheet.Cells[i, 44].Value = item.Field<string>(43) + " (Note: " + item.Field<string>(108).ToString() + " )"; excelWorksheet.Cells[i, 44].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 44].Value = item.Field<string>(43); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(109))) { excelWorksheet.Cells[i, 45].Value = item.Field<string>(44) + " (Note: " + item.Field<string>(109).ToString() + " )"; excelWorksheet.Cells[i, 45].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 45].Value = item.Field<string>(44); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(110))) { excelWorksheet.Cells[i, 46].Value = item.Field<string>(45) + " (Note: " + item.Field<string>(110).ToString() + " )"; excelWorksheet.Cells[i, 46].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 46].Value = item.Field<string>(45); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(111))) { excelWorksheet.Cells[i, 47].Value = item.Field<string>(46) + " (Note: " + item.Field<string>(111).ToString() + " )"; excelWorksheet.Cells[i, 47].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 47].Value = item.Field<string>(46); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(112))) { excelWorksheet.Cells[i, 48].Value = item.Field<string>(47) + " (Note: " + item.Field<string>(112).ToString() + " )"; excelWorksheet.Cells[i, 48].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 48].Value = item.Field<string>(47); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(113))) { excelWorksheet.Cells[i, 49].Value = item.Field<string>(48) + " (Note: " + item.Field<string>(113).ToString() + " )"; excelWorksheet.Cells[i, 49].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 49].Value = item.Field<string>(48); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(114))) { excelWorksheet.Cells[i, 50].Value = item.Field<string>(49) + " (Note: " + item.Field<string>(114).ToString() + " )"; excelWorksheet.Cells[i, 50].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 50].Value = item.Field<string>(49); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(115))) { excelWorksheet.Cells[i, 51].Value = item.Field<string>(50) + " (Note: " + item.Field<string>(115).ToString() + " )"; excelWorksheet.Cells[i, 51].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 51].Value = item.Field<string>(50); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(116))) { excelWorksheet.Cells[i, 52].Value = item.Field<string>(51) + " (Note: " + item.Field<string>(116).ToString() + " )"; excelWorksheet.Cells[i, 52].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 52].Value = item.Field<string>(51); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(117))) { excelWorksheet.Cells[i, 53].Value = item.Field<string>(52) + " (Note: " + item.Field<string>(117).ToString() + " )"; excelWorksheet.Cells[i, 53].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 53].Value = item.Field<string>(52); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(118))) { excelWorksheet.Cells[i, 54].Value = item.Field<string>(53) + " (Note: " + item.Field<string>(118).ToString() + " )"; excelWorksheet.Cells[i, 54].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 54].Value = item.Field<string>(53); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(119))) { excelWorksheet.Cells[i, 55].Value = item.Field<string>(54) + " (Note: " + item.Field<string>(119).ToString() + " )"; excelWorksheet.Cells[i, 55].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 55].Value = item.Field<string>(54); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(120))) { excelWorksheet.Cells[i, 56].Value = item.Field<string>(55) + " (Note: " + item.Field<string>(120).ToString() + " )"; excelWorksheet.Cells[i, 56].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 56].Value = item.Field<string>(55); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(121))) { excelWorksheet.Cells[i, 57].Value = item.Field<string>(56) + " (Note: " + item.Field<string>(121).ToString() + " )"; excelWorksheet.Cells[i, 57].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 57].Value = item.Field<string>(56); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(122))) { excelWorksheet.Cells[i, 58].Value = item.Field<string>(57) + " (Note: " + item.Field<string>(122).ToString() + " )"; excelWorksheet.Cells[i, 58].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 58].Value = item.Field<string>(57); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(123))) { excelWorksheet.Cells[i, 59].Value = item.Field<string>(58) + " (Note: " + item.Field<string>(123).ToString() + " )"; excelWorksheet.Cells[i, 59].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 59].Value = item.Field<string>(58); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(124))) { excelWorksheet.Cells[i, 60].Value = item.Field<string>(59) + " (Note: " + item.Field<string>(124).ToString() + " )"; excelWorksheet.Cells[i, 60].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 60].Value = item.Field<string>(59); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(125))) { excelWorksheet.Cells[i, 61].Value = item.Field<string>(60) + " (Note: " + item.Field<string>(125).ToString() + " )"; excelWorksheet.Cells[i, 61].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 61].Value = item.Field<string>(60); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(126))) { excelWorksheet.Cells[i, 62].Value = item.Field<string>(61) + " (Note: " + item.Field<string>(126).ToString() + " )"; excelWorksheet.Cells[i, 62].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 62].Value = item.Field<string>(61); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(127))) { excelWorksheet.Cells[i, 63].Value = item.Field<string>(62) + " (Note: " + item.Field<string>(127).ToString() + " )"; excelWorksheet.Cells[i, 63].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 63].Value = item.Field<string>(62); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(128))) { excelWorksheet.Cells[i, 64].Value = item.Field<string>(63) + " (Note: " + item.Field<string>(128).ToString() + " )"; excelWorksheet.Cells[i, 64].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 64].Value = item.Field<string>(63); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(129))) { excelWorksheet.Cells[i, 65].Value = item.Field<string>(64) + " (Note: " + item.Field<string>(129).ToString() + " )"; excelWorksheet.Cells[i, 65].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 65].Value = item.Field<string>(64); }
                            #endregion 
                            i++;
                        }

                        excelPackage.SaveAs(ms);
                    }
                }

                ms.Position = 0;

                string fileName;
                string excelFilePath = Helper.ExcelFilePath("ErrorList", out fileName);

                Byte[] bin = ms.ToArray();
                System.IO.File.WriteAllBytes(excelFilePath, bin);

                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error";
                objResponseMsg.FileName = fileName;
                return objResponseMsg;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
        }

        public clsHelper.ResponceMsgWithFileName ExportToOtherExcel(DataSet ds)
        {
            clsHelper.ResponceMsgWithFileName objResponseMsg = new clsHelper.ResponceMsgWithFileName();
            try
            {
                MemoryStream ms = new MemoryStream();
                using (FileStream fs = System.IO.File.OpenRead(Server.MapPath("~/Resources/WPP/WPP-BUTTWELD ErrorTemplate.xlsx")))
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(fs))
                    {
                        ExcelWorkbook excelWorkBook = excelPackage.Workbook;
                        ExcelWorksheet excelWorksheet = excelWorkBook.Worksheets.First();

                        int i = 2;
                        foreach (DataRow item in ds.Tables[0].Rows)
                        {
                            #region Generate for HEader
                            if ((!string.IsNullOrWhiteSpace(item.Field<string>("Document No.")) || !string.IsNullOrWhiteSpace(item.Field<string>("Seam No."))) && item.Field<string>("WS No.") != "" && item.Field<string>("WS No.") != null)
                            {
                                ///WPP No.
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(51))) { excelWorksheet.Cells[i, 1].Value = item.Field<string>(0) + " (Note: " + item.Field<string>(51).ToString() + " )"; excelWorksheet.Cells[i, 1].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 1].Value = item.Field<string>(0); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(52))) { excelWorksheet.Cells[i, 2].Value = item.Field<string>(1) + " (Note: " + item.Field<string>(52).ToString() + " )"; excelWorksheet.Cells[i, 2].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 2].Value = item.Field<string>(1); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(53))) { excelWorksheet.Cells[i, 3].Value = item.Field<string>(2) + " (Note: " + item.Field<string>(53).ToString() + " )"; excelWorksheet.Cells[i, 3].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 3].Value = item.Field<string>(2); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(54))) { excelWorksheet.Cells[i, 4].Value = item.Field<string>(3) + " (Note: " + item.Field<string>(54).ToString() + " )"; excelWorksheet.Cells[i, 4].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 4].Value = item.Field<string>(3); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(55))) { excelWorksheet.Cells[i, 5].Value = item.Field<string>(4) + " (Note: " + item.Field<string>(55).ToString() + " )"; excelWorksheet.Cells[i, 5].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 5].Value = item.Field<string>(4); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(56))) { excelWorksheet.Cells[i, 6].Value = item.Field<string>(5) + " (Note: " + item.Field<string>(56).ToString() + " )"; excelWorksheet.Cells[i, 6].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 6].Value = item.Field<string>(5); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(57))) { excelWorksheet.Cells[i, 7].Value = item.Field<string>(6) + " (Note: " + item.Field<string>(57).ToString() + " )"; excelWorksheet.Cells[i, 7].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 7].Value = item.Field<string>(6); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(58))) { excelWorksheet.Cells[i, 8].Value = item.Field<string>(7) + " (Note: " + item.Field<string>(58).ToString() + " )"; excelWorksheet.Cells[i, 8].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 8].Value = item.Field<string>(7); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(59))) { excelWorksheet.Cells[i, 9].Value = item.Field<string>(8) + " (Note: " + item.Field<string>(59).ToString() + " )"; excelWorksheet.Cells[i, 9].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 9].Value = item.Field<string>(8); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(60))) { excelWorksheet.Cells[i, 10].Value = item.Field<string>(9) + " (Note: " + item.Field<string>(60).ToString() + " )"; excelWorksheet.Cells[i, 10].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 10].Value = item.Field<string>(9); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(61))) { excelWorksheet.Cells[i, 11].Value = item.Field<string>(10) + " (Note: " + item.Field<string>(61).ToString() + " )"; excelWorksheet.Cells[i, 11].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 11].Value = item.Field<string>(10); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(62))) { excelWorksheet.Cells[i, 12].Value = item.Field<string>(11) + " (Note: " + item.Field<string>(62).ToString() + " )"; excelWorksheet.Cells[i, 12].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 12].Value = item.Field<string>(11); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(63))) { excelWorksheet.Cells[i, 13].Value = item.Field<string>(12) + " (Note: " + item.Field<string>(63).ToString() + " )"; excelWorksheet.Cells[i, 13].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 13].Value = item.Field<string>(12); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(64))) { excelWorksheet.Cells[i, 14].Value = item.Field<string>(13) + " (Note: " + item.Field<string>(64).ToString() + " )"; excelWorksheet.Cells[i, 14].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 14].Value = item.Field<string>(13); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(65))) { excelWorksheet.Cells[i, 15].Value = item.Field<string>(14) + " (Note: " + item.Field<string>(65).ToString() + " )"; excelWorksheet.Cells[i, 15].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 15].Value = item.Field<string>(14); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(66))) { excelWorksheet.Cells[i, 16].Value = item.Field<string>(15) + " (Note: " + item.Field<string>(66).ToString() + " )"; excelWorksheet.Cells[i, 16].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 16].Value = item.Field<string>(15); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(67))) { excelWorksheet.Cells[i, 17].Value = item.Field<string>(16) + " (Note: " + item.Field<string>(67).ToString() + " )"; excelWorksheet.Cells[i, 17].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 17].Value = item.Field<string>(16); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(68))) { excelWorksheet.Cells[i, 18].Value = item.Field<string>(17) + " (Note: " + item.Field<string>(68).ToString() + " )"; excelWorksheet.Cells[i, 18].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 18].Value = item.Field<string>(17); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(69))) { excelWorksheet.Cells[i, 19].Value = item.Field<string>(18) + " (Note: " + item.Field<string>(69).ToString() + " )"; excelWorksheet.Cells[i, 19].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 19].Value = item.Field<string>(18); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(70))) { excelWorksheet.Cells[i, 20].Value = item.Field<string>(19) + " (Note: " + item.Field<string>(70).ToString() + " )"; excelWorksheet.Cells[i, 20].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 20].Value = item.Field<string>(19); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(71))) { excelWorksheet.Cells[i, 21].Value = item.Field<string>(20) + " (Note: " + item.Field<string>(71).ToString() + " )"; excelWorksheet.Cells[i, 21].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 21].Value = item.Field<string>(20); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(72))) { excelWorksheet.Cells[i, 22].Value = item.Field<string>(21) + " (Note: " + item.Field<string>(72).ToString() + " )"; excelWorksheet.Cells[i, 22].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 22].Value = item.Field<string>(21); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(73))) { excelWorksheet.Cells[i, 23].Value = item.Field<string>(22) + " (Note: " + item.Field<string>(73).ToString() + " )"; excelWorksheet.Cells[i, 23].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 23].Value = item.Field<string>(22); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(74))) { excelWorksheet.Cells[i, 24].Value = item.Field<string>(23) + " (Note: " + item.Field<string>(74).ToString() + " )"; excelWorksheet.Cells[i, 24].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 24].Value = item.Field<string>(23); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(75))) { excelWorksheet.Cells[i, 25].Value = item.Field<string>(24) + " (Note: " + item.Field<string>(75).ToString() + " )"; excelWorksheet.Cells[i, 25].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 25].Value = item.Field<string>(24); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(76))) { excelWorksheet.Cells[i, 26].Value = item.Field<string>(25) + " (Note: " + item.Field<string>(76).ToString() + " )"; excelWorksheet.Cells[i, 26].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 26].Value = item.Field<string>(25); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(77))) { excelWorksheet.Cells[i, 27].Value = item.Field<string>(26) + " (Note: " + item.Field<string>(77).ToString() + " )"; excelWorksheet.Cells[i, 27].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 27].Value = item.Field<string>(26); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(78))) { excelWorksheet.Cells[i, 28].Value = item.Field<string>(27) + " (Note: " + item.Field<string>(78).ToString() + " )"; excelWorksheet.Cells[i, 28].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 28].Value = item.Field<string>(27); }
                            }
                            #endregion

                            #region Common Columns for Lines
                            ///lines
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(79))) { excelWorksheet.Cells[i, 29].Value = item.Field<string>(28) + " (Note: " + item.Field<string>(79).ToString() + " )"; excelWorksheet.Cells[i, 29].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 29].Value = item.Field<string>(28); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(80))) { excelWorksheet.Cells[i, 30].Value = item.Field<string>(29) + " (Note: " + item.Field<string>(80).ToString() + " )"; excelWorksheet.Cells[i, 30].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 30].Value = item.Field<string>(29); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(81))) { excelWorksheet.Cells[i, 31].Value = item.Field<string>(30) + " (Note: " + item.Field<string>(81).ToString() + " )"; excelWorksheet.Cells[i, 31].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 31].Value = item.Field<string>(30); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(82))) { excelWorksheet.Cells[i, 32].Value = item.Field<string>(31) + " (Note: " + item.Field<string>(82).ToString() + " )"; excelWorksheet.Cells[i, 32].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 32].Value = item.Field<string>(31); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(83))) { excelWorksheet.Cells[i, 33].Value = item.Field<string>(32) + " (Note: " + item.Field<string>(83).ToString() + " )"; excelWorksheet.Cells[i, 33].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 33].Value = item.Field<string>(32); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(84))) { excelWorksheet.Cells[i, 34].Value = item.Field<string>(33) + " (Note: " + item.Field<string>(84).ToString() + " )"; excelWorksheet.Cells[i, 34].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 34].Value = item.Field<string>(33); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(85))) { excelWorksheet.Cells[i, 35].Value = item.Field<string>(34) + " (Note: " + item.Field<string>(85).ToString() + " )"; excelWorksheet.Cells[i, 35].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 35].Value = item.Field<string>(34); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(86))) { excelWorksheet.Cells[i, 36].Value = item.Field<string>(35) + " (Note: " + item.Field<string>(86).ToString() + " )"; excelWorksheet.Cells[i, 36].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 36].Value = item.Field<string>(35); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(87))) { excelWorksheet.Cells[i, 37].Value = item.Field<string>(36) + " (Note: " + item.Field<string>(87).ToString() + " )"; excelWorksheet.Cells[i, 37].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 37].Value = item.Field<string>(36); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(88))) { excelWorksheet.Cells[i, 38].Value = item.Field<string>(37) + " (Note: " + item.Field<string>(88).ToString() + " )"; excelWorksheet.Cells[i, 38].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 38].Value = item.Field<string>(37); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(89))) { excelWorksheet.Cells[i, 39].Value = item.Field<string>(38) + " (Note: " + item.Field<string>(89).ToString() + " )"; excelWorksheet.Cells[i, 39].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 39].Value = item.Field<string>(38); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(90))) { excelWorksheet.Cells[i, 40].Value = item.Field<string>(39) + " (Note: " + item.Field<string>(90).ToString() + " )"; excelWorksheet.Cells[i, 40].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 40].Value = item.Field<string>(39); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(91))) { excelWorksheet.Cells[i, 41].Value = item.Field<string>(40) + " (Note: " + item.Field<string>(91).ToString() + " )"; excelWorksheet.Cells[i, 41].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 41].Value = item.Field<string>(40); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(92))) { excelWorksheet.Cells[i, 42].Value = item.Field<string>(41) + " (Note: " + item.Field<string>(92).ToString() + " )"; excelWorksheet.Cells[i, 42].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 42].Value = item.Field<string>(41); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(93))) { excelWorksheet.Cells[i, 43].Value = item.Field<string>(42) + " (Note: " + item.Field<string>(93).ToString() + " )"; excelWorksheet.Cells[i, 43].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 43].Value = item.Field<string>(42); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(94))) { excelWorksheet.Cells[i, 44].Value = item.Field<string>(43) + " (Note: " + item.Field<string>(94).ToString() + " )"; excelWorksheet.Cells[i, 44].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 44].Value = item.Field<string>(43); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(95))) { excelWorksheet.Cells[i, 45].Value = item.Field<string>(44) + " (Note: " + item.Field<string>(95).ToString() + " )"; excelWorksheet.Cells[i, 45].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 45].Value = item.Field<string>(44); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(96))) { excelWorksheet.Cells[i, 46].Value = item.Field<string>(45) + " (Note: " + item.Field<string>(96).ToString() + " )"; excelWorksheet.Cells[i, 46].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 46].Value = item.Field<string>(45); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(97))) { excelWorksheet.Cells[i, 47].Value = item.Field<string>(46) + " (Note: " + item.Field<string>(97).ToString() + " )"; excelWorksheet.Cells[i, 47].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 47].Value = item.Field<string>(46); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(98))) { excelWorksheet.Cells[i, 48].Value = item.Field<string>(47) + " (Note: " + item.Field<string>(98).ToString() + " )"; excelWorksheet.Cells[i, 48].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 48].Value = item.Field<string>(47); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(99))) { excelWorksheet.Cells[i, 49].Value = item.Field<string>(48) + " (Note: " + item.Field<string>(99).ToString() + " )"; excelWorksheet.Cells[i, 49].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 49].Value = item.Field<string>(48); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(100))) { excelWorksheet.Cells[i, 50].Value = item.Field<string>(49) + " (Note: " + item.Field<string>(100).ToString() + " )"; excelWorksheet.Cells[i, 50].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 50].Value = item.Field<string>(49); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(101))) { excelWorksheet.Cells[i, 51].Value = item.Field<string>(50) + " (Note: " + item.Field<string>(101).ToString() + " )"; excelWorksheet.Cells[i, 51].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 51].Value = item.Field<string>(50); }
                            #endregion
                            i++;
                        }

                        excelPackage.SaveAs(ms);
                    }
                }

                ms.Position = 0;

                string fileName;
                string excelFilePath = Helper.ExcelFilePath("ErrorList", out fileName);

                Byte[] bin = ms.ToArray();
                System.IO.File.WriteAllBytes(excelFilePath, bin);

                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error";
                objResponseMsg.FileName = fileName;
                return objResponseMsg;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
        }

        public string getCategoryCode(string category, string bu, string Location, string code, bool IsDesc = false)
        {
            string categorycode = string.Empty;
            if (IsDesc)
            {
                categorycode = (from glb2 in db.GLB002
                                join glb1 in db.GLB001 on glb2.Category equals glb1.Id
                                where glb1.Category == category && glb2.Location.Trim() == Location.Trim() && (glb2.Description.ToLower().Trim() == code.ToLower().Trim())
                                select glb2.Code).FirstOrDefault();
            }
            else
            {
                categorycode = (from glb2 in db.GLB002
                                join glb1 in db.GLB001 on glb2.Category equals glb1.Id
                                where glb1.Category == category && glb2.Location.Trim() == Location.Trim() && (glb2.Code.ToLower().Trim() == code.ToLower().Trim())
                                select glb2.Code).FirstOrDefault();
            }
            if (categorycode == null)
            {
                categorycode = "";
            }
            return categorycode;
        }

        public DataSet ToAddWPPHeader(ExcelPackage package, string qProject, string jointtype, out bool isError)
        {
            ExcelWorksheet excelWorksheet = package.Workbook.Worksheets.First();
            DataTable dtLinesExcel = new DataTable();
            isError = false;
            try
            {
                #region Read Lines data from excel 
                foreach (var firstRowCell in excelWorksheet.Cells[1, 1, 1, excelWorksheet.Dimension.End.Column])
                {
                    dtLinesExcel.Columns.Add(firstRowCell.Text);
                }

                for (var rowNumber = 2; rowNumber <= excelWorksheet.Dimension.End.Row; rowNumber++)
                {
                    var row = excelWorksheet.Cells[rowNumber, 1, rowNumber, excelWorksheet.Dimension.End.Column];
                    var newRow = dtLinesExcel.NewRow();

                    foreach (var cell in row)
                    {
                        newRow[cell.Start.Column - 1] = cell.Text;
                    }

                    dtLinesExcel.Rows.Add(newRow);
                }
                #endregion

                #region Error columns for Header Data
                dtLinesExcel.Columns.Add("WPPNoErrorMsg", typeof(string)); //1
                dtLinesExcel.Columns.Add("DocumentNoErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("SeamNoErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("WeldDesignationErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("WeldingProcess1ErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("TypeofWeld1ErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("WeldingProcess2ErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("TypeofWeld2ErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("WeldingProcess3ErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("TypeofWeld3ErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("DryingofElectrodeErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("DryingofFluxErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("ItemNumber1ErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("PNo1ErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("Specification1ErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("ItemNumber2ErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("PNo2ErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("Specification2ErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("ItemNumber3ErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("PNo3ErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("Specification3ErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("Remarks1ErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("Remarks2ErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("Remarks3ErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("WEPByErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("NatureofRevisionErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("NotesErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("RemarksErrorMsg", typeof(string));
                #endregion

                #region Error columns for Common Line Data
                dtLinesExcel.Columns.Add("WSNoErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("WeldingProcessErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("PQRNoErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("WeldingPositionErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("AWSClassErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("SizeofFillerMetal(mm)ErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("BatchNoErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("WMTestNoErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("FNoErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("ANoErrorMsg", typeof(string));
                //dtLinesExcel.Columns.Add("Size(mm)ErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("FluxNameErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("FluxBatchNoErrorMsg", typeof(string));
                if (jointtype.ToLower() == "t#ts" || jointtype.ToLower() == "t#t/s")
                {
                    dtLinesExcel.Columns.Add("Manuf./Chem.ErrorMsg", typeof(string));
                    dtLinesExcel.Columns.Add("HeatNo.ErrorMsg", typeof(string));
                }
                dtLinesExcel.Columns.Add("CurrentErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("PolarityErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("Amperes(Amp.)ErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("Voltage(V)ErrorMsg", typeof(string));
                if (jointtype.ToLower() != "t#ts" && jointtype.ToLower() != "t#t/s")
                {
                    dtLinesExcel.Columns.Add("ShieldingGasErrorMsg", typeof(string));
                    dtLinesExcel.Columns.Add("FlowRate[LPM]ErrorMsg", typeof(string));
                }
                dtLinesExcel.Columns.Add("TravelSpeed(mm/min)ErrorMsg", typeof(string));
                if (jointtype.ToLower() != "t#ts" && jointtype.ToLower() != "t#t/s")
                {
                    dtLinesExcel.Columns.Add("MinBeadLengthErrorMsg", typeof(string));
                }
                dtLinesExcel.Columns.Add("PreheatErrorMsg", typeof(string));
                if (jointtype.ToLower() != "t#ts" && jointtype.ToLower() != "t#t/s")
                {
                    dtLinesExcel.Columns.Add("InterpassErrorMsg", typeof(string));
                }
                dtLinesExcel.Columns.Add("No.ofLayersErrorMsg", typeof(string));
                if (jointtype.ToLower() == "t#ts" || jointtype.ToLower() == "t#t/s")
                {
                    dtLinesExcel.Columns.Add("ElectrodeErrorMsg", typeof(string));
                    dtLinesExcel.Columns.Add("DiaErrorMsg", typeof(string));
                    dtLinesExcel.Columns.Add("PeakCurrent[Amp]ErrorMsg", typeof(string));
                    dtLinesExcel.Columns.Add("BackCurrent[Amp]ErrorMsg", typeof(string));
                    dtLinesExcel.Columns.Add("PrePurgeTimeErrorMsg", typeof(string));
                    dtLinesExcel.Columns.Add("PreFusionTimeDelayErrorMsg", typeof(string));
                    dtLinesExcel.Columns.Add("UpSlopeTimeErrorMsg", typeof(string));
                    dtLinesExcel.Columns.Add("PeakCurrentTimeErrorMsg", typeof(string));
                    dtLinesExcel.Columns.Add("BackGroundCurrentTimeErrorMsg", typeof(string));
                    dtLinesExcel.Columns.Add("DownSlopeTimeErrorMsg", typeof(string));
                    dtLinesExcel.Columns.Add("DecayCurrentTimeErrorMsg", typeof(string));
                    dtLinesExcel.Columns.Add("OverlapTimeErrorMsg", typeof(string));
                    dtLinesExcel.Columns.Add("PostPurgeTimeErrorMsg", typeof(string));
                    dtLinesExcel.Columns.Add("WireFeedSpeed[mm/min]ErrorMsg", typeof(string));
                    dtLinesExcel.Columns.Add("ArcLength(Gap)(mm)ErrorMsg", typeof(string));
                    dtLinesExcel.Columns.Add("ArcRadius(mm)ErrorMsg", typeof(string));
                }
                #endregion

                #region Validate 
                var lstSeamNo = (from q12 in db.QMS012_Log
                                 join q11 in db.QMS011_Log on q12.HeaderId equals q11.HeaderId
                                 where q11.QualityProject.Trim() == qProject
                                 select new { seam = q12.SeamNo.Trim() }).ToList();
                clsHelper.ResponseMsgWithStatus objModelWPP = new clsHelper.ResponseMsgWithStatus();

                var lstarrayTypeOfWeld = clsImplementationEnum.getTypeofweld().ToList();
                List<string> lstTypeOfWeld = (from lst in lstarrayTypeOfWeld
                                              select lst.ToString().ToLower()).ToList();

                var lstarrayWEPBy = clsImplementationEnum.getWEPBy().ToList();
                List<string> lstWEPBy = (from lst in lstarrayWEPBy
                                         select lst.ToString().ToLower()).ToList();
                List<string> lstwp = new List<string>();
                string wp1 = string.Empty;
                string wp2 = string.Empty;
                string wp3 = string.Empty;
                foreach (DataRow item in dtLinesExcel.Rows)
                {
                    bool isBackGouging = false;
                    #region string variables
                    List<string> lstWppNo = new List<string>();
                    string seam = string.Empty;
                    string errorMessage = string.Empty;
                    string wppno = item.Field<string>("WPP No.");// 52
                    string Documentno = item.Field<string>("Document No.");// 53
                    string Seamno = item.Field<string>("Seam No.");// 54
                    string WeldDesignation = item.Field<string>("Weld Designation");// 55
                    string WeldingProcess1 = item.Field<string>("Welding Process 1");// 56
                    string TypeofWeld1 = item.Field<string>("Type of Weld 1");// 57
                    string WeldingProcess2 = item.Field<string>("Welding Process 2");// 58
                    string TypeofWeld2 = item.Field<string>("Type of Weld 2");// 59
                    string WeldingProcess3 = item.Field<string>("Welding Process 3");// 60
                    string TypeofWeld3 = item.Field<string>("Type of Weld 3");// 61
                    string DryingOfElectrode = item.Field<string>("Drying of Electrode");// 62
                    string DryingOfFlux = item.Field<string>("Drying of Flux");// 63
                    string ItemNumber1 = item.Field<string>("Item Number 1");// 64
                    string PNo1 = item.Field<string>("P No. [Material 1]");// 65
                    string Specification1 = item.Field<string>("Specification 1");// 66
                    string ItemNumber2 = item.Field<string>("Item Number 2");// 67
                    string PNo2 = item.Field<string>("P No. [Material 2]");// 68
                    string Specification2 = item.Field<string>("Specification 2");// 69
                    string ItemNumber3 = item.Field<string>("Item Number 3");// 70
                    string PNo3 = item.Field<string>("P No. [Material 3]");// 71
                    string Specification3 = item.Field<string>("Specification 3");//72
                    string Remarks1 = item.Field<string>("Remarks 1");//73
                    string Remarks2 = item.Field<string>("Remarks 2");//74
                    string Remarks3 = item.Field<string>("Remarks 3");//75
                    string WEPBy = item.Field<string>("WEP By");//76
                    string NatureofRevision = item.Field<string>("Nature of Revision");//77
                    string Notes = item.Field<string>("Notes");//78
                    string Remarks = item.Field<string>("Remarks");//79

                    string WSno = item.Field<string>("WS No.");//80
                    string WeldingProc = item.Field<string>("Welding Process");//81
                    string PQR = item.Field<string>("PQR No.");//82
                    string WeldingPosition = item.Field<string>("Welding Position");//83
                    string AWS = item.Field<string>("AWS Class");//84
                    string SizeofFillerMetal = item.Field<string>("Size of Filler Metal(mm)");//85
                    string Batch = item.Field<string>("Batch No");//86
                    string WMTestNo = item.Field<string>("WM Test No");//87
                    string FNo = item.Field<string>("F. No.");//88
                    string ANo = item.Field<string>("A. No.");//89
                    //string Sizemm = item.Field<string>("Size (mm)");//90
                    string FluxName = item.Field<string>("Flux Name");
                    string FluxBatch = item.Field<string>("Flux Batch No.");
                    //strings for TTS
                    string Manuf = string.Empty, Heat = string.Empty, Electrode = string.Empty, Dia = string.Empty, PeakCurrent = string.Empty, BackCurrent = string.Empty, PrePurgeTime = string.Empty, PreFusionTime = string.Empty, UpSlopeTime = string.Empty, PeakCurrentTime = string.Empty, BackGroundCurrentTime = string.Empty, DownSlopeTime = string.Empty, DecayCurrentTime = string.Empty;
                    string OverlapTime = string.Empty, PostPurgeTime = string.Empty, WireFeedSpeed = string.Empty, ArcLength = string.Empty, ArcRadius = string.Empty;
                    //string for Other 
                    string ShieldingGas = string.Empty, FlowRate = string.Empty, MinBeadLength = string.Empty, Interpass = string.Empty;

                    if (jointtype.ToLower() == "t#ts" || jointtype.ToLower() == "t#t/s")
                    {
                        Manuf = item.Field<string>("Manuf. / Chem.");
                        Heat = item.Field<string>("Heat No.");
                    }
                    string Current = item.Field<string>("Current");
                    string Polarity = item.Field<string>("Polarity");
                    string Amperes = item.Field<string>("Amperes (Amp.)");
                    string Voltage = item.Field<string>("Voltage (V)");
                    if (jointtype.ToLower() != "t#ts" && jointtype.ToLower() != "t#t/s")
                    {
                        ShieldingGas = item.Field<string>("Shielding Gas");
                        FlowRate = item.Field<string>("Flow Rate [LPM]");
                        MinBeadLength = item.Field<string>("Min. Bead Length[mm/min]");
                        Interpass = item.Field<string>("Interpass [Max.] °C");
                    }
                    string TravelSpeed = item.Field<string>("Travel Speed(mm/min)");
                    string Preheat = item.Field<string>("Preheat [min.] °C");
                    string NoofLayers = item.Field<string>("No. of Layers");

                    if (jointtype.ToLower() == "t#ts" || jointtype.ToLower() == "t#t/s")
                    {

                        Electrode = item.Field<string>("Electrode");
                        Dia = item.Field<string>("Dia");
                        PeakCurrent = item.Field<string>("Peak Current [Amp]");
                        BackCurrent = item.Field<string>("Back Current [Amp]");
                        PrePurgeTime = item.Field<string>("Pre Purge Time");
                        PreFusionTime = item.Field<string>("Pre Fusion Time Delay");
                        UpSlopeTime = item.Field<string>("Up Slope Time");
                        PeakCurrentTime = item.Field<string>("Peak Current Time");
                        BackGroundCurrentTime = item.Field<string>("Back Ground Current Time");
                        DownSlopeTime = item.Field<string>("Down Slope Time");
                        DecayCurrentTime = item.Field<string>("Decay Current Time");
                        OverlapTime = item.Field<string>("Overlap Time");
                        PostPurgeTime = item.Field<string>("Post Purge Time");
                        WireFeedSpeed = item.Field<string>("Wire Feed Speed [mm/min]");
                        ArcLength = item.Field<string>("Arc Length (Gap)(mm)");
                        ArcRadius = item.Field<string>("Arc Radius(mm)");
                    }
                    #endregion

                    #region Validate header
                    if ((!string.IsNullOrWhiteSpace(Documentno) || !string.IsNullOrWhiteSpace(seam) || !string.IsNullOrWhiteSpace(WeldDesignation)) && item.Field<string>("WS No.") != null && item.Field<string>("WS No.") != null)
                    {
                        List<string> lstExistingWPP = db.WPP010.Select(x => x.WPPNumber.ToLower()).ToList();
                        if (!string.IsNullOrWhiteSpace(wppno))
                        {
                            if (lstExistingWPP.Contains(wppno.ToLower()))
                            {
                                item["WPPNoErrorMsg"] = "WPP No is Already Exist"; isError = true;
                            }
                            else if (lstWppNo.Contains(wppno.ToLower()))
                            {
                                item["WPPNoErrorMsg"] = "WPP No is Already Exist in this sheet"; isError = true;
                            }

                            lstWppNo.Add(wppno.ToLower());
                        }
                        #region project details validaion
                        if (string.IsNullOrWhiteSpace(Documentno))
                        {
                            item["DocumentNoErrorMsg"] = "Document No is Required"; isError = true;
                        }
                        else
                        {
                            if (item.Field<string>("Document No.").Trim().Length > 50)
                            {
                                item["DocumentNoErrorMsg"] = "Document No have maximum limit of 1000 characters"; isError = true;
                            }
                        }

                        if (string.IsNullOrWhiteSpace(Seamno))
                        {
                            item["SeamNoErrorMsg"] = "Seam No is Required"; isError = true;
                        }
                        else
                        {
                            string[] seamArr = Seamno.Split(',');
                            seam = seamArr[0];
                            List<string> lstInvalidSeam = new List<string>();
                            foreach (var smno in seamArr)
                            {
                                if (!lstSeamNo.Any(i => i.seam.Trim().ToLower() == smno.Trim().ToLower()))
                                {
                                    lstInvalidSeam.Add(smno);
                                }
                            }
                            objModelWPP = SeamData(qProject.Trim(), seam);
                            if (lstInvalidSeam.Count() > 0)
                            {
                                item["SeamNoErrorMsg"] = "Seam No: " + string.Join(",", lstInvalidSeam) + " is does not belongs to Project No"; isError = true;
                            }
                        }


                        if (string.IsNullOrWhiteSpace(WeldDesignation))
                        {
                            item["WeldDesignationErrorMsg"] = "Weld Designation is Required"; isError = true;
                        }
                        else
                        {

                            if (!string.IsNullOrWhiteSpace(WeldDesignation))
                            {
                                if (WeldDesignation.Trim().Length > 50)
                                {
                                    item["WeldDesignationErrorMsg"] = "Weld Designation have maximum limit of 50 characters"; isError = true;
                                }
                                else if (!string.IsNullOrWhiteSpace(objModelWPP.WeldDesignation) ? objModelWPP.WeldDesignation.ToLower() != WeldDesignation.ToLower() : false)
                                {
                                    item["WeldDesignationErrorMsg"] = WeldDesignation + " is not valid"; ; isError = true;
                                }
                            }
                        }
                        #endregion

                        #region Welding processes
                        ///welding process 1
                        if (string.IsNullOrWhiteSpace(WeldingProcess1))
                        {
                            item["WeldingProcess1ErrorMsg"] = "Welding Process1 is Required"; isError = true;
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(getCategoryCode("Welding Process", "", objClsLoginInfo.Location.Trim(), WeldingProcess1)))
                            {
                                if (WeldingProcess1.Trim().Length > 15)
                                {
                                    item["WeldingProcess1ErrorMsg"] = "Welding Process have maximum limit of 15 characters"; isError = true;
                                }
                                else
                                {
                                    wp1 = WeldingProcess1;
                                }
                            }
                            else
                            {
                                item["WeldingProcess1ErrorMsg"] = WeldingProcess1 + " is not valid"; ; isError = true;
                            }
                            if (string.IsNullOrWhiteSpace(TypeofWeld1))
                            {
                                item["TypeofWeld1ErrorMsg"] = "Type of Weld 1 is Required"; isError = true;
                            }
                            else if (!lstTypeOfWeld.Contains(TypeofWeld1.ToLower()))
                            {
                                item["TypeofWeld1ErrorMsg"] = "Type of Weld 1 is not valid"; isError = true;
                            }
                        }

                        ///welding process 2
                        if (!string.IsNullOrWhiteSpace(WeldingProcess2))
                        {
                            if (!string.IsNullOrWhiteSpace(getCategoryCode("Welding Process", "", objClsLoginInfo.Location.Trim(), WeldingProcess2)))
                            {
                                if (WeldingProcess2.Trim().Length > 15)
                                {
                                    item["WeldingProcess2ErrorMsg"] = "Welding Process have maximum limit of 15 characters"; isError = true;
                                }
                                else
                                {
                                    wp2 = WeldingProcess2;
                                }
                            }
                            else
                            {
                                item["WeldingProcess2ErrorMsg"] = WeldingProcess2 + " is not valid"; ; isError = true;
                            }
                            if (string.IsNullOrWhiteSpace(TypeofWeld2))
                            {
                                item["TypeofWeld2ErrorMsg"] = "Type of Weld 2 is Required"; isError = true;
                            }
                            else if (!lstTypeOfWeld.Contains(TypeofWeld2.ToLower()))
                            {
                                item["TypeofWeld2ErrorMsg"] = "Type of Weld 2 is not valid"; isError = true;
                            }
                        }

                        ///welding process 3
                        if (!string.IsNullOrWhiteSpace(WeldingProcess3))
                        {
                            if (!string.IsNullOrWhiteSpace(getCategoryCode("Welding Process", "", objClsLoginInfo.Location.Trim(), WeldingProcess3)))
                            {
                                if (WeldingProcess3.Trim().Length > 15)
                                {
                                    item["WeldingProcess3ErrorMsg"] = "Welding Process have maximum limit of 15 characters"; isError = true;
                                }
                                else
                                {
                                    wp3 = WeldingProcess3;
                                }
                            }
                            else
                            {
                                item["WeldingProcess3ErrorMsg"] = WeldingProcess3 + " is not valid"; ; isError = true;
                            }
                            if (string.IsNullOrWhiteSpace(TypeofWeld3))
                            {
                                item["TypeofWeld3ErrorMsg"] = "Type of Weld 3 is Required"; isError = true;
                            }
                            else if (!lstTypeOfWeld.Contains(TypeofWeld3.ToLower()))
                            {
                                item["TypeofWeld3ErrorMsg"] = "Type of Weld 3 is not valid"; isError = true;
                            }
                        }
                        ///Drying Of Electrode
                        if ((WeldingProcess1 == "SMAW" || WeldingProcess2 == "SMAW" || WeldingProcess3 == "SMAW"))
                        {
                            if (string.IsNullOrWhiteSpace(DryingOfElectrode))
                            {
                                item["DryingofElectrodeErrorMsg"] = "Drying Of Electrode is Required"; isError = true;
                            }
                            else
                            {
                                if (DryingOfElectrode.Length > 100)
                                {
                                    item["DryingofElectrodeErrorMsg"] = "Drying Of Electrode  have maximum limit of 100 characters"; isError = true;
                                }
                            }
                        }
                        ///Drying of Flux
                        if ((WeldingProcess1 == "SMAW" || WeldingProcess2 == "SMAW" || WeldingProcess3 == "SMAW")
                            || (WeldingProcess1 == "SASC" || WeldingProcess2 == "SASC" || WeldingProcess3 == "SASC")
                            || (WeldingProcess1 == "ESW" || WeldingProcess2 == "ESW" || WeldingProcess3 == "ESW"))
                        {
                            if (string.IsNullOrWhiteSpace(DryingOfFlux))
                            {
                                item["DryingofFluxErrorMsg"] = "Drying Of Flux is Required"; isError = true;
                            }
                            else
                            {
                                if (DryingOfFlux.Length > 100)
                                {
                                    item["DryingofFluxErrorMsg"] = "Drying Of Flux  have maximum limit of 100 characters"; isError = true;
                                }
                            }
                        }

                        ///Item Number 1
                        if (string.IsNullOrWhiteSpace(ItemNumber1) && !string.IsNullOrWhiteSpace(objModelWPP.ItemNumber1))
                        {
                            item["ItemNumber1ErrorMsg"] = "Item Number 1 is Required"; isError = true;
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(objModelWPP.ItemNumber1) ? objModelWPP.ItemNumber1.ToLower() != ItemNumber1.ToLower() : false)
                            {
                                item["ItemNumber1ErrorMsg"] = "Item Number 1 is not belongs to Seam '" + seam + "'"; isError = true;
                            }
                            else
                            {
                                if (string.IsNullOrWhiteSpace(PNo1))
                                {
                                    item["PNo1ErrorMsg"] = "P No 1 is Required"; isError = true;
                                }
                                else
                                {
                                    if (PNo1.Length > 5)
                                    {
                                        item["PNo1ErrorMsg"] = "P No 1 have maximum limit of 5 characters"; isError = true;
                                    }
                                    //else if (!string.IsNullOrWhiteSpace(objModelWPP.PNumber1) ? objModelWPP.PNumber1.ToLower() != PNo1.Replace("\"", "").ToLower() : false)
                                    //{
                                    //    item["PNo1ErrorMsg"] = "P No 1  is not belongs to Seam '" + seam + "'"; isError = true;
                                    //}
                                }
                                if (string.IsNullOrWhiteSpace(Specification1))
                                {
                                    item["Specification1ErrorMsg"] = "Specification 1 is Required"; isError = true;
                                }
                                else
                                {
                                    if (Specification1.Length > 100)
                                    {
                                        item["Specification1ErrorMsg"] = "Specification 1 have maximum limit of 100 characters"; isError = true;
                                    }
                                    else if (!string.IsNullOrWhiteSpace(objModelWPP.Specification1) ? objModelWPP.Specification1.ToLower() != Specification1.ToLower() : false)
                                    {
                                        item["Specification1ErrorMsg"] = "Specification 1  is not belongs to Seam '" + seam + "'"; isError = true;
                                    }

                                }
                            }
                        }
                        ///Item Number 2
                        if (!string.IsNullOrWhiteSpace(objModelWPP.ItemNumber2) && string.IsNullOrWhiteSpace(ItemNumber2))
                        {
                            item["ItemNumber2ErrorMsg"] = "'" + seam + "' seam having Item Number 2,Please provide Item number"; isError = true;
                        }
                        else if (!string.IsNullOrWhiteSpace(ItemNumber2))
                        {

                            if (ItemNumber2.Length > 50)
                            {
                                item["ItemNumber2ErrorMsg"] = "Item Number 2 have maximum limit of 50 characters"; isError = true;
                            }
                            else if (!string.IsNullOrWhiteSpace(objModelWPP.ItemNumber2) ? objModelWPP.ItemNumber2.ToLower() != ItemNumber2.ToLower() : false)
                            {
                                item["ItemNumber1ErrorMsg"] = "Item Number 2 is not belongs to Seam '" + seam + "'"; isError = true;
                            }
                            else
                            {
                                if (string.IsNullOrWhiteSpace(PNo2))
                                {
                                    item["PNo2ErrorMsg"] = "P No 2 is Required"; isError = true;
                                }
                                else
                                {
                                    if (PNo2.Length > 5)
                                    {
                                        item["PNo2ErrorMsg"] = "P No 2 have maximum limit of 5 characters"; isError = true;
                                    }
                                    //else if (!string.IsNullOrWhiteSpace(objModelWPP.PNumber2) ? objModelWPP.PNumber2.ToLower() != PNo2.Replace("\"", "").ToLower() : false)
                                    //{
                                    //    item["PNo2ErrorMsg"] = "P No 2  is not belongs to Seam '" + seam + "'"; isError = true;
                                    //}
                                }

                                if (string.IsNullOrWhiteSpace(Specification2))
                                {
                                    item["Specification2ErrorMsg"] = "Specification 2 is Required"; isError = true;
                                }
                                else
                                {
                                    if (Specification2.Length > 100)
                                    {
                                        item["Specification2ErrorMsg"] = "Specification 2 have maximum limit of 100 characters"; isError = true;
                                    }
                                    else if (!string.IsNullOrWhiteSpace(objModelWPP.Specification2) ? objModelWPP.Specification2.ToLower() != Specification2.ToLower() : false)
                                    {
                                        item["Specification2ErrorMsg"] = "Specification 2  is not belongs to Seam '" + seam + "'"; isError = true;
                                    }
                                }
                            }
                        }
                        ///Item Number 3
                        if (!string.IsNullOrWhiteSpace(objModelWPP.ItemNumber3))
                        {
                            item["ItemNumber3ErrorMsg"] = "'" + seam + "' seam having Item Number 3,Please provide Item number"; isError = true;
                        }
                        else if (!string.IsNullOrWhiteSpace(ItemNumber3))
                        {
                            if (ItemNumber3.Length > 50)
                            {
                                item["ItemNumber3ErrorMsg"] = "Item Number 3 have maximum limit of 50 characters"; isError = true;
                            }
                            else if (!string.IsNullOrWhiteSpace(objModelWPP.ItemNumber3) ? objModelWPP.ItemNumber3.ToLower() != ItemNumber3.ToLower() : false)
                            {
                                item["ItemNumber3ErrorMsg"] = "Item Number 3 is not belongs to Seam '" + seam + "'"; isError = true;
                            }
                            else
                            {
                                if (string.IsNullOrWhiteSpace(PNo3))
                                {
                                    item["PNo3ErrorMsg"] = "P No3 is Required"; isError = true;
                                }
                                else
                                {
                                    if (PNo3.Length > 5)
                                    {
                                        item["PNo3ErrorMsg"] = "P No 3 have maximum limit of 5 characters"; isError = true;
                                    }
                                    //else if (!string.IsNullOrWhiteSpace(objModelWPP.PNumber3) ? objModelWPP.PNumber3.ToLower() != PNo3.Replace("\"", "").ToLower() : false)
                                    //{
                                    //    item["PNo3ErrorMsg"] = "P No 3  is not belongs to Seam '" + seam + "'"; isError = true;
                                    //}
                                }

                                if (string.IsNullOrWhiteSpace(Specification3))
                                {
                                    item["Specification3ErrorMsg"] = "Specification 3 is Required"; isError = true;
                                }
                                else
                                {
                                    if (Specification3.Length > 100)
                                    {
                                        item["Specification3ErrorMsg"] = "Specification 3 have maximum limit of 100 characters"; isError = true;
                                    }
                                    else if (!string.IsNullOrWhiteSpace(objModelWPP.Specification3) ? objModelWPP.Specification3.ToLower() != Specification3.ToLower() : false)
                                    {
                                        item["Specification3ErrorMsg"] = "Specification 3  is not belongs to Seam '" + seam + "'"; isError = true;
                                    }
                                }
                            }
                        }
                        ///Remarks
                        if (!string.IsNullOrWhiteSpace(Remarks1))
                        {
                            if (Remarks1.Length > 100)
                            {
                                item["Remarks1ErrorMsg"] = "Remarks 1 have maximum limit of 100 characters"; isError = true;
                            }
                        }
                        if (!string.IsNullOrWhiteSpace(Remarks2))
                        {
                            if (Remarks2.Length > 100)
                            {
                                item["Remarks2ErrorMsg"] = "Remarks 2 have maximum limit of 100 characters"; isError = true;
                            }
                        }
                        if (!string.IsNullOrWhiteSpace(Remarks3))
                        {
                            if (Remarks3.Length > 100)
                            {
                                item["Remarks3ErrorMsg"] = "Remarks 3 have maximum limit of 100 characters"; isError = true;
                            }
                        }
                        if (!string.IsNullOrWhiteSpace(Remarks))
                        {
                            if (Remarks.Length > 300)
                            {
                                item["RemarksErrorMsg"] = "Remarks have maximum limit of 300 characters"; isError = true;
                            }
                        }
                        ///WEP By
                        if (!string.IsNullOrWhiteSpace(WEPBy))
                        {
                            if (!lstWEPBy.Contains(WEPBy.ToLower()))
                            {
                                item["WEPByErrorMsg"] = "WEP By is not valid"; isError = true;
                            }
                        }

                        ///Nature of Revision 
                        if (string.IsNullOrWhiteSpace(NatureofRevision))
                        {
                            item["NatureofRevisionErrorMsg"] = "Nature of Revision is Required"; isError = true;
                        }
                        else if (NatureofRevision.Length > 100)
                        {
                            item["NatureofRevisionErrorMsg"] = "Nature of Revision have maximum limit of 100 characters"; isError = true;
                        }
                        ///Notes
                        if (string.IsNullOrWhiteSpace(Notes))
                        {
                            item["NotesErrorMsg"] = "Notes is Required"; isError = true;
                        }
                        else if (Notes.Length > 1000)
                        {
                            item["NotesErrorMsg"] = "Notes have maximum limit of 1000 characters"; isError = true;
                        }
                        #endregion
                        lstwp = new List<string>();

                        if (!string.IsNullOrWhiteSpace(wp1))
                        {
                            lstwp.Add(wp1.ToLower());
                        }
                        if (!string.IsNullOrWhiteSpace(wp2))
                        {
                            lstwp.Add(wp2.ToLower());
                        }
                        if (!string.IsNullOrWhiteSpace(wp3))
                        {
                            lstwp.Add(wp3.ToLower());
                        }
                    }
                    #endregion

                    if (item.Field<string>("WS No.") != "" && item.Field<string>("WS No.") != null)
                    {
                        #region Validate Common Columns in Lines
                        ///WeldingProcess1
                        if (string.IsNullOrWhiteSpace(WeldingProc))
                        {
                            item["WeldingProcessErrorMsg"] = "Welding process is Required"; isError = true;
                        }
                        else
                        {
                            if (WeldingProc.ToLower().Trim() == "back gouging")
                            {
                                isBackGouging = true;
                            }
                            else if (!isBackGouging && lstwp.Count > 0)
                            {
                                if (!lstwp.Contains(WeldingProc.ToLower().Trim()))
                                {
                                    item["WeldingProcessErrorMsg"] = "Welding process is not valid"; isError = true;
                                }
                            }

                        }
                        #region Validate if Weld proc not quals to back gouging
                        if (!isBackGouging)
                        {
                            ///PQR no
                            if (string.IsNullOrWhiteSpace(PQR))
                            {
                                item["PQRNoErrorMsg"] = "PQR is Required"; isError = true;
                            }
                            else if (PQR.Length > 20)
                            {
                                item["PQRNoErrorMsg"] = "PQR have maximum limit of 20 characters"; isError = true;
                            }

                            ///Weld position
                            if (string.IsNullOrWhiteSpace(WeldingProc))
                            { item["WeldingPositionErrorMsg"] = "Weling positions is Required"; isError = true; }
                            else
                            {
                                string[] WeldingPositionArray = WeldingPosition.ToLower().Split(',').ToArray();

                                List<string> lstInvalidRef = new List<string>();
                                foreach (var ite in WeldingPositionArray)
                                {
                                    if (!string.IsNullOrWhiteSpace(ite))
                                    {
                                        if (string.IsNullOrWhiteSpace(getCategoryCode("Welding Position", "", objClsLoginInfo.Location.Trim(), ite, false)))
                                        {
                                            lstInvalidRef.Add(ite);
                                            isError = true;
                                        }
                                    }
                                }
                                if (lstInvalidRef.Count() > 0)
                                {
                                    isError = true;
                                    item["WeldingPositionErrorMsg"] = string.Join(",", lstInvalidRef) + " does not belong to the Welding Position";

                                }
                            }

                            ///AWS CLass
                            var lstAWSClass = db.WPS002.Where(i => i.Location.Trim().Equals(objClsLoginInfo.Location.Trim(), StringComparison.OrdinalIgnoreCase)).ToList();
                            //.Select(i => new CategoryData { Value = i.Id.ToString(), CategoryDescription = i.AWSClass + "|" + i.FNumber + "|" + i.SFANo + "|" + i.ANumber + "|" + i.ConsumableSize }).
                            if (string.IsNullOrWhiteSpace(AWS))
                            {
                                item["AWSClassErrorMsg"] = "AWS Class is Required"; isError = true;
                            }
                            else
                            {
                                WPS002 objWPS002 = new WPS002();
                                ///AWS
                                if (!lstAWSClass.Any(x => x.AWSClass.Trim().ToLower() == AWS.Trim().ToLower()))
                                {
                                    item["AWSClassErrorMsg"] = "AWS Class is not valid"; isError = true;
                                }
                                else
                                {
                                    if (!string.IsNullOrWhiteSpace(SizeofFillerMetal))
                                    {
                                        objWPS002 = lstAWSClass.Where(x => x.AWSClass.Trim().ToLower() == AWS.Trim().ToLower() && x.Location.ToLower().Trim() == objClsLoginInfo.Location.ToLower().Trim() && x.ConsumableSize.ToLower().Trim() == SizeofFillerMetal.ToLower().Trim()).FirstOrDefault();
                                    }
                                    else
                                    {
                                        item["SizeofFillerMetal(mm)ErrorMsg"] = "Size of Filler Metal(mm) is required";
                                    }
                                }
                                if (!string.IsNullOrWhiteSpace(TravelSpeed)) //? objWPS002.TravelSpeed.Trim().ToLower() != TravelSpeed.Trim().ToLower() : false)
                                {
                                    if (TravelSpeed.Length > 10)
                                    {
                                        item["TravelSpeed(mm/min)ErrorMsg"] = "Travel Speed (mm/min) is have maximum limit of 10 characters"; isError = true;
                                    }
                                }
                                if (jointtype.ToLower() != "t#ts" && jointtype.ToLower() != "t#t/s")
                                {
                                    if (!string.IsNullOrWhiteSpace(MinBeadLength)) //? objWPS002.BeadLength.Trim().ToLower() != MinBeadLength.Trim().ToLower() : false)
                                    {
                                        if (MinBeadLength.Length > 10)
                                        {
                                            item["MinBeadLengthErrorMsg"] = "Min Bead Length(mm/min) is have maximum limit of 10 characters"; isError = true;
                                        }
                                    }
                                }
                                if (objWPS002 != null)
                                {
                                    if ((!string.IsNullOrWhiteSpace(objWPS002.ConsumableSize)) ? objWPS002.ConsumableSize.Trim().ToLower() != SizeofFillerMetal.Trim().ToLower() : false)
                                    {
                                        item["SizeofFillerMetal(mm)ErrorMsg"] = "Size of Filler Metal(mm) is not belongs to AWS Class"; isError = true;
                                    }
                                    if ((!string.IsNullOrWhiteSpace(objWPS002.FNumber)) ? objWPS002.FNumber.Trim().ToLower() != FNo.Trim().ToLower() : false)
                                    {
                                        item["FNoErrorMsg"] = "F No is not belongs to AWS Class"; isError = true;
                                    }
                                    if ((!string.IsNullOrWhiteSpace(objWPS002.ANumber)) ? objWPS002.ANumber.Trim().ToLower() != ANo.Trim().ToLower() : false)
                                    {
                                        item["ANoErrorMsg"] = "A No is not belongs to AWS Class"; isError = true;
                                    }

                                }
                                else
                                {
                                    item["SizeofFillerMetal(mm)ErrorMsg"] += "Size of Filler Metal(mm) is not maintained in AWS Class";
                                    //item["TravelSpeed(mm/min)ErrorMsg"] = "Travel Speed (mm/min) is not maintained in AWS Class";
                                    //if (jointtype.ToLower() != "t#ts" && jointtype.ToLower() != "t#t/s")
                                    //{
                                    //    item["MinBeadLengthErrorMsg"] = "A No is not maintained in AWS Class";
                                    //}
                                    item["ANoErrorMsg"] = "A No is not maintained in AWS Class";
                                    item["FNoErrorMsg"] = "F No is not maintained in AWS Class";
                                    isError = true;
                                }

                            }

                            ///BatchNo
                            if (!string.IsNullOrWhiteSpace(Batch) ? Batch.Length > 20 : false)
                            {
                                item["BatchNoErrorMsg"] = "Batch No have maximum limit of 20 characters"; isError = true;
                            }

                            ///WM Test No
                            if (!string.IsNullOrWhiteSpace(WMTestNo) ? WMTestNo.Length > 20 : false)
                            {
                                item["WMTestNoErrorMsg"] = "WM Test No have maximum limit of 20 characters"; isError = true;
                            }

                            ///Size(mm)
                            //if (!string.IsNullOrWhiteSpace(Sizemm))
                            //{
                            //    if (Sizemm.Length > 10)
                            //    {
                            //        item["Size(mm)ErrorMsg"] = Sizemm + " have maximum limit of 10 characters";
                            //        isError = true;
                            //    }
                            //}

                            ///Flux Name 
                            if (string.IsNullOrWhiteSpace(FluxName) && (WeldingProc == "SAW" || WeldingProc == "ESW" || WeldingProc == "SASC" || WeldingProc == "SAW-T" || WeldingProc == "ESSC"))
                            {
                                item["FluxNameErrorMsg"] = "Flux Name is required"; isError = true;
                            }
                            else if (FluxName.Length > 20)
                            {
                                item["FluxNameErrorMsg"] = "Flux Name have maximum limit of 20 characters"; isError = true;
                            }

                            //Flux Batch No
                            if (string.IsNullOrWhiteSpace(FluxBatch) && (WeldingProc == "SAW" || WeldingProc == "ESW" || WeldingProc == "SASC"))
                            {
                                item["FluxBatchNoErrorMsg"] = "Flux Batch No is required"; isError = true;
                            }
                            else if (FluxBatch.Length > 20)
                            {
                                item["FluxBatchNoErrorMsg"] = "Flux Batch No have maximum limit of 20 characters"; isError = true;
                            }


                            if (jointtype.ToLower() == "t#ts" || jointtype.ToLower() == "t#t/s")
                            {
                                ///Manuf./Chem.
                                if (string.IsNullOrWhiteSpace(Manuf))
                                {
                                    item["Manuf./Chem.ErrorMsg"] = "Manuf./Chem. is required"; isError = true;
                                }
                                else if (Manuf.Length > 10)
                                {
                                    item["Manuf./Chem.ErrorMsg"] = "Manuf./Chem. have maximum limit of 10 characters"; isError = true;
                                }
                                ///HeatNo.
                                if (string.IsNullOrWhiteSpace(Heat))
                                {
                                    item["HeatNo.ErrorMsg"] = "HeatNo is required"; isError = true;
                                }
                                else if (Heat.Length > 10)
                                {
                                    item["HeatNo.ErrorMsg"] = "HeatNo have maximum limit of 10 characters"; isError = true;
                                }
                            }


                            ///current and polarity
                            var lstCurrent = clsImplementationEnum.getCurrent().ToList();
                            List<string> lstTypeOfCurrent = (from lst in lstCurrent
                                                             select lst.ToString().ToLower()).ToList();

                            var lstPolarity = clsImplementationEnum.getPolarity().ToList();
                            List<string> lstTypeOfPolarity = (from lst in lstPolarity
                                                              select lst.ToString().ToLower()).ToList();

                            if (!string.IsNullOrWhiteSpace(Current))
                            {
                                if (!lstTypeOfCurrent.Contains(Current.ToLower()))
                                {
                                    item["CurrentErrorMsg"] = "Current is not valid"; isError = true;
                                }

                                if (Current == "DC" && !lstTypeOfPolarity.Contains(Polarity.ToLower()))
                                {
                                    item["PolarityErrorMsg"] = "Polarity is not valid"; isError = true;
                                }
                            }
                            ///Amperes
                            if (Amperes.Length > 10)
                            {
                                item["Amperes(Amp.)ErrorMsg"] = "Amperes have maximum limit of 10 characters"; isError = true;
                            }
                            ///Voltage
                            if (Voltage.Length > 10)
                            {
                                item["Voltage(V)ErrorMsg"] = "Voltage have maximum limit of 10 characters"; isError = true;
                            }
                            ///preheat no
                            int Preheatno;
                            if (!int.TryParse(Preheat, out Preheatno))
                            {
                                item["PreheatErrorMsg"] = "'" + Preheatno + "' is Not Valid, Please Enter Only Numeric Value"; isError = true;
                            }
                            else if (Preheat.Length > 10)
                            {
                                item["PreheatErrorMsg"] = "Preheat [min.] °C have maximum limit of 10 digit"; isError = true;
                            }
                            ///no of layers
                            if (string.IsNullOrWhiteSpace(NoofLayers))
                            {
                                item["No.ofLayersErrorMsg"] = "No of Layers is required"; isError = true;
                            }
                            else if (NoofLayers.Length > 10)
                            {
                                item["No.ofLayersErrorMsg"] = "No of Layers have maximum limit of 10 characters"; isError = true;
                            }
                            if (jointtype.ToLower() != "t#ts" && jointtype.ToLower() != "t#t/s")
                            {
                                #region Other columns except TTS
                                //Shielding Gas
                                if (!string.IsNullOrWhiteSpace(WeldingProc) ? (WeldingProc.ToUpper() == "GTAW" && WeldingProc.ToUpper() == "FCAW" && WeldingProc.ToUpper() == "GMAW") : false)
                                {
                                    if (string.IsNullOrWhiteSpace(ShieldingGas))
                                    {
                                        item["ShieldingGasErrorMsg"] = "Shielding Gas is required"; isError = true;
                                    }
                                    else if (string.IsNullOrWhiteSpace(getCategoryCode("Shielding Gas", "", objClsLoginInfo.Location.Trim(), item.Field<string>("Shielding Gas"), true)))
                                    {
                                        item["ShieldingGasErrorMsg"] = "Shielding Gas is not valid"; isError = true;
                                    }
                                }

                                //Flow Rate [LPM]
                                if (string.IsNullOrWhiteSpace(FlowRate) && (WeldingProc != "SMAW" && WeldingProc != "SAW" && WeldingProc != "SAW-T"
                                                                            && WeldingProc != "ESW" && WeldingProc != "ESSC" && WeldingProc != "SASC"))
                                {
                                    item["FlowRate[LPM]ErrorMsg"] = "FlowRate [LPM] is required"; isError = true;
                                }
                                else if (ShieldingGas.Length > 20)
                                {
                                    item["FlowRate[LPM]ErrorMsg"] = "FlowRate [LPM] have maximum limit of 20 characters"; isError = true;
                                }
                                //Interpass max C
                                int Interpassmax;
                                if (!int.TryParse(Interpass, out Interpassmax))
                                {
                                    item["InterpassErrorMsg"] = "'" + Interpass + "' is Not Valid, Please Enter Only Numeric Value"; isError = true;
                                }
                                else if (Interpass.Length > 10)
                                {
                                    item["InterpassErrorMsg"] = "Interpass [Max.] °C have maximum limit of 10 digit"; isError = true;
                                }
                                #endregion
                            }
                            else
                            {
                                #region TTS REGION for lines
                                ///electrode
                                if (string.IsNullOrWhiteSpace(Electrode))
                                {
                                    item["ElectrodeErrorMsg"] = "Electrode is required"; isError = true;
                                }
                                else if (Electrode.Length > 20)
                                {
                                    item["ElectrodeErrorMsg"] = "Electrode have maximum limit of 20 characters"; isError = true;
                                }

                                ///Dia
                                if (string.IsNullOrWhiteSpace(Dia))
                                {
                                    item["DiaErrorMsg"] = "Dia is required"; isError = true;
                                }
                                else if (Dia.Length > 20)
                                {
                                    item["DiaErrorMsg"] = "Dia have maximum limit of 20 characters"; isError = true;
                                }

                                ///Peak Current
                                if (string.IsNullOrWhiteSpace(PeakCurrent))
                                {
                                    item["PeakErrorMsg"] = "Peak Current is required"; isError = true;
                                }
                                else if (PeakCurrent.Length > 20)
                                {
                                    item["PeakErrorMsg"] = "Peak Current have maximum limit of 20 characters"; isError = true;
                                }

                                ///Peak Current
                                if (string.IsNullOrWhiteSpace(PeakCurrent))
                                {
                                    item["PeakCurrent[Amp]ErrorMsg"] = "Peak Current[Amp] is required"; isError = true;
                                }
                                else if (PeakCurrent.Length > 20)
                                {
                                    item["PeakCurrent[Amp]ErrorMsg"] = "Peak Current[Amp] have maximum limit of 20 characters"; isError = true;
                                }

                                ///Back Current
                                if (string.IsNullOrWhiteSpace(BackCurrent))
                                {
                                    item["BackCurrent[Amp]ErrorMsg"] = "Back Current[Amp] is required"; isError = true;
                                }
                                else if (BackCurrent.Length > 20)
                                {
                                    item["BackCurrent[Amp]ErrorMsg"] = "Back Current[Amp] have maximum limit of 20 characters"; isError = true;
                                }
                                ///Pre Purge Time
                                if (string.IsNullOrWhiteSpace(BackCurrent))
                                {
                                    item["PrePurgeTimeErrorMsg"] = "Pre Purge Time is required"; isError = true;
                                }
                                else if (BackCurrent.Length > 20)
                                {
                                    item["PrePurgeTimeErrorMsg"] = "Pre Purge Time have maximum limit of 20 characters"; isError = true;
                                }
                                ///Pre Fusion Time Delay
                                if (string.IsNullOrWhiteSpace(PreFusionTime))
                                {
                                    item["PreFusionTimeDelayErrorMsg"] = "Pre Fusion Time Delay is required"; isError = true;
                                }
                                else if (PreFusionTime.Length > 20)
                                {
                                    item["PreFusionTimeDelayErrorMsg"] = "Pre Fusion Time Delay have maximum limit of 20 characters"; isError = true;
                                }
                                ///Pre Fusion Time Delay
                                if (string.IsNullOrWhiteSpace(UpSlopeTime))
                                {
                                    item["UpSlopeTimeErrorMsg"] = "Up Slope Time is required"; isError = true;
                                }
                                else if (UpSlopeTime.Length > 20)
                                {
                                    item["UpSlopeTimeErrorMsg"] = "Up Slope Time have maximum limit of 20 characters"; isError = true;
                                }
                                ///Peak Current Time
                                if (string.IsNullOrWhiteSpace(PeakCurrentTime))
                                {
                                    item["PeakCurrentTimeErrorMsg"] = "Peak Current Time is required"; isError = true;
                                }
                                else if (PeakCurrentTime.Length > 20)
                                {
                                    item["PeakCurrentTimeErrorMsg"] = "Peak Current Time have maximum limit of 20 characters"; isError = true;
                                }
                                ///Back Ground Current Time
                                if (string.IsNullOrWhiteSpace(BackGroundCurrentTime))
                                {
                                    item["BackGroundCurrentTimeErrorMsg"] = "Back Ground Current Time is required"; isError = true;
                                }
                                else if (BackGroundCurrentTime.Length > 20)
                                {
                                    item["BackGroundCurrentTimeErrorMsg"] = "Back Ground Current Time have maximum limit of 20 characters"; isError = true;
                                }
                                ///Down Slope Time
                                if (string.IsNullOrWhiteSpace(DownSlopeTime))
                                {
                                    item["DownSlopeTimeErrorMsg"] = "Down Slope Time is required"; isError = true;
                                }
                                else if (DownSlopeTime.Length > 20)
                                {
                                    item["DownSlopeTimeErrorMsg"] = "Down Slope Time have maximum limit of 20 characters"; isError = true;
                                }
                                ///Decay Current Time
                                if (string.IsNullOrWhiteSpace(DecayCurrentTime))
                                {
                                    item["DecayCurrentTimeErrorMsg"] = "Decay Current Time is required"; isError = true;
                                }
                                else if (DecayCurrentTime.Length > 20)
                                {
                                    item["DecayCurrentTimeErrorMsg"] = "Decay Current Time have maximum limit of 20 characters"; isError = true;
                                }
                                ///Overlap Time
                                if (string.IsNullOrWhiteSpace(OverlapTime))
                                {
                                    item["OverlapTimeErrorMsg"] = "Overlap Time is required"; isError = true;
                                }
                                else if (OverlapTime.Length > 20)
                                {
                                    item["OverlapTimeErrorMsg"] = "Overlap Time have maximum limit of 20 characters"; isError = true;
                                }
                                ///Post Purge Time
                                if (string.IsNullOrWhiteSpace(PostPurgeTime))
                                {
                                    item["PostPurgeTimeErrorMsg"] = "Post Purge Time is required"; isError = true;
                                }
                                else if (PostPurgeTime.Length > 20)
                                {
                                    item["PostPurgeTimeErrorMsg"] = "Post Purge Time have maximum limit of 20 characters"; isError = true;
                                }
                                ///Wire Feed Speed[mm/min]
                                if (string.IsNullOrWhiteSpace(WireFeedSpeed))
                                {
                                    item["WireFeedSpeed[mm/min]ErrorMsg"] = "Wire Feed Speed[mm/min] is required"; isError = true;
                                }
                                else if (WireFeedSpeed.Length > 20)
                                {
                                    item["WireFeedSpeed[mm/min]ErrorMsg"] = "Wire Feed Speed[mm/min] have maximum limit of 20 characters"; isError = true;
                                }
                                ///Arc Length(Gap)(mm)
                                if (string.IsNullOrWhiteSpace(ArcLength))
                                {
                                    item["ArcLength(Gap)(mm)ErrorMsg"] = "Arc Length(Gap)(mm) is required"; isError = true;
                                }
                                else if (ArcLength.Length > 20)
                                {
                                    item["ArcLength(Gap)(mm)ErrorMsg"] = "Arc Length(Gap)(mm) have maximum limit of 20 characters"; isError = true;
                                }
                                ///Arc Length(Gap)(mm)
                                if (string.IsNullOrWhiteSpace(ArcRadius))
                                {
                                    item["ArcRadius(mm)ErrorMsg"] = "Arc Radius(mm) is required"; isError = true;
                                }
                                else if (ArcRadius.Length > 20)
                                {
                                    item["ArcRadius(mm)ErrorMsg"] = "Arc Radius(mm) have maximum limit of 20 characters"; isError = true;
                                }
                                #endregion
                            }
                        }
                        #endregion

                        #endregion
                    }
                    else if (!string.IsNullOrWhiteSpace(WeldingPosition) || !string.IsNullOrWhiteSpace(WeldingProc) || !string.IsNullOrWhiteSpace(AWS))
                    {
                        item["WSNoErrorMsg"] = "WS No is Required"; isError = true;
                    }

                }
                #endregion
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            DataSet ds = new DataSet();
            ds.Tables.Add(dtLinesExcel);
            return ds;
        }

        public bool isCodeExists()
        {
            bool Flag = false;
            return Flag;
        }
        #endregion
    }
}