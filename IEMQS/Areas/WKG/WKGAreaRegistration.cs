﻿using System.Web.Mvc;

namespace IEMQS.Areas.WKG
{
    public class WKGAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "WKG";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "WKG_default",
                "WKG/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}