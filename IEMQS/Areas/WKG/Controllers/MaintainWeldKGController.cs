﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQSImplementation;
using IEMQS.Models;
using IEMQS.Areas.WKG.Models;
using IEMQS.Areas.Utility.Models;
using static IEMQS.Areas.PLN.Controllers.MaintainLockingCleatController;
using System.Globalization;

namespace IEMQS.Areas.WKG.Controllers
{
    public class MaintainWeldKGController : clsBase
    {
        #region Utility
        [NonAction]
        public static string GenerateGridButton(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";


            htmlControl = "<a id='" + inputID + "' name='" + inputID + "' class='btn btn-outline btn-circle btn-sm blue' " + onClickEvent + " ><i class='" + className + "'></i> " + buttonName + "</a>";

            //htmlControl = "<i  data-modal='' id='" + inputID + "' name='" + inputID + "' style='cursor:Pointer;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";

            return htmlControl;
        }

        public string HTMLActionString(int rowId, string status, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";
            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick=" + onClickMethod : "";

            if (!string.Equals(status, clsImplementationEnum.CSRStatus.SendForDLApproval.GetStringValue()))
            {
                if (string.Equals(status, clsImplementationEnum.CSRStatus.ApprovedByDL.GetStringValue()))
                {
                    htmlControl = "";// "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
                }
                else
                {
                    htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
                }
            }

            return htmlControl;
        }

        [NonAction]
        public static string GenerateHTMLTextboxOnChanged(int rowId, string columnName, string columnValue = "", string onChangeMethod = "", bool isReadOnly = false, string inputStyle = "", bool disabled = false, string maxlength = "", string className = "")
        {
            string htmlControl = "";

            string inputID = columnName + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            className = "form-control " + className;
            string MaxCharcters = !string.IsNullOrEmpty(maxlength) ? "maxlength='" + maxlength + "'" : "";
            string onBlurEvent = !string.IsNullOrEmpty(onChangeMethod) ? "onchange='" + onChangeMethod + "'" : "";

            htmlControl = "<input type='text' " + (isReadOnly ? "disabled='disabled'" : "") + " id='" + inputID + "' " + MaxCharcters + " value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + (disabled ? "disabled" : "") + " />";

            return htmlControl;
        }
        #endregion

        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        // GET: WKG/MaintainWeldKG
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetWeldKGHeaderGridDataPartial()//string status)
        {
            //ViewBag.status = status;
            return PartialView("_GetWeldKGHeaderGridDataPartial");
        }
        public ActionResult ShowHistoryTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();

            model.TimelineTitle = "WKG TimeLine";
            model.Title = "PDinDoc";
            if (HeaderId > 0)
            {

                WKG001_Log objWKG001_Log = db.WKG001_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                model.CreatedBy = objWKG001_Log.CreatedBy != null ? Manager.GetUserNameFromPsNo(objWKG001_Log.CreatedBy) : null;
                model.CreatedOn = objWKG001_Log.CreatedOn;
                model.EditedBy = objWKG001_Log.EditedBy != null ? Manager.GetUserNameFromPsNo(objWKG001_Log.EditedBy) : null;
                model.EditedOn = objWKG001_Log.EditedOn;
                model.SubmittedBy = objWKG001_Log.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objWKG001_Log.SubmittedBy) : null;
                model.SubmittedOn = objWKG001_Log.SubmittedOn;

                model.ApprovedBy = objWKG001_Log.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objWKG001_Log.ApprovedBy) : null;
                model.ApprovedOn = objWKG001_Log.ApprovedOn;

            }
            else
            {

                WKG001_Log objWKG001_Log = db.WKG001_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                model.CreatedBy = objWKG001_Log.CreatedBy != null ? Manager.GetUserNameFromPsNo(objWKG001_Log.CreatedBy) : null;
                model.CreatedOn = objWKG001_Log.CreatedOn;
                model.EditedBy = objWKG001_Log.EditedBy != null ? Manager.GetUserNameFromPsNo(objWKG001_Log.EditedBy) : null;
                model.EditedOn = objWKG001_Log.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
        public ActionResult ShowTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();
            model.Title = "PDinDoc";
            model.TimelineTitle = "WKG Timeline";

            if (HeaderId > 0)
            {
                WKG001 objWKG001 = db.WKG001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.CreatedBy = objWKG001.CreatedBy != null ? Manager.GetUserNameFromPsNo(objWKG001.CreatedBy) : null;
                model.CreatedOn = objWKG001.CreatedOn;
                model.EditedBy = objWKG001.EditedBy != null ? Manager.GetUserNameFromPsNo(objWKG001.EditedBy) : null;
                model.EditedOn = objWKG001.EditedOn;

                // model.SubmittedBy = PMB001_Log.SendToCompiledOn != null ? Manager.GetUserNameFromPsNo(PMB001_Log.SendToCompiledOn) : null;
                //model.SubmittedOn = PMB001_Log.SendToCompiledOn;
                model.SubmittedBy = objWKG001.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objWKG001.SubmittedBy) : null;
                model.SubmittedOn = objWKG001.SubmittedOn;
                //model.CompiledBy = PMB001_Log.CompiledBy != null ? Manager.GetUserNameFromPsNo(PMB001_Log.CompiledBy) : null;
                // model.CompiledOn = PMB001_Log.CompiledOn;
                model.ApprovedBy = objWKG001.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objWKG001.ApprovedBy) : null;
                model.ApprovedOn = objWKG001.ApprovedOn;

            }
            else
            {
                WKG001 objWKG001 = db.WKG001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.CreatedBy = objWKG001.CreatedBy != null ? Manager.GetUserNameFromPsNo(objWKG001.CreatedBy) : null;
                model.CreatedOn = objWKG001.CreatedOn;
                model.EditedBy = objWKG001.EditedBy != null ? Manager.GetUserNameFromPsNo(objWKG001.EditedBy) : null;
                model.EditedOn = objWKG001.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
        public ActionResult loadHeaderDataTable(JQueryDataTableParamModel param)//, string status)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;

                string whereCondition = "1=1";
                //whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "wps10.BU", "wps10.Location");

                //if (status.ToUpper() == "PENDING")
                //{
                //    whereCondition += " and status in('" + clsImplementationEnum.WKGHeaderStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.WKGHeaderStatus.Returned.GetStringValue() + "')";
                //}

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    //whereCondition += " and (bcom2.t_desc like '%" + param.sSearch + "%' or lcom2.t_desc like '%" + param.sSearch + "%' or Location like '%" + param.sSearch + "%' or wps10.WPSNumber like '%" + param.sSearch + "%' or WPSRevNo like '%" + param.sSearch + "%'  or Status like '%" + param.sSearch + "%' or wps10.CreatedBy like '%" + param.sSearch + "%')";
                    whereCondition += Helper.MakeDatatableSearchCondition(new string[] { "wkg1.Project", "com1.t_dsca", "Product", "ProcessLicensor", "wkg1.Customer", "lcom6.t_nama", "Document", "Status", "wkg1.RevNo" }, param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }


                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstHeader = db.SP_WKG_GETHEADER(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from h in lstHeader
                          select new[] {

                        h.Project,//db.COM001.Where(i=>i.t_cprj==h.Project).Select(i=>i.t_dsca).FirstOrDefault(),//h.Project,
                        h.Product,
                        h.ProcessLicensor,
                        h.Customer,
                        h.Document,
                        h.Status,
                        Convert.ToDateTime(h.CDD).ToString("dd/MM/yyyy"),
                        "<span>R</span>" +Convert.ToString(h.RevNo),
                        Convert.ToString(h.SubmittedBy),
                        Convert.ToString(Convert.ToDateTime(h.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(h.SubmittedOn).ToString("dd/MM/yyyy")),
                        Convert.ToString(h.ApprovedBy),
                        Convert.ToString(Convert.ToDateTime(h.ApprovedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(h.ApprovedOn).ToString("dd/MM/yyyy")),
                        "<nobr><center>"+
                           "<a class='iconspace' href='"+WebsiteURL+"/WKG/MaintainWeldKG/AddWeldKGHeader?headerId=" + Convert.ToString(h.HeaderId) + "'><i class='fa fa-eye'></i></a>"+
                            Helper.GenerateActionIcon(h.HeaderId,"Delete","Delete Record","fa fa-trash-o", "DeleteDocument("+ h.HeaderId +",'/WKG/MaintainWeldKG/DeleteHeader', {headerid:"+h.HeaderId+"}, 'tblWeldKGHeader')","",  (( h.RevNo >0 && h.Status.ToLower() != clsImplementationEnum.CommonStatus.SendForApprovel.GetStringValue().ToLower()) || ( h.RevNo == 0 && h.Status.ToLower() == clsImplementationEnum.CommonStatus.Approved.GetStringValue().ToLower()) ) ? false:true) +
                             "<i class ='iconspace fa fa-clock-o' title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/WKG/MaintainWeldKG/ShowTimeline?HeaderID=" + Convert.ToInt32(h.HeaderId) + "')></i>"+
                           (h.RevNo>0?"<i title='History' class='iconspace fa fa-history' onClick='GetHistoryDataPartial("+h.HeaderId+")'></i> ":"<i title='History' class='disabledicon fa fa-history' ></i>" )+
                            "<i title='Print'  class='iconspace fa fa-print' onClick='PrintReport("+h.HeaderId+")'></i>"+
                           "</center></nobr>",
                        Convert.ToString(h.HeaderId),

                        //"<center><a title='View' href='/WKG/MaintainWeldKG/AddWeldKGHeader?headerId=" + Convert.ToString(h.HeaderId) + "'><i style='margin-left:5px;' class='fa fa-eye'></i></a><i style='margin-left:5px;cursor:pointer;' class='fa fa-print' onClick='PrintReport("+h.HeaderId+")'></i><a title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/WKG/MaintainWeldKG/ShowTimeline?HeaderID=" + Convert.ToInt32(h.HeaderId) + "')><i style='margin-left:5px;cursor:pointer;' class='fa fa-clock-o'></i></a>"
                        //+"  "+(h.RevNo>0?"<i title='History' style='margin-left:5px;cursor:pointer;' class='fa fa-history' onClick='GetHistoryDataPartial("+h.HeaderId+")'></i>":"</center>"),
                        //GetHTMLString(h.HeaderId)
                    };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult DeleteHeader(int headerid)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                objResponseMsg = Manager.DeletePDINDocument(headerid, clsImplementationEnum.PlanList.Weld_KG_Plan);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult AddWeldKGHeader(int? headerId)
        {
            WKG001 objWKG001 = new WKG001();
            List<string> lstProCat = clsImplementationEnum.getProductCategory().ToList();
            ViewBag.Product = lstProCat.Select(i => new CategoryData { Value = i.ToString(), CategoryDescription = i.ToString() }).ToList();
            int count = 0;

            if (headerId > 0)
            {
                objWKG001 = db.WKG001.Where(i => i.HeaderId == headerId).FirstOrDefault();
                count = db.WKG002.Where(i => i.HeaderId == headerId).ToList().Count;
                ViewBag.WeldKGLinesCount = count;
                ViewBag.Project = db.COM001.Where(i => i.t_cprj.Equals(objWKG001.Project, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_cprj + "-" + i.t_dsca).FirstOrDefault();
                ViewBag.Approver = db.COM003.Where(i => i.t_psno.Equals(objWKG001.ApprovedBy, StringComparison.OrdinalIgnoreCase) && i.t_actv == 1).Select(i => i.t_psno + "-" + i.t_name).FirstOrDefault();

                var PlanningDinID = db.PDN002.Where(x => x.RefId == headerId && x.DocumentNo == objWKG001.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                ViewBag.PlanningDinID = PlanningDinID;

                ViewBag.IsReviseBtnEnabled = Manager.IsReviseEnabled(PlanningDinID, objWKG001.HeaderId, objWKG001.Document);
                ViewBag.DocMessage = clsImplementationMessage.CommonMessages.DocMessage.ToString();
            }
            else
            {
                ViewBag.WeldKGLinesCount = count;
                objWKG001.Status = clsImplementationEnum.WKGHeaderStatus.Draft.GetStringValue();
                objWKG001.RevNo = 0;
            }
            return View(objWKG001);
        }

        [HttpPost]
        public ActionResult SaveHeader(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            WKG001 objWKG001 = new WKG001();
            bool savedLines = false;
            try
            {
                if (fc != null)
                {
                    int headerId = Convert.ToInt32(fc["HeaderId"]);
                    bool overWriteunmodifiedData = Convert.ToBoolean(fc["overWriteunmodifiedData"]);
                    if (headerId > 0)
                    {
                        #region Modify Existing Weld KG Header
                        objWKG001 = db.WKG001.Where(i => i.HeaderId == headerId).FirstOrDefault();

                        //objWKG001.Customer = fc["CustCode"];
                        //objWKG001.RevNo = Convert.ToInt32(fc["RevNo"]);
                        //objWKG001.Status = clsImplementationEnum.WKGHeaderStatus.Draft.GetStringValue();
                        //objWKG001.CDD = Convert.ToDateTime(fc["CDD"]);
                        objWKG001.Product = fc["Product"];
                        objWKG001.ReviseRemark = fc["ReviseRemark"];
                        objWKG001.ProcessLicensor = fc["ProcessLicensor"];
                        objWKG001.ApprovedBy = fc["ApprovedBy"];
                        objWKG001.EditedBy = objClsLoginInfo.UserName;
                        objWKG001.EditedOn = DateTime.Now;

                        if (string.Equals(objWKG001.Status, clsImplementationEnum.WKGHeaderStatus.Approved.GetStringValue()))
                        {
                            objWKG001.Status = clsImplementationEnum.WKGHeaderStatus.Draft.GetStringValue();
                            if (db.WKG002.Where(i => i.HeaderId == objWKG001.HeaderId).Any())
                            {
                                db.WKG002.Where(i => i.HeaderId == objWKG001.HeaderId).ToList().ForEach(i => i.RevNo = objWKG001.RevNo + 1);
                            }
                            objWKG001.RevNo += 1;
                        }

                        db.SaveChanges();
                        Manager.UpdatePDN002(objWKG001.HeaderId, objWKG001.Status, objWKG001.RevNo, objWKG001.Project, objWKG001.Document);
                        #endregion
                    }
                    else
                    {
                        #region Add New Weld KG Header
                        objWKG001.Project = fc["Project"];
                        objWKG001.ReviseRemark = fc["ReviseRemark"];
                        //Static Document No ,as told by saitsh pawar
                        objWKG001.Document = fc["Document"];//"H02-"+ fc["Project"] + "-PL07";
                        objWKG001.Customer = fc["CustCode"];
                        objWKG001.RevNo = Convert.ToInt32(fc["RevNo"]);
                        objWKG001.Status = clsImplementationEnum.WKGHeaderStatus.Draft.GetStringValue();
                        objWKG001.CDD = Convert.ToDateTime(fc["CDD"]);
                        objWKG001.Product = fc["Product"];
                        objWKG001.ProcessLicensor = fc["ProcessLicensor"];
                        objWKG001.ApprovedBy = fc["ApprovedBy"];
                        objWKG001.CreatedBy = objClsLoginInfo.UserName;
                        objWKG001.CreatedOn = DateTime.Now;

                        db.WKG001.Add(objWKG001);
                        db.SaveChanges();
                        Manager.UpdatePDN002(objWKG001.HeaderId, objWKG001.Status, objWKG001.RevNo, objWKG001.Project, objWKG001.Document);
                        #endregion
                        //objResponseMsg.Key = true;
                        //objResponseMsg.Value = clsImplementationMessage.WKGHeader.Save.ToString();
                        //objResponseMsg.HeaderId = objWKG001.HeaderId;
                        //objResponseMsg.Status = objWKG001.Status;
                        //objResponseMsg.RevNo = Convert.ToString(objWKG001.RevNo);
                    }

                    savedLines = SaveWeldKGLines(objWKG001.HeaderId, objWKG001.Project, overWriteunmodifiedData);

                    Manager.UpdatePDN002(objWKG001.HeaderId, objWKG001.Status, objWKG001.RevNo, objWKG001.Project, objWKG001.Document);

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.WKGHeader.Save.ToString();
                    objResponseMsg.HeaderId = objWKG001.HeaderId;
                    objResponseMsg.HeaderStatus = objWKG001.Status;
                    objResponseMsg.RevNo = Convert.ToString(objWKG001.RevNo);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult SaveHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            WKG001 objWKG001 = new WKG001();
            try
            {
                if (fc != null)
                {
                    int headerId = Convert.ToInt32(fc["HeaderId"]);
                    bool overWriteunmodifiedData = Convert.ToBoolean(fc["overWriteunmodifiedData"]);
                    if (headerId > 0)
                    {
                        #region Modify Existing Weld KG Header
                        objWKG001 = db.WKG001.Where(i => i.HeaderId == headerId).FirstOrDefault();
                        objWKG001.Product = fc["Product"];
                        objWKG001.ReviseRemark = fc["ReviseRemark"];
                        objWKG001.ProcessLicensor = fc["ProcessLicensor"];
                        objWKG001.ApprovedBy = fc["ApprovedBy"];
                        objWKG001.EditedBy = objClsLoginInfo.UserName;
                        objWKG001.EditedOn = DateTime.Now;

                        if (string.Equals(objWKG001.Status, clsImplementationEnum.WKGHeaderStatus.Approved.GetStringValue()))
                        {
                            objWKG001.Status = clsImplementationEnum.WKGHeaderStatus.Draft.GetStringValue();
                            if (db.WKG002.Where(i => i.HeaderId == objWKG001.HeaderId).Any())
                            {
                                db.WKG002.Where(i => i.HeaderId == objWKG001.HeaderId).ToList().ForEach(i => i.RevNo = objWKG001.RevNo + 1);
                            }
                            objWKG001.RevNo += 1;
                        }

                        db.SaveChanges();
                        Manager.UpdatePDN002(objWKG001.HeaderId, objWKG001.Status, objWKG001.RevNo, objWKG001.Project, objWKG001.Document);
                        #endregion
                    }
                    else
                    {
                        #region Add New Weld KG Header
                        objWKG001.Project = fc["Project"];
                        objWKG001.ReviseRemark = fc["ReviseRemark"];
                        //Static Document No ,as told by saitsh pawar
                        objWKG001.Document = fc["Document"];//"H02-"+ fc["Project"] + "-PL07";
                        objWKG001.Customer = fc["CustCode"];
                        objWKG001.RevNo = Convert.ToInt32(fc["RevNo"]);
                        objWKG001.Status = clsImplementationEnum.WKGHeaderStatus.Draft.GetStringValue();
                        objWKG001.CDD = Convert.ToDateTime(fc["CDD"]);
                        objWKG001.Product = fc["Product"];
                        objWKG001.ProcessLicensor = fc["ProcessLicensor"];
                        objWKG001.ApprovedBy = fc["ApprovedBy"];
                        objWKG001.CreatedBy = objClsLoginInfo.UserName;
                        objWKG001.CreatedOn = DateTime.Now;

                        db.WKG001.Add(objWKG001);
                        db.SaveChanges();
                        Manager.UpdatePDN002(objWKG001.HeaderId, objWKG001.Status, objWKG001.RevNo, objWKG001.Project, objWKG001.Document);
                        #endregion

                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.WKGHeader.Save.ToString();
                    objResponseMsg.HeaderId = objWKG001.HeaderId;
                    objResponseMsg.HeaderStatus = objWKG001.Status;
                    objResponseMsg.RevNo = Convert.ToString(objWKG001.RevNo);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult ReviseHeader(int strHeaderId, string strRemarks)
        {
            WKG001 objWKG001 = new WKG001();
            CustomResponceMsg objResponseMsg = new CustomResponceMsg();
            try
            {
                if (strHeaderId > 0)
                {
                    objWKG001 = db.WKG001.Where(i => i.HeaderId == strHeaderId).FirstOrDefault();
                    objWKG001.Status = clsImplementationEnum.WKGHeaderStatus.Draft.GetStringValue();
                    objWKG001.ReviseRemark = strRemarks;
                    objWKG001.EditedBy = objClsLoginInfo.UserName;
                    objWKG001.EditedOn = DateTime.Now;
                    objWKG001.SubmittedBy = null;
                    objWKG001.SubmittedOn = null;
                    if (db.WKG002.Where(i => i.HeaderId == strHeaderId).Any())
                    {
                        db.WKG002.Where(i => i.HeaderId == strHeaderId).ToList().ForEach(i => i.RevNo = objWKG001.RevNo + 1);
                    }
                    objWKG001.RevNo += 1;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Revision;
                    objResponseMsg.HeaderID = objWKG001.HeaderId;
                    objResponseMsg.Status = objWKG001.Status;
                    objResponseMsg.rev = (objWKG001.RevNo);
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable;
                }
            }
            catch (Exception ex)
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public bool SaveWeldKGLines(int headerId, string project, bool overWriteunmodifiedData)
        {
            //clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            bool savedLines = false;
            WKG001 objWKG001 = new WKG001();
            WKG002 objWKG002 = new WKG002();
            List<string> lstExistingSeams = new List<string>();
            List<string> lstSeamAdded = new List<string>();
            try
            {
                if (headerId > 0)
                {
                    objWKG001 = db.WKG001.Where(i => i.HeaderId == headerId).FirstOrDefault();
                    if (!string.IsNullOrEmpty(objWKG001.Project))
                    {
                        List<QMS012> lstQMS012 = new List<QMS012>();
                        string status = clsImplementationEnum.SeamListStatus.Approved.GetStringValue();
                        int PlanningDinID = db.PDN002.Where(x => x.RefId == headerId && x.DocumentNo == objWKG001.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                        string location = db.PDN001.Where(x => x.HeaderId == PlanningDinID).Select(x => x.Location).FirstOrDefault();
                        lstQMS012 = db.QMS012.Where(i => i.Location.Equals(location, StringComparison.OrdinalIgnoreCase) && i.Project.Equals(objWKG001.Project, StringComparison.OrdinalIgnoreCase) && i.Status.Equals(status, StringComparison.OrdinalIgnoreCase) && !i.SeamNo.StartsWith("AW") && !i.SeamNo.StartsWith("NP") && !i.SeamNo.StartsWith("PW") && !i.SeamNo.StartsWith("BW") && !i.SeamNo.StartsWith("HF") && !i.SeamNo.StartsWith("BU")).ToList();
                        //lstQMS012 = db.QMS012.Where(i => i.Project.Equals(objWKG001.Project, StringComparison.OrdinalIgnoreCase) && i.Status.Equals(status, StringComparison.OrdinalIgnoreCase)).ToList();
                        List<WKG002> lstWKG002 = new List<WKG002>();
                        if (overWriteunmodifiedData)
                        {
                            var lstWKG2 = db.WKG002.Where(i => i.HeaderId == headerId && !i.IsManualLine).ToList();
                            if (lstWKG2.Any())
                            {
                                db.WKG002.RemoveRange(lstWKG2);
                                db.SaveChanges();
                            }
                            lstWKG002 = GenerateLines(lstQMS012, headerId);
                        }
                        else
                        {
                            var lstWKG2 = db.WKG002.Where(i => i.HeaderId == headerId).ToList();
                            //if (lstWKG2.Where(i => i.IsManuallyModified).Any())
                            //{
                            //    lstExistingSeams.AddRange(lstWKG2.Select(i => i.SeamNo));
                            //}
                            if (!lstWKG2.Any())
                            {
                                lstWKG002 = GenerateLines(lstQMS012, headerId);
                            }
                            else
                            {
                                if (lstWKG2.Where(i => !i.IsManuallyModified).Any())
                                {
                                    db.WKG002.RemoveRange(lstWKG2.Where(i => !i.IsManuallyModified));
                                    db.SaveChanges();

                                    lstWKG002 = GenerateLines(lstQMS012, headerId);
                                }
                            }
                        }

                        if (lstWKG002.Any())
                        {
                            db.WKG002.AddRange(lstWKG002);
                            db.SaveChanges();

                        }
                        //if (lstExistingSeams.Any())
                        //{ }
                        //if (lstWKG002.Any())
                        //{ }

                    }
                }
                savedLines = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                savedLines = false;
            }
            return savedLines;// objResponseMsg;
        }

        public List<WKG002> GenerateLines(List<QMS012> lstQMS012, int headerId)
        {
            WKG001 objWKG001 = new WKG001();
            WKG002 objWKG002 = new WKG002();
            List<WKG002> lstWKG002 = new List<WKG002>();
            #region Generate Lines
            if (lstQMS012.Any())
            {
                if (headerId > 0)
                {
                    objWKG001 = db.WKG001.Where(i => i.HeaderId == headerId).FirstOrDefault();
                }
                foreach (var item in lstQMS012)
                {
                    if (!IsWKGLineExist(item.SeamNo, headerId))
                    {
                        ModelWeldKG objModelWeldKG = new ModelWeldKG();
                        double minThk = 0;
                        double csss = 0, faceSideArea = 0, cbArea = 0, weldKGFaceSide = 0, weldKGCBSide = 0;
                        objWKG002 = new WKG002();
                        objWKG002.HeaderId = headerId;
                        objWKG002.Project = objWKG001.Project;
                        objWKG002.Document = objWKG001.Document;
                        objWKG002.RevNo = objWKG001.RevNo;
                        objWKG002.SeamNo = item.SeamNo;
                        objWKG002.WEPTYPE = item.WEPDetail;
                        objWKG002.SeamDescription = item.SeamDescription;
                        objWKG002.JointType = item.WeldType;
                        objWKG002.SKETCH = "";

                        var strThickness = item.Thickness.Split(',');
                        double thikness1 = strThickness.Length > 0 ? Convert.ToDouble(strThickness[0].Trim()) : 0;
                        double thikness2 = strThickness.Length > 1 ? Convert.ToDouble(strThickness[1].Trim()) : 0;
                        double thikness3 = strThickness.Length > 2 ? Convert.ToDouble(strThickness[2].Trim()) : 0;
                        double thikness4 = strThickness.Length > 3 ? Convert.ToDouble(strThickness[3].Trim()) : 0;
                        double thikness5 = strThickness.Length > 4 ? Convert.ToDouble(strThickness[4].Trim()) : 0;
                        double thikness6 = strThickness.Length > 5 ? Convert.ToDouble(strThickness[5].Trim()) : 0;

                        objWKG002.Thk1 = thikness1;
                        objWKG002.Thk2 = thikness2;
                        objWKG002.Thk3 = thikness3;
                        objWKG002.Thk4 = thikness4;
                        objWKG002.Thk5 = thikness5;
                        objWKG002.Thk6 = thikness6;

                        minThk = Convert.ToDouble(CalculateMinThickness(item.WEPDetail, item.SeamCategory, thikness1, thikness2, thikness3, thikness4, thikness5, thikness6));
                        csss = Convert.ToDouble(CalculateCSSS(objWKG001.Project, item.SeamNo, item.SeamCategory, item.Position.Split(',').ToArray()));

                        //If Seam joint type is CW length in Column E should be "Length-((Column L)*pi()). Obs Id#18329
                        if (item.SeamCategory.StartsWith("CW"))
                        {
                            objWKG002.LengthArea = Convert.ToInt32(item.SeamLengthArea - (minThk * Math.PI));
                        }
                        else
                        {
                            objWKG002.LengthArea = item.SeamLengthArea;
                        }

                        ModelWeldKG oModelWeldKG = new ModelWeldKG();
                        if (item.SeamNo.StartsWith("OW") || item.SeamNo.StartsWith("CR"))
                        {
                            oModelWeldKG = objModelWeldKG.CalculateFaceSideCBackArea("OW", minThk, thikness1, thikness2, Convert.ToInt32(objWKG002.LengthArea));
                        }
                        else
                        {
                            oModelWeldKG = objModelWeldKG.CalculateFaceSideCBackArea(item.WEPDetail, minThk, thikness1, thikness2, Convert.ToInt32(objWKG002.LengthArea));
                        }
                        faceSideArea = oModelWeldKG.FaceSideArea;
                        cbArea = oModelWeldKG.CBArea;
                        weldKGFaceSide = oModelWeldKG.CalculateWeldKGFaceside(item.SeamNo, Convert.ToInt32(objWKG002.LengthArea), csss, minThk, faceSideArea,item);
                        weldKGCBSide = oModelWeldKG.CalculateWeldKGCBSide(item.SeamNo, Convert.ToInt32(objWKG002.LengthArea), csss, minThk, cbArea,item);
                        objWKG002.CSSS = csss;
                        objWKG002.MinThk = minThk;
                        objWKG002.FaceSideArea = faceSideArea;
                        objWKG002.CBarea = cbArea;
                        objWKG002.WeldKgFaceSide = weldKGFaceSide;
                        objWKG002.WeldKgCBSide = weldKGCBSide;
                        objWKG002.TotalWeldKg = Convert.ToDouble(Math.Round(weldKGFaceSide + weldKGCBSide, 2));
                        objWKG002.CreatedBy = objClsLoginInfo.UserName;
                        objWKG002.CreatedOn = DateTime.Now;

                        lstWKG002.Add(objWKG002);
                    }
                }
                //db.WKG002.AddRange(lstWKG002);
                //db.SaveChanges();
            }

            #endregion

            return lstWKG002;
        }

        public ActionResult GetWeldKGLinesPartial(int headerId)
        {

            var ProcessList = clsImplementationEnum.GetWKGProcess().ToList();
            ViewBag.ProcessList1 = ProcessList.Select(i => new SelectListItem { Value = i.ToString(), Text = i.ToString() });
            ViewBag.headerId = headerId;
            return PartialView("_GetWeldKGLinesDataPartial");
        }

        public ActionResult LoadWeldKGLinesData(JQueryDataTableParamModel param, int headerId)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                WKG001 objWKG001 = db.WKG001.Where(x => x.HeaderId == headerId).FirstOrDefault();
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                bool isDeleteEnabled = false;

                if (objWKG001.Status.ToLower() == clsImplementationEnum.WKGHeaderStatus.Draft.GetStringValue().ToLower() || objWKG001.Status.ToLower() == clsImplementationEnum.WKGHeaderStatus.Returned.GetStringValue().ToLower())
                {
                    isDeleteEnabled = true;
                }

                string whereCondition = "1=1";
                //whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "wps10.BU", "wps10.Location");

                if (headerId > 0)
                    whereCondition += " and wkg2.HeaderId=" + headerId + " ";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    //whereCondition += " and (bcom2.t_desc like '%" + param.sSearch + "%' or lcom2.t_desc like '%" + param.sSearch + "%' or Location like '%" + param.sSearch + "%' or wps10.WPSNumber like '%" + param.sSearch + "%' or WPSRevNo like '%" + param.sSearch + "%'  or Status like '%" + param.sSearch + "%' or wps10.CreatedBy like '%" + param.sSearch + "%')";
                    whereCondition += Helper.MakeDatatableSearchCondition(new string[] {"wkg2.SeamNo","wkg2.SeamDescription","WEPTYPE","LengthArea","Thk1","Thk2","Thk3", "Thk4","Thk5","CSSS","JointType","RootRunProcess",
                                                                                       "FaceSideProcess","ChipBackProcess","FaceSideArea","CBarea","WeldKgFaceSide","WeldKgCBSide","TotalWeldKg" }, param.sSearch);
                }
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstHeader = db.SP_WKG_GETLINES(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();
                var ProcessList = clsImplementationEnum.GetWKGProcess().ToList();
                var ProcessList1 = ProcessList.Select(i => new SelectListItem { Value = i.ToString(), Text = i.ToString() });

                #region Add new line section
                //Change By : Ajay Chauhan
                //Date : 21/11/2017
                //Description : Ann new lines functionality in inline grid
                //Assign From : Satish pawar
                int newRecordId = 0;
                var newRecord = new[] {
                              "",
                              Helper.GenerateHidden(newRecordId,"LineId",Convert.ToString(newRecordId)),
                              Helper.GenerateHidden(newRecordId,"HeaderId",Convert.ToString(headerId)),
                              GenerateHTMLTextboxOnChanged(newRecordId,"SeamNo","","",false,"",false,"30"),
                              GenerateHTMLTextboxOnChanged(newRecordId,"SeamDescription","","",false,"",false,"30"),
                              GenerateHTMLTextboxOnChanged(newRecordId,"WEPTYPE","","",false,"", false,"30"),
                              GenerateHTMLTextboxOnChanged(newRecordId,"LengthArea","","",false,"",false,"10"),
                              GenerateHTMLTextboxOnChanged(newRecordId,"Thk1","","",false,"",false,"10"),
                              GenerateHTMLTextboxOnChanged(newRecordId,"Thk2","","",false,"",false,"10"),
                              GenerateHTMLTextboxOnChanged(newRecordId,"Thk3","","",false,"",false,"10"),
                              GenerateHTMLTextboxOnChanged(newRecordId,"Thk4","","",false,"",false,"10"),
                              GenerateHTMLTextboxOnChanged(newRecordId,"Thk5","","",false,"",false,"10"),
                              GenerateHTMLTextboxOnChanged(newRecordId,"Thk6","","",false,"",false,"10"),
                              GenerateHTMLTextboxOnChanged(newRecordId,"CSSS","","",false,"",false,"10"),
                              GenerateHTMLTextboxOnChanged(newRecordId,"JointType","","",false,"",false,"20"),
                              //GenerateAutoComplete(newRecordId,"RootRunProcess","","",false,"","RootRunProcess"),
                              //GenerateAutoComplete(newRecordId,"FaceSideProcess","","",false,"","FaceSideProcess"),
                              //GenerateAutoComplete(newRecordId,"ChipBackProcess","","",false,"","ChipBackProcess"),
                              GenerateAutoComplete(newRecordId,"WeldingProcesses","","",true,"","WeldingProcesses"),
                              GenerateHTMLTextboxOnChanged(newRecordId,"MinThk","","",false,"",false,"10"),
                              GenerateHTMLTextboxOnChanged(newRecordId,"FaceSideArea","","",false,"DECIMAL",false,"18"),
                              GenerateHTMLTextboxOnChanged(newRecordId,"CBArea","","",false,"DECIMAL",false,"18"),
                              GenerateHTMLTextboxOnChanged(newRecordId,"WeldKgFaceSide","","",false,"DECIMAL",false,"18"),
                              GenerateHTMLTextboxOnChanged(newRecordId,"WeldKgCBSide","","",false,"DECIMAL",false,"18"),
                              GenerateHTMLTextboxOnChanged(newRecordId,"TotalWeldKg","","",false,"DECIMAL",false,"18"),
                              GenerateHTMLTextboxOnChanged(newRecordId,"Remarks","","",false,"",false,"100"),
                              GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveNewRecord();" ),

                };
                #endregion
                var res = (from h in lstHeader
                           select new[] {
                              Convert.ToString(h.ROW_NO),
                              Helper.GenerateHidden(h.LineId,"LineId",Convert.ToString(h.LineId)),
                              Helper.GenerateHidden(h.HeaderId,"HeaderId",Convert.ToString(h.HeaderId)),
                              "<nobr>"+Convert.ToString(h.SeamNo)+"</nobr>",
                              GenerateHTMLTextboxOnChanged(h.LineId,"SeamDescription",Convert.ToString(h.SeamDescription),"UpdateWeldKGLines(this,"+h.LineId+")",false,"",(string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.Approved.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false),"30"),//Convert.ToString(h.WEPTYPE),
                              GenerateHTMLTextboxOnChanged(h.LineId,"WEPTYPE",Convert.ToString(h.WEPTYPE),"UpdateWeldKGLines(this,"+h.LineId+")",false,"",(string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)|| string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.Approved.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false),"30"),//Convert.ToString(h.WEPTYPE),
                              GenerateHTMLTextboxOnChanged(h.LineId,"LengthArea",Convert.ToString(h.LengthArea),"UpdateWeldKGLines(this,"+h.LineId+")",false,"",(string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)|| string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.Approved.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false),"10","numeric"),//Convert.ToString(h.LengthArea),
                              GenerateHTMLTextboxOnChanged(h.LineId,"Thk1",Convert.ToString(h.Thk1),"UpdateWeldKGLines(this,"+h.LineId+")",false,"",(string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.Approved.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false),"10","numeric"),//Convert.ToString(h.Thk1),
                              GenerateHTMLTextboxOnChanged(h.LineId,"Thk2",Convert.ToString(h.Thk2),"UpdateWeldKGLines(this,"+h.LineId+")",false,"",(string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.Approved.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false),"10","numeric"),//Convert.ToString(h.Thk2),
                              GenerateHTMLTextboxOnChanged(h.LineId,"Thk3",Convert.ToString(h.Thk3),"UpdateWeldKGLines(this,"+h.LineId+")",false,"",(string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.Approved.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false),"10","numeric"),//Convert.ToString(h.Thk3),
                              GenerateHTMLTextboxOnChanged(h.LineId,"Thk4",Convert.ToString(h.Thk4),"UpdateWeldKGLines(this,"+h.LineId+")",false,"",(string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.Approved.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false),"10","numeric"),//Convert.ToString(h.Thk4),
                              GenerateHTMLTextboxOnChanged(h.LineId,"Thk5",Convert.ToString(h.Thk5),"UpdateWeldKGLines(this,"+h.LineId+")",false,"",(string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.Approved.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false),"10","numeric"),//Convert.ToString(h.Thk5),
                              GenerateHTMLTextboxOnChanged(h.LineId,"Thk6",Convert.ToString(h.Thk6),"UpdateWeldKGLines(this,"+h.LineId+")",false,"",(string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.Approved.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false),"10","numeric"),//Convert.ToString(h.Thk6),
                              GenerateHTMLTextboxOnChanged(h.LineId,"CSSS",Convert.ToString(h.CSSS),"UpdateWeldKGLines(this,"+h.LineId+")",false,"",(string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.Approved.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false),"10","numeric"),//Convert.ToString(h.CSSS),
                              Convert.ToString(h.JointType),
                              //GenerateAutoComplete(h.LineId, "RootRunProcess", Convert.ToString(h.RootRunProcess),"UpdateWeldKGLines(this, "+ h.LineId +");",false,"","RootRunProcess",(string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.Approved.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false)),
                              //GenerateAutoComplete(h.LineId, "FaceSideProcess", Convert.ToString(h.FaceSideProcess),"UpdateWeldKGLines(this, "+ h.LineId +");",false,"","FaceSideProcess",(string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.Approved.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false)),
                              //GenerateAutoComplete(h.LineId, "ChipBackProcess", Convert.ToString(h.ChipBackProcess),"UpdateWeldKGLines(this, "+ h.LineId +");",false,"","ChipBackProcess",(string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.Approved.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false)),
                              Convert.ToString(h.WeldingProcesses),
                              GenerateHTMLTextboxOnChanged(h.LineId,"MinThk",Convert.ToString(h.MinThk),"UpdateWeldKGLines(this,"+h.LineId+")",false,"",(string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)|| string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.Approved.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false),"10","numeric"),//Convert.ToString(h.MinThk),
                              GenerateHTMLTextboxOnChanged(h.LineId,"FaceSideArea",Convert.ToString(h.FaceSideArea),"UpdateWeldKGLines(this,"+h.LineId+")",false,"",(string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)|| string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.Approved.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false),"18","DECIMAL"),
                              GenerateHTMLTextboxOnChanged(h.LineId,"CBArea",Convert.ToString(h.CBarea),"UpdateWeldKGLines(this,"+h.LineId+")",false,"",(string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)|| string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.Approved.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false),"18","DECIMAL"),
                              GenerateHTMLTextboxOnChanged(h.LineId,"WeldKgFaceSide",Convert.ToString(h.WeldKgFaceSide),"UpdateWeldKGLines(this,"+h.LineId+")",false,"",(string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)|| string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.Approved.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false),"18","DECIMAL"),
                              GenerateHTMLTextboxOnChanged(h.LineId,"WeldKgCBSide",Convert.ToString(h.WeldKgCBSide),"UpdateWeldKGLines(this,"+h.LineId+")",false,"",(string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)|| string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.Approved.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false),"18","DECIMAL"),
                              GenerateHTMLTextboxOnChanged(h.LineId,"TotalWeldKg",Convert.ToString(h.TotalWeldKg),"UpdateWeldKGLines(this,"+h.LineId+")",false,"",(string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)|| string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.Approved.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false),"18","DECIMAL"),//Convert.ToString(h.TotalWeldKg)
                              GenerateHTMLTextboxOnChanged(h.LineId,"Remarks",Convert.ToString(h.Remarks),"UpdateWeldKGLines(this,"+h.LineId+")",false,"",(string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)|| string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.Approved.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false),"100"),
                              h.IsManualLine ? isDeleteEnabled ? HTMLActionString(h.LineId,"","Delete","Delete Record","fa fa-trash-o","DeleteRecord("+ h.LineId +");") : HTMLActionString(h.LineId,"","Delete","Delete Record","fa fa-trash-o disabledicon","DeleteRecord("+ h.LineId +");") : "",
                    }).ToList();
                res.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public string GenerateAutoComplete(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false)
        {
            string strAutoComplete = string.Empty;

            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "autocomplete form-control";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";

            strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' hdElement='" + hdElementId + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + (disabled ? "disabled" : "") + " />";

            return strAutoComplete;
        }


        [HttpPost]
        public ActionResult SaveNewLines(FormCollection fc)
        {
            int newRowIndex = 0;
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int HeaderId = Convert.ToInt32(fc["HeaderId" + newRowIndex]);
                string SeamNo = fc["SeamNo" + newRowIndex];
                if (HeaderId > 0)
                {
                    WKG002 objWKG002 = null;
                    WKG001 objWKG001 = db.WKG001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();

                    if (!IsWKGLineExist(SeamNo, HeaderId))
                    {
                        ModelWeldKG objModelWeldKG = new ModelWeldKG();
                        objWKG002 = new WKG002();
                        objWKG002.HeaderId = HeaderId;
                        objWKG002.Project = objWKG001.Project;
                        objWKG002.Document = objWKG001.Document;
                        objWKG002.RevNo = objWKG001.RevNo;
                        objWKG002.SeamNo = fc["SeamNo" + newRowIndex];
                        objWKG002.WEPTYPE = fc["WEPTYPE" + newRowIndex] != "" ? fc["WEPTYPE" + newRowIndex] : null;
                        objWKG002.JointType = fc["JointType" + newRowIndex] != "" ? fc["JointType" + newRowIndex] : null;
                        objWKG002.SeamDescription = fc["SeamDescription" + newRowIndex] != "" ? fc["SeamDescription" + newRowIndex] : null;
                        objWKG002.SKETCH = null;
                        objWKG002.LengthArea = fc["LengthArea" + newRowIndex] != "" ? Convert.ToInt32(fc["LengthArea" + newRowIndex]) : 0;
                        objWKG002.Thk1 = fc["Thk1" + newRowIndex] != "" ? Convert.ToInt32(fc["Thk1" + newRowIndex]) : 0;
                        objWKG002.Thk2 = fc["Thk2" + newRowIndex] != "" ? Convert.ToInt32(fc["Thk2" + newRowIndex]) : 0;
                        objWKG002.Thk3 = fc["Thk3" + newRowIndex] != "" ? Convert.ToInt32(fc["Thk3" + newRowIndex]) : 0;
                        objWKG002.Thk4 = fc["Thk4" + newRowIndex] != "" ? Convert.ToInt32(fc["Thk4" + newRowIndex]) : 0;
                        objWKG002.Thk5 = fc["Thk5" + newRowIndex] != "" ? Convert.ToInt32(fc["Thk5" + newRowIndex]) : 0;
                        objWKG002.Thk6 = fc["Thk6" + newRowIndex] != "" ? Convert.ToInt32(fc["Thk6" + newRowIndex]) : 0;
                        objWKG002.RootRunProcess = fc["RootRunProcess" + newRowIndex] != "" ? fc["RootRunProcess" + newRowIndex] : null;
                        objWKG002.FaceSideProcess = fc["FaceSideProcess" + newRowIndex] != "" ? fc["FaceSideProcess" + newRowIndex] : null;
                        objWKG002.ChipBackProcess = fc["ChipBackProcess" + newRowIndex] != "" ? fc["ChipBackProcess" + newRowIndex] : null;
                        objWKG002.CSSS = fc["CSSS" + newRowIndex] != "" ? Convert.ToDouble(fc["CSSS" + newRowIndex], CultureInfo.InvariantCulture) : 0;
                        objWKG002.MinThk = fc["MinThk" + newRowIndex] != "" ? Convert.ToInt32(fc["MinThk" + newRowIndex]) : 0;
                        objWKG002.FaceSideArea = fc["FaceSideArea" + newRowIndex] != "" ? Convert.ToDouble(fc["FaceSideArea" + newRowIndex], CultureInfo.InvariantCulture) : 0;
                        objWKG002.CBarea = fc["CBarea" + newRowIndex] != "" ? Convert.ToDouble(fc["CBarea" + newRowIndex], CultureInfo.InvariantCulture) : 0;
                        objWKG002.WeldKgFaceSide = fc["WeldKgFaceSide" + newRowIndex] != "" ? Convert.ToDouble(fc["WeldKgFaceSide" + newRowIndex], CultureInfo.InvariantCulture) : 0;
                        objWKG002.WeldKgCBSide = fc["WeldKgCBSide" + newRowIndex] != "" ? Convert.ToDouble(fc["WeldKgCBSide" + newRowIndex], CultureInfo.InvariantCulture) : 0;
                        objWKG002.TotalWeldKg = fc["TotalWeldKg" + newRowIndex] != "" ? Convert.ToDouble(fc["TotalWeldKg" + newRowIndex], CultureInfo.InvariantCulture) : 0;
                        objWKG002.Remarks = fc["Remarks" + newRowIndex] != "" ? fc["Remarks" + newRowIndex] : null;
                        objWKG002.CreatedBy = objClsLoginInfo.UserName;
                        objWKG002.CreatedOn = DateTime.Now;
                        objWKG002.IsManualLine = true;

                        db.WKG002.Add(objWKG002);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.Message.ToString();
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.InvalidHeaderId.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateWeldKGLines(int lineId, string columnName, string columnValue)
        {
            ResponseWeldKG objResponseMsg = new ResponseWeldKG();
            //clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            WKG001 objWKG001 = new WKG001();
            WKG002 objWKG002 = new WKG002();
            QMS012 objQMS012 = new QMS012();
            try
            {
                //ModelWeldKG objModelWeldKG = new ModelWeldKG();
                int minThk = 0;
                double csss = 0, faceSideArea = 0, cbArea = 0, weldKGFaceSide = 0, weldKGCBSide = 0;
                if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
                {
                    db.SP_WKG_UPDATE_LINES(lineId, columnName, columnValue);
                    if (lineId > 0)
                    {
                        objWKG002 = db.WKG002.Where(i => i.LineId == lineId).FirstOrDefault();
                        objWKG001 = db.WKG001.Where(i => i.HeaderId == objWKG002.HeaderId).FirstOrDefault();
                    }
                    int PlanningDinID = db.PDN002.Where(x => x.RefId == objWKG001.HeaderId && x.DocumentNo == objWKG001.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                    string location = db.PDN001.Where(x => x.HeaderId == PlanningDinID).Select(x => x.Location).FirstOrDefault();
                    objQMS012 = db.QMS012.Where(i => i.Location.Equals(location, StringComparison.OrdinalIgnoreCase) && i.SeamNo.Equals(objWKG002.SeamNo, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();

                    if (objQMS012 != null)
                    {
                        minThk = Convert.ToInt32(CalculateMinThickness(objWKG002.WEPTYPE, objQMS012.SeamCategory, Convert.ToInt32(objWKG002.Thk1), Convert.ToInt32(objWKG002.Thk2), Convert.ToInt32(objWKG002.Thk3), Convert.ToInt32(objWKG002.Thk4), Convert.ToInt32(objWKG002.Thk5), Convert.ToInt32(objWKG002.Thk6)));
                        csss = Convert.ToDouble(CalculateCSSS(objWKG001.Project, objQMS012.SeamNo, objQMS012.SeamCategory, objQMS012.Position.Split(',').ToArray()));

                        ModelWeldKG oModelWeldKG = new ModelWeldKG();
                        if (objWKG002.SeamNo.StartsWith("OW") || objWKG002.SeamNo.StartsWith("CR"))
                        {
                            oModelWeldKG = new ModelWeldKG().CalculateFaceSideCBackArea("OW", minThk, Convert.ToInt32(objWKG002.Thk1), Convert.ToInt32(objWKG002.Thk2), Convert.ToInt32(objWKG002.LengthArea));
                        }
                        else
                        {
                            oModelWeldKG = new ModelWeldKG().CalculateFaceSideCBackArea(objWKG002.WEPTYPE, minThk, Convert.ToInt32(objWKG002.Thk1), Convert.ToInt32(objWKG002.Thk2), Convert.ToInt32(objWKG002.LengthArea));
                        }
                        //var ModelWeldKG = objModelWeldKG.CalculateFaceSideCBackArea(objWKG002.WEPTYPE, minThk, Convert.ToInt32(objWKG002.Thk1), Convert.ToInt32(objWKG002.Thk2), Convert.ToInt32(objQMS012.SeamLengthArea));
                        faceSideArea = oModelWeldKG.FaceSideArea;
                        cbArea = oModelWeldKG.CBArea;
                        weldKGFaceSide = oModelWeldKG.CalculateWeldKGFaceside(objWKG002.SeamNo, Convert.ToInt32(objWKG002.LengthArea), csss, minThk, faceSideArea, objQMS012);
                        weldKGCBSide = oModelWeldKG.CalculateWeldKGCBSide(objWKG002.SeamNo, Convert.ToInt32(objWKG002.LengthArea), csss, minThk, cbArea, objQMS012);

                        objWKG002.MinThk = minThk;
                        objWKG002.FaceSideArea = faceSideArea;
                        objWKG002.CBarea = cbArea;
                        objWKG002.WeldKgFaceSide = weldKGFaceSide;
                        objWKG002.WeldKgCBSide = weldKGCBSide;
                        objWKG002.TotalWeldKg = weldKGFaceSide + weldKGCBSide;
                    }
                    objWKG002.IsManuallyModified = true;
                    objWKG002.EditedBy = objClsLoginInfo.UserName;
                    objWKG002.EditedOn = DateTime.Now;

                    if (string.Equals(objWKG001.Status, clsImplementationEnum.WKGHeaderStatus.Approved.GetStringValue(), StringComparison.OrdinalIgnoreCase))
                    {
                        objWKG001.Status = clsImplementationEnum.WKGHeaderStatus.Draft.GetStringValue();
                        if (db.WKG002.Where(i => i.HeaderId == objWKG002.HeaderId).Any())
                        {
                            db.WKG002.Where(i => i.HeaderId == objWKG002.HeaderId).ToList().ForEach(i => i.RevNo = objWKG001.RevNo + 1);
                        }
                        objWKG001.RevNo += 1;
                    }

                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Saved Successfully";
                    objResponseMsg.HeaderId = objWKG001.HeaderId;
                    objResponseMsg.HeaderStatus = objWKG001.Status;
                    objResponseMsg.RevNo = Convert.ToString(objWKG001.RevNo);

                    objResponseMsg.MinThk = objWKG002.MinThk;
                    objResponseMsg.FaceSideArea = objWKG002.FaceSideArea;
                    objResponseMsg.CBArea = objWKG002.CBarea;
                    objResponseMsg.WeldKGFaceSide = objWKG002.WeldKgFaceSide;
                    objResponseMsg.WeldKgCBSide = objWKG002.WeldKgCBSide;
                    objResponseMsg.TotalWeldKg = objWKG002.TotalWeldKg;
                }

                Manager.UpdatePDN002(objWKG001.HeaderId, objWKG001.Status, objWKG001.RevNo, objWKG001.Project, objWKG001.Document);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult DeleteLine(int lineid)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                WKG002 objWKG002 = db.WKG002.Where(x => x.LineId == lineid).FirstOrDefault();
                db.WKG002.Remove(objWKG002);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public double? CalculateCSSS(string project, string SeamNo, string SeamCategory, string[] position)
        {
            double? csss = 0;
            if (!string.IsNullOrEmpty(project))
            {
                List<string> lstBOMLIST = new List<string>();
                var strMaterialCode = string.Empty;
                switch (SeamCategory)
                {
                    case "LW":
                    case "CW":
                    case "NW":
                    case "OW":
                    case "CR":
                        if (position.Length > 0)
                        {
                            strMaterialCode = string.Join(",", Manager.GetHBOMData(project).Where(i =>  position.Contains(i.FindNo.ToString())).Select(i => i.MaterialCode).Distinct().ToList());
                            var sprg = db.COM010.Where(i => strMaterialCode.Contains(i.t_mtrl)).ToList().Max(i => (i.t_spgr * 1000));
                            csss = Convert.ToDouble(sprg);
                        }
                        break;
                        //default:
                        //if (position.Length > 0)
                        //{
                        //    strMaterialCode = string.Join(",", db.VW_IPI_GETHBOMLIST.Where(i => i.Project.Equals(project, StringComparison.OrdinalIgnoreCase) && position.Contains(i.FindNo.ToString())).Select(i => i.MaterialCode).Distinct().ToList());
                        //    var sprg = db.COM010.Where(i => strMaterialCode.Contains(i.t_mtrl)).Max(i => (i.t_spgr * 1000));
                        //    csss = Convert.ToDecimal(sprg);
                        //}
                        //break;
                }


            }
            return csss;
        }

        public double? CalculateMinThickness(string WEPType, string seamCategory, double THK1, double THK2, double THK3, double THK4, double THK5, double THK6)
        {
            double? MinThk = 0;
            double[] arrThickness = { THK1, THK2, THK3, THK4, THK5 };
            if (seamCategory.StartsWith("OW") || seamCategory.StartsWith("CR"))
            {
                MinThk = THK6;
            }
            else if (!string.IsNullOrEmpty(WEPType))
            {
                var str1 = "N51".Substring(1, "N51".Length - 1);
                var str2 = "N151".Substring(1, "N151".Length - 1);

                var wepType = WEPType.Substring(1, WEPType.Length - 1);

                if (seamCategory.StartsWith("LW") || seamCategory.StartsWith("CW") || seamCategory.StartsWith("NW"))
                {
                    if (WEPType.StartsWith("N") && (Convert.ToInt32(wepType) >= Convert.ToInt32(str1) && Convert.ToInt32(wepType) <= Convert.ToInt32(str2)))
                    {
                        MinThk = 0;
                    }
                    else
                    {
                        if (arrThickness.Where(i => i > 0).Any())
                        {
                            MinThk = arrThickness.Where(i => i > 0).Min();
                        }
                        else
                        {
                            MinThk = 0;
                        }
                    }

                }

                else if (WEPType.StartsWith("N") && (Convert.ToInt32(wepType) >= Convert.ToInt32(str1) && Convert.ToInt32(wepType) <= Convert.ToInt32(str2)))
                {
                    if (Convert.ToInt32(wepType) != 56)
                    {
                        MinThk = 0;
                    }
                    else
                    {
                        MinThk = arrThickness.Where(i => i > 0).Min();
                    }
                }
                else
                {
                    if (WEPType.StartsWith("W") && (Convert.ToInt32(wepType) == 33))
                    {
                        MinThk = arrThickness[0] + arrThickness[1];
                    }
                    else if (WEPType.StartsWith("W") && (Convert.ToInt32(wepType) == 34))
                    {
                        MinThk = arrThickness.Where(i => i > 0).Max();
                    }
                    #region Observation30533-HardikMasalawala
                    else if (WEPType.StartsWith("W") && (Convert.ToInt32(wepType) == 47))
                    {
                        MinThk = 24;
                    }
                    #endregion
                    else
                    {
                        if (arrThickness.Where(i => i > 0).Any())
                        {
                            MinThk = arrThickness.Where(i => i > 0).Min();
                        }
                        else
                        {
                            MinThk = 0;
                        }
                    }
                }
            }

            return MinThk;
        }

        [HttpPost]
        public ActionResult SendForApproval(int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            int PlanningDinID;
            WKG001 objWKG001 = new WKG001();
            try
            {
                if (headerId > 0)
                {
                    objWKG001 = db.WKG001.Where(i => i.HeaderId == headerId).FirstOrDefault();
                    if (!string.IsNullOrEmpty(objWKG001.ApprovedBy))
                    {
                        if (db.WKG002.Where(i => i.HeaderId == headerId).Any())
                        {
                            objWKG001.Status = clsImplementationEnum.WKGHeaderStatus.SentForApproval.GetStringValue();
                            objWKG001.SubmittedBy = objClsLoginInfo.UserName;
                            objWKG001.SubmittedOn = DateTime.Now;

                            db.SaveChanges();
                            PlanningDinID = db.PDN002.Where(x => x.RefId == headerId && x.DocumentNo == objWKG001.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                            objResponseMsg.Key = true;
                            objResponseMsg.Status = clsImplementationMessage.WKGHeader.SentForApproval.ToString();
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.sentforApprove.ToString();
                            objResponseMsg.PlanningDinID = PlanningDinID;
                            Manager.UpdatePDN002(objWKG001.HeaderId, objWKG001.Status, objWKG001.RevNo, objWKG001.Project, objWKG001.Document);

                            #region Send Notification
                            (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG2.GetStringValue() + "," + clsImplementationEnum.UserRoleName.PLNG1.GetStringValue(),
                                                                objWKG001.Project,
                                                                "",
                                                                "",
                                                                Manager.GetPDINDocumentNotificationMsg(objWKG001.Project, clsImplementationEnum.PlanList.Weld_KG_Plan.GetStringValue(), objWKG001.RevNo.Value.ToString(), objWKG001.Status),
                                                                clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(),
                                                                Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Weld_KG_Plan.GetStringValue(), objWKG001.HeaderId.ToString(), true),
                                                                objWKG001.ApprovedBy);
                            #endregion
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = string.Format("No Lines Exists For Document: {0} .Please Add Lines", objWKG001.Document);
                        }
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = string.Format("No Approver Selected For Document {0}", objWKG001.Document);
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult RetractHeader(int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            WKG001 objWKG001 = new WKG001();
            try
            {
                if (headerId > 0)
                {
                    objWKG001 = db.WKG001.Where(i => i.HeaderId == headerId).FirstOrDefault();
                    if (objWKG001 != null)
                    {
                        objWKG001.Status = clsImplementationEnum.WKGHeaderStatus.Draft.GetStringValue();
                        db.SaveChanges();
                        Manager.UpdatePDN002(objWKG001.HeaderId, objWKG001.Status, objWKG001.RevNo, objWKG001.Project, objWKG001.Document);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Document Retracted Successfully";
                        objResponseMsg.HeaderStatus = objWKG001.Status;
                        objResponseMsg.HeaderId = objWKG001.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetHistoryDataPartial(int headerId)
        {
            WKG001 objWKG001 = new WKG001();
            if (headerId > 0)
            {
                objWKG001 = db.WKG001.Where(i => i.HeaderId == headerId).FirstOrDefault();
            }
            return PartialView("_GetHistoryDataPartial", objWKG001);
        }

        [HttpPost]
        public JsonResult LoadHistoryDetailData(JQueryDataTableParamModel param, int headerId)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;

                string whereCondition = "1=1";
                //whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qcp1.BU", "qcp1.Location");
                whereCondition += "and wkg1.HeaderId = " + headerId;

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    //whereCondition += " and (bcom2.t_desc like '%" + param.sSearch + "%' or lcom2.t_desc like '%" + param.sSearch + "%' or (qcp1.Project+'-'+com1.t_dsca) like '%" + param.sSearch + "%' or DocumentNo like '%" + param.sSearch + "%' or QCPRev like '%" + param.sSearch + "%'  or Status like '%" + param.sSearch + "%')";
                    whereCondition += Helper.MakeDatatableSearchCondition(new string[] { "wkg1.Project", "com1.t_dsca", "Product", "ProcessLicensor", "wkg1.Customer", "lcom6.t_nama", "Document", "wkg1.RevNo", "Status", "wkg1.CreatedBy", "ecom3.t_name", "wkg1.EditedBy", "com3e.t_name", "CreatedOn", "EditedOn" }, param.sSearch);
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstResult = db.SP_WKG_GET_HEADERHISTORY(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstResult.Select(i => i.TotalCount).FirstOrDefault();

                var data = (from uc in lstResult
                            select new[]
                           {
                           Convert.ToString(uc.Id),
                           Convert.ToString(uc.Project),
                           Convert.ToString(uc.Product),
                           Convert.ToString(uc.ProcessLicensor),
                           Convert.ToString(uc.Customer),
                           Convert.ToString(uc.Document),
                           Convert.ToString("R" + uc.RevNo),
                           Convert.ToString(uc.Status),
                           uc.CreatedBy,
                           Convert.ToDateTime( uc.CreatedOn).ToString("dd/MM/yyyy"),
                           uc.EditedBy,
                           Convert.ToDateTime( uc.EditedOn).ToString("dd/MM/yyyy"),
                        Convert.ToString(uc.SubmittedBy),
                        Convert.ToString(Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy")),
                        Convert.ToString(uc.ApprovedBy),
                        Convert.ToString(Convert.ToDateTime(uc.ApprovedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.ApprovedOn).ToString("dd/MM/yyyy")),

                       // "<center><a class ='iconspace' title=\"View\" target=\"_blank\" onclick=\"HistoryView("+uc.id+",'"+role+"')\"><i class=\"fa fa-eye\"></i></a>"+" "+"<i class ='iconspace fa fa-clock-o' title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/WKG/MaintainWeldKG/ShowHistoryTimeline?HeaderID=" + Convert.ToInt32(uc.Id) + "')></i> </center>"
                           "<center><a title='View' class='iconspace' target='_blank' href='"+WebsiteURL+"/WKG/MaintainWeldKG/ViewHistory?id=" + Convert.ToString(uc.Id) + "'><i class='fa fa-eye'></i></a><i class ='iconspace fa fa-clock-o' title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/WKG/MaintainWeldKG/ShowHistoryTimeline?HeaderID=" + Convert.ToInt32(uc.Id) + "')></i></center>"

                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        //[SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult ViewHistory(int id)
        {
            WKG001_Log objWKG001 = new WKG001_Log();
            if (id > 0)
            {
                objWKG001 = db.WKG001_Log.Where(i => i.Id == id).FirstOrDefault();
                //objWKG001 = db.WKG001.Where(i => i.HeaderId == headerId).FirstOrDefault();
                ViewBag.Project = db.COM001.Where(i => i.t_cprj.Equals(objWKG001.Project, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_cprj + "-" + i.t_dsca).FirstOrDefault();
                ViewBag.Approver = db.COM003.Where(i => i.t_psno.Equals(objWKG001.ApprovedBy, StringComparison.OrdinalIgnoreCase) && i.t_actv == 1).Select(i => i.t_psno + "-" + i.t_name).FirstOrDefault();
            }
            var urlPrefix = Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.LastIndexOf('=') + 1);
            if (!objClsLoginInfo.GetUserRoleList().Contains("SHOP"))
            {
                ViewBag.RevPrev = (db.WKG001_Log.Any(q => (q.HeaderId == objWKG001.HeaderId && q.RevNo == (objWKG001.RevNo - 1))) ? urlPrefix + db.WKG001_Log.Where(q => (q.HeaderId == objWKG001.HeaderId && q.RevNo == (objWKG001.RevNo - 1))).FirstOrDefault().Id : null);
                ViewBag.RevNext = (db.WKG001_Log.Any(q => (q.HeaderId == objWKG001.HeaderId && q.RevNo == (objWKG001.RevNo + 1))) ? urlPrefix + db.WKG001_Log.Where(q => (q.HeaderId == objWKG001.HeaderId && q.RevNo == (objWKG001.RevNo + 1))).FirstOrDefault().Id : null);
            }
            return View(objWKG001);
        }

        public ActionResult GetWeldKGLinesHistoryPartial(int refId)
        {
            return PartialView("_GetWeldKGLinesHistoryPartial");
        }

        public ActionResult LoadWeldKGLinesHistoryData(JQueryDataTableParamModel param, int refId)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;

                string whereCondition = "1=1";
                //whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "wps10.BU", "wps10.Location");

                if (refId > 0)
                    whereCondition += " and RefId=" + refId;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    //whereCondition += " and (bcom2.t_desc like '%" + param.sSearch + "%' or lcom2.t_desc like '%" + param.sSearch + "%' or Location like '%" + param.sSearch + "%' or wps10.WPSNumber like '%" + param.sSearch + "%' or WPSRevNo like '%" + param.sSearch + "%'  or Status like '%" + param.sSearch + "%' or wps10.CreatedBy like '%" + param.sSearch + "%')";
                    whereCondition += Helper.MakeDatatableSearchCondition(new string[] {"wkg2.SeamNo","qms12.SeamDescription","WEPTYPE","LengthArea","Thk1","Thk2","Thk3", "Thk4","Thk5","CSSS","JointType","RootRunProcess",
                                                                                       "FaceSideProcess","ChipBackProcess","FaceSideArea","CBarea","WeldKgFaceSide","WeldKgCBSide","TotalWeldKg" }, param.sSearch);
                }
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstHeader = db.SP_WKG_GET_LINESHISTORY(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from h in lstHeader
                          select new[] {
                              Convert.ToString(h.ROW_NO),
                              Helper.GenerateHidden(h.LineId,"LineId",Convert.ToString(h.LineId)),
                              Helper.GenerateHidden(h.HeaderId,"HeaderId",Convert.ToString(h.HeaderId)),
                              "<nobr>"+Convert.ToString(h.SeamNo)+"</nobr>",
                              "<nobr>"+Convert.ToString(h.SeamDescription)+"</nobr>",
                              Convert.ToString(h.WEPTYPE),
                              //"",
                              Convert.ToString(h.LengthArea),
                              Convert.ToString(h.Thk1),
                              Convert.ToString(h.Thk2),
                              Convert.ToString(h.Thk3),
                              Convert.ToString(h.Thk4),
                              Convert.ToString(h.Thk5),
                              Convert.ToString(h.Thk6),
                              Convert.ToString(h.CSSS),
                              Convert.ToString(h.JointType),
                              Convert.ToString(h.RootRunProcess),
                              Convert.ToString(h.FaceSideProcess),
                              Convert.ToString(h.ChipBackProcess),
                              Convert.ToString(h.MinThk),
                              Convert.ToString(h.FaceSideArea),
                              Convert.ToString(h.CBarea),
                              Convert.ToString(h.WeldKgFaceSide),
                              Convert.ToString(h.WeldKgCBSide),
                              Convert.ToString(h.TotalWeldKg),
                              Convert.ToString(h.Remarks)
                              //Convert.ToString(h.seam),
                    };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public string GetHTMLString(int headerId)
        {
            string strHTML = string.Empty;
            strHTML = "<center><a class='btn btn-xs green' title='View' href='" + WebsiteURL + "/WKG/MaintainWeldKG/AddWeldKGHeader?headerId=" + Convert.ToString(headerId) + "'><i style='margin-left:5px;' class='fa fa-eye'></i></a></center>";

            return strHTML;
        }

        [HttpPost]
        public ActionResult GetProjectResult(string term)
        {
            List<Projects> lstProjects = new List<Projects>();//Manager.getProjectsByUserAutocomplete(objClsLoginInfo.UserName, term).ToList();
            if (!string.IsNullOrEmpty(term))
            {
                lstProjects = Manager.getProjectsByUserAutocomplete(objClsLoginInfo.UserName, term).Distinct().ToList();
            }
            else
            {
                lstProjects = Manager.getProjectsByUserAutocomplete(objClsLoginInfo.UserName, term).Take(10).Distinct().ToList();
            }
            return Json(lstProjects, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetProjectDetail(string projectCd, string docNo, int headerId = 0)
        {
            PurchaseDetail objProjectDetail = new PurchaseDetail();

            var objWeldKGHeader = db.WKG001.Where(i => i.Project == projectCd && i.Document.Equals(docNo, StringComparison.OrdinalIgnoreCase) && i.HeaderId != headerId).ToList();

            try
            {
                if (!string.IsNullOrEmpty(projectCd))
                {

                    if (objWeldKGHeader.Any())// && action.ToLower() != "edit")
                    {
                        objProjectDetail.headerAlreadyExist = true;
                    }
                    else
                    {
                        objProjectDetail = Manager.getProjectWiseBULocation(projectCd);
                        objProjectDetail.CDD = Manager.GetContractWiseCdd(projectCd);
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
            return Json(objProjectDetail, JsonRequestBehavior.AllowGet);
        }

        public bool isDocumentExist(string project, string docNo, int headerId = 0)
        {
            bool isDocumentExist = false;
            if (!string.IsNullOrEmpty(project) && !string.IsNullOrEmpty(docNo))
            {
                if (db.WKG001.Where(i => i.Project.Equals(project, StringComparison.OrdinalIgnoreCase) && i.Document.Equals(docNo, StringComparison.OrdinalIgnoreCase) && i.HeaderId != headerId).Any())
                {
                    isDocumentExist = true;
                }
            }
            return isDocumentExist;
        }
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "", int? HeaderId = 0, int? latestRv = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_WKG_GETHEADER(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      Documents = li.Document,
                                      Customer = li.Customer,
                                      Product = li.Product,
                                      ProcessLicensor = li.ProcessLicensor,
                                      RevNo = li.RevNo,
                                      Status = li.Status,
                                      CDD = li.CDD,

                                      //ProcessPlan = li.ProcessPlan,
                                      //CreatedBy = li.CreatedBy,
                                      //CreatedOn = li.CreatedOn == null || li.CreatedOn.Value == DateTime.MinValue ? "NA" : li.CreatedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn == null || li.EditedOn.Value == DateTime.MinValue ? "NA" : li.EditedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      SubmittedBy = li.SubmittedBy,
                                      SubmittedOn = li.SubmittedOn == null || li.SubmittedOn.Value == DateTime.MinValue ? "NA" : li.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      ApprovedBy = li.ApprovedBy,
                                      ApproveOn = li.ApprovedOn == null || li.ApprovedOn.Value == DateTime.MinValue ? "NA" : li.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      // ReturnRemark = li.ReturnRemark

                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {
                    var lst = db.SP_WKG_GETLINES(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      RowNo = li.ROW_NO,
                                      Remarks = li.Remarks,
                                      SeamNo = li.SeamNo,
                                      SeamDescription = li.SeamDescription,
                                      WEPType = li.WEPTYPE,
                                      LengthArea = li.LengthArea,
                                      Thk1 = li.Thk1,
                                      Thk2 = li.Thk2,
                                      Thk3 = li.Thk3,
                                      Thk4 = li.Thk4,
                                      Thk5 = li.Thk5,
                                      Overlay = li.Thk6,
                                      CsSS = li.CSSS,
                                      JointType = li.JointType,
                                      RootRunProcess = li.RootRunProcess,
                                      FaceSideProcess = li.FaceSideProcess,
                                      ChipBackProcess = li.ChipBackProcess,
                                      MinThk = li.MinThk,
                                      FacesideArea = li.FaceSideArea,
                                      CBArea = li.CBarea,
                                      WeldKGFaceside = li.WeldKgFaceSide,
                                      WeldKgCBSide = li.WeldKgCBSide,
                                      WeldKG = li.TotalWeldKg,
                                      PreparedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn == null || li.CreatedOn.Value == DateTime.MinValue ? "NA" : li.CreatedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      // CheckedBy = li.EditedBy,
                                      // CheckedOn = li.EditedOn == null || li.EditedOn.Value == DateTime.MinValue ? "NA" : li.EditedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),


                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.HISTORYHEADER.GetStringValue())
                {  //tack weld lines
                    var lst = db.SP_WKG_GET_HEADERHISTORY(1, int.MaxValue, strSortOrder, whereCondition).ToList();

                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No data available";
                    }

                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      Documents = li.Document,
                                      Customer = li.Customer,
                                      Product = li.Product,
                                      ProcessLicensor = li.ProcessLicensor,
                                      RevNo = li.RevNo,
                                      Status = li.Status,
                                      CDD = li.CDD,
                                      //ProcessPlan = li.ProcessPlan,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn == null || li.CreatedOn.Value == DateTime.MinValue ? "NA" : li.CreatedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn == null || li.EditedOn.Value == DateTime.MinValue ? "NA" : li.EditedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      SubmittedBy = li.SubmittedBy,
                                      SubmittedOn = li.SubmittedOn == null || li.SubmittedOn.Value == DateTime.MinValue ? "NA" : li.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      ApprovedBy = li.ApprovedBy,
                                      ApproveOn = li.ApprovedOn == null || li.ApprovedOn.Value == DateTime.MinValue ? "NA" : li.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      // ReturnRemark = li.ReturnRemark

                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.HISTORYLINES.GetStringValue())
                {
                    var lst = db.SP_WKG_GET_LINESHISTORY(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      RowNo = li.ROW_NO,
                                      Remarks = li.Remarks,
                                      SeamNo = li.SeamNo,
                                      SeamDescription = li.SeamDescription,
                                      WEPType = li.WEPTYPE,
                                      LengthArea = li.LengthArea,
                                      Thk1 = li.Thk1,
                                      Thk2 = li.Thk2,
                                      Thk3 = li.Thk3,
                                      Thk4 = li.Thk4,
                                      Thk5 = li.Thk5,
                                      Overlay = li.Thk6,
                                      CsSS = li.CSSS,
                                      JointType = li.JointType,
                                      RootRunProcess = li.RootRunProcess,
                                      FaceSideProcess = li.FaceSideProcess,
                                      ChipBackProcess = li.ChipBackProcess,
                                      MinThk = li.MinThk,
                                      FacesideArea = li.FaceSideArea,
                                      CBArea = li.CBarea,
                                      WeldKGFaceside = li.WeldKgFaceSide,
                                      WeldKgCBSide = li.WeldKgCBSide,
                                      WeldKG = li.TotalWeldKg,
                                      PreparedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn == null || li.CreatedOn.Value == DateTime.MinValue ? "NA" : li.CreatedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),


                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        public bool IsWKGLineExist(string seamNo, int headerId)
        {
            bool IsWKGLineExist = false;
            var lstWKG002 = db.WKG002.Where(i => i.SeamNo.Equals(seamNo, StringComparison.OrdinalIgnoreCase) && i.HeaderId == headerId).ToList();
            if (lstWKG002.Any())
            {
                IsWKGLineExist = true;
            }
            return IsWKGLineExist;
        }
    }
}