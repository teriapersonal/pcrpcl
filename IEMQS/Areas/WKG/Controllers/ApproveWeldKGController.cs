﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQSImplementation;
using IEMQS.Models;
using System.Globalization;
namespace IEMQS.Areas.WKG.Controllers
{
    public class ApproveWeldKGController : clsBase
    {
        #region Utility
        [NonAction]
        public static string GenerateGridButton(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";


            htmlControl = "<a id='" + inputID + "' name='" + inputID + "' class='btn btn-outline btn-circle btn-sm blue' " + onClickEvent + " ><i class='" + className + "'></i> " + buttonName + "</a>";

            //htmlControl = "<i  data-modal='' id='" + inputID + "' name='" + inputID + "' style='cursor:Pointer;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";

            return htmlControl;
        }
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "", int? HeaderId = 0, int? latestRv = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_WKG_GETHEADER(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      Documents = li.Document,
                                      Customer = li.Customer,
                                      Product = li.Product,
                                      ProcessLicensor = li.ProcessLicensor,
                                      RevNo = li.RevNo,
                                      Status = li.Status,
                                      CDD = li.CDD,

                                      //ProcessPlan = li.ProcessPlan,
                                      //CreatedBy = li.CreatedBy,
                                      //CreatedOn = li.CreatedOn == null || li.CreatedOn.Value == DateTime.MinValue ? "NA" : li.CreatedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn == null || li.EditedOn.Value == DateTime.MinValue ? "NA" : li.EditedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      SubmittedBy = li.SubmittedBy,
                                      SubmittedOn = li.SubmittedOn == null || li.SubmittedOn.Value == DateTime.MinValue ? "NA" : li.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      ApprovedBy = li.ApprovedBy,
                                      ApproveOn = li.ApprovedOn == null || li.ApprovedOn.Value == DateTime.MinValue ? "NA" : li.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      // ReturnRemark = li.ReturnRemark

                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {
                    var lst = db.SP_WKG_GETLINES(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      RowNo = li.ROW_NO,
                                      Remarks = li.Remarks,
                                      SeamNo = li.SeamNo,
                                      SeamDescription = li.SeamDescription,
                                      WEPType = li.WEPTYPE,
                                      LengthArea = li.LengthArea,
                                      Thk1 = li.Thk1,
                                      Thk2 = li.Thk2,
                                      Thk3 = li.Thk3,
                                      Thk4 = li.Thk4,
                                      Thk5 = li.Thk5,
                                      Overlay = li.Thk6,
                                      CsSS = li.CSSS,
                                      JointType = li.JointType,
                                      RootRunProcess = li.RootRunProcess,
                                      FaceSideProcess = li.FaceSideProcess,
                                      ChipBackProcess = li.ChipBackProcess,
                                      MinThk = li.MinThk,
                                      FacesideArea = li.FaceSideArea,
                                      CBArea = li.CBarea,
                                      WeldKGFaceside = li.WeldKgFaceSide,
                                      WeldKgCBSide = li.WeldKgCBSide,
                                      WeldKG = li.TotalWeldKg,
                                      PreparedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn == null || li.CreatedOn.Value == DateTime.MinValue ? "NA" : li.CreatedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      // CheckedBy = li.EditedBy,
                                      // CheckedOn = li.EditedOn == null || li.EditedOn.Value == DateTime.MinValue ? "NA" : li.EditedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),


                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.HISTORYHEADER.GetStringValue())
                {  //tack weld lines
                    var lst = db.SP_WKG_GET_HEADERHISTORY(1, int.MaxValue, strSortOrder, whereCondition).ToList();

                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No data available";
                    }

                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      Documents = li.Document,
                                      Customer = li.Customer,
                                      Product = li.Product,
                                      ProcessLicensor = li.ProcessLicensor,
                                      RevNo = li.RevNo,
                                      Status = li.Status,
                                      CDD = li.CDD,
                                      //ProcessPlan = li.ProcessPlan,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn == null || li.CreatedOn.Value == DateTime.MinValue ? "NA" : li.CreatedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn == null || li.EditedOn.Value == DateTime.MinValue ? "NA" : li.EditedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      SubmittedBy = li.SubmittedBy,
                                      SubmittedOn = li.SubmittedOn == null || li.SubmittedOn.Value == DateTime.MinValue ? "NA" : li.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      ApprovedBy = li.ApprovedBy,
                                      ApproveOn = li.ApprovedOn == null || li.ApprovedOn.Value == DateTime.MinValue ? "NA" : li.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      // ReturnRemark = li.ReturnRemark

                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.HISTORYLINES.GetStringValue())
                {
                    var lst = db.SP_WKG_GET_LINESHISTORY(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      RowNo = li.ROW_NO,
                                      Remarks = li.Remarks,
                                      SeamNo = li.SeamNo,
                                      SeamDescription = li.SeamDescription,
                                      WEPType = li.WEPTYPE,
                                      LengthArea = li.LengthArea,
                                      Thk1 = li.Thk1,
                                      Thk2 = li.Thk2,
                                      Thk3 = li.Thk3,
                                      Thk4 = li.Thk4,
                                      Thk5 = li.Thk5,
                                      Overlay = li.Thk6,
                                      CsSS = li.CSSS,
                                      JointType = li.JointType,
                                      RootRunProcess = li.RootRunProcess,
                                      FaceSideProcess = li.FaceSideProcess,
                                      ChipBackProcess = li.ChipBackProcess,
                                      MinThk = li.MinThk,
                                      FacesideArea = li.FaceSideArea,
                                      CBArea = li.CBarea,
                                      WeldKGFaceside = li.WeldKgFaceSide,
                                      WeldKgCBSide = li.WeldKgCBSide,
                                      WeldKG = li.TotalWeldKg,
                                      PreparedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn == null || li.CreatedOn.Value == DateTime.MinValue ? "NA" : li.CreatedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),


                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        public string HTMLActionString(int rowId, string status, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";
            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick=" + onClickMethod : "";

            if (!string.Equals(status, clsImplementationEnum.CSRStatus.SendForDLApproval.GetStringValue()))
            {
                if (string.Equals(status, clsImplementationEnum.CSRStatus.ApprovedByDL.GetStringValue()))
                {
                    htmlControl = "";// "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
                }
                else
                {
                    htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
                }
            }

            return htmlControl;
        }

        [NonAction]
        public static string GenerateHTMLTextboxOnChanged(int rowId, string columnName, string columnValue = "", string onChangeMethod = "", bool isReadOnly = false, string inputStyle = "", bool disabled = false, string maxlength = "", string className = "")
        {
            string htmlControl = "";

            string inputID = columnName + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            className = "form-control " + className;
            string MaxCharcters = !string.IsNullOrEmpty(maxlength) ? "maxlength='" + maxlength + "'" : "";
            string onBlurEvent = !string.IsNullOrEmpty(onChangeMethod) ? "onchange='" + onChangeMethod + "'" : "";

            htmlControl = "<input type='text' " + (isReadOnly ? "disabled='disabled'" : "") + " id='" + inputID + "' " + MaxCharcters + " value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + (disabled ? "disabled" : "") + " />";

            return htmlControl;
        }
        #endregion

        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        // GET: WKG/ApproveWeldKG
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetWeldKGHeaderGridDataPartial(string status)
        {
            ViewBag.status = status;
            return PartialView("_GetWeldKGHeaderGridDataPartial");
        }

        public ActionResult loadHeaderDataTable(JQueryDataTableParamModel param, string status)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;

                string whereCondition = "1=1";
                //whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "wps10.BU", "wps10.Location");

                if (status.ToUpper() == "PENDING")
                {
                    whereCondition += " and status in('" + clsImplementationEnum.WKGHeaderStatus.SentForApproval.GetStringValue() + "') AND ApprovedBy=" + objClsLoginInfo.UserName+"";
                }
                else
                {
                    whereCondition += " and status in('" + clsImplementationEnum.WKGHeaderStatus.SentForApproval.GetStringValue() + "','" + clsImplementationEnum.WKGHeaderStatus.Approved.GetStringValue() + "')";
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

               // whereCondition += " AND ApprovedBy=" + objClsLoginInfo.UserName;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    //whereCondition += " and (bcom2.t_desc like '%" + param.sSearch + "%' or lcom2.t_desc like '%" + param.sSearch + "%' or Location like '%" + param.sSearch + "%' or wps10.WPSNumber like '%" + param.sSearch + "%' or WPSRevNo like '%" + param.sSearch + "%'  or Status like '%" + param.sSearch + "%' or wps10.CreatedBy like '%" + param.sSearch + "%')";
                    whereCondition += Helper.MakeDatatableSearchCondition(new string[] { "wkg1.Project", "com1.t_dsca", "Product", "ProcessLicensor", "wkg1.Customer", "lcom6.t_nama", "Document", "Status", "wkg1.RevNo" }, param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                var lstHeader = db.SP_WKG_GETHEADER(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from h in lstHeader
                          select new[] {
                        Convert.ToString(h.HeaderId),
                        h.Project,//db.COM001.Where(i=>i.t_cprj==h.Project).Select(i=>i.t_dsca).FirstOrDefault(),//h.Project,
                        h.Product,
                        h.ProcessLicensor,
                        h.Customer,
                        h.Document,
                        h.Status,
                        Convert.ToDateTime(h.CDD).ToString("dd/MM/yyyy"),
                        "<span>R</span>" +Convert.ToString(h.RevNo),
                        Convert.ToString(h.SubmittedBy),
                        Convert.ToString(Convert.ToDateTime(h.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(h.SubmittedOn).ToString("dd/MM/yyyy")),
                        Convert.ToString(h.ApprovedBy),
                        Convert.ToString(Convert.ToDateTime(h.ApprovedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(h.ApprovedOn).ToString("dd/MM/yyyy")),

                         "<nobr><center>"+
                           "<a class='iconspace' href='"+WebsiteURL+"/WKG/ApproveWeldKG/ViewWeldKGHeader?headerId=" + Convert.ToString(h.HeaderId) + "'><i class='fa fa-eye'></i></a>"+
                             "<i class ='iconspace fa fa-clock-o' title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/WKG/MaintainWeldKG/ShowTimeline?HeaderID=" + Convert.ToInt32(h.HeaderId) + "')></i>"+
                           (h.RevNo>0?"<i title='History' class='iconspace fa fa-history' onClick='GetHistoryDataPartial("+h.HeaderId+")'></i> ":"<i title='History' class='disabledicon fa fa-history' ></i>" )+
                            "<i title='Print'  class='iconspace fa fa-print' onClick='PrintReport("+h.HeaderId+")'></i>"+
                           "</center></nobr>",
                        //"<center><a title='View' target='_blank' href='/WKG/ApproveWeldKG/ViewWeldKGHeader?headerId=" + Convert.ToString(h.HeaderId) + "'><i style='margin-left:5px;' class='fa fa-eye'></i></a><i style='margin-left:5px;cursor:pointer;' class='fa fa-print' onClick='PrintReport("+h.HeaderId+")'></i> <a title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/WKG/MaintainWeldKG/ShowTimeline?HeaderID=" + Convert.ToInt32(h.HeaderId) + "')><i style='margin-left:5px;cursor:pointer;' class='fa fa-clock-o'></i></a>"
                        //+"  "+(h.RevNo>0?"<i title='History' style='margin-left:5px;cursor:pointer;' class='fa fa-history' onClick='GetHistoryDataPartial("+h.HeaderId+")'></i></center>":"</center>"),
                        //GetHTMLString(h.HeaderId)
                    };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult ViewWeldKGHeader(int headerId)
        {
            WKG001 objWKG001 = new WKG001();
            if (headerId > 0)
            {
                objWKG001 = db.WKG001.Where(i => i.HeaderId == headerId).FirstOrDefault();
                if (objWKG001.ApprovedBy != null)
                {
                    if (objWKG001.ApprovedBy.Trim() != objClsLoginInfo.UserName.Trim())
                    {
                        ViewBag.IsDisplayMode = true;
                    }
                    else
                    {
                        ViewBag.IsDisplayMode = false;
                    }
                }
                else
                {
                    ViewBag.IsDisplayMode = true;
                }
                ViewBag.Project = db.COM001.Where(i => i.t_cprj.Equals(objWKG001.Project, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_cprj + "-" + i.t_dsca).FirstOrDefault();
                ViewBag.Customer = db.COM006.Where(i => i.t_bpid.Equals(objWKG001.Customer, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_bpid + "-" + i.t_nama).FirstOrDefault();
                ViewBag.Approver = db.COM003.Where(i => i.t_psno.Equals(objWKG001.ApprovedBy, StringComparison.OrdinalIgnoreCase) && i.t_actv == 1).Select(i => i.t_psno + "-" + i.t_name).FirstOrDefault();

                var PlanningDinID = db.PDN002.Where(x => x.RefId == headerId && x.DocumentNo == objWKG001.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                ViewBag.PlanningDinID = PlanningDinID;
            }
            return View(objWKG001);
        }

        public ActionResult GetWeldKGLinesPartial(int headerId)
        {
            WKG001 objWKG001 = new WKG001();
            if (headerId > 0)
                objWKG001 = db.WKG001.Where(i => i.HeaderId == headerId).FirstOrDefault();

            var PlanningDinID = db.PDN002.Where(x => x.RefId == headerId && x.DocumentNo == objWKG001.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
            ViewBag.PlanningDinID = PlanningDinID;
            ViewBag.Revoke = Manager.IsRevokeApplicable(PlanningDinID, headerId, clsImplementationEnum.PlanList.Weld_KG_Plan.GetStringValue());
            if (objWKG001.ApprovedBy != null)
            {
                if (objWKG001.ApprovedBy.Trim() != objClsLoginInfo.UserName.Trim())
                {
                    ViewBag.IsDisplayMode = true;
                }
                else
                {
                    ViewBag.IsDisplayMode = false;
                }
            }
            else
            {
                ViewBag.IsDisplayMode = true;
            }

            return PartialView("_GetWeldKGLinesDataPartial", objWKG001);
        }

        public ActionResult LoadWeldKGLinesData(JQueryDataTableParamModel param, int headerId)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                WKG001 objWKG001 = db.WKG001.Where(x => x.HeaderId == headerId).FirstOrDefault();
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                bool isDeleteEnabled = false;
                bool isEditable = true;

                if (objWKG001.Status.ToLower() == clsImplementationEnum.WKGHeaderStatus.SentForApproval.GetStringValue().ToLower() &&  objWKG001.ApprovedBy == objClsLoginInfo.UserName)
                {
                    isDeleteEnabled = true;
                }
                if (objWKG001.Status.ToLower() == clsImplementationEnum.WKGHeaderStatus.SentForApproval.GetStringValue().ToLower() && objWKG001.ApprovedBy == objClsLoginInfo.UserName)
                {
                    isEditable = false;
                }
                string whereCondition = "1=1";
                //whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "wps10.BU", "wps10.Location");

                if (headerId > 0)
                    whereCondition += " and wkg2.HeaderId=" + headerId;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    //whereCondition += " and (bcom2.t_desc like '%" + param.sSearch + "%' or lcom2.t_desc like '%" + param.sSearch + "%' or Location like '%" + param.sSearch + "%' or wps10.WPSNumber like '%" + param.sSearch + "%' or WPSRevNo like '%" + param.sSearch + "%'  or Status like '%" + param.sSearch + "%' or wps10.CreatedBy like '%" + param.sSearch + "%')";
                    whereCondition += Helper.MakeDatatableSearchCondition(new string[] {"wkg2.SeamNo","qms12.SeamDescription","WEPTYPE","LengthArea","Thk1","Thk2","Thk3", "Thk4","Thk5","CSSS","JointType","RootRunProcess",
                                                                                       "FaceSideProcess","ChipBackProcess","FaceSideArea","CBarea","WeldKgFaceSide","WeldKgCBSide","TotalWeldKg" }, param.sSearch);
                }
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }


                var lstHeader = db.SP_WKG_GETLINES(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                #region Add new line section
                //Change By : Ajay Chauhan
                //Date : 27/11/2017
                //Description : Ann new lines functionality in inline grid
                //Assign From : Satish pawar
                int newRecordId = 0;
                var newRecord = new[] {
                              "",
                              Helper.GenerateHidden(newRecordId,"LineId",Convert.ToString(newRecordId)),
                              Helper.GenerateHidden(newRecordId,"HeaderId",Convert.ToString(headerId)),
                              GenerateHTMLTextboxOnChanged(newRecordId,"SeamNo","","",false,"",false,"30"),
                              GenerateHTMLTextboxOnChanged(newRecordId,"SeamDescription","","",false,"",false,"30"),
                              GenerateHTMLTextboxOnChanged(newRecordId,"WEPTYPE","","",false,"",false,"30"),
                              GenerateHTMLTextboxOnChanged(newRecordId,"LengthArea","","",false,"",false,"10"),
                              GenerateHTMLTextboxOnChanged(newRecordId,"Thk1","","",false,"",false,"10"),
                              GenerateHTMLTextboxOnChanged(newRecordId,"Thk2","","",false,"",false,"10"),
                              GenerateHTMLTextboxOnChanged(newRecordId,"Thk3","","",false,"",false,"10"),
                              GenerateHTMLTextboxOnChanged(newRecordId,"Thk4","","",false,"",false,"10"),
                              GenerateHTMLTextboxOnChanged(newRecordId,"Thk5","","",false,"",false,"10"),
                              GenerateHTMLTextboxOnChanged(newRecordId,"Thk6","","",false,"",false,"10"),
                              GenerateHTMLTextboxOnChanged(newRecordId,"CSSS","","",false,"",false,"10"),
                              GenerateHTMLTextboxOnChanged(newRecordId,"JointType","","",false,"",false,"20"),
                              GenerateHTMLTextboxOnChanged(newRecordId,"RootRunProcess","","",false,"",false,"10"),
                              GenerateHTMLTextboxOnChanged(newRecordId,"FaceSideProcess","","",false,"",false,"10"),
                              GenerateHTMLTextboxOnChanged(newRecordId,"ChipBackProcess","","",false,"",false,"10"),
                              GenerateHTMLTextboxOnChanged(newRecordId,"MinThk","","",false,"",false,"10"),
                              GenerateHTMLTextboxOnChanged(newRecordId,"FaceSideArea","","",false,"DECIMAL",false,"18"),
                              GenerateHTMLTextboxOnChanged(newRecordId,"CBArea","","",false,"DECIMAL",false,"18"),
                              GenerateHTMLTextboxOnChanged(newRecordId,"WeldKgFaceSide","","",false,"DECIMAL",false,"18"),
                              GenerateHTMLTextboxOnChanged(newRecordId,"WeldKgCBSide","","",false,"DECIMAL",false,"18"),
                              GenerateHTMLTextboxOnChanged(newRecordId,"TotalWeldKg","","",false,"DECIMAL",false,"18"),
                              GenerateHTMLTextboxOnChanged(newRecordId,"Remarks","","",false,"",false,"100"),
                              GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveNewRecord();" ),

                };
                #endregion

                var res = (from h in lstHeader
                          select new[] {
                              Convert.ToString(h.ROW_NO),
                              Helper.GenerateHidden(h.LineId,"LineId",Convert.ToString(h.LineId)),
                              Helper.GenerateHidden(h.HeaderId,"HeaderId",Convert.ToString(h.HeaderId)),
                              "<nobr>"+Convert.ToString(h.SeamNo)+"</nobr>",
                              "<nobr>"+Convert.ToString(h.SeamDescription)+"</nobr>",
                              GenerateHTMLTextboxOnChanged(h.LineId,"WEPTYPE",Convert.ToString(h.WEPTYPE),"UpdateWeldKGLines(this,"+h.LineId+")",false,"",((string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase) && !isEditable)?false:true),"30"),//Convert.ToString(h.WEPTYPE),
                              GenerateHTMLTextboxOnChanged(h.LineId,"LengthArea",Convert.ToString(h.LengthArea),"UpdateWeldKGLines(this,"+h.LineId+")",false,"",((string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase) && !isEditable)?false:true),"10","numeric"),//Convert.ToString(h.LengthArea),
                              GenerateHTMLTextboxOnChanged(h.LineId,"Thk1",Convert.ToString(h.Thk1),"UpdateWeldKGLines(this,"+h.LineId+")",false,"",((string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase) && !isEditable)?false:true),"10","numeric"),//Convert.ToString(h.Thk1),
                              GenerateHTMLTextboxOnChanged(h.LineId,"Thk2",Convert.ToString(h.Thk2),"UpdateWeldKGLines(this,"+h.LineId+")",false,"",((string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase) && !isEditable)?false:true),"10","numeric"),//Convert.ToString(h.Thk2),
                              GenerateHTMLTextboxOnChanged(h.LineId,"Thk3",Convert.ToString(h.Thk3),"UpdateWeldKGLines(this,"+h.LineId+")",false,"",((string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase) && !isEditable) ? false:true),"10","numeric"),//Convert.ToString(h.Thk3),
                              GenerateHTMLTextboxOnChanged(h.LineId,"Thk4",Convert.ToString(h.Thk4),"UpdateWeldKGLines(this,"+h.LineId+")",false,"",((string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase) && !isEditable) ? false:true),"10","numeric"),//Convert.ToString(h.Thk4),
                              GenerateHTMLTextboxOnChanged(h.LineId,"Thk5",Convert.ToString(h.Thk5),"UpdateWeldKGLines(this,"+h.LineId+")",false,"",((string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase) && !isEditable) ? false:true),"10","numeric"),//Convert.ToString(h.Thk5),
                              GenerateHTMLTextboxOnChanged(h.LineId,"Thk6",Convert.ToString(h.Thk6),"UpdateWeldKGLines(this,"+h.LineId+")",false,"",((string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase) && !isEditable) ? false:true),"10","numeric"),//Convert.ToString(h.Thk6),
                              GenerateHTMLTextboxOnChanged(h.LineId,"CSSS",Convert.ToString(h.CSSS),"UpdateWeldKGLines(this,"+h.LineId+")",false,"",((string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase) && !isEditable) ? false:true),"10","numeric"),//Convert.ToString(h.CSSS),
                              Convert.ToString(h.JointType),
                              Convert.ToString(h.RootRunProcess),
                              Convert.ToString(h.FaceSideProcess),
                              Convert.ToString(h.ChipBackProcess),
                              GenerateHTMLTextboxOnChanged(h.LineId,"MinThk",Convert.ToString(h.MinThk),"UpdateWeldKGLines(this,"+h.LineId+")",false,"",((string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase) && !isEditable) ? false:true),"10","numeric"),
                              GenerateHTMLTextboxOnChanged(h.LineId,"FaceSideArea",Convert.ToString(h.FaceSideArea),"UpdateWeldKGLines(this,"+h.LineId+")",false,"",((string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase) && !isEditable) ? false:true),"18","DECIMAL"),
                              GenerateHTMLTextboxOnChanged(h.LineId,"CBArea",Convert.ToString(h.CBarea),"UpdateWeldKGLines(this,"+h.LineId+")",false,"",((string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase) && !isEditable) ? false:true),"18","DECIMAL"),
                              GenerateHTMLTextboxOnChanged(h.LineId,"WeldKgFaceSide",Convert.ToString(h.WeldKgFaceSide),"UpdateWeldKGLines(this,"+h.LineId+")",false,"",((string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase) && !isEditable) ? false:true),"18","DECIMAL"),
                              GenerateHTMLTextboxOnChanged(h.LineId,"WeldKgCBSide",Convert.ToString(h.WeldKgCBSide),"UpdateWeldKGLines(this,"+h.LineId+")",false,"",((string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase) && !isEditable) ? false:true),"18","DECIMAL"),
                              GenerateHTMLTextboxOnChanged(h.LineId,"TotalWeldKg",Convert.ToString(h.TotalWeldKg),"UpdateWeldKGLines(this,"+h.LineId+")",false,"",((string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase) && !isEditable) ? false:true),"18","DECIMAL"),
                              GenerateHTMLTextboxOnChanged(h.LineId,"Remarks",Convert.ToString(h.Remarks),"UpdateWeldKGLines(this,"+h.LineId+")",false,"",((string.Equals(h.Status,clsImplementationEnum.WKGHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase) && !isEditable) ? false:true), "100"),
                              h.IsManualLine ? isDeleteEnabled ? HTMLActionString(h.LineId,"","Delete","Delete Record","fa fa-trash-o","DeleteRecord("+ h.LineId +");") : HTMLActionString(h.LineId,"","Delete","Delete Record","fa fa-trash-o disabledicon","DeleteRecord("+ h.LineId +");") : "",
                    }).ToList();
                res.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult ApproveWeldKGHeader(string strHeaderIds)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (!string.IsNullOrEmpty(strHeaderIds))
                {
                    int[] headerIds = Array.ConvertAll(strHeaderIds.Split(','), i => Convert.ToInt32(i));
                    foreach (var headerId in headerIds)
                    {
                        db.SP_WKG_APPROVEHEADER(headerId, objClsLoginInfo.UserName);
                        var objWKG001 = db.WKG001.Where(i => i.HeaderId == headerId).FirstOrDefault();
                        int newId = db.WKG001_Log.Where(q => q.HeaderId == headerId).OrderByDescending(q => q.ApprovedOn).FirstOrDefault().Id;
                        Manager.UpdatePDN002(objWKG001.HeaderId, objWKG001.Status, objWKG001.RevNo, objWKG001.Project, objWKG001.Document, newId);

                        #region Send Notification
                        (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue(),
                                                            objWKG001.Project,
                                                            "",
                                                            "",
                                                            Manager.GetPDINDocumentNotificationMsg(objWKG001.Project, clsImplementationEnum.PlanList.Weld_KG_Plan.GetStringValue(), objWKG001.RevNo.Value.ToString(), objWKG001.Status),
                                                            clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(),
                                                            Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Weld_KG_Plan.GetStringValue(), objWKG001.HeaderId.ToString(), false),
                                                            objWKG001.CreatedBy);
                        #endregion
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = string.Format(clsImplementationMessage.WKGHeader.Approved.ToString());
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReturnWKGHeader(int headerId, string returnRemark)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            WKG001 objWKG001 = new WKG001();
            try
            {
                if (headerId > 0)
                {
                    objWKG001 = db.WKG001.Where(i => i.HeaderId == headerId).FirstOrDefault();
                    objWKG001.Status = clsImplementationEnum.WKGHeaderStatus.Returned.GetStringValue();
                    objWKG001.ReturnRemark = returnRemark;
                    db.SaveChanges();
                    Manager.UpdatePDN002(objWKG001.HeaderId, objWKG001.Status, objWKG001.RevNo, objWKG001.Project, objWKG001.Document);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Document Returned Succesfully";

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue(),
                                                        objWKG001.Project,
                                                        "",
                                                        "",
                                                        Manager.GetPDINDocumentNotificationMsg(objWKG001.Project, clsImplementationEnum.PlanList.Weld_KG_Plan.GetStringValue(), objWKG001.RevNo.Value.ToString(), objWKG001.Status),
                                                        clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(),
                                                        Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Weld_KG_Plan.GetStringValue(), objWKG001.HeaderId.ToString(), false),
                                                        objWKG001.CreatedBy);
                    #endregion
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Document do not exist";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetHistoryDataPartial(int headerId)
        {
            WKG001 objWKG001 = new WKG001();
            if (headerId > 0)
            {
                objWKG001 = db.WKG001.Where(i => i.HeaderId == headerId).FirstOrDefault();
            }
            return PartialView("_GetHistoryDataPartial", objWKG001);
        }

        [HttpPost]
        public JsonResult LoadHistoryDetailData(JQueryDataTableParamModel param, int headerId)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;

                string whereCondition = "1=1";
                //whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qcp1.BU", "qcp1.Location");
                whereCondition += "and wkg1.HeaderId = " + headerId;

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    //whereCondition += " and (bcom2.t_desc like '%" + param.sSearch + "%' or lcom2.t_desc like '%" + param.sSearch + "%' or (qcp1.Project+'-'+com1.t_dsca) like '%" + param.sSearch + "%' or DocumentNo like '%" + param.sSearch + "%' or QCPRev like '%" + param.sSearch + "%'  or Status like '%" + param.sSearch + "%')";
                    whereCondition += Helper.MakeDatatableSearchCondition(new string[] { "wkg1.Project", "com1.t_dsca", "Product", "ProcessLicensor", "wkg1.Customer", "lcom6.t_nama", "Document", "wkg1.RevNo", "Status", "wkg1.CreatedBy", "ecom3.t_name", "wkg1.EditedBy", "com3e.t_name", "CreatedOn", "EditedOn" }, param.sSearch);
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                whereCondition += " AND ApprovedBy like '%" + objClsLoginInfo.UserName + "%'";
                var lstResult = db.SP_WKG_GET_HEADERHISTORY(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstResult.Select(i => i.TotalCount).FirstOrDefault();

                var data = (from uc in lstResult
                            select new[]
                           {
                           Convert.ToString(uc.Id),
                           Convert.ToString(uc.Project),
                           Convert.ToString(uc.Product),
                           Convert.ToString(uc.ProcessLicensor),
                           Convert.ToString(uc.Customer),
                           Convert.ToString(uc.Document),
                           Convert.ToString("R" + uc.RevNo),
                           Convert.ToString(uc.Status),
                           uc.CreatedBy,
                           Convert.ToDateTime( uc.CreatedOn).ToString("dd/MM/yyyy"),
                           uc.EditedBy,
                           Convert.ToDateTime( uc.EditedOn).ToString("dd/MM/yyyy"),
                            Convert.ToString(uc.SubmittedBy),
                            Convert.ToString(Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy")),
                            Convert.ToString(uc.ApprovedBy),
                            Convert.ToString(Convert.ToDateTime(uc.ApprovedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.ApprovedOn).ToString("dd/MM/yyyy")),

                          // "<center><a Title='View' target='_blank' href='/WKG/ApproveWeldKG/ViewHistory?id=" + Convert.ToString(uc.Id) + "'><i style = 'margin-left:5px;' class='fa fa-eye'></i></a><a title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/WKG/MaintainWeldKG/ShowHistoryTimeline?HeaderID=" + Convert.ToInt32(uc.Id) + "')><i style = 'margin-left:5px;'  class='fa fa-clock-o'></i></a></center>"
                           "<center><a title='View' class='iconspace' target='_blank' href='"+WebsiteURL+"/WKG/ApproveWeldKG/ViewHistory?id=" + Convert.ToString(uc.Id) + "'><i class='fa fa-eye'></i></a><i class ='iconspace fa fa-clock-o' title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/WKG/MaintainWeldKG/ShowHistoryTimeline?HeaderID=" + Convert.ToInt32(uc.Id) + "')></i></center>"
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        //[SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult ViewHistory(int id)
        {
            WKG001_Log objWKG001 = new WKG001_Log();
            if (id > 0)
            {
                objWKG001 = db.WKG001_Log.Where(i => i.Id == id).FirstOrDefault();
                //objWKG001 = db.WKG001.Where(i => i.HeaderId == headerId).FirstOrDefault();
                ViewBag.Project = db.COM001.Where(i => i.t_cprj.Equals(objWKG001.Project, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_cprj + "-" + i.t_dsca).FirstOrDefault();
                ViewBag.Approver = db.COM003.Where(i => i.t_psno.Equals(objWKG001.ApprovedBy, StringComparison.OrdinalIgnoreCase) && i.t_actv == 1).Select(i => i.t_psno + "-" + i.t_name).FirstOrDefault();
            }
            var urlPrefix = Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.LastIndexOf('=') + 1);
            if (!objClsLoginInfo.GetUserRoleList().Contains("SHOP"))
            {
                ViewBag.RevPrev = (db.WKG001_Log.Any(q => (q.HeaderId == objWKG001.HeaderId && q.RevNo == (objWKG001.RevNo - 1))) ? urlPrefix + db.WKG001_Log.Where(q => (q.HeaderId == objWKG001.HeaderId && q.RevNo == (objWKG001.RevNo - 1))).FirstOrDefault().Id : null);
                ViewBag.RevNext = (db.WKG001_Log.Any(q => (q.HeaderId == objWKG001.HeaderId && q.RevNo == (objWKG001.RevNo + 1))) ? urlPrefix + db.WKG001_Log.Where(q => (q.HeaderId == objWKG001.HeaderId && q.RevNo == (objWKG001.RevNo + 1))).FirstOrDefault().Id : null);
            }
            return View(objWKG001);
        }

        public ActionResult GetWeldKGLinesHistoryPartial(int refId)
        {
            return PartialView("_GetWeldKGLinesHistoryPartial");
        }

        public ActionResult LoadWeldKGLinesHistoryData(JQueryDataTableParamModel param, int refId)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;

                string whereCondition = "1=1";
                //whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "wps10.BU", "wps10.Location");

                if (refId > 0)
                    whereCondition += " and RefId=" + refId;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    //whereCondition += " and (bcom2.t_desc like '%" + param.sSearch + "%' or lcom2.t_desc like '%" + param.sSearch + "%' or Location like '%" + param.sSearch + "%' or wps10.WPSNumber like '%" + param.sSearch + "%' or WPSRevNo like '%" + param.sSearch + "%'  or Status like '%" + param.sSearch + "%' or wps10.CreatedBy like '%" + param.sSearch + "%')";
                    whereCondition += Helper.MakeDatatableSearchCondition(new string[] {"wkg2.SeamNo","qms12.SeamDescription","WEPTYPE","LengthArea","Thk1","Thk2","Thk3", "Thk4","Thk5","CSSS","JointType","RootRunProcess",
                                                                                       "FaceSideProcess","ChipBackProcess","FaceSideArea","CBarea","WeldKgFaceSide","WeldKgCBSide","TotalWeldKg" }, param.sSearch);
                }
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstHeader = db.SP_WKG_GET_LINESHISTORY(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from h in lstHeader
                          select new[] {
                              Convert.ToString(h.ROW_NO),
                              Helper.GenerateHidden(h.LineId,"LineId",Convert.ToString(h.LineId)),
                              Helper.GenerateHidden(h.HeaderId,"HeaderId",Convert.ToString(h.HeaderId)),
                              "<nobr>"+Convert.ToString(h.SeamNo)+"</nobr>",
                              "<nobr>"+Convert.ToString(h.SeamDescription)+"</nobr>",
                              Convert.ToString(h.WEPTYPE),
                              //"",
                              Convert.ToString(h.LengthArea),
                              Convert.ToString(h.Thk1),
                              Convert.ToString(h.Thk2),
                              Convert.ToString(h.Thk3),
                              Convert.ToString(h.Thk4),
                              Convert.ToString(h.Thk5),
                              Convert.ToString(h.Thk6),
                              Convert.ToString(h.CSSS),
                              Convert.ToString(h.JointType),
                              Convert.ToString(h.RootRunProcess),
                              Convert.ToString(h.FaceSideProcess),
                              Convert.ToString(h.ChipBackProcess),
                              Convert.ToString(h.MinThk),
                              Convert.ToString(h.FaceSideArea),
                              Convert.ToString(h.CBarea),
                              Convert.ToString(h.WeldKgFaceSide),
                              Convert.ToString(h.WeldKgCBSide),
                              Convert.ToString(h.TotalWeldKg),
                              Convert.ToString(h.Remarks),
                              //Convert.ToString(h.seam),
                    };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }


        public string GetHTMLString(int headerId)
        {
            string strHTML = string.Empty;
            strHTML = "<center><a title='Title' href='"+WebsiteURL+"/WKG/ApproveWeldKG/ViewWeldKGHeader?headerId=" + Convert.ToString(headerId) + "'><i style='margin-left:5px;' class='fa fa-eye'></i></a></center>";

            return strHTML;
        }

        [HttpPost]
        public ActionResult RevokeDocument(int HeaderId, int pdinId, string Remarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (Manager.IsRevokeApplicable(pdinId, HeaderId, clsImplementationEnum.PlanList.Weld_KG_Plan.GetStringValue()))
                {
                    objResponseMsg.Key = Manager.RevokePDINDocument(HeaderId, clsImplementationEnum.PlanList.Weld_KG_Plan, Remarks);
                    if (objResponseMsg.Key)
                    {
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Revoke;
                    }
                    else
                    {
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.UnSuccess;
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.pdnRevokeMessage;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
    }
}