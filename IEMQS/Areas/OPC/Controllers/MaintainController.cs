﻿using IEMQS.Areas.MSW.Controllers;
using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsHelper;

namespace IEMQS.Areas.OPC.Controllers
{
    public class MaintainController : clsBase
    {
        // GET: OPC/Maintain
        [SessionExpireFilter]
        #region Main Grid
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetOPCGridPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetOPCGridPartial");
        }
        //datatable function for header
        [HttpPost]
        public JsonResult LoadOPCHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var user = objClsLoginInfo.UserName;

                string strWhere = string.Empty;
                strWhere += "1=1";

                #region sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                #region searching
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] columnName = { "com1.t_dsca", "Project", "Document", "Customer", "RevNo", "Product", "ProcessLicensor", "Status" };
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                #endregion

                var lstResult = db.SP_OPC_GET_HEADERDETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.Project),
                                Convert.ToString(uc.Document),
                                Convert.ToString(Manager.GetCustomerCodeAndNameByProject(uc.Project)),
                                Convert.ToString(uc.Product),
                                Convert.ToString(uc.ProcessLicensor),
                                Convert.ToString("R"+uc.RevNo),
                                Convert.ToString(uc.Status),
                                Convert.ToString(uc.SubmittedBy),
                                uc.SubmittedOn == null || uc.SubmittedOn.Value==DateTime.MinValue? "NA" : uc.SubmittedOn.Value.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture),
                                Convert.ToString(uc.ApprovedBy),
                                uc.ApprovedOn == null || uc.ApprovedOn.Value==DateTime.MinValue? "NA" : uc.ApprovedOn.Value.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture),
                               "<nobr><center>" +  
                               "<a title='View' href='"+WebsiteURL+"/OPC/Maintain/GetOPCDetails?Id="+Convert.ToInt32(uc.HeaderId)+"'><i class='iconspace fa fa-eye'></i></a>"+
                                  Helper.GenerateActionIcon(uc.HeaderId,"Delete","Delete Record","fa fa-trash-o", "DeleteDocument("+ uc.HeaderId +",'/OPC/Maintain/DeleteHeader', {headerid:"+uc.HeaderId+"}, 'tblOPCHeader')","",  (( uc.RevNo >0 && uc.Status != clsImplementationEnum.CommonStatus.SendForApprovel.GetStringValue()) || ( uc.RevNo == 0 && uc.Status == clsImplementationEnum.CommonStatus.Approved.GetStringValue()) ) ? false:true) +
                               (uc.RevNo>0 ?"<i title=\"History\" onclick=\"ViewHistoryForProjectPLN('"+uc.HeaderId+"','Initiator','/OPC/History/GetHistoryDetails','Operation Cards')\"  class='iconspace fa fa-history'></i>":"<i title=\"History\" class='disabledicon fa fa-history'></i>")+
                               "<i title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/OPC/Maintain/ShowTimeline?HeaderID=" + Convert.ToInt32(uc.HeaderId) + "')  class='iconspace fa fa-clock-o'></i>"
                               +"</center></nobr>",
                                Convert.ToString(uc.HeaderId),
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult DeleteHeader(int headerid)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                objResponseMsg = Manager.DeletePDINDocument(headerid, clsImplementationEnum.PlanList.Operation_Cards);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Maintain/Detail Page
        //main page
        [SessionExpireFilter]
        public ActionResult GetOPCDetails(int Id = 0)
        {
            OPC001 objOPC001 = new OPC001();

            if (Id > 0)
            {
                objOPC001 = db.OPC001.Where(x => x.HeaderId == Id).FirstOrDefault();
                ViewBag.Project = db.COM001.Where(i => i.t_cprj == objOPC001.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault(); ;
                if (objOPC001.ApprovedBy != null)
                {
                    ViewBag.ApproverName = db.COM003.Where(x => x.t_psno == objOPC001.ApprovedBy && x.t_actv == 1).Select(x => objOPC001.ApprovedBy + "-" + x.t_name).FirstOrDefault();
                }
                else { ViewBag.ApproverName = ""; }
                ViewBag.Customer = Manager.GetCustomerCodeAndNameByProject(objOPC001.Project);
                var PlanningDinID = db.PDN002.Where(x => x.RefId == Id && x.DocumentNo == objOPC001.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                ViewBag.PlanningDinID = PlanningDinID;
                ViewBag.IsReviseBtnEnabled = Manager.IsReviseEnabled(PlanningDinID, objOPC001.HeaderId, objOPC001.Document);
                ViewBag.DocMessage = clsImplementationMessage.CommonMessages.DocMessage.ToString();
            }

            return View(objOPC001);
        }

        //save/update header
        [HttpPost]
        public ActionResult SaveHeader(OPC001 opc001)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                if (opc001.HeaderId > 0)
                {
                    OPC001 objOPC001 = db.OPC001.Where(x => x.HeaderId == opc001.HeaderId).FirstOrDefault();
                    if (objOPC001 != null)
                    {
                        objOPC001.Product = opc001.Product;
                        objOPC001.ReviseRemark = opc001.ReviseRemark;
                        objOPC001.ProcessLicensor = opc001.ProcessLicensor;
                        objOPC001.ApprovedBy = opc001.ApprovedBy.Split('-')[0].ToString().Trim();
                        objOPC001.EditedBy = objClsLoginInfo.UserName.Trim();
                        objOPC001.EditedOn = DateTime.Now;
                        //if (objOPC001.Status == clsImplementationEnum.PTMTCTQStatus.Approved.GetStringValue())
                        //{
                        //    objOPC001.RevNo = Convert.ToInt32(objOPC001.RevNo) + 1;
                        //    objOPC001.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                        //}
                        db.SaveChanges();
                        var newId = db.OPC001.Where(q => q.Project.Equals(objOPC001.Project)).FirstOrDefault().HeaderId;

                        objResponseMsg.Key = true;
                        objResponseMsg.HeaderId = objOPC001.HeaderId;
                        objResponseMsg.Value = clsImplementationMessage.PlanMessages.Update.ToString();
                        objResponseMsg.Revision = objOPC001.RevNo.ToString();
                        objResponseMsg.status= objOPC001.Status;
                        Manager.UpdatePDN002(objOPC001.HeaderId, objOPC001.Status, objOPC001.RevNo, objOPC001.Project, objOPC001.Document);
                        var folderPath = "OPC001/" + newId + "/R" + objOPC001.RevNo;
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Process Details not available.";
                    }
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        //send header
        [HttpPost]
        public JsonResult sentForApproval(int headerid)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (headerid > 0)
                {
                    OPC001 objOPC001 = db.OPC001.Where(x => x.HeaderId == headerid).FirstOrDefault();
                    objOPC001.Status = clsImplementationEnum.CTQStatus.SendForApprovel.GetStringValue();
                    objOPC001.SubmittedBy = objClsLoginInfo.UserName;
                    objOPC001.SubmittedOn = DateTime.Now;
                    db.SaveChanges();
                    Manager.UpdatePDN002(objOPC001.HeaderId, objOPC001.Status, objOPC001.RevNo, objOPC001.Project, objOPC001.Document);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.AMessage.ToString();

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG2.GetStringValue() + "," + clsImplementationEnum.UserRoleName.PLNG1.GetStringValue(),
                                                        objOPC001.Project,
                                                        "",
                                                        "",
                                                        Manager.GetPDINDocumentNotificationMsg(objOPC001.Project, clsImplementationEnum.PlanList.Operation_Cards.GetStringValue(), objOPC001.RevNo.Value.ToString(), objOPC001.Status),
                                                        clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(),
                                                        Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Operation_Cards.GetStringValue(), objOPC001.HeaderId.ToString(), true),
                                                        objOPC001.ApprovedBy);
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        //retract header
        [HttpPost]
        public JsonResult RetractHeader(int Id = 0)
        {
            ResponseMsg objResponseMsg = new ResponseMsg();
            try
            {
                var objOPC001 = db.OPC001.Where(q => q.HeaderId == Id).FirstOrDefault();
                if (objOPC001 != null)
                {
                    if (objOPC001.CreatedBy.Trim().Equals(objClsLoginInfo.UserName.Trim()))
                    {
                        if (objOPC001.Status.ToLower().Equals(clsImplementationEnum.CommonStatus.SendForApprovel.GetStringValue().ToLower()))
                        {
                            objOPC001.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                            objOPC001.SubmittedOn = null;
                            objOPC001.SubmittedBy = null;
                            db.Entry(objOPC001).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = "Retacted Successfully";
                            Manager.UpdatePDN002(objOPC001.HeaderId, objOPC001.Status, objOPC001.RevNo, objOPC001.Project, objOPC001.Document);
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "Only headers that are sent for approval can be retracted.";
                        }
                    }
                    else
                    {

                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "You are not authorized.";
                    }
                }
                else
                {

                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Header not found.";
                }
            }
            catch (Exception)
            {

                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error while retracting Header.";

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        //revise header
        [HttpPost]
        public ActionResult ReviseHeader(int strHeaderId, string strRemarks)
        {
            CustomResponceMsg objResponseMsg = new CustomResponceMsg();
            try
            {
                OPC001 objOPC001 = db.OPC001.Where(u => u.HeaderId == strHeaderId).SingleOrDefault();
                if (objOPC001 != null)
                {
                    objOPC001.RevNo = Convert.ToInt32(objOPC001.RevNo) + 1;
                    objOPC001.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                    objOPC001.ReviseRemark = strRemarks;
                    objOPC001.EditedBy = objClsLoginInfo.UserName;
                    objOPC001.EditedOn = DateTime.Now;
                    objOPC001.ReturnRemark = null;
                    objOPC001.ApprovedOn = null;
                    objOPC001.SubmittedBy = null;
                    objOPC001.SubmittedOn = null;
                    db.SaveChanges();
                    Manager.UpdatePDN002(objOPC001.HeaderId, objOPC001.Status, objOPC001.RevNo, objOPC001.Project, objOPC001.Document);
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderID = objOPC001.HeaderId;
                    objResponseMsg.Status = objOPC001.Status;
                    objResponseMsg.rev = objOPC001.RevNo;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Revision;
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        //Main timeline 
        public ActionResult ShowTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();
            model.Title = "PDinDoc";
            model.TimelineTitle = "Operation Cards Timeline";

            if (HeaderId > 0)
            {
                OPC001 objOPC001 = db.OPC001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.CreatedBy = objOPC001.CreatedBy != null ? Manager.GetUserNameFromPsNo(objOPC001.CreatedBy) : null;
                model.CreatedOn = objOPC001.CreatedOn;
                model.EditedBy = objOPC001.EditedBy != null ? Manager.GetUserNameFromPsNo(objOPC001.EditedBy) : null;
                model.EditedOn = objOPC001.EditedOn;
                model.SubmittedBy = objOPC001.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objOPC001.SubmittedBy) : null;
                model.SubmittedOn = objOPC001.SubmittedOn;
                model.ApprovedBy = objOPC001.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objOPC001.ApprovedBy) : null;
                model.ApprovedOn = objOPC001.ApprovedOn;
            }
            else
            {
                OPC001 objOPC001 = db.OPC001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.CreatedBy = objOPC001.CreatedBy != null ? Manager.GetUserNameFromPsNo(objOPC001.CreatedBy) : null;
                model.CreatedOn = objOPC001.CreatedOn;
                model.EditedBy = objOPC001.EditedBy != null ? Manager.GetUserNameFromPsNo(objOPC001.EditedBy) : null;
                model.EditedOn = objOPC001.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }

        #endregion

        #region Export Excel
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_OPC_GET_HEADERDETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      Document = li.Document,
                                      Customer = li.Customer,
                                      Product = li.Product,
                                      ProcessLicensor = li.ProcessLicensor,
                                      Status = li.Status,
                                      RevNo = "R" + li.RevNo,
                                      SubmittedBy = li.SubmittedBy,
                                      SubmittedOn = li.SubmittedOn,
                                      ApprovedBy = li.ApprovedBy,
                                      ApprovedOn = li.ApprovedOn,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}