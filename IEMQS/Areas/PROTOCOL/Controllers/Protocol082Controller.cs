﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol082Controller : clsBase
    {
        // GET: PROTOCOL/Protocol082
        string ControllerURL = "/PROTOCOL/Protocol082/";
        string Title = "REPORT FOR VISUAL AND DIMENSION INSPECTION FOR NUB ON D'END AFTER OVERLAY";


        #region Details View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO405 objPRO405 = new PRO405();

            if (!id.HasValue)
            {
                try
                {
                    objPRO405.ProtocolNo = string.Empty;
                    objPRO405.CreatedBy = objClsLoginInfo.UserName;
                    objPRO405.CreatedOn = DateTime.Now;



                    #region  OrientationReqNubID 

                    objPRO405.PRO407.Add(new PRO407
                    {

                        OrientationReqNubID = "0°-180°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO405.PRO407.Add(new PRO407
                    {

                        OrientationReqNubID = "22.5°-202.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO405.PRO407.Add(new PRO407
                    {

                        OrientationReqNubID = "45°-225°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO405.PRO407.Add(new PRO407
                    {

                        OrientationReqNubID = "67.5°-247.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO405.PRO407.Add(new PRO407
                    {

                        OrientationReqNubID = "90°-270°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO405.PRO407.Add(new PRO407
                    {

                        OrientationReqNubID = "112.5°-292.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO405.PRO407.Add(new PRO407
                    {

                        OrientationReqNubID = "135°-315°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO405.PRO407.Add(new PRO407
                    {

                        OrientationReqNubID = "157.5°-337.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });


                    #endregion


                    #region  OtherDimensions 

                    objPRO405.PRO408.Add(new PRO408
                    {

                        OtherDimensions = "SURFACE FINISH ON MACHINED SURFACES",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO405.PRO408.Add(new PRO408
                    {

                        OtherDimensions = "LEVEL OF D'END TRIMMING LINE / REF. LINE / WEP",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO405.PRO408.Add(new PRO408
                    {

                        OtherDimensions = "CONCENTRICITY OF NUB TO D'END OPEN END",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO405.PRO408.Add(new PRO408
                    {

                        OtherDimensions = "DEPTH OF BARRIER LAYER FROM OVERLAY ON NUB FACE (TOP SIDE )",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO405.PRO408.Add(new PRO408
                    {

                        OtherDimensions = "DEPTH OF BARRIER LAYER FROM OVERLAY ON NUB FACE  (I.D. SIDE )",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });

                    objPRO405.PRO408.Add(new PRO408
                    {

                        OtherDimensions = "DEPTH OF BARRIER LAYER FROM OVERLAY ON D'END I.D. (TOP SIDE )",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO405.PRO408.Add(new PRO408
                    {

                        OtherDimensions = "DEPTH OF BARRIER LAYER FROM OVERLAY ON D'END I.D. (BOTTOM SIDE )",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO405.PRO408.Add(new PRO408
                    {

                        OtherDimensions = "RADIUS ON BARRIER LAYER (TOP SIDE)",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO405.PRO408.Add(new PRO408
                    {

                        OtherDimensions = "RADIUS ON BARRIER LAYER (BOTTOM SIDE)",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });


                    #endregion

                    db.PRO405.Add(objPRO405);
                    db.SaveChanges();
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO405.HeaderId;

                List<PRO401> _lstPRO401 = db.PRO401.Where(w => w.HeaderId == objPRO405.HeaderId).ToList();



                for (int i = 0; i < _lstPRO401.Count; i++)
                {
                    PRO406 objPRO406Copy = new PRO406();

                    objPRO406Copy.HeaderId = objPRO405.HeaderId;


                    objPRO406Copy.Orientation = _lstPRO401[i].Orientation;
                    objPRO406Copy.ActTolReqElevationOfNubFrom = "";
                    objPRO406Copy.ActDimentionOfNubId = "";
                    objPRO406Copy.ElevationOfNubBeforeOvarlay = _lstPRO401[i].ActADimension;
                    objPRO406Copy.DimensionOfNubIdBeforeOvarlay = _lstPRO401[i].ActBDimension;
                    objPRO406Copy.OvarlayThicknessNubTopSide = "";
                    objPRO406Copy.OvarlayThicknessNubIdSide = "";



                    objPRO406Copy.CreatedBy = objClsLoginInfo.UserName;
                    objPRO406Copy.CreatedOn = DateTime.Now;

                    db.PRO406.Add(objPRO406Copy);
                    db.SaveChanges();
                }


            }
            else
            {
                objPRO405 = db.PRO405.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstElevationOfNubFrom = new List<string> { "TRIMMING LINE", "REF. LINE", "B.T.L", "WEP" };


            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.ElevationOfNubFrom = lstElevationOfNubFrom.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();


            ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO406> lstPRO406 = db.PRO406.Where(x => x.HeaderId == objPRO405.HeaderId).ToList();
            List<PRO407> lstPRO407 = db.PRO407.Where(x => x.HeaderId == objPRO405.HeaderId).ToList();

            List<PRO408> lstPRO408 = db.PRO408.Where(x => x.HeaderId == objPRO405.HeaderId).ToList();

            ViewBag.lstPRO406 = lstPRO406;
            ViewBag.lstPRO407 = lstPRO407;

            ViewBag.lstPRO408 = lstPRO408;


            #endregion

            return View(objPRO405);
        }

        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO405 PRO405)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO405.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO405 objPRO405 = db.PRO405.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO405.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO405.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data

                        objPRO405.ProtocolNo = PRO405.ProtocolNo;
                        objPRO405.ElevationOfNubFrom = PRO405.ElevationOfNubFrom;
                        objPRO405.ReqElevationOfNubFrom = PRO405.ReqElevationOfNubFrom;
                        objPRO405.ReqDimensionOfNubId = PRO405.ReqDimensionOfNubId;
                        objPRO405.ReqNubTopSide = PRO405.ReqNubTopSide;
                        objPRO405.ReqNubIdSide = PRO405.ReqNubIdSide;
                        objPRO405.TolReqElevationOfNubFrom = PRO405.TolReqElevationOfNubFrom;
                        objPRO405.TolReqDimensionOfNubId = PRO405.TolReqDimensionOfNubId;
                        objPRO405.NubID = PRO405.NubID;
                        objPRO405.ReqNubID = PRO405.ReqNubID;
                        objPRO405.OutByNubID = PRO405.OutByNubID;
                        objPRO405.CheckPoint1 = PRO405.CheckPoint1;
                        objPRO405.CheckPoint2 = PRO405.CheckPoint2;
                        objPRO405.CheckPoint3 = PRO405.CheckPoint3;
                        objPRO405.CheckPoint5 = PRO405.CheckPoint5;
                        objPRO405.CheckPoint5_2 = PRO405.CheckPoint5_2;
                        objPRO405.QCRemarks = PRO405.QCRemarks;
                        objPRO405.Result = PRO405.Result;



                        objPRO405.EditedBy = objClsLoginInfo.UserName;
                        objPRO405.EditedOn = DateTime.Now;



                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO405.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO405 objPRO405 = db.PRO405.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO405 != null && string.IsNullOrWhiteSpace(objPRO405.ProtocolNo))
                    {
                        db.PRO405.Remove(objPRO405);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO405.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO406> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO406> lstAddPRO406 = new List<PRO406>();
                List<PRO406> lstDeletePRO406 = new List<PRO406>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO406 obj = db.PRO406.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO406();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }



                        obj.Orientation = item.Orientation;
                        obj.ActTolReqElevationOfNubFrom = item.ActTolReqElevationOfNubFrom;
                        obj.ActDimentionOfNubId = item.ActDimentionOfNubId;
                        obj.ElevationOfNubBeforeOvarlay = item.ElevationOfNubBeforeOvarlay;
                        obj.DimensionOfNubIdBeforeOvarlay = item.DimensionOfNubIdBeforeOvarlay;
                        obj.OvarlayThicknessNubTopSide = item.OvarlayThicknessNubTopSide;
                        obj.OvarlayThicknessNubIdSide = item.OvarlayThicknessNubIdSide;



                        if (isAdded)
                        {
                            lstAddPRO406.Add(obj);
                        }
                    }
                    if (lstAddPRO406.Count > 0)
                    {
                        db.PRO406.AddRange(lstAddPRO406);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO406 = db.PRO406.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO406.Count > 0)
                    {
                        db.PRO406.RemoveRange(lstDeletePRO406);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO406 = db.PRO406.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO406.Count > 0)
                    {
                        db.PRO406.RemoveRange(lstDeletePRO406);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2(List<PRO407> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO407> lstAddPRO407 = new List<PRO407>();
                List<PRO407> lstDeletePRO407 = new List<PRO407>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO407 obj = db.PRO407.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO407();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }


                        if (!string.IsNullOrWhiteSpace(item.OrientationReqNubID))
                        {


                            obj.OrientationReqNubID = item.OrientationReqNubID;
                            obj.ActualNubID = item.ActualNubID;
                            if (isAdded)
                            {
                                lstAddPRO407.Add(obj);
                            }
                        }


                    }
                    if (lstAddPRO407.Count > 0)
                    {
                        db.PRO407.AddRange(lstAddPRO407);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO407 = db.PRO407.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO407.Count > 0)
                    {
                        db.PRO407.RemoveRange(lstDeletePRO407);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO407 = db.PRO407.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO407.Count > 0)
                    {
                        db.PRO407.RemoveRange(lstDeletePRO407);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion



        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3(List<PRO408> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO408> lstAddPRO408 = new List<PRO408>();
                List<PRO408> lstDeletePRO408 = new List<PRO408>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO408 obj = db.PRO408.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO408();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.OtherDimensions = item.OtherDimensions;
                        obj.ReqOtherDimensions = item.ReqOtherDimensions;
                        obj.ActOtherDimensions = item.ActOtherDimensions;


                        if (isAdded)
                        {
                            lstAddPRO408.Add(obj);
                        }
                    }
                    if (lstAddPRO408.Count > 0)
                    {
                        db.PRO408.AddRange(lstAddPRO408);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO408 = db.PRO408.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO408.Count > 0)
                    {
                        db.PRO408.RemoveRange(lstDeletePRO408);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO408 = db.PRO408.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO408.Count > 0)
                    {
                        db.PRO408.RemoveRange(lstDeletePRO408);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

        #region  Linkage View Code   

        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL405 objPRL405 = new PRL405();
            objPRL405 = db.PRL405.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL405 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL405.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();
            List<string> lstElevationOfNubFrom = new List<string> { "TRIMMING LINE", "REF. LINE", "B.T.L", "WEP" };


            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.ElevationOfNubFrom = lstElevationOfNubFrom.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();


            ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL406> lstPRL406 = db.PRL406.Where(x => x.HeaderId == objPRL405.HeaderId).ToList();
            List<PRL407> lstPRL407 = db.PRL407.Where(x => x.HeaderId == objPRL405.HeaderId).ToList();

            List<PRL408> lstPRL408 = db.PRL408.Where(x => x.HeaderId == objPRL405.HeaderId).ToList();

            ViewBag.lstPRL406 = lstPRL406;
            ViewBag.lstPRL407 = lstPRL407;

            ViewBag.lstPRL408 = lstPRL408;


            #endregion

            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.DrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL405.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL405.ActFilledBy) && (objPRL405.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL405.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();

                            //lstdrawing.Add("Dummy Drawing Number");
                            //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL405);
        }

        [HttpPost]
        public ActionResult LinkageMobile(int? id, int? m, string APIRequestFrom = "", string UserName = "")
        {
            string ProjectDesc = "";
            string ManufacturingCode = "";
            string AccessRole = "";
            bool isEditable = false;
            var DrawingNo = new List<string>();
            bool isQCRemarkEditable = false;

            PRL405 objPRL405 = new PRL405();
            db.Configuration.ProxyCreationEnabled = false;
            objPRL405 = db.PRL405.Where(i => i.HeaderId == id).FirstOrDefault();
            db.Entry(objPRL405).State = System.Data.Entity.EntityState.Detached;

            NDEModels objNDEModels = new NDEModels();
            if (objPRL405 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL405.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
            AccessRole = objUserRoleAccessDetails.UserDesignation;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();
            List<string> lstElevationOfNubFrom = new List<string> { "TRIMMING LINE", "REF. LINE", "B.T.L", "WEP" };


            var YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var ElevationOfNubFrom = lstElevationOfNubFrom.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            var LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL406> lstPRL406 = db.PRL406.Where(x => x.HeaderId == objPRL405.HeaderId).ToList();
            List<PRL407> lstPRL407 = db.PRL407.Where(x => x.HeaderId == objPRL405.HeaderId).ToList();
            List<PRL408> lstPRL408 = db.PRL408.Where(x => x.HeaderId == objPRL405.HeaderId).ToList();

            #endregion

            try
            {
                isEditable = false;
                isQCRemarkEditable = false;
                if (m == 1)
                {
                    isEditable = true;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL405.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL405.ActFilledBy) && (objPRL405.RequestNo.HasValue))
                        {
                            isEditable = false;
                            isQCRemarkEditable = true;
                        }
                        else
                        {
                            DrawingNo = (new GeneralController()).GetPLMDrawingNumber(objPRL405.Project).ToList();
                        }
                    }
                }

                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    ProjectDesc = ProjectDesc,
                    ManufacturingCode = ManufacturingCode,
                    isEditable = isEditable,
                    isQCRemarkEditable = isQCRemarkEditable,
                    DrawingNo = DrawingNo,
                    AccessRole = AccessRole,

                    YesNoEnum = YesNoEnum,
                    YesNAEnum = YesNAEnum,
                    ElevationOfNubFrom = ElevationOfNubFrom,
                    OutsideInside = OutsideInside,
                    LeftRight = LeftRight,

                    objPRL405 = objPRL405,

                    lstPRL406 = lstPRL406,
                    lstPRL407 = lstPRL407,
                    lstPRL408 = lstPRL408,

                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                return Json(new
                {
                    res = "0",
                    msg = ex.Message,
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL405 PRL405, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = PRL405.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL405 objPRL405 = db.PRL405.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL405.DrawingRevisionNo = PRL405.DrawingRevisionNo;
                    objPRL405.DrawingNo = PRL405.DrawingNo;
                    objPRL405.DCRNo = PRL405.DCRNo;
                    objPRL405.ProtocolNo = PRL405.ProtocolNo;



                    objPRL405.ElevationOfNubFrom = PRL405.ElevationOfNubFrom;
                    objPRL405.ReqElevationOfNubFrom = PRL405.ReqElevationOfNubFrom;
                    objPRL405.ReqDimensionOfNubId = PRL405.ReqDimensionOfNubId;
                    objPRL405.ReqNubTopSide = PRL405.ReqNubTopSide;
                    objPRL405.ReqNubIdSide = PRL405.ReqNubIdSide;
                    objPRL405.TolReqElevationOfNubFrom = PRL405.TolReqElevationOfNubFrom;
                    objPRL405.TolReqDimensionOfNubId = PRL405.TolReqDimensionOfNubId;
                    objPRL405.NubID = PRL405.NubID;
                    objPRL405.ReqNubID = PRL405.ReqNubID;
                    objPRL405.OutByNubID = PRL405.OutByNubID;
                    objPRL405.CheckPoint1 = PRL405.CheckPoint1;
                    objPRL405.CheckPoint2 = PRL405.CheckPoint2;
                    objPRL405.CheckPoint3 = PRL405.CheckPoint3;
                    objPRL405.CheckPoint5 = PRL405.CheckPoint5;
                    objPRL405.CheckPoint5_2 = PRL405.CheckPoint5_2;
                    objPRL405.QCRemarks = PRL405.QCRemarks;
                    objPRL405.Result = PRL405.Result;



                    objPRL405.EditedBy = UserName;
                    objPRL405.EditedOn = DateTime.Now;

                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL405.ActFilledBy = UserName;
                            objPRL405.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL405.ReqFilledBy = UserName;
                            objPRL405.ReqFilledOn = DateTime.Now;
                        }
                    }

                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL405.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL405 objPRL405 = db.PRL405.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL405 != null && string.IsNullOrWhiteSpace(objPRL405.ProtocolNo))
                    {
                        db.PRL405.Remove(objPRL405);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL405.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL406> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL406> lstAddPRL406 = new List<PRL406>();
                List<PRL406> lstDeletePRL406 = new List<PRL406>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL406 obj = db.PRL406.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL406();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Orientation = item.Orientation;
                        obj.ActTolReqElevationOfNubFrom = item.ActTolReqElevationOfNubFrom;
                        obj.ActDimentionOfNubId = item.ActDimentionOfNubId;
                        obj.ElevationOfNubBeforeOvarlay = item.ElevationOfNubBeforeOvarlay;
                        obj.DimensionOfNubIdBeforeOvarlay = item.DimensionOfNubIdBeforeOvarlay;
                        obj.OvarlayThicknessNubTopSide = item.OvarlayThicknessNubTopSide;
                        obj.OvarlayThicknessNubIdSide = item.OvarlayThicknessNubIdSide;

                        if (isAdded)
                        {
                            lstAddPRL406.Add(obj);
                        }
                    }
                }
                if (lstAddPRL406.Count > 0)
                {
                    db.PRL406.AddRange(lstAddPRL406);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL406 = db.PRL406.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL406 = db.PRL406.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL406.Count > 0)
                {
                    db.PRL406.RemoveRange(lstDeletePRL406);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2Linkage(List<PRL407> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL407> lstAddPRL407 = new List<PRL407>();
                List<PRL407> lstDeletePRL407 = new List<PRL407>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL407 obj = db.PRL407.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL407();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        if (!string.IsNullOrWhiteSpace(item.OrientationReqNubID))
                        {

                            obj.OrientationReqNubID = item.OrientationReqNubID;
                            obj.ActualNubID = item.ActualNubID;

                            if (isAdded)
                            {
                                lstAddPRL407.Add(obj);
                            }
                        }
                    }
                }
                if (lstAddPRL407.Count > 0)
                {
                    db.PRL407.AddRange(lstAddPRL407);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL407 = db.PRL407.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL407 = db.PRL407.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL407.Count > 0)
                {
                    db.PRL407.RemoveRange(lstDeletePRL407);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion




        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3Linkage(List<PRL408> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL408> lstAddPRL408 = new List<PRL408>();
                List<PRL408> lstDeletePRL408 = new List<PRL408>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL408 obj = db.PRL408.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL408();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.OtherDimensions = item.OtherDimensions;
                        obj.ReqOtherDimensions = item.ReqOtherDimensions;
                        obj.ActOtherDimensions = item.ActOtherDimensions;


                        if (isAdded)
                        {
                            lstAddPRL408.Add(obj);
                        }
                    }
                }
                if (lstAddPRL408.Count > 0)
                {
                    db.PRL408.AddRange(lstAddPRL408);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL408 = db.PRL408.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL408 = db.PRL408.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL408.Count > 0)
                {
                    db.PRL408.RemoveRange(lstDeletePRL408);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Data Copy
        [HttpGet]
        public JsonResult CopyData(string strSeamNo, int HeaderId)
        {
            PRL400 _obj400 = db.PRL400.Where(w => w.SeamNo == strSeamNo).FirstOrDefault();
            if (_obj400 != null)
            {
                List<PRL401> _lstPRL401 = db.PRL401.Where(w => w.HeaderId == _obj400.HeaderId).ToList();
                List<clPRL406> _lstPRL406 = new List<clPRL406>();
                if (_lstPRL401.Count() > 0)
                {
                    for (int i = 0; i < _lstPRL401.Count; i++)
                    {
                        PRL406 objPRL406Copy = new PRL406();
                        objPRL406Copy.HeaderId = HeaderId;
                        objPRL406Copy.Orientation = _lstPRL401[i].Orientation;
                        objPRL406Copy.ActTolReqElevationOfNubFrom = "";
                        objPRL406Copy.ActDimentionOfNubId = "";
                        objPRL406Copy.ElevationOfNubBeforeOvarlay = _lstPRL401[i].ActADimension;
                        objPRL406Copy.DimensionOfNubIdBeforeOvarlay = _lstPRL401[i].ActBDimension;
                        objPRL406Copy.OvarlayThicknessNubTopSide = "";
                        objPRL406Copy.OvarlayThicknessNubIdSide = "";
                        objPRL406Copy.CreatedBy = objClsLoginInfo.UserName;
                        objPRL406Copy.CreatedOn = DateTime.Now;

                        db.PRL406.Add(objPRL406Copy);
                        clPRL406 _obj = new clPRL406();
                        _obj.HeaderId = HeaderId;
                        _obj.Orientation = _lstPRL401[i].Orientation;
                        _obj.ActTolReqElevationOfNubFrom = "";
                        _obj.ActDimentionOfNubId = "";
                        _obj.ElevationOfNubBeforeOvarlay = _lstPRL401[i].ActADimension;
                        _obj.DimensionOfNubIdBeforeOvarlay = _lstPRL401[i].ActBDimension;
                        _obj.OvarlayThicknessNubTopSide = "";
                        _obj.OvarlayThicknessNubIdSide = "";
                        _lstPRL406.Add(_obj);
                    }
                    db.SaveChanges();
                }

                PRL405 _obj405 = db.PRL405.Where(w => w.HeaderId == HeaderId).FirstOrDefault();
                if (_obj405 != null)
                {
                    _obj405.IsDataCopy = "true";
                    db.SaveChanges();
                }
                return Json(_lstPRL406, JsonRequestBehavior.AllowGet);
            }
            else
                return Json("", JsonRequestBehavior.AllowGet);
        }
        #endregion


        [HttpPost]
        public JsonResult ActCopyData(string strSeamNo, int HeaderId)
        {
            string strReturn = "";
            try
            {
                PRL400 _obj400 = db.PRL400.Where(w => w.SeamNo == strSeamNo).FirstOrDefault();
                if (_obj400 != null)
                {
                    List<PRL401> _lstPRL401 = db.PRL401.Where(w => w.HeaderId == _obj400.HeaderId).ToList();
                    List<PRL406> _lstPRL406 = db.PRL406.Where(w => w.HeaderId == HeaderId).ToList();
                    if (_lstPRL401.Count() > 0)
                    {
                        for (int i = 0; i < _lstPRL401.Count; i++)
                        {
                            if (_lstPRL406.Count >= i)
                            {
                                PRL406 _obj = _lstPRL406[i];
                                _obj.ElevationOfNubBeforeOvarlay = _lstPRL401[i].ActADimension;
                                _obj.DimensionOfNubIdBeforeOvarlay = _lstPRL401[i].ActBDimension;
                                if (strReturn == "")
                                    strReturn = _lstPRL401[i].ActADimension + "," + _lstPRL401[i].ActBDimension;
                                else
                                    strReturn += "|" + _lstPRL401[i].ActADimension + "," + _lstPRL401[i].ActBDimension;
                                db.SaveChanges();
                            }
                        }
                        PRL405 _obj405 = db.PRL405.Where(w => w.HeaderId == HeaderId).FirstOrDefault();
                        if (_obj405 != null)
                        {
                            _obj405.IsActDataCopy = "true";
                            db.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                strReturn = "Error";
            }
            return Json(strReturn, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }

    public class clPRL406
    {
        public int LineId { get; set; }
        public Nullable<int> HeaderId { get; set; }
        public string Orientation { get; set; }
        public string ActTolReqElevationOfNubFrom { get; set; }
        public string ActDimentionOfNubId { get; set; }
        public string ElevationOfNubBeforeOvarlay { get; set; }
        public string DimensionOfNubIdBeforeOvarlay { get; set; }
        public string OvarlayThicknessNubTopSide { get; set; }
        public string OvarlayThicknessNubIdSide { get; set; }
    }
}