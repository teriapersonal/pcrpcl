﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQS.Areas.NDE.Models;
using IEMQS.Models;
using IEMQSImplementation;
using static IEMQSImplementation.clsImplementationMessage;
using static IEMQSImplementation.clsBase;
using IEMQS.Areas.Utility.Controllers;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol039Controller : clsBase
    {
        // GET: PROTOCOL/Protocol039
        string ControllerURL = "/PROTOCOL/Protocol039/";
        string Title = "SET-UP INSPECTION REPORT FOR FORMED HEMISPHERICAL D'END";
        //IEMQS.DrawingReference.hedProjectDocListWebServiceClient drawref = new IEMQS.DrawingReference.hedProjectDocListWebServiceClient();

        #region Details View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO190 objPRO190 = new PRO190();
            if (!id.HasValue)
            {
                try
                {
                    objPRO190.ProtocolNo = string.Empty;
                    objPRO190.CreatedBy = objClsLoginInfo.UserName;
                    objPRO190.CreatedOn = DateTime.Now;

                    #region DIMENSIONS OF CROWN REMARK

                    objPRO190.OverCrowingRemark = "TOL: 1¼ (1.25) % OF NOMINAL I/S DIA.";

                    objPRO190.UnderCrowingRemark = "TOL:5/8 (0.625) % OF NOMINAL I/S DIA.";

                    #endregion

                    #region OVALITY 
                    objPRO190.PRO191.Add(new PRO191
                    {
                        Orientation = "0°-180° ",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO190.PRO191.Add(new PRO191
                    {
                        Orientation = "22.5°-202.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO190.PRO191.Add(new PRO191
                    {
                        Orientation = "45°-225°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO190.PRO191.Add(new PRO191
                    {
                        Orientation = "67.5°-247.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO190.PRO191.Add(new PRO191
                    {
                        Orientation = "90°-270°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO190.PRO191.Add(new PRO191
                    {
                        Orientation = "112.5°-292.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO190.PRO191.Add(new PRO191
                    {
                        Orientation = "135°-315°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO190.PRO191.Add(new PRO191
                    {
                        Orientation = "157.5°-337.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion



                    #region THICKNESS 
                    objPRO190.PRO192.Add(new PRO192
                    {
                        ThiknessColumn1 = "PETAL 1 ",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO190.PRO192.Add(new PRO192
                    {
                        ThiknessColumn1 = "PETAL 2",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO190.PRO192.Add(new PRO192
                    {
                        ThiknessColumn1 = "PETAL 3",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO190.PRO192.Add(new PRO192
                    {
                        ThiknessColumn1 = "PETAL 4",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion

                    #region DESCRIPTION 
                    objPRO190.PRO194_5.Add(new PRO194_5
                    {
                        Description = "PETAL 1 ",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO190.PRO194_5.Add(new PRO194_5
                    {
                        Description = "PETAL 2",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO190.PRO194_5.Add(new PRO194_5
                    {
                        Description = "PETAL 3",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO190.PRO194_5.Add(new PRO194_5
                    {
                        Description = "PETAL 4",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });

                    #endregion

                    db.PRO190.Add(objPRO190);
                    db.SaveChanges();
                    // return View();

                }

                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO190.HeaderId;
            }
            else
            {
                objPRO190 = db.PRO190.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lsttypesofforming = new List<string> { "HOT", "COLD", "WARM" };

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.TypesOfForming = lsttypesofforming.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();


            //  ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO191> lstPRO191 = db.PRO191.Where(x => x.HeaderId == objPRO190.HeaderId).ToList();
            List<PRO192> lstPRO192 = db.PRO192.Where(x => x.HeaderId == objPRO190.HeaderId).ToList();
            List<PRO193> lstPRO193 = db.PRO193.Where(x => x.HeaderId == objPRO190.HeaderId).ToList();
            List<PRO194> lstPRO194 = db.PRO194.Where(x => x.HeaderId == objPRO190.HeaderId).ToList();
            List<PRO194_2> lstPRO194_2 = db.PRO194_2.Where(x => x.HeaderId == objPRO190.HeaderId).ToList();
            List<PRO194_3> lstPRO194_3 = db.PRO194_3.Where(x => x.HeaderId == objPRO190.HeaderId).ToList();
            List<PRO194_4> lstPRO194_4 = db.PRO194_4.Where(x => x.HeaderId == objPRO190.HeaderId).ToList();
            List<PRO194_5> lstPRO194_5 = db.PRO194_5.Where(x => x.HeaderId == objPRO190.HeaderId).ToList();
            List<PRO194_6> lstPRO194_6 = db.PRO194_6.Where(x => x.HeaderId == objPRO190.HeaderId).ToList();
            List<SP_IPI_PROTOCOL039_GET_LINES8_DATA_Result> lstPRO194_7 = db.SP_IPI_PROTOCOL039_GET_LINES8_DATA(1, int.MaxValue, "", " HeaderId = " + objPRO190.HeaderId).ToList();
            List<SP_IPI_PROTOCOL039_GET_LINES9_DATA_Result> lstPRO194_8 = db.SP_IPI_PROTOCOL039_GET_LINES9_DATA(1, int.MaxValue, "", " HeaderId = " + objPRO190.HeaderId).ToList();
            List<PRO194_9> lstPRO194_9 = db.PRO194_9.Where(x => x.HeaderId == objPRO190.HeaderId).ToList();
            //List<PRO194_7> lstPRO194_7 = db.PRO194_7.Where(x => x.HeaderId == objPRO190.HeaderId).OrderBy(x => new { x.SeamNo, x.LineId }).ToList();
            //List<PRO194_8> lstPRO194_8 = db.PRO194_8.Where(x => x.HeaderId == objPRO190.HeaderId).OrderBy(x => new { x.SeamNo, x.LineId }).ToList();

            ViewBag.lstPRO191 = lstPRO191;
            ViewBag.lstPRO192 = lstPRO192;
            ViewBag.lstPRO193 = lstPRO193;
            ViewBag.lstPRO194 = lstPRO194;
            ViewBag.lstPRO194_2 = lstPRO194_2;
            ViewBag.lstPRO194_3 = lstPRO194_3;
            ViewBag.lstPRO194_4 = lstPRO194_4;
            ViewBag.lstPRO194_5 = lstPRO194_5;
            ViewBag.lstPRO194_6 = lstPRO194_6;
            ViewBag.lstPRO194_7 = lstPRO194_7;
            ViewBag.lstPRO194_8 = lstPRO194_8;
            ViewBag.lstPRO194_9 = lstPRO194_9;

            #endregion
            return View(objPRO190);

        }

        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO190 PRO190)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO190.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO190 objPRO190 = db.PRO190.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO190.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO190.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data

                        objPRO190.ProtocolNo = PRO190.ProtocolNo;
                        objPRO190.ReqInsideCircumferenceAtTrimmingLine = PRO190.ReqInsideCircumferenceAtTrimmingLine;
                        objPRO190.ActInsideCircumferenceAtTrimmingLine = PRO190.ActInsideCircumferenceAtTrimmingLine;
                        objPRO190.InsideCircumferenceAtTrimmingLineRemarks = PRO190.InsideCircumferenceAtTrimmingLineRemarks;
                        objPRO190.ReqTotalHeight = PRO190.ReqTotalHeight;
                        objPRO190.ActTotalHeigh = PRO190.ActTotalHeigh;
                        objPRO190.TotalHeightRemarks = PRO190.TotalHeightRemarks;
                        objPRO190.ReqOverCrowing = PRO190.ReqOverCrowing;
                        objPRO190.ActOverCrowing = PRO190.ActOverCrowing;
                        objPRO190.OverCrowingRemark = PRO190.OverCrowingRemark;
                        objPRO190.ReqUnderCrowing = PRO190.ReqUnderCrowing;
                        objPRO190.ActUnderCrowing = PRO190.ActUnderCrowing;
                        objPRO190.UnderCrowingRemark = PRO190.UnderCrowingRemark;
                        objPRO190.ReqConcentricity = PRO190.ReqConcentricity;
                        objPRO190.ActConcentricity = PRO190.ActConcentricity;
                        objPRO190.ConcentricityRemark = PRO190.ConcentricityRemark;
                        objPRO190.ReqTypeOfForming = PRO190.ReqTypeOfForming;
                        objPRO190.ActTypeOfForming = PRO190.ActTypeOfForming;
                        objPRO190.TypeOfFormingRemark = PRO190.TypeOfFormingRemark;
                        objPRO190.DimensionsOfCrown = PRO190.DimensionsOfCrown;
                        objPRO190.ReqDimensionsOfCrown = PRO190.ReqDimensionsOfCrown;
                        objPRO190.ActDimensionsOfCrownMin = PRO190.ActDimensionsOfCrownMin;
                        objPRO190.ActDimensionsOfCrownMax = PRO190.ActDimensionsOfCrownMax;
                        objPRO190.DimensionsOfCrownRemark = PRO190.DimensionsOfCrownRemark;
                        objPRO190.ReqThicknessOfCrown = PRO190.ReqThicknessOfCrown;
                        objPRO190.ActThicknessOfCrown = PRO190.ActThicknessOfCrown;
                        objPRO190.ThicknessOfCrownRemarks = PRO190.ThicknessOfCrownRemarks;
                        objPRO190.Ovality = PRO190.Ovality;
                        objPRO190.OvalityOutBy = PRO190.OvalityOutBy;
                        objPRO190.ReqThicknessMinimum = PRO190.ReqThicknessMinimum;
                        objPRO190.ReqThicknessNom = PRO190.ReqThicknessNom;
                        objPRO190.EValueTemplateInspection = PRO190.EValueTemplateInspection;
                        objPRO190.ReqChordLength = PRO190.ReqChordLength;
                        objPRO190.ActChordLength = PRO190.ActChordLength;
                        objPRO190.ReqRadius = PRO190.ReqRadius;
                        objPRO190.ActRadius = PRO190.ActRadius;
                        objPRO190.GapAllowable = PRO190.GapAllowable;
                        objPRO190.ActGap = PRO190.ActGap;
                        objPRO190.OrientationOfCrownsLongSeams = PRO190.OrientationOfCrownsLongSeams;
                        objPRO190.WepDimensionsForCrownToPTJoint = PRO190.WepDimensionsForCrownToPTJoint;
                        objPRO190.PTRootGapAllowed = PRO190.PTRootGapAllowed;
                        objPRO190.ActPTRootGapMinimum = PRO190.ActPTRootGapMinimum;
                        objPRO190.ActPTRootGapMaximum = PRO190.ActPTRootGapMaximum;
                        objPRO190.PTOffsetAllowed = PRO190.PTOffsetAllowed;
                        objPRO190.ActPTOffsetActMinimum = PRO190.ActPTOffsetActMinimum;
                        objPRO190.ActPTOffsetMaximum = PRO190.ActPTOffsetMaximum;
                        objPRO190.PTPickInOutAllowed = PRO190.PTPickInOutAllowed;
                        objPRO190.ActPTPickInOutMinimum = PRO190.ActPTPickInOutMinimum;
                        objPRO190.ActPTPickInOutMaximum = PRO190.ActPTPickInOutMaximum;
                        objPRO190.PTISWepAngleRequired = PRO190.PTISWepAngleRequired;
                        objPRO190.ActPTISIWepAngleMinimum = PRO190.ActPTISIWepAngleMinimum;
                        objPRO190.ActPTISWepAngleMaximum = PRO190.ActPTISWepAngleMaximum;
                        objPRO190.PTOSWepAngleRequired = PRO190.PTOSWepAngleRequired;
                        objPRO190.ActPTOSIWepAngleMinimum = PRO190.ActPTOSIWepAngleMinimum;
                        objPRO190.ActPTOSWepAngleMaximum = PRO190.ActPTOSWepAngleMaximum;
                        objPRO190.ReqPTRootFace = PRO190.ReqPTRootFace;
                        objPRO190.ActPTRootFaceMinimum = PRO190.ActPTRootFaceMinimum;
                        objPRO190.ActPTRootFaceMaximum = PRO190.ActPTRootFaceMaximum;
                        objPRO190.WepDimensionsForCircmferantionSeamOfTierToTireJoint = PRO190.WepDimensionsForCircmferantionSeamOfTierToTireJoint;
                        //objPRO190.TTSeamNo = PRO190.TTSeamNo;
                        //objPRO190.TTRootGapAllowed = PRO190.TTRootGapAllowed;
                        //objPRO190.TTRootGapActualMinimum = PRO190.TTRootGapActualMinimum;
                        //objPRO190.TTRootGapActualMaximum = PRO190.TTRootGapActualMaximum;
                        //objPRO190.TTOffsetAllowed = PRO190.TTOffsetAllowed;
                        //objPRO190.TTOffsetActualMinimum = PRO190.TTOffsetActualMinimum;
                        //objPRO190.TTOffsetActualMaximum = PRO190.TTOffsetActualMaximum;
                        //objPRO190.TTPickInOutAllowed = PRO190.TTPickInOutAllowed;
                        //objPRO190.TTPickInOutActualMinimum = PRO190.TTPickInOutActualMinimum;
                        //objPRO190.TTPickInOutActualMaximum = PRO190.TTPickInOutActualMaximum;
                        //objPRO190.TTISWepAngleRequired = PRO190.TTISWepAngleRequired;
                        //objPRO190.TTISWepAngleActualMinimum = PRO190.TTISWepAngleActualMinimum;
                        //objPRO190.TTISWepAngleActualMaximum = PRO190.TTISWepAngleActualMaximum;
                        //objPRO190.TTOSWepAngleRequired = PRO190.TTOSWepAngleRequired;
                        //objPRO190.TTOSWepAngleActualMinimum = PRO190.TTOSWepAngleActualMinimum;
                        //objPRO190.TTOSWepAngleActualMaximum = PRO190.TTOSWepAngleActualMaximum;
                        //objPRO190.TTRootFaceRequired = PRO190.TTRootFaceRequired;
                        //objPRO190.TTRootFaceActualMinimum = PRO190.TTRootFaceActualMinimum;
                        //objPRO190.TTRootFaceActualMaximum = PRO190.TTRootFaceActualMaximum;
                        objPRO190.PPRootGap = PRO190.PPRootGap;
                        objPRO190.PPRootFace = PRO190.PPRootFace;
                        objPRO190.PPPeakInOut = PRO190.PPPeakInOut;
                        objPRO190.Offset = PRO190.Offset;
                        objPRO190.InsideWepAngle = PRO190.InsideWepAngle;
                        objPRO190.OutsideWepAngle = PRO190.OutsideWepAngle;
                        objPRO190.HeightOfTier = PRO190.HeightOfTier;
                        objPRO190.HOTTierNo1 = PRO190.HOTTierNo1;
                        objPRO190.HOTRequired1 = PRO190.HOTRequired1;
                        objPRO190.HOTActual1 = PRO190.HOTActual1;
                        objPRO190.HOTTierNo2 = PRO190.HOTTierNo2;
                        objPRO190.HOTRequired2 = PRO190.HOTRequired2;
                        objPRO190.HOTActual2 = PRO190.HOTActual2;
                        objPRO190.HOTTierNo3 = PRO190.HOTTierNo3;
                        objPRO190.HOTRequired3 = PRO190.HOTRequired3;
                        objPRO190.HOTActual3 = PRO190.HOTActual3;
                        objPRO190.DetailThicknessReport = PRO190.DetailThicknessReport;
                        objPRO190.CrownThicknessReadings = PRO190.CrownThicknessReadings;
                        objPRO190.CheckPoint1 = PRO190.CheckPoint1;
                        objPRO190.CheckPoint2 = PRO190.CheckPoint2;
                        objPRO190.CheckPoint3 = PRO190.CheckPoint3;
                        objPRO190.CheckPoint4 = PRO190.CheckPoint4;
                        objPRO190.CheckPoint5 = PRO190.CheckPoint5;
                        objPRO190.CheckPoint6 = PRO190.CheckPoint6;
                        objPRO190.CheckPoint6_2 = PRO190.CheckPoint6_2;
                        objPRO190.CheckPoint6_3 = PRO190.CheckPoint6_3;
                        objPRO190.CheckPoint7 = PRO190.CheckPoint7;
                        objPRO190.CheckPoint7_2 = PRO190.CheckPoint7_2;
                        objPRO190.CheckPoint7_3 = PRO190.CheckPoint7_3;
                        objPRO190.CheckPoint7_4 = PRO190.CheckPoint7_4;
                        objPRO190.CheckPoint9 = PRO190.CheckPoint9;
                        objPRO190.CheckPoint10 = PRO190.CheckPoint10;
                        objPRO190.CheckPoint11 = PRO190.CheckPoint11;
                        objPRO190.QCRemarks = PRO190.QCRemarks;
                        objPRO190.Result = PRO190.Result;
                        objPRO190.ReqCladStripingWidth = PRO190.ReqCladStripingWidth;
                        objPRO190.ReqCladStripingDepth = PRO190.ReqCladStripingDepth;
                        objPRO190.ReqCladStripingOffset = PRO190.ReqCladStripingOffset;
                        objPRO190.Note1 = PRO190.Note1;
                        objPRO190.Note2 = PRO190.Note2;
                        objPRO190.Note3 = PRO190.Note3;
                        objPRO190.IdentificationDetailsOfCleats = PRO190.IdentificationDetailsOfCleats;
                        objPRO190.EditedBy = objClsLoginInfo.UserName;
                        objPRO190.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO190.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO190 objPRO190 = db.PRO190.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO190 != null && string.IsNullOrWhiteSpace(objPRO190.ProtocolNo))
                    {
                        db.PRO190.Remove(objPRO190);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO190.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO191> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO191> lstAddPRO191 = new List<PRO191>();
                List<PRO191> lstDeletePRO191 = new List<PRO191>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO191 obj = db.PRO191.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.Orientation = item.Orientation;
                                obj.Actual = item.Actual;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO191 obj = new PRO191();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.Orientation))
                            {
                                obj.Orientation = item.Orientation;
                                obj.Actual = item.Actual;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO191.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO191.Count > 0)
                    {
                        db.PRO191.AddRange(lstAddPRO191);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO191 = db.PRO191.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO191.Count > 0)
                    {
                        db.PRO191.RemoveRange(lstDeletePRO191);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO191 = db.PRO191.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO191.Count > 0)
                    {
                        db.PRO191.RemoveRange(lstDeletePRO191);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2(List<PRO192> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO192> lstAddPRO192 = new List<PRO192>();
                List<PRO192> lstDeletePRO192 = new List<PRO192>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO192 obj = db.PRO192.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.ThiknessColumn1 = item.ThiknessColumn1;
                                obj.ThiknessColumn2 = item.ThiknessColumn2;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO192 obj = new PRO192();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.ThiknessColumn1))
                            {
                                obj.ThiknessColumn1 = item.ThiknessColumn1;
                                obj.ThiknessColumn2 = item.ThiknessColumn2;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO192.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO192.Count > 0)
                    {
                        db.PRO192.AddRange(lstAddPRO192);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO192 = db.PRO192.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO192.Count > 0)
                    {
                        db.PRO192.RemoveRange(lstDeletePRO192);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO192 = db.PRO192.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO192.Count > 0)
                    {
                        db.PRO192.RemoveRange(lstDeletePRO192);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3(List<PRO193> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO193> lstAddPRO193 = new List<PRO193>();
                List<PRO193> lstDeletePRO193 = new List<PRO193>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO193 obj = db.PRO193.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.SeamNo = item.SeamNo;
                                obj.ReqOrien = item.ReqOrien;
                                obj.ReqArcLength = item.ReqArcLength;
                                obj.ActualArcLength = item.ActualArcLength;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO193 obj = new PRO193();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.SeamNo))
                            {
                                obj.SeamNo = item.SeamNo;
                                obj.ReqOrien = item.ReqOrien;
                                obj.ReqArcLength = item.ReqArcLength;
                                obj.ActualArcLength = item.ActualArcLength;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO193.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO193.Count > 0)
                    {
                        db.PRO193.AddRange(lstAddPRO193);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO193 = db.PRO193.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO193.Count > 0)
                    {
                        db.PRO193.RemoveRange(lstDeletePRO193);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO193 = db.PRO193.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO193.Count > 0)
                    {
                        db.PRO193.RemoveRange(lstDeletePRO193);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line4 details
        [HttpPost]
        public JsonResult SaveProtocolLine4(List<PRO194> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO194> lstAddPRO194 = new List<PRO194>();
                List<PRO194> lstDeletePRO194 = new List<PRO194>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO194 obj = db.PRO194.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.SeamNo = item.SeamNo;
                                obj.ReqOrientationAtCrownCenter = item.ReqOrientationAtCrownCenter;
                                obj.ActOrientationAtCrownCenter = item.ActOrientationAtCrownCenter;
                                obj.ReqArcLengthAtCL = item.ReqArcLengthAtCL;
                                obj.ActArcLengthAtCL = item.ActArcLengthAtCL;
                                obj.ReqOffsetDistFromSeam = item.ReqOffsetDistFromSeam;
                                obj.ActOffsetDistFromSeam = item.ActOffsetDistFromSeam;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO194 obj = new PRO194();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.SeamNo))
                            {
                                obj.SeamNo = item.SeamNo;
                                obj.ReqOrientationAtCrownCenter = item.ReqOrientationAtCrownCenter;
                                obj.ActOrientationAtCrownCenter = item.ActOrientationAtCrownCenter;
                                obj.ReqArcLengthAtCL = item.ReqArcLengthAtCL;
                                obj.ActArcLengthAtCL = item.ActArcLengthAtCL;
                                obj.ReqOffsetDistFromSeam = item.ReqOffsetDistFromSeam;
                                obj.ActOffsetDistFromSeam = item.ActOffsetDistFromSeam;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO194.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO194.Count > 0)
                    {
                        db.PRO194.AddRange(lstAddPRO194);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO194 = db.PRO194.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO194.Count > 0)
                    {
                        db.PRO194.RemoveRange(lstDeletePRO194);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO194 = db.PRO194.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO194.Count > 0)
                    {
                        db.PRO194.RemoveRange(lstDeletePRO194);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line5 details
        [HttpPost]
        public JsonResult SaveProtocolLine5(List<PRO194_2> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO194_2> lstAddPRO194_2 = new List<PRO194_2>();
                List<PRO194_2> lstDeletePRO194_2 = new List<PRO194_2>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO194_2 obj = db.PRO194_2.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.SeamNo = item.SeamNo;
                                obj.RootGapMin = item.RootGapMin;
                                obj.RootGapMax = item.RootGapMax;
                                obj.RootFaceMin = item.RootFaceMin;
                                obj.RootFaceMax = item.RootFaceMax;
                                obj.Peak_in_out_Top = item.Peak_in_out_Top;
                                obj.Peak_in_out_Mid = item.Peak_in_out_Mid;
                                obj.Peak_in_out_Bottom = item.Peak_in_out_Bottom;
                                obj.OffSetMin = item.OffSetMin;
                                obj.OffSetMax = item.OffSetMax;
                                obj.Inside_Wep_AngleMin = item.Inside_Wep_AngleMin;
                                obj.Inside_Wep_AngleMax = item.Inside_Wep_AngleMax;
                                obj.Outside_Wep_AngleMin = item.Outside_Wep_AngleMin;
                                obj.Outside_Wep_AngleMax = item.Outside_Wep_AngleMax;

                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO194_2 obj = new PRO194_2();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.SeamNo))
                            {
                                obj.SeamNo = item.SeamNo;
                                obj.RootGapMin = item.RootGapMin;
                                obj.RootGapMax = item.RootGapMax;
                                obj.RootFaceMin = item.RootFaceMin;
                                obj.RootGapMax = item.RootGapMax;
                                obj.Peak_in_out_Top = item.Peak_in_out_Top;
                                obj.Peak_in_out_Mid = item.Peak_in_out_Mid;
                                obj.Peak_in_out_Bottom = item.Peak_in_out_Bottom;

                                obj.OffSetMin = item.OffSetMin;
                                obj.OffSetMax = item.OffSetMax;
                                obj.Inside_Wep_AngleMin = item.Inside_Wep_AngleMin;
                                obj.Inside_Wep_AngleMax = item.Inside_Wep_AngleMax;
                                obj.Outside_Wep_AngleMin = item.Outside_Wep_AngleMin;
                                obj.Outside_Wep_AngleMax = item.Outside_Wep_AngleMax;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO194_2.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO194_2.Count > 0)
                    {
                        db.PRO194_2.AddRange(lstAddPRO194_2);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO194_2 = db.PRO194_2.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO194_2.Count > 0)
                    {
                        db.PRO194_2.RemoveRange(lstDeletePRO194_2);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO194_2 = db.PRO194_2.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO194_2.Count > 0)
                    {
                        db.PRO194_2.RemoveRange(lstDeletePRO194_2);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        //#region Line6 details
        //[HttpPost]
        //public JsonResult SaveProtocolLine6(List<PRO194_3> lst, int HeaderId)
        //{
        //    clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
        //    try
        //    {
        //        List<PRO194_3> lstAddPRO194_3 = new List<PRO194_3>();
        //        List<PRO194_3> lstDeletePRO194_3 = new List<PRO194_3>();
        //        if (lst != null)
        //        {
        //            foreach (var item in lst)
        //            {
        //                if (item.LineId > 0)
        //                {
        //                    PRO194_3 obj = db.PRO194_3.Where(x => x.LineId == item.LineId).FirstOrDefault();
        //                    if (obj != null)
        //                    {
        //                        obj.SeamNo = item.SeamNo;
        //                        obj.OffSetMin = item.OffSetMin;
        //                        obj.OffSetMax = item.OffSetMax;
        //                        obj.Inside_Wep_AngleMin = item.Inside_Wep_AngleMin;
        //                        obj.Inside_Wep_AngleMax = item.Inside_Wep_AngleMax;
        //                        obj.Outside_Wep_AngleMin = item.Outside_Wep_AngleMin;
        //                        obj.Outside_Wep_AngleMax = item.Outside_Wep_AngleMax;
        //                        obj.EditedBy = objClsLoginInfo.UserName;
        //                        obj.EditedOn = DateTime.Now;
        //                    }
        //                }
        //                else
        //                {
        //                    PRO194_3 obj = new PRO194_3();
        //                    obj.HeaderId = item.HeaderId;
        //                    if (!string.IsNullOrWhiteSpace(item.SeamNo))
        //                    {
        //                        obj.SeamNo = item.SeamNo;
        //                        obj.OffSetMin = item.OffSetMin;
        //                        obj.OffSetMax = item.OffSetMax;
        //                        obj.Inside_Wep_AngleMin = item.Inside_Wep_AngleMin;
        //                        obj.Inside_Wep_AngleMax = item.Inside_Wep_AngleMax;
        //                        obj.Outside_Wep_AngleMin = item.Outside_Wep_AngleMin;
        //                        obj.Outside_Wep_AngleMax = item.Outside_Wep_AngleMax;
        //                        obj.EditedBy = objClsLoginInfo.UserName;
        //                        obj.EditedOn = DateTime.Now;
        //                        obj.CreatedBy = objClsLoginInfo.UserName;
        //                        obj.CreatedOn = DateTime.Now;
        //                        lstAddPRO194_3.Add(obj);
        //                    }
        //                }
        //            }
        //            if (lstAddPRO194_3.Count > 0)
        //            {
        //                db.PRO194_3.AddRange(lstAddPRO194_3);
        //            }

        //            var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
        //            var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

        //            lstDeletePRO194_3 = db.PRO194_3.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
        //            if (lstDeletePRO194_3.Count > 0)
        //            {
        //                db.PRO194_3.RemoveRange(lstDeletePRO194_3);
        //            }
        //            db.SaveChanges();
        //        }
        //        else
        //        {
        //            lstDeletePRO194_3 = db.PRO194_3.Where(x => x.HeaderId == HeaderId).ToList();
        //            if (lstDeletePRO194_3.Count > 0)
        //            {
        //                db.PRO194_3.RemoveRange(lstDeletePRO194_3);
        //            }
        //            db.SaveChanges();
        //        }
        //        objResponseMsg.Key = true;
        //    }
        //    catch (Exception ex)
        //    {
        //        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        //        objResponseMsg.Key = false;
        //        objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
        //    }
        //    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        //}
        //#endregion

        #region Line7 details
        [HttpPost]
        public JsonResult SaveProtocolLine7(List<PRO194_4> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO194_4> lstAddPRO194_4 = new List<PRO194_4>();
                List<PRO194_4> lstDeletePRO194_4 = new List<PRO194_4>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO194_4 obj = db.PRO194_4.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.MaterialIdentificationReport = item.MaterialIdentificationReport;

                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO194_4 obj = new PRO194_4();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.MaterialIdentificationReport))
                            {
                                obj.MaterialIdentificationReport = item.MaterialIdentificationReport;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO194_4.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO194_4.Count > 0)
                    {
                        db.PRO194_4.AddRange(lstAddPRO194_4);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO194_4 = db.PRO194_4.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO194_4.Count > 0)
                    {
                        db.PRO194_4.RemoveRange(lstDeletePRO194_4);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO194_4 = db.PRO194_4.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO194_4.Count > 0)
                    {
                        db.PRO194_4.RemoveRange(lstDeletePRO194_4);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line8 details
        [HttpPost]
        public JsonResult SaveProtocolLine8(List<PRO194_7> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO194_7> lstAddPRO194_7 = new List<PRO194_7>();
                List<PRO194_7> lstDeletePRO194_7 = new List<PRO194_7>();

                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO194_7 obj = db.PRO194_7.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        PRO194_7 objFecthSeam = new PRO194_7();
                        if (obj == null)
                        {
                            obj = new PRO194_7();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        objFecthSeam = db.PRO194_7.Where(x => x.SeamNo == item.SeamNo && x.HeaderId == item.HeaderId).FirstOrDefault();
                        if (objFecthSeam != null)
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                            //obj.Depth = objFecthSeam.Depth;
                            //obj.Width = objFecthSeam.Width;
                            //obj.Offset = objFecthSeam.Offset;
                            //obj.SpotNoClad = objFecthSeam.SpotNoClad;

                        }
                        else
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                            //obj.Depth = item.Depth;
                            //obj.Width = item.Width;
                            //obj.Offset = item.Offset;
                            //obj.SpotNoClad = item.SpotNoClad;
                        }
                        obj.Depth = item.Depth;
                        obj.Width = item.Width;
                        obj.Offset = item.Offset;
                        obj.SpotNo = item.SpotNo;
                        obj.SpotNoClad = item.SpotNoClad;
                        if (isAdded)
                        {
                            lstAddPRO194_7.Add(obj);
                        }
                    }
                }
                if (lstAddPRO194_7.Count > 0)
                {
                    db.PRO194_7.AddRange(lstAddPRO194_7);
                }
                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRO194_7 = db.PRO194_7.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRO194_7 = db.PRO194_7.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRO194_7.Count > 0)
                {
                    db.PRO194_7.RemoveRange(lstDeletePRO194_7);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line9 details

        [HttpPost]
        public JsonResult SaveProtocolLine9(List<PRO194_8> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO194_8> lstAddPRO194_8 = new List<PRO194_8>();
                List<PRO194_8> lstDeletePRO194_8 = new List<PRO194_8>();

                if (lst != null)
                {

                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO194_8 obj = db.PRO194_8.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        PRO194_8 objFecthSeam = new PRO194_8();
                        if (obj == null)
                        {
                            obj = new PRO194_8();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        objFecthSeam = db.PRO194_8.Where(x => x.SeamNo == item.SeamNo && x.HeaderId == item.HeaderId).FirstOrDefault();
                        if (objFecthSeam != null)
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                        }
                        else
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                        }

                        obj.CleatNo = item.CleatNo;
                        obj.SizeWelded = item.SizeWelded;
                        obj.Location = item.Location;
                        obj.OutsideOrInside = item.OutsideOrInside;
                        obj.DistanceFromSeam = item.DistanceFromSeam;
                        obj.ShellEvelution = item.ShellEvelution;
                        if (isAdded)
                        {
                            lstAddPRO194_8.Add(obj);
                        }
                    }
                }
                if (lstAddPRO194_8.Count > 0)
                {
                    db.PRO194_8.AddRange(lstAddPRO194_8);
                }
                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRO194_8 = db.PRO194_8.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else { lstDeletePRO194_8 = db.PRO194_8.Where(x => x.HeaderId == HeaderId).ToList(); }
                if (lstDeletePRO194_8.Count > 0)
                {
                    db.PRO194_8.RemoveRange(lstDeletePRO194_8);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line10 details
        [HttpPost]
        public JsonResult SaveProtocolLine10(List<PRO194_5> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO194_5> lstAddPRO194_5 = new List<PRO194_5>();
                List<PRO194_5> lstDeletePRO194_5 = new List<PRO194_5>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO194_5 obj = db.PRO194_5.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.Description = item.Description;
                                obj.Petal_1 = item.Petal_1;
                                obj.Petal_2 = item.Petal_2;
                                obj.Petal_3 = item.Petal_3;
                                obj.Petal_4 = item.Petal_4;
                                obj.Petal_5 = item.Petal_5;
                                obj.Petal_6 = item.Petal_6;
                                obj.Petal_7 = item.Petal_7;
                                obj.Petal_8 = item.Petal_8;
                                obj.Petal_9 = item.Petal_9;
                                obj.Petal_10 = item.Petal_10;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO194_5 obj = new PRO194_5();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.Description))
                            {
                                obj.Description = item.Description;
                                obj.Petal_1 = item.Petal_1;
                                obj.Petal_2 = item.Petal_2;
                                obj.Petal_3 = item.Petal_3;
                                obj.Petal_4 = item.Petal_4;
                                obj.Petal_5 = item.Petal_5;
                                obj.Petal_6 = item.Petal_6;
                                obj.Petal_7 = item.Petal_7;
                                obj.Petal_8 = item.Petal_8;
                                obj.Petal_9 = item.Petal_9;
                                obj.Petal_10 = item.Petal_10;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO194_5.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO194_5.Count > 0)
                    {
                        db.PRO194_5.AddRange(lstAddPRO194_5);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO194_5 = db.PRO194_5.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO194_5.Count > 0)
                    {
                        db.PRO194_5.RemoveRange(lstDeletePRO194_5);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO194_5 = db.PRO194_5.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO194_5.Count > 0)
                    {
                        db.PRO194_5.RemoveRange(lstDeletePRO194_5);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line11 details
        [HttpPost]
        public JsonResult SaveProtocolLine11(List<PRO194_6> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO194_6> lstAddPRO194_6 = new List<PRO194_6>();
                List<PRO194_6> lstDeletePRO194_6 = new List<PRO194_6>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO194_6 obj = db.PRO194_6.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.SrNo = item.SrNo;
                                obj.CrownThicknessReadings1 = item.CrownThicknessReadings1;
                                obj.CrownThicknessReadings2 = item.CrownThicknessReadings2;
                                obj.CrownThicknessReadings3 = item.CrownThicknessReadings3;
                                obj.CrownThicknessReadings4 = item.CrownThicknessReadings4;
                                obj.CrownThicknessReadings5 = item.CrownThicknessReadings5;
                                obj.CrownThicknessReadings6 = item.CrownThicknessReadings6;
                                obj.CrownThicknessReadings7 = item.CrownThicknessReadings7;
                                obj.CrownThicknessReadings8 = item.CrownThicknessReadings8;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO194_6 obj = new PRO194_6();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.SrNo))
                            {
                                obj.SrNo = item.SrNo;
                                obj.CrownThicknessReadings1 = item.CrownThicknessReadings1;
                                obj.CrownThicknessReadings2 = item.CrownThicknessReadings2;
                                obj.CrownThicknessReadings3 = item.CrownThicknessReadings3;
                                obj.CrownThicknessReadings4 = item.CrownThicknessReadings4;
                                obj.CrownThicknessReadings5 = item.CrownThicknessReadings5;
                                obj.CrownThicknessReadings6 = item.CrownThicknessReadings6;
                                obj.CrownThicknessReadings7 = item.CrownThicknessReadings7;
                                obj.CrownThicknessReadings8 = item.CrownThicknessReadings8;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO194_6.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO194_6.Count > 0)
                    {
                        db.PRO194_6.AddRange(lstAddPRO194_6);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO194_6 = db.PRO194_6.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO194_6.Count > 0)
                    {
                        db.PRO194_6.RemoveRange(lstDeletePRO194_6);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO194_6 = db.PRO194_6.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO194_6.Count > 0)
                    {
                        db.PRO194_6.RemoveRange(lstDeletePRO194_6);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line12 details
        [HttpPost]
        public JsonResult SaveProtocolLine12(List<PRO194_9> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO194_9> lstAddPRO194_9 = new List<PRO194_9>();
                List<PRO194_9> lstDeletePRO194_9 = new List<PRO194_9>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO194_9 obj = db.PRO194_9.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.TTSeamNo = item.TTSeamNo;
                                obj.TTRootGapAllowed = item.TTRootGapAllowed;
                                obj.TTRootGapActualMinimum = item.TTRootGapActualMinimum;
                                obj.TTRootGapActualMaximum = item.TTRootGapActualMaximum;
                                obj.TTOffsetAllowed = item.TTOffsetAllowed;
                                obj.TTOffsetActualMinimum = item.TTOffsetActualMinimum;
                                obj.TTOffsetActualMaximum = item.TTOffsetActualMaximum;
                                obj.TTPickInOutAllowed = item.TTPickInOutAllowed;
                                obj.TTPickInOutActualMinimum = item.TTPickInOutActualMinimum;
                                obj.TTPickInOutActualMaximum = item.TTPickInOutActualMaximum;
                                obj.TTISWepAngleRequired = item.TTISWepAngleRequired;
                                obj.TTISWepAngleActualMinimum = item.TTISWepAngleActualMinimum;
                                obj.TTISWepAngleActualMaximum = item.TTISWepAngleActualMaximum;
                                obj.TTOSWepAngleRequired = item.TTOSWepAngleRequired;
                                obj.TTOSWepAngleActualMinimum = item.TTOSWepAngleActualMinimum;
                                obj.TTOSWepAngleActualMaximum = item.TTOSWepAngleActualMaximum;
                                obj.TTRootFaceRequired = item.TTRootFaceRequired;
                                obj.TTRootFaceActualMinimum = item.TTRootFaceActualMinimum;
                                obj.TTRootFaceActualMaximum = item.TTRootFaceActualMaximum;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO194_9 obj = new PRO194_9();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.TTSeamNo))
                            {
                                obj.TTSeamNo = item.TTSeamNo;
                                obj.TTRootGapAllowed = item.TTRootGapAllowed;
                                obj.TTRootGapActualMinimum = item.TTRootGapActualMinimum;
                                obj.TTRootGapActualMaximum = item.TTRootGapActualMaximum;
                                obj.TTOffsetAllowed = item.TTOffsetAllowed;
                                obj.TTOffsetActualMinimum = item.TTOffsetActualMinimum;
                                obj.TTOffsetActualMaximum = item.TTOffsetActualMaximum;
                                obj.TTPickInOutAllowed = item.TTPickInOutAllowed;
                                obj.TTPickInOutActualMinimum = item.TTPickInOutActualMinimum;
                                obj.TTPickInOutActualMaximum = item.TTPickInOutActualMaximum;
                                obj.TTISWepAngleRequired = item.TTISWepAngleRequired;
                                obj.TTISWepAngleActualMinimum = item.TTISWepAngleActualMinimum;
                                obj.TTISWepAngleActualMaximum = item.TTISWepAngleActualMaximum;
                                obj.TTOSWepAngleRequired = item.TTOSWepAngleRequired;
                                obj.TTOSWepAngleActualMinimum = item.TTOSWepAngleActualMinimum;
                                obj.TTOSWepAngleActualMaximum = item.TTOSWepAngleActualMaximum;
                                obj.TTRootFaceRequired = item.TTRootFaceRequired;
                                obj.TTRootFaceActualMinimum = item.TTRootFaceActualMinimum;
                                obj.TTRootFaceActualMaximum = item.TTRootFaceActualMaximum;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO194_9.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO194_9.Count > 0)
                    {
                        db.PRO194_9.AddRange(lstAddPRO194_9);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO194_9 = db.PRO194_9.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO194_9.Count > 0)
                    {
                        db.PRO194_9.RemoveRange(lstDeletePRO194_9);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO194_9 = db.PRO194_9.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO194_9.Count > 0)
                    {
                        db.PRO194_9.RemoveRange(lstDeletePRO194_9);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Linkage View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL190 objPRL190 = new PRL190();
            objPRL190 = db.PRL190.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL190 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL190.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;

                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();
            List<string> lsttypesofforming = new List<string> { "HOT", "COLD", "WARM" };
            ViewBag.TypesOfForming = lsttypesofforming.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL191> lstPRL191 = db.PRL191.Where(x => x.HeaderId == objPRL190.HeaderId).ToList();
            List<PRL192> lstPRL192 = db.PRL192.Where(x => x.HeaderId == objPRL190.HeaderId).ToList();
            List<PRL193> lstPRL193 = db.PRL193.Where(x => x.HeaderId == objPRL190.HeaderId).ToList();
            List<PRL194> lstPRL194 = db.PRL194.Where(x => x.HeaderId == objPRL190.HeaderId).ToList();
            List<PRL194_2> lstPRL194_2 = db.PRL194_2.Where(x => x.HeaderId == objPRL190.HeaderId).ToList();
            List<PRL194_3> lstPRL194_3 = db.PRL194_3.Where(x => x.HeaderId == objPRL190.HeaderId).ToList();
            List<PRL194_4> lstPRL194_4 = db.PRL194_4.Where(x => x.HeaderId == objPRL190.HeaderId).ToList();
            List<PRL194_5> lstPRL194_5 = db.PRL194_5.Where(x => x.HeaderId == objPRL190.HeaderId).ToList();
            List<PRL194_6> lstPRL194_6 = db.PRL194_6.Where(x => x.HeaderId == objPRL190.HeaderId).ToList();
            List<SP_IPI_PROTOCOL039_LINKAGE_GET_LINES8_DATA_Result> lstPRL194_7 = db.SP_IPI_PROTOCOL039_LINKAGE_GET_LINES8_DATA(1, int.MaxValue, "", " HeaderId = " + objPRL190.HeaderId).ToList();
            List<SP_IPI_PROTOCOL039_LINKAGE_GET_LINES9_DATA_Result> lstPRL194_8 = db.SP_IPI_PROTOCOL039_LINKAGE_GET_LINES9_DATA(1, int.MaxValue, "", " HeaderId = " + objPRL190.HeaderId).ToList();
            List<PRL194_9> lstPRL194_9 = db.PRL194_9.Where(x => x.HeaderId == objPRL190.HeaderId).ToList();

            ViewBag.lstPRL191 = lstPRL191;
            ViewBag.lstPRL192 = lstPRL192;
            ViewBag.lstPRL193 = lstPRL193;
            ViewBag.lstPRL194 = lstPRL194;
            ViewBag.lstPRL194_2 = lstPRL194_2;
            ViewBag.lstPRL194_3 = lstPRL194_3;
            ViewBag.lstPRL194_4 = lstPRL194_4;
            ViewBag.lstPRL194_5 = lstPRL194_5;
            ViewBag.lstPRL194_6 = lstPRL194_6;
            ViewBag.lstPRL194_7 = lstPRL194_7;
            ViewBag.lstPRL194_8 = lstPRL194_8;
            ViewBag.lstPRL194_9 = lstPRL194_9;

            #endregion
            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.lstDrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL190.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL190.ActFilledBy) && (objPRL190.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL190.Project).ToList();
                            ViewBag.lstDrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.lstDrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL190);
        }

        [HttpPost]
        public ActionResult LinkageMobile(int? id, int? m, string APIRequestFrom = "", string UserName = "")
        {
            string ProjectDesc = "";
            string ManufacturingCode = "";
            string AccessRole = "";
            bool isEditable = false;
            var DrawingNo = new List<string>();
            bool isQCRemarkEditable = false;

            PRL190 objPRL190 = new PRL190();
            db.Configuration.ProxyCreationEnabled = false;
            objPRL190 = db.PRL190.Where(i => i.HeaderId == id).FirstOrDefault();
            db.Entry(objPRL190).State = System.Data.Entity.EntityState.Detached;

            NDEModels objNDEModels = new NDEModels();
            if (objPRL190 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL190.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;

                }
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
            AccessRole = objUserRoleAccessDetails.UserDesignation;


            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();
            List<string> lsttypesofforming = new List<string> { "HOT", "COLD", "WARM" };
            var TypesOfForming = lsttypesofforming.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            var YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            var LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL191> lstPRL191 = db.PRL191.Where(x => x.HeaderId == objPRL190.HeaderId).ToList();
            List<PRL192> lstPRL192 = db.PRL192.Where(x => x.HeaderId == objPRL190.HeaderId).ToList();
            List<PRL193> lstPRL193 = db.PRL193.Where(x => x.HeaderId == objPRL190.HeaderId).ToList();
            List<PRL194> lstPRL194 = db.PRL194.Where(x => x.HeaderId == objPRL190.HeaderId).ToList();
            List<PRL194_2> lstPRL194_2 = db.PRL194_2.Where(x => x.HeaderId == objPRL190.HeaderId).ToList();
            List<PRL194_3> lstPRL194_3 = db.PRL194_3.Where(x => x.HeaderId == objPRL190.HeaderId).ToList();
            List<PRL194_4> lstPRL194_4 = db.PRL194_4.Where(x => x.HeaderId == objPRL190.HeaderId).ToList();
            List<PRL194_5> lstPRL194_5 = db.PRL194_5.Where(x => x.HeaderId == objPRL190.HeaderId).ToList();
            List<PRL194_6> lstPRL194_6 = db.PRL194_6.Where(x => x.HeaderId == objPRL190.HeaderId).ToList();
            List<SP_IPI_PROTOCOL039_LINKAGE_GET_LINES8_DATA_Result> lstPRL194_7 = db.SP_IPI_PROTOCOL039_LINKAGE_GET_LINES8_DATA(1, int.MaxValue, "", " HeaderId = " + objPRL190.HeaderId).ToList();
            List<SP_IPI_PROTOCOL039_LINKAGE_GET_LINES9_DATA_Result> lstPRL194_8 = db.SP_IPI_PROTOCOL039_LINKAGE_GET_LINES9_DATA(1, int.MaxValue, "", " HeaderId = " + objPRL190.HeaderId).ToList();
            List<PRL194_9> lstPRL194_9 = db.PRL194_9.Where(x => x.HeaderId == objPRL190.HeaderId).ToList();


            #endregion
            try
            {
                isEditable = false;
                isQCRemarkEditable = false;
                if (m == 1)
                {
                    isEditable = true;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL190.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL190.ActFilledBy) && (objPRL190.RequestNo.HasValue))
                        {
                            isEditable = false;
                            isQCRemarkEditable = true;
                        }
                        else
                        {
                            DrawingNo = (new GeneralController()).GetPLMDrawingNumber(objPRL190.Project).ToList();
                        }
                    }
                }

                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    ProjectDesc = ProjectDesc,
                    ManufacturingCode = ManufacturingCode,
                    isEditable = isEditable,
                    isQCRemarkEditable = isQCRemarkEditable,
                    DrawingNo = DrawingNo,
                    OutsideInside = OutsideInside,
                    AccessRole = AccessRole,
                    YesNoEnum = YesNoEnum,
                    YesNAEnum = YesNAEnum,
                    TypesOfForming = TypesOfForming,

                    objPRL190 = objPRL190,

                    lstPRL191 = lstPRL191,
                    lstPRL192 = lstPRL192,
                    lstPRL193 = lstPRL193,
                    lstPRL194 = lstPRL194,
                    lstPRL194_2 = lstPRL194_2,
                    lstPRL194_3 = lstPRL194_3,
                    lstPRL194_4 = lstPRL194_4,
                    lstPRL194_5 = lstPRL194_5,
                    lstPRL194_6 = lstPRL194_6,
                    lstPRL194_7 = lstPRL194_7,
                    lstPRL194_8 = lstPRL194_8,
                    lstPRL194_9 = lstPRL194_9

                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.lstDrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();

                return Json(new
                {
                    res = "0",
                    msg = ex.Message,
                }, JsonRequestBehavior.AllowGet);
            }

        }
        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL190 PRL190, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = PRL190.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL190 objPRL190 = db.PRL190.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    #region Save Data

                    objPRL190.DrawingRevisionNo = PRL190.DrawingRevisionNo;
                    objPRL190.DrawingNo = PRL190.DrawingNo;
                    objPRL190.DCRNo = PRL190.DCRNo;
                    objPRL190.ProtocolNo = PRL190.ProtocolNo != null ? PRL190.ProtocolNo : "";
                    objPRL190.ReqInsideCircumferenceAtTrimmingLine = PRL190.ReqInsideCircumferenceAtTrimmingLine;
                    objPRL190.ActInsideCircumferenceAtTrimmingLine = PRL190.ActInsideCircumferenceAtTrimmingLine;
                    objPRL190.InsideCircumferenceAtTrimmingLineRemarks = PRL190.InsideCircumferenceAtTrimmingLineRemarks;
                    objPRL190.ReqTotalHeight = PRL190.ReqTotalHeight;
                    objPRL190.ActTotalHeigh = PRL190.ActTotalHeigh;
                    objPRL190.TotalHeightRemarks = PRL190.TotalHeightRemarks;
                    objPRL190.ReqOverCrowing = PRL190.ReqOverCrowing;
                    objPRL190.ActOverCrowing = PRL190.ActOverCrowing;
                    objPRL190.OverCrowingRemark = PRL190.OverCrowingRemark;
                    objPRL190.ReqUnderCrowing = PRL190.ReqUnderCrowing;
                    objPRL190.ActUnderCrowing = PRL190.ActUnderCrowing;
                    objPRL190.UnderCrowingRemark = PRL190.UnderCrowingRemark;
                    objPRL190.ReqConcentricity = PRL190.ReqConcentricity;
                    objPRL190.ActConcentricity = PRL190.ActConcentricity;
                    objPRL190.ConcentricityRemark = PRL190.ConcentricityRemark;
                    objPRL190.ReqTypeOfForming = PRL190.ReqTypeOfForming;
                    objPRL190.ActTypeOfForming = PRL190.ActTypeOfForming;
                    objPRL190.TypeOfFormingRemark = PRL190.TypeOfFormingRemark;
                    objPRL190.DimensionsOfCrown = PRL190.DimensionsOfCrown;
                    objPRL190.ReqDimensionsOfCrown = PRL190.ReqDimensionsOfCrown;
                    objPRL190.ActDimensionsOfCrownMin = PRL190.ActDimensionsOfCrownMin;
                    objPRL190.ActDimensionsOfCrownMax = PRL190.ActDimensionsOfCrownMax;
                    objPRL190.DimensionsOfCrownRemark = PRL190.DimensionsOfCrownRemark;
                    objPRL190.ReqThicknessOfCrown = PRL190.ReqThicknessOfCrown;
                    objPRL190.ActThicknessOfCrown = PRL190.ActThicknessOfCrown;
                    objPRL190.ThicknessOfCrownRemarks = PRL190.ThicknessOfCrownRemarks;
                    objPRL190.Ovality = PRL190.Ovality;
                    objPRL190.OvalityOutBy = PRL190.OvalityOutBy;
                    objPRL190.ReqThicknessMinimum = PRL190.ReqThicknessMinimum;
                    objPRL190.ReqThicknessNom = PRL190.ReqThicknessNom;
                    objPRL190.EValueTemplateInspection = PRL190.EValueTemplateInspection;
                    objPRL190.ReqChordLength = PRL190.ReqChordLength;
                    objPRL190.ActChordLength = PRL190.ActChordLength;
                    objPRL190.ReqRadius = PRL190.ReqRadius;
                    objPRL190.ActRadius = PRL190.ActRadius;
                    objPRL190.GapAllowable = PRL190.GapAllowable;
                    objPRL190.ActGap = PRL190.ActGap;
                    objPRL190.OrientationOfCrownsLongSeams = PRL190.OrientationOfCrownsLongSeams;
                    objPRL190.WepDimensionsForCrownToPTJoint = PRL190.WepDimensionsForCrownToPTJoint;
                    objPRL190.PTRootGapAllowed = PRL190.PTRootGapAllowed;
                    objPRL190.ActPTRootGapMinimum = PRL190.ActPTRootGapMinimum;
                    objPRL190.ActPTRootGapMaximum = PRL190.ActPTRootGapMaximum;
                    objPRL190.PTOffsetAllowed = PRL190.PTOffsetAllowed;
                    objPRL190.ActPTOffsetActMinimum = PRL190.ActPTOffsetActMinimum;
                    objPRL190.ActPTOffsetMaximum = PRL190.ActPTOffsetMaximum;
                    objPRL190.PTPickInOutAllowed = PRL190.PTPickInOutAllowed;
                    objPRL190.ActPTPickInOutMinimum = PRL190.ActPTPickInOutMinimum;
                    objPRL190.ActPTPickInOutMaximum = PRL190.ActPTPickInOutMaximum;
                    objPRL190.PTISWepAngleRequired = PRL190.PTISWepAngleRequired;
                    objPRL190.ActPTISIWepAngleMinimum = PRL190.ActPTISIWepAngleMinimum;
                    objPRL190.ActPTISWepAngleMaximum = PRL190.ActPTISWepAngleMaximum;
                    objPRL190.PTOSWepAngleRequired = PRL190.PTOSWepAngleRequired;
                    objPRL190.ActPTOSIWepAngleMinimum = PRL190.ActPTOSIWepAngleMinimum;
                    objPRL190.ActPTOSWepAngleMaximum = PRL190.ActPTOSWepAngleMaximum;
                    objPRL190.ReqPTRootFace = PRL190.ReqPTRootFace;
                    objPRL190.ActPTRootFaceMinimum = PRL190.ActPTRootFaceMinimum;
                    objPRL190.ActPTRootFaceMaximum = PRL190.ActPTRootFaceMaximum;
                    objPRL190.WepDimensionsForCircmferantionSeamOfTierToTireJoint = PRL190.WepDimensionsForCircmferantionSeamOfTierToTireJoint;
                    //objPRL190.TTSeamNo = PRL190.TTSeamNo;
                    //objPRL190.TTRootGapAllowed = PRL190.TTRootGapAllowed;
                    //objPRL190.TTRootGapActualMinimum = PRL190.TTRootGapActualMinimum;
                    //objPRL190.TTRootGapActualMaximum = PRL190.TTRootGapActualMaximum;
                    //objPRL190.TTOffsetAllowed = PRL190.TTOffsetAllowed;
                    //objPRL190.TTOffsetActualMinimum = PRL190.TTOffsetActualMinimum;
                    //objPRL190.TTOffsetActualMaximum = PRL190.TTOffsetActualMaximum;
                    //objPRL190.TTPickInOutAllowed = PRL190.TTPickInOutAllowed;
                    //objPRL190.TTPickInOutActualMinimum = PRL190.TTPickInOutActualMinimum;
                    //objPRL190.TTPickInOutActualMaximum = PRL190.TTPickInOutActualMaximum;
                    //objPRL190.TTISWepAngleRequired = PRL190.TTISWepAngleRequired;
                    //objPRL190.TTISWepAngleActualMinimum = PRL190.TTISWepAngleActualMinimum;
                    //objPRL190.TTISWepAngleActualMaximum = PRL190.TTISWepAngleActualMaximum;
                    //objPRL190.TTOSWepAngleRequired = PRL190.TTOSWepAngleRequired;
                    //objPRL190.TTOSWepAngleActualMinimum = PRL190.TTOSWepAngleActualMinimum;
                    //objPRL190.TTOSWepAngleActualMaximum = PRL190.TTOSWepAngleActualMaximum;
                    //objPRL190.TTRootFaceRequired = PRL190.TTRootFaceRequired;
                    //objPRL190.TTRootFaceActualMinimum = PRL190.TTRootFaceActualMinimum;
                    //objPRL190.TTRootFaceActualMaximum = PRL190.TTRootFaceActualMaximum;
                    objPRL190.PPRootGap = PRL190.PPRootGap;
                    objPRL190.PPRootFace = PRL190.PPRootFace;
                    objPRL190.PPPeakInOut = PRL190.PPPeakInOut;
                    objPRL190.Offset = PRL190.Offset;
                    objPRL190.InsideWepAngle = PRL190.InsideWepAngle;
                    objPRL190.OutsideWepAngle = PRL190.OutsideWepAngle;
                    objPRL190.HeightOfTier = PRL190.HeightOfTier;
                    objPRL190.HOTTierNo1 = PRL190.HOTTierNo1;
                    objPRL190.HOTRequired1 = PRL190.HOTRequired1;
                    objPRL190.HOTActual1 = PRL190.HOTActual1;
                    objPRL190.HOTTierNo2 = PRL190.HOTTierNo2;
                    objPRL190.HOTRequired2 = PRL190.HOTRequired2;
                    objPRL190.HOTActual2 = PRL190.HOTActual2;
                    objPRL190.HOTTierNo3 = PRL190.HOTTierNo3;
                    objPRL190.HOTRequired3 = PRL190.HOTRequired3;
                    objPRL190.HOTActual3 = PRL190.HOTActual3;
                    objPRL190.DetailThicknessReport = PRL190.DetailThicknessReport;
                    objPRL190.CrownThicknessReadings = PRL190.CrownThicknessReadings;
                    objPRL190.CheckPoint1 = PRL190.CheckPoint1;
                    objPRL190.CheckPoint2 = PRL190.CheckPoint2;
                    objPRL190.CheckPoint3 = PRL190.CheckPoint3;
                    objPRL190.CheckPoint4 = PRL190.CheckPoint4;
                    objPRL190.CheckPoint5 = PRL190.CheckPoint5;
                    objPRL190.CheckPoint6 = PRL190.CheckPoint6;
                    objPRL190.CheckPoint6_2 = PRL190.CheckPoint6_2;
                    objPRL190.CheckPoint6_3 = PRL190.CheckPoint6_3;
                    objPRL190.CheckPoint7 = PRL190.CheckPoint7;
                    objPRL190.CheckPoint7_2 = PRL190.CheckPoint7_2;
                    objPRL190.CheckPoint7_3 = PRL190.CheckPoint7_3;
                    objPRL190.CheckPoint7_4 = PRL190.CheckPoint7_4;
                    objPRL190.CheckPoint9 = PRL190.CheckPoint9;
                    objPRL190.CheckPoint10 = PRL190.CheckPoint10;
                    objPRL190.CheckPoint11 = PRL190.CheckPoint11;
                    objPRL190.QCRemarks = PRL190.QCRemarks;
                    objPRL190.Result = PRL190.Result;
                    objPRL190.ReqCladStripingWidth = PRL190.ReqCladStripingWidth;
                    objPRL190.ReqCladStripingDepth = PRL190.ReqCladStripingDepth;
                    objPRL190.ReqCladStripingOffset = PRL190.ReqCladStripingOffset;
                    objPRL190.Note1 = PRL190.Note1;
                    objPRL190.Note2 = PRL190.Note2;
                    objPRL190.Note3 = PRL190.Note3;
                    objPRL190.IdentificationDetailsOfCleats = PRL190.IdentificationDetailsOfCleats;
                    objPRL190.EditedBy = UserName;
                    objPRL190.EditedOn = DateTime.Now;

                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL190.ActFilledBy = UserName;
                            objPRL190.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL190.ReqFilledBy = UserName;
                            objPRL190.ReqFilledOn = DateTime.Now;
                        }
                    }
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL190.HeaderId;
                    #endregion

                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL190 objPRL190 = db.PRL190.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL190 != null && string.IsNullOrWhiteSpace(objPRL190.ProtocolNo))
                    {
                        db.PRL190.Remove(objPRL190);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL190.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL191> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL191> lstAddPRL191 = new List<PRL191>();
                List<PRL191> lstDeletePRL191 = new List<PRL191>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRL191 obj = db.PRL191.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.Orientation = item.Orientation;
                                obj.Actual = item.Actual;
                                obj.EditedBy = UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRL191 obj = new PRL191();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.Orientation))
                            {
                                obj.Orientation = item.Orientation;
                                obj.Actual = item.Actual;
                                obj.CreatedBy = UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRL191.Add(obj);
                            }
                        }
                    }
                }
                if (lstAddPRL191.Count > 0)
                {
                    db.PRL191.AddRange(lstAddPRL191);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL191 = db.PRL191.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL191 = db.PRL191.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL191.Count > 0)
                {
                    db.PRL191.RemoveRange(lstDeletePRL191);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine2Linkage(List<PRL192> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL192> lstAddPRL192 = new List<PRL192>();
                List<PRL192> lstDeletePRL192 = new List<PRL192>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL192 obj = db.PRL192.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL192();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.ThiknessColumn1 = item.ThiknessColumn1;
                        obj.ThiknessColumn2 = item.ThiknessColumn2;
                        if (isAdded)
                        {
                            lstAddPRL192.Add(obj);
                        }
                    }
                }
                if (lstAddPRL192.Count > 0)
                {
                    db.PRL192.AddRange(lstAddPRL192);
                }
                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL192 = db.PRL192.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL192 = db.PRL192.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL192.Count > 0)
                {
                    db.PRL192.RemoveRange(lstDeletePRL192);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line3 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine3Linkage(List<PRL193> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL193> lstAddPRL193 = new List<PRL193>();
                List<PRL193> lstDeletePRL193 = new List<PRL193>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL193 obj = db.PRL193.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        else
                        {
                            obj = new PRL193();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }

                        if (!string.IsNullOrWhiteSpace(item.SeamNo))
                        {
                            obj.SeamNo = item.SeamNo;
                            obj.ReqOrien = item.ReqOrien;
                            obj.ReqArcLength = item.ReqArcLength;
                            obj.ActualArcLength = item.ActualArcLength;
                        }
                        if (isAdded)
                        {
                            lstAddPRL193.Add(obj);
                        }
                    }
                    if (lstAddPRL193.Count > 0)
                    {
                        db.PRL193.AddRange(lstAddPRL193);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL193 = db.PRL193.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL193 = db.PRL193.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL193.Count > 0)
                {
                    db.PRL193.RemoveRange(lstDeletePRL193);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line4 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine4Linkage(List<PRL194> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL194> lstAddPRL194 = new List<PRL194>();
                List<PRL194> lstDeletePRL194 = new List<PRL194>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL194 obj = db.PRL194.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL194();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SeamNo = item.SeamNo;
                        obj.ReqOrientationAtCrownCenter = item.ReqOrientationAtCrownCenter;
                        obj.ActOrientationAtCrownCenter = item.ActOrientationAtCrownCenter;
                        obj.ReqArcLengthAtCL = item.ReqArcLengthAtCL;
                        obj.ActArcLengthAtCL = item.ActArcLengthAtCL;
                        obj.ReqOffsetDistFromSeam = item.ReqOffsetDistFromSeam;
                        obj.ActOffsetDistFromSeam = item.ActOffsetDistFromSeam;
                        if (isAdded)
                        {
                            lstAddPRL194.Add(obj);
                        }
                    }

                    if (lstAddPRL194.Count > 0)
                    {
                        db.PRL194.AddRange(lstAddPRL194);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL194 = db.PRL194.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL194 = db.PRL194.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL194.Count > 0)
                {
                    db.PRL194.RemoveRange(lstDeletePRL194);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line5 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine5Linkage(List<PRL194_2> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL194_2> lstAddPRL194_2 = new List<PRL194_2>();
                List<PRL194_2> lstDeletePRL194_2 = new List<PRL194_2>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL194_2 obj = db.PRL194_2.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL194_2();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        obj.SeamNo = item.SeamNo;
                        obj.RootGapMin = item.RootGapMin;
                        obj.RootGapMax = item.RootGapMax;
                        obj.RootFaceMin = item.RootFaceMin;
                        obj.RootFaceMax = item.RootFaceMax;
                        obj.Peak_in_out_Top = item.Peak_in_out_Top;
                        obj.Peak_in_out_Mid = item.Peak_in_out_Mid;
                        obj.Peak_in_out_Bottom = item.Peak_in_out_Bottom;
                        obj.OffSetMin = item.OffSetMin;
                        obj.OffSetMax = item.OffSetMax;
                        obj.Inside_Wep_AngleMin = item.Inside_Wep_AngleMin;
                        obj.Inside_Wep_AngleMax = item.Inside_Wep_AngleMax;
                        obj.Outside_Wep_AngleMin = item.Outside_Wep_AngleMin;
                        obj.Outside_Wep_AngleMax = item.Outside_Wep_AngleMax;

                        if (isAdded)
                        {
                            lstAddPRL194_2.Add(obj);
                        }
                    }

                    if (lstAddPRL194_2.Count > 0)
                    {
                        db.PRL194_2.AddRange(lstAddPRL194_2);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL194_2 = db.PRL194_2.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL194_2 = db.PRL194_2.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL194_2.Count > 0)
                {
                    db.PRL194_2.RemoveRange(lstDeletePRL194_2);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        //#region Line6 Linkage
        //[HttpPost]
        //public JsonResult SaveProtocolLine6Linkage(List<PRL194_3> lst, int HeaderId)
        //{
        //    clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
        //    try
        //    {
        //        List<PRL194_3> lstAddPRL194_3 = new List<PRL194_3>();
        //        List<PRL194_3> lstDeletePRL194_3 = new List<PRL194_3>();
        //        if (lst != null)
        //        {
        //            foreach (var item in lst)
        //            {
        //                bool isAdded = false;
        //                PRL194_3 obj = db.PRL194_3.Where(x => x.LineId == item.LineId).FirstOrDefault();
        //                if (obj == null)
        //                {
        //                    obj = new PRL194_3();
        //                    obj.HeaderId = item.HeaderId;
        //                    obj.CreatedBy = objClsLoginInfo.UserName;
        //                    obj.CreatedOn = DateTime.Now;
        //                    isAdded = true;
        //                }
        //                else
        //                {
        //                    obj.EditedBy = objClsLoginInfo.UserName;
        //                    obj.EditedOn = DateTime.Now;
        //                }
        //                obj.SeamNo = item.SeamNo;
        //                obj.OffSetMin = item.OffSetMin;
        //                obj.OffSetMax = item.OffSetMax;
        //                obj.Inside_Wep_AngleMin = item.Inside_Wep_AngleMin;
        //                obj.Inside_Wep_AngleMax = item.Inside_Wep_AngleMax;
        //                obj.Outside_Wep_AngleMin = item.Outside_Wep_AngleMin;
        //                obj.Outside_Wep_AngleMax = item.Outside_Wep_AngleMax;
        //                if (isAdded)
        //                {
        //                    lstAddPRL194_3.Add(obj);
        //                }
        //            }

        //            if (lstAddPRL194_3.Count > 0)
        //            {
        //                db.PRL194_3.AddRange(lstAddPRL194_3);
        //            }

        //            var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
        //            lstDeletePRL194_3 = db.PRL194_3.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
        //        }
        //        else
        //        {
        //            lstDeletePRL194_3 = db.PRL194_3.Where(x => x.HeaderId == HeaderId).ToList();
        //        }
        //        if (lstDeletePRL194_3.Count > 0)
        //        {
        //            db.PRL194_3.RemoveRange(lstDeletePRL194_3);
        //        }
        //        db.SaveChanges();

        //        objResponseMsg.Key = true;
        //    }
        //    catch (Exception ex)
        //    {
        //        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        //        objResponseMsg.Key = false;
        //        objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
        //    }
        //    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        //}

        //#endregion

        #region Line7 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine7Linkage(List<PRL194_4> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL194_4> lstAddPRL194_4 = new List<PRL194_4>();
                List<PRL194_4> lstDeletePRL194_4 = new List<PRL194_4>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL194_4 obj = db.PRL194_4.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL194_4();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        obj.MaterialIdentificationReport = item.MaterialIdentificationReport;
                        if (isAdded)
                        {
                            lstAddPRL194_4.Add(obj);
                        }
                    }

                    if (lstAddPRL194_4.Count > 0)
                    {
                        db.PRL194_4.AddRange(lstAddPRL194_4);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL194_4 = db.PRL194_4.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL194_4 = db.PRL194_4.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL194_4.Count > 0)
                {
                    db.PRL194_4.RemoveRange(lstDeletePRL194_4);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line10 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine10Linkage(List<PRL194_5> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL194_5> lstAddPRL194_5 = new List<PRL194_5>();
                List<PRL194_5> lstDeletePRL194_5 = new List<PRL194_5>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL194_5 obj = db.PRL194_5.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL194_5();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        obj.Description = item.Description;
                        obj.Petal_1 = item.Petal_1;
                        obj.Petal_2 = item.Petal_2;
                        obj.Petal_3 = item.Petal_3;
                        obj.Petal_4 = item.Petal_4;
                        obj.Petal_5 = item.Petal_5;
                        obj.Petal_6 = item.Petal_6;
                        obj.Petal_7 = item.Petal_7;
                        obj.Petal_8 = item.Petal_8;
                        obj.Petal_9 = item.Petal_9;
                        obj.Petal_10 = item.Petal_10;
                        if (isAdded)
                        {
                            lstAddPRL194_5.Add(obj);
                        }
                    }

                    if (lstAddPRL194_5.Count > 0)
                    {
                        db.PRL194_5.AddRange(lstAddPRL194_5);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL194_5 = db.PRL194_5.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL194_5 = db.PRL194_5.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL194_5.Count > 0)
                {
                    db.PRL194_5.RemoveRange(lstDeletePRL194_5);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line11 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine11Linkage(List<PRL194_6> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL194_6> lstAddPRL194_6 = new List<PRL194_6>();
                List<PRL194_6> lstDeletePRL194_6 = new List<PRL194_6>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL194_6 obj = db.PRL194_6.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL194_6();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        obj.SrNo = item.SrNo;
                        obj.CrownThicknessReadings1 = item.CrownThicknessReadings1;
                        obj.CrownThicknessReadings2 = item.CrownThicknessReadings2;
                        obj.CrownThicknessReadings3 = item.CrownThicknessReadings3;
                        obj.CrownThicknessReadings4 = item.CrownThicknessReadings4;
                        obj.CrownThicknessReadings5 = item.CrownThicknessReadings5;
                        obj.CrownThicknessReadings6 = item.CrownThicknessReadings6;
                        obj.CrownThicknessReadings7 = item.CrownThicknessReadings7;
                        obj.CrownThicknessReadings8 = item.CrownThicknessReadings8;
                        if (isAdded)
                        {
                            lstAddPRL194_6.Add(obj);
                        }
                    }

                    if (lstAddPRL194_6.Count > 0)
                    {
                        db.PRL194_6.AddRange(lstAddPRL194_6);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL194_6 = db.PRL194_6.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL194_6 = db.PRL194_6.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL194_6.Count > 0)
                {
                    db.PRL194_6.RemoveRange(lstDeletePRL194_6);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line8 Linkage

        [HttpPost]
        public JsonResult SaveProtocolLine8Linkage(List<PRL194_7> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL194_7> lstAddPRL194_7 = new List<PRL194_7>();
                List<PRL194_7> lstDeletePRL194_7 = new List<PRL194_7>();

                if (lst != null)
                {

                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL194_7 obj = db.PRL194_7.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        PRL194_7 objFecthSeam = new PRL194_7();
                        if (obj == null)
                        {
                            obj = new PRL194_7();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        objFecthSeam = db.PRL194_7.Where(x => x.SeamNo == item.SeamNo && x.HeaderId == item.HeaderId).FirstOrDefault();
                        if (objFecthSeam != null)
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                            //obj.Depth = objFecthSeam.Depth;
                            //obj.Width = objFecthSeam.Width;
                            //obj.Offset = objFecthSeam.Offset;
                        }
                        else
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                            //obj.Depth = item.Depth;
                            //obj.Width = item.Width;
                            //obj.Offset = item.Offset;
                        }
                        obj.Depth = item.Depth;
                        obj.Width = item.Width;
                        obj.Offset = item.Offset;
                        obj.SpotNo = item.SpotNo;
                        obj.SpotNoClad = item.SpotNoClad;
                        if (isAdded)
                        {
                            lstAddPRL194_7.Add(obj);
                        }
                    }
                    if (lstAddPRL194_7.Count > 0)
                    {
                        db.PRL194_7.AddRange(lstAddPRL194_7);
                    }
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL194_7 = db.PRL194_7.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL194_7 = db.PRL194_7.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL194_7.Count > 0)
                {
                    db.PRL194_7.RemoveRange(lstDeletePRL194_7);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line9 Linkage

        [HttpPost]
        public JsonResult SaveProtocolLine9Linkage(List<PRL194_8> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL194_8> lstAddPRL194_8 = new List<PRL194_8>();
                List<PRL194_8> lstDeletePRL194_8 = new List<PRL194_8>();
                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL194_8 obj = db.PRL194_8.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        PRL194_8 objFecthSeam = new PRL194_8();
                        if (obj == null)
                        {
                            obj = new PRL194_8();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        objFecthSeam = db.PRL194_8.Where(x => x.SeamNo == item.SeamNo && x.HeaderId == item.HeaderId).FirstOrDefault();
                        if (objFecthSeam != null)
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                        }
                        else
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                        }
                        obj.CleatNo = item.CleatNo;
                        obj.SizeWelded = item.SizeWelded;
                        obj.Location = item.Location;
                        obj.OutsideOrInside = item.OutsideOrInside;
                        obj.DistanceFromSeam = item.DistanceFromSeam;
                        obj.ShellEvelution = item.ShellEvelution;
                        if (isAdded)
                        {
                            lstAddPRL194_8.Add(obj);
                        }
                    }
                    if (lstAddPRL194_8.Count > 0)
                    {
                        db.PRL194_8.AddRange(lstAddPRL194_8);
                    }
                    lstDeletePRL194_8 = db.PRL194_8.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL194_8 = db.PRL194_8.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL194_8.Count > 0)
                {
                    db.PRL194_8.RemoveRange(lstDeletePRL194_8);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line12 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine12Linkage(List<PRL194_9> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL194_9> lstAddPRL194_9 = new List<PRL194_9>();
                List<PRL194_9> lstDeletePRL194_9 = new List<PRL194_9>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL194_9 obj = db.PRL194_9.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        else
                        {
                            obj = new PRL194_9();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }

                        if (!string.IsNullOrWhiteSpace(item.TTSeamNo))
                        {
                            obj.TTSeamNo = item.TTSeamNo;
                            obj.TTRootGapAllowed = item.TTRootGapAllowed;
                            obj.TTRootGapActualMinimum = item.TTRootGapActualMinimum;
                            obj.TTRootGapActualMaximum = item.TTRootGapActualMaximum;
                            obj.TTOffsetAllowed = item.TTOffsetAllowed;
                            obj.TTOffsetActualMinimum = item.TTOffsetActualMinimum;
                            obj.TTOffsetActualMaximum = item.TTOffsetActualMaximum;
                            obj.TTPickInOutAllowed = item.TTPickInOutAllowed;
                            obj.TTPickInOutActualMinimum = item.TTPickInOutActualMinimum;
                            obj.TTPickInOutActualMaximum = item.TTPickInOutActualMaximum;
                            obj.TTISWepAngleRequired = item.TTISWepAngleRequired;
                            obj.TTISWepAngleActualMinimum = item.TTISWepAngleActualMinimum;
                            obj.TTISWepAngleActualMaximum = item.TTISWepAngleActualMaximum;
                            obj.TTOSWepAngleRequired = item.TTOSWepAngleRequired;
                            obj.TTOSWepAngleActualMinimum = item.TTOSWepAngleActualMinimum;
                            obj.TTOSWepAngleActualMaximum = item.TTOSWepAngleActualMaximum;
                            obj.TTRootFaceRequired = item.TTRootFaceRequired;
                            obj.TTRootFaceActualMinimum = item.TTRootFaceActualMinimum;
                            obj.TTRootFaceActualMaximum = item.TTRootFaceActualMaximum;
                        }
                        if (isAdded)
                        {
                            lstAddPRL194_9.Add(obj);
                        }
                    }
                    if (lstAddPRL194_9.Count > 0)
                    {
                        db.PRL194_9.AddRange(lstAddPRL194_9);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL194_9 = db.PRL194_9.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL194_9 = db.PRL194_9.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL194_9.Count > 0)
                {
                    db.PRL194_9.RemoveRange(lstDeletePRL194_9);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #endregion
    }
}
