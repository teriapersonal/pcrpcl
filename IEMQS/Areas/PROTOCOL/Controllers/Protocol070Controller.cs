﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;


namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol070Controller : clsBase
    {
        // GET: PROTOCOL/Protocol070
        string ControllerURL = "/PROTOCOL/Protocol070/";
        string Title = "HELIUM LEAK TEST REPORT";

        #region Details View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO345 objPRO345 = new PRO345();

            if (!id.HasValue)
            {
                try
                {
                    objPRO345.ProtocolNo = string.Empty;
                    objPRO345.CreatedBy = objClsLoginInfo.UserName;
                    objPRO345.CreatedOn = DateTime.Now;

                    
                    db.PRO345.Add(objPRO345);
                    db.SaveChanges();
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO345.HeaderId;
            }
            else
            {
                objPRO345 = db.PRO345.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstStageOfInspection = new List<string> { "BEFORE PWHT", "AFTER PWHT", " BEFORE HYDRO", " AFTER HYDRO", "AFTER COMPLETE WELD", "AFTER PARTIAL WELD", "AFTER ROOT RUN WELDING" };

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.StageOfInspection = lstStageOfInspection.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO346> lstPRO346 = db.PRO346.Where(x => x.HeaderId == objPRO345.HeaderId).ToList();

            ViewBag.lstPRO346 = lstPRO346;

          
            #endregion

            return View(objPRO345);
        }

        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO345 PRO345)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO345.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO345 objPRO345 = db.PRO345.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO345.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO345.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data
                        objPRO345.ProtocolNo = PRO345.ProtocolNo;
                        objPRO345.ProcedureNo = PRO345.ProcedureNo;                              
                        objPRO345.TestDescription = PRO345.TestDescription;  
                        objPRO345.DateOfTest = PRO345.DateOfTest                                  ;
                        objPRO345.StageOfInspection = PRO345.StageOfInspection;
                        objPRO345.TestTechnique = PRO345.TestTechnique;
                        objPRO345.NameOfHeliumLeakDetectorMachine = PRO345.NameOfHeliumLeakDetectorMachine;
                        objPRO345.ModelNo = PRO345.ModelNo;
                        objPRO345.ReqTestPressure = PRO345.ReqTestPressure;
                        objPRO345.ActTestPressure = PRO345.ActTestPressure;
                        objPRO345.ReqHoldingTime = PRO345.ReqHoldingTime;
                        objPRO345.ActHoldingTime = PRO345.ActHoldingTime;
                        objPRO345.ReqTestTemperature = PRO345.ReqTestTemperature;
                        objPRO345.ActTestTemperature = PRO345.ActTestTemperature;
                        objPRO345.LeakRateMeasuredOnScaleOfHeliumMachine = PRO345.LeakRateMeasuredOnScaleOfHeliumMachine;
                        objPRO345.StandardLeakSerialNumber = PRO345.StandardLeakSerialNumber;
                        objPRO345.CelebrationDueDate = PRO345.CelebrationDueDate;
                        objPRO345.LeakRateOfStandardLeak = PRO345.LeakRateOfStandardLeak;
                        objPRO345.BaselineReadingAfterConnectingSnifferProbe = PRO345.BaselineReadingAfterConnectingSnifferProbe;
                        objPRO345.AcceptanceCriteria = PRO345.AcceptanceCriteria;
                        objPRO345.LeakRateObserved = PRO345.LeakRateObserved;
                        objPRO345.AnyDeflectionOnIndicatorOfHeliumLeakDetector = PRO345.AnyDeflectionOnIndicatorOfHeliumLeakDetector;
                        objPRO345.ObservationAndRemarks = PRO345.ObservationAndRemarks;               
                        objPRO345.Result = PRO345.Result;  
                        objPRO345.EditedBy = PRO345.EditedBy;                              
                        objPRO345.EditedOn = PRO345.EditedOn;                             
                        objPRO345.EditedBy = objClsLoginInfo.UserName;                     
                        objPRO345.EditedOn = DateTime.Now;                                  

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO345.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO345 objPRO345 = db.PRO345.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO345 != null && string.IsNullOrWhiteSpace(objPRO345.ProtocolNo))
                    {
                        db.PRO345.Remove(objPRO345);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO345.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
       


        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO346> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO346> lstAddPRO346 = new List<PRO346>();
                List<PRO346> lstDeletePRO346 = new List<PRO346>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO346 obj = db.PRO346.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO346();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.GaugeSerialNumber = item.GaugeSerialNumber;
                        obj.GaugeCelebrationDueDate = item.GaugeCelebrationDueDate;

                        if (isAdded)
                        {
                            lstAddPRO346.Add(obj);
                        }
                    }
                    if (lstAddPRO346.Count > 0)
                    {
                        db.PRO346.AddRange(lstAddPRO346);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO346 = db.PRO346.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO346.Count > 0)
                    {
                        db.PRO346.RemoveRange(lstDeletePRO346);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO346 = db.PRO346.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO346.Count > 0)
                    {
                        db.PRO346.RemoveRange(lstDeletePRO346);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #endregion

        #region  Linkage View Code   

        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL345 objPRL345 = new PRL345();
            objPRL345 = db.PRL345.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL345 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL345.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();
            List<string> lstStageOfInspection = new List<string> { "BEFORE PWHT", "AFTER PWHT", " BEFORE HYDRO", " AFTER HYDRO", "AFTER COMPLETE WELD", "AFTER PARTIAL WELD", "AFTER ROOT RUN WELDING" };

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.StageOfInspection = lstStageOfInspection.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL346> lstPRL346 = db.PRL346.Where(x => x.HeaderId == objPRL345.HeaderId).ToList();

            ViewBag.lstPRL346 = lstPRL346;
           

            #endregion

            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.DrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL345.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL345.ActFilledBy) && (objPRL345.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL345.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL345);
        }

        [HttpPost]
        public ActionResult LinkageMobile(int? id, int? m, string APIRequestFrom = "", string UserName = "")
        {
            string ProjectDesc = "";
            string ManufacturingCode = "";
            string AccessRole = "";
            bool isEditable = false;
            var DrawingNo = new List<string>();
            bool isQCRemarkEditable = false;

            PRL345 objPRL345 = new PRL345();
            db.Configuration.ProxyCreationEnabled = false;
            objPRL345 = db.PRL345.Where(i => i.HeaderId == id).FirstOrDefault();
            db.Entry(objPRL345).State = System.Data.Entity.EntityState.Detached;
            NDEModels objNDEModels = new NDEModels();
            if (objPRL345 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL345.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
            AccessRole = objUserRoleAccessDetails.UserDesignation;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();
            List<string> lstStageOfInspection = new List<string> { "BEFORE PWHT", "AFTER PWHT", " BEFORE HYDRO", " AFTER HYDRO", "AFTER COMPLETE WELD", "AFTER PARTIAL WELD", "AFTER ROOT RUN WELDING" };

            var YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var StageOfInspection = lstStageOfInspection.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL346> lstPRL346 = db.PRL346.Where(x => x.HeaderId == objPRL345.HeaderId).ToList();

            #endregion
            try
            {
                isEditable = false;
                isQCRemarkEditable = false;
                if (m == 1)
                {
                    isEditable = true;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL345.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL345.ActFilledBy) && (objPRL345.RequestNo.HasValue))
                        {
                            isEditable = false;
                            isQCRemarkEditable = true;
                        }
                        else
                        {
                            DrawingNo = (new GeneralController()).GetPLMDrawingNumber(objPRL345.Project).ToList();
                        }
                    }
                }

                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    ProjectDesc = ProjectDesc,
                    ManufacturingCode = ManufacturingCode,
                    isEditable = isEditable,
                    isQCRemarkEditable = isQCRemarkEditable,
                    DrawingNo = DrawingNo,
                    AccessRole = AccessRole,

                    YesNoEnum = YesNoEnum,
                    YesNAEnum = YesNAEnum,
                    StageOfInspection = StageOfInspection,
                    OutsideInside = OutsideInside,
                    LeftRight = LeftRight,

                    objPRL345 = objPRL345,

                    lstPRL346 = lstPRL346

                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                return Json(new
                {
                    res = "0",
                    msg = ex.Message,
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL345 PRL345, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = PRL345.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL345 objPRL345 = db.PRL345.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL345.DrawingRevisionNo = PRL345.DrawingRevisionNo;
                    objPRL345.DrawingNo = PRL345.DrawingNo;
                    objPRL345.DCRNo = PRL345.DCRNo;
                    objPRL345.ProtocolNo = PRL345.ProtocolNo;
                    objPRL345.ProcedureNo = PRL345.ProcedureNo;
                    objPRL345.TestDescription = PRL345.TestDescription;
                    objPRL345.DateOfTest = PRL345.DateOfTest;
                    objPRL345.StageOfInspection = PRL345.StageOfInspection;
                    objPRL345.TestTechnique = PRL345.TestTechnique;
                    objPRL345.NameOfHeliumLeakDetectorMachine = PRL345.NameOfHeliumLeakDetectorMachine;
                    objPRL345.ModelNo = PRL345.ModelNo;
                    objPRL345.ReqTestPressure = PRL345.ReqTestPressure;
                    objPRL345.ActTestPressure = PRL345.ActTestPressure;
                    objPRL345.ReqHoldingTime = PRL345.ReqHoldingTime;
                    objPRL345.ActHoldingTime = PRL345.ActHoldingTime;
                    objPRL345.ReqTestTemperature = PRL345.ReqTestTemperature;
                    objPRL345.ActTestTemperature = PRL345.ActTestTemperature;
                    objPRL345.LeakRateMeasuredOnScaleOfHeliumMachine = PRL345.LeakRateMeasuredOnScaleOfHeliumMachine;
                    objPRL345.StandardLeakSerialNumber = PRL345.StandardLeakSerialNumber;
                    objPRL345.CelebrationDueDate = PRL345.CelebrationDueDate;
                    objPRL345.LeakRateOfStandardLeak = PRL345.LeakRateOfStandardLeak;
                    objPRL345.BaselineReadingAfterConnectingSnifferProbe = PRL345.BaselineReadingAfterConnectingSnifferProbe;
                    objPRL345.AcceptanceCriteria = PRL345.AcceptanceCriteria;
                    objPRL345.LeakRateObserved = PRL345.LeakRateObserved;
                    objPRL345.AnyDeflectionOnIndicatorOfHeliumLeakDetector = PRL345.AnyDeflectionOnIndicatorOfHeliumLeakDetector;
                    objPRL345.ObservationAndRemarks = PRL345.ObservationAndRemarks;
                    objPRL345.Result = PRL345.Result;

                    objPRL345.EditedBy = UserName;
                    objPRL345.EditedOn = DateTime.Now;

                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL345.ActFilledBy = UserName;
                            objPRL345.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL345.ReqFilledBy = UserName;
                            objPRL345.ReqFilledOn = DateTime.Now;
                        }
                    }

                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL345.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL345 objPRL345 = db.PRL345.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL345 != null && string.IsNullOrWhiteSpace(objPRL345.ProtocolNo))
                    {
                        db.PRL345.Remove(objPRL345);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL345.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL346> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL346> lstAddPRL346 = new List<PRL346>();
                List<PRL346> lstDeletePRL346 = new List<PRL346>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL346 obj = db.PRL346.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL346();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.GaugeSerialNumber = item.GaugeSerialNumber;
                        obj.GaugeCelebrationDueDate = item.GaugeCelebrationDueDate;
                       
                        if (isAdded)
                        {
                            lstAddPRL346.Add(obj);
                        }
                    }
                }
                if (lstAddPRL346.Count > 0)
                {
                    db.PRL346.AddRange(lstAddPRL346);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL346 = db.PRL346.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL346 = db.PRL346.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL346.Count > 0)
                {
                    db.PRL346.RemoveRange(lstDeletePRL346);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        

        #endregion
    }
}