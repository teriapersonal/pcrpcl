﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol010Controller : clsBase
    {
        #region Utility
        public string HTMLActionString(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", string onHrefURL = "", bool isDisable = false)
        {
            string htmlControl = "";
            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? onClickMethod : "";
            string onHref = !string.IsNullOrEmpty(onHrefURL) ? "href='" + onHrefURL + "'" : "";

            if (isDisable)
            {
                htmlControl += "<i id=\"" + inputID.ToString() + "\" name=\"" + inputID + "\" ></i>";
                //htmlControl = "<i id=\"" + inputID + "' name='" + inputID + "' style='cursor: pointer; opacity:0.3; margin-right:10px;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";
            }
            else
            {
                htmlControl += "<i id=\"" + inputID.ToString() + "\" name=\"" + inputID + "\"  style='cursor: pointer; margin-right:10px;' Title=\"" + buttonTooltip + "\" class=\"" + className + "\"  onclick=\"" + onClickEvent + "\" ></i>";
                //htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer; margin-right:10px;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
            }

            if (!string.IsNullOrEmpty(onHref))
            {
                htmlControl = "<a " + onHref + " >" + htmlControl + "</a>";
            }

            return htmlControl;
        }
        #endregion

        // GET: PROTOCOL/Protocol010
        #region Protocol

        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO045 objPRO045 = new PRO045();
            if (!id.HasValue)
            {
                objPRO045.ProtocolNo = string.Empty;
                objPRO045.CreatedBy = objClsLoginInfo.UserName;
                objPRO045.CreatedOn = DateTime.Now;
                db.PRO045.Add(objPRO045);
                db.SaveChanges();
                ViewBag.HeaderId = objPRO045.HeaderId;
            }
            else
            {
                objPRO045 = db.PRO045.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = "SET-UP & DIMENSION REPORT FOR FORMED ELBOW LONG. SEAMS";

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            List<string> lstProtocolRT = clsImplementationEnum.getProtocolRT().ToList();
            ViewBag.ProtocolRT = lstProtocolRT.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            List<string> lstProtocolUT = clsImplementationEnum.getProtocolRT().ToList();
            ViewBag.ProtocolUT = lstProtocolUT.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            ViewBag.ProtocolTOFDUT = new List<CategoryData> { new CategoryData { Value = "100%", Code = "100%", CategoryDescription = "100%" }, new CategoryData { Value = "NA", Code = "NA", CategoryDescription = "NA" } };

            return View(objPRO045);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO045 model)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = model.HeaderId;
                bool duplicateProtocol = db.PRO045.Where(x => x.ProtocolNo == model.ProtocolNo && x.HeaderId != model.HeaderId).Any();
                if (duplicateProtocol)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.ProtocolExists.ToString();
                    objResponseMsg.HeaderId = 0;
                }
                else
                {
                    if (refHeaderId.HasValue && refHeaderId > 0)
                    {
                        PRO045 objPRO045 = db.PRO045.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                        #region Save Data                                                            
                        objPRO045.HeaderId = model.HeaderId;
                        objPRO045.ProtocolNo = model.ProtocolNo;
                        objPRO045.TypeOfForming = model.TypeOfForming;
                        objPRO045.IdentificationMarking = model.IdentificationMarking;
                        objPRO045.PtcRequired = model.PtcRequired;
                        objPRO045.ItNo = model.ItNo;
                        objPRO045.HeatNo = model.HeatNo;
                        objPRO045.WepUt = model.WepUt;
                        objPRO045.WepPt = model.WepPt;
                        objPRO045.WepMt = model.WepMt;
                        objPRO045.WepCust = model.WepCust;
                        objPRO045.SeamNoPunching = model.SeamNoPunching;
                        objPRO045.NdtRt = model.NdtRt;
                        objPRO045.NdtTofdUt = model.NdtTofdUt;
                        objPRO045.NdtUt = model.NdtUt;
                        objPRO045.WepAngleIs = model.WepAngleIs;
                        objPRO045.WepAngleOs = model.WepAngleOs;
                        objPRO045.TacksFreeFromCrack = model.TacksFreeFromCrack;
                        objPRO045.RootGapRequired = model.RootGapRequired;
                        objPRO045.RootGapActMax = model.RootGapActMax;
                        objPRO045.RootGapActMin = model.RootGapActMin;
                        objPRO045.RootFaceRequired1 = model.RootFaceRequired1;
                        objPRO045.RootFaceActMax1 = model.RootFaceActMax1;
                        objPRO045.RootFaceActMin1 = model.RootFaceActMin1;
                        objPRO045.RootFaceRequired2 = model.RootFaceRequired2;
                        objPRO045.RootFaceActMax2 = model.RootFaceActMax2;
                        objPRO045.RootFaceActMin2 = model.RootFaceActMin2;
                        objPRO045.ThicknessReqired = model.ThicknessReqired;
                        objPRO045.ThicknessActual1 = model.ThicknessActual1;
                        objPRO045.ThicknessActual2 = model.ThicknessActual2;
                        objPRO045.ThicknessActual3 = model.ThicknessActual3;
                        objPRO045.ThicknessActual4 = model.ThicknessActual4;
                        objPRO045.ThicknessActual5 = model.ThicknessActual5;
                        objPRO045.ThicknessActual6 = model.ThicknessActual6;
                        objPRO045.ThicknessActual7 = model.ThicknessActual7;
                        objPRO045.ThicknessActual8 = model.ThicknessActual8;
                        objPRO045.ThicknessActual9 = model.ThicknessActual9;
                        objPRO045.OffsetTolerance = model.OffsetTolerance;
                        objPRO045.OffsetActual1 = model.OffsetActual1;
                        objPRO045.OffsetActual2 = model.OffsetActual2;
                        objPRO045.OffsetActual3 = model.OffsetActual3;
                        objPRO045.ProfileMeasurement = model.ProfileMeasurement;
                        objPRO045.SeamNo1 = model.SeamNo1;
                        objPRO045.SeamNo2 = model.SeamNo2;
                        objPRO045.FirstEndPeakIn1 = model.FirstEndPeakIn1;
                        objPRO045.FirstEndPeakIn2 = model.FirstEndPeakIn2;
                        objPRO045.FirstEndPeakOut1 = model.FirstEndPeakOut1;
                        objPRO045.FirstEndPeakOut2 = model.FirstEndPeakOut2;
                        objPRO045.MidPeakIn1 = model.MidPeakIn1;
                        objPRO045.MidPeakIn2 = model.MidPeakIn2;
                        objPRO045.MidPeakOut1 = model.MidPeakOut1;
                        objPRO045.MidPeakOut2 = model.MidPeakOut2;
                        objPRO045.SecondEndPeakIn1 = model.SecondEndPeakIn1;
                        objPRO045.SecondEndPeakIn2 = model.SecondEndPeakIn2;
                        objPRO045.SecondEndPeakOut1 = model.SecondEndPeakOut1;
                        objPRO045.SecondEndPeakOut2 = model.SecondEndPeakOut2;
                        objPRO045.Remark1 = model.Remark1;
                        objPRO045.Remark2 = model.Remark2;
                        objPRO045.ProfileMeasurementReq1 = model.ProfileMeasurementReq1;
                        objPRO045.ProfileMeasurementReq2 = model.ProfileMeasurementReq2;
                        objPRO045.ProfileMeasurementReq3 = model.ProfileMeasurementReq3;
                        objPRO045.ProfileMeasurementReq4 = model.ProfileMeasurementReq4;
                        objPRO045.ProfileMeasurementReq5 = model.ProfileMeasurementReq5;
                        objPRO045.ProfileMeasurementReq6 = model.ProfileMeasurementReq6;
                        objPRO045.ProfileMeasurementReq7 = model.ProfileMeasurementReq7;
                        objPRO045.ProfileMeasurementReq8 = model.ProfileMeasurementReq8;
                        objPRO045.ProfileMeasurementTolerance1 = model.ProfileMeasurementTolerance1;
                        objPRO045.ProfileMeasurementTolerance2 = model.ProfileMeasurementTolerance2;
                        objPRO045.ProfileMeasurementTolerance3 = model.ProfileMeasurementTolerance3;
                        objPRO045.ProfileMeasurementTolerance4 = model.ProfileMeasurementTolerance4;
                        objPRO045.ProfileMeasurementTolerance5 = model.ProfileMeasurementTolerance5;
                        objPRO045.ProfileMeasurementTolerance6 = model.ProfileMeasurementTolerance6;
                        objPRO045.ProfileMeasurementTolerance7 = model.ProfileMeasurementTolerance7;
                        objPRO045.ProfileMeasurementTolerance8 = model.ProfileMeasurementTolerance8;
                        objPRO045.ProfileMeasurementActual1 = model.ProfileMeasurementActual1;
                        objPRO045.ProfileMeasurementActual2 = model.ProfileMeasurementActual2;
                        objPRO045.ProfileMeasurementActual3 = model.ProfileMeasurementActual3;
                        objPRO045.ProfileMeasurementActual4 = model.ProfileMeasurementActual4;
                        objPRO045.ProfileMeasurementActual5 = model.ProfileMeasurementActual5;
                        objPRO045.ProfileMeasurementActual6 = model.ProfileMeasurementActual6;
                        objPRO045.ProfileMeasurementActual7 = model.ProfileMeasurementActual7;
                        objPRO045.ProfileMeasurementActual8 = model.ProfileMeasurementActual8;
                        objPRO045.CircumferenceReq1 = model.CircumferenceReq1;
                        objPRO045.CircumferenceReq2 = model.CircumferenceReq2;
                        objPRO045.CircumferenceReq3 = model.CircumferenceReq3;
                        objPRO045.CircumferenceReq4 = model.CircumferenceReq4;
                        objPRO045.CircumferenceReq5 = model.CircumferenceReq5;
                        objPRO045.CircumferenceTolerance1 = model.CircumferenceTolerance1;
                        objPRO045.CircumferenceTolerance2 = model.CircumferenceTolerance2;
                        objPRO045.CircumferenceTolerance3 = model.CircumferenceTolerance3;
                        objPRO045.CircumferenceTolerance4 = model.CircumferenceTolerance4;
                        objPRO045.CircumferenceTolerance5 = model.CircumferenceTolerance5;
                        objPRO045.CircumferenceActual1 = model.CircumferenceActual1;
                        objPRO045.CircumferenceActual2 = model.CircumferenceActual2;
                        objPRO045.CircumferenceActual3 = model.CircumferenceActual3;
                        objPRO045.CircumferenceActual4 = model.CircumferenceActual4;
                        objPRO045.CircumferenceActual5 = model.CircumferenceActual5;
                        objPRO045.OrientationReq1 = model.OrientationReq1;
                        objPRO045.OrientationReq2 = model.OrientationReq2;
                        objPRO045.OrientationReq3 = model.OrientationReq3;
                        objPRO045.OrientationReq4 = model.OrientationReq4;
                        objPRO045.OrientationAct11 = model.OrientationAct11;
                        objPRO045.OrientationAct12 = model.OrientationAct12;
                        objPRO045.OrientationAct13 = model.OrientationAct13;
                        objPRO045.OrientationAct14 = model.OrientationAct14;
                        objPRO045.OrientationAct15 = model.OrientationAct15;
                        objPRO045.OrientationAct21 = model.OrientationAct21;
                        objPRO045.OrientationAct22 = model.OrientationAct22;
                        objPRO045.OrientationAct23 = model.OrientationAct23;
                        objPRO045.OrientationAct24 = model.OrientationAct24;
                        objPRO045.OrientationAct25 = model.OrientationAct25;
                        objPRO045.OrientationRemark1 = model.OrientationRemark1;
                        objPRO045.OrientationRemark2 = model.OrientationRemark2;
                        objPRO045.OrientationRemark3 = model.OrientationRemark3;
                        objPRO045.OrientationRemark4 = model.OrientationRemark4;
                        objPRO045.OrientationRemark5 = model.OrientationRemark5;
                        objPRO045.Note1 = model.Note1;
                        objPRO045.Note2 = model.Note2;
                        objPRO045.Note3 = model.Note3;
                        objPRO045.Note4 = model.Note4;
                        objPRO045.Note5 = model.Note5;
                        objPRO045.Note6 = model.Note6;
                        objPRO045.Note8 = model.Note8;
                        objPRO045.Note9 = model.Note9;
                        objPRO045.EditedBy = objClsLoginInfo.UserName;
                        objPRO045.EditedOn = DateTime.Now;

                        objPRO045.Note1_1 = model.Note1_1;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.ProtocolSaved.ToString();
                        objResponseMsg.HeaderId = objPRO045.HeaderId;
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO045 objPRO045 = db.PRO045.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO045 != null && string.IsNullOrWhiteSpace(objPRO045.ProtocolNo))
                    {
                        db.PRO045.Remove(objPRO045);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.SETUP_AND_DIMENSION_REPORT_FOR_FORMED_ELBOW_LONG_SEAMS.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO045.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Linkage
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL045 objPRL045 = new PRL045();

            objPRL045 = db.PRL045.Where(i => i.HeaderId == id).FirstOrDefault();

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = "SET-UP & DIMENSION REPORT FOR FORMED ELBOW LONG. SEAMS";

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            List<string> lstProtocolRT = clsImplementationEnum.getProtocolRT().ToList();
            ViewBag.ProtocolRT = lstProtocolRT.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            List<string> lstProtocolUT = clsImplementationEnum.getProtocolRT().ToList();
            ViewBag.ProtocolUT = lstProtocolUT.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            ViewBag.ProtocolTOFDUT = new List<CategoryData> { new CategoryData { Value = "100%", Code = "100%", CategoryDescription = "100%" }, new CategoryData { Value = "NA", Code = "NA", CategoryDescription = "NA" } };

            if (objPRL045 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL045.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objQproject.ManufacturingCode;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }

            if (m == 1)
            {
                ViewBag.isEditable = "true";
            }
            else
            { ViewBag.isEditable = "false"; }

            return View(objPRL045);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL045 model)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = model.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL045 objPRL045 = db.PRL045.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL045.HeaderId = model.HeaderId;
                    objPRL045.ProtocolNo = model.ProtocolNo != null ? model.ProtocolNo : "" ;
                    objPRL045.DCRNo = model.DCRNo;
                    objPRL045.TypeOfForming = model.TypeOfForming;
                    objPRL045.IdentificationMarking = model.IdentificationMarking;
                    objPRL045.PtcRequired = model.PtcRequired;
                    objPRL045.ItNo = model.ItNo;
                    objPRL045.HeatNo = model.HeatNo;
                    objPRL045.WepUt = model.WepUt;
                    objPRL045.WepPt = model.WepPt;
                    objPRL045.WepMt = model.WepMt;
                    objPRL045.WepCust = model.WepCust;
                    objPRL045.SeamNoPunching = model.SeamNoPunching;
                    objPRL045.NdtRt = model.NdtRt;
                    objPRL045.NdtTofdUt = model.NdtTofdUt;
                    objPRL045.NdtUt = model.NdtUt;
                    objPRL045.WepAngleIs = model.WepAngleIs;
                    objPRL045.WepAngleOs = model.WepAngleOs;
                    objPRL045.TacksFreeFromCrack = model.TacksFreeFromCrack;
                    objPRL045.RootGapRequired = model.RootGapRequired;
                    objPRL045.RootGapActMax = model.RootGapActMax;
                    objPRL045.RootGapActMin = model.RootGapActMin;
                    objPRL045.RootFaceRequired1 = model.RootFaceRequired1;
                    objPRL045.RootFaceActMax1 = model.RootFaceActMax1;
                    objPRL045.RootFaceActMin1 = model.RootFaceActMin1;
                    objPRL045.RootFaceRequired2 = model.RootFaceRequired2;
                    objPRL045.RootFaceActMax2 = model.RootFaceActMax2;
                    objPRL045.RootFaceActMin2 = model.RootFaceActMin2;
                    objPRL045.ThicknessReqired = model.ThicknessReqired;
                    objPRL045.ThicknessActual1 = model.ThicknessActual1;
                    objPRL045.ThicknessActual2 = model.ThicknessActual2;
                    objPRL045.ThicknessActual3 = model.ThicknessActual3;
                    objPRL045.ThicknessActual4 = model.ThicknessActual4;
                    objPRL045.ThicknessActual5 = model.ThicknessActual5;
                    objPRL045.ThicknessActual6 = model.ThicknessActual6;
                    objPRL045.ThicknessActual7 = model.ThicknessActual7;
                    objPRL045.ThicknessActual8 = model.ThicknessActual8;
                    objPRL045.ThicknessActual9 = model.ThicknessActual9;
                    objPRL045.OffsetTolerance = model.OffsetTolerance;
                    objPRL045.OffsetActual1 = model.OffsetActual1;
                    objPRL045.OffsetActual2 = model.OffsetActual2;
                    objPRL045.OffsetActual3 = model.OffsetActual3;
                    objPRL045.ProfileMeasurement = model.ProfileMeasurement;
                    objPRL045.SeamNo1 = model.SeamNo1;
                    objPRL045.SeamNo2 = model.SeamNo2;
                    objPRL045.FirstEndPeakIn1 = model.FirstEndPeakIn1;
                    objPRL045.FirstEndPeakIn2 = model.FirstEndPeakIn2;
                    objPRL045.FirstEndPeakOut1 = model.FirstEndPeakOut1;
                    objPRL045.FirstEndPeakOut2 = model.FirstEndPeakOut2;
                    objPRL045.MidPeakIn1 = model.MidPeakIn1;
                    objPRL045.MidPeakIn2 = model.MidPeakIn2;
                    objPRL045.MidPeakOut1 = model.MidPeakOut1;
                    objPRL045.MidPeakOut2 = model.MidPeakOut2;
                    objPRL045.SecondEndPeakIn1 = model.SecondEndPeakIn1;
                    objPRL045.SecondEndPeakIn2 = model.SecondEndPeakIn2;
                    objPRL045.SecondEndPeakOut1 = model.SecondEndPeakOut1;
                    objPRL045.SecondEndPeakOut2 = model.SecondEndPeakOut2;
                    objPRL045.Remark1 = model.Remark1;
                    objPRL045.Remark2 = model.Remark2;
                    objPRL045.ProfileMeasurementReq1 = model.ProfileMeasurementReq1;
                    objPRL045.ProfileMeasurementReq2 = model.ProfileMeasurementReq2;
                    objPRL045.ProfileMeasurementReq3 = model.ProfileMeasurementReq3;
                    objPRL045.ProfileMeasurementReq4 = model.ProfileMeasurementReq4;
                    objPRL045.ProfileMeasurementReq5 = model.ProfileMeasurementReq5;
                    objPRL045.ProfileMeasurementReq6 = model.ProfileMeasurementReq6;
                    objPRL045.ProfileMeasurementReq7 = model.ProfileMeasurementReq7;
                    objPRL045.ProfileMeasurementReq8 = model.ProfileMeasurementReq8;
                    objPRL045.ProfileMeasurementTolerance1 = model.ProfileMeasurementTolerance1;
                    objPRL045.ProfileMeasurementTolerance2 = model.ProfileMeasurementTolerance2;
                    objPRL045.ProfileMeasurementTolerance3 = model.ProfileMeasurementTolerance3;
                    objPRL045.ProfileMeasurementTolerance4 = model.ProfileMeasurementTolerance4;
                    objPRL045.ProfileMeasurementTolerance5 = model.ProfileMeasurementTolerance5;
                    objPRL045.ProfileMeasurementTolerance6 = model.ProfileMeasurementTolerance6;
                    objPRL045.ProfileMeasurementTolerance7 = model.ProfileMeasurementTolerance7;
                    objPRL045.ProfileMeasurementTolerance8 = model.ProfileMeasurementTolerance8;
                    objPRL045.ProfileMeasurementActual1 = model.ProfileMeasurementActual1;
                    objPRL045.ProfileMeasurementActual2 = model.ProfileMeasurementActual2;
                    objPRL045.ProfileMeasurementActual3 = model.ProfileMeasurementActual3;
                    objPRL045.ProfileMeasurementActual4 = model.ProfileMeasurementActual4;
                    objPRL045.ProfileMeasurementActual5 = model.ProfileMeasurementActual5;
                    objPRL045.ProfileMeasurementActual6 = model.ProfileMeasurementActual6;
                    objPRL045.ProfileMeasurementActual7 = model.ProfileMeasurementActual7;
                    objPRL045.ProfileMeasurementActual8 = model.ProfileMeasurementActual8;
                    objPRL045.CircumferenceReq1 = model.CircumferenceReq1;
                    objPRL045.CircumferenceReq2 = model.CircumferenceReq2;
                    objPRL045.CircumferenceReq3 = model.CircumferenceReq3;
                    objPRL045.CircumferenceReq4 = model.CircumferenceReq4;
                    objPRL045.CircumferenceReq5 = model.CircumferenceReq5;
                    objPRL045.CircumferenceTolerance1 = model.CircumferenceTolerance1;
                    objPRL045.CircumferenceTolerance2 = model.CircumferenceTolerance2;
                    objPRL045.CircumferenceTolerance3 = model.CircumferenceTolerance3;
                    objPRL045.CircumferenceTolerance4 = model.CircumferenceTolerance4;
                    objPRL045.CircumferenceTolerance5 = model.CircumferenceTolerance5;
                    objPRL045.CircumferenceActual1 = model.CircumferenceActual1;
                    objPRL045.CircumferenceActual2 = model.CircumferenceActual2;
                    objPRL045.CircumferenceActual3 = model.CircumferenceActual3;
                    objPRL045.CircumferenceActual4 = model.CircumferenceActual4;
                    objPRL045.CircumferenceActual5 = model.CircumferenceActual5;
                    objPRL045.OrientationReq1 = model.OrientationReq1;
                    objPRL045.OrientationReq2 = model.OrientationReq2;
                    objPRL045.OrientationReq3 = model.OrientationReq3;
                    objPRL045.OrientationReq4 = model.OrientationReq4;
                    objPRL045.OrientationAct11 = model.OrientationAct11;
                    objPRL045.OrientationAct12 = model.OrientationAct12;
                    objPRL045.OrientationAct13 = model.OrientationAct13;
                    objPRL045.OrientationAct14 = model.OrientationAct14;
                    objPRL045.OrientationAct15 = model.OrientationAct15;
                    objPRL045.OrientationAct21 = model.OrientationAct21;
                    objPRL045.OrientationAct22 = model.OrientationAct22;
                    objPRL045.OrientationAct23 = model.OrientationAct23;
                    objPRL045.OrientationAct24 = model.OrientationAct24;
                    objPRL045.OrientationAct25 = model.OrientationAct25;
                    objPRL045.OrientationRemark1 = model.OrientationRemark1;
                    objPRL045.OrientationRemark2 = model.OrientationRemark2;
                    objPRL045.OrientationRemark3 = model.OrientationRemark3;
                    objPRL045.OrientationRemark4 = model.OrientationRemark4;
                    objPRL045.OrientationRemark5 = model.OrientationRemark5;
                    objPRL045.Note1 = model.Note1;
                    objPRL045.Note1_1 = model.Note1_1;
                    objPRL045.Note2 = model.Note2;
                    objPRL045.Note3 = model.Note3;
                    objPRL045.Note4 = model.Note4;
                    objPRL045.Note5 = model.Note5;
                    objPRL045.Note6 = model.Note6;
                    objPRL045.Note8 = model.Note8;
                    objPRL045.Note9 = model.Note9;
                    objPRL045.EditedBy = objClsLoginInfo.UserName;
                    objPRL045.EditedOn = DateTime.Now;

                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.ProtocolSaved.ToString();
                    objResponseMsg.HeaderId = objPRL045.HeaderId;
                    #endregion
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}