﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol025Controller : clsBase
    {
        string ControllerURL = "/PROTOCOL/Protocol025/";
        string Title = "PNEUMATIC TEST REPORT ";
        // GET: PROTOCOL/Protocol025
        #region Details View Code
        #region Header
        [SessionExpireFilter] //, UserPermissions, AllowAnonymous]

        public ActionResult Details(int? id,int? m)
        {
            PRO120 objPRO120 = new PRO120();
            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            List<string> lstYesNoNA = clsImplementationEnum.getYesNoNA().ToList();
            ViewBag.YesNoNAEnum = lstYesNoNA.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            if (!id.HasValue)
            {
                objPRO120.ProtocolNo = string.Empty;
                objPRO120.CreatedBy = objClsLoginInfo.UserName;
                objPRO120.CreatedOn = DateTime.Now;
                db.PRO120.Add(objPRO120);
                db.SaveChanges();
                ViewBag.HeaderId = objPRO120.HeaderId;
            }
            else
            {
                objPRO120 = db.PRO120.Where(x => x.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }

            return View(objPRO120);
        }
        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO120 PRO120)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO120.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO120 objPRO120 = db.PRO120.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO120.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO120.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data
                        objPRO120.ProtocolNo = PRO120.ProtocolNo;
                        objPRO120.InspectionCertificateNo = PRO120.InspectionCertificateNo;
                        objPRO120.EquipmentNo = PRO120.EquipmentNo;
                        objPRO120.InspectionAgency = PRO120.InspectionAgency;
                        objPRO120.DrawingNo = PRO120.DrawingNo;
                        objPRO120.ProcedureNo = PRO120.ProcedureNo;
                        objPRO120.TestDescription = PRO120.TestDescription;
                        objPRO120.PneumaticTestDate = PRO120.PneumaticTestDate;
                        objPRO120.TestPosition = PRO120.TestPosition;
                        objPRO120.TestPressureRequired = PRO120.TestPressureRequired;
                        objPRO120.TestPressureActual = PRO120.TestPressureActual;
                        objPRO120.HoldingTimeRequired = PRO120.HoldingTimeRequired;
                        objPRO120.HoldingTimeActual = PRO120.HoldingTimeActual;
                        objPRO120.TemperatureIfRequired = PRO120.TemperatureIfRequired;
                        objPRO120.TemperatureActual = PRO120.TemperatureActual;
                        objPRO120.GraphAttached = PRO120.GraphAttached;
                        objPRO120.Remarks = PRO120.Remarks;


                        objPRO120.EditedBy = objClsLoginInfo.UserName;
                        objPRO120.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol saved successfully";
                        objResponseMsg.HeaderId = objPRO120.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO120 objPRO120 = db.PRO120.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO120 != null && string.IsNullOrWhiteSpace(objPRO120.ProtocolNo))
                    {
                        db.PRO121.RemoveRange(objPRO120.PRO121.ToList());


                        db.PRO120.Remove(objPRO120);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.PNEUMATIC_TEST_REPORT.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO120.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public ActionResult GetProtocolLines1Data(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro.GaugeNo like '%" + param.sSearch
                                    + "%' or pro.CalibrationUpto like '%" + param.sSearch
                        + "%')";
                }

                var lstResult = db.SP_IPI_PROTOCOL025_GET_LINES1_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                                      "1",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "GaugeNo", "", "", false, "", false, "200","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "CalibrationUpto", "", "", false, "", false, "","QC"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine1(0);")
                                };
                var data = (from uc in lstResult
                            select new[]
                            {
                                "1",
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.LineId),
                                Helper.GenerateHTMLTextbox(uc.LineId, "GaugeNo", Convert.ToString(uc.GaugeNo), "", true, "", false, "200","QC"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "CalibrationUpto", uc.CalibrationUpto==null || uc.CalibrationUpto.Value==DateTime.MinValue ? "": Convert.ToDateTime(uc.CalibrationUpto).ToString("yyyy-MM-dd"), "", true, "", false,"","QC"),
                                Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == "QC3" ? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine1','tblProtocolLines1')"):"")

                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }
        public ActionResult SaveProtocolLine1(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRO121 objPRO121 = new PRO121();

            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;

                if (refHeaderId > 0)
                {
                    string strGaugeNo = !string.IsNullOrEmpty(fc["GaugeNo" + refLineId]) ? Convert.ToString(fc["GaugeNo" + refLineId]).Trim() : "";
                    DateTime dateCalibrationUpto = !string.IsNullOrEmpty(fc["CalibrationUpto" + refLineId]) ? Convert.ToDateTime(fc["CalibrationUpto" + refLineId].ToString()).Date : DateTime.MinValue;

                    #region Add New Spot Line1
                    if (refLineId > 0)
                    {
                        objPRO121 = db.PRO121.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRO121.EditedBy = objClsLoginInfo.UserName;
                        objPRO121.EditedOn = DateTime.Now;
                    }
                    objPRO121.HeaderId = refHeaderId;
                    objPRO121.GaugeNo = strGaugeNo;
                    if (!string.IsNullOrEmpty(fc["CalibrationUpto" + refLineId]) && dateCalibrationUpto != DateTime.MinValue)
                    {
                        objPRO121.CalibrationUpto = dateCalibrationUpto;
                    }
                    else
                    {
                        objPRO121.CalibrationUpto = null;
                    }
                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = "Data updated successfully";
                    }
                    else
                    {
                        objPRO121.CreatedBy = objClsLoginInfo.UserName;
                        objPRO121.CreatedOn = DateTime.Now;
                        db.PRO121.Add(objPRO121);
                        db.SaveChanges();
                        objResponseMsg.Value = "Data added successfully";
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRO121.LineId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeleteProtocolLine1(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRO121 objPRO121 = db.PRO121.FirstOrDefault(c => c.LineId == lineId);
                if (objPRO121 != null)
                {
                    db.PRO121.Remove(objPRO121);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #endregion

        #region Linkage View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL120 objPRL120 = new PRL120();
            objPRL120 = db.PRL120.Where(i => i.HeaderId == id).FirstOrDefault();

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;
            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            List<string> lstYesNoNA = clsImplementationEnum.getYesNoNA().ToList();
            ViewBag.YesNoNAEnum = lstYesNoNA.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            if (objPRL120 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL120.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objQproject.ManufacturingCode;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }
            if (m == 1)
            {
                ViewBag.isEditable = "true";
            }
            else
            { ViewBag.isEditable = "false"; }

            return View(objPRL120);
        }
        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL120 PRL120)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRL120.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL120 objPRL120 = db.PRL120.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    #region Save Data
                    objPRL120.ProtocolNo = PRL120.ProtocolNo;
                    objPRL120.InspectionCertificateNo = PRL120.InspectionCertificateNo;
                    objPRL120.EquipmentNo = PRL120.EquipmentNo;
                    objPRL120.InspectionAgency = PRL120.InspectionAgency;
                    objPRL120.DrawingNo = PRL120.DrawingNo;
                    objPRL120.ProcedureNo = PRL120.ProcedureNo;
                    objPRL120.TestDescription = PRL120.TestDescription;
                    objPRL120.PneumaticTestDate = PRL120.PneumaticTestDate;
                    objPRL120.TestPosition = PRL120.TestPosition;
                    objPRL120.TestPressureRequired = PRL120.TestPressureRequired;
                    objPRL120.TestPressureActual = PRL120.TestPressureActual;
                    objPRL120.HoldingTimeRequired = PRL120.HoldingTimeRequired;
                    objPRL120.HoldingTimeActual = PRL120.HoldingTimeActual;
                    objPRL120.TemperatureIfRequired = PRL120.TemperatureIfRequired;
                    objPRL120.TemperatureActual = PRL120.TemperatureActual;
                    objPRL120.GraphAttached = PRL120.GraphAttached;
                    objPRL120.Remarks = PRL120.Remarks;


                    objPRL120.EditedBy = objClsLoginInfo.UserName;
                    objPRL120.EditedOn = DateTime.Now;
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol saved successfully";
                    objResponseMsg.HeaderId = objPRL120.HeaderId;
                    #endregion

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL120 objPRL120 = db.PRL120.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL120 != null && string.IsNullOrWhiteSpace(objPRL120.ProtocolNo))
                    {
                        db.PRL121.RemoveRange(objPRL120.PRL121.ToList());
                        db.PRL120.Remove(objPRL120);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL120.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #region Line1 Linkage
        [HttpPost]
        public ActionResult GetProtocolLines1DataLinkage(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro.GaugeNo like '%" + param.sSearch
                        + "%' or pro.CalibrationUpto like '%" + param.sSearch
                    + "%')";
                }

                var lstResult = db.SP_IPI_PROTOCOL025_LINKAGE_GET_LINES1_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                                    "1",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "GaugeNo", "", "", false, "", false, "200","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "CalibrationUpto", "", "", false, "", false,"", "QC"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine1(0);")
                                };
                var data = (from uc in lstResult
                            select new[]
                            {
                                "1",
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                                Helper.GenerateHTMLTextbox(uc.LineId, "GaugeNo", Convert.ToString(uc.GaugeNo), "", true, "", false, "200","QC"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "CalibrationUpto",uc.CalibrationUpto==null || uc.CalibrationUpto.Value==DateTime.MinValue ? "":  Convert.ToDateTime(uc.CalibrationUpto).ToString("yyyy-MM-dd"), "", true, "", false,"","QC"),
                                Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == "QC3" ? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine1Linkage','tblProtocolLines1')"):"")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult SaveProtocolLine1Linkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRL121 objPRL121 = new PRL121();

            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
                if (refHeaderId > 0)
                {
                    string strGaugeNo = !string.IsNullOrEmpty(fc["GaugeNo" + refLineId]) ? Convert.ToString(fc["GaugeNo" + refLineId]).Trim() : "";
                    DateTime dateCalibrationUpto = !string.IsNullOrEmpty(fc["CalibrationUpto" + refLineId]) ? Convert.ToDateTime(fc["CalibrationUpto" + refLineId].ToString()).Date : DateTime.MinValue;

                    
                    #region Add New Spot Line1
                    if (refLineId > 0)
                    {
                        objPRL121 = db.PRL121.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRL121.EditedBy = objClsLoginInfo.UserName;
                        objPRL121.EditedOn = DateTime.Now;
                    }
                    objPRL121.HeaderId = refHeaderId;
                    objPRL121.GaugeNo = strGaugeNo;
                    if (!string.IsNullOrEmpty(fc["CalibrationUpto" + refLineId]))
                    {
                        objPRL121.CalibrationUpto = dateCalibrationUpto;
                    }
                    else
                    {
                        objPRL121.CalibrationUpto = null;
                    }
                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = "Data updated successfully";
                    }
                    else
                    {
                        objPRL121.CreatedBy = objClsLoginInfo.UserName;
                        objPRL121.CreatedOn = DateTime.Now;
                        db.PRL121.Add(objPRL121);
                        db.SaveChanges();
                        objResponseMsg.Value = "Data added successfully";
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRL121.LineId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolLine1Linkage(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRL121 objPRL121 = db.PRL121.FirstOrDefault(c => c.LineId == lineId);
                if (objPRL121 != null)
                {
                    db.PRL121.Remove(objPRL121);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #endregion
    }
}
