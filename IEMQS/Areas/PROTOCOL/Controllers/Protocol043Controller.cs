﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol043Controller : clsBase
    {
        // GET: PROTOCOL/Protocol043

        string ControllerURL = "/PROTOCOL/Protocol043/";
        string Title = "VISUAL & DIMENSION REPORT FOR ROLLED SHELL LONGITUDINAL SEAM";
        //IEMQS.DrawingReference.hedProjectDocListWebServiceClient drawref = new IEMQS.DrawingReference.hedProjectDocListWebServiceClient();

        #region Details View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO210 objPRO210 = new PRO210();
            if (!id.HasValue)
            {
                try
                {
                    objPRO210.ProtocolNo = string.Empty;
                    objPRO210.CreatedBy = objClsLoginInfo.UserName;
                    objPRO210.CreatedOn = DateTime.Now;

                    #region OUT OF ROUNDNESS
                    objPRO210.PRO211.Add(new PRO211
                    {
                        Orientation = "0°-180° ",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO210.PRO211.Add(new PRO211
                    {
                        Orientation = "22.5°-205.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO210.PRO211.Add(new PRO211
                    {
                        Orientation = "45°-225°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO210.PRO211.Add(new PRO211
                    {
                        Orientation = "67.5°-247.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO210.PRO211.Add(new PRO211
                    {
                        Orientation = "90°-270°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO210.PRO211.Add(new PRO211
                    {
                        Orientation = "112.5°-292.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO210.PRO211.Add(new PRO211
                    {
                        Orientation = "135°-315°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO210.PRO211.Add(new PRO211
                    {
                        Orientation = "157.5°-337.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion

                    #region CIRCUMFERENCE MEASUREMENT
                    objPRO210.PRO212.Add(new PRO212
                    {
                        Location = "TOP",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO210.PRO212.Add(new PRO212
                    {
                        Location = "MIDDLE",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO210.PRO212.Add(new PRO212
                    {
                        Location = "BOTTOM",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion

                    #region TOTAL SHELL HEIGHT
                    objPRO210.PRO213.Add(new PRO213
                    {
                        RequiredValue = "AT 0°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO210.PRO213.Add(new PRO213
                    {
                        RequiredValue = "AT 90°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO210.PRO213.Add(new PRO213
                    {
                        RequiredValue = "AT 180°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO210.PRO213.Add(new PRO213
                    {
                        RequiredValue = "AT 270°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion

                    db.PRO210.Add(objPRO210);
                    db.SaveChanges();
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO210.HeaderId;
            }
            else
            {
                objPRO210 = db.PRO210.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            List<string> lsttypesofforming = new List<string> { "HOT", "COLD", "WARM" };
            ViewBag.TypesOfForming = lsttypesofforming.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            ViewBag.OutsideOrInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO211> lstPRO211 = db.PRO211.Where(x => x.HeaderId == objPRO210.HeaderId).ToList();
            List<PRO212> lstPRO212 = db.PRO212.Where(x => x.HeaderId == objPRO210.HeaderId).ToList();
            List<PRO213> lstPRO213 = db.PRO213.Where(x => x.HeaderId == objPRO210.HeaderId).ToList();
            List<PRO214> lstPRO214 = db.PRO214.Where(x => x.HeaderId == objPRO210.HeaderId).ToList();
            List<SP_IPI_PROTOCOL043_GET_LINES5_DATA_Result> lstPRO214_2 = db.SP_IPI_PROTOCOL043_GET_LINES5_DATA(1, int.MaxValue, "", " HeaderId = " + objPRO210.HeaderId).ToList();

            ViewBag.lstPRO211 = lstPRO211;
            ViewBag.lstPRO212 = lstPRO212;
            ViewBag.lstPRO213 = lstPRO213;
            ViewBag.lstPRO214 = lstPRO214;
            ViewBag.lstPRO214_2 = lstPRO214_2;
            #endregion

            return View(objPRO210);
        }
        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }
        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO210 PRO210)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO210.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO210 objPRO210 = db.PRO210.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO210.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO210.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data

                        objPRO210.ProtocolNo = PRO210.ProtocolNo;
                        objPRO210.ReqOutOfRoundness = PRO210.ReqOutOfRoundness;
                        objPRO210.OutByRoundness = PRO210.OutByRoundness;
                        objPRO210.OutByActAtTop = PRO210.OutByActAtTop;
                        objPRO210.OutByActAtBottom = PRO210.OutByActAtBottom;
                        objPRO210.ReqIdCF = PRO210.ReqIdCF;
                        objPRO210.ReqOdCF = PRO210.ReqOdCF;
                        objPRO210.ReqTotalShellHeight = PRO210.ReqTotalShellHeight;
                        objPRO210.PeakInOutAllowed = PRO210.PeakInOutAllowed;
                        objPRO210.IsEvalueTemplate = PRO210.IsEvalueTemplate;
                        objPRO210.ReqChrodLength = PRO210.ReqChrodLength;
                        objPRO210.ActChordLength = PRO210.ActChordLength;
                        objPRO210.ReqRadius = PRO210.ReqRadius;
                        objPRO210.ActRadius = PRO210.ActRadius;
                        objPRO210.AllowableGap = PRO210.AllowableGap;
                        objPRO210.ActGap = PRO210.ActGap;
                        objPRO210.CheckPoint1 = PRO210.CheckPoint1;
                        objPRO210.CheckPoint2 = PRO210.CheckPoint2;
                        objPRO210.CheckPoint3 = PRO210.CheckPoint3;
                        objPRO210.CheckPoint5 = PRO210.CheckPoint5;
                        objPRO210.CheckPoint5_2 = PRO210.CheckPoint5_2;
                        objPRO210.CheckPoint6 = PRO210.CheckPoint6;
                        objPRO210.QCRemarks = PRO210.QCRemarks;
                        objPRO210.Result = PRO210.Result;
                        objPRO210.ReqCladStripingDetailsWidth = PRO210.ReqCladStripingDetailsWidth;
                        objPRO210.ReqCladStripingDetailsDepth = PRO210.ReqCladStripingDetailsDepth;
                        objPRO210.ReqCladStripingDetailsOffset = PRO210.ReqCladStripingDetailsOffset;
                        objPRO210.Note1 = PRO210.Note1;
                        objPRO210.Note2 = PRO210.Note2;
                        objPRO210.Note3 = PRO210.Note3;
                        objPRO210.EditedBy = objClsLoginInfo.UserName;
                        objPRO210.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO210.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO210 objPRO210 = db.PRO210.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO210 != null && string.IsNullOrWhiteSpace(objPRO210.ProtocolNo))
                    {
                        db.PRO210.Remove(objPRO210);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO210.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO211> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO211> lstAddPRO211 = new List<PRO211>();
                List<PRO211> lstDeletePRO211 = new List<PRO211>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO211 obj = db.PRO211.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.Orientation = item.Orientation;
                                obj.ActAtTop = item.ActAtTop;
                                obj.ActAtBottom = item.ActAtBottom;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO211 obj = new PRO211();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.Orientation))
                            {
                                obj.Orientation = item.Orientation;
                                obj.ActAtTop = item.ActAtTop;
                                obj.ActAtBottom = item.ActAtBottom;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO211.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO211.Count > 0)
                    {
                        db.PRO211.AddRange(lstAddPRO211);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO211 = db.PRO211.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO211.Count > 0)
                    {
                        db.PRO211.RemoveRange(lstDeletePRO211);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO211 = db.PRO211.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO211.Count > 0)
                    {
                        db.PRO211.RemoveRange(lstDeletePRO211);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2(List<PRO212> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO212> lstAddPRO212 = new List<PRO212>();
                List<PRO212> lstDeletePRO212 = new List<PRO212>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO212 obj = db.PRO212.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO212();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Location = item.Location;
                        obj.ActIDCF = item.ActIDCF;
                        obj.ActIODCF = item.ActIODCF;

                        if (isAdded)
                        {
                            lstAddPRO212.Add(obj);
                        }
                    }
                    if (lstAddPRO212.Count > 0)
                    {
                        db.PRO212.AddRange(lstAddPRO212);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO212 = db.PRO212.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO212.Count > 0)
                    {
                        db.PRO212.RemoveRange(lstDeletePRO212);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO212 = db.PRO212.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO212.Count > 0)
                    {
                        db.PRO212.RemoveRange(lstDeletePRO212);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3(List<PRO213> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO213> lstAddPRO213 = new List<PRO213>();
                List<PRO213> lstDeletePRO213 = new List<PRO213>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO213 obj = db.PRO213.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        else
                        {
                            obj = new PRO213();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }

                        obj.RequiredValue = item.RequiredValue;
                        obj.ActualValue = item.ActualValue;

                        if (isAdded)
                        {
                            lstAddPRO213.Add(obj);
                        }
                    }
                    if (lstAddPRO213.Count > 0)
                    {
                        db.PRO213.AddRange(lstAddPRO213);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO213 = db.PRO213.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO213.Count > 0)
                    {
                        db.PRO213.RemoveRange(lstDeletePRO213);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO213 = db.PRO213.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO213.Count > 0)
                    {
                        db.PRO213.RemoveRange(lstDeletePRO213);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line4 details
        [HttpPost]
        public JsonResult SaveProtocolLine4(List<PRO214> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO214> lstAddPRO214 = new List<PRO214>();
                List<PRO214> lstDeletePRO214 = new List<PRO214>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO214 obj = db.PRO214.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO214();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SeamNo = item.SeamNo;
                        obj.TopPeakIn = item.TopPeakIn;
                        obj.TopPeakOut = item.TopPeakOut;
                        obj.MidPeakIn = item.MidPeakIn;
                        obj.MidPeakOut = item.MidPeakOut;
                        obj.BottomPeakIn = item.BottomPeakIn;
                        obj.BottomPeakOut = item.BottomPeakOut;

                        if (isAdded)
                        {
                            lstAddPRO214.Add(obj);
                        }
                    }
                    if (lstAddPRO214.Count > 0)
                    {
                        db.PRO214.AddRange(lstAddPRO214);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO214 = db.PRO214.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO214.Count > 0)
                    {
                        db.PRO214.RemoveRange(lstDeletePRO214);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO214 = db.PRO214.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO214.Count > 0)
                    {
                        db.PRO214.RemoveRange(lstDeletePRO214);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line5 details

        [HttpPost]
        public JsonResult SaveProtocolLine5(List<PRO214_2> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO214_2> lstAddPRO214_2 = new List<PRO214_2>();
                List<PRO214_2> lstDeletePRO214_2 = new List<PRO214_2>();

                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO214_2 obj = db.PRO214_2.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        PRO214_2 objFecthSeam = new PRO214_2();
                        if (obj == null)
                        {
                            obj = new PRO214_2();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        objFecthSeam = db.PRO214_2.Where(x => x.SeamNo == item.SeamNo && x.HeaderId == item.HeaderId).FirstOrDefault();
                        if (objFecthSeam != null)
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                        }
                        else
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                        }
                        obj.Depth = item.Depth;
                        obj.Width = item.Width;
                        obj.Offset = item.Offset;
                        obj.SpotNo = item.SpotNo;
                        obj.SpotNoClad = item.SpotNoClad;
                        if (isAdded)
                        {
                            lstAddPRO214_2.Add(obj);
                        }
                    }
                }
                if (lstAddPRO214_2.Count > 0)
                {
                    db.PRO214_2.AddRange(lstAddPRO214_2);
                }
                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRO214_2 = db.PRO214_2.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRO214_2 = db.PRO214_2.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRO214_2.Count > 0)
                {
                    db.PRO214_2.RemoveRange(lstDeletePRO214_2);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

        #region Linkage View Code  
        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL210 objPRL210 = new PRL210();
            objPRL210 = db.PRL210.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL210 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL210.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }

            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            #region DIMENSION
            //objPRL195.RemOverCrowing = "TOL:1/1/4(1.25)%";
            //objPRL195.RemUnderCrowing = "TOL:5/8(0.625)%";
            #endregion

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            List<string> lsttypesofforming = new List<string> { "HOT", "COLD", "WARM" };
            ViewBag.TypesOfForming = lsttypesofforming.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideOrInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL211> lstPRL211 = db.PRL211.Where(x => x.HeaderId == objPRL210.HeaderId).ToList();
            List<PRL212> lstPRL212 = db.PRL212.Where(x => x.HeaderId == objPRL210.HeaderId).ToList();
            List<PRL213> lstPRL213 = db.PRL213.Where(x => x.HeaderId == objPRL210.HeaderId).ToList();
            List<PRL214> lstPRL214 = db.PRL214.Where(x => x.HeaderId == objPRL210.HeaderId).ToList();
            List<SP_IPI_PROTOCOL043_LINKAGE_GET_LINES5_DATA_Result> lstPRL214_2 = db.SP_IPI_PROTOCOL043_LINKAGE_GET_LINES5_DATA(1, int.MaxValue, "", " HeaderId = " + objPRL210.HeaderId).ToList();

            ViewBag.lstPRL211 = lstPRL211;
            ViewBag.lstPRL212 = lstPRL212;
            ViewBag.lstPRL213 = lstPRL213;
            ViewBag.lstPRL214 = lstPRL214;
            ViewBag.lstPRL214_2 = lstPRL214_2;
            #endregion

            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.DrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL210.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL210.ActFilledBy) && (objPRL210.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL210.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }

            return View(objPRL210);
        }

        public ActionResult LinkageMobile(int? id, int? m, string APIRequestFrom = "", string UserName = "")
        {
            string ProjectDesc = "";
            string ManufacturingCode = "";
            string AccessRole = "";
            bool isEditable = false;
            var DrawingNo = new List<string>();
            bool isQCRemarkEditable = false;

            PRL210 objPRL210 = new PRL210();

            db.Configuration.ProxyCreationEnabled = false;
            objPRL210 = db.PRL210.Where(i => i.HeaderId == id).FirstOrDefault();
            db.Entry(objPRL210).State = System.Data.Entity.EntityState.Detached;

            NDEModels objNDEModels = new NDEModels();
            if (objPRL210 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL210.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
            }
            
            if (m == 1)
            { isEditable = true; }
            else
            { isEditable = false; }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
            AccessRole = objUserRoleAccessDetails.UserDesignation;


            #region DIMENSION
            //objPRL195.RemOverCrowing = "TOL:1/1/4(1.25)%";
            //objPRL195.RemUnderCrowing = "TOL:5/8(0.625)%";
            #endregion


            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            var YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            List<string> lsttypesofforming = new List<string> { "HOT", "COLD", "WARM" };
            var TypesOfForming = lsttypesofforming.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var OutsideOrInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL211> lstPRL211 = db.PRL211.Where(x => x.HeaderId == objPRL210.HeaderId).ToList();
            List<PRL212> lstPRL212 = db.PRL212.Where(x => x.HeaderId == objPRL210.HeaderId).ToList();
            List<PRL213> lstPRL213 = db.PRL213.Where(x => x.HeaderId == objPRL210.HeaderId).ToList();
            List<PRL214> lstPRL214 = db.PRL214.Where(x => x.HeaderId == objPRL210.HeaderId).ToList();
            List<SP_IPI_PROTOCOL043_LINKAGE_GET_LINES5_DATA_Result> lstPRL214_2 = db.SP_IPI_PROTOCOL043_LINKAGE_GET_LINES5_DATA(1, int.MaxValue, "", " HeaderId = " + objPRL210.HeaderId).ToList();

            #endregion

            try
            {
                isEditable = false;
                isQCRemarkEditable = false;
                if (m == 1)
                {
                    isEditable = true;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL210.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL210.ActFilledBy) && (objPRL210.RequestNo.HasValue))
                        {
                            isEditable = false;
                            isQCRemarkEditable = true;
                        }
                        else
                        {
                            DrawingNo = (new GeneralController()).GetPLMDrawingNumber(objPRL210.Project).ToList();
                        }
                    }
                }
                else
                { isEditable = false; }

                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    ProjectDesc = ProjectDesc,
                    ManufacturingCode = ManufacturingCode,
                    isEditable = isEditable,
                    isQCRemarkEditable = isQCRemarkEditable,
                    DrawingNo = DrawingNo,
                    AccessRole = AccessRole,
                    OutsideInside = OutsideOrInside,
                    YesNoEnum = YesNoEnum,
                    YesNAEnum = YesNAEnum,
                    TypesOfForming = TypesOfForming,


                    objPRL210 = objPRL210,

                    lstPRL211 = lstPRL211,
                    lstPRL212 = lstPRL212,
                    lstPRL213 = lstPRL213,
                    lstPRL214 = lstPRL214,
                    lstPRL214_2 = lstPRL214_2

                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();

                return Json(new
                {
                    res = "0",
                    msg = ex.Message,
                }, JsonRequestBehavior.AllowGet);

            }

        }
        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL210 PRL210, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = PRL210.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL210 objPRL210 = db.PRL210.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    #region Save Data
                    objPRL210.ProtocolNo = PRL210.ProtocolNo;

                    objPRL210.DrawingRevisionNo = PRL210.DrawingRevisionNo;
                    objPRL210.DrawingNo = PRL210.DrawingNo;
                    objPRL210.DCRNo = PRL210.DCRNo;

                    objPRL210.ReqOutOfRoundness = PRL210.ReqOutOfRoundness;
                    objPRL210.OutByRoundness = PRL210.OutByRoundness;
                    objPRL210.OutByActAtTop = PRL210.OutByActAtTop;
                    objPRL210.OutByActAtBottom = PRL210.OutByActAtBottom;
                    objPRL210.ReqIdCF = PRL210.ReqIdCF;
                    objPRL210.ReqOdCF = PRL210.ReqOdCF;
                    objPRL210.ReqTotalShellHeight = PRL210.ReqTotalShellHeight;
                    objPRL210.PeakInOutAllowed = PRL210.PeakInOutAllowed;
                    objPRL210.IsEvalueTemplate = PRL210.IsEvalueTemplate;
                    objPRL210.ReqChrodLength = PRL210.ReqChrodLength;
                    objPRL210.ActChordLength = PRL210.ActChordLength;
                    objPRL210.ReqRadius = PRL210.ReqRadius;
                    objPRL210.ActRadius = PRL210.ActRadius;
                    objPRL210.AllowableGap = PRL210.AllowableGap;
                    objPRL210.ActGap = PRL210.ActGap;
                    objPRL210.CheckPoint1 = PRL210.CheckPoint1;
                    objPRL210.CheckPoint2 = PRL210.CheckPoint2;
                    objPRL210.CheckPoint3 = PRL210.CheckPoint3;
                    objPRL210.CheckPoint5 = PRL210.CheckPoint5;
                    objPRL210.CheckPoint5_2 = PRL210.CheckPoint5_2;
                    objPRL210.CheckPoint6 = PRL210.CheckPoint6;
                    objPRL210.QCRemarks = PRL210.QCRemarks;
                    objPRL210.Result = PRL210.Result;
                    objPRL210.ReqCladStripingDetailsWidth = PRL210.ReqCladStripingDetailsWidth;
                    objPRL210.ReqCladStripingDetailsDepth = PRL210.ReqCladStripingDetailsDepth;
                    objPRL210.ReqCladStripingDetailsOffset = PRL210.ReqCladStripingDetailsOffset;
                    objPRL210.Note1 = PRL210.Note1;
                    objPRL210.Note2 = PRL210.Note2;
                    objPRL210.Note3 = PRL210.Note3;
                    objPRL210.EditedBy = UserName;
                    objPRL210.EditedOn = DateTime.Now;

                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL210.ActFilledBy = UserName;
                            objPRL210.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL210.ReqFilledBy = UserName;
                            objPRL210.ReqFilledOn = DateTime.Now;
                        }
                    }

                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL210.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL210 objPRL210 = db.PRL210.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL210 != null && string.IsNullOrWhiteSpace(objPRL210.ProtocolNo))
                    {
                        db.PRL210.Remove(objPRL210);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL210.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL211> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL211> lstAddPRL211 = new List<PRL211>();
                List<PRL211> lstDeletePRL211 = new List<PRL211>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRL211 obj = db.PRL211.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.Orientation = item.Orientation;
                                obj.ActAtTop = item.ActAtTop;
                                obj.ActAtBottom = item.ActAtBottom;
                                obj.EditedBy = UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRL211 obj = new PRL211();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.Orientation))
                            {
                                obj.Orientation = item.Orientation;
                                obj.ActAtTop = item.ActAtTop;
                                obj.ActAtBottom = item.ActAtBottom;
                                obj.CreatedBy = UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRL211.Add(obj);
                            }
                        }
                    }
                }
                if (lstAddPRL211.Count > 0)
                {
                    db.PRL211.AddRange(lstAddPRL211);
                }


                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();
                    lstDeletePRL211 = db.PRL211.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                }
                else
                {
                    lstDeletePRL211 = db.PRL211.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL211.Count > 0)
                {
                    db.PRL211.RemoveRange(lstDeletePRL211);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2Linkage(List<PRL212> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL212> lstAddPRL212 = new List<PRL212>();
                List<PRL212> lstDeletePRL212 = new List<PRL212>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL212 obj = db.PRL212.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL212();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Location = item.Location;
                        obj.ActIDCF = item.ActIDCF;
                        obj.ActIODCF = item.ActIODCF;

                        if (isAdded)
                        {
                            lstAddPRL212.Add(obj);
                        }
                    }
                }
                if (lstAddPRL212.Count > 0)
                {
                    db.PRL212.AddRange(lstAddPRL212);
                }

                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL212 = db.PRL212.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL212 = db.PRL212.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL212.Count > 0)
                {
                    db.PRL212.RemoveRange(lstDeletePRL212);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3Linkage(List<PRL213> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL213> lstAddPRL213 = new List<PRL213>();
                List<PRL213> lstDeletePRL213 = new List<PRL213>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL213 obj = db.PRL213.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        else
                        {
                            obj = new PRL213();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }

                        obj.RequiredValue = item.RequiredValue;
                        obj.ActualValue = item.ActualValue;

                        if (isAdded)
                        {
                            lstAddPRL213.Add(obj);
                        }
                    }
                }
                if (lstAddPRL213.Count > 0)
                {
                    db.PRL213.AddRange(lstAddPRL213);
                }
                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL213 = db.PRL213.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL213 = db.PRL213.Where(x => x.HeaderId == HeaderId).ToList();
                }

                if (lstDeletePRL213.Count > 0)
                {
                    db.PRL213.RemoveRange(lstDeletePRL213);
                }

                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line4 details
        [HttpPost]
        public JsonResult SaveProtocolLine4Linkage(List<PRL214> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL214> lstAddPRL214 = new List<PRL214>();
                List<PRL214> lstDeletePRL214 = new List<PRL214>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL214 obj = db.PRL214.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL214();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SeamNo = item.SeamNo;
                        obj.TopPeakIn = item.TopPeakIn;
                        obj.TopPeakOut = item.TopPeakOut;
                        obj.MidPeakIn = item.MidPeakIn;
                        obj.MidPeakOut = item.MidPeakOut;
                        obj.BottomPeakIn = item.BottomPeakIn;
                        obj.BottomPeakOut = item.BottomPeakOut;

                        if (isAdded)
                        {
                            lstAddPRL214.Add(obj);
                        }
                    }
                }
                if (lstAddPRL214.Count > 0)
                {
                    db.PRL214.AddRange(lstAddPRL214);
                }
                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL214 = db.PRL214.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL214 = db.PRL214.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL214.Count > 0)
                {
                    db.PRL214.RemoveRange(lstDeletePRL214);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line5 details
        [HttpPost]
        public JsonResult SaveProtocolLine5Linkage(List<PRL214_2> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL214_2> lstAddPRL214_2 = new List<PRL214_2>();
                List<PRL214_2> lstDeletePRL214_2 = new List<PRL214_2>();

                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL214_2 obj = db.PRL214_2.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        PRL214_2 objFecthSeam = new PRL214_2();
                        if (obj == null)
                        {
                            obj = new PRL214_2();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        objFecthSeam = db.PRL214_2.Where(x => x.SeamNo == item.SeamNo && x.HeaderId == item.HeaderId).FirstOrDefault();
                        if (objFecthSeam != null)
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                        }
                        else
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                        }

                        obj.Depth = item.Depth;
                        obj.Width = item.Width;
                        obj.Offset = item.Offset;
                        obj.SpotNo = item.SpotNo;
                        obj.SpotNoClad = item.SpotNoClad;
                        if (isAdded)
                        {
                            lstAddPRL214_2.Add(obj);
                        }
                    }
                }
                if (lstAddPRL214_2.Count > 0)
                {
                    db.PRL214_2.AddRange(lstAddPRL214_2);
                }
                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL214_2 = db.PRL214_2.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL214_2 = db.PRL214_2.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL214_2.Count > 0)
                {
                    db.PRL214_2.RemoveRange(lstDeletePRL214_2);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion
    }
}