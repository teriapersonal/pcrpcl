﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol077Controller : clsBase
    {
        // GET: PROTOCOL/Protocol077
        string ControllerURL = "/PROTOCOL/Protocol077/";
        string Title = "REPORT FOR VISUAL AND DIMENSION INSPECTION FOR SQUARE TYPE NUB BEFORE OVERLAY";


        #region Details View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO380 objPRO380 = new PRO380();

            if (!id.HasValue)
            {
                try
                {
                    objPRO380.ProtocolNo = string.Empty;
                    objPRO380.CreatedBy = objClsLoginInfo.UserName;
                    objPRO380.CreatedOn = DateTime.Now;

                    #region  Orientation | PRO380
                    objPRO380.PRO381.Add(new PRO381
                    {

                        Orientation = "0º",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO380.PRO381.Add(new PRO381
                    {

                        Orientation = "45º",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO380.PRO381.Add(new PRO381
                    {

                        Orientation = "90º",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO380.PRO381.Add(new PRO381
                    {

                        Orientation = "135º",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO380.PRO381.Add(new PRO381
                    {

                        Orientation = "180º",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO380.PRO381.Add(new PRO381
                    {

                        Orientation = "225º",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO380.PRO381.Add(new PRO381
                    {

                        Orientation = "270º",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO380.PRO381.Add(new PRO381
                    {

                        Orientation = "315º",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });


                    #endregion

                    #region  OrientationReqNubID | PRO380
                    objPRO380.PRO382.Add(new PRO382
                    {

                        OrientationReqNubID = "0°-180°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO380.PRO382.Add(new PRO382
                    {

                        OrientationReqNubID = "22.5°-202.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO380.PRO382.Add(new PRO382
                    {

                        OrientationReqNubID = "45°-225°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO380.PRO382.Add(new PRO382
                    {

                        OrientationReqNubID = "67.5°-247.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO380.PRO382.Add(new PRO382
                    {

                        OrientationReqNubID = "90°-270°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO380.PRO382.Add(new PRO382
                    {

                        OrientationReqNubID = "112.5°-292.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO380.PRO382.Add(new PRO382
                    {

                        OrientationReqNubID = "135°-315°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO380.PRO382.Add(new PRO382
                    {

                        OrientationReqNubID = "157.5°-337.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });


                    #endregion

                    #region  OrientationAllowedShellOutOfRoundness | PRO380
                    objPRO380.PRO383.Add(new PRO383
                    {

                        OrientationAllowedShellOutOfRoundness = "0°-180°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO380.PRO383.Add(new PRO383
                    {

                        OrientationAllowedShellOutOfRoundness = "22.5°-202.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO380.PRO383.Add(new PRO383
                    {

                        OrientationAllowedShellOutOfRoundness = "45°-225°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO380.PRO383.Add(new PRO383
                    {

                        OrientationAllowedShellOutOfRoundness = "67.5°-247.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO380.PRO383.Add(new PRO383
                    {

                        OrientationAllowedShellOutOfRoundness = "90°-270°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO380.PRO383.Add(new PRO383
                    {

                        OrientationAllowedShellOutOfRoundness = "112.5°-292.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO380.PRO383.Add(new PRO383
                    {

                        OrientationAllowedShellOutOfRoundness = "135°-315°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO380.PRO383.Add(new PRO383
                    {

                        OrientationAllowedShellOutOfRoundness = "157.5°-337.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });


                    #endregion

                    #region  OtherDimensions | PRO380
                    objPRO380.PRO384.Add(new PRO384
                    {

                        OtherDimensions = "TOP SIDE CORNER RADIUS",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO380.PRO384.Add(new PRO384
                    {

                        OtherDimensions = "BOTTOM SIDE CORNER RADIUS",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO380.PRO384.Add(new PRO384
                    {

                        OtherDimensions = "SURFACE FINISH ON MACHINED SURFACES",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                  

                    #endregion

                    db.PRO380.Add(objPRO380);
                    db.SaveChanges();
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO380.HeaderId;
            }
            else
            {
                objPRO380 = db.PRO380.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstElevationOfNubFrom = new List<string> { "BOTTOM SIDE WEP", "BOTTOM REF. LINE", "B.T.L" };
            List<string> lstElevationOfTopSide = new List<string> { "BOTTOM SIDE WEP", "BOTTOM REF. LINE", "B.T.L" };

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.ElevationOfNubFrom = lstElevationOfNubFrom.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.ElevationOfTopSide = lstElevationOfTopSide.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO381> lstPRO381 = db.PRO381.Where(x => x.HeaderId == objPRO380.HeaderId).ToList();
            List<PRO382> lstPRO382 = db.PRO382.Where(x => x.HeaderId == objPRO380.HeaderId).ToList();
            List<PRO383> lstPRO383 = db.PRO383.Where(x => x.HeaderId == objPRO380.HeaderId).ToList();
            List<PRO384> lstPRO384 = db.PRO384.Where(x => x.HeaderId == objPRO380.HeaderId).ToList();

            ViewBag.lstPRO381 = lstPRO381;
            ViewBag.lstPRO382 = lstPRO382;
            ViewBag.lstPRO383 = lstPRO383;
            ViewBag.lstPRO384 = lstPRO384;


            #endregion

            return View(objPRO380);
        }

        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO380 PRO380)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO380.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO380 objPRO380 = db.PRO380.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO380.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO380.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data
                        objPRO380.ProtocolNo = PRO380.ProtocolNo;
                        objPRO380.ElevationOfNubFrom = PRO380.ElevationOfNubFrom;
                        objPRO380.ElevationOfTopSide = PRO380.ElevationOfTopSide;
                        objPRO380.ReqElevationOfNubFrom = PRO380.ReqElevationOfNubFrom;
                        objPRO380.ReqElevationOfTopSide = PRO380.ReqElevationOfTopSide;
                        objPRO380.ReqNubWidth = PRO380.ReqNubWidth;
                        objPRO380.ReqNubHeight = PRO380.ReqNubHeight;
                        objPRO380.TolReqElevationOfNubFrom = PRO380.TolReqElevationOfNubFrom;
                        objPRO380.TolReqElevationOfTopSide = PRO380.TolReqElevationOfTopSide;
                        objPRO380.TolReqNubWidth = PRO380.TolReqNubWidth;
                        objPRO380.TolReqNubHeight = PRO380.TolReqNubHeight;
                        objPRO380.NubID = PRO380.NubID;
                        objPRO380.ReqNubID = PRO380.ReqNubID;
                        objPRO380.OutByNubID = PRO380.OutByNubID;
                        objPRO380.ShellOutOfRoundness = PRO380.ShellOutOfRoundness;
                        objPRO380.AllowedShellOutOfRoundness = PRO380.AllowedShellOutOfRoundness;
                        objPRO380.OutByShellOutOfRoundness = PRO380.OutByShellOutOfRoundness;
                        objPRO380.CheckPoint1 = PRO380.CheckPoint1;
                        objPRO380.CheckPoint2 = PRO380.CheckPoint2;
                        objPRO380.CheckPoint3 = PRO380.CheckPoint3;
                        objPRO380.CheckPoint4 = PRO380.CheckPoint4;
                        objPRO380.CheckPoint6 = PRO380.CheckPoint6;
                        objPRO380.CheckPoint6_2 = PRO380.CheckPoint6_2;
                        objPRO380.QCRemarks = PRO380.QCRemarks;
                        objPRO380.Result = PRO380.Result;
                        objPRO380.EditedBy = PRO380.EditedBy;
                        objPRO380.EditedOn = PRO380.EditedOn;
                        objPRO380.EditedBy = objClsLoginInfo.UserName;
                        objPRO380.EditedOn = DateTime.Now;


                        
                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO380.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO380 objPRO380 = db.PRO380.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO380 != null && string.IsNullOrWhiteSpace(objPRO380.ProtocolNo))
                    {
                        db.PRO380.Remove(objPRO380);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO380.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO381> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO381> lstAddPRO381 = new List<PRO381>();
                List<PRO381> lstDeletePRO381 = new List<PRO381>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO381 obj = db.PRO381.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO381();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }




                        obj.Orientation = item.Orientation;
                        obj.ActTolReqElevationOfNubFrom = item.ActTolReqElevationOfNubFrom;
                        obj.ActTolReqElevationOfTopSide = item.ActTolReqElevationOfTopSide;
                        obj.ActTolReqNubWidth = item.ActTolReqNubWidth;
                        obj.ActTolReqNubHeight = item.ActTolReqNubHeight;
                        obj.A1DimensionsChecked = item.A1DimensionsChecked;
                        obj.A2DimensionsChecked = item.A2DimensionsChecked;
                        obj.B1DimensionsChecked = item.B1DimensionsChecked;
                        obj.B2DimensionsChecked = item.B2DimensionsChecked;
                        obj.C1DimensionsChecked = item.C1DimensionsChecked;
                        obj.C2DimensionsChecked = item.C2DimensionsChecked;

                        if (isAdded)
                        {
                            lstAddPRO381.Add(obj);
                        }
                    }
                    if (lstAddPRO381.Count > 0)
                    {
                        db.PRO381.AddRange(lstAddPRO381);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO381 = db.PRO381.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO381.Count > 0)
                    {
                        db.PRO381.RemoveRange(lstDeletePRO381);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO381 = db.PRO381.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO381.Count > 0)
                    {
                        db.PRO381.RemoveRange(lstDeletePRO381);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2(List<PRO382> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO382> lstAddPRO382 = new List<PRO382>();
                List<PRO382> lstDeletePRO382 = new List<PRO382>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO382 obj = db.PRO382.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO382();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }


                        if (!string.IsNullOrWhiteSpace(item.OrientationReqNubID))
                        {


                            obj.OrientationReqNubID = item.OrientationReqNubID;
                            obj.ActualNubID = item.ActualNubID;
                            if (isAdded)
                            {
                                lstAddPRO382.Add(obj);
                            }
                        }

                       
                    }
                    if (lstAddPRO382.Count > 0)
                    {
                        db.PRO382.AddRange(lstAddPRO382);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO382 = db.PRO382.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO382.Count > 0)
                    {
                        db.PRO382.RemoveRange(lstDeletePRO382);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO382 = db.PRO382.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO382.Count > 0)
                    {
                        db.PRO382.RemoveRange(lstDeletePRO382);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3(List<PRO383> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO383> lstAddPRO383 = new List<PRO383>();
                List<PRO383> lstDeletePRO383 = new List<PRO383>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO383 obj = db.PRO383.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO383();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        if (!string.IsNullOrWhiteSpace(item.OrientationAllowedShellOutOfRoundness))
                        {
                            obj.OrientationAllowedShellOutOfRoundness = item.OrientationAllowedShellOutOfRoundness;
                            obj.ActualShellOutOfRoundness = item.ActualShellOutOfRoundness;

                            if (isAdded)
                            {
                                lstAddPRO383.Add(obj);
                            }
                        }


                        
                    }
                    if (lstAddPRO383.Count > 0)
                    {
                        db.PRO383.AddRange(lstAddPRO383);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO383 = db.PRO383.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO383.Count > 0)
                    {
                        db.PRO383.RemoveRange(lstDeletePRO383);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO383 = db.PRO383.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO383.Count > 0)
                    {
                        db.PRO383.RemoveRange(lstDeletePRO383);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line4 details
        [HttpPost]
        public JsonResult SaveProtocolLine4(List<PRO384> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO384> lstAddPRO384 = new List<PRO384>();
                List<PRO384> lstDeletePRO384 = new List<PRO384>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO384 obj = db.PRO384.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO384();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.OtherDimensions = item.OtherDimensions;
                        obj.ReqOtherDimensions = item.ReqOtherDimensions;
                        obj.ActOtherDimensions = item.ActOtherDimensions;


                        if (isAdded)
                        {
                            lstAddPRO384.Add(obj);
                        }
                    }
                    if (lstAddPRO384.Count > 0)
                    {
                        db.PRO384.AddRange(lstAddPRO384);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO384 = db.PRO384.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO384.Count > 0)
                    {
                        db.PRO384.RemoveRange(lstDeletePRO384);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO384 = db.PRO384.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO384.Count > 0)
                    {
                        db.PRO384.RemoveRange(lstDeletePRO384);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #endregion

        #region  Linkage View Code   

        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL380 objPRL380 = new PRL380();
            objPRL380 = db.PRL380.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL380 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL380.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();
            List<string> lstElevationOfNubFrom = new List<string> { "BOTTOM SIDE WEP", "BOTTOM REF. LINE", "B.T.L" };
            List<string> lstElevationOfTopSide = new List<string> { "BOTTOM SIDE WEP", "BOTTOM REF. LINE", "B.T.L" };

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.ElevationOfNubFrom = lstElevationOfNubFrom.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.ElevationOfTopSide = lstElevationOfTopSide.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL381> lstPRL381 = db.PRL381.Where(x => x.HeaderId == objPRL380.HeaderId).ToList();
            List<PRL382> lstPRL382 = db.PRL382.Where(x => x.HeaderId == objPRL380.HeaderId).ToList();
            List<PRL383> lstPRL383 = db.PRL383.Where(x => x.HeaderId == objPRL380.HeaderId).ToList();
            List<PRL384> lstPRL384 = db.PRL384.Where(x => x.HeaderId == objPRL380.HeaderId).ToList();

            ViewBag.lstPRL381 = lstPRL381;
            ViewBag.lstPRL382 = lstPRL382;
            ViewBag.lstPRL383 = lstPRL383;
            ViewBag.lstPRL384 = lstPRL384;


            #endregion

            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.DrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL380.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL380.ActFilledBy) && (objPRL380.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL380.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL380);
        }

        [HttpPost]
        public ActionResult LinkageMobile(int? id, int? m, string APIRequestFrom = "", string UserName = "")
        {
            string ProjectDesc = "";
            string ManufacturingCode = "";
            string AccessRole = "";
            bool isEditable = false;
            var DrawingNo = new List<string>();
            bool isQCRemarkEditable = false;

            PRL380 objPRL380 = new PRL380();
            db.Configuration.ProxyCreationEnabled = false;
            objPRL380 = db.PRL380.Where(i => i.HeaderId == id).FirstOrDefault();
            db.Entry(objPRL380).State = System.Data.Entity.EntityState.Detached;
            NDEModels objNDEModels = new NDEModels();
            if (objPRL380 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL380.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
            AccessRole = objUserRoleAccessDetails.UserDesignation;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();
            List<string> lstElevationOfNubFrom = new List<string> { "BOTTOM SIDE WEP", "BOTTOM REF. LINE", "B.T.L" };
            List<string> lstElevationOfTopSide = new List<string> { "BOTTOM SIDE WEP", "BOTTOM REF. LINE", "B.T.L" };

            var YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var ElevationOfNubFrom = lstElevationOfNubFrom.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var ElevationOfTopSide = lstElevationOfTopSide.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL381> lstPRL381 = db.PRL381.Where(x => x.HeaderId == objPRL380.HeaderId).ToList();
            List<PRL382> lstPRL382 = db.PRL382.Where(x => x.HeaderId == objPRL380.HeaderId).ToList();
            List<PRL383> lstPRL383 = db.PRL383.Where(x => x.HeaderId == objPRL380.HeaderId).ToList();
            List<PRL384> lstPRL384 = db.PRL384.Where(x => x.HeaderId == objPRL380.HeaderId).ToList();


            #endregion
            try
            {
                isEditable = false;
                isQCRemarkEditable = false;
                if (m == 1)
                {
                    isEditable = true;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL380.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL380.ActFilledBy) && (objPRL380.RequestNo.HasValue))
                        {
                            isEditable = false;
                            isQCRemarkEditable = true;
                        }
                        else
                        {
                            DrawingNo = (new GeneralController()).GetPLMDrawingNumber(objPRL380.Project).ToList();
                        }
                    }
                }

                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    ProjectDesc = ProjectDesc,
                    ManufacturingCode = ManufacturingCode,
                    isEditable = isEditable,
                    isQCRemarkEditable = isQCRemarkEditable,
                    DrawingNo = DrawingNo,
                    AccessRole = AccessRole,

                    YesNoEnum = YesNoEnum,
                    YesNAEnum = YesNAEnum,
                    ElevationOfNubFrom = ElevationOfNubFrom,
                    ElevationOfTopSide = ElevationOfTopSide,
                    OutsideInside = OutsideInside,
                    LeftRight = LeftRight,

                    objPRL380 = objPRL380,

                    lstPRL381 = lstPRL381,
                    lstPRL382 = lstPRL382,
                    lstPRL383 = lstPRL383,
                    lstPRL384 = lstPRL384

                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                return Json(new
                {
                    res = "0",
                    msg = ex.Message,
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL380 PRL380, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = PRL380.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL380 objPRL380 = db.PRL380.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL380.DrawingRevisionNo = PRL380.DrawingRevisionNo;
                    objPRL380.DrawingNo = PRL380.DrawingNo;
                    objPRL380.DCRNo = PRL380.DCRNo;
                    objPRL380.ProtocolNo = PRL380.ProtocolNo;
                    objPRL380.ElevationOfNubFrom = PRL380.ElevationOfNubFrom;
                    objPRL380.ElevationOfTopSide = PRL380.ElevationOfTopSide;
                    objPRL380.ReqElevationOfNubFrom = PRL380.ReqElevationOfNubFrom;
                    objPRL380.ReqElevationOfTopSide = PRL380.ReqElevationOfTopSide;
                    objPRL380.ReqNubWidth = PRL380.ReqNubWidth;
                    objPRL380.ReqNubHeight = PRL380.ReqNubHeight;
                    objPRL380.TolReqElevationOfNubFrom = PRL380.TolReqElevationOfNubFrom;
                    objPRL380.TolReqElevationOfTopSide = PRL380.TolReqElevationOfTopSide;
                    objPRL380.TolReqNubWidth = PRL380.TolReqNubWidth;
                    objPRL380.TolReqNubHeight = PRL380.TolReqNubHeight;
                    objPRL380.NubID = PRL380.NubID;
                    objPRL380.ReqNubID = PRL380.ReqNubID;
                    objPRL380.OutByNubID = PRL380.OutByNubID;
                    objPRL380.ShellOutOfRoundness = PRL380.ShellOutOfRoundness;
                    objPRL380.AllowedShellOutOfRoundness = PRL380.AllowedShellOutOfRoundness;
                    objPRL380.OutByShellOutOfRoundness = PRL380.OutByShellOutOfRoundness;
                    objPRL380.CheckPoint1 = PRL380.CheckPoint1;
                    objPRL380.CheckPoint2 = PRL380.CheckPoint2;
                    objPRL380.CheckPoint3 = PRL380.CheckPoint3;
                    objPRL380.CheckPoint4 = PRL380.CheckPoint4;
                    objPRL380.CheckPoint6 = PRL380.CheckPoint6;
                    objPRL380.CheckPoint6_2 = PRL380.CheckPoint6_2;
                    objPRL380.QCRemarks = PRL380.QCRemarks;
                    objPRL380.Result = PRL380.Result;
                    objPRL380.EditedBy = UserName;
                    objPRL380.EditedOn = DateTime.Now;

                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL380.ActFilledBy = UserName;
                            objPRL380.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL380.ReqFilledBy = UserName;
                            objPRL380.ReqFilledOn = DateTime.Now;
                        }
                    }

                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL380.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL380 objPRL380 = db.PRL380.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL380 != null && string.IsNullOrWhiteSpace(objPRL380.ProtocolNo))
                    {
                        db.PRL380.Remove(objPRL380);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL380.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL381> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL381> lstAddPRL381 = new List<PRL381>();
                List<PRL381> lstDeletePRL381 = new List<PRL381>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL381 obj = db.PRL381.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL381();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Orientation = item.Orientation;
                        obj.ActTolReqElevationOfNubFrom = item.ActTolReqElevationOfNubFrom;
                        obj.ActTolReqElevationOfTopSide = item.ActTolReqElevationOfTopSide;
                        obj.ActTolReqNubWidth = item.ActTolReqNubWidth;
                        obj.ActTolReqNubHeight = item.ActTolReqNubHeight;
                        obj.A1DimensionsChecked = item.A1DimensionsChecked;
                        obj.A2DimensionsChecked = item.A2DimensionsChecked;
                        obj.B1DimensionsChecked = item.B1DimensionsChecked;
                        obj.B2DimensionsChecked = item.B2DimensionsChecked;
                        obj.C1DimensionsChecked = item.C1DimensionsChecked;
                        obj.C2DimensionsChecked = item.C2DimensionsChecked;

                        if (isAdded)
                        {
                            lstAddPRL381.Add(obj);
                        }
                    }
                }
                if (lstAddPRL381.Count > 0)
                {
                    db.PRL381.AddRange(lstAddPRL381);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL381 = db.PRL381.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL381 = db.PRL381.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL381.Count > 0)
                {
                    db.PRL381.RemoveRange(lstDeletePRL381);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2Linkage(List<PRL382> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL382> lstAddPRL382 = new List<PRL382>();
                List<PRL382> lstDeletePRL382 = new List<PRL382>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL382 obj = db.PRL382.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL382();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        if (!string.IsNullOrWhiteSpace(item.OrientationReqNubID))
                        {

                            obj.OrientationReqNubID = item.OrientationReqNubID;
                            obj.ActualNubID = item.ActualNubID;

                            if (isAdded)
                            {
                                lstAddPRL382.Add(obj);
                            }
                        }
                    }
                }
                if (lstAddPRL382.Count > 0)
                {
                    db.PRL382.AddRange(lstAddPRL382);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL382 = db.PRL382.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL382 = db.PRL382.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL382.Count > 0)
                {
                    db.PRL382.RemoveRange(lstDeletePRL382);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3Linkage(List<PRL383> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL383> lstAddPRL383 = new List<PRL383>();
                List<PRL383> lstDeletePRL383 = new List<PRL383>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL383 obj = db.PRL383.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL383();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }


                        if (!string.IsNullOrWhiteSpace(item.OrientationAllowedShellOutOfRoundness))
                        {


                            obj.OrientationAllowedShellOutOfRoundness = item.OrientationAllowedShellOutOfRoundness;
                            obj.ActualShellOutOfRoundness = item.ActualShellOutOfRoundness;

                            if (isAdded)
                            {
                                lstAddPRL383.Add(obj);
                            }
                        }
                    }
                }
                if (lstAddPRL383.Count > 0)
                {
                    db.PRL383.AddRange(lstAddPRL383);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL383 = db.PRL383.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL383 = db.PRL383.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL383.Count > 0)
                {
                    db.PRL383.RemoveRange(lstDeletePRL383);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line4 details
        [HttpPost]
        public JsonResult SaveProtocolLine4Linkage(List<PRL384> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL384> lstAddPRL384 = new List<PRL384>();
                List<PRL384> lstDeletePRL384 = new List<PRL384>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL384 obj = db.PRL384.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL384();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.OtherDimensions = item.OtherDimensions;
                        obj.ReqOtherDimensions = item.ReqOtherDimensions;
                        obj.ActOtherDimensions = item.ActOtherDimensions;


                        if (isAdded)
                        {
                            lstAddPRL384.Add(obj);
                        }
                    }
                }
                if (lstAddPRL384.Count > 0)
                {
                    db.PRL384.AddRange(lstAddPRL384);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL384 = db.PRL384.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL384 = db.PRL384.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL384.Count > 0)
                {
                    db.PRL384.RemoveRange(lstDeletePRL384);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #endregion

    }
}