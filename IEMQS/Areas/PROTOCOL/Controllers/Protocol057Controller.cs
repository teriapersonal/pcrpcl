﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol057Controller : clsBase
    {
        // GET: PROTOCOL/Protocol057
        string ControllerURL = "/PROTOCOL/Protocol057/";
        string Title = "BEFORE OVERLAY THICKNESS MEASUREMENT REPORT FOR SHELL-HEAD-LONG SEAM-CIRC. SEAM";

        #region Details View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO280 objPRO280 = new PRO280();
            if (!id.HasValue)
            {
                try
                {
                    objPRO280.ProtocolNo = string.Empty;
                    objPRO280.CreatedBy = objClsLoginInfo.UserName;
                    objPRO280.CreatedOn = DateTime.Now;

                    db.PRO280.Add(objPRO280);
                    db.SaveChanges();
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO280.HeaderId;
            }
            else
            {
                objPRO280 = db.PRO280.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstStageOfInspection = new List<string> { "BEFORE OVERLAY", "AFTER OVERLAY", "AFTER COMPLETE WELD", "BEFORE MACHINING", "AFTER MACHINING", "BEFORE PWHT", "AFTER PWHT", "BEFORE HYDRO", "AFTER HYDRO", "BEFORE FINAL DISPATCH" };

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.StageOfInspection = lstStageOfInspection.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO281> lstPRO281 = db.PRO281.Where(x => x.HeaderId == objPRO280.HeaderId).ToList();

            ViewBag.lstPRO281 = lstPRO281;

            #endregion

            return View(objPRO280);
        }

        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO280 PRO280)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO280.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO280 objPRO280 = db.PRO280.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO280.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO280.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data
                        objPRO280.ProtocolNo = PRO280.ProtocolNo;
                        objPRO280.SurfaceCondition = PRO280.SurfaceCondition;
                        objPRO280.StageOfInspection = PRO280.StageOfInspection;
                        objPRO280.CheckPoint1 = PRO280.CheckPoint1;
                        objPRO280.CheckPoint2 = PRO280.CheckPoint2;
                        objPRO280.CheckPoint4 = PRO280.CheckPoint4;
                        objPRO280.CheckPoint4_2 = PRO280.CheckPoint4_2;
                        objPRO280.QCRemarks = PRO280.QCRemarks;
                        objPRO280.Result = PRO280.Result;

                        objPRO280.EditedBy = objClsLoginInfo.UserName;
                        objPRO280.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO280.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO280 objPRO280 = db.PRO280.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO280 != null && string.IsNullOrWhiteSpace(objPRO280.ProtocolNo))
                    {
                        db.PRO280.Remove(objPRO280);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO280.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO281> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO281> lstAddPRO281 = new List<PRO281>();
                List<PRO281> lstDeletePRO281 = new List<PRO281>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO281 obj = db.PRO281.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO281();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SrNo = item.SrNo;
                        obj.Elevation = item.Elevation;
                        obj.Orientation = item.Orientation;
                        obj.ReqThickness = item.ReqThickness;
                        obj.ActThickness = item.ActThickness;
                        obj.Remarks = item.Remarks;
                        if (isAdded)
                        {
                            lstAddPRO281.Add(obj);
                        }
                    }
                    if (lstAddPRO281.Count > 0)
                    {
                        db.PRO281.AddRange(lstAddPRO281);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO281 = db.PRO281.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO281.Count > 0)
                    {
                        db.PRO281.RemoveRange(lstDeletePRO281);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO281 = db.PRO281.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO281.Count > 0)
                    {
                        db.PRO281.RemoveRange(lstDeletePRO281);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

        #region Linkage View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL280 objPRL280 = new PRL280();
            objPRL280 = db.PRL280.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL280 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL280.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;

                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstStageOfInspection = new List<string> { "BEFORE OVERLAY", "AFTER OVERLAY", "AFTER COMPLETE WELD", "BEFORE MACHINING", "AFTER MACHINING", "BEFORE PWHT", "AFTER PWHT", "BEFORE HYDRO", "AFTER HYDRO", "BEFORE FINAL DISPATCH" };

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.StageOfInspection = lstStageOfInspection.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL281> lstPRL281 = db.PRL281.Where(x => x.HeaderId == objPRL280.HeaderId).ToList();

            ViewBag.lstPRL281 = lstPRL281;

            #endregion
            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.DrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL280.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL280.ActFilledBy) && (objPRL280.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL280.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL280);
        }

        [HttpPost]
        public ActionResult LinkageMobile(int? id, int? m, string APIRequestFrom = "", string UserName = "")
        {
            string ProjectDesc = "";
            string ManufacturingCode = "";
            string AccessRole = "";
            bool isEditable = false;
            var DrawingNo = new List<string>();
            bool isQCRemarkEditable = false;

            PRL280 objPRL280 = new PRL280();
            db.Configuration.ProxyCreationEnabled = false;
            objPRL280 = db.PRL280.Where(i => i.HeaderId == id).FirstOrDefault();
            db.Entry(objPRL280).State = System.Data.Entity.EntityState.Detached;
            NDEModels objNDEModels = new NDEModels();
            if (objPRL280 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL280.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
            AccessRole = objUserRoleAccessDetails.UserDesignation;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstStageOfInspection = new List<string> { "BEFORE OVERLAY", "AFTER OVERLAY", "AFTER COMPLETE WELD", "BEFORE MACHINING", "AFTER MACHINING", "BEFORE PWHT", "AFTER PWHT", "BEFORE HYDRO", "AFTER HYDRO", "BEFORE FINAL DISPATCH" };

            var YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var StageOfInspection = lstStageOfInspection.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL281> lstPRL281 = db.PRL281.Where(x => x.HeaderId == objPRL280.HeaderId).ToList();



            #endregion
            try
            {
                isEditable = false;
                isQCRemarkEditable = false;
                if (m == 1)
                {
                    isEditable = true;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL280.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL280.ActFilledBy) && (objPRL280.RequestNo.HasValue))
                        {
                            isEditable = false;
                            isQCRemarkEditable = true;
                        }
                        else
                        {
                            DrawingNo = (new GeneralController()).GetPLMDrawingNumber(objPRL280.Project).ToList();
                        }
                    }
                }
                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    ProjectDesc = ProjectDesc,
                    ManufacturingCode = ManufacturingCode,
                    isEditable = isEditable,
                    isQCRemarkEditable = isQCRemarkEditable,
                    DrawingNo = DrawingNo,
                    AccessRole = AccessRole,
                    YesNoEnum = YesNoEnum,

                    objPRL280 = objPRL280,

                    lstPRL281 = lstPRL281

                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                return Json(new
                {
                    res = "0",
                    msg = ex.Message,
                }, JsonRequestBehavior.AllowGet);

            }

        }
        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL280 PRL280, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = PRL280.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL280 objPRL280 = db.PRL280.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL280.DrawingRevisionNo = PRL280.DrawingRevisionNo;
                    objPRL280.DrawingNo = PRL280.DrawingNo;
                    objPRL280.DCRNo = PRL280.DCRNo;


                    objPRL280.ProtocolNo = PRL280.ProtocolNo != null ? PRL280.ProtocolNo : "";
                    objPRL280.SurfaceCondition = PRL280.SurfaceCondition;
                    objPRL280.StageOfInspection = PRL280.StageOfInspection;
                    objPRL280.CheckPoint1 = PRL280.CheckPoint1;
                    objPRL280.CheckPoint2 = PRL280.CheckPoint2;
                    objPRL280.CheckPoint4 = PRL280.CheckPoint4;
                    objPRL280.CheckPoint4_2 = PRL280.CheckPoint4_2;
                    objPRL280.QCRemarks = PRL280.QCRemarks;
                    objPRL280.Result = PRL280.Result;

                    objPRL280.EditedBy = UserName;
                    objPRL280.EditedOn = DateTime.Now;

                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL280.ActFilledBy = UserName;
                            objPRL280.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL280.ReqFilledBy = UserName;
                            objPRL280.ReqFilledOn = DateTime.Now;
                        }
                    }
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL280.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL280 objPRL280 = db.PRL280.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL280 != null && string.IsNullOrWhiteSpace(objPRL280.ProtocolNo))
                    {
                        db.PRL280.Remove(objPRL280);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL280.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL281> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL281> lstAddPRL281 = new List<PRL281>();
                List<PRL281> lstDeletePRL281 = new List<PRL281>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL281 obj = db.PRL281.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL281();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SrNo = item.SrNo;
                        obj.Elevation = item.Elevation;
                        obj.Orientation = item.Orientation;
                        obj.ReqThickness = item.ReqThickness;
                        obj.ActThickness = item.ActThickness;
                        obj.Remarks = item.Remarks;
                        if (isAdded)
                        {
                            lstAddPRL281.Add(obj);
                        }
                    }
                    if (lstAddPRL281.Count > 0)
                    {
                        db.PRL281.AddRange(lstAddPRL281);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL281 = db.PRL281.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL281.Count > 0)
                    {
                        db.PRL281.RemoveRange(lstDeletePRL281);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL281 = db.PRL281.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL281.Count > 0)
                    {
                        db.PRL281.RemoveRange(lstDeletePRL281);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

    }
}