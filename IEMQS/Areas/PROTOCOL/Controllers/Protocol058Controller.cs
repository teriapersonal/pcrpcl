﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol058Controller : clsBase
    {
        // GET: PROTOCOL/Protocol058
        string ControllerURL = "/PROTOCOL/Protocol058/";
        string Title = "VISUAL AND OVERLAY THICKNESS MEASUREMENT REPORT FOR SHELL/HEAD/NOZZLE/LONG SEAM/CIRC. SEAM";

        #region Details View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO285 objPRO285 = new PRO285();
            if (!id.HasValue)
            {
                try
                {
                    objPRO285.ProtocolNo = string.Empty;
                    objPRO285.CreatedBy = objClsLoginInfo.UserName;
                    objPRO285.CreatedOn = DateTime.Now;



                    db.PRO285.Add(objPRO285);
                    db.SaveChanges();
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO285.HeaderId;

                List<PRO281> _lstPRO281 = db.PRO281.Where(w => w.HeaderId == objPRO285.HeaderId).ToList();

                for (int i = 0; i < _lstPRO281.Count; i++)
                {
                    PRO286 objPRO286Copy = new PRO286();
                    objPRO286Copy.SrNo = _lstPRO281[i].SrNo;
                    objPRO286Copy.HeaderId = objPRO285.HeaderId;
                    objPRO286Copy.Elevation = _lstPRO281[i].Elevation;
                    objPRO286Copy.Orientation = _lstPRO281[i].Orientation;
                    objPRO286Copy.ReqBeforeOverlayThk = _lstPRO281[i].ReqThickness;
                    objPRO286Copy.ActBeforeOverlayThk = _lstPRO281[i].ActThickness;
                    objPRO286Copy.ReqAfterOverlayThk = "";
                    objPRO286Copy.ActAfterOverlayThk = "";
                    objPRO286Copy.ReqOverlayThk = "";
                    objPRO286Copy.Remarks = "";
                    objPRO286Copy.CreatedBy = objClsLoginInfo.UserName;
                    objPRO286Copy.CreatedOn = DateTime.Now;

                    db.PRO286.Add(objPRO286Copy);
                    db.SaveChanges();
                }
            }
            else
            {
                objPRO285 = db.PRO285.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO286> lstPRO286 = db.PRO286.Where(x => x.HeaderId == objPRO285.HeaderId).ToList();

            ViewBag.lstPRO286 = lstPRO286;

            #endregion

            return View(objPRO285);
        }

        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO285 PRO285)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO285.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO285 objPRO285 = db.PRO285.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO285.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO285.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data
                        objPRO285.ProtocolNo = PRO285.ProtocolNo;
                        objPRO285.CheckPoint1 = PRO285.CheckPoint1;
                        objPRO285.CheckPoint2 = PRO285.CheckPoint2;
                        objPRO285.CheckPoint4 = PRO285.CheckPoint4;
                        objPRO285.CheckPoint4_2 = PRO285.CheckPoint4_2;
                        objPRO285.QCRemarks = PRO285.QCRemarks;
                        objPRO285.Result = PRO285.Result;

                        objPRO285.EditedBy = objClsLoginInfo.UserName;
                        objPRO285.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO285.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO285 objPRO285 = db.PRO285.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO285 != null && string.IsNullOrWhiteSpace(objPRO285.ProtocolNo))
                    {
                        db.PRO285.Remove(objPRO285);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO285.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO286> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO286> lstAddPRO286 = new List<PRO286>();
                List<PRO286> lstDeletePRO286 = new List<PRO286>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO286 obj = db.PRO286.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO286();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SrNo = item.SrNo;
                        obj.Elevation = item.Elevation;
                        obj.Orientation = item.Orientation;
                        obj.ReqBeforeOverlayThk = item.ReqBeforeOverlayThk;
                        obj.ActBeforeOverlayThk = item.ActBeforeOverlayThk;
                        obj.ReqAfterOverlayThk = item.ReqAfterOverlayThk;
                        obj.ActAfterOverlayThk = item.ActAfterOverlayThk;
                        obj.ReqOverlayThk = item.ReqOverlayThk;
                        obj.Remarks = item.Remarks;
                        if (isAdded)
                        {
                            lstAddPRO286.Add(obj);
                        }
                    }
                    if (lstAddPRO286.Count > 0)
                    {
                        db.PRO286.AddRange(lstAddPRO286);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO286 = db.PRO286.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO286.Count > 0)
                    {
                        db.PRO286.RemoveRange(lstDeletePRO286);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO286 = db.PRO286.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO286.Count > 0)
                    {
                        db.PRO286.RemoveRange(lstDeletePRO286);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

        #region Linkage View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL285 objPRL285 = new PRL285();
            objPRL285 = db.PRL285.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL285 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL285.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;

                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL286> lstPRL286 = db.PRL286.Where(x => x.HeaderId == objPRL285.HeaderId).ToList();


            ViewBag.lstPRL286 = lstPRL286;

            #endregion
            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.DrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL285.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL285.ActFilledBy) && (objPRL285.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL285.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL285);
        }

        [HttpPost]
        public ActionResult LinkageMobile(int? id, int? m, string APIRequestFrom = "", string UserName = "")
        {
            string ProjectDesc = "";
            string ManufacturingCode = "";
            string AccessRole = "";
            bool isEditable = false;
            var DrawingNo = new List<string>();
            bool isQCRemarkEditable = false;

            PRL285 objPRL285 = new PRL285();
            db.Configuration.ProxyCreationEnabled = false;
            objPRL285 = db.PRL285.Where(i => i.HeaderId == id).FirstOrDefault();
            db.Entry(objPRL285).State = System.Data.Entity.EntityState.Detached;

            NDEModels objNDEModels = new NDEModels();
            if (objPRL285 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL285.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
            AccessRole = objUserRoleAccessDetails.UserDesignation;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();

            var YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL286> lstPRL286 = db.PRL286.Where(x => x.HeaderId == objPRL285.HeaderId).ToList();


            #endregion
            try
            {
                isEditable = false;
                isQCRemarkEditable = false;
                if (m == 1)
                {
                    isEditable = true;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL285.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL285.ActFilledBy) && (objPRL285.RequestNo.HasValue))
                        {
                            isEditable = false;
                            isQCRemarkEditable = true;
                        }
                        else
                        {
                            DrawingNo = (new GeneralController()).GetPLMDrawingNumber(objPRL285.Project).ToList();
                        }
                    }
                }

                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    ProjectDesc = ProjectDesc,
                    ManufacturingCode = ManufacturingCode,
                    isEditable = isEditable,
                    isQCRemarkEditable = isQCRemarkEditable,
                    DrawingNo = DrawingNo,
                    AccessRole = AccessRole,
                    YesNoEnum = YesNoEnum,

                    objPRL285 = objPRL285,

                    lstPRL286 = lstPRL286

                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();

                return Json(new
                {
                    res = "0",
                    msg = ex.Message,
                }, JsonRequestBehavior.AllowGet);

            }

        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL285 PRL285, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = PRL285.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL285 objPRL285 = db.PRL285.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL285.DrawingRevisionNo = PRL285.DrawingRevisionNo;
                    objPRL285.DrawingNo = PRL285.DrawingNo;
                    objPRL285.DCRNo = PRL285.DCRNo;

                    objPRL285.ProtocolNo = PRL285.ProtocolNo != null ? PRL285.ProtocolNo : "";
                    objPRL285.CheckPoint1 = PRL285.CheckPoint1;
                    objPRL285.CheckPoint2 = PRL285.CheckPoint2;
                    objPRL285.CheckPoint4 = PRL285.CheckPoint4;
                    objPRL285.CheckPoint4_2 = PRL285.CheckPoint4_2;
                    objPRL285.QCRemarks = PRL285.QCRemarks;
                    objPRL285.Result = PRL285.Result;

                    objPRL285.EditedBy = UserName;
                    objPRL285.EditedOn = DateTime.Now;

                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL285.ActFilledBy = UserName;
                            objPRL285.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL285.ReqFilledBy = UserName;
                            objPRL285.ReqFilledOn = DateTime.Now;
                        }
                    }
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL285.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL285 objPRL285 = db.PRL285.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL285 != null && string.IsNullOrWhiteSpace(objPRL285.ProtocolNo))
                    {
                        db.PRL285.Remove(objPRL285);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL285.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL286> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL286> lstAddPRL286 = new List<PRL286>();
                List<PRL286> lstDeletePRL286 = new List<PRL286>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL286 obj = db.PRL286.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL286();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SrNo = item.SrNo;
                        obj.Elevation = item.Elevation;
                        obj.Orientation = item.Orientation;
                        obj.ReqBeforeOverlayThk = item.ReqBeforeOverlayThk;
                        obj.ActBeforeOverlayThk = item.ActBeforeOverlayThk;
                        obj.ReqAfterOverlayThk = item.ReqAfterOverlayThk;
                        obj.ActAfterOverlayThk = item.ActAfterOverlayThk;
                        obj.ReqOverlayThk = item.ReqOverlayThk;
                        obj.Remarks = item.Remarks;
                        if (isAdded)
                        {
                            lstAddPRL286.Add(obj);
                        }
                    }
                    if (lstAddPRL286.Count > 0)
                    {
                        db.PRL286.AddRange(lstAddPRL286);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL286 = db.PRL286.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL286.Count > 0)
                    {
                        db.PRL286.RemoveRange(lstDeletePRL286);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL286 = db.PRL286.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL286.Count > 0)
                    {
                        db.PRL286.RemoveRange(lstDeletePRL286);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        [HttpPost]

        public JsonResult CopyData(string strSeamNo, int HeaderId)
        {
            PRL285 objPRL285 = db.PRL285.Where(w => w.HeaderId == HeaderId).FirstOrDefault();
            PRL280 _obj280 = db.PRL280.Where(w => w.SeamNo == strSeamNo && w.QualityProject == objPRL285.QualityProject).OrderByDescending(x => x.HeaderId).FirstOrDefault();
            
            if (_obj280 != null)
            {
                List<PRL281> _lstPRL281 = db.PRL281.Where(w => w.HeaderId == _obj280.HeaderId).ToList();
                List<clsPRL286> _lstPRL286 = new List<clsPRL286>();
                if (_lstPRL281.Count() > 0)
                {
                    for (int i = 0; i < _lstPRL281.Count; i++)
                    {
                        PRL286 objPRL286Copy = new PRL286();
                        objPRL286Copy.SrNo = _lstPRL281[i].SrNo;
                        objPRL286Copy.HeaderId = HeaderId;
                        objPRL286Copy.Elevation = _lstPRL281[i].Elevation;
                        objPRL286Copy.Orientation = _lstPRL281[i].Orientation;
                        objPRL286Copy.ReqBeforeOverlayThk = _lstPRL281[i].ReqThickness;
                        objPRL286Copy.ActBeforeOverlayThk = _lstPRL281[i].ActThickness;
                        objPRL286Copy.ReqAfterOverlayThk = "";
                        objPRL286Copy.ActAfterOverlayThk = "";
                        objPRL286Copy.ReqOverlayThk = "";
                        objPRL286Copy.Remarks = "";
                        objPRL286Copy.CreatedBy = objClsLoginInfo.UserName;
                        objPRL286Copy.CreatedOn = DateTime.Now;

                        db.PRL286.Add(objPRL286Copy);

                        clsPRL286 _obj = new clsPRL286();
                        _obj.SrNo = _lstPRL281[i].SrNo;
                        _obj.HeaderId = HeaderId;
                        _obj.Elevation = _lstPRL281[i].Elevation;
                        _obj.Orientation = _lstPRL281[i].Orientation;
                        _obj.ReqBeforeOverlayThk = _lstPRL281[i].ReqThickness;
                        _obj.ActBeforeOverlayThk = _lstPRL281[i].ActThickness;
                        _obj.ReqAfterOverlayThk = "";
                        _obj.ActAfterOverlayThk = "";
                        _obj.ReqOverlayThk = "";
                        _obj.Remarks = "";

                        _lstPRL286.Add(_obj);
                    }
                    db.SaveChanges();
                }

                PRL285 _obj285 = db.PRL285.Where(w => w.HeaderId == HeaderId).FirstOrDefault();
                if (_obj280 != null)
                {
                    _obj285.IsDataCopy = "true";
                    db.SaveChanges();
                }
                return Json(_lstPRL286, JsonRequestBehavior.AllowGet);
            }
            return Json("", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ActCopyData(string strSeamNo, int HeaderId)
        {
            string strReturn = "";
            try
            {
                PRL285 objPRL285 = db.PRL285.Where(w => w.HeaderId == HeaderId).FirstOrDefault();
                PRL280 _obj280 = db.PRL280.Where(w => w.SeamNo == strSeamNo && w.QualityProject == objPRL285.QualityProject).OrderByDescending(x => x.HeaderId).FirstOrDefault();

                if (_obj280 != null)
                {
                    List<PRL281> _lstPRL281 = db.PRL281.Where(w => w.HeaderId == _obj280.HeaderId).ToList();
                    List<PRL286> _lstPRL286 = db.PRL286.Where(w => w.HeaderId == HeaderId).ToList();
                    if (_lstPRL281.Count() > 0)
                    {
                        for (int i = 0; i < _lstPRL281.Count; i++)
                        {
                            if (_lstPRL286.Count >= i)
                            {
                                PRL286 _obj = _lstPRL286[i];
                                _obj.ActBeforeOverlayThk = _lstPRL281[i].ActThickness;
                                if (strReturn == "")
                                    strReturn = _lstPRL281[i].ActThickness;
                                else
                                    strReturn += "|" + _lstPRL281[i].ActThickness;
                                db.SaveChanges();
                            }
                        }

                        PRL285 _obj285 = db.PRL285.Where(w => w.HeaderId == HeaderId).FirstOrDefault();
                        if (_obj285 != null)
                        {
                            _obj285.IsActDataCopy = "true";
                            db.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                strReturn = "Error";
            }
            return Json(strReturn, JsonRequestBehavior.AllowGet);
        }

        #endregion

    }

    public class clsPRL286
    {
        public int LineId { get; set; }
        public Nullable<int> HeaderId { get; set; }
        public Nullable<int> SrNo { get; set; }
        public string Elevation { get; set; }
        public string Orientation { get; set; }
        public string ReqBeforeOverlayThk { get; set; }
        public string ActBeforeOverlayThk { get; set; }
        public string ReqAfterOverlayThk { get; set; }
        public string ActAfterOverlayThk { get; set; }
        public string ReqOverlayThk { get; set; }
        public string Remarks { get; set; }
    }
}