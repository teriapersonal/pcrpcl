﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol035Controller : clsBase
    {
        // GET: PROTOCOL/Protocol035
        string ControllerURL = "/PROTOCOL/Protocol035/";
        string Title = "Nozzle cutout Marking on Shell/Cone/D'end ";
        // GET: PROTOCOL/Protocol035
        #region Details View Code
        #region Header
        [SessionExpireFilter] //, UserPermissions, AllowAnonymous]

        public ActionResult Details(int? id, int? m)
        {
            PRO170 objPRO170 = new PRO170();
            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            List<string> lstYesNoNA = clsImplementationEnum.getYesNoNA().ToList();
            ViewBag.YesNoNAEnum = lstYesNoNA.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            if (!id.HasValue)
            {
                objPRO170.ProtocolNo = string.Empty;
                objPRO170.CreatedBy = objClsLoginInfo.UserName;
                objPRO170.CreatedOn = DateTime.Now;
                db.PRO170.Add(objPRO170);
                db.SaveChanges();
                ViewBag.HeaderId = objPRO170.HeaderId;
            }
            else
            {
                objPRO170 = db.PRO170.Where(x => x.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }

            return View(objPRO170);
        }
        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO170 PRO170)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO170.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO170 objPRO170 = db.PRO170.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO170.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO170.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data
                        objPRO170.ProtocolNo = PRO170.ProtocolNo;
                        objPRO170.ReferenceLineMarked = PRO170.ReferenceLineMarked;
                        objPRO170.CircumferenceRequiredAtTop = PRO170.CircumferenceRequiredAtTop;
                        objPRO170.CircumferenceRequiredAtBottom = PRO170.CircumferenceRequiredAtBottom;
                        objPRO170.ActualTop = PRO170.ActualTop;
                        objPRO170.ActualBottom = PRO170.ActualBottom;
                        objPRO170.ArcLengthTop = PRO170.ArcLengthTop;
                        objPRO170.ArcLengthBottom = PRO170.ArcLengthBottom;
                        objPRO170.TopDegreeReq1 = PRO170.TopDegreeReq1;
                        objPRO170.TopDegreeAct1 = PRO170.TopDegreeAct1;
                        objPRO170.BottomDegree1 = PRO170.BottomDegree1;
                        objPRO170.BottomDegreeReq1 = PRO170.BottomDegreeReq1;
                        objPRO170.BottomDegreeAct1 = PRO170.BottomDegreeAct1;
                        objPRO170.TopDegreeReq2 = PRO170.TopDegreeReq2;
                        objPRO170.TopDegreeAct2 = PRO170.TopDegreeAct2;
                        objPRO170.BottomDegree2 = PRO170.BottomDegree2;
                        objPRO170.BottomDegreeReq2 = PRO170.BottomDegreeReq2;
                        objPRO170.BottomDegreeAct2 = PRO170.BottomDegreeAct2;
                        objPRO170.TopDegreeReq3 = PRO170.TopDegreeReq3;
                        objPRO170.TopDegreeAct3 = PRO170.TopDegreeAct3;
                        objPRO170.BottomDegree3 = PRO170.BottomDegree3;
                        objPRO170.BottomDegreeReq3 = PRO170.BottomDegreeReq3;
                        objPRO170.BottomDegreeAct3 = PRO170.BottomDegreeAct3;
                        objPRO170.TopDegreeReq4 = PRO170.TopDegreeReq4;
                        objPRO170.TopDegreeAct4 = PRO170.TopDegreeAct4;
                        objPRO170.BottomDegree4 = PRO170.BottomDegree4;
                        objPRO170.BottomDegreeReq4 = PRO170.BottomDegreeReq4;
                        objPRO170.BottomDegreeAct4 = PRO170.BottomDegreeAct4;
                        objPRO170.EditedBy = objClsLoginInfo.UserName;
                        objPRO170.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol saved successfully";
                        objResponseMsg.HeaderId = objPRO170.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO170 objPRO170 = db.PRO170.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO170 != null && string.IsNullOrWhiteSpace(objPRO170.ProtocolNo))
                    {
                        db.PRO171.RemoveRange(objPRO170.PRO171.ToList());
                        db.PRO170.Remove(objPRO170);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NOZZLE_CUTOUT_MARKING_ON_SHELL_CONE_DEND.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO170.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #region Line1 details
        [HttpPost]
        public ActionResult GetProtocolLines1Data(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro.NozzleNo like '%" + param.sSearch
                                    + "%' or pro.ElevationReq like '%" + param.sSearch
                                    + "%' or pro.ElevationAct like '%" + param.sSearch
                                    + "%' or pro.ReqDeg like '%" + param.sSearch
                                    + "%' or pro.MainOrient like '%" + param.sSearch
                                    + "%' or pro.ClockAntiClockWise like '%" + param.sSearch
                                    + "%' or pro.RequiredArcLength like '%" + param.sSearch
                                    + "%' or pro.ActualArcLength like '%" + param.sSearch
                                    + "%' or pro.CutReq like '%" + param.sSearch
                                    + "%' or pro.CutAct like '%" + param.sSearch
                                    + "%' or pro.SeamNo like '%" + param.sSearch
                                    + "%' or pro.Remarks like '%" + param.sSearch
                        + "%')";
                }

                var lstResult = db.SP_IPI_PROTOCOL035_GET_LINES1_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                                      "1",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                    "",
                                   Helper.GenerateHTMLTextbox(newRecordId, "NozzleNo", "", "", false, "", false, "50","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "ElevationReq", "", "", false, "", false, "50","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "ElevationAct", "", "", false, "", false, "50","PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "ReqDeg", "", "", false, "", false, "50","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "MainOrient", "", "", false, "", false, "50","PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "ClockAntiClockWise", "", "", false, "", false, "50","PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "RequiredArcLength", "", "", false, "", false, "50","PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "ActualArcLength", "", "", false, "", false, "50","PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "CutReq", "", "", false, "", false, "50","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "CutAct", "", "", false, "", false, "50","PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "SeamNo", "", "", false, "", false, "50","PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Remarks", "", "", false, "", false, "50","QC"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine1(0);")
                                };
                var data = (from uc in lstResult
                            select new[]
                            {
                                "1",
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.LineId),
                                Convert.ToString(uc.ROW_NO),
                                Helper.GenerateHTMLTextbox(uc.LineId, "NozzleNo", Convert.ToString(uc.NozzleNo), "", true, "", false, "50","QC"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "ElevationReq", Convert.ToString(uc.ElevationReq), "", true, "", false, "50","QC"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "ElevationAct", Convert.ToString(uc.ElevationAct), "", true, "", false, "50","PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "ReqDeg", Convert.ToString(uc.ReqDeg), "", true, "", false, "50","QC"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "MainOrient", Convert.ToString(uc.MainOrient), "", true, "", false, "50","PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "ClockAntiClockWise", Convert.ToString(uc.ClockAntiClockWise), "", true, "", false, "50","PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "RequiredArcLength", Convert.ToString(uc.RequiredArcLength), "", true, "", false, "50","PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "ActualArcLength", Convert.ToString(uc.ActualArcLength), "", true, "", false, "50","PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "CutReq", Convert.ToString(uc.CutReq), "", true, "", false, "50","QC"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "CutAct", Convert.ToString(uc.CutAct), "", true, "", false, "50","PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "SeamNo", Convert.ToString(uc.SeamNo), "", true, "", false, "50","PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "Remarks", Convert.ToString(uc.Remarks), "", true, "", false, "50","QC"),
                                Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == "QC3" ? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine1','tblProtocolLines1')"):"")

                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }
        public ActionResult SaveProtocolLine1(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRO171 objPRO171 = new PRO171();

            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;

                if (refHeaderId > 0)
                {
                    string strNozzleNo = !string.IsNullOrEmpty(fc["NozzleNo" + refLineId]) ? Convert.ToString(fc["NozzleNo" + refLineId]).Trim() : "";
                    string strElevationReq = !string.IsNullOrEmpty(fc["ElevationReq" + refLineId]) ? Convert.ToString(fc["ElevationReq" + refLineId]).Trim() : "";
                    string strElevationAct = !string.IsNullOrEmpty(fc["ElevationAct" + refLineId]) ? Convert.ToString(fc["ElevationAct" + refLineId]).Trim() : "";
                    string strReqDeg = !string.IsNullOrEmpty(fc["ReqDeg" + refLineId]) ? Convert.ToString(fc["ReqDeg" + refLineId]).Trim() : "";
                    string strMainOrient = !string.IsNullOrEmpty(fc["MainOrient" + refLineId]) ? Convert.ToString(fc["MainOrient" + refLineId]).Trim() : "";
                    string strClockAntiClockWise = !string.IsNullOrEmpty(fc["ClockAntiClockWise" + refLineId]) ? Convert.ToString(fc["ClockAntiClockWise" + refLineId]).Trim() : "";
                    string strRequiredArcLength = !string.IsNullOrEmpty(fc["RequiredArcLength" + refLineId]) ? Convert.ToString(fc["RequiredArcLength" + refLineId]).Trim() : "";
                    string strActualArcLength = !string.IsNullOrEmpty(fc["ActualArcLength" + refLineId]) ? Convert.ToString(fc["ActualArcLength" + refLineId]).Trim() : "";
                    string strCutReq = !string.IsNullOrEmpty(fc["CutReq" + refLineId]) ? Convert.ToString(fc["CutReq" + refLineId]).Trim() : "";
                    string strCutAct = !string.IsNullOrEmpty(fc["CutAct" + refLineId]) ? Convert.ToString(fc["CutAct" + refLineId]).Trim() : "";
                    string strSeamNo = !string.IsNullOrEmpty(fc["SeamNo" + refLineId]) ? Convert.ToString(fc["SeamNo" + refLineId]).Trim() : "";
                    string strRemarks = !string.IsNullOrEmpty(fc["Remarks" + refLineId]) ? Convert.ToString(fc["Remarks" + refLineId]).Trim() : "";

                    #region Add New Spot Line1
                    if (refLineId > 0)
                    {
                        objPRO171 = db.PRO171.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRO171.EditedBy = objClsLoginInfo.UserName;
                        objPRO171.EditedOn = DateTime.Now;
                    }
                    objPRO171.HeaderId = refHeaderId;
                    objPRO171.NozzleNo = strNozzleNo;
                    objPRO171.ElevationReq = strElevationReq;
                    objPRO171.ElevationAct = strElevationAct;
                    objPRO171.ReqDeg = strReqDeg;
                    objPRO171.MainOrient = strMainOrient;
                    objPRO171.ClockAntiClockWise = strClockAntiClockWise;
                    objPRO171.RequiredArcLength = strRequiredArcLength;
                    objPRO171.ActualArcLength = strActualArcLength;
                    objPRO171.CutReq = strCutReq;
                    objPRO171.CutAct = strCutAct;
                    objPRO171.SeamNo = strSeamNo;
                    objPRO171.Remarks = strRemarks;
                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = "Data updated successfully";
                    }
                    else
                    {
                        objPRO171.CreatedBy = objClsLoginInfo.UserName;
                        objPRO171.CreatedOn = DateTime.Now;
                        db.PRO171.Add(objPRO171);
                        db.SaveChanges();
                        objResponseMsg.Value = "Data added successfully";
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRO171.LineId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeleteProtocolLine1(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRO171 objPRO171 = db.PRO171.FirstOrDefault(c => c.LineId == lineId);
                if (objPRO171 != null)
                {
                    db.PRO171.Remove(objPRO171);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #endregion

        #region Linkage View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL170 objPRL170 = new PRL170();
            objPRL170 = db.PRL170.Where(i => i.HeaderId == id).FirstOrDefault();

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;
            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            List<string> lstYesNoNA = clsImplementationEnum.getYesNoNA().ToList();
            ViewBag.YesNoNAEnum = lstYesNoNA.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            if (objPRL170 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL170.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objQproject.ManufacturingCode;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }
            if (m == 1)
            {
                ViewBag.isEditable = "true";
            }
            else
            { ViewBag.isEditable = "false"; }

            return View(objPRL170);
        }
        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL170 PRL170)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRL170.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL170 objPRL170 = db.PRL170.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    #region Save Data
                    objPRL170.SeamNo = PRL170.SeamNo;
                    objPRL170.EquipmentNo = PRL170.EquipmentNo;
                    objPRL170.InspectionAgency = PRL170.InspectionAgency;
                    objPRL170.DrawingNo = PRL170.DrawingNo;
                    objPRL170.DrawingRevisionNo = PRL170.DrawingRevisionNo;
                    objPRL170.ManufacturingCode = PRL170.ManufacturingCode;
                    objPRL170.DCRNo = PRL170.DCRNo;
                    objPRL170.ProtocolNo = PRL170.ProtocolNo;
                    objPRL170.ReferenceLineMarked = PRL170.ReferenceLineMarked;
                    objPRL170.CircumferenceRequiredAtTop = PRL170.CircumferenceRequiredAtTop;
                    objPRL170.CircumferenceRequiredAtBottom = PRL170.CircumferenceRequiredAtBottom;
                    objPRL170.ActualTop = PRL170.ActualTop;
                    objPRL170.ActualBottom = PRL170.ActualBottom;
                    objPRL170.ArcLengthTop = PRL170.ArcLengthTop;
                    objPRL170.ArcLengthBottom = PRL170.ArcLengthBottom;
                    objPRL170.TopDegreeReq1 = PRL170.TopDegreeReq1;
                    objPRL170.TopDegreeAct1 = PRL170.TopDegreeAct1;
                    objPRL170.BottomDegree1 = PRL170.BottomDegree1;
                    objPRL170.BottomDegreeReq1 = PRL170.BottomDegreeReq1;
                    objPRL170.BottomDegreeAct1 = PRL170.BottomDegreeAct1;
                    objPRL170.TopDegreeReq2 = PRL170.TopDegreeReq2;
                    objPRL170.TopDegreeAct2 = PRL170.TopDegreeAct2;
                    objPRL170.BottomDegree2 = PRL170.BottomDegree2;
                    objPRL170.BottomDegreeReq2 = PRL170.BottomDegreeReq2;
                    objPRL170.BottomDegreeAct2 = PRL170.BottomDegreeAct2;
                    objPRL170.TopDegreeReq3 = PRL170.TopDegreeReq3;
                    objPRL170.TopDegreeAct3 = PRL170.TopDegreeAct3;
                    objPRL170.BottomDegree3 = PRL170.BottomDegree3;
                    objPRL170.BottomDegreeReq3 = PRL170.BottomDegreeReq3;
                    objPRL170.BottomDegreeAct3 = PRL170.BottomDegreeAct3;
                    objPRL170.TopDegreeReq4 = PRL170.TopDegreeReq4;
                    objPRL170.TopDegreeAct4 = PRL170.TopDegreeAct4;
                    objPRL170.BottomDegree4 = PRL170.BottomDegree4;
                    objPRL170.BottomDegreeReq4 = PRL170.BottomDegreeReq4;
                    objPRL170.BottomDegreeAct4 = PRL170.BottomDegreeAct4;
                    objPRL170.EditedBy = objClsLoginInfo.UserName;
                    objPRL170.EditedOn = DateTime.Now;
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol saved successfully";
                    objResponseMsg.HeaderId = objPRL170.HeaderId;
                    #endregion

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL170 objPRL170 = db.PRL170.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL170 != null && string.IsNullOrWhiteSpace(objPRL170.ProtocolNo))
                    {
                        db.PRL171.RemoveRange(objPRL170.PRL171.ToList());
                        db.PRL170.Remove(objPRL170);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL170.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #region Line1 Linkage
        [HttpPost]
        public ActionResult GetProtocolLines1DataLinkage(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro.NozzleNo like '%" + param.sSearch
                                    + "%' or pro.ElevationReq like '%" + param.sSearch
                                    + "%' or pro.ElevationAct like '%" + param.sSearch
                                    + "%' or pro.ReqDeg like '%" + param.sSearch
                                    + "%' or pro.MainOrient like '%" + param.sSearch
                                    + "%' or pro.ClockAntiClockWise like '%" + param.sSearch
                                    + "%' or pro.RequiredArcLength like '%" + param.sSearch
                                    + "%' or pro.ActualArcLength like '%" + param.sSearch
                                    + "%' or pro.CutReq like '%" + param.sSearch
                                    + "%' or pro.CutAct like '%" + param.sSearch
                                    + "%' or pro.SeamNo like '%" + param.sSearch
                                    + "%' or pro.Remarks like '%" + param.sSearch
                        + "%')";
                }

                var lstResult = db.SP_IPI_PROTOCOL035_LINKAGE_GET_LINES1_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                                      "1",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                    "",
                                   Helper.GenerateHTMLTextbox(newRecordId, "NozzleNo", "", "", false, "", false, "50","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "ElevationReq", "", "", false, "", false, "50","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "ElevationAct", "", "", false, "", false, "50","PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "ReqDeg", "", "", false, "", false, "50","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "MainOrient", "", "", false, "", false, "50","PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "ClockAntiClockWise", "", "", false, "", false, "50","PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "RequiredArcLength", "", "", false, "", false, "50","PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "ActualArcLength", "", "", false, "", false, "50","PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "CutReq", "", "", false, "", false, "50","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "CutAct", "", "", false, "", false, "50","PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "SeamNo", "", "", false, "", false, "50","PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Remarks", "", "", false, "", false, "50","QC"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine1(0);")
                                };
                var data = (from uc in lstResult
                            select new[]
                            {
                                "1",
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.LineId),
                                Convert.ToString(uc.ROW_NO),
                                Helper.GenerateHTMLTextbox(uc.LineId, "NozzleNo", Convert.ToString(uc.NozzleNo), "", true, "", false, "50","QC"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "ElevationReq", Convert.ToString(uc.ElevationReq), "", true, "", false, "50","QC"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "ElevationAct", Convert.ToString(uc.ElevationAct), "", true, "", false, "50","PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "ReqDeg", Convert.ToString(uc.ReqDeg), "", true, "", false, "50","QC"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "MainOrient", Convert.ToString(uc.MainOrient), "", true, "", false, "50","PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "ClockAntiClockWise", Convert.ToString(uc.ClockAntiClockWise), "", true, "", false, "50","PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "RequiredArcLength", Convert.ToString(uc.RequiredArcLength), "", true, "", false, "50","PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "ActualArcLength", Convert.ToString(uc.ActualArcLength), "", true, "", false, "50","PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "CutReq", Convert.ToString(uc.CutReq), "", true, "", false, "50","QC"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "CutAct", Convert.ToString(uc.CutAct), "", true, "", false, "50","PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "SeamNo", Convert.ToString(uc.SeamNo), "", true, "", false, "50","PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "Remarks", Convert.ToString(uc.Remarks), "", true, "", false, "50","QC"),
                                Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == "QC3" ? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine1Linkage','tblProtocolLines1')"):"")

                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult SaveProtocolLine1Linkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRL171 objPRL171 = new PRL171();

            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
                if (refHeaderId > 0)
                {
                    string strNozzleNo = !string.IsNullOrEmpty(fc["NozzleNo" + refLineId]) ? Convert.ToString(fc["NozzleNo" + refLineId]).Trim() : "";
                    string strElevationReq = !string.IsNullOrEmpty(fc["ElevationReq" + refLineId]) ? Convert.ToString(fc["ElevationReq" + refLineId]).Trim() : "";
                    string strElevationAct = !string.IsNullOrEmpty(fc["ElevationAct" + refLineId]) ? Convert.ToString(fc["ElevationAct" + refLineId]).Trim() : "";
                    string strReqDeg = !string.IsNullOrEmpty(fc["ReqDeg" + refLineId]) ? Convert.ToString(fc["ReqDeg" + refLineId]).Trim() : "";
                    string strMainOrient = !string.IsNullOrEmpty(fc["MainOrient" + refLineId]) ? Convert.ToString(fc["MainOrient" + refLineId]).Trim() : "";
                    string strClockAntiClockWise = !string.IsNullOrEmpty(fc["ClockAntiClockWise" + refLineId]) ? Convert.ToString(fc["ClockAntiClockWise" + refLineId]).Trim() : "";
                    string strRequiredArcLength = !string.IsNullOrEmpty(fc["RequiredArcLength" + refLineId]) ? Convert.ToString(fc["RequiredArcLength" + refLineId]).Trim() : "";
                    string strActualArcLength = !string.IsNullOrEmpty(fc["ActualArcLength" + refLineId]) ? Convert.ToString(fc["ActualArcLength" + refLineId]).Trim() : "";
                    string strCutReq = !string.IsNullOrEmpty(fc["CutReq" + refLineId]) ? Convert.ToString(fc["CutReq" + refLineId]).Trim() : "";
                    string strCutAct = !string.IsNullOrEmpty(fc["CutAct" + refLineId]) ? Convert.ToString(fc["CutAct" + refLineId]).Trim() : "";
                    string strSeamNo = !string.IsNullOrEmpty(fc["SeamNo" + refLineId]) ? Convert.ToString(fc["SeamNo" + refLineId]).Trim() : "";
                    string strRemarks = !string.IsNullOrEmpty(fc["Remarks" + refLineId]) ? Convert.ToString(fc["Remarks" + refLineId]).Trim() : "";


                    #region Add New Spot Line1
                    if (refLineId > 0)
                    {
                        objPRL171 = db.PRL171.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRL171.EditedBy = objClsLoginInfo.UserName;
                        objPRL171.EditedOn = DateTime.Now;
                    }
                    objPRL171.HeaderId = refHeaderId;
                    objPRL171.NozzleNo = strNozzleNo;
                    objPRL171.ElevationReq = strElevationReq;
                    objPRL171.ElevationAct = strElevationAct;
                    objPRL171.ReqDeg = strReqDeg;
                    objPRL171.MainOrient = strMainOrient;
                    objPRL171.ClockAntiClockWise = strClockAntiClockWise;
                    objPRL171.RequiredArcLength = strRequiredArcLength;
                    objPRL171.ActualArcLength = strActualArcLength;
                    objPRL171.CutReq = strCutReq;
                    objPRL171.CutAct = strCutAct;
                    objPRL171.SeamNo = strSeamNo;
                    objPRL171.Remarks = strRemarks;

                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = "Data updated successfully";
                    }
                    else
                    {
                        objPRL171.CreatedBy = objClsLoginInfo.UserName;
                        objPRL171.CreatedOn = DateTime.Now;
                        db.PRL171.Add(objPRL171);
                        db.SaveChanges();
                        objResponseMsg.Value = "Data added successfully";
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRL171.LineId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeleteProtocolLine1Linkage(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRL171 objPRL171 = db.PRL171.FirstOrDefault(c => c.LineId == lineId);
                if (objPRL171 != null)
                {
                    db.PRL171.Remove(objPRL171);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #endregion
    }
}