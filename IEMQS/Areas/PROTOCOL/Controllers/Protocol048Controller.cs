﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol048Controller : clsBase
    {
        // GET: PROTOCOL/Protocol048
        string ControllerURL = "/PROTOCOL/Protocol048/";
        string Title = "SET-UP AND DIMENSION REPORT FOR ATTACHMENT";

        #region Details View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO235 objPRO235 = new PRO235();
            if (!id.HasValue)
            {
                try
                {
                    objPRO235.ProtocolNo = string.Empty;
                    objPRO235.CreatedBy = objClsLoginInfo.UserName;
                    objPRO235.CreatedOn = DateTime.Now;

                    db.PRO235.Add(objPRO235);
                    db.SaveChanges();
                    // return View();

                }

                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO235.HeaderId;
            }
            else
            {
                objPRO235 = db.PRO235.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();


            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();



            //  ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO236> lstPRO236 = db.PRO236.Where(x => x.HeaderId == objPRO235.HeaderId).ToList();



            ViewBag.lstPRO236 = lstPRO236;



            #endregion
            return View(objPRO235);

        }

        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO235 PRO235)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO235.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO235 objPRO235 = db.PRO235.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO235.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO235.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data

                        objPRO235.ProtocolNo = PRO235.ProtocolNo;
                        objPRO235.ActualCfOfShell = PRO235.ActualCfOfShell;
                        objPRO235.ReqElevation = PRO235.ReqElevation;
                        objPRO235.ToleranceReqArcLength = PRO235.ToleranceReqArcLength;
                        objPRO235.ReqProjection = PRO235.ReqProjection;
                        objPRO235.ReqTilt = PRO235.ReqTilt;
                        objPRO235.ReqRootGap = PRO235.ReqRootGap;
                        objPRO235.ReqRootFace = PRO235.ReqRootFace;
                        objPRO235.ReqWepAngle = PRO235.ReqWepAngle;
                        objPRO235.ReqSize = PRO235.ReqSize;
                        objPRO235.CheckPoint1 = PRO235.CheckPoint1;
                        objPRO235.CheckPoint2 = PRO235.CheckPoint2;
                        objPRO235.CheckPoint3 = PRO235.CheckPoint3;
                        objPRO235.CheckPoint4 = PRO235.CheckPoint4;
                        objPRO235.CheckPoint6 = PRO235.CheckPoint6;
                        objPRO235.CheckPoint7 = PRO235.CheckPoint7;
                        objPRO235.CheckPoint7_2 = PRO235.CheckPoint7_2;
                        objPRO235.QCRemarks = PRO235.QCRemarks;
                        objPRO235.Result = PRO235.Result;
                        objPRO235.EditedBy = objClsLoginInfo.UserName;
                        objPRO235.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO235.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO235 objPRO235 = db.PRO235.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO235 != null && string.IsNullOrWhiteSpace(objPRO235.ProtocolNo))
                    {
                        db.PRO235.Remove(objPRO235);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO235.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO236> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO236> lstAddPRO236 = new List<PRO236>();
                List<PRO236> lstDeletePRO236 = new List<PRO236>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO236 obj = db.PRO236.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {


                                obj.Description = item.Description;
                                obj.Elevation = item.Elevation;
                                obj.ReqOrientation = item.ReqOrientation;
                                obj.PrincipleOrientation = item.PrincipleOrientation;
                                obj.DiffBtPrincipleOrientOnAndReqorient = item.DiffBtPrincipleOrientOnAndReqorient;
                                obj.ReqArcLength = item.ReqArcLength;
                                obj.ActArcLength = item.ActArcLength;
                                obj.ActOrientation = item.ActOrientation;
                                obj.Projection = item.Projection;
                                obj.Tilt = item.Tilt;
                                obj.RootGap = item.RootGap;
                                obj.RootFace = item.RootFace;
                                obj.WepAngle = item.WepAngle;
                                obj.Size = item.Size;
                                obj.Identification = item.Identification;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO236 obj = new PRO236();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.Description))
                            {
                                obj.Description = item.Description;
                                obj.Elevation = item.Elevation;
                                obj.ReqOrientation = item.ReqOrientation;
                                obj.PrincipleOrientation = item.PrincipleOrientation;
                                obj.DiffBtPrincipleOrientOnAndReqorient = item.DiffBtPrincipleOrientOnAndReqorient;
                                obj.ReqArcLength = item.ReqArcLength;
                                obj.ActArcLength = item.ActArcLength;
                                obj.ActOrientation = item.ActOrientation;
                                obj.Projection = item.Projection;
                                obj.Tilt = item.Tilt;
                                obj.RootGap = item.RootGap;
                                obj.RootFace = item.RootFace;
                                obj.WepAngle = item.WepAngle;
                                obj.Size = item.Size;
                                obj.Identification = item.Identification;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO236.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO236.Count > 0)
                    {
                        db.PRO236.AddRange(lstAddPRO236);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO236 = db.PRO236.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO236.Count > 0)
                    {
                        db.PRO236.RemoveRange(lstDeletePRO236);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO236 = db.PRO236.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO236.Count > 0)
                    {
                        db.PRO236.RemoveRange(lstDeletePRO236);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region Linkage View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL235 objPRL235 = new PRL235();
            objPRL235 = db.PRL235.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL235 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL235.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;

                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();



            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL236> lstPRL236 = db.PRL236.Where(x => x.HeaderId == objPRL235.HeaderId).ToList();


            ViewBag.lstPRL236 = lstPRL236;


            #endregion
            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.lstDrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL235.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL235.ActFilledBy) && (objPRL235.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL235.Project).ToList();
                            ViewBag.lstDrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.lstDrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL235);
        }

        [HttpPost]
        public ActionResult LinkageMobile(int? id, int? m, string APIRequestFrom = "", string UserName = "")
        {
            string ProjectDesc = "";
            string ManufacturingCode = "";
            string AccessRole = "";
            bool isEditable = false;
            var DrawingNo = new List<string>();
            bool isQCRemarkEditable = false;

            PRL235 objPRL235 = new PRL235();

            db.Configuration.ProxyCreationEnabled = false;
            objPRL235 = db.PRL235.Where(i => i.HeaderId == id).FirstOrDefault();
            db.Entry(objPRL235).State = System.Data.Entity.EntityState.Detached;

            NDEModels objNDEModels = new NDEModels();
            if (objPRL235 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL235.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
            AccessRole = objUserRoleAccessDetails.UserDesignation;


            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();



            var YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            var LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL236> lstPRL236 = db.PRL236.Where(x => x.HeaderId == objPRL235.HeaderId).ToList();

            #endregion
            try
            {
                isEditable = false;
                isQCRemarkEditable = false;
                if (m == 1)
                {
                    isEditable = true;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL235.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL235.ActFilledBy) && (objPRL235.RequestNo.HasValue))
                        {
                            isEditable = false;
                            isQCRemarkEditable = true;
                        }
                        else
                        {
                            DrawingNo = (new GeneralController()).GetPLMDrawingNumber(objPRL235.Project).ToList();
                        }
                    }
                }
                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    ProjectDesc = ProjectDesc,
                    ManufacturingCode = ManufacturingCode,
                    isEditable = isEditable,
                    isQCRemarkEditable = isQCRemarkEditable,
                    DrawingNo = DrawingNo,
                    AccessRole = AccessRole,
                    LeftRight = LeftRight,
                    OutsideInside = OutsideInside,

                    YesNoEnum = YesNoEnum,
                    YesNAEnum = YesNAEnum,

                    objPRL235 = objPRL235,

                    lstPRL236 = lstPRL236

                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.lstDrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();

                return Json(new
                {
                    res = "0",
                    msg = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL235 PRL235, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = PRL235.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL235 objPRL235 = db.PRL235.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    #region Save Data

                    objPRL235.DrawingRevisionNo = PRL235.DrawingRevisionNo;
                    objPRL235.DrawingNo = PRL235.DrawingNo;
                    objPRL235.DCRNo = PRL235.DCRNo;
                    objPRL235.ProtocolNo = PRL235.ProtocolNo != null ? PRL235.ProtocolNo : "";


                    objPRL235.ActualCfOfShell = PRL235.ActualCfOfShell;
                    objPRL235.ReqElevation = PRL235.ReqElevation;
                    objPRL235.ToleranceReqArcLength = PRL235.ToleranceReqArcLength;
                    objPRL235.ReqProjection = PRL235.ReqProjection;
                    objPRL235.ReqTilt = PRL235.ReqTilt;
                    objPRL235.ReqRootGap = PRL235.ReqRootGap;
                    objPRL235.ReqRootFace = PRL235.ReqRootFace;
                    objPRL235.ReqWepAngle = PRL235.ReqWepAngle;
                    objPRL235.ReqSize = PRL235.ReqSize;
                    objPRL235.CheckPoint1 = PRL235.CheckPoint1;
                    objPRL235.CheckPoint2 = PRL235.CheckPoint2;
                    objPRL235.CheckPoint3 = PRL235.CheckPoint3;
                    objPRL235.CheckPoint4 = PRL235.CheckPoint4;
                    objPRL235.CheckPoint6 = PRL235.CheckPoint6;
                    objPRL235.CheckPoint7 = PRL235.CheckPoint7;
                    objPRL235.CheckPoint7_2 = PRL235.CheckPoint7_2;
                    objPRL235.QCRemarks = PRL235.QCRemarks;
                    objPRL235.Result = PRL235.Result;
                    objPRL235.EditedBy = UserName;
                    objPRL235.EditedOn = DateTime.Now;


                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL235.ActFilledBy = UserName;
                            objPRL235.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL235.ReqFilledBy = UserName;
                            objPRL235.ReqFilledOn = DateTime.Now;
                        }
                    }
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL235.HeaderId;
                    #endregion

                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL235 objPRL235 = db.PRL235.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL235 != null && string.IsNullOrWhiteSpace(objPRL235.ProtocolNo))
                    {
                        db.PRL235.Remove(objPRL235);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL235.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL236> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL236> lstAddPRL236 = new List<PRL236>();
                List<PRL236> lstDeletePRL236 = new List<PRL236>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRL236 obj = db.PRL236.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.Description = item.Description;
                                obj.Elevation = item.Elevation;
                                obj.ReqOrientation = item.ReqOrientation;
                                obj.PrincipleOrientation = item.PrincipleOrientation;
                                obj.DiffBtPrincipleOrientOnAndReqorient = item.DiffBtPrincipleOrientOnAndReqorient;
                                obj.ReqArcLength = item.ReqArcLength;
                                obj.ActArcLength = item.ActArcLength;
                                obj.ActOrientation = item.ActOrientation;
                                obj.Projection = item.Projection;
                                obj.Tilt = item.Tilt;
                                obj.RootGap = item.RootGap;
                                obj.RootFace = item.RootFace;
                                obj.WepAngle = item.WepAngle;
                                obj.Size = item.Size;
                                obj.Identification = item.Identification;
                                obj.EditedBy = UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRL236 obj = new PRL236();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.Description))
                            {
                                obj.Description = item.Description;
                                obj.Elevation = item.Elevation;
                                obj.ReqOrientation = item.ReqOrientation;
                                obj.PrincipleOrientation = item.PrincipleOrientation;
                                obj.DiffBtPrincipleOrientOnAndReqorient = item.DiffBtPrincipleOrientOnAndReqorient;
                                obj.ReqArcLength = item.ReqArcLength;
                                obj.ActArcLength = item.ActArcLength;
                                obj.ActOrientation = item.ActOrientation;
                                obj.Projection = item.Projection;
                                obj.Tilt = item.Tilt;
                                obj.RootGap = item.RootGap;
                                obj.RootFace = item.RootFace;
                                obj.WepAngle = item.WepAngle;
                                obj.Size = item.Size;
                                obj.Identification = item.Identification;
                                obj.CreatedBy = UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRL236.Add(obj);
                            }
                        }
                    }
                }
                if (lstAddPRL236.Count > 0)
                {
                    db.PRL236.AddRange(lstAddPRL236);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL236 = db.PRL236.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL236 = db.PRL236.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL236.Count > 0)
                {
                    db.PRL236.RemoveRange(lstDeletePRL236);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

    }
}