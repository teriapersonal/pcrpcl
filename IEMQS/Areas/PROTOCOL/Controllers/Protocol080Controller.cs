﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol080Controller : clsBase
    {
        // GET: PROTOCOL/Protocol080
        string ControllerURL = "/PROTOCOL/Protocol080/";
        string Title = "REPORT FOR VISUAL AND DIMENSION INSPECTION FOR TAPER TYPE NUB AFTER OVERLAY";


        #region Details View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO395 objPRO395 = new PRO395();

            if (!id.HasValue)
            {
                try
                {
                    objPRO395.ProtocolNo = string.Empty;
                    objPRO395.CreatedBy = objClsLoginInfo.UserName;
                    objPRO395.CreatedOn = DateTime.Now;

                    #region  OrientationReqNubID | PRO395
                    objPRO395.PRO397.Add(new PRO397
                    {

                        OrientationReqNubID = "0°-180°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO395.PRO397.Add(new PRO397
                    {

                        OrientationReqNubID = "22.5°-202.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO395.PRO397.Add(new PRO397
                    {

                        OrientationReqNubID = "45°-225°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO395.PRO397.Add(new PRO397
                    {

                        OrientationReqNubID = "67.5°-247.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO395.PRO397.Add(new PRO397
                    {

                        OrientationReqNubID = "90°-270°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO395.PRO397.Add(new PRO397
                    {

                        OrientationReqNubID = "112.5°-292.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO395.PRO397.Add(new PRO397
                    {

                        OrientationReqNubID = "135°-315°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO395.PRO397.Add(new PRO397
                    {

                        OrientationReqNubID = "157.5°-337.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });


                    #endregion

                    #region  OrientationAllowedShellOutOfRoundness | PRO395
                    objPRO395.PRO398.Add(new PRO398
                    {

                        OrientationAllowedShellOutOfRoundness = "0°-180°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO395.PRO398.Add(new PRO398
                    {

                        OrientationAllowedShellOutOfRoundness = "22.5°-202.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO395.PRO398.Add(new PRO398
                    {

                        OrientationAllowedShellOutOfRoundness = "45°-225°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO395.PRO398.Add(new PRO398
                    {

                        OrientationAllowedShellOutOfRoundness = "67.5°-247.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO395.PRO398.Add(new PRO398
                    {

                        OrientationAllowedShellOutOfRoundness = "90°-270°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO395.PRO398.Add(new PRO398
                    {

                        OrientationAllowedShellOutOfRoundness = "112.5°-292.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO395.PRO398.Add(new PRO398
                    {

                        OrientationAllowedShellOutOfRoundness = "135°-315°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO395.PRO398.Add(new PRO398
                    {

                        OrientationAllowedShellOutOfRoundness = "157.5°-337.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });


                    #endregion

                    #region  OtherDimensions | PRO395
                    objPRO395.PRO399.Add(new PRO399
                    {

                        OtherDimensions = "DEPTH OF BARRIER LAYER FROM OVERLAY ON NUB FACE (TOP SIDE )",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO395.PRO399.Add(new PRO399
                    {

                        OtherDimensions = "DEPTH OF BARRIER LAYER FROM OVERLAY ON NUB FACE  (BOTTOM SIDE )S",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO395.PRO399.Add(new PRO399
                    {

                        OtherDimensions = "DEPTH OF BARRIER LAYER FROM OVERLAY ON SHELL I.D. (TOP SIDE )",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO395.PRO399.Add(new PRO399
                    {

                        OtherDimensions = "DEPTH OF BARRIER LAYER FROM OVERLAY ON SHELL I.D. (BOTTOM SIDE )",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });

                    objPRO395.PRO399.Add(new PRO399
                    {

                        OtherDimensions = "RADIUS ON BARRIER LAYER (TOP SIDE)",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });

                    objPRO395.PRO399.Add(new PRO399
                    {

                        OtherDimensions = "RADIUS ON BARRIER LAYER (BOTTOM SIDE)",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });

                    objPRO395.PRO399.Add(new PRO399
                    {

                        OtherDimensions = "TAPER ANGLE",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });

                    objPRO395.PRO399.Add(new PRO399
                    {

                        OtherDimensions = "SURFACE FINISH ON MACHINED SURFACES",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });



                    #endregion

                    db.PRO395.Add(objPRO395);
                    db.SaveChanges();
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO395.HeaderId;

                List<PRO391> _lstPRO391 = db.PRO391.Where(w => w.HeaderId == objPRO395.HeaderId).ToList();



                for (int i = 0; i < _lstPRO391.Count; i++)
                {
                    PRO396 objPRO396Copy = new PRO396();

                    objPRO396Copy.HeaderId = objPRO395.HeaderId;

                    objPRO396Copy.Orientation = _lstPRO391[i].Orientation;
                    objPRO396Copy.ActTolReqElevationOfNubFrom = "";
                    objPRO396Copy.ActTolReqElevationOfTopSide = "";
                    objPRO396Copy.ActTolReqNubWidth = "";
                    objPRO396Copy.ActTolReqNubHeight = "";
                    objPRO396Copy.A1DimensionsBefore = _lstPRO391[i].A1DimensionsChecked;
                    objPRO396Copy.A2DimensionsBefore = _lstPRO391[i].A2DimensionsChecked;
                    objPRO396Copy.B1DimensionsBefore = _lstPRO391[i].B1DimensionsChecked;
                    objPRO396Copy.B2DimensionsBefore = _lstPRO391[i].B2DimensionsChecked;
                    objPRO396Copy.C1DimensionsBefore = _lstPRO391[i].C1DimensionsChecked;
                    objPRO396Copy.C2DimensionsBefore = _lstPRO391[i].C2DimensionsChecked;
                    objPRO396Copy.AA1DimensionsAfter = "";
                    objPRO396Copy.AA2DimensionsAfter = "";
                    objPRO396Copy.BB1DimensionsAfter = "";
                    objPRO396Copy.BB2DimensionsAfter = "";
                    objPRO396Copy.CC1DimensionsAfter = "";
                    objPRO396Copy.CC2DimensionsAfter = "";
                    objPRO396Copy.A1NubTopSide = "";
                    objPRO396Copy.A2NubTopSide = "";
                    objPRO396Copy.B1NubID = "";
                    objPRO396Copy.B2NubID = "";
                    objPRO396Copy.C1NubBottomSide = "";
                    objPRO396Copy.C2NubBottomSide = "";

                    objPRO396Copy.CreatedBy = objClsLoginInfo.UserName;
                    objPRO396Copy.CreatedOn = DateTime.Now;

                    db.PRO396.Add(objPRO396Copy);
                    db.SaveChanges();
                }
            }
            else
            {
                objPRO395 = db.PRO395.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstElevationOfNubFrom = new List<string> { "BOTTOM SIDE WEP", "BOTTOM REF. LINE", "B.T.L" };
            List<string> lstElevationOfTopSide = new List<string> { "BOTTOM SIDE WEP", "BOTTOM REF. LINE", "B.T.L" };

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.ElevationOfNubFrom = lstElevationOfNubFrom.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.ElevationOfTopSide = lstElevationOfTopSide.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO396> lstPRO396 = db.PRO396.Where(x => x.HeaderId == objPRO395.HeaderId).ToList();
            List<PRO397> lstPRO397 = db.PRO397.Where(x => x.HeaderId == objPRO395.HeaderId).ToList();
            List<PRO398> lstPRO398 = db.PRO398.Where(x => x.HeaderId == objPRO395.HeaderId).ToList();
            List<PRO399> lstPRO399 = db.PRO399.Where(x => x.HeaderId == objPRO395.HeaderId).ToList();

            ViewBag.lstPRO396 = lstPRO396;
            ViewBag.lstPRO397 = lstPRO397;
            ViewBag.lstPRO398 = lstPRO398;
            ViewBag.lstPRO399 = lstPRO399;


            #endregion

            return View(objPRO395);
        }

        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO395 PRO395)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO395.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO395 objPRO395 = db.PRO395.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO395.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO395.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data
                        objPRO395.ProtocolNo = PRO395.ProtocolNo;
                        objPRO395.ElevationOfNubFrom = PRO395.ElevationOfNubFrom;
                        objPRO395.ElevationOfTopSide = PRO395.ElevationOfTopSide;
                        objPRO395.ReqElevationOfNubFrom = PRO395.ReqElevationOfNubFrom;
                        objPRO395.ReqElevationOfTopSide = PRO395.ReqElevationOfTopSide;
                        objPRO395.ReqNubWidth = PRO395.ReqNubWidth;
                        objPRO395.ReqNubHeight = PRO395.ReqNubHeight;
                        objPRO395.TolReqElevationOfNubFrom = PRO395.TolReqElevationOfNubFrom;
                        objPRO395.TolReqElevationOfTopSide = PRO395.TolReqElevationOfTopSide;
                        objPRO395.TolReqNubWidth = PRO395.TolReqNubWidth;
                        objPRO395.TolReqNubHeight = PRO395.TolReqNubHeight;
                        objPRO395.ReqNubTopSideOverlay = PRO395.ReqNubTopSideOverlay;
                        objPRO395.ReqNubIDOverlay = PRO395.ReqNubIDOverlay;
                        objPRO395.ReqNubBottomSideOverlay = PRO395.ReqNubBottomSideOverlay;
                        objPRO395.NubID = PRO395.NubID;
                        objPRO395.ReqNubID = PRO395.ReqNubID;
                        objPRO395.OutByNubID = PRO395.OutByNubID;
                        objPRO395.ShellOutOfRoundness = PRO395.ShellOutOfRoundness;
                        objPRO395.AllowedShellOutOfRoundness = PRO395.AllowedShellOutOfRoundness;
                        objPRO395.OutByShellOutOfRoundness = PRO395.OutByShellOutOfRoundness;
                        objPRO395.RequiredOverlayThickness = PRO395.RequiredOverlayThickness;
                        objPRO395.CheckPoint1 = PRO395.CheckPoint1;
                        objPRO395.CheckPoint2 = PRO395.CheckPoint2;
                        objPRO395.CheckPoint3 = PRO395.CheckPoint3;
                        objPRO395.CheckPoint4 = PRO395.CheckPoint4;
                        objPRO395.CheckPoint5 = PRO395.CheckPoint5;
                        objPRO395.CheckPoint6 = PRO395.CheckPoint6;
                        objPRO395.CheckPoint8 = PRO395.CheckPoint8;
                        objPRO395.CheckPoint8_2 = PRO395.CheckPoint8_2;
                        objPRO395.QCRemarks = PRO395.QCRemarks;
                        objPRO395.Result = PRO395.Result;
                        objPRO395.EditedBy = PRO395.EditedBy;
                        objPRO395.EditedOn = PRO395.EditedOn;
                        objPRO395.EditedBy = objClsLoginInfo.UserName;
                        objPRO395.EditedOn = DateTime.Now;



                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO395.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO395 objPRO395 = db.PRO395.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO395 != null && string.IsNullOrWhiteSpace(objPRO395.ProtocolNo))
                    {
                        db.PRO395.Remove(objPRO395);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO395.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO396> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO396> lstAddPRO396 = new List<PRO396>();
                List<PRO396> lstDeletePRO396 = new List<PRO396>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO396 obj = db.PRO396.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO396();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Orientation = item.Orientation;
                        obj.ActTolReqElevationOfNubFrom = item.ActTolReqElevationOfNubFrom;
                        obj.ActTolReqElevationOfTopSide = item.ActTolReqElevationOfTopSide;
                        obj.ActTolReqNubWidth = item.ActTolReqNubWidth;
                        obj.ActTolReqNubHeight = item.ActTolReqNubHeight;
                        obj.A1DimensionsBefore = item.A1DimensionsBefore;
                        obj.A2DimensionsBefore = item.A2DimensionsBefore;
                        obj.B1DimensionsBefore = item.B1DimensionsBefore;
                        obj.B2DimensionsBefore = item.B2DimensionsBefore;
                        obj.C1DimensionsBefore = item.C1DimensionsBefore;
                        obj.C2DimensionsBefore = item.C2DimensionsBefore;
                        obj.AA1DimensionsAfter = item.AA1DimensionsAfter;
                        obj.AA2DimensionsAfter = item.AA2DimensionsAfter;
                        obj.BB1DimensionsAfter = item.BB1DimensionsAfter;
                        obj.BB2DimensionsAfter = item.BB2DimensionsAfter;
                        obj.CC1DimensionsAfter = item.CC1DimensionsAfter;
                        obj.CC2DimensionsAfter = item.CC2DimensionsAfter;
                        obj.A1NubTopSide = item.A1NubTopSide;
                        obj.A2NubTopSide = item.A2NubTopSide;
                        obj.B1NubID = item.B1NubID;
                        obj.B2NubID = item.B2NubID;
                        obj.C1NubBottomSide = item.C1NubBottomSide;
                        obj.C2NubBottomSide = item.C2NubBottomSide;





                        if (isAdded)
                        {
                            lstAddPRO396.Add(obj);
                        }
                    }
                    if (lstAddPRO396.Count > 0)
                    {
                        db.PRO396.AddRange(lstAddPRO396);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO396 = db.PRO396.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO396.Count > 0)
                    {
                        db.PRO396.RemoveRange(lstDeletePRO396);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO396 = db.PRO396.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO396.Count > 0)
                    {
                        db.PRO396.RemoveRange(lstDeletePRO396);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2(List<PRO397> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO397> lstAddPRO397 = new List<PRO397>();
                List<PRO397> lstDeletePRO397 = new List<PRO397>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO397 obj = db.PRO397.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO397();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }


                        if (!string.IsNullOrWhiteSpace(item.OrientationReqNubID))
                        {


                            obj.OrientationReqNubID = item.OrientationReqNubID;
                            obj.ActualNubID = item.ActualNubID;


                            if (isAdded)
                            {
                                lstAddPRO397.Add(obj);
                            }
                        }


                    }
                    if (lstAddPRO397.Count > 0)
                    {
                        db.PRO397.AddRange(lstAddPRO397);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO397 = db.PRO397.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO397.Count > 0)
                    {
                        db.PRO397.RemoveRange(lstDeletePRO397);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO397 = db.PRO397.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO397.Count > 0)
                    {
                        db.PRO397.RemoveRange(lstDeletePRO397);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3(List<PRO398> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO398> lstAddPRO398 = new List<PRO398>();
                List<PRO398> lstDeletePRO398 = new List<PRO398>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO398 obj = db.PRO398.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO398();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        if (!string.IsNullOrWhiteSpace(item.OrientationAllowedShellOutOfRoundness))
                        {
                            obj.OrientationAllowedShellOutOfRoundness = item.OrientationAllowedShellOutOfRoundness;
                            obj.ActualShellOutOfRoundness = item.ActualShellOutOfRoundness;

                            if (isAdded)
                            {
                                lstAddPRO398.Add(obj);
                            }
                        }



                    }
                    if (lstAddPRO398.Count > 0)
                    {
                        db.PRO398.AddRange(lstAddPRO398);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO398 = db.PRO398.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO398.Count > 0)
                    {
                        db.PRO398.RemoveRange(lstDeletePRO398);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO398 = db.PRO398.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO398.Count > 0)
                    {
                        db.PRO398.RemoveRange(lstDeletePRO398);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line4 details
        [HttpPost]
        public JsonResult SaveProtocolLine4(List<PRO399> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO399> lstAddPRO399 = new List<PRO399>();
                List<PRO399> lstDeletePRO399 = new List<PRO399>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO399 obj = db.PRO399.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO399();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.OtherDimensions = item.OtherDimensions;
                        obj.ReqOtherDimensions = item.ReqOtherDimensions;
                        obj.ActOtherDimensions = item.ActOtherDimensions;


                        if (isAdded)
                        {
                            lstAddPRO399.Add(obj);
                        }
                    }
                    if (lstAddPRO399.Count > 0)
                    {
                        db.PRO399.AddRange(lstAddPRO399);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO399 = db.PRO399.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO399.Count > 0)
                    {
                        db.PRO399.RemoveRange(lstDeletePRO399);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO399 = db.PRO399.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO399.Count > 0)
                    {
                        db.PRO399.RemoveRange(lstDeletePRO399);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #endregion

        #region  Linkage View Code   

        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL395 objPRL395 = new PRL395();
            objPRL395 = db.PRL395.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL395 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL395.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();
            List<string> lstElevationOfNubFrom = new List<string> { "BOTTOM SIDE WEP", "BOTTOM REF. LINE", "B.T.L" };
            List<string> lstElevationOfTopSide = new List<string> { "BOTTOM SIDE WEP", "BOTTOM REF. LINE", "B.T.L" };

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.ElevationOfNubFrom = lstElevationOfNubFrom.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.ElevationOfTopSide = lstElevationOfTopSide.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL396> lstPRL396 = db.PRL396.Where(x => x.HeaderId == objPRL395.HeaderId).ToList();
            List<PRL397> lstPRL397 = db.PRL397.Where(x => x.HeaderId == objPRL395.HeaderId).ToList();
            List<PRL398> lstPRL398 = db.PRL398.Where(x => x.HeaderId == objPRL395.HeaderId).ToList();
            List<PRL399> lstPRL399 = db.PRL399.Where(x => x.HeaderId == objPRL395.HeaderId).ToList();

            ViewBag.lstPRL396 = lstPRL396;
            ViewBag.lstPRL397 = lstPRL397;
            ViewBag.lstPRL398 = lstPRL398;
            ViewBag.lstPRL399 = lstPRL399;


            #endregion

            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.DrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL395.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL395.ActFilledBy) && (objPRL395.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL395.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL395);
        }

        [HttpPost]
        public ActionResult LinkageMobile(int? id, int? m, string APIRequestFrom = "", string UserName = "")
        {
            string ProjectDesc = "";
            string ManufacturingCode = "";
            string AccessRole = "";
            bool isEditable = false;
            var DrawingNo = new List<string>();
            bool isQCRemarkEditable = false;

            PRL395 objPRL395 = new PRL395();
            db.Configuration.ProxyCreationEnabled = false;
            objPRL395 = db.PRL395.Where(i => i.HeaderId == id).FirstOrDefault();
            db.Entry(objPRL395).State = System.Data.Entity.EntityState.Detached;

            NDEModels objNDEModels = new NDEModels();
            if (objPRL395 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL395.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
            AccessRole = objUserRoleAccessDetails.UserDesignation;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();
            List<string> lstElevationOfNubFrom = new List<string> { "BOTTOM SIDE WEP", "BOTTOM REF. LINE", "B.T.L" };
            List<string> lstElevationOfTopSide = new List<string> { "BOTTOM SIDE WEP", "BOTTOM REF. LINE", "B.T.L" };

            var YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var ElevationOfNubFrom = lstElevationOfNubFrom.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var ElevationOfTopSide = lstElevationOfTopSide.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL396> lstPRL396 = db.PRL396.Where(x => x.HeaderId == objPRL395.HeaderId).ToList();
            List<PRL397> lstPRL397 = db.PRL397.Where(x => x.HeaderId == objPRL395.HeaderId).ToList();
            List<PRL398> lstPRL398 = db.PRL398.Where(x => x.HeaderId == objPRL395.HeaderId).ToList();
            List<PRL399> lstPRL399 = db.PRL399.Where(x => x.HeaderId == objPRL395.HeaderId).ToList();


            #endregion

            try
            {
                isEditable = false;
                isQCRemarkEditable = false;
                if (m == 1)
                {
                    isEditable = true;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL395.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL395.ActFilledBy) && (objPRL395.RequestNo.HasValue))
                        {
                            isEditable = false;
                            isQCRemarkEditable = true;
                        }
                        else
                        {
                            DrawingNo = (new GeneralController()).GetPLMDrawingNumber(objPRL395.Project).ToList();
                        }
                    }
                }

                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    ProjectDesc = ProjectDesc,
                    ManufacturingCode = ManufacturingCode,
                    isEditable = isEditable,
                    isQCRemarkEditable = isQCRemarkEditable,
                    DrawingNo = DrawingNo,
                    AccessRole = AccessRole,

                    YesNoEnum = YesNoEnum,
                    YesNAEnum = YesNAEnum,
                    ElevationOfNubFrom = ElevationOfNubFrom,
                    ElevationOfTopSide = ElevationOfTopSide,
                    OutsideInside = OutsideInside,
                    LeftRight = LeftRight,

                    objPRL395 = objPRL395,

                    lstPRL396 = lstPRL396,
                    lstPRL397 = lstPRL397,
                    lstPRL398 = lstPRL398,
                    lstPRL399 = lstPRL399

                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                // List<string> lstdrawing = new List<string>();
                // lstdrawing.Add("Dummy Drawing Number");
                // ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();

                return Json(new
                {
                    res = "0",
                    msg = ex.Message,

                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL395 PRL395, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = PRL395.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL395 objPRL395 = db.PRL395.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL395.DrawingRevisionNo = PRL395.DrawingRevisionNo;
                    objPRL395.DrawingNo = PRL395.DrawingNo;
                    objPRL395.DCRNo = PRL395.DCRNo;
                    objPRL395.ProtocolNo = PRL395.ProtocolNo;
                    objPRL395.ElevationOfNubFrom = PRL395.ElevationOfNubFrom;
                    objPRL395.ElevationOfTopSide = PRL395.ElevationOfTopSide;
                    objPRL395.ReqElevationOfNubFrom = PRL395.ReqElevationOfNubFrom;
                    objPRL395.ReqElevationOfTopSide = PRL395.ReqElevationOfTopSide;
                    objPRL395.ReqNubWidth = PRL395.ReqNubWidth;
                    objPRL395.ReqNubHeight = PRL395.ReqNubHeight;
                    objPRL395.TolReqElevationOfNubFrom = PRL395.TolReqElevationOfNubFrom;
                    objPRL395.TolReqElevationOfTopSide = PRL395.TolReqElevationOfTopSide;
                    objPRL395.TolReqNubWidth = PRL395.TolReqNubWidth;
                    objPRL395.TolReqNubHeight = PRL395.TolReqNubHeight;
                    objPRL395.ReqNubTopSideOverlay = PRL395.ReqNubTopSideOverlay;
                    objPRL395.ReqNubIDOverlay = PRL395.ReqNubIDOverlay;
                    objPRL395.ReqNubBottomSideOverlay = PRL395.ReqNubBottomSideOverlay;
                    objPRL395.NubID = PRL395.NubID;
                    objPRL395.ReqNubID = PRL395.ReqNubID;
                    objPRL395.OutByNubID = PRL395.OutByNubID;
                    objPRL395.ShellOutOfRoundness = PRL395.ShellOutOfRoundness;
                    objPRL395.AllowedShellOutOfRoundness = PRL395.AllowedShellOutOfRoundness;
                    objPRL395.OutByShellOutOfRoundness = PRL395.OutByShellOutOfRoundness;
                    objPRL395.RequiredOverlayThickness = PRL395.RequiredOverlayThickness;
                    objPRL395.CheckPoint1 = PRL395.CheckPoint1;
                    objPRL395.CheckPoint2 = PRL395.CheckPoint2;
                    objPRL395.CheckPoint3 = PRL395.CheckPoint3;
                    objPRL395.CheckPoint4 = PRL395.CheckPoint4;
                    objPRL395.CheckPoint5 = PRL395.CheckPoint5;
                    objPRL395.CheckPoint6 = PRL395.CheckPoint6;
                    objPRL395.CheckPoint8 = PRL395.CheckPoint8;
                    objPRL395.CheckPoint8_2 = PRL395.CheckPoint8_2;
                    objPRL395.QCRemarks = PRL395.QCRemarks;
                    objPRL395.Result = PRL395.Result;
                    objPRL395.EditedBy = UserName;
                    objPRL395.EditedOn = DateTime.Now;

                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL395.ActFilledBy = UserName;
                            objPRL395.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL395.ReqFilledBy = UserName;
                            objPRL395.ReqFilledOn = DateTime.Now;
                        }
                    }

                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL395.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL395 objPRL395 = db.PRL395.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL395 != null && string.IsNullOrWhiteSpace(objPRL395.ProtocolNo))
                    {
                        db.PRL395.Remove(objPRL395);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL395.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL396> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL396> lstAddPRL396 = new List<PRL396>();
                List<PRL396> lstDeletePRL396 = new List<PRL396>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL396 obj = db.PRL396.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL396();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Orientation = item.Orientation;
                        obj.ActTolReqElevationOfNubFrom = item.ActTolReqElevationOfNubFrom;
                        obj.ActTolReqElevationOfTopSide = item.ActTolReqElevationOfTopSide;
                        obj.ActTolReqNubWidth = item.ActTolReqNubWidth;
                        obj.ActTolReqNubHeight = item.ActTolReqNubHeight;
                        obj.A1DimensionsBefore = item.A1DimensionsBefore;
                        obj.A2DimensionsBefore = item.A2DimensionsBefore;
                        obj.B1DimensionsBefore = item.B1DimensionsBefore;
                        obj.B2DimensionsBefore = item.B2DimensionsBefore;
                        obj.C1DimensionsBefore = item.C1DimensionsBefore;
                        obj.C2DimensionsBefore = item.C2DimensionsBefore;
                        obj.AA1DimensionsAfter = item.AA1DimensionsAfter;
                        obj.AA2DimensionsAfter = item.AA2DimensionsAfter;
                        obj.BB1DimensionsAfter = item.BB1DimensionsAfter;
                        obj.BB2DimensionsAfter = item.BB2DimensionsAfter;
                        obj.CC1DimensionsAfter = item.CC1DimensionsAfter;
                        obj.CC2DimensionsAfter = item.CC2DimensionsAfter;
                        obj.A1NubTopSide = item.A1NubTopSide;
                        obj.A2NubTopSide = item.A2NubTopSide;
                        obj.B1NubID = item.B1NubID;
                        obj.B2NubID = item.B2NubID;
                        obj.C1NubBottomSide = item.C1NubBottomSide;
                        obj.C2NubBottomSide = item.C2NubBottomSide;

                        if (isAdded)
                        {
                            lstAddPRL396.Add(obj);
                        }
                    }
                }
                if (lstAddPRL396.Count > 0)
                {
                    db.PRL396.AddRange(lstAddPRL396);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL396 = db.PRL396.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL396 = db.PRL396.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL396.Count > 0)
                {
                    db.PRL396.RemoveRange(lstDeletePRL396);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2Linkage(List<PRL397> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL397> lstAddPRL397 = new List<PRL397>();
                List<PRL397> lstDeletePRL397 = new List<PRL397>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL397 obj = db.PRL397.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL397();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        if (!string.IsNullOrWhiteSpace(item.OrientationReqNubID))
                        {

                            obj.OrientationReqNubID = item.OrientationReqNubID;
                            obj.ActualNubID = item.ActualNubID;

                            if (isAdded)
                            {
                                lstAddPRL397.Add(obj);
                            }
                        }
                    }
                }
                if (lstAddPRL397.Count > 0)
                {
                    db.PRL397.AddRange(lstAddPRL397);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL397 = db.PRL397.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL397 = db.PRL397.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL397.Count > 0)
                {
                    db.PRL397.RemoveRange(lstDeletePRL397);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3Linkage(List<PRL398> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL398> lstAddPRL398 = new List<PRL398>();
                List<PRL398> lstDeletePRL398 = new List<PRL398>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL398 obj = db.PRL398.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL398();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }


                        if (!string.IsNullOrWhiteSpace(item.OrientationAllowedShellOutOfRoundness))
                        {


                            obj.OrientationAllowedShellOutOfRoundness = item.OrientationAllowedShellOutOfRoundness;
                            obj.ActualShellOutOfRoundness = item.ActualShellOutOfRoundness;

                            if (isAdded)
                            {
                                lstAddPRL398.Add(obj);
                            }
                        }
                    }
                }
                if (lstAddPRL398.Count > 0)
                {
                    db.PRL398.AddRange(lstAddPRL398);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL398 = db.PRL398.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL398 = db.PRL398.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL398.Count > 0)
                {
                    db.PRL398.RemoveRange(lstDeletePRL398);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line4 details
        [HttpPost]
        public JsonResult SaveProtocolLine4Linkage(List<PRL399> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL399> lstAddPRL399 = new List<PRL399>();
                List<PRL399> lstDeletePRL399 = new List<PRL399>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL399 obj = db.PRL399.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL399();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.OtherDimensions = item.OtherDimensions;
                        obj.ReqOtherDimensions = item.ReqOtherDimensions;
                        obj.ActOtherDimensions = item.ActOtherDimensions;


                        if (isAdded)
                        {
                            lstAddPRL399.Add(obj);
                        }
                    }
                }
                if (lstAddPRL399.Count > 0)
                {
                    db.PRL399.AddRange(lstAddPRL399);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL399 = db.PRL399.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL399 = db.PRL399.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL399.Count > 0)
                {
                    db.PRL399.RemoveRange(lstDeletePRL399);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        [HttpPost]

        public JsonResult CopyData(string strSeamNo, int HeaderId)
        {
            PRL390 _obj390 = db.PRL390.Where(w => w.SeamNo == strSeamNo).FirstOrDefault();

            if (_obj390 != null)
            {
                List<PRL391> _lstPRL391 = db.PRL391.Where(w => w.HeaderId == _obj390.HeaderId).ToList();
                List<clPRL396> _lstPRL396 = new List<clPRL396>();
                if (_lstPRL391.Count() > 0)
                {
                    for (int i = 0; i < _lstPRL391.Count; i++)
                    {
                        PRL396 objPRL396Copy = new PRL396();

                        objPRL396Copy.HeaderId = HeaderId;
                        objPRL396Copy.Orientation = _lstPRL391[i].Orientation;
                        objPRL396Copy.ActTolReqElevationOfNubFrom = "";
                        objPRL396Copy.ActTolReqElevationOfTopSide = "";
                        objPRL396Copy.ActTolReqNubWidth = "";
                        objPRL396Copy.ActTolReqNubHeight = "";
                        objPRL396Copy.A1DimensionsBefore = _lstPRL391[i].A1DimensionsChecked;
                        objPRL396Copy.A2DimensionsBefore = _lstPRL391[i].A2DimensionsChecked;
                        objPRL396Copy.B1DimensionsBefore = _lstPRL391[i].B1DimensionsChecked;
                        objPRL396Copy.B2DimensionsBefore = _lstPRL391[i].B2DimensionsChecked;
                        objPRL396Copy.C1DimensionsBefore = _lstPRL391[i].C1DimensionsChecked;
                        objPRL396Copy.C2DimensionsBefore = _lstPRL391[i].C2DimensionsChecked;
                        objPRL396Copy.AA1DimensionsAfter = "";
                        objPRL396Copy.AA2DimensionsAfter = "";
                        objPRL396Copy.BB1DimensionsAfter = "";
                        objPRL396Copy.BB2DimensionsAfter = "";
                        objPRL396Copy.CC1DimensionsAfter = "";
                        objPRL396Copy.CC2DimensionsAfter = "";
                        objPRL396Copy.A1NubTopSide = "";
                        objPRL396Copy.A2NubTopSide = "";
                        objPRL396Copy.B1NubID = "";
                        objPRL396Copy.B2NubID = "";
                        objPRL396Copy.C1NubBottomSide = "";
                        objPRL396Copy.C2NubBottomSide = "";

                        objPRL396Copy.CreatedBy = objClsLoginInfo.UserName;
                        objPRL396Copy.CreatedOn = DateTime.Now;
                        db.PRL396.Add(objPRL396Copy);

                        clPRL396 _obj = new clPRL396();
                        _obj.HeaderId = HeaderId;
                        _obj.Orientation = _lstPRL391[i].Orientation;
                        _obj.A1DimensionsBefore = _lstPRL391[i].A1DimensionsChecked;
                        _obj.A2DimensionsBefore = _lstPRL391[i].A2DimensionsChecked;
                        _obj.B1DimensionsBefore = _lstPRL391[i].B1DimensionsChecked;
                        _obj.B2DimensionsBefore = _lstPRL391[i].B2DimensionsChecked;
                        _obj.C1DimensionsBefore = _lstPRL391[i].C1DimensionsChecked;
                        _obj.C2DimensionsBefore = _lstPRL391[i].C2DimensionsChecked;

                        _lstPRL396.Add(_obj);
                    }
                    db.SaveChanges();
                }

                PRL395 _obj395 = db.PRL395.Where(w => w.HeaderId == HeaderId).FirstOrDefault();
                if (_obj395 != null)
                {
                    _obj395.IsDataCopy = "true";
                    db.SaveChanges();
                }
                return Json(_lstPRL396, JsonRequestBehavior.AllowGet);
            }
            return Json("", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ActCopyData(string strSeamNo, int HeaderId)
        {
            string strReturn = "";
            try
            {
                PRL390 _obj390 = db.PRL390.Where(w => w.SeamNo == strSeamNo).FirstOrDefault();

                if (_obj390 != null)
                {
                    List<PRL391> _lstPRL391 = db.PRL391.Where(w => w.HeaderId == _obj390.HeaderId).ToList();
                    List<PRL396> _lstPRL396 = db.PRL396.Where(w => w.HeaderId == HeaderId).ToList();
                    if (_lstPRL391.Count() > 0)
                    {
                        for (int i = 0; i < _lstPRL391.Count; i++)
                        {
                            if (_lstPRL396.Count >= i)
                            {
                                PRL396 _obj = _lstPRL396[i];
                                _obj.A1DimensionsBefore = _lstPRL391[i].A1DimensionsChecked;
                                _obj.A2DimensionsBefore = _lstPRL391[i].A2DimensionsChecked;
                                _obj.B1DimensionsBefore = _lstPRL391[i].B1DimensionsChecked;
                                _obj.B2DimensionsBefore = _lstPRL391[i].B2DimensionsChecked;
                                _obj.C1DimensionsBefore = _lstPRL391[i].C1DimensionsChecked;
                                _obj.C2DimensionsBefore = _lstPRL391[i].C2DimensionsChecked;
                                if (strReturn == "")
                                    strReturn = _lstPRL391[i].A1DimensionsChecked + "," + _lstPRL391[i].A2DimensionsChecked + "," + _lstPRL391[i].B1DimensionsChecked + "," + _lstPRL391[i].B2DimensionsChecked + "," + _lstPRL391[i].C1DimensionsChecked + "," + _lstPRL391[i].C2DimensionsChecked;
                                else
                                    strReturn += "|" + _lstPRL391[i].A1DimensionsChecked + "," + _lstPRL391[i].A2DimensionsChecked + "," + _lstPRL391[i].B1DimensionsChecked + "," + _lstPRL391[i].B2DimensionsChecked + "," + _lstPRL391[i].C1DimensionsChecked + "," + _lstPRL391[i].C2DimensionsChecked;
                                db.SaveChanges();
                            }
                        }
                        PRL395 _obj395 = db.PRL395.Where(w => w.HeaderId == HeaderId).FirstOrDefault();
                        if (_obj395 != null)
                        {
                            _obj395.IsActDataCopy = "true";
                            db.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                strReturn = "Error";
            }
            return Json(strReturn, JsonRequestBehavior.AllowGet);
        }

        #endregion

    }

    public class clPRL396
    {
        public int LineId { get; set; }
        public Nullable<int> HeaderId { get; set; }
        public string Orientation { get; set; }
        public string A1DimensionsBefore { get; set; }
        public string A2DimensionsBefore { get; set; }
        public string B1DimensionsBefore { get; set; }
        public string B2DimensionsBefore { get; set; }
        public string C1DimensionsBefore { get; set; }
        public string C2DimensionsBefore { get; set; }
    }
}