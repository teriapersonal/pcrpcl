﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol089Controller : clsBase
    {
        // GET: PROTOCOL/Protocol089
        string ControllerURL = "/PROTOCOL/Protocol089/";
        string Title = "AFTER PWHT FINAL REPORT FOR MACHINING, VISUAL ,GASKET FACE AND OTHER DIMENSION OF NOZZLE";

        #region Details View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO440 objPRO440 = new PRO440();
            if (!id.HasValue)
            {
                try
                {
                    objPRO440.ProtocolNo = string.Empty;
                    objPRO440.CreatedBy = objClsLoginInfo.UserName;
                    objPRO440.CreatedOn = DateTime.Now;

                    #region  MACHINING | PRO442
                    objPRO440.PRO442.Add(new PRO442
                    {
                        SrNo = "1",
                        Description = "Identification",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO440.PRO442.Add(new PRO442
                    {
                        SrNo = "2",
                        Description = "Visual inspection",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO440.PRO442.Add(new PRO442
                    {
                        SrNo = "3",
                        Description = "ID before O/L",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO440.PRO442.Add(new PRO442
                    {
                        SrNo = "4",
                        Description = "ID after O/L",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO440.PRO442.Add(new PRO442
                    {
                        SrNo = "5",
                        Description = "ID Machining up to length",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO440.PRO442.Add(new PRO442
                    {
                        SrNo = "6",
                        Description = "ID Surface finish",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO440.PRO442.Add(new PRO442
                    {
                        SrNo = "7",
                        Description = "Gasket face step OD",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO440.PRO442.Add(new PRO442
                    {
                        SrNo = "8",
                        Description = "Gasket height",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO440.PRO442.Add(new PRO442
                    {
                        SrNo = "9",
                        Description = "Gasket face finish",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO440.PRO442.Add(new PRO442
                    {
                        SrNo = "10",
                        Description = "Gasket step OD radius Top",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO440.PRO442.Add(new PRO442
                    {
                        SrNo = "11",
                        Description = "Gasket step OD radius Bottom",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO440.PRO442.Add(new PRO442
                    {
                        SrNo = "12",
                        Description = "Groove width",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO440.PRO442.Add(new PRO442
                    {
                        SrNo = "13",
                        Description = "Groove depth",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO440.PRO442.Add(new PRO442
                    {
                        SrNo = "14",
                        Description = "Groove corner radius",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO440.PRO442.Add(new PRO442
                    {
                        SrNo = "15",
                        Description = "Groove angle",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO440.PRO442.Add(new PRO442
                    {
                        SrNo = "16",
                        Description = "Groove taper finish",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO440.PRO442.Add(new PRO442
                    {
                        SrNo = "17",
                        Description = "Groove face finish",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO440.PRO442.Add(new PRO442
                    {
                        SrNo = "18",
                        Description = "Gasket groove P.C.D",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO440.PRO442.Add(new PRO442
                    {
                        SrNo = "19",
                        Description = "Radius Gasket face to ID",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO440.PRO442.Add(new PRO442
                    {
                        SrNo = "20",
                        Description = "O/L Thk.",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO440.PRO442.Add(new PRO442
                    {
                        SrNo = "21",
                        Description = "ID to Back face radius",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO440.PRO442.Add(new PRO442
                    {
                        SrNo = "22",
                        Description = "Back face height",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO440.PRO442.Add(new PRO442
                    {
                        SrNo = "23",
                        Description = "Back face visual",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO440.PRO442.Add(new PRO442
                    {
                        SrNo = "24",
                        Description = "Final Visual inspection after O/L",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion

                    db.PRO440.Add(objPRO440);
                    db.SaveChanges();
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO440.HeaderId;
            }
            else
            {
                objPRO440 = db.PRO440.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO441> lstPRO441 = db.PRO441.Where(x => x.HeaderId == objPRO440.HeaderId).ToList();
            List<PRO442> lstPRO442 = db.PRO442.Where(x => x.HeaderId == objPRO440.HeaderId).ToList();

            ViewBag._lstPRO441 = lstPRO441;
            ViewBag._lstPRO442 = lstPRO442;

            #endregion

            return View(objPRO440);
            //return View();
        }

        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO440 PRO440)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO440.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO440 objPRO440 = db.PRO440.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO440.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO440.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data

                        objPRO440.ProtocolNo = PRO440.ProtocolNo;
                        objPRO440.NozzleNo = PRO440.NozzleNo;
                        objPRO440.GasketFaceRaisedFaceSeamNo = PRO440.GasketFaceRaisedFaceSeamNo;
                        objPRO440.PartDescription = PRO440.PartDescription;
                        objPRO440.GasketGrooveSeamNo = PRO440.GasketGrooveSeamNo;
                        objPRO440.ItemNo = PRO440.ItemNo;
                        objPRO440.NozzleBackfaceSeamNoBFS = PRO440.NozzleBackfaceSeamNoBFS;
                        objPRO440.NozzleIDSeamNoIDS = PRO440.NozzleIDSeamNoIDS;
                        objPRO440.NozzleBackfaceIDradiusSeamNoBRS = PRO440.NozzleBackfaceIDradiusSeamNoBRS;

                        objPRO440.CheckPoint1 = PRO440.CheckPoint1;
                        objPRO440.CheckPoint2 = PRO440.CheckPoint2;
                        objPRO440.CheckPoint4_1_1 = PRO440.CheckPoint4_1_1;
                        objPRO440.CheckPoint4_1_2 = PRO440.CheckPoint4_1_2;
                        objPRO440.CheckPoint4_2_1 = PRO440.CheckPoint4_2_1;
                        objPRO440.CheckPoint4_2_2 = PRO440.CheckPoint4_2_2;
                        objPRO440.CheckPoint4_3_1 = PRO440.CheckPoint4_3_1;
                        objPRO440.CheckPoint4_3_2 = PRO440.CheckPoint4_3_2;
                        objPRO440.CheckPoint4_4_1 = PRO440.CheckPoint4_4_1;
                        objPRO440.CheckPoint4_4_2 = PRO440.CheckPoint4_4_2;
                        objPRO440.CheckPoint4_5_1 = PRO440.CheckPoint4_5_1;
                        objPRO440.CheckPoint4_5_2 = PRO440.CheckPoint4_5_2;

                        objPRO440.QCRemarks = PRO440.QCRemarks;
                        objPRO440.Result = PRO440.Result;

                        objPRO440.EditedBy = objClsLoginInfo.UserName;
                        objPRO440.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO440.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO440 objPRO440 = db.PRO440.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO440 != null && string.IsNullOrWhiteSpace(objPRO440.ProtocolNo))
                    {
                        db.PRO440.Remove(objPRO440);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO440.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO441> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO441> lstAddPRO441 = new List<PRO441>();
                List<PRO441> lstDeletePRO441 = new List<PRO441>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO441 obj = db.PRO441.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.DrgNo = item.DrgNo;
                                obj.RevNo = item.RevNo;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO441 obj = new PRO441();

                            if (!string.IsNullOrWhiteSpace(item.DrgNo))
                            {
                                obj.HeaderId = item.HeaderId;
                                obj.DrgNo = item.DrgNo;
                                obj.RevNo = item.RevNo;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO441.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO441.Count > 0)
                    {
                        db.PRO441.AddRange(lstAddPRO441);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO441 = db.PRO441.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO441.Count > 0)
                    {
                        db.PRO441.RemoveRange(lstDeletePRO441);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO441 = db.PRO441.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO441.Count > 0)
                    {
                        db.PRO441.RemoveRange(lstDeletePRO441);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2(List<PRO442> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO442> lstAddPRO442 = new List<PRO442>();
                List<PRO442> lstDeletePRO442 = new List<PRO442>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO442 obj = db.PRO442.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO442();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SrNo = item.SrNo;
                        obj.Description = item.Description;
                        obj.ReqDimension = item.ReqDimension;
                        obj.ActDimension = item.ActDimension;
                        obj.Remarks = item.Remarks;

                        if (isAdded)
                        {
                            lstAddPRO442.Add(obj);
                        }
                    }
                    if (lstAddPRO442.Count > 0)
                    {
                        db.PRO442.AddRange(lstAddPRO442);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO442 = db.PRO442.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO442.Count > 0)
                    {
                        db.PRO442.RemoveRange(lstDeletePRO442);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO442 = db.PRO442.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO442.Count > 0)
                    {
                        db.PRO442.RemoveRange(lstDeletePRO442);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

        #region Linkage View Code

        #region Header
        public ActionResult Linkage(int? id, int? m)
        {
            PRL440 objPRL440 = new PRL440();
            objPRL440 = db.PRL440.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL440 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL440.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL441> lstPRL441 = db.PRL441.Where(x => x.HeaderId == objPRL440.HeaderId).ToList();
            List<PRL442> lstPRL442 = db.PRL442.Where(x => x.HeaderId == objPRL440.HeaderId).ToList();

            ViewBag._lstPRL441 = lstPRL441;
            ViewBag._lstPRL442 = lstPRL442;

            #endregion

            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.DrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL440.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL440.ActFilledBy) && (objPRL440.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL440.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL440);

            //return View();
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL440 prl440, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = prl440.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL440 objPRL440 = db.PRL440.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL440.DrawingRevisionNo = prl440.DrawingRevisionNo;
                    objPRL440.DrawingNo = prl440.DrawingNo;
                    objPRL440.DCRNo = prl440.DCRNo;
                    objPRL440.ProtocolNo = prl440.ProtocolNo;

                    objPRL440.ProtocolNo = prl440.ProtocolNo;
                    objPRL440.NozzleNo = prl440.NozzleNo;
                    objPRL440.GasketFaceRaisedFaceSeamNo = prl440.GasketFaceRaisedFaceSeamNo;
                    objPRL440.PartDescription = prl440.PartDescription;
                    objPRL440.GasketGrooveSeamNo = prl440.GasketGrooveSeamNo;
                    objPRL440.ItemNo = prl440.ItemNo;
                    objPRL440.NozzleBackfaceSeamNoBFS = prl440.NozzleBackfaceSeamNoBFS;
                    objPRL440.NozzleIDSeamNoIDS = prl440.NozzleIDSeamNoIDS;
                    objPRL440.NozzleBackfaceIDradiusSeamNoBRS = prl440.NozzleBackfaceIDradiusSeamNoBRS;

                    objPRL440.CheckPoint1 = prl440.CheckPoint1;
                    objPRL440.CheckPoint2 = prl440.CheckPoint2;
                    objPRL440.CheckPoint4_1_1 = prl440.CheckPoint4_1_1;
                    objPRL440.CheckPoint4_1_2 = prl440.CheckPoint4_1_2;
                    objPRL440.CheckPoint4_2_1 = prl440.CheckPoint4_2_1;
                    objPRL440.CheckPoint4_2_2 = prl440.CheckPoint4_2_2;
                    objPRL440.CheckPoint4_3_1 = prl440.CheckPoint4_3_1;
                    objPRL440.CheckPoint4_3_2 = prl440.CheckPoint4_3_2;
                    objPRL440.CheckPoint4_4_1 = prl440.CheckPoint4_4_1;
                    objPRL440.CheckPoint4_4_2 = prl440.CheckPoint4_4_2;
                    objPRL440.CheckPoint4_5_1 = prl440.CheckPoint4_5_1;
                    objPRL440.CheckPoint4_5_2 = prl440.CheckPoint4_5_2;

                    objPRL440.QCRemarks = prl440.QCRemarks;
                    objPRL440.Result = prl440.Result;

                    objPRL440.EditedBy = UserName;
                    objPRL440.EditedOn = DateTime.Now;
                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL440.ActFilledBy = UserName;
                            objPRL440.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL440.ReqFilledBy = UserName;
                            objPRL440.ReqFilledOn = DateTime.Now;
                        }
                    }
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL440.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL440 objPRL440 = db.PRL440.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL440 != null && string.IsNullOrWhiteSpace(objPRL440.ProtocolNo))
                    {
                        db.PRL440.Remove(objPRL440);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL440.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL441> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRL441> lstAddPRL441 = new List<PRL441>();
                List<PRL441> lstDeletePRL441 = new List<PRL441>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRL441 obj = db.PRL441.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.DrgNo = item.DrgNo;
                                obj.RevNo = item.RevNo;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRL441 obj = new PRL441();

                            if (!string.IsNullOrWhiteSpace(item.DrgNo))
                            {
                                obj.HeaderId = item.HeaderId;
                                obj.DrgNo = item.DrgNo;
                                obj.RevNo = item.RevNo;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRL441.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRL441.Count > 0)
                    {
                        db.PRL441.AddRange(lstAddPRL441);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRL441 = db.PRL441.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRL441.Count > 0)
                    {
                        db.PRL441.RemoveRange(lstDeletePRL441);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL441 = db.PRL441.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL441.Count > 0)
                    {
                        db.PRL441.RemoveRange(lstDeletePRL441);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2Linkage(List<PRL442> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRL442> lstAddPRL442 = new List<PRL442>();
                List<PRL442> lstDeletePRL442 = new List<PRL442>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL442 obj = db.PRL442.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL442();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SrNo = item.SrNo;
                        obj.Description = item.Description;
                        obj.ReqDimension = item.ReqDimension;
                        obj.ActDimension = item.ActDimension;
                        obj.Remarks = item.Remarks;

                        if (isAdded)
                        {
                            lstAddPRL442.Add(obj);
                        }
                    }
                    if (lstAddPRL442.Count > 0)
                    {
                        db.PRL442.AddRange(lstAddPRL442);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL442 = db.PRL442.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL442.Count > 0)
                    {
                        db.PRL442.RemoveRange(lstDeletePRL442);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL442 = db.PRL442.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL442.Count > 0)
                    {
                        db.PRL442.RemoveRange(lstDeletePRL442);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion
    }
}