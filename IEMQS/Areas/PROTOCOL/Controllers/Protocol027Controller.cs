﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol027Controller : clsBase
    {
        string ControllerURL = "/PROTOCOL/Protocol027/";
        string Title = "REPORT FOR POSITIVE MATERIAL IDENTIFICATION ";
        // GET: PROTOCOL/Protocol027
        #region Details View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id,int? m)
        {
            PRO130 objPRO130 = new PRO130();
            if (!id.HasValue)
            {
                objPRO130.ProtocolNo = string.Empty;
                objPRO130.CreatedBy = objClsLoginInfo.UserName;
                objPRO130.CreatedOn = DateTime.Now;
                db.PRO130.Add(objPRO130);
                db.SaveChanges();
                ViewBag.HeaderId = objPRO130.HeaderId;
            }
            else
            {
                objPRO130 = db.PRO130.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;
            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            return View(objPRO130);
        }


        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO130 PRO130)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO130.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO130 objPRO130 = db.PRO130.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO130.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO130.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data
                        objPRO130.ProtocolNo = PRO130.ProtocolNo;
                        objPRO130.InspectionCertificateNo = PRO130.InspectionCertificateNo;
                        objPRO130.MethodOfTest = PRO130.MethodOfTest;
                        objPRO130.AnalyserMake = PRO130.AnalyserMake;
                        objPRO130.AnalyserModel = PRO130.AnalyserModel;
                        objPRO130.OperatorName = PRO130.OperatorName;
                        objPRO130.Remarks = PRO130.Remarks;
                        objPRO130.MATERIAL = PRO130.MATERIAL;
                        objPRO130.PROCEDUREREVNO = PRO130.PROCEDUREREVNO;
                        objPRO130.PMIREPORTNO = PRO130.PMIREPORTNO;
                        objPRO130.EditedBy = objClsLoginInfo.UserName;
                        objPRO130.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol saved successfully";
                        objResponseMsg.HeaderId = objPRO130.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO130 objPRO130 = db.PRO130.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO130 != null && string.IsNullOrWhiteSpace(objPRO130.ProtocolNo))
                    {
                        db.PRO131.RemoveRange(objPRO130.PRO131.ToList());
                        db.PRO130.Remove(objPRO130);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.REPORT_FOR_POSITIVE_MATERIAL_IDENTIFICATION.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO130.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region Line1 details
        [HttpPost]
        public ActionResult GetProtocolLines1Data(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro.SeamNo like '%" + param.sSearch
                        + "%' or pro.PlateNo like '%" + param.sSearch
                        + "%' or pro.Cr like '%" + param.sSearch
                        + "%' or pro.Mo like '%" + param.sSearch
                        + "%' or pro.Rr1 like '%" + param.sSearch
                        + "%' or pro.Rr2 like '%" + param.sSearch
                        + "%' or pro.Remark like '%" + param.sSearch
                        + "%')";
                }

                var lstResult = db.SP_IPI_PROTOCOL027_GET_LINES1_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] { "1",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   "",
                                   Helper.GenerateHTMLTextbox(newRecordId, "SeamNo", "", "", false, "", false, "200","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "PlateNo", "", "", false, "", false, "200","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Cr", "", "", false, "", false, "200","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Mo", "", "", false, "", false, "200","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Rr1", "", "", false, "", false, "200","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Rr2", "", "", false, "", false, "200","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Remark", "", "", false, "", false, "200","QC"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine1(0);")
                                };
                var data = (from uc in lstResult
                            select new[]
                            {
                               "1",
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                               Convert.ToString(uc.ROW_NO),
                               Helper.GenerateHTMLTextbox(uc.LineId, "SeamNo", Convert.ToString(uc.SeamNo), "", true, "", false, "200","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "PlateNo", Convert.ToString(uc.PlateNo), "", true, "", false, "200","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "Cr", Convert.ToString(uc.Cr), "", true, "", false, "200","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "Mo", Convert.ToString(uc.Mo), "", true, "", false, "200","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "Rr1", Convert.ToString(uc.Rr1), "", true, "", false, "200","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "Rr2", Convert.ToString(uc.Rr2), "", true, "", false, "200","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "Remark", Convert.ToString(uc.Remark), "", true, "", false, "200","QC"),
                               Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == "QC3" ? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine1','tblProtocolLines1')"):"")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public ActionResult SaveProtocolLine1(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRO131 objPRO131 = new PRO131();
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;

                if (refHeaderId > 0)
                {
                    string strSeamNo = !string.IsNullOrEmpty(fc["SeamNo" + refLineId]) ? Convert.ToString(fc["SeamNo" + refLineId]).Trim() : "";
                    string strPlateNo = !string.IsNullOrEmpty(fc["PlateNo" + refLineId]) ? Convert.ToString(fc["PlateNo" + refLineId]).Trim() : "";
                    string strCr = !string.IsNullOrEmpty(fc["Cr" + refLineId]) ? Convert.ToString(fc["Cr" + refLineId]).Trim() : "";
                    string strMo = !string.IsNullOrEmpty(fc["Mo" + refLineId]) ? Convert.ToString(fc["Mo" + refLineId]).Trim() : "";
                    string strRr1 = !string.IsNullOrEmpty(fc["Rr1" + refLineId]) ? Convert.ToString(fc["Rr1" + refLineId]).Trim() : "";
                    string strRr2 = !string.IsNullOrEmpty(fc["Rr2" + refLineId]) ? Convert.ToString(fc["Rr2" + refLineId]).Trim() : "";
                    string strRemark = !string.IsNullOrEmpty(fc["Remark" + refLineId]) ? Convert.ToString(fc["Remark" + refLineId]).Trim() : "";

                    #region Add New Spot Line1
                    if (refLineId > 0)
                    {
                        objPRO131 = db.PRO131.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRO131.EditedBy = objClsLoginInfo.UserName;
                        objPRO131.EditedOn = DateTime.Now;
                    }
                    objPRO131.HeaderId = refHeaderId;
                    objPRO131.SeamNo = strSeamNo;
                    objPRO131.PlateNo = strPlateNo;
                    objPRO131.Cr = strCr;
                    objPRO131.Mo = strMo;
                    objPRO131.Rr1 = strRr1;
                    objPRO131.Rr2 = strRr2;
                    objPRO131.Remark = strRemark;
                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = "Data updated successfully";
                    }
                    else
                    {
                        objPRO131.CreatedBy = objClsLoginInfo.UserName;
                        objPRO131.CreatedOn = DateTime.Now;
                        db.PRO131.Add(objPRO131);
                        db.SaveChanges();
                        objResponseMsg.Value = "Data added successfully";
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRO131.LineId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult DeleteProtocolLine1(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRO131 objPRO131 = db.PRO131.FirstOrDefault(c => c.LineId == lineId);
                if (objPRO131 != null)
                {
                    db.PRO131.Remove(objPRO131);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #endregion

        #region Linkage View Code
        #region Header
        public ActionResult Linkage(int? id, int? m)
        {
            PRL130 objPRL130 = new PRL130();
            objPRL130 = db.PRL130.Where(i => i.HeaderId == id).FirstOrDefault();

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;
            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            if (objPRL130 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL130.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objQproject.ManufacturingCode;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }

            if (m == 1)
            {
                ViewBag.isEditable = "true";
            }
            else
            {
                ViewBag.isEditable = "false";
            }

            return View(objPRL130);
        }


        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL130 PRL130)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRL130.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL130 objPRL130 = db.PRL130.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL130.ProtocolNo = PRL130.ProtocolNo != null ? PRL130.ProtocolNo : "";
                    objPRL130.InspectionCertificateNo = PRL130.InspectionCertificateNo;
                    objPRL130.EquipmentNo = PRL130.EquipmentNo;
                    objPRL130.InspectionAgency = PRL130.InspectionAgency;
                    objPRL130.DrawingNo = PRL130.DrawingNo;
                    objPRL130.DrawingRevisionNo = PRL130.DrawingRevisionNo;
                    objPRL130.InspectionDate = PRL130.InspectionDate;
                    objPRL130.MethodOfTest = PRL130.MethodOfTest;
                    objPRL130.AnalyserMake = PRL130.AnalyserMake;
                    objPRL130.AnalyserModel = PRL130.AnalyserModel;
                    objPRL130.OperatorName = PRL130.OperatorName;
                    objPRL130.Remarks = PRL130.Remarks;
                    objPRL130.MATERIAL = PRL130.MATERIAL;
                    objPRL130.PROCEDUREREVNO = PRL130.PROCEDUREREVNO;
                    objPRL130.PMIREPORTNO = PRL130.PMIREPORTNO;
                    objPRL130.EditedBy = objClsLoginInfo.UserName;
                    objPRL130.EditedOn = DateTime.Now;

                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol saved successfully";
                    objResponseMsg.HeaderId = objPRL130.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL130 objPRL130 = db.PRL130.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL130 != null && string.IsNullOrWhiteSpace(objPRL130.ProtocolNo))
                    {
                        db.PRL131.RemoveRange(objPRL130.PRL131.ToList());
                        db.PRL130.Remove(objPRL130);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL130.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #endregion



        #region Line1 details
        [HttpPost]
        public ActionResult GetProtocolLines1DataLinkage(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro.SeamNo like '%" + param.sSearch
                        + "%' or pro.PlateNo like '%" + param.sSearch
                        + "%' or pro.Cr like '%" + param.sSearch
                        + "%' or pro.Mo like '%" + param.sSearch
                        + "%' or pro.Rr1 like '%" + param.sSearch
                        + "%' or pro.Rr2 like '%" + param.sSearch
                        + "%' or pro.Remark like '%" + param.sSearch
                        + "%')";
                }

                var lstResult = db.SP_IPI_PROTOCOL027_LINKAGE_GET_LINES1_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] { "1",
                                       Convert.ToString(headerId),
                                       Convert.ToString(newRecordId),
                                       "",
                                       Helper.GenerateHTMLTextbox(newRecordId, "SeamNo", "", "", false, "", false, "200","QC"),
                                       Helper.GenerateHTMLTextbox(newRecordId, "PlateNo", "", "", false, "", false, "200","QC"),
                                       Helper.GenerateHTMLTextbox(newRecordId, "Cr", "", "", false, "", false, "200","QC"),
                                       Helper.GenerateHTMLTextbox(newRecordId, "Mo", "", "", false, "", false, "200","QC"),
                                       Helper.GenerateHTMLTextbox(newRecordId, "Rr1", "", "", false, "", false, "200","QC"),
                                       Helper.GenerateHTMLTextbox(newRecordId, "Rr2", "", "", false, "", false, "200","QC"),
                                       Helper.GenerateHTMLTextbox(newRecordId, "Remark", "", "", false, "", false, "200","QC"),
                                       Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine1(0);")
                                    };

                var data = (from uc in lstResult
                            select new[]
                            {
                                   "1",
                                   Convert.ToString(uc.HeaderId),
                                   Convert.ToString(uc.LineId),
                                   Convert.ToString(uc.ROW_NO),
                                   Helper.GenerateHTMLTextbox(uc.LineId, "SeamNo", Convert.ToString(uc.SeamNo), "", true, "", false, "200","QC"),
                                   Helper.GenerateHTMLTextbox(uc.LineId, "PlateNo", Convert.ToString(uc.PlateNo), "", true, "", false, "200","QC"),
                                   Helper.GenerateHTMLTextbox(uc.LineId, "Cr", Convert.ToString(uc.Cr), "", true, "", false, "200","QC"),
                                   Helper.GenerateHTMLTextbox(uc.LineId, "Mo", Convert.ToString(uc.Mo), "", true, "", false, "200","QC"),
                                   Helper.GenerateHTMLTextbox(uc.LineId, "Rr1", Convert.ToString(uc.Rr1), "", true, "", false, "200","QC"),
                                   Helper.GenerateHTMLTextbox(uc.LineId, "Rr2", Convert.ToString(uc.Rr2), "", true, "", false, "200","QC"),
                                   Helper.GenerateHTMLTextbox(uc.LineId, "Remark", Convert.ToString(uc.Remark), "", true, "", false, "200","QC"),
                                   Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == "QC3" ? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine1Linkage','tblProtocolLines1')"):"")
                               }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }



        [HttpPost]
        public ActionResult SaveProtocolLine1Linkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRL131 objPRL131 = new PRL131();

            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;

                if (refHeaderId > 0)
                {
                    string strSeamNo = !string.IsNullOrEmpty(fc["SeamNo" + refLineId]) ? Convert.ToString(fc["SeamNo" + refLineId]).Trim() : "";
                    string strPlateNo = !string.IsNullOrEmpty(fc["PlateNo" + refLineId]) ? Convert.ToString(fc["PlateNo" + refLineId]).Trim() : "";
                    string strCr = !string.IsNullOrEmpty(fc["Cr" + refLineId]) ? Convert.ToString(fc["Cr" + refLineId]).Trim() : "";
                    string strMo = !string.IsNullOrEmpty(fc["Mo" + refLineId]) ? Convert.ToString(fc["Mo" + refLineId]).Trim() : "";
                    string strRr1 = !string.IsNullOrEmpty(fc["Rr1" + refLineId]) ? Convert.ToString(fc["Rr1" + refLineId]).Trim() : "";
                    string strRr2 = !string.IsNullOrEmpty(fc["Rr2" + refLineId]) ? Convert.ToString(fc["Rr2" + refLineId]).Trim() : "";
                    string strRemark = !string.IsNullOrEmpty(fc["Remark" + refLineId]) ? Convert.ToString(fc["Remark" + refLineId]).Trim() : "";

                    #region Add New Spot Line1
                    if (refLineId > 0)
                    {
                        objPRL131 = db.PRL131.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRL131.EditedBy = objClsLoginInfo.UserName;
                        objPRL131.EditedOn = DateTime.Now;
                    }

                    objPRL131.HeaderId = refHeaderId;
                    objPRL131.SeamNo = strSeamNo;
                    objPRL131.PlateNo = strPlateNo;
                    objPRL131.Cr = strCr;
                    objPRL131.Mo = strMo;
                    objPRL131.Rr1 = strRr1;
                    objPRL131.Rr2 = strRr2;
                    objPRL131.Remark = strRemark;

                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = "Data updated successfully";
                    }
                    else
                    {
                        objPRL131.CreatedBy = objClsLoginInfo.UserName;
                        objPRL131.CreatedOn = DateTime.Now;
                        db.PRL131.Add(objPRL131);
                        db.SaveChanges();
                        objResponseMsg.Value = "Data added successfully";
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRL131.LineId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult DeleteProtocolLine1Linkage(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRL131 objPRL131 = db.PRL131.FirstOrDefault(c => c.LineId == lineId);
                if (objPRL131 != null)
                {
                    db.PRL131.Remove(objPRL131);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public string HTMLActionString(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", string onHrefURL = "", bool isDisable = false)
        {
            string htmlControl = "";
            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? onClickMethod : "";
            string onHref = !string.IsNullOrEmpty(onHrefURL) ? "href='" + onHrefURL + "'" : "";

            if (isDisable)
            {
                htmlControl += "<i id=\"" + inputID.ToString() + "\" name=\"" + inputID + "\" ></i>";
                //htmlControl = "<i id=\"" + inputID + "' name='" + inputID + "' style='cursor: pointer; opacity:0.3; margin-right:10px;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";
            }
            else
            {
                htmlControl += "<i id=\"" + inputID.ToString() + "\" name=\"" + inputID + "\"  style='cursor: pointer; margin-right:10px;' Title=\"" + buttonTooltip + "\" class=\"" + className + "\"  onclick=\"" + onClickEvent + "\" ></i>";
                //htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer; margin-right:10px;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
            }

            if (!string.IsNullOrEmpty(onHref))
            {
                htmlControl = "<a " + onHref + " >" + htmlControl + "</a>";
            }
            return htmlControl;
        }
    }
}