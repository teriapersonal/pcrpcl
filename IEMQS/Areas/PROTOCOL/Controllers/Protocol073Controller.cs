﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol073Controller : clsBase
    {
        // GET: PROTOCOL/Protocol073
        string ControllerURL = "/PROTOCOL/Protocol073/";
        string Title = "CuSO4 TEST REPORT (FOR CLAD STRIPING AREA)";
        //IEMQS.DrawingReference.hedProjectDocListWebServiceClient drawref = new IEMQS.DrawingReference.hedProjectDocListWebServiceClient();

        #region Details View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO360 objPRO360 = new PRO360();
            if (!id.HasValue)
            {
                try
                {
                    objPRO360.ProtocolNo = string.Empty;
                    objPRO360.CreatedBy = objClsLoginInfo.UserName;
                    objPRO360.CreatedOn = DateTime.Now;

                    db.PRO360.Add(objPRO360);
                    db.SaveChanges();
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO360.HeaderId;
            }
            else
            {
                objPRO360 = db.PRO360.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO361> lstPRO361 = db.PRO361.Where(x => x.HeaderId == objPRO360.HeaderId).ToList();


            ViewBag.lstPRO361 = lstPRO361;


            #endregion

            return View(objPRO360);
        }

        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO360 PRO360)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO360.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO360 objPRO360 = db.PRO360.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO360.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO360.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data
                        objPRO360.ProtocolNo = PRO360.ProtocolNo;

                        objPRO360.SpotNoClad = PRO360.SpotNoClad;
                        objPRO360.ReqWidth = PRO360.ReqWidth;
                        objPRO360.ReqDepth = PRO360.ReqDepth;
                        objPRO360.CheckPoint1 = PRO360.CheckPoint1;
                        objPRO360.CheckPoint2 = PRO360.CheckPoint2;
                        objPRO360.CheckPoint3 = PRO360.CheckPoint3;
                        objPRO360.QCRemarks = PRO360.QCRemarks;
                        objPRO360.Result = PRO360.Result;


                        objPRO360.EditedBy = objClsLoginInfo.UserName;
                        objPRO360.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO360.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO360 objPRO360 = db.PRO360.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO360 != null && string.IsNullOrWhiteSpace(objPRO360.ProtocolNo))
                    {
                        db.PRO360.Remove(objPRO360);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO360.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion



        #region Line1 details

        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO361> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO361> lstAddPRO361 = new List<PRO361>();
                List<PRO361> lstDeletePRO361 = new List<PRO361>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO361 obj = db.PRO361.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO361();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SpotNo = item.SpotNo;
                        obj.Width = item.Width;
                        obj.Depth = item.Depth;

                        if (isAdded)
                        {
                            lstAddPRO361.Add(obj);
                        }
                    }
                    if (lstAddPRO361.Count > 0)
                    {
                        db.PRO361.AddRange(lstAddPRO361);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO361 = db.PRO361.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO361.Count > 0)
                    {
                        db.PRO361.RemoveRange(lstDeletePRO361);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO361 = db.PRO361.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO361.Count > 0)
                    {
                        db.PRO361.RemoveRange(lstDeletePRO361);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion



        #endregion


        #region Linkage View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL360 objPRL360 = new PRL360();
            objPRL360 = db.PRL360.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL360 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL360.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL361> lstPRL361 = db.PRL361.Where(x => x.HeaderId == objPRL360.HeaderId).ToList();


            ViewBag.lstPRL361 = lstPRL361;


            #endregion

            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.DrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL360.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL360.ActFilledBy) && (objPRL360.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL360.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL360);
        }

        [HttpPost]
        public ActionResult LinkageMobile(int? id, int? m, string APIRequestFrom = "", string UserName = "")
        {
            string ProjectDesc = "";
            string ManufacturingCode = "";
            string AccessRole = "";
            bool isEditable = false;
            var DrawingNo = new List<string>();
            bool isQCRemarkEditable = false;

            PRL360 objPRL360 = new PRL360();
            db.Configuration.ProxyCreationEnabled = false;
            objPRL360 = db.PRL360.Where(i => i.HeaderId == id).FirstOrDefault();
            db.Entry(objPRL360).State = System.Data.Entity.EntityState.Detached;
            NDEModels objNDEModels = new NDEModels();
            if (objPRL360 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL360.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
            AccessRole = objUserRoleAccessDetails.UserDesignation;
            
            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            var YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL361> lstPRL361 = db.PRL361.Where(x => x.HeaderId == objPRL360.HeaderId).ToList();

            #endregion
            try
            {
                isEditable = false;
                isQCRemarkEditable = false;
                if (m == 1)
                {
                    isEditable = true;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL360.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL360.ActFilledBy) && (objPRL360.RequestNo.HasValue))
                        {
                            isEditable = false;
                            isQCRemarkEditable = true;
                        }
                        else
                        {
                            DrawingNo = (new GeneralController()).GetPLMDrawingNumber(objPRL360.Project).ToList();
                        }
                    }
                }

                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    ProjectDesc = ProjectDesc,
                    ManufacturingCode = ManufacturingCode,
                    isEditable = isEditable,
                    isQCRemarkEditable = isQCRemarkEditable,
                    DrawingNo = DrawingNo,
                    AccessRole = AccessRole,

                    YesNoEnum = YesNoEnum,
                    YesNAEnum = YesNAEnum,
                   
                    OutsideInside = OutsideInside,
                    LeftRight = LeftRight,

                    objPRL405 = objPRL360,

                    lstPRL361 = lstPRL361

                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                return Json(new
                {
                    res = "0",
                    msg = ex.Message,
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL360 prl360, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = prl360.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL360 objPRL360 = db.PRL360.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    #region Save Data
                    objPRL360.DrawingRevisionNo = prl360.DrawingRevisionNo;
                    objPRL360.DrawingNo = prl360.DrawingNo;
                    objPRL360.DCRNo = prl360.DCRNo;
                    objPRL360.ProtocolNo = prl360.ProtocolNo;
                    objPRL360.SpotNoClad = prl360.SpotNoClad;
                    objPRL360.ReqWidth = prl360.ReqWidth;
                    objPRL360.ReqDepth = prl360.ReqDepth;
                    objPRL360.CheckPoint1 = prl360.CheckPoint1;
                    objPRL360.CheckPoint2 = prl360.CheckPoint2;
                    objPRL360.CheckPoint3 = prl360.CheckPoint3;
                    objPRL360.QCRemarks = prl360.QCRemarks;
                    objPRL360.Result = prl360.Result;
                    objPRL360.EditedBy = UserName;
                    objPRL360.EditedOn = DateTime.Now;

                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL360.ActFilledBy = UserName;
                            objPRL360.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL360.ReqFilledBy = UserName;
                            objPRL360.ReqFilledOn = DateTime.Now;
                        }
                    }

                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL360.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL360 objPRL360 = db.PRL360.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL360 != null && string.IsNullOrWhiteSpace(objPRL360.ProtocolNo))
                    {
                        db.PRL360.Remove(objPRL360);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL360.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
     
        #region Line1 Linkage

        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL361> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL361> lstAddPRL361 = new List<PRL361>();
                List<PRL361> lstDeletePRL361 = new List<PRL361>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL361 obj = db.PRL361.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL361();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SpotNo = item.SpotNo;
                        obj.Width = item.Width;
                        obj.Depth = item.Depth;

                        if (isAdded)
                        {
                            lstAddPRL361.Add(obj);
                        }
                    }
                }
                if (lstAddPRL361.Count > 0)
                {
                    db.PRL361.AddRange(lstAddPRL361);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL361 = db.PRL361.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL361 = db.PRL361.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL361.Count > 0)
                {
                    db.PRL361.RemoveRange(lstDeletePRL361);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

       

        #endregion
    }
}