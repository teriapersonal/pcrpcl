﻿using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol007Controller : clsBase
    {
        string ControllerURL = "/PROTOCOL/Protocol007/";
        string Title = "WELD VISUAL & DIMENSION REPORT FOR NOZZLE";
        // GET: PROTOCOL/Protocol007
        #region Details View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id,int? m)
        {
            PRO030 objPRO030 = new PRO030();
            if (!id.HasValue)
            {
                objPRO030.ProtocolNo = string.Empty;
                objPRO030.CreatedBy = objClsLoginInfo.UserName;
                objPRO030.CreatedOn = DateTime.Now;
                db.PRO030.Add(objPRO030);
                db.SaveChanges();
                ViewBag.HeaderId = objPRO030.HeaderId;
            }
            else
            {
                objPRO030 = db.PRO030.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            return View(objPRO030);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO030 PRO030)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO030.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO030 objPRO030 = db.PRO030.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO030.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO030.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data
                        objPRO030.ProtocolNo = PRO030.ProtocolNo;
                        objPRO030.TolMaxTiltInDegMm = PRO030.TolMaxTiltInDegMm;
                        objPRO030.BoltHolesStraddled = PRO030.BoltHolesStraddled;
                        objPRO030.TolBoltHolesStraddled = PRO030.TolBoltHolesStraddled;
                        objPRO030.ActBoltHolesStraddled = PRO030.ActBoltHolesStraddled;
                        objPRO030.WelderStampPunch = PRO030.WelderStampPunch;
                        objPRO030.SeamPunch = PRO030.SeamPunch;
                        objPRO030.TempAttachRemovedAndGround = PRO030.TempAttachRemovedAndGround;
                        objPRO030.ReqDisBetPairNozzles = PRO030.ReqDisBetPairNozzles;
                        objPRO030.ActDisBetPairNozzles = PRO030.ActDisBetPairNozzles;
                        objPRO030.NozzleOrientation = PRO030.NozzleOrientation;
                        objPRO030.ReqNozzleOrientation = PRO030.ReqNozzleOrientation;
                        objPRO030.ActNozzleOrientation = PRO030.ActNozzleOrientation;
                        objPRO030.ReqElevationOfNozzle = PRO030.ReqElevationOfNozzle;
                        objPRO030.ActElevationOfNozzle = PRO030.ActElevationOfNozzle;
                        objPRO030.ReqHeightFromVessel = PRO030.ReqHeightFromVessel;
                        objPRO030.ActHeightFromVessel = PRO030.ActHeightFromVessel;
                        objPRO030.ReqOffsetDistanceOfNozzles = PRO030.ReqOffsetDistanceOfNozzles;
                        objPRO030.ActOffsetDistanceOfNozzles = PRO030.ActOffsetDistanceOfNozzles;
                        objPRO030.OffsetTol = PRO030.OffsetTol;
                        objPRO030.ActMinOffset = PRO030.ActMinOffset;
                        objPRO030.ActMaxOffset = PRO030.ActMaxOffset;
                        objPRO030.Remark1 = PRO030.Remark1; ;
                        objPRO030.Remark2 = PRO030.Remark2;
                        objPRO030.Remark3 = PRO030.Remark3;
                        objPRO030.Remark4 = PRO030.Remark4;
                        objPRO030.Remark5 = PRO030.Remark5;
                        objPRO030.Remark5_1 = PRO030.Remark5_1;
                        objPRO030.Remark6 = PRO030.Remark6; ;
                        objPRO030.Remark7 = PRO030.Remark7;
                        objPRO030.Remark8 = PRO030.Remark8;
                        objPRO030.Remark9 = PRO030.Remark9;
                        objPRO030.Result = PRO030.Result;
                        objPRO030.EditedBy = objClsLoginInfo.UserName;
                        objPRO030.EditedOn = DateTime.Now;
                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol saved successfully";
                        objResponseMsg.HeaderId = objPRO030.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO030 objPRO030 = db.PRO030.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO030 != null && string.IsNullOrWhiteSpace(objPRO030.ProtocolNo))
                    {
                        db.PRO030.Remove(objPRO030);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.WELD_VISUAL_AND_DIMENSION_REPORT_FOR_NOZZLE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO030.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion        
        #endregion

        #region Linkage View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL030 objPRL030 = new PRL030();
            objPRL030 = db.PRL030.Where(i => i.HeaderId == id).FirstOrDefault();

            if (objPRL030 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL030.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objQproject.ManufacturingCode;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            if (m == 1)
            {
                ViewBag.isEditable = "true";
            }
            else
            { ViewBag.isEditable = "false"; }

            return View(objPRL030);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL030 PRL030)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRL030.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL030 objPRL030 = db.PRL030.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL030.ProtocolNo = PRL030.ProtocolNo != null ? PRL030.ProtocolNo : "";
                    objPRL030.DCRNo = PRL030.DCRNo;
                    objPRL030.TolMaxTiltInDegMm = PRL030.TolMaxTiltInDegMm;
                    objPRL030.BoltHolesStraddled = PRL030.BoltHolesStraddled;
                    objPRL030.TolBoltHolesStraddled = PRL030.TolBoltHolesStraddled;
                    objPRL030.ActBoltHolesStraddled = PRL030.ActBoltHolesStraddled;
                    objPRL030.WelderStampPunch = PRL030.WelderStampPunch;
                    objPRL030.SeamPunch = PRL030.SeamPunch;
                    objPRL030.TempAttachRemovedAndGround = PRL030.TempAttachRemovedAndGround;
                    objPRL030.ReqDisBetPairNozzles = PRL030.ReqDisBetPairNozzles;
                    objPRL030.ActDisBetPairNozzles = PRL030.ActDisBetPairNozzles;
                    objPRL030.NozzleOrientation = PRL030.NozzleOrientation;
                    objPRL030.ReqNozzleOrientation = PRL030.ReqNozzleOrientation;
                    objPRL030.ActNozzleOrientation = PRL030.ActNozzleOrientation;
                    objPRL030.ReqElevationOfNozzle = PRL030.ReqElevationOfNozzle;
                    objPRL030.ActElevationOfNozzle = PRL030.ActElevationOfNozzle;
                    objPRL030.ReqHeightFromVessel = PRL030.ReqHeightFromVessel;
                    objPRL030.ActHeightFromVessel = PRL030.ActHeightFromVessel;
                    objPRL030.ReqOffsetDistanceOfNozzles = PRL030.ReqOffsetDistanceOfNozzles;
                    objPRL030.ActOffsetDistanceOfNozzles = PRL030.ActOffsetDistanceOfNozzles;
                    objPRL030.OffsetTol = PRL030.OffsetTol;
                    objPRL030.ActMinOffset = PRL030.ActMinOffset;
                    objPRL030.ActMaxOffset = PRL030.ActMaxOffset;
                    objPRL030.Remark1 = PRL030.Remark1; ;
                    objPRL030.Remark2 = PRL030.Remark2;
                    objPRL030.Remark3 = PRL030.Remark3;
                    objPRL030.Remark4 = PRL030.Remark4;
                    objPRL030.Remark5 = PRL030.Remark5;
                    objPRL030.Remark5_1 = PRL030.Remark5_1;
                    objPRL030.Remark6 = PRL030.Remark6; ;
                    objPRL030.Remark7 = PRL030.Remark7;
                    objPRL030.Remark8 = PRL030.Remark8;
                    objPRL030.Remark9 = PRL030.Remark9;
                    objPRL030.Result = PRL030.Result;
                    objPRL030.EditedBy = objClsLoginInfo.UserName;
                    objPRL030.EditedOn = DateTime.Now;
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol saved successfully";
                    objResponseMsg.HeaderId = objPRL030.HeaderId;
                    #endregion

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL030 objPRL030 = db.PRL030.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL030 != null && string.IsNullOrWhiteSpace(objPRL030.ProtocolNo))
                    {
                        db.PRL030.Remove(objPRL030);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL030.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion        
        #endregion
    }
}