﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;


namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol037Controller : clsBase
    {
        // GET: PROTOCOL/Protocol037
        string ControllerURL = "/PROTOCOL/Protocol037/";
        string Title = "SET-UP & DIMENSION REPORT FOR CIRCUMFERENTIAL SEAM";
        //IEMQS.DrawingReference.hedProjectDocListWebServiceClient drawref = new IEMQS.DrawingReference.hedProjectDocListWebServiceClient();

        #region Details View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO180 objPRO180 = new PRO180();
            if (!id.HasValue)
            {
                try
                {
                    objPRO180.ProtocolNo = string.Empty;
                    objPRO180.CreatedBy = objClsLoginInfo.UserName;
                    objPRO180.CreatedOn = DateTime.Now;

                    #region  TOTAL HEIGHT REQ | PRO182
                    objPRO180.PRO182.Add(new PRO182
                    {
                        RequiredValue = "At 0°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO180.PRO182.Add(new PRO182
                    {
                        RequiredValue = "At 90°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO180.PRO182.Add(new PRO182
                    {
                        RequiredValue = "At 180°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO180.PRO182.Add(new PRO182
                    {
                        RequiredValue = "At 270°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion

                    #region OUT OF ROUNDNESS | PRO183
                    objPRO180.PRO183.Add(new PRO183
                    {
                        Orientation = "0°-180°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO180.PRO183.Add(new PRO183
                    {
                        Orientation = "22.5°-202.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO180.PRO183.Add(new PRO183
                    {
                        Orientation = "45°-225°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO180.PRO183.Add(new PRO183
                    {
                        Orientation = "67.5°-247.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO180.PRO183.Add(new PRO183
                    {
                        Orientation = "90°-270°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO180.PRO183.Add(new PRO183
                    {
                        Orientation = "112.5°-292.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO180.PRO183.Add(new PRO183
                    {
                        Orientation = "135°-315°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO180.PRO183.Add(new PRO183
                    {
                        Orientation = "157.5°-337.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion                    

                    db.PRO180.Add(objPRO180);
                    db.SaveChanges();
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO180.HeaderId;
            }
            else
            {
                objPRO180 = db.PRO180.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO181> lstPRO181 = db.PRO181.Where(x => x.HeaderId == objPRO180.HeaderId).ToList();
            List<PRO182> lstPRO182 = db.PRO182.Where(x => x.HeaderId == objPRO180.HeaderId).ToList();
            List<PRO183> lstPRO183 = db.PRO183.Where(x => x.HeaderId == objPRO180.HeaderId).ToList();
            List<PRO184> lstPRO184 = db.PRO184.Where(x => x.HeaderId == objPRO180.HeaderId).ToList();
            List<PRO184_2> lstPRO184_2 = db.PRO184_2.Where(x => x.HeaderId == objPRO180.HeaderId).ToList();
            List<PRO184_3> lstPRO184_3 = db.PRO184_3.Where(x => x.HeaderId == objPRO180.HeaderId).ToList();
            List<PRO184_4> lstPRO184_4 = db.PRO184_4.Where(x => x.HeaderId == objPRO180.HeaderId).ToList();
            //List<SP_IPI_PROTOCOL037_GET_LINES8_DATA_Result> lstPRO184_5 = db.SP_IPI_PROTOCOL037_GET_LINES8_DATA(1, int.MaxValue, "", " HeaderId = " + objPRO180.HeaderId).ToList();
            //List<SP_IPI_PROTOCOL037_GET_LINES9_DATA_Result> lstPRO184_6 = db.SP_IPI_PROTOCOL037_GET_LINES9_DATA(1, int.MaxValue, "", " HeaderId = " + objPRO180.HeaderId).ToList();
            List<PRO184_5> lstPRO184_5 = db.PRO184_5.Where(x => x.HeaderId == objPRO180.HeaderId).ToList();
            List<PRO184_6> lstPRO184_6 = db.PRO184_6.Where(x => x.HeaderId == objPRO180.HeaderId).ToList();

            List<PRO184_7> lstPRO184_7 = db.PRO184_7.Where(x => x.HeaderId == objPRO180.HeaderId).ToList();
            List<PRO184_8> lstPRO184_8 = db.PRO184_8.Where(x => x.HeaderId == objPRO180.HeaderId).ToList();

            ViewBag.lstPRO181 = lstPRO181;
            ViewBag.lstPRO182 = lstPRO182;
            ViewBag.lstPRO183 = lstPRO183;
            ViewBag.lstPRO184 = lstPRO184;
            ViewBag.lstPRO184_2 = lstPRO184_2;
            ViewBag.lstPRO184_3 = lstPRO184_3;
            ViewBag.lstPRO184_4 = lstPRO184_4;
            ViewBag.lstPRO184_5 = lstPRO184_5;
            ViewBag.lstPRO184_6 = lstPRO184_6;
            ViewBag.lstPRO184_7 = lstPRO184_7;
            ViewBag.lstPRO184_8 = lstPRO184_8;
            #endregion

            return View(objPRO180);
        }

        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO180 PRO180)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO180.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO180 objPRO180 = db.PRO180.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO180.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO180.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data
                        objPRO180.ProtocolNo = PRO180.ProtocolNo;
                        objPRO180.ReqTotalHeight = PRO180.ReqTotalHeight;
                        objPRO180.ReqOutOfRoundness = PRO180.ReqOutOfRoundness;
                        objPRO180.IsAtSeam = PRO180.IsAtSeam;
                        objPRO180.ReqOutByAllowed = PRO180.ReqOutByAllowed;
                        objPRO180.ReqWEPDimensionRootGap = PRO180.ReqWEPDimensionRootGap;
                        objPRO180.ReqWEPDimensionRootFace = PRO180.ReqWEPDimensionRootFace;
                        objPRO180.ReqWEPDimensionInsideWEPAngle = PRO180.ReqWEPDimensionInsideWEPAngle;
                        objPRO180.ReqWEPDimensionOutsideWEPAngle = PRO180.ReqWEPDimensionOutsideWEPAngle;

                        objPRO180.ActMaxWEPDimensionRootGap = PRO180.ActMaxWEPDimensionRootGap;
                        objPRO180.ActMaxWEPDimensionRootFace = PRO180.ActMaxWEPDimensionRootFace;
                        objPRO180.ActMaxWEPDimensionInsideWEPAngle = PRO180.ActMaxWEPDimensionInsideWEPAngle;
                        objPRO180.ActMaxWEPDimensionOutsideWEPAngle = PRO180.ActMaxWEPDimensionOutsideWEPAngle;
                        objPRO180.ActMinWEPDimensionRootGap = PRO180.ActMinWEPDimensionRootGap;
                        objPRO180.ActMinWEPDimensionRootFace = PRO180.ActMinWEPDimensionRootFace;
                        objPRO180.ActMinWEPDimensionInsideWEPAngle = PRO180.ActMinWEPDimensionInsideWEPAngle;
                        objPRO180.ActMinWEPDimensionOutsideWEPAngle = PRO180.ActMinWEPDimensionOutsideWEPAngle;
                        objPRO180.ReqOffsetReadingAllowed = PRO180.ReqOffsetReadingAllowed;
                        objPRO180.ReqAlignmentReadingOutByAllowed = PRO180.ReqAlignmentReadingOutByAllowed;
                        objPRO180.ReqEvelutionReferenceLine = PRO180.ReqEvelutionReferenceLine;
                        objPRO180.ReqYRingToShellJoint = PRO180.ReqYRingToShellJoint;
                        objPRO180.ReqYRingToShellJointValue = PRO180.ReqYRingToShellJointValue;
                        objPRO180.ActMinYRingToShellJointValue = PRO180.ActMinYRingToShellJointValue;
                        objPRO180.ActMaxYRingToShellJointValue = PRO180.ActMaxYRingToShellJointValue;

                        objPRO180.IsOrientationOfNozzle = PRO180.IsOrientationOfNozzle;
                        objPRO180.IsConcentricity = PRO180.IsConcentricity;
                        objPRO180.ActConcentricityValue = PRO180.ActConcentricityValue;

                        objPRO180.CheckPoint1 = PRO180.CheckPoint1;
                        objPRO180.CheckPoint2 = PRO180.CheckPoint2;
                        objPRO180.CheckPoint3 = PRO180.CheckPoint3;
                        objPRO180.CheckPoint4 = PRO180.CheckPoint4;
                        objPRO180.CheckPoint5 = PRO180.CheckPoint5;
                        objPRO180.CheckPoint6 = PRO180.CheckPoint6;
                        objPRO180.CheckPoint7 = PRO180.CheckPoint7;
                        objPRO180.CheckPoint9 = PRO180.CheckPoint9;
                        objPRO180.CheckPoint9_2 = PRO180.CheckPoint9_2;
                        objPRO180.CheckPoint10 = PRO180.CheckPoint10;
                        objPRO180.CheckPoint10_2 = PRO180.CheckPoint10_2;
                        objPRO180.CheckPoint11 = PRO180.CheckPoint11;
                        objPRO180.CheckPoint11_2 = PRO180.CheckPoint11_2;
                        objPRO180.CheckPoint11_3 = PRO180.CheckPoint11_3;
                        objPRO180.CheckPoint12 = PRO180.CheckPoint12;
                        objPRO180.QCRemarks = PRO180.QCRemarks;
                        objPRO180.Result = PRO180.Result;

                        objPRO180.SpotNoOffset = PRO180.SpotNoOffset;
                        objPRO180.SpotNoClad = PRO180.SpotNoClad;
                        objPRO180.ReqCladStripingWidth = PRO180.ReqCladStripingWidth;
                        objPRO180.ReqCladStripingDepth = PRO180.ReqCladStripingDepth;

                        objPRO180.Note1 = PRO180.Note1;
                        objPRO180.Note2 = PRO180.Note2;
                        objPRO180.Note3 = PRO180.Note3;

                        objPRO180.IdentificationOfCleats = PRO180.IdentificationOfCleats;
                        objPRO180.IsEvelutionOfNozzleReferenceLine = PRO180.IsEvelutionOfNozzleReferenceLine;
                        objPRO180.AlignmentReadings = PRO180.AlignmentReadings;
                        objPRO180.ReqConcentricityValue = PRO180.ReqConcentricityValue;



                        objPRO180.EditedBy = objClsLoginInfo.UserName;
                        objPRO180.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO180.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO180 objPRO180 = db.PRO180.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO180 != null && string.IsNullOrWhiteSpace(objPRO180.ProtocolNo))
                    {
                        db.PRO180.Remove(objPRO180);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO180.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO181> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO181> lstAddPRO181 = new List<PRO181>();
                List<PRO181> lstDeletePRO181 = new List<PRO181>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO181 obj = db.PRO181.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.IdentificationMarking = item.IdentificationMarking;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO181 obj = new PRO181();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.IdentificationMarking))
                            {
                                obj.IdentificationMarking = item.IdentificationMarking;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO181.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO181.Count > 0)
                    {
                        db.PRO181.AddRange(lstAddPRO181);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO181 = db.PRO181.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO181.Count > 0)
                    {
                        db.PRO181.RemoveRange(lstDeletePRO181);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO181 = db.PRO181.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO181.Count > 0)
                    {
                        db.PRO181.RemoveRange(lstDeletePRO181);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2(List<PRO182> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO182> lstAddPRO182 = new List<PRO182>();
                List<PRO182> lstDeletePRO182 = new List<PRO182>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO182 obj = db.PRO182.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO182();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.RequiredValue = item.RequiredValue;
                        obj.ActualValue = item.ActualValue;

                        if (isAdded)
                        {
                            lstAddPRO182.Add(obj);
                        }
                    }
                    if (lstAddPRO182.Count > 0)
                    {
                        db.PRO182.AddRange(lstAddPRO182);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO182 = db.PRO182.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO182.Count > 0)
                    {
                        db.PRO182.RemoveRange(lstDeletePRO182);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO182 = db.PRO182.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO182.Count > 0)
                    {
                        db.PRO182.RemoveRange(lstDeletePRO182);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3(List<PRO183> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO183> lstAddPRO183 = new List<PRO183>();
                List<PRO183> lstDeletePRO183 = new List<PRO183>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO183 obj = db.PRO183.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(item.Orientation))
                            {
                                obj = new PRO183();
                                obj.HeaderId = item.HeaderId;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                isAdded = true;
                            }
                        }

                        if (!string.IsNullOrWhiteSpace(item.Orientation))
                        {
                            obj.Orientation = item.Orientation;
                            obj.AtTopOpenEnd = item.AtTopOpenEnd;
                            obj.AtSeam = item.AtSeam;
                            obj.AtBottomOpenEnd = item.AtBottomOpenEnd;
                        }

                        if (isAdded)
                        {
                            lstAddPRO183.Add(obj);
                        }
                    }
                    if (lstAddPRO183.Count > 0)
                    {
                        db.PRO183.AddRange(lstAddPRO183);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO183 = db.PRO183.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO183.Count > 0)
                    {
                        db.PRO183.RemoveRange(lstDeletePRO183);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO183 = db.PRO183.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO183.Count > 0)
                    {
                        db.PRO183.RemoveRange(lstDeletePRO183);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line4 details
        [HttpPost]
        public JsonResult SaveProtocolLine4(List<PRO184> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO184> lstAddPRO184 = new List<PRO184>();
                List<PRO184> lstDeletePRO184 = new List<PRO184>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO184 obj = db.PRO184.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO184();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SeamNo = item.SeamNo;
                        obj.ReqOrientation = item.ReqOrientation;
                        obj.ReqArcLength = item.ReqArcLength;
                        obj.ActArcLength = item.ActArcLength;

                        if (isAdded)
                        {
                            lstAddPRO184.Add(obj);
                        }
                    }
                    if (lstAddPRO184.Count > 0)
                    {
                        db.PRO184.AddRange(lstAddPRO184);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO184 = db.PRO184.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO184.Count > 0)
                    {
                        db.PRO184.RemoveRange(lstDeletePRO184);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO184 = db.PRO184.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO184.Count > 0)
                    {
                        db.PRO184.RemoveRange(lstDeletePRO184);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line5 details
        [HttpPost]
        public JsonResult SaveProtocolLine5(List<PRO184_2> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO184_2> lstAddPRO184_2 = new List<PRO184_2>();
                List<PRO184_2> lstDeletePRO184_2 = new List<PRO184_2>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO184_2 obj = db.PRO184_2.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO184_2();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SpotNo = item.SpotNo;
                        obj.Offset = item.Offset;

                        if (isAdded)
                        {
                            lstAddPRO184_2.Add(obj);
                        }
                    }
                    if (lstAddPRO184_2.Count > 0)
                    {
                        db.PRO184_2.AddRange(lstAddPRO184_2);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO184_2 = db.PRO184_2.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO184_2.Count > 0)
                    {
                        db.PRO184_2.RemoveRange(lstDeletePRO184_2);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO184_2 = db.PRO184_2.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO184_2.Count > 0)
                    {
                        db.PRO184_2.RemoveRange(lstDeletePRO184_2);
                    }
                    db.SaveChanges();
                }

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line6 details
        [HttpPost]
        public JsonResult SaveProtocolLine6(List<PRO184_3> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO184_3> lstAddPRO184_3 = new List<PRO184_3>();
                List<PRO184_3> lstDeletePRO184_3 = new List<PRO184_3>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO184_3 obj = db.PRO184_3.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            if (!string.IsNullOrWhiteSpace(item.Location))
                            {
                                obj = new PRO184_3();
                                obj.HeaderId = item.HeaderId;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                isAdded = true;
                            }
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        if (!string.IsNullOrWhiteSpace(item.Location))
                        {
                            obj.Location = item.Location;
                            obj.ActAt0 = item.ActAt0;
                            obj.ActAt90 = item.ActAt90;
                            obj.ActAt180 = item.ActAt180;
                            obj.ActAt270 = item.ActAt270;
                        }

                        if (isAdded)
                        {
                            lstAddPRO184_3.Add(obj);
                        }
                    }
                    if (lstAddPRO184_3.Count > 0)
                    {
                        db.PRO184_3.AddRange(lstAddPRO184_3);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO184_3 = db.PRO184_3.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO184_3.Count > 0)
                    {
                        db.PRO184_3.RemoveRange(lstDeletePRO184_3);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO184_3 = db.PRO184_3.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO184_3.Count > 0)
                    {
                        db.PRO184_3.RemoveRange(lstDeletePRO184_3);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line7 details
        [HttpPost]
        public JsonResult SaveProtocolLine7(List<PRO184_4> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO184_4> lstAddPRO184_4 = new List<PRO184_4>();
                List<PRO184_4> lstDeletePRO184_4 = new List<PRO184_4>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO184_4 obj = db.PRO184_4.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO184_4();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Description = item.Description;
                        obj.ReqValue = item.ReqValue;
                        obj.ActAt0 = item.ActAt0;
                        obj.ActAt90 = item.ActAt90;
                        obj.ActAt180 = item.ActAt180;
                        obj.ActAt270 = item.ActAt270;

                        if (isAdded)
                        {
                            lstAddPRO184_4.Add(obj);
                        }
                    }
                    if (lstAddPRO184_4.Count > 0)
                    {
                        db.PRO184_4.AddRange(lstAddPRO184_4);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO184_4 = db.PRO184_4.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO184_4.Count > 0)
                    {
                        db.PRO184_4.RemoveRange(lstDeletePRO184_4);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO184_4 = db.PRO184_4.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO184_4.Count > 0)
                    {
                        db.PRO184_4.RemoveRange(lstDeletePRO184_4);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line8 details

        [HttpPost]
        public JsonResult SaveProtocolLine8(List<PRO184_5> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO184_5> lstAddPRO184_5 = new List<PRO184_5>();
                List<PRO184_5> lstDeletePRO184_5 = new List<PRO184_5>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO184_5 obj = db.PRO184_5.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO184_5();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SpotNo = item.SpotNo;
                        obj.Width = item.Width;
                        obj.Depth = item.Depth;

                        if (isAdded)
                        {
                            lstAddPRO184_5.Add(obj);
                        }
                    }
                    if (lstAddPRO184_5.Count > 0)
                    {
                        db.PRO184_5.AddRange(lstAddPRO184_5);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO184_5 = db.PRO184_5.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO184_5.Count > 0)
                    {
                        db.PRO184_5.RemoveRange(lstDeletePRO184_5);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO184_5 = db.PRO184_5.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO184_5.Count > 0)
                    {
                        db.PRO184_5.RemoveRange(lstDeletePRO184_5);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line9 details

        [HttpPost]
        public JsonResult SaveProtocolLine9(List<PRO184_6> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO184_6> lstAddPRO184_6 = new List<PRO184_6>();
                List<PRO184_6> lstDeletePRO184_6 = new List<PRO184_6>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO184_6 obj = db.PRO184_6.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO184_6();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.CleatNo = item.CleatNo;
                        obj.ParentMetalSize = item.ParentMetalSize;
                        obj.Location = item.Location;
                        obj.OutsideInside = item.OutsideInside;
                        obj.Distance = item.Distance;
                        obj.Orientation1 = item.Orientation1;
                        obj.Orientation2 = item.Orientation2;
                        obj.Orientation3 = item.Orientation3;

                        if (isAdded)
                        {
                            lstAddPRO184_6.Add(obj);
                        }
                    }
                    if (lstAddPRO184_6.Count > 0)
                    {
                        db.PRO184_6.AddRange(lstAddPRO184_6);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO184_6 = db.PRO184_6.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO184_6.Count > 0)
                    {
                        db.PRO184_6.RemoveRange(lstDeletePRO184_6);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO184_6 = db.PRO184_6.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO184_6.Count > 0)
                    {
                        db.PRO184_6.RemoveRange(lstDeletePRO184_6);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line10 details

        [HttpPost]
        public JsonResult SaveProtocolLine10(List<PRO184_7> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO184_7> lstAddPRO184_7 = new List<PRO184_7>();
                List<PRO184_7> lstDeletePRO184_7 = new List<PRO184_7>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO184_7 obj = db.PRO184_7.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO184_7();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Description = item.Description;
                        obj.ReqValue = item.ReqValue;
                        obj.ActValue = item.ActValue;

                        if (isAdded)
                        {
                            lstAddPRO184_7.Add(obj);
                        }
                    }
                    if (lstAddPRO184_7.Count > 0)
                    {
                        db.PRO184_7.AddRange(lstAddPRO184_7);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO184_7 = db.PRO184_7.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO184_7.Count > 0)
                    {
                        db.PRO184_7.RemoveRange(lstDeletePRO184_7);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO184_7 = db.PRO184_7.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO184_7.Count > 0)
                    {
                        db.PRO184_7.RemoveRange(lstDeletePRO184_7);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line11 details

        [HttpPost]
        public JsonResult SaveProtocolLine11(List<PRO184_8> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO184_8> lstAddPRO184_8 = new List<PRO184_8>();
                List<PRO184_8> lstDeletePRO184_8 = new List<PRO184_8>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO184_8 obj = db.PRO184_8.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO184_8();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Description = item.Description;
                        obj.ReqValue = item.ReqValue;
                        obj.ActValue = item.ActValue;

                        if (isAdded)
                        {
                            lstAddPRO184_8.Add(obj);
                        }
                    }
                    if (lstAddPRO184_8.Count > 0)
                    {
                        db.PRO184_8.AddRange(lstAddPRO184_8);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO184_8 = db.PRO184_8.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO184_8.Count > 0)
                    {
                        db.PRO184_8.RemoveRange(lstDeletePRO184_8);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO184_8 = db.PRO184_8.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO184_8.Count > 0)
                    {
                        db.PRO184_8.RemoveRange(lstDeletePRO184_8);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line12 details
        [HttpPost]
        public JsonResult SaveProtocolLine12(List<PRL184_9> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRL184_9> lstAddPRL184_9 = new List<PRL184_9>();
                List<PRL184_9> lstDeletePRL184_9 = new List<PRL184_9>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRL184_9 obj = db.PRL184_9.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.DrgNo = item.DrgNo;
                                obj.RevNo = item.RevNo;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRL184_9 obj = new PRL184_9();

                            if (!string.IsNullOrWhiteSpace(item.DrgNo))
                            {
                                obj.HeaderId = item.HeaderId;
                                obj.DrgNo = item.DrgNo;
                                obj.RevNo = item.RevNo;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRL184_9.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRL184_9.Count > 0)
                    {
                        db.PRL184_9.AddRange(lstAddPRL184_9);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRL184_9 = db.PRL184_9.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRL184_9.Count > 0)
                    {
                        db.PRL184_9.RemoveRange(lstDeletePRL184_9);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL184_9 = db.PRL184_9.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL184_9.Count > 0)
                    {
                        db.PRL184_9.RemoveRange(lstDeletePRL184_9);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion



        #endregion

        #region Linkage View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL180 objPRL180 = new PRL180();
            objPRL180 = db.PRL180.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL180 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL180.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL181> lstPRL181 = db.PRL181.Where(x => x.HeaderId == objPRL180.HeaderId).ToList();
            List<PRL182> lstPRL182 = db.PRL182.Where(x => x.HeaderId == objPRL180.HeaderId).ToList();
            List<PRL183> lstPRL183 = db.PRL183.Where(x => x.HeaderId == objPRL180.HeaderId).ToList();
            List<PRL184> lstPRL184 = db.PRL184.Where(x => x.HeaderId == objPRL180.HeaderId).ToList();
            List<PRL184_2> lstPRL184_2 = db.PRL184_2.Where(x => x.HeaderId == objPRL180.HeaderId).ToList();
            List<PRL184_3> lstPRL184_3 = db.PRL184_3.Where(x => x.HeaderId == objPRL180.HeaderId).ToList();
            List<PRL184_4> lstPRL184_4 = db.PRL184_4.Where(x => x.HeaderId == objPRL180.HeaderId).ToList();
            List<PRL184_5> lstPRL184_5 = db.PRL184_5.Where(x => x.HeaderId == objPRL180.HeaderId).ToList();
            List<PRL184_6> lstPRL184_6 = db.PRL184_6.Where(x => x.HeaderId == objPRL180.HeaderId).ToList();
            List<PRL184_7> lstPRL184_7 = db.PRL184_7.Where(x => x.HeaderId == objPRL180.HeaderId).ToList();
            List<PRL184_8> lstPRL184_8 = db.PRL184_8.Where(x => x.HeaderId == objPRL180.HeaderId).ToList();
            List<PRL184_9> lstPRL184_9 = db.PRL184_9.Where(x => x.HeaderId == objPRL180.HeaderId).ToList();

            ViewBag.lstPRL181 = lstPRL181;
            ViewBag.lstPRL182 = lstPRL182;
            ViewBag.lstPRL183 = lstPRL183;
            ViewBag.lstPRL184 = lstPRL184;
            ViewBag.lstPRL184_2 = lstPRL184_2;
            ViewBag.lstPRL184_3 = lstPRL184_3;
            ViewBag.lstPRL184_4 = lstPRL184_4;
            ViewBag.lstPRL184_5 = lstPRL184_5;
            ViewBag.lstPRL184_6 = lstPRL184_6;
            ViewBag.lstPRL184_7 = lstPRL184_7;
            ViewBag.lstPRL184_8 = lstPRL184_8;
            ViewBag.lstPRL184_9 = lstPRL184_9;
            #endregion

            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.DrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL180.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL180.ActFilledBy) && (objPRL180.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL180.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL180.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { 
                    ViewBag.isEditable = "false";
                    List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL180.Project).ToList();
                    ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL180);
        }

        [HttpPost]
        public ActionResult LinkageMobile(int? id, int? m, string APIRequestFrom = "", string UserName = "")
        {
            string ProjectDesc = "";
            string ManufacturingCode = "";
            string AccessRole = "";
            bool isEditable = false;
            var DrawingNo = new List<string>();
            bool isQCRemarkEditable = false;


            PRL180 objPRL180 = new PRL180();
            db.Configuration.ProxyCreationEnabled = false;
            objPRL180 = db.PRL180.Where(i => i.HeaderId == id).FirstOrDefault();
            db.Entry(objPRL180).State = System.Data.Entity.EntityState.Detached;

            NDEModels objNDEModels = new NDEModels();
            if (objPRL180 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL180.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }

            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
            AccessRole = objUserRoleAccessDetails.UserDesignation;


            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            var YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            var LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL181> lstPRL181 = db.PRL181.Where(x => x.HeaderId == objPRL180.HeaderId).ToList();
            List<PRL182> lstPRL182 = db.PRL182.Where(x => x.HeaderId == objPRL180.HeaderId).ToList();
            List<PRL183> lstPRL183 = db.PRL183.Where(x => x.HeaderId == objPRL180.HeaderId).ToList();
            List<PRL184> lstPRL184 = db.PRL184.Where(x => x.HeaderId == objPRL180.HeaderId).ToList();
            List<PRL184_2> lstPRL184_2 = db.PRL184_2.Where(x => x.HeaderId == objPRL180.HeaderId).ToList();
            List<PRL184_3> lstPRL184_3 = db.PRL184_3.Where(x => x.HeaderId == objPRL180.HeaderId).ToList();
            List<PRL184_4> lstPRL184_4 = db.PRL184_4.Where(x => x.HeaderId == objPRL180.HeaderId).ToList();
            List<PRL184_5> lstPRL184_5 = db.PRL184_5.Where(x => x.HeaderId == objPRL180.HeaderId).ToList();
            List<PRL184_6> lstPRL184_6 = db.PRL184_6.Where(x => x.HeaderId == objPRL180.HeaderId).ToList();
            List<PRL184_7> lstPRL184_7 = db.PRL184_7.Where(x => x.HeaderId == objPRL180.HeaderId).ToList();
            List<PRL184_8> lstPRL184_8 = db.PRL184_8.Where(x => x.HeaderId == objPRL180.HeaderId).ToList();
            List<PRL184_9> lstPRL184_9 = db.PRL184_9.Where(x => x.HeaderId == objPRL180.HeaderId).ToList();

            #endregion

            try
            {
                isEditable = false;
                isQCRemarkEditable = false;
                if (m == 1)
                {
                    isEditable = true;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL180.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL180.ActFilledBy) && (objPRL180.RequestNo.HasValue))
                        {
                            isEditable = false;
                            isQCRemarkEditable = true;
                        }
                        else
                        {
                            DrawingNo = (new GeneralController()).GetPLMDrawingNumber(objPRL180.Project).ToList();
                            //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = false; }

                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    ProjectDesc = ProjectDesc,
                    ManufacturingCode = ManufacturingCode,
                    isEditable = isEditable,
                    isQCRemarkEditable = isQCRemarkEditable,
                    DrawingNo = DrawingNo,
                    OutsideInside = OutsideInside,
                    AccessRole = AccessRole,
                    LeftRight = LeftRight,
                    YesNoEnum = YesNoEnum,
                    YesNAEnum = YesNAEnum,

                    objPRL180 = objPRL180,
                    lstPRL181 = lstPRL181,
                    lstPRL182 = lstPRL182,
                    lstPRL183 = lstPRL183,
                    lstPRL184 = lstPRL184,
                    lstPRL184_2 = lstPRL184_2,
                    lstPRL184_3 = lstPRL184_3,
                    lstPRL184_4 = lstPRL184_4,
                    lstPRL184_5 = lstPRL184_5,
                    lstPRL184_6 = lstPRL184_6,
                    lstPRL184_7 = lstPRL184_7,
                    lstPRL184_8 = lstPRL184_8,
                    lstPRL184_9 = lstPRL184_9

                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();

                return Json(new
                {
                    res = "0",
                    msg = ex.Message,
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL180 prl180, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = prl180.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL180 objPRL180 = db.PRL180.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    #region Save Data
                    objPRL180.DrawingRevisionNo = prl180.DrawingRevisionNo;
                    objPRL180.DrawingNo = prl180.DrawingNo;
                    objPRL180.DCRNo = prl180.DCRNo;
                    objPRL180.ProtocolNo = prl180.ProtocolNo;
                    objPRL180.ReqTotalHeight = prl180.ReqTotalHeight;
                    objPRL180.ReqOutOfRoundness = prl180.ReqOutOfRoundness;
                    objPRL180.IsAtSeam = prl180.IsAtSeam;
                    objPRL180.ReqOutByAllowed = prl180.ReqOutByAllowed;
                    objPRL180.ReqWEPDimensionRootGap = prl180.ReqWEPDimensionRootGap;
                    objPRL180.ReqWEPDimensionRootFace = prl180.ReqWEPDimensionRootFace;
                    objPRL180.ReqWEPDimensionInsideWEPAngle = prl180.ReqWEPDimensionInsideWEPAngle;
                    objPRL180.ReqWEPDimensionOutsideWEPAngle = prl180.ReqWEPDimensionOutsideWEPAngle;
                    objPRL180.ActMaxWEPDimensionRootGap = prl180.ActMaxWEPDimensionRootGap;
                    objPRL180.ActMaxWEPDimensionRootFace = prl180.ActMaxWEPDimensionRootFace;
                    objPRL180.ActMaxWEPDimensionInsideWEPAngle = prl180.ActMaxWEPDimensionInsideWEPAngle;
                    objPRL180.ActMaxWEPDimensionOutsideWEPAngle = prl180.ActMaxWEPDimensionOutsideWEPAngle;
                    objPRL180.ActMinWEPDimensionRootGap = prl180.ActMinWEPDimensionRootGap;
                    objPRL180.ActMinWEPDimensionRootFace = prl180.ActMinWEPDimensionRootFace;
                    objPRL180.ActMinWEPDimensionInsideWEPAngle = prl180.ActMinWEPDimensionInsideWEPAngle;
                    objPRL180.ActMinWEPDimensionOutsideWEPAngle = prl180.ActMinWEPDimensionOutsideWEPAngle;
                    objPRL180.ReqOffsetReadingAllowed = prl180.ReqOffsetReadingAllowed;
                    objPRL180.ReqAlignmentReadingOutByAllowed = prl180.ReqAlignmentReadingOutByAllowed;
                    objPRL180.ReqEvelutionReferenceLine = prl180.ReqEvelutionReferenceLine;
                    objPRL180.ReqYRingToShellJoint = prl180.ReqYRingToShellJoint;
                    objPRL180.ReqYRingToShellJointValue = prl180.ReqYRingToShellJointValue;
                    objPRL180.ActMinYRingToShellJointValue = prl180.ActMinYRingToShellJointValue;
                    objPRL180.ActMaxYRingToShellJointValue = prl180.ActMaxYRingToShellJointValue;
                    objPRL180.IsOrientationOfNozzle = prl180.IsOrientationOfNozzle;
                    objPRL180.IsConcentricity = prl180.IsConcentricity;
                    objPRL180.ActConcentricityValue = prl180.ActConcentricityValue;
                    objPRL180.CheckPoint1 = prl180.CheckPoint1;
                    objPRL180.CheckPoint2 = prl180.CheckPoint2;
                    objPRL180.CheckPoint3 = prl180.CheckPoint3;
                    objPRL180.CheckPoint4 = prl180.CheckPoint4;
                    objPRL180.CheckPoint5 = prl180.CheckPoint5;
                    objPRL180.CheckPoint6 = prl180.CheckPoint6;
                    objPRL180.CheckPoint7 = prl180.CheckPoint7;
                    objPRL180.CheckPoint9 = prl180.CheckPoint9;
                    objPRL180.CheckPoint9_2 = prl180.CheckPoint9_2;
                    objPRL180.CheckPoint10 = prl180.CheckPoint10;
                    objPRL180.CheckPoint10_2 = prl180.CheckPoint10_2;
                    objPRL180.CheckPoint11 = prl180.CheckPoint11;
                    objPRL180.CheckPoint11_2 = prl180.CheckPoint11_2;
                    objPRL180.CheckPoint11_3 = prl180.CheckPoint11_3;
                    objPRL180.CheckPoint12 = prl180.CheckPoint12;
                    objPRL180.QCRemarks = prl180.QCRemarks;
                    objPRL180.Result = prl180.Result;
                    objPRL180.SpotNoOffset = prl180.SpotNoOffset;
                    objPRL180.SpotNoClad = prl180.SpotNoClad;
                    objPRL180.ReqCladStripingWidth = prl180.ReqCladStripingWidth;
                    objPRL180.ReqCladStripingDepth = prl180.ReqCladStripingDepth;
                    objPRL180.Note1 = prl180.Note1;
                    objPRL180.Note2 = prl180.Note2;
                    objPRL180.Note3 = prl180.Note3;
                    objPRL180.IdentificationOfCleats = prl180.IdentificationOfCleats;
                    objPRL180.IsEvelutionOfNozzleReferenceLine = prl180.IsEvelutionOfNozzleReferenceLine;
                    objPRL180.AlignmentReadings = prl180.AlignmentReadings;
                    objPRL180.ReqConcentricityValue = prl180.ReqConcentricityValue;
                    objPRL180.EditedBy = UserName;
                    objPRL180.EditedOn = DateTime.Now;

                    objPRL180.TOL = prl180.TOL;
                    objPRL180.IsAtTop = prl180.IsAtTop;
                    objPRL180.IsAtBottom = prl180.IsAtBottom;
                    objPRL180.ORIENTATIONLONGSEAMS = prl180.ORIENTATIONLONGSEAMS;
                    objPRL180.OUTOFROUNDNESS = prl180.OUTOFROUNDNESS;
                    objPRL180.TolWEPDimensionRootGap = prl180.TolWEPDimensionRootGap;
                    objPRL180.TolWEPDimensionRootFace = prl180.TolWEPDimensionRootFace;
                    objPRL180.TolWEPDimensionInsideWEPAngle = prl180.TolWEPDimensionInsideWEPAngle;
                    objPRL180.TolWEPDimensionOutsideWEPAngle = prl180.TolWEPDimensionOutsideWEPAngle;
                    objPRL180.AlignmentReadings = prl180.AlignmentReadings;
                    objPRL180.CheckPoint13 = prl180.CheckPoint13;
                    objPRL180.MaxAllowedConcentricity = prl180.MaxAllowedConcentricity;
                    objPRL180.InspectionAgency = prl180.InspectionAgency;
                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL180.ActFilledBy = UserName;
                            objPRL180.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL180.ReqFilledBy = UserName;
                            objPRL180.ReqFilledOn = DateTime.Now;
                        }
                    }

                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL180.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL180 objPRL180 = db.PRL180.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL180 != null && string.IsNullOrWhiteSpace(objPRL180.ProtocolNo))
                    {
                        db.PRL180.Remove(objPRL180);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL180.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL181> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL181> lstAddPRL181 = new List<PRL181>();
                List<PRL181> lstDeletePRL181 = new List<PRL181>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRL181 obj = db.PRL181.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.IdentificationMarking = item.IdentificationMarking;
                                obj.EditedBy = UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRL181 obj = new PRL181();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.IdentificationMarking))
                            {
                                obj.IdentificationMarking = item.IdentificationMarking;
                                obj.CreatedBy = UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRL181.Add(obj);
                            }
                        }
                    }
                }
                if (lstAddPRL181.Count > 0)
                {
                    db.PRL181.AddRange(lstAddPRL181);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL181 = db.PRL181.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL181 = db.PRL181.Where(x => x.HeaderId == HeaderId).ToList();
                }

                if (lstDeletePRL181.Count > 0)
                {
                    db.PRL181.RemoveRange(lstDeletePRL181);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2Linkage(List<PRL182> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL182> lstAddPRL182 = new List<PRL182>();
                List<PRL182> lstDeletePRL182 = new List<PRL182>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL182 obj = db.PRL182.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL182();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.RequiredValue = item.RequiredValue;
                        obj.ActualValue = item.ActualValue;

                        if (isAdded)
                        {
                            lstAddPRL182.Add(obj);
                        }
                    }
                }
                if (lstAddPRL182.Count > 0)
                {
                    db.PRL182.AddRange(lstAddPRL182);
                }


                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL182 = db.PRL182.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL182 = db.PRL182.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL182.Count > 0)
                {
                    db.PRL182.RemoveRange(lstDeletePRL182);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3Linkage(List<PRL183> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL183> lstAddPRL183 = new List<PRL183>();
                List<PRL183> lstDeletePRL183 = new List<PRL183>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL183 obj = db.PRL183.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(item.Orientation))
                            {
                                obj = new PRL183();
                                obj.HeaderId = item.HeaderId;
                                obj.CreatedBy = UserName;
                                obj.CreatedOn = DateTime.Now;
                                isAdded = true;
                            }
                        }

                        if (!string.IsNullOrWhiteSpace(item.Orientation))
                        {
                            obj.Orientation = item.Orientation;
                            obj.AtTopOpenEnd = item.AtTopOpenEnd;
                            obj.AtSeam = item.AtSeam;
                            obj.AtBottomOpenEnd = item.AtBottomOpenEnd;
                        }

                        if (isAdded)
                        {
                            lstAddPRL183.Add(obj);
                        }
                    }
                }
                if (lstAddPRL183.Count > 0)
                {
                    db.PRL183.AddRange(lstAddPRL183);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL183 = db.PRL183.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL183 = db.PRL183.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL183.Count > 0)
                {
                    db.PRL183.RemoveRange(lstDeletePRL183);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line4 details
        [HttpPost]
        public JsonResult SaveProtocolLine4Linkage(List<PRL184> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL184> lstAddPRL184 = new List<PRL184>();
                List<PRL184> lstDeletePRL184 = new List<PRL184>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL184 obj = db.PRL184.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL184();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SeamNo = item.SeamNo;
                        obj.ReqOrientation = item.ReqOrientation;
                        obj.ReqArcLength = item.ReqArcLength;
                        obj.ActArcLength = item.ActArcLength;

                        if (isAdded)
                        {
                            lstAddPRL184.Add(obj);
                        }
                    }
                }
                if (lstAddPRL184.Count > 0)
                {
                    db.PRL184.AddRange(lstAddPRL184);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL184 = db.PRL184.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL184 = db.PRL184.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL184.Count > 0)
                {
                    db.PRL184.RemoveRange(lstDeletePRL184);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line5 details
        [HttpPost]
        public JsonResult SaveProtocolLine5Linkage(List<PRL184_2> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL184_2> lstAddPRL184_2 = new List<PRL184_2>();
                List<PRL184_2> lstDeletePRL184_2 = new List<PRL184_2>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL184_2 obj = db.PRL184_2.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL184_2();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SpotNo = item.SpotNo;
                        obj.Offset = item.Offset;

                        if (isAdded)
                        {
                            lstAddPRL184_2.Add(obj);
                        }
                    }
                }
                if (lstAddPRL184_2.Count > 0)
                {
                    db.PRL184_2.AddRange(lstAddPRL184_2);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL184_2 = db.PRL184_2.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL184_2 = db.PRL184_2.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL184_2.Count > 0)
                {
                    db.PRL184_2.RemoveRange(lstDeletePRL184_2);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line6 details
        [HttpPost]
        public JsonResult SaveProtocolLine6Linkage(List<PRL184_3> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL184_3> lstAddPRL184_3 = new List<PRL184_3>();
                List<PRL184_3> lstDeletePRL184_3 = new List<PRL184_3>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL184_3 obj = db.PRL184_3.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            if (!string.IsNullOrWhiteSpace(item.Location))
                            {
                                obj = new PRL184_3();
                                obj.HeaderId = item.HeaderId;
                                obj.CreatedBy = UserName;
                                obj.CreatedOn = DateTime.Now;
                                isAdded = true;
                            }
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        if (!string.IsNullOrWhiteSpace(item.Location))
                        {
                            obj.Location = item.Location;
                            obj.ActAt0 = item.ActAt0;
                            obj.ActAt90 = item.ActAt90;
                            obj.ActAt180 = item.ActAt180;
                            obj.ActAt270 = item.ActAt270;
                        }

                        if (isAdded)
                        {
                            lstAddPRL184_3.Add(obj);
                        }
                    }
                }
                if (lstAddPRL184_3.Count > 0)
                {
                    db.PRL184_3.AddRange(lstAddPRL184_3);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL184_3 = db.PRL184_3.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL184_3 = db.PRL184_3.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL184_3.Count > 0)
                {
                    db.PRL184_3.RemoveRange(lstDeletePRL184_3);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line7 details
        [HttpPost]
        public JsonResult SaveProtocolLine7Linkage(List<PRL184_4> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL184_4> lstAddPRL184_4 = new List<PRL184_4>();
                List<PRL184_4> lstDeletePRL184_4 = new List<PRL184_4>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL184_4 obj = db.PRL184_4.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL184_4();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Description = item.Description;
                        obj.ReqValue = item.ReqValue;
                        obj.ActAt0 = item.ActAt0;
                        obj.ActAt90 = item.ActAt90;
                        obj.ActAt180 = item.ActAt180;
                        obj.ActAt270 = item.ActAt270;

                        if (isAdded)
                        {
                            lstAddPRL184_4.Add(obj);
                        }
                    }
                }
                if (lstAddPRL184_4.Count > 0)
                {
                    db.PRL184_4.AddRange(lstAddPRL184_4);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL184_4 = db.PRL184_4.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL184_4 = db.PRL184_4.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL184_4.Count > 0)
                {
                    db.PRL184_4.RemoveRange(lstDeletePRL184_4);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line8 details

        [HttpPost]
        public JsonResult SaveProtocolLine8Linkage(List<PRL184_5> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL184_5> lstAddPRL184_5 = new List<PRL184_5>();
                List<PRL184_5> lstDeletePRL184_5 = new List<PRL184_5>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL184_5 obj = db.PRL184_5.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL184_5();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SpotNo = item.SpotNo;
                        obj.Width = item.Width;
                        obj.Depth = item.Depth;

                        if (isAdded)
                        {
                            lstAddPRL184_5.Add(obj);
                        }
                    }
                }
                if (lstAddPRL184_5.Count > 0)
                {
                    db.PRL184_5.AddRange(lstAddPRL184_5);
                }

                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL184_5 = db.PRL184_5.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL184_5 = db.PRL184_5.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL184_5.Count > 0)
                {
                    db.PRL184_5.RemoveRange(lstDeletePRL184_5);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line9 details

        [HttpPost]
        public JsonResult SaveProtocolLine9Linkage(List<PRL184_6> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL184_6> lstAddPRL184_6 = new List<PRL184_6>();
                List<PRL184_6> lstDeletePRL184_6 = new List<PRL184_6>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL184_6 obj = db.PRL184_6.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL184_6();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.CleatNo = item.CleatNo;
                        obj.ParentMetalSize = item.ParentMetalSize;
                        obj.Location = item.Location;
                        obj.OutsideInside = item.OutsideInside;
                        obj.Distance = item.Distance;
                        obj.Orientation1 = item.Orientation1;
                        obj.Orientation2 = item.Orientation2;
                        obj.Orientation3 = item.Orientation3;

                        if (isAdded)
                        {
                            lstAddPRL184_6.Add(obj);
                        }
                    }
                }
                if (lstAddPRL184_6.Count > 0)
                {
                    db.PRL184_6.AddRange(lstAddPRL184_6);
                }


                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL184_6 = db.PRL184_6.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL184_6 = db.PRL184_6.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL184_6.Count > 0)
                {
                    db.PRL184_6.RemoveRange(lstDeletePRL184_6);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line10 details

        [HttpPost]
        public JsonResult SaveProtocolLine10Linkage(List<PRL184_7> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL184_7> lstAddPRL184_7 = new List<PRL184_7>();
                List<PRL184_7> lstDeletePRL184_7 = new List<PRL184_7>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL184_7 obj = db.PRL184_7.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL184_7();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Description = item.Description;
                        obj.ReqValue = item.ReqValue;
                        obj.ActValue = item.ActValue;

                        if (isAdded)
                        {
                            lstAddPRL184_7.Add(obj);
                        }
                    }
                }
                if (lstAddPRL184_7.Count > 0)
                {
                    db.PRL184_7.AddRange(lstAddPRL184_7);
                }


                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL184_7 = db.PRL184_7.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL184_7 = db.PRL184_7.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL184_7.Count > 0)
                {
                    db.PRL184_7.RemoveRange(lstDeletePRL184_7);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line11 details

        [HttpPost]
        public JsonResult SaveProtocolLine11Linkage(List<PRL184_8> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL184_8> lstAddPRL184_8 = new List<PRL184_8>();
                List<PRL184_8> lstDeletePRL184_8 = new List<PRL184_8>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL184_8 obj = db.PRL184_8.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL184_8();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Description = item.Description;
                        obj.ReqValue = item.ReqValue;
                        obj.ActValue = item.ActValue;

                        if (isAdded)
                        {
                            lstAddPRL184_8.Add(obj);
                        }
                    }
                }
                if (lstAddPRL184_8.Count > 0)
                {
                    db.PRL184_8.AddRange(lstAddPRL184_8);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL184_8 = db.PRL184_8.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL184_8 = db.PRL184_8.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL184_8.Count > 0)
                {
                    db.PRL184_8.RemoveRange(lstDeletePRL184_8);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line12 details
        [HttpPost]
        public JsonResult SaveProtocolLine12Linkage(List<PRL184_9> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRL184_9> lstAddPRL184_9 = new List<PRL184_9>();
                List<PRL184_9> lstDelete184_9 = new List<PRL184_9>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRL184_9 obj = db.PRL184_9.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.DrgNo = item.DrgNo;
                                obj.RevNo = item.RevNo;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRL184_9 obj = new PRL184_9();

                            if (!string.IsNullOrWhiteSpace(item.DrgNo))
                            {
                                obj.HeaderId = item.HeaderId;
                                obj.DrgNo = item.DrgNo;
                                obj.RevNo = item.RevNo;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRL184_9.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRL184_9.Count > 0)
                    {
                        db.PRL184_9.AddRange(lstAddPRL184_9);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDelete184_9 = db.PRL184_9.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDelete184_9.Count > 0)
                    {
                        db.PRL184_9.RemoveRange(lstDelete184_9);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDelete184_9 = db.PRL184_9.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDelete184_9.Count > 0)
                    {
                        db.PRL184_9.RemoveRange(lstDelete184_9);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        [HttpPost]
        public ActionResult getDrgNo(string ProjectNo)
        {
            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(ProjectNo).ToList();
            lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            return Json(lstdrawing.Select(x => new
            {
                Value = x,
                Text = x
            }).ToList(), JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}