﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsBase;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol038Controller : clsBase
    {
        // GET: PROTOCOL/Protocol038

        string ControllerURL = "/PROTOCOL/Protocol038/";
        string Title = "SET-UP INSPECTION REPORT FOR FORMED HEMISPHERICAL D'END";
        //IEMQS.DrawingReference.hedProjectDocListWebServiceClient drawref = new IEMQS.DrawingReference.hedProjectDocListWebServiceClient();

        #region Details View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO185 objPRO185 = new PRO185();
            if (!id.HasValue)
            {
                try
                {
                    objPRO185.ProtocolNo = string.Empty;
                    objPRO185.CreatedBy = objClsLoginInfo.UserName;
                    objPRO185.CreatedOn = DateTime.Now;

                    #region DIMENSION
                    objPRO185.OverCrowingRemarks = "TOL: 1¼ (1.25) % OF NOMINAL I/S DIA.";
                    objPRO185.UnderCrowingRemarks = "TOL:5/8 (0.625) % OF NOMINAL I/S DIA.";
                    #endregion                      

                    #region OVALITY
                    objPRO185.PRO186.Add(new PRO186
                    {
                        Orientation = "0°-180° ",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO185.PRO186.Add(new PRO186
                    {
                        Orientation = "22.5°-202.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO185.PRO186.Add(new PRO186
                    {
                        Orientation = "45°-225°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO185.PRO186.Add(new PRO186
                    {
                        Orientation = "67.5°-247.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO185.PRO186.Add(new PRO186
                    {
                        Orientation = "90°-270°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO185.PRO186.Add(new PRO186
                    {
                        Orientation = "112.5°-292.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO185.PRO186.Add(new PRO186
                    {
                        Orientation = "135°-315°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO185.PRO186.Add(new PRO186
                    {
                        Orientation = "157.5°-337.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion

                    #region  THICKNESS
                    objPRO185.PRO187.Add(new PRO187
                    {
                        ThiknessColumn1 = "PETAL 1",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO185.PRO187.Add(new PRO187
                    {
                        ThiknessColumn1 = "PETAL 2",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO185.PRO187.Add(new PRO187
                    {
                        ThiknessColumn1 = "PETAL 3",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO185.PRO187.Add(new PRO187
                    {
                        ThiknessColumn1 = "PETAL 4",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion

                    #region DETAIL THICKNESS REPORT
                    objPRO185.PRO189_4.Add(new PRO189_4
                    {
                        Description = "PETAL 1",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO185.PRO189_4.Add(new PRO189_4
                    {
                        Description = "PETAL 2",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO185.PRO189_4.Add(new PRO189_4
                    {
                        Description = "PETAL 3",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO185.PRO189_4.Add(new PRO189_4
                    {
                        Description = "PETAL 4",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion



                    db.PRO185.Add(objPRO185);
                    db.SaveChanges();
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO185.HeaderId;
            }
            else
            {
                objPRO185 = db.PRO185.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            List<string> lsttypesofforming = new List<string> { "HOT", "COLD", "WARM" };

            //List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            ViewBag.TypesOfForming = lsttypesofforming.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();


            ViewBag.OutsideOrInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO186> lstPRO186 = db.PRO186.Where(x => x.HeaderId == objPRO185.HeaderId).ToList();
            List<PRO187> lstPRO187 = db.PRO187.Where(x => x.HeaderId == objPRO185.HeaderId).ToList();
            List<PRO188> lstPRO188 = db.PRO188.Where(x => x.HeaderId == objPRO185.HeaderId).ToList();
            List<PRO189> lstPRO189 = db.PRO189.Where(x => x.HeaderId == objPRO185.HeaderId).ToList();
            List<PRO189_2> lstPRO189_2 = db.PRO189_2.Where(x => x.HeaderId == objPRO185.HeaderId).ToList();
            List<PRO189_3> lstPRO189_3 = db.PRO189_3.Where(x => x.HeaderId == objPRO185.HeaderId).ToList();
            List<PRO189_4> lstPRO189_4 = db.PRO189_4.Where(x => x.HeaderId == objPRO185.HeaderId).ToList();

            List<SP_IPI_PROTOCOL038_GET_LINES8_DATA_Result> lstPRO189_5 = db.SP_IPI_PROTOCOL038_GET_LINES8_DATA(1, int.MaxValue, "", " HeaderId = " + objPRO185.HeaderId).ToList();
            List<SP_IPI_PROTOCOL038_GET_LINES9_DATA_Result> lstPRO189_6 = db.SP_IPI_PROTOCOL038_GET_LINES9_DATA(1, int.MaxValue, "", " HeaderId = " + objPRO185.HeaderId).ToList();


            ViewBag.lstPRO186 = lstPRO186;
            ViewBag.lstPRO187 = lstPRO187;
            ViewBag.lstPRO188 = lstPRO188;
            ViewBag.lstPRO189 = lstPRO189;
            ViewBag.lstPRO189_2 = lstPRO189_2;
            ViewBag.lstPRO189_3 = lstPRO189_3;
            ViewBag.lstPRO189_4 = lstPRO189_4;
            ViewBag.lstPRO189_5 = lstPRO189_5;
            ViewBag.lstPRO189_6 = lstPRO189_6;
            #endregion

            return View(objPRO185);
        }
        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }
        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO185 PRO185)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO185.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO185 objPRO185 = db.PRO185.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO185.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO185.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data
                        objPRO185.ProtocolNo = PRO185.ProtocolNo;
                        objPRO185.InsideCircumferenceRequired = PRO185.InsideCircumferenceRequired;
                        objPRO185.InsideCircumferenceActual = PRO185.InsideCircumferenceActual;
                        objPRO185.InsideCircumferenceRemarks = PRO185.InsideCircumferenceRemarks;
                        objPRO185.TotalHeightRequired = PRO185.TotalHeightRequired;
                        objPRO185.TotalHeightActual = PRO185.TotalHeightActual;
                        objPRO185.TotalHeightRemarks = PRO185.TotalHeightRemarks;
                        objPRO185.OverCrowingRequired = PRO185.OverCrowingRequired;
                        objPRO185.OverCrowingActual = PRO185.OverCrowingActual;
                        objPRO185.OverCrowingRemarks = PRO185.OverCrowingRemarks;
                        objPRO185.UnderCrowingRequired = PRO185.UnderCrowingRequired;
                        objPRO185.UnderCrowingActual = PRO185.UnderCrowingActual;
                        objPRO185.UnderCrowingRemarks = PRO185.UnderCrowingRemarks;
                        objPRO185.ConcentricityRequired = PRO185.ConcentricityRequired;
                        objPRO185.ConcentricityActual = PRO185.ConcentricityActual;
                        objPRO185.ConcentricityRemarks = PRO185.ConcentricityRemarks;
                        objPRO185.TypeOfFormingRequired = PRO185.TypeOfFormingRequired;
                        objPRO185.TypeOfFormingActual = PRO185.TypeOfFormingActual;
                        objPRO185.TypeOfFormingRemarks = PRO185.TypeOfFormingRemarks;
                        objPRO185.Ovality = PRO185.Ovality;
                        objPRO185.OutBy = PRO185.OutBy;
                        objPRO185.ThiknessReqMin = PRO185.ThiknessReqMin;
                        objPRO185.ThiknessReqNom = PRO185.ThiknessReqNom;
                        objPRO185.EvalueTemplate = PRO185.EvalueTemplate;
                        objPRO185.ChordLengthReq = PRO185.ChordLengthReq;
                        objPRO185.ChordLengthAct = PRO185.ChordLengthAct;
                        objPRO185.RadiusReq = PRO185.RadiusReq;
                        objPRO185.RadiusAct = PRO185.RadiusAct;
                        objPRO185.GapAllowable = PRO185.GapAllowable;
                        objPRO185.GapAct = PRO185.GapAct;
                        objPRO185.RootGapReq = PRO185.RootGapReq;
                        objPRO185.RootFaceReq = PRO185.RootFaceReq;
                        objPRO185.Pick_in_out = PRO185.Pick_in_out;
                        objPRO185.OffSet = PRO185.OffSet;
                        objPRO185.Inside_Wep_Angle = PRO185.Inside_Wep_Angle;
                        objPRO185.Outside_Wep_Angle = PRO185.Outside_Wep_Angle;
                        objPRO185.Detail_Thikness_Report = PRO185.Detail_Thikness_Report;
                        objPRO185.CheckPoint1 = PRO185.CheckPoint1;
                        objPRO185.CheckPoint2 = PRO185.CheckPoint2;
                        objPRO185.CheckPoint3 = PRO185.CheckPoint3;
                        objPRO185.CheckPoint4 = PRO185.CheckPoint4;
                        objPRO185.CheckPoint5 = PRO185.CheckPoint5;
                        objPRO185.CheckPoint6 = PRO185.CheckPoint6;
                        objPRO185.CheckPoint6_2 = PRO185.CheckPoint6_2;
                        objPRO185.CheckPoint6_3 = PRO185.CheckPoint6_3;
                        objPRO185.CheckPoint7 = PRO185.CheckPoint7;
                        objPRO185.CheckPoint7_2 = PRO185.CheckPoint7_2;
                        objPRO185.CheckPoint7_3 = PRO185.CheckPoint7_3;
                        objPRO185.CheckPoint7_4 = PRO185.CheckPoint7_4;
                        objPRO185.CheckPoint9 = PRO185.CheckPoint9;
                        objPRO185.CheckPoint10 = PRO185.CheckPoint10;
                        objPRO185.CheckPoint11 = PRO185.CheckPoint11;
                        objPRO185.QCRemarks = PRO185.QCRemarks;
                        objPRO185.Result = PRO185.Result;
                        objPRO185.ReqCladStripingWidth = PRO185.ReqCladStripingWidth;
                        objPRO185.ReqCladStripingDepth = PRO185.ReqCladStripingDepth;
                        objPRO185.ReqCladStripingOffset = PRO185.ReqCladStripingOffset;
                        objPRO185.Note1 = PRO185.Note1;
                        objPRO185.Note2 = PRO185.Note2;
                        objPRO185.Note3 = PRO185.Note3;


                        objPRO185.IdentificationOfCleats = PRO185.IdentificationOfCleats;
                        objPRO185.EditedBy = objClsLoginInfo.UserName;
                        objPRO185.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO185.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO185 objPRO185 = db.PRO185.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO185 != null && string.IsNullOrWhiteSpace(objPRO185.ProtocolNo))
                    {
                        db.PRO185.Remove(objPRO185);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO185.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion


        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO186> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO186> lstAddPRO186 = new List<PRO186>();
                List<PRO186> lstDeletePRO186 = new List<PRO186>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO186 obj = db.PRO186.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.Orientation = item.Orientation;
                                obj.Actual = item.Actual;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO186 obj = new PRO186();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.Orientation))
                            {
                                obj.Orientation = item.Orientation;
                                obj.Actual = item.Actual;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO186.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO186.Count > 0)
                    {
                        db.PRO186.AddRange(lstAddPRO186);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO186 = db.PRO186.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO186.Count > 0)
                    {
                        db.PRO186.RemoveRange(lstDeletePRO186);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO186 = db.PRO186.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO186.Count > 0)
                    {
                        db.PRO186.RemoveRange(lstDeletePRO186);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2(List<PRO187> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO187> lstAddPRO187 = new List<PRO187>();
                List<PRO187> lstDeletePRO187 = new List<PRO187>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO187 obj = db.PRO187.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO187();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.ThiknessColumn1 = item.ThiknessColumn1;
                        obj.ThiknessColumn2 = item.ThiknessColumn2;
                        if (isAdded)
                        {
                            lstAddPRO187.Add(obj);
                        }
                    }
                    if (lstAddPRO187.Count > 0)
                    {
                        db.PRO187.AddRange(lstAddPRO187);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO187 = db.PRO187.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO187.Count > 0)
                    {
                        db.PRO187.RemoveRange(lstDeletePRO187);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO187 = db.PRO187.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO187.Count > 0)
                    {
                        db.PRO187.RemoveRange(lstDeletePRO187);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3(List<PRO188> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO188> lstAddPRO188 = new List<PRO188>();
                List<PRO188> lstDeletePRO188 = new List<PRO188>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO188 obj = db.PRO188.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        else
                        {
                            obj = new PRO188();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        obj.SeamNo = item.SeamNo;
                        obj.ReqOrien = item.ReqOrien;
                        obj.ReqArcLength = item.ReqArcLength;
                        obj.ActualArcLength = item.ActualArcLength;


                        if (isAdded)
                        {
                            lstAddPRO188.Add(obj);
                        }
                    }
                    if (lstAddPRO188.Count > 0)
                    {
                        db.PRO188.AddRange(lstAddPRO188);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO188 = db.PRO188.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO188.Count > 0)
                    {
                        db.PRO188.RemoveRange(lstDeletePRO188);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO188 = db.PRO188.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO188.Count > 0)
                    {
                        db.PRO188.RemoveRange(lstDeletePRO188);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line4 details
        [HttpPost]
        public JsonResult SaveProtocolLine4(List<PRO189> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO189> lstAddPRO189 = new List<PRO189>();
                List<PRO189> lstDeletePRO189 = new List<PRO189>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO189 obj = db.PRO189.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO189();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SeamNo = item.SeamNo;
                        obj.RootGapMin = item.RootGapMin;
                        obj.RootGapMax = item.RootGapMax;
                        obj.RootFaceMin = item.RootFaceMin;
                        obj.RootFaceMax = item.RootFaceMax;
                        obj.Peak_in_out_Top = item.Peak_in_out_Top;
                        obj.Peak_in_out_Mid = item.Peak_in_out_Mid;
                        obj.Peak_in_out_Bottom = item.Peak_in_out_Bottom;

                        obj.OffSetMin = item.OffSetMin;
                        obj.OffSetMax = item.OffSetMax;
                        obj.Inside_Wep_AngleMin = item.Inside_Wep_AngleMin;
                        obj.Inside_Wep_AngleMax = item.Inside_Wep_AngleMax;
                        obj.Outside_Wep_AngleMin = item.Outside_Wep_AngleMin;
                        obj.Outside_Wep_AngleMax = item.Outside_Wep_AngleMax;

                        if (isAdded)
                        {
                            lstAddPRO189.Add(obj);
                        }
                    }
                    if (lstAddPRO189.Count > 0)
                    {
                        db.PRO189.AddRange(lstAddPRO189);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO189 = db.PRO189.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO189.Count > 0)
                    {
                        db.PRO189.RemoveRange(lstDeletePRO189);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO189 = db.PRO189.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO189.Count > 0)
                    {
                        db.PRO189.RemoveRange(lstDeletePRO189);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        //#region Line5 details
        //[HttpPost]
        //public JsonResult SaveProtocolLine5(List<PRO189_2> lst, int HeaderId)
        //{
        //    clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
        //    try
        //    {
        //        List<PRO189_2> lstAddPRO189_2 = new List<PRO189_2>();
        //        List<PRO189_2> lstDeletePRO189_2 = new List<PRO189_2>();
        //        if (lst != null)
        //        {
        //            foreach (var item in lst)
        //            {
        //                bool isAdded = false;
        //                PRO189_2 obj = db.PRO189_2.Where(x => x.LineId == item.LineId).FirstOrDefault();
        //                if (obj == null)
        //                {
        //                    obj = new PRO189_2();
        //                    obj.HeaderId = item.HeaderId;
        //                    obj.CreatedBy = objClsLoginInfo.UserName;
        //                    obj.CreatedOn = DateTime.Now;
        //                    isAdded = true;
        //                }
        //                else
        //                {
        //                    obj.EditedBy = objClsLoginInfo.UserName;
        //                    obj.EditedOn = DateTime.Now;
        //                }
        //                obj.SeamNo = item.SeamNo;
        //                obj.OffSetMin = item.OffSetMin;
        //                obj.OffSetMax = item.OffSetMax;
        //                obj.Inside_Wep_AngleMin = item.Inside_Wep_AngleMin;
        //                obj.Inside_Wep_AngleMax = item.Inside_Wep_AngleMax;
        //                obj.Outside_Wep_AngleMin = item.Outside_Wep_AngleMin;
        //                obj.Outside_Wep_AngleMax = item.Outside_Wep_AngleMax;
        //                if (isAdded)
        //                {
        //                    lstAddPRO189_2.Add(obj);
        //                }
        //            }
        //            if (lstAddPRO189_2.Count > 0)
        //            {
        //                db.PRO189_2.AddRange(lstAddPRO189_2);
        //            }

        //            var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

        //            lstDeletePRO189_2 = db.PRO189_2.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
        //            if (lstDeletePRO189_2.Count > 0)
        //            {
        //                db.PRO189_2.RemoveRange(lstDeletePRO189_2);
        //            }
        //            db.SaveChanges();
        //        }
        //        else
        //        {
        //            lstDeletePRO189_2 = db.PRO189_2.Where(x => x.HeaderId == HeaderId).ToList();
        //            if (lstDeletePRO189_2.Count > 0)
        //            {
        //                db.PRO189_2.RemoveRange(lstDeletePRO189_2);
        //            }
        //            db.SaveChanges();
        //        }

        //        objResponseMsg.Key = true;
        //    }
        //    catch (Exception ex)
        //    {
        //        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        //        objResponseMsg.Key = false;
        //        objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
        //    }
        //    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        //}
        //#endregion

        #region Line6 details
        [HttpPost]
        public JsonResult SaveProtocolLine6(List<PRO189_3> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO189_3> lstAddPRO189_3 = new List<PRO189_3>();
                List<PRO189_3> lstDeletePRO189_3 = new List<PRO189_3>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO189_3 obj = db.PRO189_3.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO189_3();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        obj.MaterialIdentificationReport = item.MaterialIdentificationReport;

                        if (isAdded)
                        {
                            lstAddPRO189_3.Add(obj);
                        }
                    }
                    if (lstAddPRO189_3.Count > 0)
                    {
                        db.PRO189_3.AddRange(lstAddPRO189_3);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO189_3 = db.PRO189_3.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO189_3.Count > 0)
                    {
                        db.PRO189_3.RemoveRange(lstDeletePRO189_3);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO189_3 = db.PRO189_3.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO189_3.Count > 0)
                    {
                        db.PRO189_3.RemoveRange(lstDeletePRO189_3);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line7 details
        [HttpPost]
        public JsonResult SaveProtocolLine7(List<PRO189_4> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO189_4> lstAddPRO189_4 = new List<PRO189_4>();
                List<PRO189_4> lstDeletePRO189_4 = new List<PRO189_4>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO189_4 obj = db.PRO189_4.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO189_4();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        obj.Description = item.Description;
                        obj.Petal_1 = item.Petal_1;
                        obj.Petal_2 = item.Petal_2;
                        obj.Petal_3 = item.Petal_3;
                        obj.Petal_4 = item.Petal_4;
                        obj.Petal_5 = item.Petal_5;
                        obj.Petal_6 = item.Petal_6;
                        obj.Petal_7 = item.Petal_7;
                        obj.Petal_8 = item.Petal_8;
                        obj.Petal_9 = item.Petal_9;
                        obj.Petal_10 = item.Petal_10;

                        if (isAdded)
                        {
                            lstAddPRO189_4.Add(obj);
                        }
                    }
                    if (lstAddPRO189_4.Count > 0)
                    {
                        db.PRO189_4.AddRange(lstAddPRO189_4);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO189_4 = db.PRO189_4.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO189_4.Count > 0)
                    {
                        db.PRO189_4.RemoveRange(lstDeletePRO189_4);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO189_4 = db.PRO189_4.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO189_4.Count > 0)
                    {
                        db.PRO189_4.RemoveRange(lstDeletePRO189_4);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line8 details

        [HttpPost]
        public JsonResult SaveProtocolLine8(List<PRO189_5> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO189_5> lstAddPRO189_5 = new List<PRO189_5>();
                List<PRO189_5> lstDeletePRO189_5 = new List<PRO189_5>();

                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO189_5 obj = db.PRO189_5.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        PRO189_5 objFecthSeam = new PRO189_5();
                        if (obj == null)
                        {
                            //if (!string.IsNullOrWhiteSpace(item.Depth))
                            {
                                obj = new PRO189_5();
                                obj.HeaderId = item.HeaderId;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                isAdded = true;
                            }
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        objFecthSeam = db.PRO189_5.Where(x => x.SeamNo == item.SeamNo && x.HeaderId == item.HeaderId).FirstOrDefault();
                        if (objFecthSeam != null)
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                            //obj.Depth = objFecthSeam.Depth;
                            //obj.Width = objFecthSeam.Width;
                            //obj.Offset = objFecthSeam.Offset;
                            //obj.SpotNoClad = objFecthSeam.SpotNoClad;
                        }
                        else
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                            //obj.Depth = item.Depth;
                            //obj.Width = item.Width;
                            //obj.Offset = item.Offset;
                            //obj.SpotNoClad = item.SpotNoClad;
                        }

                        obj.SpotNo = item.SpotNo;
                        obj.Depth = item.Depth;
                        obj.Width = item.Width;
                        obj.Offset = item.Offset;
                        obj.SpotNoClad = item.SpotNoClad;
                        if (isAdded)
                        {
                            lstAddPRO189_5.Add(obj);
                        }
                    }
                }
                if (lstAddPRO189_5.Count > 0)
                {
                    db.PRO189_5.AddRange(lstAddPRO189_5);
                }
                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRO189_5 = db.PRO189_5.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRO189_5 = db.PRO189_5.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRO189_5.Count > 0)
                {
                    db.PRO189_5.RemoveRange(lstDeletePRO189_5);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line9 details

        [HttpPost]
        public JsonResult SaveProtocolLine9(List<PRO189_6> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO189_6> lstAddPRO189_6 = new List<PRO189_6>();
                List<PRO189_6> lstDeletePRO189_6 = new List<PRO189_6>();

                if (lst != null)
                {

                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO189_6 obj = db.PRO189_6.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        PRO189_6 objFecthSeam = new PRO189_6();
                        if (obj == null)
                        {
                            obj = new PRO189_6();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        objFecthSeam = db.PRO189_6.Where(x => x.SeamNo == item.SeamNo && x.HeaderId == item.HeaderId).FirstOrDefault();
                        if (objFecthSeam != null)
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                        }
                        else
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                        }

                        obj.CleatNo = item.CleatNo;
                        obj.SizeWelded = item.SizeWelded;
                        obj.Location = item.Location;
                        obj.OutsideOrInside = item.OutsideOrInside;
                        obj.DistanceFromSeam = item.DistanceFromSeam;
                        obj.ShellEvelution = item.ShellEvelution;
                        if (isAdded)
                        {
                            lstAddPRO189_6.Add(obj);
                        }
                    }
                }
                if (lstAddPRO189_6.Count > 0)
                {
                    db.PRO189_6.AddRange(lstAddPRO189_6);
                }
                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRO189_6 = db.PRO189_6.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else { lstDeletePRO189_6 = db.PRO189_6.Where(x => x.HeaderId == HeaderId).ToList(); }
                if (lstDeletePRO189_6.Count > 0)
                {
                    db.PRO189_6.RemoveRange(lstDeletePRO189_6);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #endregion


        #region Linkage View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL185 objPRL185 = new PRL185();
            objPRL185 = db.PRL185.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL185 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL185.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;

                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lsttypesofforming = new List<string> { "HOT", "COLD", "WARM" };


            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.TypesOfForming = lsttypesofforming.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            ViewBag.OutsideOrInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines

            List<PRL186> lstPRL186 = db.PRL186.Where(x => x.HeaderId == objPRL185.HeaderId).ToList();
            List<PRL187> lstPRL187 = db.PRL187.Where(x => x.HeaderId == objPRL185.HeaderId).ToList();
            List<PRL188> lstPRL188 = db.PRL188.Where(x => x.HeaderId == objPRL185.HeaderId).ToList();
            List<PRL189> lstPRL189 = db.PRL189.Where(x => x.HeaderId == objPRL185.HeaderId).ToList();
            List<PRL189_2> lstPRL189_2 = db.PRL189_2.Where(x => x.HeaderId == objPRL185.HeaderId).ToList();
            List<PRL189_3> lstPRL189_3 = db.PRL189_3.Where(x => x.HeaderId == objPRL185.HeaderId).ToList();
            List<PRL189_4> lstPRL189_4 = db.PRL189_4.Where(x => x.HeaderId == objPRL185.HeaderId).ToList();

            List<SP_IPI_PROTOCOL038_LINKAGE_GET_LINES8_DATA_Result> lstPRL189_5 = db.SP_IPI_PROTOCOL038_LINKAGE_GET_LINES8_DATA(1, int.MaxValue, "", " HeaderId = " + objPRL185.HeaderId).ToList();
            List<SP_IPI_PROTOCOL038_LINKAGE_GET_LINES9_DATA_Result> lstPRL189_6 = db.SP_IPI_PROTOCOL038_LINKAGE_GET_LINES9_DATA(1, int.MaxValue, "", " HeaderId = " + objPRL185.HeaderId).ToList();


            ViewBag.lstPRL186 = lstPRL186;
            ViewBag.lstPRL187 = lstPRL187;
            ViewBag.lstPRL188 = lstPRL188;
            ViewBag.lstPRL189 = lstPRL189;
            ViewBag.lstPRL189_2 = lstPRL189_2;
            ViewBag.lstPRL189_3 = lstPRL189_3;
            ViewBag.lstPRL189_4 = lstPRL189_4;
            ViewBag.lstPRL189_5 = lstPRL189_5;
            ViewBag.lstPRL189_6 = lstPRL189_6;
            #endregion

            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.DrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL185.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL185.ActFilledBy) && (objPRL185.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL185.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL185);
        }

        [HttpPost]
        public ActionResult LinkageMobile(int? id, int? m, string APIRequestFrom = "", string UserName = "")
        {
            string ProjectDesc = "";
            string ManufacturingCode = "";
            string AccessRole = "";
            bool isEditable = false;
            var DrawingNo = new List<string>();
            bool isQCRemarkEditable = false;

            PRL185 objPRL185 = new PRL185();
            db.Configuration.ProxyCreationEnabled = false;
            objPRL185 = db.PRL185.Where(i => i.HeaderId == id).FirstOrDefault();
            db.Entry(objPRL185).State = System.Data.Entity.EntityState.Detached;
            NDEModels objNDEModels = new NDEModels();
            if (objPRL185 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL185.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
            }
            
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
            AccessRole = objUserRoleAccessDetails.UserDesignation;
           

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lsttypesofforming = new List<string> { "HOT", "COLD", "WARM" };


            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            var YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var TypesOfForming = lsttypesofforming.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var OutsideOrInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines

            List<PRL186> lstPRL186 = db.PRL186.Where(x => x.HeaderId == objPRL185.HeaderId).ToList();
            List<PRL187> lstPRL187 = db.PRL187.Where(x => x.HeaderId == objPRL185.HeaderId).ToList();
            List<PRL188> lstPRL188 = db.PRL188.Where(x => x.HeaderId == objPRL185.HeaderId).ToList();
            List<PRL189> lstPRL189 = db.PRL189.Where(x => x.HeaderId == objPRL185.HeaderId).ToList();
            List<PRL189_2> lstPRL189_2 = db.PRL189_2.Where(x => x.HeaderId == objPRL185.HeaderId).ToList();
            List<PRL189_3> lstPRL189_3 = db.PRL189_3.Where(x => x.HeaderId == objPRL185.HeaderId).ToList();
            List<PRL189_4> lstPRL189_4 = db.PRL189_4.Where(x => x.HeaderId == objPRL185.HeaderId).ToList();

            List<SP_IPI_PROTOCOL038_LINKAGE_GET_LINES8_DATA_Result> lstPRL189_5 = db.SP_IPI_PROTOCOL038_LINKAGE_GET_LINES8_DATA(1, int.MaxValue, "", " HeaderId = " + objPRL185.HeaderId).ToList();
            List<SP_IPI_PROTOCOL038_LINKAGE_GET_LINES9_DATA_Result> lstPRL189_6 = db.SP_IPI_PROTOCOL038_LINKAGE_GET_LINES9_DATA(1, int.MaxValue, "", " HeaderId = " + objPRL185.HeaderId).ToList();

           
            #endregion
            try
            {
                isEditable = false;
                isQCRemarkEditable = false;
                if (m == 1)
                {
                    isEditable = true;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL185.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL185.ActFilledBy) && (objPRL185.RequestNo.HasValue))
                        {
                            isEditable = false;
                            isQCRemarkEditable = true;
                        }
                        else
                        {
                            DrawingNo = (new GeneralController()).GetPLMDrawingNumber(objPRL185.Project).ToList();
                        }
                    }
                }
                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    ProjectDesc = ProjectDesc,
                    ManufacturingCode = ManufacturingCode,
                    isEditable = isEditable,
                    isQCRemarkEditable = isQCRemarkEditable,
                    DrawingNo = DrawingNo,
                    AccessRole = AccessRole,
                    YesNoEnum = YesNoEnum,
                    YesNAEnum = YesNAEnum,
                    OutsideInside = OutsideOrInside,

                    objPRL185 = objPRL185,

                    lstPRL186 = lstPRL186,
                    lstPRL187 = lstPRL187,
                    lstPRL188 = lstPRL188,
                    lstPRL189 = lstPRL189,
                    lstPRL189_2 = lstPRL189_2,
                    lstPRL189_3 = lstPRL189_3,
                    lstPRL189_4 = lstPRL189_4,
                    lstPRL189_5 = lstPRL189_5,
                    lstPRL189_6 = lstPRL189_6,
                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                return Json(new
                {
                    res = "0",
                    msg = ex.Message,
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL185 prl185, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = prl185.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL185 objPRL185 = db.PRL185.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL185.DrawingRevisionNo = prl185.DrawingRevisionNo;
                    objPRL185.DrawingNo = prl185.DrawingNo;
                    objPRL185.DCRNo = prl185.DCRNo;
                    objPRL185.ProtocolNo = prl185.ProtocolNo;

                    objPRL185.InsideCircumferenceRequired = prl185.InsideCircumferenceRequired;
                    objPRL185.InsideCircumferenceActual = prl185.InsideCircumferenceActual;
                    objPRL185.InsideCircumferenceRemarks = prl185.InsideCircumferenceRemarks;
                    objPRL185.TotalHeightRequired = prl185.TotalHeightRequired;
                    objPRL185.TotalHeightActual = prl185.TotalHeightActual;
                    objPRL185.TotalHeightRemarks = prl185.TotalHeightRemarks;
                    objPRL185.OverCrowingRequired = prl185.OverCrowingRequired;
                    objPRL185.OverCrowingActual = prl185.OverCrowingActual;
                    objPRL185.OverCrowingRemarks = prl185.OverCrowingRemarks;
                    objPRL185.UnderCrowingRequired = prl185.UnderCrowingRequired;
                    objPRL185.UnderCrowingActual = prl185.UnderCrowingActual;
                    objPRL185.UnderCrowingRemarks = prl185.UnderCrowingRemarks;
                    objPRL185.ConcentricityRequired = prl185.ConcentricityRequired;
                    objPRL185.ConcentricityActual = prl185.ConcentricityActual;
                    objPRL185.ConcentricityRemarks = prl185.ConcentricityRemarks;
                    objPRL185.TypeOfFormingRequired = prl185.TypeOfFormingRequired;
                    objPRL185.TypeOfFormingActual = prl185.TypeOfFormingActual;
                    objPRL185.TypeOfFormingRemarks = prl185.TypeOfFormingRemarks;
                    objPRL185.Ovality = prl185.Ovality;
                    objPRL185.OutBy = prl185.OutBy;
                    objPRL185.ThiknessReqMin = prl185.ThiknessReqMin;
                    objPRL185.ThiknessReqNom = prl185.ThiknessReqNom;
                    objPRL185.EvalueTemplate = prl185.EvalueTemplate;
                    objPRL185.ChordLengthReq = prl185.ChordLengthReq;
                    objPRL185.ChordLengthAct = prl185.ChordLengthAct;
                    objPRL185.RadiusReq = prl185.RadiusReq;
                    objPRL185.RadiusAct = prl185.RadiusAct;
                    objPRL185.GapAllowable = prl185.GapAllowable;
                    objPRL185.GapAct = prl185.GapAct;
                    objPRL185.RootGapReq = prl185.RootGapReq;
                    objPRL185.RootFaceReq = prl185.RootFaceReq;
                    objPRL185.Pick_in_out = prl185.Pick_in_out;
                    objPRL185.OffSet = prl185.OffSet;
                    objPRL185.Inside_Wep_Angle = prl185.Inside_Wep_Angle;
                    objPRL185.Outside_Wep_Angle = prl185.Outside_Wep_Angle;
                    objPRL185.Detail_Thikness_Report = prl185.Detail_Thikness_Report;
                    objPRL185.CheckPoint1 = prl185.CheckPoint1;
                    objPRL185.CheckPoint2 = prl185.CheckPoint2;
                    objPRL185.CheckPoint3 = prl185.CheckPoint3;
                    objPRL185.CheckPoint4 = prl185.CheckPoint4;
                    objPRL185.CheckPoint5 = prl185.CheckPoint5;
                    objPRL185.CheckPoint6 = prl185.CheckPoint6;
                    objPRL185.CheckPoint6_2 = prl185.CheckPoint6_2;
                    objPRL185.CheckPoint6_3 = prl185.CheckPoint6_3;
                    objPRL185.CheckPoint7 = prl185.CheckPoint7;
                    objPRL185.CheckPoint7_2 = prl185.CheckPoint7_2;
                    objPRL185.CheckPoint7_3 = prl185.CheckPoint7_3;
                    objPRL185.CheckPoint7_4 = prl185.CheckPoint7_4;
                    objPRL185.CheckPoint9 = prl185.CheckPoint9;
                    objPRL185.CheckPoint10 = prl185.CheckPoint10;
                    objPRL185.CheckPoint11 = prl185.CheckPoint11;
                    objPRL185.QCRemarks = prl185.QCRemarks;
                    objPRL185.Result = prl185.Result;
                    objPRL185.ReqCladStripingWidth = prl185.ReqCladStripingWidth;
                    objPRL185.ReqCladStripingDepth = prl185.ReqCladStripingDepth;
                    objPRL185.ReqCladStripingOffset = prl185.ReqCladStripingOffset;
                    objPRL185.Note1 = prl185.Note1;
                    objPRL185.Note2 = prl185.Note2;
                    objPRL185.Note3 = prl185.Note3;


                    objPRL185.IdentificationOfCleats = prl185.IdentificationOfCleats;
                    objPRL185.EditedBy = UserName;
                    objPRL185.EditedOn = DateTime.Now;
                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL185.ActFilledBy = UserName;
                            objPRL185.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL185.ReqFilledBy = UserName;
                            objPRL185.ReqFilledOn = DateTime.Now;
                        }
                    }
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL185.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL185 objPRL185 = db.PRL185.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL185 != null && string.IsNullOrWhiteSpace(objPRL185.ProtocolNo))
                    {
                        db.PRL185.Remove(objPRL185);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL185.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL186> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL186> lstAddPRL186 = new List<PRL186>();
                List<PRL186> lstDeletePRL186 = new List<PRL186>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRL186 obj = db.PRL186.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.Orientation = item.Orientation;
                                obj.Actual = item.Actual;
                                obj.EditedBy = UserName;
                                obj.EditedOn = DateTime.Now;


                            }
                        }
                        else
                        {
                            PRL186 obj = new PRL186();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.Orientation))
                            {
                                obj.Orientation = item.Orientation;
                                obj.Actual = item.Actual;
                                obj.EditedBy = UserName;
                                obj.EditedOn = DateTime.Now;
                                obj.CreatedBy = UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRL186.Add(obj);
                            }
                        }
                    }
                }
                if (lstAddPRL186.Count > 0)
                {
                    db.PRL186.AddRange(lstAddPRL186);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL186 = db.PRL186.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL186 = db.PRL186.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL186.Count > 0)
                {
                    db.PRL186.RemoveRange(lstDeletePRL186);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2Linkage(List<PRL187> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL187> lstAddPRL187 = new List<PRL187>();
                List<PRL187> lstDeletePRL187 = new List<PRL187>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL187 obj = db.PRL187.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL187();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.ThiknessColumn1 = item.ThiknessColumn1;
                        obj.ThiknessColumn2 = item.ThiknessColumn2;

                        if (isAdded)
                        {
                            lstAddPRL187.Add(obj);
                        }
                    }
                }
                if (lstAddPRL187.Count > 0)
                {
                    db.PRL187.AddRange(lstAddPRL187);
                }
                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL187 = db.PRL187.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL187 = db.PRL187.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL187.Count > 0)
                {
                    db.PRL187.RemoveRange(lstDeletePRL187);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3Linkage(List<PRL188> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL188> lstAddPRL188 = new List<PRL188>();
                List<PRL188> lstDeletePRL188 = new List<PRL188>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL188 obj = db.PRL188.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        else
                        {
                            obj = new PRL188();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }

                        obj.SeamNo = item.SeamNo;
                        obj.ReqOrien = item.ReqOrien;
                        obj.ReqArcLength = item.ReqArcLength;
                        obj.ActualArcLength = item.ActualArcLength;

                        if (isAdded)
                        {
                            lstAddPRL188.Add(obj);
                        }
                    }
                    if (lstAddPRL188.Count > 0)
                    {
                        db.PRL188.AddRange(lstAddPRL188);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL188 = db.PRL188.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL188 = db.PRL188.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL188.Count > 0)
                {
                    db.PRL188.RemoveRange(lstDeletePRL188);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line4 details
        [HttpPost]
        public JsonResult SaveProtocolLine4Linkage(List<PRL189> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL189> lstAddPRL189 = new List<PRL189>();
                List<PRL189> lstDeletePRL189 = new List<PRL189>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL189 obj = db.PRL189.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL189();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SeamNo = item.SeamNo;
                        obj.RootGapMin = item.RootGapMin;
                        obj.RootGapMax = item.RootGapMax;
                        obj.RootFaceMin = item.RootFaceMin;
                        obj.RootFaceMax = item.RootFaceMax;
                        obj.Peak_in_out_Top = item.Peak_in_out_Top;
                        obj.Peak_in_out_Mid = item.Peak_in_out_Mid;
                        obj.Peak_in_out_Bottom = item.Peak_in_out_Bottom;

                        obj.OffSetMin = item.OffSetMin;
                        obj.OffSetMax = item.OffSetMax;
                        obj.Inside_Wep_AngleMin = item.Inside_Wep_AngleMin;
                        obj.Inside_Wep_AngleMax = item.Inside_Wep_AngleMax;
                        obj.Outside_Wep_AngleMin = item.Outside_Wep_AngleMin;
                        obj.Outside_Wep_AngleMax = item.Outside_Wep_AngleMax;

                        if (isAdded)
                        {
                            lstAddPRL189.Add(obj);
                        }
                    }

                    if (lstAddPRL189.Count > 0)
                    {
                        db.PRL189.AddRange(lstAddPRL189);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL189 = db.PRL189.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL189 = db.PRL189.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL189.Count > 0)
                {
                    db.PRL189.RemoveRange(lstDeletePRL189);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        //#region Line5 details
        //[HttpPost]
        //public JsonResult SaveProtocolLine5Linkage(List<PRL189_2> lst, int HeaderId)
        //{
        //    clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
        //    try
        //    {
        //        List<PRL189_2> lstAddPRL189_2 = new List<PRL189_2>();
        //        List<PRL189_2> lstDeletePRL189_2 = new List<PRL189_2>();
        //        if (lst != null)
        //        {
        //            foreach (var item in lst)
        //            {
        //                bool isAdded = false;
        //                PRL189_2 obj = db.PRL189_2.Where(x => x.LineId == item.LineId).FirstOrDefault();
        //                if (obj == null)
        //                {
        //                    obj = new PRL189_2();
        //                    obj.HeaderId = item.HeaderId;
        //                    obj.CreatedBy = objClsLoginInfo.UserName;
        //                    obj.CreatedOn = DateTime.Now;
        //                    isAdded = true;
        //                }
        //                else
        //                {
        //                    obj.EditedBy = objClsLoginInfo.UserName;
        //                    obj.EditedOn = DateTime.Now;
        //                }
        //                obj.SeamNo = item.SeamNo;
        //                obj.OffSetMin = item.OffSetMin;
        //                obj.OffSetMax = item.OffSetMax;
        //                obj.Inside_Wep_AngleMin = item.Inside_Wep_AngleMin;
        //                obj.Inside_Wep_AngleMax = item.Inside_Wep_AngleMax;
        //                obj.Outside_Wep_AngleMin = item.Outside_Wep_AngleMin;
        //                obj.Outside_Wep_AngleMax = item.Outside_Wep_AngleMax;

        //                if (isAdded)
        //                {
        //                    lstAddPRL189_2.Add(obj);
        //                }
        //            }

        //            if (lstAddPRL189_2.Count > 0)
        //            {
        //                db.PRL189_2.AddRange(lstAddPRL189_2);
        //            }

        //            var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
        //            lstDeletePRL189_2 = db.PRL189_2.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
        //        }
        //        else
        //        {
        //            lstDeletePRL189_2 = db.PRL189_2.Where(x => x.HeaderId == HeaderId).ToList();
        //        }
        //        if (lstDeletePRL189_2.Count > 0)
        //        {
        //            db.PRL189_2.RemoveRange(lstDeletePRL189_2);
        //        }
        //        db.SaveChanges();
        //        objResponseMsg.Key = true;
        //    }
        //    catch (Exception ex)
        //    {
        //        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        //        objResponseMsg.Key = false;
        //        objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
        //    }
        //    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        //}
        //#endregion

        #region Line6 details
        [HttpPost]
        public JsonResult SaveProtocolLine6Linkage(List<PRL189_3> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL189_3> lstAddPRL189_3 = new List<PRL189_3>();
                List<PRL189_3> lstDeletePRL189_3 = new List<PRL189_3>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL189_3 obj = db.PRL189_3.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL189_3();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        obj.MaterialIdentificationReport = item.MaterialIdentificationReport;
                        if (isAdded)
                        {
                            lstAddPRL189_3.Add(obj);
                        }
                    }

                    if (lstAddPRL189_3.Count > 0)
                    {
                        db.PRL189_3.AddRange(lstAddPRL189_3);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL189_3 = db.PRL189_3.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL189_3 = db.PRL189_3.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL189_3.Count > 0)
                {
                    db.PRL189_3.RemoveRange(lstDeletePRL189_3);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line7 details
        [HttpPost]
        public JsonResult SaveProtocolLine7Linkage(List<PRL189_4> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL189_4> lstAddPRL189_4 = new List<PRL189_4>();
                List<PRL189_4> lstDeletePRL189_4 = new List<PRL189_4>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL189_4 obj = db.PRL189_4.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL189_4();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        obj.Description = item.Description;
                        obj.Petal_1 = item.Petal_1;
                        obj.Petal_2 = item.Petal_2;
                        obj.Petal_3 = item.Petal_3;
                        obj.Petal_4 = item.Petal_4;
                        obj.Petal_5 = item.Petal_5;
                        obj.Petal_6 = item.Petal_6;
                        obj.Petal_7 = item.Petal_7;
                        obj.Petal_8 = item.Petal_8;
                        obj.Petal_9 = item.Petal_9;
                        obj.Petal_10 = item.Petal_10;

                        if (isAdded)
                        {
                            lstAddPRL189_4.Add(obj);
                        }
                    }

                    if (lstAddPRL189_4.Count > 0)
                    {
                        db.PRL189_4.AddRange(lstAddPRL189_4);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL189_4 = db.PRL189_4.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL189_4 = db.PRL189_4.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL189_4.Count > 0)
                {
                    db.PRL189_4.RemoveRange(lstDeletePRL189_4);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line8 details

        [HttpPost]
        public JsonResult SaveProtocolLine8Linkage(List<PRL189_5> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL189_5> lstAddPRL189_5 = new List<PRL189_5>();
                List<PRL189_5> lstDeletePRL189_5 = new List<PRL189_5>();

                if (lst != null)
                {

                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL189_5 obj = db.PRL189_5.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        PRL189_5 objFecthSeam = new PRL189_5();
                        if (obj == null)
                        {
                            obj = new PRL189_5();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        objFecthSeam = db.PRL189_5.Where(x => x.SeamNo == item.SeamNo && x.HeaderId == item.HeaderId).FirstOrDefault();
                        if (objFecthSeam != null)
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                            //obj.Depth = objFecthSeam.Depth;
                            //obj.Width = objFecthSeam.Width;
                            //obj.Offset = objFecthSeam.Offset;
                            //obj.SpotNoClad = objFecthSeam.SpotNoClad;
                        }
                        else
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                            //obj.Depth = item.Depth;
                            //obj.Width = item.Width;
                            //obj.Offset = item.Offset;
                            //obj.SpotNoClad = item.SpotNoClad;
                        }
                        obj.SpotNo = item.SpotNo;
                        obj.Depth = item.Depth;
                        obj.Width = item.Width;
                        obj.Offset = item.Offset;
                        obj.SpotNoClad = item.SpotNoClad;
                        if (isAdded)
                        {
                            lstAddPRL189_5.Add(obj);
                        }
                    }
                    if (lstAddPRL189_5.Count > 0)
                    {
                        db.PRL189_5.AddRange(lstAddPRL189_5);
                    }
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL189_5 = db.PRL189_5.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL189_5 = db.PRL189_5.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL189_5.Count > 0)
                {
                    db.PRL189_5.RemoveRange(lstDeletePRL189_5);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line9 details

        [HttpPost]
        public JsonResult SaveProtocolLine9Linkage(List<PRL189_6> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL189_6> lstAddPRL189_6 = new List<PRL189_6>();
                List<PRL189_6> lstDeletePRL189_6 = new List<PRL189_6>();
                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL189_6 obj = db.PRL189_6.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        PRL189_6 objFecthSeam = new PRL189_6();
                        if (obj == null)
                        {
                            obj = new PRL189_6();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        objFecthSeam = db.PRL189_6.Where(x => x.SeamNo == item.SeamNo && x.HeaderId == item.HeaderId).FirstOrDefault();
                        if (objFecthSeam != null)
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                        }
                        else
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                        }
                        obj.CleatNo = item.CleatNo;
                        obj.SizeWelded = item.SizeWelded;
                        obj.Location = item.Location;
                        obj.OutsideOrInside = item.OutsideOrInside;
                        obj.DistanceFromSeam = item.DistanceFromSeam;
                        obj.ShellEvelution = item.ShellEvelution;
                        if (isAdded)
                        {
                            lstAddPRL189_6.Add(obj);
                        }
                    }
                    if (lstAddPRL189_6.Count > 0)
                    {
                        db.PRL189_6.AddRange(lstAddPRL189_6);
                    }
                    lstDeletePRL189_6 = db.PRL189_6.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL189_6 = db.PRL189_6.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL189_6.Count > 0)
                {
                    db.PRL189_6.RemoveRange(lstDeletePRL189_6);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion


        #endregion


    }
}