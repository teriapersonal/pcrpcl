﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol064Controller : clsBase
    {
        // GET: PROTOCOL/Protocol064
        string ControllerURL = "/PROTOCOL/Protocol064/";
        string Title = "SET-UP AND DIMENSION REPORT FOR  LONGITUDINAL SEAM OF TUBESHEET AND BAFFLE";

        #region Details View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO315 objPRO315 = new PRO315();
            if (!id.HasValue)
            {
                try
                {
                    objPRO315.ProtocolNo = string.Empty;
                    objPRO315.CreatedBy = objClsLoginInfo.UserName;
                    objPRO315.CreatedOn = DateTime.Now;

                    #region  Orientation | PRO316
                    objPRO315.PRO316.Add(new PRO316
                    {
                        Orientation = "0°-180° ",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO315.PRO316.Add(new PRO316
                    {
                        Orientation = "45°-225°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO315.PRO316.Add(new PRO316
                    {
                        Orientation = "90°-270°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO315.PRO316.Add(new PRO316
                    {
                        Orientation = "135°-315°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });

                    #endregion

                    #region ORIENTATION FLATNESS | PRO318  
                    objPRO315.PRO318.Add(new PRO318
                    {
                        Location = "0°-180°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO315.PRO318.Add(new PRO318
                    {
                        Location = "45°-225°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO315.PRO318.Add(new PRO318
                    {
                        Location = "90°-270°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO315.PRO318.Add(new PRO318
                    {
                        Location = "135°-315°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion

                    db.PRO315.Add(objPRO315);
                    db.SaveChanges();
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO315.HeaderId;
            }
            else
            {
                objPRO315 = db.PRO315.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstyesnona = clsImplementationEnum.getYesNoNA().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNoNAEnum = lstyesnona.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO316> lstPRO316 = db.PRO316.Where(x => x.HeaderId == objPRO315.HeaderId).ToList();
            List<PRO317> lstPRO317 = db.PRO317.Where(x => x.HeaderId == objPRO315.HeaderId).ToList();
            List<PRO318> lstPRO318 = db.PRO318.Where(x => x.HeaderId == objPRO315.HeaderId).ToList();
            List<PRO319> lstPRO319 = db.PRO319.Where(x => x.HeaderId == objPRO315.HeaderId).ToList();

            ViewBag.lstPRO316 = lstPRO316;
            ViewBag.lstPRO317 = lstPRO317;
            ViewBag.lstPRO318 = lstPRO318;
            ViewBag.lstPRO319 = lstPRO319;

            #endregion

            return View(objPRO315);
        }

        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO315 PRO315)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO315.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO315 objPRO315 = db.PRO315.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO315.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO315.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data

                        objPRO315.ProtocolNo = PRO315.ProtocolNo;
                        objPRO315.ReqOutsideDiameter = PRO315.ReqOutsideDiameter;
                        objPRO315.ReqInsideDiameter = PRO315.ReqInsideDiameter;
                        objPRO315.ReqRootGap = PRO315.ReqRootGap;
                        objPRO315.ReqRootFace = PRO315.ReqRootFace;
                        objPRO315.ReqTopSideWepAngle = PRO315.ReqTopSideWepAngle;
                        objPRO315.ReqBottomSideWepAngle = PRO315.ReqBottomSideWepAngle;
                        objPRO315.ReqOffset = PRO315.ReqOffset;
                        objPRO315.FlatnessReadings = PRO315.FlatnessReadings;
                        objPRO315.OutByLocation = PRO315.OutByLocation;
                        objPRO315.OutByLocation1 = PRO315.OutByLocation1;
                        objPRO315.OutByLocation2 = PRO315.OutByLocation2;
                        objPRO315.OutByLocation3 = PRO315.OutByLocation3;
                        objPRO315.OutByLocation4 = PRO315.OutByLocation4;
                        objPRO315.OutByLocation5 = PRO315.OutByLocation5;
                        objPRO315.OutByLocation6 = PRO315.OutByLocation6;
                        objPRO315.OutByLocation7 = PRO315.OutByLocation7;
                        objPRO315.OutByLocation8 = PRO315.OutByLocation8;
                        objPRO315.OutByLocation9 = PRO315.OutByLocation9;
                        objPRO315.OutByLocation10 = PRO315.OutByLocation10;
                        objPRO315.OutByLocation11 = PRO315.OutByLocation11;
                        objPRO315.OutByLocation12 = PRO315.OutByLocation12;
                        objPRO315.CheckPoint1 = PRO315.CheckPoint1;
                        objPRO315.CheckPoint2 = PRO315.CheckPoint2;
                        objPRO315.CheckPoint3 = PRO315.CheckPoint3;
                        objPRO315.CheckPoint4 = PRO315.CheckPoint4;
                        objPRO315.CheckPoint5 = PRO315.CheckPoint5;
                        objPRO315.CheckPoint6 = PRO315.CheckPoint6;
                        objPRO315.CheckPoint7 = PRO315.CheckPoint7;
                        objPRO315.CheckPoint8 = PRO315.CheckPoint8;
                        objPRO315.CheckPoint8_2 = PRO315.CheckPoint8_2;
                        objPRO315.CheckPoint8_3 = PRO315.CheckPoint8_3;
                        objPRO315.CheckPoint9 = PRO315.CheckPoint9;
                        objPRO315.CheckPoint11 = PRO315.CheckPoint11;
                        objPRO315.CheckPoint11_2 = PRO315.CheckPoint11_2;
                        objPRO315.QCRemarks = PRO315.QCRemarks;
                        objPRO315.Result = PRO315.Result;

                        objPRO315.EditedBy = objClsLoginInfo.UserName;
                        objPRO315.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO315.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO315 objPRO315 = db.PRO315.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO315 != null && string.IsNullOrWhiteSpace(objPRO315.ProtocolNo))
                    {
                        db.PRO315.Remove(objPRO315);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO315.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO316> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO316> lstAddPRO316 = new List<PRO316>();
                List<PRO316> lstDeletePRO316 = new List<PRO316>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO316 obj = db.PRO316.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO316();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Orientation = item.Orientation;
                        obj.ActOutsideDiameter = item.ActOutsideDiameter;
                        obj.ActInsideDiameter = item.ActInsideDiameter;

                        if (isAdded)
                        {
                            lstAddPRO316.Add(obj);
                        }
                    }
                    if (lstAddPRO316.Count > 0)
                    {
                        db.PRO316.AddRange(lstAddPRO316);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO316 = db.PRO316.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO316.Count > 0)
                    {
                        db.PRO316.RemoveRange(lstDeletePRO316);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO316 = db.PRO316.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO316.Count > 0)
                    {
                        db.PRO316.RemoveRange(lstDeletePRO316);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2(List<PRO317> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO317> lstAddPRO317 = new List<PRO317>();
                List<PRO317> lstDeletePRO317 = new List<PRO317>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO317 obj = db.PRO317.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {

                                obj.SeamNo = item.SeamNo;
                                obj.ActMaxRootGap = item.ActMaxRootGap;
                                obj.ActMaxRootFace = item.ActMaxRootFace;
                                obj.ActMaxTopSideWepAngle = item.ActMaxTopSideWepAngle;
                                obj.ActMaxBottomSideWepAngle = item.ActMaxBottomSideWepAngle;
                                obj.ActMaxOffset = item.ActMaxOffset;
                                obj.ActMinRootGap = item.ActMinRootGap;
                                obj.ActMinRootFace = item.ActMinRootFace;
                                obj.ActMinTopSideWepAngle = item.ActMinTopSideWepAngle;
                                obj.ActMinBottomSideWepAngle = item.ActMinBottomSideWepAngle;
                                obj.ActMinOffset = item.ActMinOffset;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO317 obj = new PRO317();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.SeamNo))
                            {
                                obj.SeamNo = item.SeamNo;
                                obj.ActMaxRootGap = item.ActMaxRootGap;
                                obj.ActMaxRootFace = item.ActMaxRootFace;
                                obj.ActMaxTopSideWepAngle = item.ActMaxTopSideWepAngle;
                                obj.ActMaxBottomSideWepAngle = item.ActMaxBottomSideWepAngle;
                                obj.ActMaxOffset = item.ActMaxOffset;
                                obj.ActMinRootGap = item.ActMinRootGap;
                                obj.ActMinRootFace = item.ActMinRootFace;
                                obj.ActMinTopSideWepAngle = item.ActMinTopSideWepAngle;
                                obj.ActMinBottomSideWepAngle = item.ActMinBottomSideWepAngle;
                                obj.ActMinOffset = item.ActMinOffset;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO317.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO317.Count > 0)
                    {
                        db.PRO317.AddRange(lstAddPRO317);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO317 = db.PRO317.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO317.Count > 0)
                    {
                        db.PRO317.RemoveRange(lstDeletePRO317);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO317 = db.PRO317.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO317.Count > 0)
                    {
                        db.PRO317.RemoveRange(lstDeletePRO317);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3(List<PRO318> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO318> lstAddPRO318 = new List<PRO318>();
                List<PRO318> lstDeletePRO318 = new List<PRO318>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (!string.IsNullOrWhiteSpace(item.Location))
                        {
                            bool isAdded = false;
                            PRO318 obj = db.PRO318.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj == null)
                            {
                                obj = new PRO318();
                                obj.HeaderId = item.HeaderId;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                isAdded = true;
                            }
                            else
                            {
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }

                            obj.Location = item.Location;
                            obj.ActualValue1 = item.ActualValue1;
                            obj.ActualValue2 = item.ActualValue2;
                            obj.ActualValue3 = item.ActualValue3;
                            obj.ActualValue4 = item.ActualValue4;
                            obj.ActualValue5 = item.ActualValue5;
                            obj.ActualValue6 = item.ActualValue6;
                            obj.ActualValue7 = item.ActualValue7;
                            obj.ActualValue8 = item.ActualValue8;
                            obj.ActualValue9 = item.ActualValue9;
                            obj.ActualValue10 = item.ActualValue10;
                            obj.ActualValue11 = item.ActualValue11;
                            obj.ActualValue12 = item.ActualValue12;

                            if (isAdded)
                            {
                                lstAddPRO318.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO318.Count > 0)
                    {
                        db.PRO318.AddRange(lstAddPRO318);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO318 = db.PRO318.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO318.Count > 0)
                    {
                        db.PRO318.RemoveRange(lstDeletePRO318);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO318 = db.PRO318.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO318.Count > 0)
                    {
                        db.PRO318.RemoveRange(lstDeletePRO318);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line4 details
        [HttpPost]
        public JsonResult SaveProtocolLine4(List<PRO319> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO319> lstAddPRO319 = new List<PRO319>();
                List<PRO319> lstDeletePRO319 = new List<PRO319>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO319 obj = db.PRO319.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.IdentificationMarking = item.IdentificationMarking;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO319 obj = new PRO319();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.IdentificationMarking))
                            {
                                obj.IdentificationMarking = item.IdentificationMarking;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO319.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO319.Count > 0)
                    {
                        db.PRO319.AddRange(lstAddPRO319);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO319 = db.PRO319.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO319.Count > 0)
                    {
                        db.PRO319.RemoveRange(lstDeletePRO319);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO319 = db.PRO319.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO319.Count > 0)
                    {
                        db.PRO319.RemoveRange(lstDeletePRO319);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

        #region Linkage View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL315 objPRL315 = new PRL315();
            objPRL315 = db.PRL315.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL315 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL315.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;

                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstyesnona = clsImplementationEnum.getYesNoNA().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNoNAEnum = lstyesnona.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL316> lstPRL316 = db.PRL316.Where(x => x.HeaderId == objPRL315.HeaderId).ToList();
            List<PRL317> lstPRL317 = db.PRL317.Where(x => x.HeaderId == objPRL315.HeaderId).ToList();
            List<PRL318> lstPRL318 = db.PRL318.Where(x => x.HeaderId == objPRL315.HeaderId).ToList();
            List<PRL319> lstPRL319 = db.PRL319.Where(x => x.HeaderId == objPRL315.HeaderId).ToList();

            ViewBag.lstPRL316 = lstPRL316;
            ViewBag.lstPRL317 = lstPRL317;
            ViewBag.lstPRL318 = lstPRL318;
            ViewBag.lstPRL319 = lstPRL319;

            #endregion
            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.DrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL315.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL315.ActFilledBy) && (objPRL315.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL315.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL315);
        }

        [HttpPost]
        public ActionResult LinkageMobile(int? id, int? m, string APIRequestFrom = "", string UserName = "")
        {
            string ProjectDesc = "";
            string ManufacturingCode = "";
            string AccessRole = "";
            bool isEditable = false;
            var DrawingNo = new List<string>();
            bool isQCRemarkEditable = false;

            PRL315 objPRL315 = new PRL315();
            db.Configuration.ProxyCreationEnabled = false;
            objPRL315 = db.PRL315.Where(i => i.HeaderId == id).FirstOrDefault();
            db.Entry(objPRL315).State = System.Data.Entity.EntityState.Detached;
            NDEModels objNDEModels = new NDEModels();
            if (objPRL315 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL315.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
            }
           
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
            AccessRole = objUserRoleAccessDetails.UserDesignation;
          

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstyesnona = clsImplementationEnum.getYesNoNA().ToList();

            var YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var YesNoNAEnum = lstyesnona.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL316> lstPRL316 = db.PRL316.Where(x => x.HeaderId == objPRL315.HeaderId).ToList();
            List<PRL317> lstPRL317 = db.PRL317.Where(x => x.HeaderId == objPRL315.HeaderId).ToList();
            List<PRL318> lstPRL318 = db.PRL318.Where(x => x.HeaderId == objPRL315.HeaderId).ToList();
            List<PRL319> lstPRL319 = db.PRL319.Where(x => x.HeaderId == objPRL315.HeaderId).ToList();


            #endregion
            try
            {
                isEditable = false;
                isQCRemarkEditable = false;
                if (m == 1)
                {
                    isEditable = true;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL315.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL315.ActFilledBy) && (objPRL315.RequestNo.HasValue))
                        {
                            isEditable = false;
                            isQCRemarkEditable = true;
                        }
                        else
                        {
                            DrawingNo = (new GeneralController()).GetPLMDrawingNumber(objPRL315.Project).ToList();
                        }
                    }
                }
                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    ProjectDesc = ProjectDesc,
                    ManufacturingCode = ManufacturingCode,
                    isEditable = isEditable,
                    isQCRemarkEditable = isQCRemarkEditable,
                    DrawingNo = DrawingNo,
                    AccessRole = AccessRole,
                    YesNoEnum = YesNoEnum,
                    YesNoNAEnum = YesNoNAEnum,

                    objPRL315 = objPRL315,

                    lstPRL316 = lstPRL316,
                    lstPRL317 = lstPRL317,
                    lstPRL318 = lstPRL318,
                    lstPRL319 = lstPRL319,

                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                return Json(new
                {
                    res = "0",
                    msg = ex.Message,
                }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL315 PRL315, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = PRL315.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL315 objPRL315 = db.PRL315.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL315.DrawingRevisionNo = PRL315.DrawingRevisionNo;
                    objPRL315.DrawingNo = PRL315.DrawingNo;
                    objPRL315.DCRNo = PRL315.DCRNo;
                    objPRL315.ProtocolNo = PRL315.ProtocolNo != null ? PRL315.ProtocolNo : "";
                    objPRL315.ReqOutsideDiameter = PRL315.ReqOutsideDiameter;
                    objPRL315.ReqInsideDiameter = PRL315.ReqInsideDiameter;
                    objPRL315.ReqRootGap = PRL315.ReqRootGap;
                    objPRL315.ReqRootFace = PRL315.ReqRootFace;
                    objPRL315.ReqTopSideWepAngle = PRL315.ReqTopSideWepAngle;
                    objPRL315.ReqBottomSideWepAngle = PRL315.ReqBottomSideWepAngle;
                    objPRL315.ReqOffset = PRL315.ReqOffset;
                    objPRL315.FlatnessReadings = PRL315.FlatnessReadings;
                    objPRL315.OutByLocation = PRL315.OutByLocation;
                    objPRL315.OutByLocation1 = PRL315.OutByLocation1;
                    objPRL315.OutByLocation2 = PRL315.OutByLocation2;
                    objPRL315.OutByLocation3 = PRL315.OutByLocation3;
                    objPRL315.OutByLocation4 = PRL315.OutByLocation4;
                    objPRL315.OutByLocation5 = PRL315.OutByLocation5;
                    objPRL315.OutByLocation6 = PRL315.OutByLocation6;
                    objPRL315.OutByLocation7 = PRL315.OutByLocation7;
                    objPRL315.OutByLocation8 = PRL315.OutByLocation8;
                    objPRL315.OutByLocation9 = PRL315.OutByLocation9;
                    objPRL315.OutByLocation10 = PRL315.OutByLocation10;
                    objPRL315.OutByLocation11 = PRL315.OutByLocation11;
                    objPRL315.OutByLocation12 = PRL315.OutByLocation12;
                    objPRL315.CheckPoint1 = PRL315.CheckPoint1;
                    objPRL315.CheckPoint2 = PRL315.CheckPoint2;
                    objPRL315.CheckPoint3 = PRL315.CheckPoint3;
                    objPRL315.CheckPoint4 = PRL315.CheckPoint4;
                    objPRL315.CheckPoint5 = PRL315.CheckPoint5;
                    objPRL315.CheckPoint6 = PRL315.CheckPoint6;
                    objPRL315.CheckPoint7 = PRL315.CheckPoint7;
                    objPRL315.CheckPoint8 = PRL315.CheckPoint8;
                    objPRL315.CheckPoint8_2 = PRL315.CheckPoint8_2;
                    objPRL315.CheckPoint8_3 = PRL315.CheckPoint8_3;
                    objPRL315.CheckPoint9 = PRL315.CheckPoint9;
                    objPRL315.CheckPoint11 = PRL315.CheckPoint11;
                    objPRL315.CheckPoint11_2 = PRL315.CheckPoint11_2;
                    objPRL315.QCRemarks = PRL315.QCRemarks;
                    objPRL315.Result = PRL315.Result;

                    objPRL315.EditedBy = UserName;
                    objPRL315.EditedOn = DateTime.Now;

                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL315.ActFilledBy = UserName;
                            objPRL315.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL315.ReqFilledBy = UserName;
                            objPRL315.ReqFilledOn = DateTime.Now;
                        }
                    }
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL315.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL315 objPRL315 = db.PRL315.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL315 != null && string.IsNullOrWhiteSpace(objPRL315.ProtocolNo))
                    {
                        db.PRL315.Remove(objPRL315);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL315.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL316> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL316> lstAddPRL316 = new List<PRL316>();
                List<PRL316> lstDeletePRL316 = new List<PRL316>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL316 obj = db.PRL316.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL316();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Orientation = item.Orientation;
                        obj.ActOutsideDiameter = item.ActOutsideDiameter;
                        obj.ActInsideDiameter = item.ActInsideDiameter;

                        if (isAdded)
                        {
                            lstAddPRL316.Add(obj);
                        }
                    }
                    if (lstAddPRL316.Count > 0)
                    {
                        db.PRL316.AddRange(lstAddPRL316);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL316 = db.PRL316.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL316.Count > 0)
                    {
                        db.PRL316.RemoveRange(lstDeletePRL316);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL316 = db.PRL316.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL316.Count > 0)
                    {
                        db.PRL316.RemoveRange(lstDeletePRL316);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine2Linkage(List<PRL317> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL317> lstAddPRL317 = new List<PRL317>();
                List<PRL317> lstDeletePRL317 = new List<PRL317>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRL317 obj = db.PRL317.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {

                                obj.SeamNo = item.SeamNo;
                                obj.ActMaxRootGap = item.ActMaxRootGap;
                                obj.ActMaxRootFace = item.ActMaxRootFace;
                                obj.ActMaxTopSideWepAngle = item.ActMaxTopSideWepAngle;
                                obj.ActMaxBottomSideWepAngle = item.ActMaxBottomSideWepAngle;
                                obj.ActMaxOffset = item.ActMaxOffset;
                                obj.ActMinRootGap = item.ActMinRootGap;
                                obj.ActMinRootFace = item.ActMinRootFace;
                                obj.ActMinTopSideWepAngle = item.ActMinTopSideWepAngle;
                                obj.ActMinBottomSideWepAngle = item.ActMinBottomSideWepAngle;
                                obj.ActMinOffset = item.ActMinOffset;
                                obj.EditedBy = UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRL317 obj = new PRL317();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.SeamNo))
                            {
                                obj.SeamNo = item.SeamNo;
                                obj.ActMaxRootGap = item.ActMaxRootGap;
                                obj.ActMaxRootFace = item.ActMaxRootFace;
                                obj.ActMaxTopSideWepAngle = item.ActMaxTopSideWepAngle;
                                obj.ActMaxBottomSideWepAngle = item.ActMaxBottomSideWepAngle;
                                obj.ActMaxOffset = item.ActMaxOffset;
                                obj.ActMinRootGap = item.ActMinRootGap;
                                obj.ActMinRootFace = item.ActMinRootFace;
                                obj.ActMinTopSideWepAngle = item.ActMinTopSideWepAngle;
                                obj.ActMinBottomSideWepAngle = item.ActMinBottomSideWepAngle;
                                obj.ActMinOffset = item.ActMinOffset;
                                obj.EditedBy = UserName;
                                obj.EditedOn = DateTime.Now;
                                obj.CreatedBy = UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRL317.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRL317.Count > 0)
                    {
                        db.PRL317.AddRange(lstAddPRL317);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRL317 = db.PRL317.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRL317.Count > 0)
                    {
                        db.PRL317.RemoveRange(lstDeletePRL317);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL317 = db.PRL317.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL317.Count > 0)
                    {
                        db.PRL317.RemoveRange(lstDeletePRL317);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line3 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine3Linkage(List<PRL318> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL318> lstAddPRL318 = new List<PRL318>();
                List<PRL318> lstDeletePRL318 = new List<PRL318>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (!string.IsNullOrWhiteSpace(item.Location))
                        {
                            bool isAdded = false;
                            PRL318 obj = db.PRL318.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj == null)
                            {
                                obj = new PRL318();
                                obj.HeaderId = item.HeaderId;
                                obj.CreatedBy = UserName;
                                obj.CreatedOn = DateTime.Now;
                                isAdded = true;
                            }
                            else
                            {
                                obj.EditedBy = UserName;
                                obj.EditedOn = DateTime.Now;
                            }

                            obj.Location = item.Location;
                            obj.ActualValue1 = item.ActualValue1;
                            obj.ActualValue2 = item.ActualValue2;
                            obj.ActualValue3 = item.ActualValue3;
                            obj.ActualValue4 = item.ActualValue4;
                            obj.ActualValue5 = item.ActualValue5;
                            obj.ActualValue6 = item.ActualValue6;
                            obj.ActualValue7 = item.ActualValue7;
                            obj.ActualValue8 = item.ActualValue8;
                            obj.ActualValue9 = item.ActualValue9;
                            obj.ActualValue10 = item.ActualValue10;
                            obj.ActualValue11 = item.ActualValue11;
                            obj.ActualValue12 = item.ActualValue12;

                            if (isAdded)
                            {
                                lstAddPRL318.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRL318.Count > 0)
                    {
                        db.PRL318.AddRange(lstAddPRL318);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL318 = db.PRL318.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL318.Count > 0)
                    {
                        db.PRL318.RemoveRange(lstDeletePRL318);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL318 = db.PRL318.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL318.Count > 0)
                    {
                        db.PRL318.RemoveRange(lstDeletePRL318);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line4 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine4Linkage(List<PRL319> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL319> lstAddPRL319 = new List<PRL319>();
                List<PRL319> lstDeletePRL319 = new List<PRL319>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRL319 obj = db.PRL319.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.IdentificationMarking = item.IdentificationMarking;
                                obj.EditedBy = UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRL319 obj = new PRL319();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.IdentificationMarking))
                            {
                                obj.IdentificationMarking = item.IdentificationMarking;
                                obj.CreatedBy = UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRL319.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRL319.Count > 0)
                    {
                        db.PRL319.AddRange(lstAddPRL319);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL319 = db.PRL319.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL319.Count > 0)
                    {
                        db.PRL319.RemoveRange(lstDeletePRL319);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL319 = db.PRL319.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL319.Count > 0)
                    {
                        db.PRL319.RemoveRange(lstDeletePRL319);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

    }
}