﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol032Controller : clsBase
    {
        string ControllerURL = "/PROTOCOL/Protocol032/";
        string Title = "OVERLAY THICKNESS MEASUREMENT REPORT FOR SHELL/D'END/LONG SEAM/CIRC. SEAM";
        // GET: PROTOCOL/Protocol032
        #region Details View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id,int? m)
        {
            PRO155 objPRO155 = new PRO155();
            if (!id.HasValue)
            {
                objPRO155.ProtocolNo = string.Empty;
                objPRO155.CreatedBy = objClsLoginInfo.UserName;
                objPRO155.CreatedOn = DateTime.Now;
                db.PRO155.Add(objPRO155);
                db.SaveChanges();
                ViewBag.HeaderId = objPRO155.HeaderId;
            }
            else
            {
                objPRO155 = db.PRO155.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            return View(objPRO155);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO155 PRO155)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO155.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO155 objPRO155 = db.PRO155.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO155.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO155.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data
                        objPRO155.ProtocolNo = PRO155.ProtocolNo;
                        objPRO155.Remark2 = PRO155.Remark2;
                        objPRO155.EditedBy = objClsLoginInfo.UserName;
                        objPRO155.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol saved successfully";
                        objResponseMsg.HeaderId = objPRO155.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO155 objPRO155 = db.PRO155.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO155 != null && string.IsNullOrWhiteSpace(objPRO155.ProtocolNo))
                    {
                        db.PRO156.RemoveRange(objPRO155.PRO156.ToList());
                        db.PRO155.Remove(objPRO155);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.OVERLAY_THICKNESS_MEASUREMENT_REPORT_FOR_SHELL_DEND_LONG_SEAM_CIRC_SEAM.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO155.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #endregion


        #region Line1 details
        [HttpPost]
        public ActionResult GetProtocolLines1Data(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro.Description like '%" + param.sSearch
                        + "%' or pro.SeamNo like '%" + param.sSearch
                        + "%' or pro.Elevation like '%" + param.sSearch
                        + "%' or pro.Orientation like '%" + param.sSearch
                        + "%' or pro.BeforeOverlayThkReq like '%" + param.sSearch
                        + "%' or pro.BeforeOverlayThkAct like '%" + param.sSearch
                        + "%' or pro.AfterOverlayThkReq like '%" + param.sSearch
                        + "%' or pro.AfterOverlayThkAct like '%" + param.sSearch
                        + "%' or pro.OverlayThk like '%" + param.sSearch
                        + "%' or pro.Remarks like '%" + param.sSearch
                        + "%')";
                }

                var lstResult = db.SP_IPI_PROTOCOL032_GET_LINES1_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] { "1",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   "",
                                   Helper.GenerateHTMLTextbox(newRecordId, "Description", "", "", false, "", false, "100","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "SeamNo", "", "", false, "", false, "50","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Elevation", "", "", false, "", false, "50","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Orientation", "", "", false, "", false, "50","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "BeforeOverlayThkReq", "", "", false, "", false, "50","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "BeforeOverlayThkAct", "", "", false, "", false, "50","PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "AfterOverlayThkReq", "", "", false, "", false, "50","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "AfterOverlayThkAct", "", "", false, "", false, "50","PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "OverlayThk", "", "", false, "", false, "50","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Remarks", "", "", false, "", false, "100","QC"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine1(0);")
                                };

                var data = (from uc in lstResult
                            select new[]
                            {
                               "1",
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                               Convert.ToString(uc.ROW_NO),
                               Helper.GenerateHTMLTextbox(uc.LineId, "Description", Convert.ToString(uc.Description), "", true, "", false, "100","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "SeamNo", Convert.ToString(uc.SeamNo), "", true, "", false, "50","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "Elevation", Convert.ToString(uc.Elevation), "", true, "", false, "50","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "Orientation", Convert.ToString(uc.Orientation), "", true, "", false, "50","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "BeforeOverlayThkReq", Convert.ToString(uc.BeforeOverlayThkReq), "", true, "", false, "50","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "BeforeOverlayThkAct", Convert.ToString(uc.BeforeOverlayThkAct), "", true, "", false, "50","PROD"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "AfterOverlayThkReq", Convert.ToString(uc.AfterOverlayThkReq), "", true, "", false, "50","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "AfterOverlayThkAct", Convert.ToString(uc.AfterOverlayThkAct), "", true, "", false, "50","PROD"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "OverlayThk", Convert.ToString(uc.OverlayThk), "", true, "", false, "50","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "Remarks", Convert.ToString(uc.Remarks), "", true, "", false, "100","QC"),
                               Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == "QC3" ? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine1','tblProtocolLines1')"):"")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine1(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRO156 objPRO156 = new PRO156();
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;

                if (refHeaderId > 0)
                {
                    string strDescription = !string.IsNullOrEmpty(fc["Description" + refLineId]) ? Convert.ToString(fc["Description" + refLineId]).Trim() : "";
                    string strSeamNo = !string.IsNullOrEmpty(fc["SeamNo" + refLineId]) ? Convert.ToString(fc["SeamNo" + refLineId]).Trim() : "";
                    string strElevation = !string.IsNullOrEmpty(fc["Elevation" + refLineId]) ? Convert.ToString(fc["Elevation" + refLineId]).Trim() : "";
                    string strOrientation = !string.IsNullOrEmpty(fc["Orientation" + refLineId]) ? Convert.ToString(fc["Orientation" + refLineId]).Trim() : "";
                    string strBeforeOverlayThkReq = !string.IsNullOrEmpty(fc["BeforeOverlayThkReq" + refLineId]) ? Convert.ToString(fc["BeforeOverlayThkReq" + refLineId]).Trim() : "";
                    string strBeforeOverlayThkAct = !string.IsNullOrEmpty(fc["BeforeOverlayThkAct" + refLineId]) ? Convert.ToString(fc["BeforeOverlayThkAct" + refLineId]).Trim() : "";
                    string strAfterOverlayThkReq = !string.IsNullOrEmpty(fc["AfterOverlayThkReq" + refLineId]) ? Convert.ToString(fc["AfterOverlayThkReq" + refLineId]).Trim() : "";
                    string strAfterOverlayThkAct = !string.IsNullOrEmpty(fc["AfterOverlayThkAct" + refLineId]) ? Convert.ToString(fc["AfterOverlayThkAct" + refLineId]).Trim() : "";
                    string strOverlayThk = !string.IsNullOrEmpty(fc["OverlayThk" + refLineId]) ? Convert.ToString(fc["OverlayThk" + refLineId]).Trim() : "";
                    string strRemarks = !string.IsNullOrEmpty(fc["Remarks" + refLineId]) ? Convert.ToString(fc["Remarks" + refLineId]).Trim() : "";

                    #region Add New Spot Line1
                    if (refLineId > 0)
                    {
                        objPRO156 = db.PRO156.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRO156.EditedBy = objClsLoginInfo.UserName;
                        objPRO156.EditedOn = DateTime.Now;
                    }
                    objPRO156.HeaderId = refHeaderId;
                    objPRO156.Description = strDescription;
                    objPRO156.SeamNo = strSeamNo;
                    objPRO156.Elevation = strElevation;
                    objPRO156.Orientation = strOrientation;
                    objPRO156.BeforeOverlayThkReq = strBeforeOverlayThkReq;
                    objPRO156.BeforeOverlayThkAct = strBeforeOverlayThkAct;
                    objPRO156.AfterOverlayThkReq = strAfterOverlayThkReq;
                    objPRO156.AfterOverlayThkAct = strAfterOverlayThkAct;
                    objPRO156.OverlayThk = strOverlayThk;
                    objPRO156.Remarks = strRemarks;

                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = "Data updated successfully";
                    }
                    else
                    {
                        objPRO156.CreatedBy = objClsLoginInfo.UserName;
                        objPRO156.CreatedOn = DateTime.Now;
                        db.PRO156.Add(objPRO156);
                        db.SaveChanges();
                        objResponseMsg.Value = "Data added successfully";
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRO156.LineId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult DeleteProtocolLine1(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRO156 objPRO156 = db.PRO156.FirstOrDefault(c => c.LineId == lineId);
                if (objPRO156 != null)
                {
                    db.PRO156.Remove(objPRO156);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region Linkage View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL155 objPRL155 = new PRL155();
            objPRL155 = db.PRL155.Where(i => i.HeaderId == id).FirstOrDefault();

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            if (objPRL155 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL155.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objQproject.ManufacturingCode;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }

            if (m == 1)
            {
                ViewBag.isEditable = "true";
            }
            else
            {
                ViewBag.isEditable = "false";
            }
            return View(objPRL155);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL155 PRL155)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRL155.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL155 objPRL155 = db.PRL155.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL155.ProtocolNo = PRL155.ProtocolNo != null ? PRL155.ProtocolNo : "";
                    objPRL155.Remark2 = PRL155.Remark2;
                    objPRL155.DCRNo = PRL155.DCRNo;
                    objPRL155.EquipmentNo = PRL155.EquipmentNo;
                    objPRL155.InspectionAgency = PRL155.InspectionAgency;
                    objPRL155.SeamNo = PRL155.SeamNo;
                    objPRL155.DrawingNo = PRL155.DrawingNo;
                    objPRL155.DrawingRevisionNo = PRL155.DrawingRevisionNo;
                    objPRL155.ManufacturingCode = PRL155.ManufacturingCode;
                    objPRL155.EditedBy = objClsLoginInfo.UserName;
                    objPRL155.EditedOn = DateTime.Now;

                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol saved successfully";
                    objResponseMsg.HeaderId = objPRL155.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL155 objPRL155 = db.PRL155.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL155 != null && string.IsNullOrWhiteSpace(objPRL155.ProtocolNo))
                    {
                        db.PRL156.RemoveRange(objPRL155.PRL156.ToList());
                        db.PRL155.Remove(objPRL155);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL155.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region Line1 details
        [HttpPost]
        public ActionResult GetProtocolLines1DataLinkage(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro.Description like '%" + param.sSearch
                        + "%' or pro.SeamNo like '%" + param.sSearch
                        + "%' or pro.Elevation like '%" + param.sSearch
                        + "%' or pro.Orientation like '%" + param.sSearch
                        + "%' or pro.BeforeOverlayThkReq like '%" + param.sSearch
                        + "%' or pro.BeforeOverlayThkAct like '%" + param.sSearch
                        + "%' or pro.AfterOverlayThkReq like '%" + param.sSearch
                        + "%' or pro.AfterOverlayThkAct like '%" + param.sSearch
                        + "%' or pro.OverlayThk like '%" + param.sSearch
                        + "%' or pro.Remarks like '%" + param.sSearch
                        + "%')";
                }

                var lstResult = db.SP_IPI_PROTOCOL032_LINKAGE_GET_LINES1_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] { "1",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   "",
                                   Helper.GenerateHTMLTextbox(newRecordId, "Description", "", "", false, "", false, "100","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "SeamNo", "", "", false, "", false, "50","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Elevation", "", "", false, "", false, "50","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Orientation", "", "", false, "", false, "50","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "BeforeOverlayThkReq", "", "", false, "", false, "50","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "BeforeOverlayThkAct", "", "", false, "", false, "50","PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "AfterOverlayThkReq", "", "", false, "", false, "50","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "AfterOverlayThkAct", "", "", false, "", false, "50","PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "OverlayThk", "", "", false, "", false, "50","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Remarks", "", "", false, "", false, "100","QC"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine1(0);")
                                };


                var data = (from uc in lstResult
                            select new[]
                            {
                                "1",
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                               Convert.ToString(uc.ROW_NO),
                               Helper.GenerateHTMLTextbox(uc.LineId, "Description", Convert.ToString(uc.Description), "", true, "", false, "100","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "SeamNo", Convert.ToString(uc.SeamNo), "", true, "", false, "50","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "Elevation", Convert.ToString(uc.Elevation), "", true, "", false, "50","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "Orientation", Convert.ToString(uc.Orientation), "", true, "", false, "50","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "BeforeOverlayThkReq", Convert.ToString(uc.BeforeOverlayThkReq), "", true, "", false, "50","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "BeforeOverlayThkAct", Convert.ToString(uc.BeforeOverlayThkAct), "", true, "", false, "50","PROD"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "AfterOverlayThkReq", Convert.ToString(uc.AfterOverlayThkReq), "", true, "", false, "50","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "AfterOverlayThkAct", Convert.ToString(uc.AfterOverlayThkAct), "", true, "", false, "50","PROD"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "OverlayThk", Convert.ToString(uc.OverlayThk), "", true, "", false, "50","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "Remarks", Convert.ToString(uc.Remarks), "", true, "", false, "100","QC"),
                               Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == "QC3" ? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine1Linkage','tblProtocolLines1')"):"")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine1Linkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRL156 objPRL156 = new PRL156();

            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;

                if (refHeaderId > 0)
                {
                    string strDescription = !string.IsNullOrEmpty(fc["Description" + refLineId]) ? Convert.ToString(fc["Description" + refLineId]).Trim() : "";
                    string strSeamNo = !string.IsNullOrEmpty(fc["SeamNo" + refLineId]) ? Convert.ToString(fc["SeamNo" + refLineId]).Trim() : "";
                    string strElevation = !string.IsNullOrEmpty(fc["Elevation" + refLineId]) ? Convert.ToString(fc["Elevation" + refLineId]).Trim() : "";
                    string strOrientation = !string.IsNullOrEmpty(fc["Orientation" + refLineId]) ? Convert.ToString(fc["Orientation" + refLineId]).Trim() : "";
                    string strBeforeOverlayThkReq = !string.IsNullOrEmpty(fc["BeforeOverlayThkReq" + refLineId]) ? Convert.ToString(fc["BeforeOverlayThkReq" + refLineId]).Trim() : "";
                    string strBeforeOverlayThkAct = !string.IsNullOrEmpty(fc["BeforeOverlayThkAct" + refLineId]) ? Convert.ToString(fc["BeforeOverlayThkAct" + refLineId]).Trim() : "";
                    string strAfterOverlayThkReq = !string.IsNullOrEmpty(fc["AfterOverlayThkReq" + refLineId]) ? Convert.ToString(fc["AfterOverlayThkReq" + refLineId]).Trim() : "";
                    string strAfterOverlayThkAct = !string.IsNullOrEmpty(fc["AfterOverlayThkAct" + refLineId]) ? Convert.ToString(fc["AfterOverlayThkAct" + refLineId]).Trim() : "";
                    string strOverlayThk = !string.IsNullOrEmpty(fc["OverlayThk" + refLineId]) ? Convert.ToString(fc["OverlayThk" + refLineId]).Trim() : "";
                    string strRemarks = !string.IsNullOrEmpty(fc["Remarks" + refLineId]) ? Convert.ToString(fc["Remarks" + refLineId]).Trim() : "";

                    #region Add New Spot Line1
                    if (refLineId > 0)
                    {
                        objPRL156 = db.PRL156.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRL156.EditedBy = objClsLoginInfo.UserName;
                        objPRL156.EditedOn = DateTime.Now;
                    }
                    objPRL156.HeaderId = refHeaderId;
                    objPRL156.Description = strDescription;
                    objPRL156.SeamNo = strSeamNo;
                    objPRL156.Elevation = strElevation;
                    objPRL156.Orientation = strOrientation;
                    objPRL156.BeforeOverlayThkReq = strBeforeOverlayThkReq;
                    objPRL156.BeforeOverlayThkAct = strBeforeOverlayThkAct;
                    objPRL156.AfterOverlayThkReq = strAfterOverlayThkReq;
                    objPRL156.AfterOverlayThkAct = strAfterOverlayThkAct;
                    objPRL156.OverlayThk = strOverlayThk;
                    objPRL156.Remarks = strRemarks;

                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = "Data updated successfully";
                    }
                    else
                    {
                        objPRL156.CreatedBy = objClsLoginInfo.UserName;
                        objPRL156.CreatedOn = DateTime.Now;
                        db.PRL156.Add(objPRL156);
                        db.SaveChanges();
                        objResponseMsg.Value = "Data added successfully";
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRL156.LineId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine1Linkage(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRL156 objPRL156 = db.PRL156.FirstOrDefault(c => c.LineId == lineId);
                if (objPRL156 != null)
                {
                    db.PRL156.Remove(objPRL156);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #endregion

        public string HTMLActionString(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", string onHrefURL = "", bool isDisable = false)
        {
            string htmlControl = "";
            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? onClickMethod : "";
            string onHref = !string.IsNullOrEmpty(onHrefURL) ? "href='" + onHrefURL + "'" : "";

            if (isDisable)
            {
                htmlControl += "<i id=\"" + inputID.ToString() + "\" name=\"" + inputID + "\" ></i>";
                //htmlControl = "<i id=\"" + inputID + "' name='" + inputID + "' style='cursor: pointer; opacity:0.3; margin-right:10px;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";
            }
            else
            {
                htmlControl += "<i id=\"" + inputID.ToString() + "\" name=\"" + inputID + "\"  style='cursor: pointer; margin-right:10px;' Title=\"" + buttonTooltip + "\" class=\"" + className + "\"  onclick=\"" + onClickEvent + "\" ></i>";
                //htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer; margin-right:10px;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
            }

            if (!string.IsNullOrEmpty(onHref))
            {
                htmlControl = "<a " + onHref + " >" + htmlControl + "</a>";
            }
            return htmlControl;
        }
    }
}