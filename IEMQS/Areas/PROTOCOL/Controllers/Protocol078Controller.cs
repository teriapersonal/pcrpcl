﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol078Controller : clsBase
    {
        // GET: PROTOCOL/Protocol078
        string ControllerURL = "/PROTOCOL/Protocol078/";
        string Title = "REPORT FOR VISUAL AND DIMENSION INSPECTION FOR SQUARE TYPE NUB AFTER OVERLAY";
        #region Details View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO385 objPRO385 = new PRO385();

            if (!id.HasValue)
            {
                try
                {
                    objPRO385.ProtocolNo = string.Empty;
                    objPRO385.CreatedBy = objClsLoginInfo.UserName;
                    objPRO385.CreatedOn = DateTime.Now;

                    #region  Orientation | PRO386
                    //objPRO385.PRO386.Add(new PRO386
                    //{
                    //    Orientation = "0º",
                    //    CreatedBy = objClsLoginInfo.UserName,
                    //    CreatedOn = DateTime.Now
                    //});
                    //objPRO385.PRO386.Add(new PRO386
                    //{
                    //    Orientation = "45º",
                    //    CreatedBy = objClsLoginInfo.UserName,
                    //    CreatedOn = DateTime.Now
                    //});
                    //objPRO385.PRO386.Add(new PRO386
                    //{
                    //    Orientation = "90º",
                    //    CreatedBy = objClsLoginInfo.UserName,
                    //    CreatedOn = DateTime.Now
                    //});
                    //objPRO385.PRO386.Add(new PRO386
                    //{
                    //    Orientation = "135º",
                    //    CreatedBy = objClsLoginInfo.UserName,
                    //    CreatedOn = DateTime.Now
                    //});
                    //objPRO385.PRO386.Add(new PRO386
                    //{
                    //    Orientation = "180º",
                    //    CreatedBy = objClsLoginInfo.UserName,
                    //    CreatedOn = DateTime.Now
                    //});
                    //objPRO385.PRO386.Add(new PRO386
                    //{
                    //    Orientation = "225º",
                    //    CreatedBy = objClsLoginInfo.UserName,
                    //    CreatedOn = DateTime.Now
                    //});
                    //objPRO385.PRO386.Add(new PRO386
                    //{
                    //    Orientation = "270º",
                    //    CreatedBy = objClsLoginInfo.UserName,
                    //    CreatedOn = DateTime.Now
                    //});
                    //objPRO385.PRO386.Add(new PRO386
                    //{
                    //    Orientation = "315º",
                    //    CreatedBy = objClsLoginInfo.UserName,
                    //    CreatedOn = DateTime.Now

                    //});


                    #endregion

                    #region  OrientationReqNubID | PRO387
                    objPRO385.PRO387.Add(new PRO387
                    {

                        OrientationReqNubID = "0°-180°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO385.PRO387.Add(new PRO387
                    {

                        OrientationReqNubID = "22.5°-202.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO385.PRO387.Add(new PRO387
                    {

                        OrientationReqNubID = "45°-225°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO385.PRO387.Add(new PRO387
                    {

                        OrientationReqNubID = "67.5°-247.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO385.PRO387.Add(new PRO387
                    {

                        OrientationReqNubID = "90°-270°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO385.PRO387.Add(new PRO387
                    {

                        OrientationReqNubID = "112.5°-292.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO385.PRO387.Add(new PRO387
                    {

                        OrientationReqNubID = "135°-315°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO385.PRO387.Add(new PRO387
                    {

                        OrientationReqNubID = "157.5°-337.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });


                    #endregion

                    #region  OrientationAllowedShellOutOfRoundness | PRO388
                    objPRO385.PRO388.Add(new PRO388
                    {

                        OrientationAllowedShellOutOfRoundness = "0°-180°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO385.PRO388.Add(new PRO388
                    {

                        OrientationAllowedShellOutOfRoundness = "22.5°-202.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO385.PRO388.Add(new PRO388
                    {

                        OrientationAllowedShellOutOfRoundness = "45°-225°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO385.PRO388.Add(new PRO388
                    {

                        OrientationAllowedShellOutOfRoundness = "67.5°-247.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO385.PRO388.Add(new PRO388
                    {

                        OrientationAllowedShellOutOfRoundness = "90°-270°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO385.PRO388.Add(new PRO388
                    {

                        OrientationAllowedShellOutOfRoundness = "112.5°-292.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO385.PRO388.Add(new PRO388
                    {

                        OrientationAllowedShellOutOfRoundness = "135°-315°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO385.PRO388.Add(new PRO388
                    {

                        OrientationAllowedShellOutOfRoundness = "157.5°-337.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });


                    #endregion

                    #region  OtherDimensions | PRO389
                    objPRO385.PRO389.Add(new PRO389
                    {

                        OtherDimensions = "DEPTH OF BARRIER LAYER FROM OVERLAY ON NUB FACE (TOP SIDE )",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO385.PRO389.Add(new PRO389
                    {

                        OtherDimensions = "DEPTH OF BARRIER LAYER FROM OVERLAY ON NUB FACE  (BOTTOM SIDE )",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO385.PRO389.Add(new PRO389
                    {

                        OtherDimensions = "DEPTH OF BARRIER LAYER FROM OVERLAY ON SHELL I.D. (TOP SIDE )",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO385.PRO389.Add(new PRO389
                    {

                        OtherDimensions = "DEPTH OF BARRIER LAYER FROM OVERLAY ON SHELL I.D. (BOTTOM SIDE )",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO385.PRO389.Add(new PRO389
                    {

                        OtherDimensions = "RADIUS ON BARRIER LAYER (TOP SIDE)",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO385.PRO389.Add(new PRO389
                    {

                        OtherDimensions = "RADIUS ON BARRIER LAYER (BOTTOM SIDE)",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO385.PRO389.Add(new PRO389
                    {

                        OtherDimensions = "SURFACE FINISH ON MACHINED SURFACES",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });


                    #endregion

                    db.PRO385.Add(objPRO385);
                    db.SaveChanges();
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO385.HeaderId;

                List<PRO381> _lstPRO381 = db.PRO381.Where(w => w.HeaderId == objPRO385.HeaderId).ToList();

                for (int i = 0; i < _lstPRO381.Count; i++)
                {
                    PRO386 objPRO386Copy = new PRO386();

                    objPRO386Copy.HeaderId = objPRO385.HeaderId;
                    objPRO386Copy.Orientation = _lstPRO381[i].Orientation;
                    objPRO386Copy.ActTolReqElevationOfNubFrom = "";
                    objPRO386Copy.ActTolReqElevationOfTopSide = "";
                    objPRO386Copy.ActTolReqNubWidth = "";
                    objPRO386Copy.ActTolReqNubHeight = "";
                    objPRO386Copy.A1DimensionsBefore = _lstPRO381[i].A1DimensionsChecked;
                    objPRO386Copy.A2DimensionsBefore = _lstPRO381[i].A2DimensionsChecked;
                    objPRO386Copy.B1DimensionsBefore = _lstPRO381[i].B1DimensionsChecked;
                    objPRO386Copy.B2DimensionsBefore = _lstPRO381[i].B2DimensionsChecked;
                    objPRO386Copy.C1DimensionsBefore = _lstPRO381[i].C1DimensionsChecked;
                    objPRO386Copy.C2DimensionsBefore = _lstPRO381[i].C2DimensionsChecked;
                    objPRO386Copy.AA1DimensionsAfter = "";
                    objPRO386Copy.AA2DimensionsAfter = "";
                    objPRO386Copy.BB1DimensionsAfter = "";
                    objPRO386Copy.BB2DimensionsAfter = "";
                    objPRO386Copy.CC1DimensionsAfter = "";
                    objPRO386Copy.CC2DimensionsAfter = "";
                    objPRO386Copy.A1NubTopSide = "";
                    objPRO386Copy.A2NubTopSide = "";
                    objPRO386Copy.B1NubID = "";
                    objPRO386Copy.B2NubID = "";
                    objPRO386Copy.C1NubBottomSide = "";
                    objPRO386Copy.C2NubBottomSide = "";


                    objPRO386Copy.CreatedBy = objClsLoginInfo.UserName;
                    objPRO386Copy.CreatedOn = DateTime.Now;

                    db.PRO386.Add(objPRO386Copy);
                    db.SaveChanges();
                }
            }
            else
            {
                objPRO385 = db.PRO385.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstElevationOfNubFrom = new List<string> { "BOTTOM SIDE WEP", "BOTTOM REF. LINE", "B.T.L" };
            List<string> lstElevationOfTopSide = new List<string> { "BOTTOM SIDE WEP", "BOTTOM REF. LINE", "B.T.L" };

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.ElevationOfNubFrom = lstElevationOfNubFrom.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.ElevationOfTopSide = lstElevationOfTopSide.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO386> lstPRO386 = db.PRO386.Where(x => x.HeaderId == objPRO385.HeaderId).ToList();
            List<PRO387> lstPRO387 = db.PRO387.Where(x => x.HeaderId == objPRO385.HeaderId).ToList();
            List<PRO388> lstPRO388 = db.PRO388.Where(x => x.HeaderId == objPRO385.HeaderId).ToList();
            List<PRO389> lstPRO389 = db.PRO389.Where(x => x.HeaderId == objPRO385.HeaderId).ToList();

            ViewBag.lstPRO386 = lstPRO386;
            ViewBag.lstPRO387 = lstPRO387;
            ViewBag.lstPRO388 = lstPRO388;
            ViewBag.lstPRO389 = lstPRO389;


            #endregion

            return View(objPRO385);
        }

        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO385 PRO385)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO385.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO385 objPRO385 = db.PRO385.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO385.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO385.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data
                        objPRO385.ProtocolNo = PRO385.ProtocolNo;
                        objPRO385.ElevationOfNubFrom = PRO385.ElevationOfNubFrom;
                        objPRO385.ElevationOfTopSide = PRO385.ElevationOfTopSide;
                        objPRO385.ReqElevationOfNubFrom = PRO385.ReqElevationOfNubFrom;
                        objPRO385.ReqElevationOfTopSide = PRO385.ReqElevationOfTopSide;
                        objPRO385.ReqNubWidth = PRO385.ReqNubWidth;
                        objPRO385.ReqNubHeight = PRO385.ReqNubHeight;
                        objPRO385.TolReqElevationOfNubFrom = PRO385.TolReqElevationOfNubFrom;
                        objPRO385.TolReqElevationOfTopSide = PRO385.TolReqElevationOfTopSide;
                        objPRO385.TolReqNubWidth = PRO385.TolReqNubWidth;
                        objPRO385.TolReqNubHeight = PRO385.TolReqNubHeight;
                        objPRO385.ReqNubTopSideOverlay = PRO385.ReqNubTopSideOverlay;
                        objPRO385.ReqNubIDOverlay = PRO385.ReqNubIDOverlay;
                        objPRO385.ReqNubBottomSideOverlay = PRO385.ReqNubBottomSideOverlay;
                        objPRO385.NubID = PRO385.NubID;
                        objPRO385.ReqNubID = PRO385.ReqNubID;
                        objPRO385.OutByNubID = PRO385.OutByNubID;
                        objPRO385.ShellOutOfRoundness = PRO385.ShellOutOfRoundness;
                        objPRO385.AllowedShellOutOfRoundness = PRO385.AllowedShellOutOfRoundness;
                        objPRO385.OutByShellOutOfRoundness = PRO385.OutByShellOutOfRoundness;
                        objPRO385.RequiredOverlayThickness = PRO385.RequiredOverlayThickness;
                        objPRO385.CheckPoint1 = PRO385.CheckPoint1;
                        objPRO385.CheckPoint2 = PRO385.CheckPoint2;
                        objPRO385.CheckPoint3 = PRO385.CheckPoint3;
                        objPRO385.CheckPoint4 = PRO385.CheckPoint4;
                        objPRO385.CheckPoint5 = PRO385.CheckPoint5;
                        objPRO385.CheckPoint6 = PRO385.CheckPoint6;
                        objPRO385.CheckPoint8 = PRO385.CheckPoint8;
                        objPRO385.CheckPoint8_2 = PRO385.CheckPoint8_2;
                        objPRO385.QCRemarks = PRO385.QCRemarks;
                        objPRO385.Result = PRO385.Result;
                        objPRO385.EditedBy = PRO385.EditedBy;
                        objPRO385.EditedOn = PRO385.EditedOn;
                        objPRO385.EditedBy = objClsLoginInfo.UserName;
                        objPRO385.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO385.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO385 objPRO385 = db.PRO385.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO385 != null && string.IsNullOrWhiteSpace(objPRO385.ProtocolNo))
                    {
                        db.PRO385.Remove(objPRO385);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO385.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO386> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO386> lstAddPRO386 = new List<PRO386>();
                List<PRO386> lstDeletePRO386 = new List<PRO386>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO386 obj = db.PRO386.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO386();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Orientation = item.Orientation;
                        obj.ActTolReqElevationOfNubFrom = item.ActTolReqElevationOfNubFrom;
                        obj.ActTolReqElevationOfTopSide = item.ActTolReqElevationOfTopSide;
                        obj.ActTolReqNubWidth = item.ActTolReqNubWidth;
                        obj.ActTolReqNubHeight = item.ActTolReqNubHeight;
                        obj.A1DimensionsBefore = item.A1DimensionsBefore;
                        obj.A2DimensionsBefore = item.A2DimensionsBefore;
                        obj.B1DimensionsBefore = item.B1DimensionsBefore;
                        obj.B2DimensionsBefore = item.B2DimensionsBefore;
                        obj.C1DimensionsBefore = item.C1DimensionsBefore;
                        obj.C2DimensionsBefore = item.C2DimensionsBefore;
                        obj.AA1DimensionsAfter = item.AA1DimensionsAfter;
                        obj.AA2DimensionsAfter = item.AA2DimensionsAfter;
                        obj.BB1DimensionsAfter = item.BB1DimensionsAfter;
                        obj.BB2DimensionsAfter = item.BB2DimensionsAfter;
                        obj.CC1DimensionsAfter = item.CC1DimensionsAfter;
                        obj.CC2DimensionsAfter = item.CC2DimensionsAfter;
                        obj.A1NubTopSide = item.A1NubTopSide;
                        obj.A2NubTopSide = item.A2NubTopSide;
                        obj.B1NubID = item.B1NubID;
                        obj.B2NubID = item.B2NubID;
                        obj.C1NubBottomSide = item.C1NubBottomSide;
                        obj.C2NubBottomSide = item.C2NubBottomSide;

                        if (isAdded)
                        {
                            lstAddPRO386.Add(obj);
                        }
                    }
                    if (lstAddPRO386.Count > 0)
                    {
                        db.PRO386.AddRange(lstAddPRO386);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO386 = db.PRO386.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO386.Count > 0)
                    {
                        db.PRO386.RemoveRange(lstDeletePRO386);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO386 = db.PRO386.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO386.Count > 0)
                    {
                        db.PRO386.RemoveRange(lstDeletePRO386);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2(List<PRO387> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO387> lstAddPRO387 = new List<PRO387>();
                List<PRO387> lstDeletePRO387 = new List<PRO387>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO387 obj = db.PRO387.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO387();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }


                        if (!string.IsNullOrWhiteSpace(item.OrientationReqNubID))
                        {


                            obj.OrientationReqNubID = item.OrientationReqNubID;
                            obj.ActualNubID = item.ActualNubID;
                            if (isAdded)
                            {
                                lstAddPRO387.Add(obj);
                            }
                        }


                    }
                    if (lstAddPRO387.Count > 0)
                    {
                        db.PRO387.AddRange(lstAddPRO387);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO387 = db.PRO387.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO387.Count > 0)
                    {
                        db.PRO387.RemoveRange(lstDeletePRO387);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO387 = db.PRO387.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO387.Count > 0)
                    {
                        db.PRO387.RemoveRange(lstDeletePRO387);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3(List<PRO388> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO388> lstAddPRO388 = new List<PRO388>();
                List<PRO388> lstDeletePRO388 = new List<PRO388>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO388 obj = db.PRO388.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO388();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        if (!string.IsNullOrWhiteSpace(item.OrientationAllowedShellOutOfRoundness))
                        {
                            obj.OrientationAllowedShellOutOfRoundness = item.OrientationAllowedShellOutOfRoundness;
                            obj.ActualShellOutOfRoundness = item.ActualShellOutOfRoundness;

                            if (isAdded)
                            {
                                lstAddPRO388.Add(obj);
                            }
                        }



                    }
                    if (lstAddPRO388.Count > 0)
                    {
                        db.PRO388.AddRange(lstAddPRO388);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO388 = db.PRO388.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO388.Count > 0)
                    {
                        db.PRO388.RemoveRange(lstDeletePRO388);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO388 = db.PRO388.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO388.Count > 0)
                    {
                        db.PRO388.RemoveRange(lstDeletePRO388);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line4 details
        [HttpPost]
        public JsonResult SaveProtocolLine4(List<PRO389> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO389> lstAddPRO389 = new List<PRO389>();
                List<PRO389> lstDeletePRO389 = new List<PRO389>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO389 obj = db.PRO389.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO389();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.OtherDimensions = item.OtherDimensions;
                        obj.ReqOtherDimensions = item.ReqOtherDimensions;
                        obj.ActOtherDimensions = item.ActOtherDimensions;


                        if (isAdded)
                        {
                            lstAddPRO389.Add(obj);
                        }
                    }
                    if (lstAddPRO389.Count > 0)
                    {
                        db.PRO389.AddRange(lstAddPRO389);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO389 = db.PRO389.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO389.Count > 0)
                    {
                        db.PRO389.RemoveRange(lstDeletePRO389);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO389 = db.PRO389.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO389.Count > 0)
                    {
                        db.PRO389.RemoveRange(lstDeletePRO389);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

        #region  Linkage View Code   

        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL385 objPRL385 = new PRL385();
            objPRL385 = db.PRL385.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL385 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL385.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();
            List<string> lstElevationOfNubFrom = new List<string> { "BOTTOM SIDE WEP", "BOTTOM REF. LINE", "B.T.L" };
            List<string> lstElevationOfTopSide = new List<string> { "BOTTOM SIDE WEP", "BOTTOM REF. LINE", "B.T.L" };

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.ElevationOfNubFrom = lstElevationOfNubFrom.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.ElevationOfTopSide = lstElevationOfTopSide.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL386> lstPRL386 = db.PRL386.Where(x => x.HeaderId == objPRL385.HeaderId).ToList();
            List<PRL387> lstPRL387 = db.PRL387.Where(x => x.HeaderId == objPRL385.HeaderId).ToList();
            List<PRL388> lstPRL388 = db.PRL388.Where(x => x.HeaderId == objPRL385.HeaderId).ToList();
            List<PRL389> lstPRL389 = db.PRL389.Where(x => x.HeaderId == objPRL385.HeaderId).ToList();

            ViewBag.lstPRL386 = lstPRL386;
            ViewBag.lstPRL387 = lstPRL387;
            ViewBag.lstPRL388 = lstPRL388;
            ViewBag.lstPRL389 = lstPRL389;


            #endregion

            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.DrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL385.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL385.ActFilledBy) && (objPRL385.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL385.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL385);
        }

        [HttpPost]
        public ActionResult LinkageMobile(int? id, int? m, string APIRequestFrom = "", string UserName = "")
        {
            string ProjectDesc = "";
            string ManufacturingCode = "";
            string AccessRole = "";
            bool isEditable = false;
            var DrawingNo = new List<string>();
            bool isQCRemarkEditable = false;

            PRL385 objPRL385 = new PRL385();
            db.Configuration.ProxyCreationEnabled = false;
            objPRL385 = db.PRL385.Where(i => i.HeaderId == id).FirstOrDefault();
            db.Entry(objPRL385).State = System.Data.Entity.EntityState.Detached;
            NDEModels objNDEModels = new NDEModels();
            if (objPRL385 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL385.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
            AccessRole = objUserRoleAccessDetails.UserDesignation;
           
            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();
            List<string> lstElevationOfNubFrom = new List<string> { "BOTTOM SIDE WEP", "BOTTOM REF. LINE", "B.T.L" };
            List<string> lstElevationOfTopSide = new List<string> { "BOTTOM SIDE WEP", "BOTTOM REF. LINE", "B.T.L" };

            var YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var ElevationOfNubFrom = lstElevationOfNubFrom.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var ElevationOfTopSide = lstElevationOfTopSide.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL386> lstPRL386 = db.PRL386.Where(x => x.HeaderId == objPRL385.HeaderId).ToList();
            List<PRL387> lstPRL387 = db.PRL387.Where(x => x.HeaderId == objPRL385.HeaderId).ToList();
            List<PRL388> lstPRL388 = db.PRL388.Where(x => x.HeaderId == objPRL385.HeaderId).ToList();
            List<PRL389> lstPRL389 = db.PRL389.Where(x => x.HeaderId == objPRL385.HeaderId).ToList();

            #endregion
            try
            {
                isEditable = false;
                isQCRemarkEditable = false;
                if (m == 1)
                {
                    isEditable = true;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL385.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL385.ActFilledBy) && (objPRL385.RequestNo.HasValue))
                        {
                            isEditable = false;
                            isQCRemarkEditable = true;
                        }
                        else
                        {
                            DrawingNo = (new GeneralController()).GetPLMDrawingNumber(objPRL385.Project).ToList();
                        }
                    }
                }

                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    ProjectDesc = ProjectDesc,
                    ManufacturingCode = ManufacturingCode,
                    isEditable = isEditable,
                    isQCRemarkEditable = isQCRemarkEditable,
                    DrawingNo = DrawingNo,
                    AccessRole = AccessRole,

                    YesNoEnum = YesNoEnum,
                    YesNAEnum = YesNAEnum,
                    ElevationOfNubFrom = ElevationOfNubFrom,
                    ElevationOfTopSide = ElevationOfTopSide,
                    OutsideInside = OutsideInside,
                    LeftRight = LeftRight,

                    objPRL385 = objPRL385,

                    lstPRL386 = lstPRL386,
                    lstPRL387 = lstPRL387,
                    lstPRL388 = lstPRL388,
                    lstPRL389 = lstPRL389,

                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                return Json(new
                {
                    res = "0",
                    msg = ex.Message,
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL385 PRL385, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = PRL385.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL385 objPRL385 = db.PRL385.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL385.DrawingRevisionNo = PRL385.DrawingRevisionNo;
                    objPRL385.DrawingNo = PRL385.DrawingNo;
                    objPRL385.DCRNo = PRL385.DCRNo;
                    objPRL385.ProtocolNo = PRL385.ProtocolNo;
                    objPRL385.ElevationOfNubFrom = PRL385.ElevationOfNubFrom;
                    objPRL385.ElevationOfTopSide = PRL385.ElevationOfTopSide;
                    objPRL385.ReqElevationOfNubFrom = PRL385.ReqElevationOfNubFrom;
                    objPRL385.ReqElevationOfTopSide = PRL385.ReqElevationOfTopSide;
                    objPRL385.ReqNubWidth = PRL385.ReqNubWidth;
                    objPRL385.ReqNubHeight = PRL385.ReqNubHeight;
                    objPRL385.TolReqElevationOfNubFrom = PRL385.TolReqElevationOfNubFrom;
                    objPRL385.TolReqElevationOfTopSide = PRL385.TolReqElevationOfTopSide;
                    objPRL385.TolReqNubWidth = PRL385.TolReqNubWidth;
                    objPRL385.TolReqNubHeight = PRL385.TolReqNubHeight;
                    objPRL385.ReqNubTopSideOverlay = PRL385.ReqNubTopSideOverlay;
                    objPRL385.ReqNubIDOverlay = PRL385.ReqNubIDOverlay;
                    objPRL385.ReqNubBottomSideOverlay = PRL385.ReqNubBottomSideOverlay;
                    objPRL385.NubID = PRL385.NubID;
                    objPRL385.ReqNubID = PRL385.ReqNubID;
                    objPRL385.OutByNubID = PRL385.OutByNubID;
                    objPRL385.ShellOutOfRoundness = PRL385.ShellOutOfRoundness;
                    objPRL385.AllowedShellOutOfRoundness = PRL385.AllowedShellOutOfRoundness;
                    objPRL385.OutByShellOutOfRoundness = PRL385.OutByShellOutOfRoundness;
                    objPRL385.RequiredOverlayThickness = PRL385.RequiredOverlayThickness;
                    objPRL385.CheckPoint1 = PRL385.CheckPoint1;
                    objPRL385.CheckPoint2 = PRL385.CheckPoint2;
                    objPRL385.CheckPoint3 = PRL385.CheckPoint3;
                    objPRL385.CheckPoint4 = PRL385.CheckPoint4;
                    objPRL385.CheckPoint5 = PRL385.CheckPoint5;
                    objPRL385.CheckPoint6 = PRL385.CheckPoint6;
                    objPRL385.CheckPoint8 = PRL385.CheckPoint8;
                    objPRL385.CheckPoint8_2 = PRL385.CheckPoint8_2;
                    objPRL385.QCRemarks = PRL385.QCRemarks;
                    objPRL385.Result = PRL385.Result;
                    objPRL385.EditedBy = UserName;
                    objPRL385.EditedOn = DateTime.Now;

                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL385.ActFilledBy = UserName;
                            objPRL385.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL385.ReqFilledBy = UserName;
                            objPRL385.ReqFilledOn = DateTime.Now;
                        }
                    }

                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL385.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL385 objPRL385 = db.PRL385.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL385 != null && string.IsNullOrWhiteSpace(objPRL385.ProtocolNo))
                    {
                        db.PRL385.Remove(objPRL385);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL385.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL386> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL386> lstAddPRL386 = new List<PRL386>();
                List<PRL386> lstDeletePRL386 = new List<PRL386>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL386 obj = db.PRL386.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL386();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Orientation = item.Orientation;
                        obj.ActTolReqElevationOfNubFrom = item.ActTolReqElevationOfNubFrom;
                        obj.ActTolReqElevationOfTopSide = item.ActTolReqElevationOfTopSide;
                        obj.ActTolReqNubWidth = item.ActTolReqNubWidth;
                        obj.ActTolReqNubHeight = item.ActTolReqNubHeight;
                        obj.A1DimensionsBefore = item.A1DimensionsBefore;
                        obj.A2DimensionsBefore = item.A2DimensionsBefore;
                        obj.B1DimensionsBefore = item.B1DimensionsBefore;
                        obj.B2DimensionsBefore = item.B2DimensionsBefore;
                        obj.C1DimensionsBefore = item.C1DimensionsBefore;
                        obj.C2DimensionsBefore = item.C2DimensionsBefore;
                        obj.AA1DimensionsAfter = item.AA1DimensionsAfter;
                        obj.AA2DimensionsAfter = item.AA2DimensionsAfter;
                        obj.BB1DimensionsAfter = item.BB1DimensionsAfter;
                        obj.BB2DimensionsAfter = item.BB2DimensionsAfter;
                        obj.CC1DimensionsAfter = item.CC1DimensionsAfter;
                        obj.CC2DimensionsAfter = item.CC2DimensionsAfter;
                        obj.A1NubTopSide = item.A1NubTopSide;
                        obj.A2NubTopSide = item.A2NubTopSide;
                        obj.B1NubID = item.B1NubID;
                        obj.B2NubID = item.B2NubID;
                        obj.C1NubBottomSide = item.C1NubBottomSide;
                        obj.C2NubBottomSide = item.C2NubBottomSide;

                        if (isAdded)
                        {
                            lstAddPRL386.Add(obj);
                        }
                    }
                }
                if (lstAddPRL386.Count > 0)
                {
                    db.PRL386.AddRange(lstAddPRL386);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL386 = db.PRL386.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL386 = db.PRL386.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL386.Count > 0)
                {
                    db.PRL386.RemoveRange(lstDeletePRL386);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2Linkage(List<PRL387> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL387> lstAddPRL387 = new List<PRL387>();
                List<PRL387> lstDeletePRL387 = new List<PRL387>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL387 obj = db.PRL387.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL387();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        if (!string.IsNullOrWhiteSpace(item.OrientationReqNubID))
                        {

                            obj.OrientationReqNubID = item.OrientationReqNubID;
                            obj.ActualNubID = item.ActualNubID;

                            if (isAdded)
                            {
                                lstAddPRL387.Add(obj);
                            }
                        }
                    }
                }
                if (lstAddPRL387.Count > 0)
                {
                    db.PRL387.AddRange(lstAddPRL387);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL387 = db.PRL387.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL387 = db.PRL387.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL387.Count > 0)
                {
                    db.PRL387.RemoveRange(lstDeletePRL387);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3Linkage(List<PRL388> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL388> lstAddPRL388 = new List<PRL388>();
                List<PRL388> lstDeletePRL388 = new List<PRL388>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL388 obj = db.PRL388.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL388();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }


                        if (!string.IsNullOrWhiteSpace(item.OrientationAllowedShellOutOfRoundness))
                        {


                            obj.OrientationAllowedShellOutOfRoundness = item.OrientationAllowedShellOutOfRoundness;
                            obj.ActualShellOutOfRoundness = item.ActualShellOutOfRoundness;

                            if (isAdded)
                            {
                                lstAddPRL388.Add(obj);
                            }
                        }
                    }
                }
                if (lstAddPRL388.Count > 0)
                {
                    db.PRL388.AddRange(lstAddPRL388);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL388 = db.PRL388.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL388 = db.PRL388.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL388.Count > 0)
                {
                    db.PRL388.RemoveRange(lstDeletePRL388);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line4 details
        [HttpPost]
        public JsonResult SaveProtocolLine4Linkage(List<PRL389> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL389> lstAddPRL389 = new List<PRL389>();
                List<PRL389> lstDeletePRL389 = new List<PRL389>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL389 obj = db.PRL389.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL389();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.OtherDimensions = item.OtherDimensions;
                        obj.ReqOtherDimensions = item.ReqOtherDimensions;
                        obj.ActOtherDimensions = item.ActOtherDimensions;


                        if (isAdded)
                        {
                            lstAddPRL389.Add(obj);
                        }
                    }
                }
                if (lstAddPRL389.Count > 0)
                {
                    db.PRL389.AddRange(lstAddPRL389);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL389 = db.PRL389.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL389 = db.PRL389.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL389.Count > 0)
                {
                    db.PRL389.RemoveRange(lstDeletePRL389);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion



        [HttpPost]

        public JsonResult CopyData(string strSeamNo, int HeaderId)
        {
            PRL380 _obj380 = db.PRL380.Where(w => w.SeamNo == strSeamNo).FirstOrDefault();
            if (_obj380 != null)
            {
                List<PRL381> _lstPRL381 = db.PRL381.Where(w => w.HeaderId == _obj380.HeaderId).ToList();
                List<clPRL386> _lstPRL386 = new List<clPRL386>();
                if (_lstPRL381.Count() > 0)
                {
                    for (int i = 0; i < _lstPRL381.Count; i++)
                    {
                        PRL386 objPRL386Copy = new PRL386();
                        objPRL386Copy.HeaderId = HeaderId;
                        objPRL386Copy.Orientation = _lstPRL381[i].Orientation;
                        objPRL386Copy.ActTolReqElevationOfNubFrom = "";
                        objPRL386Copy.ActTolReqElevationOfTopSide = "";
                        objPRL386Copy.ActTolReqNubWidth = "";
                        objPRL386Copy.ActTolReqNubHeight = "";
                        objPRL386Copy.A1DimensionsBefore = _lstPRL381[i].A1DimensionsChecked;
                        objPRL386Copy.A2DimensionsBefore = _lstPRL381[i].A2DimensionsChecked;
                        objPRL386Copy.B1DimensionsBefore = _lstPRL381[i].B1DimensionsChecked;
                        objPRL386Copy.B2DimensionsBefore = _lstPRL381[i].B2DimensionsChecked;
                        objPRL386Copy.C1DimensionsBefore = _lstPRL381[i].C1DimensionsChecked;
                        objPRL386Copy.C2DimensionsBefore = _lstPRL381[i].C2DimensionsChecked;
                        objPRL386Copy.AA1DimensionsAfter = "";
                        objPRL386Copy.AA2DimensionsAfter = "";
                        objPRL386Copy.BB1DimensionsAfter = "";
                        objPRL386Copy.BB2DimensionsAfter = "";
                        objPRL386Copy.CC1DimensionsAfter = "";
                        objPRL386Copy.CC2DimensionsAfter = "";
                        objPRL386Copy.A1NubTopSide = "";
                        objPRL386Copy.A2NubTopSide = "";
                        objPRL386Copy.B1NubID = "";
                        objPRL386Copy.B2NubID = "";
                        objPRL386Copy.C1NubBottomSide = "";
                        objPRL386Copy.C2NubBottomSide = "";

                        objPRL386Copy.CreatedBy = objClsLoginInfo.UserName;
                        objPRL386Copy.CreatedOn = DateTime.Now;

                        db.PRL386.Add(objPRL386Copy);
                        clPRL386 _obj = new clPRL386();
                        _obj.HeaderId = HeaderId;
                        _obj.Orientation = _lstPRL381[i].Orientation;
                        _obj.ActTolReqElevationOfNubFrom = "";
                        _obj.ActTolReqElevationOfTopSide = "";
                        _obj.ActTolReqNubWidth = "";
                        _obj.ActTolReqNubHeight = "";
                        _obj.A1DimensionsBefore = _lstPRL381[i].A1DimensionsChecked;
                        _obj.A2DimensionsBefore = _lstPRL381[i].A2DimensionsChecked;
                        _obj.B1DimensionsBefore = _lstPRL381[i].B1DimensionsChecked;
                        _obj.B2DimensionsBefore = _lstPRL381[i].B2DimensionsChecked;
                        _obj.C1DimensionsBefore = _lstPRL381[i].C1DimensionsChecked;
                        _obj.C2DimensionsBefore = _lstPRL381[i].C2DimensionsChecked;
                        _obj.AA1DimensionsAfter = "";
                        _obj.AA2DimensionsAfter = "";
                        _obj.BB1DimensionsAfter = "";
                        _obj.BB2DimensionsAfter = "";
                        _obj.CC1DimensionsAfter = "";
                        _obj.CC2DimensionsAfter = "";
                        _obj.A1NubTopSide = "";
                        _obj.A2NubTopSide = "";
                        _obj.B1NubID = "";
                        _obj.B2NubID = "";
                        _obj.C1NubBottomSide = "";
                        _obj.C2NubBottomSide = "";

                        _lstPRL386.Add(_obj);
                    }
                    db.SaveChanges();
                }

                PRL385 _obj385 = db.PRL385.Where(w => w.HeaderId == HeaderId).FirstOrDefault();
                if (_obj385 != null)
                {
                    _obj385.IsDataCopy = "true";
                    db.SaveChanges();
                }
                return Json(_lstPRL386, JsonRequestBehavior.AllowGet);
            }
            return Json("", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ActCopyData(string strSeamNo, int HeaderId)
        {
            string strReturn = "";
            try
            {
                PRL380 _obj380 = db.PRL380.Where(w => w.SeamNo == strSeamNo).FirstOrDefault();
                if (_obj380 != null)
                {
                    List<PRL381> _lstPRL381 = db.PRL381.Where(w => w.HeaderId == _obj380.HeaderId).ToList();
                    List<PRL386> _lstPRL386 = db.PRL386.Where(w => w.HeaderId == HeaderId).ToList();
                    if (_lstPRL381.Count() > 0)
                    {
                        for (int i = 0; i < _lstPRL381.Count; i++)
                        {
                            if (_lstPRL386.Count >= i)
                            {
                                PRL386 _obj = _lstPRL386[i];
                                _obj.A1DimensionsBefore = _lstPRL381[i].A1DimensionsChecked;
                                _obj.A2DimensionsBefore = _lstPRL381[i].A2DimensionsChecked;
                                _obj.B1DimensionsBefore = _lstPRL381[i].B1DimensionsChecked;
                                _obj.B2DimensionsBefore = _lstPRL381[i].B2DimensionsChecked;
                                _obj.C1DimensionsBefore = _lstPRL381[i].C1DimensionsChecked;
                                _obj.C2DimensionsBefore = _lstPRL381[i].C2DimensionsChecked;
                                if (strReturn == "")
                                    strReturn = _lstPRL381[i].A1DimensionsChecked+","+ _lstPRL381[i].A2DimensionsChecked+","+ _lstPRL381[i].B1DimensionsChecked+","+ _lstPRL381[i].B2DimensionsChecked+","+ _lstPRL381[i].C1DimensionsChecked+","+ _lstPRL381[i].C2DimensionsChecked;
                                else
                                    strReturn += "|" + _lstPRL381[i].A1DimensionsChecked + "," + _lstPRL381[i].A2DimensionsChecked + "," + _lstPRL381[i].B1DimensionsChecked + "," + _lstPRL381[i].B2DimensionsChecked + "," + _lstPRL381[i].C1DimensionsChecked + "," + _lstPRL381[i].C2DimensionsChecked;
                                db.SaveChanges();
                            }
                        }
                        PRL385 _obj385 = db.PRL385.Where(w => w.HeaderId == HeaderId).FirstOrDefault();
                        if (_obj385 != null)
                        {
                            _obj385.IsActDataCopy = "true";
                            db.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                strReturn = "Error";
            }
            return Json(strReturn, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }

    public class clPRL386
    {
        public int LineId { get; set; }
        public Nullable<int> HeaderId { get; set; }
        public string Orientation { get; set; }
        public string ActTolReqElevationOfNubFrom { get; set; }
        public string ActTolReqElevationOfTopSide { get; set; }
        public string ActTolReqNubWidth { get; set; }
        public string ActTolReqNubHeight { get; set; }
        public string A1DimensionsBefore { get; set; }
        public string A2DimensionsBefore { get; set; }
        public string B1DimensionsBefore { get; set; }
        public string B2DimensionsBefore { get; set; }
        public string C1DimensionsBefore { get; set; }
        public string C2DimensionsBefore { get; set; }
        public string AA1DimensionsAfter { get; set; }
        public string AA2DimensionsAfter { get; set; }
        public string BB1DimensionsAfter { get; set; }
        public string BB2DimensionsAfter { get; set; }
        public string CC1DimensionsAfter { get; set; }
        public string CC2DimensionsAfter { get; set; }
        public string A1NubTopSide { get; set; }
        public string A2NubTopSide { get; set; }
        public string B1NubID { get; set; }
        public string B2NubID { get; set; }
        public string C1NubBottomSide { get; set; }
        public string C2NubBottomSide { get; set; }
    }
}