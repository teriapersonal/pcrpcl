﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQS.Areas.NDE.Models;
using IEMQS.Models;
using IEMQSImplementation;
using static IEMQSImplementation.clsImplementationMessage;
using static IEMQSImplementation.clsBase;
using IEMQS.Areas.Utility.Controllers;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol076Controller : clsBase
    {
        // GET: PROTOCOL/Protocol076

        string ControllerURL = "/PROTOCOL/Protocol076/";
        string Title = "VISUAL & DIMENSION REPORT FOR FORMED CONE";
        //IEMQS.DrawingReference.hedProjectDocListWebServiceClient drawref = new IEMQS.DrawingReference.hedProjectDocListWebServiceClient();

        #region Details View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO375 objPRO375 = new PRO375();
            if (!id.HasValue)
            {
                try
                {
                    objPRO375.ProtocolNo = string.Empty;
                    objPRO375.CreatedBy = objClsLoginInfo.UserName;
                    objPRO375.CreatedOn = DateTime.Now;

                    #region TYPE
                    objPRO375.RemarksInsideCirc2 = "NOTE : IN CASE OF CONE CIRCUMFERENCE TO BE CHECKED IN CLOCK WISE AND ANTI CLOCK WISE THAN AVERAGE SHALL BE REPORTED  \n ABBREVIATION  \n C.W: CLOCK WISE,  \n A.C.W: ANTI CLOCK WISE,  \n AVG: AVERAGE";
                    #endregion

                    #region OVALITY
                    objPRO375.PRO376.Add(new PRO376
                    {
                        Orientation = "0°-180° ",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO375.PRO376.Add(new PRO376
                    {
                        Orientation = "22.5°-202.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO375.PRO376.Add(new PRO376
                    {
                        Orientation = "45°-225°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO375.PRO376.Add(new PRO376
                    {
                        Orientation = "67.5°-247.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO375.PRO376.Add(new PRO376
                    {
                        Orientation = "90°-270°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO375.PRO376.Add(new PRO376
                    {
                        Orientation = "112.5°-292.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO375.PRO376.Add(new PRO376
                    {
                        Orientation = "135°-315°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO375.PRO376.Add(new PRO376
                    {
                        Orientation = "157.5°-337.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion


                    #region  OVERALL HEIGHT OF CONE
                    objPRO375.PRO377.Add(new PRO377
                    {
                        Orientation = "0°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO375.PRO377.Add(new PRO377
                    {
                        Orientation = "90°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO375.PRO377.Add(new PRO377
                    {
                        Orientation = "180°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO375.PRO377.Add(new PRO377
                    {
                        Orientation = "270°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion

                    #region  " e " VALUE TEMPLATE READING AS APPLICABLE
                    objPRO375.PRO378.Add(new PRO378
                    {
                        ReqAtTop = "PETAL-1",
                        ReqAtMiddle = "PETAL-1",
                        ReqAtBottom = "PETAL-1",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO375.PRO378.Add(new PRO378
                    {
                        ReqAtTop = "SEAM",
                        ReqAtMiddle = "SEAM",
                        ReqAtBottom = "SEAM",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO375.PRO378.Add(new PRO378
                    {
                        ReqAtTop = "PETAL: 2",
                        ReqAtMiddle = "PETAL: 2",
                        ReqAtBottom = "PETAL: 2",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO375.PRO378.Add(new PRO378
                    {
                        ReqAtTop = "SEAM",
                        ReqAtMiddle = "SEAM",
                        ReqAtBottom = "SEAM",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO375.PRO378.Add(new PRO378
                    {
                        ReqAtTop = "PETAL: 3",
                        ReqAtMiddle = "PETAL: 3",
                        ReqAtBottom = "PETAL: 3",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO375.PRO378.Add(new PRO378
                    {
                        ReqAtTop = "SEAM",
                        ReqAtMiddle = "SEAM",
                        ReqAtBottom = "SEAM",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO375.PRO378.Add(new PRO378
                    {
                        ReqAtTop = "PETAL: 4",
                        ReqAtMiddle = "PETAL: 4",
                        ReqAtBottom = "PETAL: 4",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO375.PRO378.Add(new PRO378
                    {
                        ReqAtTop = "SEAM",
                        ReqAtMiddle = "SEAM",
                        ReqAtBottom = "SEAM",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO375.PRO378.Add(new PRO378
                    {
                        ReqAtTop = "PETAL: 5",
                        ReqAtMiddle = "PETAL: 5",
                        ReqAtBottom = "PETAL: 5",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO375.PRO378.Add(new PRO378
                    {
                        ReqAtTop = "SEAM",
                        ReqAtMiddle = "SEAM",
                        ReqAtBottom = "SEAM",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO375.PRO378.Add(new PRO378
                    {
                        ReqAtTop = "PETAL: 6",
                        ReqAtMiddle = "PETAL: 6",
                        ReqAtBottom = "PETAL: 6",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO375.PRO378.Add(new PRO378
                    {
                        ReqAtTop = "SEAM",
                        ReqAtMiddle = "SEAM",
                        ReqAtBottom = "SEAM",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO375.PRO378.Add(new PRO378
                    {
                        ReqAtTop = "PETAL: 7",
                        ReqAtMiddle = "PETAL: 7",
                        ReqAtBottom = "PETAL: 7",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO375.PRO378.Add(new PRO378
                    {
                        ReqAtTop = "SEAM",
                        ReqAtMiddle = "SEAM",
                        ReqAtBottom = "SEAM",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO375.PRO378.Add(new PRO378
                    {
                        ReqAtTop = "PETAL: 8",
                        ReqAtMiddle = "PETAL: 8",
                        ReqAtBottom = "PETAL: 8",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO375.PRO378.Add(new PRO378
                    {
                        ReqAtTop = "SEAM",
                        ReqAtMiddle = "SEAM",
                        ReqAtBottom = "SEAM",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion

                    #region ACTUAL GAP BY USING KNUCKLE RADIUS TEMPLATE 
                    objPRO375.PRO379_3.Add(new PRO379_3
                    {
                        PetalNo = "1",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO375.PRO379_3.Add(new PRO379_3
                    {
                        PetalNo = "2",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO375.PRO379_3.Add(new PRO379_3
                    {
                        PetalNo = "3",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO375.PRO379_3.Add(new PRO379_3
                    {
                        PetalNo = "4",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO375.PRO379_3.Add(new PRO379_3
                    {
                        PetalNo = "5",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO375.PRO379_3.Add(new PRO379_3
                    {
                        PetalNo = "6",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO375.PRO379_3.Add(new PRO379_3
                    {
                        PetalNo = "7",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO375.PRO379_3.Add(new PRO379_3
                    {
                        PetalNo = "8",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });

                    #endregion

                    db.PRO375.Add(objPRO375);
                    db.SaveChanges();
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO375.HeaderId;
            }
            else
            {
                objPRO375 = db.PRO375.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lsttype = new List<string> { "TORI CONE", "STRAIGHT CONE"};



            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            ViewBag.Type = lsttype.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();


            ViewBag.OutsideOrInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO376> lstPRO376 = db.PRO376.Where(x => x.HeaderId == objPRO375.HeaderId).ToList();
            List<PRO377> lstPRO377 = db.PRO377.Where(x => x.HeaderId == objPRO375.HeaderId).ToList();
            List<PRO378> lstPRO378 = db.PRO378.Where(x => x.HeaderId == objPRO375.HeaderId).ToList();
            List<PRO379> lstPRO379 = db.PRO379.Where(x => x.HeaderId == objPRO375.HeaderId).ToList();
            List<PRO379_2> lstPRO379_2 = db.PRO379_2.Where(x => x.HeaderId == objPRO375.HeaderId).ToList();
            List<PRO379_3> lstPRO379_3 = db.PRO379_3.Where(x => x.HeaderId == objPRO375.HeaderId).ToList();
            //List<PRO379_4> lstPRO379_4 = db.PRO379_4.Where(x => x.HeaderId == objPRO375.HeaderId).ToList();
            //List<PRO379_5> lstPRO379_5 = db.PRO379_5.Where(x => x.HeaderId == objPRO375.HeaderId).ToList();
            //List<PRO379_6> lstPRO379_6 = db.PRO379_6.Where(x => x.HeaderId == objPRO375.HeaderId).ToList();
            //List<PRO379_9> lstPRO379_9 = db.PRO379_9.Where(x => x.HeaderId == objPRO375.HeaderId).ToList();

            List<SP_IPI_PROTOCOL076_GET_LINES7_DATA_Result> lstPRO379_4 = db.SP_IPI_PROTOCOL076_GET_LINES7_DATA(1, int.MaxValue, "", " HeaderId = " + objPRO375.HeaderId).ToList();

            //List<SP_IPI_PROTOCOL041_GET_LINES10_DATA_Result> lstPRO379_7 = db.SP_IPI_PROTOCOL041_GET_LINES10_DATA(1, int.MaxValue, "", " HeaderId = " + objPRO375.HeaderId).ToList();
            //List<SP_IPI_PROTOCOL041_GET_LINES11_DATA_Result> lstPRO379_8 = db.SP_IPI_PROTOCOL041_GET_LINES11_DATA(1, int.MaxValue, "", " HeaderId = " + objPRO375.HeaderId).ToList();


            ViewBag.lstPRO376 = lstPRO376;
            ViewBag.lstPRO377 = lstPRO377;
            ViewBag.lstPRO378 = lstPRO378;
            ViewBag.lstPRO379 = lstPRO379;
            ViewBag.lstPRO379_2 = lstPRO379_2;
            ViewBag.lstPRO379_3 = lstPRO379_3;
            ViewBag.lstPRO379_4 = lstPRO379_4;
            //ViewBag.lstPRO379_5 = lstPRO379_5;
            //ViewBag.lstPRO379_6 = lstPRO379_6;
            //ViewBag.lstPRO379_7 = lstPRO379_7;
            //ViewBag.lstPRO379_8 = lstPRO379_8;
            //ViewBag.lstPRO379_9 = lstPRO379_9;

            #endregion

            return View(objPRO375);
        }
        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }
        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO375 PRO375)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO375.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO375 objPRO375 = db.PRO375.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO375.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO375.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data


                        objPRO375.ProtocolNo = PRO375.ProtocolNo;
                        objPRO375.Type = PRO375.Type;
                        objPRO375.DiscOutsideCirc = PRO375.DiscOutsideCirc;
                        objPRO375.ReqTopOutsideCirc = PRO375.ReqTopOutsideCirc;
                        objPRO375.ActTopOutsideCirc = PRO375.ActTopOutsideCirc;
                        objPRO375.RemarksOutsideCirc = PRO375.RemarksOutsideCirc;
                        objPRO375.ReqBottomOutsideCirc = PRO375.ReqBottomOutsideCirc;
                        objPRO375.ActBottomOutsideCirc = PRO375.ActBottomOutsideCirc;
                        objPRO375.DiscInsideCirc1 = PRO375.DiscInsideCirc1;
                        objPRO375.ReqTopInsideCirc = PRO375.ReqTopInsideCirc;
                        objPRO375.ActTopInsideCirc = PRO375.ActTopInsideCirc;
                        objPRO375.RemarksInsideCirc = PRO375.RemarksInsideCirc;
                        objPRO375.ReqBottomInsideCirc = PRO375.ReqBottomInsideCirc;
                        objPRO375.ActBottomInsideCirc = PRO375.ActBottomInsideCirc;
                        objPRO375.DiscInsideCirc2 = PRO375.DiscInsideCirc2;
                        objPRO375.ReqTopInsideCirc2 = PRO375.ReqTopInsideCirc2;
                        objPRO375.ActCwTopInsideCirc2 = PRO375.ActCwTopInsideCirc2;
                        objPRO375.ActAcwTopInsideCirc2 = PRO375.ActAcwTopInsideCirc2;
                        objPRO375.ActAvgTopInsideCirc2 = PRO375.ActAvgTopInsideCirc2;
                        objPRO375.RemarksInsideCirc2 = PRO375.RemarksInsideCirc2;
                        objPRO375.ReqBottomInsideCirc2 = PRO375.ReqBottomInsideCirc2;
                        objPRO375.ActCwBottomInsideCirc2 = PRO375.ActCwBottomInsideCirc2;
                        objPRO375.ActAcwBottomInsideCirc2 = PRO375.ActAcwBottomInsideCirc2;
                        objPRO375.ActAvgBottomInsideCirc2 = PRO375.ActAvgBottomInsideCirc2;
                        objPRO375.ReqMinThickness = PRO375.ReqMinThickness;
                        objPRO375.ActMinThickness = PRO375.ActMinThickness;
                        objPRO375.RemarkMinThickness = PRO375.RemarkMinThickness;
                        objPRO375.Ovality = PRO375.Ovality;
                        objPRO375.OutByActTop = PRO375.OutByActTop;
                        objPRO375.OutByActBottom = PRO375.OutByActBottom;
                        objPRO375.ReqHieghtOfCone = PRO375.ReqHieghtOfCone;
                        objPRO375.ActCfTop = PRO375.ActCfTop;
                        objPRO375.ActCfBot = PRO375.ActCfBot;
                        objPRO375.FormulaTop1 = PRO375.FormulaTop1;
                        objPRO375.FormulaTop2 = PRO375.FormulaTop2;
                        objPRO375.FormulaTop3 = PRO375.FormulaTop3;
                        objPRO375.FormulaTop4 = PRO375.FormulaTop4;
                        objPRO375.FormulaBot1 = PRO375.FormulaBot1;
                        objPRO375.FormulaBot2 = PRO375.FormulaBot2;
                        objPRO375.FormulaBot3 = PRO375.FormulaBot3;
                        objPRO375.FormulaBot4 = PRO375.FormulaBot4;
                        objPRO375.ActTop1 = PRO375.ActTop1;
                        objPRO375.ActTop2 = PRO375.ActTop2;
                        objPRO375.ActTop3 = PRO375.ActTop3;
                        objPRO375.ActTop4 = PRO375.ActTop4;
                        objPRO375.ActBot1 = PRO375.ActBot1;
                        objPRO375.ActBot2 = PRO375.ActBot2;
                        objPRO375.ActBot3 = PRO375.ActBot3;
                        objPRO375.ActBot4 = PRO375.ActBot4;
                        objPRO375.Act0 = PRO375.Act0;
                        objPRO375.Act90 = PRO375.Act90;
                        objPRO375.Act180 = PRO375.Act180;
                        objPRO375.Act270 = PRO375.Act270;
                        objPRO375.ReqConcentricity = PRO375.ReqConcentricity;
                        objPRO375.ActConcentricity = PRO375.ActConcentricity;
                        objPRO375.evalueTemplate = PRO375.evalueTemplate;
                        objPRO375.AtMiddle = PRO375.AtMiddle;
                        objPRO375.ReqChordLengthTop = PRO375.ReqChordLengthTop;
                        objPRO375.ReqRadiusTop = PRO375.ReqRadiusTop;
                        objPRO375.ReqChordLengthMiddle = PRO375.ReqChordLengthMiddle;
                        objPRO375.ReqRadiusMiddle = PRO375.ReqRadiusMiddle;
                        objPRO375.ReqChordLengthBottom = PRO375.ReqChordLengthBottom;
                        objPRO375.ReqRadiusBottom = PRO375.ReqRadiusBottom;
                        objPRO375.ActChordLengthTop = PRO375.ActChordLengthTop;
                        objPRO375.ActRadiusTop = PRO375.ActRadiusTop;
                        objPRO375.ActChordLengthMiddle = PRO375.ActChordLengthMiddle;
                        objPRO375.ActRadiusMiddle = PRO375.ActRadiusMiddle;
                        objPRO375.ActChordLengthAtBottom = PRO375.ActChordLengthAtBottom;
                        objPRO375.ActRadiusAtBottom = PRO375.ActRadiusAtBottom;
                        objPRO375.TopGapAllowed = PRO375.TopGapAllowed;
                        objPRO375.MiddleGapAllowed = PRO375.MiddleGapAllowed;
                        objPRO375.BottomGapAllowed = PRO375.BottomGapAllowed;
                        objPRO375.ProfileMeasurement = PRO375.ProfileMeasurement;
                        objPRO375.OffsetAllowed = PRO375.OffsetAllowed;
                        objPRO375.ReqKnuckleRadiusAtTop = PRO375.ReqKnuckleRadiusAtTop;
                        objPRO375.ActKnuckleRadiusAtTop = PRO375.ActKnuckleRadiusAtTop;
                        objPRO375.ReqKnuckleRadiusAtBottom = PRO375.ReqKnuckleRadiusAtBottom;
                        objPRO375.ActKnuckleRadiusAtBottom = PRO375.ActKnuckleRadiusAtBottom;
                        objPRO375.ReqStraightFace = PRO375.ReqStraightFace;
                        objPRO375.CheckPoint1 = PRO375.CheckPoint1;
                        objPRO375.CheckPoint2 = PRO375.CheckPoint2;
                        objPRO375.CheckPoint3 = PRO375.CheckPoint3;
                        objPRO375.CheckPoint4 = PRO375.CheckPoint4;
                        objPRO375.CheckPoint5 = PRO375.CheckPoint5;
                        objPRO375.CheckPoint7 = PRO375.CheckPoint7;
                        objPRO375.CheckPoint7_2 = PRO375.CheckPoint7_2;
                        objPRO375.CheckPoint8 = PRO375.CheckPoint8;
                        objPRO375.QCRemarks = PRO375.QCRemarks;
                        objPRO375.Result = PRO375.Result;
                        objPRO375.ReqWidth = PRO375.ReqWidth;
                        objPRO375.ReqDepth = PRO375.ReqDepth;
                        objPRO375.ReqOffset = PRO375.ReqOffset;
                        objPRO375.Note1 = PRO375.Note1;
                        objPRO375.Note2 = PRO375.Note2;
                        objPRO375.Note3 = PRO375.Note3;


                        objPRO375.EditedBy = objClsLoginInfo.UserName;
                        objPRO375.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO375.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO375 objPRO375 = db.PRO375.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO375 != null && string.IsNullOrWhiteSpace(objPRO375.ProtocolNo))
                    {
                        db.PRO375.Remove(objPRO375);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO375.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO376> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO376> lstAddPRO376 = new List<PRO376>();
                List<PRO376> lstDeletePRO376 = new List<PRO376>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO376 obj = db.PRO376.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.Orientation = item.Orientation;
                                obj.ActAtTop = item.ActAtTop;
                                obj.ActAtBottom = item.ActAtBottom;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO376 obj = new PRO376();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.Orientation))
                            {

                                obj.Orientation = item.Orientation;
                                obj.ActAtTop = item.ActAtTop;
                                obj.ActAtBottom = item.ActAtBottom;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO376.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO376.Count > 0)
                    {
                        db.PRO376.AddRange(lstAddPRO376);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO376 = db.PRO376.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO376.Count > 0)
                    {
                        db.PRO376.RemoveRange(lstDeletePRO376);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO376 = db.PRO376.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO376.Count > 0)
                    {
                        db.PRO376.RemoveRange(lstDeletePRO376);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2(List<PRO377> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO377> lstAddPRO377 = new List<PRO377>();
                List<PRO377> lstDeletePRO377 = new List<PRO377>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO377 obj = db.PRO377.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO377();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Orientation = item.Orientation;
                        obj.Actual = item.Actual;
                        if (isAdded)
                        {
                            lstAddPRO377.Add(obj);
                        }
                    }
                    if (lstAddPRO377.Count > 0)
                    {
                        db.PRO377.AddRange(lstAddPRO377);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO377 = db.PRO377.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO377.Count > 0)
                    {
                        db.PRO377.RemoveRange(lstDeletePRO377);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO377 = db.PRO377.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO377.Count > 0)
                    {
                        db.PRO377.RemoveRange(lstDeletePRO377);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3(List<PRO378> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO378> lstAddPRO378 = new List<PRO378>();
                List<PRO378> lstDeletePRO378 = new List<PRO378>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO378 obj = db.PRO378.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        else
                        {
                            obj = new PRO378();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        obj.ReqAtTop = item.ReqAtTop;
                        obj.ActAtTop = item.ActAtTop;
                        obj.ReqAtMiddle = item.ReqAtMiddle;
                        obj.ActAtMiddle = item.ActAtMiddle;
                        obj.ReqAtBottom = item.ReqAtBottom;
                        obj.ActAtBottom = item.ActAtBottom;


                        if (isAdded)
                        {
                            lstAddPRO378.Add(obj);
                        }
                    }
                    if (lstAddPRO378.Count > 0)
                    {
                        db.PRO378.AddRange(lstAddPRO378);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO378 = db.PRO378.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO378.Count > 0)
                    {
                        db.PRO378.RemoveRange(lstDeletePRO378);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO378 = db.PRO378.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO378.Count > 0)
                    {
                        db.PRO378.RemoveRange(lstDeletePRO378);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line4 details
        [HttpPost]
        public JsonResult SaveProtocolLine4(List<PRO379> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO379> lstAddPRO379 = new List<PRO379>();
                List<PRO379> lstDeletePRO379 = new List<PRO379>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO379 obj = db.PRO379.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO379();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SeamNo = item.SeamNo;
                        obj.ReqOrien = item.ReqOrien;
                        obj.ReqAtTop = item.ReqAtTop;
                        obj.ActAtTop = item.ActAtTop;
                        obj.ReqAtBottom = item.ReqAtBottom;
                        obj.ActAtBottom = item.ActAtBottom;



                        if (isAdded)
                        {
                            lstAddPRO379.Add(obj);
                        }
                    }
                    if (lstAddPRO379.Count > 0)
                    {
                        db.PRO379.AddRange(lstAddPRO379);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO379 = db.PRO379.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO379.Count > 0)
                    {
                        db.PRO379.RemoveRange(lstDeletePRO379);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO379 = db.PRO379.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO379.Count > 0)
                    {
                        db.PRO379.RemoveRange(lstDeletePRO379);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line5 details
        [HttpPost]
        public JsonResult SaveProtocolLine5(List<PRO379_2> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO379_2> lstAddPRO379_2 = new List<PRO379_2>();
                List<PRO379_2> lstDeletePRO379_2 = new List<PRO379_2>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO379_2 obj = db.PRO379_2.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO379_2();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }



                        obj.SeamNo = item.SeamNo;
                        obj.TopPeakIn = item.TopPeakIn;
                        obj.TopPeakOut = item.TopPeakOut;
                        obj.MidPeakIn = item.MidPeakIn;
                        obj.MidPrakOut = item.MidPrakOut;
                        obj.BottomPeakIn = item.BottomPeakIn;
                        obj.BottomPeakOut = item.BottomPeakOut;
                        obj.ActOffsetMin = item.ActOffsetMin;
                        obj.ActOffsetMax = item.ActOffsetMax;

                        if (isAdded)
                        {
                            lstAddPRO379_2.Add(obj);
                        }
                    }
                    if (lstAddPRO379_2.Count > 0)
                    {
                        db.PRO379_2.AddRange(lstAddPRO379_2);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO379_2 = db.PRO379_2.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO379_2.Count > 0)
                    {
                        db.PRO379_2.RemoveRange(lstDeletePRO379_2);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO379_2 = db.PRO379_2.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO379_2.Count > 0)
                    {
                        db.PRO379_2.RemoveRange(lstDeletePRO379_2);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line6 details
        [HttpPost]
        public JsonResult SaveProtocolLine6(List<PRO379_3> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO379_3> lstAddPRO379_3 = new List<PRO379_3>();
                List<PRO379_3> lstDeletePRO379_3 = new List<PRO379_3>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO379_3 obj = db.PRO379_3.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO379_3();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }



                        obj.PetalNo = item.PetalNo;
                        obj.K1 = item.K1;
                        obj.K2 = item.K2;
                        obj.K3 = item.K3;
                        obj.K1T = item.K1T;
                        obj.K2T = item.K2T;
                        obj.K3T = item.K3T;
                        obj.ActStraightAtTop = item.ActStraightAtTop;
                        obj.ActStraightAtBottom = item.ActStraightAtBottom;

                        if (isAdded)
                        {
                            lstAddPRO379_3.Add(obj);
                        }
                    }
                    if (lstAddPRO379_3.Count > 0)
                    {
                        db.PRO379_3.AddRange(lstAddPRO379_3);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO379_3 = db.PRO379_3.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO379_3.Count > 0)
                    {
                        db.PRO379_3.RemoveRange(lstDeletePRO379_3);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO379_3 = db.PRO379_3.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO379_3.Count > 0)
                    {
                        db.PRO379_3.RemoveRange(lstDeletePRO379_3);
                    }
                    db.SaveChanges();
                }

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion



        #region Line7 details

        [HttpPost]
        public JsonResult SaveProtocolLine7(List<PRO379_4> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO379_4> lstAddPRO379_4 = new List<PRO379_4>();
                List<PRO379_4> lstDeletePRO379_4 = new List<PRO379_4>();

                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO379_4 obj = db.PRO379_4.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        PRO379_4 objFecthSeam = new PRO379_4();
                        if (obj == null)
                        {
                            obj = new PRO379_4();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        objFecthSeam = db.PRO379_4.Where(x => x.SeamNo == item.SeamNo && x.HeaderId == item.HeaderId).FirstOrDefault();
                        if (objFecthSeam != null)
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                            //obj.Depth = objFecthSeam.Depth;
                            //obj.Width = objFecthSeam.Width;
                            //obj.Offset = objFecthSeam.Offset;
                        }
                        else
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                            //obj.Depth = item.Depth;
                            //obj.Width = item.Width;
                            //obj.Offset = item.Offset;
                        }

                        obj.SpotNo = item.SpotNo;
                        obj.Depth = item.Depth;
                        obj.Width = item.Width;
                        obj.Offset = item.Offset;
                        obj.SpotNoClad = item.SpotNoClad;
                        if (isAdded)
                        {
                            lstAddPRO379_4.Add(obj);
                        }
                    }
                }
                if (lstAddPRO379_4.Count > 0)
                {
                    db.PRO379_4.AddRange(lstAddPRO379_4);
                }
                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRO379_4 = db.PRO379_4.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRO379_4 = db.PRO379_4.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRO379_4.Count > 0)
                {
                    db.PRO379_4.RemoveRange(lstDeletePRO379_4);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion


        #region Linkage View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL375 objPRL375 = new PRL375();
            objPRL375 = db.PRL375.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL375 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL375.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;

                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();
            List<string> lsttype = new List<string> { "TORI CONE", "STRAIGHT CONE" };


            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.Type = lsttype.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL376> lstPRL376 = db.PRL376.Where(x => x.HeaderId == objPRL375.HeaderId).ToList();
            List<PRL377> lstPRL377 = db.PRL377.Where(x => x.HeaderId == objPRL375.HeaderId).ToList();
            List<PRL378> lstPRL378 = db.PRL378.Where(x => x.HeaderId == objPRL375.HeaderId).ToList();
            List<PRL379> lstPRL379 = db.PRL379.Where(x => x.HeaderId == objPRL375.HeaderId).ToList();
            List<PRL379_2> lstPRL379_2 = db.PRL379_2.Where(x => x.HeaderId == objPRL375.HeaderId).ToList();
            List<PRL379_3> lstPRL379_3 = db.PRL379_3.Where(x => x.HeaderId == objPRL375.HeaderId).ToList();
            //List<PRL379_4> lstPRL379_4 = db.PRL379_4.Where(x => x.HeaderId == objPRL375.HeaderId).ToList();
            //List<PRL379_5> lstPRL379_5 = db.PRL379_5.Where(x => x.HeaderId == objPRL375.HeaderId).ToList();
            //List<PRL379_6> lstPRL379_6 = db.PRL379_6.Where(x => x.HeaderId == objPRL375.HeaderId).ToList();
            //List<PRL379_9> lstPRL379_9 = db.PRL379_9.Where(x => x.HeaderId == objPRL375.HeaderId).ToList();

            List<SP_IPI_PROTOCOL076_LINKAGE_GET_LINES7_DATA_Result> lstPRL379_4 = db.SP_IPI_PROTOCOL076_LINKAGE_GET_LINES7_DATA(1, int.MaxValue, "", " HeaderId = " + objPRL375.HeaderId).ToList();

            //List<SP_IPI_PROTOCOL041_GET_LINES10_DATA_Result> lstPRL379_7 = db.SP_IPI_PROTOCOL041_GET_LINES10_DATA(1, int.MaxValue, "", " HeaderId = " + objPRL375.HeaderId).ToList();
            //List<SP_IPI_PROTOCOL041_GET_LINES11_DATA_Result> lstPRL379_8 = db.SP_IPI_PROTOCOL041_GET_LINES11_DATA(1, int.MaxValue, "", " HeaderId = " + objPRL375.HeaderId).ToList();


            ViewBag.lstPRL376 = lstPRL376;
            ViewBag.lstPRL377 = lstPRL377;
            ViewBag.lstPRL378 = lstPRL378;
            ViewBag.lstPRL379 = lstPRL379;
            ViewBag.lstPRL379_2 = lstPRL379_2;
            ViewBag.lstPRL379_3 = lstPRL379_3;
            ViewBag.lstPRL379_4 = lstPRL379_4;
            //ViewBag.lstPRL379_5 = lstPRL379_5;
            //ViewBag.lstPRL379_6 = lstPRL379_6;
            //ViewBag.lstPRL379_7 = lstPRL379_7;
            //ViewBag.lstPRL379_8 = lstPRL379_8;
            //ViewBag.lstPRL379_9 = lstPRL379_9;

            #endregion

            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.lstDrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL375.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL375.ActFilledBy) && (objPRL375.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL375.Project).ToList();
                            ViewBag.lstDrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.lstDrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL375);
        }

        [HttpPost]
        public ActionResult LinkageMobile(int? id, int? m, string APIRequestFrom = "", string UserName = "")
        {
            string ProjectDesc = "";
            string ManufacturingCode = "";
            string AccessRole = "";
            bool isEditable = false;
            var DrawingNo = new List<string>();
            bool isQCRemarkEditable = false;

            PRL375 objPRL375 = new PRL375();
            db.Configuration.ProxyCreationEnabled = false;
            objPRL375 = db.PRL375.Where(i => i.HeaderId == id).FirstOrDefault();
            db.Entry(objPRL375).State = System.Data.Entity.EntityState.Detached;
            NDEModels objNDEModels = new NDEModels();
            if (objPRL375 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL375.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;

                }
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
            AccessRole = objUserRoleAccessDetails.UserDesignation;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();
            List<string> lsttype = new List<string> { "TORI CONE", "STRAIGHT CONE" };


            var YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var Type = lsttype.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL376> lstPRL376 = db.PRL376.Where(x => x.HeaderId == objPRL375.HeaderId).ToList();
            List<PRL377> lstPRL377 = db.PRL377.Where(x => x.HeaderId == objPRL375.HeaderId).ToList();
            List<PRL378> lstPRL378 = db.PRL378.Where(x => x.HeaderId == objPRL375.HeaderId).ToList();
            List<PRL379> lstPRL379 = db.PRL379.Where(x => x.HeaderId == objPRL375.HeaderId).ToList();
            List<PRL379_2> lstPRL379_2 = db.PRL379_2.Where(x => x.HeaderId == objPRL375.HeaderId).ToList();
            List<PRL379_3> lstPRL379_3 = db.PRL379_3.Where(x => x.HeaderId == objPRL375.HeaderId).ToList();
            //List<PRL379_4> lstPRL379_4 = db.PRL379_4.Where(x => x.HeaderId == objPRL375.HeaderId).ToList();
            //List<PRL379_5> lstPRL379_5 = db.PRL379_5.Where(x => x.HeaderId == objPRL375.HeaderId).ToList();
            //List<PRL379_6> lstPRL379_6 = db.PRL379_6.Where(x => x.HeaderId == objPRL375.HeaderId).ToList();
            //List<PRL379_9> lstPRL379_9 = db.PRL379_9.Where(x => x.HeaderId == objPRL375.HeaderId).ToList();

            List<SP_IPI_PROTOCOL076_LINKAGE_GET_LINES7_DATA_Result> lstPRL379_4 = db.SP_IPI_PROTOCOL076_LINKAGE_GET_LINES7_DATA(1, int.MaxValue, "", " HeaderId = " + objPRL375.HeaderId).ToList();

            //List<SP_IPI_PROTOCOL041_GET_LINES10_DATA_Result> lstPRL379_7 = db.SP_IPI_PROTOCOL041_GET_LINES10_DATA(1, int.MaxValue, "", " HeaderId = " + objPRL375.HeaderId).ToList();
            //List<SP_IPI_PROTOCOL041_GET_LINES11_DATA_Result> lstPRL379_8 = db.SP_IPI_PROTOCOL041_GET_LINES11_DATA(1, int.MaxValue, "", " HeaderId = " + objPRL375.HeaderId).ToList();


            //ViewBag.lstPRL379_5 = lstPRL379_5;
            //ViewBag.lstPRL379_6 = lstPRL379_6;
            //ViewBag.lstPRL379_7 = lstPRL379_7;
            //ViewBag.lstPRL379_8 = lstPRL379_8;
            //ViewBag.lstPRL379_9 = lstPRL379_9;

            #endregion

            try
            {
                isEditable = false;
                isQCRemarkEditable = false;
                if (m == 1)
                {
                    isEditable = true;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL375.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL375.ActFilledBy) && (objPRL375.RequestNo.HasValue))
                        {
                            isEditable = false;
                            isQCRemarkEditable = true;
                        }
                        else
                        {
                            DrawingNo = (new GeneralController()).GetPLMDrawingNumber(objPRL375.Project).ToList();
                        }
                    }
                }

                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    ProjectDesc = ProjectDesc,
                    ManufacturingCode = ManufacturingCode,
                    isEditable = isEditable,
                    isQCRemarkEditable = isQCRemarkEditable,
                    DrawingNo = DrawingNo,
                    AccessRole = AccessRole,

                    YesNoEnum = YesNoEnum,
                    YesNAEnum = YesNAEnum,
                    Type = Type,
                    OutsideInside = OutsideInside,
                    LeftRight = LeftRight,

                    objPRL375 = objPRL375,

                    lstPRL376 = lstPRL376,
                    lstPRL377 = lstPRL377,
                    lstPRL378 = lstPRL378,
                    lstPRL379 = lstPRL379,
                    lstPRL379_2 = lstPRL379_2,
                    lstPRL379_3 = lstPRL379_3,
                    lstPRL379_4 = lstPRL379_4,

                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                return Json(new
                {
                    res = "0",
                    msg = ex.Message,
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL375 PRL375, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = PRL375.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL375 objPRL375 = db.PRL375.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    #region Save Data

                    objPRL375.DrawingRevisionNo = PRL375.DrawingRevisionNo;
                    objPRL375.DrawingNo = PRL375.DrawingNo;
                    objPRL375.DCRNo = PRL375.DCRNo;
                    objPRL375.ProtocolNo = PRL375.ProtocolNo != null ? PRL375.ProtocolNo : "";


                    objPRL375.Type = PRL375.Type;
                    objPRL375.DiscOutsideCirc = PRL375.DiscOutsideCirc;
                    objPRL375.ReqTopOutsideCirc = PRL375.ReqTopOutsideCirc;
                    objPRL375.ActTopOutsideCirc = PRL375.ActTopOutsideCirc;
                    objPRL375.RemarksOutsideCirc = PRL375.RemarksOutsideCirc;
                    objPRL375.ReqBottomOutsideCirc = PRL375.ReqBottomOutsideCirc;
                    objPRL375.ActBottomOutsideCirc = PRL375.ActBottomOutsideCirc;
                    objPRL375.DiscInsideCirc1 = PRL375.DiscInsideCirc1;
                    objPRL375.ReqTopInsideCirc = PRL375.ReqTopInsideCirc;
                    objPRL375.ActTopInsideCirc = PRL375.ActTopInsideCirc;
                    objPRL375.RemarksInsideCirc = PRL375.RemarksInsideCirc;
                    objPRL375.ReqBottomInsideCirc = PRL375.ReqBottomInsideCirc;
                    objPRL375.ActBottomInsideCirc = PRL375.ActBottomInsideCirc;
                    objPRL375.DiscInsideCirc2 = PRL375.DiscInsideCirc2;
                    objPRL375.ReqTopInsideCirc2 = PRL375.ReqTopInsideCirc2;
                    objPRL375.ActCwTopInsideCirc2 = PRL375.ActCwTopInsideCirc2;
                    objPRL375.ActAcwTopInsideCirc2 = PRL375.ActAcwTopInsideCirc2;
                    objPRL375.ActAvgTopInsideCirc2 = PRL375.ActAvgTopInsideCirc2;
                    objPRL375.RemarksInsideCirc2 = PRL375.RemarksInsideCirc2;
                    objPRL375.ReqBottomInsideCirc2 = PRL375.ReqBottomInsideCirc2;
                    objPRL375.ActCwBottomInsideCirc2 = PRL375.ActCwBottomInsideCirc2;
                    objPRL375.ActAcwBottomInsideCirc2 = PRL375.ActAcwBottomInsideCirc2;
                    objPRL375.ActAvgBottomInsideCirc2 = PRL375.ActAvgBottomInsideCirc2;
                    objPRL375.ReqMinThickness = PRL375.ReqMinThickness;
                    objPRL375.ActMinThickness = PRL375.ActMinThickness;
                    objPRL375.RemarkMinThickness = PRL375.RemarkMinThickness;
                    objPRL375.Ovality = PRL375.Ovality;
                    objPRL375.OutByActTop = PRL375.OutByActTop;
                    objPRL375.OutByActBottom = PRL375.OutByActBottom;
                    objPRL375.ReqHieghtOfCone = PRL375.ReqHieghtOfCone;
                    objPRL375.ActCfTop = PRL375.ActCfTop;
                    objPRL375.ActCfBot = PRL375.ActCfBot;
                    objPRL375.FormulaTop1 = PRL375.FormulaTop1;
                    objPRL375.FormulaTop2 = PRL375.FormulaTop2;
                    objPRL375.FormulaTop3 = PRL375.FormulaTop3;
                    objPRL375.FormulaTop4 = PRL375.FormulaTop4;
                    objPRL375.FormulaBot1 = PRL375.FormulaBot1;
                    objPRL375.FormulaBot2 = PRL375.FormulaBot2;
                    objPRL375.FormulaBot3 = PRL375.FormulaBot3;
                    objPRL375.FormulaBot4 = PRL375.FormulaBot4;
                    objPRL375.ActTop1 = PRL375.ActTop1;
                    objPRL375.ActTop2 = PRL375.ActTop2;
                    objPRL375.ActTop3 = PRL375.ActTop3;
                    objPRL375.ActTop4 = PRL375.ActTop4;
                    objPRL375.ActBot1 = PRL375.ActBot1;
                    objPRL375.ActBot2 = PRL375.ActBot2;
                    objPRL375.ActBot3 = PRL375.ActBot3;
                    objPRL375.ActBot4 = PRL375.ActBot4;
                    objPRL375.Act0 = PRL375.Act0;
                    objPRL375.Act90 = PRL375.Act90;
                    objPRL375.Act180 = PRL375.Act180;
                    objPRL375.Act270 = PRL375.Act270;
                    objPRL375.ReqConcentricity = PRL375.ReqConcentricity;
                    objPRL375.ActConcentricity = PRL375.ActConcentricity;
                    objPRL375.evalueTemplate = PRL375.evalueTemplate;
                    objPRL375.AtMiddle = PRL375.AtMiddle;
                    objPRL375.ReqChordLengthTop = PRL375.ReqChordLengthTop;
                    objPRL375.ReqRadiusTop = PRL375.ReqRadiusTop;
                    objPRL375.ReqChordLengthMiddle = PRL375.ReqChordLengthMiddle;
                    objPRL375.ReqRadiusMiddle = PRL375.ReqRadiusMiddle;
                    objPRL375.ReqChordLengthBottom = PRL375.ReqChordLengthBottom;
                    objPRL375.ReqRadiusBottom = PRL375.ReqRadiusBottom;
                    objPRL375.ActChordLengthTop = PRL375.ActChordLengthTop;
                    objPRL375.ActRadiusTop = PRL375.ActRadiusTop;
                    objPRL375.ActChordLengthMiddle = PRL375.ActChordLengthMiddle;
                    objPRL375.ActRadiusMiddle = PRL375.ActRadiusMiddle;
                    objPRL375.ActChordLengthAtBottom = PRL375.ActChordLengthAtBottom;
                    objPRL375.ActRadiusAtBottom = PRL375.ActRadiusAtBottom;
                    objPRL375.TopGapAllowed = PRL375.TopGapAllowed;
                    objPRL375.MiddleGapAllowed = PRL375.MiddleGapAllowed;
                    objPRL375.BottomGapAllowed = PRL375.BottomGapAllowed;
                    objPRL375.ProfileMeasurement = PRL375.ProfileMeasurement;
                    objPRL375.OffsetAllowed = PRL375.OffsetAllowed;
                    objPRL375.ReqKnuckleRadiusAtTop = PRL375.ReqKnuckleRadiusAtTop;
                    objPRL375.ActKnuckleRadiusAtTop = PRL375.ActKnuckleRadiusAtTop;
                    objPRL375.ReqKnuckleRadiusAtBottom = PRL375.ReqKnuckleRadiusAtBottom;
                    objPRL375.ActKnuckleRadiusAtBottom = PRL375.ActKnuckleRadiusAtBottom;
                    objPRL375.ReqStraightFace = PRL375.ReqStraightFace;
                    objPRL375.CheckPoint1 = PRL375.CheckPoint1;
                    objPRL375.CheckPoint2 = PRL375.CheckPoint2;
                    objPRL375.CheckPoint3 = PRL375.CheckPoint3;
                    objPRL375.CheckPoint4 = PRL375.CheckPoint4;
                    objPRL375.CheckPoint5 = PRL375.CheckPoint5;
                    objPRL375.CheckPoint7 = PRL375.CheckPoint7;
                    objPRL375.CheckPoint7_2 = PRL375.CheckPoint7_2;
                    objPRL375.CheckPoint8 = PRL375.CheckPoint8;
                    objPRL375.QCRemarks = PRL375.QCRemarks;
                    objPRL375.Result = PRL375.Result;
                    objPRL375.ReqWidth = PRL375.ReqWidth;
                    objPRL375.ReqDepth = PRL375.ReqDepth;
                    objPRL375.ReqOffset = PRL375.ReqOffset;
                    objPRL375.Note1 = PRL375.Note1;
                    objPRL375.Note2 = PRL375.Note2;
                    objPRL375.Note3 = PRL375.Note3;


                    objPRL375.EditedBy = UserName;
                    objPRL375.EditedOn = DateTime.Now;

                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL375.ActFilledBy = UserName;
                            objPRL375.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL375.ReqFilledBy = UserName;
                            objPRL375.ReqFilledOn = DateTime.Now;
                        }
                    }
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL375.HeaderId;
                    #endregion

                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL375 objPRL375 = db.PRL375.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL375 != null && string.IsNullOrWhiteSpace(objPRL375.ProtocolNo))
                    {
                        db.PRL375.Remove(objPRL375);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL375.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL376> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL376> lstAddPRL376 = new List<PRL376>();
                List<PRL376> lstDeletePRL376 = new List<PRL376>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRL376 obj = db.PRL376.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.Orientation = item.Orientation;
                                obj.ActAtTop = item.ActAtTop;
                                obj.ActAtBottom = item.ActAtBottom;
                                obj.EditedBy = UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRL376 obj = new PRL376();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.Orientation))
                            {
                                obj.Orientation = item.Orientation;
                                obj.ActAtTop = item.ActAtTop;
                                obj.ActAtBottom = item.ActAtBottom;
                                obj.CreatedBy = UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRL376.Add(obj);
                            }
                        }
                    }
                }
                if (lstAddPRL376.Count > 0)
                {
                    db.PRL376.AddRange(lstAddPRL376);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL376 = db.PRL376.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL376 = db.PRL376.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL376.Count > 0)
                {
                    db.PRL376.RemoveRange(lstDeletePRL376);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine2Linkage(List<PRL377> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL377> lstAddPRL377 = new List<PRL377>();
                List<PRL377> lstDeletePRL377 = new List<PRL377>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL377 obj = db.PRL377.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL377();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Orientation = item.Orientation;
                        obj.Actual = item.Actual;
                        if (isAdded)
                        {
                            lstAddPRL377.Add(obj);
                        }
                    }
                }
                if (lstAddPRL377.Count > 0)
                {
                    db.PRL377.AddRange(lstAddPRL377);
                }
                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL377 = db.PRL377.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL377 = db.PRL377.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL377.Count > 0)
                {
                    db.PRL377.RemoveRange(lstDeletePRL377);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line3 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine3Linkage(List<PRL378> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL378> lstAddPRL378 = new List<PRL378>();
                List<PRL378> lstDeletePRL378 = new List<PRL378>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL378 obj = db.PRL378.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        else
                        {
                            obj = new PRL378();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }

                        if (!string.IsNullOrWhiteSpace(item.ReqAtTop))
                        {
                            obj.ReqAtTop = item.ReqAtTop;
                            obj.ActAtTop = item.ActAtTop;
                            obj.ReqAtMiddle = item.ReqAtMiddle;
                            obj.ActAtMiddle = item.ActAtMiddle;
                            obj.ReqAtBottom = item.ReqAtBottom;
                            obj.ActAtBottom = item.ActAtBottom;
                        }
                        if (isAdded)
                        {
                            lstAddPRL378.Add(obj);
                        }
                    }
                    if (lstAddPRL378.Count > 0)
                    {
                        db.PRL378.AddRange(lstAddPRL378);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL378 = db.PRL378.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL378 = db.PRL378.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL378.Count > 0)
                {
                    db.PRL378.RemoveRange(lstDeletePRL378);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line4 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine4Linkage(List<PRL379> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL379> lstAddPRL379 = new List<PRL379>();
                List<PRL379> lstDeletePRL379 = new List<PRL379>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL379 obj = db.PRL379.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL379();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SeamNo = item.SeamNo;
                        obj.ReqOrien = item.ReqOrien;
                        obj.ReqAtTop = item.ReqAtTop;
                        obj.ActAtTop = item.ActAtTop;
                        obj.ReqAtBottom = item.ReqAtBottom;
                        obj.ActAtBottom = item.ActAtBottom;

                        if (isAdded)
                        {
                            lstAddPRL379.Add(obj);
                        }
                    }

                    if (lstAddPRL379.Count > 0)
                    {
                        db.PRL379.AddRange(lstAddPRL379);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL379 = db.PRL379.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL379 = db.PRL379.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL379.Count > 0)
                {
                    db.PRL379.RemoveRange(lstDeletePRL379);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line5 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine5Linkage(List<PRL379_2> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL379_2> lstAddPRL379_2 = new List<PRL379_2>();
                List<PRL379_2> lstDeletePRL379_2 = new List<PRL379_2>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL379_2 obj = db.PRL379_2.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL379_2();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        obj.SeamNo = item.SeamNo;
                        obj.TopPeakIn = item.TopPeakIn;
                        obj.TopPeakOut = item.TopPeakOut;
                        obj.MidPeakIn = item.MidPeakIn;
                        obj.MidPrakOut = item.MidPrakOut;
                        obj.BottomPeakIn = item.BottomPeakIn;
                        obj.BottomPeakOut = item.BottomPeakOut;
                        obj.ActOffsetMin = item.ActOffsetMin;
                        obj.ActOffsetMax = item.ActOffsetMax;


                        if (isAdded)
                        {
                            lstAddPRL379_2.Add(obj);
                        }
                    }

                    if (lstAddPRL379_2.Count > 0)
                    {
                        db.PRL379_2.AddRange(lstAddPRL379_2);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL379_2 = db.PRL379_2.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL379_2 = db.PRL379_2.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL379_2.Count > 0)
                {
                    db.PRL379_2.RemoveRange(lstDeletePRL379_2);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line6 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine6Linkage(List<PRL379_3> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL379_3> lstAddPRL379_3 = new List<PRL379_3>();
                List<PRL379_3> lstDeletePRL379_3 = new List<PRL379_3>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL379_3 obj = db.PRL379_3.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL379_3();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.PetalNo = item.PetalNo;
                        obj.K1 = item.K1;
                        obj.K2 = item.K2;
                        obj.K3 = item.K3;
                        obj.K1T = item.K1T;
                        obj.K2T = item.K2T;
                        obj.K3T = item.K3T;
                        obj.ActStraightAtTop = item.ActStraightAtTop;
                        obj.ActStraightAtBottom = item.ActStraightAtBottom;

                        if (isAdded)
                        {
                            lstAddPRL379_3.Add(obj);
                        }
                    }

                    if (lstAddPRL379_3.Count > 0)
                    {
                        db.PRL379_3.AddRange(lstAddPRL379_3);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL379_3 = db.PRL379_3.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL379_3 = db.PRL379_3.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL379_3.Count > 0)
                {
                    db.PRL379_3.RemoveRange(lstDeletePRL379_3);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line7 Linkage

        [HttpPost]
        public JsonResult SaveProtocolLine7Linkage(List<PRL379_4> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL379_4> lstAddPRL379_4 = new List<PRL379_4>();
                List<PRL379_4> lstDeletePRL379_4 = new List<PRL379_4>();

                if (lst != null)
                {

                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL379_4 obj = db.PRL379_4.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        PRL379_4 objFecthSeam = new PRL379_4();
                        if (obj == null)
                        {
                            obj = new PRL379_4();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        objFecthSeam = db.PRL379_4.Where(x => x.SeamNo == item.SeamNo && x.HeaderId == item.HeaderId).FirstOrDefault();
                        if (objFecthSeam != null)
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                            //obj.Depth = objFecthSeam.Depth;
                            //obj.Width = objFecthSeam.Width;
                            //obj.Offset = objFecthSeam.Offset;
                        }
                        else
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                            //obj.Depth = item.Depth;
                            //obj.Width = item.Width;
                            //obj.Offset = item.Offset;
                        }
                        obj.Depth = item.Depth;
                        obj.Width = item.Width;
                        obj.Offset = item.Offset;
                        obj.SpotNo = item.SpotNo;
                        obj.SpotNoClad = item.SpotNoClad;
                        if (isAdded)
                        {
                            lstAddPRL379_4.Add(obj);
                        }
                    }
                    if (lstAddPRL379_4.Count > 0)
                    {
                        db.PRL379_4.AddRange(lstAddPRL379_4);
                    }
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL379_4 = db.PRL379_4.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL379_4 = db.PRL379_4.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL379_4.Count > 0)
                {
                    db.PRL379_4.RemoveRange(lstDeletePRL379_4);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #endregion
    }
}