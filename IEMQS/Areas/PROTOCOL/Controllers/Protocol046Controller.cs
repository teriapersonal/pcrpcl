﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsBase;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol046Controller : clsBase
    {
        // GET: PROTOCOL/Protocol046
        string ControllerURL = "/PROTOCOL/Protocol046/";
        string Title = "VISUAL AND DIMENSION INSPECTION REPORT FOR FORMED ELLIPSOIDAL/TORI SPHERICAL D'END";


        #region Details View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO225 objPRO225 = new PRO225();
            if (!id.HasValue)
            {
                try
                {
                    objPRO225.ProtocolNo = string.Empty;
                    objPRO225.CreatedBy = objClsLoginInfo.UserName;
                    objPRO225.CreatedOn = DateTime.Now;

                    #region OVALITY 
                    objPRO225.PRO226.Add(new PRO226
                    {
                        Orientation = "0°-180° ",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO225.PRO226.Add(new PRO226
                    {
                        Orientation = "22.5°-202.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO225.PRO226.Add(new PRO226
                    {
                        Orientation = "45°-225°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO225.PRO226.Add(new PRO226
                    {
                        Orientation = "67.5°-247.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO225.PRO226.Add(new PRO226
                    {
                        Orientation = "90°-270°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO225.PRO226.Add(new PRO226
                    {
                        Orientation = "112.5°-292.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO225.PRO226.Add(new PRO226
                    {
                        Orientation = "135°-315°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO225.PRO226.Add(new PRO226
                    {
                        Orientation = "157.5°-337.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion

                    #region TOTAL HEIGHT
                    objPRO225.PRO228.Add(new PRO228
                    {
                        TotalHeight1 = "AT 0°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO225.PRO228.Add(new PRO228
                    {
                        TotalHeight1 = "AT 90°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO225.PRO228.Add(new PRO228
                    {
                        TotalHeight1 = "AT 180°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO225.PRO228.Add(new PRO228
                    {
                        TotalHeight1 = "AT 270°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });

                    #endregion



                    db.PRO225.Add(objPRO225);
                    db.SaveChanges();
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO225.HeaderId;
            }
            else
            {
                objPRO225 = db.PRO225.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO226> lstPRO226 = db.PRO226.Where(x => x.HeaderId == objPRO225.HeaderId).ToList();
            List<PRO227> lstPRO227 = db.PRO227.Where(x => x.HeaderId == objPRO225.HeaderId).ToList();
            List<PRO228> lstPRO228 = db.PRO228.Where(x => x.HeaderId == objPRO225.HeaderId).ToList();
            List<PRO229> lstPRO229 = db.PRO229.Where(x => x.HeaderId == objPRO225.HeaderId).ToList();
            List<PRO229_2> lstPRO229_2 = db.PRO229_2.Where(x => x.HeaderId == objPRO225.HeaderId).ToList();
            List<SP_IPI_PROTOCOL046_GET_LINES6_DATA_Result> lstPRO229_3 = db.SP_IPI_PROTOCOL046_GET_LINES6_DATA(1, int.MaxValue, "", " HeaderId = " + objPRO225.HeaderId).ToList();

            ViewBag.lstPRO226 = lstPRO226;
            ViewBag.lstPRO227 = lstPRO227;
            ViewBag.lstPRO228 = lstPRO228;
            ViewBag.lstPRO229 = lstPRO229;
            ViewBag.lstPRO229_2 = lstPRO229_2;
            ViewBag.lstPRO229_3 = lstPRO229_3;
            #endregion
            return View(objPRO225);
        }

        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO225 PRO225)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO225.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO225 objPRO225 = db.PRO225.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO225.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO225.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data

                        objPRO225.ProtocolNo = PRO225.ProtocolNo;
                        objPRO225.Ovality = PRO225.Ovality;
                        objPRO225.OutBy = PRO225.OutBy;
                        objPRO225.ReqTotalHeight = PRO225.ReqTotalHeight;
                        objPRO225.ReqCircBottom = PRO225.ReqCircBottom;
                        objPRO225.ActCircBottom = PRO225.ActCircBottom;
                        objPRO225.EvalueTemplateInspection = PRO225.EvalueTemplateInspection;
                        objPRO225.ReqChordLengthBottom = PRO225.ReqChordLengthBottom;
                        objPRO225.ActChordLengthBottom = PRO225.ActChordLengthBottom;
                        objPRO225.ReqRadiusBottom = PRO225.ReqRadiusBottom;
                        objPRO225.ActRadiusBottom = PRO225.ActRadiusBottom;
                        objPRO225.ReqGapBottom = PRO225.ReqGapBottom;
                        objPRO225.ActGapBottom = PRO225.ActGapBottom;
                        objPRO225.ProfileMeasurement = PRO225.ProfileMeasurement;
                        objPRO225.ReqAllowedOffset = PRO225.ReqAllowedOffset;
                        objPRO225.ReqOverCrowing = PRO225.ReqOverCrowing;
                        objPRO225.ActOverCrowing = PRO225.ActOverCrowing;
                        objPRO225.ReqUnderCrowing = PRO225.ReqUnderCrowing;
                        objPRO225.ActUnderCrowing = PRO225.ActUnderCrowing;
                        objPRO225.ReqMinThickness = PRO225.ReqMinThickness;
                        objPRO225.ActMinThickness = PRO225.ActMinThickness;
                        objPRO225.CheckPoint1 = PRO225.CheckPoint1;
                        objPRO225.CheckPoint2 = PRO225.CheckPoint2;
                        objPRO225.CheckPoint2_2 = PRO225.CheckPoint2_2;
                        objPRO225.CheckPoint4 = PRO225.CheckPoint4;
                        objPRO225.CheckPoint5 = PRO225.CheckPoint5;
                        objPRO225.CheckPoint6 = PRO225.CheckPoint6;
                        objPRO225.QCRemarks = PRO225.QCRemarks;
                        objPRO225.Result = PRO225.Result;
                        objPRO225.ReqWidth = PRO225.ReqWidth;
                        objPRO225.ReqDepth = PRO225.ReqDepth;
                        objPRO225.AllowedOffset = PRO225.AllowedOffset;
                        objPRO225.Note1 = PRO225.Note1;
                        objPRO225.Note2 = PRO225.Note2;
                        objPRO225.Note3 = PRO225.Note3;

                        objPRO225.MinGapAtKnuckleAreaUsingTemplate = PRO225.MinGapAtKnuckleAreaUsingTemplate;
                        objPRO225.MaxGapAtKnuckleAreaUsingTemplate = PRO225.MaxGapAtKnuckleAreaUsingTemplate;

                        objPRO225.ReqStraightFace = PRO225.ReqStraightFace;
                        objPRO225.ActStraightFace = PRO225.ActStraightFace;
                        objPRO225.ReqCrownRadius = PRO225.ReqCrownRadius;
                        objPRO225.ActCrownRadius = PRO225.ActCrownRadius;
                        objPRO225.ReqKnuckleRadius = PRO225.ReqKnuckleRadius;
                        objPRO225.ActKnuckleRadius = PRO225.ActKnuckleRadius;


                        objPRO225.EditedBy = objClsLoginInfo.UserName;
                        objPRO225.EditedOn = DateTime.Now;


                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO225.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO225 objPRO225 = db.PRO225.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO225 != null && string.IsNullOrWhiteSpace(objPRO225.ProtocolNo))
                    {
                        db.PRO225.Remove(objPRO225);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO225.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO226> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO226> lstAddPRO226 = new List<PRO226>();
                List<PRO226> lstDeletePRO226 = new List<PRO226>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO226 obj = db.PRO226.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.Orientation = item.Orientation;
                                obj.Actual = item.Actual;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO226 obj = new PRO226();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.Orientation))
                            {
                                obj.Orientation = item.Orientation;
                                obj.Actual = item.Actual;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO226.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO226.Count > 0)
                    {
                        db.PRO226.AddRange(lstAddPRO226);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO226 = db.PRO226.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO226.Count > 0)
                    {
                        db.PRO226.RemoveRange(lstDeletePRO226);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO226 = db.PRO226.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO226.Count > 0)
                    {
                        db.PRO226.RemoveRange(lstDeletePRO226);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2(List<PRO227> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO227> lstAddPRO227 = new List<PRO227>();
                List<PRO227> lstDeletePRO227 = new List<PRO227>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO227 obj = db.PRO227.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.SeamNo = item.SeamNo;
                                obj.ReqOrien = item.ReqOrien;
                                obj.ReqArcLength = item.ReqArcLength;
                                obj.ActArcLength = item.ActArcLength;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO227 obj = new PRO227();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.SeamNo))
                            {
                                obj.SeamNo = item.SeamNo;
                                obj.ReqOrien = item.ReqOrien;
                                obj.ReqArcLength = item.ReqArcLength;
                                obj.ActArcLength = item.ActArcLength;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO227.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO227.Count > 0)
                    {
                        db.PRO227.AddRange(lstAddPRO227);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO227 = db.PRO227.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO227.Count > 0)
                    {
                        db.PRO227.RemoveRange(lstDeletePRO227);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO227 = db.PRO227.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO227.Count > 0)
                    {
                        db.PRO227.RemoveRange(lstDeletePRO227);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3(List<PRO228> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO228> lstAddPRO228 = new List<PRO228>();
                List<PRO228> lstDeletePRO228 = new List<PRO228>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO228 obj = db.PRO228.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.TotalHeight1 = item.TotalHeight1;
                                obj.TotalHeight2 = item.TotalHeight2;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO228 obj = new PRO228();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.TotalHeight1))
                            {
                                obj.TotalHeight1 = item.TotalHeight1;
                                obj.TotalHeight2 = item.TotalHeight2;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO228.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO228.Count > 0)
                    {
                        db.PRO228.AddRange(lstAddPRO228);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO228 = db.PRO228.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO228.Count > 0)
                    {
                        db.PRO228.RemoveRange(lstDeletePRO228);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO228 = db.PRO228.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO228.Count > 0)
                    {
                        db.PRO228.RemoveRange(lstDeletePRO228);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line4 details
        [HttpPost]
        public JsonResult SaveProtocolLine4(List<PRO229> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO229> lstAddPRO229 = new List<PRO229>();
                List<PRO229> lstDeletePRO229 = new List<PRO229>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO229 obj = db.PRO229.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.SeamNo = item.SeamNo;
                                obj.TopPeakIn = item.TopPeakIn;
                                obj.TopPeakOut = item.TopPeakOut;
                                obj.MidPeakIn = item.MidPeakIn;
                                obj.MidPeakOut = item.MidPeakOut;
                                obj.BottonPeakIn = item.BottonPeakIn;
                                obj.BottonPeakOut = item.BottonPeakOut;
                                obj.ActMin = item.ActMin;
                                obj.ActMax = item.ActMax;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO229 obj = new PRO229();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.SeamNo))
                            {
                                obj.SeamNo = item.SeamNo;
                                obj.TopPeakIn = item.TopPeakIn;
                                obj.TopPeakOut = item.TopPeakOut;
                                obj.MidPeakIn = item.MidPeakIn;
                                obj.MidPeakOut = item.MidPeakOut;
                                obj.BottonPeakIn = item.BottonPeakIn;
                                obj.BottonPeakOut = item.BottonPeakOut;
                                obj.ActMin = item.ActMin;
                                obj.ActMax = item.ActMax;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO229.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO229.Count > 0)
                    {
                        db.PRO229.AddRange(lstAddPRO229);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO229 = db.PRO229.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO229.Count > 0)
                    {
                        db.PRO229.RemoveRange(lstDeletePRO229);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO229 = db.PRO229.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO229.Count > 0)
                    {
                        db.PRO229.RemoveRange(lstDeletePRO229);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        //#region Line5 details
        //[HttpPost]
        //public JsonResult SaveProtocolLine5(List<PRO229_2> lst, int HeaderId)
        //{
        //    clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
        //    try
        //    {
        //        List<PRO229_2> lstAddPRO229_2 = new List<PRO229_2>();
        //        List<PRO229_2> lstDeletePRO229_2 = new List<PRO229_2>();
        //        if (lst != null)
        //        {
        //            foreach (var item in lst)
        //            {
        //                if (item.LineId > 0)
        //                {
        //                    PRO229_2 obj = db.PRO229_2.Where(x => x.LineId == item.LineId).FirstOrDefault();
        //                    if (obj != null)
        //                    {
        //                        obj.ActMin = item.ActMin;
        //                        obj.ActMax = item.ActMax;
        //                        obj.EditedBy = objClsLoginInfo.UserName;
        //                        obj.EditedOn = DateTime.Now;
        //                    }
        //                }
        //                else
        //                {
        //                    PRO229_2 obj = new PRO229_2();
        //                    obj.HeaderId = item.HeaderId;
        //                    if (!string.IsNullOrWhiteSpace(item.ActMin))
        //                    {
        //                        obj.ActMin = item.ActMin;
        //                        obj.ActMax = item.ActMax;
        //                        obj.EditedBy = objClsLoginInfo.UserName;
        //                        obj.EditedOn = DateTime.Now;
        //                        obj.CreatedBy = objClsLoginInfo.UserName;
        //                        obj.CreatedOn = DateTime.Now;
        //                        lstAddPRO229_2.Add(obj);
        //                    }
        //                }
        //            }
        //            if (lstAddPRO229_2.Count > 0)
        //            {
        //                db.PRO229_2.AddRange(lstAddPRO229_2);
        //            }

        //            var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
        //            var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

        //            lstDeletePRO229_2 = db.PRO229_2.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
        //            if (lstDeletePRO229_2.Count > 0)
        //            {
        //                db.PRO229_2.RemoveRange(lstDeletePRO229_2);
        //            }
        //            db.SaveChanges();
        //        }
        //        else
        //        {
        //            lstDeletePRO229_2 = db.PRO229_2.Where(x => x.HeaderId == HeaderId).ToList();
        //            if (lstDeletePRO229_2.Count > 0)
        //            {
        //                db.PRO229_2.RemoveRange(lstDeletePRO229_2);
        //            }
        //            db.SaveChanges();
        //        }
        //        objResponseMsg.Key = true;
        //    }
        //    catch (Exception ex)
        //    {
        //        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        //        objResponseMsg.Key = false;
        //        objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
        //    }
        //    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        //}
        //#endregion

        #region Line6 details
        [HttpPost]
        public JsonResult SaveProtocolLine6(List<PRO229_3> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO229_3> lstAddPRO229_3 = new List<PRO229_3>();
                List<PRO229_3> lstDeletePRO229_3 = new List<PRO229_3>();

                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO229_3 obj = db.PRO229_3.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        PRO229_3 objFecthSeam = new PRO229_3();
                        if (obj == null)
                        {
                            obj = new PRO229_3();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        objFecthSeam = db.PRO229_3.Where(x => x.SeamNo == item.SeamNo && x.HeaderId == item.HeaderId).FirstOrDefault();
                        if (objFecthSeam != null)
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                            //obj.Depth = objFecthSeam.Depth;
                            //obj.Width = objFecthSeam.Width;
                            //obj.Offset = objFecthSeam.Offset;
                        }
                        else
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                            //obj.Depth = item.Depth;
                            //obj.Width = item.Width;
                            //obj.Offset = item.Offset;
                        }
                        obj.Depth = item.Depth;
                        obj.Width = item.Width;
                        obj.Offset = item.Offset;
                        obj.SpotNo = item.SpotNo;
                        obj.SpotNoClad = item.SpotNoClad;
                        if (isAdded)
                        {
                            lstAddPRO229_3.Add(obj);
                        }
                    }
                }
                if (lstAddPRO229_3.Count > 0)
                {
                    db.PRO229_3.AddRange(lstAddPRO229_3);
                }
                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRO229_3 = db.PRO229_3.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRO229_3 = db.PRO229_3.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRO229_3.Count > 0)
                {
                    db.PRO229_3.RemoveRange(lstDeletePRO229_3);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

        #region Linkage View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL225 objPRL225 = new PRL225();
            objPRL225 = db.PRL225.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL225 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL225.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;

                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL226> lstPRL226 = db.PRL226.Where(x => x.HeaderId == objPRL225.HeaderId).ToList();
            List<PRL227> lstPRL227 = db.PRL227.Where(x => x.HeaderId == objPRL225.HeaderId).ToList();
            List<PRL228> lstPRL228 = db.PRL228.Where(x => x.HeaderId == objPRL225.HeaderId).ToList();
            List<PRL229> lstPRL229 = db.PRL229.Where(x => x.HeaderId == objPRL225.HeaderId).ToList();
            List<PRL229_2> lstPRL229_2 = db.PRL229_2.Where(x => x.HeaderId == objPRL225.HeaderId).ToList();
            List<SP_IPI_PROTOCOL046_LINKAGE_GET_LINES6_DATA_Result> lstPRL229_3 = db.SP_IPI_PROTOCOL046_LINKAGE_GET_LINES6_DATA(1, int.MaxValue, "", " HeaderId = " + objPRL225.HeaderId).ToList();

            ViewBag.lstPRL226 = lstPRL226;
            ViewBag.lstPRL227 = lstPRL227;
            ViewBag.lstPRL228 = lstPRL228;
            ViewBag.lstPRL229 = lstPRL229;
            ViewBag.lstPRL229_2 = lstPRL229_2;
            ViewBag.lstPRL229_3 = lstPRL229_3;

            #endregion
            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.lstDrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL225.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL225.ActFilledBy) && (objPRL225.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL225.Project).ToList();
                            ViewBag.lstDrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.lstDrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL225);
        }

        [HttpPost]
        public ActionResult LinkageMobile(int? id, int? m, string APIRequestFrom = "", string UserName = "")
        {
            string ProjectDesc = "";
            string ManufacturingCode = "";
            string AccessRole = "";
            bool isEditable = false;
            var DrawingNo = new List<string>();
            bool isQCRemarkEditable = false;

            PRL225 objPRL225 = new PRL225();

            db.Configuration.ProxyCreationEnabled = false;
            objPRL225 = db.PRL225.Where(i => i.HeaderId == id).FirstOrDefault();
            db.Entry(objPRL225).State = System.Data.Entity.EntityState.Detached;

            NDEModels objNDEModels = new NDEModels();
            if (objPRL225 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL225.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;

                }

            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
            AccessRole = objUserRoleAccessDetails.UserDesignation;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            var YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            var LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL226> lstPRL226 = db.PRL226.Where(x => x.HeaderId == objPRL225.HeaderId).ToList();
            List<PRL227> lstPRL227 = db.PRL227.Where(x => x.HeaderId == objPRL225.HeaderId).ToList();
            List<PRL228> lstPRL228 = db.PRL228.Where(x => x.HeaderId == objPRL225.HeaderId).ToList();
            List<PRL229> lstPRL229 = db.PRL229.Where(x => x.HeaderId == objPRL225.HeaderId).ToList();
            List<PRL229_2> lstPRL229_2 = db.PRL229_2.Where(x => x.HeaderId == objPRL225.HeaderId).ToList();
            List<SP_IPI_PROTOCOL046_LINKAGE_GET_LINES6_DATA_Result> lstPRL229_3 = db.SP_IPI_PROTOCOL046_LINKAGE_GET_LINES6_DATA(1, int.MaxValue, "", " HeaderId = " + objPRL225.HeaderId).ToList();

            #endregion
            try
            {
                isEditable = false;
                isQCRemarkEditable = false;
                if (m == 1)
                {
                    isEditable = true;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL225.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL225.ActFilledBy) && (objPRL225.RequestNo.HasValue))
                        {
                            isEditable = false;
                            isQCRemarkEditable = true;
                        }
                        else
                        {
                            DrawingNo = (new GeneralController()).GetPLMDrawingNumber(objPRL225.Project).ToList();
                        }
                    }
                }

                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    ProjectDesc = ProjectDesc,
                    ManufacturingCode = ManufacturingCode,
                    isEditable = isEditable,
                    isQCRemarkEditable = isQCRemarkEditable,
                    DrawingNo = DrawingNo,
                    AccessRole = AccessRole,
                    LeftRight = LeftRight,
                    YesNoEnum = YesNoEnum,
                    YesNAEnum = YesNAEnum,
                    OutsideInside = OutsideInside,

                    objPRL225 = objPRL225,

                    lstPRL226 = lstPRL226,
                    lstPRL227 = lstPRL227,
                    lstPRL228 = lstPRL228,
                    lstPRL229 = lstPRL229,
                    lstPRL229_2 = lstPRL229_2,
                    lstPRL229_3 = lstPRL229_3

                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.lstDrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();

                return Json(new
                {
                    res = "0",
                    msg = ex.Message,
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL225 PRL225, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = PRL225.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL225 objPRL225 = db.PRL225.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    #region Save Data

                    objPRL225.DrawingRevisionNo = PRL225.DrawingRevisionNo;
                    objPRL225.DrawingNo = PRL225.DrawingNo;
                    objPRL225.DCRNo = PRL225.DCRNo;
                    objPRL225.ProtocolNo = PRL225.ProtocolNo != null ? PRL225.ProtocolNo : "";

                    objPRL225.Ovality = PRL225.Ovality;
                    objPRL225.OutBy = PRL225.OutBy;
                    objPRL225.ReqTotalHeight = PRL225.ReqTotalHeight;
                    objPRL225.ReqCircBottom = PRL225.ReqCircBottom;
                    objPRL225.ActCircBottom = PRL225.ActCircBottom;
                    objPRL225.EvalueTemplateInspection = PRL225.EvalueTemplateInspection;
                    objPRL225.ReqChordLengthBottom = PRL225.ReqChordLengthBottom;
                    objPRL225.ActChordLengthBottom = PRL225.ActChordLengthBottom;
                    objPRL225.ReqRadiusBottom = PRL225.ReqRadiusBottom;
                    objPRL225.ActRadiusBottom = PRL225.ActRadiusBottom;
                    objPRL225.ReqGapBottom = PRL225.ReqGapBottom;
                    objPRL225.ActGapBottom = PRL225.ActGapBottom;
                    objPRL225.ProfileMeasurement = PRL225.ProfileMeasurement;
                    objPRL225.ReqAllowedOffset = PRL225.ReqAllowedOffset;
                    objPRL225.ReqOverCrowing = PRL225.ReqOverCrowing;
                    objPRL225.ActOverCrowing = PRL225.ActOverCrowing;
                    objPRL225.ReqUnderCrowing = PRL225.ReqUnderCrowing;
                    objPRL225.ActUnderCrowing = PRL225.ActUnderCrowing;
                    objPRL225.ReqMinThickness = PRL225.ReqMinThickness;
                    objPRL225.ActMinThickness = PRL225.ActMinThickness;
                    objPRL225.CheckPoint1 = PRL225.CheckPoint1;
                    objPRL225.CheckPoint2 = PRL225.CheckPoint2;
                    objPRL225.CheckPoint2_2 = PRL225.CheckPoint2_2;
                    objPRL225.CheckPoint4 = PRL225.CheckPoint4;
                    objPRL225.CheckPoint5 = PRL225.CheckPoint5;
                    objPRL225.CheckPoint6 = PRL225.CheckPoint6;
                    objPRL225.QCRemarks = PRL225.QCRemarks;
                    objPRL225.Result = PRL225.Result;
                    objPRL225.ReqWidth = PRL225.ReqWidth;
                    objPRL225.ReqDepth = PRL225.ReqDepth;
                    objPRL225.AllowedOffset = PRL225.AllowedOffset;
                    objPRL225.Note1 = PRL225.Note1;
                    objPRL225.Note2 = PRL225.Note2;
                    objPRL225.Note3 = PRL225.Note3;

                    objPRL225.MinGapAtKnuckleAreaUsingTemplate = PRL225.MinGapAtKnuckleAreaUsingTemplate;
                    objPRL225.MaxGapAtKnuckleAreaUsingTemplate = PRL225.MaxGapAtKnuckleAreaUsingTemplate;
                    objPRL225.ReqStraightFace = PRL225.ReqStraightFace;
                    objPRL225.ActStraightFace = PRL225.ActStraightFace;
                    objPRL225.ReqCrownRadius = PRL225.ReqCrownRadius;
                    objPRL225.ActCrownRadius = PRL225.ActCrownRadius;
                    objPRL225.ReqKnuckleRadius = PRL225.ReqKnuckleRadius;
                    objPRL225.ActKnuckleRadius = PRL225.ActKnuckleRadius;


                    objPRL225.EditedBy = UserName;
                    objPRL225.EditedOn = DateTime.Now;

                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL225.ActFilledBy = UserName;
                            objPRL225.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL225.ReqFilledBy = UserName;
                            objPRL225.ReqFilledOn = DateTime.Now;
                        }
                    }
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL225.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL225 objPRL225 = db.PRL225.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL225 != null && string.IsNullOrWhiteSpace(objPRL225.ProtocolNo))
                    {
                        db.PRL225.Remove(objPRL225);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL225.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region Line1 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL226> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL226> lstAddPRL226 = new List<PRL226>();
                List<PRL226> lstDeletePRL226 = new List<PRL226>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRL226 obj = db.PRL226.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.Orientation = item.Orientation;
                                obj.Actual = item.Actual;
                                obj.EditedBy = UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRL226 obj = new PRL226();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.Orientation))
                            {
                                obj.Orientation = item.Orientation;
                                obj.Actual = item.Actual;
                                obj.CreatedBy = UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRL226.Add(obj);
                            }
                        }
                    }
                }
                if (lstAddPRL226.Count > 0)
                {
                    db.PRL226.AddRange(lstAddPRL226);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL226 = db.PRL226.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL226 = db.PRL226.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL226.Count > 0)
                {
                    db.PRL226.RemoveRange(lstDeletePRL226);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine2Linkage(List<PRL227> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL227> lstAddPRL227 = new List<PRL227>();
                List<PRL227> lstDeletePRL227 = new List<PRL227>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL227 obj = db.PRL227.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL227();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SeamNo = item.SeamNo;
                        obj.ReqOrien = item.ReqOrien;
                        obj.ReqArcLength = item.ReqArcLength;
                        obj.ActArcLength = item.ActArcLength;
                        if (isAdded)
                        {
                            lstAddPRL227.Add(obj);
                        }
                    }
                }
                if (lstAddPRL227.Count > 0)
                {
                    db.PRL227.AddRange(lstAddPRL227);
                }
                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL227 = db.PRL227.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL227 = db.PRL227.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL227.Count > 0)
                {
                    db.PRL227.RemoveRange(lstDeletePRL227);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line3 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine3Linkage(List<PRL228> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL228> lstAddPRL228 = new List<PRL228>();
                List<PRL228> lstDeletePRL228 = new List<PRL228>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL228 obj = db.PRL228.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        else
                        {
                            obj = new PRL228();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }

                        if (!string.IsNullOrWhiteSpace(item.TotalHeight1))
                        {
                            obj.TotalHeight1 = item.TotalHeight1;
                            obj.TotalHeight2 = item.TotalHeight2;
                        }
                        if (isAdded)
                        {
                            lstAddPRL228.Add(obj);
                        }
                    }
                    if (lstAddPRL228.Count > 0)
                    {
                        db.PRL228.AddRange(lstAddPRL228);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL228 = db.PRL228.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL228 = db.PRL228.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL228.Count > 0)
                {
                    db.PRL228.RemoveRange(lstDeletePRL228);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line4 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine4Linkage(List<PRL229> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL229> lstAddPRL229 = new List<PRL229>();
                List<PRL229> lstDeletePRL229 = new List<PRL229>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL229 obj = db.PRL229.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL229();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SeamNo = item.SeamNo;
                        obj.TopPeakIn = item.TopPeakIn;
                        obj.TopPeakOut = item.TopPeakOut;
                        obj.MidPeakIn = item.MidPeakIn;
                        obj.MidPeakOut = item.MidPeakOut;
                        obj.BottonPeakIn = item.BottonPeakIn;
                        obj.BottonPeakOut = item.BottonPeakOut;
                        obj.ActMin = item.ActMin;
                        obj.ActMax = item.ActMax;
                        if (isAdded)
                        {
                            lstAddPRL229.Add(obj);
                        }
                    }

                    if (lstAddPRL229.Count > 0)
                    {
                        db.PRL229.AddRange(lstAddPRL229);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL229 = db.PRL229.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL229 = db.PRL229.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL229.Count > 0)
                {
                    db.PRL229.RemoveRange(lstDeletePRL229);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        //#region Line5 Linkage
        //[HttpPost]
        //public JsonResult SaveProtocolLine5Linkage(List<PRL229_2> lst, int HeaderId)
        //{
        //    clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
        //    try
        //    {
        //        List<PRL229_2> lstAddPRL229_2 = new List<PRL229_2>();
        //        List<PRL229_2> lstDeletePRL229_2 = new List<PRL229_2>();
        //        if (lst != null)
        //        {
        //            foreach (var item in lst)
        //            {
        //                bool isAdded = false;
        //                PRL229_2 obj = db.PRL229_2.Where(x => x.LineId == item.LineId).FirstOrDefault();
        //                if (obj == null)
        //                {
        //                    obj = new PRL229_2();
        //                    obj.HeaderId = item.HeaderId;
        //                    obj.CreatedBy = UserName;
        //                    obj.CreatedOn = DateTime.Now;
        //                    isAdded = true;
        //                }
        //                else
        //                {
        //                    obj.EditedBy = UserName;
        //                    obj.EditedOn = DateTime.Now;
        //                }
        //                obj.ActMin = item.ActMin;
        //                obj.ActMax = item.ActMax;

        //                if (isAdded)
        //                {
        //                    lstAddPRL229_2.Add(obj);
        //                }
        //            }

        //            if (lstAddPRL229_2.Count > 0)
        //            {
        //                db.PRL229_2.AddRange(lstAddPRL229_2);
        //            }

        //            var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
        //            lstDeletePRL229_2 = db.PRL229_2.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
        //        }
        //        else
        //        {
        //            lstDeletePRL229_2 = db.PRL229_2.Where(x => x.HeaderId == HeaderId).ToList();
        //        }
        //        if (lstDeletePRL229_2.Count > 0)
        //        {
        //            db.PRL229_2.RemoveRange(lstDeletePRL229_2);
        //        }
        //        db.SaveChanges();
        //        objResponseMsg.Key = true;
        //    }
        //    catch (Exception ex)
        //    {
        //        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        //        objResponseMsg.Key = false;
        //        objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
        //    }
        //    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        //}
        //#endregion

        #region Line6 Linkage

        [HttpPost]
        public JsonResult SaveProtocolLine6Linkage(List<PRL229_3> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL229_3> lstAddPRL229_3 = new List<PRL229_3>();
                List<PRL229_3> lstDeletePRL229_3 = new List<PRL229_3>();

                if (lst != null)
                {

                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL229_3 obj = db.PRL229_3.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        PRL229_3 objFecthSeam = new PRL229_3();
                        if (obj == null)
                        {
                            obj = new PRL229_3();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        objFecthSeam = db.PRL229_3.Where(x => x.SeamNo == item.SeamNo && x.HeaderId == item.HeaderId).FirstOrDefault();
                        if (objFecthSeam != null)
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                            //obj.Depth = objFecthSeam.Depth;
                            //obj.Width = objFecthSeam.Width;
                            //obj.Offset = objFecthSeam.Offset;
                        }
                        else
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                            //obj.Depth = item.Depth;
                            //obj.Width = item.Width;
                            //obj.Offset = item.Offset;
                        }
                        obj.Depth = item.Depth;
                        obj.Width = item.Width;
                        obj.Offset = item.Offset;
                        obj.SpotNo = item.SpotNo;
                        obj.SpotNoClad = item.SpotNoClad;
                        if (isAdded)
                        {
                            lstAddPRL229_3.Add(obj);
                        }
                    }
                    if (lstAddPRL229_3.Count > 0)
                    {
                        db.PRL229_3.AddRange(lstAddPRL229_3);
                    }
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL229_3 = db.PRL229_3.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL229_3 = db.PRL229_3.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL229_3.Count > 0)
                {
                    db.PRL229_3.RemoveRange(lstDeletePRL229_3);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion
    }
}