﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol071Controller : clsBase
    {
        // GET: PROTOCOL/Protocol071
        string ControllerURL = "/PROTOCOL/Protocol071/";
        string Title = "HYDROSTATIC TEST REPORT (PRESSURE TEST)";

        #region Details View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO350 objPRO350 = new PRO350();
            if (!id.HasValue)
            {
                try
                {
                    objPRO350.ProtocolNo = string.Empty;
                    objPRO350.CreatedBy = objClsLoginInfo.UserName;
                    objPRO350.CreatedOn = DateTime.Now;

                    #region
                    objPRO350.QCRemarks = "NO PRESSURE DROP AND NO LEAKAGE OBSERVED.";
                    #endregion

                    #region  LOCATION | PRO352
                    objPRO350.PRO352.Add(new PRO352
                    {
                        Location = "TOP",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO350.PRO352.Add(new PRO352
                    {
                        Location = "MIDDLE",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO350.PRO352.Add(new PRO352
                    {
                        Location = "BOTTOM",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });

                    #endregion

                    db.PRO350.Add(objPRO350);
                    db.SaveChanges();
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO350.HeaderId;
            }
            else
            {
                objPRO350 = db.PRO350.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstyesnona = clsImplementationEnum.getYesNoNA().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNoNAEnum = lstyesnona.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO351> lstPRO351 = db.PRO351.Where(x => x.HeaderId == objPRO350.HeaderId).ToList();
            List<PRO352> lstPRO352 = db.PRO352.Where(x => x.HeaderId == objPRO350.HeaderId).ToList();

            ViewBag.lstPRO351 = lstPRO351;
            ViewBag.lstPRO352 = lstPRO352;

            #endregion

            return View(objPRO350);
        }

        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO350 PRO350)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO350.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO350 objPRO350 = db.PRO350.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO350.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO350.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data

                        objPRO350.ProtocolNo = PRO350.ProtocolNo;
                        objPRO350.ProcedureNo = PRO350.ProcedureNo;
                        objPRO350.TestDescription = PRO350.TestDescription;
                        objPRO350.DateOfTest = PRO350.DateOfTest;
                        objPRO350.TestPosition = PRO350.TestPosition;
                        objPRO350.TestMedium = PRO350.TestMedium;
                        objPRO350.ReqChlorideContentOfWater = PRO350.ReqChlorideContentOfWater;
                        objPRO350.ActChlorideContentOfWater = PRO350.ActChlorideContentOfWater;
                        objPRO350.ReqTestPressure = PRO350.ReqTestPressure;
                        objPRO350.ActTestPressure = PRO350.ActTestPressure;
                        objPRO350.ReqHoldingTime = PRO350.ReqHoldingTime;
                        objPRO350.ActHoldingTime = PRO350.ActHoldingTime;
                        objPRO350.ReqTestTemprature = PRO350.ReqTestTemprature;
                        objPRO350.ActTestTemprature = PRO350.ActTestTemprature;
                        objPRO350.OutesideCircumferenceInMM = PRO350.OutesideCircumferenceInMM;
                        objPRO350.Graph = PRO350.Graph;
                        objPRO350.QCRemarks = PRO350.QCRemarks;
                        objPRO350.Result = PRO350.Result;

                        objPRO350.EditedBy = objClsLoginInfo.UserName;
                        objPRO350.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO350.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO350 objPRO350 = db.PRO350.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO350 != null && string.IsNullOrWhiteSpace(objPRO350.ProtocolNo))
                    {
                        db.PRO350.Remove(objPRO350);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO350.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO351> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO351> lstAddPRO351 = new List<PRO351>();
                List<PRO351> lstDeletePRO351 = new List<PRO351>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO351 obj = db.PRO351.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.GaugeSerialNumber = item.GaugeSerialNumber;
                                obj.GaugeCelebrationDueDate = item.GaugeCelebrationDueDate;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO351 obj = new PRO351();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.GaugeSerialNumber))
                            {
                                obj.GaugeSerialNumber = item.GaugeSerialNumber;
                                obj.GaugeCelebrationDueDate = item.GaugeCelebrationDueDate;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO351.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO351.Count > 0)
                    {
                        db.PRO351.AddRange(lstAddPRO351);
                    }



                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();



                    lstDeletePRO351 = db.PRO351.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO351.Count > 0)
                    {
                        db.PRO351.RemoveRange(lstDeletePRO351);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO351 = db.PRO351.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO351.Count > 0)
                    {
                        db.PRO351.RemoveRange(lstDeletePRO351);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2(List<PRO352> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO352> lstAddPRO352 = new List<PRO352>();
                List<PRO352> lstDeletePRO352 = new List<PRO352>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO352 obj = db.PRO352.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO352();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Location = item.Location;
                        obj.BeforeHydroTest = item.BeforeHydroTest;
                        obj.DuringHydroTest = item.DuringHydroTest;
                        obj.AfterHydroTest = item.AfterHydroTest;


                        if (isAdded)
                        {
                            lstAddPRO352.Add(obj);
                        }
                    }
                    if (lstAddPRO352.Count > 0)
                    {
                        db.PRO352.AddRange(lstAddPRO352);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO352 = db.PRO352.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO352.Count > 0)
                    {
                        db.PRO352.RemoveRange(lstDeletePRO352);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO352 = db.PRO352.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO352.Count > 0)
                    {
                        db.PRO352.RemoveRange(lstDeletePRO352);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #endregion

        #region Linkage View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)

        {

            PRL350 objPRL350 = new PRL350();
            objPRL350 = db.PRL350.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL350 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL350.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;

                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstyesnona = clsImplementationEnum.getYesNoNA().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNoNAEnum = lstyesnona.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL351> lstPRL351 = db.PRL351.Where(x => x.HeaderId == objPRL350.HeaderId).ToList();
            List<PRL352> lstPRL352 = db.PRL352.Where(x => x.HeaderId == objPRL350.HeaderId).ToList();



            ViewBag.lstPRL351 = lstPRL351;
            ViewBag.lstPRL352 = lstPRL352;


            #endregion

            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.DrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL350.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL350.ActFilledBy) && (objPRL350.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL350.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL350);
        }

        [HttpPost]
        public ActionResult LinkageMobile(int? id, int? m, string APIRequestFrom = "", string UserName = "")
        {
            string ProjectDesc = "";
            string ManufacturingCode = "";
            string AccessRole = "";
            bool isEditable = false;
            var DrawingNo = new List<string>();
            bool isQCRemarkEditable = false;

            PRL350 objPRL350 = new PRL350();
            db.Configuration.ProxyCreationEnabled = false;
            objPRL350 = db.PRL350.Where(i => i.HeaderId == id).FirstOrDefault();
            db.Entry(objPRL350).State = System.Data.Entity.EntityState.Detached;
            NDEModels objNDEModels = new NDEModels();
            if (objPRL350 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL350.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }   
            }
           
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
            AccessRole = objUserRoleAccessDetails.UserDesignation;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstyesnona = clsImplementationEnum.getYesNoNA().ToList();

            var YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var YesNoNAEnum = lstyesnona.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL351> lstPRL351 = db.PRL351.Where(x => x.HeaderId == objPRL350.HeaderId).ToList();
            List<PRL352> lstPRL352 = db.PRL352.Where(x => x.HeaderId == objPRL350.HeaderId).ToList();

            #endregion
            try
            {
                isEditable = false;
                isQCRemarkEditable = false;
                if (m == 1)
                {
                    isEditable = true;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL350.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL350.ActFilledBy) && (objPRL350.RequestNo.HasValue))
                        {
                            isEditable = false;
                            isQCRemarkEditable = true;
                        }
                        else
                        {
                            DrawingNo = (new GeneralController()).GetPLMDrawingNumber(objPRL350.Project).ToList();
                        }
                    }
                }

                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    ProjectDesc = ProjectDesc,
                    ManufacturingCode = ManufacturingCode,
                    isEditable = isEditable,
                    isQCRemarkEditable = isQCRemarkEditable,
                    DrawingNo = DrawingNo,
                    AccessRole = AccessRole,

                    YesNoEnum = YesNoEnum,
                    YesNAEnum = YesNAEnum,
                    YesNoNAEnum = YesNoNAEnum,
                   
                    objPRL350 = objPRL350,

                    lstPRL351 = lstPRL351,
                    lstPRL352 = lstPRL352

                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                return Json(new
                {
                    res = "0",
                    msg = ex.Message,
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL350 prl350, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = prl350.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL350 objPRL350 = db.PRL350.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL350.DrawingRevisionNo = prl350.DrawingRevisionNo;
                    objPRL350.DrawingNo = prl350.DrawingNo;
                    objPRL350.DCRNo = prl350.DCRNo;
                    objPRL350.ProtocolNo = prl350.ProtocolNo;

                    objPRL350.ProcedureNo = prl350.ProcedureNo;
                    objPRL350.TestDescription = prl350.TestDescription;
                    objPRL350.DateOfTest = prl350.DateOfTest;
                    objPRL350.TestPosition = prl350.TestPosition;
                    objPRL350.TestMedium = prl350.TestMedium;
                    objPRL350.ReqChlorideContentOfWater = prl350.ReqChlorideContentOfWater;
                    objPRL350.ActChlorideContentOfWater = prl350.ActChlorideContentOfWater;
                    objPRL350.ReqTestPressure = prl350.ReqTestPressure;
                    objPRL350.ActTestPressure = prl350.ActTestPressure;
                    objPRL350.ReqHoldingTime = prl350.ReqHoldingTime;
                    objPRL350.ActHoldingTime = prl350.ActHoldingTime;
                    objPRL350.ReqTestTemprature = prl350.ReqTestTemprature;
                    objPRL350.ActTestTemprature = prl350.ActTestTemprature;
                    objPRL350.OutesideCircumferenceInMM = prl350.OutesideCircumferenceInMM;
                    objPRL350.Graph = prl350.Graph;
                    objPRL350.QCRemarks = prl350.QCRemarks;
                    objPRL350.Result = prl350.Result;



                    objPRL350.EditedBy = UserName;
                    objPRL350.EditedOn = DateTime.Now;
                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL350.ActFilledBy = UserName;
                            objPRL350.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL350.ReqFilledBy = UserName;
                            objPRL350.ReqFilledOn = DateTime.Now;
                        }
                    }
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL350.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL350 objPRL350 = db.PRL350.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL350 != null && string.IsNullOrWhiteSpace(objPRL350.ProtocolNo))
                    {
                        db.PRL350.Remove(objPRL350);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL350.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL351> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL351> lstAddPRL351 = new List<PRL351>();
                List<PRL351> lstDeletePRL351 = new List<PRL351>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL351 obj = db.PRL351.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL351();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.GaugeSerialNumber = item.GaugeSerialNumber;
                        obj.GaugeCelebrationDueDate = item.GaugeCelebrationDueDate;
                        if (isAdded)
                        {
                            lstAddPRL351.Add(obj);
                        }
                    }
                    if (lstAddPRL351.Count > 0)
                    {
                        db.PRL351.AddRange(lstAddPRL351);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL351 = db.PRL351.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL351.Count > 0)
                    {
                        db.PRL351.RemoveRange(lstDeletePRL351);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL351 = db.PRL351.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL351.Count > 0)
                    {
                        db.PRL351.RemoveRange(lstDeletePRL351);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2Linkage(List<PRL352> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL352> lstAddPRL352 = new List<PRL352>();
                List<PRL352> lstDeletePRL352 = new List<PRL352>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL352 obj = db.PRL352.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL352();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Location = item.Location;
                        obj.BeforeHydroTest = item.BeforeHydroTest;
                        obj.DuringHydroTest = item.DuringHydroTest;
                        obj.AfterHydroTest = item.AfterHydroTest;

                        if (isAdded)
                        {
                            lstAddPRL352.Add(obj);
                        }
                    }
                    if (lstAddPRL352.Count > 0)
                    {
                        db.PRL352.AddRange(lstAddPRL352);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL352 = db.PRL352.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL352.Count > 0)
                    {
                        db.PRL352.RemoveRange(lstDeletePRL352);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL352 = db.PRL352.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL352.Count > 0)
                    {
                        db.PRL352.RemoveRange(lstDeletePRL352);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #endregion


    }
}