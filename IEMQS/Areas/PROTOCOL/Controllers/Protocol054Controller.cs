﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol054Controller : clsBase
    {
        // GET: PROTOCOL/Protocol054

        string ControllerURL = "/PROTOCOL/Protocol054/";
        string Title = "REPORT FOR SET-UP AND DIMENSION INSPECTION OF TSR";

        #region Details View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO265 objPRO265 = new PRO265();
            if (!id.HasValue)
            {
                try
                {
                    objPRO265.ProtocolNo = string.Empty;
                    objPRO265.CreatedBy = objClsLoginInfo.UserName;
                    objPRO265.CreatedOn = DateTime.Now;



                    #region OVALITY 
                    objPRO265.PRO266.Add(new PRO266
                    {
                        Orientation = "0°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO265.PRO266.Add(new PRO266
                    {
                        Orientation = "30°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO265.PRO266.Add(new PRO266
                    {
                        Orientation = "60°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO265.PRO266.Add(new PRO266
                    {
                        Orientation = "90°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO265.PRO266.Add(new PRO266
                    {
                        Orientation = "120°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO265.PRO266.Add(new PRO266
                    {
                        Orientation = "150°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO265.PRO266.Add(new PRO266
                    {
                        Orientation = "180°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO265.PRO266.Add(new PRO266
                    {
                        Orientation = "210°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO265.PRO266.Add(new PRO266
                    {
                        Orientation = "240°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO265.PRO266.Add(new PRO266
                    {
                        Orientation = "270°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO265.PRO266.Add(new PRO266
                    {
                        Orientation = "300°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO265.PRO266.Add(new PRO266
                    {
                        Orientation = "330°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });

                    #endregion







                    db.PRO265.Add(objPRO265);
                    db.SaveChanges();
                    // return View();

                }

                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO265.HeaderId;
            }
            else
            {
                objPRO265 = db.PRO265.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstElevationOfTSRFrom = new List<string> { "REF. LINE", "TAN LINE" };

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.ElevationOfTSRFrom = lstElevationOfTSRFrom.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();


            //  ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO266> lstPRO266 = db.PRO266.Where(x => x.HeaderId == objPRO265.HeaderId).ToList();
            List<PRO267> lstPRO267 = db.PRO267.Where(x => x.HeaderId == objPRO265.HeaderId).ToList();
            List<PRO268> lstPRO268 = db.PRO268.Where(x => x.HeaderId == objPRO265.HeaderId).ToList();




            ViewBag.lstPRO266 = lstPRO266;
            ViewBag.lstPRO267 = lstPRO267;
            ViewBag.lstPRO268 = lstPRO268;



            #endregion
            return View(objPRO265);

        }

        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO265 PRO265)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO265.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO265 objPRO265 = db.PRO265.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO265.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO265.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data

                        objPRO265.ProtocolNo = PRO265.ProtocolNo;
                        objPRO265.ElevationOfTSRFrom = PRO265.ElevationOfTSRFrom;
                        objPRO265.ReqElevationOfTSRFrom = PRO265.ReqElevationOfTSRFrom;
                        objPRO265.ReqTSRWidth = PRO265.ReqTSRWidth;
                        objPRO265.ReqTSRHeight = PRO265.ReqTSRHeight;
                        objPRO265.TSRID = PRO265.TSRID;
                        objPRO265.FlatnessIn = PRO265.FlatnessIn;
                        objPRO265.ReqTSRID = PRO265.ReqTSRID;
                        objPRO265.TolElevationOfTSRFrom = PRO265.TolElevationOfTSRFrom;
                        objPRO265.TolTSRWidth = PRO265.TolTSRWidth;
                        objPRO265.TolTSRHeight = PRO265.TolTSRHeight;
                        objPRO265.TolTSRID = PRO265.TolTSRID;
                        objPRO265.TolLevelnessOfTopFaceOfTSR = PRO265.TolLevelnessOfTopFaceOfTSR;
                        objPRO265.TolTiltingOfTSR = PRO265.TolTiltingOfTSR;
                        objPRO265.TolFlatnessIn = PRO265.TolFlatnessIn;
                        objPRO265.WEPSeamNo = PRO265.WEPSeamNo;
                        objPRO265.ReqRootGap = PRO265.ReqRootGap;
                        objPRO265.ReqRootFace = PRO265.ReqRootFace;
                        objPRO265.ReqOneSideWepAngle = PRO265.ReqOneSideWepAngle;
                        objPRO265.ReqOtherSideWepAngle = PRO265.ReqOtherSideWepAngle;
                        objPRO265.ActMaxRootGap = PRO265.ActMaxRootGap;
                        objPRO265.ActMaxRootFace = PRO265.ActMaxRootFace;
                        objPRO265.ActMaxOneSideWepAngle = PRO265.ActMaxOneSideWepAngle;
                        objPRO265.ActMaxOtherSideWepAngle = PRO265.ActMaxOtherSideWepAngle;
                        objPRO265.ActMinRootGap = PRO265.ActMinRootGap;
                        objPRO265.ActMinRootFace = PRO265.ActMinRootFace;
                        objPRO265.ActMinOneSideWepAngle = PRO265.ActMinOneSideWepAngle;
                        objPRO265.ActMinOtherSideWepAngle = PRO265.ActMinOtherSideWepAngle;
                        objPRO265.ReqRootGap1 = PRO265.ReqRootGap1;
                        objPRO265.ReqRootFace1 = PRO265.ReqRootFace1;
                        objPRO265.ReqTopSideWepAngle = PRO265.ReqTopSideWepAngle;
                        objPRO265.ReqBottomSideWepAngle = PRO265.ReqBottomSideWepAngle;
                        objPRO265.ReqOffset = PRO265.ReqOffset;
                        objPRO265.LevelnessOfTopFaceOfTSR = PRO265.LevelnessOfTopFaceOfTSR;
                        objPRO265.CheckPoint1 = PRO265.CheckPoint1;
                        objPRO265.CheckPoint2 = PRO265.CheckPoint2;
                        objPRO265.CheckPoint3 = PRO265.CheckPoint3;
                        objPRO265.CheckPoint4 = PRO265.CheckPoint4;
                        objPRO265.CheckPoint5 = PRO265.CheckPoint5;
                        objPRO265.CheckPoint6 = PRO265.CheckPoint6;
                        objPRO265.CheckPoint7 = PRO265.CheckPoint7;
                        objPRO265.CheckPoint9 = PRO265.CheckPoint9;
                        objPRO265.CheckPoint9_2 = PRO265.CheckPoint9_2;
                        objPRO265.QCRemarks = PRO265.QCRemarks;
                        objPRO265.Result = PRO265.Result;

                        objPRO265.EditedBy = objClsLoginInfo.UserName;
                        objPRO265.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO265.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO265 objPRO265 = db.PRO265.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO265 != null && string.IsNullOrWhiteSpace(objPRO265.ProtocolNo))
                    {
                        db.PRO265.Remove(objPRO265);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO265.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO266> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO266> lstAddPRO266 = new List<PRO266>();
                List<PRO266> lstDeletePRO266 = new List<PRO266>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO266 obj = db.PRO266.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {


                                obj.Orientation = item.Orientation;
                                obj.ActElevationOfTSRFrom = item.ActElevationOfTSRFrom;
                                obj.ActTSRWidth = item.ActTSRWidth;
                                obj.ActTSRHeight = item.ActTSRHeight;
                                obj.ActTSRID = item.ActTSRID;
                                //obj.LevelnessOfTopFaceOfTSR = item.LevelnessOfTopFaceOfTSR;
                                obj.ActTiltingOfTSR = item.ActTiltingOfTSR;
                                obj.ActFlatnessIn = item.ActFlatnessIn;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO266 obj = new PRO266();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.Orientation))
                            {
                                obj.Orientation = item.Orientation;
                                obj.ActElevationOfTSRFrom = item.ActElevationOfTSRFrom;
                                obj.ActTSRWidth = item.ActTSRWidth;
                                obj.ActTSRHeight = item.ActTSRHeight;
                                obj.ActTSRID = item.ActTSRID;
                                //obj.LevelnessOfTopFaceOfTSR = item.LevelnessOfTopFaceOfTSR;
                                obj.ActTiltingOfTSR = item.ActTiltingOfTSR;
                                obj.ActFlatnessIn = item.ActFlatnessIn;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO266.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO266.Count > 0)
                    {
                        db.PRO266.AddRange(lstAddPRO266);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO266 = db.PRO266.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO266.Count > 0)
                    {
                        db.PRO266.RemoveRange(lstDeletePRO266);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO266 = db.PRO266.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO266.Count > 0)
                    {
                        db.PRO266.RemoveRange(lstDeletePRO266);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2(List<PRO267> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO267> lstAddPRO267 = new List<PRO267>();
                List<PRO267> lstDeletePRO267 = new List<PRO267>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO267 obj = db.PRO267.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {

                                obj.SeamNo = item.SeamNo;
                                obj.ActMaxRootGap = item.ActMaxRootGap;
                                obj.ActMaxRootFace = item.ActMaxRootFace;
                                obj.ActMaxTopSideWepAngle = item.ActMaxTopSideWepAngle;
                                obj.ActMaxBottomSideWepAngle = item.ActMaxBottomSideWepAngle;
                                obj.ActMaxOffset = item.ActMaxOffset;
                                obj.ActMinRootGap = item.ActMinRootGap;
                                obj.ActMinRootFace = item.ActMinRootFace;
                                obj.ActMinTopSideWepAngle = item.ActMinTopSideWepAngle;
                                obj.ActMinBottomSideWepAngle = item.ActMinBottomSideWepAngle;
                                obj.ActMinOffset = item.ActMinOffset;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO267 obj = new PRO267();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.SeamNo))
                            {
                                obj.SeamNo = item.SeamNo;
                                obj.ActMaxRootGap = item.ActMaxRootGap;
                                obj.ActMaxRootFace = item.ActMaxRootFace;
                                obj.ActMaxTopSideWepAngle = item.ActMaxTopSideWepAngle;
                                obj.ActMaxBottomSideWepAngle = item.ActMaxBottomSideWepAngle;
                                obj.ActMaxOffset = item.ActMaxOffset;
                                obj.ActMinRootGap = item.ActMinRootGap;
                                obj.ActMinRootFace = item.ActMinRootFace;
                                obj.ActMinTopSideWepAngle = item.ActMinTopSideWepAngle;
                                obj.ActMinBottomSideWepAngle = item.ActMinBottomSideWepAngle;
                                obj.ActMinOffset = item.ActMinOffset;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO267.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO267.Count > 0)
                    {
                        db.PRO267.AddRange(lstAddPRO267);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO267 = db.PRO267.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO267.Count > 0)
                    {
                        db.PRO267.RemoveRange(lstDeletePRO267);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO267 = db.PRO267.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO267.Count > 0)
                    {
                        db.PRO267.RemoveRange(lstDeletePRO267);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3(List<PRO268> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO268> lstAddPRO268 = new List<PRO268>();
                List<PRO268> lstDeletePRO268 = new List<PRO268>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO268 obj = db.PRO268.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.IdentificationMarking = item.IdentificationMarking;

                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO268 obj = new PRO268();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.IdentificationMarking))
                            {
                                obj.IdentificationMarking = item.IdentificationMarking;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO268.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO268.Count > 0)
                    {
                        db.PRO268.AddRange(lstAddPRO268);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO268 = db.PRO268.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO268.Count > 0)
                    {
                        db.PRO268.RemoveRange(lstDeletePRO268);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO268 = db.PRO268.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO268.Count > 0)
                    {
                        db.PRO268.RemoveRange(lstDeletePRO268);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion
        #region Linkage View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL265 objPRL265 = new PRL265();
            objPRL265 = db.PRL265.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL265 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL265.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;

                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstElevationOfTSRFrom = new List<string> { "REF. LINE", "TAN LINE" };

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.ElevationOfTSRFrom = lstElevationOfTSRFrom.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();


            //  ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL266> lstPRL266 = db.PRL266.Where(x => x.HeaderId == objPRL265.HeaderId).ToList();
            List<PRL267> lstPRL267 = db.PRL267.Where(x => x.HeaderId == objPRL265.HeaderId).ToList();
            List<PRL268> lstPRL268 = db.PRL268.Where(x => x.HeaderId == objPRL265.HeaderId).ToList();




            ViewBag.lstPRL266 = lstPRL266;
            ViewBag.lstPRL267 = lstPRL267;
            ViewBag.lstPRL268 = lstPRL268;


            #endregion
            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.lstDrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL265.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL265.ActFilledBy) && (objPRL265.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL265.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.lstDrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL265);
        }

        [HttpPost]
        public ActionResult LinkageMobile(int? id, int? m, string APIRequestFrom = "", string UserName = "")
        {
            string ProjectDesc = "";
            string ManufacturingCode = "";
            string AccessRole = "";
            bool isEditable = false;
            var DrawingNo = new List<string>();
            bool isQCRemarkEditable = false;

            PRL265 objPRL265 = new PRL265();
            db.Configuration.ProxyCreationEnabled = false;
            objPRL265 = db.PRL265.Where(i => i.HeaderId == id).FirstOrDefault();
            db.Entry(objPRL265).State = System.Data.Entity.EntityState.Detached;

            NDEModels objNDEModels = new NDEModels();
            if (objPRL265 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL265.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
            AccessRole = objUserRoleAccessDetails.UserDesignation;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstElevationOfTSRFrom = new List<string> { "REF. LINE", "TAN LINE" };

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            var YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var ElevationOfTSRFrom = lstElevationOfTSRFrom.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();


            //  ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL266> lstPRL266 = db.PRL266.Where(x => x.HeaderId == objPRL265.HeaderId).ToList();
            List<PRL267> lstPRL267 = db.PRL267.Where(x => x.HeaderId == objPRL265.HeaderId).ToList();
            List<PRL268> lstPRL268 = db.PRL268.Where(x => x.HeaderId == objPRL265.HeaderId).ToList();


            #endregion
            try
            {
                isEditable = false;
                isQCRemarkEditable = false;
                if (m == 1)
                {
                    isEditable = true;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL265.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL265.ActFilledBy) && (objPRL265.RequestNo.HasValue))
                        {
                            isEditable = false;
                            isQCRemarkEditable = true;
                        }
                        else
                        {
                            DrawingNo = (new GeneralController()).GetPLMDrawingNumber(objPRL265.Project).ToList();
                        }
                    }
                }

                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    ProjectDesc = ProjectDesc,
                    ManufacturingCode = ManufacturingCode,
                    isEditable = isEditable,
                    isQCRemarkEditable = isQCRemarkEditable,
                    DrawingNo = DrawingNo,
                    OutsideInside = OutsideInside,
                    ElevationOfTSRFrom = ElevationOfTSRFrom,
                    AccessRole = AccessRole,
                    YesNoEnum = YesNoEnum,
                    YesNAEnum = YesNAEnum,

                    objPRL265 = objPRL265,


                    lstPRL266 = lstPRL266,
                    lstPRL267 = lstPRL267,
                    lstPRL268 = lstPRL268,

                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.lstDrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();

                return Json(new
                {
                    res = "0",
                    msg = ex.Message,
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL265 PRL265, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = PRL265.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL265 objPRL265 = db.PRL265.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    #region Save Data

                    objPRL265.DrawingRevisionNo = PRL265.DrawingRevisionNo;
                    objPRL265.DrawingNo = PRL265.DrawingNo;
                    objPRL265.DCRNo = PRL265.DCRNo;
                    objPRL265.ProtocolNo = PRL265.ProtocolNo != null ? PRL265.ProtocolNo : "";
                    objPRL265.ElevationOfTSRFrom = PRL265.ElevationOfTSRFrom;
                    objPRL265.ReqElevationOfTSRFrom = PRL265.ReqElevationOfTSRFrom;
                    objPRL265.ReqTSRWidth = PRL265.ReqTSRWidth;
                    objPRL265.ReqTSRHeight = PRL265.ReqTSRHeight;
                    objPRL265.TSRID = PRL265.TSRID;
                    objPRL265.FlatnessIn = PRL265.FlatnessIn;
                    objPRL265.ReqTSRID = PRL265.ReqTSRID;
                    objPRL265.TolElevationOfTSRFrom = PRL265.TolElevationOfTSRFrom;
                    objPRL265.TolTSRWidth = PRL265.TolTSRWidth;
                    objPRL265.TolTSRHeight = PRL265.TolTSRHeight;
                    objPRL265.TolTSRID = PRL265.TolTSRID;
                    objPRL265.TolLevelnessOfTopFaceOfTSR = PRL265.TolLevelnessOfTopFaceOfTSR;
                    objPRL265.TolTiltingOfTSR = PRL265.TolTiltingOfTSR;
                    objPRL265.TolFlatnessIn = PRL265.TolFlatnessIn;
                    objPRL265.WEPSeamNo = PRL265.WEPSeamNo;
                    objPRL265.ReqRootGap = PRL265.ReqRootGap;
                    objPRL265.ReqRootFace = PRL265.ReqRootFace;
                    objPRL265.ReqOneSideWepAngle = PRL265.ReqOneSideWepAngle;
                    objPRL265.ReqOtherSideWepAngle = PRL265.ReqOtherSideWepAngle;
                    objPRL265.ActMaxRootGap = PRL265.ActMaxRootGap;
                    objPRL265.ActMaxRootFace = PRL265.ActMaxRootFace;
                    objPRL265.ActMaxOneSideWepAngle = PRL265.ActMaxOneSideWepAngle;
                    objPRL265.ActMaxOtherSideWepAngle = PRL265.ActMaxOtherSideWepAngle;
                    objPRL265.ActMinRootGap = PRL265.ActMinRootGap;
                    objPRL265.ActMinRootFace = PRL265.ActMinRootFace;
                    objPRL265.ActMinOneSideWepAngle = PRL265.ActMinOneSideWepAngle;
                    objPRL265.ActMinOtherSideWepAngle = PRL265.ActMinOtherSideWepAngle;
                    objPRL265.ReqRootGap1 = PRL265.ReqRootGap1;
                    objPRL265.ReqRootFace1 = PRL265.ReqRootFace1;
                    objPRL265.ReqTopSideWepAngle = PRL265.ReqTopSideWepAngle;
                    objPRL265.ReqBottomSideWepAngle = PRL265.ReqBottomSideWepAngle;
                    objPRL265.ReqOffset = PRL265.ReqOffset;
                    objPRL265.LevelnessOfTopFaceOfTSR = PRL265.LevelnessOfTopFaceOfTSR;
                    objPRL265.CheckPoint1 = PRL265.CheckPoint1;
                    objPRL265.CheckPoint2 = PRL265.CheckPoint2;
                    objPRL265.CheckPoint3 = PRL265.CheckPoint3;
                    objPRL265.CheckPoint4 = PRL265.CheckPoint4;
                    objPRL265.CheckPoint5 = PRL265.CheckPoint5;
                    objPRL265.CheckPoint6 = PRL265.CheckPoint6;
                    objPRL265.CheckPoint7 = PRL265.CheckPoint7;
                    objPRL265.CheckPoint9 = PRL265.CheckPoint9;
                    objPRL265.CheckPoint9_2 = PRL265.CheckPoint9_2;
                    objPRL265.QCRemarks = PRL265.QCRemarks;
                    objPRL265.Result = PRL265.Result;

                    objPRL265.EditedBy = UserName;
                    objPRL265.EditedOn = DateTime.Now;

                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL265.ActFilledBy = UserName;
                            objPRL265.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL265.ReqFilledBy = UserName;
                            objPRL265.ReqFilledOn = DateTime.Now;
                        }
                    }
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL265.HeaderId;
                    #endregion

                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL265 objPRL265 = db.PRL265.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL265 != null && string.IsNullOrWhiteSpace(objPRL265.ProtocolNo))
                    {
                        db.PRL265.Remove(objPRL265);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL265.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL266> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL266> lstAddPRL266 = new List<PRL266>();
                List<PRL266> lstDeletePRL266 = new List<PRL266>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRL266 obj = db.PRL266.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.Orientation = item.Orientation;
                                obj.ActElevationOfTSRFrom = item.ActElevationOfTSRFrom;
                                obj.ActTSRWidth = item.ActTSRWidth;
                                obj.ActTSRHeight = item.ActTSRHeight;
                                obj.ActTSRID = item.ActTSRID;
                                //obj.LevelnessOfTopFaceOfTSR = item.LevelnessOfTopFaceOfTSR;
                                obj.ActTiltingOfTSR = item.ActTiltingOfTSR;
                                obj.ActFlatnessIn = item.ActFlatnessIn;
                                obj.EditedBy = UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRL266 obj = new PRL266();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.Orientation))
                            {
                                obj.Orientation = item.Orientation;
                                obj.ActElevationOfTSRFrom = item.ActElevationOfTSRFrom;
                                obj.ActTSRWidth = item.ActTSRWidth;
                                obj.ActTSRHeight = item.ActTSRHeight;
                                obj.ActTSRID = item.ActTSRID;
                                //obj.LevelnessOfTopFaceOfTSR = item.LevelnessOfTopFaceOfTSR;
                                obj.ActTiltingOfTSR = item.ActTiltingOfTSR;
                                obj.ActFlatnessIn = item.ActFlatnessIn;
                                obj.CreatedBy = UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRL266.Add(obj);
                            }
                        }
                    }
                }
                if (lstAddPRL266.Count > 0)
                {
                    db.PRL266.AddRange(lstAddPRL266);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL266 = db.PRL266.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL266 = db.PRL266.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL266.Count > 0)
                {
                    db.PRL266.RemoveRange(lstDeletePRL266);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine2Linkage(List<PRL267> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL267> lstAddPRL267 = new List<PRL267>();
                List<PRL267> lstDeletePRL267 = new List<PRL267>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.SeamNo != null)
                        {
                            bool isAdded = false;
                            PRL267 obj = db.PRL267.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj == null)
                            {
                                obj = new PRL267();
                                obj.HeaderId = item.HeaderId;
                                obj.CreatedBy = UserName;
                                obj.CreatedOn = DateTime.Now;
                                isAdded = true;
                            }
                            else
                            {
                                obj.EditedBy = UserName;
                                obj.EditedOn = DateTime.Now;
                            }

                            obj.SeamNo = item.SeamNo;
                            obj.ActMaxRootGap = item.ActMaxRootGap;
                            obj.ActMaxRootFace = item.ActMaxRootFace;
                            obj.ActMaxTopSideWepAngle = item.ActMaxTopSideWepAngle;
                            obj.ActMaxBottomSideWepAngle = item.ActMaxBottomSideWepAngle;
                            obj.ActMaxOffset = item.ActMaxOffset;
                            obj.ActMinRootGap = item.ActMinRootGap;
                            obj.ActMinRootFace = item.ActMinRootFace;
                            obj.ActMinTopSideWepAngle = item.ActMinTopSideWepAngle;
                            obj.ActMinBottomSideWepAngle = item.ActMinBottomSideWepAngle;
                            obj.ActMinOffset = item.ActMinOffset;
                            if (isAdded)
                            {
                                lstAddPRL267.Add(obj);
                            }
                        }
                    }
                }
                if (lstAddPRL267.Count > 0)
                {
                    db.PRL267.AddRange(lstAddPRL267);
                }
                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL267 = db.PRL267.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL267 = db.PRL267.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL267.Count > 0)
                {
                    db.PRL267.RemoveRange(lstDeletePRL267);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line3 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine3Linkage(List<PRL268> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL268> lstAddPRL268 = new List<PRL268>();
                List<PRL268> lstDeletePRL268 = new List<PRL268>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL268 obj = db.PRL268.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        else
                        {
                            obj = new PRL268();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }

                        if (!string.IsNullOrWhiteSpace(item.IdentificationMarking))
                        {
                            obj.IdentificationMarking = item.IdentificationMarking;
                        }
                        if (isAdded)
                        {
                            lstAddPRL268.Add(obj);
                        }
                    }
                    if (lstAddPRL268.Count > 0)
                    {
                        db.PRL268.AddRange(lstAddPRL268);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL268 = db.PRL268.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL268 = db.PRL268.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL268.Count > 0)
                {
                    db.PRL268.RemoveRange(lstDeletePRL268);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion



        #endregion
    }
}