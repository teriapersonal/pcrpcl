﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol002Controller : clsBase
    {
        string ControllerURL = "/PROTOCOL/Protocol002/";
        string Title = "SET-UP & DIMENSION REPORT FOR CIRCUMFERENTIAL SEAM";

        // GET: PROTOCOL/Protocol002
        //[SessionExpireFilter, UserPermissions, AllowAnonymous]

        #region Details View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO005 objPRO005 = new PRO005();
            if (!id.HasValue)
            {
                objPRO005.ProtocolNo = string.Empty;
                objPRO005.CreatedBy = objClsLoginInfo.UserName;
                objPRO005.CreatedOn = DateTime.Now;
                db.PRO005.Add(objPRO005);
                db.SaveChanges();
                ViewBag.HeaderId = objPRO005.HeaderId;
            }
            else
            {
                objPRO005 = db.PRO005.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            List<string> lstProtocolRT = clsImplementationEnum.getProtocolRT().ToList();
            ViewBag.ProtocolRT = lstProtocolRT.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            List<string> lstProtocolUT = clsImplementationEnum.getProtocolRT().ToList();
            ViewBag.ProtocolUT = lstProtocolUT.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            return View(objPRO005);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO005 pro005)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = pro005.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO005 objPRO005 = db.PRO005.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO005.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == pro005.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data
                        objPRO005.ProtocolNo = pro005.ProtocolNo;
                        objPRO005.IdentificationMarking = pro005.IdentificationMarking;
                        objPRO005.PtcRequired = pro005.PtcRequired;
                        objPRO005.ITNo = pro005.ITNo;
                        objPRO005.HeatNo = pro005.HeatNo;
                        objPRO005.WepUtClear = pro005.WepUtClear;
                        objPRO005.WepMtClear = pro005.WepMtClear;
                        objPRO005.WepPtClear = pro005.WepPtClear;
                        objPRO005.WepCuso4Clear = pro005.WepCuso4Clear;
                        objPRO005.ExtentOfRT = pro005.ExtentOfRT;
                        objPRO005.ExtentOfUT = pro005.ExtentOfUT;
                        objPRO005.WEPAngleis = pro005.WEPAngleis;
                        objPRO005.WEPAngleos = pro005.WEPAngleos;
                        objPRO005.TacksFreeFromCrack = pro005.TacksFreeFromCrack;
                        objPRO005.RequriedRootGap = pro005.RequriedRootGap;
                        objPRO005.RequriedRootFace = pro005.RequriedRootFace;
                        objPRO005.RequiredRemark = pro005.RequiredRemark;
                        objPRO005.ActMaxRootGap = pro005.ActMaxRootGap;
                        objPRO005.ActMaxRootFace = pro005.ActMaxRootFace;
                        objPRO005.ActMaxRemark = pro005.ActMaxRemark;
                        objPRO005.ActMinRootGap = pro005.ActMinRootGap;
                        objPRO005.ActMinRootFace = pro005.ActMinRootFace;
                        objPRO005.ActMinRemark = pro005.ActMinRemark; ;
                        objPRO005.TotalHeightRequired = pro005.TotalHeightRequired;
                        objPRO005.HeightAt0 = pro005.HeightAt0;
                        objPRO005.HeightAt45 = pro005.HeightAt45;
                        objPRO005.HeightAt90 = pro005.HeightAt90;
                        objPRO005.HeightAt135 = pro005.HeightAt135; ;
                        objPRO005.HeightAt180 = pro005.HeightAt180;
                        objPRO005.HeightAt225 = pro005.HeightAt225;
                        objPRO005.HeightAt270 = pro005.HeightAt270;
                        objPRO005.HeightAt315 = pro005.HeightAt315;
                        objPRO005.TTLToTopShellWEPRequiredLength = pro005.TTLToTopShellWEPRequiredLength;
                        objPRO005.Top0 = pro005.Top0;
                        objPRO005.Top45 = pro005.Top45;
                        objPRO005.Top90 = pro005.Top90;
                        objPRO005.Top135 = pro005.Top135;
                        objPRO005.Top180 = pro005.Top180;
                        objPRO005.Top225 = pro005.Top225;
                        objPRO005.Top270 = pro005.Top270;
                        objPRO005.Top315 = pro005.Top315;
                        objPRO005.Note2 = pro005.Note2;
                        objPRO005.Note2_1 = pro005.Note2_1;
                        objPRO005.Note4 = pro005.Note4;
                        objPRO005.Note51 = pro005.Note51;
                        objPRO005.Note52 = pro005.Note52;
                        objPRO005.Note6 = pro005.Note6;
                        objPRO005.Note7 = pro005.Note7;
                        objPRO005.Note8 = pro005.Note8;
                        objPRO005.Note9 = pro005.Note9;
                        objPRO005.Note10 = pro005.Note10;
                        objPRO005.AllowableOffset = pro005.AllowableOffset;
                        objPRO005.AlignmentReadingsRemark = pro005.AlignmentReadingsRemark;
                        objPRO005.CladStripingWidthMax = pro005.CladStripingWidthMax;
                        objPRO005.CladStripingWidthMin = pro005.CladStripingWidthMin;
                        objPRO005.CladStripingDepthMax = pro005.CladStripingDepthMax;
                        objPRO005.CladStripingDepthMin = pro005.CladStripingDepthMin;
                        objPRO005.CladdedVessel = pro005.CladdedVessel;
                        objPRO005.FNote2 = pro005.FNote2;
                        objPRO005.FNote3 = pro005.FNote3;
                        objPRO005.EditedBy = objClsLoginInfo.UserName;
                        objPRO005.EditedOn = DateTime.Now;

                        objPRO005.SeamNoPunchingATEvery900MM = pro005.SeamNoPunchingATEvery900MM;
                        objPRO005.TopRemarks = pro005.TopRemarks;
                        objPRO005.Note1 = pro005.Note1;
                        objPRO005.FNote1 = pro005.FNote1;
                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol saved successfully";
                        objResponseMsg.HeaderId = objPRO005.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO005 objPRO005 = db.PRO005.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO005 != null && string.IsNullOrWhiteSpace(objPRO005.ProtocolNo))
                    {
                        db.PRO006.RemoveRange(objPRO005.PRO006.ToList());
                        db.PRO007.RemoveRange(objPRO005.PRO007.ToList());
                        db.PRO008.RemoveRange(objPRO005.PRO008.ToList());
                        db.PRO009_1.RemoveRange(objPRO005.PRO009_1.ToList());
                        db.PRO009_2.RemoveRange(objPRO005.PRO009_2.ToList());
                        db.PRO005.Remove(objPRO005);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.SETUP_AND_DIMENSION_REPORT_FOR_CIRCUMFERENTIAL_SEAM.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO005.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public ActionResult GetProtocolLines1Data(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro.SpotNo like '%" + param.sSearch
                        + "%' or pro.Offset like '%" + param.sSearch
                        + "%')";
                }

                var lstResult = db.SP_IPI_PROTOCOL002_GET_LINES1_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                    "1",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "SpotNo", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "Offset", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine1(0);")
                                };
                var data = (from uc in lstResult
                            select new[]
                            {
                                 "1",
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                               Helper.GenerateHTMLTextbox(uc.LineId, "SpotNo", Convert.ToString(uc.SpotNo), "", true, "", false, "10","PROD"),
                               Helper.GenerateNumericTextbox(uc.LineId, "Offset",  Convert.ToString(uc.Offset), "", true, "", false, "10","PROD"),
                               Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","")
                               + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.PROD3.GetStringValue()? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine1','tblProtocolLines1')") : "")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine1(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRO006 objPRO006 = new PRO006();
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;

                if (refHeaderId > 0)
                {
                    string intSpotNo = !string.IsNullOrEmpty(fc["SpotNo" + refLineId]) ? Convert.ToString(fc["SpotNo" + refLineId]).Trim() : "";
                    decimal? strOffset = (new clsManager()).GetDecimalValue(Convert.ToString(fc["Offset" + refLineId]));
                    if (refLineId > 0)
                    {
                        objPRO006 = db.PRO006.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                    }
                    #region Add New Spot Line1
                    objPRO006.HeaderId = refHeaderId;
                    objPRO006.SpotNo = intSpotNo;
                    objPRO006.Offset = strOffset;
                    if (refLineId > 0)
                    {
                        objPRO006.EditedBy = objClsLoginInfo.UserName;
                        objPRO006.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Value = "Data updated successfully";
                    }
                    else
                    {
                        objPRO006.CreatedBy = objClsLoginInfo.UserName;
                        objPRO006.CreatedOn = DateTime.Now;
                        db.PRO006.Add(objPRO006);
                        db.SaveChanges();
                        objResponseMsg.Value = "Data added successfully";
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRO006.LineId;
                    #endregion
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine1(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRO006 objPRO006 = db.PRO006.FirstOrDefault(c => c.LineId == lineId);
                if (objPRO006 != null)
                {
                    db.PRO006.Remove(objPRO006);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public ActionResult GetProtocolLines2Data(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro.SeamNo like '%" + param.sSearch
                        + "%' or pro.ReqOrien like '%" + param.sSearch
                        + "%' or pro.ReqArcLength1 like '%" + param.sSearch
                        + "%' or pro.ReqArcLength2 like '%" + param.sSearch
                        + "%' or pro.ReqArcLength3 like '%" + param.sSearch
                        + "%' or pro.ActualArc1 like '%" + param.sSearch
                        + "%' or pro.ActualArc2 like '%" + param.sSearch
                        + "%' or pro.ActualArc3 like '%" + param.sSearch
                        + "%' or pro.Remark like '%" + param.sSearch
                        + "%')";
                }
                //var lstResult = db.SP_IPI_PROTOCOL_GET_LINES1_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                var lstResult = db.SP_IPI_PROTOCOL002_GET_LINES2_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                     "2",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "SeamNo", "", "", false, "", false, "10","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "ReqOrien", "", "", false, "", false, "10","QC"),
                                   Helper.GenerateNumericTextbox(newRecordId, "ReqArcLength1", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "ReqArcLength2", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "ReqArcLength3", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "ActualArc1", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "ActualArc2", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "ActualArc3", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "RemarkLine2_", "", "", false, "", false, "100","PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine2(0);")
                                };

                var data = (from uc in lstResult
                            select new[]
                            {
                                  "2",
                                 Convert.ToString(uc.HeaderId),
                                 Convert.ToString(uc.LineId),
                                 Helper.GenerateHTMLTextbox(uc.LineId, "SeamNo", uc.SeamNo, "", true, "", false, "10","QC"),
                                 Helper.GenerateHTMLTextbox(uc.LineId, "ReqOrien", uc.ReqOrien, "", true, "", false, "10","QC"),
                                 Helper.GenerateNumericTextbox(uc.LineId, "ReqArcLength1",  Convert.ToString(uc.ReqArcLength1), "", true, "", false, "10","PROD"),
                                 Helper.GenerateNumericTextbox(uc.LineId, "ReqArcLength2", Convert.ToString(uc.ReqArcLength2), "", true, "", false, "10","PROD"),
                                 Helper.GenerateNumericTextbox(uc.LineId, "ReqArcLength3", Convert.ToString(uc.ReqArcLength3), "", true, "", false, "10","PROD"),
                                 Helper.GenerateNumericTextbox(uc.LineId, "ActualArc1", Convert.ToString(uc.ActualArc1), "", true, "", false, "10","PROD"),
                                 Helper.GenerateNumericTextbox(uc.LineId, "ActualArc2", Convert.ToString(uc.ActualArc2), "", true, "", false, "10","PROD"),
                                 Helper.GenerateNumericTextbox(uc.LineId, "ActualArc3", Convert.ToString(uc.ActualArc3), "", true, "", false, "10","PROD"),
                                 Helper.GenerateHTMLTextbox(uc.LineId, "RemarkLine2_", Convert.ToString(uc.Remark), "", true, "", false, "100","PROD"),
                                 Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","")
                               + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() ==  clsImplementationEnum.UserRoleName.QC3.GetStringValue().ToUpper()? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine2','tblProtocolLines2')") : "")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine2(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRO007 objPRO007 = new PRO007();
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;

                if (refHeaderId > 0)
                {
                    if (refLineId > 0)
                    {
                        objPRO007 = db.PRO007.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                    }
                    string strSeamNo = !string.IsNullOrEmpty(fc["SeamNo" + refLineId]) ? Convert.ToString(fc["SeamNo" + refLineId]).Trim() : "";
                    string strReqOrien = !string.IsNullOrEmpty(fc["ReqOrien" + refLineId]) ? Convert.ToString(fc["ReqOrien" + refLineId]).Trim() : "";
                    string strRemark = !string.IsNullOrEmpty(fc["RemarkLine2_" + refLineId]) ? Convert.ToString(fc["RemarkLine2_" + refLineId]).Trim() : "";
                    decimal? decReqArcLength1 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["ReqArcLength1" + refLineId]));
                    decimal? decReqArcLength2 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["ReqArcLength2" + refLineId]));
                    decimal? decReqArcLength3 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["ReqArcLength3" + refLineId]));
                    decimal? decActualArc1 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["ActualArc1" + refLineId]));
                    decimal? decActualArc2 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["ActualArc2" + refLineId]));
                    decimal? decActualArc3 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["ActualArc3" + refLineId]));

                    objPRO007.HeaderId = refHeaderId;
                    objPRO007.SeamNo = strSeamNo;
                    objPRO007.ReqOrien = strReqOrien;
                    objPRO007.ReqArcLength1 = decReqArcLength1;
                    objPRO007.ReqArcLength2 = decReqArcLength2;
                    objPRO007.ReqArcLength3 = decReqArcLength3;
                    objPRO007.ActualArc1 = decActualArc1;
                    objPRO007.ActualArc2 = decActualArc2;
                    objPRO007.ActualArc3 = decActualArc3;
                    objPRO007.Remark = strRemark;

                    #region Add New Spot Line1

                    if (refLineId > 0)
                    {
                        objPRO007.EditedBy = objClsLoginInfo.UserName;
                        objPRO007.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Value = "Data updated successfully";
                    }
                    else
                    {
                        objPRO007.CreatedBy = objClsLoginInfo.UserName;
                        objPRO007.CreatedOn = DateTime.Now;
                        db.PRO007.Add(objPRO007);
                        db.SaveChanges();
                        objResponseMsg.Value = "Data added successfully";
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRO007.LineId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine2(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRO007 objPRO007 = db.PRO007.FirstOrDefault(c => c.LineId == lineId);
                if (objPRO007 != null)
                {
                    db.PRO007.Remove(objPRO007);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line3 details
        [HttpPost]
        public ActionResult GetProtocolLines3Data(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro.Location like '%" + param.sSearch
                        + "%' or pro.At0 like '%" + param.sSearch
                        + "%' or pro.At90 like '%" + param.sSearch
                        + "%' or pro.At180 like '%" + param.sSearch
                        + "%' or pro.At270 like '%" + param.sSearch
                        + "%' or pro.Remark like '%" + param.sSearch
                        + "%')";
                }
                var lstResult = db.SP_IPI_PROTOCOL002_GET_LINES3_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                var lstOutBy = db.SP_IPI_PROTOCOL002_GET_OUTBY(intHeaderId).FirstOrDefault();
                var outBy0 = lstOutBy.OutBy0;
                var outBy90 = lstOutBy.OutBy90;
                var outBy180 = lstOutBy.OutBy180;
                var outBy270 = lstOutBy.OutBy270;

                int newRecordId = 0;
                var newRecord = new[] {
                       "3",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Location", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "At0", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "At90", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "At180", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "At270", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "RemarkLine3_", "", "", false, "", false, "100","PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine3(0);")
                                };

                var data = (from uc in lstResult
                            select new[]
                            {
                                   "3",
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),

                               Helper.GenerateHTMLTextbox(uc.LineId, "Location",  Convert.ToString(uc.Location), "", true, "", false, "10","PROD"),
                               Helper.GenerateNumericTextbox(uc.LineId, "At0",    Convert.ToString(uc.At0), "", true, "", false, "10","PROD"),
                               Helper.GenerateNumericTextbox(uc.LineId, "At90",  Convert.ToString(uc.At90), "", true, "", false, "10","PROD"),
                               Helper.GenerateNumericTextbox(uc.LineId, "At180", Convert.ToString(uc.At180), "", true, "", false, "10","PROD"),
                               Helper.GenerateNumericTextbox(uc.LineId, "At270",  Convert.ToString(uc.At270), "", true, "", false, "10","PROD"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "RemarkLine3_", Convert.ToString(uc.Remark), "", true, "", false, "100","PROD"),

                               Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","")
                               + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.PROD3.GetStringValue()? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine3','tblProtocolLines3')") : "")
                           }).ToList();

                var outBy = new[] {
                    "",
                                   Convert.ToString(headerId),
                                   "",
                                   "<strong>Out By</strong>",
                                   Helper.GenerateNumericTextbox(0, "OutBy",  outBy0.ToString(), "", true, "", false),
                                   Helper.GenerateNumericTextbox(90, "OutBy", outBy90.ToString(), "", true, "", false),
                                   Helper.GenerateNumericTextbox(180, "OutBy",outBy180.ToString(), "", true, "",false),
                                   Helper.GenerateNumericTextbox(270, "OutBy",outBy270.ToString(), "", true, "",false),
                                   "",
                                   ""
                                    };
                data.Insert(0, newRecord);
                data.Insert(data.Count, outBy);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition,
                    outby = true
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine3(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRO008 objPRO008 = new PRO008();
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;

                if (refHeaderId > 0)
                {
                    PRO005 objPRO005 = db.PRO005.Where(x => x.HeaderId == refHeaderId).FirstOrDefault();

                    if (refLineId > 0)
                    {
                        objPRO008 = db.PRO008.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRO008.EditedBy = objClsLoginInfo.UserName;
                        objPRO008.EditedOn = DateTime.Now;
                    }
                    string strLocation = !string.IsNullOrEmpty(fc["Location" + refLineId]) ? Convert.ToString(fc["Location" + refLineId]).Trim() : "";
                    string strRemark = !string.IsNullOrEmpty(fc["RemarkLine3_" + refLineId]) ? Convert.ToString(fc["RemarkLine3_" + refLineId]).Trim() : "";
                    decimal? decAt0 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["At0" + refLineId]));
                    decimal? decAt90 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["At90" + refLineId]));
                    decimal? decAt180 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["At180" + refLineId]));
                    decimal? decAt270 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["At270" + refLineId]));

                    #region Add New Spot Line1
                    objPRO008.HeaderId = refHeaderId;
                    objPRO008.Location = strLocation;
                    objPRO008.Remark = strRemark;
                    objPRO008.At0 = decAt0;
                    objPRO008.At90 = decAt90;
                    objPRO008.At180 = decAt180;
                    objPRO008.At270 = decAt270;

                    objPRO005.OutBy0 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["OutBy0"]));
                    objPRO005.OutBy90 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["OutBy90"]));
                    objPRO005.OutBy180 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["OutBy180"]));
                    objPRO005.OutBy270 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["OutBy270"]));

                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = "Data updated successfully";
                    }
                    else
                    {
                        objPRO008.CreatedBy = objClsLoginInfo.UserName;
                        objPRO008.CreatedOn = DateTime.Now;
                        db.PRO008.Add(objPRO008);
                        db.SaveChanges();
                        objResponseMsg.Value = "Data added successfully";
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRO008.LineId;
                    #endregion
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine3(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRO008 objPRO008 = db.PRO008.FirstOrDefault(c => c.LineId == lineId);
                if (objPRO008 != null)
                {
                    db.PRO008.Remove(objPRO008);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line4 Width details
        [HttpPost]
        public ActionResult GetProtocolLines4WidthData(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro.SpotNo like '%" + param.sSearch
                        + "%' or pro.Width like '%" + param.sSearch
                        + "%' or pro.Remarks like '%" + param.sSearch
                        + "%')";
                }
                var lstResult = db.SP_IPI_PROTOCOL002_GET_LINES4_WIDTH_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                //var lstResult = db.SP_IPI_PROTOCOL002_GET_LINES4_WIDTH_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                       "4",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "SpotNoWidth", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "Width", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "RemarksWidth", "", "", false, "", false, "100","PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine4(0);")
                                };

                var data = (from uc in lstResult
                            select new[]
                            {
                                  "4",
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),

                               Helper.GenerateHTMLTextbox(uc.LineId,  "SpotNoWidth",Convert.ToString(uc.SpotNo), "", true, "", false, "10","PROD"),
                               Helper.GenerateNumericTextbox(uc.LineId, "Width", Convert.ToString(uc.Width),"", true, "", false, "10","PROD"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "RemarksWidth",Convert.ToString(uc.Remarks), "", true, "", false, "100","PROD"),

                               Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","")
                               + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.PROD3.GetStringValue()? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine4Width','tblProtocolLines4Width')") : "")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine4(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRO009_1 objPRO009_1 = new PRO009_1();
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;

                if (refHeaderId > 0)
                {
                    string strSpotNo = !string.IsNullOrEmpty(fc["SpotNoWidth" + refLineId]) ? Convert.ToString(fc["SpotNoWidth" + refLineId]).Trim() : "";
                    string strRemarks = !string.IsNullOrEmpty(fc["RemarksWidth" + refLineId]) ? Convert.ToString(fc["RemarksWidth" + refLineId]).Trim() : "";
                    decimal? decWidth = (new clsManager()).GetDecimalValue(Convert.ToString(fc["Width" + refLineId]));
                    if (refLineId > 0)
                    {
                        objPRO009_1 = db.PRO009_1.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRO009_1.EditedBy = objClsLoginInfo.UserName;
                        objPRO009_1.EditedOn = DateTime.Now;
                    }
                    #region Add New Spot Line1
                    objPRO009_1.HeaderId = refHeaderId;
                    objPRO009_1.SpotNo = strSpotNo;
                    objPRO009_1.Remarks = strRemarks;
                    objPRO009_1.Width = decWidth;
                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.SpotUpdate;
                    }
                    else
                    {
                        objPRO009_1.CreatedBy = objClsLoginInfo.UserName;
                        objPRO009_1.CreatedOn = DateTime.Now;
                        db.PRO009_1.Add(objPRO009_1);
                        db.SaveChanges();
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.SpotAdded;
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRO009_1.LineId;
                    #endregion
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine4Width(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRO009_1 objPRO009_1 = db.PRO009_1.FirstOrDefault(c => c.LineId == lineId);
                if (objPRO009_1 != null)
                {
                    db.PRO009_1.Remove(objPRO009_1);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line4 Depth details
        [HttpPost]
        public ActionResult GetProtocolLines4DepthData(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro.SpotNo like '%" + param.sSearch
                        + "%' or pro.Depth like '%" + param.sSearch
                        + "%' or pro.Remarks like '%" + param.sSearch
                        + "%')";
                }
                var lstResult = db.SP_IPI_PROTOCOL002_GET_LINES4_DEPTH_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                //var lstResult = db.SP_IPI_PROTOCOL002_GET_LINES4_DEPTH_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                      "5",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "SpotNoDepth", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "Depth", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "RemarksDepth", "", "", false, "", false, "100","PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine5(0);")
                                };

                var data = (from uc in lstResult
                            select new[]
                            {
                                  "5",
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                               Helper.GenerateHTMLTextbox(uc.LineId, "SpotNoDepth",Convert.ToString(uc.SpotNo), "", true, "", false, "10","PROD"),
                               Helper.GenerateNumericTextbox(uc.LineId, "Depth",Convert.ToString(uc.Depth), "", true, "", false, "10","PROD"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "RemarksDepth",Convert.ToString(uc.Remarks), "", true, "", false, "100","PROD"),

                               Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","")
                               + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.PROD3.GetStringValue()? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine4Depth','tblProtocolLines4Depth')") : "")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine5(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRO009_2 objPRO009_2 = new PRO009_2();
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
                if (refHeaderId > 0)
                {
                    string strSpotNo = !string.IsNullOrEmpty(fc["SpotNoDepth" + refLineId]) ? Convert.ToString(fc["SpotNoDepth" + refLineId]).Trim() : "";
                    string strRemarks = !string.IsNullOrEmpty(fc["RemarksDepth" + refLineId]) ? Convert.ToString(fc["RemarksDepth" + refLineId]).Trim() : "";
                    decimal? decDepth = (new clsManager()).GetDecimalValue(Convert.ToString(fc["Depth" + refLineId]));
                    if (refLineId > 0)
                    {
                        objPRO009_2 = db.PRO009_2.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRO009_2.EditedBy = objClsLoginInfo.UserName;
                        objPRO009_2.EditedOn = DateTime.Now;
                    }
                    #region Add New Spot Line1
                    objPRO009_2.HeaderId = refHeaderId;
                    objPRO009_2.SpotNo = strSpotNo;
                    objPRO009_2.Remarks = strRemarks;
                    objPRO009_2.Depth = decDepth;
                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.SpotUpdate;
                    }
                    else
                    {
                        objPRO009_2.CreatedBy = objClsLoginInfo.UserName;
                        objPRO009_2.CreatedOn = DateTime.Now;
                        db.PRO009_2.Add(objPRO009_2);
                        db.SaveChanges();
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.SpotAdded;
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRO009_2.LineId;
                    #endregion
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine4Depth(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRO009_2 objPRO009_2 = db.PRO009_2.FirstOrDefault(c => c.LineId == lineId);
                if (objPRO009_2 != null)
                {
                    db.PRO009_2.Remove(objPRO009_2);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #endregion

        #region Linkage View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL005 objPRL005 = new PRL005();
            objPRL005 = db.PRL005.Where(i => i.HeaderId == id).FirstOrDefault();

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;
            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            List<string> lstProtocolRT = clsImplementationEnum.getProtocolRT().ToList();
            ViewBag.ProtocolRT = lstProtocolRT.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            List<string> lstProtocolUT = clsImplementationEnum.getProtocolRT().ToList();
            ViewBag.ProtocolUT = lstProtocolUT.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();
            if (objPRL005 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL005.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objQproject.ManufacturingCode;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }
            if (m == 1)
            {
                ViewBag.isEditable = "true";
            }
            else
            { ViewBag.isEditable = "false"; }

            return View(objPRL005);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL005 PRL005)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRL005.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL005 objPRL005 = db.PRL005.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL005.ProtocolNo = PRL005.ProtocolNo != null ? PRL005.ProtocolNo : "";
                    objPRL005.DCRNo = PRL005.DCRNo;
                    objPRL005.IdentificationMarking = PRL005.IdentificationMarking;
                    objPRL005.PtcRequired = PRL005.PtcRequired;
                    objPRL005.ITNo = PRL005.ITNo;
                    objPRL005.HeatNo = PRL005.HeatNo;
                    objPRL005.WepUtClear = PRL005.WepUtClear;
                    objPRL005.WepMtClear = PRL005.WepMtClear;
                    objPRL005.WepPtClear = PRL005.WepPtClear;
                    objPRL005.WepCuso4Clear = PRL005.WepCuso4Clear;
                    objPRL005.ExtentOfRT = PRL005.ExtentOfRT;
                    objPRL005.ExtentOfUT = PRL005.ExtentOfUT;
                    objPRL005.WEPAngleis = PRL005.WEPAngleis;
                    objPRL005.WEPAngleos = PRL005.WEPAngleos;
                    objPRL005.TacksFreeFromCrack = PRL005.TacksFreeFromCrack;
                    objPRL005.RequriedRootGap = PRL005.RequriedRootGap;
                    objPRL005.RequriedRootFace = PRL005.RequriedRootFace;
                    objPRL005.RequiredRemark = PRL005.RequiredRemark;
                    objPRL005.ActMaxRootGap = PRL005.ActMaxRootGap;
                    objPRL005.ActMaxRootFace = PRL005.ActMaxRootFace;
                    objPRL005.ActMaxRemark = PRL005.ActMaxRemark;
                    objPRL005.ActMinRootGap = PRL005.ActMinRootGap;
                    objPRL005.ActMinRootFace = PRL005.ActMinRootFace;
                    objPRL005.ActMinRemark = PRL005.ActMinRemark; ;
                    objPRL005.TotalHeightRequired = PRL005.TotalHeightRequired;
                    objPRL005.HeightAt0 = PRL005.HeightAt0;
                    objPRL005.HeightAt45 = PRL005.HeightAt45;
                    objPRL005.HeightAt90 = PRL005.HeightAt90;
                    objPRL005.HeightAt135 = PRL005.HeightAt135; ;
                    objPRL005.HeightAt180 = PRL005.HeightAt180;
                    objPRL005.HeightAt225 = PRL005.HeightAt225;
                    objPRL005.HeightAt270 = PRL005.HeightAt270;
                    objPRL005.HeightAt315 = PRL005.HeightAt315;
                    objPRL005.TTLToTopShellWEPRequiredLength = PRL005.TTLToTopShellWEPRequiredLength;
                    objPRL005.Top0 = PRL005.Top0;
                    objPRL005.Top45 = PRL005.Top45;
                    objPRL005.Top90 = PRL005.Top90;
                    objPRL005.Top135 = PRL005.Top135;
                    objPRL005.Top180 = PRL005.Top180;
                    objPRL005.Top225 = PRL005.Top225;
                    objPRL005.Top270 = PRL005.Top270;
                    objPRL005.Top315 = PRL005.Top315;
                    objPRL005.Note2 = PRL005.Note2;
                    objPRL005.Note2_1 = PRL005.Note2_1;
                    objPRL005.Note4 = PRL005.Note4;
                    objPRL005.Note51 = PRL005.Note51;
                    objPRL005.Note52 = PRL005.Note52;
                    objPRL005.Note6 = PRL005.Note6;
                    objPRL005.Note7 = PRL005.Note7;
                    objPRL005.Note8 = PRL005.Note8;
                    objPRL005.Note9 = PRL005.Note9;
                    objPRL005.Note10 = PRL005.Note10;
                    objPRL005.AllowableOffset = PRL005.AllowableOffset;
                    objPRL005.AlignmentReadingsRemark = PRL005.AlignmentReadingsRemark;
                    objPRL005.CladStripingWidthMax = PRL005.CladStripingWidthMax;
                    objPRL005.CladStripingWidthMin = PRL005.CladStripingWidthMin;
                    objPRL005.CladStripingDepthMax = PRL005.CladStripingDepthMax;
                    objPRL005.CladStripingDepthMin = PRL005.CladStripingDepthMin;
                    objPRL005.CladdedVessel = PRL005.CladdedVessel;
                    objPRL005.FNote2 = PRL005.FNote2;
                    objPRL005.FNote3 = PRL005.FNote3;
                    objPRL005.EditedBy = objClsLoginInfo.UserName;
                    objPRL005.EditedOn = DateTime.Now;

                    objPRL005.SeamNoPunchingATEvery900MM = PRL005.SeamNoPunchingATEvery900MM;
                    objPRL005.TopRemarks = PRL005.TopRemarks;
                    objPRL005.Note1 = PRL005.Note1;
                    objPRL005.FNote1 = PRL005.FNote1;
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol saved successfully";
                    objResponseMsg.HeaderId = objPRL005.HeaderId;
                    #endregion

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line1 details
        [HttpPost]
        public ActionResult GetProtocolLines1DataLinkage(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro.SpotNo like '%" + param.sSearch
                        + "%' or pro.Offset like '%" + param.sSearch
                        + "%')";
                }

                var lstResult = db.SP_IPI_PROTOCOL002_LINKAGE_GET_LINES1_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                    "1",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "SpotNo", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "Offset", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine1(0);")
                                };
                var data = (from uc in lstResult
                            select new[]
                            {
                               "1",
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                               Helper.GenerateHTMLTextbox(uc.LineId, "SpotNo",   Convert.ToString(uc.SpotNo), "", true, "", false, "10","PROD"),
                               Helper.GenerateNumericTextbox(uc.LineId, "Offset",Convert.ToString(uc.Offset), "", true, "", false, "10","PROD"),
                               Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","")
                               + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.PROD3.GetStringValue()? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine1Linkage','tblProtocolLines1')") : "")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine1Linkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRL006 objPRL006 = new PRL006();
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
                if (refHeaderId > 0)
                {
                    string intSpotNo = !string.IsNullOrEmpty(fc["SpotNo" + refLineId]) ? Convert.ToString(fc["SpotNo" + refLineId]).Trim() : "";
                    decimal? strOffset = (new clsManager()).GetDecimalValue(Convert.ToString(fc["Offset" + refLineId]));
                    #region Add New Spot Line1
                    if (refLineId > 0)
                    {
                        objPRL006 = db.PRL006.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRL006.EditedBy = objClsLoginInfo.UserName;
                        objPRL006.EditedOn = DateTime.Now;
                    }
                    
                    objPRL006.HeaderId = refHeaderId;
                    objPRL006.SpotNo = intSpotNo;
                    objPRL006.Offset = strOffset;
                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = "Data updated successfully";
                    }
                    else
                    {
                        objPRL006.CreatedBy = objClsLoginInfo.UserName;
                        objPRL006.CreatedOn = DateTime.Now;
                        db.PRL006.Add(objPRL006);
                        db.SaveChanges();
                        objResponseMsg.Value = "Data added successfully";
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRL006.LineId;
                    #endregion

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine1Linkage(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRL006 objPRL006 = db.PRL006.FirstOrDefault(c => c.LineId == lineId);
                if (objPRL006 != null)
                {
                    db.PRL006.Remove(objPRL006);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public ActionResult GetProtocolLines2DataLinkage(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro.SeamNo like '%" + param.sSearch
                        + "%' or pro.ReqOrien like '%" + param.sSearch
                        + "%' or pro.ReqArcLength1 like '%" + param.sSearch
                        + "%' or pro.ReqArcLength2 like '%" + param.sSearch
                        + "%' or pro.ReqArcLength3 like '%" + param.sSearch
                        + "%' or pro.ActualArc1 like '%" + param.sSearch
                        + "%' or pro.ActualArc2 like '%" + param.sSearch
                        + "%' or pro.ActualArc3 like '%" + param.sSearch
                        + "%' or pro.Remark like '%" + param.sSearch
                        + "%')";
                }
                //var lstResult = db.SP_IPI_PROTOCOL_GET_LINES1_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                var lstResult = db.SP_IPI_PROTOCOL002_LINKAGE_GET_LINES2_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                           "2",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "SeamNo", "", "", false, "", false, "10","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "ReqOrien", "", "", false, "", false, "10","QC"),
                                   Helper.GenerateNumericTextbox(newRecordId, "ReqArcLength1", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "ReqArcLength2", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "ReqArcLength3", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "ActualArc1", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "ActualArc2", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "ActualArc3", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "RemarkLine2_", "", "", false, "", false, "100","PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine2(0);")
                                };

                var data = (from uc in lstResult
                            select new[]
                            {
                               "2",
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),

                               Helper.GenerateHTMLTextbox(uc.LineId, "SeamNo", Convert.ToString(uc.SeamNo),"", true, "", false, "10","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "ReqOrien",Convert.ToString(uc.ReqOrien), "", true, "", false, "10","QC"),
                               Helper.GenerateNumericTextbox(uc.LineId, "ReqArcLength1",Convert.ToString(uc.ReqArcLength1), "", true, "", false, "10","PROD"),
                               Helper.GenerateNumericTextbox(uc.LineId, "ReqArcLength2",Convert.ToString(uc.ReqArcLength2), "", true, "", false, "10","PROD"),
                               Helper.GenerateNumericTextbox(uc.LineId, "ReqArcLength3",Convert.ToString(uc.ReqArcLength3), "", true, "", false, "10","PROD"),
                               Helper.GenerateNumericTextbox(uc.LineId, "ActualArc1", Convert.ToString(uc.ActualArc1), "", true, "", false, "10","PROD"),
                               Helper.GenerateNumericTextbox(uc.LineId, "ActualArc2", Convert.ToString(uc.ActualArc2), "", true, "", false, "10","PROD"),
                               Helper.GenerateNumericTextbox(uc.LineId, "ActualArc3", Convert.ToString(uc.ActualArc3), "", true, "", false, "10","PROD"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "RemarkLine2_",  Convert.ToString(uc.Remark), "", true, "", false, "100","PROD"),

                                Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","")
                               + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.QC3.GetStringValue().ToUpper()? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine2Linkage','tblProtocolLines2')") : "")
                                }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine2Linkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRL007 objPRL007 = new PRL007();
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;

                if (refHeaderId > 0)
                {
                    string strSeamNo = !string.IsNullOrEmpty(fc["SeamNo" + refLineId]) ? Convert.ToString(fc["SeamNo" + refLineId]).Trim() : "";
                    string strReqOrien = !string.IsNullOrEmpty(fc["ReqOrien" + refLineId]) ? Convert.ToString(fc["ReqOrien" + refLineId]).Trim() : "";
                    string strRemark = !string.IsNullOrEmpty(fc["RemarkLine2_" + refLineId]) ? Convert.ToString(fc["RemarkLine2_" + refLineId]).Trim() : "";
                    decimal? decReqArcLength1 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["ReqArcLength1" + refLineId]));
                    decimal? decReqArcLength2 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["ReqArcLength2" + refLineId]));
                    decimal? decReqArcLength3 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["ReqArcLength3" + refLineId]));
                    decimal? decActualArc1 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["ActualArc1" + refLineId]));
                    decimal? decActualArc2 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["ActualArc2" + refLineId]));
                    decimal? decActualArc3 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["ActualArc3" + refLineId]));

                    #region Add New Spot Line1
                    if (refLineId > 0)
                    {
                        objPRL007 = db.PRL007.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRL007.EditedBy = objClsLoginInfo.UserName;
                        objPRL007.EditedOn = DateTime.Now;
                    }

                    objPRL007.HeaderId = refHeaderId;
                    objPRL007.SeamNo = strSeamNo;
                    objPRL007.ReqOrien = strReqOrien;
                    objPRL007.ReqArcLength1 = decReqArcLength1;
                    objPRL007.ReqArcLength2 = decReqArcLength2;
                    objPRL007.ReqArcLength3 = decReqArcLength3;
                    objPRL007.ActualArc1 = decActualArc1;
                    objPRL007.ActualArc2 = decActualArc2;
                    objPRL007.ActualArc3 = decActualArc3;
                    objPRL007.Remark = strRemark;
                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = "Data updated successfully";
                    }
                    else
                    {
                        objPRL007.CreatedBy = objClsLoginInfo.UserName;
                        objPRL007.CreatedOn = DateTime.Now;
                        db.PRL007.Add(objPRL007);
                        db.SaveChanges();
                        objResponseMsg.Value = "Data added successfully";
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRL007.LineId;
                    #endregion

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine2Linkage(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRL007 objPRL007 = db.PRL007.FirstOrDefault(c => c.LineId == lineId);
                if (objPRL007 != null)
                {
                    db.PRL007.Remove(objPRL007);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line3 details
        [HttpPost]
        public ActionResult GetProtocolLines3DataLinkage(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro.Location like '%" + param.sSearch
                        + "%' or pro.At0 like '%" + param.sSearch
                        + "%' or pro.At90 like '%" + param.sSearch
                        + "%' or pro.At180 like '%" + param.sSearch
                        + "%' or pro.At270 like '%" + param.sSearch
                        + "%' or pro.Remark like '%" + param.sSearch
                        + "%')";
                }
                var lstResult = db.SP_IPI_PROTOCOL002_LINKAGE_GET_LINES3_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                var lstOutBy = db.SP_IPI_PROTOCOL002_LINKAGE_GET_OUTBY(intHeaderId).FirstOrDefault();
                var outBy0 = lstOutBy.OutBy0;
                var outBy90 = lstOutBy.OutBy90;
                var outBy180 = lstOutBy.OutBy180;
                var outBy270 = lstOutBy.OutBy270;
                //var lstResult = db.SP_IPI_PROTOCOL002_GET_LINES3_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                       "3",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Location", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "At0", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "At90", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "At180", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "At270", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "RemarkLine3_", "", "", false, "", false, "100","PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine3(0);")
                                };

                var data = (from uc in lstResult
                            select new[]
                            {
                                   "3",
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                               Helper.GenerateHTMLTextbox(uc.LineId, "Location", Convert.ToString(uc.Location), "", true, "", false, "10","PROD"),
                               Helper.GenerateNumericTextbox(uc.LineId, "At0",   Convert.ToString(uc.At0), "", true, "", false, "10","PROD"),
                               Helper.GenerateNumericTextbox(uc.LineId, "At90",  Convert.ToString(uc.At90), "", true, "", false, "10","PROD"),
                               Helper.GenerateNumericTextbox(uc.LineId, "At180", Convert.ToString(uc.At180), "", true, "", false, "10","PROD"),
                               Helper.GenerateNumericTextbox(uc.LineId, "At270", Convert.ToString(uc.At270), "", true, "", false, "10","PROD"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "RemarkLine3_", Convert.ToString(uc.Remark), "", true, "", false, "100","PROD"),

                                 Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","")
                               + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.QC3.GetStringValue().ToUpper() ? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine3Linkage','tblProtocolLines3')") : "")
         }).ToList();

                var outBy = new[] {
                    "",
                                   Convert.ToString(headerId),
                                   "",
                                   "<strong>Out By</strong>",
                                   Helper.GenerateNumericTextbox(0, "OutBy",  outBy0.ToString(), "", true, "", false),
                                   Helper.GenerateNumericTextbox(90, "OutBy", outBy90.ToString(), "", true, "", false),
                                   Helper.GenerateNumericTextbox(180, "OutBy",outBy180.ToString(), "", true, "",false),
                                   Helper.GenerateNumericTextbox(270, "OutBy",outBy270.ToString(), "", true, "",false),
                                   "",
                                   ""
                                    };
                data.Insert(0, newRecord);
                data.Insert(data.Count, outBy);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition,
                    outby = true
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine3Linkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRL008 objPRL008 = new PRL008();
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;

                if (refHeaderId > 0)
                {
                    string strLocation = !string.IsNullOrEmpty(fc["Location" + refLineId]) ? Convert.ToString(fc["Location" + refLineId]).Trim() : "";
                    string strRemark = !string.IsNullOrEmpty(fc["RemarkLine3_" + refLineId]) ? Convert.ToString(fc["RemarkLine3_" + refLineId]).Trim() : "";
                    decimal? decAt0 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["At0" + refLineId]));
                    decimal? decAt90 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["At90" + refLineId]));
                    decimal? decAt180 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["At180" + refLineId]));
                    decimal? decAt270 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["At270" + refLineId]));

                    #region Add New Spot Line1
                    if (refLineId > 0)
                    {
                        objPRL008 = db.PRL008.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRL008.EditedBy = objClsLoginInfo.UserName;
                        objPRL008.EditedOn = DateTime.Now;
                    }
                    objPRL008.HeaderId = refHeaderId;
                    objPRL008.Location = strLocation;
                    objPRL008.Remark = strRemark;
                    objPRL008.At0 = decAt0;
                    objPRL008.At90 = decAt90;
                    objPRL008.At180 = decAt180;
                    objPRL008.At270 = decAt270;

                    PRL005 objPRL005 = db.PRL005.Where(x => x.HeaderId == refHeaderId).FirstOrDefault();
                    objPRL005.OutBy0 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["OutBy0"]));
                    objPRL005.OutBy90 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["OutBy90"]));
                    objPRL005.OutBy180 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["OutBy180"]));
                    objPRL005.OutBy270 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["OutBy270"]));

                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = "Data updated successfully"; 
                    }
                    else
                    {
                        objPRL008.CreatedBy = objClsLoginInfo.UserName;
                        objPRL008.CreatedOn = DateTime.Now;
                        db.PRL008.Add(objPRL008);
                        db.SaveChanges();
                        objResponseMsg.Value = "Data added successfully";
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRL008.LineId;
                    #endregion
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine3Linkage(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRL008 objPRL008 = db.PRL008.FirstOrDefault(c => c.LineId == lineId);
                if (objPRL008 != null)
                {
                    db.PRL008.Remove(objPRL008);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line4 Width details
        [HttpPost]
        public ActionResult GetProtocolLines4WidthDataLinkage(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro.SpotNo like '%" + param.sSearch
                        + "%' or pro.Width like '%" + param.sSearch
                        + "%' or pro.Remarks like '%" + param.sSearch
                        + "%')";
                }
                var lstResult = db.SP_IPI_PROTOCOL002_LINKAGE_GET_LINES4_WIDTH_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                //var lstResult = db.SP_IPI_PROTOCOL002_GET_LINES4_WIDTH_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                    "4",
                    Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "SpotNoWidth", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "Width", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "RemarksWidth", "", "", false, "", false, "100","PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine4(0);")
                                };

                var data = (from uc in lstResult
                            select new[]
                            {
                               "4",
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                               Helper.GenerateHTMLTextbox(uc.LineId, "SpotNoWidth",  Convert.ToString(uc.SpotNo), "", true, "", false, "10","PROD"),
                               Helper.GenerateNumericTextbox(uc.LineId, "Width",     Convert.ToString(uc.Width), "", true, "", false, "10","PROD"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "RemarksWidth", Convert.ToString(uc.Remarks), "", true, "", false, "100","PROD"),
                               Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","")
                               + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.PROD3.GetStringValue()? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine4WidthLinkage','tblProtocolLines4Width')") : "")
           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine4Linkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRL009_1 objPRL009_1 = new PRL009_1();
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;

                if (refHeaderId > 0)
                {
                    string strSpotNo = !string.IsNullOrEmpty(fc["SpotNoWidth" + refLineId]) ? Convert.ToString(fc["SpotNoWidth" + refLineId]).Trim() : "";
                    string strRemarks = !string.IsNullOrEmpty(fc["RemarksWidth" + refLineId]) ? Convert.ToString(fc["RemarksWidth" + refLineId]).Trim() : "";
                    decimal? decWidth = (new clsManager()).GetDecimalValue(Convert.ToString(fc["Width" + refLineId]));

                    #region Add New Spot Line1
                    if (refLineId > 0)
                    {
                        objPRL009_1 = db.PRL009_1.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRL009_1.EditedBy = objClsLoginInfo.UserName;
                        objPRL009_1.EditedOn = DateTime.Now;
                    }
                    objPRL009_1.HeaderId = refHeaderId;
                    objPRL009_1.SpotNo = strSpotNo;
                    objPRL009_1.Remarks = strRemarks;
                    objPRL009_1.Width = decWidth;
                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.SpotUpdate;
                    }
                    else
                    {
                        objPRL009_1.CreatedBy = objClsLoginInfo.UserName;
                        objPRL009_1.CreatedOn = DateTime.Now;
                        db.PRL009_1.Add(objPRL009_1);
                        db.SaveChanges();
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.SpotAdded;
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRL009_1.LineId;
                    #endregion
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine4WidthLinkage(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRL009_1 objPRL009_1 = db.PRL009_1.FirstOrDefault(c => c.LineId == lineId);
                if (objPRL009_1 != null)
                {
                    db.PRL009_1.Remove(objPRL009_1);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line4 Depth details
        [HttpPost]
        public ActionResult GetProtocolLines4DepthDataLinkage(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro.SpotNo like '%" + param.sSearch
                        + "%' or pro.Depth like '%" + param.sSearch
                        + "%' or pro.Remarks like '%" + param.sSearch
                        + "%')";
                }
                var lstResult = db.SP_IPI_PROTOCOL002_LINKAGE_GET_LINES4_DEPTH_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                //var lstResult = db.SP_IPI_PROTOCOL002_GET_LINES4_DEPTH_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                                    "5",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "SpotNoDepth", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "Depth", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "RemarksDepth", "", "", false, "", false, "100","PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine5(0);")
                                };

                var data = (from uc in lstResult
                            select new[]
                            {
                               "5",
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                               Helper.GenerateHTMLTextbox(uc.LineId, "SpotNoDepth",  Convert.ToString(uc.SpotNo),"", true, "", false, "10","PROD"),
                               Helper.GenerateNumericTextbox(uc.LineId, "Depth",   Convert.ToString(uc.Depth),"", true, "", false, "10","PROD"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "RemarksDepth",Convert.ToString(uc.Remarks), "", true, "", false, "100","PROD"),

                               Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.PROD3.GetStringValue()? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine4DepthLinkage','tblProtocolLines4Depth')") : "")
                }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine5Linkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRL009_2 objPRL009_2 = new PRL009_2();
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;

                if (refHeaderId > 0)
                {
                    string strSpotNo = !string.IsNullOrEmpty(fc["SpotNoDepth" + refLineId]) ? Convert.ToString(fc["SpotNoDepth" + refLineId]).Trim() : "";
                    string strRemarks = !string.IsNullOrEmpty(fc["RemarksDepth" + refLineId]) ? Convert.ToString(fc["RemarksDepth" + refLineId]).Trim() : "";
                    decimal? decDepth = (new clsManager()).GetDecimalValue(Convert.ToString(fc["Depth" + refLineId]));

                    #region Add New Spot Line1
                    if (refLineId > 0)
                    {
                        objPRL009_2 = db.PRL009_2.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRL009_2.EditedBy = objClsLoginInfo.UserName;
                        objPRL009_2.EditedOn = DateTime.Now;
                    }
                    objPRL009_2.HeaderId = refHeaderId;
                    objPRL009_2.SpotNo = strSpotNo;
                    objPRL009_2.Remarks = strRemarks;
                    objPRL009_2.Depth = decDepth;
                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.SpotUpdate;
                    }
                    else
                    {
                        objPRL009_2.CreatedBy = objClsLoginInfo.UserName;
                        objPRL009_2.CreatedOn = DateTime.Now;
                        db.PRL009_2.Add(objPRL009_2);
                        db.SaveChanges();
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.SpotAdded;
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRL009_2.LineId;
                    #endregion
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine4DepthLinkage(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRL009_2 objPRL009_2 = db.PRL009_2.FirstOrDefault(c => c.LineId == lineId);
                if (objPRL009_2 != null)
                {
                    db.PRL009_2.Remove(objPRL009_2);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #endregion
    }
}