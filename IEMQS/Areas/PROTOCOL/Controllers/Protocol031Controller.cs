﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol031Controller : clsBase
    {
        // GET: PROTOCOL/Protocol017
        string ControllerURL = "/PROTOCOL/Protocol031/";
        string Title = "REPORT FOR VISUAL AND DIM OF TAILING LUG";
        #region Details View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id,int? m)
        {
            PRO150 objPRO150 = new PRO150();
            if (!id.HasValue)
            {
                objPRO150.ProtocolNo = string.Empty;
                objPRO150.CreatedBy = objClsLoginInfo.UserName;
                objPRO150.CreatedOn = DateTime.Now;
                db.PRO150.Add(objPRO150);
                db.SaveChanges();
                ViewBag.HeaderId = objPRO150.HeaderId;
            }
            else
            {
                objPRO150 = db.PRO150.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            return View(objPRO150);

        }
        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO150 PRO150)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO150.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO150 objPRO150 = db.PRO150.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO150.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO150.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data
                        objPRO150.ProtocolNo = PRO150.ProtocolNo;
                        objPRO150.Note1 = PRO150.Note1;
                        objPRO150.Note2 = PRO150.Note2;
                        objPRO150.EditedBy = objClsLoginInfo.UserName;
                        objPRO150.EditedOn = DateTime.Now;
                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol saved successfully";
                        objResponseMsg.HeaderId = objPRO150.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO150 objPRO150 = db.PRO150.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO150 != null && string.IsNullOrWhiteSpace(objPRO150.ProtocolNo))
                    {
                        db.PRO151.RemoveRange(objPRO150.PRO151.ToList());
                       
                        db.PRO150.Remove(objPRO150);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.REPORT_FOR_VISUAL_AND_DIM_OF_TAILING_LUG.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO150.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public ActionResult GetProtocolLines1Data(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro.DIMENSION like '%" + param.sSearch
                        + "%' or pro.Required like '%" + param.sSearch
                        + "%' or pro.Actual like '%" + param.sSearch
                        + "%' or pro.Remark like '%" + param.sSearch
                        + "%')";
                }

                var lstResult = db.SP_IPI_PROTOCOL031_GET_LINES1_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                                   "1",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "DIMENSION", "", "", true, "", false, "20", "QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Required", "", "", true, "", false, "50", "QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Actual", "", "", true, "", false, "50", "PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Remark", "", "", true, "", false, "100", "QC"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine1(0);")
                                };
                var data = (from uc in lstResult
                            select new[]
                            {
                               "1",
                                Convert.ToString(headerId),
                                Convert.ToString(uc.LineId),
                                Helper.GenerateHTMLTextbox(uc.LineId, "DIMENSION", uc.DIMENSION, "", true, "", false, "20", "QC"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "Required", uc.Required, "", true, "", false, "50", "QC"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "Actual", uc.Actual, "", true, "", false, "50", "PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "Remark", uc.Remark, "", true, "", false, "100", "QC"),
                                  HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.QC3.GetStringValue().ToUpper() ? HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine1','tblProtocolLines1')") : "")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult SaveProtocolLine1(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRO151 objPRO151 = null;
            int newRowIndex = 0;
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                if (refHeaderId > 0)
                {
                    int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
                    if (refLineId > 0)
                    {
                        objPRO151 = db.PRO151.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();

                        objPRO151.DIMENSION = !string.IsNullOrEmpty(fc["DIMENSION" + refLineId]) ? Convert.ToString(fc["DIMENSION" + refLineId]).Trim() : "";
                        objPRO151.Required = !string.IsNullOrEmpty(fc["Required" + refLineId]) ? Convert.ToString(fc["Required" + refLineId]).Trim() : "";
                        objPRO151.Actual = !string.IsNullOrEmpty(fc["Actual" + refLineId]) ? Convert.ToString(fc["Actual" + refLineId]).Trim() : "";
                        objPRO151.Remark = !string.IsNullOrEmpty(fc["Remark" + refLineId]) ? Convert.ToString(fc["Remark" + refLineId]).Trim() : "";

                        objPRO151.EditedBy = objClsLoginInfo.UserName;
                        objPRO151.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Value = "Data updated successfully";
                    }
                    else
                    {
                        objPRO151 = new PRO151();
                        objPRO151.HeaderId = refHeaderId;
                        objPRO151.DIMENSION = !string.IsNullOrEmpty(fc["DIMENSION" + newRowIndex]) ? Convert.ToString(fc["DIMENSION" + newRowIndex]).Trim() : "";
                        objPRO151.Required = !string.IsNullOrEmpty(fc["Required" + newRowIndex]) ? Convert.ToString(fc["Required" + newRowIndex]).Trim() : "";
                        objPRO151.Actual = !string.IsNullOrEmpty(fc["Actual" + newRowIndex]) ? Convert.ToString(fc["Actual" + newRowIndex]).Trim() : "";
                        objPRO151.Remark = !string.IsNullOrEmpty(fc["Remark" + newRowIndex]) ? Convert.ToString(fc["Remark" + newRowIndex]).Trim() : "";

                        objPRO151.CreatedBy = objClsLoginInfo.UserName;
                        objPRO151.CreatedOn = DateTime.Now;
                        db.PRO151.Add(objPRO151);
                        db.SaveChanges();
                        objResponseMsg.Value = "Data added successfully";
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRO151.LineId;

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeleteProtocolLine1(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRO151 objPRO151 = db.PRO151.FirstOrDefault(c => c.LineId == lineId);
                if (objPRO151 != null)
                {
                    db.PRO151.Remove(objPRO151);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

        #region Linkage View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL150 objPRL150 = new PRL150();
            objPRL150 = db.PRL150.Where(i => i.HeaderId == id).FirstOrDefault();

            if (objPRL150 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL150.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objQproject.ManufacturingCode;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            if (m == 1)
            {
                ViewBag.isEditable = "true";
            }
            else
            { ViewBag.isEditable = "false"; }

            return View(objPRL150);
        }
        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL150 PRL150)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRL150.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL150 objPRL150 = db.PRL150.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL150.DCRNo = PRL150.DCRNo;
                    objPRL150.SeamNo = PRL150.SeamNo;
                    objPRL150.ProtocolNo = PRL150.ProtocolNo != null ? PRL150.ProtocolNo : "";
                    objPRL150.DrawingRevisionNo = PRL150.DrawingRevisionNo;
                    objPRL150.ManufacturingCode= PRL150.ManufacturingCode;
                    objPRL150.Note1 = PRL150.Note1;
                    objPRL150.Note2 = PRL150.Note2;

                    objPRL150.EditedBy = objClsLoginInfo.UserName;
                    objPRL150.EditedOn = DateTime.Now;
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol saved successfully";
                    objResponseMsg.HeaderId = objPRL150.HeaderId;
                    #endregion
                 }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL150 objPRL150 = db.PRL150.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL150 != null && string.IsNullOrWhiteSpace(objPRL150.ProtocolNo))
                    {
                        db.PRL151.RemoveRange(objPRL150.PRL151.ToList());


                        db.PRL150.Remove(objPRL150);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL150.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion  

        #region Line1 details
        [HttpPost]
        public ActionResult GetProtocolLines1DataLinkage(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro.DIMENSION like '%" + param.sSearch
                        + "%' or pro.Required like '%" + param.sSearch
                        + "%' or pro.Actual like '%" + param.sSearch
                        + "%' or pro.Remark like '%" + param.sSearch
                       
                        + "%')";
                }

                var lstResult = db.SP_IPI_PROTOCOL031_LINKAGE_GET_LINES1_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                                   "1",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "DIMENSION", "", "", true, "", false, "20", "QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Required", "", "", true, "", false, "50", "QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Actual", "", "", true, "", false, "50", "PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Remark", "", "", true, "", false, "100", "QC"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine1(0);")
                                };
                var data = (from uc in lstResult
                            select new[]
                            {
                                "1",
                                Convert.ToString(headerId),
                                Convert.ToString(uc.LineId),
                                Helper.GenerateHTMLTextbox(uc.LineId, "DIMENSION", uc.DIMENSION, "", true, "", false, "20", "QC"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "Required", uc.Required, "", true, "", false, "50", "QC"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "Actual", uc.Actual, "", true, "", false, "50", "PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "Remark", uc.Remark, "", true, "", false, "100", "QC"),
                                        HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.QC3.GetStringValue().ToUpper() ? HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine1Linkage','tblProtocolLines1')") : "")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine1Linkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRL151 objPRL151 = null;
            int newRowIndex = 0;
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                if (refHeaderId > 0)
                {
                    int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
                    if (refLineId > 0)
                    {
                        objPRL151 = db.PRL151.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();

                        objPRL151.DIMENSION = !string.IsNullOrEmpty(fc["DIMENSION" + refLineId]) ? Convert.ToString(fc["DIMENSION" + refLineId]).Trim() : "";
                        objPRL151.Required = !string.IsNullOrEmpty(fc["Required" + refLineId]) ? Convert.ToString(fc["Required" + refLineId]).Trim() : "";
                        objPRL151.Actual = !string.IsNullOrEmpty(fc["Actual" + refLineId]) ? Convert.ToString(fc["Actual" + refLineId]).Trim() : "";
                        objPRL151.Remark = !string.IsNullOrEmpty(fc["Remark" + refLineId]) ? Convert.ToString(fc["Remark" + refLineId]).Trim() : "";
                       
                        objPRL151.EditedBy = objClsLoginInfo.UserName;
                        objPRL151.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Value = "Data updated successfully";
                    }
                    else
                    {
                        objPRL151 = new PRL151();
                        objPRL151.HeaderId = refHeaderId;
                        objPRL151.DIMENSION = !string.IsNullOrEmpty(fc["DIMENSION" + newRowIndex]) ? Convert.ToString(fc["DIMENSION" + newRowIndex]).Trim() : "";
                        objPRL151.Required = !string.IsNullOrEmpty(fc["Required" + newRowIndex]) ? Convert.ToString(fc["Required" + newRowIndex]).Trim() : "";
                        objPRL151.Actual = !string.IsNullOrEmpty(fc["Actual" + newRowIndex]) ? Convert.ToString(fc["Actual" + newRowIndex]).Trim() : "";
                        objPRL151.Remark = !string.IsNullOrEmpty(fc["Remark" + newRowIndex]) ? Convert.ToString(fc["Remark" + newRowIndex]).Trim() : "";
                        objPRL151.CreatedBy = objClsLoginInfo.UserName;
                        objPRL151.CreatedOn = DateTime.Now;
                        db.PRL151.Add(objPRL151);
                        db.SaveChanges();
                        objResponseMsg.Value = "Data added successfully";
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRL151.LineId;

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolLine1Linkage(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRL151 objPRL151 = db.PRL151.FirstOrDefault(c => c.LineId == lineId);
                if (objPRL151 != null)
                {
                    db.PRL151.Remove(objPRL151);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #endregion
        public string HTMLActionString(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", string onHrefURL = "", bool isDisable = false)
        {
            string htmlControl = "";
            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? onClickMethod : "";
            string onHref = !string.IsNullOrEmpty(onHrefURL) ? "href='" + onHrefURL + "'" : "";

            if (isDisable)
            {
                htmlControl += "<i id=\"" + inputID.ToString() + "\" name=\"" + inputID + "\" ></i>";
                //htmlControl = "<i id=\"" + inputID + "' name='" + inputID + "' style='cursor: pointer; opacity:0.3; margin-right:10px;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";
            }
            else
            {
                htmlControl += "<i id=\"" + inputID.ToString() + "\" name=\"" + inputID + "\"  style='cursor: pointer; margin-right:10px;' Title=\"" + buttonTooltip + "\" class=\"" + className + "\"  onclick=\"" + onClickEvent + "\" ></i>";
                //htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer; margin-right:10px;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
            }

            if (!string.IsNullOrEmpty(onHref))
            {
                htmlControl = "<a " + onHref + " >" + htmlControl + "</a>";
            }

            return htmlControl;
        }

    }

}    