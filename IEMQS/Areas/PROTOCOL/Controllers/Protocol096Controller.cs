﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol096Controller : clsBase
    {
        // GET: PROTOCOL/Protocol096
        string ControllerURL = "/PROTOCOL/Protocol096/";
        string Title = "Final Dimension Inspection of Lattice Beams";

        #region Details View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO475 objPRO475 = new PRO475();
            if (!id.HasValue)
            {
                try
                {
                    objPRO475.ProtocolNo = string.Empty;
                    objPRO475.CreatedBy = objClsLoginInfo.UserName;
                    objPRO475.CreatedOn = DateTime.Now;

                    #region PRO477
                    objPRO475.PRO477.Add(new PRO477
                    {
                        SrNo = "1",
                        Description = "TOP FLANGE (Length)",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO475.PRO477.Add(new PRO477
                    {
                        SrNo = "2",
                        Description = "TOP FLANGE (Width)",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO475.PRO477.Add(new PRO477
                    {
                        SrNo = "3",
                        Description = "TOP FLANGE (Thickness)",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO475.PRO477.Add(new PRO477
                    {
                        SrNo = "4",
                        Description = "TOP FLANGE (Notch)",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO475.PRO477.Add(new PRO477
                    {
                        SrNo = "5",
                        Description = "BOTTOM FLANGE (Length)",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO475.PRO477.Add(new PRO477
                    {
                        SrNo = "6",
                        Description = "BOTTOM FLANGE (Width)",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO475.PRO477.Add(new PRO477
                    {
                        SrNo = "7",
                        Description = "BOTTOM FLANGE (Thickness)",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO475.PRO477.Add(new PRO477
                    {
                        SrNo = "8",
                        Description = "BEAM HEIGHT",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO475.PRO477.Add(new PRO477
                    {
                        SrNo = "9",
                        Description = "WEB (Length)",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO475.PRO477.Add(new PRO477
                    {
                        SrNo = "10",
                        Description = "WEB (Thickness)",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO475.PRO477.Add(new PRO477
                    {
                        SrNo = "11",
                        Description = "SLOTS IN WEB (Slot Qty)",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO475.PRO477.Add(new PRO477
                    {
                        SrNo = "12",
                        Description = "SLOTS IN WEB (Distance from CL)",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO475.PRO477.Add(new PRO477
                    {
                        SrNo = "13",
                        Description = "SLOTS IN WEB (Elevation from bottom flange)",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO475.PRO477.Add(new PRO477
                    {
                        SrNo = "14",
                        Description = "SLOTS IN WEB (Distance between slots)",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO475.PRO477.Add(new PRO477
                    {
                        SrNo = "15",
                        Description = "TRIANGULAR CUTOUTS IN WEB (Dimension)",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO475.PRO477.Add(new PRO477
                    {
                        SrNo = "16",
                        Description = "TRIANGULAR CUTOUTS IN WEB (Pitch)",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO475.PRO477.Add(new PRO477
                    {
                        SrNo = "17",
                        Description = "VENT HOLES IN WEB (Diameter)",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO475.PRO477.Add(new PRO477
                    {
                        SrNo = "18",
                        Description = "VENT HOLES IN WEB (Qty)",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO475.PRO477.Add(new PRO477
                    {
                        SrNo = "19",
                        Description = "VENT HOLES IN WEB (Location)",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO475.PRO477.Add(new PRO477
                    {
                        SrNo = "20",
                        Description = "CAMBER",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO475.PRO477.Add(new PRO477
                    {
                        SrNo = "21",
                        Description = "SWEEP",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO475.PRO477.Add(new PRO477
                    {
                        SrNo = "22",
                        Description = "LEVLNESS OF EACH SEATING SURFACE",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO475.PRO477.Add(new PRO477
                    {
                        SrNo = "23",
                        Description = "MAXIMUM OUT OF SQUARENESS OF TOP FLANGE ACROSS WIDTH",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion

                    db.PRO475.Add(objPRO475);
                    db.SaveChanges();
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO475.HeaderId;
            }
            else
            {
                objPRO475 = db.PRO475.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO476> lstPRO476 = db.PRO476.Where(x => x.HeaderId == objPRO475.HeaderId).ToList();
            List<PRO477> lstPRO477 = db.PRO477.Where(x => x.HeaderId == objPRO475.HeaderId).ToList();
            List<PRO478> lstPRO478 = db.PRO478.Where(x => x.HeaderId == objPRO475.HeaderId).ToList();

            ViewBag._lstPRO476 = lstPRO476;
            ViewBag._lstPRO477 = lstPRO477;
            ViewBag._lstPRO478 = lstPRO478;

            #endregion

            return View(objPRO475);
            //return View();
        }

        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO475 PRO475)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO475.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO475 objPRO475 = db.PRO475.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO475.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO475.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data

                        objPRO475.ProtocolNo = PRO475.ProtocolNo;

                        objPRO475.ReqActualValue1 = PRO475.ReqActualValue1;
                        objPRO475.ReqActualValue2 = PRO475.ReqActualValue2;

                        objPRO475.CheckPoint1 = PRO475.CheckPoint1;
                        objPRO475.CheckPoint2 = PRO475.CheckPoint2;
                        objPRO475.CheckPoint4_1_1 = PRO475.CheckPoint4_1_1;
                        objPRO475.CheckPoint4_1_2 = PRO475.CheckPoint4_1_2;
                        objPRO475.CheckPoint4_2_1 = PRO475.CheckPoint4_2_1;
                        objPRO475.CheckPoint4_2_2 = PRO475.CheckPoint4_2_2;
                        objPRO475.CheckPoint4_3_1 = PRO475.CheckPoint4_3_1;
                        objPRO475.CheckPoint4_3_2 = PRO475.CheckPoint4_3_2;
                        objPRO475.CheckPoint4_4_1 = PRO475.CheckPoint4_4_1;
                        objPRO475.CheckPoint4_4_2 = PRO475.CheckPoint4_4_2;
                        objPRO475.CheckPoint4_5_1 = PRO475.CheckPoint4_5_1;
                        objPRO475.CheckPoint4_5_2 = PRO475.CheckPoint4_5_2;

                        objPRO475.QCRemarks = PRO475.QCRemarks;
                        objPRO475.Result = PRO475.Result;

                        objPRO475.EditedBy = objClsLoginInfo.UserName;
                        objPRO475.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO475.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO475 objPRO475 = db.PRO475.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO475 != null && string.IsNullOrWhiteSpace(objPRO475.ProtocolNo))
                    {
                        db.PRO475.Remove(objPRO475);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO475.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO476> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO476> lstAddPRO476 = new List<PRO476>();
                List<PRO476> lstDeletePRO476 = new List<PRO476>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO476 obj = db.PRO476.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.DrgNo = item.DrgNo;
                                obj.RevNo = item.RevNo;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO476 obj = new PRO476();

                            if (!string.IsNullOrWhiteSpace(item.DrgNo))
                            {
                                obj.HeaderId = item.HeaderId;
                                obj.DrgNo = item.DrgNo;
                                obj.RevNo = item.RevNo;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO476.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO476.Count > 0)
                    {
                        db.PRO476.AddRange(lstAddPRO476);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO476 = db.PRO476.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO476.Count > 0)
                    {
                        db.PRO476.RemoveRange(lstDeletePRO476);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO476 = db.PRO476.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO476.Count > 0)
                    {
                        db.PRO476.RemoveRange(lstDeletePRO476);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2(List<PRO477> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO477> lstAddPRO477 = new List<PRO477>();
                List<PRO477> lstDeletePRO477 = new List<PRO477>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO477 obj = db.PRO477.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO477();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SrNo = item.SrNo;
                        obj.Description = item.Description;
                        obj.ReqDimension = item.ReqDimension;
                        obj.ActDimension1 = item.ActDimension1;
                        obj.ActDimension2 = item.ActDimension2;
                        obj.Remarks = item.Remarks;

                        if (isAdded)
                        {
                            lstAddPRO477.Add(obj);
                        }
                    }
                    if (lstAddPRO477.Count > 0)
                    {
                        db.PRO477.AddRange(lstAddPRO477);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO477 = db.PRO477.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO477.Count > 0)
                    {
                        db.PRO477.RemoveRange(lstDeletePRO477);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO477 = db.PRO477.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO477.Count > 0)
                    {
                        db.PRO477.RemoveRange(lstDeletePRO477);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3(List<PRO478> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO478> lstAddPRO478 = new List<PRO478>();
                List<PRO478> lstDeletePRO478 = new List<PRO478>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO478 obj = db.PRO478.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO478();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.MaterialIdentification = item.MaterialIdentification;

                        if (isAdded)
                        {
                            lstAddPRO478.Add(obj);
                        }
                    }
                    if (lstAddPRO478.Count > 0)
                    {
                        db.PRO478.AddRange(lstAddPRO478);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO478 = db.PRO478.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO478.Count > 0)
                    {
                        db.PRO478.RemoveRange(lstDeletePRO478);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO478 = db.PRO478.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO478.Count > 0)
                    {
                        db.PRO478.RemoveRange(lstDeletePRO478);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

        #region Linkage View Code

        #region Header
        public ActionResult Linkage(int? id, int? m)
        {
            PRL475 objPRL475 = new PRL475();
            objPRL475 = db.PRL475.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL475 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL475.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL476> lstPRL476 = db.PRL476.Where(x => x.HeaderId == objPRL475.HeaderId).ToList();
            List<PRL477> lstPRL477 = db.PRL477.Where(x => x.HeaderId == objPRL475.HeaderId).ToList();
            List<PRL478> lstPRL478 = db.PRL478.Where(x => x.HeaderId == objPRL475.HeaderId).ToList();

            ViewBag._lstPRL476 = lstPRL476;
            ViewBag._lstPRL477 = lstPRL477;
            ViewBag._lstPRL478 = lstPRL478;

            #endregion

            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.DrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL475.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL475.ActFilledBy) && (objPRL475.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL475.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL475);

            //return View();
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL475 prl475, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = prl475.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL475 objPRL475 = db.PRL475.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL475.DrawingRevisionNo = prl475.DrawingRevisionNo;
                    objPRL475.DrawingNo = prl475.DrawingNo;
                    objPRL475.DCRNo = prl475.DCRNo;
                    objPRL475.ProtocolNo = prl475.ProtocolNo;

                    objPRL475.ReqActualValue1 = prl475.ReqActualValue1;
                    objPRL475.ReqActualValue2 = prl475.ReqActualValue2;

                    objPRL475.CheckPoint1 = prl475.CheckPoint1;
                    objPRL475.CheckPoint2 = prl475.CheckPoint2;
                    objPRL475.CheckPoint4_1_1 = prl475.CheckPoint4_1_1;
                    objPRL475.CheckPoint4_1_2 = prl475.CheckPoint4_1_2;
                    objPRL475.CheckPoint4_2_1 = prl475.CheckPoint4_2_1;
                    objPRL475.CheckPoint4_2_2 = prl475.CheckPoint4_2_2;
                    objPRL475.CheckPoint4_3_1 = prl475.CheckPoint4_3_1;
                    objPRL475.CheckPoint4_3_2 = prl475.CheckPoint4_3_2;
                    objPRL475.CheckPoint4_4_1 = prl475.CheckPoint4_4_1;
                    objPRL475.CheckPoint4_4_2 = prl475.CheckPoint4_4_2;
                    objPRL475.CheckPoint4_5_1 = prl475.CheckPoint4_5_1;
                    objPRL475.CheckPoint4_5_2 = prl475.CheckPoint4_5_2;

                    objPRL475.QCRemarks = prl475.QCRemarks;
                    objPRL475.Result = prl475.Result;

                    objPRL475.EditedBy = UserName;
                    objPRL475.EditedOn = DateTime.Now;
                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL475.ActFilledBy = UserName;
                            objPRL475.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL475.ReqFilledBy = UserName;
                            objPRL475.ReqFilledOn = DateTime.Now;
                        }
                    }
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL475.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL475 objPRL475 = db.PRL475.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL475 != null && string.IsNullOrWhiteSpace(objPRL475.ProtocolNo))
                    {
                        db.PRL475.Remove(objPRL475);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL475.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL476> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRL476> lstAddPRL476 = new List<PRL476>();
                List<PRL476> lstDeletePRL476 = new List<PRL476>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRL476 obj = db.PRL476.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.DrgNo = item.DrgNo;
                                obj.RevNo = item.RevNo;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRL476 obj = new PRL476();

                            if (!string.IsNullOrWhiteSpace(item.DrgNo))
                            {
                                obj.HeaderId = item.HeaderId;
                                obj.DrgNo = item.DrgNo;
                                obj.RevNo = item.RevNo;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRL476.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRL476.Count > 0)
                    {
                        db.PRL476.AddRange(lstAddPRL476);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRL476 = db.PRL476.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRL476.Count > 0)
                    {
                        db.PRL476.RemoveRange(lstDeletePRL476);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL476 = db.PRL476.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL476.Count > 0)
                    {
                        db.PRL476.RemoveRange(lstDeletePRL476);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2Linkage(List<PRL477> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRL477> lstAddPRL477 = new List<PRL477>();
                List<PRL477> lstDeletePRL477 = new List<PRL477>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL477 obj = db.PRL477.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL477();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SrNo = item.SrNo;
                        obj.Description = item.Description;
                        obj.ReqDimension = item.ReqDimension;
                        obj.ActDimension1 = item.ActDimension1;
                        obj.ActDimension2 = item.ActDimension2;
                        obj.Remarks = item.Remarks;

                        if (isAdded)
                        {
                            lstAddPRL477.Add(obj);
                        }
                    }
                    if (lstAddPRL477.Count > 0)
                    {
                        db.PRL477.AddRange(lstAddPRL477);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL477 = db.PRL477.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL477.Count > 0)
                    {
                        db.PRL477.RemoveRange(lstDeletePRL477);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL477 = db.PRL477.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL477.Count > 0)
                    {
                        db.PRL477.RemoveRange(lstDeletePRL477);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3Linkage(List<PRL478> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRL478> lstAddPRL478 = new List<PRL478>();
                List<PRL478> lstDeletePRL478 = new List<PRL478>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL478 obj = db.PRL478.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL478();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.MaterialIdentification = item.MaterialIdentification;

                        if (isAdded)
                        {
                            lstAddPRL478.Add(obj);
                        }
                    }
                    if (lstAddPRL478.Count > 0)
                    {
                        db.PRL478.AddRange(lstAddPRL478);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL478 = db.PRL478.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL478.Count > 0)
                    {
                        db.PRL478.RemoveRange(lstDeletePRL478);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL478 = db.PRL478.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL478.Count > 0)
                    {
                        db.PRL478.RemoveRange(lstDeletePRL478);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion
    }
}