﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol090Controller : clsBase
    {
        // GET: PROTOCOL/Protocol090
        string ControllerURL = "/PROTOCOL/Protocol090/";
        string Title = "INITIAL MACHINING VISUAL AND DIMENSION REPORT FOR GASKET FACE AND OTHER DIMENSIONS OF FLANGE";

        #region Details View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO445 objPRO445 = new PRO445();
            if (!id.HasValue)
            {
                try
                {
                    objPRO445.ProtocolNo = string.Empty;
                    objPRO445.CreatedBy = objClsLoginInfo.UserName;
                    objPRO445.CreatedOn = DateTime.Now;

                    #region  MACHINING | PRO447
                    objPRO445.PRO447.Add(new PRO447
                    {
                        SrNo = "1",
                        Description = "Identification",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO445.PRO447.Add(new PRO447
                    {
                        SrNo = "2",
                        Description = "Visual inspection",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO445.PRO447.Add(new PRO447
                    {
                        SrNo = "3",
                        Description = "Gasket face step OD",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO445.PRO447.Add(new PRO447
                    {
                        SrNo = "4",
                        Description = "Gasket height",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO445.PRO447.Add(new PRO447
                    {
                        SrNo = "5",
                        Description = "Gasket face finish",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO445.PRO447.Add(new PRO447
                    {
                        SrNo = "6",
                        Description = "Gasket step OD radius Top",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO445.PRO447.Add(new PRO447
                    {
                        SrNo = "7",
                        Description = "Gasket step OD radius Bottom",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO445.PRO447.Add(new PRO447
                    {
                        SrNo = "8",
                        Description = "Groove width",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO445.PRO447.Add(new PRO447
                    {
                        SrNo = "9",
                        Description = "Groove depth",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO445.PRO447.Add(new PRO447
                    {
                        SrNo = "10",
                        Description = "Groove corner radius",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO445.PRO447.Add(new PRO447
                    {
                        SrNo = "11",
                        Description = "Groove angle",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO445.PRO447.Add(new PRO447
                    {
                        SrNo = "12",
                        Description = "Groove taper finish",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO445.PRO447.Add(new PRO447
                    {
                        SrNo = "13",
                        Description = "Groove face finish",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO445.PRO447.Add(new PRO447
                    {
                        SrNo = "14",
                        Description = "Gasket groove P.C.D",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO445.PRO447.Add(new PRO447
                    {
                        SrNo = "15",
                        Description = "Final Visual inspection after O/L",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion

                    db.PRO445.Add(objPRO445);
                    db.SaveChanges();
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO445.HeaderId;
            }
            else
            {
                objPRO445 = db.PRO445.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO446> lstPRO446 = db.PRO446.Where(x => x.HeaderId == objPRO445.HeaderId).ToList();
            List<PRO447> lstPRO447 = db.PRO447.Where(x => x.HeaderId == objPRO445.HeaderId).ToList();

            ViewBag._lstPRO446 = lstPRO446;
            ViewBag._lstPRO447 = lstPRO447;

            #endregion

            return View(objPRO445);
            //return View();
        }

        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO445 PRO445)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO445.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO445 objPRO445 = db.PRO445.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO445.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO445.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data

                        objPRO445.ProtocolNo = PRO445.ProtocolNo;
                        objPRO445.NozzleNo = PRO445.NozzleNo;
                        objPRO445.GasketFaceRaisedFaceSeamNo = PRO445.GasketFaceRaisedFaceSeamNo;
                        objPRO445.PartDescription = PRO445.PartDescription;
                        objPRO445.GasketGrooveSeamNo = PRO445.GasketGrooveSeamNo;
                        objPRO445.ItemNo = PRO445.ItemNo;
                        objPRO445.CheckPoint1 = PRO445.CheckPoint1;
                        objPRO445.CheckPoint2 = PRO445.CheckPoint2;
                        objPRO445.CheckPoint4_1_1 = PRO445.CheckPoint4_1_1;
                        objPRO445.CheckPoint4_1_2 = PRO445.CheckPoint4_1_2;
                        objPRO445.CheckPoint4_2_1 = PRO445.CheckPoint4_2_1;
                        objPRO445.CheckPoint4_2_2 = PRO445.CheckPoint4_2_2;
                        objPRO445.CheckPoint4_3_1 = PRO445.CheckPoint4_3_1;
                        objPRO445.CheckPoint4_3_2 = PRO445.CheckPoint4_3_2;
                        objPRO445.CheckPoint4_4_1 = PRO445.CheckPoint4_4_1;
                        objPRO445.CheckPoint4_4_2 = PRO445.CheckPoint4_4_2;
                        objPRO445.CheckPoint4_5_1 = PRO445.CheckPoint4_5_1;
                        objPRO445.CheckPoint4_5_2 = PRO445.CheckPoint4_5_2;

                        objPRO445.QCRemarks = PRO445.QCRemarks;
                        objPRO445.Result = PRO445.Result;

                        objPRO445.EditedBy = objClsLoginInfo.UserName;
                        objPRO445.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO445.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO445 objPRO445 = db.PRO445.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO445 != null && string.IsNullOrWhiteSpace(objPRO445.ProtocolNo))
                    {
                        db.PRO445.Remove(objPRO445);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO445.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO446> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO446> lstAddPRO446 = new List<PRO446>();
                List<PRO446> lstDeletePRO446 = new List<PRO446>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO446 obj = db.PRO446.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.DrgNo = item.DrgNo;
                                obj.RevNo = item.RevNo;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO446 obj = new PRO446();

                            if (!string.IsNullOrWhiteSpace(item.DrgNo))
                            {
                                obj.HeaderId = item.HeaderId;
                                obj.DrgNo = item.DrgNo;
                                obj.RevNo = item.RevNo;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO446.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO446.Count > 0)
                    {
                        db.PRO446.AddRange(lstAddPRO446);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO446 = db.PRO446.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO446.Count > 0)
                    {
                        db.PRO446.RemoveRange(lstDeletePRO446);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO446 = db.PRO446.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO446.Count > 0)
                    {
                        db.PRO446.RemoveRange(lstDeletePRO446);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2(List<PRO447> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO447> lstAddPRO447 = new List<PRO447>();
                List<PRO447> lstDeletePRO447 = new List<PRO447>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO447 obj = db.PRO447.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO447();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SrNo = item.SrNo;
                        obj.Description = item.Description;
                        obj.ReqDimension = item.ReqDimension;
                        obj.ActDimension = item.ActDimension;
                        obj.Remarks = item.Remarks;

                        if (isAdded)
                        {
                            lstAddPRO447.Add(obj);
                        }
                    }
                    if (lstAddPRO447.Count > 0)
                    {
                        db.PRO447.AddRange(lstAddPRO447);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO447 = db.PRO447.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO447.Count > 0)
                    {
                        db.PRO447.RemoveRange(lstDeletePRO447);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO447 = db.PRO447.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO447.Count > 0)
                    {
                        db.PRO447.RemoveRange(lstDeletePRO447);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

        #region Linkage View Code

        #region Header
        public ActionResult Linkage(int? id, int? m)
        {
            PRL445 objPRL445 = new PRL445();
            objPRL445 = db.PRL445.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL445 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL445.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL446> lstPRL446 = db.PRL446.Where(x => x.HeaderId == objPRL445.HeaderId).ToList();
            List<PRL447> lstPRL447 = db.PRL447.Where(x => x.HeaderId == objPRL445.HeaderId).ToList();

            ViewBag._lstPRL446 = lstPRL446;
            ViewBag._lstPRL447 = lstPRL447;

            #endregion

            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.DrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL445.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL445.ActFilledBy) && (objPRL445.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL445.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL445);

            //return View();
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL445 prl445, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = prl445.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL445 objPRL445 = db.PRL445.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL445.DrawingRevisionNo = prl445.DrawingRevisionNo;
                    objPRL445.DrawingNo = prl445.DrawingNo;
                    objPRL445.DCRNo = prl445.DCRNo;
                    objPRL445.ProtocolNo = prl445.ProtocolNo;

                    objPRL445.ProtocolNo = prl445.ProtocolNo;
                    objPRL445.NozzleNo = prl445.NozzleNo;
                    objPRL445.GasketFaceRaisedFaceSeamNo = prl445.GasketFaceRaisedFaceSeamNo;
                    objPRL445.PartDescription = prl445.PartDescription;
                    objPRL445.GasketGrooveSeamNo = prl445.GasketGrooveSeamNo;
                    objPRL445.ItemNo = prl445.ItemNo;
                    objPRL445.CheckPoint1 = prl445.CheckPoint1;
                    objPRL445.CheckPoint2 = prl445.CheckPoint2;
                    objPRL445.CheckPoint4_1_1 = prl445.CheckPoint4_1_1;
                    objPRL445.CheckPoint4_1_2 = prl445.CheckPoint4_1_2;
                    objPRL445.CheckPoint4_2_1 = prl445.CheckPoint4_2_1;
                    objPRL445.CheckPoint4_2_2 = prl445.CheckPoint4_2_2;
                    objPRL445.CheckPoint4_3_1 = prl445.CheckPoint4_3_1;
                    objPRL445.CheckPoint4_3_2 = prl445.CheckPoint4_3_2;
                    objPRL445.CheckPoint4_4_1 = prl445.CheckPoint4_4_1;
                    objPRL445.CheckPoint4_4_2 = prl445.CheckPoint4_4_2;
                    objPRL445.CheckPoint4_5_1 = prl445.CheckPoint4_5_1;
                    objPRL445.CheckPoint4_5_2 = prl445.CheckPoint4_5_2;

                    objPRL445.QCRemarks = prl445.QCRemarks;
                    objPRL445.Result = prl445.Result;

                    objPRL445.EditedBy = UserName;
                    objPRL445.EditedOn = DateTime.Now;
                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL445.ActFilledBy = UserName;
                            objPRL445.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL445.ReqFilledBy = UserName;
                            objPRL445.ReqFilledOn = DateTime.Now;
                        }
                    }
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL445.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL445 objPRL445 = db.PRL445.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL445 != null && string.IsNullOrWhiteSpace(objPRL445.ProtocolNo))
                    {
                        db.PRL445.Remove(objPRL445);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL445.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL446> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRL446> lstAddPRL446 = new List<PRL446>();
                List<PRL446> lstDeletePRL446 = new List<PRL446>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRL446 obj = db.PRL446.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.DrgNo = item.DrgNo;
                                obj.RevNo = item.RevNo;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRL446 obj = new PRL446();

                            if (!string.IsNullOrWhiteSpace(item.DrgNo))
                            {
                                obj.HeaderId = item.HeaderId;
                                obj.DrgNo = item.DrgNo;
                                obj.RevNo = item.RevNo;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRL446.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRL446.Count > 0)
                    {
                        db.PRL446.AddRange(lstAddPRL446);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRL446 = db.PRL446.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRL446.Count > 0)
                    {
                        db.PRL446.RemoveRange(lstDeletePRL446);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL446 = db.PRL446.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL446.Count > 0)
                    {
                        db.PRL446.RemoveRange(lstDeletePRL446);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2Linkage(List<PRL447> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRL447> lstAddPRL447 = new List<PRL447>();
                List<PRL447> lstDeletePRL447 = new List<PRL447>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL447 obj = db.PRL447.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL447();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SrNo = item.SrNo;
                        obj.Description = item.Description;
                        obj.ReqDimension = item.ReqDimension;
                        obj.ActDimension = item.ActDimension;
                        obj.Remarks = item.Remarks;

                        if (isAdded)
                        {
                            lstAddPRL447.Add(obj);
                        }
                    }
                    if (lstAddPRL447.Count > 0)
                    {
                        db.PRL447.AddRange(lstAddPRL447);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL447 = db.PRL447.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL447.Count > 0)
                    {
                        db.PRL447.RemoveRange(lstDeletePRL447);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL447 = db.PRL447.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL447.Count > 0)
                    {
                        db.PRL447.RemoveRange(lstDeletePRL447);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion
    }
}