﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol049Controller : clsBase
    {
        // GET: PROTOCOL/Protocol049
        string ControllerURL = "/PROTOCOL/Protocol049/";
        string Title = "VISUAL AND DIMENSION REPORT FOR ATTACHMENT";

        #region Details View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO240 objPRO240 = new PRO240();
            if (!id.HasValue)
            {
                try
                {
                    objPRO240.ProtocolNo = string.Empty;
                    objPRO240.CreatedBy = objClsLoginInfo.UserName;
                    objPRO240.CreatedOn = DateTime.Now;

                    db.PRO240.Add(objPRO240);
                    db.SaveChanges();
                    // return View();

                }

                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO240.HeaderId;
            }
            else
            {
                objPRO240 = db.PRO240.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();


            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();



            //  ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO241> lstPRO241 = db.PRO241.Where(x => x.HeaderId == objPRO240.HeaderId).ToList();



            ViewBag.lstPRO241 = lstPRO241;



            #endregion
            return View(objPRO240);

        }

        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO240 PRO240)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO240.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO240 objPRO240 = db.PRO240.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO240.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO240.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data

                        objPRO240.ProtocolNo = PRO240.ProtocolNo;
                        objPRO240.ActualCfOfShell = PRO240.ActualCfOfShell;
                        objPRO240.ReqElevation = PRO240.ReqElevation;
                        objPRO240.ToleranceReqArcLength = PRO240.ToleranceReqArcLength;
                        objPRO240.ReqProjection = PRO240.ReqProjection;
                        objPRO240.ReqTilt = PRO240.ReqTilt;
                        objPRO240.ReqSize = PRO240.ReqSize;
                        objPRO240.CheckPoint1 = PRO240.CheckPoint1;
                        objPRO240.CheckPoint2 = PRO240.CheckPoint2;
                        objPRO240.CheckPoint3 = PRO240.CheckPoint3;
                        objPRO240.CheckPoint5 = PRO240.CheckPoint5;
                        objPRO240.CheckPoint5_2 = PRO240.CheckPoint5_2;
                        objPRO240.QCRemarks = PRO240.QCRemarks;
                        objPRO240.Result = PRO240.Result;
                        objPRO240.EditedBy = objClsLoginInfo.UserName;
                        objPRO240.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO240.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO240 objPRO240 = db.PRO240.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO240 != null && string.IsNullOrWhiteSpace(objPRO240.ProtocolNo))
                    {
                        db.PRO240.Remove(objPRO240);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO240.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO241> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO241> lstAddPRO241 = new List<PRO241>();
                List<PRO241> lstDeletePRO241 = new List<PRO241>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO241 obj = db.PRO241.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {


                                obj.Description = item.Description;
                                obj.Elevation = item.Elevation;
                                obj.ReqOrientation = item.ReqOrientation;
                                obj.PrincipleOrientation = item.PrincipleOrientation;
                                obj.DiffBtPrincipleOrientOnAndReqorient = item.DiffBtPrincipleOrientOnAndReqorient;
                                obj.ReqArcLength = item.ReqArcLength;
                                obj.ActArcLength = item.ActArcLength;
                                obj.ActOrientation = item.ActOrientation;
                                obj.Projection = item.Projection;
                                obj.Tilt = item.Tilt;
                                obj.Size = item.Size;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO241 obj = new PRO241();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.Description))
                            {
                                obj.Description = item.Description;
                                obj.Elevation = item.Elevation;
                                obj.ReqOrientation = item.ReqOrientation;
                                obj.PrincipleOrientation = item.PrincipleOrientation;
                                obj.DiffBtPrincipleOrientOnAndReqorient = item.DiffBtPrincipleOrientOnAndReqorient;
                                obj.ReqArcLength = item.ReqArcLength;
                                obj.ActArcLength = item.ActArcLength;
                                obj.ActOrientation = item.ActOrientation;
                                obj.Projection = item.Projection;
                                obj.Tilt = item.Tilt;
                                obj.Size = item.Size;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO241.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO241.Count > 0)
                    {
                        db.PRO241.AddRange(lstAddPRO241);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO241 = db.PRO241.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO241.Count > 0)
                    {
                        db.PRO241.RemoveRange(lstDeletePRO241);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO241 = db.PRO241.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO241.Count > 0)
                    {
                        db.PRO241.RemoveRange(lstDeletePRO241);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region Linkage View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL240 objPRL240 = new PRL240();
            objPRL240 = db.PRL240.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL240 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL240.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;

                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();



            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL241> lstPRL241 = db.PRL241.Where(x => x.HeaderId == objPRL240.HeaderId).ToList();


            ViewBag.lstPRL241 = lstPRL241;


            #endregion
            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.lstDrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL240.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL240.ActFilledBy) && (objPRL240.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL240.Project).ToList();
                            ViewBag.lstDrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.lstDrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL240);
        }

        [HttpPost]
        public ActionResult LinkageMobile(int? id, int? m, string APIRequestFrom = "", string UserName = "")
        {
            string ProjectDesc = "";
            string ManufacturingCode = "";
            string AccessRole = "";
            bool isEditable = false;
            var DrawingNo = new List<string>();
            bool isQCRemarkEditable = false;

            PRL240 objPRL240 = new PRL240();

            db.Configuration.ProxyCreationEnabled = false;
            objPRL240 = db.PRL240.Where(i => i.HeaderId == id).FirstOrDefault();
            db.Entry(objPRL240).State = System.Data.Entity.EntityState.Detached;

            NDEModels objNDEModels = new NDEModels();
            if (objPRL240 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL240.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
            AccessRole = objUserRoleAccessDetails.UserDesignation;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            var YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL241> lstPRL241 = db.PRL241.Where(x => x.HeaderId == objPRL240.HeaderId).ToList();

            #endregion
            try
            {
                isEditable = false;
                isQCRemarkEditable = false;
                if (m == 1)
                {
                    isEditable = true;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL240.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL240.ActFilledBy) && (objPRL240.RequestNo.HasValue))
                        {
                            isEditable = false;
                            isQCRemarkEditable = true;
                        }
                        else
                        {
                            DrawingNo = (new GeneralController()).GetPLMDrawingNumber(objPRL240.Project).ToList();
                        }
                    }
                }

                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    ProjectDesc = ProjectDesc,
                    ManufacturingCode = ManufacturingCode,
                    isEditable = isEditable,
                    isQCRemarkEditable = isQCRemarkEditable,
                    DrawingNo = DrawingNo,
                    AccessRole = AccessRole,
                    LeftRight = LeftRight,
                    OutsideInside = OutsideInside,

                    YesNoEnum = YesNoEnum,
                    YesNAEnum = YesNAEnum,

                    objPRL240 = objPRL240,

                    lstPRL241 = lstPRL241

                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.lstDrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();

                return Json(new
                {
                    res = "0",
                    msg = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL240 PRL240, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = PRL240.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL240 objPRL240 = db.PRL240.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    #region Save Data

                    objPRL240.DrawingRevisionNo = PRL240.DrawingRevisionNo;
                    objPRL240.DrawingNo = PRL240.DrawingNo;
                    objPRL240.DCRNo = PRL240.DCRNo;
                    objPRL240.ProtocolNo = PRL240.ProtocolNo != null ? PRL240.ProtocolNo : "";


                    objPRL240.ActualCfOfShell = PRL240.ActualCfOfShell;
                    objPRL240.ReqElevation = PRL240.ReqElevation;
                    objPRL240.ToleranceReqArcLength = PRL240.ToleranceReqArcLength;
                    objPRL240.ReqProjection = PRL240.ReqProjection;
                    objPRL240.ReqTilt = PRL240.ReqTilt;
                    objPRL240.ReqSize = PRL240.ReqSize;
                    objPRL240.CheckPoint1 = PRL240.CheckPoint1;
                    objPRL240.CheckPoint2 = PRL240.CheckPoint2;
                    objPRL240.CheckPoint3 = PRL240.CheckPoint3;


                    objPRL240.CheckPoint5 = PRL240.CheckPoint5;
                    objPRL240.CheckPoint5_2 = PRL240.CheckPoint5_2;
                    objPRL240.QCRemarks = PRL240.QCRemarks;
                    objPRL240.Result = PRL240.Result;
                    objPRL240.EditedBy = UserName;
                    objPRL240.EditedOn = DateTime.Now;



                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL240.ActFilledBy = UserName;
                            objPRL240.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL240.ReqFilledBy = UserName;
                            objPRL240.ReqFilledOn = DateTime.Now;
                        }
                    }
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL240.HeaderId;
                    #endregion

                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL240 objPRL240 = db.PRL240.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL240 != null && string.IsNullOrWhiteSpace(objPRL240.ProtocolNo))
                    {
                        db.PRL240.Remove(objPRL240);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL240.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL241> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL241> lstAddPRL241 = new List<PRL241>();
                List<PRL241> lstDeletePRL241 = new List<PRL241>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRL241 obj = db.PRL241.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.Description = item.Description;
                                obj.Elevation = item.Elevation;
                                obj.ReqOrientation = item.ReqOrientation;
                                obj.PrincipleOrientation = item.PrincipleOrientation;
                                obj.DiffBtPrincipleOrientOnAndReqorient = item.DiffBtPrincipleOrientOnAndReqorient;
                                obj.ReqArcLength = item.ReqArcLength;
                                obj.ActArcLength = item.ActArcLength;
                                obj.ActOrientation = item.ActOrientation;
                                obj.Projection = item.Projection;
                                obj.Tilt = item.Tilt;
                                obj.Size = item.Size;
                                obj.EditedBy = UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRL241 obj = new PRL241();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.Description))
                            {
                                obj.Description = item.Description;
                                obj.Elevation = item.Elevation;
                                obj.ReqOrientation = item.ReqOrientation;
                                obj.PrincipleOrientation = item.PrincipleOrientation;
                                obj.DiffBtPrincipleOrientOnAndReqorient = item.DiffBtPrincipleOrientOnAndReqorient;
                                obj.ReqArcLength = item.ReqArcLength;
                                obj.ActArcLength = item.ActArcLength;
                                obj.ActOrientation = item.ActOrientation;
                                obj.Projection = item.Projection;
                                obj.Tilt = item.Tilt;
                                obj.Size = item.Size;
                                obj.CreatedBy = UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRL241.Add(obj);
                            }
                        }
                    }
                }
                if (lstAddPRL241.Count > 0)
                {
                    db.PRL241.AddRange(lstAddPRL241);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL241 = db.PRL241.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL241 = db.PRL241.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL241.Count > 0)
                {
                    db.PRL241.RemoveRange(lstDeletePRL241);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

    }
}