﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol028Controller : clsBase
    {
        string ControllerURL = "/PROTOCOL/Protocol028/";
        string Title = "REPORT OF FINAL MACHINING INSPECTION OF TUBESHEET";
        // GET: PROTOCOL/Protocol028
        #region Details View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id,int? m)
        {
            PRO135 objPRO135 = new PRO135();
            if (!id.HasValue)
            {
                objPRO135.ProtocolNo = string.Empty;
                objPRO135.CreatedBy = objClsLoginInfo.UserName;
                objPRO135.CreatedOn = DateTime.Now;
                objPRO135.PRO136.Add(new PRO136
                {
                    Characteristics = "LAYOUT OF TUBE HOLES AS PER DRG.",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRO135.PRO136.Add(new PRO136
                {
                    Characteristics = "TOTAL NO OF TUBE HOLES.",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRO135.PRO136.Add(new PRO136
                {
                    Characteristics = "GO GAUGING OF HOLES.",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRO135.PRO136.Add(new PRO136
                {
                    Characteristics = "NO GO GAUGING OF HOLES.",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRO135.PRO136.Add(new PRO136
                {
                    Characteristics = " O T L OF TUBE HOLES",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRO135.PRO136.Add(new PRO136
                {
                    Characteristics = "VISUAL INSPECTION OF TUBE HOLE FINISH.",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRO135.PRO136.Add(new PRO136
                {
                    Characteristics = "BOTH SIDE HOLES SARP CORNER DEBARRING.",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRO135.PRO136.Add(new PRO136
                {
                    Characteristics = " J PREPARATION ",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRO135.PRO136.Add(new PRO136
                {
                    Characteristics = "J GROOVE DEPTH",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRO135.PRO136.Add(new PRO136
                {
                    Characteristics = " J GROOVE  WIDTH AT TOP",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRO135.PRO136.Add(new PRO136
                {
                    Characteristics = "GO GAUGE  FOR 'J' GROOVES",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRO135.PRO136.Add(new PRO136
                {
                    Characteristics = "NO GO GAUGE FOR 'J' GROOVES",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRO135.PRO136.Add(new PRO136
                {
                    Characteristics = "TUBE HOLES SIZE,",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRO135.PRO136.Add(new PRO136
                {
                    Characteristics = "STEPS IN TUBE HOLES AS PER DRAWING",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRO135.PRO136.Add(new PRO136
                {
                    Characteristics = "TUBE HOLES PITCH",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRO135.PRO136.Add(new PRO136
                {
                    Characteristics = "TUBE HOLES  LIGAMENT.",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRO135.PRO136.Add(new PRO136
                {
                    Characteristics = "TOTAL THICKNESS",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRO135.PRO136.Add(new PRO136
                {
                    Characteristics = "TUBE SHEET OD",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRO135.PRO136.Add(new PRO136
                {
                    Characteristics = "TUBE SHEET THICKNESS AFTER M/C",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRO135.PRO136.Add(new PRO136
                {
                    Characteristics = "CHANNEL SIDE TUBE SHEET LIP I.D",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRO135.PRO136.Add(new PRO136
                {
                    Characteristics = "SHELL SIDE TUBE SHEET LIP I.D",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRO135.PRO136.Add(new PRO136
                {
                    Characteristics = "WEP OF LIP AS PER DRAWING ",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRO135.PRO136.Add(new PRO136
                {
                    Characteristics = "STEPS ON TUBESHEET THICKNESS AS PER DRW.",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRO135.PRO136.Add(new PRO136
                {
                    Characteristics = "NUMBER OF THREADED HOLES FOR TIE ROD",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRO135.PRO136.Add(new PRO136
                {
                    Characteristics = "POSITION OF THREADED HOLES FOR TIE ROD",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRO135.PRO136.Add(new PRO136
                {
                    Characteristics = "GO - NOGO GAUGING FOR THREADED HOLES",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRO135.PRO136.Add(new PRO136
                {
                    Characteristics = "OUTER MOST HOLES TO TUBESHEET OD DISTANCE",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRO135.PRO136.Add(new PRO136
                {
                    Characteristics = "IDENTIFICATION ON PART.",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });


                db.PRO135.Add(objPRO135);
                db.SaveChanges();
                ViewBag.HeaderId = objPRO135.HeaderId;
            }
            else
            {
                objPRO135 = db.PRO135.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstYesNoNA = clsImplementationEnum.getYesNoNA().ToList();
            ViewBag.YesNoNAEnum = lstYesNoNA.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }

            return View(objPRO135);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO135 PRO135)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO135.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO135 objPRO135 = db.PRO135.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO135.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO135.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data
                        objPRO135.ProtocolNo = PRO135.ProtocolNo;
                        objPRO135.PartNameNumber = PRO135.PartNameNumber;
                        objPRO135.Vendor = PRO135.Vendor;
                        objPRO135.InspectionCertificateNo = PRO135.InspectionCertificateNo;
                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol saved successfully";
                        objResponseMsg.HeaderId = objPRO135.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO135 objPRO135 = db.PRO135.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO135 != null && string.IsNullOrWhiteSpace(objPRO135.ProtocolNo))
                    {
                        db.PRO136.RemoveRange(objPRO135.PRO136.ToList());
                        db.PRO135.Remove(objPRO135);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.REPORT_OF_FINAL_MACHINING_INSPECTION_OF_TUBESHEET.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO135.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public ActionResult GetProtocolLines1Data(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro.Characteristics like '%" + param.sSearch
                        + "%' or pro.Required like '%" + param.sSearch
                        + "%' or pro.Tolerance like '%" + param.sSearch
                        + "%' or pro.Actual like '%" + param.sSearch
                         + "%' or pro.Remarks like '%" + param.sSearch
                        + "%')";
                }

                var lstResult = db.SP_IPI_PROTOCOL028_GET_LINES1_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] { "1",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                    "",
                                   Helper.GenerateHTMLTextbox(newRecordId, "Characteristics", "", "", false, "", false, "200","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Required", "", "", false, "", false, "200","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Tolerance", "", "", false, "", false, "200","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Actual", "", "", false, "", false, "200","PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Remarks", "", "", false, "", false, "200","QC"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine1(0);")
                                };
                var data = (from uc in lstResult
                            select new[]
                            {
                                "1",
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                               Convert.ToString(uc.ROW_NO),
                               Helper.GenerateHTMLTextbox(uc.LineId, "Characteristics", Convert.ToString(uc.Characteristics), "", true, "", false, "200","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "Required", Convert.ToString(uc.Required), "", true, "", false, "200","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "Tolerance", Convert.ToString(uc.Tolerance), "", true, "", false, "200","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "Actual", Convert.ToString(uc.Actual), "", true, "", false, "200","PROD"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "Remarks", Convert.ToString(uc.Remarks), "", true, "", false, "200","QC"),
                               Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == "QC3" ? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine1','tblProtocolLines1')"):"")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine1(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRO136 objPRO136 = new PRO136();
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;

                if (refHeaderId > 0)
                {
                    string strCharacteristics = !string.IsNullOrEmpty(fc["Characteristics" + refLineId]) ? Convert.ToString(fc["Characteristics" + refLineId]).Trim() : "";
                    string strRequired = !string.IsNullOrEmpty(fc["Required" + refLineId]) ? Convert.ToString(fc["Required" + refLineId]).Trim() : "";
                    string strTolerance = !string.IsNullOrEmpty(fc["Tolerance" + refLineId]) ? Convert.ToString(fc["Tolerance" + refLineId]).Trim() : "";
                    string strActual = !string.IsNullOrEmpty(fc["Actual" + refLineId]) ? Convert.ToString(fc["Actual" + refLineId]).Trim() : "";
                    string strRemarks = !string.IsNullOrEmpty(fc["Remarks" + refLineId]) ? Convert.ToString(fc["Remarks" + refLineId]).Trim() : "";


                    #region Add New Spot Line1
                    if (refLineId > 0)
                    {
                        objPRO136 = db.PRO136.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRO136.EditedBy = objClsLoginInfo.UserName;
                        objPRO136.EditedOn = DateTime.Now;
                    }
                    objPRO136.HeaderId = refHeaderId;
                    objPRO136.Characteristics = strCharacteristics;
                    objPRO136.Required = strRequired;
                    objPRO136.Tolerance = strTolerance;
                    objPRO136.Actual = strActual;
                    objPRO136.Remarks = strRemarks;
                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = "Data updated successfully";
                    }
                    else
                    {
                        objPRO136.CreatedBy = objClsLoginInfo.UserName;
                        objPRO136.CreatedOn = DateTime.Now;
                        db.PRO136.Add(objPRO136);
                        db.SaveChanges();
                        objResponseMsg.Value = "Data added successfully";
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRO136.LineId;
                    #endregion

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolLine1(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRO136 objPRO136 = db.PRO136.FirstOrDefault(c => c.LineId == lineId);
                if (objPRO136 != null)
                {
                    db.PRO136.Remove(objPRO136);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion
        #endregion

        #region Linkage View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL135 objPRL135 = new PRL135();
            objPRL135 = db.PRL135.Where(i => i.HeaderId == id).FirstOrDefault();

            if (objPRL135 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL135.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objQproject.ManufacturingCode;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }

             UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;
             ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            if (m == 1)
            {
                ViewBag.isEditable = "true";
            }
            else
            { ViewBag.isEditable = "false"; }

            return View(objPRL135);
        }
        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL135 PRL135)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRL135.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL135 objPRL135 = db.PRL135.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL135.ProtocolNo = PRL135.ProtocolNo != null ? PRL135.ProtocolNo : "";
                    objPRL135.EditedBy = objClsLoginInfo.UserName;
                    objPRL135.EditedOn = DateTime.Now;
                    objPRL135.InspectionDate = PRL135.InspectionDate;
                    objPRL135.InspectionAgency = PRL135.InspectionAgency;
                    objPRL135.EquipmentNo = PRL135.EquipmentNo;
                    objPRL135.StageCode = PRL135.StageCode;
                    objPRL135.DrawingNo = PRL135.DrawingNo;
                    objPRL135.PartNameNumber = PRL135.PartNameNumber;
                    objPRL135.DCRNo = PRL135.DCRNo;
                    objPRL135.Vendor = PRL135.Vendor;
                    objPRL135.InspectionCertificateNo = PRL135.InspectionCertificateNo;
                    
                  
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol saved successfully";
                    objResponseMsg.HeaderId = objPRL135.HeaderId;
                    #endregion

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL135 objPRL135 = db.PRL135.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL135 != null && string.IsNullOrWhiteSpace(objPRL135.ProtocolNo))
                    {
                        db.PRL136.RemoveRange(objPRL135.PRL136.ToList());
                        db.PRL135.Remove(objPRL135);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL135.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 Linkage
        [HttpPost]
        public ActionResult GetProtocolLines1DataLinkage(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro.Characteristics like '%" + param.sSearch
                        + "%' or pro.Required like '%" + param.sSearch
                        + "%' or pro.Tolerance like '%" + param.sSearch
                        + "%' or pro.Actual like '%" + param.sSearch
                         + "%' or pro.Remarks like '%" + param.sSearch
                        + "%')";
                }

                var lstResult = db.SP_IPI_PROTOCOL028_LINKAGE_GET_LINES1_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] { "1",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                    "",
                                   Helper.GenerateHTMLTextbox(newRecordId, "Characteristics", "", "", false, "", false, "200","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Required", "", "", false, "", false, "200","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Tolerance", "", "", false, "", false, "200","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Actual", "", "", false, "", false, "200","PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Remarks", "", "", false, "", false, "200","QC"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine1(0);")
                                };
                var data = (from uc in lstResult
                            select new[]
                            {
                                "1",
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                               Convert.ToString(uc.ROW_NO),
                               Helper.GenerateHTMLTextbox(uc.LineId, "Characteristics", Convert.ToString(uc.Characteristics), "", true, "", false, "200","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "Required", Convert.ToString(uc.Required), "", true, "", false, "200","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "Tolerance", Convert.ToString(uc.Tolerance), "", true, "", false, "200","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "Actual", Convert.ToString(uc.Actual), "", true, "", false, "200","PROD"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "Remarks", Convert.ToString(uc.Remarks), "", true, "", false, "200","QC"),
                               Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == "QC3" ? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine1Linkage','tblProtocolLines1')"):"")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult SaveProtocolLine1Linkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRL136 objPRL136 = new PRL136();
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;

                if (refHeaderId > 0)
                {
                    string strCharacteristics = !string.IsNullOrEmpty(fc["Characteristics" + refLineId]) ? Convert.ToString(fc["Characteristics" + refLineId]).Trim() : "";
                    string strRequired = !string.IsNullOrEmpty(fc["Required" + refLineId]) ? Convert.ToString(fc["Required" + refLineId]).Trim() : "";
                    string strTolerance = !string.IsNullOrEmpty(fc["Tolerance" + refLineId]) ? Convert.ToString(fc["Tolerance" + refLineId]).Trim() : "";
                    string strActual = !string.IsNullOrEmpty(fc["Actual" + refLineId]) ? Convert.ToString(fc["Actual" + refLineId]).Trim() : "";
                    string strRemarks = !string.IsNullOrEmpty(fc["Remarks" + refLineId]) ? Convert.ToString(fc["Remarks" + refLineId]).Trim() : "";


                    #region Add New Spot Line1
                    if (refLineId > 0)
                    {
                        objPRL136 = db.PRL136.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRL136.EditedBy = objClsLoginInfo.UserName;
                        objPRL136.EditedOn = DateTime.Now;
                    }
                    objPRL136.HeaderId = refHeaderId;
                    objPRL136.Characteristics = strCharacteristics;
                    objPRL136.Required = strRequired;
                    objPRL136.Tolerance = strTolerance;
                    objPRL136.Actual = strActual;
                    objPRL136.Remarks = strRemarks;
                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = "Data updated successfully";
                    }
                    else
                    {
                        objPRL136.CreatedBy = objClsLoginInfo.UserName;
                        objPRL136.CreatedOn = DateTime.Now;
                        db.PRL136.Add(objPRL136);
                        db.SaveChanges();
                        objResponseMsg.Value = "Data added successfully";
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRL136.LineId;
                    #endregion

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolLine1Linkage(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRL136 objPRL136 = db.PRL136.FirstOrDefault(c => c.LineId == lineId);
                if (objPRL136 != null)
                {
                    db.PRL136.Remove(objPRL136);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #endregion


        public string HTMLActionString(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", string onHrefURL = "", bool isDisable = false)
        {
            string htmlControl = "";
            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? onClickMethod : "";
            string onHref = !string.IsNullOrEmpty(onHrefURL) ? "href='" + onHrefURL + "'" : "";

            if (isDisable)
            {
                htmlControl += "<i id=\"" + inputID.ToString() + "\" name=\"" + inputID + "\" ></i>";
                htmlControl = "<i id=\"" + inputID + "' name='" + inputID + "' style='cursor: pointer; opacity:0.3; margin-right:10px;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";
            }
            else
            {
                htmlControl += "<i id=\"" + inputID.ToString() + "\" name=\"" + inputID + "\"  style='cursor: pointer; margin-right:10px;' Title=\"" + buttonTooltip + "\" class=\"" + className + "\"  onclick=\"" + onClickEvent + "\" ></i>";
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer; margin-right:10px;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
            }

            if (!string.IsNullOrEmpty(onHref))
            {
                htmlControl = "<a " + onHref + " >" + htmlControl + "</a>";
            }

            return htmlControl;
        }
    }
}