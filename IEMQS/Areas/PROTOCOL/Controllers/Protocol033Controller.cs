﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol033Controller : clsBase
    {
        string ControllerURL = "/PROTOCOL/Protocol033/";
        string Title = "OVERLAY THICKNESS MEASUREMENT REPORT FOR NOZZLE/PIPE/ELBOW";
        // GET: PROTOCOL/Protocol033
        #region Details View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO160 objPRO160 = new PRO160();
            if (!id.HasValue)
            {
                objPRO160.ProtocolNo = string.Empty;
                objPRO160.CreatedBy = objClsLoginInfo.UserName;
                objPRO160.CreatedOn = DateTime.Now;
                db.PRO160.Add(objPRO160);
                db.SaveChanges();
                ViewBag.HeaderId = objPRO160.HeaderId;
            }
            else
            {
                objPRO160 = db.PRO160.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            return View(objPRO160);
        }
        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO160 PRO160)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO160.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO160 objPRO160 = db.PRO160.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO160.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO160.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data
                        objPRO160.ProtocolNo = PRO160.ProtocolNo;
                        objPRO160.Remark2 = PRO160.Remark2;
                        objPRO160.EditedBy = objClsLoginInfo.UserName;
                        objPRO160.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol saved successfully";
                        objResponseMsg.HeaderId = objPRO160.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO160 objPRO160 = db.PRO160.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO160 != null && string.IsNullOrWhiteSpace(objPRO160.ProtocolNo))
                    {
                        db.PRO161.RemoveRange(objPRO160.PRO161.ToList());
                        db.PRO160.Remove(objPRO160);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.OVERLAY_THICKNESS_MEASUREMENT_REPORT_FOR_NOZZLE_PIPE_ELBOW.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO160.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
     
        #region Line1 details
        [HttpPost]
        public ActionResult GetProtocolLines1Data(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro.Description like '%" + param.sSearch
                        + "%' or pro.SeamNo like '%" + param.sSearch
                        + "%' or pro.BeforeOverlayThkReq like '%" + param.sSearch
                        + "%' or pro.BeforeOverlayThkAct like '%" + param.sSearch
                         + "%' or pro.AfterOverlayThkReq like '%" + param.sSearch
                          + "%' or pro.AfterOverlayThkAct like '%" + param.sSearch
                           + "%' or pro.OverlayThk like '%" + param.sSearch
                            + "%' or pro.Remarks like '%" + param.sSearch
                        + "%')";
                }

                var lstResult = db.SP_IPI_PROTOCOL033_GET_LINES1_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] { "1",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                    "",
                                   Helper.GenerateHTMLTextbox(newRecordId, "Description", "", "", false, "", false, "100","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "SeamNo", "", "", false, "", false, "50","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "BeforeOverlayThkReq", "", "", false, "", false, "50","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "BeforeOverlayThkAct", "", "", false, "", false, "50","PROD"),
                                    Helper.GenerateHTMLTextbox(newRecordId, "AfterOverlayThkReq", "", "", false, "", false, "50","QC"),
                                     Helper.GenerateHTMLTextbox(newRecordId, "AfterOverlayThkAct", "", "", false, "", false, "50","PROD"),
                                      Helper.GenerateHTMLTextbox(newRecordId, "OverlayThk", "", "", false, "", false, "50","QC"),
                                       Helper.GenerateHTMLTextbox(newRecordId, "Remarks", "", "", false, "", false, "100","QC"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine1(0);")
                                };
                var data = (from uc in lstResult
                            select new[]
                            {
                                "1",
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                               Convert.ToString(uc.ROW_NO),
                               Helper.GenerateHTMLTextbox(uc.LineId, "Description", Convert.ToString(uc.Description), "", true, "", false, "100","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "SeamNo", Convert.ToString(uc.SeamNo), "", true, "", false, "50","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "BeforeOverlayThkReq", Convert.ToString(uc.BeforeOverlayThkReq), "", true, "", false, "50","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "BeforeOverlayThkAct", Convert.ToString(uc.BeforeOverlayThkAct), "", true, "", false, "50","PROD"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "AfterOverlayThkReq", Convert.ToString(uc.AfterOverlayThkReq), "", true, "", false, "50","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "AfterOverlayThkAct", Convert.ToString(uc.AfterOverlayThkAct), "", true, "", false, "50","PROD"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "OverlayThk", Convert.ToString(uc.OverlayThk), "", true, "", false, "50","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "Remarks", Convert.ToString(uc.Remarks), "", true, "", false, "100","QC"),
                               Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == "QC3" ? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine1','tblProtocolLines1')"):"")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine1(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRO161 objPRO161 = new PRO161();
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;

                if (refHeaderId > 0)
                {
                    string strDescription = !string.IsNullOrEmpty(fc["Description" + refLineId]) ? Convert.ToString(fc["Description" + refLineId]).Trim() : "";
                    string strSeamNo = !string.IsNullOrEmpty(fc["SeamNo" + refLineId]) ? Convert.ToString(fc["SeamNo" + refLineId]).Trim() : "";
                    string strBeforeOverlayThkReq = !string.IsNullOrEmpty(fc["BeforeOverlayThkReq" + refLineId]) ? Convert.ToString(fc["BeforeOverlayThkReq" + refLineId]).Trim() : "";
                    string strBeforeOverlayThkAct = !string.IsNullOrEmpty(fc["BeforeOverlayThkAct" + refLineId]) ? Convert.ToString(fc["BeforeOverlayThkAct" + refLineId]).Trim() : "";
                    string strAfterOverlayThkReq = !string.IsNullOrEmpty(fc["AfterOverlayThkReq" + refLineId]) ? Convert.ToString(fc["AfterOverlayThkReq" + refLineId]).Trim() : "";
                    string strAfterOverlayThkAct = !string.IsNullOrEmpty(fc["AfterOverlayThkAct" + refLineId]) ? Convert.ToString(fc["AfterOverlayThkAct" + refLineId]).Trim() : "";
                    string strOverlayThk = !string.IsNullOrEmpty(fc["OverlayThk" + refLineId]) ? Convert.ToString(fc["OverlayThk" + refLineId]).Trim() : "";
                    string strRemarks = !string.IsNullOrEmpty(fc["Remarks" + refLineId]) ? Convert.ToString(fc["Remarks" + refLineId]).Trim() : "";

                    #region Add New Spot Line1
                    if (refLineId > 0)
                    {
                        objPRO161 = db.PRO161.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRO161.EditedBy = objClsLoginInfo.UserName;
                        objPRO161.EditedOn = DateTime.Now;
                    }
                    objPRO161.HeaderId = refHeaderId;
                    objPRO161.Description = strDescription;
                    objPRO161.SeamNo = strSeamNo;
                    objPRO161.BeforeOverlayThkReq = strBeforeOverlayThkReq;
                    objPRO161.BeforeOverlayThkAct = strBeforeOverlayThkAct;
                    objPRO161.AfterOverlayThkReq = strAfterOverlayThkReq;
                    objPRO161.AfterOverlayThkAct = strAfterOverlayThkAct;
                    objPRO161.OverlayThk = strOverlayThk;
                    objPRO161.Remarks = strRemarks;

                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = "Data updated successfully";
                    }
                    else
                    {
                        objPRO161.CreatedBy = objClsLoginInfo.UserName;
                        objPRO161.CreatedOn = DateTime.Now;
                        db.PRO161.Add(objPRO161);
                        db.SaveChanges();
                        objResponseMsg.Value = "Data added successfully";
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRO161.LineId;
                    #endregion

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine1(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRO161 objPRO161 = db.PRO161.FirstOrDefault(c => c.LineId == lineId);
                if (objPRO161 != null)
                {
                    db.PRO161.Remove(objPRO161);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion
        #endregion


        #region Linkage View Code
        #region Header
        public ActionResult Linkage(int? id, int? m)
        {
            PRL160 objPRL160 = new PRL160();
            objPRL160 = db.PRL160.Where(i => i.HeaderId == id).FirstOrDefault();

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;
            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();
            if (objPRL160 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL160.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objQproject.ManufacturingCode;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }

            if (m == 1)
            {
                ViewBag.isEditable = "true";
            }
            else
            { ViewBag.isEditable = "false"; }

            return View(objPRL160);
        }
        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL160 PRL160)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRL160.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL160 objPRL160 = db.PRL160.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL160.ProtocolNo = PRL160.ProtocolNo != null ? PRL160.ProtocolNo : "";
                    objPRL160.SeamNo = PRL160.SeamNo;
                    objPRL160.EquipmentNo = PRL160.EquipmentNo;
                    objPRL160.InspectionAgency = PRL160.InspectionAgency;
                    objPRL160.DrawingNo = PRL160.DrawingNo;
                    objPRL160.DrawingRevisionNo = PRL160.DrawingRevisionNo;
                    objPRL160.ManufacturingCode = PRL160.ManufacturingCode;
                    objPRL160.DCRNo = PRL160.DCRNo;
                    objPRL160.Remark2 = PRL160.Remark2;
                    objPRL160.EditedBy = objClsLoginInfo.UserName;
                    objPRL160.EditedOn = DateTime.Now;
                    
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol saved successfully";
                    objResponseMsg.HeaderId = objPRL160.HeaderId;
                    #endregion

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL160 objPRL160 = db.PRL160.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL160 != null && string.IsNullOrWhiteSpace(objPRL160.ProtocolNo))
                    {
                        db.PRL161.RemoveRange(objPRL160.PRL161.ToList());
                        db.PRL160.Remove(objPRL160);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL160.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line1 details
        [HttpPost]
        public ActionResult GetProtocolLines1DataLinkage(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro.Description like '%" + param.sSearch
                        + "%' or pro.SeamNo like '%" + param.sSearch
                        + "%' or pro.BeforeOverlayThkReq like '%" + param.sSearch
                        + "%' or pro.BeforeOverlayThkAct like '%" + param.sSearch
                         + "%' or pro.AfterOverlayThkReq like '%" + param.sSearch
                          + "%' or pro.AfterOverlayThkAct like '%" + param.sSearch
                           + "%' or pro.OverlayThk like '%" + param.sSearch
                            + "%' or pro.Remarks like '%" + param.sSearch
                        + "%')";
                }

                var lstResult = db.SP_IPI_PROTOCOL033_LINKAGE_GET_LINES1_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] { "1",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                    "",
                                   Helper.GenerateHTMLTextbox(newRecordId, "Description", "", "", false, "", false, "100","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "SeamNo", "", "", false, "", false, "50","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "BeforeOverlayThkReq", "", "", false, "", false, "50","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "BeforeOverlayThkAct", "", "", false, "", false, "50","PROD"),
                                    Helper.GenerateHTMLTextbox(newRecordId, "AfterOverlayThkReq", "", "", false, "", false, "50","QC"),
                                     Helper.GenerateHTMLTextbox(newRecordId, "AfterOverlayThkAct", "", "", false, "", false, "50","PROD"),
                                      Helper.GenerateHTMLTextbox(newRecordId, "OverlayThk", "", "", false, "", false, "50","QC"),
                                       Helper.GenerateHTMLTextbox(newRecordId, "Remarks", "", "", false, "", false, "100","QC"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine1(0);")
                                };
                var data = (from uc in lstResult
                            select new[]
                            {
                                "1",
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                               Convert.ToString(uc.ROW_NO),
                               Helper.GenerateHTMLTextbox(uc.LineId, "Description", Convert.ToString(uc.Description), "", true, "", false, "100","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "SeamNo", Convert.ToString(uc.SeamNo), "", true, "", false, "50","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "BeforeOverlayThkReq", Convert.ToString(uc.BeforeOverlayThkReq), "", true, "", false, "50","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "BeforeOverlayThkAct", Convert.ToString(uc.BeforeOverlayThkAct), "", true, "", false, "50","PROD"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "AfterOverlayThkReq", Convert.ToString(uc.AfterOverlayThkReq), "", true, "", false, "50","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "AfterOverlayThkAct", Convert.ToString(uc.AfterOverlayThkAct), "", true, "", false, "50","PROD"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "OverlayThk", Convert.ToString(uc.OverlayThk), "", true, "", false, "50","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "Remarks", Convert.ToString(uc.Remarks), "", true, "", false, "100","QC"),
                               Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == "QC3" ? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine1Linkage','tblProtocolLines1')"):"")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine1Linkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRL161 objPRL161 = new PRL161();
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;

                if (refHeaderId > 0)
                {
                    string strDescription = !string.IsNullOrEmpty(fc["Description" + refLineId]) ? Convert.ToString(fc["Description" + refLineId]).Trim() : "";
                    string strSeamNo = !string.IsNullOrEmpty(fc["SeamNo" + refLineId]) ? Convert.ToString(fc["SeamNo" + refLineId]).Trim() : "";
                    string strBeforeOverlayThkReq = !string.IsNullOrEmpty(fc["BeforeOverlayThkReq" + refLineId]) ? Convert.ToString(fc["BeforeOverlayThkReq" + refLineId]).Trim() : "";
                    string strBeforeOverlayThkAct = !string.IsNullOrEmpty(fc["BeforeOverlayThkAct" + refLineId]) ? Convert.ToString(fc["BeforeOverlayThkAct" + refLineId]).Trim() : "";
                    string strAfterOverlayThkReq = !string.IsNullOrEmpty(fc["AfterOverlayThkReq" + refLineId]) ? Convert.ToString(fc["AfterOverlayThkReq" + refLineId]).Trim() : "";
                    string strAfterOverlayThkAct = !string.IsNullOrEmpty(fc["AfterOverlayThkAct" + refLineId]) ? Convert.ToString(fc["AfterOverlayThkAct" + refLineId]).Trim() : "";
                    string strOverlayThk = !string.IsNullOrEmpty(fc["OverlayThk" + refLineId]) ? Convert.ToString(fc["OverlayThk" + refLineId]).Trim() : "";
                    string strRemarks = !string.IsNullOrEmpty(fc["Remarks" + refLineId]) ? Convert.ToString(fc["Remarks" + refLineId]).Trim() : "";

                    #region Add New Spot Line1
                    if (refLineId > 0)
                    {
                        objPRL161 = db.PRL161.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRL161.EditedBy = objClsLoginInfo.UserName;
                        objPRL161.EditedOn = DateTime.Now;
                    }
                    objPRL161.HeaderId = refHeaderId;
                    objPRL161.Description = strDescription;
                    objPRL161.SeamNo = strSeamNo;
                    objPRL161.BeforeOverlayThkReq = strBeforeOverlayThkReq;
                    objPRL161.BeforeOverlayThkAct = strBeforeOverlayThkAct;
                    objPRL161.AfterOverlayThkReq = strAfterOverlayThkReq;
                    objPRL161.AfterOverlayThkAct = strAfterOverlayThkAct;
                    objPRL161.OverlayThk = strOverlayThk;
                    objPRL161.Remarks = strRemarks;

                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = "Data updated successfully";
                    }
                    else
                    {
                        objPRL161.CreatedBy = objClsLoginInfo.UserName;
                        objPRL161.CreatedOn = DateTime.Now;
                        db.PRL161.Add(objPRL161);
                        db.SaveChanges();
                        objResponseMsg.Value = "Data added successfully";
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRL161.LineId;
                    #endregion

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolLine1Linkage(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRL161 objPRL161 = db.PRL161.FirstOrDefault(c => c.LineId == lineId);
                if (objPRL161 != null)
                {
                    db.PRL161.Remove(objPRL161);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #endregion
       
        public string HTMLActionString(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", string onHrefURL = "", bool isDisable = false)
        {
            string htmlControl = "";
            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? onClickMethod : "";
            string onHref = !string.IsNullOrEmpty(onHrefURL) ? "href='" + onHrefURL + "'" : "";

            if (isDisable)
            {
                htmlControl += "<i id=\"" + inputID.ToString() + "\" name=\"" + inputID + "\" ></i>";
                htmlControl = "<i id=\"" + inputID + "' name='" + inputID + "' style='cursor: pointer; opacity:0.3; margin-right:10px;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";
            }
            else
            {
                htmlControl += "<i id=\"" + inputID.ToString() + "\" name=\"" + inputID + "\"  style='cursor: pointer; margin-right:10px;' Title=\"" + buttonTooltip + "\" class=\"" + className + "\"  onclick=\"" + onClickEvent + "\" ></i>";
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer; margin-right:10px;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
            }

            if (!string.IsNullOrEmpty(onHref))
            {
                htmlControl = "<a " + onHref + " >" + htmlControl + "</a>";
            }

            return htmlControl;
        }
    }

}