﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol051Controller : clsBase
    {
        // GET: PROTOCOL/Protocol051

        string ControllerURL = "/PROTOCOL/Protocol051/";
        string Title = "VISUAL AND DIMENSION REPORT AFTER WELD OVERALY ON SHELL";
        //IEMQS.DrawingReference.hedProjectDocListWebServiceClient drawref = new IEMQS.DrawingReference.hedProjectDocListWebServiceClient();

        #region Details View Code
        #region Header
        [SessionExpireFilter]

        public ActionResult Details(int? id, int? m)
        {
            PRO250 objPRO250 = new PRO250();
            if (!id.HasValue)
            {
                try
                {
                    objPRO250.ProtocolNo = string.Empty;
                    objPRO250.CreatedBy = objClsLoginInfo.UserName;
                    objPRO250.CreatedOn = DateTime.Now;

                    #region OUT OF ROUNDNESS
                    objPRO250.PRO251.Add(new PRO251
                    {
                        Orientation = "0°-180° ",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO250.PRO251.Add(new PRO251
                    {
                        Orientation = "22.5°-205.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO250.PRO251.Add(new PRO251
                    {
                        Orientation = "45°-225°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO250.PRO251.Add(new PRO251
                    {
                        Orientation = "67.5°-247.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO250.PRO251.Add(new PRO251
                    {
                        Orientation = "90°-270°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO250.PRO251.Add(new PRO251
                    {
                        Orientation = "112.5°-292.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO250.PRO251.Add(new PRO251
                    {
                        Orientation = "135°-315°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO250.PRO251.Add(new PRO251
                    {
                        Orientation = "157.5°-337.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion

                    #region CIRCUMFERENCE MEASUREMENT
                    objPRO250.PRO252.Add(new PRO252
                    {
                        Location = "TOP",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO250.PRO252.Add(new PRO252
                    {
                        Location = "MIDDLE",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO250.PRO252.Add(new PRO252
                    {
                        Location = "BOTTOM",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion

                    #region TOTAL SHELL HEIGHT
                    objPRO250.PRO253.Add(new PRO253
                    {
                        RequiredValue = "AT 0°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO250.PRO253.Add(new PRO253
                    {
                        RequiredValue = "AT 90°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO250.PRO253.Add(new PRO253
                    {
                        RequiredValue = "AT 180°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO250.PRO253.Add(new PRO253
                    {
                        RequiredValue = "AT 270°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion

                    #region WELDING SEQUENCE & DIRECTION OF WELD OVERLAY
                    objPRO250.PRO254.Add(new PRO254
                    {
                        TotalNoOfDirection = "1",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO250.PRO254.Add(new PRO254
                    {
                        TotalNoOfDirection = "2",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion

                    db.PRO250.Add(objPRO250);
                    db.SaveChanges();
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO250.HeaderId;
            }
            else
            {
                objPRO250 = db.PRO250.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            List<string> lstWidthOfEsscStripUsedForOverlay = new List<string> { "30", "45", "60", "90", "120" };
            ViewBag.WidthOfEsscStripUsedForOverlay = lstWidthOfEsscStripUsedForOverlay.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            //List<string> lstActSeqFollowed = new List<string> { "→", "←" };
            ViewBag.ActSeqFollowed = new List<SelectListItem>() { new SelectListItem() { Value = "LEFT", Text = "←" }, new SelectListItem() { Value = "RIGHT", Text = "→" } };//lstActSeqFollowed.Select(x => new SelectListItem() { Value ="LEFT", Text = "←" }).ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            ViewBag.OutsideOrInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO251> lstPRO251 = db.PRO251.Where(x => x.HeaderId == objPRO250.HeaderId).ToList();
            List<PRO252> lstPRO252 = db.PRO252.Where(x => x.HeaderId == objPRO250.HeaderId).ToList();
            List<PRO253> lstPRO253 = db.PRO253.Where(x => x.HeaderId == objPRO250.HeaderId).ToList();
            List<PRO254> lstPRO254 = db.PRO254.Where(x => x.HeaderId == objPRO250.HeaderId).ToList();

            ViewBag.lstPRO251 = lstPRO251;
            ViewBag.lstPRO252 = lstPRO252;
            ViewBag.lstPRO253 = lstPRO253;
            ViewBag.lstPRO254 = lstPRO254;
            #endregion

            return View(objPRO250);

        }

        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }
        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO250 PRO250)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO250.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO250 objPRO250 = db.PRO250.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO250.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO250.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data

                        objPRO250.ProtocolNo = PRO250.ProtocolNo;
                        objPRO250.ReqIdOutOfRoundness = PRO250.ReqIdOutOfRoundness;
                        objPRO250.OutByOrientation = PRO250.OutByOrientation;
                        objPRO250.OutByActAtTopOpenEnd = PRO250.OutByActAtTopOpenEnd;
                        objPRO250.OutByActAtBottomOpenEnd = PRO250.OutByActAtBottomOpenEnd;
                        objPRO250.ReqIdCf = PRO250.ReqIdCf;
                        objPRO250.ReqOdCf = PRO250.ReqOdCf;
                        objPRO250.ReqTotalLength = PRO250.ReqTotalLength;
                        objPRO250.WidthOfEsscStripUsedForOverlay = PRO250.WidthOfEsscStripUsedForOverlay;
                        objPRO250.CheckPoint1 = PRO250.CheckPoint1;
                        objPRO250.CheckPoint2 = PRO250.CheckPoint2;
                        objPRO250.CheckPoint3 = PRO250.CheckPoint3;
                        objPRO250.CheckPoint4 = PRO250.CheckPoint4;
                        objPRO250.CheckPoint6 = PRO250.CheckPoint6;
                        objPRO250.CheckPoint6_2 = PRO250.CheckPoint6_2;
                        objPRO250.QCRemarks = PRO250.QCRemarks;
                        objPRO250.Result = PRO250.Result;

                        objPRO250.EditedBy = objClsLoginInfo.UserName;
                        objPRO250.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO250.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO250 objPRO250 = db.PRO250.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO250 != null && string.IsNullOrWhiteSpace(objPRO250.ProtocolNo))
                    {
                        db.PRO250.Remove(objPRO250);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO250.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO251> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO251> lstAddPRO251 = new List<PRO251>();
                List<PRO251> lstDeletePRO251 = new List<PRO251>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO251 obj = db.PRO251.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.Orientation = item.Orientation;
                                obj.ActAtTopOpenEnd = item.ActAtTopOpenEnd;
                                obj.ActAtBottomOpenEnd = item.ActAtBottomOpenEnd;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO251 obj = new PRO251();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.Orientation))
                            {
                                obj.Orientation = item.Orientation;
                                obj.ActAtTopOpenEnd = item.ActAtTopOpenEnd;
                                obj.ActAtBottomOpenEnd = item.ActAtBottomOpenEnd;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO251.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO251.Count > 0)
                    {
                        db.PRO251.AddRange(lstAddPRO251);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO251 = db.PRO251.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO251.Count > 0)
                    {
                        db.PRO251.RemoveRange(lstDeletePRO251);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO251 = db.PRO251.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO251.Count > 0)
                    {
                        db.PRO251.RemoveRange(lstDeletePRO251);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2(List<PRO252> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO252> lstAddPRO252 = new List<PRO252>();
                List<PRO252> lstDeletePRO252 = new List<PRO252>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO252 obj = db.PRO252.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO252();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Location = item.Location;
                        obj.ActIdCf = item.ActIdCf;
                        obj.ActOdCf = item.ActOdCf;

                        if (isAdded)
                        {
                            lstAddPRO252.Add(obj);
                        }
                    }
                    if (lstAddPRO252.Count > 0)
                    {
                        db.PRO252.AddRange(lstAddPRO252);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO252 = db.PRO252.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO252.Count > 0)
                    {
                        db.PRO252.RemoveRange(lstDeletePRO252);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO252 = db.PRO252.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO252.Count > 0)
                    {
                        db.PRO252.RemoveRange(lstDeletePRO252);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3(List<PRO253> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO253> lstAddPRO253 = new List<PRO253>();
                List<PRO253> lstDeletePRO253 = new List<PRO253>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO253 obj = db.PRO253.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        else
                        {
                            obj = new PRO253();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }

                        obj.RequiredValue = item.RequiredValue;
                        obj.ActualValue = item.ActualValue;

                        if (isAdded)
                        {
                            lstAddPRO253.Add(obj);
                        }
                    }
                    if (lstAddPRO253.Count > 0)
                    {
                        db.PRO253.AddRange(lstAddPRO253);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO253 = db.PRO253.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO253.Count > 0)
                    {
                        db.PRO253.RemoveRange(lstDeletePRO253);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO253 = db.PRO253.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO253.Count > 0)
                    {
                        db.PRO253.RemoveRange(lstDeletePRO253);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line4 details
        [HttpPost]
        public JsonResult SaveProtocolLine4(List<PRO254> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO254> lstAddPRO254 = new List<PRO254>();
                List<PRO254> lstDeletePRO254 = new List<PRO254>();

                List<PRO254> objDelete = db.PRO254.Where(x => x.HeaderId == HeaderId).ToList();
                db.PRO254.RemoveRange(objDelete);
                db.SaveChanges();

                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        PRO254 obj = new PRO254();
                        obj.HeaderId = item.HeaderId;
                        if (!string.IsNullOrWhiteSpace(item.TotalNoOfDirection))
                        {
                            obj.TotalNoOfDirection = item.TotalNoOfDirection;
                            obj.ActSeqFollowed = item.ActSeqFollowed;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            lstAddPRO254.Add(obj);
                        }
                    }

                    if (lstAddPRO254.Count > 0)
                    {
                        db.PRO254.AddRange(lstAddPRO254);
                    }

                    //var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    //var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    //lstDeletePRO254 = db.PRO254.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    //if (lstDeletePRO254.Count > 0)
                    //{
                    //    db.PRO254.RemoveRange(lstDeletePRO254);
                    //}
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO254 = db.PRO254.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO254.Count > 0)
                    {
                        db.PRO254.RemoveRange(lstDeletePRO254);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion
        #endregion


        #region Linkage View Code  
        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL250 objPRL250 = new PRL250();
            objPRL250 = db.PRL250.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL250 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL250.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }

            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;



            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            List<string> lstWidthOfEsscStripUsedForOverlay = new List<string> { "30", "45", "60", "90", "120" };
            ViewBag.WidthOfEsscStripUsedForOverlay = lstWidthOfEsscStripUsedForOverlay.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            //List<string> lstActSeqFollowed = new List<string> { "→", "←" };
            ViewBag.ActSeqFollowed = new List<SelectListItem>() { new SelectListItem() { Value = "LEFT", Text = "←" }, new SelectListItem() { Value = "RIGHT", Text = "→" } };//lstActSeqFollowed.Select(x => new SelectListItem() { Value ="LEFT", Text = "←" }).ToList();
            //List<string> lstActSeqFollowed = new List<string> { "→", "←" };
            //ViewBag.ActSeqFollowed = lstActSeqFollowed.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideOrInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL251> lstPRL251 = db.PRL251.Where(x => x.HeaderId == objPRL250.HeaderId).ToList();
            List<PRL252> lstPRL252 = db.PRL252.Where(x => x.HeaderId == objPRL250.HeaderId).ToList();
            List<PRL253> lstPRL253 = db.PRL253.Where(x => x.HeaderId == objPRL250.HeaderId).ToList();
            List<PRL254> lstPRL254 = db.PRL254.Where(x => x.HeaderId == objPRL250.HeaderId).ToList();


            ViewBag.lstPRL251 = lstPRL251;
            ViewBag.lstPRL252 = lstPRL252;
            ViewBag.lstPRL253 = lstPRL253;
            ViewBag.lstPRL254 = lstPRL254;

            #endregion

            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.DrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL250.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL250.ActFilledBy) && (objPRL250.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL250.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }

            return View(objPRL250);
        }

        [HttpPost]
        public ActionResult LinkageMobile(int? id, int? m, string APIRequestFrom = "", string UserName = "")
        {
            string ProjectDesc = "";
            string ManufacturingCode = "";
            string AccessRole = "";
            bool isEditable = false;
            var DrawingNo = new List<string>();
            bool isQCRemarkEditable = false;

            PRL250 objPRL250 = new PRL250();
            db.Configuration.ProxyCreationEnabled = false;
            objPRL250 = db.PRL250.Where(i => i.HeaderId == id).FirstOrDefault();
            db.Entry(objPRL250).State = System.Data.Entity.EntityState.Detached;
            NDEModels objNDEModels = new NDEModels();
            if (objPRL250 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL250.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
            }

            if (m == 1)
            { isEditable = true; }
            else
            { isEditable = false; }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
            AccessRole = objUserRoleAccessDetails.UserDesignation;


            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            var YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            List<string> lstWidthOfEsscStripUsedForOverlay = new List<string> { "30", "45", "60", "90", "120" };
            var WidthOfEsscStripUsedForOverlay = lstWidthOfEsscStripUsedForOverlay.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            //List<string> lstActSeqFollowed = new List<string> { "→", "←" };
            var ActSeqFollowed = new List<SelectListItem>() { new SelectListItem() { Value = "LEFT", Text = "←" }, new SelectListItem() { Value = "RIGHT", Text = "→" } };//lstActSeqFollowed.Select(x => new SelectListItem() { Value ="LEFT", Text = "←" }).ToList();
            //List<string> lstActSeqFollowed = new List<string> { "→", "←" };
            //var ActSeqFollowed = lstActSeqFollowed.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var OutsideOrInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL251> lstPRL251 = db.PRL251.Where(x => x.HeaderId == objPRL250.HeaderId).ToList();
            List<PRL252> lstPRL252 = db.PRL252.Where(x => x.HeaderId == objPRL250.HeaderId).ToList();
            List<PRL253> lstPRL253 = db.PRL253.Where(x => x.HeaderId == objPRL250.HeaderId).ToList();
            List<PRL254> lstPRL254 = db.PRL254.Where(x => x.HeaderId == objPRL250.HeaderId).ToList();

            #endregion

            try
            {
                isEditable = false;
                isQCRemarkEditable = false;
                if (m == 1)
                {
                    isEditable = true;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL250.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL250.ActFilledBy) && (objPRL250.RequestNo.HasValue))
                        {
                            isEditable = false;
                            isQCRemarkEditable = true;
                        }
                        else
                        {
                            DrawingNo = (new GeneralController()).GetPLMDrawingNumber(objPRL250.Project).ToList();
                        }
                    }
                }


                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    ProjectDesc = ProjectDesc,
                    ManufacturingCode = ManufacturingCode,
                    isEditable = isEditable,
                    isQCRemarkEditable = isQCRemarkEditable,
                    DrawingNo = DrawingNo,
                    AccessRole = AccessRole,
                    OutsideInside = OutsideOrInside,
                    ActSeqFollowed = ActSeqFollowed,
                    WidthOfEsscStripUsedForOverlay = WidthOfEsscStripUsedForOverlay,
                    YesNoEnum = YesNoEnum,
                    YesNAEnum = YesNAEnum,

                    objPRL250 = objPRL250,

                    lstPRL251 = lstPRL251,
                    lstPRL252 = lstPRL252,
                    lstPRL253 = lstPRL253,
                    lstPRL254 = lstPRL254,

                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();

                return Json(new
                {
                    res = "0",
                    msg = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }

        }
        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL250 PRL250, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = PRL250.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL250 objPRL250 = db.PRL250.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    #region Save Data
                    objPRL250.ProtocolNo = PRL250.ProtocolNo;

                    objPRL250.DrawingRevisionNo = PRL250.DrawingRevisionNo;
                    objPRL250.DrawingNo = PRL250.DrawingNo;
                    objPRL250.DCRNo = PRL250.DCRNo;

                    objPRL250.ReqIdOutOfRoundness = PRL250.ReqIdOutOfRoundness;
                    objPRL250.OutByOrientation = PRL250.OutByOrientation;
                    objPRL250.OutByActAtTopOpenEnd = PRL250.OutByActAtTopOpenEnd;
                    objPRL250.OutByActAtBottomOpenEnd = PRL250.OutByActAtBottomOpenEnd;
                    objPRL250.ReqIdCf = PRL250.ReqIdCf;
                    objPRL250.ReqOdCf = PRL250.ReqOdCf;
                    objPRL250.ReqTotalLength = PRL250.ReqTotalLength;
                    objPRL250.WidthOfEsscStripUsedForOverlay = PRL250.WidthOfEsscStripUsedForOverlay;
                    objPRL250.CheckPoint1 = PRL250.CheckPoint1;
                    objPRL250.CheckPoint2 = PRL250.CheckPoint2;
                    objPRL250.CheckPoint3 = PRL250.CheckPoint3;
                    objPRL250.CheckPoint4 = PRL250.CheckPoint4;
                    objPRL250.CheckPoint6 = PRL250.CheckPoint6;
                    objPRL250.CheckPoint6_2 = PRL250.CheckPoint6_2;
                    objPRL250.QCRemarks = PRL250.QCRemarks;
                    objPRL250.Result = PRL250.Result;

                    objPRL250.EditedBy = UserName;
                    objPRL250.EditedOn = DateTime.Now;

                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL250.ActFilledBy = UserName;
                            objPRL250.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL250.ReqFilledBy = UserName;
                            objPRL250.ReqFilledOn = DateTime.Now;
                        }
                    }

                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL250.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL250 objPRL250 = db.PRL250.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL250 != null && string.IsNullOrWhiteSpace(objPRL250.ProtocolNo))
                    {
                        db.PRL250.Remove(objPRL250);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL250.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL251> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL251> lstAddPRL251 = new List<PRL251>();
                List<PRL251> lstDeletePRL251 = new List<PRL251>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRL251 obj = db.PRL251.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.Orientation = item.Orientation;
                                obj.ActAtTopOpenEnd = item.ActAtTopOpenEnd;
                                obj.ActAtBottomOpenEnd = item.ActAtBottomOpenEnd;
                                obj.EditedBy = UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRL251 obj = new PRL251();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.Orientation))
                            {
                                obj.Orientation = item.Orientation;
                                obj.ActAtTopOpenEnd = item.ActAtTopOpenEnd;
                                obj.ActAtBottomOpenEnd = item.ActAtBottomOpenEnd;
                                obj.CreatedBy = UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRL251.Add(obj);
                            }
                        }
                    }
                }
                if (lstAddPRL251.Count > 0)
                {
                    db.PRL251.AddRange(lstAddPRL251);
                }


                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();
                    lstDeletePRL251 = db.PRL251.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                }
                else
                {
                    lstDeletePRL251 = db.PRL251.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL251.Count > 0)
                {
                    db.PRL251.RemoveRange(lstDeletePRL251);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2Linkage(List<PRL252> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL252> lstAddPRL252 = new List<PRL252>();
                List<PRL252> lstDeletePRL252 = new List<PRL252>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL252 obj = db.PRL252.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL252();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Location = item.Location;
                        obj.ActIdCf = item.ActIdCf;
                        obj.ActOdCf = item.ActOdCf;

                        if (isAdded)
                        {
                            lstAddPRL252.Add(obj);
                        }
                    }
                }
                if (lstAddPRL252.Count > 0)
                {
                    db.PRL252.AddRange(lstAddPRL252);
                }

                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL252 = db.PRL252.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL252 = db.PRL252.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL252.Count > 0)
                {
                    db.PRL252.RemoveRange(lstDeletePRL252);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3Linkage(List<PRL253> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL253> lstAddPRL253 = new List<PRL253>();
                List<PRL253> lstDeletePRL253 = new List<PRL253>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL253 obj = db.PRL253.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        else
                        {
                            obj = new PRL253();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }

                        obj.RequiredValue = item.RequiredValue;
                        obj.ActualValue = item.ActualValue;

                        if (isAdded)
                        {
                            lstAddPRL253.Add(obj);
                        }
                    }
                }
                if (lstAddPRL253.Count > 0)
                {
                    db.PRL253.AddRange(lstAddPRL253);
                }
                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL253 = db.PRL253.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL253 = db.PRL253.Where(x => x.HeaderId == HeaderId).ToList();
                }

                if (lstDeletePRL253.Count > 0)
                {
                    db.PRL253.RemoveRange(lstDeletePRL253);
                }

                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line4 details
        [HttpPost]
        public JsonResult SaveProtocolLine4Linkage(List<PRL254> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL254> lstAddPRL254 = new List<PRL254>();
                List<PRL254> lstDeletePRL254 = new List<PRL254>();

                if (lst != null)
                {
                    List<PRL254> objDelete = db.PRL254.Where(x => x.HeaderId == HeaderId).ToList();
                    db.PRL254.RemoveRange(objDelete);
                    db.SaveChanges();

                    foreach (var item in lst)
                    {
                        PRL254 obj = new PRL254();
                        obj.HeaderId = item.HeaderId;
                        obj.CreatedBy = UserName;
                        obj.CreatedOn = DateTime.Now;
                        obj.TotalNoOfDirection = item.TotalNoOfDirection;
                        obj.ActSeqFollowed = item.ActSeqFollowed;
                        lstAddPRL254.Add(obj);
                    }
                    if (lstAddPRL254.Count > 0)
                    {
                        db.PRL254.AddRange(lstAddPRL254);
                        db.SaveChanges();
                    }
                }

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion
        #endregion
    }
}