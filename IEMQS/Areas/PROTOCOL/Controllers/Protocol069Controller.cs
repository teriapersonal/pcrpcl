﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol069Controller : clsBase
    {
        // GET: PROTOCOL/Protocol069

        string ControllerURL = "/PROTOCOL/Protocol069/";
        string Title = "AIR TEST OF REINFORCEMENT PADS OF NOZZLE";

        #region Details View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO340 objPRO340 = new PRO340();
            if (!id.HasValue)
            {
                try
                {
                    objPRO340.ProtocolNo = string.Empty;
                    objPRO340.CreatedBy = objClsLoginInfo.UserName;
                    objPRO340.CreatedOn = DateTime.Now;

                    #region NOTES
                    objPRO340.Notes = " 1. AIR TEST OF REINFORCEMENT PAD CHECKED AND FOUND SATISFACTORY. \r\n 2.COMPLETE WELD JOINTS EXAMINED WITH SOAP SOLUTION TO ENSURE NO LEAKAGE";

                    #endregion


                    db.PRO340.Add(objPRO340);
                    db.SaveChanges();
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO340.HeaderId;
            }
            else
            {
                objPRO340 = db.PRO340.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstStage = new List<string> { " BEFORE PWHT", "AFTER PWHT", " BEFORE HYDRO", "AFTER HYDRO", "AFTER COMPLETE WELD", "AFTER PARTIAL WELD", "AFTER ROOT RUN WELDING" };

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.Stage = lstStage.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO341> lstPRO341 = db.PRO341.Where(x => x.HeaderId == objPRO340.HeaderId).ToList();
            List<PRO342> lstPRO342 = db.PRO342.Where(x => x.HeaderId == objPRO340.HeaderId).ToList();


            ViewBag.lstPRO341 = lstPRO341;
            ViewBag.lstPRO342 = lstPRO342;


            #endregion

            return View(objPRO340);
        }

        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO340 PRO340)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO340.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO340 objPRO340 = db.PRO340.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO340.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO340.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data


                        objPRO340.ProtocolNo = PRO340.ProtocolNo;
                        objPRO340.Stage = PRO340.Stage;
                        objPRO340.PressureGaugeDetails = PRO340.PressureGaugeDetails;
                        objPRO340.Notes = PRO340.Notes;
                        objPRO340.Result = PRO340.Result;

                        objPRO340.EditedBy = objClsLoginInfo.UserName;
                        objPRO340.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO340.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO340 objPRO340 = db.PRO340.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO340 != null && string.IsNullOrWhiteSpace(objPRO340.ProtocolNo))
                    {
                        db.PRO340.Remove(objPRO340);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO340.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO341> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO341> lstAddPRO341 = new List<PRO341>();
                List<PRO341> lstDeletePRO341 = new List<PRO341>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO341 obj = db.PRO341.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO341();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }



                        obj.SrNo = item.SrNo;
                        obj.NozzleNo = item.NozzleNo;
                        obj.RequiredPressure = item.RequiredPressure;
                        obj.ActualPressure = item.ActualPressure;
                        obj.Remarks = item.Remarks;


                        if (isAdded)
                        {
                            lstAddPRO341.Add(obj);
                        }
                    }
                    if (lstAddPRO341.Count > 0)
                    {
                        db.PRO341.AddRange(lstAddPRO341);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO341 = db.PRO341.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO341.Count > 0)
                    {
                        db.PRO341.RemoveRange(lstDeletePRO341);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO341 = db.PRO341.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO341.Count > 0)
                    {
                        db.PRO341.RemoveRange(lstDeletePRO341);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2(List<PRO342> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO342> lstAddPRO342 = new List<PRO342>();
                List<PRO342> lstDeletePRO342 = new List<PRO342>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO342 obj = db.PRO342.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {

                                obj.GaugeSerialNumber = item.GaugeSerialNumber;
                                obj.GaugeCelebrationDueDate = item.GaugeCelebrationDueDate;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO342 obj = new PRO342();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.GaugeSerialNumber))
                            {
                                obj.GaugeSerialNumber = item.GaugeSerialNumber;
                                obj.GaugeCelebrationDueDate = item.GaugeCelebrationDueDate;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO342.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO342.Count > 0)
                    {
                        db.PRO342.AddRange(lstAddPRO342);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO342 = db.PRO342.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO342.Count > 0)
                    {
                        db.PRO342.RemoveRange(lstDeletePRO342);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO342 = db.PRO342.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO342.Count > 0)
                    {
                        db.PRO342.RemoveRange(lstDeletePRO342);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion



        #endregion

        #region  Linkage View Code   

        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL340 objPRL340 = new PRL340();
            objPRL340 = db.PRL340.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL340 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL340.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstStage = new List<string> { " BEFORE PWHT", "AFTER PWHT", " BEFORE HYDRO", "AFTER HYDRO", "AFTER COMPLETE WELD", "AFTER PARTIAL WELD", "AFTER ROOT RUN WELDING" };

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.Stage = lstStage.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();


            #region bind lines
            List<PRL341> lstPRL341 = db.PRL341.Where(x => x.HeaderId == objPRL340.HeaderId).ToList();
            List<PRL342> lstPRL342 = db.PRL342.Where(x => x.HeaderId == objPRL340.HeaderId).ToList();


            ViewBag.lstPRL341 = lstPRL341;
            ViewBag.lstPRL342 = lstPRL342;


            #endregion

            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                //ViewBag.isActualPressureEditable = "false";
                //ViewBag.isRemarksEditable = "false"; 
                //ViewBag.isPressureGaugeDetailsEditable = "false";

                //ViewBag.isGaugeSerialNumberEditable = "false";
                //ViewBag.isGaugeCelebrationDueDateEditable = "false";

                //ViewBag.isNotesEditable = "false";

                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.DrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL340.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL340.ActFilledBy) && (objPRL340.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                            //ViewBag.isActualPressureEditable = "true";
                            //ViewBag.isRemarksEditable = "true";
                            //ViewBag.isPressureGaugeDetailsEditable = "true";

                            //ViewBag.isGaugeSerialNumberEditable = "true";
                            //ViewBag.isGaugeCelebrationDueDateEditable = "true";

                            //ViewBag.isNotesEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL340.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL340);
        }

        [HttpPost]
        public ActionResult LinkageMobile(int? id, int? m, string APIRequestFrom = "", string UserName = "")
        {
            string ProjectDesc = "";
            string ManufacturingCode = "";
            string AccessRole = "";
            bool isEditable = false;
            var DrawingNo = new List<string>();
            bool isQCRemarkEditable = false;

            PRL340 objPRL340 = new PRL340();
            db.Configuration.ProxyCreationEnabled = false;
            objPRL340 = db.PRL340.Where(i => i.HeaderId == id).FirstOrDefault();
            db.Entry(objPRL340).State = System.Data.Entity.EntityState.Detached;
            NDEModels objNDEModels = new NDEModels();
            if (objPRL340 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL340.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }

            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
            AccessRole = objUserRoleAccessDetails.UserDesignation;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstStage = new List<string> { " BEFORE PWHT", "AFTER PWHT", " BEFORE HYDRO", "AFTER HYDRO", "AFTER COMPLETE WELD", "AFTER PARTIAL WELD", "AFTER ROOT RUN WELDING" };

            var YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var Stage = lstStage.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();


            #region bind lines
            List<PRL341> lstPRL341 = db.PRL341.Where(x => x.HeaderId == objPRL340.HeaderId).ToList();
            List<PRL342> lstPRL342 = db.PRL342.Where(x => x.HeaderId == objPRL340.HeaderId).ToList();


            #endregion
            try
            {
                isEditable = false;
                isQCRemarkEditable = false;
                if (m == 1)
                {
                    isEditable = true;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL340.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL340.ActFilledBy) && (objPRL340.RequestNo.HasValue))
                        {
                            isEditable = false;
                            isQCRemarkEditable = true;
                        }
                        else
                        {
                            DrawingNo = (new GeneralController()).GetPLMDrawingNumber(objPRL340.Project).ToList();
                        }
                    }
                }
                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    ProjectDesc = ProjectDesc,
                    ManufacturingCode = ManufacturingCode,
                    isEditable = isEditable,
                    isQCRemarkEditable = isQCRemarkEditable,
                    DrawingNo = DrawingNo,
                    AccessRole = AccessRole,
                    YesNoEnum = YesNoEnum,
                    Stage = Stage,
                    YesNAEnum = YesNAEnum,

                    objPRL340 = objPRL340,


                    lstPRL341 = lstPRL341,
                    lstPRL342 = lstPRL342

                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                return Json(new
                {
                    res = "0",
                    msg = ex.Message,
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL340 PRL340, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = PRL340.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL340 objPRL340 = db.PRL340.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data

                    objPRL340.DrawingRevisionNo = PRL340.DrawingRevisionNo;
                    objPRL340.DrawingNo = PRL340.DrawingNo;
                    objPRL340.DCRNo = PRL340.DCRNo;
                    objPRL340.ProtocolNo = PRL340.ProtocolNo != null ? PRL340.ProtocolNo : "";

                    objPRL340.ProtocolNo = PRL340.ProtocolNo;
                    objPRL340.Stage = PRL340.Stage;
                    objPRL340.PressureGaugeDetails = PRL340.PressureGaugeDetails;
                    objPRL340.Notes = PRL340.Notes;
                    objPRL340.Result = PRL340.Result;

                    objPRL340.EditedBy = UserName;
                    objPRL340.EditedOn = DateTime.Now;

                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL340.ActFilledBy = UserName;
                            objPRL340.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL340.ReqFilledBy = UserName;
                            objPRL340.ReqFilledOn = DateTime.Now;
                        }
                    }

                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL340.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL340 objPRL340 = db.PRL340.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL340 != null && string.IsNullOrWhiteSpace(objPRL340.ProtocolNo))
                    {
                        db.PRL340.Remove(objPRL340);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL340.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL341> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL341> lstAddPRL341 = new List<PRL341>();
                List<PRL341> lstDeletePRL341 = new List<PRL341>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL341 obj = db.PRL341.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL341();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SrNo = item.SrNo;
                        obj.NozzleNo = item.NozzleNo;
                        obj.RequiredPressure = item.RequiredPressure;
                        obj.ActualPressure = item.ActualPressure;
                        obj.Remarks = item.Remarks;

                        if (isAdded)
                        {
                            lstAddPRL341.Add(obj);
                        }
                    }
                }
                if (lstAddPRL341.Count > 0)
                {
                    db.PRL341.AddRange(lstAddPRL341);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL341 = db.PRL341.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL341 = db.PRL341.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL341.Count > 0)
                {
                    db.PRL341.RemoveRange(lstDeletePRL341);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2Linkage(List<PRL342> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL342> lstAddPRL342 = new List<PRL342>();
                List<PRL342> lstDeletePRL342 = new List<PRL342>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRL342 obj = db.PRL342.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {

                                obj.GaugeSerialNumber = item.GaugeSerialNumber;
                                obj.GaugeCelebrationDueDate = item.GaugeCelebrationDueDate;
                                obj.EditedBy = UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRL342 obj = new PRL342();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.GaugeSerialNumber))
                            {
                                obj.GaugeSerialNumber = item.GaugeSerialNumber;
                                obj.GaugeCelebrationDueDate = item.GaugeCelebrationDueDate;
                                obj.EditedBy = UserName;
                                obj.EditedOn = DateTime.Now;
                                obj.CreatedBy = UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRL342.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRL342.Count > 0)
                    {
                        db.PRL342.AddRange(lstAddPRL342);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRL342 = db.PRL342.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRL342.Count > 0)
                    {
                        db.PRL342.RemoveRange(lstDeletePRL342);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL342 = db.PRL342.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL342.Count > 0)
                    {
                        db.PRL342.RemoveRange(lstDeletePRL342);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion




        #endregion
    }
}