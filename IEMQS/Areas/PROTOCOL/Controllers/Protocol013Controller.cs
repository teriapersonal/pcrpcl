﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol013Controller : clsBase
    {
        string ControllerURL = "/PROTOCOL/Protocol013/";
        string Title = "NUB DIMENSION INSPECTION REPORTS FOR TAPER TYPE";
        // GET: PROTOCOL/Protocol013
        #region Details View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id,int? m)
        {
            PRO060 objPRO060 = new PRO060();
            if (!id.HasValue)
            {
                objPRO060.ProtocolNo = string.Empty;
                objPRO060.CreatedBy = objClsLoginInfo.UserName;
                objPRO060.CreatedOn = DateTime.Now;
                objPRO060.PRO061.Add(new PRO061
                {
                    Orientation = "0°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRO060.PRO061.Add(new PRO061
                {
                    Orientation = "30°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRO060.PRO061.Add(new PRO061
                {
                    Orientation = "60°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRO060.PRO061.Add(new PRO061
                {
                    Orientation = "90°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRO060.PRO061.Add(new PRO061
                {
                    Orientation = "120°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRO060.PRO061.Add(new PRO061
                {
                    Orientation = "150°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRO060.PRO061.Add(new PRO061
                {
                    Orientation = "180°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRO060.PRO061.Add(new PRO061
                {
                    Orientation = "210°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRO060.PRO061.Add(new PRO061
                {
                    Orientation = "240°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRO060.PRO061.Add(new PRO061
                {
                    Orientation = "270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRO060.PRO061.Add(new PRO061
                {
                    Orientation = "300°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRO060.PRO061.Add(new PRO061
                {
                    Orientation = "330°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                db.PRO060.Add(objPRO060);
                db.SaveChanges();
                ViewBag.HeaderId = objPRO060.HeaderId;
            }
            else
            {
                objPRO060 = db.PRO060.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            return View(objPRO060);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO060 PRO060)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO060.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO060 objPRO060 = db.PRO060.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO060.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO060.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data
                        objPRO060.ProtocolNo = PRO060.ProtocolNo;
                        objPRO060.ElevationReq = PRO060.ElevationReq;
                        objPRO060.ElevationTol = PRO060.ElevationTol;
                        objPRO060.NubWidthReq = PRO060.NubWidthReq;
                        objPRO060.NubWidthTol = PRO060.NubWidthTol;
                        objPRO060.NubHeightReq = PRO060.NubHeightReq;
                        objPRO060.NubHeightTol = PRO060.NubHeightTol;
                        objPRO060.NubIDReq = PRO060.NubIDReq;
                        objPRO060.NubIDTol = PRO060.NubIDTol;
                        objPRO060.Note2 = PRO060.Note2;
                        objPRO060.Note3 = PRO060.Note3;
                        objPRO060.Note4 = PRO060.Note4;
                        objPRO060.Note5 = PRO060.Note5;
                        objPRO060.Note5_1 = PRO060.Note5_1;
                        objPRO060.Note6 = PRO060.Note6;

                        objPRO060.EditedBy = objClsLoginInfo.UserName;
                        objPRO060.EditedOn = DateTime.Now;
                        var objPRO061 = PRO060.PRO061.ToList();
                        objPRO060.PRO061.ToList().ForEach(x =>
                        {
                            var obj = PRO060.PRO061.ToList().Where(i => i.LineId == x.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                x.EditedBy = objClsLoginInfo.UserName;
                                x.EditedOn = DateTime.Now;
                                x.A1 = obj.A1;
                                x.A2 = obj.A2;
                                x.B1 = obj.B1;
                                x.B2 = obj.B2;
                                x.C1 = obj.C1;
                                x.C2 = obj.C2;
                                x.Elevation = obj.Elevation;
                                x.NubWidth = obj.NubWidth;
                                x.NubHeight = obj.NubHeight;
                                x.NubID = obj.NubID;
                            }
                        });
                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol saved successfully";
                        objResponseMsg.HeaderId = objPRO060.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO060 objPRO060 = db.PRO060.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO060 != null && string.IsNullOrWhiteSpace(objPRO060.ProtocolNo))
                    {
                        db.PRO060.Remove(objPRO060);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO060.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region Line1 details
        [HttpPost]
        public ActionResult GetProtocolLines1Data(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro.Orientation like '%" + param.sSearch
                        + "%' or pro.Elevation like '%" + param.sSearch
                        + "%' or pro.NubWidth like '%" + param.sSearch
                        + "%' or pro.NubHeight like '%" + param.sSearch
                        + "%' or pro.NubID like '%" + param.sSearch
                        + "%' or pro.A1 like '%" + param.sSearch
                        + "%' or pro.A2 like '%" + param.sSearch
                        + "%' or pro.B1 like '%" + param.sSearch
                        + "%' or pro.B2 like '%" + param.sSearch
                        + "%' or pro.C1 like '%" + param.sSearch
                        + "%' or pro.C2 like '%" + param.sSearch
                        + "%')";
                }

                var lstResult = db.SP_IPI_PROTOCOL013_GET_LINES1_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                                   "1",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Orientation", "", "", true, "", false, "20", "PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "Elevation", "", "", true, "width:110px;", false, "10", "PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "NubWidth", "", "", true, "width:110px;", false, "10", "PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "NubHeight", "", "", true, "width:110px;", false, "10", "PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "NubID", "", "", true, "width:110px;", false, "10", "PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "A1", "", "", true, "width:110px;", false, "10", "PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "A2", "", "", true, "width:110px;", false, "10", "PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "B1", "", "", true, "width:110px;", false, "10", "PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "B2", "", "", true, "width:110px;", false, "10", "PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "C1", "", "", true, "width:110px;", false, "10", "PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "C2", "", "", true, "width:110px;", false, "10", "PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine1(0);")
                                };
                var data = (from uc in lstResult
                            select new[]
                            {
                                "1",
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.LineId),
                                Helper.GenerateHTMLTextbox(uc.LineId, "Orientation", uc.Orientation, "width:110px;", true, "", false, "20", "PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId, "Elevation", uc.Elevation.ToString(), "", true, "width:110px;", false, "10", "PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId, "NubWidth", uc.NubWidth.ToString(), "", true, "width:110px;", false, "10", "PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId, "NubHeight", uc.NubHeight.ToString(), "", true, "width:110px;", false, "10", "PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId, "NubID", uc.NubID.ToString(), "", true, "width:110px;", false, "10","PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId, "A1", uc.A1.ToString(), "", true, "width:110px;", false, "10", "PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId, "A2", uc.A2.ToString(), "", true, "width:110px;", false, "10", "PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId, "B1", uc.B1.ToString(), "", true, "width:110px;", false, "10", "PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId, "B2", uc.B2.ToString(), "", true, "width:110px;", false, "10", "PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId, "C1", uc.C1.ToString(), "", true, "width:110px;", false, "10", "PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId, "C2", uc.C2.ToString(), "", true, "width:110px;", false, "10", "PROD"),
                                HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.QC3.GetStringValue().ToUpper() ? HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine1','tblProtocolLines1')") : "")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine1(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRO061 objPRO061 = null;
            int newRowIndex = 0;
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                if (refHeaderId > 0)
                {
                    int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
                    if (refLineId > 0)
                    {
                        objPRO061 = db.PRO061.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRO061.Orientation = !string.IsNullOrEmpty(fc["Orientation" + refLineId]) ? Convert.ToString(fc["Orientation" + refLineId]).Trim() : "";
                        objPRO061.Elevation = (new clsManager()).GetDecimalValue(Convert.ToString(fc["Elevation" + refLineId]));
                        objPRO061.NubWidth = (new clsManager()).GetDecimalValue(Convert.ToString(fc["NubWidth" + refLineId]));
                        objPRO061.NubHeight = (new clsManager()).GetDecimalValue(Convert.ToString(fc["NubHeight" + refLineId]));
                        objPRO061.NubID = (new clsManager()).GetDecimalValue(Convert.ToString(fc["NubID" + refLineId]));
                        objPRO061.A1 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["A1" + refLineId]));
                        objPRO061.A2 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["A2" + refLineId]));
                        objPRO061.B1 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["B1" + refLineId]));
                        objPRO061.B2 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["B2" + refLineId]));
                        objPRO061.C1 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["C1" + refLineId]));
                        objPRO061.C2 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["C2" + refLineId]));
                        objPRO061.EditedBy = objClsLoginInfo.UserName;
                        objPRO061.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Value = "Data updated successfully";
                    }
                    else
                    {
                        objPRO061 = new PRO061();
                        objPRO061.HeaderId = refHeaderId;
                        objPRO061.Orientation = !string.IsNullOrEmpty(fc["Orientation" + newRowIndex]) ? Convert.ToString(fc["Orientation" + newRowIndex]).Trim() : "";
                        objPRO061.Elevation = (new clsManager()).GetDecimalValue(Convert.ToString(fc["Elevation" + newRowIndex]));
                        objPRO061.NubWidth = (new clsManager()).GetDecimalValue(Convert.ToString(fc["NubWidth" + newRowIndex]));
                        objPRO061.NubHeight = (new clsManager()).GetDecimalValue(Convert.ToString(fc["NubHeight" + newRowIndex]));
                        objPRO061.NubID = (new clsManager()).GetDecimalValue(Convert.ToString(fc["NubID" + newRowIndex]));
                        objPRO061.A1 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["A1" + newRowIndex]));
                        objPRO061.A2 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["A2" + newRowIndex]));
                        objPRO061.B1 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["B1" + newRowIndex]));
                        objPRO061.B2 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["B2" + newRowIndex]));
                        objPRO061.C1 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["C1" + newRowIndex]));
                        objPRO061.C2 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["C2" + newRowIndex]));
                        objPRO061.CreatedBy = objClsLoginInfo.UserName;
                        objPRO061.CreatedOn = DateTime.Now;
                        db.PRO061.Add(objPRO061);
                        db.SaveChanges();
                        objResponseMsg.Value = "Data added successfully";
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRO061.LineId;

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine1(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRO061 objPRO061 = db.PRO061.FirstOrDefault(c => c.LineId == lineId);
                if (objPRO061 != null)
                {
                    db.PRO061.Remove(objPRO061);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

        #region Linkage View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL060 objPRL060 = new PRL060();
            objPRL060 = db.PRL060.Where(i => i.HeaderId == id).FirstOrDefault();

            if (objPRL060 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL060.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objQproject.ManufacturingCode;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            if (m == 1)
            {
                ViewBag.isEditable = "true";
            }
            else
            { ViewBag.isEditable = "false"; }

            return View( objPRL060);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL060 PRL060)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRL060.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL060 objPRL060 = db.PRL060.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL060.DCRNo = PRL060.ProtocolNo != null ? PRL060.ProtocolNo : "";
                    objPRL060.ProtocolNo = PRL060.ProtocolNo;
                    objPRL060.ElevationReq = PRL060.ElevationReq;
                    objPRL060.ElevationTol = PRL060.ElevationTol;
                    objPRL060.NubWidthReq = PRL060.NubWidthReq;
                    objPRL060.NubWidthTol = PRL060.NubWidthTol;
                    objPRL060.NubHeightReq = PRL060.NubHeightReq;
                    objPRL060.NubHeightTol = PRL060.NubHeightTol;
                    objPRL060.NubIDReq = PRL060.NubIDReq;
                    objPRL060.NubIDTol = PRL060.NubIDTol;
                    objPRL060.Note2 = PRL060.Note2;
                    objPRL060.Note3 = PRL060.Note3;
                    objPRL060.Note4 = PRL060.Note4;
                    objPRL060.Note5 = PRL060.Note5;
                    objPRL060.Note5_1 = PRL060.Note5_1;
                    objPRL060.Note6 = PRL060.Note6;
                    
                    objPRL060.EditedBy = objClsLoginInfo.UserName;
                    objPRL060.EditedOn = DateTime.Now;
                                        
                    objPRL060.PRL061.ToList().ForEach(x =>
                    {
                        var obj = PRL060.PRL061.ToList().Where(i => i.LineId == x.LineId).FirstOrDefault();
                        if (obj != null)
                        {
                            x.EditedBy = objClsLoginInfo.UserName;
                            x.EditedOn = DateTime.Now;
                            x.A1 = obj.A1;
                            x.A2 = obj.A2;
                            x.B1 = obj.B1;
                            x.B2 = obj.B2;
                            x.C1 = obj.C1;
                            x.C2 = obj.C2;
                            x.Elevation = obj.Elevation;
                            x.NubWidth = obj.NubWidth;
                            x.NubHeight = obj.NubHeight;
                            x.NubID = obj.NubID;
                        }
                    });
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol saved successfully";
                    objResponseMsg.HeaderId = objPRL060.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL060 objPRL060 = db.PRL060.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL060 != null && string.IsNullOrWhiteSpace(objPRL060.ProtocolNo))
                    {
                        db.PRL060.Remove(objPRL060);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL060.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region Line1 details
        [HttpPost]
        public ActionResult GetProtocolLines1DataLinkage(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro.Orientation like '%" + param.sSearch
                        + "%' or pro.Elevation like '%" + param.sSearch
                        + "%' or pro.NubWidth like '%" + param.sSearch
                        + "%' or pro.NubHeight like '%" + param.sSearch
                        + "%' or pro.NubID like '%" + param.sSearch
                        + "%' or pro.A1 like '%" + param.sSearch
                        + "%' or pro.A2 like '%" + param.sSearch
                        + "%' or pro.B1 like '%" + param.sSearch
                        + "%' or pro.B2 like '%" + param.sSearch
                        + "%' or pro.C1 like '%" + param.sSearch
                        + "%' or pro.C2 like '%" + param.sSearch
                        + "%')";
                }

                var lstResult = db.SP_IPI_PROTOCOL013_LINKAGE_GET_LINES1_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                                   "1",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Orientation", "", "", true, "", false, "20", "PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "Elevation", "", "", true, "width:110px;", false, "10", "PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "NubWidth", "", "", true, "width:110px;", false, "10", "PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "NubHeight", "", "", true, "width:110px;", false, "10", "PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "NubID", "", "", true, "width:110px;", false, "10", "PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "A1", "", "", true, "width:110px;", false, "10", "PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "A2", "", "", true, "width:110px;", false, "10", "PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "B1", "", "", true, "width:110px;", false, "10", "PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "B2", "", "", true, "width:110px;", false, "10", "PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "C1", "", "", true, "width:110px;", false, "10", "PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "C2", "", "", true, "width:110px;", false, "10", "PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine1(0);")
                                };
                var data = (from uc in lstResult
                            select new[]
                            {
                                "1",
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.LineId),
                                Helper.GenerateHTMLTextbox(uc.LineId, "Orientation", uc.Orientation, "width:110px;", true, "", false, "20", "PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId, "Elevation", uc.Elevation.ToString(), "", true, "width:110px;", false, "10", "PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId, "NubWidth", uc.NubWidth.ToString(), "", true, "width:110px;", false, "10", "PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId, "NubHeight", uc.NubHeight.ToString(), "", true, "width:110px;", false, "10", "PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId, "NubID", uc.NubID.ToString(), "", true, "width:110px;", false, "10", "PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId, "A1", uc.A1.ToString(), "", true, "width:110px;", false, "10", "PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId, "A2", uc.A2.ToString(), "", true, "width:110px;", false, "10", "PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId, "B1", uc.B1.ToString(), "", true, "width:110px;", false, "10", "PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId, "B2", uc.B2.ToString(), "", true, "width:110px;", false, "10", "PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId, "C1", uc.C1.ToString(), "", true, "width:110px;", false, "10", "PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId, "C2", uc.C2.ToString(), "", true, "width:110px;", false, "10", "PROD"),
                                HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.QC3.GetStringValue().ToUpper() ? HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine1Linkage','tblProtocolLines1')") : "")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine1Linkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRL061 objPRL061 = null;
            int newRowIndex = 0;
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                if (refHeaderId > 0)
                {
                    int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
                    if (refLineId > 0)
                    {
                        objPRL061 = db.PRL061.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRL061.Orientation = !string.IsNullOrEmpty(fc["Orientation" + refLineId]) ? Convert.ToString(fc["Orientation" + refLineId]).Trim() : "";
                        objPRL061.Elevation = (new clsManager()).GetDecimalValue(Convert.ToString(fc["Elevation" + refLineId]));
                        objPRL061.NubWidth = (new clsManager()).GetDecimalValue(Convert.ToString(fc["NubWidth" + refLineId]));
                        objPRL061.NubHeight = (new clsManager()).GetDecimalValue(Convert.ToString(fc["NubHeight" + refLineId]));
                        objPRL061.NubID = (new clsManager()).GetDecimalValue(Convert.ToString(fc["NubID" + refLineId]));
                        objPRL061.A1 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["A1" + refLineId]));
                        objPRL061.A2 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["A2" + refLineId]));
                        objPRL061.B1 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["B1" + refLineId]));
                        objPRL061.B2 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["B2" + refLineId]));
                        objPRL061.C1 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["C1" + refLineId]));
                        objPRL061.C2 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["C2" + refLineId]));
                        objPRL061.EditedBy = objClsLoginInfo.UserName;
                        objPRL061.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Value = "Data updated successfully";
                    }
                    else
                    {
                        objPRL061 = new PRL061();
                        objPRL061.HeaderId = refHeaderId;
                        objPRL061.Orientation = !string.IsNullOrEmpty(fc["Orientation" + newRowIndex]) ? Convert.ToString(fc["Orientation" + newRowIndex]).Trim() : "";
                        objPRL061.Elevation = (new clsManager()).GetDecimalValue(Convert.ToString(fc["Elevation" + newRowIndex]));
                        objPRL061.NubWidth = (new clsManager()).GetDecimalValue(Convert.ToString(fc["NubWidth" + newRowIndex]));
                        objPRL061.NubHeight = (new clsManager()).GetDecimalValue(Convert.ToString(fc["NubHeight" + newRowIndex]));
                        objPRL061.NubID = (new clsManager()).GetDecimalValue(Convert.ToString(fc["NubID" + newRowIndex]));
                        objPRL061.A1 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["A1" + newRowIndex]));
                        objPRL061.A2 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["A2" + newRowIndex]));
                        objPRL061.B1 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["B1" + newRowIndex]));
                        objPRL061.B2 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["B2" + newRowIndex]));
                        objPRL061.C1 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["C1" + newRowIndex]));
                        objPRL061.C2 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["C2" + newRowIndex]));
                        objPRL061.CreatedBy = objClsLoginInfo.UserName;
                        objPRL061.CreatedOn = DateTime.Now;
                        db.PRL061.Add(objPRL061);
                        db.SaveChanges();
                        objResponseMsg.Value = "Data added successfully";
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRL061.LineId;
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine1Linkage(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRL061 objPRL061 = db.PRL061.FirstOrDefault(c => c.LineId == lineId);
                if (objPRL061 != null)
                {
                    db.PRL061.Remove(objPRL061);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion
        public string HTMLActionString(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", string onHrefURL = "", bool isDisable = false)
        {
            string htmlControl = "";
            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? onClickMethod : "";
            string onHref = !string.IsNullOrEmpty(onHrefURL) ? "href='" + onHrefURL + "'" : "";

            if (isDisable)
            {
                htmlControl += "<i id=\"" + inputID.ToString() + "\" name=\"" + inputID + "\" ></i>";
                //htmlControl = "<i id=\"" + inputID + "' name='" + inputID + "' style='cursor: pointer; opacity:0.3; margin-right:10px;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";
            }
            else
            {
                htmlControl += "<i id=\"" + inputID.ToString() + "\" name=\"" + inputID + "\"  style='cursor: pointer; margin-right:10px;' Title=\"" + buttonTooltip + "\" class=\"" + className + "\"  onclick=\"" + onClickEvent + "\" ></i>";
                //htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer; margin-right:10px;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
            }

            if (!string.IsNullOrEmpty(onHref))
            {
                htmlControl = "<a " + onHref + " >" + htmlControl + "</a>";
            }

            return htmlControl;
        }
    }
}