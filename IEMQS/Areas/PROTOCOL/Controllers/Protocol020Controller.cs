﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;
namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol020Controller : clsBase
    {
        string ControllerURL = "/PROTOCOL/Protocol020/";
        string Title = "VISUAL & DIMENSION INSPECTION OF SKIRT ASSEMBLY";
        // GET: PROTOCOL/Protocol020
        #region Details View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO095 objPRO095 = new PRO095();
            if (!id.HasValue)
            {
                objPRO095.ProtocolNo = string.Empty;
                objPRO095.CreatedBy = objClsLoginInfo.UserName;
                objPRO095.CreatedOn = DateTime.Now;
                db.PRO095.Add(objPRO095);
                db.SaveChanges();
                ViewBag.HeaderId = objPRO095.HeaderId;
            }
            else
            {
                objPRO095 = db.PRO095.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            List<string> lstYesNoNA = clsImplementationEnum.getYesNoNA().ToList();
            ViewBag.YesNoNAEnum = lstYesNoNA.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            return View(objPRO095);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO095 PRO095)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO095.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO095 objPRO095 = db.PRO095.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO095.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO095.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data
                        objPRO095.ProtocolNo = PRO095.ProtocolNo;
                        objPRO095.OutSideCircumferenceReq = PRO095.OutSideCircumferenceReq;
                        objPRO095.ActualTop = PRO095.ActualTop;
                        objPRO095.ActualBottom = PRO095.ActualBottom;
                        objPRO095.IDCircumferenceAtOpenEndReq = PRO095.IDCircumferenceAtOpenEndReq;
                        objPRO095.IDCircumferenceAtOpenEndAct = PRO095.IDCircumferenceAtOpenEndAct;
                        objPRO095.ReqArcLength1 = PRO095.ReqArcLength1;
                        objPRO095.ReqArcLength2 = PRO095.ReqArcLength2;
                        objPRO095.ReqArcLength3 = PRO095.ReqArcLength3;
                        objPRO095.ReqArcLength4 = PRO095.ReqArcLength4;
                        objPRO095.ArcLengthTop1 = PRO095.ArcLengthTop1;
                        objPRO095.ArcLengthTop2 = PRO095.ArcLengthTop2;
                        objPRO095.ArcLengthTop3 = PRO095.ArcLengthTop3;
                        objPRO095.ArcLengthTop4 = PRO095.ArcLengthTop4;
                        objPRO095.ArcLengthBottom1 = PRO095.ArcLengthBottom1;
                        objPRO095.ArcLengthBottom2 = PRO095.ArcLengthBottom2;
                        objPRO095.ArcLengthBottom3 = PRO095.ArcLengthBottom3;
                        objPRO095.ArcLengthBottom4 = PRO095.ArcLengthBottom4;
                        objPRO095.ReqId = PRO095.ReqId;
                        objPRO095.ActualAtTop1 = PRO095.ActualAtTop1;
                        objPRO095.ActualAtTop2 = PRO095.ActualAtTop2;
                        objPRO095.ActualAtTop3 = PRO095.ActualAtTop3;
                        objPRO095.ActualAtTop4 = PRO095.ActualAtTop4;
                        objPRO095.ActualAtTop5 = PRO095.ActualAtTop5;
                        objPRO095.ActualAtTop6 = PRO095.ActualAtTop6;
                        objPRO095.ActualAtTop7 = PRO095.ActualAtTop7;
                        objPRO095.ActualAtTop8 = PRO095.ActualAtTop8;
                        objPRO095.ActualAtBottom1 = PRO095.ActualAtBottom1;
                        objPRO095.ActualAtBottom2 = PRO095.ActualAtBottom2;
                        objPRO095.ActualAtBottom3 = PRO095.ActualAtBottom3;
                        objPRO095.ActualAtBottom4 = PRO095.ActualAtBottom4;
                        objPRO095.ActualAtBottom5 = PRO095.ActualAtBottom5;
                        objPRO095.ActualAtBottom6 = PRO095.ActualAtBottom6;
                        objPRO095.ActualAtBottom7 = PRO095.ActualAtBottom7;
                        objPRO095.ActualAtBottom8 = PRO095.ActualAtBottom8;
                        objPRO095.Remarks1 = PRO095.Remarks1;
                        objPRO095.Remarks2 = PRO095.Remarks2;
                        objPRO095.Remarks3 = PRO095.Remarks3;
                        objPRO095.Remarks4 = PRO095.Remarks4;
                        objPRO095.Remarks5 = PRO095.Remarks5;
                        objPRO095.Remarks6 = PRO095.Remarks6;
                        objPRO095.Remarks7 = PRO095.Remarks7;
                        objPRO095.Remarks8 = PRO095.Remarks8;
                        objPRO095.Note2 = PRO095.Note2;
                        objPRO095.Note31 = PRO095.Note31;
                        objPRO095.Note32 = PRO095.Note32.HasValue ? Manager.getDateTime(PRO095.Note32.Value.ToShortDateString()) : PRO095.Note32;
                        objPRO095.Note4 = PRO095.Note4;
                        objPRO095.Note5 = PRO095.Note5;
                        objPRO095.Note6 = PRO095.Note6;
                        objPRO095.Note7 = PRO095.Note7;
                        objPRO095.Note8 = PRO095.Note8;
                        objPRO095.Note9 = PRO095.Note9;
                        objPRO095.Note10 = PRO095.Note10;
                        objPRO095.Note11 = PRO095.Note11;
                        objPRO095.Note12 = PRO095.Note12;
                        objPRO095.Note13 = PRO095.Note13;
                        objPRO095.Note14 = PRO095.Note14;
                        objPRO095.Note15 = PRO095.Note15;
                        objPRO095.Note16 = PRO095.Note16;
                        objPRO095.Note17 = PRO095.Note17;
                        objPRO095.RequiredHoleDia = PRO095.RequiredHoleDia;
                        objPRO095.ActualHoleDiaMin = PRO095.ActualHoleDiaMin;
                        objPRO095.ActualHoleDiaMax = PRO095.ActualHoleDiaMax;
                        objPRO095.TotalNoOfHoles = PRO095.TotalNoOfHoles;
                        objPRO095.HoleNumbers = PRO095.HoleNumbers;
                        objPRO095.DistanceReq1 = PRO095.DistanceReq1;
                        objPRO095.DistanceReq2 = PRO095.DistanceReq2;
                        objPRO095.DistanceReq3 = PRO095.DistanceReq3;
                        objPRO095.DistanceReq4 = PRO095.DistanceReq4;
                        objPRO095.DistanceAct1 = PRO095.DistanceAct1;
                        objPRO095.DistanceAct2 = PRO095.DistanceAct2;
                        objPRO095.DistanceAct3 = PRO095.DistanceAct3;
                        objPRO095.DistanceAct4 = PRO095.DistanceAct4;
                        objPRO095.ConentricityReq = PRO095.ConentricityReq;
                        objPRO095.ConentricityAct = PRO095.ConentricityAct;
                        objPRO095.FlatnessReadingOfBaseRingTol = PRO095.FlatnessReadingOfBaseRingTol;

                        objPRO095.EditedBy = objClsLoginInfo.UserName;
                        objPRO095.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol saved successfully";
                        objResponseMsg.HeaderId = objPRO095.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO095 objPRO095 = db.PRO095.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO095 != null && string.IsNullOrWhiteSpace(objPRO095.ProtocolNo))
                    {
                        db.PRO096.RemoveRange(objPRO095.PRO096.ToList());
                        db.PRO095.Remove(objPRO095);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_INSPECTION_OF_SKIRT_ASSEMBLY.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO095.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line1 details
        [HttpPost]
        public ActionResult GetProtocolLines1Data(JQueryDataTableParamModel param, string headerId)
        {
            try
            {

                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro.Description like '%" + param.sSearch
                        + "%' or pro.ElevationReq like '%" + param.sSearch
                        + "%' or pro.ElevationAct like '%" + param.sSearch
                        + "%' or pro.ProjectionReq like '%" + param.sSearch
                        + "%' or pro.ProjectionAct like '%" + param.sSearch
                        + "%' or pro.OrientationReq like '%" + param.sSearch
                        + "%' or pro.OrientationReqArcLen like '%" + param.sSearch
                        + "%' or pro.OrientationAct like '%" + param.sSearch
                        + "%' or pro.OrientationActArcLen like '%" + param.sSearch
                        + "%' or Remark like '%" + param.sSearch
                        + "%')";
                }

                var lstResult = db.SP_IPI_PROTOCOL020_GET_LINES1_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] {"1",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Description", "", "", false, "", false, "25","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "ElevationReq", "", "", false, "", false, "25","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "ElevationAct", "", "", false, "", false, "25","PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "ProjectionReq", "", "", false, "", false, "25","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "ProjectionAct", "", "", false, "", false, "25","PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "OrientationReq", "", "", false, "", false, "25","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "OrientationReqArcLen", "", "", false, "", false, "25","PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "OrientationAct", "", "", false, "", false, "25","PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "OrientationActArcLen", "", "", false, "", false, "25","PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Remark", "", "", false, "", false, "25","PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine1(0);")
                                };
                var data = (from uc in lstResult
                            select new[]
                            {"1",
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                               Helper.GenerateHTMLTextbox(uc.LineId, "Description",Convert.ToString(uc.Description),"", true, "", false, "25","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "ElevationReq",Convert.ToString(uc.ElevationReq),"", true, "", false, "25","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "ElevationAct",Convert.ToString(uc.ElevationAct),"", true, "", false, "25","PROD"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "ProjectionReq",Convert.ToString(uc.ProjectionReq),"", true, "", false, "25","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "ProjectionAct",Convert.ToString(uc.ProjectionAct),"", true, "", false, "25","PROD"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "OrientationReq",Convert.ToString(uc.OrientationReq),"", true, "", false, "25","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "OrientationReqArcLen",Convert.ToString(uc.OrientationReqArcLen),"", true, "", false, "25","PROD"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "OrientationAct",Convert.ToString(uc.OrientationAct),"", true, "", false, "25","PROD"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "OrientationActArcLen",Convert.ToString(uc.OrientationActArcLen),"", true, "", false, "25","PROD"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "Remark",Convert.ToString(uc.Remark),"", true, "", false, "25","PROD"),
                               Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == "QC3" ? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine1','tblProtocolLines1')"):"")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine1(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRO096 objPRO096 = new PRO096();
            // int newRowIndex = 0;
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
                if (refHeaderId > 0)
                {
                    string strDescription = !string.IsNullOrEmpty(fc["Description" + refLineId]) ? Convert.ToString(fc["Description" + refLineId]).Trim() : "";
                    string strElevationReq = !string.IsNullOrEmpty(fc["ElevationReq" + refLineId]) ? Convert.ToString(fc["ElevationReq" + refLineId]).Trim() : "";
                    string strElevationAct = !string.IsNullOrEmpty(fc["ElevationAct" + refLineId]) ? Convert.ToString(fc["ElevationAct" + refLineId]).Trim() : "";
                    string strProjectionReq = !string.IsNullOrEmpty(fc["ProjectionReq" + refLineId]) ? Convert.ToString(fc["ProjectionReq" + refLineId]).Trim() : "";
                    string strProjectionAct = !string.IsNullOrEmpty(fc["ProjectionAct" + refLineId]) ? Convert.ToString(fc["ProjectionAct" + refLineId]).Trim() : "";
                    string strOrientationReq = !string.IsNullOrEmpty(fc["OrientationReq" + refLineId]) ? Convert.ToString(fc["OrientationReq" + refLineId]).Trim() : "";
                    string strOrientationReqArcLen = !string.IsNullOrEmpty(fc["OrientationReqArcLen" + refLineId]) ? Convert.ToString(fc["OrientationReqArcLen" + refLineId]).Trim() : "";
                    string strOrientationAct = !string.IsNullOrEmpty(fc["OrientationAct" + refLineId]) ? Convert.ToString(fc["OrientationAct" + refLineId]).Trim() : "";
                    string strOrientationActArcLen = !string.IsNullOrEmpty(fc["OrientationActArcLen" + refLineId]) ? Convert.ToString(fc["OrientationActArcLen" + refLineId]).Trim() : "";
                    string strRemark = !string.IsNullOrEmpty(fc["Remark" + refLineId]) ? Convert.ToString(fc["Remark" + refLineId]).Trim() : "";

                    #region Add New Spot Line1
                    if (refLineId > 0)
                    {
                        objPRO096 = db.PRO096.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRO096.EditedBy = objClsLoginInfo.UserName;
                        objPRO096.EditedOn = DateTime.Now;
                    }
                    objPRO096.HeaderId = refHeaderId;
                    objPRO096.Description = strDescription;
                    objPRO096.ElevationReq = strElevationReq;
                    objPRO096.ElevationAct = strElevationAct;
                    objPRO096.ProjectionReq = strProjectionReq;
                    objPRO096.ProjectionAct = strProjectionAct;
                    objPRO096.OrientationReq = strOrientationReq;
                    objPRO096.OrientationReqArcLen = strOrientationReqArcLen;
                    objPRO096.OrientationAct = strOrientationAct;
                    objPRO096.OrientationActArcLen = strOrientationActArcLen;
                    objPRO096.Remark = strRemark;


                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = "Data updated successfully";
                    }
                    else
                    {
                        objPRO096.CreatedBy = objClsLoginInfo.UserName;
                        objPRO096.CreatedOn = DateTime.Now;
                        db.PRO096.Add(objPRO096);
                        db.SaveChanges();
                        objResponseMsg.Value = "Data added successfully";
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRO096.LineId;
                    #endregion

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine1(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRO096 objPRO096 = db.PRO096.FirstOrDefault(c => c.LineId == lineId);
                if (objPRO096 != null)
                {
                    db.PRO096.Remove(objPRO096);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public ActionResult GetProtocolLines2Data(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro.HoleNo like '%" + param.sSearch
                        + "%' or pro.BcdReq like '%" + param.sSearch
                        + "%' or pro.BcdAct like '%" + param.sSearch
                        + "%')";
                }

                var lstResult = db.SP_IPI_PROTOCOL020_GET_LINES2_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] {"2",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "HoleNo", "", "", false, "", false, "25","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "BcdReq", "", "", false, "", false, "25","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "BcdAct", "", "", false, "", false, "25","PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine2(0);")
                                };
                var data = (from uc in lstResult
                            select new[]
                            {"2",
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                               Helper.GenerateHTMLTextbox(uc.LineId, "HoleNo",Convert.ToString(uc.HoleNo), "", true, "", false, "25","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "BcdReq",Convert.ToString(uc.BcdReq), "", true, "", false, "25","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "BcdAct",Convert.ToString(uc.BcdAct), "", true, "", false, "25","PROD"),
                               Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == "QC3" ? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine2','tblProtocolLines2')"):"")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine2(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRO097 objPRO097 = new PRO097();
            //int newRowIndex = 0;
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
                if (refHeaderId > 0)
                {
                    string strHoleNo = !string.IsNullOrEmpty(fc["HoleNo" + refLineId]) ? Convert.ToString(fc["HoleNo" + refLineId]).Trim() : "";
                    string strBcdReq = !string.IsNullOrEmpty(fc["BcdReq" + refLineId]) ? Convert.ToString(fc["BcdReq" + refLineId]).Trim() : "";
                    string strBcdAct = !string.IsNullOrEmpty(fc["BcdAct" + refLineId]) ? Convert.ToString(fc["BcdAct" + refLineId]).Trim() : "";

                    #region Add New Spot Line2
                    if (refLineId > 0)
                    {
                        objPRO097 = db.PRO097.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRO097.EditedBy = objClsLoginInfo.UserName;
                        objPRO097.EditedOn = DateTime.Now;
                    }
                    objPRO097.HeaderId = refHeaderId;
                    objPRO097.HoleNo = strHoleNo;
                    objPRO097.BcdReq = strBcdReq;
                    objPRO097.BcdAct = strBcdAct;
                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = "Data updated successfully";
                    }
                    else
                    {
                        objPRO097.CreatedBy = objClsLoginInfo.UserName;
                        objPRO097.CreatedOn = DateTime.Now;
                        db.PRO097.Add(objPRO097);
                        db.SaveChanges();
                        objResponseMsg.Value = "Data added successfully";
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRO097.LineId;
                    #endregion

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine2(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRO097 objPRO097 = db.PRO097.FirstOrDefault(c => c.LineId == lineId);
                if (objPRO097 != null)
                {
                    db.PRO097.Remove(objPRO097);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line3 details
        [HttpPost]
        public ActionResult GetProtocolLines3Data(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro.Location like '%" + param.sSearch
                        + "%' or pro.At0 like '%" + param.sSearch
                        + "%' or pro.At90 like '%" + param.sSearch
                        + "%' or pro.At180 like '%" + param.sSearch
                        + "%' or pro.At270 like '%" + param.sSearch
                        + "%' or pro.Remark like '%" + param.sSearch
                        + "%')";
                }

                var lstResult = db.SP_IPI_PROTOCOL020_GET_LINES3_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                var lstOutBy = db.SP_IPI_PROTOCOL020_GET_OUTBY(intHeaderId).FirstOrDefault();
                var outBy0 = lstOutBy.OutBy0;
                var outBy90 = lstOutBy.OutBy90;
                var outBy180 = lstOutBy.OutBy180;
                var outBy270 = lstOutBy.OutBy270;

                int newRecordId = 0;
                var newRecord = new[] {"3",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Location", "", "", false, "", false, "25","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "At0", "", "", false, "", false, "25","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "At90", "", "", false, "", false, "25","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "At180", "", "", false, "", false, "25","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "At270", "", "", false, "", false, "25","PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Remark", "", "", false, "", false, "25","PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine3(0);")
                                };
                var data = (from uc in lstResult
                            select new[]
                            {"3",
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                               Helper.GenerateHTMLTextbox(uc.LineId, "Location",Convert.ToString(uc.Location), "", true, "", false, "25","PROD"),
                               Helper.GenerateNumericTextbox(uc.LineId, "At0",Convert.ToString(uc.At0), "", true, "", false, "25","PROD"),
                               Helper.GenerateNumericTextbox(uc.LineId, "At90",Convert.ToString(uc.At90), "", true, "", false, "25","PROD"),
                               Helper.GenerateNumericTextbox(uc.LineId, "At180",Convert.ToString(uc.At180), "", true, "", false, "25","PROD"),
                               Helper.GenerateNumericTextbox(uc.LineId, "At270",Convert.ToString(uc.At270), "", true, "", false, "25","PROD"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "Remark",Convert.ToString(uc.Remark), "", true, "", false, "25","PROD"),
                               Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == "QC3" ? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine3','tblProtocolLines3')"):"")
                           }).ToList();

                var outBy = new[] {
                                   "3",
                                   Convert.ToString(headerId),
                                   "",
                                   "<strong>OUT BY</strong>",
                                   Helper.GenerateNumericTextbox(0, "OutBy",  outBy0.ToString(), "", true, "", false),
                                   Helper.GenerateNumericTextbox(90, "OutBy", outBy90.ToString(), "", true, "", false),
                                   Helper.GenerateNumericTextbox(180, "OutBy",outBy180.ToString(), "", true, "",false),
                                   Helper.GenerateNumericTextbox(270, "OutBy",outBy270.ToString(), "", true, "",false),
                                   "",
                                   ""
                                    };

                data.Insert(0, newRecord);
                data.Insert(data.Count, outBy);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition,
                    outby = true,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine3(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRO098 objPRO098 = new PRO098();
            // int newRowIndex = 0;
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
                if (refHeaderId > 0)
                {
                    string strLocation = !string.IsNullOrEmpty(fc["Location" + refLineId]) ? Convert.ToString(fc["Location" + refLineId]).Trim() : "";
                    decimal? strAt0 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["At0" + refLineId]));
                    decimal? strAt90 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["At90" + refLineId]));
                    decimal? strAt180 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["At180" + refLineId]));
                    decimal? strAt270 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["At270" + refLineId]));
                    string strRemark = !string.IsNullOrEmpty(fc["Remark" + refLineId]) ? Convert.ToString(fc["Remark" + refLineId]).Trim() : "";

                    #region Add New Spot Line3
                    if (refLineId > 0)
                    {
                        objPRO098 = db.PRO098.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRO098.EditedBy = objClsLoginInfo.UserName;
                        objPRO098.EditedOn = DateTime.Now;
                    }
                    objPRO098.HeaderId = refHeaderId;
                    objPRO098.Location = strLocation;
                    objPRO098.At0 = strAt0;
                    objPRO098.At90 = strAt90;
                    objPRO098.At180 = strAt180;
                    objPRO098.At270 = strAt270;
                    objPRO098.Remark = strRemark;

                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = "Data updated successfully";
                    }
                    else
                    {
                        objPRO098.CreatedBy = objClsLoginInfo.UserName;
                        objPRO098.CreatedOn = DateTime.Now;
                        db.PRO098.Add(objPRO098);
                        db.SaveChanges();
                        objResponseMsg.Value = "Data added successfully";
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRO098.LineId;
                    #endregion

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine3(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRO098 objPRO098 = db.PRO098.FirstOrDefault(c => c.LineId == lineId);
                if (objPRO098 != null)
                {
                    db.PRO098.Remove(objPRO098);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line4 details
        [HttpPost]
        public ActionResult GetProtocolLines4Data(JQueryDataTableParamModel param, string headerId)
        {
            try
            {

                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro.Description like '%" + param.sSearch
                       + "%' or pro.BrlToTrl like '%" + param.sSearch
                       + "%' or pro.TrlToOpenEnd like '%" + param.sSearch
                       + "%' or pro.YIn like '%" + param.sSearch
                       + "%' or pro.YMid like '%" + param.sSearch
                       + "%' or pro.YOut like '%" + param.sSearch
                       + "%' or pro.H like '%" + param.sSearch
                       + "%' or pro.X like '%" + param.sSearch

                       + "%')";
                }

                var lstResult = db.SP_IPI_PROTOCOL020_GET_LINES4_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] {"4",

                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                      "",
                                   Helper.GenerateNumericTextbox(newRecordId, "BrlToTrl", "", "", false, "", false, "25","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "TrlToOpenEnd", "", "", false, "", false, "25","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "YIn", "", "", false, "", false, "25","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "YMid", "", "", false, "", false, "25","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "YOut", "", "", false, "", false, "25","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "H", "", "", false, "", false, "25","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "X", "", "", false, "", false, "25","PROD"),
            "","",
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine4(0);")
                                };
                var data = (from uc in lstResult
                            select new[]
                            {"4",

                                Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                                 Convert.ToString(uc.ROW_NO),
                                Helper.GenerateNumericTextbox(uc.LineId, "BrlToTrl",Convert.ToString(uc.BrlToTrl), "", true, "", false, "25","PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId, "TrlToOpenEnd",Convert.ToString(uc.TrlToOpenEnd), "", true, "", false, "25","PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId, "YIn",Convert.ToString(uc.YIn), "", true, "", false, "25","PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId, "YMid",Convert.ToString(uc.YMid), "", true, "", false, "25","PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId, "YOut",Convert.ToString(uc.YOut), "", true, "", false, "25","PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId, "H",Convert.ToString(uc.H), "", true, "", false, "25","PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId, "X",Convert.ToString(uc.X), "", true, "", false, "25","PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId, "YMin", Convert.ToString(uc.YMin), "", true, "", false, "25","PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId, "H+(uc.X-uc.YMin)", Convert.ToString(uc.H+(uc.X-uc.YMin)), "", true, "", false, "25","PROD"),
                                Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == "PROD3" ? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine4','tblProtocolLines4')"):"")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine4(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRO099 objPRO099 = new PRO099();
            // int newRowIndex = 0;
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
                if (refHeaderId > 0)
                {
                    decimal? strBrlToTrl = (new clsManager()).GetDecimalValue(Convert.ToString(fc["BrlToTrl" + refLineId]));
                    decimal? strTrlToOpenEnd = (new clsManager()).GetDecimalValue(Convert.ToString(fc["TrlToOpenEnd" + refLineId]));
                    decimal? strYIn = (new clsManager()).GetDecimalValue(Convert.ToString(fc["YIn" + refLineId]));
                    decimal? strYMid = (new clsManager()).GetDecimalValue(Convert.ToString(fc["YMid" + refLineId]));
                    decimal? strYOut = (new clsManager()).GetDecimalValue(Convert.ToString(fc["YOut" + refLineId]));
                    decimal? strH = (new clsManager()).GetDecimalValue(Convert.ToString(fc["H" + refLineId]));
                    decimal? strX = (new clsManager()).GetDecimalValue(Convert.ToString(fc["X" + refLineId]));
                    #region Add New Spot Line4
                    if (refLineId > 0)
                    {
                        objPRO099 = db.PRO099.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRO099.EditedBy = objClsLoginInfo.UserName;
                        objPRO099.EditedOn = DateTime.Now;
                    }
                    objPRO099.HeaderId = refHeaderId;
                    objPRO099.BrlToTrl = strBrlToTrl;
                    objPRO099.TrlToOpenEnd = strTrlToOpenEnd;
                    objPRO099.YIn = strYIn;
                    objPRO099.YMid = strYMid;
                    objPRO099.YOut = strYOut;
                    objPRO099.H = strH;
                    objPRO099.X = strX;


                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = "Data updated successfully";
                    }
                    else
                    {
                        objPRO099.CreatedBy = objClsLoginInfo.UserName;
                        objPRO099.CreatedOn = DateTime.Now;
                        db.PRO099.Add(objPRO099);
                        db.SaveChanges();
                        objResponseMsg.Value = "Data added successfully";
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRO099.LineId;
                    #endregion

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolLine4(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRO099 objPRO099 = db.PRO099.FirstOrDefault(c => c.LineId == lineId);
                if (objPRO099 != null)
                {
                    db.PRO099.Remove(objPRO099);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #endregion

        #region Linkage View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL095 objPRL095 = new PRL095();
            objPRL095 = db.PRL095.Where(i => i.HeaderId == id).FirstOrDefault();

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;
            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            List<string> lstYesNoNA = clsImplementationEnum.getYesNoNA().ToList();
            ViewBag.YesNoNAEnum = lstYesNoNA.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            if (objPRL095 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL095.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objQproject.ManufacturingCode;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }

            if (m == 1)
            {
                ViewBag.isEditable = "true";
            }
            else
            { ViewBag.isEditable = "false"; }

            return View(objPRL095);
        }


        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL095 PRL095)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRL095.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL095 objPRL095 = db.PRL095.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    #region Save Data
                    objPRL095.DCRNo = PRL095.DCRNo;
                    objPRL095.ProtocolNo = PRL095.ProtocolNo;
                    objPRL095.OutSideCircumferenceReq = PRL095.OutSideCircumferenceReq;
                    objPRL095.ActualTop = PRL095.ActualTop;
                    objPRL095.ActualBottom = PRL095.ActualBottom;
                    objPRL095.IDCircumferenceAtOpenEndReq = PRL095.IDCircumferenceAtOpenEndReq;
                    objPRL095.IDCircumferenceAtOpenEndAct = PRL095.IDCircumferenceAtOpenEndAct;
                    objPRL095.ReqArcLength1 = PRL095.ReqArcLength1;
                    objPRL095.ReqArcLength2 = PRL095.ReqArcLength2;
                    objPRL095.ReqArcLength3 = PRL095.ReqArcLength3;
                    objPRL095.ReqArcLength4 = PRL095.ReqArcLength4;
                    objPRL095.ArcLengthTop1 = PRL095.ArcLengthTop1;
                    objPRL095.ArcLengthTop2 = PRL095.ArcLengthTop2;
                    objPRL095.ArcLengthTop3 = PRL095.ArcLengthTop3;
                    objPRL095.ArcLengthTop4 = PRL095.ArcLengthTop4;
                    objPRL095.ArcLengthBottom1 = PRL095.ArcLengthBottom1;
                    objPRL095.ArcLengthBottom2 = PRL095.ArcLengthBottom2;
                    objPRL095.ArcLengthBottom3 = PRL095.ArcLengthBottom3;
                    objPRL095.ArcLengthBottom4 = PRL095.ArcLengthBottom4;
                    objPRL095.ReqId = PRL095.ReqId;
                    objPRL095.ActualAtTop1 = PRL095.ActualAtTop1;
                    objPRL095.ActualAtTop2 = PRL095.ActualAtTop2;
                    objPRL095.ActualAtTop3 = PRL095.ActualAtTop3;
                    objPRL095.ActualAtTop4 = PRL095.ActualAtTop4;
                    objPRL095.ActualAtTop5 = PRL095.ActualAtTop5;
                    objPRL095.ActualAtTop6 = PRL095.ActualAtTop6;
                    objPRL095.ActualAtTop7 = PRL095.ActualAtTop7;
                    objPRL095.ActualAtTop8 = PRL095.ActualAtTop8;
                    objPRL095.ActualAtBottom1 = PRL095.ActualAtBottom1;
                    objPRL095.ActualAtBottom2 = PRL095.ActualAtBottom2;
                    objPRL095.ActualAtBottom3 = PRL095.ActualAtBottom3;
                    objPRL095.ActualAtBottom4 = PRL095.ActualAtBottom4;
                    objPRL095.ActualAtBottom5 = PRL095.ActualAtBottom5;
                    objPRL095.ActualAtBottom6 = PRL095.ActualAtBottom6;
                    objPRL095.ActualAtBottom7 = PRL095.ActualAtBottom7;
                    objPRL095.ActualAtBottom8 = PRL095.ActualAtBottom8;
                    objPRL095.Remarks1 = PRL095.Remarks1;
                    objPRL095.Remarks2 = PRL095.Remarks2;
                    objPRL095.Remarks3 = PRL095.Remarks3;
                    objPRL095.Remarks4 = PRL095.Remarks4;
                    objPRL095.Remarks5 = PRL095.Remarks5;
                    objPRL095.Remarks6 = PRL095.Remarks6;
                    objPRL095.Remarks7 = PRL095.Remarks7;
                    objPRL095.Remarks8 = PRL095.Remarks8;
                    objPRL095.Note2 = PRL095.Note2;
                    objPRL095.Note31 = PRL095.Note31;
                    objPRL095.Note32 = PRL095.Note32.HasValue ? Manager.getDateTime(PRL095.Note32.Value.ToShortDateString()) : PRL095.Note32;

                    objPRL095.Note4 = PRL095.Note4;
                    objPRL095.Note5 = PRL095.Note5;
                    objPRL095.Note6 = PRL095.Note6;
                    objPRL095.Note7 = PRL095.Note7;
                    objPRL095.Note8 = PRL095.Note8;
                    objPRL095.Note9 = PRL095.Note9;
                    objPRL095.Note10 = PRL095.Note10;
                    objPRL095.Note11 = PRL095.Note11;
                    objPRL095.Note12 = PRL095.Note12;
                    objPRL095.Note13 = PRL095.Note13;
                    objPRL095.Note14 = PRL095.Note14;
                    objPRL095.Note15 = PRL095.Note15;
                    objPRL095.Note16 = PRL095.Note16;
                    objPRL095.Note17 = PRL095.Note17;
                    objPRL095.RequiredHoleDia = PRL095.RequiredHoleDia;
                    objPRL095.ActualHoleDiaMin = PRL095.ActualHoleDiaMin;
                    objPRL095.ActualHoleDiaMax = PRL095.ActualHoleDiaMax;
                    objPRL095.TotalNoOfHoles = PRL095.TotalNoOfHoles;
                    objPRL095.HoleNumbers = PRL095.HoleNumbers;
                    objPRL095.DistanceReq1 = PRL095.DistanceReq1;
                    objPRL095.DistanceReq2 = PRL095.DistanceReq2;
                    objPRL095.DistanceReq3 = PRL095.DistanceReq3;
                    objPRL095.DistanceReq4 = PRL095.DistanceReq4;
                    objPRL095.DistanceAct1 = PRL095.DistanceAct1;
                    objPRL095.DistanceAct2 = PRL095.DistanceAct2;
                    objPRL095.DistanceAct3 = PRL095.DistanceAct3;
                    objPRL095.DistanceAct4 = PRL095.DistanceAct4;
                    objPRL095.ConentricityReq = PRL095.ConentricityReq;
                    objPRL095.ConentricityAct = PRL095.ConentricityAct;
                    objPRL095.FlatnessReadingOfBaseRingTol = PRL095.FlatnessReadingOfBaseRingTol;

                    objPRL095.EditedBy = objClsLoginInfo.UserName;
                    objPRL095.EditedOn = DateTime.Now;

                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol saved successfully";
                    objResponseMsg.HeaderId = objPRL095.HeaderId;
                    #endregion
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                }


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL095 objPRL095 = db.PRL095.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL095 != null && string.IsNullOrWhiteSpace(objPRL095.ProtocolNo))
                    {

                        db.PRL095.Remove(objPRL095);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL095.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line1 Linkage
        [HttpPost]
        public ActionResult GetProtocolLines1DataLinkage(JQueryDataTableParamModel param, string headerId)
        {
            try
            {

                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (prl.Description like '%" + param.sSearch
                        + "%' or prl.ElevationReq like '%" + param.sSearch
                        + "%' or prl.ElevationAct like '%" + param.sSearch
                        + "%' or prl.ProjectionReq like '%" + param.sSearch
                        + "%' or prl.ProjectionAct like '%" + param.sSearch
                        + "%' or prl.OrientationReq like '%" + param.sSearch
                        + "%' or prl.OrientationReqArcLen like '%" + param.sSearch
                        + "%' or prl.OrientationAct like '%" + param.sSearch
                        + "%' or prl.OrientationActArcLen like '%" + param.sSearch
                        + "%' or Remark like '%" + param.sSearch
                        + "%')";
                }

                var lstResult = db.SP_IPI_PROTOCOL020_LINKAGE_GET_LINES1_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] {"1",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Description", "", "", false, "", false, "25","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "ElevationReq", "", "", false, "", false, "25","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "ElevationAct", "", "", false, "", false, "25","PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "ProjectionReq", "", "", false, "", false, "25","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "ProjectionAct", "", "", false, "", false, "25","PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "OrientationReq", "", "", false, "", false, "25","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "OrientationReqArcLen", "", "", false, "", false, "25","PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "OrientationAct", "", "", false, "", false, "25","PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "OrientationActArcLen", "", "", false, "", false, "25","PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Remark", "", "", false, "", false, "25","PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine1(0);")
                                };
                var data = (from uc in lstResult
                            select new[]
                            {"1",
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                               Helper.GenerateHTMLTextbox(uc.LineId, "Description",Convert.ToString(uc.Description),"", true, "", false, "25","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "ElevationReq",Convert.ToString(uc.ElevationReq),"", true, "", false, "25","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "ElevationAct",Convert.ToString(uc.ElevationAct),"", true, "", false, "25","PROD"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "ProjectionReq",Convert.ToString(uc.ProjectionReq),"", true, "", false, "25","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "ProjectionAct",Convert.ToString(uc.ProjectionAct),"", true, "", false, "25","PROD"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "OrientationReq",Convert.ToString(uc.OrientationReq),"", true, "", false, "25","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "OrientationReqArcLen",Convert.ToString(uc.OrientationReqArcLen),"", true, "", false, "25","PROD"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "OrientationAct",Convert.ToString(uc.OrientationAct),"", true, "", false, "25","PROD"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "OrientationActArcLen",Convert.ToString(uc.OrientationActArcLen),"", true, "", false, "25","PROD"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "Remark",Convert.ToString(uc.Remark),"", true, "", false, "25","PROD"),
                               Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == "QC3" ? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine1Linkage','tblProtocolLines1')"):"")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine1Linkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRL096 objPRL096 = new PRL096();
            // int newRowIndex = 0;
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
                if (refHeaderId > 0)
                {
                    string strDescription = !string.IsNullOrEmpty(fc["Description" + refLineId]) ? Convert.ToString(fc["Description" + refLineId]).Trim() : "";
                    string strElevationReq = !string.IsNullOrEmpty(fc["ElevationReq" + refLineId]) ? Convert.ToString(fc["ElevationReq" + refLineId]).Trim() : "";
                    string strElevationAct = !string.IsNullOrEmpty(fc["ElevationAct" + refLineId]) ? Convert.ToString(fc["ElevationAct" + refLineId]).Trim() : "";
                    string strProjectionReq = !string.IsNullOrEmpty(fc["ProjectionReq" + refLineId]) ? Convert.ToString(fc["ProjectionReq" + refLineId]).Trim() : "";
                    string strProjectionAct = !string.IsNullOrEmpty(fc["ProjectionAct" + refLineId]) ? Convert.ToString(fc["ProjectionAct" + refLineId]).Trim() : "";
                    string strOrientationReq = !string.IsNullOrEmpty(fc["OrientationReq" + refLineId]) ? Convert.ToString(fc["OrientationReq" + refLineId]).Trim() : "";
                    string strOrientationReqArcLen = !string.IsNullOrEmpty(fc["OrientationReqArcLen" + refLineId]) ? Convert.ToString(fc["OrientationReqArcLen" + refLineId]).Trim() : "";
                    string strOrientationAct = !string.IsNullOrEmpty(fc["OrientationAct" + refLineId]) ? Convert.ToString(fc["OrientationAct" + refLineId]).Trim() : "";
                    string strOrientationActArcLen = !string.IsNullOrEmpty(fc["OrientationActArcLen" + refLineId]) ? Convert.ToString(fc["OrientationActArcLen" + refLineId]).Trim() : "";
                    string strRemark = !string.IsNullOrEmpty(fc["Remark" + refLineId]) ? Convert.ToString(fc["Remark" + refLineId]).Trim() : "";

                    #region Add New Spot Line1
                    if (refLineId > 0)
                    {
                        objPRL096 = db.PRL096.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRL096.EditedBy = objClsLoginInfo.UserName;
                        objPRL096.EditedOn = DateTime.Now;
                    }
                    objPRL096.HeaderId = refHeaderId;
                    objPRL096.Description = strDescription;
                    objPRL096.ElevationReq = strElevationReq;
                    objPRL096.ElevationAct = strElevationAct;
                    objPRL096.ProjectionReq = strProjectionReq;
                    objPRL096.ProjectionAct = strProjectionAct;
                    objPRL096.OrientationReq = strOrientationReq;
                    objPRL096.OrientationReqArcLen = strOrientationReqArcLen;
                    objPRL096.OrientationAct = strOrientationAct;
                    objPRL096.OrientationActArcLen = strOrientationActArcLen;
                    objPRL096.Remark = strRemark;


                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = "Data updated successfully";
                    }
                    else
                    {
                        objPRL096.CreatedBy = objClsLoginInfo.UserName;
                        objPRL096.CreatedOn = DateTime.Now;
                        db.PRL096.Add(objPRL096);
                        db.SaveChanges();
                        objResponseMsg.Value = "Data added successfully";
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRL096.LineId;
                    #endregion

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine1Linkage(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRL096 objPRL096 = db.PRL096.FirstOrDefault(c => c.LineId == lineId);
                if (objPRL096 != null)
                {
                    db.PRL096.Remove(objPRL096);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 Linkage
        [HttpPost]
        public ActionResult GetProtocolLines2DataLinkage(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (prl.HoleNo like '%" + param.sSearch
                        + "%' or prl.BcdReq like '%" + param.sSearch
                        + "%' or prl.BcdAct like '%" + param.sSearch
                        + "%')";
                }

                var lstResult = db.SP_IPI_PROTOCOL020_LINKAGE_GET_LINES2_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] {"2",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "HoleNo", "", "", false, "", false, "25","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "BcdReq", "", "", false, "", false, "25","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "BcdAct", "", "", false, "", false, "25","PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine2(0);")
                                };
                var data = (from uc in lstResult
                            select new[]
                            {"2",
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                               Helper.GenerateHTMLTextbox(uc.LineId, "HoleNo",Convert.ToString(uc.HoleNo), "", true, "", false, "25","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "BcdReq",Convert.ToString(uc.BcdReq), "", true, "", false, "25","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "BcdAct",Convert.ToString(uc.BcdAct), "", true, "", false, "25","PROD"),
                               Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == "QC3" ? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine2Linkage','tblProtocolLines2')"):"")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine2Linkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRL097 objPRL097 = new PRL097();
            //int newRowIndex = 0;
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
                if (refHeaderId > 0)
                {
                    string strHoleNo = !string.IsNullOrEmpty(fc["HoleNo" + refLineId]) ? Convert.ToString(fc["HoleNo" + refLineId]).Trim() : "";
                    string strBcdReq = !string.IsNullOrEmpty(fc["BcdReq" + refLineId]) ? Convert.ToString(fc["BcdReq" + refLineId]).Trim() : "";
                    string strBcdAct = !string.IsNullOrEmpty(fc["BcdAct" + refLineId]) ? Convert.ToString(fc["BcdAct" + refLineId]).Trim() : "";

                    #region Add New Spot Line2
                    if (refLineId > 0)
                    {
                        objPRL097 = db.PRL097.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRL097.EditedBy = objClsLoginInfo.UserName;
                        objPRL097.EditedOn = DateTime.Now;
                    }
                    objPRL097.HeaderId = refHeaderId;
                    objPRL097.HoleNo = strHoleNo;
                    objPRL097.BcdReq = strBcdReq;
                    objPRL097.BcdAct = strBcdAct;
                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = "Data updated successfully";
                    }
                    else
                    {
                        objPRL097.CreatedBy = objClsLoginInfo.UserName;
                        objPRL097.CreatedOn = DateTime.Now;
                        db.PRL097.Add(objPRL097);
                        db.SaveChanges();
                        objResponseMsg.Value = "Data added successfully";
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRL097.LineId;
                    #endregion

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine2Linkage(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRL097 objPRL097 = db.PRL097.FirstOrDefault(c => c.LineId == lineId);
                if (objPRL097 != null)
                {
                    db.PRL097.Remove(objPRL097);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line3 Linkage
        [HttpPost]
        public ActionResult GetProtocolLines3DataLinkage(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (prl.Location like '%" + param.sSearch
                        + "%' or prl.At0 like '%" + param.sSearch
                        + "%' or prl.At90 like '%" + param.sSearch
                        + "%' or prl.At180 like '%" + param.sSearch
                        + "%' or prl.At270 like '%" + param.sSearch
                        + "%' or prl.Remark like '%" + param.sSearch
                        + "%')";
                }

                var lstResult = db.SP_IPI_PROTOCOL020_LINKAGE_GET_LINES3_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                var lstOutBy = db.SP_IPI_PROTOCOL020_LINKAGE_GET_OUTBY(intHeaderId).FirstOrDefault();
                var outBy0 = lstOutBy.OutBy0;
                var outBy90 = lstOutBy.OutBy90;
                var outBy180 = lstOutBy.OutBy180;
                var outBy270 = lstOutBy.OutBy270;

                int newRecordId = 0;
                var newRecord = new[] {"3",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Location", "", "", false, "", false, "25","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "At0", "", "", false, "", false, "25","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "At90", "", "", false, "", false, "25","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "At180", "", "", false, "", false, "25","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "At270", "", "", false, "", false, "25","PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Remark", "", "", false, "", false, "25","PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine3(0);")
                                };
                var data = (from uc in lstResult
                            select new[]
                            {"3",
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                               Helper.GenerateHTMLTextbox(uc.LineId, "Location",Convert.ToString(uc.Location), "", true, "", false, "25","PROD"),
                               Helper.GenerateNumericTextbox(uc.LineId, "At0",Convert.ToString(uc.At0), "", true, "", false, "25","PROD"),
                               Helper.GenerateNumericTextbox(uc.LineId, "At90",Convert.ToString(uc.At90), "", true, "", false, "25","PROD"),
                               Helper.GenerateNumericTextbox(uc.LineId, "At180",Convert.ToString(uc.At180), "", true, "", false, "25","PROD"),
                               Helper.GenerateNumericTextbox(uc.LineId, "At270",Convert.ToString(uc.At270), "", true, "", false, "25","PROD"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "Remark",Convert.ToString(uc.Remark), "", true, "", false, "25","PROD"),
                               Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == "QC3" ? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine3Linkage','tblProtocolLines3')"):"")
                           }).ToList();

                var outBy = new[] {
                                   "3",
                                   Convert.ToString(headerId),
                                   "",
                                   "<strong>OUT BY</strong>",
                                   Helper.GenerateNumericTextbox(0, "OutBy",  outBy0.ToString(), "", true, "", false),
                                   Helper.GenerateNumericTextbox(90, "OutBy", outBy90.ToString(), "", true, "", false),
                                   Helper.GenerateNumericTextbox(180, "OutBy",outBy180.ToString(), "", true, "",false),
                                   Helper.GenerateNumericTextbox(270, "OutBy",outBy270.ToString(), "", true, "",false),
                                   "",
                                   ""
                                    };

                data.Insert(0, newRecord);
                data.Insert(data.Count, outBy);



                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition,
                    outby = true,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult SaveProtocolLine3Linkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRL098 objPRL098 = new PRL098();
            // int newRowIndex = 0;
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
                if (refHeaderId > 0)
                {
                    string strLocation = !string.IsNullOrEmpty(fc["Location" + refLineId]) ? Convert.ToString(fc["Location" + refLineId]).Trim() : "";
                    decimal? strAt0 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["At0" + refLineId]));
                    decimal? strAt90 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["At90" + refLineId]));
                    decimal? strAt180 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["At180" + refLineId]));
                    decimal? strAt270 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["At270" + refLineId]));
                    string strRemark = !string.IsNullOrEmpty(fc["Remark" + refLineId]) ? Convert.ToString(fc["Remark" + refLineId]).Trim() : "";

                    #region Add New Spot Line3
                    if (refLineId > 0)
                    {
                        objPRL098 = db.PRL098.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRL098.EditedBy = objClsLoginInfo.UserName;
                        objPRL098.EditedOn = DateTime.Now;
                    }
                    objPRL098.HeaderId = refHeaderId;
                    objPRL098.Location = strLocation;
                    objPRL098.At0 = strAt0;
                    objPRL098.At90 = strAt90;
                    objPRL098.At180 = strAt180;
                    objPRL098.At270 = strAt270;
                    objPRL098.Remark = strRemark;

                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = "Data updated successfully";
                    }
                    else
                    {
                        objPRL098.CreatedBy = objClsLoginInfo.UserName;
                        objPRL098.CreatedOn = DateTime.Now;
                        db.PRL098.Add(objPRL098);
                        db.SaveChanges();
                        objResponseMsg.Value = "Data added successfully";
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRL098.LineId;
                    #endregion

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolLine3Linkage(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRL098 objPRL098 = db.PRL098.FirstOrDefault(c => c.LineId == lineId);
                if (objPRL098 != null)
                {
                    db.PRL098.Remove(objPRL098);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line4 Linkage
        [HttpPost]
        public ActionResult GetProtocolLines4DataLinkage(JQueryDataTableParamModel param, string headerId)
        {
            try
            {

                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (prl.Description like '%" + param.sSearch
                       + "%' or prl.BrlToTrl like '%" + param.sSearch
                       + "%' or prl.TrlToOpenEnd like '%" + param.sSearch
                       + "%' or prl.YIn like '%" + param.sSearch
                       + "%' or prl.YMid like '%" + param.sSearch
                       + "%' or prl.YOut like '%" + param.sSearch
                       + "%' or prl.H like '%" + param.sSearch
                       + "%' or prl.X like '%" + param.sSearch
                       + "%')";
                }

                var lstResult = db.SP_IPI_PROTOCOL020_LINKAGE_GET_LINES4_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] {"4",

                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                      "",
                                   Helper.GenerateNumericTextbox(newRecordId, "BrlToTrl", "", "", false, "", false, "25","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "TrlToOpenEnd", "", "", false, "", false, "25","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "YIn", "", "", false, "", false, "25","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "YMid", "", "", false, "", false, "25","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "YOut", "", "", false, "", false, "25","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "H", "", "", false, "", false, "25","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "X", "", "", false, "", false, "25","PROD"),
                                   "",
                                    "",
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine4(0);")
                                };
                var data = (from uc in lstResult
                            select new[]
                            {"4",
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.LineId),
                                Convert.ToString(uc.ROW_NO),
                                Helper.GenerateNumericTextbox(uc.LineId, "BrlToTrl",Convert.ToString(uc.BrlToTrl), "", true, "", false, "25","PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId, "TrlToOpenEnd",Convert.ToString(uc.TrlToOpenEnd), "", true, "", false, "25","PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId, "YIn",Convert.ToString(uc.YIn), "", true, "", false, "25","PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId, "YMid",Convert.ToString(uc.YMid), "", true, "", false, "25","PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId, "YOut",Convert.ToString(uc.YOut), "", true, "", false, "25","PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId, "H",Convert.ToString(uc.H), "", true, "", false, "25","PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId, "X",Convert.ToString(uc.X), "", true, "", false, "25","PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId, "YMin", Convert.ToString(uc.YMin), "", true, "", true, "25","PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId, "H+(uc.X-uc.YMin)", Convert.ToString(uc.H+(uc.X-uc.YMin)), "", true, "", true, "25","PROD"),
                                Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == "PROD3" ? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine4Linkage','tblProtocolLines4')"):"")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine4Linkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRL099 objPRL099 = new PRL099();
            // int newRowIndex = 0;
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
                if (refHeaderId > 0)
                {
                    decimal? strBrlToTrl = (new clsManager()).GetDecimalValue(Convert.ToString(fc["BrlToTrl" + refLineId]));
                    decimal? strTrlToOpenEnd = (new clsManager()).GetDecimalValue(Convert.ToString(fc["TrlToOpenEnd" + refLineId]));
                    decimal? strYIn = (new clsManager()).GetDecimalValue(Convert.ToString(fc["YIn" + refLineId]));
                    decimal? strYMid = (new clsManager()).GetDecimalValue(Convert.ToString(fc["YMid" + refLineId]));
                    decimal? strYOut = (new clsManager()).GetDecimalValue(Convert.ToString(fc["YOut" + refLineId]));
                    decimal? strH = (new clsManager()).GetDecimalValue(Convert.ToString(fc["H" + refLineId]));
                    decimal? strX = (new clsManager()).GetDecimalValue(Convert.ToString(fc["X" + refLineId]));
                    #region Add New Spot Line4
                    if (refLineId > 0)
                    {
                        objPRL099 = db.PRL099.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRL099.EditedBy = objClsLoginInfo.UserName;
                        objPRL099.EditedOn = DateTime.Now;
                    }
                    objPRL099.HeaderId = refHeaderId;
                    objPRL099.BrlToTrl = strBrlToTrl;
                    objPRL099.TrlToOpenEnd = strTrlToOpenEnd;
                    objPRL099.YIn = strYIn;
                    objPRL099.YMid = strYMid;
                    objPRL099.YOut = strYOut;
                    objPRL099.H = strH;
                    objPRL099.X = strX;

                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = "Data updated successfully";
                    }
                    else
                    {
                        objPRL099.CreatedBy = objClsLoginInfo.UserName;
                        objPRL099.CreatedOn = DateTime.Now;
                        db.PRL099.Add(objPRL099);
                        db.SaveChanges();
                        objResponseMsg.Value = "Data added successfully";
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRL099.LineId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolLine4Linkage(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRL099 objPRL099 = db.PRL099.FirstOrDefault(c => c.LineId == lineId);
                if (objPRL099 != null)
                {
                    db.PRL099.Remove(objPRL099);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #endregion
        public string HTMLActionString(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", string onHrefURL = "", bool isDisable = false)
        {
            string htmlControl = "";
            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? onClickMethod : "";
            string onHref = !string.IsNullOrEmpty(onHrefURL) ? "href='" + onHrefURL + "'" : "";

            if (isDisable)
            {
                htmlControl += "<i id=\"" + inputID.ToString() + "\" name=\"" + inputID + "\" ></i>";
                //htmlControl = "<i id=\"" + inputID + "' name='" + inputID + "' style='cursor: pointer; opacity:0.3; margin-right:10px;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";
            }
            else
            {
                htmlControl += "<i id=\"" + inputID.ToString() + "\" name=\"" + inputID + "\"  style='cursor: pointer; margin-right:10px;' Title=\"" + buttonTooltip + "\" class=\"" + className + "\"  onclick=\"" + onClickEvent + "\" ></i>";
                //htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer; margin-right:10px;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
            }

            if (!string.IsNullOrEmpty(onHref))
            {
                htmlControl = "<a " + onHref + " >" + htmlControl + "</a>";
            }

            return htmlControl;
        }
    }
}

