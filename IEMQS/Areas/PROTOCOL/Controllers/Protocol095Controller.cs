﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol095Controller : clsBase
    {
        // GET: PROTOCOL/Protocol095
        string ControllerURL = "/PROTOCOL/Protocol095/";
        string Title = "Final Assembly Inspection of Vapour Liquid Distribution Tray";

        #region Details View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO470 objPRO470 = new PRO470();
            if (!id.HasValue)
            {
                try
                {
                    objPRO470.ProtocolNo = string.Empty;
                    objPRO470.CreatedBy = objClsLoginInfo.UserName;
                    objPRO470.CreatedOn = DateTime.Now;

                    #region PRO472
                    objPRO470.PRO472.Add(new PRO472
                    {
                        SrNo = "1",
                        Description = "TRAY OD",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO470.PRO472.Add(new PRO472
                    {
                        SrNo = "2",
                        Description = "HEIGHT OF VLDT",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO470.PRO472.Add(new PRO472
                    {
                        SrNo = "3",
                        Description = "SUPPORT BEAM TO BEAM DISTANCE",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO470.PRO472.Add(new PRO472
                    {
                        SrNo = "4",
                        Description = "GAP BETWEEN TRAY PANELS TO PANELS AT SUPPORT BEAM ",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO470.PRO472.Add(new PRO472
                    {
                        SrNo = "5",
                        Description = "GAP BETWEEN TRAY PANELS TO TRAY PANELS AT BACKING BAR",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO470.PRO472.Add(new PRO472
                    {
                        SrNo = "6",
                        Description = "LEVELNESS OF TRAY",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO470.PRO472.Add(new PRO472
                    {
                        SrNo = "7",
                        Description = "LIFT CHANNEL WIDTH & HEIGHT",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO470.PRO472.Add(new PRO472
                    {
                        SrNo = "8",
                        Description = "SUPPORT ROD HOLES (QTY)",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO470.PRO472.Add(new PRO472
                    {
                        SrNo = "9",
                        Description = "SUPPORT ROD HOLES (DIAMETER & PCD)",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO470.PRO472.Add(new PRO472
                    {
                        SrNo = "10",
                        Description = "SUPPORT ROD HOLES (ORIENTATION)",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO470.PRO472.Add(new PRO472
                    {
                        SrNo = "11",
                        Description = "UNIFLOW DISTRIBUTION IN TRAY PANELS (PITCH)",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO470.PRO472.Add(new PRO472
                    {
                        SrNo = "12",
                        Description = "UNIFLOW DISTRIBUTION IN TRAY PANELS (QTY)",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO470.PRO472.Add(new PRO472
                    {
                        SrNo = "13",
                        Description = "UNIFLOW DISTRIBUTION IN TRAY PANELS (ORIENTATION)",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO470.PRO472.Add(new PRO472
                    {
                        SrNo = "14",
                        Description = "UNIFLOW DISTRIBUTION IN TRAY PANELS (VAPOR INLET)",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO470.PRO472.Add(new PRO472
                    {
                        SrNo = "15",
                        Description = "UNIFLOW DISTRIBUTION IN TRAY PANELS (TOTAL HEIGHT FROM TRAY)",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO470.PRO472.Add(new PRO472
                    {
                        SrNo = "16",
                        Description = "UNIFLOW DISTRIBUTION IN TRAY PANELS (SPRAY DEVICE TO ORIFICE PLATE SPACING)",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO470.PRO472.Add(new PRO472
                    {
                        SrNo = "17",
                        Description = "UNIFLOW DISTRIBUTION IN TRAY PANELS (LOCATION)",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO470.PRO472.Add(new PRO472
                    {
                        SrNo = "18",
                        Description = "BANDING PLATE SLOT",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO470.PRO472.Add(new PRO472
                    {
                        SrNo = "19",
                        Description = "LIFTING LUG  (SIZE & DIA)",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO470.PRO472.Add(new PRO472
                    {
                        SrNo = "20",
                        Description = "LIFTING LUG (QTY)",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO470.PRO472.Add(new PRO472
                    {
                        SrNo = "21",
                        Description = "VERIFY CLEARANCE BETWEEN OD OF DOWNCOMER AND OUTER EDGE OF BANDING PLATE",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO470.PRO472.Add(new PRO472
                    {
                        SrNo = "22",
                        Description = "J-CLIPS (DIMENSION & QTY)",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO470.PRO472.Add(new PRO472
                    {
                        SrNo = "23",
                        Description = "J-CLIPS (LOCATION)",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO470.PRO472.Add(new PRO472
                    {
                        SrNo = "24",
                        Description = "OTHER DIMENSIONS",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO470.PRO472.Add(new PRO472
                    {
                        SrNo = "25",
                        Description = "IDENTIFICATION & MATCH MARKING",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO470.PRO472.Add(new PRO472
                    {
                        SrNo = "26",
                        Description = "WELD & OVERALL VISUAL",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion

                    db.PRO470.Add(objPRO470);
                    db.SaveChanges();
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO470.HeaderId;
            }
            else
            {
                objPRO470 = db.PRO470.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO471> lstPRO471 = db.PRO471.Where(x => x.HeaderId == objPRO470.HeaderId).ToList();
            List<PRO472> lstPRO472 = db.PRO472.Where(x => x.HeaderId == objPRO470.HeaderId).ToList();
            List<PRO473> lstPRO473 = db.PRO473.Where(x => x.HeaderId == objPRO470.HeaderId).ToList();

            ViewBag._lstPRO471 = lstPRO471;
            ViewBag._lstPRO472 = lstPRO472;
            ViewBag._lstPRO473 = lstPRO473;

            #endregion

            return View(objPRO470);
            //return View();
        }

        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO470 PRO470)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO470.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO470 objPRO470 = db.PRO470.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO470.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO470.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data

                        objPRO470.ProtocolNo = PRO470.ProtocolNo;

                        objPRO470.CheckPoint1 = PRO470.CheckPoint1;
                        objPRO470.CheckPoint2 = PRO470.CheckPoint2;
                        objPRO470.CheckPoint4_1_1 = PRO470.CheckPoint4_1_1;
                        objPRO470.CheckPoint4_1_2 = PRO470.CheckPoint4_1_2;
                        objPRO470.CheckPoint4_2_1 = PRO470.CheckPoint4_2_1;
                        objPRO470.CheckPoint4_2_2 = PRO470.CheckPoint4_2_2;
                        objPRO470.CheckPoint4_3_1 = PRO470.CheckPoint4_3_1;
                        objPRO470.CheckPoint4_3_2 = PRO470.CheckPoint4_3_2;
                        objPRO470.CheckPoint4_4_1 = PRO470.CheckPoint4_4_1;
                        objPRO470.CheckPoint4_4_2 = PRO470.CheckPoint4_4_2;
                        objPRO470.CheckPoint4_5_1 = PRO470.CheckPoint4_5_1;
                        objPRO470.CheckPoint4_5_2 = PRO470.CheckPoint4_5_2;

                        objPRO470.QCRemarks = PRO470.QCRemarks;
                        objPRO470.Result = PRO470.Result;

                        objPRO470.EditedBy = objClsLoginInfo.UserName;
                        objPRO470.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO470.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO470 objPRO470 = db.PRO470.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO470 != null && string.IsNullOrWhiteSpace(objPRO470.ProtocolNo))
                    {
                        db.PRO470.Remove(objPRO470);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO470.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO471> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO471> lstAddPRO471 = new List<PRO471>();
                List<PRO471> lstDeletePRO471 = new List<PRO471>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO471 obj = db.PRO471.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.DrgNo = item.DrgNo;
                                obj.RevNo = item.RevNo;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO471 obj = new PRO471();

                            if (!string.IsNullOrWhiteSpace(item.DrgNo))
                            {
                                obj.HeaderId = item.HeaderId;
                                obj.DrgNo = item.DrgNo;
                                obj.RevNo = item.RevNo;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO471.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO471.Count > 0)
                    {
                        db.PRO471.AddRange(lstAddPRO471);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO471 = db.PRO471.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO471.Count > 0)
                    {
                        db.PRO471.RemoveRange(lstDeletePRO471);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO471 = db.PRO471.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO471.Count > 0)
                    {
                        db.PRO471.RemoveRange(lstDeletePRO471);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2(List<PRO472> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO472> lstAddPRO472 = new List<PRO472>();
                List<PRO472> lstDeletePRO472 = new List<PRO472>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO472 obj = db.PRO472.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO472();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SrNo = item.SrNo;
                        obj.Description = item.Description;
                        obj.ReqDimension = item.ReqDimension;
                        obj.ActDimension = item.ActDimension;
                        obj.Remarks = item.Remarks;

                        if (isAdded)
                        {
                            lstAddPRO472.Add(obj);
                        }
                    }
                    if (lstAddPRO472.Count > 0)
                    {
                        db.PRO472.AddRange(lstAddPRO472);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO472 = db.PRO472.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO472.Count > 0)
                    {
                        db.PRO472.RemoveRange(lstDeletePRO472);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO472 = db.PRO472.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO472.Count > 0)
                    {
                        db.PRO472.RemoveRange(lstDeletePRO472);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3(List<PRO473> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO473> lstAddPRO473 = new List<PRO473>();
                List<PRO473> lstDeletePRO473 = new List<PRO473>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO473 obj = db.PRO473.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO473();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.MaterialIdentification = item.MaterialIdentification;

                        if (isAdded)
                        {
                            lstAddPRO473.Add(obj);
                        }
                    }
                    if (lstAddPRO473.Count > 0)
                    {
                        db.PRO473.AddRange(lstAddPRO473);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO473 = db.PRO473.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO473.Count > 0)
                    {
                        db.PRO473.RemoveRange(lstDeletePRO473);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO473 = db.PRO473.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO473.Count > 0)
                    {
                        db.PRO473.RemoveRange(lstDeletePRO473);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

        #region Linkage View Code

        #region Header
        public ActionResult Linkage(int? id, int? m)
        {
            PRL470 objPRL470 = new PRL470();
            objPRL470 = db.PRL470.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL470 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL470.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL471> lstPRL471 = db.PRL471.Where(x => x.HeaderId == objPRL470.HeaderId).ToList();
            List<PRL472> lstPRL472 = db.PRL472.Where(x => x.HeaderId == objPRL470.HeaderId).ToList();
            List<PRL473> lstPRL473 = db.PRL473.Where(x => x.HeaderId == objPRL470.HeaderId).ToList();

            ViewBag._lstPRL471 = lstPRL471;
            ViewBag._lstPRL472 = lstPRL472;
            ViewBag._lstPRL473 = lstPRL473;

            #endregion

            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.DrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL470.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL470.ActFilledBy) && (objPRL470.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL470.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL470);

            //return View();
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL470 prl470, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = prl470.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL470 objPRL470 = db.PRL470.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL470.DrawingRevisionNo = prl470.DrawingRevisionNo;
                    objPRL470.DrawingNo = prl470.DrawingNo;
                    objPRL470.DCRNo = prl470.DCRNo;
                    objPRL470.ProtocolNo = prl470.ProtocolNo;

                    objPRL470.CheckPoint1 = prl470.CheckPoint1;
                    objPRL470.CheckPoint2 = prl470.CheckPoint2;
                    objPRL470.CheckPoint4_1_1 = prl470.CheckPoint4_1_1;
                    objPRL470.CheckPoint4_1_2 = prl470.CheckPoint4_1_2;
                    objPRL470.CheckPoint4_2_1 = prl470.CheckPoint4_2_1;
                    objPRL470.CheckPoint4_2_2 = prl470.CheckPoint4_2_2;
                    objPRL470.CheckPoint4_3_1 = prl470.CheckPoint4_3_1;
                    objPRL470.CheckPoint4_3_2 = prl470.CheckPoint4_3_2;
                    objPRL470.CheckPoint4_4_1 = prl470.CheckPoint4_4_1;
                    objPRL470.CheckPoint4_4_2 = prl470.CheckPoint4_4_2;
                    objPRL470.CheckPoint4_5_1 = prl470.CheckPoint4_5_1;
                    objPRL470.CheckPoint4_5_2 = prl470.CheckPoint4_5_2;

                    objPRL470.QCRemarks = prl470.QCRemarks;
                    objPRL470.Result = prl470.Result;

                    objPRL470.EditedBy = UserName;
                    objPRL470.EditedOn = DateTime.Now;
                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL470.ActFilledBy = UserName;
                            objPRL470.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL470.ReqFilledBy = UserName;
                            objPRL470.ReqFilledOn = DateTime.Now;
                        }
                    }
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL470.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL470 objPRL470 = db.PRL470.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL470 != null && string.IsNullOrWhiteSpace(objPRL470.ProtocolNo))
                    {
                        db.PRL470.Remove(objPRL470);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL470.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL471> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRL471> lstAddPRL471 = new List<PRL471>();
                List<PRL471> lstDeletePRL471 = new List<PRL471>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRL471 obj = db.PRL471.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.DrgNo = item.DrgNo;
                                obj.RevNo = item.RevNo;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRL471 obj = new PRL471();

                            if (!string.IsNullOrWhiteSpace(item.DrgNo))
                            {
                                obj.HeaderId = item.HeaderId;
                                obj.DrgNo = item.DrgNo;
                                obj.RevNo = item.RevNo;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRL471.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRL471.Count > 0)
                    {
                        db.PRL471.AddRange(lstAddPRL471);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRL471 = db.PRL471.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRL471.Count > 0)
                    {
                        db.PRL471.RemoveRange(lstDeletePRL471);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL471 = db.PRL471.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL471.Count > 0)
                    {
                        db.PRL471.RemoveRange(lstDeletePRL471);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2Linkage(List<PRL472> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRL472> lstAddPRL472 = new List<PRL472>();
                List<PRL472> lstDeletePRL472 = new List<PRL472>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL472 obj = db.PRL472.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL472();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SrNo = item.SrNo;
                        obj.Description = item.Description;
                        obj.ReqDimension = item.ReqDimension;
                        obj.ActDimension = item.ActDimension;
                        obj.Remarks = item.Remarks;

                        if (isAdded)
                        {
                            lstAddPRL472.Add(obj);
                        }
                    }
                    if (lstAddPRL472.Count > 0)
                    {
                        db.PRL472.AddRange(lstAddPRL472);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL472 = db.PRL472.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL472.Count > 0)
                    {
                        db.PRL472.RemoveRange(lstDeletePRL472);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL472 = db.PRL472.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL472.Count > 0)
                    {
                        db.PRL472.RemoveRange(lstDeletePRL472);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3Linkage(List<PRL473> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRL473> lstAddPRL473 = new List<PRL473>();
                List<PRL473> lstDeletePRL473 = new List<PRL473>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL473 obj = db.PRL473.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL473();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.MaterialIdentification = item.MaterialIdentification;

                        if (isAdded)
                        {
                            lstAddPRL473.Add(obj);
                        }
                    }
                    if (lstAddPRL473.Count > 0)
                    {
                        db.PRL473.AddRange(lstAddPRL473);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL473 = db.PRL473.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL473.Count > 0)
                    {
                        db.PRL473.RemoveRange(lstDeletePRL473);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL473 = db.PRL473.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL473.Count > 0)
                    {
                        db.PRL473.RemoveRange(lstDeletePRL473);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion
    }
}