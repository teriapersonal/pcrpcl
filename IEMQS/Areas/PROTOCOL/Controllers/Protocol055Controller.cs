﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol055Controller : clsBase
    {
        // GET: PROTOCOL/Protocol055

        string ControllerURL = "/PROTOCOL/Protocol055/";
        string Title = "REPORT FOR VISUAL AND DIMENSION INSPECTION OF TSR";

        #region Details View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO270 objPRO270 = new PRO270();
            if (!id.HasValue)
            {
                try
                {
                    objPRO270.ProtocolNo = string.Empty;
                    objPRO270.CreatedBy = objClsLoginInfo.UserName;
                    objPRO270.CreatedOn = DateTime.Now;



                    #region OVALITY 
                    objPRO270.PRO271.Add(new PRO271
                    {
                        Orientation = "0°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO270.PRO271.Add(new PRO271
                    {
                        Orientation = "30°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO270.PRO271.Add(new PRO271
                    {
                        Orientation = "60°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO270.PRO271.Add(new PRO271
                    {
                        Orientation = "90°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO270.PRO271.Add(new PRO271
                    {
                        Orientation = "120°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO270.PRO271.Add(new PRO271
                    {
                        Orientation = "150°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO270.PRO271.Add(new PRO271
                    {
                        Orientation = "180°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO270.PRO271.Add(new PRO271
                    {
                        Orientation = "210°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO270.PRO271.Add(new PRO271
                    {
                        Orientation = "240°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO270.PRO271.Add(new PRO271
                    {
                        Orientation = "270°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO270.PRO271.Add(new PRO271
                    {
                        Orientation = "300°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO270.PRO271.Add(new PRO271
                    {
                        Orientation = "330°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });

                    #endregion

                    db.PRO270.Add(objPRO270);
                    db.SaveChanges();
                    // return View();

                }

                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO270.HeaderId;
            }
            else
            {
                objPRO270 = db.PRO270.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstElevationOfTSRFrom = new List<string> { "REF. LINE", "TAN LINE" };

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.ElevationOfTSRFrom = lstElevationOfTSRFrom.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();


            //  ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO271> lstPRO271 = db.PRO271.Where(x => x.HeaderId == objPRO270.HeaderId).ToList();
            //List<PRO267> lstPRO267 = db.PRO267.Where(x => x.HeaderId == objPRO270.HeaderId).ToList();
            //List<PRO268> lstPRO268 = db.PRO268.Where(x => x.HeaderId == objPRO270.HeaderId).ToList();




            ViewBag.lstPRO271 = lstPRO271;
            //ViewBag.lstPRO267 = lstPRO267;
            //ViewBag.lstPRO268 = lstPRO268;



            #endregion
            return View(objPRO270);

        }

        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO270 PRO270)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO270.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO270 objPRO270 = db.PRO270.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO270.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO270.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data

                        objPRO270.ProtocolNo = PRO270.ProtocolNo;
                        objPRO270.ElevationOfTSRFrom = PRO270.ElevationOfTSRFrom;
                        objPRO270.ReqElevationOfTSRFrom = PRO270.ReqElevationOfTSRFrom;
                        objPRO270.ReqTSRWidth = PRO270.ReqTSRWidth;
                        objPRO270.ReqTSRHeight = PRO270.ReqTSRHeight;
                        objPRO270.TSRID = PRO270.TSRID;
                        objPRO270.FlatnessIn = PRO270.FlatnessIn;
                        objPRO270.ReqTSRID = PRO270.ReqTSRID;
                        objPRO270.TolElevationOfTSRFrom = PRO270.TolElevationOfTSRFrom;
                        objPRO270.TolTSRWidth = PRO270.TolTSRWidth;
                        objPRO270.TolTSRHeight = PRO270.TolTSRHeight;
                        objPRO270.TolTSRID = PRO270.TolTSRID;
                        objPRO270.TolLevelnessOfTopFaceOfTSR = PRO270.TolLevelnessOfTopFaceOfTSR;
                        objPRO270.TolTiltingOfTSR = PRO270.TolTiltingOfTSR;
                        objPRO270.TolFlatnessIn = PRO270.TolFlatnessIn;
                        objPRO270.LevelnessOfTopFaceOfTSR = PRO270.LevelnessOfTopFaceOfTSR;
                        //objPRO270.SeamNo = PRO270.SeamNo;
                        //objPRO270.ReqRootGap = PRO270.ReqRootGap;
                        //objPRO270.ReqRootFace = PRO270.ReqRootFace;
                        //objPRO270.ReqOneSideWepAngle = PRO270.ReqOneSideWepAngle;
                        //objPRO270.ReqOtherSideWepAngle = PRO270.ReqOtherSideWepAngle;
                        //objPRO270.ActMaxRootGap = PRO270.ActMaxRootGap;
                        //objPRO270.ActMaxRootFace = PRO270.ActMaxRootFace;
                        //objPRO270.ActMaxOneSideWepAngle = PRO270.ActMaxOneSideWepAngle;
                        //objPRO270.ActMaxOtherSideWepAngle = PRO270.ActMaxOtherSideWepAngle;
                        //objPRO270.ActMinRootGap = PRO270.ActMinRootGap;
                        //objPRO270.ActMinRootFace = PRO270.ActMinRootFace;
                        //objPRO270.ActMinOneSideWepAngle = PRO270.ActMinOneSideWepAngle;
                        //objPRO270.ActMinOtherSideWepAngle = PRO270.ActMinOtherSideWepAngle;
                        //objPRO270.ReqRootGap1 = PRO270.ReqRootGap1;
                        //objPRO270.ReqRootFace1 = PRO270.ReqRootFace1;
                        //objPRO270.ReqTopSideWepAngle = PRO270.ReqTopSideWepAngle;
                        //objPRO270.ReqBottomSideWepAngle = PRO270.ReqBottomSideWepAngle;
                        //objPRO270.ReqOffset = PRO270.ReqOffset;
                        objPRO270.CheckPoint1 = PRO270.CheckPoint1;
                        objPRO270.CheckPoint2 = PRO270.CheckPoint2;
                        objPRO270.CheckPoint3 = PRO270.CheckPoint3;
                        objPRO270.CheckPoint4 = PRO270.CheckPoint4;
                        objPRO270.CheckPoint5 = PRO270.CheckPoint5;
                        objPRO270.CheckPoint6 = PRO270.CheckPoint6;
                        objPRO270.CheckPoint7 = PRO270.CheckPoint7;
                        objPRO270.CheckPoint9 = PRO270.CheckPoint9;
                        objPRO270.CheckPoint9_2 = PRO270.CheckPoint9_2;
                        objPRO270.QCRemarks = PRO270.QCRemarks;
                        objPRO270.Result = PRO270.Result;

                        objPRO270.EditedBy = objClsLoginInfo.UserName;
                        objPRO270.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO270.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO270 objPRO270 = db.PRO270.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO270 != null && string.IsNullOrWhiteSpace(objPRO270.ProtocolNo))
                    {
                        db.PRO270.Remove(objPRO270);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO270.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO271> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO271> lstAddPRO271 = new List<PRO271>();
                List<PRO271> lstDeletePRO271 = new List<PRO271>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO271 obj = db.PRO271.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.Orientation = item.Orientation;
                                obj.ActElevationOfTSRFrom = item.ActElevationOfTSRFrom;
                                obj.ActTSRWidth = item.ActTSRWidth;
                                obj.ActTSRHeight = item.ActTSRHeight;
                                obj.ActTSRID = item.ActTSRID;
                                //obj.LevelnessOfTopFaceOfTSR = item.LevelnessOfTopFaceOfTSR;
                                obj.ActTiltingOfTSR = item.ActTiltingOfTSR;
                                obj.ActFlatnessIn = item.ActFlatnessIn;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO271 obj = new PRO271();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.Orientation))
                            {
                                obj.Orientation = item.Orientation;
                                obj.ActElevationOfTSRFrom = item.ActElevationOfTSRFrom;
                                obj.ActTSRWidth = item.ActTSRWidth;
                                obj.ActTSRHeight = item.ActTSRHeight;
                                obj.ActTSRID = item.ActTSRID;
                                //obj.LevelnessOfTopFaceOfTSR = item.LevelnessOfTopFaceOfTSR;
                                obj.ActTiltingOfTSR = item.ActTiltingOfTSR;
                                obj.ActFlatnessIn = item.ActFlatnessIn;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO271.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO271.Count > 0)
                    {
                        db.PRO271.AddRange(lstAddPRO271);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO271 = db.PRO271.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO271.Count > 0)
                    {
                        db.PRO271.RemoveRange(lstDeletePRO271);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO271 = db.PRO271.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO271.Count > 0)
                    {
                        db.PRO271.RemoveRange(lstDeletePRO271);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion





        #endregion

        #region Linkage View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL270 objPRL270 = new PRL270();
            objPRL270 = db.PRL270.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL270 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL270.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;

                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstElevationOfTSRFrom = new List<string> { "REF. LINE", "TAN LINE" };

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.ElevationOfTSRFrom = lstElevationOfTSRFrom.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();


            //  ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL271> lstPRL271 = db.PRL271.Where(x => x.HeaderId == objPRL270.HeaderId).ToList();
            //List<PRL267> lstPRL267 = db.PRL267.Where(x => x.HeaderId == objPRO270.HeaderId).ToList();
            //List<PRL268> lstPRL268 = db.PRL268.Where(x => x.HeaderId == objPRO270.HeaderId).ToList();




            ViewBag.lstPRL271 = lstPRL271;
            //ViewBag.lstPRL267 = lstPRL267;
            //ViewBag.lstPRL268 = lstPRL268;


            #endregion
            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.DrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL270.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL270.ActFilledBy) && (objPRL270.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL270.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.lstDrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL270);
        }

        [HttpPost]
        public ActionResult LinkageMobile(int? id, int? m, string APIRequestFrom = "", string UserName = "")
        {
            string ProjectDesc = "";
            string ManufacturingCode = "";
            string AccessRole = "";
            bool isEditable = false;
            var DrawingNo = new List<string>();
            bool isQCRemarkEditable = false;

            PRL270 objPRL270 = new PRL270();
            db.Configuration.ProxyCreationEnabled = false;
            objPRL270 = db.PRL270.Where(i => i.HeaderId == id).FirstOrDefault();
            db.Entry(objPRL270).State = System.Data.Entity.EntityState.Detached;

            NDEModels objNDEModels = new NDEModels();
            if (objPRL270 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL270.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
            AccessRole = objUserRoleAccessDetails.UserDesignation;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstElevationOfTSRFrom = new List<string> { "REF. LINE", "TAN LINE" };

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            var YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var ElevationOfTSRFrom = lstElevationOfTSRFrom.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();


            //  ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL271> lstPRL271 = db.PRL271.Where(x => x.HeaderId == objPRL270.HeaderId).ToList();
            //List<PRL267> lstPRL267 = db.PRL267.Where(x => x.HeaderId == objPRO270.HeaderId).ToList();
            //List<PRL268> lstPRL268 = db.PRL268.Where(x => x.HeaderId == objPRO270.HeaderId).ToList();

            //ViewBag.lstPRL267 = lstPRL267;
            //ViewBag.lstPRL268 = lstPRL268;

            #endregion
            try
            {
                isEditable = false;
                isQCRemarkEditable = false;
                if (m == 1)
                {
                    isEditable = true;

                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL270.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL270.ActFilledBy) && (objPRL270.RequestNo.HasValue))
                        {
                            isEditable = false;
                            isQCRemarkEditable = true;
                        }
                        else
                        {
                            DrawingNo = (new GeneralController()).GetPLMDrawingNumber(objPRL270.Project).ToList();
                        }
                    }
                }

                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    ProjectDesc = ProjectDesc,
                    ManufacturingCode = ManufacturingCode,
                    isEditable = isEditable,
                    isQCRemarkEditable = isQCRemarkEditable,
                    DrawingNo = DrawingNo,
                    OutsideInside = OutsideInside,
                    ElevationOfTSRFrom = ElevationOfTSRFrom,
                    AccessRole = AccessRole,
                    YesNoEnum = YesNoEnum,
                    YesNAEnum = YesNAEnum,

                    objPRL270 = objPRL270,

                    lstPRL271 = lstPRL271

                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.lstDrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();

                return Json(new
                {
                    res = "0",
                    msg = ex.Message,
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL270 PRL270, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = PRL270.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL270 objPRL270 = db.PRL270.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    #region Save Data

                    objPRL270.DrawingRevisionNo = PRL270.DrawingRevisionNo;
                    objPRL270.DrawingNo = PRL270.DrawingNo;
                    objPRL270.DCRNo = PRL270.DCRNo;
                    objPRL270.ProtocolNo = PRL270.ProtocolNo != null ? PRL270.ProtocolNo : "";
                    objPRL270.ElevationOfTSRFrom = PRL270.ElevationOfTSRFrom;
                    objPRL270.ReqElevationOfTSRFrom = PRL270.ReqElevationOfTSRFrom;
                    objPRL270.ReqTSRWidth = PRL270.ReqTSRWidth;
                    objPRL270.ReqTSRHeight = PRL270.ReqTSRHeight;
                    objPRL270.TSRID = PRL270.TSRID;
                    objPRL270.FlatnessIn = PRL270.FlatnessIn;
                    objPRL270.ReqTSRID = PRL270.ReqTSRID;
                    objPRL270.TolElevationOfTSRFrom = PRL270.TolElevationOfTSRFrom;
                    objPRL270.TolTSRWidth = PRL270.TolTSRWidth;
                    objPRL270.TolTSRHeight = PRL270.TolTSRHeight;
                    objPRL270.TolTSRID = PRL270.TolTSRID;
                    objPRL270.TolLevelnessOfTopFaceOfTSR = PRL270.TolLevelnessOfTopFaceOfTSR;
                    objPRL270.TolTiltingOfTSR = PRL270.TolTiltingOfTSR;
                    objPRL270.TolFlatnessIn = PRL270.TolFlatnessIn;
                    objPRL270.LevelnessOfTopFaceOfTSR = PRL270.LevelnessOfTopFaceOfTSR;
                    //objPRL270.SeamNo = PRL270.SeamNo;
                    //objPRL270.ReqRootGap = PRL270.ReqRootGap;
                    //objPRL270.ReqRootFace = PRL270.ReqRootFace;
                    //objPRL270.ReqOneSideWepAngle = PRL270.ReqOneSideWepAngle;
                    //objPRL270.ReqOtherSideWepAngle = PRL270.ReqOtherSideWepAngle;
                    //objPRL270.ActMaxRootGap = PRL270.ActMaxRootGap;
                    //objPRL270.ActMaxRootFace = PRL270.ActMaxRootFace;
                    //objPRL270.ActMaxOneSideWepAngle = PRL270.ActMaxOneSideWepAngle;
                    //objPRL270.ActMaxOtherSideWepAngle = PRL270.ActMaxOtherSideWepAngle;
                    //objPRL270.ActMinRootGap = PRL270.ActMinRootGap;
                    //objPRL270.ActMinRootFace = PRL270.ActMinRootFace;
                    //objPRL270.ActMinOneSideWepAngle = PRL270.ActMinOneSideWepAngle;
                    //objPRL270.ActMinOtherSideWepAngle = PRL270.ActMinOtherSideWepAngle;
                    //objPRL270.ReqRootGap1 = PRL270.ReqRootGap1;
                    //objPRL270.ReqRootFace1 = PRL270.ReqRootFace1;
                    //objPRL270.ReqTopSideWepAngle = PRL270.ReqTopSideWepAngle;
                    //objPRL270.ReqBottomSideWepAngle = PRL270.ReqBottomSideWepAngle;
                    //objPRL270.ReqOffset = PRL270.ReqOffset;
                    objPRL270.CheckPoint1 = PRL270.CheckPoint1;
                    objPRL270.CheckPoint2 = PRL270.CheckPoint2;
                    objPRL270.CheckPoint3 = PRL270.CheckPoint3;
                    objPRL270.CheckPoint4 = PRL270.CheckPoint4;
                    objPRL270.CheckPoint5 = PRL270.CheckPoint5;
                    objPRL270.CheckPoint6 = PRL270.CheckPoint6;
                    objPRL270.CheckPoint7 = PRL270.CheckPoint7;
                    objPRL270.CheckPoint9 = PRL270.CheckPoint9;
                    objPRL270.CheckPoint9_2 = PRL270.CheckPoint9_2;
                    objPRL270.QCRemarks = PRL270.QCRemarks;
                    objPRL270.Result = PRL270.Result;

                    objPRL270.EditedBy = UserName;
                    objPRL270.EditedOn = DateTime.Now;

                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL270.ActFilledBy = UserName;
                            objPRL270.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL270.ReqFilledBy = UserName;
                            objPRL270.ReqFilledOn = DateTime.Now;
                        }
                    }
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL270.HeaderId;
                    #endregion

                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL270 objPRL270 = db.PRL270.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL270 != null && string.IsNullOrWhiteSpace(objPRL270.ProtocolNo))
                    {
                        db.PRL270.Remove(objPRL270);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL270.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL271> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL271> lstAddPRL271 = new List<PRL271>();
                List<PRL271> lstDeletePRL271 = new List<PRL271>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRL271 obj = db.PRL271.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.Orientation = item.Orientation;
                                obj.ActElevationOfTSRFrom = item.ActElevationOfTSRFrom;
                                obj.ActTSRWidth = item.ActTSRWidth;
                                obj.ActTSRHeight = item.ActTSRHeight;
                                obj.ActTSRID = item.ActTSRID;
                                //obj.LevelnessOfTopFaceOfTSR = item.LevelnessOfTopFaceOfTSR;
                                obj.ActTiltingOfTSR = item.ActTiltingOfTSR;
                                obj.ActFlatnessIn = item.ActFlatnessIn;
                                obj.EditedBy = UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRL271 obj = new PRL271();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.Orientation))
                            {
                                obj.Orientation = item.Orientation;
                                obj.ActElevationOfTSRFrom = item.ActElevationOfTSRFrom;
                                obj.ActTSRWidth = item.ActTSRWidth;
                                obj.ActTSRHeight = item.ActTSRHeight;
                                obj.ActTSRID = item.ActTSRID;
                                //obj.LevelnessOfTopFaceOfTSR = item.LevelnessOfTopFaceOfTSR;
                                obj.ActTiltingOfTSR = item.ActTiltingOfTSR;
                                obj.ActFlatnessIn = item.ActFlatnessIn;
                                obj.CreatedBy = UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRL271.Add(obj);
                            }
                        }
                    }
                }
                if (lstAddPRL271.Count > 0)
                {
                    db.PRL271.AddRange(lstAddPRL271);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL271 = db.PRL271.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL271 = db.PRL271.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL271.Count > 0)
                {
                    db.PRL271.RemoveRange(lstDeletePRL271);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion




        #endregion
    }
}
