﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol092Controller : clsBase
    {
        // GET: PROTOCOL/Protocol092
        string ControllerURL = "/PROTOCOL/Protocol092/";
        string Title = "REPORT FOR WATER BOX MACHINING INSPECTION - R1";

        #region Details View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO455 objPRO455 = new PRO455();
            if (!id.HasValue)
            {
                try
                {
                    objPRO455.ProtocolNo = string.Empty;
                    objPRO455.CreatedBy = objClsLoginInfo.UserName;
                    objPRO455.CreatedOn = DateTime.Now;

                    #region  MACHINING | PRO457
                    objPRO455.PRO457.Add(new PRO457
                    {
                        SrNo = "1",
                        Description = "Identification",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO455.PRO457.Add(new PRO457
                    {
                        SrNo = "2",
                        Description = "Visual inspection",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO455.PRO457.Add(new PRO457
                    {
                        SrNo = "3",
                        Description = "Bolt hole size",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO455.PRO457.Add(new PRO457
                    {
                        SrNo = "4",
                        Description = "Total nos. of bolt holes",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO455.PRO457.Add(new PRO457
                    {
                        SrNo = "5",
                        Description = "Bolt holes layout as per drg.",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO455.PRO457.Add(new PRO457
                    {
                        SrNo = "6",
                        Description = "Bolt holes surface finish",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO455.PRO457.Add(new PRO457
                    {
                        SrNo = "7",
                        Description = "Bolt hole  equal pitch",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO455.PRO457.Add(new PRO457
                    {
                        SrNo = "8",
                        Description = "Bolt hole to bolt hole distance A",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO455.PRO457.Add(new PRO457
                    {
                        SrNo = "9",
                        Description = "Bolt hole to bolt hole distance B",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO455.PRO457.Add(new PRO457
                    {
                        SrNo = "10",
                        Description = "Bolt hole to bolt hole distance C",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO455.PRO457.Add(new PRO457
                    {
                        SrNo = "11",
                        Description = "Bolt hole to bolt hole distance D",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO455.PRO457.Add(new PRO457
                    {
                        SrNo = "12",
                        Description = "Bolt holes ref distance",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO455.PRO457.Add(new PRO457
                    {
                        SrNo = "13",
                        Description = "Out side flange edge to bolt hole centre distance",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO455.PRO457.Add(new PRO457
                    {
                        SrNo = "14",
                        Description = "Flange thickness",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO455.PRO457.Add(new PRO457
                    {
                        SrNo = "15",
                        Description = "Surface Finish of flange",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO455.PRO457.Add(new PRO457
                    {
                        SrNo = "16",
                        Description = "Vent holes as per drg.",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO455.PRO457.Add(new PRO457
                    {
                        SrNo = "17",
                        Description = "Vent holes dimensions",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO455.PRO457.Add(new PRO457
                    {
                        SrNo = "18",
                        Description = "Deburring",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion

                    #region HOLE TO HOLE LIGAMENT | PRO458
                    for (int i = 1; i < 180; i++)
                    {
                        PRO458 obj = new PRO458();
                        if (i != 179)
                        {
                            obj.ReqHoleNoToHoleNo1 = (i + " to " + (i + 1));
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                        }
                        else
                        {
                            obj.ReqHoleNoToHoleNo1 = (i + " to 1");
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                        }
                        //System.Threading.Thread.Sleep(100);

                        objPRO455.PRO458.Add(obj);
                    }
                    #endregion

                    #region OUT SIDE FRAME EDGE TO HOLE CENTER LIGAMENT & FRAME THICKNESS | PRO459
                    for (int i = 1; i < 180; i++)
                    {
                        PRO459 obj = new PRO459();
                        obj.ReqHoleNo = Convert.ToString(i);
                        obj.CreatedBy = objClsLoginInfo.UserName;
                        obj.CreatedOn = DateTime.Now;
                        objPRO455.PRO459.Add(obj);
                    }
                    #endregion

                    db.PRO455.Add(objPRO455);
                    db.SaveChanges();
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO455.HeaderId;
            }
            else
            {
                objPRO455 = db.PRO455.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO456> lstPRO456 = db.PRO456.Where(x => x.HeaderId == objPRO455.HeaderId).ToList();
            List<PRO457> lstPRO457 = db.PRO457.Where(x => x.HeaderId == objPRO455.HeaderId).ToList();
            List<PRO458> lstPRO458 = db.PRO458.Where(x => x.HeaderId == objPRO455.HeaderId).ToList();
            List<PRO459> lstPRO459 = db.PRO459.Where(x => x.HeaderId == objPRO455.HeaderId).ToList();

            ViewBag._lstPRO456 = lstPRO456;
            ViewBag._lstPRO457 = lstPRO457;
            ViewBag._lstPRO458 = lstPRO458;
            ViewBag._lstPRO459 = lstPRO459;

            #endregion

            return View(objPRO455);
            //return View();
        }

        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO455 PRO455)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO455.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO455 objPRO455 = db.PRO455.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO455.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO455.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data

                        objPRO455.ProtocolNo = PRO455.ProtocolNo;
                        objPRO455.InspectionStage = PRO455.InspectionStage;

                        objPRO455.PartDescription = PRO455.PartDescription;
                        objPRO455.ItemNo = PRO455.ItemNo;
                        objPRO455.ReqLigament = PRO455.ReqLigament;
                        objPRO455.ActHoleDia = PRO455.ActHoleDia;
                        objPRO455.ActLigamentFromCustomerSide = PRO455.ActLigamentFromCustomerSide;
                        objPRO455.ActFrameThickness = PRO455.ActFrameThickness;

                        objPRO455.CheckPoint1 = PRO455.CheckPoint1;
                        objPRO455.CheckPoint2 = PRO455.CheckPoint2;
                        objPRO455.CheckPoint3_1_1 = PRO455.CheckPoint3_1_1;
                        objPRO455.CheckPoint3_1_2 = PRO455.CheckPoint3_1_2;
                        objPRO455.CheckPoint3_2_1 = PRO455.CheckPoint3_2_1;
                        objPRO455.CheckPoint3_2_2 = PRO455.CheckPoint3_2_2;
                        objPRO455.CheckPoint3_3_1 = PRO455.CheckPoint3_3_1;
                        objPRO455.CheckPoint3_3_2 = PRO455.CheckPoint3_3_2;
                        objPRO455.CheckPoint3_4_1 = PRO455.CheckPoint3_4_1;
                        objPRO455.CheckPoint3_4_2 = PRO455.CheckPoint3_4_2;
                        objPRO455.CheckPoint3_5_1 = PRO455.CheckPoint3_5_1;
                        objPRO455.CheckPoint3_5_2 = PRO455.CheckPoint3_5_2;

                        objPRO455.QCRemarks = PRO455.QCRemarks;
                        objPRO455.Result = PRO455.Result;

                        objPRO455.EditedBy = objClsLoginInfo.UserName;
                        objPRO455.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO455.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO455 objPRO455 = db.PRO455.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO455 != null && string.IsNullOrWhiteSpace(objPRO455.ProtocolNo))
                    {
                        db.PRO455.Remove(objPRO455);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO455.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO456> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO456> lstAddPRO456 = new List<PRO456>();
                List<PRO456> lstDeletePRO456 = new List<PRO456>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO456 obj = db.PRO456.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.DrgNo = item.DrgNo;
                                obj.RevNo = item.RevNo;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO456 obj = new PRO456();

                            if (!string.IsNullOrWhiteSpace(item.DrgNo))
                            {
                                obj.HeaderId = item.HeaderId;
                                obj.DrgNo = item.DrgNo;
                                obj.RevNo = item.RevNo;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO456.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO456.Count > 0)
                    {
                        db.PRO456.AddRange(lstAddPRO456);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO456 = db.PRO456.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO456.Count > 0)
                    {
                        db.PRO456.RemoveRange(lstDeletePRO456);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO456 = db.PRO456.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO456.Count > 0)
                    {
                        db.PRO456.RemoveRange(lstDeletePRO456);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2(List<PRO457> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO457> lstAddPRO457 = new List<PRO457>();
                List<PRO457> lstDeletePRO457 = new List<PRO457>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO457 obj = db.PRO457.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO457();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SrNo = item.SrNo;
                        obj.Description = item.Description;
                        obj.ReqValue = item.ReqValue;
                        obj.ReqTolerance = item.ReqTolerance;
                        obj.ActValue = item.ActValue;
                        obj.Remarks = item.Remarks;

                        if (isAdded)
                        {
                            lstAddPRO457.Add(obj);
                        }
                    }
                    if (lstAddPRO457.Count > 0)
                    {
                        db.PRO457.AddRange(lstAddPRO457);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO457 = db.PRO457.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO457.Count > 0)
                    {
                        db.PRO457.RemoveRange(lstDeletePRO457);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO457 = db.PRO457.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO457.Count > 0)
                    {
                        db.PRO457.RemoveRange(lstDeletePRO457);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3(List<PRO458> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO458> lstAddPRO458 = new List<PRO458>();
                List<PRO458> lstDeletePRO458 = new List<PRO458>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO458 obj = db.PRO458.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.ReqHoleNoToHoleNo1 = item.ReqHoleNoToHoleNo1;
                                obj.ReqHoleNoToHoleNo2 = item.ReqHoleNoToHoleNo2;
                                obj.ActHoleToHoleLigament = item.ActHoleToHoleLigament;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO458 obj = new PRO458();

                            if (!string.IsNullOrWhiteSpace(item.ActValue))
                            {
                                obj.HeaderId = item.HeaderId;
                                obj.ReqHoleNoToHoleNo1 = item.ReqHoleNoToHoleNo1;
                                obj.ReqHoleNoToHoleNo2 = item.ReqHoleNoToHoleNo2;
                                obj.ActHoleToHoleLigament = item.ActHoleToHoleLigament;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO458.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO458.Count > 0)
                    {
                        db.PRO458.AddRange(lstAddPRO458);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO458 = db.PRO458.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO458.Count > 0)
                    {
                        db.PRO458.RemoveRange(lstDeletePRO458);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO458 = db.PRO458.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO458.Count > 0)
                    {
                        db.PRO458.RemoveRange(lstDeletePRO458);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line4 details
        [HttpPost]
        public JsonResult SaveProtocolLine4(List<PRO459> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO459> lstAddPRO459 = new List<PRO459>();
                List<PRO459> lstDeletePRO459 = new List<PRO459>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO459 obj = db.PRO459.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.ReqHoleNo = item.ReqHoleNo;
                                obj.ActHoleDIA = item.ActHoleDIA;
                                obj.ActLigamentFromOuterSide = item.ActLigamentFromOuterSide;
                                obj.ActFrameThickness = item.ActFrameThickness;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO459 obj = new PRO459();

                            if (!string.IsNullOrWhiteSpace(item.ReqHoleNo))
                            {
                                obj.HeaderId = item.HeaderId;
                                obj.ReqHoleNo = item.ReqHoleNo;
                                obj.ActHoleDIA = item.ActHoleDIA;
                                obj.ActLigamentFromOuterSide = item.ActLigamentFromOuterSide;
                                obj.ActFrameThickness = item.ActFrameThickness;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO459.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO459.Count > 0)
                    {
                        db.PRO459.AddRange(lstAddPRO459);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO459 = db.PRO459.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO459.Count > 0)
                    {
                        db.PRO459.RemoveRange(lstDeletePRO459);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO459 = db.PRO459.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO459.Count > 0)
                    {
                        db.PRO459.RemoveRange(lstDeletePRO459);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

        #region Linkage View Code

        #region Header
        public ActionResult Linkage(int? id, int? m)
        {
            PRL455 objPRL455 = new PRL455();
            objPRL455 = db.PRL455.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL455 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL455.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL456> lstPRL456 = db.PRL456.Where(x => x.HeaderId == objPRL455.HeaderId).ToList();
            List<PRL457> lstPRL457 = db.PRL457.Where(x => x.HeaderId == objPRL455.HeaderId).ToList();
            List<PRL458> lstPRL458 = db.PRL458.Where(x => x.HeaderId == objPRL455.HeaderId).ToList();
            List<PRL459> lstPRL459 = db.PRL459.Where(x => x.HeaderId == objPRL455.HeaderId).ToList();

            ViewBag._lstPRL456 = lstPRL456;
            ViewBag._lstPRL457 = lstPRL457;
            ViewBag._lstPRL458 = lstPRL458;
            ViewBag._lstPRL459 = lstPRL459;
            #endregion

            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.DrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL455.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL455.ActFilledBy) && (objPRL455.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL455.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL455);

            //return View();
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL455 PRL455, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = PRL455.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL455 objPRL455 = db.PRL455.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL455.DrawingRevisionNo = PRL455.DrawingRevisionNo;
                    objPRL455.DrawingNo = PRL455.DrawingNo;
                    objPRL455.DCRNo = PRL455.DCRNo;
                    objPRL455.ProtocolNo = PRL455.ProtocolNo;

                    objPRL455.ProtocolNo = PRL455.ProtocolNo;
                    objPRL455.InspectionStage = PRL455.InspectionStage;

                    objPRL455.PartDescription = PRL455.PartDescription;
                    objPRL455.ItemNo = PRL455.ItemNo;
                    objPRL455.ReqLigament = PRL455.ReqLigament;
                    objPRL455.ActHoleDia = PRL455.ActHoleDia;
                    objPRL455.ActLigamentFromCustomerSide = PRL455.ActLigamentFromCustomerSide;
                    objPRL455.ActFrameThickness = PRL455.ActFrameThickness;

                    objPRL455.CheckPoint1 = PRL455.CheckPoint1;
                    objPRL455.CheckPoint2 = PRL455.CheckPoint2;
                    objPRL455.CheckPoint3_1_1 = PRL455.CheckPoint3_1_1;
                    objPRL455.CheckPoint3_1_2 = PRL455.CheckPoint3_1_2;
                    objPRL455.CheckPoint3_2_1 = PRL455.CheckPoint3_2_1;
                    objPRL455.CheckPoint3_2_2 = PRL455.CheckPoint3_2_2;
                    objPRL455.CheckPoint3_3_1 = PRL455.CheckPoint3_3_1;
                    objPRL455.CheckPoint3_3_2 = PRL455.CheckPoint3_3_2;
                    objPRL455.CheckPoint3_4_1 = PRL455.CheckPoint3_4_1;
                    objPRL455.CheckPoint3_4_2 = PRL455.CheckPoint3_4_2;
                    objPRL455.CheckPoint3_5_1 = PRL455.CheckPoint3_5_1;
                    objPRL455.CheckPoint3_5_2 = PRL455.CheckPoint3_5_2;

                    objPRL455.QCRemarks = PRL455.QCRemarks;
                    objPRL455.Result = PRL455.Result;

                    objPRL455.EditedBy = UserName;
                    objPRL455.EditedOn = DateTime.Now;
                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL455.ActFilledBy = UserName;
                            objPRL455.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL455.ReqFilledBy = UserName;
                            objPRL455.ReqFilledOn = DateTime.Now;
                        }
                    }
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL455.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL455 objPRL455 = db.PRL455.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL455 != null && string.IsNullOrWhiteSpace(objPRL455.ProtocolNo))
                    {
                        db.PRL455.Remove(objPRL455);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL455.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL456> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRL456> lstAddPRL456 = new List<PRL456>();
                List<PRL456> lstDeletePRL456 = new List<PRL456>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRL456 obj = db.PRL456.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.DrgNo = item.DrgNo;
                                obj.RevNo = item.RevNo;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRL456 obj = new PRL456();

                            if (!string.IsNullOrWhiteSpace(item.DrgNo))
                            {
                                obj.HeaderId = item.HeaderId;
                                obj.DrgNo = item.DrgNo;
                                obj.RevNo = item.RevNo;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRL456.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRL456.Count > 0)
                    {
                        db.PRL456.AddRange(lstAddPRL456);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRL456 = db.PRL456.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRL456.Count > 0)
                    {
                        db.PRL456.RemoveRange(lstDeletePRL456);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL456 = db.PRL456.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL456.Count > 0)
                    {
                        db.PRL456.RemoveRange(lstDeletePRL456);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2Linkage(List<PRL457> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRL457> lstAddPRL457 = new List<PRL457>();
                List<PRL457> lstDeletePRL457 = new List<PRL457>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL457 obj = db.PRL457.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL457();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SrNo = item.SrNo;
                        obj.Description = item.Description;
                        obj.ReqValue = item.ReqValue;
                        obj.ReqTolerance = item.ReqTolerance;
                        obj.ActValue = item.ActValue;
                        obj.Remarks = item.Remarks;

                        if (isAdded)
                        {
                            lstAddPRL457.Add(obj);
                        }
                    }
                    if (lstAddPRL457.Count > 0)
                    {
                        db.PRL457.AddRange(lstAddPRL457);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL457 = db.PRL457.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL457.Count > 0)
                    {
                        db.PRL457.RemoveRange(lstDeletePRL457);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL457 = db.PRL457.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL457.Count > 0)
                    {
                        db.PRL457.RemoveRange(lstDeletePRL457);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3Linkage(List<PRL458> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRL458> lstAddPRL458 = new List<PRL458>();
                List<PRL458> lstDeletePRL458 = new List<PRL458>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRL458 obj = db.PRL458.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.ReqHoleNoToHoleNo1 = item.ReqHoleNoToHoleNo1;
                                obj.ReqHoleNoToHoleNo2 = item.ReqHoleNoToHoleNo2;
                                obj.ActHoleToHoleLigament = item.ActHoleToHoleLigament;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRL458 obj = new PRL458();

                            if (!string.IsNullOrWhiteSpace(item.ActValue))
                            {
                                obj.HeaderId = item.HeaderId;
                                obj.ReqHoleNoToHoleNo1 = item.ReqHoleNoToHoleNo1;
                                obj.ReqHoleNoToHoleNo2 = item.ReqHoleNoToHoleNo2;
                                obj.ActHoleToHoleLigament = item.ActHoleToHoleLigament;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRL458.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRL458.Count > 0)
                    {
                        db.PRL458.AddRange(lstAddPRL458);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRL458 = db.PRL458.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRL458.Count > 0)
                    {
                        db.PRL458.RemoveRange(lstDeletePRL458);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL458 = db.PRL458.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL458.Count > 0)
                    {
                        db.PRL458.RemoveRange(lstDeletePRL458);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line4 details
        [HttpPost]
        public JsonResult SaveProtocolLine4Linkage(List<PRL459> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRL459> lstAddPRL459 = new List<PRL459>();
                List<PRL459> lstDeletePRL459 = new List<PRL459>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRL459 obj = db.PRL459.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.ReqHoleNo = item.ReqHoleNo;
                                obj.ActHoleDIA = item.ActHoleDIA;
                                obj.ActLigamentFromOuterSide = item.ActLigamentFromOuterSide;
                                obj.ActFrameThickness = item.ActFrameThickness;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRL459 obj = new PRL459();

                            if (!string.IsNullOrWhiteSpace(item.ReqHoleNo))
                            {
                                obj.HeaderId = item.HeaderId;
                                obj.ReqHoleNo = item.ReqHoleNo;
                                obj.ActHoleDIA = item.ActHoleDIA;
                                obj.ActLigamentFromOuterSide = item.ActLigamentFromOuterSide;
                                obj.ActFrameThickness = item.ActFrameThickness;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRL459.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRL459.Count > 0)
                    {
                        db.PRL459.AddRange(lstAddPRL459);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRL459 = db.PRL459.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRL459.Count > 0)
                    {
                        db.PRL459.RemoveRange(lstDeletePRL459);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL459 = db.PRL459.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL459.Count > 0)
                    {
                        db.PRL459.RemoveRange(lstDeletePRL459);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion
    }
}