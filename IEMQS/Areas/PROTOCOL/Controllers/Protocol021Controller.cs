﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol021Controller : clsBase
    {
        // GET: PROTOCOL/Protocol021
        string ControllerURL = "/PROTOCOL/Protocol021/";
        string Title = "REPORT OF HARDNESS TEST";
        #region Details View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id,int? m)
        {
            PRO100 objPRO100 = new PRO100();
            if (!id.HasValue)
            {
                objPRO100.ProtocolNo = string.Empty;
                objPRO100.CreatedBy = objClsLoginInfo.UserName;
                objPRO100.CreatedOn = DateTime.Now;
                db.PRO100.Add(objPRO100);
                db.SaveChanges();
                ViewBag.HeaderId = objPRO100.HeaderId;
            }
            else
            {
                objPRO100 = db.PRO100.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            return View(objPRO100);

        }
        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO100 PRO100)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO100.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO100 objPRO100 = db.PRO100.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO100.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO100.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data
                        objPRO100.ProtocolNo = PRO100.ProtocolNo;
                        objPRO100.HardnessTestingEquipmentDetails = PRO100.HardnessTestingEquipmentDetails;
                        objPRO100.InspectionCertificateNo = PRO100.InspectionCertificateNo;
                        


                        objPRO100.EditedBy = objClsLoginInfo.UserName;
                        objPRO100.EditedOn = DateTime.Now;
                        

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol saved successfully";
                        objResponseMsg.HeaderId = objPRO100.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO100 objPRO100 = db.PRO100.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO100 != null && string.IsNullOrWhiteSpace(objPRO100.ProtocolNo))
                    {
                        db.PRO101.RemoveRange(objPRO100.PRO101.ToList());
                        db.PRO100.Remove(objPRO100);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.REPORT_OF_HARDNESS_TEST.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO100.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public ActionResult GetProtocolLines1Data(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro.SeamNo like '%" + param.sSearch
                        + "%' or pro.Point like '%" + param.sSearch
                        + "%' or pro.Location like '%" + param.sSearch
                        + "%' or pro.Reading1 like '%" + param.sSearch
                        + "%' or pro.Reading2 like '%" + param.sSearch
                        + "%' or pro.Reading3 like '%" + param.sSearch
                        + "%' or pro.Reading4 like '%" + param.sSearch
                        + "%' or pro.Reading5 like '%" + param.sSearch
                        + "%' or pro.Remarks like '%" + param.sSearch
                       

                        + "%')";
                }

                var lstResult = db.SP_IPI_PROTOCOL021_GET_LINES1_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                                   "1",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "SeamNo", "", "", true, "", false, "25","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Point", "", "", true, "", false, "25","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Location", "", "", true, "", false, "25","QC"),
                                   Helper.GenerateNumericTextbox(newRecordId, "Reading1", "", "", true, "", false, "10","QC"),
                                   Helper.GenerateNumericTextbox(newRecordId, "Reading2", "", "", true, "", false, "10","QC"),
                                   Helper.GenerateNumericTextbox(newRecordId, "Reading3", "", "", true, "", false, "10","QC"),
                                   Helper.GenerateNumericTextbox(newRecordId, "Reading4", "", "", true, "", false, "10","QC"),
                                   Helper.GenerateNumericTextbox(newRecordId, "Reading5", "", "", true, "", false, "10","QC"),
                                   "",
                                   Helper.GenerateHTMLTextbox(newRecordId, "Remarks", "", "", true, "", false, "100","QC"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine1(0);")
                                };
                var data = (from uc in lstResult
                            select new[]
                                {
                                   "1",
                                   Convert.ToString(uc.HeaderId),
                                   Convert.ToString(uc.LineId),
                                   Helper.GenerateHTMLTextbox(uc.LineId, "SeamNo", Convert.ToString(uc.SeamNo) ,"", true, "", false, "25","QC"),
                                   Helper.GenerateHTMLTextbox(uc.LineId, "Point", Convert.ToString(uc.Point), "", true, "", false, "25","QC"),
                                   Helper.GenerateHTMLTextbox(uc.LineId, "Location", Convert.ToString(uc.Location), "", true, "", false, "25","QC"),
                                   Helper.GenerateNumericTextbox(uc.LineId, "Reading1", Convert.ToString(uc.Reading1), "", true, "", false, "10","QC"),
                                   Helper.GenerateNumericTextbox(uc.LineId, "Reading2", Convert.ToString(uc.Reading2), "", true, "", false, "10","QC"),
                                   Helper.GenerateNumericTextbox(uc.LineId, "Reading3", Convert.ToString(uc.Reading3), "", true,"", false, "10","QC"),
                                   Helper.GenerateNumericTextbox(uc.LineId, "Reading4", Convert.ToString(uc.Reading4), "", true,"", false, "10","QC"),
                                   Helper.GenerateNumericTextbox(uc.LineId, "Reading5",  Convert.ToString(uc.Reading5), "", true,"", false, "10","QC"),
                                    Convert.ToString(uc.Average),
                                   Helper.GenerateHTMLTextbox(uc.LineId, "Remarks", Convert.ToString(uc.Remarks), "",true, "", false, "100","QC"),

                               
                                HTMLActionString(uc.LineId, "Edit", "Edit Record", "fa fa-pencil-square-o iconspace editline", "") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.QC3.GetStringValue().ToUpper() ? HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation(" + uc.LineId + "," + uc.HeaderId + ",'" + ControllerURL + "DeleteProtocolLine1','tblProtocolLines1')") : "")
                            }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult SaveProtocolLine1(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRO101 objPRO101 = new PRO101();
           // int newRowIndex = 0;
            try
            {
                int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                if (refHeaderId > 0)
                {
                    
                    string strSeamNo = !string.IsNullOrEmpty(fc["SeamNo" + refLineId]) ? Convert.ToString(fc["SeamNo" + refLineId]).Trim() : "";
                    string strPoint = !string.IsNullOrEmpty(fc["Point" + refLineId]) ? Convert.ToString(fc["Point" + refLineId]).Trim() : "";
                    string strLocation= !string.IsNullOrEmpty(fc["Location" + refLineId]) ? Convert.ToString(fc["Location" + refLineId]).Trim() : "";
                    
                    decimal? decReading1 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["Reading1" + refLineId]));
                    decimal? decReading2 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["Reading2" + refLineId]));
                    decimal? decReading3 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["Reading3" + refLineId]));
                    decimal? decReading4 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["Reading4" + refLineId]));
                    decimal? decReading5 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["Reading5" + refLineId]));
                   
                    string strRemarks = !string.IsNullOrEmpty(fc["Remarks" + refLineId]) ? Convert.ToString(fc["Remarks" + refLineId]).Trim() : "";
                    #region Add New Spot Line1
                    if (refLineId > 0)
                    {

                        objPRO101 = db.PRO101.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRO101.EditedBy = objClsLoginInfo.UserName;
                        objPRO101.EditedOn = DateTime.Now;
                    }
                    objPRO101.HeaderId = refHeaderId;
                    objPRO101.SeamNo = strSeamNo;
                    objPRO101.Point = strPoint;
                    objPRO101.Location = strLocation;
            
                    objPRO101.Reading1 = decReading1;
                    objPRO101.Reading2 = decReading2;
                    objPRO101.Reading3 = decReading3;
                    objPRO101.Reading4 = decReading4;
                    objPRO101.Reading5 = decReading5;
                    objPRO101.Remarks = strRemarks;
                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = "Data updated successfully";
                    }
                    else
                    {
                        objPRO101.CreatedBy = objClsLoginInfo.UserName;
                        objPRO101.CreatedOn = DateTime.Now;
                        db.PRO101.Add(objPRO101);
                        db.SaveChanges();
                        objResponseMsg.Value = "Data added successfully";
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRO101.LineId;
                    #endregion

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeleteProtocolLine1(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRO101 objPRO101 = db.PRO101.FirstOrDefault(c => c.LineId == lineId);
                if (objPRO101 != null)
                {
                    db.PRO101.Remove(objPRO101);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #endregion

        #region Linkage View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL100 objPRL100 = new PRL100();
            objPRL100 = db.PRL100.Where(i => i.HeaderId == id).FirstOrDefault();

            if (objPRL100 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL100.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objQproject.ManufacturingCode;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;
            
            if (m == 1)
            {
                ViewBag.isEditable = "true";
            }
            else
            { ViewBag.isEditable = "false"; }

            return View(objPRL100);
        }
        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL100 PRL100)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRL100.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL100 objPRL100 = db.PRL100.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL100.DCRNo = PRL100.DCRNo;
                    objPRL100.ProtocolNo = PRL100.ProtocolNo != null ? PRL100.ProtocolNo : "";
                    objPRL100.HardnessTestingEquipmentDetails = PRL100.HardnessTestingEquipmentDetails;
                    objPRL100.InspectionCertificateNo = PRL100.InspectionCertificateNo;
                    objPRL100.EditedBy = objClsLoginInfo.UserName;
                    objPRL100.EditedOn = DateTime.Now;

                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol saved successfully";
                    objResponseMsg.HeaderId = objPRL100.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL100 objPRL100 = db.PRL100.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL100 != null && string.IsNullOrWhiteSpace(objPRL100.ProtocolNo))
                    {
                        db.PRL101.RemoveRange(objPRL100.PRL101.ToList());
                       
                        db.PRL100.Remove(objPRL100);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL100.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #region Line1 details
        [HttpPost]
        public ActionResult GetProtocolLines1DataLinkage(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro.SeamNo like '%" + param.sSearch
                        + "%' or pro.Point like '%" + param.sSearch
                        + "%' or pro.Location like '%" + param.sSearch
                        + "%' or pro.Reading1 like '%" + param.sSearch
                        + "%' or pro.Reading2 like '%" + param.sSearch
                        + "%' or pro.Reading3 like '%" + param.sSearch
                        + "%' or pro.Reading4 like '%" + param.sSearch
                        + "%' or pro.Reading5 like '%" + param.sSearch
                        + "%' or pro.Remarks like '%" + param.sSearch
                        + "%')";
                }

                var lstResult = db.SP_IPI_PROTOCOL021_LINKAGE_GET_LINES1_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                                  "1",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "SeamNo", "", "", false, "", false, "25","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Point", "", "", false, "", false, "25","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Location", "", "", false, "", false, "10","QC"),
                                   Helper.GenerateNumericTextbox(newRecordId, "Reading1", "", "", false, "", false, "10","QC"),
                                   Helper.GenerateNumericTextbox(newRecordId, "Reading2", "", "", false, "", false, "10","QC"),
                                   Helper.GenerateNumericTextbox(newRecordId, "Reading3", "", "", false, "", false, "10","QC"),
                                   Helper.GenerateNumericTextbox(newRecordId, "Reading4", "", "", false, "", false, "10","QC"),
                                   Helper.GenerateNumericTextbox(newRecordId, "Reading5", "", "", false, "", false, "10","QC"),
                                   "",
                                   Helper.GenerateHTMLTextbox(newRecordId, "Remarks", "", "", false, "", false, "100","QC"),
                                  Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine1(0);")
                                };
                var data = (from uc in lstResult
                            select new[]
                            {
                                "1",
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.LineId),
                                Helper.GenerateHTMLTextbox(uc.LineId, "SeamNo", Convert.ToString(uc.SeamNo) ,"", true, "", false, "25","QC"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "Point", Convert.ToString(uc.Point), "", true, "", false, "25","QC"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "Location", Convert.ToString(uc.Location), "", true, "", false, "25","QC"),
                                Helper.GenerateNumericTextbox(uc.LineId, "Reading1", Convert.ToString(uc.Reading1), "", true, "", false, "10","QC"),
                                Helper.GenerateNumericTextbox(uc.LineId, "Reading2", Convert.ToString(uc.Reading2), "", true, "", false, "10","QC"),
                                Helper.GenerateNumericTextbox(uc.LineId, "Reading3", Convert.ToString(uc.Reading3), "", true,"", false, "10","QC"),
                                Helper.GenerateNumericTextbox(uc.LineId, "Reading4", Convert.ToString(uc.Reading4), "", true,"", false, "10","QC"),
                                Helper.GenerateNumericTextbox(uc.LineId, "Reading5",  Convert.ToString(uc.Reading5), "", true,"", false, "10","QC"),
                                Convert.ToString(uc.Average),
                                Helper.GenerateHTMLTextbox(uc.LineId, "Remarks", Convert.ToString(uc.Remarks), "",true, "", false, "100","QC"),

                                Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == "QC3" ? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine1Linkage','tblProtocolLines1')"):"")
                         }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine1Linkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRL101 objPRL101 = new PRL101();
          
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;

                if (refHeaderId > 0)
                {
                    

                    string strSeamNo = !string.IsNullOrEmpty(fc["SeamNo" + refLineId]) ? Convert.ToString(fc["SeamNo" + refLineId]).Trim() : "";
                    string strPoint = !string.IsNullOrEmpty(fc["Point" + refLineId]) ? Convert.ToString(fc["Point" + refLineId]).Trim() : "";
                    string strLocation = !string.IsNullOrEmpty(fc["Location" + refLineId]) ? Convert.ToString(fc["Location" + refLineId]).Trim() : "";

                    decimal? decReading1 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["Reading1" + refLineId]));
                    decimal? decReading2 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["Reading2" + refLineId]));
                    decimal? decReading3 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["Reading3" + refLineId]));
                    decimal? decReading4 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["Reading4" + refLineId]));
                    decimal? decReading5 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["Reading5" + refLineId]));

                    string strRemarks = !string.IsNullOrEmpty(fc["Remarks" + refLineId]) ? Convert.ToString(fc["Remarks" + refLineId]).Trim() : "";



                    #region Add New Spot Line1
                    if (refLineId > 0)
                    {
                        objPRL101 = db.PRL101.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRL101.EditedBy = objClsLoginInfo.UserName;
                        objPRL101.EditedOn = DateTime.Now;
                    }
                    objPRL101.HeaderId = refHeaderId;
                    objPRL101.SeamNo = strSeamNo;
                    objPRL101.Point = strPoint;
                    objPRL101.Location = strLocation;
                    objPRL101.Reading1 = decReading1;
                    objPRL101.Reading2 = decReading2;
                    objPRL101.Reading3 = decReading3;
                    objPRL101.Reading4 = decReading4;
                    objPRL101.Reading5 = decReading5;
                    objPRL101.Remarks = strRemarks;
                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = "Data updated successfully";
                    }
                    else
                    {

                        objPRL101.CreatedBy = objClsLoginInfo.UserName;
                        objPRL101.CreatedOn = DateTime.Now;
                        db.PRL101.Add(objPRL101);
                        db.SaveChanges();

                        
                        objResponseMsg.Value = "Data added successfully";
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRL101.LineId;
                    #endregion

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolLine1Linkage(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRL101 objPRL101 = db.PRL101.FirstOrDefault(c => c.LineId == lineId);
                if (objPRL101 != null)
                {
                    db.PRL101.Remove(objPRL101);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        #endregion
        #endregion
        public string HTMLActionString(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", string onHrefURL = "", bool isDisable = false)
        {
            string htmlControl = "";
            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? onClickMethod : "";
            string onHref = !string.IsNullOrEmpty(onHrefURL) ? "href='" + onHrefURL + "'" : "";

            if (isDisable)
            {
                htmlControl += "<i id=\"" + inputID.ToString() + "\" name=\"" + inputID + "\" ></i>";
                //htmlControl = "<i id=\"" + inputID + "' name='" + inputID + "' style='cursor: pointer; opacity:0.3; margin-right:10px;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";
            }
            else
            {
                htmlControl += "<i id=\"" + inputID.ToString() + "\" name=\"" + inputID + "\"  style='cursor: pointer; margin-right:10px;' Title=\"" + buttonTooltip + "\" class=\"" + className + "\"  onclick=\"" + onClickEvent + "\" ></i>";
                //htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer; margin-right:10px;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
            }

            if (!string.IsNullOrEmpty(onHref))
            {
                htmlControl = "<a " + onHref + " >" + htmlControl + "</a>";
            }

            return htmlControl;
        }
   
    }
    
}