﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol029Controller : clsBase
    {
        string ControllerURL = "/PROTOCOL/Protocol029/";
        string Title = "REPORT OR ARM REVIEW";
        // GET: PROTOCOL/Protocol029
        #region Details View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO140 objPRO140 = new PRO140();
            if (!id.HasValue)
            {
                objPRO140.ProtocolNo = string.Empty;
                objPRO140.CreatedBy = objClsLoginInfo.UserName;
                objPRO140.CreatedOn = DateTime.Now;
                db.PRO140.Add(objPRO140);
                db.SaveChanges();
                ViewBag.HeaderId = objPRO140.HeaderId;
            }
            else
            {
                objPRO140 = db.PRO140.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            return View(objPRO140);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO140 PRO140)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO140.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO140 objPRO140 = db.PRO140.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO140.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO140.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data
                        objPRO140.ProtocolNo = PRO140.ProtocolNo;
                        objPRO140.PreparedBy = PRO140.PreparedBy;
                        objPRO140.EditedBy = objClsLoginInfo.UserName;
                        objPRO140.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol saved successfully";
                        objResponseMsg.HeaderId = objPRO140.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO140 objPRO140 = db.PRO140.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO140 != null && string.IsNullOrWhiteSpace(objPRO140.ProtocolNo))
                    {
                        db.PRO141.RemoveRange(objPRO140.PRO141.ToList());
                        db.PRO140.Remove(objPRO140);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.REPORT_OF_ARM_REVIEW.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO140.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line1 details
        [HttpPost]
        public ActionResult GetProtocolLines1Data(JQueryDataTableParamModel param, string headerId)
        {
            try
            {

                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro.ARMClauseNo like '%" + param.sSearch
                        + "%' or pro.Description like '%" + param.sSearch
                        + "%' or pro.Specification like '%" + param.sSearch
                        + "%' or pro.Observation like '%" + param.sSearch
                        + "%')";
                }

                var lstResult = db.SP_IPI_PROTOCOL029_GET_LINES1_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] {"1",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   "",
                                   Helper.GenerateHTMLTextbox(newRecordId, "ARMClauseNo", "", "", false, "", false, "200","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Description", "", "", false, "", false, "1000","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Specification", "", "", false, "", false, "200","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Observation", "", "", false, "", false, "1000","QC"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine1(0);")
                                };
                var data = (from uc in lstResult
                            select new[]
                            {"1",
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                               Convert.ToString(uc.ROW_NO),
                               Helper.GenerateHTMLTextbox(uc.LineId, "ARMClauseNo",Convert.ToString(uc.ARMClauseNo),"", true, "", false, "200","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "Description",Convert.ToString(uc.Description),"", true, "", false, "1000","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "Specification",Convert.ToString(uc.Specification),"", true, "", false, "200","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "Observation",Convert.ToString(uc.Observation),"", true, "", false, "1000","QC"),

                               Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == "QC3" ? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine1','tblProtocolLines1')"):"")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine1(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRO141 objPRO141 = new PRO141();
            // int newRowIndex = 0;
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
                if (refHeaderId > 0)
                {
                    string strARMClauseNo = !string.IsNullOrEmpty(fc["ARMClauseNo" + refLineId]) ? Convert.ToString(fc["ARMClauseNo" + refLineId]).Trim() : "";
                    string strDescription = !string.IsNullOrEmpty(fc["Description" + refLineId]) ? Convert.ToString(fc["Description" + refLineId]).Trim() : "";
                    string strSpecification = !string.IsNullOrEmpty(fc["Specification" + refLineId]) ? Convert.ToString(fc["Specification" + refLineId]).Trim() : "";
                    string strObservation = !string.IsNullOrEmpty(fc["Observation" + refLineId]) ? Convert.ToString(fc["Observation" + refLineId]).Trim() : "";

                    #region Add New Spot Line1
                    if (refLineId > 0)
                    {
                        objPRO141 = db.PRO141.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRO141.EditedBy = objClsLoginInfo.UserName;
                        objPRO141.EditedOn = DateTime.Now;
                    }
                    objPRO141.HeaderId = refHeaderId;
                    objPRO141.ARMClauseNo = strARMClauseNo;
                    objPRO141.Description = strDescription;
                    objPRO141.Specification = strSpecification;
                    objPRO141.Observation = strObservation;


                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = "Data updated successfully";
                    }
                    else
                    {
                        objPRO141.CreatedBy = objClsLoginInfo.UserName;
                        objPRO141.CreatedOn = DateTime.Now;
                        db.PRO141.Add(objPRO141);
                        db.SaveChanges();
                        objResponseMsg.Value = "Data added successfully";
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRO141.LineId;
                    #endregion

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine1(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRO141 objPRO141 = db.PRO141.FirstOrDefault(c => c.LineId == lineId);
                if (objPRO141 != null)
                {
                    db.PRO141.Remove(objPRO141);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #endregion
        #region Linkage View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL140 objPRL140 = new PRL140();
            objPRL140 = db.PRL140.Where(i => i.HeaderId == id).FirstOrDefault();

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;
            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;
            if (objPRL140 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL140.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objQproject.ManufacturingCode;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }

            if (m == 1)
            {
                ViewBag.isEditable = "true";
            }
            else
            { ViewBag.isEditable = "false"; }

            return View(objPRL140);
        }


        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL140 PRL140)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRL140.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL140 objPRL140 = db.PRL140.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    #region Save Data
                    objPRL140.ProtocolNo = PRL140.ProtocolNo != null ? PRL140.ProtocolNo : "";
                    objPRL140.Project = PRL140.Project;
                    objPRL140.JobDescription = PRL140.JobDescription;
                    objPRL140.BU = PRL140.BU;
                    objPRL140.ARMNoRev = PRL140.ARMNoRev;
                    objPRL140.ManufacturingCode = PRL140.ManufacturingCode;

                    objPRL140.PreparedBy = PRL140.PreparedBy;
                    objPRL140.EditedBy = objClsLoginInfo.UserName;
                    objPRL140.EditedOn = DateTime.Now;

                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol saved successfully";
                    objResponseMsg.HeaderId = objPRL140.HeaderId;
                    #endregion
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                }


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL140 objPRL140 = db.PRL140.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL140 != null && string.IsNullOrWhiteSpace(objPRL140.ProtocolNo))
                    {

                        db.PRL140.Remove(objPRL140);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL140.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line1 details
        [HttpPost]
        public ActionResult GetProtocolLines1DataLinkage(JQueryDataTableParamModel param, string headerId)
        {
            try
            {

                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro.ARMClauseNo like '%" + param.sSearch
                        + "%' or pro.Description like '%" + param.sSearch
                        + "%' or pro.Specification like '%" + param.sSearch
                        + "%' or pro.Observation like '%" + param.sSearch
                        + "%')";
                }
                var lstResult = db.SP_IPI_PROTOCOL029_LINKAGE_GET_LINES1_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] {"1",
                                           Convert.ToString(headerId),
                                           Convert.ToString(newRecordId),
                                           "",
                                           Helper.GenerateHTMLTextbox(newRecordId, "ARMClauseNo", "", "", false, "", false, "25","QC"),
                                           Helper.GenerateHTMLTextbox(newRecordId, "Description", "", "", false, "", false, "25","QC"),
                                           Helper.GenerateHTMLTextbox(newRecordId, "Specification", "", "", false, "", false, "25","QC"),
                                           Helper.GenerateHTMLTextbox(newRecordId, "Observation", "", "", false, "", false, "25","QC"),
                                          
                                           Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine1(0);")
                                        };
                var data = (from uc in lstResult
                            select new[]
                            {"1",
                                       Convert.ToString(uc.HeaderId),
                                       Convert.ToString(uc.LineId),
                                       Convert.ToString(uc.ROW_NO),
                                       Helper.GenerateHTMLTextbox(uc.LineId, "ARMClauseNo",Convert.ToString(uc.ARMClauseNo),"", true, "", false, "25","QC"),
                                       Helper.GenerateHTMLTextbox(uc.LineId, "Description",Convert.ToString(uc.Description),"", true, "", false, "25","QC"),
                                       Helper.GenerateHTMLTextbox(uc.LineId, "Specification",Convert.ToString(uc.Specification),"", true, "", false, "25","QC"),
                                       Helper.GenerateHTMLTextbox(uc.LineId, "Observation",Convert.ToString(uc.Observation),"", true, "", false, "25","QC"),
                                      
                                       Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == "QC3" ? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine1Linkage','tblProtocolLines1')"):"")
                                   }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine1Linkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRL141 objPRL141 = new PRL141();
            // int newRowIndex = 0;
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
                if (refHeaderId > 0)
                {
                    string strARMClauseNo = !string.IsNullOrEmpty(fc["ARMClauseNo" + refLineId]) ? Convert.ToString(fc["ARMClauseNo" + refLineId]).Trim() : "";
                    string strDescription = !string.IsNullOrEmpty(fc["Description" + refLineId]) ? Convert.ToString(fc["Description" + refLineId]).Trim() : "";
                    string strSpecification = !string.IsNullOrEmpty(fc["Specification" + refLineId]) ? Convert.ToString(fc["Specification" + refLineId]).Trim() : "";
                    string strObservation = !string.IsNullOrEmpty(fc["Observation" + refLineId]) ? Convert.ToString(fc["Observation" + refLineId]).Trim() : "";
                   

                    #region Add New Spot Line1
                    if (refLineId > 0)
                    {
                        objPRL141 = db.PRL141.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRL141.EditedBy = objClsLoginInfo.UserName;
                        objPRL141.EditedOn = DateTime.Now;
                    }
                    objPRL141.HeaderId = refHeaderId;
                    objPRL141.ARMClauseNo = strARMClauseNo;
                    objPRL141.Description = strDescription;
                    objPRL141.Specification = strSpecification;
                    objPRL141.Observation = strObservation;

                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = "Data updated successfully";
                    }
                    else
                    {
                        objPRL141.CreatedBy = objClsLoginInfo.UserName;
                        objPRL141.CreatedOn = DateTime.Now;
                        db.PRL141.Add(objPRL141);
                        db.SaveChanges();
                        objResponseMsg.Value = "Data added successfully";
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRL141.LineId;
                    #endregion

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine1Linkage(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRL141 objPRL141 = db.PRL141.FirstOrDefault(c => c.LineId == lineId);
                if (objPRL141 != null)
                {
                    db.PRL141.Remove(objPRL141);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #endregion
        public string HTMLActionString(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", string onHrefURL = "", bool isDisable = false)
        {
            string htmlControl = "";
            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? onClickMethod : "";
            string onHref = !string.IsNullOrEmpty(onHrefURL) ? "href='" + onHrefURL + "'" : "";

            if (isDisable)
            {
                htmlControl += "<i id=\"" + inputID.ToString() + "\" name=\"" + inputID + "\" ></i>";
                //htmlControl = "<i id=\"" + inputID + "' name='" + inputID + "' style='cursor: pointer; opacity:0.3; margin-right:10px;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";
            }
            else
            {
                htmlControl += "<i id=\"" + inputID.ToString() + "\" name=\"" + inputID + "\"  style='cursor: pointer; margin-right:10px;' Title=\"" + buttonTooltip + "\" class=\"" + className + "\"  onclick=\"" + onClickEvent + "\" ></i>";
                //htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer; margin-right:10px;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
            }

            if (!string.IsNullOrEmpty(onHref))
            {
                htmlControl = "<a " + onHref + " >" + htmlControl + "</a>";
            }

            return htmlControl;
        }
    }
}
