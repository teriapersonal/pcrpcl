﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;


namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol052Controller : clsBase
    {
        // GET: PROTOCOL/Protocol052
        string ControllerURL = "/PROTOCOL/Protocol052/";
        string Title = "SET-UP  & DIMENSION REPORT FOR";

        #region Details View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO255 objPRO255 = new PRO255();
            if (!id.HasValue)
            {
                try
                {
                    objPRO255.ProtocolNo = string.Empty;
                    objPRO255.CreatedBy = objClsLoginInfo.UserName;
                    objPRO255.CreatedOn = DateTime.Now;

                    #region TOTAL SHELL HEIGHT
                    objPRO255.PRO256.Add(new PRO256
                    {
                        RequiredValue = "AT 0°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO255.PRO256.Add(new PRO256
                    {
                        RequiredValue = "AT 90°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO255.PRO256.Add(new PRO256
                    {
                        RequiredValue = "AT 180°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO255.PRO256.Add(new PRO256
                    {
                        RequiredValue = "AT 270°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion

                    db.PRO255.Add(objPRO255);
                    db.SaveChanges();
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO255.HeaderId;
            }
            else
            {
                objPRO255 = db.PRO255.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            //List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();
            List<string> lstProtocolHeaderName = new List<string> { "PIPE TO PIPE", "PIPE TO FLANGE", "PIPE TO ELBOW", "PIPE TO FORGING", "FORGING TO FLANGE", "ELBOW TO FLANGE" };

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            //ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.ProtocolHeaderName = lstProtocolHeaderName.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO256> lstPRO256 = db.PRO256.Where(x => x.HeaderId == objPRO255.HeaderId).ToList();
            List<PRO257> lstPRO257 = db.PRO257.Where(x => x.HeaderId == objPRO255.HeaderId).ToList();
            List<PRO258> lstPRO258 = db.PRO258.Where(x => x.HeaderId == objPRO255.HeaderId).ToList();
            List<PRO259> lstPRO259 = db.PRO259.Where(x => x.HeaderId == objPRO255.HeaderId).ToList();
            List<PRO259_2> lstPRO259_2 = db.PRO259_2.Where(x => x.HeaderId == objPRO255.HeaderId).ToList();
            List<PRO259_3> lstPRO259_3 = db.PRO259_3.Where(x => x.HeaderId == objPRO255.HeaderId).ToList();
            List<PRO259_4> lstPRO259_4 = db.PRO259_4.Where(x => x.HeaderId == objPRO255.HeaderId).ToList();


            ViewBag.lstPRO256 = lstPRO256;
            ViewBag.lstPRO257 = lstPRO257;
            ViewBag.lstPRO258 = lstPRO258;
            ViewBag.lstPRO259 = lstPRO259;
            ViewBag.lstPRO259_2 = lstPRO259_2;
            ViewBag.lstPRO259_3 = lstPRO259_3;
            ViewBag.lstPRO259_4 = lstPRO259_4;

            #endregion

            return View(objPRO255);
        }

        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO255 PRO255)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO255.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO255 objPRO255 = db.PRO255.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO255.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO255.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data
                        objPRO255.ProtocolNo = PRO255.ProtocolNo;
                        objPRO255.ProtocolHeaderName = PRO255.ProtocolHeaderName;
                        objPRO255.ReqTotalLength = PRO255.ReqTotalLength;
                        objPRO255.ProjectionInCaseOfElbow = PRO255.ProjectionInCaseOfElbow;
                        objPRO255.ReqLengthOfFirstPart = PRO255.ReqLengthOfFirstPart;
                        objPRO255.ReqLengthOfSecondPart = PRO255.ReqLengthOfSecondPart;
                        objPRO255.ReqProjectionInCaseOfElbow = PRO255.ReqProjectionInCaseOfElbow;
                        objPRO255.ActLengthOfFisrtPart = PRO255.ActLengthOfFisrtPart;
                        objPRO255.ActLengthOfSecondPart = PRO255.ActLengthOfSecondPart;
                        objPRO255.ActProjectionInCaseOfElbow = PRO255.ActProjectionInCaseOfElbow;
                        objPRO255.OrientationOfLongSeams = PRO255.OrientationOfLongSeams;
                        objPRO255.AllowableOffset = PRO255.AllowableOffset;
                        objPRO255.StraddlingOfBoltHoles = PRO255.StraddlingOfBoltHoles;
                        objPRO255.TiltingOfFlange = PRO255.TiltingOfFlange;
                        objPRO255.ReqStraddlingOfBoltHoles = PRO255.ReqStraddlingOfBoltHoles;
                        objPRO255.ReqTiltingOfFlange = PRO255.ReqTiltingOfFlange;
                        objPRO255.ActStraddlingOfBoltHoles = PRO255.ActStraddlingOfBoltHoles;
                        objPRO255.ActTiltingOfFlange = PRO255.ActTiltingOfFlange;
                        objPRO255.ReqRootGap = PRO255.ReqRootGap;
                        objPRO255.ReqRootFace = PRO255.ReqRootFace;
                        objPRO255.ReqInsideWepAngle = PRO255.ReqInsideWepAngle;
                        objPRO255.ReqOutsideWepAngle = PRO255.ReqOutsideWepAngle;
                        objPRO255.ActMaxRootGap = PRO255.ActMaxRootGap;
                        objPRO255.ActMaxRootFace = PRO255.ActMaxRootFace;
                        objPRO255.ActMaxInsideWepAngle = PRO255.ActMaxInsideWepAngle;
                        objPRO255.ActMaxOutsideWepAngle = PRO255.ActMaxOutsideWepAngle;
                        objPRO255.ActMinRootGap = PRO255.ActMinRootGap;
                        objPRO255.ActMinRootFace = PRO255.ActMinRootFace;
                        objPRO255.ActMinInsideWepAngle = PRO255.ActMinInsideWepAngle;
                        objPRO255.ActMinOutsideWepAngle = PRO255.ActMinOutsideWepAngle;
                        objPRO255.AlignmentReadings = PRO255.AlignmentReadings;
                        objPRO255.OutByAlignmentReadings = PRO255.OutByAlignmentReadings;
                        objPRO255.OutByAlignmentReadingsAt0 = PRO255.OutByAlignmentReadingsAt0;
                        objPRO255.OutByAlignmentReadingsAt90 = PRO255.OutByAlignmentReadingsAt90;
                        objPRO255.OutByAlignmentReadingsAt180 = PRO255.OutByAlignmentReadingsAt180;
                        objPRO255.OutByAlignmentReadingsAt270 = PRO255.OutByAlignmentReadingsAt270;
                        objPRO255.CheckPoint1 = PRO255.CheckPoint1;
                        objPRO255.CheckPoint2 = PRO255.CheckPoint2;
                        objPRO255.CheckPoint3 = PRO255.CheckPoint3;
                        objPRO255.CheckPoint4 = PRO255.CheckPoint4;
                        objPRO255.CheckPoint5 = PRO255.CheckPoint5;
                        objPRO255.CheckPoint6 = PRO255.CheckPoint6;
                        objPRO255.CheckPoint7 = PRO255.CheckPoint7;
                        objPRO255.CheckPoint9 = PRO255.CheckPoint9;
                        objPRO255.CheckPoint9_2 = PRO255.CheckPoint9_2;
                        objPRO255.CheckPoint10 = PRO255.CheckPoint10;
                        objPRO255.CheckPoint10_2 = PRO255.CheckPoint10_2;
                        objPRO255.CheckPoint10_3 = PRO255.CheckPoint10_3;
                        objPRO255.CheckPoint11 = PRO255.CheckPoint11;
                        objPRO255.QCRemarks = PRO255.QCRemarks;
                        objPRO255.Result = PRO255.Result;

                        objPRO255.ReqWidth = PRO255.ReqWidth;
                        objPRO255.ReqDepth = PRO255.ReqDepth;

                        objPRO255.Note1 = PRO255.Note1;
                        objPRO255.Note2 = PRO255.Note2;
                        objPRO255.Note3 = PRO255.Note3;

                        objPRO255.IdentificationOfCleats = PRO255.IdentificationOfCleats;

                        objPRO255.SpotNoOffset = PRO255.SpotNoOffset;
                        objPRO255.SpotNoClad = PRO255.SpotNoClad;

                        objPRO255.EditedBy = objClsLoginInfo.UserName;
                        objPRO255.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO255.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO255 objPRO255 = db.PRO255.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO255 != null && string.IsNullOrWhiteSpace(objPRO255.ProtocolNo))
                    {
                        db.PRO255.Remove(objPRO255);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO255.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO256> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO256> lstAddPRO256 = new List<PRO256>();
                List<PRO256> lstDeletePRO256 = new List<PRO256>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO256 obj = db.PRO256.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO256();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.RequiredValue = item.RequiredValue;
                        obj.ActualValue = item.ActualValue;
                        if (isAdded)
                        {
                            lstAddPRO256.Add(obj);
                        }
                    }
                    if (lstAddPRO256.Count > 0)
                    {
                        db.PRO256.AddRange(lstAddPRO256);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO256 = db.PRO256.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO256.Count > 0)
                    {
                        db.PRO256.RemoveRange(lstDeletePRO256);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO256 = db.PRO256.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO256.Count > 0)
                    {
                        db.PRO256.RemoveRange(lstDeletePRO256);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2(List<PRO257> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO257> lstAddPRO257 = new List<PRO257>();
                List<PRO257> lstDeletePRO257 = new List<PRO257>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO257 obj = db.PRO257.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO257();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SeamNo = item.SeamNo;
                        obj.ReqOrien = item.ReqOrien;
                        obj.ReqArcLength = item.ReqArcLength;
                        obj.ActArcLength = item.ActArcLength;
                        if (isAdded)
                        {
                            lstAddPRO257.Add(obj);
                        }
                    }
                    if (lstAddPRO257.Count > 0)
                    {
                        db.PRO257.AddRange(lstAddPRO257);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO257 = db.PRO257.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO257.Count > 0)
                    {
                        db.PRO257.RemoveRange(lstDeletePRO257);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO257 = db.PRO257.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO257.Count > 0)
                    {
                        db.PRO257.RemoveRange(lstDeletePRO257);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3(List<PRO258> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO258> lstAddPRO258 = new List<PRO258>();
                List<PRO258> lstDeletePRO258 = new List<PRO258>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO258 obj = db.PRO258.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO258();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SpotNo = item.SpotNo;
                        obj.OffSet = item.OffSet;
                        if (isAdded)
                        {
                            lstAddPRO258.Add(obj);
                        }
                    }
                    if (lstAddPRO258.Count > 0)
                    {
                        db.PRO258.AddRange(lstAddPRO258);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO258 = db.PRO258.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO258.Count > 0)
                    {
                        db.PRO258.RemoveRange(lstDeletePRO258);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO258 = db.PRO258.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO258.Count > 0)
                    {
                        db.PRO258.RemoveRange(lstDeletePRO258);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line4 details
        [HttpPost]
        public JsonResult SaveProtocolLine4(List<PRO259> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO259> lstAddPRO259 = new List<PRO259>();
                List<PRO259> lstDeletePRO259 = new List<PRO259>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO259 obj = db.PRO259.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            if (!string.IsNullOrWhiteSpace(item.Location))
                            {
                                obj = new PRO259();
                                obj.HeaderId = item.HeaderId;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                isAdded = true;
                            }
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        if (!string.IsNullOrWhiteSpace(item.Location))
                        {
                            obj.Location = item.Location;
                            obj.At0 = item.At0;
                            obj.At90 = item.At90;
                            obj.At180 = item.At180;
                            obj.At270 = item.At270;
                        }

                        if (isAdded)
                        {
                            lstAddPRO259.Add(obj);
                        }
                    }
                    if (lstAddPRO259.Count > 0)
                    {
                        db.PRO259.AddRange(lstAddPRO259);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO259 = db.PRO259.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO259.Count > 0)
                    {
                        db.PRO259.RemoveRange(lstDeletePRO259);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO259 = db.PRO259.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO259.Count > 0)
                    {
                        db.PRO259.RemoveRange(lstDeletePRO259);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line5 details
        [HttpPost]
        public JsonResult SaveProtocolLine5(List<PRO259_2> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO259_2> lstAddPRO259_2 = new List<PRO259_2>();
                List<PRO259_2> lstDeletePRO259_2 = new List<PRO259_2>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO259_2 obj = db.PRO259_2.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO259_2();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.IdentificationMarking = item.IdentificationMarking;
                        if (isAdded)
                        {
                            lstAddPRO259_2.Add(obj);
                        }
                    }
                    if (lstAddPRO259_2.Count > 0)
                    {
                        db.PRO259_2.AddRange(lstAddPRO259_2);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO259_2 = db.PRO259_2.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO259_2.Count > 0)
                    {
                        db.PRO259_2.RemoveRange(lstDeletePRO259_2);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO259_2 = db.PRO259_2.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO259_2.Count > 0)
                    {
                        db.PRO259_2.RemoveRange(lstDeletePRO259_2);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line6 details
        [HttpPost]
        public JsonResult SaveProtocolLine6(List<PRO259_3> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO259_3> lstAddPRO259_3 = new List<PRO259_3>();
                List<PRO259_3> lstDeletePRO259_3 = new List<PRO259_3>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO259_3 obj = db.PRO259_3.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO259_3();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SpotNo = item.SpotNo;
                        obj.ActWidth = item.ActWidth;
                        obj.ActDepth = item.ActDepth;
                        if (isAdded)
                        {
                            lstAddPRO259_3.Add(obj);
                        }
                    }
                    if (lstAddPRO259_3.Count > 0)
                    {
                        db.PRO259_3.AddRange(lstAddPRO259_3);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO259_3 = db.PRO259_3.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO259_3.Count > 0)
                    {
                        db.PRO259_3.RemoveRange(lstDeletePRO259_3);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO259_3 = db.PRO259_3.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO259_3.Count > 0)
                    {
                        db.PRO259_3.RemoveRange(lstDeletePRO259_3);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line7 details
        [HttpPost]
        public JsonResult SaveProtocolLine7(List<PRO259_4> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO259_4> lstAddPRO259_4 = new List<PRO259_4>();
                List<PRO259_4> lstDeletePRO259_4 = new List<PRO259_4>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO259_4 obj = db.PRO259_4.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO259_4();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.CleatNo = item.CleatNo;
                        obj.SizeWelded = item.SizeWelded;
                        obj.Location = item.Location;
                        obj.OutsideOrInside = item.OutsideOrInside;
                        obj.DistanceFromSeam = item.DistanceFromSeam;
                        obj.ClockWise = item.ClockWise;
                        if (isAdded)
                        {
                            lstAddPRO259_4.Add(obj);
                        }
                    }
                    if (lstAddPRO259_4.Count > 0)
                    {
                        db.PRO259_4.AddRange(lstAddPRO259_4);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO259_4 = db.PRO259_4.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO259_4.Count > 0)
                    {
                        db.PRO259_4.RemoveRange(lstDeletePRO259_4);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO259_4 = db.PRO259_4.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO259_4.Count > 0)
                    {
                        db.PRO259_4.RemoveRange(lstDeletePRO259_4);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

        #region Linkage View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL255 objPRL255 = new PRL255();
            objPRL255 = db.PRL255.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL255 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL255.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;

                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            //List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();
            List<string> lstProtocolHeaderName = new List<string> { "PIPE TO PIPE", "PIPE TO FLANGE", "PIPE TO ELBOW", "PIPE TO FORGING", "FORGING TO FLANGE", "ELBOW TO FLANGE" };

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            //ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.ProtocolHeaderName = lstProtocolHeaderName.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL256> lstPRL256 = db.PRL256.Where(x => x.HeaderId == objPRL255.HeaderId).ToList();
            List<PRL257> lstPRL257 = db.PRL257.Where(x => x.HeaderId == objPRL255.HeaderId).ToList();
            List<PRL258> lstPRL258 = db.PRL258.Where(x => x.HeaderId == objPRL255.HeaderId).ToList();
            List<PRL259> lstPRL259 = db.PRL259.Where(x => x.HeaderId == objPRL255.HeaderId).ToList();
            List<PRL259_2> lstPRL259_2 = db.PRL259_2.Where(x => x.HeaderId == objPRL255.HeaderId).ToList();
            List<PRL259_3> lstPRL259_3 = db.PRL259_3.Where(x => x.HeaderId == objPRL255.HeaderId).ToList();
            List<PRL259_4> lstPRL259_4 = db.PRL259_4.Where(x => x.HeaderId == objPRL255.HeaderId).ToList();


            ViewBag.lstPRL256 = lstPRL256;
            ViewBag.lstPRL257 = lstPRL257;
            ViewBag.lstPRL258 = lstPRL258;
            ViewBag.lstPRL259 = lstPRL259;
            ViewBag.lstPRL259_2 = lstPRL259_2;
            ViewBag.lstPRL259_3 = lstPRL259_3;
            ViewBag.lstPRL259_4 = lstPRL259_4;
            #endregion
            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.DrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL255.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL255.ActFilledBy) && (objPRL255.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL255.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL255);
        }

        // USED in MOBILE APPLICATION
        [HttpPost]
        public ActionResult LinkageMobile(int? id, int? m, string APIRequestFrom = "", string UserName = "")
        {
            string ProjectDesc = "";
            string ManufacturingCode = "";
            string AccessRole = "";
            bool isEditable = false;
            var DrawingNo = new List<string>();
            bool isQCRemarkEditable = false;

            PRL255 objPRL255 = new PRL255();
            db.Configuration.ProxyCreationEnabled = false;
            objPRL255 = db.PRL255.Where(i => i.HeaderId == id).FirstOrDefault();
            db.Entry(objPRL255).State = System.Data.Entity.EntityState.Detached;
            NDEModels objNDEModels = new NDEModels();
            if (objPRL255 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL255.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
            AccessRole = objUserRoleAccessDetails.UserDesignation;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            //List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();
            List<string> lstProtocolHeaderName = new List<string> { "PIPE TO PIPE", "PIPE TO FLANGE", "PIPE TO ELBOW", "PIPE TO FORGING", "FORGING TO FLANGE", "ELBOW TO FLANGE" };

            var YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            //ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var ProtocolHeaderName = lstProtocolHeaderName.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL256> lstPRL256 = db.PRL256.Where(x => x.HeaderId == objPRL255.HeaderId).ToList();
            List<PRL257> lstPRL257 = db.PRL257.Where(x => x.HeaderId == objPRL255.HeaderId).ToList();
            List<PRL258> lstPRL258 = db.PRL258.Where(x => x.HeaderId == objPRL255.HeaderId).ToList();
            List<PRL259> lstPRL259 = db.PRL259.Where(x => x.HeaderId == objPRL255.HeaderId).ToList();
            List<PRL259_2> lstPRL259_2 = db.PRL259_2.Where(x => x.HeaderId == objPRL255.HeaderId).ToList();
            List<PRL259_3> lstPRL259_3 = db.PRL259_3.Where(x => x.HeaderId == objPRL255.HeaderId).ToList();
            List<PRL259_4> lstPRL259_4 = db.PRL259_4.Where(x => x.HeaderId == objPRL255.HeaderId).ToList();

            #endregion
            try
            {
                isEditable = false;
                isQCRemarkEditable = false;
                if (m == 1)
                {
                    isEditable = true;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL255.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL255.ActFilledBy) && (objPRL255.RequestNo.HasValue))
                        {
                            isEditable = false;
                            isQCRemarkEditable = true;
                        }
                        else
                        {
                            DrawingNo = (new GeneralController()).GetPLMDrawingNumber(objPRL255.Project).ToList();
                        }
                    }
                }
                else
                { isEditable = false; }

                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    ProjectDesc = ProjectDesc,
                    ManufacturingCode = ManufacturingCode,
                    isEditable = isEditable,
                    isQCRemarkEditable = isQCRemarkEditable,
                    DrawingNo = DrawingNo,
                    OutsideInside = OutsideInside,
                    ProtocolHeaderName = ProtocolHeaderName,
                    AccessRole = AccessRole,
                    YesNoEnum = YesNoEnum,
                    YesNAEnum = YesNAEnum,
                    
                    objPRL255 = objPRL255,

                    lstPRL256 = lstPRL256,
                    lstPRL257 = lstPRL257,
                    lstPRL258 = lstPRL258,
                    lstPRL259 = lstPRL259,
                    lstPRL259_2 = lstPRL259_2,
                    lstPRL259_3 = lstPRL259_3,
                    lstPRL259_4 = lstPRL259_4,
                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();

                return Json(new
                {
                    res = "0",
                    msg = ex.Message,
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL255 PRL255, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = PRL255.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL255 objPRL255 = db.PRL255.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL255.DrawingRevisionNo = PRL255.DrawingRevisionNo;
                    objPRL255.DrawingNo = PRL255.DrawingNo;
                    objPRL255.DCRNo = PRL255.DCRNo;
                    objPRL255.ProtocolNo = PRL255.ProtocolNo != null ? PRL255.ProtocolNo : "";
                    objPRL255.ProtocolHeaderName = PRL255.ProtocolHeaderName;
                    objPRL255.ReqTotalLength = PRL255.ReqTotalLength;
                    objPRL255.ProjectionInCaseOfElbow = PRL255.ProjectionInCaseOfElbow;
                    objPRL255.ReqLengthOfFirstPart = PRL255.ReqLengthOfFirstPart;
                    objPRL255.ReqLengthOfSecondPart = PRL255.ReqLengthOfSecondPart;
                    objPRL255.ReqProjectionInCaseOfElbow = PRL255.ReqProjectionInCaseOfElbow;
                    objPRL255.ActLengthOfFisrtPart = PRL255.ActLengthOfFisrtPart;
                    objPRL255.ActLengthOfSecondPart = PRL255.ActLengthOfSecondPart;
                    objPRL255.ActProjectionInCaseOfElbow = PRL255.ActProjectionInCaseOfElbow;
                    objPRL255.OrientationOfLongSeams = PRL255.OrientationOfLongSeams;
                    objPRL255.AllowableOffset = PRL255.AllowableOffset;
                    objPRL255.StraddlingOfBoltHoles = PRL255.StraddlingOfBoltHoles;
                    objPRL255.TiltingOfFlange = PRL255.TiltingOfFlange;
                    objPRL255.ReqStraddlingOfBoltHoles = PRL255.ReqStraddlingOfBoltHoles;
                    objPRL255.ReqTiltingOfFlange = PRL255.ReqTiltingOfFlange;
                    objPRL255.ActStraddlingOfBoltHoles = PRL255.ActStraddlingOfBoltHoles;
                    objPRL255.ActTiltingOfFlange = PRL255.ActTiltingOfFlange;
                    objPRL255.ReqRootGap = PRL255.ReqRootGap;
                    objPRL255.ReqRootFace = PRL255.ReqRootFace;
                    objPRL255.ReqInsideWepAngle = PRL255.ReqInsideWepAngle;
                    objPRL255.ReqOutsideWepAngle = PRL255.ReqOutsideWepAngle;
                    objPRL255.ActMaxRootGap = PRL255.ActMaxRootGap;
                    objPRL255.ActMaxRootFace = PRL255.ActMaxRootFace;
                    objPRL255.ActMaxInsideWepAngle = PRL255.ActMaxInsideWepAngle;
                    objPRL255.ActMaxOutsideWepAngle = PRL255.ActMaxOutsideWepAngle;
                    objPRL255.ActMinRootGap = PRL255.ActMinRootGap;
                    objPRL255.ActMinRootFace = PRL255.ActMinRootFace;
                    objPRL255.ActMinInsideWepAngle = PRL255.ActMinInsideWepAngle;
                    objPRL255.ActMinOutsideWepAngle = PRL255.ActMinOutsideWepAngle;
                    objPRL255.AlignmentReadings = PRL255.AlignmentReadings;
                    objPRL255.OutByAlignmentReadings = PRL255.OutByAlignmentReadings;
                    objPRL255.OutByAlignmentReadingsAt0 = PRL255.OutByAlignmentReadingsAt0;
                    objPRL255.OutByAlignmentReadingsAt90 = PRL255.OutByAlignmentReadingsAt90;
                    objPRL255.OutByAlignmentReadingsAt180 = PRL255.OutByAlignmentReadingsAt180;
                    objPRL255.OutByAlignmentReadingsAt270 = PRL255.OutByAlignmentReadingsAt270;
                    objPRL255.CheckPoint1 = PRL255.CheckPoint1;
                    objPRL255.CheckPoint2 = PRL255.CheckPoint2;
                    objPRL255.CheckPoint3 = PRL255.CheckPoint3;
                    objPRL255.CheckPoint4 = PRL255.CheckPoint4;
                    objPRL255.CheckPoint5 = PRL255.CheckPoint5;
                    objPRL255.CheckPoint6 = PRL255.CheckPoint6;
                    objPRL255.CheckPoint7 = PRL255.CheckPoint7;
                    objPRL255.CheckPoint9 = PRL255.CheckPoint9;
                    objPRL255.CheckPoint9_2 = PRL255.CheckPoint9_2;
                    objPRL255.CheckPoint10 = PRL255.CheckPoint10;
                    objPRL255.CheckPoint10_2 = PRL255.CheckPoint10_2;
                    objPRL255.CheckPoint10_3 = PRL255.CheckPoint10_3;
                    objPRL255.CheckPoint11 = PRL255.CheckPoint11;
                    objPRL255.QCRemarks = PRL255.QCRemarks;
                    objPRL255.Result = PRL255.Result;

                    objPRL255.ReqWidth = PRL255.ReqWidth;
                    objPRL255.ReqDepth = PRL255.ReqDepth;

                    objPRL255.Note1 = PRL255.Note1;
                    objPRL255.Note2 = PRL255.Note2;
                    objPRL255.Note3 = PRL255.Note3;

                    objPRL255.IdentificationOfCleats = PRL255.IdentificationOfCleats;

                    objPRL255.SpotNoOffset = PRL255.SpotNoOffset;
                    objPRL255.SpotNoClad = PRL255.SpotNoClad;

                    objPRL255.EditedBy = UserName;
                    objPRL255.EditedOn = DateTime.Now;

                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL255.ActFilledBy = UserName;
                            objPRL255.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL255.ReqFilledBy = UserName;
                            objPRL255.ReqFilledOn = DateTime.Now;
                        }
                    }
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL255.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL255 objPRL255 = db.PRL255.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL255 != null && string.IsNullOrWhiteSpace(objPRL255.ProtocolNo))
                    {
                        db.PRL255.Remove(objPRL255);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL255.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL256> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
            try
            {
                List<PRL256> lstAddPRL256 = new List<PRL256>();
                List<PRL256> lstDeletePRL256 = new List<PRL256>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL256 obj = db.PRL256.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL256();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.RequiredValue = item.RequiredValue;
                        obj.ActualValue = item.ActualValue;
                        if (isAdded)
                        {
                            lstAddPRL256.Add(obj);
                        }
                    }

                    if (lstAddPRL256.Count > 0)
                    {
                        db.PRL256.AddRange(lstAddPRL256);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL256 = db.PRL256.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL256 = db.PRL256.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL256.Count > 0)
                {
                    db.PRL256.RemoveRange(lstDeletePRL256);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine2Linkage(List<PRL257> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL257> lstAddPRL257 = new List<PRL257>();
                List<PRL257> lstDeletePRL257 = new List<PRL257>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL257 obj = db.PRL257.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL257();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SeamNo = item.SeamNo;
                        obj.ReqOrien = item.ReqOrien;
                        obj.ReqArcLength = item.ReqArcLength;
                        obj.ActArcLength = item.ActArcLength;
                        if (isAdded)
                        {
                            lstAddPRL257.Add(obj);
                        }
                    }

                    if (lstAddPRL257.Count > 0)
                    {
                        db.PRL257.AddRange(lstAddPRL257);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL257 = db.PRL257.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL257 = db.PRL257.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL257.Count > 0)
                {
                    db.PRL257.RemoveRange(lstDeletePRL257);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line3 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine3Linkage(List<PRL258> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL258> lstAddPRL258 = new List<PRL258>();
                List<PRL258> lstDeletePRL258 = new List<PRL258>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL258 obj = db.PRL258.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL258();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SpotNo = item.SpotNo;
                        obj.OffSet = item.OffSet;
                        if (isAdded)
                        {
                            lstAddPRL258.Add(obj);
                        }
                    }

                    if (lstAddPRL258.Count > 0)
                    {
                        db.PRL258.AddRange(lstAddPRL258);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL258 = db.PRL258.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL258 = db.PRL258.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL258.Count > 0)
                {
                    db.PRL258.RemoveRange(lstDeletePRL258);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line4 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine4Linkage(List<PRL259> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL259> lstAddPRL259 = new List<PRL259>();
                List<PRL259> lstDeletePRL259 = new List<PRL259>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL259 obj = db.PRL259.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            if (!string.IsNullOrWhiteSpace(item.Location))
                            {
                                obj = new PRL259();
                                obj.HeaderId = item.HeaderId;
                                obj.CreatedBy = UserName;
                                obj.CreatedOn = DateTime.Now;
                                isAdded = true;
                            }
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        if (!string.IsNullOrWhiteSpace(item.Location))
                        {
                            obj.Location = item.Location;
                            obj.At0 = item.At0;
                            obj.At90 = item.At90;
                            obj.At180 = item.At180;
                            obj.At270 = item.At270;
                        }

                        if (isAdded)
                        {
                            lstAddPRL259.Add(obj);
                        }
                    }
                    if (lstAddPRL259.Count > 0)
                    {
                        db.PRL259.AddRange(lstAddPRL259);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL259 = db.PRL259.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL259.Count > 0)
                    {
                        db.PRL259.RemoveRange(lstDeletePRL259);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL259 = db.PRL259.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL259.Count > 0)
                    {
                        db.PRL259.RemoveRange(lstDeletePRL259);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line5 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine5Linkage(List<PRL259_2> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL259_2> lstAddPRL259_2 = new List<PRL259_2>();
                List<PRL259_2> lstDeletePRL259_2 = new List<PRL259_2>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL259_2 obj = db.PRL259_2.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL259_2();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.IdentificationMarking = item.IdentificationMarking;
                        if (isAdded)
                        {
                            lstAddPRL259_2.Add(obj);
                        }
                    }

                    if (lstAddPRL259_2.Count > 0)
                    {
                        db.PRL259_2.AddRange(lstAddPRL259_2);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL259_2 = db.PRL259_2.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL259_2 = db.PRL259_2.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL259_2.Count > 0)
                {
                    db.PRL259_2.RemoveRange(lstDeletePRL259_2);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line6 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine6Linkage(List<PRL259_3> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL259_3> lstAddPRL259_3 = new List<PRL259_3>();
                List<PRL259_3> lstDeletePRL259_3 = new List<PRL259_3>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL259_3 obj = db.PRL259_3.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL259_3();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SpotNo = item.SpotNo;
                        obj.ActWidth = item.ActWidth;
                        obj.ActDepth = item.ActDepth;
                        if (isAdded)
                        {
                            lstAddPRL259_3.Add(obj);
                        }
                    }

                    if (lstAddPRL259_3.Count > 0)
                    {
                        db.PRL259_3.AddRange(lstAddPRL259_3);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL259_3 = db.PRL259_3.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL259_3 = db.PRL259_3.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL259_3.Count > 0)
                {
                    db.PRL259_3.RemoveRange(lstDeletePRL259_3);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line7 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine7Linkage(List<PRL259_4> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL259_4> lstAddPRL259_4 = new List<PRL259_4>();
                List<PRL259_4> lstDeletePRL259_4 = new List<PRL259_4>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL259_4 obj = db.PRL259_4.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL259_4();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.CleatNo = item.CleatNo;
                        obj.SizeWelded = item.SizeWelded;
                        obj.Location = item.Location;
                        obj.OutsideOrInside = item.OutsideOrInside;
                        obj.DistanceFromSeam = item.DistanceFromSeam;
                        obj.ClockWise = item.ClockWise;
                        if (isAdded)
                        {
                            lstAddPRL259_4.Add(obj);
                        }
                    }

                    if (lstAddPRL259_4.Count > 0)
                    {
                        db.PRL259_4.AddRange(lstAddPRL259_4);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL259_4 = db.PRL259_4.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL259_4 = db.PRL259_4.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL259_4.Count > 0)
                {
                    db.PRL259_4.RemoveRange(lstDeletePRL259_4);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

    }
}