﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol030Controller : clsBase
    {
        string ControllerURL = "/PROTOCOL/Protocol030/";
        string Title = "VISUAL & DIMENSION INSPECTION REPORT FOR OVERLAY ON SHELL/D'END";
       
        // GET: PROTOCOL/Protocol030
        #region Protocol Details Page
        [SessionExpireFilter] //, UserPermissions, AllowAnonymous]
       
            public ActionResult Details(int? id,int? m)
        {
            PRO145 objPRO145 = new PRO145();
            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            List<string> lstYesNoNA = clsImplementationEnum.getYesNoNA().ToList();
            ViewBag.YesNoNAEnum = lstYesNoNA.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            if (!id.HasValue)
            {
                objPRO145.ProtocolNo = string.Empty;
                objPRO145.CreatedBy = objClsLoginInfo.UserName;
                objPRO145.CreatedOn = DateTime.Now;
                db.PRO145.Add(objPRO145);
                db.SaveChanges();
                ViewBag.HeaderId = objPRO145.HeaderId;
            }
            else
            {
                objPRO145 = db.PRO145.Where(x => x.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }

            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            return View(objPRO145);
        }
        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO145 PRO145)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO145.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO145 objPRO145 = db.PRO145.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var lstPRO145 = db.PRO075.Where(o => o.HeaderId != refHeaderId && o.ProtocolNo.Trim() == PRO145.ProtocolNo.Trim()).ToList();
                    if (lstPRO145.Count == 0)
                    {
                        #region Save Data

                        objPRO145.ProtocolNo = PRO145.ProtocolNo;
                        objPRO145.TopRequiredNom1 = PRO145.TopRequiredNom1;
                        objPRO145.TopRequiredMin1 = PRO145.TopRequiredMin1;
                        objPRO145.TopActual1 = PRO145.TopActual1;
                        objPRO145.TopRequiredNom2 = PRO145.TopRequiredNom2;
                        objPRO145.TopRequiredMin2 = PRO145.TopRequiredMin2;
                        objPRO145.TopActual7 = PRO145.TopActual7;
                        objPRO145.TopActual2 = PRO145.TopActual2;
                        objPRO145.TopActual8 = PRO145.TopActual8;
                        objPRO145.TopActual3 = PRO145.TopActual3;
                        objPRO145.TopActual9 = PRO145.TopActual9;
                        objPRO145.TopActual4 = PRO145.TopActual4;
                        objPRO145.TopActual10 = PRO145.TopActual10;
                        objPRO145.TopActual5 = PRO145.TopActual5;
                        objPRO145.TopActual11 = PRO145.TopActual11;
                        objPRO145.TopActual6 = PRO145.TopActual6;
                        objPRO145.TopActual12 = PRO145.TopActual12;
                        objPRO145.BottomRequiredNom1 = PRO145.BottomRequiredNom1;
                        objPRO145.BottomRequiredMin1 = PRO145.BottomRequiredMin1;
                        objPRO145.BottomActual1 = PRO145.BottomActual1;
                        objPRO145.BottomRequiredNom2 = PRO145.BottomRequiredNom2;
                        objPRO145.BottomRequiredMin2 = PRO145.BottomRequiredMin2;
                        objPRO145.BottomActual7 = PRO145.BottomActual7;
                        objPRO145.BottomActual2 = PRO145.BottomActual2;
                        objPRO145.BottomActual8 = PRO145.BottomActual8;
                        objPRO145.BottomActual3 = PRO145.BottomActual3;
                        objPRO145.BottomActual9 = PRO145.BottomActual9;
                        objPRO145.BottomActual4 = PRO145.BottomActual4;
                        objPRO145.BottomActual10 = PRO145.BottomActual10;
                        objPRO145.BottomActual5 = PRO145.BottomActual5;
                        objPRO145.BottomActual11 = PRO145.BottomActual11;
                        objPRO145.BottomActual6 = PRO145.BottomActual6;
                        objPRO145.BottomActual12 = PRO145.BottomActual12;
                        objPRO145.Remark21 = PRO145.Remark21;
                        objPRO145.Remark22 = PRO145.Remark22;

                        objPRO145.EditedBy = objClsLoginInfo.UserName;
                        objPRO145.EditedOn = DateTime.Now;
                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.ProtocolSaved;
                        objResponseMsg.HeaderId = objPRO145.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.ProtocolExists;
                    }
                }
            }

            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId > 0)
                {
                    PRO145 objPRO145 = db.PRO145.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO145 != null && string.IsNullOrWhiteSpace(objPRO145.ProtocolNo))
                    {
                        db.PRO145.Remove(objPRO145);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_INSPECTION_REPORT_FOR_OVERLAY_ON_SHELL_DEND.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO145.HeaderId;
                    }

                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Linkage Details Page
        [SessionExpireFilter] //, UserPermissions, AllowAnonymous]
        public ActionResult Linkage(int? id, int? m)
        {//PRL075
            PRL145 objPRL145 = new PRL145();

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            List<string> lstYesNoNA = clsImplementationEnum.getYesNoNA().ToList();
            ViewBag.YesNoNAEnum = lstYesNoNA.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();


            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            objPRL145 = db.PRL145.Where(x => x.HeaderId == id).FirstOrDefault();

            if (objPRL145 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL145.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objQproject.ManufacturingCode;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }
            if (m == 1)
            {
                ViewBag.isEditable = "true";
            }
            else
            { ViewBag.isEditable = "false"; }

            return View(objPRL145);
        }

        public ActionResult SaveProtocolHeaderDataLinkage(PRL145 PRL145)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRL145.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL145 objPRL145 = db.PRL145.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data

                    objPRL145.SeamNo= PRL145.SeamNo;
                    objPRL145.EquipmentNo = PRL145.EquipmentNo;
                    objPRL145.InspectionAgency = PRL145.InspectionAgency;
                    objPRL145.DrawingNo = PRL145.DrawingNo;
                    objPRL145.DrawingRevisionNo = PRL145.DrawingRevisionNo;
                    objPRL145.ManufacturingCode = PRL145.ManufacturingCode;
                    objPRL145.DCRNo = PRL145.DCRNo;
                    objPRL145.ProtocolNo = PRL145.ProtocolNo != null ? PRL145.ProtocolNo : "";
                    objPRL145.TopRequiredNom1 = PRL145.TopRequiredNom1;
                    objPRL145.TopRequiredMin1 = PRL145.TopRequiredMin1;
                    objPRL145.TopActual1 = PRL145.TopActual1;
                    objPRL145.TopRequiredNom2 = PRL145.TopRequiredNom2;
                    objPRL145.TopRequiredMin2 = PRL145.TopRequiredMin2;
                    objPRL145.TopActual7 = PRL145.TopActual7;
                    objPRL145.TopActual2 = PRL145.TopActual2;
                    objPRL145.TopActual8 = PRL145.TopActual8;
                    objPRL145.TopActual3 = PRL145.TopActual3;
                    objPRL145.TopActual9 = PRL145.TopActual9;
                    objPRL145.TopActual4 = PRL145.TopActual4;
                    objPRL145.TopActual10 = PRL145.TopActual10;
                    objPRL145.TopActual5 = PRL145.TopActual5;
                    objPRL145.TopActual11 = PRL145.TopActual11;
                    objPRL145.TopActual6 = PRL145.TopActual6;
                    objPRL145.TopActual12 = PRL145.TopActual12;
                    objPRL145.BottomRequiredNom1 = PRL145.BottomRequiredNom1;
                    objPRL145.BottomRequiredMin1 = PRL145.BottomRequiredMin1;
                    objPRL145.BottomActual1 = PRL145.BottomActual1;
                    objPRL145.BottomRequiredNom2 = PRL145.BottomRequiredNom2;
                    objPRL145.BottomRequiredMin2 = PRL145.BottomRequiredMin2;
                    objPRL145.BottomActual7 = PRL145.BottomActual7;
                    objPRL145.BottomActual2 = PRL145.BottomActual2;
                    objPRL145.BottomActual8 = PRL145.BottomActual8;
                    objPRL145.BottomActual3 = PRL145.BottomActual3;
                    objPRL145.BottomActual9 = PRL145.BottomActual9;
                    objPRL145.BottomActual4 = PRL145.BottomActual4;
                    objPRL145.BottomActual10 = PRL145.BottomActual10;
                    objPRL145.BottomActual5 = PRL145.BottomActual5;
                    objPRL145.BottomActual11 = PRL145.BottomActual11;
                    objPRL145.BottomActual6 = PRL145.BottomActual6;
                    objPRL145.BottomActual12 = PRL145.BottomActual12;
                    objPRL145.Remark21 = PRL145.Remark21;
                    objPRL145.Remark22 = PRL145.Remark22;
                    objPRL145.EditedBy = objClsLoginInfo.UserName;
                    objPRL145.EditedOn = DateTime.Now;
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.LinkageSaved;
                    objResponseMsg.HeaderId = objPRL145.HeaderId;

                    #endregion

                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}