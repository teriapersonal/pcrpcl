﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol042Controller : clsBase
    {
        // GET: PROTOCOL/Protocol042
        string ControllerURL = "/PROTOCOL/Protocol042/";
        string Title = "SET-UP & DIMENSION REPORT FOR NOZZLE";
        //IEMQS.DrawingReference.hedProjectDocListWebServiceClient drawref = new IEMQS.DrawingReference.hedProjectDocListWebServiceClient();

        #region Details View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO205 objPRO205 = new PRO205();
            if (!id.HasValue)
            {
                try
                {
                    objPRO205.ProtocolNo = string.Empty;
                    objPRO205.CreatedBy = objClsLoginInfo.UserName;
                    objPRO205.CreatedOn = DateTime.Now;

                    #region OUT OF ROUNDNESS
                    objPRO205.PRO207.Add(new PRO207
                    {
                        Orient = "0°-180° ",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO205.PRO207.Add(new PRO207
                    {
                        Orient = "22.5°-202.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO205.PRO207.Add(new PRO207
                    {
                        Orient = "45°-225°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO205.PRO207.Add(new PRO207
                    {
                        Orient = "67.5°-247.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO205.PRO207.Add(new PRO207
                    {
                        Orient = "90°-270°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO205.PRO207.Add(new PRO207
                    {
                        Orient = "112.5°-292.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO205.PRO207.Add(new PRO207
                    {
                        Orient = "135°-315°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO205.PRO207.Add(new PRO207
                    {
                        Orient = "157.5°-337.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion


                    db.PRO205.Add(objPRO205);
                    db.SaveChanges();


                }

                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO205.HeaderId;
            }
            else
            {
                objPRO205 = db.PRO205.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstMaxTiltIn = new List<string> { "DEG", "MM" };
            List<string> lstElevationOfNozzleFrom = new List<string> { "T.L.", "REF. LINE", "SHELL WEP","HEAD WEP", "Y-RING WEP" };
            List<string> lstHeightFromDrop = new List<string> { "VESSEL C.L", "SHELL OD", "T.L." };

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.MaxTiltIn = lstMaxTiltIn.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();


            ViewBag.ElevationOfNozzleFrom = lstElevationOfNozzleFrom.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            ViewBag.HeightFromDrop = lstHeightFromDrop.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();


            //  ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO206> lstPRO206 = db.PRO206.Where(x => x.HeaderId == objPRO205.HeaderId).ToList();
            List<PRO207> lstPRO207 = db.PRO207.Where(x => x.HeaderId == objPRO205.HeaderId).ToList();
            List<PRO208> lstPRO208 = db.PRO208.Where(x => x.HeaderId == objPRO205.HeaderId).ToList();
            List<PRO209> lstPRO209 = db.PRO209.Where(x => x.HeaderId == objPRO205.HeaderId).ToList();
            List<PRO209_2> lstPRO209_2 = db.PRO209_2.Where(x => x.HeaderId == objPRO205.HeaderId).ToList();
            List<PRO209_3> lstPRO209_3 = db.PRO209_3.Where(x => x.HeaderId == objPRO205.HeaderId).ToList();

            ViewBag.lstPRO206 = lstPRO206;
            ViewBag.lstPRO207 = lstPRO207;
            ViewBag.lstPRO208 = lstPRO208;
            ViewBag.lstPRO209 = lstPRO209;
            ViewBag.lstPRO209_2 = lstPRO209_2;
            ViewBag.lstPRO209_3 = lstPRO209_3;

            #endregion
            return View(objPRO205);

        }

        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO205 PRO205)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO205.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO205 objPRO205 = db.PRO205.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO205.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO205.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data  


                        objPRO205.ProtocolNo = PRO205.ProtocolNo;
                        objPRO205.ReqWEPDimensionRootFace = PRO205.ReqWEPDimensionRootFace;
                        objPRO205.ReqWEPDimensionRootGap = PRO205.ReqWEPDimensionRootGap;
                        objPRO205.ReqWEPDimensionInsideWEPAngle = PRO205.ReqWEPDimensionInsideWEPAngle;
                        objPRO205.ReqWEPDimensionOutsideWEPAngle = PRO205.ReqWEPDimensionOutsideWEPAngle;
                        objPRO205.ReqWEPDimensionOffset = PRO205.ReqWEPDimensionOffset;
                        objPRO205.ActMaxWEPDimensionRootFace = PRO205.ActMaxWEPDimensionRootFace;
                        objPRO205.ActMaxWEPDimensionRootGap = PRO205.ActMaxWEPDimensionRootGap;
                        objPRO205.ActMaxWEPDimensionInsideWEPAngle = PRO205.ActMaxWEPDimensionInsideWEPAngle;
                        objPRO205.ActMaxWEPDimensionOutsideWEPAngle = PRO205.ActMaxWEPDimensionOutsideWEPAngle;
                        objPRO205.ActMaxWEPDimensionOffset = PRO205.ActMaxWEPDimensionOffset;
                        objPRO205.ActMinWEPDimensionRootFace = PRO205.ActMinWEPDimensionRootFace;
                        objPRO205.ActMinWEPDimensionRootGap = PRO205.ActMinWEPDimensionRootGap;
                        objPRO205.ActMinWEPDimensionInsideWEPAngle = PRO205.ActMinWEPDimensionInsideWEPAngle;
                        objPRO205.ActMinWEPDimensionOutsideWEPAngle = PRO205.ActMinWEPDimensionOutsideWEPAngle;
                        objPRO205.ActMinWEPDimensionOffset = PRO205.ActMinWEPDimensionOffset;
                        objPRO205.BoltHolesStraddled = PRO205.BoltHolesStraddled;
                        objPRO205.TolBoltHolesStraddled = PRO205.TolBoltHolesStraddled;
                        objPRO205.ActBoltHolesStraddled = PRO205.ActBoltHolesStraddled;
                        objPRO205.MaxTiltIn = PRO205.MaxTiltIn;
                        objPRO205.TolMaxTiltIn = PRO205.TolMaxTiltIn;
                        objPRO205.ActMaxTiltIn = PRO205.ActMaxTiltIn;
                        objPRO205.OrientationOfNozzle = PRO205.OrientationOfNozzle;
                        objPRO205.OrientationOfNozzleCircDistFrom = PRO205.OrientationOfNozzleCircDistFrom;
                        objPRO205.ElevationOfNozzleFrom = PRO205.ElevationOfNozzleFrom;
                        objPRO205.HeightFrom = PRO205.HeightFrom;
                        objPRO205.HeightFromDrop = PRO205.HeightFromDrop;
                        objPRO205.ReqOrientationOfNozzle = PRO205.ReqOrientationOfNozzle;
                        objPRO205.ActOrientationOfNozzle = PRO205.ActOrientationOfNozzle;
                        objPRO205.ReqElevationOfNozzleFrom = PRO205.ReqElevationOfNozzleFrom;
                        objPRO205.ActElevationOfNozzleFrom = PRO205.ActElevationOfNozzleFrom;
                        objPRO205.ReqHeightFrom = PRO205.ReqHeightFrom;
                        objPRO205.ActHeightFrom = PRO205.ActHeightFrom;
                        objPRO205.CirOfShellAtNozzleElevation = PRO205.CirOfShellAtNozzleElevation;
                        objPRO205.ReqCirOfShellAtNozzleElevation = PRO205.ReqCirOfShellAtNozzleElevation;
                        objPRO205.OffsetDistOfNozzle = PRO205.OffsetDistOfNozzle;
                        objPRO205.ReqOffsetDistOfNozzle = PRO205.ReqOffsetDistOfNozzle;
                        objPRO205.ActOffsetDistOfNozzle = PRO205.ActOffsetDistOfNozzle;
                        objPRO205.DistBTPairNozzle = PRO205.DistBTPairNozzle;
                        objPRO205.ReqDistBTPairNozzle = PRO205.ReqDistBTPairNozzle;
                        objPRO205.ActDistBTPairNozzle = PRO205.ActDistBTPairNozzle;
                        objPRO205.IdentificationOfRFPad = PRO205.IdentificationOfRFPad;
                        objPRO205.CheckPoint1 = PRO205.CheckPoint1;
                        objPRO205.CheckPoint2 = PRO205.CheckPoint2;
                        objPRO205.CheckPoint3 = PRO205.CheckPoint3;
                        objPRO205.CheckPoint4 = PRO205.CheckPoint4;
                        objPRO205.CheckPoint5 = PRO205.CheckPoint5;
                        objPRO205.CheckPoint6 = PRO205.CheckPoint6;
                        objPRO205.CheckPoint7 = PRO205.CheckPoint7;
                        objPRO205.CheckPoint9 = PRO205.CheckPoint9;
                        objPRO205.CheckPoint9_2 = PRO205.CheckPoint9_2;
                        objPRO205.CheckPoint10 = PRO205.CheckPoint10;
                        objPRO205.CheckPoint10_2 = PRO205.CheckPoint10_2;
                        objPRO205.CheckPoint10_3 = PRO205.CheckPoint10_3;
                        objPRO205.CheckPoint11 = PRO205.CheckPoint11;
                        objPRO205.CheckPoint12 = PRO205.CheckPoint12;
                        objPRO205.QCRemarks = PRO205.QCRemarks;
                        objPRO205.Result = PRO205.Result;
                        objPRO205.OutOfRoundnessAllowed = PRO205.OutOfRoundnessAllowed;
                        objPRO205.OutOfRoundnessOutBy = PRO205.OutOfRoundnessOutBy;
                        objPRO205.SpotNoClad = PRO205.SpotNoClad;
                        objPRO205.ReqWidth = PRO205.ReqWidth;
                        objPRO205.ReqDepth = PRO205.ReqDepth;
                        objPRO205.AllowedOffset = PRO205.AllowedOffset;
                        objPRO205.Note1 = PRO205.Note1;
                        objPRO205.Note2 = PRO205.Note2;
                        objPRO205.Note3 = PRO205.Note3;
                        objPRO205.IdentificationOfCleats = PRO205.IdentificationOfCleats;

                        objPRO205.ReqWEPDimensionRootFaceTol = PRO205.ReqWEPDimensionRootFaceTol;
                        objPRO205.ReqWEPDimensionRootGapTol = PRO205.ReqWEPDimensionRootGapTol;
                        objPRO205.ReqWEPDimensionInsideWEPAngleTol = PRO205.ReqWEPDimensionInsideWEPAngleTol;
                        objPRO205.ReqWEPDimensionOutsideWEPAngleTol = PRO205.ReqWEPDimensionOutsideWEPAngleTol;
                        objPRO205.IsOrientationOfNozzle = PRO205.IsOrientationOfNozzle;
                        objPRO205.ReqOrientationOfNozzleTol = PRO205.ReqOrientationOfNozzleTol;
                        objPRO205.IsElevationOfNozzleFrom = PRO205.IsElevationOfNozzleFrom;
                        objPRO205.ReqElevationOfNozzleFromTol = PRO205.ReqElevationOfNozzleFromTol;
                        objPRO205.ReqHeightFromTol = PRO205.ReqHeightFromTol;
                        objPRO205.OffsetDistOfNozzleTol = PRO205.OffsetDistOfNozzleTol;
                        objPRO205.DistBTPairNozzleTol = PRO205.DistBTPairNozzleTol;
                        objPRO205.IsConcentricity = PRO205.IsConcentricity;
                        objPRO205.MaxAllowConcentricity = PRO205.MaxAllowConcentricity;
                        objPRO205.ActConcentricity = PRO205.ActConcentricity;
                        objPRO205.IsElevationOfReferanceLine = PRO205.IsElevationOfReferanceLine;
                        objPRO205.ActElevationOfReferanceLine = PRO205.ActElevationOfReferanceLine;
                        objPRO205.CheckPoint13 = PRO205.CheckPoint13;
                        objPRO205.AngularNozzle = PRO205.AngularNozzle;
                        
                        objPRO205.EditedBy = objClsLoginInfo.UserName;
                        objPRO205.EditedOn = DateTime.Now;


                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO205.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO205 objPRO205 = db.PRO205.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO205 != null && string.IsNullOrWhiteSpace(objPRO205.ProtocolNo))
                    {
                        db.PRO205.Remove(objPRO205);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO205.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion        

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO206> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO206> lstAddPRO206 = new List<PRO206>();
                List<PRO206> lstDeletePRO206 = new List<PRO206>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO206 obj = db.PRO206.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.ActIdentificationMarking = item.ActIdentificationMarking;

                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO206 obj = new PRO206();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.ActIdentificationMarking))
                            {
                                obj.ActIdentificationMarking = item.ActIdentificationMarking;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO206.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO206.Count > 0)
                    {
                        db.PRO206.AddRange(lstAddPRO206);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO206 = db.PRO206.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO206.Count > 0)
                    {
                        db.PRO206.RemoveRange(lstDeletePRO206);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO206 = db.PRO206.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO206.Count > 0)
                    {
                        db.PRO206.RemoveRange(lstDeletePRO206);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line4 details
        [HttpPost]
        public JsonResult SaveProtocolLine4(List<PRO209_2> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO209_2> lstAddPRO209_2 = new List<PRO209_2>();
                List<PRO209_2> lstDeletePRO209_2 = new List<PRO209_2>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO209_2 obj = db.PRO209_2.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {

                                obj.ActIdentificationOfRFPad = item.ActIdentificationOfRFPad;

                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO209_2 obj = new PRO209_2();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.ActIdentificationOfRFPad))
                            {

                                obj.ActIdentificationOfRFPad = item.ActIdentificationOfRFPad;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO209_2.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO209_2.Count > 0)
                    {
                        db.PRO209_2.AddRange(lstAddPRO209_2);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO209_2 = db.PRO209_2.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO209_2.Count > 0)
                    {
                        db.PRO209_2.RemoveRange(lstDeletePRO209_2);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO209_2 = db.PRO209_2.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO209_2.Count > 0)
                    {
                        db.PRO209_2.RemoveRange(lstDeletePRO209_2);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2(List<PRO207> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO207> lstAddPRO207 = new List<PRO207>();
                List<PRO207> lstDeletePRO207 = new List<PRO207>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO207 obj = db.PRO207.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.Orient = item.Orient;
                                obj.AtNozzleLocation = item.AtNozzleLocation;

                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO207 obj = new PRO207();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.Orient))
                            {
                                obj.Orient = item.Orient;
                                obj.AtNozzleLocation = item.AtNozzleLocation;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO207.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO207.Count > 0)
                    {
                        db.PRO207.AddRange(lstAddPRO207);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO207 = db.PRO207.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO207.Count > 0)
                    {
                        db.PRO207.RemoveRange(lstDeletePRO207);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO207 = db.PRO207.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO207.Count > 0)
                    {
                        db.PRO207.RemoveRange(lstDeletePRO207);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3(List<PRO208> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO208> lstAddPRO208 = new List<PRO208>();
                List<PRO208> lstDeletePRO208 = new List<PRO208>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO208 obj = db.PRO208.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.SpotNo = item.SpotNo;
                                obj.Width = item.Width;
                                obj.Depth = item.Depth;
                                obj.Offset = item.Offset;

                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO208 obj = new PRO208();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.SpotNo))
                            {
                                obj.SpotNo = item.SpotNo;
                                obj.Width = item.Width;
                                obj.Depth = item.Depth;
                                obj.Offset = item.Offset;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO208.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO208.Count > 0)
                    {
                        db.PRO208.AddRange(lstAddPRO208);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO208 = db.PRO208.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO208.Count > 0)
                    {
                        db.PRO208.RemoveRange(lstDeletePRO208);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO208 = db.PRO208.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO208.Count > 0)
                    {
                        db.PRO208.RemoveRange(lstDeletePRO208);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line5 details
        [HttpPost]
        public JsonResult SaveProtocolLine5(List<PRO209> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO209> lstAddPRO209 = new List<PRO209>();
                List<PRO209> lstDeletePRO209 = new List<PRO209>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO209 obj = db.PRO209.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.CleatNo = item.CleatNo;
                                obj.SizeWeldOnParentMetal = item.SizeWeldOnParentMetal;
                                obj.Location = item.Location;
                                obj.OutsideOrInside = item.OutsideOrInside;
                                obj.DistanceFromSeamCenter = item.DistanceFromSeamCenter;
                                obj.CircumDistanceFrom = item.CircumDistanceFrom;

                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO209 obj = new PRO209();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.CleatNo))
                            {
                                obj.CleatNo = item.CleatNo;
                                obj.SizeWeldOnParentMetal = item.SizeWeldOnParentMetal;
                                obj.Location = item.Location;
                                obj.OutsideOrInside = item.OutsideOrInside;
                                obj.DistanceFromSeamCenter = item.DistanceFromSeamCenter;
                                obj.CircumDistanceFrom = item.CircumDistanceFrom;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO209.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO209.Count > 0)
                    {
                        db.PRO209.AddRange(lstAddPRO209);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO209 = db.PRO209.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO209.Count > 0)
                    {
                        db.PRO209.RemoveRange(lstDeletePRO209);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO209 = db.PRO209.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO209.Count > 0)
                    {
                        db.PRO209.RemoveRange(lstDeletePRO209);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line6 details
        [HttpPost]
        public JsonResult SaveProtocolLine6(List<PRO209_3> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO209_3> lstAddPRO209_3 = new List<PRO209_3>();
                List<PRO209_3> lstDeletePRO209_3 = new List<PRO209_3>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO209_3 obj = db.PRO209_3.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.DrgNo = item.DrgNo;
                                obj.RevNo = item.RevNo;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO209_3 obj = new PRO209_3();

                            if (!string.IsNullOrWhiteSpace(item.DrgNo))
                            {
                                obj.HeaderId = item.HeaderId;
                                obj.DrgNo = item.DrgNo;
                                obj.RevNo = item.RevNo;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO209_3.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO209_3.Count > 0)
                    {
                        db.PRO209_3.AddRange(lstAddPRO209_3);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO209_3 = db.PRO209_3.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO209_3.Count > 0)
                    {
                        db.PRO209_3.RemoveRange(lstDeletePRO209_3);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO209_3 = db.PRO209_3.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO209_3.Count > 0)
                    {
                        db.PRO209_3.RemoveRange(lstDeletePRO209_3);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

        #region Linkage View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL205 objPRL205 = new PRL205();
            objPRL205 = db.PRL205.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL205 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL205.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;

                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();
            List<string> lsttypesofforming = new List<string> { "HOT", "COLD", "WARM" };
            List<string> lstMaxTiltIn = new List<string> { "DEG", "MM" };
            List<string> lstElevationOfNozzleFrom = new List<string> { "T.L.", "REF. LINE", "SHELL WEP", "HEAD WEP", "Y-RING WEP" };
            List<string> lstHeightFromDrop = new List<string> { "VESSEL C.L", "SHELL OD", "T.L." };

            ViewBag.TypesOfForming = lsttypesofforming.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.MaxTiltIn = lstMaxTiltIn.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();


            ViewBag.ElevationOfNozzleFrom = lstElevationOfNozzleFrom.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.HeightFromDrop = lstHeightFromDrop.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL206> lstPRL206 = db.PRL206.Where(x => x.HeaderId == objPRL205.HeaderId).ToList();
            List<PRL207> lstPRL207 = db.PRL207.Where(x => x.HeaderId == objPRL205.HeaderId).ToList();
            List<PRL208> lstPRL208 = db.PRL208.Where(x => x.HeaderId == objPRL205.HeaderId).ToList();
            List<PRL209> lstPRL209 = db.PRL209.Where(x => x.HeaderId == objPRL205.HeaderId).ToList();
            List<PRL209_2> lstPRL209_2 = db.PRL209_2.Where(x => x.HeaderId == objPRL205.HeaderId).ToList();
            List<PRL209_3> lstPRL209_3 = db.PRL209_3.Where(x => x.HeaderId == objPRL205.HeaderId).ToList();
            
            ViewBag.lstPRL206 = lstPRL206;
            ViewBag.lstPRL207 = lstPRL207;
            ViewBag.lstPRL208 = lstPRL208;
            ViewBag.lstPRL209 = lstPRL209;
            ViewBag.lstPRL209_2 = lstPRL209_2;
            ViewBag.lstPRL209_3 = lstPRL209_3;
            
            #endregion
            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.lstDrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL205.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL205.ActFilledBy) && (objPRL205.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL205.Project).ToList();
                            ViewBag.lstDrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.lstDrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL205);
        }

        [HttpPost]
        public ActionResult LinkageMobile(int? id, int? m, string APIRequestFrom = "", string UserName = "")
        {

            string ProjectDesc = "";
            string ManufacturingCode = "";
            string AccessRole = "";
            bool isEditable = false;
            var DrawingNo = new List<string>();
            bool isQCRemarkEditable = false;

            PRL205 objPRL205 = new PRL205();

            db.Configuration.ProxyCreationEnabled = false;
            objPRL205 = db.PRL205.Where(i => i.HeaderId == id).FirstOrDefault();
            db.Entry(objPRL205).State = System.Data.Entity.EntityState.Detached;

            NDEModels objNDEModels = new NDEModels();
            if (objPRL205 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL205.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
            AccessRole = objUserRoleAccessDetails.UserDesignation;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();
            List<string> lsttypesofforming = new List<string> { "HOT", "COLD", "WARM" };
            List<string> lstMaxTiltIn = new List<string> { "DEG", "MM" };
            List<string> lstElevationOfNozzleFrom = new List<string> { "T.L.", "REF. LINE", "SHELL WEP" };
            List<string> lstHeightFromDrop = new List<string> { "VESSEL C.L", "SHELL OD", "T.L." };

            var TypesOfForming = lsttypesofforming.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            var YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var MaxTiltIn = lstMaxTiltIn.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();


            var ElevationOfNozzleFrom = lstElevationOfNozzleFrom.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var HeightFromDrop = lstHeightFromDrop.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            var LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL206> lstPRL206 = db.PRL206.Where(x => x.HeaderId == objPRL205.HeaderId).ToList();
            List<PRL207> lstPRL207 = db.PRL207.Where(x => x.HeaderId == objPRL205.HeaderId).ToList();
            List<PRL208> lstPRL208 = db.PRL208.Where(x => x.HeaderId == objPRL205.HeaderId).ToList();
            List<PRL209> lstPRL209 = db.PRL209.Where(x => x.HeaderId == objPRL205.HeaderId).ToList();
            List<PRL209_2> lstPRL209_2 = db.PRL209_2.Where(x => x.HeaderId == objPRL205.HeaderId).ToList();
            //List<PRL194_3> lstPRL194_3 = db.PRL194_3.Where(x => x.HeaderId == objPRL205.HeaderId).ToList();
            //List<PRL194_4> lstPRL194_4 = db.PRL194_4.Where(x => x.HeaderId == objPRL205.HeaderId).ToList();
            //List<PRL194_5> lstPRL194_5 = db.PRL194_5.Where(x => x.HeaderId == objPRL205.HeaderId).ToList();
            //List<PRL194_6> lstPRL194_6 = db.PRL194_6.Where(x => x.HeaderId == objPRL205.HeaderId).ToList();
            //List<SP_IPI_PROTOCOL039_LINKAGE_GET_LINES8_DATA_Result> lstPRL194_7 = db.SP_IPI_PROTOCOL039_LINKAGE_GET_LINES8_DATA(1, int.MaxValue, "", " HeaderId = " + objPRL205.HeaderId).ToList();
            //List<SP_IPI_PROTOCOL039_LINKAGE_GET_LINES9_DATA_Result> lstPRL194_8 = db.SP_IPI_PROTOCOL039_LINKAGE_GET_LINES9_DATA(1, int.MaxValue, "", " HeaderId = " + objPRL205.HeaderId).ToList();

            //lstPRL194_3 = lstPRL194_3,
            //lstPRL194_4 = lstPRL194_4,
            //lstPRL194_5 = lstPRL194_5,
            //lstPRL194_6 = lstPRL194_6,
            //lstPRL194_7 = lstPRL194_7,
            //lstPRL194_8 = lstPRL194_8,

            #endregion
            try
            {
                isEditable = false;
                isQCRemarkEditable = false;
                if (m == 1)
                {
                    isEditable = true;

                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL205.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL205.ActFilledBy) && (objPRL205.RequestNo.HasValue))
                        {
                            isEditable = false;
                            isQCRemarkEditable = true;
                        }
                        else
                        {
                            DrawingNo = (new GeneralController()).GetPLMDrawingNumber(objPRL205.Project).ToList();
                        }
                    }
                }
                else
                {
                    isEditable = false;
                }

                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    ProjectDesc = ProjectDesc,
                    ManufacturingCode = ManufacturingCode,
                    isEditable = isEditable,
                    isQCRemarkEditable = isQCRemarkEditable,
                    DrawingNo = DrawingNo,
                    AccessRole = AccessRole,
                    OutsideInside = OutsideInside,
                    YesNoEnum = YesNoEnum,
                    YesNAEnum = YesNAEnum,
                    TypesOfForming = TypesOfForming,
                    MaxTiltIn = MaxTiltIn,
                    ElevationOfNozzleFrom = ElevationOfNozzleFrom,
                    HeightFromDrop = HeightFromDrop,
                    LeftRight = LeftRight,

                    objPRL205 = objPRL205,

                    lstPRL206 = lstPRL206,
                    lstPRL207 = lstPRL207,
                    lstPRL208 = lstPRL208,
                    lstPRL209 = lstPRL209,
                    lstPRL209_2 = lstPRL209_2

                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.lstDrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();

                return Json(new
                {
                    res = "0",
                    msg = ex.Message,
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL205 PRL205, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = PRL205.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL205 objPRL205 = db.PRL205.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    #region Save Data

                    objPRL205.DrawingRevisionNo = PRL205.DrawingRevisionNo;
                    objPRL205.DrawingNo = PRL205.DrawingNo;
                    objPRL205.DCRNo = PRL205.DCRNo;
                    objPRL205.ProtocolNo = PRL205.ProtocolNo != null ? PRL205.ProtocolNo : "";
                    objPRL205.InspectionAgency = PRL205.InspectionAgency;
                    objPRL205.ReqWEPDimensionRootFace = PRL205.ReqWEPDimensionRootFace;
                    objPRL205.ReqWEPDimensionRootGap = PRL205.ReqWEPDimensionRootGap;
                    objPRL205.ReqWEPDimensionInsideWEPAngle = PRL205.ReqWEPDimensionInsideWEPAngle;
                    objPRL205.ReqWEPDimensionOutsideWEPAngle = PRL205.ReqWEPDimensionOutsideWEPAngle;
                    objPRL205.ReqWEPDimensionOffset = PRL205.ReqWEPDimensionOffset;
                    objPRL205.ActMaxWEPDimensionRootFace = PRL205.ActMaxWEPDimensionRootFace;
                    objPRL205.ActMaxWEPDimensionRootGap = PRL205.ActMaxWEPDimensionRootGap;
                    objPRL205.ActMaxWEPDimensionInsideWEPAngle = PRL205.ActMaxWEPDimensionInsideWEPAngle;
                    objPRL205.ActMaxWEPDimensionOutsideWEPAngle = PRL205.ActMaxWEPDimensionOutsideWEPAngle;
                    objPRL205.ActMaxWEPDimensionOffset = PRL205.ActMaxWEPDimensionOffset;
                    objPRL205.ActMinWEPDimensionRootFace = PRL205.ActMinWEPDimensionRootFace;
                    objPRL205.ActMinWEPDimensionRootGap = PRL205.ActMinWEPDimensionRootGap;
                    objPRL205.ActMinWEPDimensionInsideWEPAngle = PRL205.ActMinWEPDimensionInsideWEPAngle;
                    objPRL205.ActMinWEPDimensionOutsideWEPAngle = PRL205.ActMinWEPDimensionOutsideWEPAngle;
                    objPRL205.ActMinWEPDimensionOffset = PRL205.ActMinWEPDimensionOffset;
                    objPRL205.BoltHolesStraddled = PRL205.BoltHolesStraddled;
                    objPRL205.TolBoltHolesStraddled = PRL205.TolBoltHolesStraddled;
                    objPRL205.ActBoltHolesStraddled = PRL205.ActBoltHolesStraddled;
                    objPRL205.MaxTiltIn = PRL205.MaxTiltIn;
                    objPRL205.TolMaxTiltIn = PRL205.TolMaxTiltIn;
                    objPRL205.ActMaxTiltIn = PRL205.ActMaxTiltIn;
                    objPRL205.OrientationOfNozzle = PRL205.OrientationOfNozzle;
                    objPRL205.OrientationOfNozzleCircDistFrom = PRL205.OrientationOfNozzleCircDistFrom;
                    objPRL205.ElevationOfNozzleFrom = PRL205.ElevationOfNozzleFrom;
                    objPRL205.HeightFrom = PRL205.HeightFrom;
                    objPRL205.HeightFromDrop = PRL205.HeightFromDrop;
                    objPRL205.ReqOrientationOfNozzle = PRL205.ReqOrientationOfNozzle;
                    objPRL205.ActOrientationOfNozzle = PRL205.ActOrientationOfNozzle;
                    objPRL205.ReqElevationOfNozzleFrom = PRL205.ReqElevationOfNozzleFrom;
                    objPRL205.ActElevationOfNozzleFrom = PRL205.ActElevationOfNozzleFrom;
                    objPRL205.ReqHeightFrom = PRL205.ReqHeightFrom;
                    objPRL205.ActHeightFrom = PRL205.ActHeightFrom;
                    objPRL205.CirOfShellAtNozzleElevation = PRL205.CirOfShellAtNozzleElevation;
                    objPRL205.ReqCirOfShellAtNozzleElevation = PRL205.ReqCirOfShellAtNozzleElevation;
                    objPRL205.OffsetDistOfNozzle = PRL205.OffsetDistOfNozzle;
                    objPRL205.ReqOffsetDistOfNozzle = PRL205.ReqOffsetDistOfNozzle;
                    objPRL205.ActOffsetDistOfNozzle = PRL205.ActOffsetDistOfNozzle;
                    objPRL205.DistBTPairNozzle = PRL205.DistBTPairNozzle;
                    objPRL205.ReqDistBTPairNozzle = PRL205.ReqDistBTPairNozzle;
                    objPRL205.ActDistBTPairNozzle = PRL205.ActDistBTPairNozzle;
                    objPRL205.IdentificationOfRFPad = PRL205.IdentificationOfRFPad;
                    objPRL205.CheckPoint1 = PRL205.CheckPoint1;
                    objPRL205.CheckPoint2 = PRL205.CheckPoint2;
                    objPRL205.CheckPoint3 = PRL205.CheckPoint3;
                    objPRL205.CheckPoint4 = PRL205.CheckPoint4;
                    objPRL205.CheckPoint5 = PRL205.CheckPoint5;
                    objPRL205.CheckPoint6 = PRL205.CheckPoint6;
                    objPRL205.CheckPoint7 = PRL205.CheckPoint7;
                    objPRL205.CheckPoint9 = PRL205.CheckPoint9;
                    objPRL205.CheckPoint9_2 = PRL205.CheckPoint9_2;
                    objPRL205.CheckPoint10 = PRL205.CheckPoint10;
                    objPRL205.CheckPoint10_2 = PRL205.CheckPoint10_2;
                    objPRL205.CheckPoint10_3 = PRL205.CheckPoint10_3;
                    objPRL205.CheckPoint11 = PRL205.CheckPoint11;
                    objPRL205.CheckPoint12 = PRL205.CheckPoint12;
                    objPRL205.QCRemarks = PRL205.QCRemarks;
                    objPRL205.Result = PRL205.Result;
                    objPRL205.OutOfRoundnessAllowed = PRL205.OutOfRoundnessAllowed;
                    objPRL205.OutOfRoundnessOutBy = PRL205.OutOfRoundnessOutBy;
                    objPRL205.SpotNoClad = PRL205.SpotNoClad;
                    objPRL205.ReqWidth = PRL205.ReqWidth;
                    objPRL205.ReqDepth = PRL205.ReqDepth;
                    objPRL205.AllowedOffset = PRL205.AllowedOffset;
                    objPRL205.Note1 = PRL205.Note1;
                    objPRL205.Note2 = PRL205.Note2;
                    objPRL205.Note3 = PRL205.Note3;
                    objPRL205.IdentificationOfCleats = PRL205.IdentificationOfCleats;

                    objPRL205.ReqWEPDimensionRootFaceTol = PRL205.ReqWEPDimensionRootFaceTol;
                    objPRL205.ReqWEPDimensionRootGapTol = PRL205.ReqWEPDimensionRootGapTol;
                    objPRL205.ReqWEPDimensionInsideWEPAngleTol = PRL205.ReqWEPDimensionInsideWEPAngleTol;
                    objPRL205.ReqWEPDimensionOutsideWEPAngleTol = PRL205.ReqWEPDimensionOutsideWEPAngleTol;
                    objPRL205.IsOrientationOfNozzle = PRL205.IsOrientationOfNozzle;
                    objPRL205.ReqOrientationOfNozzleTol = PRL205.ReqOrientationOfNozzleTol;
                    objPRL205.IsElevationOfNozzleFrom = PRL205.IsElevationOfNozzleFrom;
                    objPRL205.ReqElevationOfNozzleFromTol = PRL205.ReqElevationOfNozzleFromTol;
                    objPRL205.ReqHeightFromTol = PRL205.ReqHeightFromTol;
                    objPRL205.OffsetDistOfNozzleTol = PRL205.OffsetDistOfNozzleTol;
                    objPRL205.DistBTPairNozzleTol = PRL205.DistBTPairNozzleTol;
                    objPRL205.IsConcentricity = PRL205.IsConcentricity;
                    objPRL205.MaxAllowConcentricity = PRL205.MaxAllowConcentricity;
                    objPRL205.ActConcentricity = PRL205.ActConcentricity;
                    objPRL205.IsElevationOfReferanceLine = PRL205.IsElevationOfReferanceLine;
                    objPRL205.ActElevationOfReferanceLine = PRL205.ActElevationOfReferanceLine;
                    objPRL205.CheckPoint13 = PRL205.CheckPoint13;
                    objPRL205.AngularNozzle = PRL205.AngularNozzle;
                    objPRL205.ReqWEPDimensionOffsetTol = PRL205.ReqWEPDimensionOffsetTol;
                    objPRL205.IsAngle = PRL205.IsAngle;
                    objPRL205.RequiredAngle = PRL205.RequiredAngle;
                    objPRL205.TolAngle = PRL205.TolAngle;
                    objPRL205.ActAngle = PRL205.ActAngle;



                    objPRL205.EditedBy = UserName;
                    objPRL205.EditedOn = DateTime.Now;
                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL205.ActFilledBy = UserName;
                            objPRL205.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL205.ReqFilledBy = UserName;
                            objPRL205.ReqFilledOn = DateTime.Now;
                        }
                    }
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL205.HeaderId;
                    #endregion

                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL205 objPRL205 = db.PRL205.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL205 != null && string.IsNullOrWhiteSpace(objPRL205.ProtocolNo))
                    {
                        db.PRL205.Remove(objPRL205);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL205.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL206> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL206> lstAddPRL206 = new List<PRL206>();
                List<PRL206> lstDeletePRL206 = new List<PRL206>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRL206 obj = db.PRL206.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.ActIdentificationMarking = item.ActIdentificationMarking;
                                obj.EditedBy = UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRL206 obj = new PRL206();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.ActIdentificationMarking))
                            {
                                obj.ActIdentificationMarking = item.ActIdentificationMarking;
                                obj.CreatedBy = UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRL206.Add(obj);
                            }
                        }
                    }
                }
                if (lstAddPRL206.Count > 0)
                {
                    db.PRL206.AddRange(lstAddPRL206);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL206 = db.PRL206.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL206 = db.PRL206.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL206.Count > 0)
                {
                    db.PRL206.RemoveRange(lstDeletePRL206);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line4 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine4Linkage(List<PRL209_2> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL209_2> lstAddPRL209_2 = new List<PRL209_2>();
                List<PRL209_2> lstDeletePRL209_2 = new List<PRL209_2>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL209_2 obj = db.PRL209_2.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL209_2();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.ActIdentificationOfRFPad = item.ActIdentificationOfRFPad;
                        if (isAdded)
                        {
                            lstAddPRL209_2.Add(obj);
                        }
                    }

                    if (lstAddPRL209_2.Count > 0)
                    {
                        db.PRL209_2.AddRange(lstAddPRL209_2);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL209_2 = db.PRL209_2.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL209_2 = db.PRL209_2.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL209_2.Count > 0)
                {
                    db.PRL209_2.RemoveRange(lstDeletePRL209_2);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine2Linkage(List<PRL207> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL207> lstAddPRL207 = new List<PRL207>();
                List<PRL207> lstDeletePRL207 = new List<PRL207>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL207 obj = db.PRL207.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL207();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        if (!string.IsNullOrWhiteSpace(item.Orient))
                        {
                            obj.Orient = item.Orient;
                            obj.AtNozzleLocation = item.AtNozzleLocation;
                            if (isAdded)
                            {
                                lstAddPRL207.Add(obj);
                            }
                        }
                    }
                }
                if (lstAddPRL207.Count > 0)
                {
                    db.PRL207.AddRange(lstAddPRL207);
                }
                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL207 = db.PRL207.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL207 = db.PRL207.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL207.Count > 0)
                {
                    db.PRL207.RemoveRange(lstDeletePRL207);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line3 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine3Linkage(List<PRL208> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL208> lstAddPRL208 = new List<PRL208>();
                List<PRL208> lstDeletePRL208 = new List<PRL208>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL208 obj = db.PRL208.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        else
                        {
                            obj = new PRL208();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }

                        if (!string.IsNullOrWhiteSpace(item.SpotNo))
                        {
                            obj.SpotNo = item.SpotNo;
                            obj.Width = item.Width;
                            obj.Depth = item.Depth;
                            obj.Offset = item.Offset;
                        }
                        if (isAdded)
                        {
                            lstAddPRL208.Add(obj);
                        }
                    }
                    if (lstAddPRL208.Count > 0)
                    {
                        db.PRL208.AddRange(lstAddPRL208);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL208 = db.PRL208.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL208 = db.PRL208.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL208.Count > 0)
                {
                    db.PRL208.RemoveRange(lstDeletePRL208);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line5 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine5Linkage(List<PRL209> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL209> lstAddPRL209 = new List<PRL209>();
                List<PRL209> lstDeletePRL209 = new List<PRL209>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL209 obj = db.PRL209.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL209();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        obj.CleatNo = item.CleatNo;
                        obj.SizeWeldOnParentMetal = item.SizeWeldOnParentMetal;
                        obj.Location = item.Location;
                        obj.OutsideOrInside = item.OutsideOrInside;
                        obj.DistanceFromSeamCenter = item.DistanceFromSeamCenter;
                        obj.CircumDistanceFrom = item.CircumDistanceFrom;

                        if (isAdded)
                        {
                            lstAddPRL209.Add(obj);
                        }
                    }

                    if (lstAddPRL209.Count > 0)
                    {
                        db.PRL209.AddRange(lstAddPRL209);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL209 = db.PRL209.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL209 = db.PRL209.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL209.Count > 0)
                {
                    db.PRL209.RemoveRange(lstDeletePRL209);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line6 details
        [HttpPost]
        public JsonResult SaveProtocolLine6Linkage(List<PRL209_3> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRL209_3> lstAddPRL209_3 = new List<PRL209_3>();
                List<PRL209_3> lstDelete209_3 = new List<PRL209_3>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRL209_3 obj = db.PRL209_3.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.DrgNo = item.DrgNo;
                                obj.RevNo = item.RevNo;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRL209_3 obj = new PRL209_3();

                            if (!string.IsNullOrWhiteSpace(item.DrgNo))
                            {
                                obj.HeaderId = item.HeaderId;
                                obj.DrgNo = item.DrgNo;
                                obj.RevNo = item.RevNo;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRL209_3.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRL209_3.Count > 0)
                    {
                        db.PRL209_3.AddRange(lstAddPRL209_3);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDelete209_3 = db.PRL209_3.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDelete209_3.Count > 0)
                    {
                        db.PRL209_3.RemoveRange(lstDelete209_3);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDelete209_3 = db.PRL209_3.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDelete209_3.Count > 0)
                    {
                        db.PRL209_3.RemoveRange(lstDelete209_3);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

        [HttpPost]
        public ActionResult getDrgNo(string ProjectNo)
        {
            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(ProjectNo).ToList();
            lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            return Json(lstdrawing.Select(x => new
            {
                Value = x,
                Text = x
            }).ToList(), JsonRequestBehavior.AllowGet);
        }
    }
}