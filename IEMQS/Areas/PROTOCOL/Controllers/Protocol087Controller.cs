﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsBase;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol087Controller : clsBase
    {
        // GET: PROTOCOL/Protocol087
        string ControllerURL = "/PROTOCOL/Protocol087/";
        string Title = "REPORT FOR WELD VISUAL & DIMENSION OF SPOOL TYPE-3 (BOTH SIDE FLANGES)";

        #region Details View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO430 objPRO430 = new PRO430();
            if (!id.HasValue)
            {
                try
                {
                    objPRO430.ProtocolNo = string.Empty;
                    objPRO430.CreatedBy = objClsLoginInfo.UserName;
                    objPRO430.CreatedOn = DateTime.Now;

                    db.PRO430.Add(objPRO430);
                    db.SaveChanges();
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO430.HeaderId;
            }
            else
            {
                objPRO430 = db.PRO430.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstyesnona = clsImplementationEnum.getYesNoNA().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNoNAEnum = lstyesnona.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO431> lstPRO431 = db.PRO431.Where(x => x.HeaderId == objPRO430.HeaderId).ToList();

            ViewBag.lstPRO431 = lstPRO431;

            #endregion

            return View(objPRO430);
        }

        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO430 PRO430)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO430.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO430 objPRO430 = db.PRO430.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO430.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO430.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data

                        objPRO430.ProtocolNo = PRO430.ProtocolNo;
                        objPRO430.IsBoltHolesStraddledFor1Flange = PRO430.IsBoltHolesStraddledFor1Flange;
                        objPRO430.IsBoltHolesStraddledFor2Flange = PRO430.IsBoltHolesStraddledFor2Flange;
                        objPRO430.ReqTolTiltingOf1Flange = PRO430.ReqTolTiltingOf1Flange;
                        objPRO430.ReqTolTiltingOf2Flange = PRO430.ReqTolTiltingOf2Flange;
                        objPRO430.ReqTolBoltHolesStraddledFor1Flange = PRO430.ReqTolBoltHolesStraddledFor1Flange;
                        objPRO430.ReqTolBoltHolesStraddledFor2Flange = PRO430.ReqTolBoltHolesStraddledFor2Flange;
                        objPRO430.ActBoltHolesStraddledFor1Flange = PRO430.ActBoltHolesStraddledFor1Flange;
                        objPRO430.ActBoltHolesStraddledFor2Flange = PRO430.ActBoltHolesStraddledFor2Flange;
                        objPRO430.ActTiltingOf1Flange = PRO430.ActTiltingOf1Flange;
                        objPRO430.ActTiltingOf2Flange = PRO430.ActTiltingOf2Flange;
                        objPRO430.ReqOrientation2FlangeWithRes1Flange = PRO430.ReqOrientation2FlangeWithRes1Flange;
                        objPRO430.ActOrientation2FlangeWithRes1Flange = PRO430.ActOrientation2FlangeWithRes1Flange;
                        objPRO430.ReqElevationOf2FlangeFromBottomface1Flange = PRO430.ReqElevationOf2FlangeFromBottomface1Flange;
                        objPRO430.ActElevationOf2FlangeFromBottomface1Flange = PRO430.ActElevationOf2FlangeFromBottomface1Flange;
                        objPRO430.ReqProjecttionOf2FlangeFromCenter1Flange = PRO430.ReqProjecttionOf2FlangeFromCenter1Flange;
                        objPRO430.ActProjecttionOf2FlangeFromCenter1Flange = PRO430.ActProjecttionOf2FlangeFromCenter1Flange;

                        objPRO430.CheckPoint1 = PRO430.CheckPoint1;
                        objPRO430.CheckPoint3 = PRO430.CheckPoint3;
                        objPRO430.CheckPoint4 = PRO430.CheckPoint4;
                        objPRO430.CheckPoint5 = PRO430.CheckPoint5;
                        objPRO430.CheckPoint6 = PRO430.CheckPoint6;
                        objPRO430.CheckPoint7 = PRO430.CheckPoint7;
                        objPRO430.CheckPoint8 = PRO430.CheckPoint8;
                        objPRO430.CheckPoint9 = PRO430.CheckPoint9;
                        objPRO430.CheckPoint10 = PRO430.CheckPoint10;
                        objPRO430.CheckPoint10_2 = PRO430.CheckPoint10_2;

                        objPRO430.QCRemarks = PRO430.QCRemarks;
                        objPRO430.Result = PRO430.Result;

                        objPRO430.EditedBy = objClsLoginInfo.UserName;
                        objPRO430.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO430.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO430 objPRO430 = db.PRO430.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO430 != null && string.IsNullOrWhiteSpace(objPRO430.ProtocolNo))
                    {
                        db.PRO430.Remove(objPRO430);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO430.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO431> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO431> lstAddPRO431 = new List<PRO431>();
                List<PRO431> lstDeletePRO431 = new List<PRO431>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO431 obj = db.PRO431.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.Description = item.Description;
                                obj.ReqElevation = item.ReqElevation;
                                obj.ActElevation = item.ActElevation;
                                obj.ReqOrientation = item.ReqOrientation;
                                obj.ActOrientation = item.ActOrientation;
                                obj.ReqProjection = item.ReqProjection;
                                obj.ActProjection = item.ActProjection;
                                obj.ReqSize = item.ReqSize;
                                obj.ActSize = item.ActSize;
                                obj.ReqTilt = item.ReqTilt;
                                obj.ActTilt = item.ActTilt;

                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO431 obj = new PRO431();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.Description))
                            {
                                obj.Description = item.Description;
                                obj.ReqElevation = item.ReqElevation;
                                obj.ActElevation = item.ActElevation;
                                obj.ReqOrientation = item.ReqOrientation;
                                obj.ActOrientation = item.ActOrientation;
                                obj.ReqProjection = item.ReqProjection;
                                obj.ActProjection = item.ActProjection;
                                obj.ReqSize = item.ReqSize;
                                obj.ActSize = item.ActSize;
                                obj.ReqTilt = item.ReqTilt;
                                obj.ActTilt = item.ActTilt;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO431.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO431.Count > 0)
                    {
                        db.PRO431.AddRange(lstAddPRO431);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO431 = db.PRO431.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO431.Count > 0)
                    {
                        db.PRO431.RemoveRange(lstDeletePRO431);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO431 = db.PRO431.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO431.Count > 0)
                    {
                        db.PRO431.RemoveRange(lstDeletePRO431);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #endregion

        #region Linkage View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL430 objPRL430 = new PRL430();
            objPRL430 = db.PRL430.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL430 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL430.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstyesnona = clsImplementationEnum.getYesNoNA().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNoNAEnum = lstyesnona.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL431> lstPRL431 = db.PRL431.Where(x => x.HeaderId == objPRL430.HeaderId).ToList();

            ViewBag.lstPRL431 = lstPRL431;

            #endregion

            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.DrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL430.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL430.ActFilledBy) && (objPRL430.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL430.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL430);
        }

        [HttpPost]
        public ActionResult LinkageMobile(int? id, int? m, string APIRequestFrom = "", string UserName = "")
        {
            string ProjectDesc = "";
            string ManufacturingCode = "";
            string AccessRole = "";
            bool isEditable = false;
            var DrawingNo = new List<string>();
            bool isQCRemarkEditable = false;

            PRL430 objPRL430 = new PRL430();
            db.Configuration.ProxyCreationEnabled = false;
            objPRL430 = db.PRL430.Where(i => i.HeaderId == id).FirstOrDefault();
            db.Entry(objPRL430).State = System.Data.Entity.EntityState.Detached;
            
            NDEModels objNDEModels = new NDEModels();
            if (objPRL430 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL430.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
            AccessRole = objUserRoleAccessDetails.UserDesignation;
            
            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstyesnona = clsImplementationEnum.getYesNoNA().ToList();

            var YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var YesNoNAEnum = lstyesnona.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL431> lstPRL431 = db.PRL431.Where(x => x.HeaderId == objPRL430.HeaderId).ToList();


            #endregion
            try
            {
                isEditable = false;
                isQCRemarkEditable = false;
                if (m == 1)
                {
                    isEditable = true;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL430.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL430.ActFilledBy) && (objPRL430.RequestNo.HasValue))
                        {
                            isEditable = false;
                            isQCRemarkEditable = true;
                        }
                        else
                        {
                            DrawingNo = (new GeneralController()).GetPLMDrawingNumber(objPRL430.Project).ToList();
                        }
                    }
                }
                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    ProjectDesc = ProjectDesc,
                    ManufacturingCode = ManufacturingCode,
                    isEditable = isEditable,
                    isQCRemarkEditable = isQCRemarkEditable,
                    DrawingNo = DrawingNo,
                    AccessRole = AccessRole,
                    YesNoEnum = YesNoEnum,
                    YesNAEnum = YesNAEnum,
                    YesNoNAEnum = YesNoNAEnum,

                    objPRL430 = objPRL430,
                    lstPRL431 = lstPRL431
                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                return Json(new
                {
                    res = "0",
                    msg = ex.Message,
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL430 prl430, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = prl430.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL430 objPRL430 = db.PRL430.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL430.DrawingRevisionNo = prl430.DrawingRevisionNo;
                    objPRL430.DrawingNo = prl430.DrawingNo;
                    objPRL430.DCRNo = prl430.DCRNo;
                    objPRL430.ProtocolNo = prl430.ProtocolNo;

                    objPRL430.IsBoltHolesStraddledFor1Flange = prl430.IsBoltHolesStraddledFor1Flange;
                    objPRL430.IsBoltHolesStraddledFor2Flange = prl430.IsBoltHolesStraddledFor2Flange;
                    objPRL430.ReqTolTiltingOf1Flange = prl430.ReqTolTiltingOf1Flange;
                    objPRL430.ReqTolTiltingOf2Flange = prl430.ReqTolTiltingOf2Flange;
                    objPRL430.ReqTolBoltHolesStraddledFor1Flange = prl430.ReqTolBoltHolesStraddledFor1Flange;
                    objPRL430.ReqTolBoltHolesStraddledFor2Flange = prl430.ReqTolBoltHolesStraddledFor2Flange;
                    objPRL430.ActBoltHolesStraddledFor1Flange = prl430.ActBoltHolesStraddledFor1Flange;
                    objPRL430.ActBoltHolesStraddledFor2Flange = prl430.ActBoltHolesStraddledFor2Flange;
                    objPRL430.ActTiltingOf1Flange = prl430.ActTiltingOf1Flange;
                    objPRL430.ActTiltingOf2Flange = prl430.ActTiltingOf2Flange;
                    objPRL430.ReqOrientation2FlangeWithRes1Flange = prl430.ReqOrientation2FlangeWithRes1Flange;
                    objPRL430.ActOrientation2FlangeWithRes1Flange = prl430.ActOrientation2FlangeWithRes1Flange;
                    objPRL430.ReqElevationOf2FlangeFromBottomface1Flange = prl430.ReqElevationOf2FlangeFromBottomface1Flange;
                    objPRL430.ActElevationOf2FlangeFromBottomface1Flange = prl430.ActElevationOf2FlangeFromBottomface1Flange;
                    objPRL430.ReqProjecttionOf2FlangeFromCenter1Flange = prl430.ReqProjecttionOf2FlangeFromCenter1Flange;
                    objPRL430.ActProjecttionOf2FlangeFromCenter1Flange = prl430.ActProjecttionOf2FlangeFromCenter1Flange;
                    objPRL430.CheckPoint1 = prl430.CheckPoint1;
                    objPRL430.CheckPoint3 = prl430.CheckPoint3;
                    objPRL430.CheckPoint4 = prl430.CheckPoint4;
                    objPRL430.CheckPoint5 = prl430.CheckPoint5;
                    objPRL430.CheckPoint6 = prl430.CheckPoint6;
                    objPRL430.CheckPoint7 = prl430.CheckPoint7;
                    objPRL430.CheckPoint8 = prl430.CheckPoint8;
                    objPRL430.CheckPoint9 = prl430.CheckPoint9;
                    objPRL430.CheckPoint10 = prl430.CheckPoint10;
                    objPRL430.CheckPoint10_2 = prl430.CheckPoint10_2;

                    objPRL430.QCRemarks = prl430.QCRemarks;
                    objPRL430.Result = prl430.Result;

                    objPRL430.EditedBy = UserName;
                    objPRL430.EditedOn = DateTime.Now;
                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL430.ActFilledBy = UserName;
                            objPRL430.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL430.ReqFilledBy = UserName;
                            objPRL430.ReqFilledOn = DateTime.Now;
                        }
                    }
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL430.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL430 objPRL430 = db.PRL430.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL430 != null && string.IsNullOrWhiteSpace(objPRL430.ProtocolNo))
                    {
                        db.PRL430.Remove(objPRL430);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL430.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL431> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL431> lstAddPRL431 = new List<PRL431>();
                List<PRL431> lstDeletePRL431 = new List<PRL431>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL431 obj = db.PRL431.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL431();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Description = item.Description;
                        obj.ReqElevation = item.ReqElevation;
                        obj.ActElevation = item.ActElevation;
                        obj.ReqOrientation = item.ReqOrientation;
                        obj.ActOrientation = item.ActOrientation;
                        obj.ReqProjection = item.ReqProjection;
                        obj.ActProjection = item.ActProjection;
                        obj.ReqSize = item.ReqSize;
                        obj.ActSize = item.ActSize;
                        obj.ReqTilt = item.ReqTilt;
                        obj.ActTilt = item.ActTilt;

                        if (isAdded)
                        {
                            lstAddPRL431.Add(obj);
                        }
                    }
                    if (lstAddPRL431.Count > 0)
                    {
                        db.PRL431.AddRange(lstAddPRL431);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL431 = db.PRL431.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL431.Count > 0)
                    {
                        db.PRL431.RemoveRange(lstDeletePRL431);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL431 = db.PRL431.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL431.Count > 0)
                    {
                        db.PRL431.RemoveRange(lstDeletePRL431);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #endregion
    }
}