﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;


namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol066Controller : clsBase
    {
        // GET: PROTOCOL/Protocol066
        string ControllerURL = "/PROTOCOL/Protocol066/";
        string Title = "REPORT OF FINAL MACHINING INSPECTION OF TUBESHEET";

        #region Details View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO325 objPRO325 = new PRO325();
            if (!id.HasValue)
            {
                try
                {
                    objPRO325.ProtocolNo = string.Empty;
                    objPRO325.CreatedBy = objClsLoginInfo.UserName;
                    objPRO325.CreatedOn = DateTime.Now;

                    #region  Characteristics | PRO326
                    objPRO325.PRO326.Add(new PRO326
                    {
                        SrNo = 1,
                        Characteristics = "IDENTIFICATION",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO325.PRO326.Add(new PRO326
                    {
                        SrNo = 2,
                        Characteristics = "VISUAL INSPECTION",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO325.PRO326.Add(new PRO326
                    {
                        SrNo = 3,
                        Characteristics = "LAYOUT OF TUBE HOLES AS PER DRG.",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO325.PRO326.Add(new PRO326
                    {
                        SrNo = 4,
                        Characteristics = "TOTAL NO OF TUBE HOLES.",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO325.PRO326.Add(new PRO326
                    {
                        SrNo = 5,
                        Characteristics = "GO GAUGING OF HOLES.",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO325.PRO326.Add(new PRO326
                    {
                        SrNo = 6,
                        Characteristics = "NO GO GAUGING OF HOLES.",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });
                    objPRO325.PRO326.Add(new PRO326
                    {
                        SrNo = 7,
                        Characteristics = "O T L OF TUBE HOLES",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });
                    objPRO325.PRO326.Add(new PRO326
                    {
                        SrNo = 8,
                        Characteristics = "VISUAL INSPECTION OF TUBE HOLE FINISH.",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });
                    objPRO325.PRO326.Add(new PRO326
                    {
                        SrNo = 9,
                        Characteristics = "BOTH SIDE HOLES SHARP CORNER DEBARRING.",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });
                    objPRO325.PRO326.Add(new PRO326
                    {
                        SrNo = 10,
                        Characteristics = " \"J\" PREPARATION",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });
                    objPRO325.PRO326.Add(new PRO326
                    {
                        SrNo = 11,
                        Characteristics = "\"J\" GROOVE DEPTH",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });
                    objPRO325.PRO326.Add(new PRO326
                    {
                        SrNo = 12,
                        Characteristics = " J' GROOVE  WIDTH AT TOP",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });
                    objPRO325.PRO326.Add(new PRO326
                    {
                        SrNo = 13,
                        Characteristics = "GO GAUGE  FOR \"J' GROOVES",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });
                    objPRO325.PRO326.Add(new PRO326
                    {
                        SrNo = 14,
                        Characteristics = "NO GO GAUGE FOR \"J' GROOVES",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });
                    objPRO325.PRO326.Add(new PRO326
                    {
                        SrNo = 15,
                        Characteristics = "TUBE HOLES SIZE",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });
                    objPRO325.PRO326.Add(new PRO326
                    {
                        SrNo = 16,
                        Characteristics = "STEPS IN TUBE HOLES AS PER DRAWING",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });
                    objPRO325.PRO326.Add(new PRO326
                    {
                        SrNo = 17,
                        Characteristics = "TUBE HOLES PITCH",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });
                    objPRO325.PRO326.Add(new PRO326
                    {
                        SrNo = 18,
                        Characteristics = "TUBE HOLES  LIGAMENT.",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });
                    objPRO325.PRO326.Add(new PRO326
                    {
                        SrNo = 19,
                        Characteristics = "TOTAL THICKNESS",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });
                    objPRO325.PRO326.Add(new PRO326
                    {
                        SrNo = 20,
                        Characteristics = "TUBE SHEET OD",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });
                    objPRO325.PRO326.Add(new PRO326
                    {
                        SrNo = 21,
                        Characteristics = "TUBE SHEET THICKNESS AFTER M/C",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });
                    objPRO325.PRO326.Add(new PRO326
                    {
                        SrNo = 22,
                        Characteristics = "CHANNEL SIDE TUBE SHEET LIP I.D",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });
                    objPRO325.PRO326.Add(new PRO326
                    {
                        SrNo = 23,
                        Characteristics = "SHELL SIDE TUBE SHEET LIP I.D",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });
                    objPRO325.PRO326.Add(new PRO326
                    {
                        SrNo = 24,
                        Characteristics = "WEP OF LIP AS PER DRAWING",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });
                    objPRO325.PRO326.Add(new PRO326
                    {
                        SrNo = 25,
                        Characteristics = "STEPS ON TUBESHEET THICKNESS AS PER DRW",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });
                    objPRO325.PRO326.Add(new PRO326
                    {
                        SrNo = 26,
                        Characteristics = "NUMBER OF THREADED HOLES FOR TIE ROD",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });
                    objPRO325.PRO326.Add(new PRO326
                    {
                        SrNo = 27,
                        Characteristics = "POSITION OF THREADED HOLES FOR TIE ROD",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });
                    objPRO325.PRO326.Add(new PRO326
                    {
                        SrNo = 28,
                        Characteristics = "GO - NOGO GAUGING FOR THREADED HOLES",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });
                    objPRO325.PRO326.Add(new PRO326
                    {
                        SrNo = 29,
                        Characteristics = "EXPANSION GROOVE LENGTH DIMENSIONS",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });
                    objPRO325.PRO326.Add(new PRO326
                    {
                        SrNo = 30,
                        Characteristics = "EXPANSION DIAMETER DIMENSIONS",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });
                    objPRO325.PRO326.Add(new PRO326
                    {
                        SrNo = 31,
                        Characteristics = "NUMBER OF IMPINGEMENT HOLES",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });
                    objPRO325.PRO326.Add(new PRO326
                    {
                        SrNo = 32,
                        Characteristics = "DIMENSIONS OF IMPINGEMENT HOLES",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });
                    objPRO325.PRO326.Add(new PRO326
                    {
                        SrNo = 33,
                        Characteristics = "OUTER MOST HOLES TO TUBESHEET OD DISTANCE",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });

                    #endregion

                    db.PRO325.Add(objPRO325);
                    db.SaveChanges();
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO325.HeaderId;
            }
            else
            {
                objPRO325 = db.PRO325.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO326> lstPRO326 = db.PRO326.Where(x => x.HeaderId == objPRO325.HeaderId).ToList();
            ViewBag.lstPRO326 = lstPRO326;

            List<PRO327> lstPRO327 = db.PRO327.Where(x => x.HeaderId == objPRO325.HeaderId).ToList();
            ViewBag.lstPRO327 = lstPRO327;

            List<PRO328> _lstPRO328 = db.PRO328.Where(x => x.HeaderId == objPRO325.HeaderId).ToList();
            ViewBag._lstPRO328 = _lstPRO328;
            #endregion

            return View(objPRO325);
        }

        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO325 PRO325)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO325.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO325 objPRO325 = db.PRO325.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO325.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO325.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data
                        objPRO325.ProtocolNo = PRO325.ProtocolNo;
                        objPRO325.InspectionStage = PRO325.InspectionStage;
                        objPRO325.CheckPoint1 = PRO325.CheckPoint1;
                        objPRO325.CheckPoint2 = PRO325.CheckPoint2;
                        objPRO325.CheckPoint3 = PRO325.CheckPoint3;
                        objPRO325.CheckPoint3_2 = PRO325.CheckPoint3_2;
                        objPRO325.QCRemarks = PRO325.QCRemarks;
                        objPRO325.Result = PRO325.Result;
                        //objPRO325.EditedBy = PRO325.EditedBy;
                        //objPRO325.EditedOn = PRO325.EditedOn;

                        objPRO325.CheckPoint3_1_1 = PRO325.CheckPoint3_1_1;
                        objPRO325.CheckPoint3_1_2 = PRO325.CheckPoint3_1_2;
                        objPRO325.CheckPoint3_2_1 = PRO325.CheckPoint3_2_1;
                        objPRO325.CheckPoint3_2_2 = PRO325.CheckPoint3_2_2;
                        objPRO325.CheckPoint3_3_1 = PRO325.CheckPoint3_3_1;
                        objPRO325.CheckPoint3_3_2 = PRO325.CheckPoint3_3_2;
                        objPRO325.CheckPoint3_4_1 = PRO325.CheckPoint3_4_1;
                        objPRO325.CheckPoint3_4_2 = PRO325.CheckPoint3_4_2;
                        objPRO325.CheckPoint3_5_1 = PRO325.CheckPoint3_5_1;
                        objPRO325.CheckPoint3_5_2 = PRO325.CheckPoint3_5_2;

                        objPRO325.EditedBy = objClsLoginInfo.UserName;
                        objPRO325.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO325.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO325 objPRO325 = db.PRO325.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO325 != null && string.IsNullOrWhiteSpace(objPRO325.ProtocolNo))
                    {
                        db.PRO325.Remove(objPRO325);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO325.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO326> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO326> lstAddPRO326 = new List<PRO326>();
                List<PRO326> lstDeletePRO326 = new List<PRO326>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO326 obj = db.PRO326.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO326();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SrNo = item.SrNo;
                        obj.Characteristics = item.Characteristics;
                        obj.Required = item.Required;
                        obj.Tolerance = item.Tolerance;
                        obj.Actual = item.Actual;
                        obj.Remarks = item.Remarks;

                        if (isAdded)
                        {
                            lstAddPRO326.Add(obj);
                        }
                    }
                    if (lstAddPRO326.Count > 0)
                    {
                        db.PRO326.AddRange(lstAddPRO326);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO326 = db.PRO326.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO326.Count > 0)
                    {
                        db.PRO326.RemoveRange(lstDeletePRO326);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO326 = db.PRO326.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO326.Count > 0)
                    {
                        db.PRO326.RemoveRange(lstDeletePRO326);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details

        [HttpPost]
        public JsonResult SaveProtocolLine2(List<PRO327> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO327> lstAddPRO327 = new List<PRO327>();
                List<PRO327> lstDeletePRO327 = new List<PRO327>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO327 obj = db.PRO327.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO327();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.MaterialIdentification = item.MaterialIdentification;

                        if (isAdded)
                        {
                            lstAddPRO327.Add(obj);
                        }
                    }
                    if (lstAddPRO327.Count > 0)
                    {
                        db.PRO327.AddRange(lstAddPRO327);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO327 = db.PRO327.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO327.Count > 0)
                    {
                        db.PRO327.RemoveRange(lstDeletePRO327);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO327 = db.PRO327.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO327.Count > 0)
                    {
                        db.PRO327.RemoveRange(lstDeletePRO327);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3(List<PRO328> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO328> lstAddPRO328 = new List<PRO328>();
                List<PRO328> lstDeletePRO328 = new List<PRO328>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO328 obj = db.PRO328.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.DrgNo = item.DrgNo;
                                obj.RevNo = item.RevNo;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO328 obj = new PRO328();

                            if (!string.IsNullOrWhiteSpace(item.DrgNo))
                            {
                                obj.HeaderId = item.HeaderId;
                                obj.DrgNo = item.DrgNo;
                                obj.RevNo = item.RevNo;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO328.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO328.Count > 0)
                    {
                        db.PRO328.AddRange(lstAddPRO328);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO328 = db.PRO328.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO328.Count > 0)
                    {
                        db.PRO328.RemoveRange(lstDeletePRO328);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO328 = db.PRO328.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO328.Count > 0)
                    {
                        db.PRO328.RemoveRange(lstDeletePRO328);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

        #region  Linkage View Code   

        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL325 objPRL325 = new PRL325();
            objPRL325 = db.PRL325.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL325 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL325.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL326> lstPRL326 = db.PRL326.Where(x => x.HeaderId == objPRL325.HeaderId).ToList();

            List<PRL327> lstPRL327 = db.PRL327.Where(x => x.HeaderId == objPRL325.HeaderId).ToList();

            ViewBag.lstPRL326 = lstPRL326;
            ViewBag.lstPRL327 = lstPRL327;

            List<PRL328> _lstPRL328 = db.PRL328.Where(x => x.HeaderId == objPRL325.HeaderId).ToList();
            ViewBag._lstPRL328 = _lstPRL328;

            #endregion

            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.DrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL325.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL325.ActFilledBy) && (objPRL325.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL325.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL325);
        }

        [HttpPost]
        public ActionResult LinkageMobile(int? id, int? m, string APIRequestFrom = "", string UserName = "")
        {
            string ProjectDesc = "";
            string ManufacturingCode = "";
            string AccessRole = "";
            bool isEditable = false;
            var DrawingNo = new List<string>();
            bool isQCRemarkEditable = false;

            PRL325 objPRL325 = new PRL325();
            db.Configuration.ProxyCreationEnabled = false;
            objPRL325 = db.PRL325.Where(i => i.HeaderId == id).FirstOrDefault();
            db.Entry(objPRL325).State = System.Data.Entity.EntityState.Detached;
            NDEModels objNDEModels = new NDEModels();
            if (objPRL325 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL325.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
            AccessRole = objUserRoleAccessDetails.UserDesignation;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            var YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL326> lstPRL326 = db.PRL326.Where(x => x.HeaderId == objPRL325.HeaderId).ToList();

            List<PRL327> lstPRL327 = db.PRL327.Where(x => x.HeaderId == objPRL325.HeaderId).ToList();
            

            #endregion
            try
            {
                isEditable = false;
                isQCRemarkEditable = false;
                if (m == 1)
                {
                    isEditable = true;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL325.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL325.ActFilledBy) && (objPRL325.RequestNo.HasValue))
                        {
                            isEditable = false;
                            isQCRemarkEditable = true;
                        }
                        else
                        {
                            DrawingNo = (new GeneralController()).GetPLMDrawingNumber(objPRL325.Project).ToList();
                        }
                    }
                }
                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    ProjectDesc = ProjectDesc,
                    ManufacturingCode = ManufacturingCode,
                    isEditable = isEditable,
                    isQCRemarkEditable = isQCRemarkEditable,
                    DrawingNo = DrawingNo,
                    AccessRole = AccessRole,
                    YesNoEnum = YesNoEnum,
                    LeftRight = LeftRight,
                    OutsideInside = OutsideInside,
                    YesNAEnum = YesNAEnum,

                    objPRL325 = objPRL325,


                    lstPRL326 = lstPRL326,
                    lstPRL327 = lstPRL327

                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                return Json(new
                {
                    res = "0",
                    msg = ex.Message,
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL325 PRL325, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = PRL325.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL325 objPRL325 = db.PRL325.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL325.DrawingRevisionNo = PRL325.DrawingRevisionNo;
                    objPRL325.DrawingNo = PRL325.DrawingNo;
                    objPRL325.DCRNo = PRL325.DCRNo;
                    objPRL325.ProtocolNo = PRL325.ProtocolNo;
                    objPRL325.InspectionStage = PRL325.InspectionStage;
                    objPRL325.CheckPoint1 = PRL325.CheckPoint1;
                    objPRL325.CheckPoint2 = PRL325.CheckPoint2;
                    objPRL325.CheckPoint3 = PRL325.CheckPoint3;
                    objPRL325.CheckPoint3_2 = PRL325.CheckPoint3_2;
                    objPRL325.QCRemarks = PRL325.QCRemarks;
                    objPRL325.Result = PRL325.Result;

                    objPRL325.CheckPoint3_1_1 = PRL325.CheckPoint3_1_1;
                    objPRL325.CheckPoint3_1_2 = PRL325.CheckPoint3_1_2;
                    objPRL325.CheckPoint3_2_1 = PRL325.CheckPoint3_2_1;
                    objPRL325.CheckPoint3_2_2 = PRL325.CheckPoint3_2_2;
                    objPRL325.CheckPoint3_3_1 = PRL325.CheckPoint3_3_1;
                    objPRL325.CheckPoint3_3_2 = PRL325.CheckPoint3_3_2;
                    objPRL325.CheckPoint3_4_1 = PRL325.CheckPoint3_4_1;
                    objPRL325.CheckPoint3_4_2 = PRL325.CheckPoint3_4_2;
                    objPRL325.CheckPoint3_5_1 = PRL325.CheckPoint3_5_1;
                    objPRL325.CheckPoint3_5_2 = PRL325.CheckPoint3_5_2;

                    objPRL325.EditedBy = UserName;
                    objPRL325.EditedOn = DateTime.Now;

                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL325.ActFilledBy = UserName;
                            objPRL325.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL325.ReqFilledBy = UserName;
                            objPRL325.ReqFilledOn = DateTime.Now;
                        }
                    }

                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL325.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL325 objPRL325 = db.PRL325.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL325 != null && string.IsNullOrWhiteSpace(objPRL325.ProtocolNo))
                    {
                        db.PRL325.Remove(objPRL325);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL325.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL326> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL326> lstAddPRL326 = new List<PRL326>();
                List<PRL326> lstDeletePRL326 = new List<PRL326>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL326 obj = db.PRL326.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL326();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SrNo = item.SrNo;
                        obj.Characteristics = item.Characteristics;
                        obj.Required = item.Required;
                        obj.Tolerance = item.Tolerance;
                        obj.Actual = item.Actual;
                        obj.Remarks = item.Remarks;

                        if (isAdded)
                        {
                            lstAddPRL326.Add(obj);
                        }
                    }
                }
                if (lstAddPRL326.Count > 0)
                {
                    db.PRL326.AddRange(lstAddPRL326);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL326 = db.PRL326.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL326 = db.PRL326.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL326.Count > 0)
                {
                    db.PRL326.RemoveRange(lstDeletePRL326);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2Linkage(List<PRL327> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL327> lstAddPRL327 = new List<PRL327>();
                List<PRL327> lstDeletePRL327 = new List<PRL327>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL327 obj = db.PRL327.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL327();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.MaterialIdentification = item.MaterialIdentification;


                        if (isAdded)
                        {
                            lstAddPRL327.Add(obj);
                        }
                    }
                }
                if (lstAddPRL327.Count > 0)
                {
                    db.PRL327.AddRange(lstAddPRL327);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL327 = db.PRL327.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL327 = db.PRL327.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL327.Count > 0)
                {
                    db.PRL327.RemoveRange(lstDeletePRL327);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3Linkage(List<PRL328> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRL328> lstAddPRL328 = new List<PRL328>();
                List<PRL328> lstDeletePRL328 = new List<PRL328>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRL328 obj = db.PRL328.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.DrgNo = item.DrgNo;
                                obj.RevNo = item.RevNo;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRL328 obj = new PRL328();

                            if (!string.IsNullOrWhiteSpace(item.DrgNo))
                            {
                                obj.HeaderId = item.HeaderId;
                                obj.DrgNo = item.DrgNo;
                                obj.RevNo = item.RevNo;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRL328.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRL328.Count > 0)
                    {
                        db.PRL328.AddRange(lstAddPRL328);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRL328 = db.PRL328.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRL328.Count > 0)
                    {
                        db.PRL328.RemoveRange(lstDeletePRL328);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL328 = db.PRL328.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL328.Count > 0)
                    {
                        db.PRL328.RemoveRange(lstDeletePRL328);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion
    }
}