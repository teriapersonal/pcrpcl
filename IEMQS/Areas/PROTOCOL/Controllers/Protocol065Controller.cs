﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol065Controller : clsBase
    {
        // GET: PROTOCOL/Protocol065
        string ControllerURL = "/PROTOCOL/Protocol065/";
        string Title = "VISUAL AND DIMENSION REPORT FOR  LONGITUDINAL SEAM OF TUBESHEET AND BAFFLE";

        #region Details View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO320 objPRO320 = new PRO320();
            if (!id.HasValue)
            {
                try
                {
                    objPRO320.ProtocolNo = string.Empty;
                    objPRO320.CreatedBy = objClsLoginInfo.UserName;
                    objPRO320.CreatedOn = DateTime.Now;

                    #region ORIENTATION 
                    objPRO320.PRO321.Add(new PRO321
                    {
                        Orientation = "0°-180°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO320.PRO321.Add(new PRO321
                    {
                        Orientation = "45°-225°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO320.PRO321.Add(new PRO321
                    {
                        Orientation = "90°-270°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO320.PRO321.Add(new PRO321
                    {
                        Orientation = "135°-315°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion

                    #region ORIENTATION FLATNESS  
                    objPRO320.PRO322.Add(new PRO322
                    {
                        Location = "0°-180°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO320.PRO322.Add(new PRO322
                    {
                        Location = "45°-225°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO320.PRO322.Add(new PRO322
                    {
                        Location = "90°-270°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO320.PRO322.Add(new PRO322
                    {
                        Location = "135°-315°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });

                    #endregion

                    db.PRO320.Add(objPRO320);
                    db.SaveChanges();
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO320.HeaderId;
            }
            else
            {
                objPRO320 = db.PRO320.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstyesnona = clsImplementationEnum.getYesNoNA().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNoNAEnum = lstyesnona.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO321> lstPRO321 = db.PRO321.Where(x => x.HeaderId == objPRO320.HeaderId).ToList();
            List<PRO322> lstPRO322 = db.PRO322.Where(x => x.HeaderId == objPRO320.HeaderId).ToList();
            List<PRO323> lstPRO323 = db.PRO323.Where(x => x.HeaderId == objPRO320.HeaderId).ToList();


            ViewBag.lstPRO321 = lstPRO321;
            ViewBag.lstPRO322 = lstPRO322;
            ViewBag.lstPRO323 = lstPRO323;

            #endregion

            return View(objPRO320);
        }

        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO320 PRO320)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO320.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO320 objPRO320 = db.PRO320.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO320.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO320.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data


                        objPRO320.ProtocolNo = PRO320.ProtocolNo;
                        objPRO320.ReqOutsideDiameter = PRO320.ReqOutsideDiameter;
                        objPRO320.ReqInsideDiameter = PRO320.ReqInsideDiameter;
                        objPRO320.FlatnessReadings = PRO320.FlatnessReadings;
                        objPRO320.OutByLocation = PRO320.OutByLocation;
                        objPRO320.FlatnessReadings1 = PRO320.FlatnessReadings1;
                        objPRO320.FlatnessReadings2 = PRO320.FlatnessReadings2;
                        objPRO320.FlatnessReadings3 = PRO320.FlatnessReadings3;
                        objPRO320.FlatnessReadings4 = PRO320.FlatnessReadings4;
                        objPRO320.FlatnessReadings5 = PRO320.FlatnessReadings5;
                        objPRO320.FlatnessReadings6 = PRO320.FlatnessReadings6;
                        objPRO320.FlatnessReadings7 = PRO320.FlatnessReadings7;
                        objPRO320.FlatnessReadings8 = PRO320.FlatnessReadings8;
                        objPRO320.FlatnessReadings9 = PRO320.FlatnessReadings9;
                        objPRO320.FlatnessReadings10 = PRO320.FlatnessReadings10;
                        objPRO320.FlatnessReadings11 = PRO320.FlatnessReadings11;
                        objPRO320.FlatnessReadings12 = PRO320.FlatnessReadings12;
                        objPRO320.CheckPoint1 = PRO320.CheckPoint1;
                        objPRO320.CheckPoint2 = PRO320.CheckPoint2;
                        objPRO320.CheckPoint3 = PRO320.CheckPoint3;
                        objPRO320.CheckPoint4 = PRO320.CheckPoint4;
                        objPRO320.CheckPoint5 = PRO320.CheckPoint5;
                        objPRO320.CheckPoint7 = PRO320.CheckPoint7;
                        objPRO320.CheckPoint7_2 = PRO320.CheckPoint7_2;
                        objPRO320.QCRemarks = PRO320.QCRemarks;
                        objPRO320.Result = PRO320.Result;

                        objPRO320.EditedBy = objClsLoginInfo.UserName;
                        objPRO320.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO320.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO320 objPRO320 = db.PRO320.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO320 != null && string.IsNullOrWhiteSpace(objPRO320.ProtocolNo))
                    {
                        db.PRO320.Remove(objPRO320);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO320.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO321> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO321> lstAddPRO321 = new List<PRO321>();
                List<PRO321> lstDeletePRO321 = new List<PRO321>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO321 obj = db.PRO321.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO321();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Orientation = item.Orientation;
                        obj.ActOutsideDiameter = item.ActOutsideDiameter;
                        obj.ActInsideDiameter = item.ActInsideDiameter;

                        if (isAdded)
                        {
                            lstAddPRO321.Add(obj);
                        }
                    }
                    if (lstAddPRO321.Count > 0)
                    {
                        db.PRO321.AddRange(lstAddPRO321);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO321 = db.PRO321.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO321.Count > 0)
                    {
                        db.PRO321.RemoveRange(lstDeletePRO321);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO321 = db.PRO321.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO321.Count > 0)
                    {
                        db.PRO321.RemoveRange(lstDeletePRO321);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2(List<PRO322> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO322> lstAddPRO322 = new List<PRO322>();
                List<PRO322> lstDeletePRO322 = new List<PRO322>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO322 obj = db.PRO322.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.Location = item.Location;
                                obj.ActualValue1 = item.ActualValue1;
                                obj.ActualValue2 = item.ActualValue2;
                                obj.ActualValue3 = item.ActualValue3;
                                obj.ActualValue4 = item.ActualValue4;
                                obj.ActualValue5 = item.ActualValue5;
                                obj.ActualValue6 = item.ActualValue6;
                                obj.ActualValue7 = item.ActualValue7;
                                obj.ActualValue8 = item.ActualValue8;
                                obj.ActualValue9 = item.ActualValue9;
                                obj.ActualValue10 = item.ActualValue10;
                                obj.ActualValue11 = item.ActualValue11;
                                obj.ActualValue12 = item.ActualValue12;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO322 obj = new PRO322();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.Location))
                            {
                                obj.Location = item.Location;
                                obj.ActualValue1 = item.ActualValue1;
                                obj.ActualValue2 = item.ActualValue2;
                                obj.ActualValue3 = item.ActualValue3;
                                obj.ActualValue4 = item.ActualValue4;
                                obj.ActualValue5 = item.ActualValue5;
                                obj.ActualValue6 = item.ActualValue6;
                                obj.ActualValue7 = item.ActualValue7;
                                obj.ActualValue8 = item.ActualValue8;
                                obj.ActualValue9 = item.ActualValue9;
                                obj.ActualValue10 = item.ActualValue10;
                                obj.ActualValue11 = item.ActualValue11;
                                obj.ActualValue12 = item.ActualValue12;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO322.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO322.Count > 0)
                    {
                        db.PRO322.AddRange(lstAddPRO322);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO322 = db.PRO322.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO322.Count > 0)
                    {
                        db.PRO322.RemoveRange(lstDeletePRO322);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO322 = db.PRO322.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO322.Count > 0)
                    {
                        db.PRO322.RemoveRange(lstDeletePRO322);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3(List<PRO323> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO323> lstAddPRO323 = new List<PRO323>();
                List<PRO323> lstDeletePRO323 = new List<PRO323>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (!string.IsNullOrWhiteSpace(item.IdentificationMarking))
                        {
                            bool isAdded = false;
                            PRO323 obj = db.PRO323.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj == null)
                            {
                                obj = new PRO323();
                                obj.HeaderId = item.HeaderId;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                isAdded = true;
                            }
                            else
                            {
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }

                            obj.IdentificationMarking = item.IdentificationMarking;


                            if (isAdded)
                            {
                                lstAddPRO323.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO323.Count > 0)
                    {
                        db.PRO323.AddRange(lstAddPRO323);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO323 = db.PRO323.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO323.Count > 0)
                    {
                        db.PRO323.RemoveRange(lstDeletePRO323);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO323 = db.PRO323.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO323.Count > 0)
                    {
                        db.PRO323.RemoveRange(lstDeletePRO323);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

        #region Linkage View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL320 objPRL320 = new PRL320();
            objPRL320 = db.PRL320.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL320 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL320.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;

                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstyesnona = clsImplementationEnum.getYesNoNA().ToList();


            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNoNAEnum = lstyesnona.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL321> lstPRL321 = db.PRL321.Where(x => x.HeaderId == objPRL320.HeaderId).ToList();
            List<PRL322> lstPRL322 = db.PRL322.Where(x => x.HeaderId == objPRL320.HeaderId).ToList();
            List<PRL323> lstPRL323 = db.PRL323.Where(x => x.HeaderId == objPRL320.HeaderId).ToList();

            ViewBag.lstPRL321 = lstPRL321;
            ViewBag.lstPRL322 = lstPRL322;
            ViewBag.lstPRL323 = lstPRL323;

            #endregion
            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.DrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL320.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL320.ActFilledBy) && (objPRL320.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL320.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL320);
        }

        [HttpPost]
        public ActionResult LinkageMobile(int? id, int? m, string APIRequestFrom = "", string UserName = "")
        {
            string ProjectDesc = "";
            string ManufacturingCode = "";
            string AccessRole = "";
            bool isEditable = false;
            var DrawingNo = new List<string>();
            bool isQCRemarkEditable = false;

            PRL320 objPRL320 = new PRL320();
            db.Configuration.ProxyCreationEnabled = false;
            objPRL320 = db.PRL320.Where(i => i.HeaderId == id).FirstOrDefault();
            db.Entry(objPRL320).State = System.Data.Entity.EntityState.Detached;
            NDEModels objNDEModels = new NDEModels();
            if (objPRL320 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL320.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }               
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
            AccessRole = objUserRoleAccessDetails.UserDesignation;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstyesnona = clsImplementationEnum.getYesNoNA().ToList();


            var YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var YesNoNAEnum = lstyesnona.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL321> lstPRL321 = db.PRL321.Where(x => x.HeaderId == objPRL320.HeaderId).ToList();
            List<PRL322> lstPRL322 = db.PRL322.Where(x => x.HeaderId == objPRL320.HeaderId).ToList();
            List<PRL323> lstPRL323 = db.PRL323.Where(x => x.HeaderId == objPRL320.HeaderId).ToList();

            #endregion
            try
            {
                isEditable = false;
                isQCRemarkEditable = false;
                if (m == 1)
                {
                    isEditable = true;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL320.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL320.ActFilledBy) && (objPRL320.RequestNo.HasValue))
                        {
                            isEditable = false;
                            isQCRemarkEditable = true;
                        }
                        else
                        {
                            DrawingNo = (new GeneralController()).GetPLMDrawingNumber(objPRL320.Project).ToList();
                        }
                    }
                }
                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    ProjectDesc = ProjectDesc,
                    ManufacturingCode = ManufacturingCode,
                    isEditable = isEditable,
                    isQCRemarkEditable = isQCRemarkEditable,
                    DrawingNo = DrawingNo,
                    AccessRole = AccessRole,
                    YesNoEnum = YesNoEnum,
                    YesNoNAEnum = YesNoNAEnum,
                    YesNAEnum = YesNAEnum,

                    objPRL320 = objPRL320,

                    lstPRL321 = lstPRL321,
                    lstPRL322 = lstPRL322,
                    lstPRL323 = lstPRL323,

                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                return Json(new
                {
                    res = "0",
                    msg = ex.Message,
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL320 PRL320, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = PRL320.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL320 objPRL320 = db.PRL320.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL320.DrawingRevisionNo = PRL320.DrawingRevisionNo;
                    objPRL320.DrawingNo = PRL320.DrawingNo;
                    objPRL320.DCRNo = PRL320.DCRNo;
                    objPRL320.ProtocolNo = PRL320.ProtocolNo != null ? PRL320.ProtocolNo : "";

                    objPRL320.ReqOutsideDiameter = PRL320.ReqOutsideDiameter;
                    objPRL320.ReqInsideDiameter = PRL320.ReqInsideDiameter;
                    objPRL320.FlatnessReadings = PRL320.FlatnessReadings;
                    objPRL320.OutByLocation = PRL320.OutByLocation;
                    objPRL320.FlatnessReadings1 = PRL320.FlatnessReadings1;
                    objPRL320.FlatnessReadings2 = PRL320.FlatnessReadings2;
                    objPRL320.FlatnessReadings3 = PRL320.FlatnessReadings3;
                    objPRL320.FlatnessReadings4 = PRL320.FlatnessReadings4;
                    objPRL320.FlatnessReadings5 = PRL320.FlatnessReadings5;
                    objPRL320.FlatnessReadings6 = PRL320.FlatnessReadings6;
                    objPRL320.FlatnessReadings7 = PRL320.FlatnessReadings7;
                    objPRL320.FlatnessReadings8 = PRL320.FlatnessReadings8;
                    objPRL320.FlatnessReadings9 = PRL320.FlatnessReadings9;
                    objPRL320.FlatnessReadings10 = PRL320.FlatnessReadings10;
                    objPRL320.FlatnessReadings11 = PRL320.FlatnessReadings11;
                    objPRL320.FlatnessReadings12 = PRL320.FlatnessReadings12;
                    objPRL320.CheckPoint1 = PRL320.CheckPoint1;
                    objPRL320.CheckPoint2 = PRL320.CheckPoint2;
                    objPRL320.CheckPoint3 = PRL320.CheckPoint3;
                    objPRL320.CheckPoint4 = PRL320.CheckPoint4;
                    objPRL320.CheckPoint5 = PRL320.CheckPoint5;
                    objPRL320.CheckPoint7 = PRL320.CheckPoint7;
                    objPRL320.CheckPoint7_2 = PRL320.CheckPoint7_2;
                    objPRL320.QCRemarks = PRL320.QCRemarks;
                    objPRL320.Result = PRL320.Result;

                    objPRL320.EditedBy = UserName;
                    objPRL320.EditedOn = DateTime.Now;

                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL320.ActFilledBy = UserName;
                            objPRL320.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL320.ReqFilledBy = UserName;
                            objPRL320.ReqFilledOn = DateTime.Now;
                        }
                    }
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL320.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL320 objPRL320 = db.PRL320.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL320 != null && string.IsNullOrWhiteSpace(objPRL320.ProtocolNo))
                    {
                        db.PRL320.Remove(objPRL320);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL320.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL321> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL321> lstAddPRL321 = new List<PRL321>();
                List<PRL321> lstDeletePRL321 = new List<PRL321>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL321 obj = db.PRL321.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL321();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Orientation = item.Orientation;
                        obj.ActOutsideDiameter = item.ActOutsideDiameter;
                        obj.ActInsideDiameter = item.ActInsideDiameter;

                        if (isAdded)
                        {
                            lstAddPRL321.Add(obj);
                        }
                    }
                    if (lstAddPRL321.Count > 0)
                    {
                        db.PRL321.AddRange(lstAddPRL321);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL321 = db.PRL321.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL321.Count > 0)
                    {
                        db.PRL321.RemoveRange(lstDeletePRL321);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL321 = db.PRL321.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL321.Count > 0)
                    {
                        db.PRL321.RemoveRange(lstDeletePRL321);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine2Linkage(List<PRL322> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL322> lstAddPRL322 = new List<PRL322>();
                List<PRL322> lstDeletePRL322 = new List<PRL322>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL322 obj = db.PRL322.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL322();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        if (!string.IsNullOrWhiteSpace(item.Location))
                        {
                            obj.Location = item.Location;
                            obj.ActualValue1 = item.ActualValue1;
                            obj.ActualValue2 = item.ActualValue2;
                            obj.ActualValue3 = item.ActualValue3;
                            obj.ActualValue4 = item.ActualValue4;
                            obj.ActualValue5 = item.ActualValue5;
                            obj.ActualValue6 = item.ActualValue6;
                            obj.ActualValue7 = item.ActualValue7;
                            obj.ActualValue8 = item.ActualValue8;
                            obj.ActualValue9 = item.ActualValue9;
                            obj.ActualValue10 = item.ActualValue10;
                            obj.ActualValue11 = item.ActualValue11;
                            obj.ActualValue12 = item.ActualValue12;

                            if (isAdded)
                            {
                                lstAddPRL322.Add(obj);
                            }
                        }


                    }
                    if (lstAddPRL322.Count > 0)
                    {
                        db.PRL322.AddRange(lstAddPRL322);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL322 = db.PRL322.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL322.Count > 0)
                    {
                        db.PRL322.RemoveRange(lstDeletePRL322);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL322 = db.PRL322.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL322.Count > 0)
                    {
                        db.PRL322.RemoveRange(lstDeletePRL322);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line3 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine3Linkage(List<PRL323> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL323> lstAddPRL323 = new List<PRL323>();
                List<PRL323> lstDeletePRL323 = new List<PRL323>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (!string.IsNullOrWhiteSpace(item.IdentificationMarking))
                        {

                            bool isAdded = false;
                            PRL323 obj = db.PRL323.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj == null)
                            {
                                obj = new PRL323();
                                obj.HeaderId = item.HeaderId;
                                obj.CreatedBy = UserName;
                                obj.CreatedOn = DateTime.Now;
                                isAdded = true;
                            }
                            else
                            {
                                obj.EditedBy = UserName;
                                obj.EditedOn = DateTime.Now;
                            }

                            obj.IdentificationMarking = item.IdentificationMarking;

                            if (isAdded)
                            {
                                lstAddPRL323.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRL323.Count > 0)
                    {
                        db.PRL323.AddRange(lstAddPRL323);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL323 = db.PRL323.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL323.Count > 0)
                    {
                        db.PRL323.RemoveRange(lstDeletePRL323);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL323 = db.PRL323.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL323.Count > 0)
                    {
                        db.PRL323.RemoveRange(lstDeletePRL323);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

    }
}