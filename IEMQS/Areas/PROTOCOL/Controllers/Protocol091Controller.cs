﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol091Controller : clsBase
    {
        // GET: PROTOCOL/Protocol091
        string ControllerURL = "/PROTOCOL/Protocol091/";
        string Title = "INITIAL MACHINING VISUAL AND DIMENSION REPORT FOR GASKET FACE AND OTHER DIMENSIONS OF NOZZLE";

        #region Details View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO450 objPRO450 = new PRO450();
            if (!id.HasValue)
            {
                try
                {
                    objPRO450.ProtocolNo = string.Empty;
                    objPRO450.CreatedBy = objClsLoginInfo.UserName;
                    objPRO450.CreatedOn = DateTime.Now;

                    #region  MACHINING | PRO452
                    objPRO450.PRO452.Add(new PRO452
                    {
                        SrNo = "1",
                        Description = "Identification",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO450.PRO452.Add(new PRO452
                    {
                        SrNo = "2",
                        Description = "Visual inspection",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO450.PRO452.Add(new PRO452
                    {
                        SrNo = "3",
                        Description = "ID before O/L",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO450.PRO452.Add(new PRO452
                    {
                        SrNo = "4",
                        Description = "ID after O/L",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO450.PRO452.Add(new PRO452
                    {
                        SrNo = "5",
                        Description = "ID Machining up to length",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO450.PRO452.Add(new PRO452
                    {
                        SrNo = "6",
                        Description = "ID Surface finish",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO450.PRO452.Add(new PRO452
                    {
                        SrNo = "7",
                        Description = "Gasket face step OD",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO450.PRO452.Add(new PRO452
                    {
                        SrNo = "8",
                        Description = "Gasket height",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO450.PRO452.Add(new PRO452
                    {
                        SrNo = "9",
                        Description = "Gasket face finish",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO450.PRO452.Add(new PRO452
                    {
                        SrNo = "10",
                        Description = "Gasket step OD radius Top",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO450.PRO452.Add(new PRO452
                    {
                        SrNo = "11",
                        Description = "Gasket step OD radius Bottom",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO450.PRO452.Add(new PRO452
                    {
                        SrNo = "12",
                        Description = "Groove width",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO450.PRO452.Add(new PRO452
                    {
                        SrNo = "13",
                        Description = "Groove depth",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO450.PRO452.Add(new PRO452
                    {
                        SrNo = "14",
                        Description = "Groove corner radius",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO450.PRO452.Add(new PRO452
                    {
                        SrNo = "15",
                        Description = "Groove angle",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO450.PRO452.Add(new PRO452
                    {
                        SrNo = "16",
                        Description = "Groove taper finish",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO450.PRO452.Add(new PRO452
                    {
                        SrNo = "17",
                        Description = "Groove face finish",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO450.PRO452.Add(new PRO452
                    {
                        SrNo = "18",
                        Description = "Gasket groove P.C.D",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO450.PRO452.Add(new PRO452
                    {
                        SrNo = "19",
                        Description = "Radius Gasket face to ID",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO450.PRO452.Add(new PRO452
                    {
                        SrNo = "20",
                        Description = "O/L Thk.",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO450.PRO452.Add(new PRO452
                    {
                        SrNo = "21",
                        Description = "ID to Back face radius",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO450.PRO452.Add(new PRO452
                    {
                        SrNo = "22",
                        Description = "Back face height",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO450.PRO452.Add(new PRO452
                    {
                        SrNo = "23",
                        Description = "Back face visual",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO450.PRO452.Add(new PRO452
                    {
                        SrNo = "24",
                        Description = "Final Visual inspection after O/L",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion

                    db.PRO450.Add(objPRO450);
                    db.SaveChanges();
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO450.HeaderId;
            }
            else
            {
                objPRO450 = db.PRO450.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO451> lstPRO451 = db.PRO451.Where(x => x.HeaderId == objPRO450.HeaderId).ToList();
            List<PRO452> lstPRO452 = db.PRO452.Where(x => x.HeaderId == objPRO450.HeaderId).ToList();

            ViewBag._lstPRO451 = lstPRO451;
            ViewBag._lstPRO452 = lstPRO452;

            #endregion

            return View(objPRO450);
            //return View();
        }

        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO450 PRO450)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO450.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO450 objPRO450 = db.PRO450.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO450.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO450.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data

                        objPRO450.ProtocolNo = PRO450.ProtocolNo;
                        objPRO450.NozzleNo = PRO450.NozzleNo;
                        objPRO450.GasketFaceRaisedFaceSeamNo = PRO450.GasketFaceRaisedFaceSeamNo;
                        objPRO450.PartDescription = PRO450.PartDescription;
                        objPRO450.GasketGrooveSeamNo = PRO450.GasketGrooveSeamNo;
                        objPRO450.ItemNo = PRO450.ItemNo;
                        objPRO450.NozzleBackfaceSeamNoBFS = PRO450.NozzleBackfaceSeamNoBFS;
                        objPRO450.NozzleIDSeamNoIDS = PRO450.NozzleIDSeamNoIDS;
                        objPRO450.NozzleBackfaceIDradiusSeamNoBRS = PRO450.NozzleBackfaceIDradiusSeamNoBRS;

                        objPRO450.CheckPoint1 = PRO450.CheckPoint1;
                        objPRO450.CheckPoint2 = PRO450.CheckPoint2;
                        objPRO450.CheckPoint4_1_1 = PRO450.CheckPoint4_1_1;
                        objPRO450.CheckPoint4_1_2 = PRO450.CheckPoint4_1_2;
                        objPRO450.CheckPoint4_2_1 = PRO450.CheckPoint4_2_1;
                        objPRO450.CheckPoint4_2_2 = PRO450.CheckPoint4_2_2;
                        objPRO450.CheckPoint4_3_1 = PRO450.CheckPoint4_3_1;
                        objPRO450.CheckPoint4_3_2 = PRO450.CheckPoint4_3_2;
                        objPRO450.CheckPoint4_4_1 = PRO450.CheckPoint4_4_1;
                        objPRO450.CheckPoint4_4_2 = PRO450.CheckPoint4_4_2;
                        objPRO450.CheckPoint4_5_1 = PRO450.CheckPoint4_5_1;
                        objPRO450.CheckPoint4_5_2 = PRO450.CheckPoint4_5_2;

                        objPRO450.QCRemarks = PRO450.QCRemarks;
                        objPRO450.Result = PRO450.Result;

                        objPRO450.EditedBy = objClsLoginInfo.UserName;
                        objPRO450.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO450.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO450 objPRO450 = db.PRO450.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO450 != null && string.IsNullOrWhiteSpace(objPRO450.ProtocolNo))
                    {
                        db.PRO450.Remove(objPRO450);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO450.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO451> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO451> lstAddPRO451 = new List<PRO451>();
                List<PRO451> lstDeletePRO451 = new List<PRO451>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO451 obj = db.PRO451.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.DrgNo = item.DrgNo;
                                obj.RevNo = item.RevNo;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO451 obj = new PRO451();

                            if (!string.IsNullOrWhiteSpace(item.DrgNo))
                            {
                                obj.HeaderId = item.HeaderId;
                                obj.DrgNo = item.DrgNo;
                                obj.RevNo = item.RevNo;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO451.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO451.Count > 0)
                    {
                        db.PRO451.AddRange(lstAddPRO451);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO451 = db.PRO451.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO451.Count > 0)
                    {
                        db.PRO451.RemoveRange(lstDeletePRO451);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO451 = db.PRO451.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO451.Count > 0)
                    {
                        db.PRO451.RemoveRange(lstDeletePRO451);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2(List<PRO452> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO452> lstAddPRO452 = new List<PRO452>();
                List<PRO452> lstDeletePRO452 = new List<PRO452>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO452 obj = db.PRO452.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO452();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SrNo = item.SrNo;
                        obj.Description = item.Description;
                        obj.ReqDimension = item.ReqDimension;
                        obj.ActDimension = item.ActDimension;
                        obj.Remarks = item.Remarks;

                        if (isAdded)
                        {
                            lstAddPRO452.Add(obj);
                        }
                    }
                    if (lstAddPRO452.Count > 0)
                    {
                        db.PRO452.AddRange(lstAddPRO452);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO452 = db.PRO452.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO452.Count > 0)
                    {
                        db.PRO452.RemoveRange(lstDeletePRO452);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO452 = db.PRO452.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO452.Count > 0)
                    {
                        db.PRO452.RemoveRange(lstDeletePRO452);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

        #region Linkage View Code

        #region Header
        public ActionResult Linkage(int? id, int? m)
        {
            PRL450 objPRL450 = new PRL450();
            objPRL450 = db.PRL450.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL450 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL450.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL451> lstPRL451 = db.PRL451.Where(x => x.HeaderId == objPRL450.HeaderId).ToList();
            List<PRL452> lstPRL452 = db.PRL452.Where(x => x.HeaderId == objPRL450.HeaderId).ToList();

            ViewBag._lstPRL451 = lstPRL451;
            ViewBag._lstPRL452 = lstPRL452;

            #endregion

            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.DrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL450.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL450.ActFilledBy) && (objPRL450.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL450.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL450);

            //return View();
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL450 prl450, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = prl450.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL450 objPRL450 = db.PRL450.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL450.DrawingRevisionNo = prl450.DrawingRevisionNo;
                    objPRL450.DrawingNo = prl450.DrawingNo;
                    objPRL450.DCRNo = prl450.DCRNo;
                    objPRL450.ProtocolNo = prl450.ProtocolNo;

                    objPRL450.ProtocolNo = prl450.ProtocolNo;
                    objPRL450.NozzleNo = prl450.NozzleNo;
                    objPRL450.GasketFaceRaisedFaceSeamNo = prl450.GasketFaceRaisedFaceSeamNo;
                    objPRL450.PartDescription = prl450.PartDescription;
                    objPRL450.GasketGrooveSeamNo = prl450.GasketGrooveSeamNo;
                    objPRL450.ItemNo = prl450.ItemNo;
                    objPRL450.NozzleBackfaceSeamNoBFS = prl450.NozzleBackfaceSeamNoBFS;
                    objPRL450.NozzleIDSeamNoIDS = prl450.NozzleIDSeamNoIDS;
                    objPRL450.NozzleBackfaceIDradiusSeamNoBRS = prl450.NozzleBackfaceIDradiusSeamNoBRS;

                    objPRL450.CheckPoint1 = prl450.CheckPoint1;
                    objPRL450.CheckPoint2 = prl450.CheckPoint2;
                    objPRL450.CheckPoint4_1_1 = prl450.CheckPoint4_1_1;
                    objPRL450.CheckPoint4_1_2 = prl450.CheckPoint4_1_2;
                    objPRL450.CheckPoint4_2_1 = prl450.CheckPoint4_2_1;
                    objPRL450.CheckPoint4_2_2 = prl450.CheckPoint4_2_2;
                    objPRL450.CheckPoint4_3_1 = prl450.CheckPoint4_3_1;
                    objPRL450.CheckPoint4_3_2 = prl450.CheckPoint4_3_2;
                    objPRL450.CheckPoint4_4_1 = prl450.CheckPoint4_4_1;
                    objPRL450.CheckPoint4_4_2 = prl450.CheckPoint4_4_2;
                    objPRL450.CheckPoint4_5_1 = prl450.CheckPoint4_5_1;
                    objPRL450.CheckPoint4_5_2 = prl450.CheckPoint4_5_2;

                    objPRL450.QCRemarks = prl450.QCRemarks;
                    objPRL450.Result = prl450.Result;

                    objPRL450.EditedBy = UserName;
                    objPRL450.EditedOn = DateTime.Now;
                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL450.ActFilledBy = UserName;
                            objPRL450.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL450.ReqFilledBy = UserName;
                            objPRL450.ReqFilledOn = DateTime.Now;
                        }
                    }
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL450.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL450 objPRL450 = db.PRL450.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL450 != null && string.IsNullOrWhiteSpace(objPRL450.ProtocolNo))
                    {
                        db.PRL450.Remove(objPRL450);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL450.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL451> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRL451> lstAddPRL451 = new List<PRL451>();
                List<PRL451> lstDeletePRL451 = new List<PRL451>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRL451 obj = db.PRL451.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.DrgNo = item.DrgNo;
                                obj.RevNo = item.RevNo;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRL451 obj = new PRL451();

                            if (!string.IsNullOrWhiteSpace(item.DrgNo))
                            {
                                obj.HeaderId = item.HeaderId;
                                obj.DrgNo = item.DrgNo;
                                obj.RevNo = item.RevNo;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRL451.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRL451.Count > 0)
                    {
                        db.PRL451.AddRange(lstAddPRL451);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRL451 = db.PRL451.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRL451.Count > 0)
                    {
                        db.PRL451.RemoveRange(lstDeletePRL451);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL451 = db.PRL451.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL451.Count > 0)
                    {
                        db.PRL451.RemoveRange(lstDeletePRL451);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2Linkage(List<PRL452> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRL452> lstAddPRL452 = new List<PRL452>();
                List<PRL452> lstDeletePRL452 = new List<PRL452>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL452 obj = db.PRL452.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL452();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SrNo = item.SrNo;
                        obj.Description = item.Description;
                        obj.ReqDimension = item.ReqDimension;
                        obj.ActDimension = item.ActDimension;
                        obj.Remarks = item.Remarks;

                        if (isAdded)
                        {
                            lstAddPRL452.Add(obj);
                        }
                    }
                    if (lstAddPRL452.Count > 0)
                    {
                        db.PRL452.AddRange(lstAddPRL452);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL452 = db.PRL452.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL452.Count > 0)
                    {
                        db.PRL452.RemoveRange(lstDeletePRL452);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL452 = db.PRL452.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL452.Count > 0)
                    {
                        db.PRL452.RemoveRange(lstDeletePRL452);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion
    }
}