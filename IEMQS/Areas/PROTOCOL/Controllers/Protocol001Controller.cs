﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol001Controller : clsBase
    {
        string ControllerURL = "/PROTOCOL/Protocol001/";
        // GET: PROTOCOL/Protocol001
        #region Details View

        #region Header Details Page
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO001 objPRO001 = new PRO001();
            if (!id.HasValue)
            {
                objPRO001.ProtocolNo = string.Empty;
                objPRO001.CreatedBy = objClsLoginInfo.UserName;
                objPRO001.CreatedOn = DateTime.Now;
                db.PRO001.Add(objPRO001);
                db.SaveChanges();
                ViewBag.HeaderId = objPRO001.HeaderId;
            }
            else
            {
                objPRO001 = db.PRO001.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            ViewBag.YesNoEnum = new List<CategoryData> { new CategoryData { Value = "Yes", Code = "Yes", CategoryDescription = "Yes" }, new CategoryData { Value = "No", Code = "No", CategoryDescription = "No" } };
            ViewBag.YesNAEnum = new List<CategoryData> { new CategoryData { Value = "Yes", Code = "Yes", CategoryDescription = "Yes" }, new CategoryData { Value = "NA", Code = "NA", CategoryDescription = "NA" } };
            ViewBag.YesNoNAEnum = new List<CategoryData> { new CategoryData { Value = "Yes", Code = "Yes", CategoryDescription = "Yes" }, new CategoryData { Value = "No", Code = "No", CategoryDescription = "No" }, new CategoryData { Value = "NA", Code = "NA", CategoryDescription = "NA" } };
            ViewBag.RTUTExtentEnum = new List<CategoryData> { new CategoryData { Value = "100%", Code = "100%", CategoryDescription = "100%" }, new CategoryData { Value = "SPOT", Code = "SPOT", CategoryDescription = "SPOT" }, new CategoryData { Value = "NA", Code = "NA", CategoryDescription = "NA" } };
            ViewBag.TOFDUTExtentEnum = new List<CategoryData> { new CategoryData { Value = "100%", Code = "100%", CategoryDescription = "100%" }, new CategoryData { Value = "NA", Code = "NA", CategoryDescription = "NA" } };

            UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = "SET-UP & DIMENSION REPORT FOR ROLLED SHELL LONGITUDINAL SEAM";
            ViewBag.ControllerURL = ControllerURL;
            return View(objPRO001);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO001 objPRO001 = db.PRO001.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRO001.ProtocolNo = Convert.ToString(fc["ProtocolNo"]);
                    objPRO001.IdentificationMarking = Convert.ToString(fc["IdentificationMarking"]);
                    objPRO001.PtcRequired = Convert.ToString(fc["PtcRequired"]);
                    objPRO001.WepUTMTPTCUSO4Clear = Convert.ToString(fc["WepUTMTPTCUSO4Clear"]);
                    objPRO001.SeamNoPunchingAtEvery900mm = Convert.ToString(fc["SeamNoPunchingAtEvery900mm"]);
                    objPRO001.ExtentOfNDTRT = Convert.ToString(fc["ExtentOfNDTRT"]);
                    objPRO001.ExtentOfNDTUT = Convert.ToString(fc["ExtentOfNDTUT"]);
                    objPRO001.ExtentOfNDTTOFD = Convert.ToString(fc["ExtentOfNDTTOFD"]);
                    objPRO001.WEPAngleis = Convert.ToString(fc["WEPAngleis"]);
                    objPRO001.WEPAngleos = Convert.ToString(fc["WEPAngleos"]);
                    objPRO001.TacksFreeFromCrack = Convert.ToString(fc["TacksFreeFromCrack"]);
                    objPRO001.RequriedRootGap = GetDecimalValue(Convert.ToString(fc["RequriedRootGap"]));
                    objPRO001.RequriedRootFace = GetDecimalValue(Convert.ToString(fc["RequriedRootFace"]));
                    objPRO001.ActMaxRootGap = GetDecimalValue(Convert.ToString(fc["ActMaxRootGap"]));
                    objPRO001.ActMaxRootFace = GetDecimalValue(Convert.ToString(fc["ActMaxRootFace"]));
                    objPRO001.ActMinRootGap = GetDecimalValue(Convert.ToString(fc["ActMinRootGap"]));
                    objPRO001.ActMinRootFace = GetDecimalValue(Convert.ToString(fc["ActMinRootFace"]));
                    objPRO001.ShellThiknessRequired = GetDecimalValue(Convert.ToString(fc["ShellThiknessRequired"]));
                    objPRO001.OffsetAllowed = GetDecimalValue(Convert.ToString(fc["OffsetAllowed"]));
                    objPRO001.RrequriedActual1 = GetDecimalValue(Convert.ToString(fc["RrequriedActual1"]));
                    objPRO001.ActMaxActual1 = GetDecimalValue(Convert.ToString(fc["ActMaxActual1"]));
                    objPRO001.ActMinActual1 = GetDecimalValue(Convert.ToString(fc["ActMinActual1"]));
                    objPRO001.RrequiredActual2 = GetDecimalValue(Convert.ToString(fc["RrequiredActual2"]));
                    objPRO001.ActMaxActual2 = GetDecimalValue(Convert.ToString(fc["ActMaxActual2"]));
                    objPRO001.ActMinActual2 = GetDecimalValue(Convert.ToString(fc["ActMinActual2"]));
                    objPRO001.RequriedActual3 = GetDecimalValue(Convert.ToString(fc["RequriedActual3"]));
                    objPRO001.ActMaxActual3 = GetDecimalValue(Convert.ToString(fc["ActMaxActual3"]));
                    objPRO001.ActMinActual3 = GetDecimalValue(Convert.ToString(fc["ActMinActual3"]));
                    objPRO001.RrequriedActual4 = GetDecimalValue(Convert.ToString(fc["RrequriedActual4"]));
                    objPRO001.ActMaxActual4 = GetDecimalValue(Convert.ToString(fc["ActMaxActual4"]));
                    objPRO001.ActMinActual4 = GetDecimalValue(Convert.ToString(fc["ActMinActual4"]));
                    objPRO001.RequredActual5 = GetDecimalValue(Convert.ToString(fc["RequredActual5"]));
                    objPRO001.ActMaxActual5 = GetDecimalValue(Convert.ToString(fc["ActMaxActual5"]));
                    objPRO001.ActMinActual5 = GetDecimalValue(Convert.ToString(fc["ActMinActual5"]));
                    objPRO001.TotalShellHeightRequried = GetDecimalValue(Convert.ToString(fc["TotalShellHeightRequried"]));
                    objPRO001.ReqIdCFTop = Convert.ToString(fc["ReqIdCFTop"]);
                    objPRO001.ReqIdCFMiddle = Convert.ToString(fc["ReqIdCFMiddle"]);
                    objPRO001.ReqIdCFBottom = Convert.ToString(fc["ReqIdCFBottom"]);
                    objPRO001.ActReqIdCFTop = Convert.ToString(fc["ActReqIdCFTop"]);
                    objPRO001.ActReqIdCFMiddle = Convert.ToString(fc["ActReqIdCFMiddle"]);
                    objPRO001.ActReqIdCFBottom = Convert.ToString(fc["ActReqIdCFBottom"]);
                    objPRO001.ReqODCFTop = Convert.ToString(fc["ReqODCFTop"]);
                    objPRO001.ReqODCFTopMiddle = Convert.ToString(fc["ReqODCFTopMiddle"]);
                    objPRO001.ReqODCFTopBottom = Convert.ToString(fc["ReqODCFTopBottom"]);
                    objPRO001.ActReqODCFTop = Convert.ToString(fc["ActReqODCFTop"]);
                    objPRO001.ActReqODCFTopMiddle = Convert.ToString(fc["ActReqODCFTopMiddle"]);
                    objPRO001.ActReqODCFTopBottom = Convert.ToString(fc["ActReqODCFTopBottom"]);
                    objPRO001.ActualAt0 = GetDecimalValue(Convert.ToString(fc["ActualAt0"]));
                    objPRO001.ActualAt90 = GetDecimalValue(Convert.ToString(fc["ActualAt90"]));
                    objPRO001.ActualAt180 = GetDecimalValue(Convert.ToString(fc["ActualAt180"]));
                    objPRO001.ActualAt270 = GetDecimalValue(Convert.ToString(fc["ActualAt270"]));
                    objPRO001.SeamNo1 = Convert.ToString(fc["SeamNo1"]);
                    objPRO001.SeamNo1Required = Convert.ToString(fc["SeamNo1Required"]);
                    objPRO001.SeamNo1Actual = Convert.ToString(fc["SeamNo1Actual"]);
                    objPRO001.SeamNo1Remark = Convert.ToString(fc["SeamNo1Remark"]);
                    objPRO001.SeamNo2 = Convert.ToString(fc["SeamNo2"]);
                    objPRO001.SeamNo2Required = Convert.ToString(fc["SeamNo2Required"]);
                    objPRO001.SeamNo2Actual = Convert.ToString(fc["SeamNo2Actual"]);
                    objPRO001.SeamNo2Remark = Convert.ToString(fc["SeamNo2Remark"]);
                    objPRO001.SeamNo3 = Convert.ToString(fc["SeamNo3"]);
                    objPRO001.SeamNo3Required = Convert.ToString(fc["SeamNo3Required"]);
                    objPRO001.SeamNo3Actual = Convert.ToString(fc["SeamNo3Actual"]);
                    objPRO001.SeamNo3Remark = Convert.ToString(fc["SeamNo3Remark"]);
                    objPRO001.NoteLine1 = Convert.ToString(fc["NoteLine1"]);
                    objPRO001.NoteLine2_1 = Convert.ToString(fc["NoteLine2_1"]);
                    objPRO001.NoteLine2_2 = GetDatetimeValue(Convert.ToString(fc["NoteLine2_2"]));
                    objPRO001.NoteLine3 = Convert.ToString(fc["NoteLine3"]);
                    objPRO001.NoteLine4 = Convert.ToString(fc["NoteLine4"]);
                    objPRO001.NoteLine5 = Convert.ToString(fc["NoteLine5"]);
                    objPRO001.NoteLine6 = Convert.ToString(fc["NoteLine6"]);
                    objPRO001.NoteLine7 = Convert.ToString(fc["NoteLine7"]);
                    objPRO001.NoteLine9 = Convert.ToString(fc["NoteLine9"]);
                    objPRO001.NoteLine10 = Convert.ToString(fc["NoteLine10"]);
                    objPRO001.NoteLine11 = Convert.ToString(fc["NoteLine11"]);
                    objPRO001.NoteLine12 = Convert.ToString(fc["NoteLine12"]);
                    objPRO001.NoteLine13 = Convert.ToString(fc["NoteLine13"]);
                    objPRO001.NoteLine14 = Convert.ToString(fc["NoteLine14"]);
                    objPRO001.ReqTopID = GetDecimalValue(Convert.ToString(fc["ReqTopID"]));
                    objPRO001.ActTopID1 = GetDecimalValue(Convert.ToString(fc["ActTopID1"]));
                    objPRO001.ActTopID2 = GetDecimalValue(Convert.ToString(fc["ActTopID2"]));
                    objPRO001.ActTopID3 = GetDecimalValue(Convert.ToString(fc["ActTopID3"]));
                    objPRO001.ActTopID4 = GetDecimalValue(Convert.ToString(fc["ActTopID4"]));
                    objPRO001.ActTopID5 = GetDecimalValue(Convert.ToString(fc["ActTopID5"]));
                    objPRO001.ActTopID6 = GetDecimalValue(Convert.ToString(fc["ActTopID6"]));
                    objPRO001.ActTopID7 = GetDecimalValue(Convert.ToString(fc["ActTopID7"]));
                    objPRO001.ActTopID8 = GetDecimalValue(Convert.ToString(fc["ActTopID8"]));
                    objPRO001.ActTopID9 = GetDecimalValue(Convert.ToString(fc["ActTopID9"]));
                    objPRO001.ReqBotID = GetDecimalValue(Convert.ToString(fc["ReqBotID"]));
                    objPRO001.ActBottomID1 = GetDecimalValue(Convert.ToString(fc["ActBottomID1"]));
                    objPRO001.ActBottomID2 = GetDecimalValue(Convert.ToString(fc["ActBottomID2"]));
                    objPRO001.ActBottomID3 = GetDecimalValue(Convert.ToString(fc["ActBottomID3"]));
                    objPRO001.ActBottomID4 = GetDecimalValue(Convert.ToString(fc["ActBottomID4"]));
                    objPRO001.ActBottomID5 = GetDecimalValue(Convert.ToString(fc["ActBottomID5"]));
                    objPRO001.ActBottomID6 = GetDecimalValue(Convert.ToString(fc["ActBottomID6"]));
                    objPRO001.ActBottomID7 = GetDecimalValue(Convert.ToString(fc["ActBottomID7"]));
                    objPRO001.ActBottomID8 = GetDecimalValue(Convert.ToString(fc["ActBottomID8"]));
                    objPRO001.ActBottomID9 = GetDecimalValue(Convert.ToString(fc["ActBottomID9"]));
                    objPRO001.Remarks = Convert.ToString(fc["Remarks"]);
                    objPRO001.ProfileMeasurementTol = GetDecimalValue(Convert.ToString(fc["ProfileMeasurementTol"]));
                    objPRO001.ProfileMeasurementSeamNo1 = Convert.ToString(fc["ProfileMeasurementSeamNo1"]);
                    objPRO001.ProfileMeasurementSeamNo2 = Convert.ToString(fc["ProfileMeasurementSeamNo2"]);
                    objPRO001.ProfileMeasurementSeamNo3 = Convert.ToString(fc["ProfileMeasurementSeamNo3"]);
                    objPRO001.TopPeakIn1 = Convert.ToString(fc["TopPeakIn1"]);
                    objPRO001.TopPeakIn2 = Convert.ToString(fc["TopPeakIn2"]);
                    objPRO001.TopPeakIn3 = Convert.ToString(fc["TopPeakIn3"]);
                    objPRO001.TopPeakOut1 = Convert.ToString(fc["TopPeakOut1"]);
                    objPRO001.TopPeakOut2 = Convert.ToString(fc["TopPeakOut2"]);
                    objPRO001.TopPeakOut3 = Convert.ToString(fc["TopPeakOut3"]);
                    objPRO001.MidPeakIn1 = Convert.ToString(fc["MidPeakIn1"]);
                    objPRO001.MidPeakIn2 = Convert.ToString(fc["MidPeakIn2"]);
                    objPRO001.MidPeakIn3 = Convert.ToString(fc["MidPeakIn3"]);
                    objPRO001.MidPeakOut1 = Convert.ToString(fc["MidPeakOut1"]);
                    objPRO001.MidPeakOut2 = Convert.ToString(fc["MidPeakOut2"]);
                    objPRO001.MidPeakOut3 = Convert.ToString(fc["MidPeakOut3"]);
                    objPRO001.BottomPeakIn1 = Convert.ToString(fc["BottomPeakIn1"]);
                    objPRO001.BottomPeakIn2 = Convert.ToString(fc["BottomPeakIn2"]);
                    objPRO001.BottomPeakIn3 = Convert.ToString(fc["BottomPeakIn3"]);
                    objPRO001.BottomPeakOut1 = Convert.ToString(fc["BottomPeakOut1"]);
                    objPRO001.BottomPeakOut2 = Convert.ToString(fc["BottomPeakOut2"]);
                    objPRO001.BottomPeakOut3 = Convert.ToString(fc["BottomPeakOut3"]);
                    objPRO001.ProfileMeasurementRemarks = Convert.ToString(fc["ProfileMeasurementRemarks"]);
                    objPRO001.ChordLengthReq = Convert.ToString(fc["ChordLengthReq"]);
                    objPRO001.ChordLengthAct = Convert.ToString(fc["ChordLengthAct"]);
                    objPRO001.RadiusReq = Convert.ToString(fc["RadiusReq"]);
                    objPRO001.RadiusAct = Convert.ToString(fc["RadiusAct"]);
                    objPRO001.GapAllowable1 = Convert.ToString(fc["GapAllowable1"]);
                    objPRO001.GapAllowable2 = Convert.ToString(fc["GapAllowable2"]);
                    objPRO001.GapAct = Convert.ToString(fc["GapAct"]);
                    objPRO001.EValueTemplateInspectionRemarks = Convert.ToString(fc["EValueTemplateInspectionRemarks"]);
                    objPRO001.InspectionRemark = Convert.ToString(fc["InspectionRemark"]);
                    objPRO001.WidthRequiredMax = GetDecimalValue(Convert.ToString(fc["WidthRequiredMax"]));
                    objPRO001.WidthRequiredMin = GetDecimalValue(Convert.ToString(fc["WidthRequiredMin"]));
                    objPRO001.DepthRequriedMax = GetDecimalValue(Convert.ToString(fc["DepthRequriedMax"]));
                    objPRO001.DepthRequriedMin = GetDecimalValue(Convert.ToString(fc["DepthRequriedMin"]));
                    objPRO001.Note1 = Convert.ToString(fc["Note1"]);
                    objPRO001.Note2 = Convert.ToString(fc["Note2"]);
                    objPRO001.Note3 = Convert.ToString(fc["Note3"]);
                    objPRO001.CladdedVessel = Convert.ToString(fc["CladdedVessel"]);
                    objPRO001.CreatedBy = objClsLoginInfo.UserName;
                    objPRO001.CreatedOn = DateTime.Now;
                    objPRO001.EditedBy = objClsLoginInfo.UserName;
                    objPRO001.EditedOn = DateTime.Now;

                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol saved successfully";
                    objResponseMsg.HeaderId = objPRO001.HeaderId;
                    #endregion

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO001 objPRO001 = db.PRO001.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO001 != null && string.IsNullOrWhiteSpace(objPRO001.ProtocolNo))
                    {
                        db.PRO002.RemoveRange(objPRO001.PRO002.ToList());
                        db.PRO003.RemoveRange(objPRO001.PRO003.ToList());
                        db.PRO001.Remove(objPRO001);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.SETUP_AND_DIMENSION_REPORT_FOR_ROLLED_SHELL_LONGITUDINAL_SEAM.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO001.HeaderId;
                    }

                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line1 details
        [HttpPost]
        public ActionResult GetProtocolLines1Data(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro002.SpotNo like '%" + param.sSearch
                        + "%' or pro002.Remarks like '%" + param.sSearch
                        + "%' or pro002.SpotWidth =" + param.sSearch
                        + "%')";
                }
                var lstResult = db.SP_IPI_PROTOCOL_GET_LINES1_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                PRO001 objPRO001 = db.PRO001.Where(c => c.HeaderId == intHeaderId).FirstOrDefault();

                int newRecordId = 0;
                var newRecord = new[] {
                                   "1",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "SpotNo", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "SpotWidth", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Remarks", "", "", false, "", false, "100","PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine1(0);")
                                };

                var data = (from uc in lstResult
                            select new[]
                            {
                            "1",
                            Convert.ToString(uc.HeaderId),
                            Convert.ToString(uc.LineId),
                            Helper.GenerateHTMLTextbox(uc.LineId, "SpotNo", uc.SpotNo, "", true, "", false, "10", "PROD"),
                            Helper.GenerateNumericTextbox(uc.LineId, "SpotWidth", Convert.ToString(uc.SpotWidth), "", true, "", false, "10", "PROD"),
                            Helper.GenerateHTMLTextbox(uc.LineId, "Remarks", uc.Remarks, "", true, "", false, "100", "PROD"),
                            Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (GetUserAccessRights().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.PROD3.GetStringValue() ? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine1','tblProtocolLines1')") : "")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine1(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRO002 objPRO002 = new PRO002();
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
                if (refHeaderId > 0)
                {
                    PRO001 objPRO001 = db.PRO001.Where(x => x.HeaderId == refHeaderId).FirstOrDefault();

                    string intSpotNo = !string.IsNullOrEmpty(fc["SpotNo" + refLineId]) ? Convert.ToString(fc["SpotNo" + refLineId]).Trim() : "";
                    decimal? strSpotWidth = GetDecimalValue(Convert.ToString(fc["SpotWidth" + refLineId]));//  !string.IsNullOrEmpty(fc["SpotWidth" + newRowIndex]) ? Convert.ToDecimal(fc["SpotWidth" + newRowIndex]) : 0;
                    string strRemarks = !string.IsNullOrEmpty(fc["Remarks" + refLineId]) ? Convert.ToString(fc["Remarks" + refLineId]).Trim() : string.Empty;

                    #region Insert/Update Spot Line1
                    if (refLineId > 0)
                    {
                        objPRO002 = db.PRO002.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRO002.EditedBy = objClsLoginInfo.UserName;
                        objPRO002.EditedOn = DateTime.Now;
                    }
                    objPRO002.HeaderId = objPRO001.HeaderId;
                    objPRO002.SpotNo = intSpotNo;
                    objPRO002.SpotWidth = strSpotWidth;
                    objPRO002.Remarks = strRemarks;

                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.SpotUpdate;
                    }
                    else
                    {
                        objPRO002.CreatedBy = objClsLoginInfo.UserName;
                        objPRO002.CreatedOn = DateTime.Now;
                        db.PRO002.Add(objPRO002);
                        db.SaveChanges();
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.SpotAdded;
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRO002.HeaderId;
                    #endregion
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine1(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRO002 objPRO002 = db.PRO002.FirstOrDefault(c => c.LineId == lineId);
                if (objPRO002 != null)
                {
                    db.PRO002.Remove(objPRO002);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Spot deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }


            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line2 details
        [HttpPost]
        public ActionResult GetProtocolLines2Data(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro003.SpotNo like '%" + param.sSearch
                        + "%' or pro003.Remarks like '%" + param.sSearch
                        + "%' or pro003.SpotDepth =" + param.sSearch
                        + "%')";
                }
                var lstResult = db.SP_IPI_PROTOCOL_GET_LINES2_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                PRO001 objPRO001 = db.PRO001.Where(c => c.HeaderId == intHeaderId).FirstOrDefault();

                int newRecordId = 0;
                var newRecord = new[] {
                                   "2",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "SpotNo2", "", "", false, "", false, "10", "PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "SpotDepth2", "", "", false, "", false, "10", "PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Remarks2", "", "", false, "", false, "100", "PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine2(0);")
                                };

                var data = (from uc in lstResult
                            select new[]
                            {
                                "2",
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.LineId),
                                Helper.GenerateHTMLTextbox(uc.LineId, "SpotNo2", uc.SpotNo, "", true, "", false, "10", "PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId, "SpotDepth2", Convert.ToString(uc.SpotDepth), "", true, "", false, "10", "PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "Remarks2", uc.Remarks, "", true, "", false, "100", "PROD"),
                                Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") +
                                (GetUserAccessRights().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.PROD3.GetStringValue() ? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine2','tblProtocolLines2')") : "")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine2(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRO003 objPRO003 = new PRO003();
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
                if (refHeaderId > 0)
                {
                    PRO001 objPRO001 = db.PRO001.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    string intSpotNo = !string.IsNullOrEmpty(fc["SpotNo2" + refLineId]) ? Convert.ToString(fc["SpotNo2" + refLineId]).Trim() : "";
                    decimal? strSpotDepth = GetDecimalValue(Convert.ToString(fc["SpotDepth2" + refLineId])); // !string.IsNullOrEmpty(fc["SpotDepth2" + newRowIndex]) ? Convert.ToDecimal(fc["SpotDepth2" + newRowIndex]) : 0;
                    string strRemarks = !string.IsNullOrEmpty(fc["Remarks2" + refLineId]) ? Convert.ToString(fc["Remarks2" + refLineId]).Trim() : string.Empty;


                    #region Add New Spot Line2
                    if (refLineId > 0)
                    {
                        objPRO003 = db.PRO003.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                    }
                    objPRO003.HeaderId = objPRO001.HeaderId;
                    objPRO003.SpotNo = intSpotNo;
                    objPRO003.SpotDepth = strSpotDepth;
                    objPRO003.Remarks = strRemarks;
                    if (refLineId > 0)
                    {
                        objPRO003.EditedBy = objClsLoginInfo.UserName;
                        objPRO003.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.SpotUpdate;
                    }
                    else
                    {
                        objPRO003.CreatedBy = objClsLoginInfo.UserName;
                        objPRO003.CreatedOn = DateTime.Now;
                        db.PRO003.Add(objPRO003);
                        db.SaveChanges();
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.SpotAdded;
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRO003.HeaderId;
                    #endregion
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine2(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRO003 objPRO003 = db.PRO003.FirstOrDefault(c => c.LineId == lineId);
                if (objPRO003 != null)
                {
                    db.PRO003.Remove(objPRO003);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Spot deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }


            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #endregion

        #region Linkage View

        #region Header Details Page
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL001 objPRL001 = new PRL001();
            if (!id.HasValue)
            {
                objPRL001.ProtocolNo = string.Empty;
                objPRL001.CreatedBy = objClsLoginInfo.UserName;
                objPRL001.CreatedOn = DateTime.Now;
                db.PRL001.Add(objPRL001);
                db.SaveChanges();
                ViewBag.HeaderId = objPRL001.HeaderId;
            }
            else
            {
                objPRL001 = db.PRL001.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }

            ViewBag.YesNoEnum = new List<CategoryData> { new CategoryData { Value = "Yes", Code = "Yes", CategoryDescription = "Yes" }, new CategoryData { Value = "No", Code = "No", CategoryDescription = "No" } };
            ViewBag.YesNAEnum = new List<CategoryData> { new CategoryData { Value = "Yes", Code = "Yes", CategoryDescription = "Yes" }, new CategoryData { Value = "NA", Code = "NA", CategoryDescription = "NA" } };
            ViewBag.YesNoNAEnum = new List<CategoryData> { new CategoryData { Value = "Yes", Code = "Yes", CategoryDescription = "Yes" }, new CategoryData { Value = "No", Code = "No", CategoryDescription = "No" }, new CategoryData { Value = "NA", Code = "NA", CategoryDescription = "NA" } };
            ViewBag.RTUTExtentEnum = new List<CategoryData> { new CategoryData { Value = "100%", Code = "100%", CategoryDescription = "100%" }, new CategoryData { Value = "SPOT", Code = "SPOT", CategoryDescription = "SPOT" }, new CategoryData { Value = "NA", Code = "NA", CategoryDescription = "NA" } };
            ViewBag.TOFDUTExtentEnum = new List<CategoryData> { new CategoryData { Value = "100%", Code = "100%", CategoryDescription = "100%" }, new CategoryData { Value = "NA", Code = "NA", CategoryDescription = "NA" } };

            UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = "SET-UP & DIMENSION REPORT FOR ROLLED SHELL LONGITUDINAL SEAM";
            ViewBag.ControllerURL = ControllerURL;
            var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL001.QualityProject).FirstOrDefault();
            ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
            ViewBag.ManufacturingCode = objQproject.ManufacturingCode;

            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }

            return View(objPRL001);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL001 objPRL001 = db.PRL001.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL001.ProtocolNo = Convert.ToString(fc["ProtocolNo"]);
                    objPRL001.DCRNo = Convert.ToString(fc["DCRNo"]);
                    objPRL001.IdentificationMarking = Convert.ToString(fc["IdentificationMarking"]);
                    objPRL001.PtcRequired = Convert.ToString(fc["PtcRequired"]);
                    objPRL001.WepUTMTPTCUSO4Clear = Convert.ToString(fc["WepUTMTPTCUSO4Clear"]);
                    objPRL001.SeamNoPunchingAtEvery900mm = Convert.ToString(fc["SeamNoPunchingAtEvery900mm"]);
                    objPRL001.ExtentOfNDTRT = Convert.ToString(fc["ExtentOfNDTRT"]);
                    objPRL001.ExtentOfNDTUT = Convert.ToString(fc["ExtentOfNDTUT"]);
                    objPRL001.ExtentOfNDTTOFD = Convert.ToString(fc["ExtentOfNDTTOFD"]);
                    objPRL001.WEPAngleis = Convert.ToString(fc["WEPAngleis"]);
                    objPRL001.WEPAngleos = Convert.ToString(fc["WEPAngleos"]);
                    objPRL001.TacksFreeFromCrack = Convert.ToString(fc["TacksFreeFromCrack"]);
                    objPRL001.RequriedRootGap = GetDecimalValue(Convert.ToString(fc["RequriedRootGap"]));
                    objPRL001.RequriedRootFace = GetDecimalValue(Convert.ToString(fc["RequriedRootFace"]));
                    objPRL001.ActMaxRootGap = GetDecimalValue(Convert.ToString(fc["ActMaxRootGap"]));
                    objPRL001.ActMaxRootFace = GetDecimalValue(Convert.ToString(fc["ActMaxRootFace"]));
                    objPRL001.ActMinRootGap = GetDecimalValue(Convert.ToString(fc["ActMinRootGap"]));
                    objPRL001.ActMinRootFace = GetDecimalValue(Convert.ToString(fc["ActMinRootFace"]));
                    objPRL001.ShellThiknessRequired = GetDecimalValue(Convert.ToString(fc["ShellThiknessRequired"]));
                    objPRL001.OffsetAllowed = GetDecimalValue(Convert.ToString(fc["OffsetAllowed"]));
                    objPRL001.RrequriedActual1 = GetDecimalValue(Convert.ToString(fc["RrequriedActual1"]));
                    objPRL001.ActMaxActual1 = GetDecimalValue(Convert.ToString(fc["ActMaxActual1"]));
                    objPRL001.ActMinActual1 = GetDecimalValue(Convert.ToString(fc["ActMinActual1"]));
                    objPRL001.RrequiredActual2 = GetDecimalValue(Convert.ToString(fc["RrequiredActual2"]));
                    objPRL001.ActMaxActual2 = GetDecimalValue(Convert.ToString(fc["ActMaxActual2"]));
                    objPRL001.ActMinActual2 = GetDecimalValue(Convert.ToString(fc["ActMinActual2"]));
                    objPRL001.RequriedActual3 = GetDecimalValue(Convert.ToString(fc["RequriedActual3"]));
                    objPRL001.ActMaxActual3 = GetDecimalValue(Convert.ToString(fc["ActMaxActual3"]));
                    objPRL001.ActMinActual3 = GetDecimalValue(Convert.ToString(fc["ActMinActual3"]));
                    objPRL001.RrequriedActual4 = GetDecimalValue(Convert.ToString(fc["RrequriedActual4"]));
                    objPRL001.ActMaxActual4 = GetDecimalValue(Convert.ToString(fc["ActMaxActual4"]));
                    objPRL001.ActMinActual4 = GetDecimalValue(Convert.ToString(fc["ActMinActual4"]));
                    objPRL001.RequredActual5 = GetDecimalValue(Convert.ToString(fc["RequredActual5"]));
                    objPRL001.ActMaxActual5 = GetDecimalValue(Convert.ToString(fc["ActMaxActual5"]));
                    objPRL001.ActMinActual5 = GetDecimalValue(Convert.ToString(fc["ActMinActual5"]));
                    objPRL001.TotalShellHeightRequried = GetDecimalValue(Convert.ToString(fc["TotalShellHeightRequried"]));
                    objPRL001.ReqIdCFTop = Convert.ToString(fc["ReqIdCFTop"]);
                    objPRL001.ReqIdCFMiddle = Convert.ToString(fc["ReqIdCFMiddle"]);
                    objPRL001.ReqIdCFBottom = Convert.ToString(fc["ReqIdCFBottom"]);
                    objPRL001.ActReqIdCFTop = Convert.ToString(fc["ActReqIdCFTop"]);
                    objPRL001.ActReqIdCFMiddle = Convert.ToString(fc["ActReqIdCFMiddle"]);
                    objPRL001.ActReqIdCFBottom = Convert.ToString(fc["ActReqIdCFBottom"]);
                    objPRL001.ReqODCFTop = Convert.ToString(fc["ReqODCFTop"]);
                    objPRL001.ReqODCFTopMiddle = Convert.ToString(fc["ReqODCFTopMiddle"]);
                    objPRL001.ReqODCFTopBottom = Convert.ToString(fc["ReqODCFTopBottom"]);
                    objPRL001.ActReqODCFTop = Convert.ToString(fc["ActReqODCFTop"]);
                    objPRL001.ActReqODCFTopMiddle = Convert.ToString(fc["ActReqODCFTopMiddle"]);
                    objPRL001.ActReqODCFTopBottom = Convert.ToString(fc["ActReqODCFTopBottom"]);
                    objPRL001.ActualAt0 = GetDecimalValue(Convert.ToString(fc["ActualAt0"]));
                    objPRL001.ActualAt90 = GetDecimalValue(Convert.ToString(fc["ActualAt90"]));
                    objPRL001.ActualAt180 = GetDecimalValue(Convert.ToString(fc["ActualAt180"]));
                    objPRL001.ActualAt270 = GetDecimalValue(Convert.ToString(fc["ActualAt270"]));
                    objPRL001.SeamNo1 = Convert.ToString(fc["SeamNo1"]);
                    objPRL001.SeamNo1Required = Convert.ToString(fc["SeamNo1Required"]);
                    objPRL001.SeamNo1Actual = Convert.ToString(fc["SeamNo1Actual"]);
                    objPRL001.SeamNo1Remark = Convert.ToString(fc["SeamNo1Remark"]);
                    objPRL001.SeamNo2 = Convert.ToString(fc["SeamNo2"]);
                    objPRL001.SeamNo2Required = Convert.ToString(fc["SeamNo2Required"]);
                    objPRL001.SeamNo2Actual = Convert.ToString(fc["SeamNo2Actual"]);
                    objPRL001.SeamNo2Remark = Convert.ToString(fc["SeamNo2Remark"]);
                    objPRL001.SeamNo3 = Convert.ToString(fc["SeamNo3"]);
                    objPRL001.SeamNo3Required = Convert.ToString(fc["SeamNo3Required"]);
                    objPRL001.SeamNo3Actual = Convert.ToString(fc["SeamNo3Actual"]);
                    objPRL001.SeamNo3Remark = Convert.ToString(fc["SeamNo3Remark"]);
                    objPRL001.NoteLine1 = Convert.ToString(fc["NoteLine1"]);
                    objPRL001.NoteLine2_1 = Convert.ToString(fc["NoteLine2_1"]);
                    objPRL001.NoteLine2_2 = GetDatetimeValue(Convert.ToString(fc["NoteLine2_2"]));
                    objPRL001.NoteLine3 = Convert.ToString(fc["NoteLine3"]);
                    objPRL001.NoteLine4 = Convert.ToString(fc["NoteLine4"]);
                    objPRL001.NoteLine5 = Convert.ToString(fc["NoteLine5"]);
                    objPRL001.NoteLine6 = Convert.ToString(fc["NoteLine6"]);
                    objPRL001.NoteLine7 = Convert.ToString(fc["NoteLine7"]);
                    objPRL001.NoteLine9 = Convert.ToString(fc["NoteLine9"]);
                    objPRL001.NoteLine10 = Convert.ToString(fc["NoteLine10"]);
                    objPRL001.NoteLine11 = Convert.ToString(fc["NoteLine11"]);
                    objPRL001.NoteLine12 = Convert.ToString(fc["NoteLine12"]);
                    objPRL001.NoteLine13 = Convert.ToString(fc["NoteLine13"]);
                    objPRL001.NoteLine14 = Convert.ToString(fc["NoteLine14"]);
                    objPRL001.ReqTopID = GetDecimalValue(Convert.ToString(fc["ReqTopID"]));
                    objPRL001.ActTopID1 = GetDecimalValue(Convert.ToString(fc["ActTopID1"]));
                    objPRL001.ActTopID2 = GetDecimalValue(Convert.ToString(fc["ActTopID2"]));
                    objPRL001.ActTopID3 = GetDecimalValue(Convert.ToString(fc["ActTopID3"]));
                    objPRL001.ActTopID4 = GetDecimalValue(Convert.ToString(fc["ActTopID4"]));
                    objPRL001.ActTopID5 = GetDecimalValue(Convert.ToString(fc["ActTopID5"]));
                    objPRL001.ActTopID6 = GetDecimalValue(Convert.ToString(fc["ActTopID6"]));
                    objPRL001.ActTopID7 = GetDecimalValue(Convert.ToString(fc["ActTopID7"]));
                    objPRL001.ActTopID8 = GetDecimalValue(Convert.ToString(fc["ActTopID8"]));
                    objPRL001.ActTopID9 = GetDecimalValue(Convert.ToString(fc["ActTopID9"]));
                    objPRL001.ReqBotID = GetDecimalValue(Convert.ToString(fc["ReqBotID"]));
                    objPRL001.ActBottomID1 = GetDecimalValue(Convert.ToString(fc["ActBottomID1"]));
                    objPRL001.ActBottomID2 = GetDecimalValue(Convert.ToString(fc["ActBottomID2"]));
                    objPRL001.ActBottomID3 = GetDecimalValue(Convert.ToString(fc["ActBottomID3"]));
                    objPRL001.ActBottomID4 = GetDecimalValue(Convert.ToString(fc["ActBottomID4"]));
                    objPRL001.ActBottomID5 = GetDecimalValue(Convert.ToString(fc["ActBottomID5"]));
                    objPRL001.ActBottomID6 = GetDecimalValue(Convert.ToString(fc["ActBottomID6"]));
                    objPRL001.ActBottomID7 = GetDecimalValue(Convert.ToString(fc["ActBottomID7"]));
                    objPRL001.ActBottomID8 = GetDecimalValue(Convert.ToString(fc["ActBottomID8"]));
                    objPRL001.ActBottomID9 = GetDecimalValue(Convert.ToString(fc["ActBottomID9"]));
                    objPRL001.Remarks = Convert.ToString(fc["Remarks"]);
                    objPRL001.ProfileMeasurementTol = GetDecimalValue(Convert.ToString(fc["ProfileMeasurementTol"]));
                    objPRL001.ProfileMeasurementSeamNo1 = Convert.ToString(fc["ProfileMeasurementSeamNo1"]);
                    objPRL001.ProfileMeasurementSeamNo2 = Convert.ToString(fc["ProfileMeasurementSeamNo2"]);
                    objPRL001.ProfileMeasurementSeamNo3 = Convert.ToString(fc["ProfileMeasurementSeamNo3"]);
                    objPRL001.TopPeakIn1 = Convert.ToString(fc["TopPeakIn1"]);
                    objPRL001.TopPeakIn2 = Convert.ToString(fc["TopPeakIn2"]);
                    objPRL001.TopPeakIn3 = Convert.ToString(fc["TopPeakIn3"]);
                    objPRL001.TopPeakOut1 = Convert.ToString(fc["TopPeakOut1"]);
                    objPRL001.TopPeakOut2 = Convert.ToString(fc["TopPeakOut2"]);
                    objPRL001.TopPeakOut3 = Convert.ToString(fc["TopPeakOut3"]);
                    objPRL001.MidPeakIn1 = Convert.ToString(fc["MidPeakIn1"]);
                    objPRL001.MidPeakIn2 = Convert.ToString(fc["MidPeakIn2"]);
                    objPRL001.MidPeakIn3 = Convert.ToString(fc["MidPeakIn3"]);
                    objPRL001.MidPeakOut1 = Convert.ToString(fc["MidPeakOut1"]);
                    objPRL001.MidPeakOut2 = Convert.ToString(fc["MidPeakOut2"]);
                    objPRL001.MidPeakOut3 = Convert.ToString(fc["MidPeakOut3"]);
                    objPRL001.BottomPeakIn1 = Convert.ToString(fc["BottomPeakIn1"]);
                    objPRL001.BottomPeakIn2 = Convert.ToString(fc["BottomPeakIn2"]);
                    objPRL001.BottomPeakIn3 = Convert.ToString(fc["BottomPeakIn3"]);
                    objPRL001.BottomPeakOut1 = Convert.ToString(fc["BottomPeakOut1"]);
                    objPRL001.BottomPeakOut2 = Convert.ToString(fc["BottomPeakOut2"]);
                    objPRL001.BottomPeakOut3 = Convert.ToString(fc["BottomPeakOut3"]);
                    objPRL001.ProfileMeasurementRemarks = Convert.ToString(fc["ProfileMeasurementRemarks"]);
                    objPRL001.ChordLengthReq = Convert.ToString(fc["ChordLengthReq"]);
                    objPRL001.ChordLengthAct = Convert.ToString(fc["ChordLengthAct"]);
                    objPRL001.RadiusReq = Convert.ToString(fc["RadiusReq"]);
                    objPRL001.RadiusAct = Convert.ToString(fc["RadiusAct"]);
                    objPRL001.GapAllowable1 = Convert.ToString(fc["GapAllowable1"]);
                    objPRL001.GapAllowable2 = Convert.ToString(fc["GapAllowable2"]);
                    objPRL001.GapAct = Convert.ToString(fc["GapAct"]);
                    objPRL001.EValueTemplateInspectionRemarks = Convert.ToString(fc["EValueTemplateInspectionRemarks"]);
                    objPRL001.InspectionRemark = Convert.ToString(fc["InspectionRemark"]);
                    objPRL001.WidthRequiredMax = GetDecimalValue(Convert.ToString(fc["WidthRequiredMax"]));
                    objPRL001.WidthRequiredMin = GetDecimalValue(Convert.ToString(fc["WidthRequiredMin"]));
                    objPRL001.DepthRequriedMax = GetDecimalValue(Convert.ToString(fc["DepthRequriedMax"]));
                    objPRL001.DepthRequriedMin = GetDecimalValue(Convert.ToString(fc["DepthRequriedMin"]));
                    objPRL001.Note1 = Convert.ToString(fc["Note1"]);
                    objPRL001.Note2 = Convert.ToString(fc["Note2"]);
                    objPRL001.Note3 = Convert.ToString(fc["Note3"]);
                    objPRL001.CladdedVessel = Convert.ToString(fc["CladdedVessel"]);
                    objPRL001.EditedBy = objClsLoginInfo.UserName;
                    objPRL001.EditedOn = DateTime.Now;

                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol saved successfully";
                    objResponseMsg.HeaderId = objPRL001.HeaderId;
                    #endregion

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line1 details
        [HttpPost]
        public ActionResult GetProtocolLines1DataLinkage(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (prl002.SpotNo like '%" + param.sSearch
                        + "%' or prl002.Remarks like '%" + param.sSearch
                        + "%' or prl002.SpotWidth =" + param.sSearch
                        + "%')";
                }
                var lstResult = db.SP_IPI_PROTOCOL1_LINKAGE_GET_LINES1_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                PRL001 objPRL001 = db.PRL001.Where(c => c.HeaderId == intHeaderId).FirstOrDefault();

                int newRecordId = 0;
                var newRecord = new[] {
                                   "1",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "SpotNo", "", "", false, "", false, "10", "PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "SpotWidth", "", "", false, "", false, "10", "PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Remarks", "", "", false, "", false, "100", "PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine1(0);")
                                };

                var data = (from uc in lstResult
                            select new[]
                            {
                                "1",
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                               Helper.GenerateHTMLTextbox(uc.LineId, "SpotNo", uc.SpotNo, "", true, "", false, "10", "PROD"),
                               Helper.GenerateNumericTextbox(uc.LineId, "SpotWidth", Convert.ToString(uc.SpotWidth), "", true, "", false, "10", "PROD"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "Remarks", uc.Remarks, "", true, "", false, "100", "PROD"),
                               Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") +
                              (GetUserAccessRights().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.PROD3.GetStringValue() ? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine1Linkage','tblProtocolLines1')") : "")

                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine1Linkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRL002 objPRL002 = new PRL002();
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
                if (refHeaderId > 0)
                {
                    PRL001 objPRL001 = db.PRL001.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    string intSpotNo = !string.IsNullOrEmpty(fc["SpotNo" + refLineId]) ? Convert.ToString(fc["SpotNo" + refLineId]).Trim() : "";
                    decimal? strSpotWidth = GetDecimalValue(Convert.ToString(fc["SpotWidth" + refLineId]));//  !string.IsNullOrEmpty(fc["SpotWidth" + newRowIndex]) ? Convert.ToDecimal(fc["SpotWidth" + newRowIndex]) : 0;
                    string strRemarks = !string.IsNullOrEmpty(fc["Remarks" + refLineId]) ? Convert.ToString(fc["Remarks" + refLineId]).Trim() : string.Empty;
                    if (refLineId > 0)
                    {
                        objPRL002 = db.PRL002.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                    }

                    #region Add New Spot Line1
                    objPRL002.HeaderId = objPRL001.HeaderId;
                    objPRL002.SpotNo = intSpotNo;
                    objPRL002.SpotWidth = strSpotWidth;
                    objPRL002.Remarks = strRemarks;
                    if (refLineId > 0)
                    {
                        objPRL002.EditedBy = objClsLoginInfo.UserName;
                        objPRL002.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.SpotUpdate;
                    }
                    else
                    {
                        objPRL002.CreatedBy = objClsLoginInfo.UserName;
                        objPRL002.CreatedOn = DateTime.Now;
                        db.PRL002.Add(objPRL002);
                        db.SaveChanges();
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.SpotAdded;
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRL002.HeaderId;
                    #endregion

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine1Linkage(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRL002 objPRL002 = db.PRL002.FirstOrDefault(c => c.LineId == lineId);
                if (objPRL002 != null)
                {
                    db.PRL002.Remove(objPRL002);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Spot deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }


            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line2 details
        [HttpPost]
        public ActionResult GetProtocolLines2DataLinkage(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (prl003.SpotNo like '%" + param.sSearch
                        + "%' or prl003.Remarks like '%" + param.sSearch
                        + "%' or prl003.SpotDepth =" + param.sSearch
                        + "%')";
                }
                var lstResult = db.SP_IPI_PROTOCOL1_LINKAGE_GET_LINES2_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                PRL001 objPRL001 = db.PRL001.Where(c => c.HeaderId == intHeaderId).FirstOrDefault();

                int newRecordId = 0;
                var newRecord = new[] {
                                    "2",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "SpotNo2", "", "", false, "", false, "10", "PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "SpotDepth2", "", "", false, "", false, "10", "PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Remarks2", "", "", false, "", false, "100", "PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine2(0);")
                                };

                var data = (from uc in lstResult
                            select new[]
                            {
                                "2",
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.LineId),
                                Helper.GenerateHTMLTextbox(uc.LineId, "SpotNo2", uc.SpotNo, "", true, "", false, "10", "PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId, "SpotDepth2", Convert.ToString(uc.SpotDepth), "", true, "", false, "10", "PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "Remarks2", uc.Remarks, "", true, "", false, "100", "PROD"),
                                Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") +
                                (GetUserAccessRights().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.PROD3.GetStringValue() ? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine2Linkage','tblProtocolLines2')") : "")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine2Linkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRL003 objPRL003 = new PRL003();
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
                if (refHeaderId > 0)
                {
                    PRL001 objPRL001 = db.PRL001.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    string intSpotNo = !string.IsNullOrEmpty(fc["SpotNo2" + refLineId]) ? Convert.ToString(fc["SpotNo2" + refLineId]).Trim() : "";
                    decimal? strSpotDepth = GetDecimalValue(Convert.ToString(fc["SpotDepth2" + refLineId])); // !string.IsNullOrEmpty(fc["SpotDepth2" + newRowIndex]) ? Convert.ToDecimal(fc["SpotDepth2" + newRowIndex]) : 0;
                    string strRemarks = !string.IsNullOrEmpty(fc["Remarks2" + refLineId]) ? Convert.ToString(fc["Remarks2" + refLineId]).Trim() : string.Empty;

                    #region Add/Update Spot Line2
                    if (refLineId > 0)
                    {
                        objPRL003 = db.PRL003.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                    }

                    objPRL003.HeaderId = objPRL001.HeaderId;
                    objPRL003.SpotNo = intSpotNo;
                    objPRL003.SpotDepth = strSpotDepth;
                    objPRL003.Remarks = strRemarks;
                    if (refLineId > 0)
                    {
                        objPRL003.EditedBy = objClsLoginInfo.UserName;
                        objPRL003.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.SpotUpdate;
                    }
                    else
                    {
                        objPRL003.CreatedBy = objClsLoginInfo.UserName;
                        objPRL003.CreatedOn = DateTime.Now;
                        db.PRL003.Add(objPRL003);
                        db.SaveChanges();
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.SpotAdded;
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRL003.HeaderId;
                    #endregion
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine2Linkage(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRL003 objPRL003 = db.PRL003.FirstOrDefault(c => c.LineId == lineId);
                if (objPRL003 != null)
                {
                    db.PRL003.Remove(objPRL003);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Spot deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }


            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion


        #endregion

        #region Common Functions
        public UserRoleAccessDetails GetUserAccessRights()
        {
            UserRoleAccessDetails objUserRoleAccessDetails = new UserRoleAccessDetails();

            try
            {
                var role = (from a in db.ATH001
                            join b in db.ATH004 on a.Role equals b.Id
                            where a.Employee.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase)
                            select new { RoleId = a.Role, RoleDesc = b.Role }).ToList();

                if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PROD3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.PROD3.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Initiator.GetStringValue();
                }
                else if (role.Where(i => (i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.QC3.GetStringValue(), StringComparison.OrdinalIgnoreCase) || i.RoleDesc == clsImplementationEnum.UserRoleName.QC2.GetStringValue() || i.RoleDesc == clsImplementationEnum.UserRoleName.QI2.GetStringValue() || i.RoleDesc == clsImplementationEnum.UserRoleName.QI3.GetStringValue())).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.QC3.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Initiator.GetStringValue();
                }
                else
                {
                    objUserRoleAccessDetails.UserRole = string.Empty;
                    objUserRoleAccessDetails.UserDesignation = string.Empty;
                }

            }
            catch
            {

            }
            return objUserRoleAccessDetails;
        }

        public int? GetIntegerValue(string value)
        {
            int outValue;
            if (int.TryParse(value, out outValue))
            {
                return outValue;
            }
            else
            {
                return null;
            }
        }
        public decimal? GetDecimalValue(string value)
        {
            decimal outValue;
            if (decimal.TryParse(value, out outValue))
            {
                return outValue;
            }
            else
            {
                return null;
            }
        }
        public DateTime? GetDatetimeValue(string value)
        {
            string[] formats = { "dd/MM/yyyy", "dd/MMM/yyyy" };
            DateTime outValue;
            if (DateTime.TryParseExact(value,
                      formats, null,
                        DateTimeStyles.None,
                        out outValue))
            { return outValue; }
            //if (DateTime.TryParse(value, out outValue))
            //{
            //    return outValue;
            //}
            else
            {
                return null;
            }
        }

        public string HTMLActionString(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", string onHrefURL = "", bool isDisable = false)
        {
            string htmlControl = "";
            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";
            string onHref = !string.IsNullOrEmpty(onHrefURL) ? "href='" + onHrefURL + "'" : "";

            if (isDisable)
            {
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer; opacity:0.3; margin-right:10px;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";
            }
            else
            {
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer; margin-right:10px;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
            }

            if (!string.IsNullOrEmpty(onHref))
            {
                htmlControl = "<a " + onHref + " >" + htmlControl + "</a>";
            }

            return htmlControl;
        }
        #endregion
    }
}