﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol009Controller : clsBase
    {
        string ControllerURL = "/PROTOCOL/Protocol009/";

        #region Utility
        public string HTMLActionString(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", string onHrefURL = "", bool isDisable = false)
        {
            string htmlControl = "";
            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? onClickMethod : "";
            string onHref = !string.IsNullOrEmpty(onHrefURL) ? "href='" + onHrefURL + "'" : "";

            if (isDisable)
            {
                htmlControl += "<i id=\"" + inputID.ToString() + "\" name=\"" + inputID + "\" ></i>";
                //htmlControl = "<i id=\"" + inputID + "' name='" + inputID + "' style='cursor: pointer; opacity:0.3; margin-right:10px;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";
            }
            else
            {
                htmlControl += "<i id=\"" + inputID.ToString() + "\" name=\"" + inputID + "\"  style='cursor: pointer; margin-right:10px;' Title=\"" + buttonTooltip + "\" class=\"" + className + "\"  onclick=\"" + onClickEvent + "\" ></i>";
                //htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer; margin-right:10px;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
            }

            if (!string.IsNullOrEmpty(onHref))
            {
                htmlControl = "<a " + onHref + " >" + htmlControl + "</a>";
            }

            return htmlControl;
        }
        #endregion

        #region Protocol
        // GET: PROTOCOL/Protocol009
        #region Header Details Page
        [SessionExpireFilter]// [UserPermissions, AllowAnonymous]
        public ActionResult Details(int? id, int? m)
        {
            PRO040 objPRO040 = new PRO040();
            if (!id.HasValue)
            {
                objPRO040.ProtocolNo = string.Empty;
                objPRO040.CreatedBy = objClsLoginInfo.UserName;
                objPRO040.CreatedOn = DateTime.Now;
                db.PRO040.Add(objPRO040);
                db.SaveChanges();
                ViewBag.HeaderId = objPRO040.HeaderId;
            }
            else
            {
                objPRO040 = db.PRO040.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = "VISUAL & DIMENSION REPORT FOR CIRCUMFERENTIAL SEAM";
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            List<string> lstProtocolRT = clsImplementationEnum.getProtocolRT().ToList();
            ViewBag.ProtocolRT = lstProtocolRT.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            List<string> lstProtocolUT = clsImplementationEnum.getProtocolRT().ToList();
            ViewBag.ProtocolUT = lstProtocolUT.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            return View(objPRO040);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO040 model)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = model.HeaderId;
                bool duplicateProtocol = db.PRO040.Where(x => x.ProtocolNo == model.ProtocolNo && x.HeaderId != model.HeaderId).Any();
                if (duplicateProtocol)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.ProtocolExists.ToString();
                    objResponseMsg.HeaderId = 0;
                }
                else
                {
                    if (refHeaderId.HasValue && refHeaderId > 0)
                    {
                        PRO040 objPRO040 = db.PRO040.Where(x => x.HeaderId == refHeaderId).FirstOrDefault();

                        #region Save Data
                        objPRO040.HeaderId = model.HeaderId;
                        objPRO040.ProtocolNo = model.ProtocolNo;
                        objPRO040.AllowableOffset = model.AllowableOffset;
                        objPRO040.TotalHeight = model.TotalHeight;
                        objPRO040.RequiredOrientation1 = model.RequiredOrientation1;
                        objPRO040.RequiredOrientation2 = model.RequiredOrientation2;
                        objPRO040.RequiredOrientation3 = model.RequiredOrientation3;
                        objPRO040.RequiredOrientation4 = model.RequiredOrientation4;
                        objPRO040.RequiredOrientation5 = model.RequiredOrientation5;
                        objPRO040.RequiredOrientation6 = model.RequiredOrientation6;
                        objPRO040.RequiredOrientation7 = model.RequiredOrientation7;
                        objPRO040.RequiredOrientation8 = model.RequiredOrientation8;
                        objPRO040.ActualOrientation1 = model.ActualOrientation1;
                        objPRO040.ActualOrientation2 = model.ActualOrientation2;
                        objPRO040.ActualOrientation3 = model.ActualOrientation3;
                        objPRO040.ActualOrientation4 = model.ActualOrientation4;
                        objPRO040.ActualOrientation5 = model.ActualOrientation5;
                        objPRO040.ActualOrientation6 = model.ActualOrientation6;
                        objPRO040.ActualOrientation7 = model.ActualOrientation7;
                        objPRO040.ActualOrientation8 = model.ActualOrientation8;
                        objPRO040.CommentIfAny = model.CommentIfAny;
                        objPRO040.RequiredChordLength = model.RequiredChordLength;
                        objPRO040.ActualChordLength = model.ActualChordLength;
                        objPRO040.RequiredRadius = model.RequiredRadius;
                        objPRO040.ActualRadius = model.ActualRadius;
                        objPRO040.AllowableGap = model.AllowableGap;
                        objPRO040.ActualGap = model.ActualGap;
                        objPRO040.Remarks = model.Remarks;
                        objPRO040.CladdedVessel = model.CladdedVessel;
                        objPRO040.OrientationRequiredAtTop1 = model.OrientationRequiredAtTop1;
                        objPRO040.OrientationRequiredAtTop2 = model.OrientationRequiredAtTop2;
                        objPRO040.OrientationRequiredAtTop3 = model.OrientationRequiredAtTop3;
                        objPRO040.OrientationRequiredAtTop4 = model.OrientationRequiredAtTop4;
                        objPRO040.OrientationActualAtTop1 = model.OrientationActualAtTop1;
                        objPRO040.OrientationActualAtTop2 = model.OrientationActualAtTop2;
                        objPRO040.OrientationActualAtTop3 = model.OrientationActualAtTop3;
                        objPRO040.OrientationActualAtTop4 = model.OrientationActualAtTop4;
                        objPRO040.OrientationRequiredAtBottom1 = model.OrientationRequiredAtBottom1;
                        objPRO040.OrientationRequiredAtBottom2 = model.OrientationRequiredAtBottom2;
                        objPRO040.OrientationRequiredAtBottom3 = model.OrientationRequiredAtBottom3;
                        objPRO040.OrientationRequiredAtBottom4 = model.OrientationRequiredAtBottom4;
                        objPRO040.OrientationActualAtBottom1 = model.OrientationActualAtBottom1;
                        objPRO040.OrientationActualAtBottom2 = model.OrientationActualAtBottom2;
                        objPRO040.OrientationActualAtBottom3 = model.OrientationActualAtBottom3;
                        objPRO040.OrientationActualAtBottom4 = model.OrientationActualAtBottom4;
                        objPRO040.OrientationRemark1 = model.OrientationRemark1;
                        objPRO040.OrientationRemark2 = model.OrientationRemark2;
                        objPRO040.OrientationRemark3 = model.OrientationRemark3;
                        objPRO040.OrientationRemark4 = model.OrientationRemark4;
                        objPRO040.Remark1 = model.Remark1;
                        objPRO040.Remark2Note = model.Remark2Note;
                        objPRO040.Remark2 = model.Remark2;
                        objPRO040.Remark4 = model.Remark4;
                        objPRO040.Remark5 = model.Remark5;
                        objPRO040.CladStripingWidthMax = model.CladStripingWidthMax;
                        objPRO040.CladStripingWidthMin = model.CladStripingWidthMin;
                        objPRO040.CladStripingDepthMax = model.CladStripingDepthMax;
                        objPRO040.CladStripingDepthMin = model.CladStripingDepthMin;
                        objPRO040.FNote1 = model.FNote1;
                        objPRO040.FNote2 = model.FNote2;
                        objPRO040.FNote3 = model.FNote3;


                        objPRO040.EditedBy = objClsLoginInfo.UserName;
                        objPRO040.EditedOn = DateTime.Now;

                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol saved successfully";
                        objResponseMsg.HeaderId = objPRO040.HeaderId;
                        #endregion
                    }
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO040 objPRO040 = db.PRO040.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO040 != null && string.IsNullOrWhiteSpace(objPRO040.ProtocolNo))
                    {
                        db.PRO041.RemoveRange(objPRO040.PRO041.ToList());
                        db.PRO042.RemoveRange(objPRO040.PRO042.ToList());
                        db.PRO043.RemoveRange(objPRO040.PRO043.ToList());
                        db.PRO044_1.RemoveRange(objPRO040.PRO044_1.ToList());
                        db.PRO044_2.RemoveRange(objPRO040.PRO044_2.ToList());
                        db.PRO040.Remove(objPRO040);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_REPORT_FOR_CIRCUMFERENTIAL_SEAM.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO040.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line 1
        [HttpPost]
        public ActionResult GetProtocolLines1Data(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (Orientation like '%" + param.sSearch
                        + "%' or Offset like '%" + param.sSearch
                        + "%')";
                }

                var lstResult = db.SP_IPI_PROTOCOL009_GET_LINES1_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                                   "1",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateNumericTextbox(newRecordId, "Orientation", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "Offset", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine1(0);")
                                };
                var data = (from uc in lstResult
                            select new[]
                                {
                                "1",
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.LineId),
                                Helper.GenerateNumericTextbox(uc.LineId, "Orientation",  Convert.ToString(uc.Orientation), "", true, "", false, "10","PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId, "Offset", Convert.ToString(uc.Offset), "", true, "", false, "10","PROD"),
                                Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.PROD3.GetStringValue()? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine1','tblProtocolLines1')") : "")
                               }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine1(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRO041 objPRO041 = new PRO041();
            int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                if (refHeaderId > 0)
                {
                    decimal? Orientation = (new clsManager()).GetDecimalValue(Convert.ToString(fc["Orientation" + refLineId]));
                    decimal? Offset = (new clsManager()).GetDecimalValue(Convert.ToString(fc["Offset" + refLineId]));

                    #region Add New Spot Line1
                    if (refLineId > 0)
                    {
                        objPRO041 = db.PRO041.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                    }
                    objPRO041.HeaderId = refHeaderId;
                    objPRO041.Orientation = Orientation;
                    objPRO041.Offset = Offset;
                    if (refLineId > 0)
                    {
                        objPRO041.EditedBy = objClsLoginInfo.UserName;
                        objPRO041.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Value = "Data updated successfully";
                    }
                    else
                    {

                        objPRO041.CreatedBy = objClsLoginInfo.UserName;
                        objPRO041.CreatedOn = DateTime.Now;
                        db.PRO041.Add(objPRO041);
                        db.SaveChanges();
                        objResponseMsg.Value = "Data added successfully";
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRO041.LineId;
                    #endregion

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine1(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRO041 objPRO041 = db.PRO041.FirstOrDefault(c => c.LineId == lineId);
                if (objPRO041 != null)
                {
                    db.PRO041.Remove(objPRO041);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Spot deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line 2
        [HttpPost]
        public ActionResult GetProtocolLines2Data(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (Location like '%" + param.sSearch
                        + "%' or At0 like '%" + param.sSearch
                        + "%' or At90 like '%" + param.sSearch
                        + "%' or At180 like '%" + param.sSearch
                        + "%' or At270 like '%" + param.sSearch
                        + "%' or Remark like '%" + param.sSearch
                        + "%')";
                }

                var lstResult = db.SP_IPI_PROTOCOL009_GET_LINES2_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                var lstOutBy = db.SP_IPI_PROTOCOL009_GET_OUTBY(intHeaderId).FirstOrDefault();
                var outBy0 = lstOutBy.OutBy0;
                var outBy90 = lstOutBy.OutBy90;
                var outBy180 = lstOutBy.OutBy180;
                var outBy270 = lstOutBy.OutBy270;

                int newRecordId = 0;
                var newRecord = new[] {"2",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Location", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "At0", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "At90", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "At180", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "At270", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Remark", "", "", false, "", false, "100","PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine2(0);")
                                };
                var data = (from uc in lstResult
                            select new[]
                            { "2",
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                               Helper.GenerateHTMLTextbox(uc.LineId, "Location",  Convert.ToString(uc.Location), "", true, "", false, "10","PROD"),
                               Helper.GenerateNumericTextbox(uc.LineId, "At0",   Convert.ToString(uc.At0), "", true, "", false, "10","PROD"),
                               Helper.GenerateNumericTextbox(uc.LineId, "At90",  Convert.ToString(uc.At90), "", true, "", false, "10","PROD"),
                               Helper.GenerateNumericTextbox(uc.LineId, "At180", Convert.ToString(uc.At180), "", true, "", false, "10","PROD"),
                               Helper.GenerateNumericTextbox(uc.LineId, "At270", Convert.ToString(uc.At270), "", true, "", false, "10","PROD"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "Remark",   Convert.ToString(uc.Remark), "", true, "", false, "100","PROD"),
                              Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() ==clsImplementationEnum.UserRoleName.QC3.GetStringValue()? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine2','tblProtocolLines2')"):"")
                           }).ToList();

                var outBy = new[] {
                                   "2",
                                   Convert.ToString(headerId),
                                   "",
                                   "<strong>Out By</strong>",
                                   Helper.GenerateNumericTextbox(0, "OutBy",  outBy0.ToString(), "", true, "", false,"",""),
                                   Helper.GenerateNumericTextbox(90, "OutBy", outBy90.ToString(), "", true, "", false,"",""),
                                   Helper.GenerateNumericTextbox(180, "OutBy",outBy180.ToString(), "", true, "",false,"",""),
                                   Helper.GenerateNumericTextbox(270, "OutBy",outBy270.ToString(), "", true, "",false,"",""),
                                   "",
                                   ""
                                    };
                data.Insert(0, newRecord);
                data.Insert(data.Count, outBy);
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition,
                    outby = true,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine2(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRO042 objPRO042 = new PRO042();
            int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                if (refHeaderId > 0)
                {
                    string Location = !string.IsNullOrEmpty(fc["Location" + refLineId]) ? Convert.ToString(fc["Location" + refLineId]).Trim() : "";
                    string Remark = !string.IsNullOrEmpty(fc["Remark" + refLineId]) ? Convert.ToString(fc["Remark" + refLineId]).Trim() : "";
                    decimal? At0 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["At0" + refLineId]));
                    decimal? At90 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["At90" + refLineId]));
                    decimal? At180 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["At180" + refLineId]));
                    decimal? At270 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["At270" + refLineId]));


                    #region Add New Spot Line1
                    if (refLineId > 0)
                    {
                        objPRO042 = db.PRO042.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRO042.EditedBy = objClsLoginInfo.UserName;
                        objPRO042.EditedOn = DateTime.Now;
                    }
                    objPRO042.HeaderId = refHeaderId;
                    objPRO042.Location = Location;
                    objPRO042.At0 = At0;
                    objPRO042.At90 = At90;
                    objPRO042.At180 = At180;
                    objPRO042.At270 = At270;
                    objPRO042.Remark = Remark;

                    PRO040 objPRO040 = db.PRO040.Where(x => x.HeaderId == refHeaderId).FirstOrDefault();
                    objPRO040.OutBy0 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["OutBy0"]));
                    objPRO040.OutBy90 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["OutBy90"]));
                    objPRO040.OutBy180 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["OutBy180"]));
                    objPRO040.OutBy270 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["OutBy270"]));

                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = "Data updated successfully";
                    }
                    else
                    {
                        objPRO042.CreatedBy = objClsLoginInfo.UserName;
                        objPRO042.CreatedOn = DateTime.Now;
                        db.PRO042.Add(objPRO042);
                        db.SaveChanges();
                        objResponseMsg.Value = "Data added successfully";
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRO042.LineId;
                    #endregion

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine2(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRO042 objPRO042 = db.PRO042.FirstOrDefault(c => c.LineId == lineId);
                if (objPRO042 != null)
                {
                    db.PRO042.Remove(objPRO042);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Alignment deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line 3
        [HttpPost]
        public ActionResult GetProtocolLines3Data(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (SeamNo like '%" + param.sSearch
                        + "%' or ReqOrientation like '%" + param.sSearch
                        + "%' or ReqArcLength like '%" + param.sSearch
                        + "%' or ActArcLength like '%" + param.sSearch
                        + "%')";
                }

                var lstResult = db.SP_IPI_PROTOCOL009_GET_LINES3_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                                    "3",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "SeamNo", "", "", false, "", false, "20","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "ReqOrientation", "", "", false, "", false, "20","QC"),
                                   Helper.GenerateNumericTextbox(newRecordId, "ReqArcLength", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "ActArcLength", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine3(0);")
                                };
                var data = (from uc in lstResult
                            select new[]
                            {  "3",
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                               Helper.GenerateHTMLTextbox(uc.LineId, "SeamNo", Convert.ToString(uc.SeamNo), "", true, "", false, "20","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "ReqOrientation",Convert.ToString(uc.ReqOrientation), "", true, "", false, "20","QC"),
                               Helper.GenerateNumericTextbox(uc.LineId, "ReqArcLength",Convert.ToString(uc.ReqArcLength), "", true, "", false, "10","PROD"),
                               Helper.GenerateNumericTextbox(uc.LineId, "ActArcLength",Convert.ToString(uc.ActArcLength), "", true, "", false, "10","PROD"),
                              Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.QC3.GetStringValue()? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o","DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine3','tblProtocolLines3')"):"")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine3(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRO043 objPRO043 = new PRO043();
            int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                if (refHeaderId > 0)
                {
                    string SeamNo = !string.IsNullOrEmpty(fc["SeamNo" + refLineId]) ? Convert.ToString(fc["SeamNo" + refLineId]).Trim() : "";
                    string ReqOrientation = !string.IsNullOrEmpty(fc["ReqOrientation" + refLineId]) ? Convert.ToString(fc["ReqOrientation" + refLineId]).Trim() : "";
                    decimal? ReqArcLength = (new clsManager()).GetDecimalValue(Convert.ToString(fc["ReqArcLength" + refLineId]));
                    decimal? ActArcLength = (new clsManager()).GetDecimalValue(Convert.ToString(fc["ActArcLength" + refLineId]));
                    if (refLineId > 0)
                    {
                        objPRO043 = db.PRO043.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRO043.EditedBy = objClsLoginInfo.UserName;
                        objPRO043.EditedOn = DateTime.Now;
                    }
                    objPRO043.HeaderId = refHeaderId;
                    objPRO043.SeamNo = SeamNo;
                    objPRO043.ReqOrientation = ReqOrientation;
                    objPRO043.ReqArcLength = ReqArcLength;
                    objPRO043.ActArcLength = ActArcLength;
                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = "Data updated successfully";
                    }
                    else
                    {
                        objPRO043.CreatedBy = objClsLoginInfo.UserName;
                        objPRO043.CreatedOn = DateTime.Now;
                        db.PRO043.Add(objPRO043);
                        db.SaveChanges();
                        objResponseMsg.Value = "Data added successfully";
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRO043.LineId;
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine3(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRO043 objPRO043 = db.PRO043.FirstOrDefault(c => c.LineId == lineId);
                if (objPRO043 != null)
                {
                    db.PRO043.Remove(objPRO043);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Seam deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line 4
        [HttpPost]
        public ActionResult GetProtocolLines4Data(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (SpotNo like '%" + param.sSearch
                        + "%' or Width like '%" + param.sSearch
                        + "%' or Remarks like '%" + param.sSearch
                        + "%')";
                }

                var lstResult = db.SP_IPI_PROTOCOL009_GET_LINES4_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                                 "4",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "SpotNo1", "", "", false, "", false, "10" ,"PROD" ),
                                   Helper.GenerateNumericTextbox(newRecordId, "Width", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Remarks", "", "", false, "", false, "100","PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine4(0);")
                                };
                var data = (from uc in lstResult
                            select new[]
                            {
                                 "4",
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.LineId),
                                Helper.GenerateHTMLTextbox(uc.LineId, "SpotNo1", Convert.ToString(uc.SpotNo),"", true, "", false, "10" ,"PROD" ),
                                Helper.GenerateNumericTextbox(uc.LineId, "Width",Convert.ToString(uc.Width), "", true, "", false, "10","PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "Remarks", Convert.ToString(uc.Remarks),"", true, "", false, "100","PROD"),
                                Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.PROD3.GetStringValue()? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine4','tblProtocolLines4')"):"")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine4(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRO044_1 objPRO044_1 = new PRO044_1();
            int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                if (refHeaderId > 0)
                {
                    string SpotNo = !string.IsNullOrEmpty(fc["SpotNo1" + refLineId]) ? Convert.ToString(fc["SpotNo1" + refLineId]).Trim() : "";
                    string Remarks = !string.IsNullOrEmpty(fc["Remarks" + refLineId]) ? Convert.ToString(fc["Remarks" + refLineId]).Trim() : "";
                    decimal? Width = (new clsManager()).GetDecimalValue(Convert.ToString(fc["Width" + refLineId]));
                    if (refLineId > 0)
                    {
                        objPRO044_1 = db.PRO044_1.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRO044_1.EditedBy = objClsLoginInfo.UserName;
                        objPRO044_1.EditedOn = DateTime.Now;
                    }
                    objPRO044_1.HeaderId = refHeaderId;
                    objPRO044_1.SpotNo = SpotNo;
                    objPRO044_1.Width = Width;
                    objPRO044_1.Remarks = Remarks;
                    if (refLineId > 0)
                    {
                        db.SaveChanges(); objResponseMsg.Value = clsImplementationMessage.CommonMessages.SpotUpdate;
                    }
                    else
                    {
                        objPRO044_1.CreatedBy = objClsLoginInfo.UserName;
                        objPRO044_1.CreatedOn = DateTime.Now;
                        db.PRO044_1.Add(objPRO044_1);
                        db.SaveChanges(); objResponseMsg.Value = clsImplementationMessage.CommonMessages.SpotAdded;
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRO044_1.LineId;

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine4(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRO044_1 objPRO044_1 = db.PRO044_1.FirstOrDefault(c => c.LineId == lineId);
                if (objPRO044_1 != null)
                {
                    db.PRO044_1.Remove(objPRO044_1);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Clat striping width deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line 5
        [HttpPost]
        public ActionResult GetProtocolLines5Data(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (SpotNo like '%" + param.sSearch
                        + "%' or Depth like '%" + param.sSearch
                        + "%' or Remarks like '%" + param.sSearch
                        + "%')";
                }

                var lstResult = db.SP_IPI_PROTOCOL009_GET_LINES5_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                                   "5",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "SpotNo2", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "Depth", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Remarks2", "", "", false, "", false, "100","PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine5(0);")
                                };
                var data = (from uc in lstResult
                            select new[]
                            {
                               "5",
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                               Helper.GenerateHTMLTextbox(uc.LineId, "SpotNo2",Convert.ToString(uc.SpotNo), "", true, "", false, "10","PROD"),
                               Helper.GenerateNumericTextbox(uc.LineId, "Depth", Convert.ToString(uc.Depth), "", true, "", false, "10","PROD"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "Remarks2",   Convert.ToString(uc.Remarks), "", true, "", false, "100","PROD"),
                               Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.PROD3.GetStringValue()? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine5','tblProtocolLines5')"):"")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine5(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRO044_2 objPRO044_2 = new PRO044_2();
            int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                if (refHeaderId > 0)
                {
                    string SpotNo = !string.IsNullOrEmpty(fc["SpotNo2" + refLineId]) ? Convert.ToString(fc["SpotNo2" + refLineId]).Trim() : "";
                    string Remarks = !string.IsNullOrEmpty(fc["Remarks2" + refLineId]) ? Convert.ToString(fc["Remarks2" + refLineId]).Trim() : "";
                    decimal? Depth = (new clsManager()).GetDecimalValue(Convert.ToString(fc["Depth" + refLineId]));
                    if (refLineId > 0)
                    {
                        objPRO044_2 = db.PRO044_2.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRO044_2.EditedBy = objClsLoginInfo.UserName;
                        objPRO044_2.EditedOn = DateTime.Now;
                    }
                    objPRO044_2.HeaderId = refHeaderId;
                    objPRO044_2.SpotNo = SpotNo;
                    objPRO044_2.Depth = Depth;
                    objPRO044_2.Remarks = Remarks;
                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.SpotUpdate;
                    }
                    else
                    {
                        objPRO044_2.CreatedBy = objClsLoginInfo.UserName;
                        objPRO044_2.CreatedOn = DateTime.Now;
                        db.PRO044_2.Add(objPRO044_2);
                        db.SaveChanges();
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.SpotAdded;
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRO044_2.LineId;


                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine5(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRO044_2 objPRO044_2 = db.PRO044_2.FirstOrDefault(c => c.LineId == lineId);
                if (objPRO044_2 != null)
                {
                    db.PRO044_2.Remove(objPRO044_2);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Clat striping depth deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #endregion

        #region Protocol Linkage

        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL040 objPRL040 = db.PRL040.Where(i => i.HeaderId == id).FirstOrDefault();

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = "VISUAL & DIMENSION REPORT FOR CIRCUMFERENTIAL SEAM";
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            List<string> lstProtocolRT = clsImplementationEnum.getProtocolRT().ToList();
            ViewBag.ProtocolRT = lstProtocolRT.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            List<string> lstProtocolUT = clsImplementationEnum.getProtocolRT().ToList();
            ViewBag.ProtocolUT = lstProtocolUT.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            if (objPRL040 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL040.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objQproject.ManufacturingCode;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }
            if (m == 1)
            {
                ViewBag.isEditable = "true";
            }
            else
            { ViewBag.isEditable = "false"; }
            return View(objPRL040);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL040 model)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = model.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL040 objPRL040 = db.PRL040.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL040.DCRNo = model.DCRNo;
                    objPRL040.ProtocolNo = model.ProtocolNo != null ? model.ProtocolNo : "";
                    objPRL040.AllowableOffset = model.AllowableOffset;
                    objPRL040.TotalHeight = model.TotalHeight;
                    objPRL040.RequiredOrientation1 = model.RequiredOrientation1;
                    objPRL040.RequiredOrientation2 = model.RequiredOrientation2;
                    objPRL040.RequiredOrientation3 = model.RequiredOrientation3;
                    objPRL040.RequiredOrientation4 = model.RequiredOrientation4;
                    objPRL040.RequiredOrientation5 = model.RequiredOrientation5;
                    objPRL040.RequiredOrientation6 = model.RequiredOrientation6;
                    objPRL040.RequiredOrientation7 = model.RequiredOrientation7;
                    objPRL040.RequiredOrientation8 = model.RequiredOrientation8;
                    objPRL040.ActualOrientation1 = model.ActualOrientation1;
                    objPRL040.ActualOrientation2 = model.ActualOrientation2;
                    objPRL040.ActualOrientation3 = model.ActualOrientation3;
                    objPRL040.ActualOrientation4 = model.ActualOrientation4;
                    objPRL040.ActualOrientation5 = model.ActualOrientation5;
                    objPRL040.ActualOrientation6 = model.ActualOrientation6;
                    objPRL040.ActualOrientation7 = model.ActualOrientation7;
                    objPRL040.ActualOrientation8 = model.ActualOrientation8;
                    objPRL040.CommentIfAny = model.CommentIfAny;
                    objPRL040.RequiredChordLength = model.RequiredChordLength;
                    objPRL040.ActualChordLength = model.ActualChordLength;
                    objPRL040.RequiredRadius = model.RequiredRadius;
                    objPRL040.ActualRadius = model.ActualRadius;
                    objPRL040.AllowableGap = model.AllowableGap;
                    objPRL040.ActualGap = model.ActualGap;
                    objPRL040.Remarks = model.Remarks;
                    objPRL040.OrientationRequiredAtTop1 = model.OrientationRequiredAtTop1;
                    objPRL040.OrientationRequiredAtTop2 = model.OrientationRequiredAtTop2;
                    objPRL040.OrientationRequiredAtTop3 = model.OrientationRequiredAtTop3;
                    objPRL040.OrientationRequiredAtTop4 = model.OrientationRequiredAtTop4;
                    objPRL040.OrientationActualAtTop1 = model.OrientationActualAtTop1;
                    objPRL040.OrientationActualAtTop2 = model.OrientationActualAtTop2;
                    objPRL040.OrientationActualAtTop3 = model.OrientationActualAtTop3;
                    objPRL040.OrientationActualAtTop4 = model.OrientationActualAtTop4;
                    objPRL040.OrientationRequiredAtBottom1 = model.OrientationRequiredAtBottom1;
                    objPRL040.OrientationRequiredAtBottom2 = model.OrientationRequiredAtBottom2;
                    objPRL040.OrientationRequiredAtBottom3 = model.OrientationRequiredAtBottom3;
                    objPRL040.OrientationRequiredAtBottom4 = model.OrientationRequiredAtBottom4;
                    objPRL040.OrientationActualAtBottom1 = model.OrientationActualAtBottom1;
                    objPRL040.OrientationActualAtBottom2 = model.OrientationActualAtBottom2;
                    objPRL040.OrientationActualAtBottom3 = model.OrientationActualAtBottom3;
                    objPRL040.OrientationActualAtBottom4 = model.OrientationActualAtBottom4;
                    objPRL040.OrientationRemark1 = model.OrientationRemark1;
                    objPRL040.OrientationRemark2 = model.OrientationRemark2;
                    objPRL040.OrientationRemark3 = model.OrientationRemark3;
                    objPRL040.OrientationRemark4 = model.OrientationRemark4;

                    objPRL040.OutBy0 = model.OutBy0;
                    objPRL040.OutBy90 = model.OutBy90;
                    objPRL040.OutBy180 = model.OutBy180;
                    objPRL040.OutBy270 = model.OutBy270;

                    objPRL040.Remark1 = model.Remark1;
                    objPRL040.Remark2Note = model.Remark2Note;
                    objPRL040.Remark2 = model.Remark2;
                    objPRL040.Remark4 = model.Remark4;
                    objPRL040.Remark5 = model.Remark5;
                    objPRL040.CladStripingWidthMax = model.CladStripingWidthMax;
                    objPRL040.CladStripingWidthMin = model.CladStripingWidthMin;
                    objPRL040.CladStripingDepthMax = model.CladStripingDepthMax;
                    objPRL040.CladStripingDepthMin = model.CladStripingDepthMin;
                    objPRL040.CladdedVessel = model.CladdedVessel;
                    objPRL040.FNote1 = model.FNote1;
                    objPRL040.FNote2 = model.FNote2;
                    objPRL040.FNote3 = model.FNote3;
                    objPRL040.EditedBy = objClsLoginInfo.UserName;
                    objPRL040.EditedOn = DateTime.Now;

                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol saved successfully";
                    objResponseMsg.HeaderId = objPRL040.HeaderId;

                    #endregion
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line 1
        [HttpPost]
        public ActionResult GetProtocolLinkageLines1Data(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (Orientation like '%" + param.sSearch
                        + "%' or Offset like '%" + param.sSearch
                        + "%')";
                }

                var lstResult = db.SP_IPI_PROTOCOL009_LINKAGE_GET_LINES1_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                    "1",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateNumericTextbox(newRecordId, "Orientation", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "Offset", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine1(0);")
                                };
                var data = (from uc in lstResult
                            select new[]
                            {
                                "1",
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                               Helper.GenerateNumericTextbox(uc.LineId, "Orientation",  Convert.ToString(uc.Orientation), "", true, "", false, "10","PROD"),
                               Helper.GenerateNumericTextbox(uc.LineId, "Offset",Convert.ToString(uc.Offset), "", true, "", false, "10","PROD"),
                                                      Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.PROD3.GetStringValue()? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLinkageLine1','tblProtocolLines1')"):"")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLinkageLine1(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRL041 objPRL041 = new PRL041();
            int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                if (refHeaderId > 0)
                {
                    decimal? Orientation = (new clsManager()).GetDecimalValue(Convert.ToString(fc["Orientation" + refLineId]));
                    decimal? Offset = (new clsManager()).GetDecimalValue(Convert.ToString(fc["Offset" + refLineId]));


                    #region Add New Spot Line1
                    if (refLineId > 0)
                    {
                        objPRL041 = db.PRL041.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRL041.EditedBy = objClsLoginInfo.UserName;
                        objPRL041.EditedOn = DateTime.Now;
                    }

                    objPRL041.HeaderId = refHeaderId;
                    objPRL041.Orientation = Orientation;
                    objPRL041.Offset = Offset;
                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.DataUpdate;
                    }
                    else
                    {
                        objPRL041.CreatedBy = objClsLoginInfo.UserName;
                        objPRL041.CreatedOn = DateTime.Now;
                        db.PRL041.Add(objPRL041);
                        db.SaveChanges();
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.DataAdded;

                    }
              
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Offset added successfully";
                    objResponseMsg.HeaderId = objPRL041.LineId;
                    #endregion

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLinkageLine1(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRL041 objPRL041 = db.PRL041.FirstOrDefault(c => c.LineId == lineId);
                if (objPRL041 != null)
                {
                    db.PRL041.Remove(objPRL041);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Spot deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line 2
        [HttpPost]
        public ActionResult GetProtocolLinkageLines2Data(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (Location like '%" + param.sSearch
                        + "%' or At0 like '%" + param.sSearch
                        + "%' or At90 like '%" + param.sSearch
                        + "%' or At180 like '%" + param.sSearch
                        + "%' or At270 like '%" + param.sSearch
                        + "%' or Remark like '%" + param.sSearch
                        + "%')";
                }

                var lstResult = db.SP_IPI_PROTOCOL009_LINKAGE_GET_LINES2_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                var lstOutBy = db.SP_IPI_PROTOCOL009_LINKAGE_GET_OUTBY(intHeaderId).FirstOrDefault();
                var outBy0 = lstOutBy.OutBy0;
                var outBy90 = lstOutBy.OutBy90;
                var outBy180 = lstOutBy.OutBy180;
                var outBy270 = lstOutBy.OutBy270;
                int newRecordId = 0;
                var newRecord = new[] {
                                  "2",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Location", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "At0", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "At90", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "At180", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "At270", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Remark", "", "", false, "", false, "100","PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine2(0);")
                                };
                var data = (from uc in lstResult
                            select new[]
                            {  "2",
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.LineId),
                                Helper.GenerateHTMLTextbox(uc.LineId, "Location", Convert.ToString(uc.Location),"", true, "", false, "10","PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId, "At0", Convert.ToString(uc.At0),"", true, "", false, "10","PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId, "At90",Convert.ToString(uc.At90),"", true, "", false, "10","PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId, "At180", Convert.ToString(uc.At180),"", true, "", false, "10","PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId, "At270",Convert.ToString(uc.At270), "", true, "", false, "10","PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "Remark", Convert.ToString(uc.Remark),"", true, "", false, "100","PROD"),
                               Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.QC3.GetStringValue()? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLinkageLine2','tblProtocolLines2')"):"")
                           }).ToList();
                var outBy = new[] {
                    "",
                                   Convert.ToString(headerId),
                                   "",
                                   "<strong>Out By</strong>",
                                   Helper.GenerateNumericTextbox(0, "OutBy",  outBy0.ToString(), "", true, "", false,"",""),
                                   Helper.GenerateNumericTextbox(90, "OutBy", outBy90.ToString(), "", true, "", false,"",""),
                                   Helper.GenerateNumericTextbox(180, "OutBy",outBy180.ToString(), "", true, "",false,"",""),
                                   Helper.GenerateNumericTextbox(270, "OutBy",outBy270.ToString(), "", true, "",false,"",""),
                                   "",
                                   ""
                                    };
                data.Insert(0, newRecord);
                data.Insert(data.Count, outBy);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition,
                    outby = true
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLinkageLine2(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRL042 objPRL042 = new PRL042();
            int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                if (refHeaderId > 0)
                {
                    string Location = !string.IsNullOrEmpty(fc["Location" + refLineId]) ? Convert.ToString(fc["Location" + refLineId]).Trim() : "";
                    string Remark = !string.IsNullOrEmpty(fc["Remark" + refLineId]) ? Convert.ToString(fc["Remark" + refLineId]).Trim() : "";
                    decimal? At0 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["At0" + refLineId]));
                    decimal? At90 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["At90" + refLineId]));
                    decimal? At180 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["At180" + refLineId]));
                    decimal? At270 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["At270" + refLineId]));

                    PRL040 objPRL040 = db.PRL040.Where(x => x.HeaderId == refHeaderId).FirstOrDefault();
                    objPRL040.OutBy0 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["OutBy0"]));
                    objPRL040.OutBy90 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["OutBy90"]));
                    objPRL040.OutBy180 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["OutBy180"]));
                    objPRL040.OutBy270 = (new clsManager()).GetDecimalValue(Convert.ToString(fc["OutBy270"]));

                    #region Add New Spot Line1
                    if (refLineId > 0)
                    {
                        objPRL042 = db.PRL042.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRL042.EditedBy = objClsLoginInfo.UserName;
                        objPRL042.EditedOn = DateTime.Now;
                    }
                    objPRL042.HeaderId = refHeaderId;
                    objPRL042.Location = Location;
                    objPRL042.At0 = At0;
                    objPRL042.At90 = At90;
                    objPRL042.At180 = At180;
                    objPRL042.At270 = At270;
                    objPRL042.Remark = Remark;
                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.DataUpdate;
                    }
                    else
                    {
                        objPRL042.CreatedBy = objClsLoginInfo.UserName;
                        objPRL042.CreatedOn = DateTime.Now;
                        db.PRL042.Add(objPRL042);
                        db.SaveChanges();
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.DataAdded;
                    }
                 
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRL042.LineId;
                    #endregion

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLinkageLine2(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRL042 objPRL042 = db.PRL042.FirstOrDefault(c => c.LineId == lineId);
                if (objPRL042 != null)
                {
                    db.PRL042.Remove(objPRL042);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Alignment deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line 3
        [HttpPost]
        public ActionResult GetProtocolLinkageLines3Data(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (SeamNo like '%" + param.sSearch
                        + "%' or ReqOrientation like '%" + param.sSearch
                        + "%' or ReqArcLength like '%" + param.sSearch
                        + "%' or ActArcLength like '%" + param.sSearch
                        + "%')";
                }

                var lstResult = db.SP_IPI_PROTOCOL009_LINKAGE_GET_LINES3_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                                  "3",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "SeamNo", "", "", false, "", false, "20","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "ReqOrientation", "", "", false, "", false, "20","QC"),
                                   Helper.GenerateNumericTextbox(newRecordId, "ReqArcLength", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "ActArcLength", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine3(0);")
                                };
                var data = (from uc in lstResult
                            select new[]
                            {"3",
                            Convert.ToString(uc.HeaderId),
                            Convert.ToString(uc.LineId),
                            Helper.GenerateHTMLTextbox(uc.LineId, "SeamNo", Convert.ToString(uc.SeamNo), "", true, "", false, "20","QC"),
                            Helper.GenerateHTMLTextbox(uc.LineId, "ReqOrientation",  Convert.ToString(uc.ReqOrientation), "", true, "", false, "20","QC"),
                            Helper.GenerateNumericTextbox(uc.LineId, "ReqArcLength", Convert.ToString(uc.ReqArcLength), "", true, "", false, "10","PROD"),
                            Helper.GenerateNumericTextbox(uc.LineId, "ActArcLength", Convert.ToString(uc.ActArcLength), "", true, "", false, "10","PROD"),
                           Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.QC3.GetStringValue()? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLinkageLine3','tblProtocolLines3')"):"")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLinkageLine3(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRL043 objPRL043 = new PRL043();
            int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                if (refHeaderId > 0)
                {
                    string SeamNo = !string.IsNullOrEmpty(fc["SeamNo" + refLineId]) ? Convert.ToString(fc["SeamNo" + refLineId]).Trim() : "";
                    string ReqOrientation = !string.IsNullOrEmpty(fc["ReqOrientation" + refLineId]) ? Convert.ToString(fc["ReqOrientation" + refLineId]).Trim() : "";
                    decimal? ReqArcLength = (new clsManager()).GetDecimalValue(Convert.ToString(fc["ReqArcLength" + refLineId]));
                    decimal? ActArcLength = (new clsManager()).GetDecimalValue(Convert.ToString(fc["ActArcLength" + refLineId]));
                    if (refLineId > 0)
                    {
                        objPRL043 = db.PRL043.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRL043.EditedBy = objClsLoginInfo.UserName;
                        objPRL043.EditedOn = DateTime.Now;
                    }
                    objPRL043.HeaderId = refHeaderId;
                    objPRL043.SeamNo = SeamNo;
                    objPRL043.ReqOrientation = ReqOrientation;
                    objPRL043.ReqArcLength = ReqArcLength;
                    objPRL043.ActArcLength = ActArcLength;
          
                    db.SaveChanges();
                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.DataUpdate;
                    }
                    else
                    {
                        objPRL043.CreatedBy = objClsLoginInfo.UserName;
                        objPRL043.CreatedOn = DateTime.Now;
                        db.PRL043.Add(objPRL043);
                        db.SaveChanges();
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.DataAdded;
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRL043.LineId;


                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLinkageLine3(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRL043 objPRL043 = db.PRL043.FirstOrDefault(c => c.LineId == lineId);
                if (objPRL043 != null)
                {
                    db.PRL043.Remove(objPRL043);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Seam deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line 4
        [HttpPost]
        public ActionResult GetProtocolLinkageLines4Data(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (SpotNo like '%" + param.sSearch
                        + "%' or Width like '%" + param.sSearch
                        + "%' or Remarks like '%" + param.sSearch
                        + "%')";
                }

                var lstResult = db.SP_IPI_PROTOCOL009_LINKAGE_GET_LINES4_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                                   "4",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "SpotNo1", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "Width", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Remarks", "", "", false, "", false, "100","PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine4(0);")
                                };
                var data = (from uc in lstResult
                            select new[]
                            { "4",
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                               Helper.GenerateHTMLTextbox(uc.LineId, "SpotNo1", Convert.ToString(uc.SpotNo),"", true, "", false, "10","PROD"),
                               Helper.GenerateNumericTextbox(uc.LineId, "Width", Convert.ToString(uc.Width), "", true, "", false, "10","PROD"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "Remarks",  Convert.ToString(uc.Remarks), "", true, "", false, "100","PROD"),
                              Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.PROD3.GetStringValue()? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLinkageLine4','tblProtocolLines4')"):"")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLinkageLine4(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRL044_1 objPRL044_1 = new PRL044_1();
            int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                if (refHeaderId > 0)
                {
                    string SpotNo = !string.IsNullOrEmpty(fc["SpotNo1" + refLineId]) ? Convert.ToString(fc["SpotNo1" + refLineId]).Trim() : "";
                    string Remarks = !string.IsNullOrEmpty(fc["Remarks" + refLineId]) ? Convert.ToString(fc["Remarks" + refLineId]).Trim() : "";
                    decimal? Width = (new clsManager()).GetDecimalValue(Convert.ToString(fc["Width" + refLineId]));
                    if (refLineId > 0)
                    {
                        objPRL044_1 = db.PRL044_1.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRL044_1.EditedBy = objClsLoginInfo.UserName;
                        objPRL044_1.EditedOn = DateTime.Now;
                    }
                    objPRL044_1.HeaderId = refHeaderId;
                    objPRL044_1.SpotNo = SpotNo;
                    objPRL044_1.Width = Width;
                    objPRL044_1.Remarks = Remarks;
                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.SpotUpdate;
                    }
                    else
                    {
                        objPRL044_1.CreatedBy = objClsLoginInfo.UserName;
                        objPRL044_1.CreatedOn = DateTime.Now;
                        db.PRL044_1.Add(objPRL044_1);
                        db.SaveChanges();
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.SpotAdded;
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRL044_1.LineId;
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLinkageLine4(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRL044_1 objPRL044_1 = db.PRL044_1.FirstOrDefault(c => c.LineId == lineId);
                if (objPRL044_1 != null)
                {
                    db.PRL044_1.Remove(objPRL044_1);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Clat striping width deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line 5
        [HttpPost]
        public ActionResult GetProtocolLinkageLines5Data(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (SpotNo like '%" + param.sSearch
                        + "%' or Depth like '%" + param.sSearch
                        + "%' or Remarks like '%" + param.sSearch
                        + "%')";
                }

                var lstResult = db.SP_IPI_PROTOCOL009_LINKAGE_GET_LINES5_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                                  "5",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "SpotNo2", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "Depth", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Remarks2", "", "", false, "", false, "100","PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine5(0);")
                                };
                var data = (from uc in lstResult
                            select new[]
                            {"5",
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                               Helper.GenerateHTMLTextbox(uc.LineId, "SpotNo2",  Convert.ToString(uc.SpotNo),"", true, "", false, "10","PROD"),
                               Helper.GenerateNumericTextbox(uc.LineId, "Depth", Convert.ToString(uc.Depth), "", true, "", false, "10","PROD"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "Remarks2", Convert.ToString(uc.Remarks), "", true, "", false, "100","PROD"),
                            Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.PROD3.GetStringValue()? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLinkageLine5','tblProtocolLines5')"):"")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLinkageLine5(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRL044_2 objPRL044_2 = new PRL044_2();
            int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                if (refHeaderId > 0)
                {
                    string SpotNo = !string.IsNullOrEmpty(fc["SpotNo2" + refLineId]) ? Convert.ToString(fc["SpotNo2" + refLineId]).Trim() : "";
                    string Remarks = !string.IsNullOrEmpty(fc["Remarks2" + refLineId]) ? Convert.ToString(fc["Remarks2" + refLineId]).Trim() : "";
                    decimal? Depth = (new clsManager()).GetDecimalValue(Convert.ToString(fc["Depth" + refLineId]));
                    if (refLineId > 0)
                    {
                        objPRL044_2 = db.PRL044_2.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRL044_2.EditedBy = objClsLoginInfo.UserName;
                        objPRL044_2.EditedOn = DateTime.Now;
                    }
                    objPRL044_2.HeaderId = refHeaderId;
                    objPRL044_2.SpotNo = SpotNo;
                    objPRL044_2.Depth = Depth;
                    objPRL044_2.Remarks = Remarks;
                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.SpotUpdate;
                    }
                    else
                    {
                        objPRL044_2.CreatedBy = objClsLoginInfo.UserName;
                        objPRL044_2.CreatedOn = DateTime.Now;
                        db.PRL044_2.Add(objPRL044_2);
                        db.SaveChanges();
                        db.SaveChanges();
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.SpotAdded;
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRL044_2.LineId;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLinkageLine5(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRL044_2 objPRL044_2 = db.PRL044_2.FirstOrDefault(c => c.LineId == lineId);
                if (objPRL044_2 != null)
                {
                    db.PRL044_2.Remove(objPRL044_2);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Clat striping depth deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #endregion

    }
}