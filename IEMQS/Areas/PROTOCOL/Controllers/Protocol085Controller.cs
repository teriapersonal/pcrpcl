﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;


namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol085Controller : clsBase
    {
        // GET: PROTOCOL/Protocol085
        string ControllerURL = "/PROTOCOL/Protocol085/";
        string Title = "REPORT FOR WELD VISUAL & DIMENSION OF SPOOL TYPE-1 (BOTH SIDE FLANGES)";

        #region Details View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO420 objPRO420 = new PRO420();
            if (!id.HasValue)
            {
                try
                {
                    objPRO420.ProtocolNo = string.Empty;
                    objPRO420.CreatedBy = objClsLoginInfo.UserName;
                    objPRO420.CreatedOn = DateTime.Now;

                    db.PRO420.Add(objPRO420);
                    db.SaveChanges();
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO420.HeaderId;
            }
            else
            {
                objPRO420 = db.PRO420.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstyesnona = clsImplementationEnum.getYesNoNA().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNoNAEnum = lstyesnona.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO421> lstPRO421 = db.PRO421.Where(x => x.HeaderId == objPRO420.HeaderId).ToList();

            ViewBag.lstPRO421 = lstPRO421;

            #endregion

            return View(objPRO420);
        }

        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO420 PRO420)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO420.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO420 objPRO420 = db.PRO420.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO420.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO420.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data

                        objPRO420.ProtocolNo = PRO420.ProtocolNo;
                        objPRO420.IsBoltHolesStraddledForTopFlange = PRO420.IsBoltHolesStraddledForTopFlange;
                        objPRO420.IsBoltHolesStraddledForBottomFlange = PRO420.IsBoltHolesStraddledForBottomFlange;
                        objPRO420.ReqTolTiltingOfTopFlange = PRO420.ReqTolTiltingOfTopFlange;
                        objPRO420.ReqTolTiltingOfBottomFlange = PRO420.ReqTolTiltingOfBottomFlange;
                        objPRO420.ReqTolBoltHolesStraddledForTopFlange = PRO420.ReqTolBoltHolesStraddledForTopFlange;
                        objPRO420.ReqTolBoltHolesStraddledForBottomFlange = PRO420.ReqTolBoltHolesStraddledForBottomFlange;
                        objPRO420.ActBoltHolesStraddledForTopFlange = PRO420.ActBoltHolesStraddledForTopFlange;
                        objPRO420.ActBoltHolesStraddledForBottomFlange = PRO420.ActBoltHolesStraddledForBottomFlange;
                        objPRO420.ActTiltingOfTopFlange = PRO420.ActTiltingOfTopFlange;
                        objPRO420.ActTiltingOfBottomFlange = PRO420.ActTiltingOfBottomFlange;
                        objPRO420.ReqOrientationTopFlangeWithResToBottomFlange = PRO420.ReqOrientationTopFlangeWithResToBottomFlange;
                        objPRO420.ActOrientationTopFlangeWithResToBottomFlange = PRO420.ActOrientationTopFlangeWithResToBottomFlange;
                        objPRO420.ReqElevationOfTopFlangeCenterFromBottom = PRO420.ReqElevationOfTopFlangeCenterFromBottom;
                        objPRO420.ActElevationOfTopFlangeCenterFromBottom = PRO420.ActElevationOfTopFlangeCenterFromBottom;
                        objPRO420.ReqProjecttionFromSpoolCenterToTop = PRO420.ReqProjecttionFromSpoolCenterToTop;
                        objPRO420.ActProjecttionFromSpoolCenterToTop = PRO420.ActProjecttionFromSpoolCenterToTop;

                        objPRO420.CheckPoint1 = PRO420.CheckPoint1;
                        objPRO420.CheckPoint3 = PRO420.CheckPoint3;
                        objPRO420.CheckPoint4 = PRO420.CheckPoint4;
                        objPRO420.CheckPoint5 = PRO420.CheckPoint5;
                        objPRO420.CheckPoint6 = PRO420.CheckPoint6;
                        objPRO420.CheckPoint7 = PRO420.CheckPoint7;
                        objPRO420.CheckPoint8 = PRO420.CheckPoint8;
                        objPRO420.CheckPoint9 = PRO420.CheckPoint9;
                        objPRO420.CheckPoint10 = PRO420.CheckPoint10;
                        objPRO420.CheckPoint10_2 = PRO420.CheckPoint10_2;

                        objPRO420.QCRemarks = PRO420.QCRemarks;
                        objPRO420.Result = PRO420.Result;

                        objPRO420.EditedBy = objClsLoginInfo.UserName;
                        objPRO420.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO420.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO420 objPRO420 = db.PRO420.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO420 != null && string.IsNullOrWhiteSpace(objPRO420.ProtocolNo))
                    {
                        db.PRO420.Remove(objPRO420);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO420.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO421> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO421> lstAddPRO421 = new List<PRO421>();
                List<PRO421> lstDeletePRO421 = new List<PRO421>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO421 obj = db.PRO421.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.Description = item.Description;
                                obj.ReqElevation = item.ReqElevation;
                                obj.ActElevation = item.ActElevation;
                                obj.ReqOrientation = item.ReqOrientation;
                                obj.ActOrientation = item.ActOrientation;
                                obj.ReqProjection = item.ReqProjection;
                                obj.ActProjection = item.ActProjection;
                                obj.ReqSize = item.ReqSize;
                                obj.ActSize = item.ActSize;
                                obj.ReqTilt = item.ReqTilt;
                                obj.ActTilt = item.ActTilt;

                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO421 obj = new PRO421();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.Description))
                            {
                                obj.Description = item.Description;
                                obj.ReqElevation = item.ReqElevation;
                                obj.ActElevation = item.ActElevation;
                                obj.ReqOrientation = item.ReqOrientation;
                                obj.ActOrientation = item.ActOrientation;
                                obj.ReqProjection = item.ReqProjection;
                                obj.ActProjection = item.ActProjection;
                                obj.ReqSize = item.ReqSize;
                                obj.ActSize = item.ActSize;
                                obj.ReqTilt = item.ReqTilt;
                                obj.ActTilt = item.ActTilt;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO421.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO421.Count > 0)
                    {
                        db.PRO421.AddRange(lstAddPRO421);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO421 = db.PRO421.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO421.Count > 0)
                    {
                        db.PRO421.RemoveRange(lstDeletePRO421);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO421 = db.PRO421.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO421.Count > 0)
                    {
                        db.PRO421.RemoveRange(lstDeletePRO421);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #endregion

        #region Linkage View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL420 objPRL420 = new PRL420();
            objPRL420 = db.PRL420.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL420 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL420.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstyesnona = clsImplementationEnum.getYesNoNA().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNoNAEnum = lstyesnona.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL421> lstPRL421 = db.PRL421.Where(x => x.HeaderId == objPRL420.HeaderId).ToList();

            ViewBag.lstPRL421 = lstPRL421;

            #endregion

            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.DrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL420.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL420.ActFilledBy) && (objPRL420.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL420.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL420);
        }

        [HttpPost]
        public ActionResult LinkageMobile(int? id, int? m, string APIRequestFrom = "", string UserName = "")
        {
            string ProjectDesc = "";
            string ManufacturingCode = "";
            string AccessRole = "";
            bool isEditable = false;
            var DrawingNo = new List<string>();
            bool isQCRemarkEditable = false;

            PRL420 objPRL420 = new PRL420();
            db.Configuration.ProxyCreationEnabled = false;
            objPRL420 = db.PRL420.Where(i => i.HeaderId == id).FirstOrDefault();
            db.Entry(objPRL420).State = System.Data.Entity.EntityState.Detached;
            NDEModels objNDEModels = new NDEModels();
            if (objPRL420 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL420.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
            AccessRole = objUserRoleAccessDetails.UserDesignation;


            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstyesnona = clsImplementationEnum.getYesNoNA().ToList();

            var YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var YesNoNAEnum = lstyesnona.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL421> lstPRL421 = db.PRL421.Where(x => x.HeaderId == objPRL420.HeaderId).ToList();

            #endregion
            try
            {
                isEditable = false;
                isQCRemarkEditable = false;
                if (m == 1)
                {
                    isEditable = true;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL420.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL420.ActFilledBy) && (objPRL420.RequestNo.HasValue))
                        {
                            isEditable = false;
                            isQCRemarkEditable = true;
                        }
                        else
                        {
                            DrawingNo = (new GeneralController()).GetPLMDrawingNumber(objPRL420.Project).ToList();
                        }
                    }
                }
                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    ProjectDesc = ProjectDesc,
                    ManufacturingCode = ManufacturingCode,
                    isEditable = isEditable,
                    isQCRemarkEditable = isQCRemarkEditable,
                    DrawingNo = DrawingNo,
                    AccessRole = AccessRole,
                    YesNoEnum = YesNoEnum,
                    YesNAEnum = YesNAEnum,
                    YesNoNAEnum = YesNoNAEnum,

                    objPRL420 = objPRL420,

                    lstPRL421 = lstPRL421

                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                return Json(new
                {
                    res = "0",
                    msg = ex.Message,
                }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL420 prl420, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = prl420.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL420 objPRL420 = db.PRL420.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL420.DrawingRevisionNo = prl420.DrawingRevisionNo;
                    objPRL420.DrawingNo = prl420.DrawingNo;
                    objPRL420.DCRNo = prl420.DCRNo;
                    objPRL420.ProtocolNo = prl420.ProtocolNo;

                    objPRL420.IsBoltHolesStraddledForTopFlange = prl420.IsBoltHolesStraddledForTopFlange;
                    objPRL420.IsBoltHolesStraddledForBottomFlange = prl420.IsBoltHolesStraddledForBottomFlange;
                    objPRL420.ReqTolTiltingOfTopFlange = prl420.ReqTolTiltingOfTopFlange;
                    objPRL420.ReqTolTiltingOfBottomFlange = prl420.ReqTolTiltingOfBottomFlange;
                    objPRL420.ReqTolBoltHolesStraddledForTopFlange = prl420.ReqTolBoltHolesStraddledForTopFlange;
                    objPRL420.ReqTolBoltHolesStraddledForBottomFlange = prl420.ReqTolBoltHolesStraddledForBottomFlange;
                    objPRL420.ActBoltHolesStraddledForTopFlange = prl420.ActBoltHolesStraddledForTopFlange;
                    objPRL420.ActBoltHolesStraddledForBottomFlange = prl420.ActBoltHolesStraddledForBottomFlange;
                    objPRL420.ActTiltingOfTopFlange = prl420.ActTiltingOfTopFlange;
                    objPRL420.ActTiltingOfBottomFlange = prl420.ActTiltingOfBottomFlange;
                    objPRL420.ReqOrientationTopFlangeWithResToBottomFlange = prl420.ReqOrientationTopFlangeWithResToBottomFlange;
                    objPRL420.ActOrientationTopFlangeWithResToBottomFlange = prl420.ActOrientationTopFlangeWithResToBottomFlange;
                    objPRL420.ReqElevationOfTopFlangeCenterFromBottom = prl420.ReqElevationOfTopFlangeCenterFromBottom;
                    objPRL420.ActElevationOfTopFlangeCenterFromBottom = prl420.ActElevationOfTopFlangeCenterFromBottom;
                    objPRL420.ReqProjecttionFromSpoolCenterToTop = prl420.ReqProjecttionFromSpoolCenterToTop;
                    objPRL420.ActProjecttionFromSpoolCenterToTop = prl420.ActProjecttionFromSpoolCenterToTop;

                    objPRL420.CheckPoint1 = prl420.CheckPoint1;
                    objPRL420.CheckPoint3 = prl420.CheckPoint3;
                    objPRL420.CheckPoint4 = prl420.CheckPoint4;
                    objPRL420.CheckPoint5 = prl420.CheckPoint5;
                    objPRL420.CheckPoint6 = prl420.CheckPoint6;
                    objPRL420.CheckPoint7 = prl420.CheckPoint7;
                    objPRL420.CheckPoint8 = prl420.CheckPoint8;
                    objPRL420.CheckPoint9 = prl420.CheckPoint9;
                    objPRL420.CheckPoint10 = prl420.CheckPoint10;
                    objPRL420.CheckPoint10_2 = prl420.CheckPoint10_2;

                    objPRL420.QCRemarks = prl420.QCRemarks;
                    objPRL420.Result = prl420.Result;

                    objPRL420.EditedBy = UserName;
                    objPRL420.EditedOn = DateTime.Now;
                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL420.ActFilledBy = UserName;
                            objPRL420.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL420.ReqFilledBy = UserName;
                            objPRL420.ReqFilledOn = DateTime.Now;
                        }
                    }
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL420.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL420 objPRL420 = db.PRL420.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL420 != null && string.IsNullOrWhiteSpace(objPRL420.ProtocolNo))
                    {
                        db.PRL420.Remove(objPRL420);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL420.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL421> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL421> lstAddPRL421 = new List<PRL421>();
                List<PRL421> lstDeletePRL421 = new List<PRL421>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL421 obj = db.PRL421.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL421();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Description = item.Description;
                        obj.ReqElevation = item.ReqElevation;
                        obj.ActElevation = item.ActElevation;
                        obj.ReqOrientation = item.ReqOrientation;
                        obj.ActOrientation = item.ActOrientation;
                        obj.ReqProjection = item.ReqProjection;
                        obj.ActProjection = item.ActProjection;
                        obj.ReqSize = item.ReqSize;
                        obj.ActSize = item.ActSize;
                        obj.ReqTilt = item.ReqTilt;
                        obj.ActTilt = item.ActTilt;

                        if (isAdded)
                        {
                            lstAddPRL421.Add(obj);
                        }
                    }
                    if (lstAddPRL421.Count > 0)
                    {
                        db.PRL421.AddRange(lstAddPRL421);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL421 = db.PRL421.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL421.Count > 0)
                    {
                        db.PRL421.RemoveRange(lstDeletePRL421);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL421 = db.PRL421.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL421.Count > 0)
                    {
                        db.PRL421.RemoveRange(lstDeletePRL421);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #endregion
    }
}