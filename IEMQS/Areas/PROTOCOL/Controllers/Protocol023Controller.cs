﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol023Controller : clsBase
    {
        string ControllerURL = "/PROTOCOL/Protocol023/";
        string Title = "HELIUM LEAK TEST REPORT";
        
        // GET: PROTOCOL/Protocol023
        #region Details View Code
        #region Header
        [SessionExpireFilter] //, UserPermissions, AllowAnonymous]
        public ActionResult Details(int? id,int? m)
        {
            PRO110 objPRO110 = new PRO110();
            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            List<string> lstYesNoNA = clsImplementationEnum.getYesNoNA().ToList();
            ViewBag.YesNoNAEnum = lstYesNoNA.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            if (!id.HasValue)
            {
                objPRO110.ProtocolNo = string.Empty;
                objPRO110.CreatedBy = objClsLoginInfo.UserName;
                objPRO110.CreatedOn = DateTime.Now;
                db.PRO110.Add(objPRO110);
                db.SaveChanges();
                ViewBag.HeaderId = objPRO110.HeaderId;
            }
            else
            {
                objPRO110 = db.PRO110.Where(x => x.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }

            return View(objPRO110);
        }
        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO110 PRO110)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO110.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO110 objPRO110 = db.PRO110.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO110.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO110.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data
                        objPRO110.ProtocolNo = PRO110.ProtocolNo;
                        objPRO110.InspectionCertificateNo = PRO110.InspectionCertificateNo;
                        objPRO110.Date = PRO110.Date.HasValue ? Manager.getDateTime(PRO110.Date.Value.ToShortDateString()) : PRO110.Date;
                        objPRO110.InspectionAgency = PRO110.InspectionAgency;
                        objPRO110.DrawingNo = PRO110.DrawingNo;
                        objPRO110.TestDecription = PRO110.TestDecription;
                        objPRO110.StageOfInspection = PRO110.StageOfInspection;
                        objPRO110.ProcedureNo = PRO110.ProcedureNo;
                        objPRO110.NameOfHeliumLeakDetectorMachine = PRO110.NameOfHeliumLeakDetectorMachine;
                        objPRO110.ModelNo = PRO110.ModelNo;
                        objPRO110.TestPressure = PRO110.TestPressure;
                        objPRO110.HoldingTime = PRO110.HoldingTime;
                        objPRO110.LeakRateMeasuredOnScale = PRO110.LeakRateMeasuredOnScale;
                        objPRO110.LeakRateOfStandardLeak = PRO110.LeakRateOfStandardLeak;
                        objPRO110.BaselineReadingAfter = PRO110.BaselineReadingAfter;
                        objPRO110.AnyDeflectionOnIndicator = PRO110.AnyDeflectionOnIndicator;
                        objPRO110.ObservationAndRemarks = PRO110.ObservationAndRemarks;
                        objPRO110.LeakRateObserved = PRO110.LeakRateObserved;
                        objPRO110.AcceptanceCriteria = PRO110.AcceptanceCriteria;

                        objPRO110.EditedBy = objClsLoginInfo.UserName;
                        objPRO110.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol saved successfully";
                        objResponseMsg.HeaderId = objPRO110.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO110 objPRO110 = db.PRO110.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO110 != null && string.IsNullOrWhiteSpace(objPRO110.ProtocolNo))
                    {
                        db.PRO111.RemoveRange(objPRO110.PRO111.ToList());
                        db.PRO110.Remove(objPRO110);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.HELIUM_LEAK_TEST_REPORT.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO110.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public ActionResult GetProtocolLines1Data(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro.GaugeNo like '%" + param.sSearch
                                    + "%' or pro.CalibrationUpto like '%" + param.sSearch
                        + "%')";
                }

                var lstResult = db.SP_IPI_PROTOCOL023_GET_LINES1_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                                      "1",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "GaugeNo", "", "", false, "", false, "20","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "CalibrationUpto", "", "", false, "", false, "","QC"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine1(0);")
                                };
                var data = (from uc in lstResult
                            select new[]
                            {
                                "1",
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.LineId),

                                Helper.GenerateHTMLTextbox(uc.LineId, "GaugeNo", Convert.ToString(uc.GaugeNo), "", true, "", false, "20","QC"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "CalibrationUpto",uc.CalibrationUpto==null || uc.CalibrationUpto.Value==DateTime.MinValue ? "": Convert.ToDateTime(uc.CalibrationUpto).ToString("yyyy-MM-dd"), "", true, "", false,"","QC"),
                                Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == "QC3" ? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine1','tblProtocolLines1')"):"")

                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }
        public ActionResult SaveProtocolLine1(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRO111 objPRO111 = new PRO111();

            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;

                if (refHeaderId > 0)
                {
                    string strGaugeNo = !string.IsNullOrEmpty(fc["GaugeNo" + refLineId]) ? Convert.ToString(fc["GaugeNo" + refLineId]).Trim() : "";
                    DateTime dateCalibrationUpto = !string.IsNullOrEmpty(fc["CalibrationUpto" + refLineId]) ? Convert.ToDateTime(fc["CalibrationUpto" + refLineId].ToString()).Date : DateTime.MinValue;

                    #region Add New Spot Line1
                    if (refLineId > 0)
                    {
                        objPRO111 = db.PRO111.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRO111.EditedBy = objClsLoginInfo.UserName;
                        objPRO111.EditedOn = DateTime.Now;
                    }
                    objPRO111.HeaderId = refHeaderId;
                    objPRO111.GaugeNo = strGaugeNo;
                    if (!string.IsNullOrEmpty(fc["CalibrationUpto" + refLineId]) && dateCalibrationUpto != DateTime.MinValue)
                    {
                        objPRO111.CalibrationUpto = dateCalibrationUpto;
                    }
                    else
                    {
                        objPRO111.CalibrationUpto = null;
                    }
                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = "Data updated successfully";
                    }
                    else
                    {
                        objPRO111.CreatedBy = objClsLoginInfo.UserName;
                        objPRO111.CreatedOn = DateTime.Now;
                        db.PRO111.Add(objPRO111);
                        db.SaveChanges();
                        objResponseMsg.Value = "Data added successfully";
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRO111.LineId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeleteProtocolLine1(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRO111 objPRO111 = db.PRO111.FirstOrDefault(c => c.LineId == lineId);
                if (objPRO111 != null)
                {
                    db.PRO111.Remove(objPRO111);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #endregion

        #region Linkage View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL110 objPRL110 = new PRL110();
            objPRL110 = db.PRL110.Where(i => i.HeaderId == id).FirstOrDefault();

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;
            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            if (objPRL110 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL110.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objQproject.ManufacturingCode;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }
            if (m == 1)
            {
                ViewBag.isEditable = "true";
            }
            else
            { ViewBag.isEditable = "false"; }

            return View(objPRL110);
        }
        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL110 PRL110)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRL110.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL110 objPRL110 = db.PRL110.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data

                    objPRL110.ProtocolNo = PRL110.ProtocolNo != null ? PRL110.ProtocolNo : "";
                    objPRL110.InspectionCertificateNo = PRL110.InspectionCertificateNo;
                    objPRL110.Date = PRL110.Date.HasValue ? Manager.getDateTime(PRL110.Date.Value.ToShortDateString()) : PRL110.Date;
                    objPRL110.InspectionAgency = PRL110.InspectionAgency;
                    objPRL110.DrawingNo = PRL110.DrawingNo;
                    objPRL110.TestDecription = PRL110.TestDecription;
                    objPRL110.StageOfInspection = PRL110.StageOfInspection;
                    objPRL110.ProcedureNo = PRL110.ProcedureNo;
                    objPRL110.NameOfHeliumLeakDetectorMachine = PRL110.NameOfHeliumLeakDetectorMachine;
                    objPRL110.ModelNo = PRL110.ModelNo;
                    objPRL110.TestPressure = PRL110.TestPressure;
                    objPRL110.HoldingTime = PRL110.HoldingTime;
                    objPRL110.LeakRateMeasuredOnScale = PRL110.LeakRateMeasuredOnScale;
                    objPRL110.LeakRateOfStandardLeak = PRL110.LeakRateOfStandardLeak;
                    objPRL110.BaselineReadingAfter = PRL110.BaselineReadingAfter;
                    objPRL110.AnyDeflectionOnIndicator = PRL110.AnyDeflectionOnIndicator;
                    objPRL110.ObservationAndRemarks = PRL110.ObservationAndRemarks;
                    objPRL110.LeakRateObserved = PRL110.LeakRateObserved;
                    objPRL110.AcceptanceCriteria = PRL110.AcceptanceCriteria;

                    objPRL110.EditedBy = objClsLoginInfo.UserName;
                    objPRL110.EditedOn = DateTime.Now;

                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol saved successfully";
                    objResponseMsg.HeaderId = objPRL110.HeaderId;
                    #endregion

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL110 objPRL110 = db.PRL110.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL110 != null && string.IsNullOrWhiteSpace(objPRL110.ProtocolNo))
                    {
                        db.PRL111.RemoveRange(objPRL110.PRL111.ToList());
                        db.PRL110.Remove(objPRL110);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL110.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 Linkage
        [HttpPost]
        public ActionResult GetProtocolLines1DataLinkage(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro.GaugeNo like '%" + param.sSearch
                        + "%' or pro.CalibrationUpto like '%" + param.sSearch
                    + "%')";
                }

                var lstResult = db.SP_IPI_PROTOCOL023_LINKAGE_GET_LINES1_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                                    "1",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "GaugeNo", "", "", false, "", false, "20","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "CalibrationUpto", "", "", false, "", false,"", "QC"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine1(0);")
                                };
                var data = (from uc in lstResult
                            select new[]
                            {
                                "1",
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                                Helper.GenerateHTMLTextbox(uc.LineId, "GaugeNo", Convert.ToString(uc.GaugeNo), "", true, "", false, "20","QC"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "CalibrationUpto", uc.CalibrationUpto==null || uc.CalibrationUpto.Value==DateTime.MinValue ? "": Convert.ToDateTime(uc.CalibrationUpto).ToString("yyyy-MM-dd"), "", true, "", false,"","QC"),
                                Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == "QC3" ? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine1Linkage','tblProtocolLines1')"):"")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult SaveProtocolLine1Linkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRL111 objPRL111 = new PRL111();

            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
                if (refHeaderId > 0)
                {
                    string strGaugeNo = !string.IsNullOrEmpty(fc["GaugeNo" + refLineId]) ? Convert.ToString(fc["GaugeNo" + refLineId]).Trim() : "";
                    DateTime dateCalibrationUpto = !string.IsNullOrEmpty(fc["CalibrationUpto" + refLineId]) ? Convert.ToDateTime(fc["CalibrationUpto" + refLineId].ToString()).Date : DateTime.MinValue;

                    #region Add New Spot Line1
                    if (refLineId > 0)
                    {
                        objPRL111 = db.PRL111.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRL111.EditedBy = objClsLoginInfo.UserName;
                        objPRL111.EditedOn = DateTime.Now;
                    }
                    objPRL111.HeaderId = refHeaderId;
                    objPRL111.GaugeNo = strGaugeNo;
                    if (!string.IsNullOrEmpty(fc["CalibrationUpto" + refLineId]) && dateCalibrationUpto != DateTime.MinValue)
                    {
                        objPRL111.CalibrationUpto = dateCalibrationUpto;
                    }
                    else
                    {
                        objPRL111.CalibrationUpto = null;
                    }
                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = "Data updated successfully";
                    }
                    else
                    {
                        objPRL111.CreatedBy = objClsLoginInfo.UserName;
                        objPRL111.CreatedOn = DateTime.Now;
                        db.PRL111.Add(objPRL111);
                        db.SaveChanges();
                        objResponseMsg.Value = "Data added successfully";
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRL111.LineId;
                    #endregion
                }
            }


            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolLine1Linkage(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRL111 objPRL111 = db.PRL111.FirstOrDefault(c => c.LineId == lineId);
                if (objPRL111 != null)
                {
                    db.PRL111.Remove(objPRL111);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #endregion
   }
}