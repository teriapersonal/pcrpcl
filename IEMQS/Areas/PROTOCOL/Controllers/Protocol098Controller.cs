﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol098Controller : clsBase
    {
        // GET: PROTOCOL/Protocol098
        string ControllerURL = "/PROTOCOL/Protocol098/";
        string Title = "FINAL DIMENSION AND VISUAL INSPECTION REPORT";
        //IEMQS.DrawingReference.hedProjectDocListWebServiceClient drawref = new IEMQS.DrawingReference.hedProjectDocListWebServiceClient();

        #region Details View Code
        //       #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO480 objPRO480 = new PRO480();
            if (!id.HasValue)
            {
                ViewBag.isEditable = "true";
                try
                {
                    //   objPRO480.LTProjectNo = string.Empty;
                    objPRO480.CreatedBy = objClsLoginInfo.UserName;
                    objPRO480.CreatedOn = DateTime.Now;
                    #region  CIRCUMFERENCE MEASUREMENT
                    objPRO480.PRO482.Add(new PRO482
                    {
                        Location = "SHELL-1",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });

                    objPRO480.PRO482.Add(new PRO482
                    {
                        Location = "SHELL-2",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO480.PRO482.Add(new PRO482
                    {
                        Location = "SHELL-3",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO480.PRO482.Add(new PRO482
                    {
                        Location = "SHELL-4",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO480.PRO482.Add(new PRO482
                    {
                        Location = "SHELL-5",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO480.PRO482.Add(new PRO482
                    {
                        Location = "SHELL-6",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO480.PRO482.Add(new PRO482
                    {
                        Location = "SHELL-7",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO480.PRO482.Add(new PRO482
                    {
                        Location = "SHELL-8",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO480.PRO482.Add(new PRO482
                    {
                        Location = "SHELL-9",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO480.PRO482.Add(new PRO482
                    {
                        Location = "SHELL-10",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO480.PRO482.Add(new PRO482
                    {
                        Location = "SHELL-11",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO480.PRO482.Add(new PRO482
                    {
                        Location = "SHELL-12",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO480.PRO482.Add(new PRO482
                    {
                        Location = "SHELL-13",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO480.PRO482.Add(new PRO482
                    {
                        Location = "SHELL-14",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO480.PRO482.Add(new PRO482
                    {
                        Location = "SHELL-15",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO480.PRO482.Add(new PRO482
                    {
                        Location = "SHELL-16",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO480.PRO482.Add(new PRO482
                    {
                        Location = "SHELL-17",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO480.PRO482.Add(new PRO482
                    {
                        Location = "SHELL-18",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO480.PRO482.Add(new PRO482
                    {
                        Location = "SHELL-19",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO480.PRO482.Add(new PRO482
                    {
                        Location = "SHELL-20",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO480.PRO482.Add(new PRO482
                    {
                        Location = "SHELL-21",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion

                    #region LENGTH DIMENSION
                    objPRO480.PRO483.Add(new PRO483
                    {
                        Orientation = "AT 0°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO480.PRO483.Add(new PRO483
                    {
                        Orientation = "AT 45°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO480.PRO483.Add(new PRO483
                    {
                        Orientation = "AT 90°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO480.PRO483.Add(new PRO483
                    {
                        Orientation = "AT 135°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO480.PRO483.Add(new PRO483
                    {
                        Orientation = "AT 180°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO480.PRO483.Add(new PRO483
                    {
                        Orientation = "AT 225°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO480.PRO483.Add(new PRO483
                    {
                        Orientation = "AT 270°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO480.PRO483.Add(new PRO483
                    {
                        Orientation = "AT 315°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion

                    #region Notes

                    objPRO480.PRO484.Add(new PRO484 { NotesType = "LENGTH DIMENSION", Notes = "1) ALL DIMENSIONS ARE IN 'MM' UNLESS SPECIFIED.", CreatedBy = objClsLoginInfo.UserName, CreatedOn = DateTime.Now });
                    objPRO480.PRO484.Add(new PRO484 { NotesType = "LENGTH DIMENSION", Notes = "2) DIMENSIONS OF OPEN END WEP CHECKED &FOUND SATISFACTORY.", CreatedBy = objClsLoginInfo.UserName, CreatedOn = DateTime.Now });
                    objPRO480.PRO484.Add(new PRO484 { NotesType = "LENGTH DIMENSION", Notes = "3) FINAL VISUAL INSPECTION OF INSIDE & OUTSIDE SURFACE AND ALL WELDS CARRIED OUT AND FOUND SATISFACTORY.", CreatedBy = objClsLoginInfo.UserName, CreatedOn = DateTime.Now });
                    objPRO480.PRO484.Add(new PRO484 { NotesType = "LENGTH DIMENSION", Notes = "4) MEASURING AND TESTING EQUIPMENT SERIAL NUMBER &CALIBRATION DUE DATE :", CreatedBy = objClsLoginInfo.UserName, CreatedOn = DateTime.Now });
                    objPRO480.PRO484.Add(new PRO484 { NotesType = "LENGTH DIMENSION", Notes = "5) FINAL VISUAL INSPECTION OF SEAM AND SURFACE IS CHECKED AND FOUND SATISFACTORY.", CreatedBy = objClsLoginInfo.UserName, CreatedOn = DateTime.Now });
                    objPRO480.PRO484.Add(new PRO484 { NotesType = "LENGTH DIMENSION", Notes = "6) REINFORCEMENT OF WELD JOINTS IS CHECKED AND FOUND WITHIN LIMITS AS PER TABLE 6.6 OF ASME SEC VIII DIV 2.", CreatedBy = objClsLoginInfo.UserName, CreatedOn = DateTime.Now });
                    objPRO480.PRO484.Add(new PRO484 { NotesType = "LENGTH DIMENSION", Notes = "7) OFFSET IN BUTT WELD JOINTS IS CHECKED AND FOUND WITHIN LIMITS AS PER TABLE 6.4 OF ASME SEC VIII DIV 2.", CreatedBy = objClsLoginInfo.UserName, CreatedOn = DateTime.Now });
                    objPRO480.PRO484.Add(new PRO484 { NotesType = "LENGTH DIMENSION", Notes = "8) MAXIMUM DEVIATION FROM TRUE CIRCULAR FORM 'e' ON SHELL CHECKED WITH TEMPLATE HAVING CHORD LENGTH OF____MM AND FOUND ____MM MAXIMUM(ALLOWED ___MM MAX.).", CreatedBy = objClsLoginInfo.UserName, CreatedOn = DateTime.Now });
                    objPRO480.PRO484.Add(new PRO484 { NotesType = "LENGTH DIMENSION", Notes = "9) MAXIMUM DEVIATION FROM TRUE CIRCULAR FORM 'e' IN HEAD CHECKED WITH TEMPLATE HAVING CHORD LENGTH OF ____MM AND FOUND____MM MAXIMUM(ALLOWED ___MM MAX.).", CreatedBy = objClsLoginInfo.UserName, CreatedOn = DateTime.Now });
                    objPRO480.PRO484.Add(new PRO484 { NotesType = "LENGTH DIMENSION", Notes = "10) MAXIMUM PEAKING HEIGHT(DP) AT CATEGORY 'A' JOINTS IN HEAD CHEKED WITH TEMPLATE HAVING CHORD LENGTH OF _____MM AND FOUND______MM MAXIMUM.", CreatedBy = objClsLoginInfo.UserName, CreatedOn = DateTime.Now });
                    objPRO480.PRO484.Add(new PRO484 { NotesType = "LENGTH DIMENSION", Notes = "11) OUT OF ROUNDNESS OF SHELL CHECKED AND FOUND _____MM MAXIMUM.,  this remarks shall be editable at time of ICL approval also and at time of Atending entry also", CreatedBy = objClsLoginInfo.UserName, CreatedOn = DateTime.Now });
                    #endregion

                    /*

                    #region DIMENSION OF WELDED TRAY SUPPORT RING 
                    objPRO480.PRO484_6.Add(new PRO484_6
                    {
                        TsrOrientation = "0°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO480.PRO484_6.Add(new PRO484_6
                    {
                        TsrOrientation = "30°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO480.PRO484_6.Add(new PRO484_6
                    {
                        TsrOrientation = "60°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO480.PRO484_6.Add(new PRO484_6
                    {
                        TsrOrientation = "90°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO480.PRO484_6.Add(new PRO484_6
                    {
                        TsrOrientation = "120°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO480.PRO484_6.Add(new PRO484_6
                    {
                        TsrOrientation = "150°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO480.PRO484_6.Add(new PRO484_6
                    {
                        TsrOrientation = "180°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO480.PRO484_6.Add(new PRO484_6
                    {
                        TsrOrientation = "210°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO480.PRO484_6.Add(new PRO484_6
                    {
                        TsrOrientation = "240°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO480.PRO484_6.Add(new PRO484_6
                    {
                        TsrOrientation = "270°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO480.PRO484_6.Add(new PRO484_6
                    {
                        TsrOrientation = "300°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO480.PRO484_6.Add(new PRO484_6
                    {
                        TsrOrientation = "330°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion
                    */

                    #region DIMENSION OF NUB ON BOTTOM HEAD
                    objPRO480.PRO484_7.Add(new PRO484_7
                    {
                        Orientation = "AT 0°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO480.PRO484_7.Add(new PRO484_7
                    {
                        Orientation = "AT 45°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO480.PRO484_7.Add(new PRO484_7
                    {
                        Orientation = "AT 90°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO480.PRO484_7.Add(new PRO484_7
                    {
                        Orientation = "AT 135°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO480.PRO484_7.Add(new PRO484_7
                    {
                        Orientation = "AT 180°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO480.PRO484_7.Add(new PRO484_7
                    {
                        Orientation = "AT 225°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO480.PRO484_7.Add(new PRO484_7
                    {
                        Orientation = "AT 270°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO480.PRO484_7.Add(new PRO484_7
                    {
                        Orientation = "AT 315°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion

                    db.PRO480.Add(objPRO480);
                    db.SaveChanges();
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO480.HeaderId;
            }
            else
            {
                objPRO480 = db.PRO480.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
                if (m == 1)
                { ViewBag.isEditable = "true"; }
                else
                { ViewBag.isEditable = "false"; }
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            //List<string> lstagignment = clsImplementationEnum.getagignment().ToList();

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            //ViewBag.YesNoEnum = list.Where(x => x.Value == "selectedValue").First();
            //selected.Selected = true;

            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            //ViewBag.AgignmentEnum = lstagignment.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var Files = (new clsFileUpload()).GetDocuments("PRO480/Sketch1/" + id);
            if (Files.Count() > 0)
            {
                ViewBag.image1 = Files.FirstOrDefault().URL;
            }
            var Files2 = (new clsFileUpload()).GetDocuments("PRO480/Sketch2/" + id);
            if (Files2.Count() > 0)
            {
                ViewBag.image2 = Files2.FirstOrDefault().URL;
            }
            string[] arrImage3 = new string[objPRO480.PRO484_11.Count()];
            int p = 1, j = 0;
            if (objPRO480.PRO484_11.Count() > 0)
            {
                foreach (var item in objPRO480.PRO484_11)
                {
                    var filename = (new clsFileUpload()).GetDocuments("PRO480/Sketch3/" + id + "/" + item.LineId);
                    if (filename.Count() > 0)
                    {
                        arrImage3[j] = filename.FirstOrDefault().URL;
                    }
                }

            }
            ViewBag.Sketch3 = arrImage3;
            ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO481> lstPRO481 = db.PRO481.Where(x => x.HeaderId == objPRO480.HeaderId).ToList();
            List<PRO482> lstPRO482 = db.PRO482.Where(x => x.HeaderId == objPRO480.HeaderId).ToList();
            List<PRO483> lstPRO483 = db.PRO483.Where(x => x.HeaderId == objPRO480.HeaderId).ToList();

            List<PRO484_1> lstPRO484_1 = db.PRO484_1.Where(x => x.HeaderId == objPRO480.HeaderId).ToList();
            List<PRO484_2> lstPRO484_2 = db.PRO484_2.Where(x => x.HeaderId == objPRO480.HeaderId).ToList();
            List<PRO484_3> lstPRO484_3 = db.PRO484_3.Where(x => x.HeaderId == objPRO480.HeaderId).ToList();
            List<PRO484_4> lstPRO484_4 = db.PRO484_4.Where(x => x.HeaderId == objPRO480.HeaderId).ToList();
            List<PRO484_5> lstPRO484_5 = db.PRO484_5.Where(x => x.HeaderId == objPRO480.HeaderId).ToList();
            List<PRO484_5_1> lstPRO484_5_1 = db.PRO484_5_1.Where(x => x.HeaderId == objPRO480.HeaderId).ToList();
            List<PRO484_6> lstPRO484_6 = db.PRO484_6.Where(x => x.HeaderId == objPRO480.HeaderId).ToList();
            List<PRO484_6_1> lstPRO484_6_1 = db.PRO484_6_1.Where(x => x.HeaderId == objPRO480.HeaderId).ToList();

            List<PRO484_7> lstPRO484_7 = db.PRO484_7.Where(x => x.HeaderId == objPRO480.HeaderId).ToList();
            List<PRO484_8> lstPRO484_8 = db.PRO484_8.Where(x => x.HeaderId == objPRO480.HeaderId).ToList();
            List<PRO484_9> lstPRO484_9 = db.PRO484_9.Where(x => x.HeaderId == objPRO480.HeaderId).ToList();
            List<PRO484_10> lstPRO484_10 = db.PRO484_10.Where(x => x.HeaderId == objPRO480.HeaderId).ToList();
            List<PRO484_11> lstPRO484_11 = db.PRO484_11.Where(x => x.HeaderId == objPRO480.HeaderId).ToList();

            List<PRO484> lstPRO484Notes1 = db.PRO484.Where(x => x.HeaderId == objPRO480.HeaderId && x.NotesType == "LENGTH DIMENSION").ToList();
            List<PRO484> lstPRO484Notes2 = db.PRO484.Where(x => x.HeaderId == objPRO480.HeaderId && x.NotesType == "DIMENSIONS OF NOZZLES").ToList();
            List<PRO484> lstPRO484Notes3 = db.PRO484.Where(x => x.HeaderId == objPRO480.HeaderId && x.NotesType == "DIMENSIONS OF EXTERNAL ATTACHMENTS").ToList();
            List<PRO484> lstPRO484Notes4 = db.PRO484.Where(x => x.HeaderId == objPRO480.HeaderId && x.NotesType == "DIMENSIONS OF INTERNAL ATTACHMENTS").ToList();
            List<PRO484> lstPRO484Notes5 = db.PRO484.Where(x => x.HeaderId == objPRO480.HeaderId && x.NotesType == "DIMENSIONS OF THERMOWELL BOSS").ToList();
            List<PRO484> lstPRO484Notes6 = db.PRO484.Where(x => x.HeaderId == objPRO480.HeaderId && x.NotesType == "DIMENSIONS OF BUTTERING").ToList();
            List<PRO484> lstPRO484Notes7 = db.PRO484.Where(x => x.HeaderId == objPRO480.HeaderId && x.NotesType == "DIMENSION OF WELDED TRAY SUPPORT RING").ToList();
            List<PRO484> lstPRO484Notes8 = db.PRO484.Where(x => x.HeaderId == objPRO480.HeaderId && x.NotesType == "DIMENSION OF NUB/TRAY SUPPORT RING(TSR)").ToList();
            List<PRO484> lstPRO484Notes9 = db.PRO484.Where(x => x.HeaderId == objPRO480.HeaderId && x.NotesType == "DIMENSION OF NUB ON BOTTOM HEAD").ToList();
            List<PRO484> lstPRO484Notes10 = db.PRO484.Where(x => x.HeaderId == objPRO480.HeaderId && x.NotesType == "BOTTOM CATALYST SUPPORT AFTER INSTALLATION").ToList();
            List<PRO484> lstPRO484Notes11 = db.PRO484.Where(x => x.HeaderId == objPRO480.HeaderId && x.NotesType == "SUPPORT BRACKETS FOR MATERIAL TEST BLOCKS").ToList();
            List<PRO484> lstPRO484Notes12 = db.PRO484.Where(x => x.HeaderId == objPRO480.HeaderId && x.NotesType == "DIMENSION OF SUPPORT CLEATS FOR INLET BASKET").ToList();
            List<PRO484> lstPRO484Notes13 = db.PRO484.Where(x => x.HeaderId == objPRO480.HeaderId && x.NotesType == "DIMENSION OF BEAM SUPPORT CLIPS ON NUB").ToList();

            ViewBag.lstPRO484Notes1 = lstPRO484Notes1;
            ViewBag.lstPRO484Notes2 = lstPRO484Notes2;
            ViewBag.lstPRO484Notes3 = lstPRO484Notes3;
            ViewBag.lstPRO484Notes4 = lstPRO484Notes4;
            ViewBag.lstPRO484Notes5 = lstPRO484Notes5;
            ViewBag.lstPRO484Notes6 = lstPRO484Notes6;
            ViewBag.lstPRO484Notes7 = lstPRO484Notes7;
            ViewBag.lstPRO484Notes8 = lstPRO484Notes8;
            ViewBag.lstPRO484Notes9 = lstPRO484Notes9;
            ViewBag.lstPRO484Notes10 = lstPRO484Notes10;
            ViewBag.lstPRO484Notes11 = lstPRO484Notes11;
            ViewBag.lstPRO484Notes12 = lstPRO484Notes12;
            ViewBag.lstPRO484Notes13 = lstPRO484Notes13;

            ViewBag.lstPRO481 = lstPRO481;
            ViewBag.lstPRO482 = lstPRO482;
            ViewBag.lstPRO483 = lstPRO483;

            ViewBag.lstPRO484_1 = lstPRO484_1;
            ViewBag.lstPRO484_2 = lstPRO484_2;
            ViewBag.lstPRO484_3 = lstPRO484_3;
            ViewBag.lstPRO484_4 = lstPRO484_4;
            ViewBag.lstPRO484_5 = lstPRO484_5;
            ViewBag.lstPRO484_5_1 = lstPRO484_5_1;
            ViewBag.lstPRO484_6 = lstPRO484_6;
            ViewBag.lstPRO484_6_1 = lstPRO484_6_1;
            ViewBag.lstPRO484_7 = lstPRO484_7;
            ViewBag.lstPRO484_8 = lstPRO484_8;
            ViewBag.lstPRO484_9 = lstPRO484_9;
            ViewBag.lstPRO484_10 = lstPRO484_10;
            ViewBag.lstPRO484_11 = lstPRO484_11;
            ViewBag.lstPRO484_6_1 = lstPRO484_6_1;
            #endregion

            return View(objPRO480);
        }

        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }
        [HttpPost]
        public JsonResult SaveUpdateTSRNo27(int headerid, int lineid, string tsrno)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRL484_11 objPRL484_11 = new PRL484_11();
                if (!db.PRL484_11.Any(x => x.LineId != lineid && x.TSRNo == tsrno && x.HeaderId == headerid))
                {
                    if (lineid > 0)
                    {
                        objPRL484_11 = db.PRL484_11.Where(x => x.HeaderId == headerid && x.LineId == lineid).FirstOrDefault();
                        objPRL484_11.TSRNo = tsrno;
                        objPRL484_11.HeaderId = headerid;
                    }
                    else
                    {
                        objPRL484_11 = new PRL484_11();
                        objPRL484_11.TSRNo = tsrno;
                        objPRL484_11.CreatedBy = objClsLoginInfo.UserName;
                        objPRL484_11.CreatedOn = DateTime.Now;
                        objPRL484_11.HeaderId = headerid;
                        db.PRL484_11.Add(objPRL484_11);
                    }
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.RequestId = objPRL484_11.LineId;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "TSR No already exists";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Add header
        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO480 PRO480)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO480.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO480 objPRO480 = db.PRO480.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    //var ProtocolExist = db.PRO480.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO480.ProtocolNo).FirstOrDefault();
                    //if (ProtocolExist == null)
                    //{
                    #region Save Data
                    objPRO480.ProtocolNo = PRO480.ProtocolNo;
                    objPRO480.IsSurfaceAlignment = PRO480.IsSurfaceAlignment;
                    objPRO480.SurfaceAlignment = PRO480.SurfaceAlignment;
                    objPRO480.SketchType = PRO480.SketchType;
                    objPRO480.IsSketch = PRO480.IsSketch;
                    objPRO480.SketchPath = PRO480.SketchPath;
                    objPRO480.IsAlignmentReading = PRO480.IsAlignmentReading;
                    objPRO480.AlignmentReadingAt0 = PRO480.AlignmentReadingAt0;
                    objPRO480.IsAlignmentReadingAt0 = PRO480.IsAlignmentReadingAt0;
                    objPRO480.AlignmentReadingAt45 = PRO480.AlignmentReadingAt45;
                    objPRO480.IsAlignmentReadingAt45 = PRO480.IsAlignmentReadingAt45;
                    objPRO480.AlignmentReadingAt90 = PRO480.AlignmentReadingAt90;
                    objPRO480.IsAlignmentReadingAt90 = PRO480.IsAlignmentReadingAt90;
                    objPRO480.AlignmentReadingAt135 = PRO480.AlignmentReadingAt135;
                    objPRO480.IsAlignmentReadingAt135 = PRO480.IsAlignmentReadingAt135;
                    objPRO480.AlignmentReadingAt180 = PRO480.AlignmentReadingAt180;
                    objPRO480.IsAlignmentReadingAt180 = PRO480.IsAlignmentReadingAt180;
                    objPRO480.AlignmentReadingAt225 = PRO480.AlignmentReadingAt225;
                    objPRO480.IsAlignmentReadingAt225 = PRO480.IsAlignmentReadingAt225;
                    objPRO480.AlignmentReadingAt270 = PRO480.AlignmentReadingAt270;
                    objPRO480.IsAlignmentReadingAt270 = PRO480.IsAlignmentReadingAt270;
                    objPRO480.AlignmentReadingAt315 = PRO480.AlignmentReadingAt315;
                    objPRO480.IsAlignmentReadingAt315 = PRO480.IsAlignmentReadingAt315;
                    objPRO480.AlignmentReadingOutBy = PRO480.AlignmentReadingOutBy;
                    objPRO480.IsCircumferenceMeasurement = PRO480.IsCircumferenceMeasurement;
                    objPRO480.IsInsideCircumference = PRO480.IsInsideCircumference;
                    objPRO480.InsideCircumferenceReqValue = PRO480.InsideCircumferenceReqValue;
                    objPRO480.IsOutsideCircumference = PRO480.IsOutsideCircumference;
                    objPRO480.OutsideCircumferenceReqValue = PRO480.OutsideCircumferenceReqValue;
                    objPRO480.IsLengthDimension = PRO480.IsLengthDimension;
                    objPRO480.LengthDimensionBTLtoTTL = PRO480.LengthDimensionBTLtoTTL;
                    objPRO480.LengthDimensionBTLtoTTLReqValue = PRO480.LengthDimensionBTLtoTTLReqValue;
                    objPRO480.LengthDimensionBTLtoBaseRing = PRO480.LengthDimensionBTLtoBaseRing;
                    objPRO480.IsLengthDimensionBTLtoBaseRing = PRO480.IsLengthDimensionBTLtoBaseRing;
                    objPRO480.LengthDimensionBTLtoBaseRingReqValue = PRO480.LengthDimensionBTLtoBaseRingReqValue;
                    objPRO480.IsNozzleDimension = PRO480.IsNozzleDimension;
                    objPRO480.NozzleDimensionElevationTOL = PRO480.NozzleDimensionElevationTOL;
                    objPRO480.NozzleDimensionProjectionTOL = PRO480.NozzleDimensionProjectionTOL;
                    objPRO480.NozzleDimensionOrientationTOL = PRO480.NozzleDimensionOrientationTOL;
                    objPRO480.IsExternalAttachmentDimension = PRO480.IsExternalAttachmentDimension;
                    objPRO480.ExternalAttachmentDimensionElevationTOL = PRO480.ExternalAttachmentDimensionElevationTOL;
                    objPRO480.ExternalAttachmentDimensionProjectionTOL = PRO480.ExternalAttachmentDimensionProjectionTOL;
                    objPRO480.ExternalAttachmentDimensionOrientationTOL = PRO480.ExternalAttachmentDimensionOrientationTOL;
                    objPRO480.ExternalAttachmentDimensionDescription = PRO480.ExternalAttachmentDimensionDescription;
                    objPRO480.ExternalAttachmentDimensionElevationReq = PRO480.ExternalAttachmentDimensionElevationReq;
                    objPRO480.ExternalAttachmentDimensionElevationAct = PRO480.ExternalAttachmentDimensionElevationAct;
                    objPRO480.ExternalAttachmentDimensionProjectionReq = PRO480.ExternalAttachmentDimensionProjectionReq;
                    objPRO480.ExternalAttachmentDimensionProjectionAct = PRO480.ExternalAttachmentDimensionProjectionAct;
                    objPRO480.ExternalAttachmentDimensionOrientationReq = PRO480.ExternalAttachmentDimensionOrientationReq;
                    objPRO480.ExternalAttachmentDimensionOrientationAct = PRO480.ExternalAttachmentDimensionOrientationAct;
                    objPRO480.ExternalAttachmentDimensionOrientationArcReq = PRO480.ExternalAttachmentDimensionOrientationArcReq;
                    objPRO480.ExternalAttachmentDimensionOrientationArcFromReq = PRO480.ExternalAttachmentDimensionOrientationArcFromReq;
                    objPRO480.ExternalAttachmentDimensionOrientationArcAct = PRO480.ExternalAttachmentDimensionOrientationArcAct;
                    objPRO480.ExternalAttachmentDimensionTITLReq = PRO480.ExternalAttachmentDimensionTITLReq;
                    objPRO480.ExternalAttachmentDimensionTITLAct = PRO480.ExternalAttachmentDimensionTITLAct;
                    objPRO480.ExternalAttachmentDimensionSizeReq = PRO480.ExternalAttachmentDimensionSizeReq;
                    objPRO480.ExternalAttachmentDimensionSizeAct = PRO480.ExternalAttachmentDimensionSizeAct;
                    objPRO480.IsInternalAttachmentDimension = PRO480.IsInternalAttachmentDimension;
                    objPRO480.InternalAttachmentDimensionElevationTOL = PRO480.InternalAttachmentDimensionElevationTOL;
                    objPRO480.InternalAttachmentDimensionProjectionTOL = PRO480.InternalAttachmentDimensionProjectionTOL;
                    objPRO480.InternalAttachmentDimensionOrientationTOL = PRO480.InternalAttachmentDimensionOrientationTOL;
                    objPRO480.InternalAttachmentDimensionDescription = PRO480.InternalAttachmentDimensionDescription;
                    objPRO480.InternalAttachmentDimensionElevationReq = PRO480.InternalAttachmentDimensionElevationReq;
                    objPRO480.InternalAttachmentDimensionElevationAct = PRO480.InternalAttachmentDimensionElevationAct;
                    objPRO480.InternalAttachmentDimensionProjectionReq = PRO480.InternalAttachmentDimensionProjectionReq;
                    objPRO480.InternalAttachmentDimensionProjectionAct = PRO480.InternalAttachmentDimensionProjectionAct;
                    objPRO480.InternalAttachmentDimensionOrientationReq = PRO480.InternalAttachmentDimensionOrientationReq;
                    objPRO480.InternalAttachmentDimensionOrientationAct = PRO480.InternalAttachmentDimensionOrientationAct;
                    objPRO480.InternalAttachmentDimensionOrientationArcReq = PRO480.InternalAttachmentDimensionOrientationArcReq;
                    objPRO480.InternalAttachmentDimensionOrientationArcFromReq = PRO480.InternalAttachmentDimensionOrientationArcFromReq;
                    objPRO480.InternalAttachmentDimensionOrientationArcAct = PRO480.InternalAttachmentDimensionOrientationArcAct;
                    objPRO480.InternalAttachmentDimensionTITLReq = PRO480.InternalAttachmentDimensionTITLReq;
                    objPRO480.InternalAttachmentDimensionTITLAct = PRO480.InternalAttachmentDimensionTITLAct;
                    objPRO480.InternalAttachmentDimensionSizeReq = PRO480.InternalAttachmentDimensionSizeReq;
                    objPRO480.InternalAttachmentDimensionSizeAct = PRO480.InternalAttachmentDimensionSizeAct;
                    objPRO480.IsOutOfRoundness = PRO480.IsOutOfRoundness;
                    objPRO480.OutOfRoundnessAt0 = PRO480.OutOfRoundnessAt0;
                    objPRO480.OutOfRoundnessAt22 = PRO480.OutOfRoundnessAt22;
                    objPRO480.OutOfRoundnessAt45 = PRO480.OutOfRoundnessAt45;
                    objPRO480.OutOfRoundnessAt67 = PRO480.OutOfRoundnessAt67;
                    objPRO480.OutOfRoundnessAt90 = PRO480.OutOfRoundnessAt90;
                    objPRO480.OutOfRoundnessAt112 = PRO480.OutOfRoundnessAt112;
                    objPRO480.OutOfRoundnessAt135 = PRO480.OutOfRoundnessAt135;
                    objPRO480.OutOfRoundnessAt157 = PRO480.OutOfRoundnessAt157;
                    objPRO480.IsThermowellBossDimension = PRO480.IsThermowellBossDimension;
                    objPRO480.IsButteringDimension = PRO480.IsButteringDimension;
                    objPRO480.ButteringDimensionElevationTOL = PRO480.ButteringDimensionElevationTOL;
                    objPRO480.ButteringDimensionProjectionTOL = PRO480.ButteringDimensionProjectionTOL;
                    objPRO480.ButteringDimensionOrientationTOL = PRO480.ButteringDimensionOrientationTOL;
                    objPRO480.IsDimensionOfWeldedTraySupportRingTSR = PRO480.IsDimensionOfWeldedTraySupportRingTSR;
                    objPRO480.IsDimensionOfNUBTraySupportRingTSR = PRO480.IsDimensionOfNUBTraySupportRingTSR;
                    objPRO480.IsDimensionOfNUBOnBottomHead = PRO480.IsDimensionOfNUBOnBottomHead;
                    objPRO480.NUBonBottomDimensionElevationFrom = PRO480.NUBonBottomDimensionElevationFrom;
                    objPRO480.NUBonBottomDimensionElevationReq = PRO480.NUBonBottomDimensionElevationReq;
                    objPRO480.NUBonBottomDimensionElevationTOL = PRO480.NUBonBottomDimensionElevationTOL;
                    objPRO480.NUBonBottomDimensionInsideRadiusFrom = PRO480.NUBonBottomDimensionInsideRadiusFrom;
                    objPRO480.NUBonBottomDimensionInsideRadiusReq = PRO480.NUBonBottomDimensionInsideRadiusReq;
                    objPRO480.NUBonBottomDimensionInsideRadiusTOL = PRO480.NUBonBottomDimensionInsideRadiusTOL;
                    objPRO480.LevelnessOfTopFaceTSRTOL = PRO480.LevelnessOfTopFaceTSRTOL;
                    objPRO480.IsDimensionOfBottomCatalyst = PRO480.IsDimensionOfBottomCatalyst;
                    objPRO480.DimensionOfBottomCatalystRadialDistanceReq = PRO480.DimensionOfBottomCatalystRadialDistanceReq;
                    objPRO480.DimensionOfBottomCatalystSizeReq = PRO480.DimensionOfBottomCatalystSizeReq;
                    objPRO480.DimensionOfBottomCatalystSketchPath = PRO480.DimensionOfBottomCatalystSketchPath;
                    objPRO480.IsDimensionOfSupportBrackets = PRO480.IsDimensionOfSupportBrackets;
                    objPRO480.IsDimensionOfSupportBracketsDistance = PRO480.IsDimensionOfSupportBracketsDistance;
                    objPRO480.IsDimensionOfSupportCleats = PRO480.IsDimensionOfSupportCleats;
                    objPRO480.IsDimensionOfSupportCleatsDistance = PRO480.IsDimensionOfSupportCleatsDistance;
                    objPRO480.IsDimensionOfBeamSupportClips = PRO480.IsDimensionOfBeamSupportClips;
                    objPRO480.EditedBy = objClsLoginInfo.UserName; ;
                    objPRO480.EditedOn = DateTime.Now;

                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRO480.HeaderId;
                    #endregion
                    //}
                    //else
                    //{
                    //    objResponseMsg.Key = false;
                    //    objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    //}
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO481> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO481> lstAddPRO481 = new List<PRO481>();
                List<PRO481> lstDeletePRO481 = new List<PRO481>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO481 obj = db.PRO481.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.AlignmentReadingAt0 = item.AlignmentReadingAt0;
                                obj.AlignmentReadingAt45 = item.AlignmentReadingAt45;
                                obj.AlignmentReadingAt90 = item.AlignmentReadingAt90;
                                obj.AlignmentReadingAt135 = item.AlignmentReadingAt135;
                                obj.AlignmentReadingAt180 = item.AlignmentReadingAt180;
                                obj.AlignmentReadingAt225 = item.AlignmentReadingAt225;
                                obj.AlignmentReadingAt270 = item.AlignmentReadingAt270;
                                obj.AlignmentReadingAt315 = item.AlignmentReadingAt315;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO481 obj = new PRO481();
                            obj.HeaderId = item.HeaderId;
                            obj.Location = item.Location;
                            obj.AlignmentReadingAt0 = item.AlignmentReadingAt0;
                            obj.AlignmentReadingAt45 = item.AlignmentReadingAt45;
                            obj.AlignmentReadingAt90 = item.AlignmentReadingAt90;
                            obj.AlignmentReadingAt135 = item.AlignmentReadingAt135;
                            obj.AlignmentReadingAt180 = item.AlignmentReadingAt180;
                            obj.AlignmentReadingAt225 = item.AlignmentReadingAt225;
                            obj.AlignmentReadingAt270 = item.AlignmentReadingAt270;
                            obj.AlignmentReadingAt315 = item.AlignmentReadingAt315;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            lstAddPRO481.Add(obj);
                        }
                    }
                    if (lstAddPRO481.Count > 0)
                    {
                        db.PRO481.AddRange(lstAddPRO481);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO481 = db.PRO481.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO481.Count > 0)
                    {
                        db.PRO481.RemoveRange(lstDeletePRO481);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO481 = db.PRO481.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO481.Count > 0)
                    {
                        db.PRO481.RemoveRange(lstDeletePRO481);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2(List<PRO482> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO482> lstAddPRO482 = new List<PRO482>();
                List<PRO482> lstDeletePRO482 = new List<PRO482>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO482 obj = db.PRO482.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO482();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        obj.Location = item.Location;
                        obj.InsideCircumferance = item.InsideCircumferance;
                        obj.OutsideCircumferance = item.OutsideCircumferance;

                        if (isAdded)
                        {
                            lstAddPRO482.Add(obj);
                        }
                    }
                    if (lstAddPRO482.Count > 0)
                    {
                        db.PRO482.AddRange(lstAddPRO482);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO482 = db.PRO482.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO482.Count > 0)
                    {
                        db.PRO482.RemoveRange(lstDeletePRO482);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO482 = db.PRO482.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO482.Count > 0)
                    {
                        db.PRO482.RemoveRange(lstDeletePRO482);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3(List<PRO483> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO483> lstAddPRO483 = new List<PRO483>();
                List<PRO483> lstDeletePRO483 = new List<PRO483>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO483 obj = db.PRO483.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        else
                        {
                            obj = new PRO483();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }

                        if (!string.IsNullOrWhiteSpace(item.Orientation))
                        {
                            obj.Orientation = item.Orientation;
                            obj.DimensionBTLtoTTL = item.DimensionBTLtoTTL;
                            obj.DimensionBTLtoBaseRing = item.DimensionBTLtoBaseRing;
                        }
                        if (isAdded)
                        {
                            lstAddPRO483.Add(obj);
                        }
                    }
                    if (lstAddPRO483.Count > 0)
                    {
                        db.PRO483.AddRange(lstAddPRO483);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO483 = db.PRO483.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO483.Count > 0)
                    {
                        db.PRO483.RemoveRange(lstDeletePRO483);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO483 = db.PRO483.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO483.Count > 0)
                    {
                        db.PRO483.RemoveRange(lstDeletePRO483);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line5 details
        [HttpPost]
        public JsonResult SaveProtocolLine5(List<PRO484_1> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO484_1> lstAddPRO484_1 = new List<PRO484_1>();
                List<PRO484_1> lstDeletePRO484_1 = new List<PRO484_1>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO484_1 obj = db.PRO484_1.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO484_1();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.NozzleNo = item.NozzleNo;
                        obj.Size = item.Size;
                        obj.ReqElevation = item.ReqElevation;
                        obj.ActElevation = item.ActElevation;
                        obj.ReqProjection = item.ReqProjection;
                        obj.ActProjection = item.ActProjection;
                        obj.ReqOrientation = item.ReqOrientation;
                        obj.ActOrientation = item.ActOrientation;
                        obj.ReqOrientationArc = item.ReqOrientationArc;
                        obj.ActOrientationArc = item.ActOrientationArc;
                        obj.ReqStraddling = item.ReqStraddling;
                        obj.ActStarddling = item.ActStarddling;
                        obj.ReqTilt = item.ReqTilt;
                        obj.ActTilt = item.ActTilt;
                        //obj.Notes = item.Notes;
                        if (isAdded)
                        {
                            lstAddPRO484_1.Add(obj);
                        }
                    }
                    if (lstAddPRO484_1.Count > 0)
                    {
                        db.PRO484_1.AddRange(lstAddPRO484_1);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO484_1 = db.PRO484_1.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO484_1.Count > 0)
                    {
                        db.PRO484_1.RemoveRange(lstDeletePRO484_1);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO484_1 = db.PRO484_1.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO484_1.Count > 0)
                    {
                        db.PRO484_1.RemoveRange(lstDeletePRO484_1);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line10 details
        [HttpPost]
        public JsonResult SaveProtocolLine10(List<PRO484_2> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO484_2> lstAddPRO484_2 = new List<PRO484_2>();
                List<PRO484_2> lstDeletePRO484_2 = new List<PRO484_2>();

                if (lst != null)
                {

                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO484_2 obj = db.PRO484_2.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        PRO484_2 objFecthSeam = new PRO484_2();
                        if (obj == null)
                        {
                            obj = new PRO484_2();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        obj.Location = item.Location;
                        obj.OutOfRoundnessOrientationAt0 = item.OutOfRoundnessOrientationAt0;
                        obj.OutOfRoundnessOrientationAt22 = item.OutOfRoundnessOrientationAt22;
                        obj.OutOfRoundnessOrientationAt45 = item.OutOfRoundnessOrientationAt45;
                        obj.OutOfRoundnessOrientationAt67 = item.OutOfRoundnessOrientationAt67;
                        obj.OutOfRoundnessOrientationAt90 = item.OutOfRoundnessOrientationAt90;
                        obj.OutOfRoundnessOrientationAt112 = item.OutOfRoundnessOrientationAt112;
                        obj.OutOfRoundnessOrientationAt135 = item.OutOfRoundnessOrientationAt135;
                        obj.OutOfRoundnessOrientationAt157 = item.OutOfRoundnessOrientationAt157;

                        if (isAdded)
                        {
                            lstAddPRO484_2.Add(obj);
                        }
                    }
                }
                if (lstAddPRO484_2.Count > 0)
                {
                    db.PRO484_2.AddRange(lstAddPRO484_2);
                }
                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRO484_2 = db.PRO484_2.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else { lstDeletePRO484_2 = db.PRO484_2.Where(x => x.HeaderId == HeaderId).ToList(); }
                if (lstDeletePRO484_2.Count > 0)
                {
                    db.PRO484_2.RemoveRange(lstDeletePRO484_2);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line11 details
        [HttpPost]
        public JsonResult SaveProtocolLine11(List<PRO484_3> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO484_3> lstAddPRO484_3 = new List<PRO484_3>();
                List<PRO484_3> lstDeletePRO484_3 = new List<PRO484_3>();

                if (lst != null)
                {

                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO484_3 obj = db.PRO484_3.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        PRO484_3 objFecthSeam = new PRO484_3();
                        if (obj == null)
                        {
                            obj = new PRO484_3();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        obj.Description = item.Description;
                        obj.ReqElevation = item.ReqElevation;
                        obj.ActElevation = item.ActElevation;
                        obj.ReqProjection = item.ReqProjection;
                        obj.ActProjection = item.ActProjection;
                        obj.ReqOrientation = item.ReqOrientation;
                        obj.ActOrientation = item.ActOrientation;
                        obj.ReqOrientationArc = item.ReqOrientationArc;
                        obj.ReqOrientationArcFrom = item.ReqOrientationArcFrom;
                        obj.ActOrientationArc = item.ActOrientationArc;
                        obj.ReqDaimeterOfHole = item.ReqDaimeterOfHole;
                        obj.ActDaimeterOfHole = item.ActDaimeterOfHole;
                        obj.ReqLigament = item.ReqLigament;
                        obj.ActLigament = item.ActLigament;

                        if (isAdded)
                        {
                            lstAddPRO484_3.Add(obj);
                        }
                    }
                }
                if (lstAddPRO484_3.Count > 0)
                {
                    db.PRO484_3.AddRange(lstAddPRO484_3);
                }
                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRO484_3 = db.PRO484_3.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else { lstDeletePRO484_3 = db.PRO484_3.Where(x => x.HeaderId == HeaderId).ToList(); }
                if (lstDeletePRO484_3.Count > 0)
                {
                    db.PRO484_3.RemoveRange(lstDeletePRO484_3);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion


        #region Line13 details
        [HttpPost]
        public JsonResult SaveProtocolLine13(List<PRO484_4> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO484_4> lstAddPRO484_4 = new List<PRO484_4>();
                List<PRO484_4> lstDeletePRO484_4 = new List<PRO484_4>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO484_4 obj = db.PRO484_4.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO484_4();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        obj.Description = item.Description;
                        obj.ReqElevation = item.ReqElevation;
                        obj.ActElevation = item.ActElevation;
                        obj.ReqProjection = item.ReqProjection;
                        obj.ActProjection = item.ActProjection;
                        obj.ReqOrientation = item.ReqOrientation;
                        obj.ActOrientation = item.ActOrientation;
                        obj.ReqOrientationArc = item.ReqOrientationArc;
                        obj.ReqOrientationArcFrom = item.ReqOrientationArcFrom;
                        obj.ActOrientationArc = item.ActOrientationArc;
                        obj.ReqSizeOfPad = item.ReqSizeOfPad;
                        obj.ActSizeOfPad = item.ActSizeOfPad;
                        if (isAdded)
                        {
                            lstAddPRO484_4.Add(obj);
                        }
                    }
                    if (lstAddPRO484_4.Count > 0)
                    {
                        db.PRO484_4.AddRange(lstAddPRO484_4);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO484_4 = db.PRO484_4.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO484_4.Count > 0)
                    {
                        db.PRO484_4.RemoveRange(lstDeletePRO484_4);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO484_4 = db.PRO484_4.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO484_4.Count > 0)
                    {
                        db.PRO484_4.RemoveRange(lstDeletePRO484_4);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line15 details
        [HttpPost]
        public JsonResult SaveProtocolLine15(List<TSRDetails> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO484_5> lstAddPRO484_5 = new List<PRO484_5>();
                List<PRO484_5> lstDeletePRO484_5 = new List<PRO484_5>();


                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO484_5 obj = db.PRO484_5.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO484_5();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.TSRNo = item.TSRNo;
                        obj.TSRNoDescription = item.TSRNoDescription;
                        obj.ElevationFrom = item.ElevationFrom;
                        obj.ElevationReq = item.ElevationReq;
                        obj.ElevationTOL = item.ElevationTOL;
                        obj.TSRWidthReq = item.TSRWidthReq;
                        obj.TSRWidthTOL = item.TSRWidthTOL;
                        obj.TSRHeightReq = item.TSRHeightReq;
                        obj.TSRHeightTOL = item.TSRHeightTOL;
                        obj.IsTSRID = item.IsTSRID;
                        obj.TSRIDReq = item.TSRIDReq;
                        obj.TSRIDTOL = item.TSRIDTOL;
                        obj.LevelnessOfTopFaceTSRTOL = item.LevelnessOfTopFaceTSRTOL;
                        obj.TiltingOfTSRTOL = item.TiltingOfTSRTOL;
                        obj.FlatnessIn = item.FlatnessIn;
                        obj.ArcDistanceOfTSRTOL = item.ArcDistanceOfTSRTOL;
                        if (isAdded)
                        {
                            db.PRO484_5.Add(obj);
                        }
                        db.SaveChanges();
                        #region Sublines 
                        List<PRO484_5_1> lstAddPRO484_5_1 = new List<PRO484_5_1>();
                        List<PRO484_5_1> lstDeletePRO484_5_1 = new List<PRO484_5_1>();
                        foreach (var lspro in item.LinesData)
                        {
                            if (!string.IsNullOrWhiteSpace(lspro.Orientation))
                            {
                                bool isLineAdded = false;
                                PRO484_5_1 objPRO484_5_1 = db.PRO484_5_1.Where(x => x.SublineId == lspro.SublineId).FirstOrDefault();
                                if (objPRO484_5_1 == null)
                                {
                                    objPRO484_5_1 = new PRO484_5_1();
                                    objPRO484_5_1.HeaderId = item.HeaderId;
                                    objPRO484_5_1.CreatedBy = objClsLoginInfo.UserName;
                                    objPRO484_5_1.CreatedOn = DateTime.Now;
                                    isLineAdded = true;
                                }
                                else
                                {
                                    objPRO484_5_1.EditedBy = objClsLoginInfo.UserName;
                                    objPRO484_5_1.EditedOn = DateTime.Now;
                                }

                                objPRO484_5_1.LineId = obj.LineId;
                                objPRO484_5_1.HeaderId = obj.HeaderId;
                                objPRO484_5_1.Orientation = lspro.Orientation;
                                objPRO484_5_1.ElevationOfTSR = lspro.ElevationOfTSR;
                                objPRO484_5_1.TSRWidth = lspro.TSRWidth;
                                objPRO484_5_1.TSRHeight = lspro.TSRHeight;
                                objPRO484_5_1.TSRID = lspro.TSRID;
                                objPRO484_5_1.TSRTilting = lspro.TSRTilting;
                                objPRO484_5_1.TSRArcDistance = lspro.TSRArcDistance;

                                if (isLineAdded)
                                {
                                    lstAddPRO484_5_1.Add(objPRO484_5_1);
                                }
                            }
                        }
                        if (lstAddPRO484_5_1.Count > 0)
                        {
                            db.PRO484_5_1.AddRange(lstAddPRO484_5_1);
                        }
                        db.SaveChanges();
                        #endregion
                        #region Notes
                        List<PRO484> lstAddPRO484 = new List<PRO484>();
                        List<PRO484> lstDeletePRO484 = new List<PRO484>();

                        foreach (var notes in item.NotesData)
                        {
                            bool isnotesAdded = false;
                            PRO484 objNotes = db.PRO484.Where(x => x.LineId == notes.LineId && x.NotesType == "DIMENSION OF WELDED TRAY SUPPORT RING").FirstOrDefault();
                            if (objNotes == null)
                            {
                                objNotes = new PRO484();
                                objNotes.HeaderId = notes.HeaderId;
                                objNotes.CreatedBy = objClsLoginInfo.UserName;
                                objNotes.CreatedOn = DateTime.Now;
                                isnotesAdded = true;
                            }
                            else
                            {
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                            objNotes.Notes = notes.Notes;
                            objNotes.NotesType = notes.NotesType;
                            objNotes.FilterColumn = item.TSRNo;

                            if (isnotesAdded)
                            {
                                lstAddPRO484.Add(objNotes);
                            }
                        }
                        if (lstAddPRO484.Count > 0)
                        {
                            db.PRO484.AddRange(lstAddPRO484);
                        }

                        var notesLineIds = item.NotesData.Where(x => x.LineId != 0).Select(a => a.LineId);
                        lstDeletePRO484 = db.PRO484.Where(x => !notesLineIds.Contains(x.LineId) && x.HeaderId == HeaderId && x.FilterColumn == obj.TSRNo && x.NotesType == "DIMENSION OF WELDED TRAY SUPPORT RING").ToList();
                        if (lstDeletePRO484.Count > 0)
                        {
                            db.PRO484.RemoveRange(lstDeletePRO484);
                        }
                        db.SaveChanges();
                        #endregion
                    }
                    //if (lstAddPRO484_11.Count > 0)
                    //{
                    //    db.TSRDetails.AddRange(lstAddPRO484_11);
                    //}

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO484_5 = db.PRO484_5.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    List<int> lstLineIds = lstDeletePRO484_5.Select(x => x.LineId).ToList();
                    if (lstDeletePRO484_5.Count > 0)
                    {
                        List<PRO484_5_1> lstDelPRO484_5_1 = db.PRO484_5_1.Where(x => lstLineIds.Contains(Convert.ToInt32(x.LineId))).ToList();

                        if (lstDelPRO484_5_1.Count > 0)
                        {
                            db.PRO484_5_1.RemoveRange(lstDelPRO484_5_1);
                        }
                        db.PRO484_5.RemoveRange(lstDeletePRO484_5);
                    }
                    db.SaveChanges();
                }
                else
                {
                    List<PRO484_5> lstDeletePRO484_11 = db.PRO484_5.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO484_11.Count > 0)
                    {
                        List<PRO484_5_1> lstDelPRO484_5_1 = db.PRO484_5_1.Where(x => x.HeaderId == HeaderId).ToList();
                        db.PRO484_5_1.RemoveRange(lstDelPRO484_5_1);
                        db.PRO484_5.RemoveRange(lstDeletePRO484_5);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line17 details
        [HttpPost]
        public JsonResult SaveProtocolLine17(List<TSRDetailsPRO484_6> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO484_6> lstAddPRO484_6 = new List<PRO484_6>();
                List<PRO484_6> lstDeletePRO484_6 = new List<PRO484_6>();

                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO484_6 obj = db.PRO484_6.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO484_6();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.TSRNo = item.TSRNo;
                        obj.TSRNoDescription = item.TSRNoDescription;
                        obj.ElevationFrom = item.ElevationFrom;
                        obj.ElevationReq = item.ElevationReq;
                        obj.ElevationTOL = item.ElevationTOL;
                        obj.TSRWidthReq = item.TSRWidthReq;
                        obj.TSRWidthTOL = item.TSRWidthTOL;
                        obj.TSRProjectionReq = item.TSRProjectionReq;
                        obj.TSRProjectionTOL = item.TSRProjectionTOL;
                        obj.IsInsideDiameter = item.IsInsideDiameter;
                        obj.InsideDiameterReq = item.InsideDiameterReq;
                        obj.InsideDiameterTOL = item.InsideDiameterTOL;
                        obj.LevelnessOfTopFaceTSRTOL = item.LevelnessOfTopFaceTSRTOL;
                        if (isAdded)
                        {
                            db.PRO484_6.Add(obj);
                        }
                        db.SaveChanges();
                        #region sublines
                        List<PRO484_6_1> lstAddPRO484_6_1 = new List<PRO484_6_1>();
                        List<PRO484_6_1> lstDeletePRO484_6_1 = new List<PRO484_6_1>();
                        foreach (var lspro in item.LinesData)
                        {
                            if (!string.IsNullOrWhiteSpace(lspro.Orientation))
                            {
                                bool isLineAdded = false;
                                PRO484_6_1 objPRO484_6_1 = db.PRO484_6_1.Where(x => x.SublineId == lspro.SublineId).FirstOrDefault();
                                if (objPRO484_6_1 == null)
                                {
                                    objPRO484_6_1 = new PRO484_6_1();
                                    objPRO484_6_1.HeaderId = item.HeaderId;
                                    objPRO484_6_1.CreatedBy = objClsLoginInfo.UserName;
                                    objPRO484_6_1.CreatedOn = DateTime.Now;
                                    objPRO484_6_1.LineId = obj.LineId;

                                    isLineAdded = true;
                                }
                                else
                                {
                                    objPRO484_6_1.EditedBy = objClsLoginInfo.UserName;
                                    objPRO484_6_1.EditedOn = DateTime.Now;
                                }

                                objPRO484_6_1.Orientation = lspro.Orientation;
                                objPRO484_6_1.ElevationFrom = lspro.ElevationFrom;
                                objPRO484_6_1.Width = lspro.Width;
                                objPRO484_6_1.Projection = lspro.Projection;
                                objPRO484_6_1.InsideDiameter = lspro.InsideDiameter;

                                if (isLineAdded)
                                {
                                    lstAddPRO484_6_1.Add(objPRO484_6_1);
                                }
                            }
                        }
                        if (lstAddPRO484_6_1.Count > 0)
                        {
                            db.PRO484_6_1.AddRange(lstAddPRO484_6_1);

                        }
                        #endregion

                        #region Notes
                        List<PRO484> lstAddPRO484 = new List<PRO484>();
                        List<PRO484> lstDeletePRO484 = new List<PRO484>();

                        foreach (var notes in item.NotesData)
                        {
                            bool isnotesAdded = false;
                            PRO484 objNotes = db.PRO484.Where(x => x.LineId == notes.LineId && x.NotesType == "DIMENSION OF NUB/TRAY SUPPORT RING(TSR)").FirstOrDefault();
                            if (objNotes == null)
                            {
                                objNotes = new PRO484();
                                objNotes.HeaderId = notes.HeaderId;
                                objNotes.CreatedBy = objClsLoginInfo.UserName;
                                objNotes.CreatedOn = DateTime.Now;
                                isnotesAdded = true;
                            }
                            else
                            {
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                            objNotes.Notes = notes.Notes;
                            objNotes.NotesType = notes.NotesType;
                            objNotes.FilterColumn = item.TSRNo;

                            if (isnotesAdded)
                            {
                                lstAddPRO484.Add(objNotes);
                            }
                        }
                        if (lstAddPRO484.Count > 0)
                        {
                            db.PRO484.AddRange(lstAddPRO484);
                        }

                        var notesLineIds = item.NotesData.Where(x => x.LineId != 0).Select(a => a.LineId);
                        lstDeletePRO484 = db.PRO484.Where(x => !notesLineIds.Contains(x.LineId) && x.HeaderId == HeaderId && x.FilterColumn == obj.TSRNo && x.NotesType == "DIMENSION OF NUB/TRAY SUPPORT RING(TSR)").ToList();
                        if (lstDeletePRO484.Count > 0)
                        {
                            db.PRO484.RemoveRange(lstDeletePRO484);
                        }
                        db.SaveChanges();
                        #endregion
                    }
                    //if (lstAddPRO484_11.Count > 0)
                    //{
                    //    db.TSRDetails.AddRange(lstAddPRO484_11);
                    //}

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO484_6 = db.PRO484_6.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    List<int> lstLineIds = lstDeletePRO484_6.Select(x => x.LineId).ToList();
                    if (lstDeletePRO484_6.Count > 0)
                    {
                        List<PRO484_6_1> lstDelPRO484_6_1 = db.PRO484_6_1.Where(x => lstLineIds.Contains(Convert.ToInt32(x.LineId))).ToList();

                        if (lstDelPRO484_6_1.Count > 0)
                        {
                            db.PRO484_6_1.RemoveRange(lstDelPRO484_6_1);
                        }
                        db.PRO484_6.RemoveRange(lstDeletePRO484_6);
                    }
                    db.SaveChanges();
                }
                else
                {
                    List<PRO484_6> lstDeletePRO484_11 = db.PRO484_6.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO484_11.Count > 0)
                    {
                        List<PRO484_6_1> lstDelPRO484_6_1 = db.PRO484_6_1.Where(x => x.HeaderId == HeaderId).ToList();
                        db.PRO484_6_1.RemoveRange(lstDelPRO484_6_1);
                        db.PRO484_6.RemoveRange(lstDeletePRO484_6);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }
        #endregion

        #region Line19 details
        [HttpPost]
        public JsonResult SaveProtocolLine19(List<PRO484_7> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO484_7> lstAddPRO484_11 = new List<PRO484_7>();
                List<PRO484_7> lstDeletePRO484_11 = new List<PRO484_7>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO484_7 obj = db.PRO484_7.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO484_7();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        obj.Orientation = item.Orientation;
                        obj.ElevationFrom = item.ElevationFrom;
                        obj.InsideRadiusFrom = item.InsideRadiusFrom;
                        if (isAdded)
                        {
                            lstAddPRO484_11.Add(obj);
                        }
                    }
                    if (lstAddPRO484_11.Count > 0)
                    {
                        db.PRO484_7.AddRange(lstAddPRO484_11);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO484_11 = db.PRO484_7.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO484_11.Count > 0)
                    {
                        db.PRO484_7.RemoveRange(lstDeletePRO484_11);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO484_11 = db.PRO484_7.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO484_11.Count > 0)
                    {
                        db.PRO484_7.RemoveRange(lstDeletePRO484_11);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line21 details
        [HttpPost]
        public JsonResult SaveProtocolLine21(List<PRO484_8> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO484_8> lstAddPRO484_11 = new List<PRO484_8>();
                List<PRO484_8> lstDeletePRO484_11 = new List<PRO484_8>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO484_8 obj = db.PRO484_8.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO484_8();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        obj.StudNo = item.StudNo;
                        obj.RadialDistance = item.RadialDistance;
                        obj.ReqOrientation = item.ReqOrientation;
                        obj.ActOrientation = item.ActOrientation;
                        obj.Size = item.Size;
                        if (isAdded)
                        {
                            lstAddPRO484_11.Add(obj);
                        }
                    }
                    if (lstAddPRO484_11.Count > 0)
                    {
                        db.PRO484_8.AddRange(lstAddPRO484_11);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO484_11 = db.PRO484_8.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO484_11.Count > 0)
                    {
                        db.PRO484_8.RemoveRange(lstDeletePRO484_11);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO484_11 = db.PRO484_8.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO484_11.Count > 0)
                    {
                        db.PRO484_8.RemoveRange(lstDeletePRO484_11);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line23 details
        [HttpPost]
        public JsonResult SaveProtocolLine23(List<PRO484_9> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO484_9> lstAddPRO484_11 = new List<PRO484_9>();
                List<PRO484_9> lstDeletePRO484_11 = new List<PRO484_9>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO484_9 obj = db.PRO484_9.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO484_9();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        obj.BracketsReqElevation = item.BracketsReqElevation;
                        obj.BracketsActElevation = item.BracketsActElevation;
                        obj.BracketsReqOrientation = item.BracketsReqOrientation;
                        obj.BracketsActOrientation = item.BracketsActOrientation;
                        obj.BracketsReqProjection = item.BracketsReqProjection;
                        obj.BracketsActProjection = item.BracketsActProjection;
                        obj.DistanceBetweenTwoBrackets = item.DistanceBetweenTwoBrackets;
                        obj.Remark = item.Remark;
                        if (isAdded)
                        {
                            lstAddPRO484_11.Add(obj);
                        }
                    }
                    if (lstAddPRO484_11.Count > 0)
                    {
                        db.PRO484_9.AddRange(lstAddPRO484_11);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO484_11 = db.PRO484_9.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO484_11.Count > 0)
                    {
                        db.PRO484_9.RemoveRange(lstDeletePRO484_11);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO484_11 = db.PRO484_9.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO484_11.Count > 0)
                    {
                        db.PRO484_9.RemoveRange(lstDeletePRO484_11);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line25 details
        [HttpPost]
        public JsonResult SaveProtocolLine25(List<PRO484_10> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO484_10> lstAddPRO484_11 = new List<PRO484_10>();
                List<PRO484_10> lstDeletePRO484_11 = new List<PRO484_10>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO484_10 obj = db.PRO484_10.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO484_10();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        obj.CleatNo = item.CleatNo;
                        obj.RadialDistance = item.RadialDistance;
                        obj.ReqOrientation = item.ReqOrientation;
                        obj.ActOrientation = item.ActOrientation;
                        obj.ReqProjection = item.ReqProjection;
                        obj.ActProjection = item.ActProjection;
                        if (isAdded)
                        {
                            lstAddPRO484_11.Add(obj);
                        }
                    }
                    if (lstAddPRO484_11.Count > 0)
                    {
                        db.PRO484_10.AddRange(lstAddPRO484_11);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO484_11 = db.PRO484_10.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO484_11.Count > 0)
                    {
                        db.PRO484_10.RemoveRange(lstDeletePRO484_11);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO484_11 = db.PRO484_10.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO484_11.Count > 0)
                    {
                        db.PRO484_10.RemoveRange(lstDeletePRO484_11);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line27 details
        [HttpPost]
        public JsonResult SaveProtocolLine27(List<TSRDetailsPRO484_11> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO484_11> lstAddPRO484_11 = new List<PRO484_11>();
                List<PRO484_11> lstDeletePRO484_11 = new List<PRO484_11>();

                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO484_11 obj = db.PRO484_11.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO484_11();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.TSRNo = item.TSRNo;
                        obj.TSRNoDescription = item.TSRNoDescription;
                        obj.OffsetDistanceFrom = item.OffsetDistanceFrom;
                        obj.ElevationReq = item.ElevationReq;
                        obj.ProjectionReq = item.ProjectionReq;
                        obj.TSRSketchPath = item.TSRSketchPath;
                        if (isAdded)
                        {
                            db.PRO484_11.Add(obj);
                        }
                        db.SaveChanges();
                        List<PRO484_11_1> lstAddPRO484_11_1 = new List<PRO484_11_1>();
                        List<PRO484_11_1> lstDeletePRO484_11_1 = new List<PRO484_11_1>();
                        foreach (var lspro in item.LinesData)
                        {
                            if (!string.IsNullOrWhiteSpace(lspro.BeamNo))
                            {
                                bool isLineAdded = false;
                                PRO484_11_1 objPRO484_11_1 = db.PRO484_11_1.Where(x => x.SublineId == lspro.SublineId).FirstOrDefault();
                                if (objPRO484_11_1 == null)
                                {
                                    objPRO484_11_1 = new PRO484_11_1();
                                    objPRO484_11_1.HeaderId = item.HeaderId;
                                    objPRO484_11_1.CreatedBy = objClsLoginInfo.UserName;
                                    objPRO484_11_1.CreatedOn = DateTime.Now;
                                    objPRO484_11_1.LineId = obj.LineId;

                                    isLineAdded = true;
                                }
                                else
                                {
                                    objPRO484_11_1.EditedBy = objClsLoginInfo.UserName;
                                    objPRO484_11_1.EditedOn = DateTime.Now;
                                }

                                objPRO484_11_1.BeamNo = lspro.BeamNo;
                                objPRO484_11_1.DistanceOrientation = lspro.DistanceOrientation;
                                objPRO484_11_1.ReqDistance = lspro.ReqDistance;
                                objPRO484_11_1.ReqDistanceTowards = lspro.ReqDistanceTowards;
                                objPRO484_11_1.ActDistance = lspro.ActDistance;
                                objPRO484_11_1.ActElevation = lspro.ActElevation;
                                objPRO484_11_1.ActProjection = lspro.ActProjection;

                                if (isLineAdded)
                                {
                                    lstAddPRO484_11_1.Add(objPRO484_11_1);
                                }
                            }

                            #region Notes
                            List<PRO484> lstAddPRO484 = new List<PRO484>();
                            List<PRO484> lstDeletePRO484 = new List<PRO484>();

                            foreach (var notes in item.NotesData)
                            {
                                bool isnotesAdded = false;
                                PRO484 objNotes = db.PRO484.Where(x => x.LineId == notes.LineId && x.NotesType == "DIMENSION OF SUPPORT CLEATS FOR INLET BASKET").FirstOrDefault();
                                if (objNotes == null)
                                {
                                    objNotes = new PRO484();
                                    objNotes.HeaderId = notes.HeaderId;
                                    objNotes.CreatedBy = objClsLoginInfo.UserName;
                                    objNotes.CreatedOn = DateTime.Now;
                                    isnotesAdded = true;
                                }
                                else
                                {
                                    obj.EditedBy = objClsLoginInfo.UserName;
                                    obj.EditedOn = DateTime.Now;
                                }
                                objNotes.Notes = notes.Notes;
                                objNotes.NotesType = notes.NotesType;
                                objNotes.FilterColumn = item.TSRNo;

                                if (isnotesAdded)
                                {
                                    lstAddPRO484.Add(objNotes);
                                }
                            }
                            if (lstAddPRO484.Count > 0)
                            {
                                db.PRO484.AddRange(lstAddPRO484);
                            }

                            var notesLineIds = item.NotesData.Where(x => x.LineId != 0).Select(a => a.LineId);
                            lstDeletePRO484 = db.PRO484.Where(x => !notesLineIds.Contains(x.LineId) && x.HeaderId == HeaderId && x.FilterColumn == obj.TSRNo && x.NotesType == "DIMENSION OF SUPPORT CLEATS FOR INLET BASKET").ToList();
                            if (lstDeletePRO484.Count > 0)
                            {
                                db.PRO484.RemoveRange(lstDeletePRO484);
                            }
                            db.SaveChanges();
                            #endregion
                        }
                        if (lstAddPRO484_11_1.Count > 0)
                        {
                            db.PRO484_11_1.AddRange(lstAddPRO484_11_1);
                            db.SaveChanges();
                        }

                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO484_11 = db.PRO484_11.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    List<int> lstLineIds = lstDeletePRO484_11.Select(x => x.LineId).ToList();
                    if (lstDeletePRO484_11.Count > 0)
                    {
                        List<PRO484_11_1> lstDelPRO484_11_1 = db.PRO484_11_1.Where(x => lstLineIds.Contains(Convert.ToInt32(x.LineId))).ToList();

                        if (lstDelPRO484_11_1.Count > 0)
                        {
                            db.PRO484_11_1.RemoveRange(lstDelPRO484_11_1);
                        }
                        db.PRO484_11.RemoveRange(lstDeletePRO484_11);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO484_11 = db.PRO484_11.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO484_11.Count > 0)
                    {
                        List<PRO484_11_1> lstDelPRO484_11_1 = db.PRO484_11_1.Where(x => x.HeaderId == HeaderId).ToList();
                        db.PRO484_11_1.RemoveRange(lstDelPRO484_11_1);
                        db.PRO484_11.RemoveRange(lstDeletePRO484_11);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }
        #endregion

        #region All Notes Line details
        [HttpPost]
        public JsonResult SaveProtocolLineNotes(List<PRO484> lst, int HeaderId, string NotesType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO484> lstAddPRO484 = new List<PRO484>();
                List<PRO484> lstDeletePRO484 = new List<PRO484>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO484 obj = db.PRO484.Where(x => x.LineId == item.LineId && x.NotesType == NotesType).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO484();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        obj.Notes = item.Notes;
                        obj.NotesType = item.NotesType;
                        obj.FilterColumn = item.FilterColumn;

                        if (isAdded)
                        {
                            lstAddPRO484.Add(obj);
                        }
                    }
                    if (lstAddPRO484.Count > 0)
                    {
                        db.PRO484.AddRange(lstAddPRO484);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO484 = db.PRO484.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId && x.NotesType == NotesType).ToList();
                    if (lstDeletePRO484.Count > 0)
                    {
                        db.PRO484.RemoveRange(lstDeletePRO484);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO484 = db.PRO484.Where(x => x.HeaderId == HeaderId && x.NotesType == NotesType).ToList();
                    if (lstDeletePRO484.Count > 0)
                    {
                        db.PRO484.RemoveRange(lstDeletePRO484);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        ///LINKAGE
          #region Linkage View Code
        //       #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL480 objPRL480 = new PRL480();
            objPRL480 = db.PRL480.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL480 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL480.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;

                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
                //var Files = (new clsFileUpload()).GetDocuments("PRL480/Sketch1/" + id);
                FCS001 Files = db.FCS001.FirstOrDefault(w => w.TableId == id && w.TableName == "PRL480//Sketch1");
                if (Files!= null)
                {
                    ViewBag.image1 = Files.MainDocumentPath;
                }
                //var Files2 = (new clsFileUpload()).GetDocuments("PRL480/Sketch2/" + id);
                FCS001 Files2 = db.FCS001.FirstOrDefault(w => w.TableId == id && w.TableName == "PRL480//Sketch2");
                if (Files2 != null)
                {
                    ViewBag.image2 = Files2.MainDocumentPath;
                }

                List<FCS001> Files3 = db.FCS001.Where(w => w.TableName == "PRL480//Sketch3").ToList();
                if (Files3 != null)
                {
                    ViewBag.image3 = Files3;
                }

                //string[] arrImage3 = new string[objPRL480.PRL484_11.Count()];
                //int p = 1, j = 0;
                //if (objPRL480.PRL484_11.Count() > 0)
                //{
                //    foreach (var item in objPRL480.PRL484_11)
                //    {
                //        var filename = (new clsFileUpload()).GetDocuments("PRL480/Sketch3/" + id + "/" + item.LineId);
                //        if (filename.Count() > 0)
                //        {
                //            arrImage3[j] = filename.FirstOrDefault().URL;
                //        }
                //        else
                //        {
                //            arrImage3[j] = null;
                //        }
                //        j++;
                //    }

                //}
                //ViewBag.Sketch3 = arrImage3;
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstalignment = clsImplementationEnum.getAlignment().ToList();

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.AgignmentEnum = lstalignment.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL481> lstPRL481 = db.PRL481.Where(x => x.HeaderId == objPRL480.HeaderId).ToList();
            List<PRL482> lstPRL482 = db.PRL482.Where(x => x.HeaderId == objPRL480.HeaderId).ToList();
            List<PRL483> lstPRL483 = db.PRL483.Where(x => x.HeaderId == objPRL480.HeaderId).ToList();

            List<PRL484_1> lstPRL484_1 = db.PRL484_1.Where(x => x.HeaderId == objPRL480.HeaderId).ToList();
            List<PRL484_2> lstPRL484_2 = db.PRL484_2.Where(x => x.HeaderId == objPRL480.HeaderId).ToList();
            List<PRL484_3> lstPRL484_3 = db.PRL484_3.Where(x => x.HeaderId == objPRL480.HeaderId).ToList();
            List<PRL484_4> lstPRL484_4 = db.PRL484_4.Where(x => x.HeaderId == objPRL480.HeaderId).ToList();
            List<PRL484_5> lstPRL484_5 = db.PRL484_5.Where(x => x.HeaderId == objPRL480.HeaderId).ToList();
            List<PRL484_5_1> lstPRL484_5_1 = db.PRL484_5_1.Where(x => x.HeaderId == objPRL480.HeaderId).ToList();

            List<PRL484_6> lstPRL484_6 = db.PRL484_6.Where(x => x.HeaderId == objPRL480.HeaderId).ToList();
            List<PRL484_6_1> lstPRL484_6_1 = db.PRL484_6_1.Where(x => x.HeaderId == objPRL480.HeaderId).ToList();

            List<PRL484_7> lstPRL484_7 = db.PRL484_7.Where(x => x.HeaderId == objPRL480.HeaderId).ToList();
            List<PRL484_8> lstPRL484_8 = db.PRL484_8.Where(x => x.HeaderId == objPRL480.HeaderId).ToList();
            List<PRL484_9> lstPRL484_9 = db.PRL484_9.Where(x => x.HeaderId == objPRL480.HeaderId).ToList();
            List<PRL484_10> lstPRL484_10 = db.PRL484_10.Where(x => x.HeaderId == objPRL480.HeaderId).ToList();
            List<PRL484_11> lstPRL484_11 = db.PRL484_11.Where(x => x.HeaderId == objPRL480.HeaderId).ToList();
            List<PRL484_12> lstPRL484_12 = db.PRL484_12.Where(x => x.HeaderId == objPRL480.HeaderId).ToList();
            List<PRL485> lstPRL485External = db.PRL485.Where(x => x.HeaderId == objPRL480.HeaderId && x.ExternalInternalType == "External").ToList();
            List<PRL485> lstPRL485Internal = db.PRL485.Where(x => x.HeaderId == objPRL480.HeaderId && x.ExternalInternalType == "Internal").ToList();


            List<PRL484> lstPRL484Notes1 = db.PRL484.Where(x => x.HeaderId == objPRL480.HeaderId && x.NotesType == "LENGTH DIMENSION").ToList();
            List<PRL484> lstPRL484Notes2 = db.PRL484.Where(x => x.HeaderId == objPRL480.HeaderId && x.NotesType == "DIMENSIONS OF NOZZLES").ToList();
            List<PRL484> lstPRL484Notes3 = db.PRL484.Where(x => x.HeaderId == objPRL480.HeaderId && x.NotesType == "DIMENSIONS OF EXTERNAL ATTACHMENTS").ToList();
            List<PRL484> lstPRL484Notes4 = db.PRL484.Where(x => x.HeaderId == objPRL480.HeaderId && x.NotesType == "DIMENSIONS OF INTERNAL ATTACHMENTS").ToList();
            List<PRL484> lstPRL484Notes5 = db.PRL484.Where(x => x.HeaderId == objPRL480.HeaderId && x.NotesType == "DIMENSIONS OF THERMOWELL BOSS").ToList();
            List<PRL484> lstPRL484Notes6 = db.PRL484.Where(x => x.HeaderId == objPRL480.HeaderId && x.NotesType == "DIMENSIONS OF BUTTERING").ToList();
            List<PRL484> lstPRL484Notes7 = db.PRL484.Where(x => x.HeaderId == objPRL480.HeaderId && x.NotesType == "DIMENSION OF WELDED TRAY SUPPORT RING").ToList();
            List<PRL484> lstPRL484Notes8 = db.PRL484.Where(x => x.HeaderId == objPRL480.HeaderId && x.NotesType == "DIMENSION OF NUB/TRAY SUPPORT RING(TSR)").ToList();
            List<PRL484> lstPRL484Notes9 = db.PRL484.Where(x => x.HeaderId == objPRL480.HeaderId && x.NotesType == "DIMENSION OF NUB ON BOTTOM HEAD").ToList();
            List<PRL484> lstPRL484Notes10 = db.PRL484.Where(x => x.HeaderId == objPRL480.HeaderId && x.NotesType == "BOTTOM CATALYST SUPPORT AFTER INSTALLATION").ToList();
            List<PRL484> lstPRL484Notes11 = db.PRL484.Where(x => x.HeaderId == objPRL480.HeaderId && x.NotesType == "SUPPORT BRACKETS FOR MATERIAL TEST BLOCKS").ToList();
            List<PRL484> lstPRL484Notes12 = db.PRL484.Where(x => x.HeaderId == objPRL480.HeaderId && x.NotesType == "DIMENSION OF SUPPORT CLEATS FOR INLET BASKET").ToList();
            List<PRL484> lstPRL484Notes13 = db.PRL484.Where(x => x.HeaderId == objPRL480.HeaderId && x.NotesType == "DIMENSION OF BEAM SUPPORT CLIPS ON NUB").ToList();



            ViewBag.lstPRL484Notes1 = lstPRL484Notes1;
            ViewBag.lstPRL484Notes2 = lstPRL484Notes2;
            ViewBag.lstPRL484Notes3 = lstPRL484Notes3;
            ViewBag.lstPRL484Notes4 = lstPRL484Notes4;
            ViewBag.lstPRL484Notes5 = lstPRL484Notes5;
            ViewBag.lstPRL484Notes6 = lstPRL484Notes6;
            ViewBag.lstPRL484Notes7 = lstPRL484Notes7;
            ViewBag.lstPRL484Notes8 = lstPRL484Notes8;
            ViewBag.lstPRL484Notes9 = lstPRL484Notes9;
            ViewBag.lstPRL484Notes10 = lstPRL484Notes10;
            ViewBag.lstPRL484Notes11 = lstPRL484Notes11;
            ViewBag.lstPRL484Notes12 = lstPRL484Notes12;
            ViewBag.lstPRL484Notes13 = lstPRL484Notes13;

            ViewBag.lstPRL481 = lstPRL481;
            ViewBag.lstPRL482 = lstPRL482;
            ViewBag.lstPRL483 = lstPRL483;

            ViewBag.lstPRL484_1 = lstPRL484_1;
            ViewBag.lstPRL484_2 = lstPRL484_2;
            ViewBag.lstPRL484_3 = lstPRL484_3;
            ViewBag.lstPRL484_4 = lstPRL484_4;
            ViewBag.lstPRL484_5 = lstPRL484_5;
            ViewBag.lstPRL484_5_1 = lstPRL484_5_1;
            ViewBag.lstPRL484_6 = lstPRL484_6;
            ViewBag.lstPRL484_6_1 = lstPRL484_6_1;
            ViewBag.lstPRL484_7 = lstPRL484_7;
            ViewBag.lstPRL484_8 = lstPRL484_8;
            ViewBag.lstPRL484_9 = lstPRL484_9;
            ViewBag.lstPRL484_10 = lstPRL484_10;
            ViewBag.lstPRL484_11 = lstPRL484_11;
            ViewBag.lstPRL484_12 = lstPRL484_12;
            ViewBag.lstPRL484_6_1 = lstPRL484_6_1;
            ViewBag.lstPRLExterNalAttach = lstPRL485External;
            ViewBag.lstPRLinternalAttach = lstPRL485Internal;

            #endregion

            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                ViewBag.lstDrawingNo = null;
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.DrawingNo = null;
                    ViewBag.lstDrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL480.OfferDate.HasValue && (objPRL480.RequestNo.HasValue) && !string.IsNullOrWhiteSpace(objPRL480.ActFilledBy))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                            List<string> lstdrawing1 = (new GeneralController()).GetPLMDrawingNumber(objPRL480.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing1.AsEnumerable().Select(x => new { Value = x, Text = x }).ToList();
                            ViewBag.lstDrawingNo = lstdrawing1.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL480.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Value = x, Text = x }).ToList();
                            ViewBag.lstDrawingNo = lstdrawing.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
                        }
                    }
                    else
                    {
                        List<string> lstdrawing1 = (new GeneralController()).GetPLMDrawingNumber(objPRL480.Project).ToList();
                        ViewBag.DrawingNo = lstdrawing1.AsEnumerable().Select(x => new { Value = x, Text = x }).ToList();
                        ViewBag.lstDrawingNo = lstdrawing1.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
                    }
                }
                else
                {
                    ViewBag.isEditable = "false";
                    List<string> lstdrawing1 = (new GeneralController()).GetPLMDrawingNumber(objPRL480.Project).ToList();
                    ViewBag.DrawingNo = lstdrawing1.AsEnumerable().Select(x => new { Value = x, Text = x }).ToList();
                    ViewBag.lstDrawingNo = lstdrawing1.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);                
            }

            return View(objPRL480);
        }


        #endregion

        #region Add header
        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL480 PRL480, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;

                int? refHeaderId = PRL480.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL480 objPRL480 = db.PRL480.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    //var ProtocolExist = db.PRL480.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRL480.ProtocolNo).FirstOrDefault();
                    //if (ProtocolExist == null)
                    //{
                    #region Save Data
                    objPRL480.ProtocolNo = PRL480.ProtocolNo;
                    objPRL480.Customer = PRL480.Customer;
                    objPRL480.Owner = PRL480.Owner;
                    objPRL480.PONo = PRL480.PONo;
                    objPRL480.ReportNo = PRL480.ReportNo;
                    objPRL480.IsSurfaceAlignment = PRL480.IsSurfaceAlignment;
                    objPRL480.SurfaceAlignment = PRL480.SurfaceAlignment;
                    objPRL480.StageCode = PRL480.StageCode;

                    objPRL480.SketchType = PRL480.SketchType;
                    objPRL480.IsSketch = PRL480.IsSketch;
                    objPRL480.SketchPath = PRL480.SketchPath;
                    objPRL480.IsAlignmentReading = PRL480.IsAlignmentReading;
                    objPRL480.AlignmentReadingAt0 = PRL480.AlignmentReadingAt0;
                    objPRL480.IsAlignmentReadingAt0 = PRL480.IsAlignmentReadingAt0;
                    objPRL480.AlignmentReadingAt45 = PRL480.AlignmentReadingAt45;
                    objPRL480.IsAlignmentReadingAt45 = PRL480.IsAlignmentReadingAt45;
                    objPRL480.AlignmentReadingAt90 = PRL480.AlignmentReadingAt90;
                    objPRL480.IsAlignmentReadingAt90 = PRL480.IsAlignmentReadingAt90;
                    objPRL480.AlignmentReadingAt135 = PRL480.AlignmentReadingAt135;
                    objPRL480.IsAlignmentReadingAt135 = PRL480.IsAlignmentReadingAt135;
                    objPRL480.AlignmentReadingAt180 = PRL480.AlignmentReadingAt180;
                    objPRL480.IsAlignmentReadingAt180 = PRL480.IsAlignmentReadingAt180;
                    objPRL480.AlignmentReadingAt225 = PRL480.AlignmentReadingAt225;
                    objPRL480.IsAlignmentReadingAt225 = PRL480.IsAlignmentReadingAt225;
                    objPRL480.AlignmentReadingAt270 = PRL480.AlignmentReadingAt270;
                    objPRL480.IsAlignmentReadingAt270 = PRL480.IsAlignmentReadingAt270;
                    objPRL480.AlignmentReadingAt315 = PRL480.AlignmentReadingAt315;
                    objPRL480.IsAlignmentReadingAt315 = PRL480.IsAlignmentReadingAt315;
                    objPRL480.AlignmentReadingOutBy = PRL480.AlignmentReadingOutBy;
                    objPRL480.IsCircumferenceMeasurement = PRL480.IsCircumferenceMeasurement;
                    objPRL480.IsInsideCircumference = PRL480.IsInsideCircumference;
                    objPRL480.InsideCircumferenceReqValue = PRL480.InsideCircumferenceReqValue;
                    objPRL480.IsOutsideCircumference = PRL480.IsOutsideCircumference;
                    objPRL480.OutsideCircumferenceReqValue = PRL480.OutsideCircumferenceReqValue;
                    objPRL480.IsLengthDimension = PRL480.IsLengthDimension;
                    objPRL480.LengthDimensionBTLtoTTL = PRL480.LengthDimensionBTLtoTTL;
                    objPRL480.LengthDimensionBTLtoTTLReqValue = PRL480.LengthDimensionBTLtoTTLReqValue;
                    objPRL480.LengthDimensionBTLtoBaseRing = PRL480.LengthDimensionBTLtoBaseRing;
                    objPRL480.IsLengthDimensionBTLtoBaseRing = PRL480.IsLengthDimensionBTLtoBaseRing;
                    objPRL480.LengthDimensionBTLtoBaseRingReqValue = PRL480.LengthDimensionBTLtoBaseRingReqValue;
                    objPRL480.IsNozzleDimension = PRL480.IsNozzleDimension;
                    objPRL480.NozzleDimensionElevationTOL = PRL480.NozzleDimensionElevationTOL;
                    objPRL480.NozzleDimensionProjectionTOL = PRL480.NozzleDimensionProjectionTOL;
                    objPRL480.NozzleDimensionOrientationTOL = PRL480.NozzleDimensionOrientationTOL;
                    objPRL480.IsExternalAttachmentDimension = PRL480.IsExternalAttachmentDimension;
                    objPRL480.ExternalAttachmentDimensionElevationTOL = PRL480.ExternalAttachmentDimensionElevationTOL;
                    objPRL480.ExternalAttachmentDimensionProjectionTOL = PRL480.ExternalAttachmentDimensionProjectionTOL;
                    objPRL480.ExternalAttachmentDimensionOrientationTOL = PRL480.ExternalAttachmentDimensionOrientationTOL;
                    objPRL480.ExternalAttachmentDimensionDescription = PRL480.ExternalAttachmentDimensionDescription;
                    objPRL480.ExternalAttachmentDimensionElevationReq = PRL480.ExternalAttachmentDimensionElevationReq;
                    objPRL480.ExternalAttachmentDimensionElevationAct = PRL480.ExternalAttachmentDimensionElevationAct;
                    objPRL480.ExternalAttachmentDimensionProjectionReq = PRL480.ExternalAttachmentDimensionProjectionReq;
                    objPRL480.ExternalAttachmentDimensionProjectionAct = PRL480.ExternalAttachmentDimensionProjectionAct;
                    objPRL480.ExternalAttachmentDimensionOrientationReq = PRL480.ExternalAttachmentDimensionOrientationReq;
                    objPRL480.ExternalAttachmentDimensionOrientationAct = PRL480.ExternalAttachmentDimensionOrientationAct;
                    objPRL480.ExternalAttachmentDimensionOrientationArcReq = PRL480.ExternalAttachmentDimensionOrientationArcReq;
                    objPRL480.ExternalAttachmentDimensionOrientationArcFromReq = PRL480.ExternalAttachmentDimensionOrientationArcFromReq;
                    objPRL480.ExternalAttachmentDimensionOrientationArcAct = PRL480.ExternalAttachmentDimensionOrientationArcAct;
                    objPRL480.ExternalAttachmentDimensionTITLReq = PRL480.ExternalAttachmentDimensionTITLReq;
                    objPRL480.ExternalAttachmentDimensionTITLAct = PRL480.ExternalAttachmentDimensionTITLAct;
                    objPRL480.ExternalAttachmentDimensionSizeReq = PRL480.ExternalAttachmentDimensionSizeReq;
                    objPRL480.ExternalAttachmentDimensionSizeAct = PRL480.ExternalAttachmentDimensionSizeAct;
                    objPRL480.IsInternalAttachmentDimension = PRL480.IsInternalAttachmentDimension;
                    objPRL480.InternalAttachmentDimensionElevationTOL = PRL480.InternalAttachmentDimensionElevationTOL;
                    objPRL480.InternalAttachmentDimensionProjectionTOL = PRL480.InternalAttachmentDimensionProjectionTOL;
                    objPRL480.InternalAttachmentDimensionOrientationTOL = PRL480.InternalAttachmentDimensionOrientationTOL;
                    objPRL480.InternalAttachmentDimensionDescription = PRL480.InternalAttachmentDimensionDescription;
                    objPRL480.InternalAttachmentDimensionElevationReq = PRL480.InternalAttachmentDimensionElevationReq;
                    objPRL480.InternalAttachmentDimensionElevationAct = PRL480.InternalAttachmentDimensionElevationAct;
                    objPRL480.InternalAttachmentDimensionProjectionReq = PRL480.InternalAttachmentDimensionProjectionReq;
                    objPRL480.InternalAttachmentDimensionProjectionAct = PRL480.InternalAttachmentDimensionProjectionAct;
                    objPRL480.InternalAttachmentDimensionOrientationReq = PRL480.InternalAttachmentDimensionOrientationReq;
                    objPRL480.InternalAttachmentDimensionOrientationAct = PRL480.InternalAttachmentDimensionOrientationAct;
                    objPRL480.InternalAttachmentDimensionOrientationArcReq = PRL480.InternalAttachmentDimensionOrientationArcReq;
                    objPRL480.InternalAttachmentDimensionOrientationArcFromReq = PRL480.InternalAttachmentDimensionOrientationArcFromReq;
                    objPRL480.InternalAttachmentDimensionOrientationArcAct = PRL480.InternalAttachmentDimensionOrientationArcAct;
                    objPRL480.InternalAttachmentDimensionTITLReq = PRL480.InternalAttachmentDimensionTITLReq;
                    objPRL480.InternalAttachmentDimensionTITLAct = PRL480.InternalAttachmentDimensionTITLAct;
                    objPRL480.InternalAttachmentDimensionSizeReq = PRL480.InternalAttachmentDimensionSizeReq;
                    objPRL480.InternalAttachmentDimensionSizeAct = PRL480.InternalAttachmentDimensionSizeAct;
                    objPRL480.IsOutOfRoundness = PRL480.IsOutOfRoundness;
                    objPRL480.OutOfRoundnessAt0 = PRL480.OutOfRoundnessAt0;
                    objPRL480.OutOfRoundnessAt22 = PRL480.OutOfRoundnessAt22;
                    objPRL480.OutOfRoundnessAt45 = PRL480.OutOfRoundnessAt45;
                    objPRL480.OutOfRoundnessAt67 = PRL480.OutOfRoundnessAt67;
                    objPRL480.OutOfRoundnessAt90 = PRL480.OutOfRoundnessAt90;
                    objPRL480.OutOfRoundnessAt112 = PRL480.OutOfRoundnessAt112;
                    objPRL480.OutOfRoundnessAt135 = PRL480.OutOfRoundnessAt135;
                    objPRL480.OutOfRoundnessAt157 = PRL480.OutOfRoundnessAt157;
                    objPRL480.IsThermowellBossDimension = PRL480.IsThermowellBossDimension;
                    objPRL480.IsButteringDimension = PRL480.IsButteringDimension;
                    objPRL480.ButteringDimensionElevationTOL = PRL480.ButteringDimensionElevationTOL;
                    objPRL480.ButteringDimensionProjectionTOL = PRL480.ButteringDimensionProjectionTOL;
                    objPRL480.ButteringDimensionOrientationTOL = PRL480.ButteringDimensionOrientationTOL;
                    objPRL480.IsDimensionOfWeldedTraySupportRingTSR = PRL480.IsDimensionOfWeldedTraySupportRingTSR;
                    objPRL480.IsDimensionOfNUBTraySupportRingTSR = PRL480.IsDimensionOfNUBTraySupportRingTSR;
                    objPRL480.IsDimensionOfNUBOnBottomHead = PRL480.IsDimensionOfNUBOnBottomHead;
                    objPRL480.NUBonBottomDimensionElevationFrom = PRL480.NUBonBottomDimensionElevationFrom;
                    objPRL480.NUBonBottomDimensionElevationReq = PRL480.NUBonBottomDimensionElevationReq;
                    objPRL480.NUBonBottomDimensionElevationTOL = PRL480.NUBonBottomDimensionElevationTOL;
                    objPRL480.NUBonBottomDimensionInsideRadiusFrom = PRL480.NUBonBottomDimensionInsideRadiusFrom;
                    objPRL480.NUBonBottomDimensionInsideRadiusReq = PRL480.NUBonBottomDimensionInsideRadiusReq;
                    objPRL480.NUBonBottomDimensionInsideRadiusTOL = PRL480.NUBonBottomDimensionInsideRadiusTOL;
                    objPRL480.LevelnessOfTopFaceTSRTOL = PRL480.LevelnessOfTopFaceTSRTOL;
                    objPRL480.IsDimensionOfBottomCatalyst = PRL480.IsDimensionOfBottomCatalyst;
                    objPRL480.DimensionOfBottomCatalystRadialDistanceReq = PRL480.DimensionOfBottomCatalystRadialDistanceReq;
                    objPRL480.DimensionOfBottomCatalystSizeReq = PRL480.DimensionOfBottomCatalystSizeReq;
                    objPRL480.DimensionOfBottomCatalystSketchPath = PRL480.DimensionOfBottomCatalystSketchPath;
                    objPRL480.IsDimensionOfSupportBrackets = PRL480.IsDimensionOfSupportBrackets;
                    objPRL480.IsDimensionOfSupportBracketsDistance = PRL480.IsDimensionOfSupportBracketsDistance;
                    objPRL480.IsDimensionOfSupportCleats = PRL480.IsDimensionOfSupportCleats;
                    objPRL480.IsDimensionOfSupportCleatsDistance = PRL480.IsDimensionOfSupportCleatsDistance;
                    objPRL480.IsDimensionOfBeamSupportClips = PRL480.IsDimensionOfBeamSupportClips;
                    objPRL480.EditedBy = objClsLoginInfo.UserName;
                    objPRL480.EditedOn = DateTime.Now;
                    objPRL480.DimensionOfSupportCleatsRadialDistance = PRL480.DimensionOfSupportCleatsRadialDistance;
                    objPRL480.StudsForBottomCatalystSupportRadialDistance = PRL480.StudsForBottomCatalystSupportRadialDistance;
                    objPRL480.StudsForBottomCatalystSupportSizeReq = PRL480.StudsForBottomCatalystSupportSizeReq;
                    objPRL480.InspectionAgency = PRL480.InspectionAgency;
                    objPRL480.Title1 = PRL480.Title1;
                    objPRL480.Title2 = PRL480.Title2;
                    objPRL480.Title3 = PRL480.Title3;
                    objPRL480.Title4 = PRL480.Title4;
                    objPRL480.Title5 = PRL480.Title5;
                    objPRL480.Title6 = PRL480.Title6;
                    objPRL480.Title7 = PRL480.Title7;
                    objPRL480.Title8 = PRL480.Title8;
                    objPRL480.Title9 = PRL480.Title9;
                    objPRL480.Title10 = PRL480.Title10;
                    objPRL480.Title11 = PRL480.Title11;
                    objPRL480.Title12 = PRL480.Title12;
                    objPRL480.Title13 = PRL480.Title13;
                    objPRL480.Title14 = PRL480.Title14;
                    objPRL480.Title15 = PRL480.Title15;
                    objPRL480.Title16 = PRL480.Title16;


                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL480.ActFilledBy = UserName;
                            objPRL480.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL480.ReqFilledBy = UserName;
                            objPRL480.ReqFilledOn = DateTime.Now;
                        }
                    }
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL480.HeaderId;
                    #endregion
                    //}
                    //else
                    //{
                    //    objResponseMsg.Key = false;
                    //    objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    //}
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL481> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRL481> lstAddPRL481 = new List<PRL481>();
                List<PRL481> lstDeletePRL481 = new List<PRL481>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRL481 obj = db.PRL481.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.Location = item.Location;
                                obj.AlignmentReadingAt0 = item.AlignmentReadingAt0;
                                obj.AlignmentReadingAt45 = item.AlignmentReadingAt45;
                                obj.AlignmentReadingAt90 = item.AlignmentReadingAt90;
                                obj.AlignmentReadingAt135 = item.AlignmentReadingAt135;
                                obj.AlignmentReadingAt180 = item.AlignmentReadingAt180;
                                obj.AlignmentReadingAt225 = item.AlignmentReadingAt225;
                                obj.AlignmentReadingAt270 = item.AlignmentReadingAt270;
                                obj.AlignmentReadingAt315 = item.AlignmentReadingAt315;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRL481 obj = new PRL481();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.Location))
                            {
                                obj.Location = item.Location;
                                obj.AlignmentReadingAt0 = item.AlignmentReadingAt0;
                                obj.AlignmentReadingAt45 = item.AlignmentReadingAt45;
                                obj.AlignmentReadingAt90 = item.AlignmentReadingAt90;
                                obj.AlignmentReadingAt135 = item.AlignmentReadingAt135;
                                obj.AlignmentReadingAt180 = item.AlignmentReadingAt180;
                                obj.AlignmentReadingAt225 = item.AlignmentReadingAt225;
                                obj.AlignmentReadingAt270 = item.AlignmentReadingAt270;
                                obj.AlignmentReadingAt315 = item.AlignmentReadingAt315;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRL481.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRL481.Count > 0)
                    {
                        db.PRL481.AddRange(lstAddPRL481);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRL481 = db.PRL481.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRL481.Count > 0)
                    {
                        db.PRL481.RemoveRange(lstDeletePRL481);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL481 = db.PRL481.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL481.Count > 0)
                    {
                        db.PRL481.RemoveRange(lstDeletePRL481);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2Linkage(List<PRL482> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRL482> lstAddPRL482 = new List<PRL482>();
                List<PRL482> lstDeletePRL482 = new List<PRL482>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        if (!string.IsNullOrWhiteSpace(item.Location))
                        {
                            PRL482 obj = db.PRL482.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj == null)
                            {
                                obj = new PRL482();
                                obj.HeaderId = item.HeaderId;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                isAdded = true;
                            }
                            else
                            {
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                            obj.Location = item.Location;
                            obj.InsideCircumferance = item.InsideCircumferance;
                            obj.OutsideCircumferance = item.OutsideCircumferance;

                            if (isAdded)
                            {
                                lstAddPRL482.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRL482.Count > 0)
                    {
                        db.PRL482.AddRange(lstAddPRL482);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL482 = db.PRL482.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL482.Count > 0)
                    {
                        db.PRL482.RemoveRange(lstDeletePRL482);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL482 = db.PRL482.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL482.Count > 0)
                    {
                        db.PRL482.RemoveRange(lstDeletePRL482);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3Linkage(List<PRL483> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRL483> lstAddPRL483 = new List<PRL483>();
                List<PRL483> lstDeletePRL483 = new List<PRL483>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL483 obj = db.PRL483.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        else
                        {
                            obj = new PRL483();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            if (!string.IsNullOrWhiteSpace(item.Orientation))
                            {
                                isAdded = true;
                            }
                        }

                        obj.Orientation = item.Orientation;
                        obj.DimensionBTLtoTTL = item.DimensionBTLtoTTL;
                        obj.DimensionBTLtoBaseRing = item.DimensionBTLtoBaseRing;

                        if (isAdded)
                        {
                            lstAddPRL483.Add(obj);
                        }
                    }
                    if (lstAddPRL483.Count > 0)
                    {
                        db.PRL483.AddRange(lstAddPRL483);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL483 = db.PRL483.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL483.Count > 0)
                    {
                        db.PRL483.RemoveRange(lstDeletePRL483);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL483 = db.PRL483.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL483.Count > 0)
                    {
                        db.PRL483.RemoveRange(lstDeletePRL483);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line5 details
        [HttpPost]
        public JsonResult SaveProtocolLine5Linkage(List<PRL484_1> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRL484_1> lstAddPRL484_1 = new List<PRL484_1>();
                List<PRL484_1> lstDeletePRL484_1 = new List<PRL484_1>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL484_1 obj = db.PRL484_1.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL484_1();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            if (!string.IsNullOrWhiteSpace(item.NozzleNo))
                            {
                                isAdded = true;
                            }
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.NozzleNo = item.NozzleNo;
                        obj.Size = item.Size;
                        obj.ReqElevation = item.ReqElevation;
                        obj.ActElevation = item.ActElevation;
                        obj.ReqProjection = item.ReqProjection;
                        obj.ActProjection = item.ActProjection;
                        obj.ReqOrientation = item.ReqOrientation;
                        obj.ActOrientation = item.ActOrientation;
                        obj.ReqOrientationArc = item.ReqOrientationArc;
                        obj.ActOrientationArc = item.ActOrientationArc;
                        obj.ReqStraddling = item.ReqStraddling;
                        obj.ActStarddling = item.ActStarddling;
                        obj.ReqTilt = item.ReqTilt;
                        obj.ActTilt = item.ActTilt;
                        //obj.Notes = item.Notes;
                        if (isAdded)
                        {
                            lstAddPRL484_1.Add(obj);
                        }
                    }
                    if (lstAddPRL484_1.Count > 0)
                    {
                        db.PRL484_1.AddRange(lstAddPRL484_1);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL484_1 = db.PRL484_1.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL484_1.Count > 0)
                    {
                        db.PRL484_1.RemoveRange(lstDeletePRL484_1);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL484_1 = db.PRL484_1.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL484_1.Count > 0)
                    {
                        db.PRL484_1.RemoveRange(lstDeletePRL484_1);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line10 details
        [HttpPost]
        public JsonResult SaveProtocolLine10Linkage(List<PRL484_2> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRL484_2> lstAddPRL484_2 = new List<PRL484_2>();
                List<PRL484_2> lstDeletePRL484_2 = new List<PRL484_2>();

                if (lst != null)
                {

                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL484_2 obj = db.PRL484_2.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        PRL484_2 objFecthSeam = new PRL484_2();
                        if (obj == null)
                        {
                            obj = new PRL484_2();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            if (!string.IsNullOrWhiteSpace(item.Location))
                            {
                                isAdded = true;
                            }
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        obj.Location = item.Location;
                        obj.OutOfRoundnessOrientationAt0 = item.OutOfRoundnessOrientationAt0;
                        obj.OutOfRoundnessOrientationAt22 = item.OutOfRoundnessOrientationAt22;
                        obj.OutOfRoundnessOrientationAt45 = item.OutOfRoundnessOrientationAt45;
                        obj.OutOfRoundnessOrientationAt67 = item.OutOfRoundnessOrientationAt67;
                        obj.OutOfRoundnessOrientationAt90 = item.OutOfRoundnessOrientationAt90;
                        obj.OutOfRoundnessOrientationAt112 = item.OutOfRoundnessOrientationAt112;
                        obj.OutOfRoundnessOrientationAt135 = item.OutOfRoundnessOrientationAt135;
                        obj.OutOfRoundnessOrientationAt157 = item.OutOfRoundnessOrientationAt157;

                        if (isAdded)
                        {
                            lstAddPRL484_2.Add(obj);
                        }
                    }
                }
                if (lstAddPRL484_2.Count > 0)
                {
                    db.PRL484_2.AddRange(lstAddPRL484_2);
                }
                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL484_2 = db.PRL484_2.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else { lstDeletePRL484_2 = db.PRL484_2.Where(x => x.HeaderId == HeaderId).ToList(); }
                if (lstDeletePRL484_2.Count > 0)
                {
                    db.PRL484_2.RemoveRange(lstDeletePRL484_2);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line11 details
        [HttpPost]
        public JsonResult SaveProtocolLine11Linkage(List<PRL484_3> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRL484_3> lstAddPRL484_3 = new List<PRL484_3>();
                List<PRL484_3> lstDeletePRL484_3 = new List<PRL484_3>();

                if (lst != null)
                {

                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL484_3 obj = db.PRL484_3.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        PRL484_3 objFecthSeam = new PRL484_3();
                        if (obj == null)
                        {
                            obj = new PRL484_3();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            if (!string.IsNullOrWhiteSpace(item.Description))
                                isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        obj.Description = item.Description;
                        obj.ReqElevation = item.ReqElevation;
                        obj.ActElevation = item.ActElevation;
                        obj.ReqProjection = item.ReqProjection;
                        obj.ActProjection = item.ActProjection;
                        obj.ReqOrientation = item.ReqOrientation;
                        obj.ActOrientation = item.ActOrientation;
                        obj.ReqOrientationArc = item.ReqOrientationArc;
                        obj.ReqOrientationArcFrom = item.ReqOrientationArcFrom;
                        obj.ActOrientationArc = item.ActOrientationArc;
                        obj.ReqDaimeterOfHole = item.ReqDaimeterOfHole;
                        obj.ActDaimeterOfHole = item.ActDaimeterOfHole;
                        obj.ReqLigament = item.ReqLigament;
                        obj.ActLigament = item.ActLigament;

                        if (isAdded)
                        {
                            lstAddPRL484_3.Add(obj);
                        }
                    }
                }
                if (lstAddPRL484_3.Count > 0)
                {
                    db.PRL484_3.AddRange(lstAddPRL484_3);
                }
                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL484_3 = db.PRL484_3.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else { lstDeletePRL484_3 = db.PRL484_3.Where(x => x.HeaderId == HeaderId).ToList(); }
                if (lstDeletePRL484_3.Count > 0)
                {
                    db.PRL484_3.RemoveRange(lstDeletePRL484_3);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion


        #region Line13 details
        [HttpPost]
        public JsonResult SaveProtocolLine13Linkage(List<PRL484_4> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRL484_4> lstAddPRL484_4 = new List<PRL484_4>();
                List<PRL484_4> lstDeletePRL484_4 = new List<PRL484_4>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL484_4 obj = db.PRL484_4.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL484_4();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            if (!string.IsNullOrWhiteSpace(item.Description))
                            {
                                isAdded = true;
                            }
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        obj.Description = item.Description;
                        obj.ReqElevation = item.ReqElevation;
                        obj.ActElevation = item.ActElevation;
                        obj.ReqProjection = item.ReqProjection;
                        obj.ActProjection = item.ActProjection;
                        obj.ReqOrientation = item.ReqOrientation;
                        obj.ActOrientation = item.ActOrientation;
                        obj.ReqOrientationArc = item.ReqOrientationArc;
                        obj.ReqOrientationArcFrom = item.ReqOrientationArcFrom;
                        obj.ActOrientationArc = item.ActOrientationArc;
                        obj.ReqSizeOfPad = item.ReqSizeOfPad;
                        obj.ActSizeOfPad = item.ActSizeOfPad;
                        if (isAdded)
                        {
                            lstAddPRL484_4.Add(obj);
                        }
                    }
                    if (lstAddPRL484_4.Count > 0)
                    {
                        db.PRL484_4.AddRange(lstAddPRL484_4);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL484_4 = db.PRL484_4.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL484_4.Count > 0)
                    {
                        db.PRL484_4.RemoveRange(lstDeletePRL484_4);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL484_4 = db.PRL484_4.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL484_4.Count > 0)
                    {
                        db.PRL484_4.RemoveRange(lstDeletePRL484_4);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line15 details
        [HttpPost]
        public JsonResult SaveProtocolLine15Linkage(List<TSRDetailsPRL484_5> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRL484_5> lstAddPRL484_5 = new List<PRL484_5>();
                List<PRL484_5> lstDeletePRL484_5 = new List<PRL484_5>();


                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL484_5 obj = db.PRL484_5.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (!db.PRL484_5.Any(x => x.LineId != item.LineId && x.TSRNo == item.TSRNo && x.HeaderId == item.HeaderId))
                        {
                            if (obj == null)
                            {
                                obj = new PRL484_5();
                                obj.HeaderId = item.HeaderId;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                if (!string.IsNullOrWhiteSpace(item.TSRNo))
                                    isAdded = true;
                            }
                            else
                            {
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                            if (string.IsNullOrWhiteSpace(item.TSRNo))
                            {

                                obj.TSRNo = "TSR No";
                            }
                            else
                            {
                                obj.TSRNo = item.TSRNo;
                            }

                            obj.TSRNoDescription = item.TSRNoDescription;
                            obj.ElevationFrom = item.ElevationFrom;
                            obj.ElevationReq = item.ElevationReq;
                            obj.ElevationTOL = item.ElevationTOL;
                            obj.TSRWidthReq = item.TSRWidthReq;
                            obj.TSRWidthTOL = item.TSRWidthTOL;
                            obj.TSRHeightReq = item.TSRHeightReq;
                            obj.TSRHeightTOL = item.TSRHeightTOL;
                            if (item.IsTSRID == null)
                            {
                                obj.IsTSRID = "Yes";
                            }
                            else
                            {
                                obj.IsTSRID = item.IsTSRID;
                            }
                            obj.TSRIDReq = item.TSRIDReq;
                            obj.TSRIDTOL = item.TSRIDTOL;
                            obj.LevelnessOfTopFaceTSRTOL = item.LevelnessOfTopFaceTSRTOL;
                            obj.TiltingOfTSRTOL = item.TiltingOfTSRTOL;
                            obj.FlatnessIn = item.FlatnessIn;
                            obj.ArcDistanceOfTSRTOL = item.ArcDistanceOfTSRTOL;
                            if (isAdded)
                            {
                                db.PRL484_5.Add(obj);
                            }
                            db.SaveChanges();
                            #region Sublines 
                            List<PRL484_5_1> lstAddPRL484_5_1 = new List<PRL484_5_1>();
                            List<PRL484_5_1> lstDeletePRL484_5_1 = new List<PRL484_5_1>();
                            foreach (var lspro in item.LinesData)
                            {
                                if (!string.IsNullOrWhiteSpace(lspro.Orientation))
                                {
                                    bool isLineAdded = false;
                                    PRL484_5_1 objPRL484_5_1 = db.PRL484_5_1.Where(x => x.SublineId == lspro.SublineId).FirstOrDefault();
                                    if (objPRL484_5_1 == null)
                                    {
                                        objPRL484_5_1 = new PRL484_5_1();
                                        objPRL484_5_1.HeaderId = item.HeaderId;
                                        objPRL484_5_1.CreatedBy = objClsLoginInfo.UserName;
                                        objPRL484_5_1.CreatedOn = DateTime.Now;
                                        if (!string.IsNullOrWhiteSpace(lspro.Orientation))
                                            isLineAdded = true;
                                    }
                                    else
                                    {
                                        objPRL484_5_1.EditedBy = objClsLoginInfo.UserName;
                                        objPRL484_5_1.EditedOn = DateTime.Now;
                                    }

                                    objPRL484_5_1.LineId = obj.LineId;
                                    objPRL484_5_1.HeaderId = obj.HeaderId;
                                    objPRL484_5_1.Orientation = lspro.Orientation;
                                    objPRL484_5_1.ElevationOfTSR = lspro.ElevationOfTSR;
                                    objPRL484_5_1.TSRWidth = lspro.TSRWidth;
                                    objPRL484_5_1.TSRHeight = lspro.TSRHeight;
                                    objPRL484_5_1.TSRID = lspro.TSRID;
                                    objPRL484_5_1.TSRTilting = lspro.TSRTilting;
                                    objPRL484_5_1.TSRArcDistance = lspro.TSRArcDistance;

                                    if (isLineAdded)
                                    {
                                        lstAddPRL484_5_1.Add(objPRL484_5_1);
                                    }
                                }
                            }
                            if (lstAddPRL484_5_1.Count > 0)
                            {
                                db.PRL484_5_1.AddRange(lstAddPRL484_5_1);
                            }
                            db.SaveChanges();
                            #endregion
                            #region Notes
                            List<PRL484> lstAddPRL484 = new List<PRL484>();
                            List<PRL484> lstDeletePRL484 = new List<PRL484>();
                            if (!string.IsNullOrWhiteSpace(item.TSRNo))
                            {
                                foreach (var notes in item.NotesData)
                                {
                                    bool isnotesAdded = false;
                                    PRL484 objNotes = db.PRL484.Where(x => x.LineId == notes.LineId && x.NotesType == "DIMENSION OF WELDED TRAY SUPPORT RING").FirstOrDefault();
                                    if (objNotes == null)
                                    {
                                        objNotes = new PRL484();
                                        objNotes.HeaderId = notes.HeaderId;
                                        objNotes.CreatedBy = objClsLoginInfo.UserName;
                                        objNotes.CreatedOn = DateTime.Now;
                                        isnotesAdded = true;
                                    }
                                    else
                                    {
                                        obj.EditedBy = objClsLoginInfo.UserName;
                                        obj.EditedOn = DateTime.Now;
                                    }
                                    objNotes.Notes = notes.Notes;
                                    objNotes.NotesType = notes.NotesType;
                                    objNotes.FilterColumn = item.TSRNo;

                                    if (isnotesAdded)
                                    {
                                        lstAddPRL484.Add(objNotes);
                                    }
                                }
                                if (lstAddPRL484.Count > 0)
                                {
                                    db.PRL484.AddRange(lstAddPRL484);
                                }

                                var notesLineIds = item.NotesData.Where(x => x.LineId != 0).Select(a => a.LineId);
                                lstDeletePRL484 = db.PRL484.Where(x => !notesLineIds.Contains(x.LineId) && x.HeaderId == HeaderId && x.FilterColumn == obj.TSRNo && x.NotesType == "DIMENSION OF WELDED TRAY SUPPORT RING").ToList();
                                if (lstDeletePRL484.Count > 0)
                                {
                                    db.PRL484.RemoveRange(lstDeletePRL484);
                                }
                            }
                            db.SaveChanges();
                            #endregion
                        }
                    }
                    //if (lstAddPRL484_11.Count > 0)
                    //{
                    //    db.TSRDetails.AddRange(lstAddPRL484_11);
                    //}

                    //var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    //lstDeletePRL484_5 = db.PRL484_5.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    //List<int> lstLineIds = lstDeletePRL484_5.Select(x => x.LineId).ToList();
                    //if (lstDeletePRL484_5.Count > 0)
                    //{
                    //    List<PRL484_5_1> lstDelPRL484_5_1 = db.PRL484_5_1.Where(x => lstLineIds.Contains(x.LineId)).ToList();

                    //    if (lstDelPRL484_5_1.Count > 0)
                    //    {
                    //        db.PRL484_5_1.RemoveRange(lstDelPRL484_5_1);
                    //    }
                    //    db.PRL484_5.RemoveRange(lstDeletePRL484_5);
                    //}

                    db.SaveChanges();
                }
                else
                {
                    List<PRL484_5> lstDeletePRL484_11 = db.PRL484_5.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL484_11.Count > 0)
                    {
                        List<PRL484_5_1> lstDelPRL484_5_1 = db.PRL484_5_1.Where(x => x.HeaderId == HeaderId).ToList();
                        db.PRL484_5_1.RemoveRange(lstDelPRL484_5_1);
                        db.PRL484_5.RemoveRange(lstDeletePRL484_5);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line17 details
        [HttpPost]
        public JsonResult SaveProtocolLine17Linkage(List<TSRDetailsPRL484_6> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRL484_6> lstAddPRL484_6 = new List<PRL484_6>();
                List<PRL484_6> lstDeletePRL484_6 = new List<PRL484_6>();

                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL484_6 obj = db.PRL484_6.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL484_6();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            if (!string.IsNullOrWhiteSpace(item.TSRNo))
                                isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.TSRNo = item.TSRNo;
                        obj.TSRNoDescription = item.TSRNoDescription;
                        obj.ElevationFrom = item.ElevationFrom;
                        obj.ElevationReq = item.ElevationReq;
                        obj.ElevationTOL = item.ElevationTOL;
                        obj.TSRWidthReq = item.TSRWidthReq;
                        obj.TSRWidthTOL = item.TSRWidthTOL;
                        obj.TSRProjectionReq = item.TSRProjectionReq;
                        obj.TSRProjectionTOL = item.TSRProjectionTOL;
                        obj.IsInsideDiameter = item.IsInsideDiameter;
                        obj.InsideDiameterReq = item.InsideDiameterReq;
                        obj.InsideDiameterTOL = item.InsideDiameterTOL;
                        obj.LevelnessOfTopFaceTSRTOL = item.LevelnessOfTopFaceTSRTOL;
                        if (isAdded)
                        {
                            db.PRL484_6.Add(obj);
                        }
                        db.SaveChanges();
                        #region sublines
                        List<PRL484_6_1> lstAddPRL484_6_1 = new List<PRL484_6_1>();
                        List<PRL484_6_1> lstDeletePRL484_6_1 = new List<PRL484_6_1>();
                        foreach (var lspro in item.LinesData)
                        {
                            if (!string.IsNullOrWhiteSpace(lspro.Orientation))
                            {
                                bool isLineAdded = false;
                                PRL484_6_1 objPRL484_6_1 = db.PRL484_6_1.Where(x => x.SublineId == lspro.SublineId).FirstOrDefault();
                                if (objPRL484_6_1 == null)
                                {
                                    objPRL484_6_1 = new PRL484_6_1();
                                    objPRL484_6_1.HeaderId = item.HeaderId;
                                    objPRL484_6_1.CreatedBy = objClsLoginInfo.UserName;
                                    objPRL484_6_1.CreatedOn = DateTime.Now;
                                    objPRL484_6_1.LineId = obj.LineId;

                                    isLineAdded = true;
                                }
                                else
                                {
                                    objPRL484_6_1.EditedBy = objClsLoginInfo.UserName;
                                    objPRL484_6_1.EditedOn = DateTime.Now;
                                }

                                objPRL484_6_1.Orientation = lspro.Orientation;
                                objPRL484_6_1.ElevationFrom = lspro.ElevationFrom;
                                objPRL484_6_1.Width = lspro.Width;
                                objPRL484_6_1.Projection = lspro.Projection;
                                objPRL484_6_1.InsideDiameter = lspro.InsideDiameter;

                                if (isLineAdded)
                                {
                                    lstAddPRL484_6_1.Add(objPRL484_6_1);
                                }
                            }
                        }
                        if (lstAddPRL484_6_1.Count > 0)
                        {
                            db.PRL484_6_1.AddRange(lstAddPRL484_6_1);

                        }
                        db.SaveChanges();
                        #endregion

                        #region Notes
                        List<PRL484> lstAddPRL484 = new List<PRL484>();
                        List<PRL484> lstDeletePRL484 = new List<PRL484>();
                        if (item.NotesData != null)
                        {
                            foreach (var notes in item.NotesData)
                            {
                                bool isnotesAdded = false;
                                PRL484 objNotes = db.PRL484.Where(x => x.LineId == notes.LineId && x.NotesType == "DIMENSION OF NUB/TRAY SUPPORT RING(TSR)").FirstOrDefault();
                                if (objNotes == null)
                                {
                                    objNotes = new PRL484();
                                    objNotes.HeaderId = notes.HeaderId;
                                    objNotes.CreatedBy = objClsLoginInfo.UserName;
                                    objNotes.CreatedOn = DateTime.Now;
                                    isnotesAdded = true;
                                }
                                else
                                {
                                    obj.EditedBy = objClsLoginInfo.UserName;
                                    obj.EditedOn = DateTime.Now;
                                }
                                objNotes.Notes = notes.Notes;
                                objNotes.NotesType = notes.NotesType;
                                objNotes.FilterColumn = item.TSRNo;

                                if (isnotesAdded)
                                {
                                    lstAddPRL484.Add(objNotes);
                                }
                            }

                        }

                        if (lstAddPRL484.Count > 0)
                        {
                            db.PRL484.AddRange(lstAddPRL484);
                        }

                        var notesLineIds = item.NotesData.Where(x => x.LineId != 0).Select(a => a.LineId);
                        lstDeletePRL484 = db.PRL484.Where(x => !notesLineIds.Contains(x.LineId) && x.HeaderId == HeaderId && x.FilterColumn == obj.TSRNo && x.NotesType == "DIMENSION OF NUB/TRAY SUPPORT RING(TSR)").ToList();
                        if (lstDeletePRL484.Count > 0)
                        {
                            db.PRL484.RemoveRange(lstDeletePRL484);
                        }
                        db.SaveChanges();
                        #endregion
                    }
                    //if (lstAddPRL484_11.Count > 0)
                    //{
                    //    db.TSRDetails.AddRange(lstAddPRL484_11);
                    //}

                    //var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId).ToList();

                    //if (LineIds.Count > 0)
                    //{
                    //    foreach(var lineIds  in LineIds)
                    //    {
                    //        lstDeletePRL484_6 = db.PRL484_6.Where(x => !lineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    //        List<int> lstLineIds = lstDeletePRL484_6.Select(x => x.LineId).ToList();
                    //        if (lstDeletePRL484_6.Count > 0)
                    //        {
                    //            List<PRL484_6_1> lstDelPRL484_6_1 = db.PRL484_6_1.Where(x => lstLineIds.Contains(x.LineId)).ToList();

                    //            if (lstDelPRL484_6_1.Count > 0)
                    //            {
                    //                db.PRL484_6_1.RemoveRange(lstDelPRL484_6_1);
                    //            }
                    //            db.PRL484_6.RemoveRange(lstDeletePRL484_6);
                    //        }
                    //    }

                    //}
                    db.SaveChanges();
                }
                else
                {
                    List<PRL484_6> lstDeletePRL484_11 = db.PRL484_6.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL484_11.Count > 0)
                    {
                        List<PRL484_6_1> lstDelPRL484_6_1 = db.PRL484_6_1.Where(x => x.HeaderId == HeaderId).ToList();
                        db.PRL484_6_1.RemoveRange(lstDelPRL484_6_1);
                        db.PRL484_6.RemoveRange(lstDeletePRL484_6);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }
        #endregion

        #region Line19 details
        [HttpPost]
        public JsonResult SaveProtocolLine19Linkage(List<PRL484_7> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRL484_7> lstAddPRL484_11 = new List<PRL484_7>();
                List<PRL484_7> lstDeletePRL484_11 = new List<PRL484_7>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL484_7 obj = db.PRL484_7.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL484_7();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            if (!string.IsNullOrWhiteSpace(item.Orientation))
                                isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        obj.Orientation = item.Orientation;
                        obj.ElevationFrom = item.ElevationFrom;
                        obj.InsideRadiusFrom = item.InsideRadiusFrom;
                        if (isAdded)
                        {
                            lstAddPRL484_11.Add(obj);
                        }
                    }
                    if (lstAddPRL484_11.Count > 0)
                    {
                        db.PRL484_7.AddRange(lstAddPRL484_11);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL484_11 = db.PRL484_7.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL484_11.Count > 0)
                    {
                        db.PRL484_7.RemoveRange(lstDeletePRL484_11);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL484_11 = db.PRL484_7.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL484_11.Count > 0)
                    {
                        db.PRL484_7.RemoveRange(lstDeletePRL484_11);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line21 details
        [HttpPost]
        public JsonResult SaveProtocolLine21Linkage(List<PRL484_8> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRL484_8> lstAddPRL484_11 = new List<PRL484_8>();
                List<PRL484_8> lstDeletePRL484_11 = new List<PRL484_8>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL484_8 obj = db.PRL484_8.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL484_8();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            if (!string.IsNullOrWhiteSpace(item.StudNo))
                                isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        obj.StudNo = item.StudNo;
                        obj.RadialDistance = item.RadialDistance;
                        obj.ReqOrientation = item.ReqOrientation;
                        obj.ActOrientation = item.ActOrientation;
                        obj.Size = item.Size;
                        if (isAdded)
                        {
                            lstAddPRL484_11.Add(obj);
                        }
                    }
                    if (lstAddPRL484_11.Count > 0)
                    {
                        db.PRL484_8.AddRange(lstAddPRL484_11);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL484_11 = db.PRL484_8.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL484_11.Count > 0)
                    {
                        db.PRL484_8.RemoveRange(lstDeletePRL484_11);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL484_11 = db.PRL484_8.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL484_11.Count > 0)
                    {
                        db.PRL484_8.RemoveRange(lstDeletePRL484_11);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line23 details
        [HttpPost]
        public JsonResult SaveProtocolLine23Linkage(List<PRL484_9> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRL484_9> lstAddPRL484_11 = new List<PRL484_9>();
                List<PRL484_9> lstDeletePRL484_11 = new List<PRL484_9>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL484_9 obj = db.PRL484_9.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL484_9();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        obj.BracketsReqElevation = item.BracketsReqElevation;
                        obj.BracketsActElevation = item.BracketsActElevation;
                        obj.BracketsReqOrientation = item.BracketsReqOrientation;
                        obj.BracketsActOrientation = item.BracketsActOrientation;
                        obj.BracketsReqProjection = item.BracketsReqProjection;
                        obj.BracketsActProjection = item.BracketsActProjection;
                        obj.DistanceBetweenTwoBrackets = item.DistanceBetweenTwoBrackets;
                        obj.Remark = item.Remark;
                        if (isAdded)
                        {
                            lstAddPRL484_11.Add(obj);
                        }
                    }
                    if (lstAddPRL484_11.Count > 0)
                    {
                        db.PRL484_9.AddRange(lstAddPRL484_11);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL484_11 = db.PRL484_9.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL484_11.Count > 0)
                    {
                        db.PRL484_9.RemoveRange(lstDeletePRL484_11);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL484_11 = db.PRL484_9.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL484_11.Count > 0)
                    {
                        db.PRL484_9.RemoveRange(lstDeletePRL484_11);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line25 details
        [HttpPost]
        public JsonResult SaveProtocolLine25Linkage(List<PRL484_10> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRL484_10> lstAddPRL484_11 = new List<PRL484_10>();
                List<PRL484_10> lstDeletePRL484_11 = new List<PRL484_10>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL484_10 obj = db.PRL484_10.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL484_10();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            if (!string.IsNullOrWhiteSpace(item.CleatNo))
                                isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        obj.CleatNo = item.CleatNo;
                        obj.RadialDistance = item.RadialDistance;
                        obj.ReqOrientation = item.ReqOrientation;
                        obj.ActOrientation = item.ActOrientation;
                        obj.ReqProjection = item.ReqProjection;
                        obj.ActProjection = item.ActProjection;
                        if (isAdded)
                        {
                            lstAddPRL484_11.Add(obj);
                        }
                    }
                    if (lstAddPRL484_11.Count > 0)
                    {
                        db.PRL484_10.AddRange(lstAddPRL484_11);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL484_11 = db.PRL484_10.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL484_11.Count > 0)
                    {
                        db.PRL484_10.RemoveRange(lstDeletePRL484_11);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL484_11 = db.PRL484_10.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL484_11.Count > 0)
                    {
                        db.PRL484_10.RemoveRange(lstDeletePRL484_11);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line27 details
        [HttpPost]
        public JsonResult SaveProtocolLine27Linkage(List<TSRDetailsPRL484_11> lst, int HeaderId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                List<PRL484_11> lstAddPRL484_11 = new List<PRL484_11>();
                List<PRL484_11> lstDeletePRL484_11 = new List<PRL484_11>();
                List<int> lstLinesupload = new List<int>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        #region Header
                        PRL484_11 obj = db.PRL484_11.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL484_11();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            if (!string.IsNullOrWhiteSpace(item.TSRNo))
                                isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.TSRNo = item.TSRNo;
                        obj.TSRNoDescription = item.TSRNoDescription;
                        obj.OffsetDistanceFrom = item.OffsetDistanceFrom;
                        obj.ElevationReq = item.ElevationReq;
                        obj.ProjectionReq = item.ProjectionReq;
                        obj.TSRSketchPath = item.TSRSketchPath;
                        if (isAdded)
                        {
                            db.PRL484_11.Add(obj);
                        }
                        db.SaveChanges();
                        lstLinesupload.Add(obj.LineId);
                        #endregion

                        List<PRL484_11_1> lstAddPRL484_11_1 = new List<PRL484_11_1>();
                        List<PRL484_11_1> lstDeletePRL484_11_1 = new List<PRL484_11_1>();
                        foreach (var lspro in item.LinesData)
                        {
                            if (!string.IsNullOrWhiteSpace(lspro.BeamNo))
                            {
                                bool isLineAdded = false;
                                PRL484_11_1 objPRL484_11_1 = db.PRL484_11_1.Where(x => x.SublineId == lspro.SublineId).FirstOrDefault();
                                if (objPRL484_11_1 == null)
                                {
                                    objPRL484_11_1 = new PRL484_11_1();
                                    objPRL484_11_1.HeaderId = item.HeaderId;
                                    objPRL484_11_1.CreatedBy = objClsLoginInfo.UserName;
                                    objPRL484_11_1.CreatedOn = DateTime.Now;
                                    objPRL484_11_1.LineId = obj.LineId;
                                    if (!string.IsNullOrWhiteSpace(lspro.BeamNo))
                                        isLineAdded = true;
                                }
                                else
                                {
                                    objPRL484_11_1.EditedBy = objClsLoginInfo.UserName;
                                    objPRL484_11_1.EditedOn = DateTime.Now;
                                }

                                objPRL484_11_1.BeamNo = lspro.BeamNo;
                                if (string.IsNullOrWhiteSpace(lspro.DistanceOrientation))
                                {
                                    objPRL484_11_1.DistanceOrientation = "°SIDE ";
                                }
                                else
                                {
                                    objPRL484_11_1.DistanceOrientation = lspro.DistanceOrientation.Trim();
                                }
                                if (string.IsNullOrWhiteSpace(lspro.ReqDistance))
                                {
                                    objPRL484_11_1.ReqDistance = " (TOWARDS   °)";
                                }
                                else
                                {
                                    objPRL484_11_1.ReqDistance = lspro.ReqDistance.Trim();

                                }
                                objPRL484_11_1.ReqDistanceTowards = lspro.ReqDistanceTowards;
                                objPRL484_11_1.ActDistance = lspro.ActDistance;
                                objPRL484_11_1.ActElevation = lspro.ActElevation;
                                objPRL484_11_1.ActProjection = lspro.ActProjection;

                                if (isLineAdded)
                                {
                                    lstAddPRL484_11_1.Add(objPRL484_11_1);
                                }
                            }

                        }
                        if (lstAddPRL484_11_1.Count > 0)
                        {
                            db.PRL484_11_1.AddRange(lstAddPRL484_11_1);
                            db.SaveChanges();
                        }

                        if (!string.IsNullOrWhiteSpace(item.TSRNo))
                        {
                            #region Notes
                            List<PRL484> lstAddPRL484 = new List<PRL484>();
                            List<PRL484> lstDeletePRL484 = new List<PRL484>();
                            if (item.NotesData != null)
                            {

                                foreach (var notes in item.NotesData)
                                {
                                    bool isnotesAdded = false;
                                    PRL484 objNotes = db.PRL484.Where(x => x.LineId == notes.LineId && x.NotesType == "DIMENSION OF BEAM SUPPORT CLIPS ON NUB").FirstOrDefault();
                                    if (objNotes == null)
                                    {
                                        objNotes = new PRL484();
                                        objNotes.HeaderId = notes.HeaderId;
                                        objNotes.CreatedBy = objClsLoginInfo.UserName;
                                        objNotes.CreatedOn = DateTime.Now;
                                        isnotesAdded = true;
                                    }
                                    else
                                    {
                                        obj.EditedBy = objClsLoginInfo.UserName;
                                        obj.EditedOn = DateTime.Now;
                                    }
                                    objNotes.Notes = notes.Notes;
                                    objNotes.NotesType = notes.NotesType;
                                    objNotes.FilterColumn = item.TSRNo;

                                    if (isnotesAdded)
                                    {
                                        lstAddPRL484.Add(objNotes);
                                    }
                                }
                                if (lstAddPRL484.Count > 0)
                                {
                                    db.PRL484.AddRange(lstAddPRL484);
                                }



                                var notesLineIds = item.NotesData.Where(x => x.LineId != 0).Select(a => a.LineId);
                                lstDeletePRL484 = db.PRL484.Where(x => !notesLineIds.Contains(x.LineId) && x.HeaderId == HeaderId && x.FilterColumn == obj.TSRNo && x.NotesType == "DIMENSION OF BEAM SUPPORT CLIPS ON NUB").ToList();
                                if (lstDeletePRL484.Count > 0)
                                {
                                    db.PRL484.RemoveRange(lstDeletePRL484);
                                }

                                db.SaveChanges();
                            }
                            db.SaveChanges();
                            #endregion
                        }

                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL484_11 = db.PRL484_11.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    List<int> lstLineIds = lstDeletePRL484_11.Select(x => x.LineId).ToList();
                    if (lstDeletePRL484_11.Count > 0)
                    {
                        List<PRL484_11_1> lstDelPRL484_11_1 = db.PRL484_11_1.Where(x => lstLineIds.Contains(Convert.ToInt32(x.LineId))).ToList();

                        if (lstDelPRL484_11_1.Count > 0)
                        {
                            db.PRL484_11_1.RemoveRange(lstDelPRL484_11_1);
                        }
                        db.PRL484_11.RemoveRange(lstDeletePRL484_11);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL484_11 = db.PRL484_11.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL484_11.Count > 0)
                    {
                        List<PRL484_11_1> lstDelPRL484_11_1 = db.PRL484_11_1.Where(x => x.HeaderId == HeaderId).ToList();
                        db.PRL484_11_1.RemoveRange(lstDelPRL484_11_1);
                        db.PRL484_11.RemoveRange(lstDeletePRL484_11);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                if (lstLinesupload.Count > 0)
                {
                    objResponseMsg.ActionValue = string.Join(",", lstLinesupload.ToList());
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }
        #endregion

        #region Line29 details
        [HttpPost]
        public JsonResult SaveProtocolLine29Linkage(List<PRL484_12> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRL484_12> lstAddPRL484_12 = new List<PRL484_12>();
                List<PRL484_12> lstDelete484_12 = new List<PRL484_12>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRL484_12 obj = db.PRL484_12.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.DrawingNo = item.DrawingNo;
                                obj.DrawingRevNo = item.DrawingRevNo;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRL484_12 obj = new PRL484_12();

                            if (!string.IsNullOrWhiteSpace(item.DrawingNo))
                            {
                                obj.HeaderId = item.HeaderId;
                                obj.DrawingNo = item.DrawingNo;
                                obj.DrawingRevNo = item.DrawingRevNo;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRL484_12.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRL484_12.Count > 0)
                    {
                        db.PRL484_12.AddRange(lstAddPRL484_12);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDelete484_12 = db.PRL484_12.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDelete484_12.Count > 0)
                    {
                        db.PRL484_12.RemoveRange(lstDelete484_12);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDelete484_12 = db.PRL484_12.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDelete484_12.Count > 0)
                    {
                        db.PRL484_12.RemoveRange(lstDelete484_12);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region All Notes Line details
        [HttpPost]
        public JsonResult SaveProtocolLineNotesLinkage(List<PRL484> lst, int HeaderId, string NotesType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRL484> lstAddPRL484 = new List<PRL484>();
                List<PRL484> lstDeletePRL484 = new List<PRL484>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL484 obj = db.PRL484.Where(x => x.LineId == item.LineId && x.NotesType == NotesType).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL484();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        obj.Notes = item.Notes;
                        obj.NotesType = item.NotesType;
                        obj.FilterColumn = item.FilterColumn;

                        if (isAdded)
                        {
                            lstAddPRL484.Add(obj);
                        }
                    }
                    if (lstAddPRL484.Count > 0)
                    {
                        db.PRL484.AddRange(lstAddPRL484);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL484 = db.PRL484.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId && x.NotesType == NotesType).ToList();
                    if (lstDeletePRL484.Count > 0)
                    {
                        db.PRL484.RemoveRange(lstDeletePRL484);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL484 = db.PRL484.Where(x => x.HeaderId == HeaderId && x.NotesType == NotesType).ToList();
                    if (lstDeletePRL484.Count > 0)
                    {
                        db.PRL484.RemoveRange(lstDeletePRL484);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region External / Internal Attachment
        public JsonResult SaveProtocolLine7Linkage(List<PRL485> lst, int HeaderId, string NotesType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRL485> lstAddPRL485 = new List<PRL485>();
                List<PRL485> lstDeletePRL485 = new List<PRL485>();

                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL485 obj = db.PRL485.Where(x => x.LineId == item.LineId && x.ExternalInternalType == "External").FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL485();
                            obj.HeaderId = item.HeaderId;
                            obj.ExternalInternalType = "External";
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        obj.AttachmentDimensionDescription = item.AttachmentDimensionDescription;
                        obj.AttachmentDimensionElevationReq = item.AttachmentDimensionElevationReq;
                        obj.AttachmentDimensionElevationAct = item.AttachmentDimensionElevationAct;
                        obj.AttachmentDimensionProjectionReq = item.AttachmentDimensionProjectionReq;
                        obj.AttachmentDimensionProjectionAct = item.AttachmentDimensionProjectionAct;
                        obj.AttachmentDimensionOrientationReq = item.AttachmentDimensionOrientationReq;
                        obj.AttachmentDimensionOrientationAct = item.AttachmentDimensionOrientationAct;
                        obj.AttachmentDimensionOrientationArcReq = item.AttachmentDimensionOrientationArcReq;
                        obj.AttachmentDimensionOrientationArcAct = item.AttachmentDimensionOrientationArcAct;
                        obj.AttachmentDimensionTITLReq = item.AttachmentDimensionTITLReq;
                        obj.AttachmentDimensionTITLAct = item.AttachmentDimensionTITLAct;
                        obj.AttachmentDimensionSizeReq = item.AttachmentDimensionSizeReq;
                        obj.AttachmentDimensionSizeAct = item.AttachmentDimensionSizeAct;
                        //obj.NotesType = item.NotesType;
                        //obj.FilterColumn = item.FilterColumn;

                        if (isAdded)
                        {
                            lstAddPRL485.Add(obj);
                        }
                    }
                    if (lstAddPRL485.Count > 0)
                    {
                        db.PRL485.AddRange(lstAddPRL485);
                    }
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL485 = db.PRL485.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId && x.ExternalInternalType == "External").ToList();
                    if (lstDeletePRL485.Count > 0)
                    {
                        db.PRL485.RemoveRange(lstDeletePRL485);
                    }
                    db.SaveChanges();

                }
                else
                {
                    lstDeletePRL485 = db.PRL485.Where(x => x.HeaderId == HeaderId && x.ExternalInternalType == "External").ToList();
                    if (lstDeletePRL485.Count > 0)
                    {
                        db.PRL485.RemoveRange(lstDeletePRL485);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveProtocolLine91Linkage(List<PRL485> lst, int HeaderId, string NotesType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRL485> lstAddPRL485 = new List<PRL485>();
                List<PRL485> lstDeletePRL485 = new List<PRL485>();

                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL485 obj = db.PRL485.Where(x => x.LineId == item.LineId && x.ExternalInternalType == "Internal").FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL485();
                            obj.HeaderId = item.HeaderId;
                            obj.ExternalInternalType = "Internal";
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        obj.AttachmentDimensionDescription = item.AttachmentDimensionDescription;
                        obj.AttachmentDimensionElevationReq = item.AttachmentDimensionElevationReq;
                        obj.AttachmentDimensionElevationAct = item.AttachmentDimensionElevationAct;
                        obj.AttachmentDimensionProjectionReq = item.AttachmentDimensionProjectionReq;
                        obj.AttachmentDimensionProjectionAct = item.AttachmentDimensionProjectionAct;
                        obj.AttachmentDimensionOrientationReq = item.AttachmentDimensionOrientationReq;
                        obj.AttachmentDimensionOrientationAct = item.AttachmentDimensionOrientationAct;
                        obj.AttachmentDimensionOrientationArcReq = item.AttachmentDimensionOrientationArcReq;
                        obj.AttachmentDimensionOrientationArcAct = item.AttachmentDimensionOrientationArcAct;
                        obj.AttachmentDimensionTITLReq = item.AttachmentDimensionTITLReq;
                        obj.AttachmentDimensionTITLAct = item.AttachmentDimensionTITLAct;
                        obj.AttachmentDimensionSizeReq = item.AttachmentDimensionSizeReq;
                        obj.AttachmentDimensionSizeAct = item.AttachmentDimensionSizeAct;
                        //obj.NotesType = item.NotesType;
                        //obj.FilterColumn = item.FilterColumn;

                        if (isAdded)
                        {
                            lstAddPRL485.Add(obj);
                        }
                    }
                    if (lstAddPRL485.Count > 0)
                    {
                        db.PRL485.AddRange(lstAddPRL485);
                    }
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL485 = db.PRL485.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId && x.ExternalInternalType == "Internal").ToList();
                    if (lstDeletePRL485.Count > 0)
                    {
                        db.PRL485.RemoveRange(lstDeletePRL485);
                    }
                    db.SaveChanges();

                }
                else
                {
                    lstDeletePRL485 = db.PRL485.Where(x => x.HeaderId == HeaderId && x.ExternalInternalType == "Internal").ToList();
                    if (lstDeletePRL485.Count > 0)
                    {
                        db.PRL485.RemoveRange(lstDeletePRL485);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        [HttpPost]
        public ActionResult DeletePhoto(int PhotoNo, int Headerid, string strTableName)
        {
            try
            {
                var Files = (new clsFileUpload()).GetDocuments(strTableName + "/" + Headerid);
                foreach (var item in Files)
                {
                    (new clsFileUpload()).DeleteFile(strTableName + "/" + Headerid, item.Name);
                }

                return Json(true);
            }
            catch
            {
                return Json(false);
            }
        }

        public ActionResult DeleteMultiplePhoto(int PhotoNo, int Headerid, string strTableName, string LineId)
        {
            try
            {
                var Files = (new clsFileUpload()).GetDocuments(strTableName + "/" + Headerid + "/" + LineId);
                foreach (var item in Files)
                {
                    (new clsFileUpload()).DeleteFile(strTableName + "/" + Headerid + "/" + LineId, item.Name);
                }

                return Json(true);
            }
            catch
            {
                return Json(false);
            }
        }

        public JsonResult GetImagePathofLine(Int32 lineid)
        {
            string imagepath = "";
            try
            {
                FCS001 _obj = db.FCS001.FirstOrDefault(f => f.TableId == lineid && f.TableName == "PRL480//Sketch3");
                if (_obj != null)
                    imagepath = _obj.MainDocumentPath;
            }
            catch { }
            return Json(imagepath, JsonRequestBehavior.AllowGet);
        }

    }
    public class TSRDetails
    {
        public int LineId { get; set; }
        public Nullable<int> HeaderId { get; set; }
        public string TSRNo { get; set; }
        public string TSRNoDescription { get; set; }
        public string ElevationFrom { get; set; }
        public string ElevationReq { get; set; }
        public string ElevationTOL { get; set; }
        public string TSRWidthReq { get; set; }
        public string TSRWidthTOL { get; set; }
        public string TSRHeightReq { get; set; }
        public string TSRHeightTOL { get; set; }
        public string IsTSRID { get; set; }
        public string TSRIDReq { get; set; }
        public string TSRIDTOL { get; set; }
        public string LevelnessOfTopFaceTSRTOL { get; set; }
        public string TiltingOfTSRTOL { get; set; }
        public string FlatnessIn { get; set; }
        public string ArcDistanceOfTSRTOL { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public List<PRO484_5_1> LinesData { get; set; }
        public List<PRO484> NotesData { get; set; }
    }

    public class TSRDetailsPRO484_6
    {
        public int LineId { get; set; }
        public Nullable<int> HeaderId { get; set; }
        public string TSRNo { get; set; }
        public string TSRNoDescription { get; set; }
        public string ElevationFrom { get; set; }
        public string ElevationReq { get; set; }
        public string ElevationTOL { get; set; }
        public string TSRWidthReq { get; set; }
        public string TSRWidthTOL { get; set; }
        public string TSRProjectionReq { get; set; }
        public string TSRProjectionTOL { get; set; }
        public string IsInsideDiameter { get; set; }
        public string InsideDiameterReq { get; set; }
        public string InsideDiameterTOL { get; set; }
        public string LevelnessOfTopFaceTSRTOL { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public List<PRO484_6_1> LinesData { get; set; }
        public List<PRO484> NotesData { get; set; }
    }

    public class TSRDetailsPRO484_11
    {
        public List<PRO484> NotesData { get; set; }
        public List<PRO484_11_1> LinesData { get; set; }
        public int LineId { get; set; }
        public Nullable<int> HeaderId { get; set; }
        public string TSRNo { get; set; }
        public string TSRNoDescription { get; set; }
        public string OffsetDistanceFrom { get; set; }
        public string ElevationReq { get; set; }
        public string ProjectionReq { get; set; }
        public string TSRSketchPath { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }

    }


    public class TSRDetailsPRL484_5
    {
        public int LineId { get; set; }
        public Nullable<int> HeaderId { get; set; }
        public string TSRNo { get; set; }
        public string TSRNoDescription { get; set; }
        public string ElevationFrom { get; set; }
        public string ElevationReq { get; set; }
        public string ElevationTOL { get; set; }
        public string TSRWidthReq { get; set; }
        public string TSRWidthTOL { get; set; }
        public string TSRHeightReq { get; set; }
        public string TSRHeightTOL { get; set; }
        public string IsTSRID { get; set; }
        public string TSRIDReq { get; set; }
        public string TSRIDTOL { get; set; }
        public string LevelnessOfTopFaceTSRTOL { get; set; }
        public string TiltingOfTSRTOL { get; set; }
        public string FlatnessIn { get; set; }
        public string ArcDistanceOfTSRTOL { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public List<PRL484_5_1> LinesData { get; set; }
        public List<PRL484> NotesData { get; set; }
    }

    public class TSRDetailsPRL484_6
    {
        public int LineId { get; set; }
        public Nullable<int> HeaderId { get; set; }
        public string TSRNo { get; set; }
        public string TSRNoDescription { get; set; }
        public string ElevationFrom { get; set; }
        public string ElevationReq { get; set; }
        public string ElevationTOL { get; set; }
        public string TSRWidthReq { get; set; }
        public string TSRWidthTOL { get; set; }
        public string TSRProjectionReq { get; set; }
        public string TSRProjectionTOL { get; set; }
        public string IsInsideDiameter { get; set; }
        public string InsideDiameterReq { get; set; }
        public string InsideDiameterTOL { get; set; }
        public string LevelnessOfTopFaceTSRTOL { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public List<PRL484_6_1> LinesData { get; set; }
        public List<PRL484> NotesData { get; set; }
    }

    public class TSRDetailsPRL484_11
    {
        public List<PRL484> NotesData { get; set; }
        public List<PRL484_11_1> LinesData { get; set; }
        public int LineId { get; set; }
        public Nullable<int> HeaderId { get; set; }
        public string TSRNo { get; set; }
        public string TSRNoDescription { get; set; }
        public string OffsetDistanceFrom { get; set; }
        public string ElevationReq { get; set; }
        public string ProjectionReq { get; set; }
        public string TSRSketchPath { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }

    }
}