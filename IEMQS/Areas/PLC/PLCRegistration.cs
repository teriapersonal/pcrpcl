﻿using System.Web.Mvc;

namespace IEMQS.Areas.PLC
{
    public class PLCAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "PLC";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "PLC_default",
                "PLC/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}