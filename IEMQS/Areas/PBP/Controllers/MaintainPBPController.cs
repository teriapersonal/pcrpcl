﻿using IEMQS.Areas.TEMP.Controllers;
using IEMQS.DESCore;
using IEMQS.DESServices;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsHelper;

namespace IEMQS.Areas.PBP.Controllers
{
    public class MaintainPBPController : clsBase
    {
        [SessionExpireFilter]
        public ActionResult Index(string type)
        {
            if (Manager.GetpageAccess("PBP", "MaintainPBP", "Index?Type=" + type, objClsLoginInfo.UserName) == 0)
                return Redirect("~/Authentication/Authenticate/AccessDenied");
            
            var objPBP002 = db.PBP001.FirstOrDefault(i => i.ProductCode.ToLower() == type.ToLower());
            ViewBag.Id = objPBP002 != null ? objPBP002.Id : 0;
            ViewBag.Title = clsImplementationEnum.PBPIndexTitle.MaintainPBP.GetStringValue() + (objPBP002 != null ? "<br/>Product Type: "+ objPBP002.Product : "");
            ViewBag.IndexType = clsImplementationEnum.PBPIndexType.maintain.GetStringValue();
            ViewBag.ProductCode = type;
            return View();
        } 
        
        [SessionExpireFilter]
        public ActionResult Approve(string type)
        {
            if (Manager.GetpageAccess("PBP", "MaintainPBP", "Approve?Type=" + type, objClsLoginInfo.UserName) == 0)
                return Redirect("~/Authentication/Authenticate/AccessDenied");

            var objPBP002 = db.PBP001.FirstOrDefault(i => i.ProductCode.ToLower() == type.ToLower());
            ViewBag.Title = clsImplementationEnum.PBPIndexTitle.ApprovePBP.GetStringValue() + (objPBP002 != null ? "<br/>Product Type: " + objPBP002.Product : "");
            ViewBag.IndexType = clsImplementationEnum.PBPIndexType.approve.GetStringValue();
            ViewBag.Id = objPBP002 != null ? objPBP002.Id : 0;
            ViewBag.ProductCode = type;
            return View("Index");
        }

        public ActionResult GetIndexGridDataPartial(string status, string title, string indextype, string ProductCode)
        {
            ViewBag.Status = status;
            ViewBag.GridTitle = title;
            ViewBag.ProductCode = ProductCode;
            ViewBag.indextype = indextype;
            return PartialView("_GetIndexGridDataPartial");
        }

        public JsonResult LoadPBPData(JQueryDataTableParamModel param, string ProductCode, string indextype)
        {
            try
            {
                UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = "1=1 and product='" + ProductCode + "'";
                if (param.Status == "Pending")
                {
                    if(indextype == clsImplementationEnum.PBPIndexType.approve.GetStringValue())
                        strWhere += " and [Status]='" + clsImplementationEnum.PBPStatus.SentForApproval.GetStringValue() + "'";
                    else
                        strWhere += " and [Status] in ('" + clsImplementationEnum.PBPStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.PBPStatus.Returned.GetStringValue() + "')";
                }

                string[] columnName = { "[Procedure]", "RevNo", "[Description]", "ProcessLicensorDesc", "Status" };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                else
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By [" + sortColumnName + "] " + sortDirection + " ";
                }
                var lstResult = db.SP_PBP_GET_MAINTAIN_DATA(StartIndex, EndIndex, strSortOrder, strWhere).ToList();
                var CheckedIds = "";
                if (param.bCheckedAll && lstResult.Any())
                    CheckedIds = String.Join(",", db.SP_PBP_GET_MAINTAIN_DATA(1, 0, "", strWhere).Select(s => s.Id).ToList());
                bool isInitiator = (objUserRoleAccessDetails.UserDesignation == clsImplementationEnum.UserAccessRole.Initiator.GetStringValue());
                var statusApproved = clsImplementationEnum.PBPStatus.Approved.GetStringValue();
                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.Id),
                               uc.Procedure,
                               "R"+uc.RevNo,
                               uc.Description,
                               uc.ProcessLicensorDesc,
                               uc.Status,
                               (indextype == "m" || param.Status != "Pending"? uc.ReturnRemark: Helper.GenerateTextbox(uc.Id,"ReturnRemark",uc.ReturnRemark)),
                               uc.ApprovedBy,
                               GenerateAction(uc,indextype,param.Status)
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere,
                    checkedIds = CheckedIds
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        private string GenerateAction(SP_PBP_GET_MAINTAIN_DATA_Result uc,string indextype = "m", string Status = "Pending")
        {
            var tdData = "<center>";
            if (indextype == "m")
            {
                if(Status == "Pending")
                {
                    tdData += Helper.HTMLActionString(uc.Id, "Edit", "Edit Record", "fa fa-pencil-square-o", "EnabledEditLine(" + uc.Id + ");") 
                        + Helper.HTMLActionString(uc.Id, "Delete", "Delete Record", "fa fa-trash-o ", "DeleteLIne(" + uc.Id + ")", "", uc.RevNo > 0);
                }
                else
                {
                    tdData += Helper.HTMLActionString(uc.Id, "Retract", "Retract", "fa fa-repeat", "RetractPBP(" + uc.Id + ")", "", uc.Status != clsImplementationEnum.PBPStatus.SentForApproval.GetStringValue())
                           + Helper.HTMLActionString(uc.Id, "Revise", "Revise", "fa fa-retweet", "RevisePBP(" + uc.Id + ")", "", uc.Status != clsImplementationEnum.PBPStatus.Approved.GetStringValue());
                }
            }
            else
            {
                tdData += Helper.HTMLActionString(uc.Id, "Cancel", "Cancel", "fa fa-ban", "CancelPBP(" + uc.Id + ")","", uc.Status != clsImplementationEnum.PBPStatus.SentForApproval.GetStringValue() && uc.Status != clsImplementationEnum.PBPStatus.Approved.GetStringValue()); 
            }
            tdData += Helper.HTMLActionString(uc.Id, "Attachments", "Add/Update Attachment", "fa fa-paperclip attachments", "ViewAttachment(" + uc.Id + ","+(uc.Status == clsImplementationEnum.PBPStatus.Draft.GetStringValue() || uc.Status == clsImplementationEnum.PBPStatus.Returned.GetStringValue() ? "true":"false" ) +")")
                + Helper.HTMLActionString(uc.Id, "History", "History", "fa fa-history", "HistoryPBP(" + uc.Id + ")", "", uc.RevNo == 0) 
                + "</center>";
            return tdData;
        }

        [HttpPost]
        public JsonResult LoadPBPEditableData(int id, string ProductCode, bool isReadOnly = false)
        {
            try
            {
                var data = new List<string[]>();

                var result = new List<ProcesslicensorEnt>();
                if (TempData["ProcessLicensor"] == null)
                    result = db.PBP003.Select(x => new ProcesslicensorEnt { ProcessLicensor = x.ProcessLicensor, Id = x.Id }).Distinct().ToList();
                else
                    result = (List<ProcesslicensorEnt>)TempData["ProcessLicensor"];
                TempData["ProcessLicensor"] = result;

                var items = (from li in result
                             select new SelectItemList
                             {
                                 id = li.Id.ToString(),
                                 text = li.ProcessLicensor.ToString()
                             }).ToList();

                if (id > 0)
                {
                    var lstResult = db.SP_PBP_GET_MAINTAIN_DATA(1, 0, "", "A.Id = " + id).Take(1).ToList();

                    data = (from uc in lstResult
                            select new[]
                           {
                                clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                                clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                                clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                                isReadOnly ? uc.Description:  Helper.GenerateTextbox(uc.Id,"Description",uc.Description, "",false, "", "250","form-control"),
                                isReadOnly ? uc.ProcessLicensorDesc:  MultiSelectDropdown(items,id,uc.ProcessLicensor,false,"","","ProcessLicensor"),
                                clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                                clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                                isReadOnly ? uc.ApprovedBy: Helper.HTMLAutoComplete(uc.Id,"txtApprovedBy",uc.ApprovedBy,"",false,"","",false,"","","ApprovedBy","form-control")+""+GenerateHidden(uc.Id,"ApprovedBy",uc.ApprovedBy, "ApprovedBy"),
                                isReadOnly
                                    ? GenerateAction(uc) :  ("<center>"+
                                    Helper.HTMLActionString(uc.Id, "Update", "Update Record", "fa fa-floppy-o", "SaveEditRecord(" + uc.Id + ");", "", false) +
                                    Helper.HTMLActionString(uc.Id, "Cancel", "Cancel Edit", "fa fa-ban", "CancelEditLine(" + uc.Id + "); ", "", false) +
                                    "</center>")
                           }).ToList();
                }
                else
                {

                    var objProcedureDtl = GetMaxDocumentNo(ProductCode);
                    PBP002 objNewPBP002 = new PBP002();

                    objNewPBP002.Active = true;
                    objNewPBP002.CreatedBy = objClsLoginInfo.UserName;
                    objNewPBP002.CreatedOn = DateTime.Now;
                    objNewPBP002.Product = ProductCode;
                    objNewPBP002.DocNo = objProcedureDtl.DocNo;
                    objNewPBP002.Procedure = objProcedureDtl.Procedure;
                    objNewPBP002.RevNo = 0;
                    objNewPBP002.Status = clsImplementationEnum.PBPStatus.Draft.GetStringValue();
                    db.PBP002.Add(objNewPBP002);
                    db.SaveChanges();

                    data.Add(new[]
                           {
                               Convert.ToString(objNewPBP002.Id),
                                objNewPBP002.Procedure ,
                                "R0",
                                Helper.GenerateTextbox(objNewPBP002.Id,"Description",  "", "",false, "", "250","form-control"),
                                "<select id='ProcessLicensor" + objNewPBP002.Id + "'  multiple='multiple' class='form-control multiselect-drodown' />",
                                 objNewPBP002.Status,
                                "",
                                Helper.HTMLAutoComplete(objNewPBP002.Id,"txtApprovedBy",objNewPBP002.ApprovedBy,"",false,"","",false,"","","ApprovedBy","form-control")+""+GenerateHidden(objNewPBP002.Id,"ApprovedBy","","ApprovedBy"),
                                "<center>"+ Helper.HTMLActionString(objNewPBP002.Id,"Add","Add New Record","fa fa-plus","SaveEditRecord("+objNewPBP002.Id+");")+
                                Helper.HTMLActionString(objNewPBP002.Id,"Attachments","Add/Update Attachment","fa fa-paperclip attachments","ViewAttachment(" + objNewPBP002.Id + ",true)")+"</center>"
                           });
                }

                return Json(new
                {
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SubmitPBP(string strHeader)
        {
            var objResponseMsg = new ResponceMsgWithObject(); 
            try
            {
                if (!string.IsNullOrEmpty(strHeader))
                {
                    List<string> submittedPBP = new List<string>();
                    List<string> pendingPBP = new List<string>();
                    int[] headerIds = Array.ConvertAll(strHeader.Split(','), s => int.Parse(s));
                    foreach (int headerId in headerIds)
                    {
                        var PBP001 = db.PBP002.FirstOrDefault(x => x.Id == headerId);
                        if (SendForApproval(headerId))
                            submittedPBP.Add(PBP001.Procedure);
                        else
                            pendingPBP.Add(PBP001.Procedure);
                    }
                    objResponseMsg.data = new {
                        submittedPBP = (submittedPBP.Count > 0 ? objResponseMsg.data = "Procedure " + String.Join(",", submittedPBP) + " submitted successfully" : ""),
                        pendingPBP = (pendingPBP.Count > 0? objResponseMsg.data = "Fail to Submit Procedure:" + String.Join(",", pendingPBP) + ".": "")
                    };
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Procedure has been sucessfully submitted for approval";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please select atleast one record sending for approval";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        private bool SendForApproval(int headerId)
        {
            var objPBP002 = db.PBP002.FirstOrDefault(i => i.Id == headerId);
            var lineStatus = new string[] { clsImplementationEnum.PBPStatus.Draft.GetStringValue(), clsImplementationEnum.PBPStatus.Returned.GetStringValue() };
            if (objPBP002 != null && lineStatus.Contains(objPBP002.Status))
            {
                objPBP002.Status = clsImplementationEnum.PBPStatus.SentForApproval.GetStringValue();
                objPBP002.ReturnRemark = "";
                objPBP002.SubmittedBy = objClsLoginInfo.UserName;
                objPBP002.SubmittedOn = DateTime.Now;

                db.SaveChanges();

                #region Send Notification
                //(new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG1.GetStringValue()+","+clsImplementationEnum.UserRoleName.PLNG2.GetStringValue(),"", "", "", "PBP: " + objPBP002.Procedure+ " has been submitted for your approval", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/PBP/MaintainPBP/Approve?type=" + objPBP002.Product);
                #endregion

                return true;
            }
            return false;
        }


        [HttpPost]
        public ActionResult ApprovePBP(string strHeader)
        {
            var objResponseMsg = new ResponceMsgWithObject(); 
            try
            {
                if (!string.IsNullOrEmpty(strHeader))
                {
                    List<string> approvedPBP = new List<string>();
                    List<string> pendingPBP = new List<string>();
                    int[] headerIds = Array.ConvertAll(strHeader.Split(','), s => int.Parse(s));
                    foreach (int headerId in headerIds)
                    {
                        var PBP001 = db.PBP002.FirstOrDefault(x => x.Id == headerId);
                        if (ApproveByPLNG(headerId))
                            approvedPBP.Add(PBP001.Procedure);
                        else
                            pendingPBP.Add(PBP001.Procedure);
                    }
                    objResponseMsg.data = new
                    {
                        approvedPBP = (approvedPBP.Count > 0 ? objResponseMsg.data = "Procedure " + String.Join(",", approvedPBP) + " Approved successfully" : ""),
                        pendingPBP = (pendingPBP.Count > 0 ? objResponseMsg.data = "Fail to Approve Procedure:" + String.Join(",", pendingPBP) + "." : "")
                    };
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Procedure has been sucessfully submitted for approval";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please select atleast one record for approve";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        private bool ApproveByPLNG(int headerId)
        {
            var lineStatus = clsImplementationEnum.PBPStatus.SentForApproval.GetStringValue();
            var objPBP002 = db.PBP002.FirstOrDefault(i => i.Id == headerId && i.Status == lineStatus);
            if (objPBP002 != null)
            {
                objPBP002.Status = clsImplementationEnum.PBPStatus.Approved.GetStringValue();
                objPBP002.ApprovedBy = objClsLoginInfo.UserName;
                objPBP002.ApprovedOn = DateTime.Now;
                db.SaveChanges();
                var lstPBP002_Log = db.PBP002_Log.Where(c => c.Id == objPBP002.Id).ToList();
                if (lstPBP002_Log.Count > 0)
                {
                    lstPBP002_Log.ForEach(x =>
                    {
                        x.Status = clsImplementationEnum.PBPStatus.Superseded.GetStringValue();
                    });
                }
                #region Maintain Log Details
                PBP002_Log objPBP002_Log = new PBP002_Log();
                objPBP002_Log.Id = objPBP002.Id;
                objPBP002_Log.Procedure = objPBP002.Procedure;
                objPBP002_Log.ProcessLicensor = objPBP002.ProcessLicensor;
                objPBP002_Log.Product = objPBP002.Product;
                objPBP002_Log.Active = objPBP002.Active;
                objPBP002_Log.Description = objPBP002.Description;
                objPBP002_Log.DocNo = objPBP002.DocNo;
                objPBP002_Log.RevNo = objPBP002.RevNo;
                objPBP002_Log.Status = objPBP002.Status;
                objPBP002_Log.CreatedBy = objPBP002.CreatedBy;
                objPBP002_Log.CreatedOn = objPBP002.CreatedOn;
                objPBP002_Log.EditedBy = objPBP002.EditedBy;
                objPBP002_Log.EditedOn = objPBP002.EditedOn;
                objPBP002_Log.SubmittedBy = objPBP002.SubmittedBy;
                objPBP002_Log.SubmittedOn = objPBP002.SubmittedOn;
                objPBP002_Log.ApprovedBy = objPBP002.ApprovedBy;
                objPBP002_Log.ApprovedOn = objPBP002.ApprovedOn;
                objPBP002_Log.ReturnRemark = objPBP002.ReturnRemark;
                db.PBP002_Log.Add(objPBP002_Log);
                db.SaveChanges();

                #endregion

                #region Send Notification
                //(new clsManager()).SendNotificationByUserPSNumber(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue(),objPBP002.SubmittedBy, "PBP: " + objPBP002.Procedure + " is approved", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/PBP/MaintainPBP/Index?type="+objPBP002.Product);
                #endregion

                return true;
            }
            return false;
        }

        [HttpPost]
        public ActionResult ReturnPBP(string strHeader, string[] ReturnRemarks)
        {
            var objResponseMsg = new ResponceMsgWithObject();
            try
            {
                if (!string.IsNullOrEmpty(strHeader))
                {
                    int[] headerIds = Array.ConvertAll(strHeader.Split(','), s => int.Parse(s));

                    List<string> returnPBP = new List<string>();
                    List<string> pendingPBP = new List<string>();
                    for (int i = 0; i < headerIds.Length; i++)
                    {
                        var id = headerIds[i];
                        var PBP001 = db.PBP002.FirstOrDefault(x => x.Id == id);
                        if (ReturnByPLNG(id, ReturnRemarks[i]))
                            returnPBP.Add(PBP001.Procedure);
                        else
                            pendingPBP.Add(PBP001.Procedure);
                    }
                    objResponseMsg.data = new
                    {
                        returnPBP = (returnPBP.Count > 0 ? objResponseMsg.data = "Procedure " + String.Join(",", returnPBP) + " Returned successfully" : ""),
                        pendingPBP = (pendingPBP.Count > 0 ? objResponseMsg.data = "Fail to Return Procedure:" + String.Join(",", pendingPBP) + "." : "")
                    };
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "PBP(s) has been sucessfully returned";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please select atleast one record for return";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        private bool ReturnByPLNG(int headerId, string returnRemark)
        {
            var lineStatus = clsImplementationEnum.PBPStatus.SentForApproval.GetStringValue();
            var objPBP002 = db.PBP002.FirstOrDefault(i => i.Id == headerId && i.Status == lineStatus);
            if (objPBP002 != null)
            {
                objPBP002.Status = clsImplementationEnum.PBPStatus.Returned.GetStringValue();
                objPBP002.ReturnRemark = returnRemark;
                objPBP002.ApprovedBy = objClsLoginInfo.UserName;
                objPBP002.ApprovedOn = DateTime.Now;
                db.SaveChanges();

                #region Send Notification
                //(new clsManager()).SendNotificationByUserPSNumber(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue(),objPBP002.SubmittedBy, "PBP: " + objPBP002.Procedure + " has been returned", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/PBP/MaintainPBP/Index?type="+objPBP002.Product);
                #endregion

                return true;
            }
            return false;
        }

        [HttpPost]
        public ActionResult RevisePBP(int headerId)
        {
            var objResponseMsg = new ResponseMsg();
            try
            {
                var objPBP002 = db.PBP002.FirstOrDefault(c => c.Id == headerId);
                if (objPBP002 != null)
                {
                    if (objPBP002.Status != clsImplementationEnum.PBPStatus.Approved.GetStringValue())
                    {
                        objResponseMsg.Value = "This data is already Revised, it cannot be revise now!!";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    objPBP002.Status = clsImplementationEnum.PBPStatus.Draft.GetStringValue();
                    objPBP002.RevNo = objPBP002.RevNo + 1;
                    objPBP002.EditedBy = objClsLoginInfo.UserName;
                    objPBP002.EditedOn = DateTime.Now;
                    objPBP002.ApprovedBy = null;
                    objPBP002.ApprovedOn = null;
                    objPBP002.ReturnRemark = null;
                         
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "PBP revised successfully";
                }
                else
                {
                    objResponseMsg.Value = "Record Not Found";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult RetractPBP(int headerId)
        {
            var objResponseMsg = new ResponseMsg();
            try
            {
                var objPBP002 = db.PBP002.FirstOrDefault(c => c.Id == headerId);
                if (objPBP002 != null)
                {
                    if (objPBP002.Status != clsImplementationEnum.PBPStatus.SentForApproval.GetStringValue())
                    {
                        objResponseMsg.Value = "PBP cannot be retract if it's not in sent for approval status!!";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    objPBP002.Status = clsImplementationEnum.PBPStatus.Draft.GetStringValue();
                    objPBP002.EditedBy = objClsLoginInfo.UserName;
                    objPBP002.EditedOn = DateTime.Now;
                    objPBP002.SubmittedBy = null;
                    objPBP002.SubmittedOn = null;
                         
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "PBP revised successfully";
                }
                else
                {
                    objResponseMsg.Value = "Record Not Found";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CancelPBP(int headerId)
        {
            var objResponseMsg = new ResponseMsg();
            try
            {
                var objPBP002 = db.PBP002.FirstOrDefault(c => c.Id == headerId);
                if (objPBP002 != null)
                {
                    if (objPBP002.Status != clsImplementationEnum.PBPStatus.SentForApproval.GetStringValue() && objPBP002.Status != clsImplementationEnum.PBPStatus.Approved.GetStringValue())
                    {
                        objResponseMsg.Value = "PBP cannot be cancel if it's not in sent for approval or approved status!!";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    objPBP002.Status = clsImplementationEnum.PBPStatus.Cancel.GetStringValue();
                    objPBP002.EditedBy = objClsLoginInfo.UserName;
                    objPBP002.EditedOn = DateTime.Now;
                    objPBP002.SubmittedBy = null;
                    objPBP002.SubmittedOn = null;

                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "PBP revised successfully";
                }
                else
                {
                    objResponseMsg.Value = "Record Not Found";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetAllProcessLicensor(string search)
        {
            List<CategoryData> lstProcessLicensor = new List<CategoryData>();
            if (!string.IsNullOrWhiteSpace(search))
            {
                lstProcessLicensor = (from p3 in db.PBP003
                                      where (p3.ProcessLicensor.Contains(search))
                                      select new CategoryData { id = p3.Id.ToString(), text = p3.ProcessLicensor, Description = p3.ProcessLicensor }).ToList();
            }
            else
            {
                lstProcessLicensor = (from p3 in db.PBP003
                                      where (p3.ProcessLicensor.Contains(search))
                                      select new CategoryData { id = p3.Id.ToString(), text = p3.ProcessLicensor, Description = p3.ProcessLicensor }).ToList();
            }
            return Json(lstProcessLicensor, JsonRequestBehavior.AllowGet);
        }

        public ProcedureDtlEnt GetMaxDocumentNo(string ProductCode)
        {
            ProcedureDtlEnt objProcedureDtlEnt = new ProcedureDtlEnt();
            var objMaxProductCode = db.PBP002.Where(i => i.Product.ToUpper() == ProductCode.ToUpper()).OrderByDescending(i => i.DocNo).FirstOrDefault();
            if (objMaxProductCode != null)
            {
                objProcedureDtlEnt.DocNo = objMaxProductCode.DocNo.Value + 1;
            }
            else
            {
                objProcedureDtlEnt.DocNo = 1;
            }
            objProcedureDtlEnt.Procedure = "H02-PLNG-" + ProductCode.ToUpper() + "-" + objProcedureDtlEnt.DocNo.ToString("D3");
            return objProcedureDtlEnt;
        }

        public static string MultiSelectDropdown(List<SelectItemList> list, int rowID, string selectedValue, bool Disabled = false, string OnChangeEvent = "", string OnBlurEvent = "", string ColumnName = "")
        {
            string multipleSelect = string.Empty;
            string[] arrayVal = { };

            try
            {
                if (!string.IsNullOrWhiteSpace(selectedValue))
                {
                    arrayVal = selectedValue.Split(',');
                }

                multipleSelect += "<select data-name='ddlmultiple' data-oldvalue='" + selectedValue + "' name='ProcessLicensor" + rowID + "' id='ProcessLicensor" + rowID + "' data-lineid='" + rowID + "' multiple='multiple'  style='width: 100 % ' colname='" + ColumnName + "' class='form-control multiselect-drodown' " + (Disabled ? "disabled " : "") + (OnChangeEvent != string.Empty ? "onchange=" + OnChangeEvent + "" : "") + (OnBlurEvent != string.Empty ? " onblur='" + OnBlurEvent + "'" : "") + " >";
                foreach (var val in arrayVal)
                {
                    if (val != null)
                    {
                        var item = list.FirstOrDefault(w => w.id == val);
                        if (item != null)
                            multipleSelect += "<option value='" + item.id + "' selected" + " >" + item.text + "</option>";
                    }
                }
                foreach (var item in list)
                {
                    if (arrayVal.Contains(item.id + ""))
                        continue;

                    multipleSelect += "<option value='" + item.id + "'  " + ((arrayVal.Contains(item.id)) ? " selected" : "") + " >" + item.text + "</option>";
                }
                multipleSelect += "</select>";
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return multipleSelect;
        }

        [HttpPost]
        public ActionResult SaveUtiltiyBPB(FormCollection fc)
        {
            int Id = Convert.ToInt32(fc["Id"]);
            PBP002 objPBP002 = new PBP002();
            ResponseMsg objResponseMsg = new ResponseMsg();
            try
            {
                if (Id > 0)
                {
                    objPBP002 = db.PBP002.Where(x => x.Id == Id).FirstOrDefault();
                    if (objPBP002 != null)
                    {
                        objPBP002.ApprovedBy = fc["ApprovedBy" + Id];
                        objPBP002.Description = fc["Description" + Id];
                        objPBP002.ProcessLicensor = fc["ProcessLicensor" + Id] == "null"? null: fc["ProcessLicensor" + Id];
                        //objPBP002.RevNo = Convert.ToInt32(fc["RevNo" + Id].ToString());
                        objPBP002.EditedBy = objClsLoginInfo.UserName;
                        objPBP002.EditedOn = DateTime.Now;
                    }
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update;
                }
                else
                {
                    string ProductCode = fc["ProductCode"];

                    var objProcedureDtl = GetMaxDocumentNo(ProductCode);
                    PBP002 objNewPBP002 = new PBP002();
                    objNewPBP002.Active = true;
                    objNewPBP002.Product = ProductCode;
                    objNewPBP002.DocNo = objProcedureDtl.DocNo;
                    objNewPBP002.Procedure = objProcedureDtl.Procedure;
                    objNewPBP002.Description = fc["Description" + Id];
                    objNewPBP002.ProcessLicensor = fc["ProcessLicensor" + Id] == "null"? null: fc["ProcessLicensor" + Id];
                    objNewPBP002.RevNo = 0;
                    objPBP002.ApprovedBy = fc["ApprovedBy" + Id];
                    objNewPBP002.Status = clsImplementationEnum.PBPStatus.Draft.GetStringValue();
                    objNewPBP002.CreatedBy = objClsLoginInfo.UserName;
                    objNewPBP002.CreatedOn = DateTime.Now;

                    db.PBP002.Add(objNewPBP002);
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert;
                }
            }
            catch (Exception ex)
            {
                ex.Message.ToString();

                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg);
        }

        [HttpPost]
        public JsonResult DeleteLines(int lineId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            var objPBP002 = db.PBP002.FirstOrDefault(x => x.Id == lineId);
            try
            {
                if (objPBP002 != null)
                {
                    //delete document before deleting material
                    string deletefolderPath = "PBP002/" + objPBP002.Id;
                    //var lstRefDoc = (new clsFileUpload()).GetDocuments(deletefolderPath, true).Select(x => x.Name).ToList();
                    var doc = db.FCS001.Where(f => f.TableId == objPBP002.Id && f.TableName == "PBP002").ToList();
                    foreach(var itm in doc)
                    {
                         IEMQS.Areas.Utility.Controllers.FileUploadController _obj = new IEMQS.Areas.Utility.Controllers.FileUploadController();
                        _obj.DeleteFileOnFCSServerAsync(itm.DocumentMappingId, CommonService.GetUseIPConfig);
                    }
                    //foreach (var filename in lstRefDoc)
                    //{
                    //    (new clsFileUpload()).DeleteFileAsync(deletefolderPath, filename.ToString());
                    //}

                    db.PBP002.Remove(objPBP002);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Record deleted successfully";
                }
                else
                {
                    objResponseMsg.Value = "Record already deleted";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg);
        }

        [HttpPost]
        public ActionResult GetApprovedBy(string term)
        {
            try
            {
                string role = "PLNG2,PLNG1";
                List<SP_GET_EMPLOYEE_ROLE_DESCRIPTION_LIST_Result> lstgetAllbom = db.SP_GET_EMPLOYEE_ROLE_DESCRIPTION_LIST(role).ToList();

                if (lstgetAllbom != null)
                {
                    var item = lstgetAllbom.Where(x => term == "" || x.EmployeeNameRole.Trim().Contains(term.Trim())).Select(x => new
                    {
                        Employee = x.Employee,
                        EmployeeNameRole = x.EmployeeNameRole
                    }).Distinct().Take(10).ToList();

                    return Json(item, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        #region History Detail Page

        [HttpPost]
        public ActionResult GetHistoryDataPartial(int HeaderId)
        {
            var objPBP002_Log = new PBP002_Log();
            objPBP002_Log = db.PBP002_Log.FirstOrDefault(x => x.Id == HeaderId);
            return PartialView("_GetPBPHistoryDataPartial", objPBP002_Log);
        }
         
        [HttpPost]
        public ActionResult GetPBPHistoryHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = param.sColumns.Split(',')[sortColumnIndex];
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(param.Headerid))
                {
                    whereCondition += " AND Id = " + param.Headerid;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                string[] columnName = { "RevNo", "[Description]", "ProcessLicensorDesc", "Status" };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                var lstResult = db.SP_PBP_GET_HISTORY_MAINTAIN_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                var data = (from uc in lstResult
                            select new[]
                            {
                               Convert.ToString(uc.LogId),
                               "R"+uc.RevNo,
                               uc.Description,
                               uc.ProcessLicensorDesc,
                               uc.Status
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        public UserRoleAccessDetails GetUserAccessRights()
        {
            UserRoleAccessDetails objUserRoleAccessDetails = new UserRoleAccessDetails();

            try
            {
                var role = (from a in db.ATH001
                            join b in db.ATH004 on a.Role equals b.Id
                            where a.Employee.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase)
                            select new { RoleId = a.Role, RoleDesc = b.Role }).ToList();

                if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PLNG1.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.PLNG1.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Approver.GetStringValue();
                }
                else if(role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PLNG2.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.PLNG2.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Approver.GetStringValue();
                }
                else if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.PLNG3.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Initiator.GetStringValue();
                }
                else
                {
                    objUserRoleAccessDetails.UserRole = "";
                    objUserRoleAccessDetails.UserDesignation = "";
                }
            }
            catch
            {

            }
            return objUserRoleAccessDetails;
        }

        [NonAction]
        public static string GenerateHidden(int rowId, string columnName, string columnValue = "", string className = "")
        {
            string htmlControl = "";

            string inputID = columnName + "" + rowId.ToString();
            string inputValue = columnValue;

            htmlControl = "<input type='hidden' id='" + inputID + "' value='" + inputValue + "' name='" + inputID + "' colname='" + columnName + "' class='" + className + "'/>";

            return htmlControl;
        }

        public class ProcesslicensorEnt
        {
            public string ProcessLicensor { get; set; }
            public int Id { get; set; }
        }
        public class ProcedureDtlEnt
        {
            public int DocNo { get; set; }
            public string Procedure { get; set; }
        }
    }
}