﻿using IEMQS.Areas.Authorization.Models;
using IEMQS.Areas.NDE.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.Authorization.Controllers
{
    public class AuthorizationMatrixController : clsBase
    {
        #region Utility
        public JsonResult GetAllBU(int? Id)
        {
            int BU = Convert.ToInt32(clsImplementationEnum.BLDNumber.BU.GetStringValue());

            var lstBUs = Manager.getBUsByID(BU);
            if (Id == 0)
            {
                List<string> userBU = Manager.GetUserwiseBU(objClsLoginInfo.UserName);
                lstBUs = lstBUs.Where(c => userBU.Contains(c.t_dimx)).ToList();
            }

            try
            {
                var items = (from li in lstBUs
                             select new
                             {
                                 id = li.t_dimx,
                                 text = li.t_desc,
                             }).ToList();

                return Json(items, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetAllLocation(int? Id)
        {
            int LOC = Convert.ToInt32(clsImplementationEnum.BLDNumber.LOC.GetStringValue());

            var lstLocations = Manager.getBUsByID(LOC);
            if (Id == 0)
            {
                List<string> userLoc = Manager.GetUserwiseLocation(objClsLoginInfo.UserName);
                lstLocations = lstLocations.Where(c => userLoc.Contains(c.t_dimx)).ToList();
            }

            try
            {
                var items = (from li in lstLocations
                             select new
                             {
                                 id = li.t_dimx,
                                 text = li.t_desc,
                             }).ToList();

                return Json(items, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        public ActionResult Index()
        {
            ViewBag.isAuthAdmin = (IsAuthAdmin(objClsLoginInfo.UserName) ? 1 : 0);
            return View();
        }

        private bool IsAuthAdmin(string UserPSNo)
        {
            var UserRoles = (from a in db.ATH001
                             join b in db.ATH004 on a.Role equals b.Id
                             where a.Employee.Equals(UserPSNo, StringComparison.OrdinalIgnoreCase)
                             select new { RoleId = a.Role, RoleDesc = b.Role }).ToList();

            bool isAdmin = false;
            if (UserRoles.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.ITA1.GetStringValue(), StringComparison.OrdinalIgnoreCase) || i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.ITA2.GetStringValue(), StringComparison.OrdinalIgnoreCase) || i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.ITA3.GetStringValue(), StringComparison.OrdinalIgnoreCase) || i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PMG1.GetStringValue(), StringComparison.OrdinalIgnoreCase) || i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PMG2.GetStringValue(), StringComparison.OrdinalIgnoreCase) || i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PMG3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
            {
                isAdmin = true;
            }
            return isAdmin;
        }

        [HttpPost]
        public ActionResult GetAuthorizationMatrixHtml(int Id, AuthorizationPopupType Type = AuthorizationPopupType.NewRecord)
        {
            int BU = Convert.ToInt32(clsImplementationEnum.BLDNumber.BU.GetStringValue());
            int LOC = Convert.ToInt32(clsImplementationEnum.BLDNumber.LOC.GetStringValue());
            int DEP = Convert.ToInt32(clsImplementationEnum.BLDNumber.DEP.GetStringValue());
            ATH001 objATH001 = new ATH001();

            if (Id > 0)
            {
                objATH001 = db.ATH001.Where(x => x.Id == Id).FirstOrDefault();

                var objAssignEmployees = (from a1 in db.ATH001
                                          join c3 in db.COM003 on a1.Employee equals c3.t_psno
                                          where a1.BU == objATH001.BU && a1.Location == objATH001.Location
                                           && a1.Contract == objATH001.Contract && a1.Project == objATH001.Project
                                          select new { Psno = a1.Employee, Name = a1.Employee + " - " + c3.t_name, Role = a1.Role }
                                     ).ToList();//.Select(i => new { Psno = i.Psno, Name = i.Name }).Distinct().ToList();
                ViewBag.AssignEmployees = objAssignEmployees.Select(i => new { Psno = i.Psno, Name = i.Name }).Distinct().ToList();
                ViewBag.AssignEmployeesRoles = objAssignEmployees.Select(i => new { Psno = i.Psno, Role = i.Role }).Distinct().ToList();
            }

            ViewBag.Type = Type;

            var lstBUs = Manager.getBUsByID(BU);
            ViewBag.BUs = new SelectList(lstBUs.Select(x => new { x.t_dimx, x.t_desc }).ToList(), "t_dimx", "t_desc");

            var lstLocs = Manager.getLocByID(LOC);
            ViewBag.Locs = new SelectList(lstLocs.Select(x => new { x.t_dimx, x.t_desc }).ToList(), "t_dimx", "t_desc");

            ViewBag.Roles = new SelectList(db.ATH004.OrderBy(x => x.Role).ToList(), "Id", "Role");

            var lstEmployee = (from a in db.COM003
                               where a.t_actv == 1
                               orderby a.t_psno, a.t_name
                               select new { empCode = a.t_psno, empDesc = a.t_psno + " - " + a.t_name }).ToList();

            ViewBag.Employee = new SelectList(lstEmployee, "empCode", "empDesc");
            ViewBag.isAuthAdmin = (IsAuthAdmin(objClsLoginInfo.UserName) ? 1 : 0);
            return PartialView("_AuthorizationMatrixHtmlPartial", objATH001);
        }

        [HttpPost]
        public JsonResult LoadAuthMatrixData(JQueryDataTableParamModel param)
        {
            try
            {
                IEnumerable<ATH001> _List = new List<ATH001>();
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                var totalRecords = new ObjectParameter("totalRecords", typeof(int));
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                Func<ATH001, string> orderingFunction = (tsk =>
                                        sortColumnIndex == 2 && isLocationSortable ? Convert.ToString(tsk.Location) : "");
                string strWhere = "1=1";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere = " (BU like '%" + param.sSearch
                        + "%' or Location like '%" + param.sSearch
                        + "%' or Contract like '%" + param.sSearch
                        + "%' or Project like '%" + param.sSearch
                        + "%' or Role like '%" + param.sSearch
                        + "%' or Employee like '%" + param.sSearch
                        + "%')";

                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                #region Sorting
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                #endregion

                var lstATH001 = db.SP_ATH_GET_ATH_DETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, totalRecords, strWhere
                                ).ToList();

                var data = (from uc in lstATH001
                            select new[]
                           {
                           Convert.ToString(uc.Id),
                           Convert.ToString(uc.BU),
                           Convert.ToString(uc.Location),
                           Convert.ToString(uc.Contract),
                           Convert.ToString(uc.Project),
                           Convert.ToString(uc.Role),
                           Convert.ToString(uc.Employee),
                           Convert.ToString(uc.Id)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = totalRecords.Value,
                    iTotalDisplayRecords = totalRecords.Value,
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public List<ATH001> GetAllAuthMatrixdata(string SearchParameter, int StartIndex, int EndIndex)
        {
            List<ATH001> List = db.ATH001.ToList();
            if (EndIndex == -1)
                return List.Skip(StartIndex).ToList();
            else
                return List.Skip(StartIndex).Take(EndIndex).ToList();
        }

        [HttpPost]
        public ActionResult SaveAuthMatrix(ATH001 ath001, FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var BuIds = fc["BU"].ToString().Split(',');
                var LocIds = fc["Location"].ToString().Split(',');
                //var Contract = fc["ddlContract"].ToString();
                //var Project = fc["ddlProject"].ToString();
                var Contract = ath001.Contract != null && ath001.Contract != string.Empty ? ath001.Contract : "None";
                var Project = ath001.Project != null && ath001.Project != string.Empty ? ath001.Project : "None";
                var arrProj = Project.Split(',');
                var flag = false;
                if (BuIds != null && LocIds != null)
                {
                    foreach (var buid in BuIds)
                    {
                        foreach (var locid in LocIds)
                        {
                            foreach (var proj in arrProj)
                            {
                                int TrIndex = 0;
                                while (!String.IsNullOrWhiteSpace(fc["hdnEmployee" + TrIndex]) && !String.IsNullOrWhiteSpace(fc["ddlRole" + TrIndex]))
                                {
                                    var EmployeeId = fc["hdnEmployee" + TrIndex];
                                    var Roles = fc["ddlRole" + TrIndex];
                                    foreach (var roleid in Roles.Split(',').Select(s => Convert.ToInt32(s)))
                                    {
                                        var data = db.ATH001.Where(x => x.BU == buid && x.Location == locid && x.Contract == Contract && x.Employee == EmployeeId && x.Project == Project && x.Role == roleid).FirstOrDefault();
                                        if (data == null)
                                        {
                                            flag = true;
                                            ATH001 objATH001 = new ATH001();
                                            objATH001.BU = buid;
                                            objATH001.Location = locid;
                                            //objATH001.Contract = fc["ddlContract"].ToString();
                                            //objATH001.Project = fc["ddlProject"].ToString();
                                            objATH001.Contract = Contract;
                                            objATH001.Project = proj;
                                            objATH001.Role = roleid;
                                            objATH001.Employee = EmployeeId;
                                            objATH001.CreatedBy = objClsLoginInfo.UserName;
                                            objATH001.CreatedOn = DateTime.Now;
                                            db.ATH001.Add(objATH001);
                                            db.SaveChanges();
                                            objResponseMsg.Key = true;
                                            objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();

                                            #region Check for user access request available in ATH020
                                            string RequestedStatus = clsImplementationEnum.AccessRequest.Requested.GetStringValue();
                                            ATH020 objATH020 = db.ATH020.FirstOrDefault(c => c.BU == buid && c.Location == locid && c.PSNo == EmployeeId && c.Status == RequestedStatus);
                                            if (objATH020 != null)
                                            {
                                                if (objATH020.RoleId == roleid)
                                                {
                                                    objATH020.Status = clsImplementationEnum.AccessRequest.Approved.GetStringValue();
                                                }
                                                else
                                                {
                                                    objATH020.Status = clsImplementationEnum.AccessRequest.Rejected.GetStringValue();
                                                }
                                                objATH020.EditedBy = objClsLoginInfo.UserName;
                                                objATH020.EditedOn = DateTime.Now;
                                                objATH020.RefId = objATH001.Id;
                                                db.SaveChanges();

                                                #region Send Notification
                                                ATH004 objATH004 = db.ATH004.FirstOrDefault(c => c.Id == objATH020.RoleId.Value);
                                                if (objATH020.Status == clsImplementationEnum.AccessRequest.Approved.GetStringValue())
                                                {
                                                    (new clsManager()).SendNotificationByUserPSNumber(objATH004.Role, objATH020.PSNo, "Your request for role " + objATH004.Role + " has been approved", clsImplementationEnum.NotificationType.Information.GetStringValue());
                                                }
                                                else
                                                {
                                                    (new clsManager()).SendNotificationByUserPSNumber(objATH004.Role, objATH020.PSNo, "Your request for role " + objATH004.Role + " has been rejected", clsImplementationEnum.NotificationType.Information.GetStringValue());
                                                }
                                                #endregion

                                            }
                                            #endregion

                                        }
                                        //else
                                        //{
                                        //    ATH001 objATH001 = db.ATH001.Where(x => x.Id == ath001.Id).FirstOrDefault();
                                        //    objATH001.BU = buid;
                                        //    objATH001.Location = locid;
                                        //    objATH001.Contract = fc["ddlContract"].ToString();
                                        //    objATH001.Project = fc["ddlProject"].ToString();
                                        //    objATH001.Role =roleid;
                                        //    objATH001.Employee = EmployeeId;
                                        //    objATH001.EditedBy = objClsLoginInfo.UserName;
                                        //    objATH001.EditedOn = DateTime.Now;
                                        //    db.SaveChanges();
                                        //    objResponseMsg.Key = true;
                                        //    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                                        //}
                                    }
                                    TrIndex++;
                                }
                            }
                        }
                    }
                    if (!flag)
                    {
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Comulative;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteAuthMatrix(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                Manager.MaintainATH001Log(Id);
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Delete.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetContractByBU(string BUId, int? isAdmin)
        {
            if (!string.IsNullOrWhiteSpace(BUId))
            {
                List<string> listBU = BUId.Split(',').ToList();
                List<string> userContract = new List<string>();
                if(isAdmin == 0)
                {
                    userContract = Manager.GetContractByUser(objClsLoginInfo.UserName);
                }

                var lstContract = (from cm004 in db.COM004
                                   join cm008 in db.COM008 on cm004.t_cono equals cm008.t_cono
                                   where listBU.Contains(cm008.t_csbu) && (isAdmin == 0 ? (userContract.Contains(cm004.t_cono)) : true)
                                   orderby cm004.t_cono
                                   select new { cm004.t_cono, cm004.t_desc }).Distinct().ToList();
                return Json(lstContract, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetProjectByContract(string contIds)
        {
            if (!string.IsNullOrWhiteSpace(contIds))
            {
                string[] arrcontIds = contIds.Split(',');
                var lstProject = (from cm001 in db.COM001
                                  join cm005 in db.COM005 on cm001.t_cprj equals cm005.t_sprj
                                  where arrcontIds.Contains(cm005.t_cono)
                                  orderby cm001.t_cprj
                                  select new { cm001.t_cprj, cm001.t_dsca }).Distinct().ToList();
                return Json(lstProject, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }
        public class departmentUser
        {
            public string t_depc { get; set; }
            public string t_name { get; set; }
        }
        [HttpPost]
        public JsonResult GetUserDepartmentWise(string search, string department)
        {
            try
            {
                string query = "SELECT * FROM FN_GET_DEPARTMENTWISE_USER('" + department + "','')";
                var items = db.Database.SqlQuery<departmentUser>(query).ToList();
                return Json(items, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult GetContractByProject(string projIds)
        {
            if (!string.IsNullOrWhiteSpace(projIds))
            {
                string[] arrprojIds = projIds.Split(',');
                var lstContract = (from cm001 in db.COM001
                                   join cm005 in db.COM005 on cm001.t_cprj equals cm005.t_sprj
                                   where projIds.Contains(cm005.t_sprj)
                                   select new { cm001.t_cprj, cm005.t_cono }).Distinct().ToList();
                return Json(lstContract, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        ////created by Nikita (24/07/2017)
        ////description :for NDE Quality project autocomplete
        [HttpPost]
        public ActionResult GetContractBySearch(string term, string BUId, int? isAdmin)
        {
            if (!string.IsNullOrWhiteSpace(BUId))
            {
                List<string> listBU = BUId.Split(',').ToList();
                List<string> userContract = new List<string>();
                if (isAdmin == 0)
                {
                    userContract = Manager.GetContractByUser(objClsLoginInfo.UserName);
                }

                var lstContract = (from cm004 in db.COM004
                                   join cm008 in db.COM008 on cm004.t_cono equals cm008.t_cono
                                   where listBU.Contains(cm008.t_csbu) && (cm004.t_cono.Contains(term) || cm004.t_desc.Contains(term)) && (isAdmin == 0 ? (userContract.Contains(cm004.t_cono)) : true)
                                   orderby cm004.t_cono
                                   select new { cm004.t_cono, cm004.t_desc }).Distinct().ToList();

                if (isAdmin == 1)
                    lstContract.Add(new { t_cono = "ALL", t_desc = "ALL" });

                return Json(lstContract, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetProjectByContractSearch(string term, string contIds, int? isAdmin)
        {
            List<ddlValue> lstProject = new List<ddlValue>();
            if (!string.IsNullOrWhiteSpace(contIds))
            {
                string[] arrcontIds = contIds.Split(',');
                List<string> userProjects = new List<string>();
                if (isAdmin == 0)
                {
                    userProjects = Manager.getUserProjects(objClsLoginInfo.UserName).Select(s=>s.projectCode).ToList();
                }

                if (!arrcontIds.Contains("ALL"))
                {
                    lstProject = (from cm001 in db.COM001
                                  join cm005 in db.COM005 on cm001.t_cprj equals cm005.t_sprj
                                  where arrcontIds.Contains(cm005.t_cono) && (cm001.t_cprj.Contains(term) || cm001.t_dsca.Contains(term)) && (isAdmin == 0 ? (userProjects.Contains(cm001.t_cprj)) : true)
                                  orderby cm001.t_cprj
                                  select new ddlValue { id = cm001.t_cprj, text = cm001.t_cprj + "-" + cm001.t_dsca, t_cprj = cm001.t_cprj, t_dsca = cm001.t_dsca }).Distinct().ToList();
                }
                if (isAdmin == 1)
                {
                    lstProject.Add(new ddlValue { id = "ALL", text = "ALL" + "-" + "ALL", t_cprj = "ALL", t_dsca = "ALL" });
                }
                return Json(lstProject, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetRoleSearch(string empName)
        {
            var objResponseMsg = new clsHelper.ResponceMsgWithObject();
            objResponseMsg.Key = true;
            var AssignedRoles = db.ATH001.Where(x => x.Employee == empName).Select(x => x.Role).ToList();
            var lstRoles = new List<BULocWiseCategoryModel>();
            if (AssignedRoles.Count > 0)
            {
                var ApplicableRoleList = db.ATH009.Where(x => AssignedRoles.Contains(x.Role)).Select(x => x.ApplicableRole).ToList();
                //To allow current role as well in selection for other BU Location
                ApplicableRoleList = AssignedRoles.Union(ApplicableRoleList).ToList();
                if (ApplicableRoleList.Count > 0)
                {
                    lstRoles = (from roles in db.ATH004
                                where ApplicableRoleList.Contains(roles.Id)
                                orderby roles.Role
                                select new BULocWiseCategoryModel() { CatDesc = roles.Role + " - " + roles.Description, CatID = roles.Id.ToString() }).Distinct().ToList();

                }
                else
                {
                    //var lstRoles = (from roles in db.ATH004
                    //                orderby roles.Role
                    //                select new BULocWiseCategoryModel() { CatDesc = roles.Role + " - " + roles.Description, CatID = roles.Id.ToString() }).Distinct().ToList();
                    lstRoles = null;
                }
            }
            else
            {
                var AssignedRoles2 = db.ATH015.Where(x => x.Employee == empName && x.Role != null && x.IsActive == true).Select(x => x.Role.Value).ToList();
                var ApplicableRoleList = db.ATH009.Where(x => AssignedRoles2.Contains(x.Role)).Select(x => x.ApplicableRole).ToList();

                ApplicableRoleList = AssignedRoles2.Union(ApplicableRoleList).ToList();
                if (ApplicableRoleList.Count > 0)
                {
                    lstRoles = (from roles in db.ATH004
                                where ApplicableRoleList.Contains(roles.Id)
                                orderby roles.Role
                                select new BULocWiseCategoryModel() { CatDesc = roles.Role + " - " + roles.Description, CatID = roles.Id.ToString() }).Distinct().ToList();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "ISO Designation is not maintained for selected employee.";
                }

            }
            objResponseMsg.data = lstRoles;
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetEmployeeSearch(string term = "")
        {
            dynamic lstEmployee;

            if (!string.IsNullOrWhiteSpace(term))
            {
                lstEmployee = (from a in db.COM003
                               where ((a.t_psno.Contains(term) || a.t_name.Contains(term)) && a.t_actv == 1)
                               orderby a.t_psno, a.t_name
                               select new { empCode = a.t_psno, empDesc = a.t_psno + " - " + a.t_name }).Distinct().ToList();
            }
            else
            {
                lstEmployee = (from a in db.COM003
                               where a.t_actv == 1
                               orderby a.t_psno, a.t_name
                               select new { empCode = a.t_psno, empDesc = a.t_psno + " - " + a.t_name }).Distinct().Take(10).ToList();
            }
            return Json(lstEmployee, JsonRequestBehavior.AllowGet);

        }

        public ActionResult EmployeeWiseRoleDetails()
        {
            return PartialView("_EmployeeWiseRoleDetailPartial");
        }

        [HttpPost]
        public JsonResult LoadEmployeeWiseRoleMatrixData(JQueryDataTableParamModel param)
        {
            try
            {
                IEnumerable<ATH001> _List = new List<ATH001>();
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                var totalRecords = new ObjectParameter("totalRecords", typeof(int));
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = "1=1";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere = " (BU like '%" + param.sSearch
                        + "%' or Location like '%" + param.sSearch
                        + "%' or EmpName like '%" + param.sSearch
                        + "%' or Roles like '%" + param.sSearch
                        + "%')";
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }


                #region Sorting
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstResult = db.SP_ATH_GET_EMPLOYEE_WISE_ROLE_DETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                           Convert.ToString(uc.BU),
                           Convert.ToString(uc.Location),
                           Convert.ToString(uc.EmpName),
                           Convert.ToString(uc.Roles)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = lstResult != null && lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0,
                    iTotalDisplayRecords = lstResult != null && lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0,
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        #region -- Created By Chandrashekhar (11/08/2018) 
        //[SessionExpireFilter]
        //[AllowAnonymous]
        //[UserPermissions]
        public ActionResult EmployeeRoles()
        {
            return View();
        }

        public ActionResult LoadEmpRoleDataTable(JQueryDataTableParamModel param)
        {
            try
            {
                int HeaderId = param.Headerid != "" ? Convert.ToInt32(param.Headerid) : 0;
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = " 1=1 ";

                #region Datatable Sorting 

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                string _urlform = string.Empty;
                string indextype = param.Department;

                #endregion

                string[] columnName = { "Employee", "Role", "CreatedBy", "CreatedOn", "EditedBy", "EditedOn" };

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(param.SearchFilter))
                        whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                var lst = db.SP_ATH_GET_EMPLOYEE_ROLE_MASTER(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lst.Select(i => i.TotalCount).FirstOrDefault();

                int newRecordId = 0;
                var newRecord = new[] {
                                    Helper.HTMLAutoComplete(newRecordId,"txtEmployee","","",false,"","Employee")+""+Helper.GenerateHidden(newRecordId,"Employee"),
                                    Helper.HTMLAutoComplete(newRecordId,"txtRole","","",false,"","Role")+""+Helper.GenerateHidden(newRecordId,"Role"),
                                    "",
                                    "",
                                    "",
                                    "",
                                    Helper.GenerateGridButton(newRecordId, "Add", "Add Rocord", "fa fa-plus", "SaveRecord(0);"),
                                };

                var res = (from h in lst
                           select new[] {
                                    Convert.ToString(h.Employee),
                                    Convert.ToString(h.Role),
                                    Convert.ToString(h.CreatedBy),
                                    Convert.ToDateTime(h.CreatedOn).ToString("dd/MM/yyyy"),
                                    Convert.ToString(h.EditedBy),
                                    Convert.ToString(Convert.ToDateTime(h.EditedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "":Convert.ToDateTime(h.EditedOn).ToString("dd/MM/yyyy")),
                                   (h.IsActive!=null && Convert.ToBoolean(h.IsActive)) ?GeneratePartButtonNew(h.Id, "De-Activate", "De-Activate", "padding btn red", "UpdateRecord("+h.Id+",false);"):GeneratePartButtonNew(h.Id, "Activate", "Activate", "padding btn green", "UpdateRecord(" + h.Id + ",true);")
                                    }).ToList();

                res.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    whereCondition = whereCondition,
                    strSortOrder = strSortOrder,
                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [NonAction]
        public static string GeneratePartButtonNew(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";


            htmlControl = "<a id='" + inputID + "' name='" + inputID + "' class='" + className + "' " + onClickEvent + " > " + buttonName + "</a>";

            //htmlControl = "<i  data-modal='' id='" + inputID + "' name='" + inputID + "' style='cursor:Pointer;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";

            return htmlControl;
        }

        [HttpPost]
        public ActionResult GetRoleMaster(string term = "")
        {
            var lstEmployee = (from a in db.ATH004
                               where (term == "" || a.Role.Trim().ToLower().Contains(term.Trim().ToLower()))
                               orderby a.Role, a.Description
                               select new { empCode = a.Id, empDesc = a.Role }).ToList();
            return Json(lstEmployee, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveEmployeeRoleMaster(FormCollection fc)
        {
            string Employee0 = fc["Employee0"].ToString();

            int Role0 = 0;
            if (fc["Role0"] != null && fc["Role0"].ToString() != "")
                Role0 = Convert.ToInt32(fc["Role0"]);

            bool? IsActive0 = null;
            if (fc["IsActive"] != null && fc["IsActive"].ToString() != "")
                IsActive0 = Convert.ToBoolean(fc["IsActive"]);

            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (!db.ATH015.Any(u => u.Employee == Employee0 && u.Role == Role0))
                {
                    ATH015 objATH015 = new ATH015();
                    objATH015.Employee = Employee0;
                    objATH015.Role = Role0;
                    objATH015.IsActive = IsActive0;
                    objATH015.CreatedBy = objClsLoginInfo.UserName;
                    objATH015.CreatedOn = DateTime.Now;
                    db.ATH015.Add(objATH015);
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateRecord(FormCollection fc)
        {
            int Id = Convert.ToInt32(fc["Id"].ToString());
            bool? IsActive = Convert.ToBoolean(fc["IsActive"]);

            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                var objATH015 = db.ATH015.Where(u => u.Id == Id).FirstOrDefault();
                objATH015.IsActive = IsActive;
                objATH015.EditedBy = objClsLoginInfo.UserName;
                objATH015.EditedOn = DateTime.Now;
                db.SaveChanges();

                if (Convert.ToBoolean(IsActive))
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Active.ToString();
                }
                else
                {
                    var ATH001Ids = db.ATH001.Where(u => u.Employee.Trim() == objATH015.Employee.Trim() && u.Role == objATH015.Role).Select(s => s.Id).ToList();
                    Manager.MaintainATH001Log(ATH001Ids);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Inactive.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        #endregion

        #region Export Excell
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var totalRecords = new ObjectParameter("totalRecords", typeof(int));
                    var lst = db.SP_ATH_GET_ATH_DETAILS(1, int.MaxValue, strSortOrder, totalRecords, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      BU = li.BU,
                                      Location = li.Location,
                                      Contract = li.Contract,
                                      Project = li.Project,
                                      Role = li.Role,
                                      Employee = li.Employee,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.EmployeeWiseRoles.GetStringValue())
                {
                    var lst = db.SP_ATH_GET_EMPLOYEE_WISE_ROLE_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      BU = li.BU,
                                      Location = li.Location,
                                      EmpName = li.EmpName,
                                      Roles = li.Roles
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {
                    var lst = db.SP_ATH_GET_EMPLOYEE_ROLE_MASTER(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Employee = li.Employee,
                                      Role = li.Role,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn,
                                      Status = (Convert.ToString(li.IsActive) != "True" ? "De -Activated" : "Activated")
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        [HttpPost]
        public ActionResult GetEmployeeUpdate(string search)
        {
            try
            {
                string ITARole = clsImplementationEnum.UserRoleName.ITA1.GetStringValue().ToLower();
                var GetRoleId = db.ATH004.Where(x => x.Role.ToLower() == ITARole).FirstOrDefault();
                if (!string.IsNullOrEmpty(search))
                {
                    var GetRoleWiseEmployee = (from a1 in db.ATH001 join a2 in db.COM003 on a1.Employee equals a2.t_psno
                                               where ((a2.t_psno.Contains(search) || a2.t_name.Contains(search)))
                                               && a1.Role == GetRoleId.Id
                                               select new { empCode = a2.t_psno, empDesc = a2.t_psno + " - " + a2.t_name }).Distinct().Take(10).ToList();
                    return Json(GetRoleWiseEmployee, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    var GetRoleWiseEmployee = (from a1 in db.ATH001 join a2 in db.COM003 on a1.Employee equals a2.t_psno where  a1.Role == GetRoleId.Id select new { empCode = a2.t_psno, empDesc = a2.t_psno + " - " + a2.t_name }).Distinct().Take(10).ToList();
                    return Json(GetRoleWiseEmployee, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception)
            {
                throw;
            }
            
        }

        [HttpPost]
        public ActionResult GetDepartUpdate(string search)
        {
            try
            {
                if (!string.IsNullOrEmpty(search))
                {
                    var GetDepartment = (from roles in db.FN_GET_LOCATIONWISE_DEPARTMENT("HZW", "")
                                 where ((roles.t_dimx.Contains(search) || roles.t_desc.Contains(search)))
                                 select new { text = roles.t_dimx + " - " + roles.t_desc, Id = roles.t_dimx.ToString() }).Distinct().Take(10).ToList();

                    return Json(GetDepartment, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var GetDepartment = (from roles in db.FN_GET_LOCATIONWISE_DEPARTMENT("HZW", "")
                                         select new { text = roles.t_dimx + " - " + roles.t_desc, Id = roles.t_dimx.ToString() }).Distinct().Take(10).ToList();
                    return Json(GetDepartment, JsonRequestBehavior.AllowGet);
                }
               
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpPost]
        public ActionResult GetRoles(string search)
        {
            try
            {
                if (!string.IsNullOrEmpty(search))
                {
                    var GetRoles = (from roles in db.ATH004
                                    where ((roles.Role.Contains(search) || roles.Description.Contains(search)))
                                    select new { text = roles.Role + " - " + roles.Description, Id = roles.Id.ToString() }).Distinct().Take(10).ToList();
                    return Json(GetRoles, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var GetRoles = (from roles in db.ATH004
                                    select new { text = roles.Role + " - " + roles.Description, Id = roles.Id.ToString() }).Distinct().Take(10).ToList();
                    return Json(GetRoles, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpPost]
        public ActionResult UpdateDepartmentRoles(string Employee, string Department, string Role)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int Status = 0;
                if (string.IsNullOrEmpty(Employee) && (string.IsNullOrEmpty(Department) || string.IsNullOrEmpty(Role)))
                    Status = 1;

                if (Status == 0)
                {
                    string ITARole = clsImplementationEnum.UserRoleName.ITA1.GetStringValue().ToLower();
                    var GetRoleId = db.ATH004.Where(x => x.Role.ToLower() == ITARole).FirstOrDefault();

                    if (!string.IsNullOrEmpty(Department))
                    {
                        var objComm003 = db.COM003.Where(x => x.t_psno == Employee).FirstOrDefault();
                        if (objComm003 != null)
                            objComm003.t_depc = Department;

                        db.SaveChanges();
                    }
                    var lstExceptITA = db.ATH001.Where(x => x.Role != GetRoleId.Id && x.Employee == Employee).ToList();
                    if (lstExceptITA.Count() > 0)
                    {
                        List<ATH001_Log> lstLog = new List<ATH001_Log>();

                        foreach (var item in lstExceptITA)
                        {
                            ATH001_Log objATH001 = new ATH001_Log();
                            objATH001.RefId = item.Id;
                            objATH001.BU = item.BU;
                            objATH001.Location = item.Location;
                            objATH001.Contract = item.Contract;
                            objATH001.Project = item.Project;
                            objATH001.Role = item.Role;
                            objATH001.Employee = item.Employee;
                            objATH001.CreatedBy = item.CreatedBy;
                            objATH001.CreatedOn = item.CreatedOn;
                            objATH001.EditedBy = item.EditedBy;
                            objATH001.EditedOn = item.EditedOn;

                            lstLog.Add(objATH001);
                        }
                        db.ATH001_Log.AddRange(lstLog);
                        db.SaveChanges();

                        db.ATH001.RemoveRange(lstExceptITA);
                        db.SaveChanges();
                    }

                    if (!string.IsNullOrEmpty(Role))
                    {
                        ATH001 ATH001 = new ATH001();
                        ATH001.BU = "ALL";
                        ATH001.Location = "HZW";
                        ATH001.Contract = "ALL";
                        ATH001.Project = "ALL";
                        ATH001.Role = Convert.ToInt32(Role);
                        ATH001.Employee = Employee;
                        ATH001.CreatedBy = objClsLoginInfo.UserName;
                        ATH001.CreatedOn = DateTime.Now;
                        db.ATH001.Add(ATH001);
                        db.SaveChanges();
                    }

                    objResponseMsg.Key = true;
                }
                else
                {
                    objResponseMsg.Key = false;
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

    }
}