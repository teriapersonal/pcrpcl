﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.Authorization.Controllers
{
    public class EquipController : clsBase
    {
        // GET: Authorization/Equipment
        /// <summary>
        /// created by nikita vibhandik : 7/7/2017
        /// description :CRUD of equipment master
        /// </summary>
        /// <returns></returns>
        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]

        public ActionResult Index()
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("NDE019");
            return View();
        }

        [HttpPost]
        public ActionResult GetDataHtml(int Id)
        {
            NDE019 objNDE019 = new NDE019();
            if (Id > 0)
            {
                objNDE019 = db.NDE019.Where(x => x.Id == Id).FirstOrDefault();
                ViewBag.Action = "editmode";
            }
            var user = objClsLoginInfo.UserName;
            string location = objClsLoginInfo.Location;
          //  string[] location = db.ATH001.Where(i => i.Employee == user).Select(i => i.Location).ToArray();
            var lstLocation = (from a in db.COM002
                               where a.t_dtyp == 1 && a.t_dimx == location
                               select new { a.t_dimx, DescLocation = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();
            ViewBag.location = location;
            objNDE019.Location = lstLocation.DescLocation;
            return PartialView("_GetEquipmentHtmlPartial", objNDE019);
        }

        [HttpPost]
        public JsonResult checkExist(string Location,string TestInstrument,int Id = 0)
        {
            bool flag = true;

           List<NDE019> lstNDE019 = db.NDE019.Where(x => x.Location.Trim() == Location.Trim() && x.TestInstrument.Trim() == TestInstrument.Trim() && x.Id != Id).ToList();
            if (lstNDE019.Any())
            {
                flag = false;
            }
            return Json(flag);
        }

        [HttpPost]
        public ActionResult SaveEquipment(FormCollection fc, NDE019 nde019)
        {
            NDE019 objNDE019 = new NDE019();
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (nde019.Id > 0)
                {
                    objNDE019 = db.NDE019.Where(x => x.Id == nde019.Id).FirstOrDefault();
                    objNDE019.Location = fc["Location"].Split('-')[0];
                    objNDE019.TestInstrument = nde019.TestInstrument;
                    objNDE019.InstrumentSrNo = nde019.InstrumentSrNo;
                    objNDE019.Make = nde019.Make;
                    objNDE019.Model = nde019.Model;
                    objNDE019.EditedBy = objClsLoginInfo.UserName;
                    objNDE019.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.ProbeMessages.Update.ToString();
                }
                else
                {
                    objNDE019.Location = fc["Location"].Split('-')[0];
                    objNDE019.TestInstrument = nde019.TestInstrument;
                    objNDE019.InstrumentSrNo = nde019.InstrumentSrNo;
                    objNDE019.Make = nde019.Make;
                    objNDE019.Model = nde019.Model;
                    objNDE019.CreatedBy = objClsLoginInfo.UserName;
                    objNDE019.CreatedOn = DateTime.Now;
                    db.NDE019.Add(objNDE019);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.ProbeMessages.Insert.ToString();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteEquipment(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                NDE019 objNDE019 = db.NDE019.Where(x => x.Id == Id).FirstOrDefault();
                db.NDE019.Remove(objNDE019);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Delete.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult LoadEquipmentData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                strWhere += "where 1=1";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    //strWhere += " and (Location like '%" + param.sSearch + "%' or InstrumentSrNo like '%" + param.sSearch + "%' or Make like '%" + param.sSearch + "%' or TestInstrument like '%" + param.sSearch + "%' or Model like '%" + param.sSearch + "%')";
                    string[] arrayLikeCon = {
                                                "Location",
                                                "InstrumentSrNo",
                                                "Make",
                                                "TestInstrument",
                                                "Model",
                                                "NDE.CreatedBy+'-'+c32.t_namb"
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_EQUIPMENT_DETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                           Convert.ToString(uc.Id),
                           Convert.ToString(uc.Location),
                           Convert.ToString(uc.InstrumentSrNo),
                            Convert.ToString(uc.Make),
                           Convert.ToString(uc.Model),
                           Convert.ToString(uc.TestInstrument),
                           Convert.ToString(uc.CreatedBy),
                               Convert.ToString(uc.Id)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "", int? HeaderId = 0, int? latestRv = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_EQUIPMENT_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Location = li.Location,
                                      InstrumentSrNo = li.InstrumentSrNo,
                                      Make = li.Make,
                                      Model = li.Model,
                                      TestInstrument = li.TestInstrument,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }



                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

    }
}
