﻿using IEMQS.Models;
using IEMQSImplementation;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.Authorization.Controllers
{
    public class SearchUnitController : clsBase
    {
        #region Utility
        public bool CheckExcellFormat(ExcelPackage package)
        {
            bool isValidFormat = true;
            ExcelWorksheet excelWorksheet = package.Workbook.Worksheets.First();
            if (excelWorksheet.Cells["A1"].Text.ToUpper() != "SEARCHUNIT")
            {
                isValidFormat = false;
            }

            if (excelWorksheet.Cells["B1"].Text.ToUpper() != "FREQUENCY")
            {
                isValidFormat = false;
            }

            if (excelWorksheet.Cells["C1"].Text.ToUpper() != "ELEMENTSIZE")
            {
                isValidFormat = false;
            }
            if (excelWorksheet.Cells["D1"].Text.ToUpper() != "BEAMANGLE")
            {
                isValidFormat = false;
            }
            if (excelWorksheet.Cells["E1"].Text.ToUpper() != "SEARCHUNITMAKE")
            {
                isValidFormat = false;
            }

            return isValidFormat;
        }
        #endregion
        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult Index()
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("NDE017");
            return View();
        }

        [HttpPost]
        public ActionResult GetDataHtml(int Id)
        {
            NDE017 objNDE017 = new NDE017();
            if (Id > 0)
            {
                objNDE017 = db.NDE017.Where(x => x.Id == Id).FirstOrDefault();
            }
            return PartialView("_GetSearchUnitHtmlPartial", objNDE017);
        }

        [HttpPost]
        public ActionResult SaveSearchUnit(FormCollection fc, NDE017 nde017)
        {
            NDE017 objNDE017 = new NDE017();
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (nde017.Id > 0)
                {
                    objNDE017 = db.NDE017.Where(x => x.Id == nde017.Id).FirstOrDefault();
                    objNDE017.SearchUnit = nde017.SearchUnit;
                    objNDE017.ElementSize = nde017.ElementSize;
                    objNDE017.Frequency = Convert.ToDecimal(fc["Frequency"], CultureInfo.InvariantCulture); 
                    objNDE017.BeamAngle = nde017.BeamAngle;
                    objNDE017.SearchUnitMake = nde017.SearchUnitMake;
                    objNDE017.EditedBy = objClsLoginInfo.UserName;
                    objNDE017.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.ProbeMessages.Update.ToString();
                }
                else
                {
                    objNDE017.SearchUnit = nde017.SearchUnit;
                    objNDE017.ElementSize = nde017.ElementSize;
                    objNDE017.Frequency = Convert.ToDecimal(fc["Frequency"], CultureInfo.InvariantCulture); 
                    objNDE017.BeamAngle = nde017.BeamAngle;
                    objNDE017.SearchUnitMake = nde017.SearchUnitMake;
                    objNDE017.CreatedBy = objClsLoginInfo.UserName;
                    objNDE017.CreatedOn = DateTime.Now;
                    db.NDE017.Add(objNDE017);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.ProbeMessages.Insert.ToString();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteSearchUnit(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                NDE017 objNDE017 = db.NDE017.Where(x => x.Id == Id).FirstOrDefault();
                db.NDE017.Remove(objNDE017);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Delete.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult LoadSearchUnitData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                strWhere += "where 1=1";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "SearchUnit",
                                                "Frequency",
                                                "BeamAngle",
                                                "ElementSize",
                                                "SearchUnitMake",
                                                "NDE.CreatedBy+'-'+c32.t_namb"
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstResult = db.SP_SEARCH_UNIT_GETDETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                           Convert.ToString(uc.Id),
                           Convert.ToString(uc.SearchUnit),
                           Convert.ToString(uc.Frequency),
                            Convert.ToString(uc.ElementSize),
                           Convert.ToString(uc.BeamAngle),
                            Convert.ToString(uc.SearchUnitMake),
                           Convert.ToString(uc.CreatedBy),
                               Convert.ToString(uc.Id)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "", int? HeaderId = 0, int? latestRv = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_SEARCH_UNIT_GETDETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {

                                      SearchUnit = li.SearchUnit,
                                      Frequency = li.Frequency,
                                      ElementSize = li.ElementSize,
                                      BeamAngle = li.BeamAngle,
                                      SearchUnitMake = li.SearchUnitMake,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }



                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        #region Import Excel
        public FileResult DownloadSample()
        {
            byte[] fileBytes = System.IO.File.ReadAllBytes(Server.MapPath("~/Areas/Authorization/Views/SearchUnit/SearchUnitTemplate.xlsx"));
            string fileName = " UploadTemplate.xlsx";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }
        public ActionResult ReadExcel()
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            if (Request.Files.Count == 0)
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please select a file first!";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            HttpPostedFileBase upload = Request.Files[0];
            if (Path.GetExtension(upload.FileName) == ".xlsx" || Path.GetExtension(upload.FileName) == ".xls")
            {
                ExcelPackage package = new ExcelPackage(upload.InputStream);
                //validation for excell format code by Ajay Chauhan on 30-3-2018
                if (!CheckExcellFormat(package))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Excell format is not valid";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                DataTable dt = Manager.ExcelToDataTable(package);
                return validateAndSave(dt);
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please upload valid excel file!";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }

        }
        //validate mandatory fields and date from excel
        public ActionResult validateAndSave(DataTable dt)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            bool hasError = false;
            if (!dt.Columns.Contains("Error"))
            {
                dt.Columns.Add("Error", typeof(string));
            }
            foreach (DataRow item in dt.Rows)
            {
                string psno = item.Field<string>(3);
                List<string> errors = new List<string>();

                string SearchUnit = item.Field<string>(0);
                string Frequency = item.Field<string>(1);
                string ElementSize = item.Field<string>(2);
                string BeamAngle = item.Field<string>(3);
                string SearchUnitMake = item.Field<string>(4);

                if (string.IsNullOrWhiteSpace(SearchUnit) || string.IsNullOrWhiteSpace(Frequency) ||
                    string.IsNullOrWhiteSpace(ElementSize) || string.IsNullOrWhiteSpace(BeamAngle) ||
                    string.IsNullOrWhiteSpace(SearchUnitMake) )
                {
                    errors.Add("Mandatory fields are required"); hasError = true;
                }
                if (!string.IsNullOrWhiteSpace(SearchUnit) && SearchUnit.Length > 20)
                {
                    errors.Add("Search Unit/Wedge Angle have maximum limit of 20 characters"); hasError = true;
                }
                if (!string.IsNullOrWhiteSpace(Frequency) && Frequency.Length > 8)
                {
                    errors.Add("Frequency(MHz) have maximum limit of 8 characters"); hasError = true;                    
                }
                if (!string.IsNullOrWhiteSpace(ElementSize) && ElementSize.Length > 18)
                {
                    errors.Add("Element Size(mm) have maximum limit of 18 characters"); hasError = true;
                }
                if (!string.IsNullOrWhiteSpace(BeamAngle) && BeamAngle.Length > 5)
                {
                    errors.Add("Beam Angle(Deg.) have maximum limit of 5 characters"); hasError = true;
                }
                if (!string.IsNullOrWhiteSpace(SearchUnitMake) && SearchUnitMake.Length > 100)
                {
                    errors.Add("Search Unit Make have maximum limit of 100 characters"); hasError = true;
                }

                string depc = item.Field<string>(2);
                if (!string.IsNullOrWhiteSpace(Frequency) && !(IsDecimalNumeric(Frequency)))
                {
                    errors.Add("Frequency(MHz) must be numeric decimal"); hasError = true;
                }               

                if (errors.Count > 0)
                {
                    item["Error"] = string.Join(",", errors.ToArray());
                }
            }
            if (!hasError)
            {
                List<NDE017> listNDE017 = new List<NDE017>();
                foreach (DataRow item in dt.Rows)
                {
                    listNDE017.Add(new NDE017()
                    {
                        SearchUnit = item.Field<string>(0),
                        Frequency = Convert.ToDecimal(item.Field<string>(1)),
                        ElementSize =(item.Field<string>(2)),
                        BeamAngle = (item.Field<string>(3)),
                        SearchUnitMake = (item.Field<string>(4)),
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    }
                   );
                }
                if (listNDE017.Count > 0)
                {
                    db.NDE017.AddRange(listNDE017);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Search Unit Upload Successfully!";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return ExportToExcel(dt);
            }
        }
        //insert column wise data of excel into database
        public ActionResult ExportToExcel(DataTable dt)
        {
            clsHelper.ResponceMsgWithFileName objResponseMsg = new clsHelper.ResponceMsgWithFileName();
            try
            {
                MemoryStream ms = new MemoryStream();
                using (FileStream fs = System.IO.File.OpenRead(Server.MapPath("~/Areas/Authorization/Views/SearchUnit/SearchUnitErrorList.xlsx")))
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(fs))
                    {
                        ExcelWorkbook excelWorkBook = excelPackage.Workbook;
                        ExcelWorksheet excelWorksheet = excelWorkBook.Worksheets.First();
                        int i = 2;
                        foreach (DataRow item in dt.Rows)
                        {
                            excelWorksheet.Cells[i, 1].Value = item.Field<string>(0);
                            excelWorksheet.Cells[i, 2].Value = item.Field<string>(1);
                            excelWorksheet.Cells[i, 3].Value = item.Field<string>(2);
                            excelWorksheet.Cells[i, 4].Value = item.Field<string>(3);
                            excelWorksheet.Cells[i, 5].Value = item.Field<string>(4);
                            excelWorksheet.Cells[i, 6].Value = item.Field<string>(5);
                            i++;
                        }
                        excelPackage.SaveAs(ms);
                    }
                }

                ms.Position = 0;
                              
                string fileName;
                string excelFilePath = Helper.ExcelFilePath("ErrorList", out fileName);
                Byte[] bin = ms.ToArray();
                System.IO.File.WriteAllBytes(excelFilePath, bin);

                objResponseMsg.Key = false;
                objResponseMsg.FileName = fileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                
            }
            catch (Exception)
            {
                throw;
            }
        }        
        bool IsDecimalNumeric(string s)
        {
            decimal outdecumal;
            return decimal.TryParse(s, out outdecumal);
        }
        
        #endregion
    }
}