﻿using IEMQS.Models;
using IEMQSImplementation;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.Authorization.Controllers
{
    public class PautProbeController : clsBase
    {
        #region Utility
        public bool CheckExcellFormat(ExcelPackage package)
        {
            bool isValidFormat = true;
            ExcelWorksheet excelWorksheet = package.Workbook.Worksheets.First();
            if (excelWorksheet.Cells["A1"].Text.ToUpper() != "PROBE")
            {
                isValidFormat = false;
            }

            if (excelWorksheet.Cells["B1"].Text.ToUpper() != "PROBEMAKE")
            {
                isValidFormat = false;
            }

            if (excelWorksheet.Cells["C1"].Text.ToUpper() != "FREQUENCY")
            {
                isValidFormat = false;
            }

            if (excelWorksheet.Cells["D1"].Text.ToUpper() != "ELEMENTSIZE")
            {
                isValidFormat = false;
            }
            if (excelWorksheet.Cells["E1"].Text.ToUpper() != "ELEMENTPITCH")
            {
                isValidFormat = false;
            }
            if (excelWorksheet.Cells["F1"].Text.ToUpper() != "ELEMENTGAPDISTANCE")
            {
                isValidFormat = false;
            }
            if (excelWorksheet.Cells["G1"].Text.ToUpper() != "NOOFELEMENT")
            {
                isValidFormat = false;
            }
            
            return isValidFormat;
        }
        #endregion
        /// <summary>
        /// Created by Nikita on 7th July 2017
        /// </summary>
        /// <returns></returns>
        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        // GET: Authorization/PautProbe
        public ActionResult Index()
        {
            if (TempData["Error"] != null && TempData["Error"].ToString() != "")
            {
                ViewBag.error = TempData["Error"].ToString();
            }
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("NDE018");
            return View();
        }

        [HttpPost]
        public ActionResult GetDataHtml(int Id)
        {//partial view for pop up
            NDE018 objNDE018 = new NDE018();
            if (Id > 0)
            {
                objNDE018 = db.NDE018.Where(x => x.Id == Id).FirstOrDefault();
                ViewBag.Action = "editmode";
            }
            return PartialView("_GetProbeHtmlPartial", objNDE018);
        }

        [HttpPost]
        public ActionResult SaveProbe(FormCollection fc, NDE018 nde018)
        {
            NDE018 objNDE018 = new NDE018();
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (nde018.Id > 0)
                {
                    objNDE018 = db.NDE018.Where(x => x.Id == nde018.Id).FirstOrDefault();
                    // objNDE018.Probe = nde018.Probe;
                    objNDE018.ProbeMake = nde018.ProbeMake;
                    objNDE018.ElementSize = Convert.ToDecimal(nde018.ElementSize);
                    objNDE018.ElementPitch = Convert.ToDecimal(nde018.ElementPitch);
                    objNDE018.ElementGapDistance = Convert.ToDecimal(nde018.ElementGapDistance);
                    objNDE018.Frequency = Convert.ToInt32(nde018.Frequency);
                    objNDE018.NoOfElement = nde018.NoOfElement;
                    objNDE018.EditedBy = objClsLoginInfo.UserName;
                    objNDE018.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.ProbeMessages.Update.ToString();
                }
                else
                {
                    objNDE018.Probe = nde018.Probe;
                    objNDE018.ProbeMake = nde018.ProbeMake;
                    objNDE018.ElementSize = nde018.ElementSize;
                    objNDE018.ElementPitch = nde018.ElementPitch;
                    objNDE018.ElementGapDistance = nde018.ElementGapDistance;
                    objNDE018.Frequency = nde018.Frequency;
                    objNDE018.NoOfElement = nde018.NoOfElement;
                    objNDE018.CreatedBy = objClsLoginInfo.UserName;
                    objNDE018.CreatedOn = DateTime.Now;
                    db.NDE018.Add(objNDE018);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.ProbeMessages.Insert.ToString();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProbe(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                NDE018 objNDE018 = db.NDE018.Where(x => x.Id == Id).FirstOrDefault();
                db.NDE018.Remove(objNDE018);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Delete.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult LoadProbeData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                strWhere += "where 1=1";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    //strWhere += " and (Probe like '%" + param.sSearch + "%' or ProbeMake like '%" + param.sSearch + "%' or Frequency like '%" + param.sSearch + "%' or ElementPitch like '%" + param.sSearch + "%' or ElementSize like '%" + param.sSearch + "%')";

                    string[] arrayLikeCon = {
                                                "Probe",
                                                "ProbeMake",
                                                "Frequency",
                                                "ElementPitch",
                                                "ElementSize",
                                                "ElementGapDistance",
                                                "NDE.CreatedBy+'-'+c32.t_namb"
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);

                }
                else
                {

                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstResult = db.SP_PROBE_DETAILS
                                (
                                startIndex, endIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                           Convert.ToString(uc.Id),
                           Convert.ToString(uc.Probe),
                           Convert.ToString(uc.ProbeMake),
                            Convert.ToString(uc.Frequency),
                           Convert.ToString(uc.ElementPitch),
                           Convert.ToString(uc.ElementSize),
                           Convert.ToString(uc.ElementGapDistance),
                            Convert.ToString(uc.NoOfElement),
                           Convert.ToString(uc.CreatedBy),
                             Convert.ToString(uc.Id)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "", int? HeaderId = 0, int? latestRv = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_PROBE_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Probe = li.Probe,
                                      ProbeMake = li.ProbeMake,
                                      Frequency = li.Frequency,
                                      Pitch = li.ElementPitch,
                                      Size = li.ElementSize,
                                      GapDistence = li.ElementGapDistance,
                                      NoOfElement = li.NoOfElement,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }



                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        #region Import Excel
        public FileResult DownloadSample()
        {
            byte[] fileBytes = System.IO.File.ReadAllBytes(Server.MapPath("~/Areas/Authorization/Views/PautProbe/PAUTProbeTemplate.xlsx"));
            string fileName = " UploadTemplate.xlsx";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }
        public ActionResult ReadExcel()
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            if (Request.Files.Count == 0)
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please select a file first!";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            HttpPostedFileBase upload = Request.Files[0];
            if (Path.GetExtension(upload.FileName) == ".xlsx" || Path.GetExtension(upload.FileName) == ".xls")
            {
                ExcelPackage package = new ExcelPackage(upload.InputStream);
                //validation for excell format code by Ajay Chauhan on 30-3-2018
                if (!CheckExcellFormat(package))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Excell format is not valid";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                DataTable dt = Manager.ExcelToDataTable(package);
                return validateAndSave(dt);
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please upload valid excel file!";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }

        }
        //validate mandatory fields and date from excel
        public ActionResult validateAndSave(DataTable dt)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            bool hasError = false;
            if (!dt.Columns.Contains("Error"))
            {
                dt.Columns.Add("Error", typeof(string));
            }
            foreach (DataRow item in dt.Rows)
            {
                string psno = item.Field<string>(3);
                List<string> errors = new List<string>();

                string Probe = item.Field<string>(0);
                string ProbeMake = item.Field<string>(1);
                string Frequency = item.Field<string>(2);
                string ElementSize = item.Field<string>(3);
                string ElementPitch = item.Field<string>(4);
                string ElementGapDistance = item.Field<string>(5);
                string NoOfElement = item.Field<string>(6);

                if (string.IsNullOrWhiteSpace(Probe) || string.IsNullOrWhiteSpace(ProbeMake) ||
                    string.IsNullOrWhiteSpace(Frequency) || string.IsNullOrWhiteSpace(ElementSize) ||
                    string.IsNullOrWhiteSpace(ElementPitch) || string.IsNullOrWhiteSpace(ElementGapDistance) ||
                    string.IsNullOrWhiteSpace(NoOfElement))
                {
                    errors.Add("Mandatory fields are required"); hasError = true;
                }
                if (!string.IsNullOrWhiteSpace(Probe) && Probe.Length > 20)
                {
                    errors.Add("Probe have maximum limit of 20 characters"); hasError = true;
                }
                if (!string.IsNullOrWhiteSpace(ProbeMake) && ProbeMake.Length > 30)
                {
                    errors.Add("Probe Make have maximum limit of 30 characters"); hasError = true;
                }
                if (!string.IsNullOrWhiteSpace(Frequency) && Frequency.Length > 8)
                {
                    errors.Add("Frequency(MHz) have maximum limit of 8 characters"); hasError = true;
                }
                if (!string.IsNullOrWhiteSpace(NoOfElement) && NoOfElement.Length > 5)
                {
                    errors.Add("No Of Element have maximum limit of 5 characters"); hasError = true;
                }

                string depc = item.Field<string>(2);
                if (!string.IsNullOrWhiteSpace(Frequency) && !(IsDecimalNumeric(Frequency)))
                {
                    errors.Add("Frequency(MHz) must be numeric decimal"); hasError = true;

                }
                if (!string.IsNullOrWhiteSpace(ElementSize) && !(IsDecimalNumeric(ElementSize)))
                {
                    errors.Add("Element Size(mm) must be numeric decimal"); hasError = true;

                }
                if (!string.IsNullOrWhiteSpace(ElementPitch) && !(IsDecimalNumeric(ElementPitch)))
                {
                    errors.Add("Element Pitch(mm) must be numeric decimal"); hasError = true;

                }
                if (!string.IsNullOrWhiteSpace(ElementGapDistance) && !(IsDecimalNumeric(ElementGapDistance)))
                {
                    errors.Add("Element Gap Distance(mm) must be numeric decimal"); hasError = true;

                }
                if (!string.IsNullOrWhiteSpace(NoOfElement) && !(IsIntNumeric(NoOfElement)))
                {
                    errors.Add("No. of Element must be numeric"); hasError = true;

                }

                if (errors.Count > 0)
                {
                    //item.SetField<string>(5, string.Join(",", errors.ToArray()));
                    item["Error"] = string.Join(",", errors.ToArray());
                }
            }
            if (!hasError)
            {
                List<NDE018> listNDE018 = new List<NDE018>();
                foreach (DataRow item in dt.Rows)
                {
                    listNDE018.Add(new NDE018()
                    {
                        Probe = item.Field<string>(0),
                        ProbeMake = item.Field<string>(1),
                        Frequency = Convert.ToDecimal(item.Field<string>(2)),
                        ElementSize = Convert.ToDecimal(item.Field<string>(3)),
                        ElementPitch = Convert.ToDecimal(item.Field<string>(4)),
                        ElementGapDistance = Convert.ToDecimal(item.Field<string>(5)),
                        NoOfElement = Convert.ToInt32(item.Field<string>(6)),
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    }
                   );
                }
                if (listNDE018.Count > 0)
                {
                    db.NDE018.AddRange(listNDE018);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Prob Upload Successfully!";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return ExportToExcel(dt);
            }
        }
        //insert column wise data of excel into database
        public ActionResult ExportToExcel(DataTable dt)
        {
            clsHelper.ResponceMsgWithFileName objResponseMsg = new clsHelper.ResponceMsgWithFileName();
            try
            {
                MemoryStream ms = new MemoryStream();
                using (FileStream fs = System.IO.File.OpenRead(Server.MapPath("~/Areas/Authorization/Views/PautProbe/PAUTProbeErrorList.xlsx")))
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(fs))
                    {
                        ExcelWorkbook excelWorkBook = excelPackage.Workbook;
                        ExcelWorksheet excelWorksheet = excelWorkBook.Worksheets.First();
                        int i = 2;
                        foreach (DataRow item in dt.Rows)
                        {
                            excelWorksheet.Cells[i, 1].Value = item.Field<string>(0);
                            excelWorksheet.Cells[i, 2].Value = item.Field<string>(1);
                            excelWorksheet.Cells[i, 3].Value = item.Field<string>(2);
                            excelWorksheet.Cells[i, 4].Value = item.Field<string>(3);
                            excelWorksheet.Cells[i, 5].Value = item.Field<string>(4);
                            excelWorksheet.Cells[i, 6].Value = item.Field<string>(5);
                            excelWorksheet.Cells[i, 7].Value = item.Field<string>(6);
                            excelWorksheet.Cells[i, 8].Value = item.Field<string>(7);
                            i++;
                        }
                        excelPackage.SaveAs(ms);
                    }
                }

                ms.Position = 0;
                //ViewBag.suc = "false";
                //return File(ms, "Not");
               
                string fileName;
                string excelFilePath = Helper.ExcelFilePath("ErrorList", out fileName);
                
                Byte[] bin = ms.ToArray();
                System.IO.File.WriteAllBytes(excelFilePath, bin);

                objResponseMsg.Key = false;
                objResponseMsg.FileName = fileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);


                //return new FileStreamResult(ms, "application/xlsx")
                //{
                //    FileDownloadName = "ErrorList.xlsx"
                //};
            }
            catch (Exception)
            {
                throw;
            }
        }        
        bool IsDecimalNumeric(string s)
        {
            decimal outdecumal;
            return decimal.TryParse(s, out outdecumal);
        }
        bool IsIntNumeric(string s)
        {
            int outint;
            return int.TryParse(s, out outint);
        }

        #endregion
    }
}