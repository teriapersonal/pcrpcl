﻿using IEMQS.Models;
using IEMQSImplementation;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.Authorization.Controllers
{
    public class ExtWelderStampController : clsBase
    {
        // GET: Authorization/ExtWelderStamp
        [AllowAnonymous]
        [SessionExpireFilter]
        [UserPermissions]
        public ActionResult Index()
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("QMS004");
            ViewBag.IsDisplayOnly = false;
            return View();
        }
        [HttpPost]
        public ActionResult getData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = "1=1";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and  (Location like '%" + param.sSearch
                        + "%' or WelderPSNo like '%" + param.sSearch
                        + "%' or FatherName like '%" + param.sSearch
                          + "%' or Name like '%" + param.sSearch
                        + "%' or Surname like '%" + param.sSearch
                        + "%' or Sex like '%" + param.sSearch
                        + "%' or DOJ like '%" + param.sSearch
                        + "%' or LocalAddress like '%" + param.sSearch
                        + "%' or LocalCity like '%" + param.sSearch
                        + "%' or LocalState like '%" + param.sSearch
                        + "%' or PermanentAddress like '%" + param.sSearch
                        + "%' or PermanentCity like '%" + param.sSearch
                        + "%' or PermanentState like '%" + param.sSearch
                        + "%' or ContactNo like '%" + param.sSearch
                        + "%' or Local like '%" + param.sSearch
                        + "%' or ContractorCode like '%" + param.sSearch
                        + "%' or Stamp like '%" + param.sSearch
                        + "%' or Evaluation like '%" + param.sSearch
                        + "%' or CSNNo like '%" + param.sSearch
                        + "%' or ActiveWithCompany like '%" + param.sSearch
                        + "%' or ResignDate like '%" + param.sSearch
                        + "%' or CreatedBy like '%" + param.sSearch
                        + "%' or CreatedOn like '%" + param.sSearch
                        + "%')";

                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstResult = db.SP_ATH_GET_ExtWelderstamp
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                            {
                           Convert.ToString(uc.Location),
                           Convert.ToString(uc.WelderPSNo),
                            Convert.ToString(uc.Name),
                           Convert.ToString(uc.FatherName),
                           Convert.ToString(uc.Surname),
                           Convert.ToString(uc.Sex),
                           Convert.ToString(uc.DOJ),
                           Convert.ToString(uc.LocalAddress),
                           Convert.ToString(uc.LocalCity),
                           Convert.ToString(uc.LocalState),
                           Convert.ToString(uc.PermanentAddress),
                           Convert.ToString(uc.PermanentCity),
                           Convert.ToString(uc.PermanentState),
                           Convert.ToString(uc.ContactNo),
                           Convert.ToString(uc.Local),
                           Convert.ToString(uc.ContractorCode),
                           Convert.ToString(uc.Stamp),
                           Convert.ToString(uc.Evaluation),
                           Convert.ToString(uc.CSNNo),
                           Convert.ToString(uc.ActiveWithCompany),
                           Convert.ToString(uc.ResignDate),
                           Convert.ToString(uc.CreatedBy),
                           Convert.ToDateTime(uc.CreatedOn).ToString("dd/MM/yyyy"),
                           Convert.ToString(uc.Id),
                           Convert.ToString(uc.Id)
                           }).ToList();


                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult verifyStampNo(string stampNo)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                var IntStampExists = db.QMS003.Any(x => x.Stamp == stampNo && x.Evaluation == true);
                var ExtStampExists = db.QMS004.Any(x => x.Stamp == stampNo && x.Evaluation == true);
                if (IntStampExists || ExtStampExists)
                {
                    objResponseMsg.Key = true;
                }
                else
                {
                    objResponseMsg.Key = false;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetExt(int Id)
        {
            QMS004 objQMS004 = new QMS004();
            if (Id > 0)
            {
                objQMS004 = db.QMS004.Where(x => x.Id == Id).FirstOrDefault();
                ViewBag.Location = db.WQR008.Where(i => i.Location.Equals(objQMS004.Location, StringComparison.OrdinalIgnoreCase)).Select(i => i.Location + "-" + i.Description).FirstOrDefault();
                ViewBag.Cotractor = db.COM006.Where(i => i.t_bpid.Equals(objQMS004.ContractorCode, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_bpid + "-" + i.t_nama).FirstOrDefault();
            }

            ViewBag.lstyesno = clsImplementationEnum.getyesno().ToArray();
            ViewBag.ShopCode = db.COM002.Where(i => i.t_dimx.Equals(objQMS004.ShopCode, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_dimx + "-" + i.t_desc).FirstOrDefault();
            return PartialView("ExtWelderStampHtml", objQMS004);
        }

        [HttpPost]
        public ActionResult GetLocation(string term)
        {
            List<CategoryData> lstLocation = new List<CategoryData>();
            if (string.IsNullOrWhiteSpace(term))
            {
                lstLocation = db.WQR008.Select(x => new CategoryData { Code = x.Location + "-" + x.Description, Value = x.Location }).ToList();
            }
            else {
                lstLocation = db.WQR008.Where(x=>x.Location.Contains(term) || x.Description.Contains(term)).Select(x => new CategoryData { Code = x.Location + "-" + x.Description, Value = x.Location }).ToList();
            }
            return Json(lstLocation, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetContractor(string term)
        {
            List<CategoryData> lstContractor = new List<CategoryData>();
            if (string.IsNullOrWhiteSpace(term))
            {
                lstContractor = (from a in db.COM006
                                 select new CategoryData { Code = a.t_bpid, Value = a.t_bpid + "-" + a.t_nama }).ToList();
            }
            else
            {
                lstContractor = (from a in db.COM006
                                 where a.t_bpid.Contains(term) || a.t_nama.Contains(term)
                                 select new CategoryData { Code = a.t_bpid, Value = a.t_bpid + "-" + a.t_nama }).ToList();
            }

            return Json(lstContractor, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult SaveExt(QMS004 qms004, FormCollection fc)
        {
            var WelderPSNo = qms004.WelderPSNo;
            var Stamp = fc["txtStamp"].ToString().ToUpper();
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var psno = qms004.WelderPSNo;
                var stamp = fc["txtStamp"].ToString().ToUpper();
                var isvalid = db.QMS004.Any(x => (x.WelderPSNo == psno && x.Evaluation == true) || (x.Stamp == stamp && x.Evaluation == true));
                if (isvalid == false)
                {
                    bool d1, d2;
                    if (fc["ddlEval"].ToString() == "Yes")
                    {
                        d1 = true;
                    }
                    else
                    {
                        d1 = false;
                    }
                    if (fc["ddlcomp"].ToString() == "Yes")
                    {
                        d2 = true;
                    }
                    else
                    {
                        d2 = false;
                    }
                    QMS004 objqms004 = new QMS004();
                    objqms004.Location = fc["ddlLoca"].ToString().Split('-')[0];
                    objqms004.WelderPSNo = qms004.WelderPSNo;
                    objqms004.FatherName = fc["txtfname"].ToString();
                    objqms004.Surname = fc["txtSurname"].ToString();
                    objqms004.Name = fc["txtfirstname"].ToString();

                    objqms004.ContractorCode = fc["ddlcode"].ToString();
                    objqms004.Stamp = fc["txtStamp"].ToString().ToUpper();
                    objqms004.Evaluation = d1;
                    objqms004.CSNNo = fc["txtcsn"].ToString();
                    objqms004.ActiveWithCompany = d2;
                    string txtdate = fc["txtdate"];
                    if (string.IsNullOrWhiteSpace(txtdate) || txtdate == "01/01/0001")
                    {
                        objqms004.ResignDate = null;
                    }
                    else
                    {
                        objqms004.ResignDate = DateTime.ParseExact(txtdate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                    }
                    objqms004.ShopCode = fc["ShopCode"];
                    objqms004.CreatedBy = objClsLoginInfo.UserName;
                    objqms004.CreatedOn = DateTime.Now;
                    db.QMS004.Add(objqms004);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.Message.ToString();
                }


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteExt(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS004 objQMS004 = db.QMS004.Where(x => x.Id == Id).FirstOrDefault();
                db.QMS004.Remove(objQMS004);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Delete.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateExt(QMS004 qms004, FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int id = Convert.ToInt32(fc["hfid"].ToString());
                if (id > 0)
                {
                    bool d1, d2;
                    if (fc["ddlEval"].ToString() == "Yes")
                    {
                        d1 = true;
                    }
                    else
                    {
                        d1 = false;
                    }
                    if (fc["ddlcomp"].ToString() == "Yes")
                    {
                        d2 = true;
                    }
                    else
                    {
                        d2 = false;
                    }


                    QMS004 objqms004 = db.QMS004.Where(x => x.Id == id).FirstOrDefault();
                    objqms004.WelderPSNo = qms004.WelderPSNo;
                    objqms004.FatherName = fc["txtfname"].ToString();
                    objqms004.Surname = fc["txtSurname"].ToString();

                    objqms004.ContractorCode = fc["ddlcode"].ToString();
                    objqms004.Stamp = fc["txtStamp"].ToString();
                    objqms004.Evaluation = d1;
                    objqms004.CSNNo = fc["txtcsn"].ToString();
                    objqms004.ActiveWithCompany = d2;

                    string txtdate = fc["txtdate"];
                    if (string.IsNullOrWhiteSpace(txtdate) || txtdate == "01/01/0001")
                    {
                        objqms004.ResignDate = null;
                    }
                    else
                    {
                        objqms004.ResignDate = DateTime.ParseExact(txtdate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                    }

                    objqms004.ShopCode = fc["ShopCode"];
                    objqms004.EditedBy = objClsLoginInfo.UserName;
                    objqms004.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public string getCodeValue(string WelderPSNo)
        {
            var projdesc = db.COM003.Where(i => i.t_psno == WelderPSNo && i.t_actv == 1).Select(i => i.t_psno + " - " + i.t_name).FirstOrDefault();
            return projdesc;
        }
        [HttpPost]
        public ActionResult getDesc(string WelderPSNo)
        {
            ResponceMsgWithStatus1 objResponseMsg = new ResponceMsgWithStatus1();
            try
            {
                objResponseMsg.projdesc = db.COM003.Where(i => i.t_psno == WelderPSNo && i.t_actv == 1).Select(i => i.t_psno + " - " + i.t_name).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult verifyEmployee(string employee)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                employee = employee.Split('-')[0].Trim();
                var Exists = db.COM003.Any(x => x.t_psno == employee && x.t_actv == 1);
                if (Exists == false)
                {
                    objResponseMsg.Key = false;
                }
                else
                {
                    objResponseMsg.Key = true;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DisplayExtWeld()
        {
            ViewBag.IsDisplayOnly = true;
            return View("Index");
        }

        #region ExcelHelper
        [SessionExpireFilter]
        public ActionResult ImportExcel(HttpPostedFileBase upload)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            if (upload != null)
            {
                if (Path.GetExtension(upload.FileName).ToLower() == ".xlsx" || Path.GetExtension(upload.FileName).ToLower() == ".xls")
                {
                    ExcelPackage package = new ExcelPackage(upload.InputStream);
                    ExcelWorksheet excelWorksheet = package.Workbook.Worksheets.First();

                    DataTable dtHeaderExcel = new DataTable();
                    List<CategoryData> errors = new List<CategoryData>();
                    string Approved = clsImplementationEnum.MIPHeaderStatus.Approved.GetStringValue();
                    string Draft = clsImplementationEnum.MIPHeaderStatus.Draft.GetStringValue();
                    string Returned = clsImplementationEnum.MIPHeaderStatus.Returned.GetStringValue();
                    bool isError;

                    DataSet ds = ToChechValidation(package, out isError);
                    if (ds.Tables[0].Rows.Count == 0)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Record Found!";
                    }
                    else if (!isError)
                    {
                        try
                        {
                            foreach (DataRow item in ds.Tables[0].Rows)
                            {
                                var objQMS004 = new QMS004();
                                string Location = item.Field<string>("Location");
                                string WelderPSNo = item.Field<string>("WelderPSNo");
                                string Name = item.Field<string>("Name");
                                string FatherName = item.Field<string>("FatherName");
                                string Surname = item.Field<string>("Surname");
                                string ContractorCode = item.Field<string>("ContractorCode");
                                string Stamp = item.Field<string>("Stamp");
                                bool Evaluation = item.Field<string>("Evaluation") == clsImplementationEnum.yesno.Yes.GetStringValue();
                                string CSNNo = item.Field<string>("CSNNo");
                                bool ActiveWithCompany = item.Field<string>("ActiveWithCompany") == clsImplementationEnum.yesno.Yes.GetStringValue();
                                string ShopCode = item.Field<string>("ShopCode");
                                string txtResignDate = item.Field<string>("ResignDate");
                                DateTime? ResignDate = null;
                                if (!string.IsNullOrWhiteSpace(txtResignDate) && txtResignDate != "01/01/0001")
                                    ResignDate = DateTime.ParseExact(txtResignDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                                
                                #region Add New MIP Master Operations
                                objQMS004.Location = Location;
                                objQMS004.WelderPSNo = WelderPSNo;
                                objQMS004.FatherName = FatherName;
                                objQMS004.Surname = Surname;
                                objQMS004.Name = Name;
                                objQMS004.ContractorCode = ContractorCode;
                                objQMS004.Stamp = Stamp;
                                objQMS004.Evaluation = Evaluation;
                                objQMS004.CSNNo = CSNNo;
                                objQMS004.ActiveWithCompany = ActiveWithCompany;
                                objQMS004.ResignDate = ResignDate;
                                objQMS004.ShopCode = ShopCode;
                                objQMS004.CreatedBy = objClsLoginInfo.UserName;
                                objQMS004.CreatedOn = DateTime.Now;

                                db.QMS004.Add(objQMS004);
                                db.SaveChanges();
                                #endregion
                            }
                            db.SaveChanges();

                            objResponseMsg.Key = true;
                            objResponseMsg.Value = "MIP Operation added successfully";
                        }
                        catch (Exception ex)
                        {
                            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = ex.Message;
                        }
                    }
                    else
                    {
                        objResponseMsg = ErrorExportToExcel(ds);
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Excel file is not valid";
                }
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please enter valid Project no.";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public clsHelper.ResponceMsgWithFileName ErrorExportToExcel(DataSet ds)
        {
            clsHelper.ResponceMsgWithFileName objResponseMsg = new clsHelper.ResponceMsgWithFileName();
            try
            {
                MemoryStream ms = new MemoryStream();
                using (FileStream fs = System.IO.File.OpenRead(Server.MapPath("~/Resources/ExtWelderStamp/External Welder Stamp Excel Template.xlsx")))
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(fs))
                    {
                        ExcelWorkbook excelWorkBook = excelPackage.Workbook;
                        ExcelWorksheet excelWorksheet = excelWorkBook.Worksheets.First();
                        int i = 2;
                        foreach (DataRow item in ds.Tables[0].Rows)
                        {
                            //Location
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("LocationErrorMsg")))
                            {
                                excelWorksheet.Cells[i, 1].Value = item.Field<string>(0) + " (Note: " + item.Field<string>("LocationErrorMsg") + " )";
                                excelWorksheet.Cells[i, 1].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 1].Value = item.Field<string>(0); }
                            //WelderPSNo
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("WelderPSNoErrorMsg")))
                            {
                                excelWorksheet.Cells[i, 2].Value = item.Field<string>(1) + " (Note: " + item.Field<string>("WelderPSNoErrorMsg").ToString() + " )";
                                excelWorksheet.Cells[i, 2].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 2].Value = item.Field<string>(1); }
                            //Name
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("NameErrorMsg")))
                            {
                                excelWorksheet.Cells[i, 3].Value = item.Field<string>(2) + " (Note: " + item.Field<string>("NameErrorMsg").ToString() + " )";
                                excelWorksheet.Cells[i, 3].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 3].Value = item.Field<string>(2); }
                            //FatherName
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("FatherNameErrorMsg")))
                            {
                                excelWorksheet.Cells[i, 4].Value = item.Field<string>(3) + " (Note: " + item.Field<string>("FatherNameErrorMsg").ToString() + " )";
                                excelWorksheet.Cells[i, 4].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 4].Value = item.Field<string>(3); }
                            //Surname
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("SurnameErrorMsg")))
                            {
                                excelWorksheet.Cells[i, 5].Value = item.Field<string>(4) + " (Note: " + item.Field<string>("SurnameErrorMsg").ToString() + " )";
                                excelWorksheet.Cells[i, 5].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 5].Value = item.Field<string>(4); }
                            //ContractorCode
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("ContractorCodeErrorMsg")))
                            {
                                excelWorksheet.Cells[i, 6].Value = item.Field<string>(5) + " (Note: " + item.Field<string>("ContractorCodeErrorMsg").ToString() + " )";
                                excelWorksheet.Cells[i, 6].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 6].Value = item.Field<string>(5); }
                            //Stamp
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("StampErrorMsg")))
                            {
                                excelWorksheet.Cells[i, 7].Value = item.Field<string>(6) + " (Note: " + item.Field<string>("StampErrorMsg").ToString() + " )";
                                excelWorksheet.Cells[i, 7].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 7].Value = item.Field<string>(6); }
                            //Evaluation
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("EvaluationErrorMsg")))
                            {
                                excelWorksheet.Cells[i, 8].Value = item.Field<string>(7) + " (Note: " + item.Field<string>("EvaluationErrorMsg").ToString() + " )";
                                excelWorksheet.Cells[i, 8].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 8].Value = item.Field<string>(7); }
                            //CSNNo
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("CSNNoErrorMsg")))
                            {
                                excelWorksheet.Cells[i, 9].Value = item.Field<string>(8) + " (Note: " + item.Field<string>("CSNNoErrorMsg").ToString() + " )";
                                excelWorksheet.Cells[i, 9].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 9].Value = item.Field<string>(8); }
                            //ActiveWithCompany
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("ActiveWithCompanyErrorMsg")))
                            {
                                excelWorksheet.Cells[i, 10].Value = item.Field<string>(9) + " (Note: " + item.Field<string>("ActiveWithCompanyErrorMsg").ToString() + " )";
                                excelWorksheet.Cells[i, 10].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 10].Value = item.Field<string>(9); }
                            //ResignDate
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("ResignDateErrorMsg")))
                            {
                                excelWorksheet.Cells[i, 11].Value = item.Field<string>(10) + " (Note: " + item.Field<string>("ResignDateErrorMsg").ToString() + " )";
                                excelWorksheet.Cells[i, 11].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 11].Value = item.Field<string>(10); }
                            //ShopCode
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("ShopCodeErrorMsg")))
                            {
                                excelWorksheet.Cells[i, 12].Value = item.Field<string>(11) + " (Note: " + item.Field<string>("ShopCodeErrorMsg").ToString() + " )";
                                excelWorksheet.Cells[i, 12].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 12].Value = item.Field<string>(11); }
                            i++;
                        }
                        excelPackage.SaveAs(ms);
                    }
                }

                ms.Position = 0;

                string fileName;
                string excelFilePath = Helper.ExcelFilePath(objClsLoginInfo.UserName, out fileName);

                Byte[] bin = ms.ToArray();
                System.IO.File.WriteAllBytes(excelFilePath, bin);

                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error";
                objResponseMsg.FileName = fileName;
                return objResponseMsg;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
        }

        public DataSet ToChechValidation(ExcelPackage package, out bool isError)
        {
            ExcelWorksheet excelWorksheet = package.Workbook.Worksheets.First();
            DataTable dtHeaderExcel = new DataTable();
            isError = false;
            int notesIndex = 0;
            try
            {
                #region Read Lines data from excel 
                foreach (var firstRowCell in excelWorksheet.Cells[1, 1, 1, excelWorksheet.Dimension.End.Column])
                {
                    dtHeaderExcel.Columns.Add(firstRowCell.Text);
                }

                for (var rowNumber = 2; rowNumber <= excelWorksheet.Dimension.End.Row; rowNumber++)
                {
                    var row = excelWorksheet.Cells[rowNumber, 1, rowNumber, excelWorksheet.Dimension.End.Column];
                    var newRow = dtHeaderExcel.NewRow();
                    if (excelWorksheet.Cells[rowNumber, 1].Value != null)
                    {
                        foreach (var cell in row)
                        {
                            newRow[cell.Start.Column - 1] = cell.Text;
                        }
                    }
                    else
                    {
                        notesIndex = rowNumber + 1;
                        break;
                    }

                    dtHeaderExcel.Rows.Add(newRow);
                }
                #endregion

                #region Validation for Lines
                dtHeaderExcel.Columns.Add("LocationErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("WelderPSNoErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("FatherNameErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("SurnameErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("NameErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("ContractorCodeErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("StampErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("EvaluationErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("CSNNoErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("ActiveWithCompanyErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("ResignDateErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("ShopCodeErrorMsg", typeof(string));
                
                var lstContractorCode = db.COM006.Select(s=>s.t_bpid).ToList();
                var lstLocation = db.WQR008.Select(x => x.Location ).ToList();
                var lstYN = clsImplementationEnum.getyesno();
                var lstShopCode = db.COM002.Where(i => i.t_dtyp == 3 && i.t_dimx != "").Select(i => i.t_dimx).ToList();
                var lstStamp = db.QMS003.Select(x => x.Stamp).Concat(db.QMS004.Select(x => x.Stamp)).Distinct().ToList();

                foreach (DataRow item in dtHeaderExcel.Rows)
                {
                    string Location = item.Field<string>("Location");
                    string WelderPSNo = item.Field<string>("WelderPSNo");
                    string Name = item.Field<string>("Name");
                    string FatherName = item.Field<string>("FatherName");
                    string Surname = item.Field<string>("Surname");
                    string ContractorCode = item.Field<string>("ContractorCode");
                    string Stamp = item.Field<string>("Stamp");
                    string Evaluation = item.Field<string>("Evaluation");
                    string CSNNo = item.Field<string>("CSNNo");
                    string ActiveWithCompany = item.Field<string>("ActiveWithCompany");
                    string ResignDate = item.Field<string>("ResignDate");
                    string ShopCode = item.Field<string>("ShopCode");

                    string errorMessage = string.Empty;

                    if (string.IsNullOrEmpty(Location))
                    {
                        item["LocationErrorMsg"] = "Location is required";
                        isError = true;
                    }
                    else if (Location.Length > 9)
                    {
                        item["LocationErrorMsg"] = "Location maxlength exceeded";
                        isError = true;
                    }
                    else if (!lstLocation.Contains(Location))
                    {
                        item["LocationErrorMsg"] = "Location is Invalid";
                        isError = true;
                    }

                    if (string.IsNullOrEmpty(WelderPSNo))
                    {
                        item["WelderPSNoErrorMsg"] = "WelderPSNo is required";
                        isError = true;
                    }
                    else if (WelderPSNo.Length > 9)
                    {
                        item["WelderPSNoErrorMsg"] = "WelderPSNo maxlength exceeded";
                        isError = true;
                    }

                    if (string.IsNullOrEmpty(Name))
                    {
                        item["NameErrorMsg"] = "Name is required";
                        isError = true;
                    }
                    else if (Name.Length > 20)
                    {
                        item["NameErrorMsg"] = "Name maxlength exceeded";
                        isError = true;
                    }

                    if (!string.IsNullOrEmpty(FatherName) && FatherName.Length > 20)
                    {
                        item["ErrorMsg"] = "Father Name maxlength exceeded";
                        isError = true;
                    }

                    if (string.IsNullOrEmpty(Surname))
                    {
                        item["SurnameErrorMsg"] = "Surname is required";
                        isError = true;
                    }
                    else if (Surname.Length > 20)
                    {
                        item["SurnameErrorMsg"] = "Surname maxlength exceeded";
                        isError = true;
                    }

                    if (string.IsNullOrEmpty(ContractorCode))
                    {
                        item["ContractorCodeErrorMsg"] = "Contractor Code is required";
                        isError = true;
                    }
                    else if (ContractorCode.Length > 18)
                    {
                        item["ContractorCodeErrorMsg"] = "Contractor Code maxlength exceeded";
                        isError = true;
                    }
                    else if (!lstContractorCode.Contains(ContractorCode))
                    {
                        item["ContractorCodeErrorMsg"] = "Contractor Code is Invalid";
                        isError = true;
                    }

                    if (string.IsNullOrEmpty(Stamp))
                    {
                        item["StampErrorMsg"] = "Stamp is required";
                        isError = true;
                    }
                    else if (Stamp.Length > 6)
                    {
                        item["StampErrorMsg"] = "Stamp maxlength exceeded";
                        isError = true;
                    }
                    else if (lstStamp.Contains(Stamp))
                    {
                        item["StampErrorMsg"] = "Stamp is Already Exist";
                        isError = true;
                    }

                    if (string.IsNullOrEmpty(Evaluation))
                    {
                        item["EvaluationErrorMsg"] = "Evaluation is required";
                        isError = true;
                    }
                    else if (!lstYN.Contains(Evaluation))
                    {
                        item["EvaluationErrorMsg"] = "Evaluation is Invalid";
                        isError = true;
                    }

                    if (string.IsNullOrEmpty(CSNNo))
                    {
                        item["CSNNoErrorMsg"] = "CSN No. is required";
                        isError = true;
                    }
                    else if (CSNNo.Length > 10)
                    {
                        item["CSNNoErrorMsg"] = "CSN No. maxlength exceeded";
                        isError = true;
                    }

                    if (string.IsNullOrEmpty(ActiveWithCompany))
                    {
                        item["ActiveWithCompanyErrorMsg"] = "Active With Company is required";
                        isError = true;
                    }
                    else if (!lstYN.Contains(ActiveWithCompany))
                    {
                        item["ActiveWithCompanyErrorMsg"] = "Active With Company is Invalid";
                        isError = true;
                    }
                    else if (ActiveWithCompany == clsImplementationEnum.yesno.Yes.GetStringValue() && string.IsNullOrEmpty(ResignDate))
                    {
                        item["ResignDateErrorMsg"] = "Resign Date is required when Active With Company is Yes";
                        isError = true;
                    }

                    DateTime tempParse;
                    if (!string.IsNullOrEmpty(ResignDate) && !DateTime.TryParseExact(ResignDate, @"d/M/yyyy", CultureInfo.InvariantCulture, 0, out tempParse))
                    {
                        item["ResignDateErrorMsg"] = "Resign Date invalid date format";
                        isError = true;
                    }

                    if (string.IsNullOrEmpty(ShopCode))
                    {
                        item["ShopCodeErrorMsg"] = "Shop Code is required";
                        isError = true;
                    }
                    else if (ShopCode.Length > 9)
                    {
                        item["ShopCodeErrorMsg"] = "Shop Code maxlength exceeded";
                        isError = true;
                    }
                    else if (!lstShopCode.Contains(ShopCode))
                    {
                        item["ShopCodeErrorMsg"] = "Shop Code is Invalid";
                        isError = true;
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            DataSet ds = new DataSet();
            ds.Tables.Add(dtHeaderExcel);
            return ds;
        }

        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "", int? HeaderId = 0, int? latestRv = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_ATH_GET_ExtWelderstamp(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Location = li.Location,
                                      WelderPsNo = li.WelderPSNo,
                                      FathersName = li.FatherName,
                                      lastName = li.Surname,
                                      Stamp = li.Stamp,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }



                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}