﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.Authorization.Controllers
{
    public class StageMasterController : clsBase
    {
        // GET: Authorization/StageMaster
        //[AllowAnonymous]
        [SessionExpireFilter]
        //[UserPermissions]
        public ActionResult Index()
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("QMS002");
            return View();
        }

        [HttpPost]
        public ActionResult getData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = "1=1";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere+= "  and (e.t_desc like '%" + param.sSearch
                        + "%' or d.t_desc like '%" + param.sSearch
                        + "%' or a.StageCode like '%" + param.sSearch
                        + "%' or a.StageDesc like '%" + param.sSearch
                        + "%' or a.StageType like '%" + param.sSearch
                        + "%' or a.SequenceNo like '%" + param.sSearch
                        + "%' or a.NotesDrawing like '%" + param.sSearch
                        + "%' or a.GPASCalculation like '%" + param.sSearch
                        + "%' or a.OverlayStage like '%" + param.sSearch
                        + "%' or a.PTCRelatedStage like '%" + param.sSearch
                        + "%' or a.PrePostWeldHeadTreatment like '%" + param.sSearch
                        + "%' or a.IsHydroStage like '%" + param.sSearch
                        + "%' or a.IsWelderApplicable like '%" + param.sSearch
                        + "%' or a.StageDepartment like '%" + param.sSearch
                        + "%' or a.WeldVisualStage like '%" + param.sSearch
                        //+ "%' or a.IsSetupStage like '%" + param.sSearch
                        + "%' or a.CreatedBy like '%" + param.sSearch
                        + "%' or a.CreatedOn like '%" + param.sSearch
                        + "%')";

                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                #region Sorting
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion
                var lstResult = db.SP_ATH_GET_StageMaster
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                            {
                           Convert.ToString(uc.BU),
                           Convert.ToString(uc.Location),
                           Convert.ToString(uc.StageCode),
                           Convert.ToString(uc.StageDesc),
                           Convert.ToString(uc.StageType),
                           Convert.ToString(uc.SequenceNo),
                           Convert.ToString(uc.NotesDrawing),
                           Convert.ToString(uc.GPASCalculation),
                           Convert.ToString(uc.OverlayStage),
                           Convert.ToString(uc.PTCRelatedStage),
                           Convert.ToString(uc.PrePostWeldHeadTreatment),
                           Convert.ToString(uc.IsHydroStage),
                           Convert.ToString(uc.IsWelderApplicable),
                           Convert.ToString(uc.IsProtocolApplicable),
                           Convert.ToString(uc.StageDepartment),
                           Convert.ToString(uc.WeldVisualStage),
                           //Convert.ToString(uc.IsSetupStage),
                           Convert.ToString(uc.CreatedBy),
                           Convert.ToString(uc.CreatedOn),
                           //Convert.ToString(uc.Id),
                           "<center><i  data-modal='' id='btnUpdate' name='btnUpdate' style='cursor:pointer;' Title='Update Record' class='fa fa-edit' onClick='UpdateRecord(" + uc.Id + " )' ></i>"+
                           ( stageCodeIsUsed(uc.Id)?"":"&nbsp;<i  data-modal='' id='btnDelete' name='btnAction' style='cursor:pointer;' Title='Delete Record' class='fa fa-trash-o' onClick='DeleteRecord(" + uc.Id + ")'></i></center>"),
                Convert.ToString(uc.Id)
                           }).ToList();


                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult GetStageMaster(int Id)
        {
            QMS002 objQMS002 = new QMS002();
            if (Id > 0)
            {
                NDEModels objNDEModels = new NDEModels();
                objQMS002 = db.QMS002.Where(x => x.Id == Id).FirstOrDefault();
                if (stageCodeIsUsed(objQMS002.Id))
                    ViewBag.StageCodeUsed = "Yes";
                ViewBag.SurfaceCondition = objNDEModels.GetCategory("Surface Condition", objQMS002.DefaultSurfaceCondition, objQMS002.BU, objQMS002.Location, true).CategoryDescription;
            }

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.lstyesno = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();


            List<string> lstHeat = clsImplementationEnum.getHeatTreatment().ToList();
            ViewBag.lstHeat = lstHeat.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();


            var BU = (from a in db.COM002
                      where a.t_dtyp == 2 && a.t_dimx != ""
                      select new { a.t_dimx, Desc = a.t_desc }).ToList();
            ViewBag.BU = new SelectList(BU, "t_dimx", "Desc");

            var Location = (from a in db.COM002
                            where a.t_dtyp == 1 && a.t_dimx != ""
                            select new { a.t_dimx, Desc = a.t_desc }).ToList();

            ViewBag.Loca = new SelectList(Location, "t_dimx", "Desc");

            var StageType = (from a in db.GLB002
                             where a.Category == 10 && a.IsActive == true
                             select new { a.Code, Desc = a.Code + " - " + a.Description }).ToList();

            ViewBag.StageType = new SelectList(StageType, "Code", "Desc");

            var StageDept = (from a in db.GLB002
                             where a.Category == 11 && a.IsActive == true
                             select new { a.Code, Desc = a.Code + " - " + a.Description }).ToList();

            ViewBag.StageDept = new SelectList(StageDept, "Code", "Desc");
         

            return PartialView("StageMasterHtml", objQMS002);
        }
        [HttpPost]
        public ActionResult SaveStageMaster(QMS002 qms002, FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var BU = fc["ddlBU"].ToString();
                var Location = fc["ddlLoca"].ToString();
                var StageCode = fc["txtScode"].ToString().ToUpper();
                var isvalid = db.QMS002.Any(x => x.BU == BU && x.Location == Location && x.StageCode == StageCode);
                if (isvalid == false)
                {
                    bool d1, d2, d3, d4, d5, d6, WeldVisualStage, IsSetupStage, IsGroundSpot;
                    if (fc["ddlGPAS"].ToString() == "Yes")
                    {
                        d1 = true;
                    }
                    else
                    {
                        d1 = false;
                    }
                    if (fc["ddlOverlay"].ToString() == "Yes")
                    {
                        d2 = true;
                    }
                    else
                    {
                        d2 = false;
                    }
                    if (fc["ddlPTC"].ToString() == "Yes")
                    {
                        d3 = true;
                    }
                    else
                    {
                        d3 = false;
                    }
                    if (fc["ddlHydro"].ToString() == "Yes")
                    {
                        d4 = true;
                    }
                    else
                    {
                        d4 = false;
                    }
                    if (fc["dllWeld"].ToString() == "Yes")
                    {
                        d5 = true;
                    }
                    else
                    {
                        d5 = false;
                    }
                    if (fc["dllProtocol"].ToString() == "Yes")
                    {
                        d6 = true;
                    }
                    else
                    {
                        d6 = false;
                    }

                    if (fc["ddlWeldVisualStatge"].ToString().ToLower() == "yes")
                    {
                        WeldVisualStage = true;
                    }
                    else
                    {
                        WeldVisualStage = false;
                    } 
                    if (fc["ddlIsSetupStage"].ToString().ToLower() == "yes")
                    {
                        IsSetupStage = true;
                    }
                    else
                    {
                        IsSetupStage = false;
                    }
                    if (fc["ddlIsGroundSpot"].ToString().ToLower() == "yes")
                    {
                        IsGroundSpot = true;
                    }
                    else
                    {
                        IsGroundSpot = false;
                    }
                    QMS002 objqms002 = new QMS002();
                    objqms002.BU = fc["ddlBU"].ToString();
                    objqms002.Location = fc["ddlLoca"].ToString();
                    objqms002.StageCode = fc["txtScode"].ToString().ToUpper();
                    objqms002.StageDesc = fc["txtSdesc"].ToString();
                    objqms002.StageType = fc["ddlsType"].ToString().Split('-')[0].Trim();
                    objqms002.SequenceNo = fc["txtseq"] != null && fc["txtseq"].ToString() != string.Empty ? Convert.ToInt32(fc["txtseq"].ToString()) : 0;
                    objqms002.NotesDrawing = fc["txtND"].ToString();
                    objqms002.GPASCalculation = d1;
                    objqms002.OverlayStage = d2;
                    objqms002.PTCRelatedStage = d3;
                    objqms002.PrePostWeldHeadTreatment = fc["ddlHT"].ToString();
                    objqms002.IsHydroStage = d4;
                    objqms002.IsWelderApplicable = d5;
                    objqms002.IsProtocolApplicable = d6;
                    objqms002.StageDepartment = fc["ddlStageDept"].ToString();
                    objqms002.WeldVisualStage = WeldVisualStage;
                    objqms002.IsSetupStage = IsSetupStage;
                    objqms002.IsGroundSpot = IsGroundSpot;
                    objqms002.DefaultSurfaceCondition = qms002.DefaultSurfaceCondition;
                    objqms002.CreatedBy = objClsLoginInfo.UserName;
                    objqms002.CreatedOn = DateTime.Now;
                    db.QMS002.Add(objqms002);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.Message.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public bool stageCodeIsUsed(int id)
        {
            QMS002 objQMS002 = db.QMS002.Where(x => x.Id == id).FirstOrDefault();
            var objQMS016 = db.QMS016.Where(x => x.StageCode == objQMS002.StageCode && x.Location == objQMS002.Location && x.BU == objQMS002.BU).ToList();
            if (objQMS016.Any())
                return true;
            var objQMS021 = db.QMS021.Where(x => x.StageCode == objQMS002.StageCode && x.Location == objQMS002.Location && x.BU == objQMS002.BU).ToList();
            if (objQMS021.Any())
                return true;
            var objQMS031 = db.QMS031.Where(x => x.StageCode == objQMS002.StageCode && x.Location == objQMS002.Location && x.BU == objQMS002.BU).ToList();
            if (objQMS031.Any())
                return true;
            var objQMS036 = db.QMS036.Where(x => x.StageCode == objQMS002.StageCode && x.Location == objQMS002.Location && x.BU == objQMS002.BU).ToList();
            if (objQMS036.Any())
                return true;
            return false;
        }
        [HttpPost]
        public ActionResult DeleteStageMaster(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS002 objQMS002 = db.QMS002.Where(x => x.Id == Id).FirstOrDefault();
                db.QMS002.Remove(objQMS002);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Delete.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateStageMaster(QMS002 qms002, FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int id = Convert.ToInt32(fc["hfid"].ToString());
                if (id > 0)
                {
                    bool d1, d2, d3, d4, d5, d6, WeldVisualStage, IsSetupStage, IsGroundSpot;
                    if (fc["ddlGPAS"].ToString() == "Yes")
                    {
                        d1 = true;
                    }
                    else
                    {
                        d1 = false;
                    }
                    if (fc["ddlOverlay"].ToString() == "Yes")
                    {
                        d2 = true;
                    }
                    else
                    {
                        d2 = false;
                    }
                    if (fc["ddlPTC"].ToString() == "Yes")
                    {
                        d3 = true;
                    }
                    else
                    {
                        d3 = false;
                    }
                    if (fc["ddlHydro"].ToString() == "Yes")
                    {
                        d4 = true;
                    }
                    else
                    {
                        d4 = false;
                    }
                    if (fc["dllWeld"].ToString() == "Yes")
                    {
                        d5 = true;
                    }
                    else
                    {
                        d5 = false;
                    }
                    if (fc["dllProtocol"].ToString() == "Yes")
                    {
                        d6 = true;
                    }
                    else
                    {
                        d6 = false;
                    }

                    if (fc["ddlWeldVisualStatge"].ToString().ToLower() == "yes")
                    {
                        WeldVisualStage = true;
                    }
                    else
                    {
                        WeldVisualStage = false;
                    }
                    if (fc["ddlIsSetupStage"].ToString().ToLower() == "yes")
                    {
                        IsSetupStage = true;
                    }
                    else
                    {
                        IsSetupStage = false;
                    }
                    if (fc["ddlIsGroundSpot"].ToString().ToLower() == "yes")
                    {
                        IsGroundSpot = true;
                    }
                    else
                    {
                        IsGroundSpot = false;
                    }

                    QMS002 objqms002 = db.QMS002.Where(x => x.Id == id).FirstOrDefault();
                    objqms002.DefaultSurfaceCondition = qms002.DefaultSurfaceCondition;
                    objqms002.StageDesc = fc["txtSdesc"].ToString();
                    objqms002.StageType = fc["ddlsType"].ToString().Split('-')[0].Trim();
                    objqms002.SequenceNo = Convert.ToInt32(fc["txtseq"].ToString());
                    objqms002.NotesDrawing = fc["txtND"].ToString();
                    objqms002.GPASCalculation = d1;
                    objqms002.OverlayStage = d2;
                    objqms002.PTCRelatedStage = d3;
                    objqms002.PrePostWeldHeadTreatment = fc["ddlHT"].ToString();
                    objqms002.IsHydroStage = d4;
                    objqms002.IsWelderApplicable = d5;
                    objqms002.IsProtocolApplicable = d6;
                    objqms002.StageDepartment = fc["ddlStageDept"].ToString();
                    objqms002.WeldVisualStage = WeldVisualStage;
                    objqms002.IsSetupStage = IsSetupStage;
                    objqms002.IsGroundSpot = IsGroundSpot;
                    objqms002.EditedBy = objClsLoginInfo.UserName;
                    objqms002.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #region Export Excell
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_ATH_GET_StageMaster(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      BU = Convert.ToString(li.BU),
                                      Location = Convert.ToString(li.Location),
                                      StageCode = Convert.ToString(li.StageCode),
                                      StageDesc = Convert.ToString(li.StageDesc),
                                      StageType = Convert.ToString(li.StageType),
                                      SequenceNo = Convert.ToString(li.SequenceNo),
                                      NotesDrawing = Convert.ToString(li.NotesDrawing),
                                      GPASCalculation = Convert.ToString(li.GPASCalculation),
                                      OverlayStage = Convert.ToString(li.OverlayStage),
                                      PTCRelatedStage = Convert.ToString(li.PTCRelatedStage),
                                      PrePostWeldHeadTreatment = Convert.ToString(li.PrePostWeldHeadTreatment),
                                      IsHydroStage = Convert.ToString(li.IsHydroStage),
                                      IsWelderApplicable = Convert.ToString(li.IsWelderApplicable),
                                      IsProtocolApplicable = Convert.ToString(li.IsProtocolApplicable == true ? "Yes" : "No"),
                                      StageDepartment = Convert.ToString(li.StageDepartment),
                                      IsWeldVisualStage = Convert.ToString(li.WeldVisualStage),
                                      IsSetupStage = li.IsSetupStage,
                                      DefaultSurfaceCondition = li.DefaultSurfaceConditionDesc,
                                      IsGroundSpot = li.IsGroundSpot,
                                      CreatedBy = Convert.ToString(li.CreatedBy),
                                      CreatedOn = Convert.ToString(li.CreatedOn),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}
