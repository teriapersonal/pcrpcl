﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Models;
using IEMQSImplementation;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.Authorization.Controllers
{
    public class GlobalDataController : clsBase
    {
        #region Utility
        public JsonResult GetAllBU()
        {
            try
            {
                var lstBUs = (from a in db.COM002
                              where a.t_dtyp == 2 && a.t_dimx != ""
                              select new { a.t_dimx, Desc = a.t_desc }).ToList();

                var items = (from li in lstBUs
                             select new
                             {
                                 id = li.t_dimx,
                                 text = li.Desc,
                             }).ToList();

                return Json(items, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetAllLocation()
        {
            var lstLocations = (from a in db.COM002
                                where a.t_dtyp == 1 && a.t_dimx != null
                                select new { a.t_dimx, Desc = a.t_desc }).ToList();

            try
            {
                var items = (from li in lstLocations
                             select new
                             {
                                 id = li.t_dimx,
                                 text = li.Desc,
                             }).ToList();

                return Json(items, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        public bool CheckExcellFormat(ExcelPackage package)
        {
            bool isValidFormat = true;
            ExcelWorksheet excelWorksheet = package.Workbook.Worksheets.First();
            if (excelWorksheet.Cells["A1"].Text.ToUpper() != "BU")
            {
                isValidFormat = false;
            }

            if (excelWorksheet.Cells["B1"].Text.ToUpper() != "LOCATION")
            {
                isValidFormat = false;
            }

            if (excelWorksheet.Cells["C1"].Text.ToUpper() != "CATEGORY")
            {
                isValidFormat = false;
            }

            if (excelWorksheet.Cells["D1"].Text.ToUpper() != "CODE")
            {
                isValidFormat = false;
            }
            if (excelWorksheet.Cells["E1"].Text.ToUpper() != "DESCRIPTION")
            {
                isValidFormat = false;
            }
            return isValidFormat;
        }
        #endregion

        // GET: Authorization/GlobalData
        [AllowAnonymous]
        [SessionExpireFilter]
        [UserPermissions]
        public ActionResult Index()
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("GLB002");
            ViewBag.Category = (from a in db.GLB001
                            where a.IsActive == true
                            select new BULocWiseCategoryModel() { CatID = a.Id.ToString(), CatDesc = a.Category }).ToList();
            return View();
        }
        [HttpPost]
        public ActionResult getData(JQueryDataTableParamModel param)
        {
            try
            {
                // IEnumerable<ATH001> _List = new List<ATH001>();

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                //var totalRecords = new ObjectParameter("totalRecords", typeof(int));
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                int CategoryId = Convert.ToInt32(param.CategoryId);

                string strWhere = "1=1";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += string.Format(" AND (c.t_desc like '%{0}%' or d.t_desc like '%{0}%' or a.Category like '%{0}%' or a.Code like '%{0}%' or b.Description like '%{0}%' or a.CreatedBy + ' - ' + e.t_name like '%{0}%'", param.sSearch);

                    DateTime tempdate;
                    if (DateTime.TryParse(param.sSearch, out tempdate))
                        strWhere += " OR CONVERT(date, a.CreatedOn) Like '" + tempdate.ToString("yyyy-MM-dd") + "%'";
                    strWhere += ")";
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                #region Sorting
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion
                var lstResult = db.SP_ATH_GET_GlobalData_DETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, CategoryId, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                           Convert.ToString(uc.BUDesc),
                           Convert.ToString(uc.LocaDesc),
                           Convert.ToString(uc.CatDesc),
                           Convert.ToString(uc.Code),
                           Convert.ToString(uc.Description),
                           Convert.ToString(uc.CreatedBy),
                           Convert.ToString(uc.CreatedOn),
                            Convert.ToString(uc.Id),
                           Convert.ToString(uc.Id)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere,
                    categoryId = CategoryId
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult GetDataHtml(int Id, string Passval)
        {
            GLB002 objGLB002 = new GLB002();

            var BU = (from a in db.COM002
                      where a.t_dtyp == 2 && a.t_dimx != ""
                      select new { a.t_dimx, Desc = a.t_desc }).ToList();
            ViewBag.BU = new SelectList(BU, "t_dimx", "Desc");

            var Location = (from a in db.COM002
                            where a.t_dtyp == 1 && a.t_dimx != null
                            select new { a.t_dimx, Desc = a.t_desc }).ToList();

            ViewBag.Loca = new SelectList(Location, "t_dimx", "Desc");
            ViewBag.Categoryid = Passval.Split('-')[0].ToString();
            ViewBag.Category = Passval.Split('-')[1].ToString();

            return PartialView("GetGlobalDataHtml", objGLB002);
        }

        [HttpPost]
        public ActionResult GetDataHtmlUpdate(int Id, string Passval)
        {
            GLB002 objGLB002 = new GLB002();

            if (Id > 0)
            {
                objGLB002 = db.GLB002.Where(x => x.Id == Id).FirstOrDefault();
            }

            var BU = (from a in db.COM002
                      where a.t_dtyp == 2 && a.t_dimx != ""
                      select new { a.t_dimx, Desc = a.t_desc }).ToList();
            ViewBag.BU = new SelectList(BU, "t_dimx", "Desc");

            var Location = (from a in db.COM002
                            where a.t_dtyp == 1 && a.t_dimx != null
                            select new { a.t_dimx, Desc = a.t_desc }).ToList();

            ViewBag.Loca = new SelectList(Location, "t_dimx", "Desc");
            ViewBag.Category = Passval;

            return PartialView("GetGlobalDataHtml", objGLB002);
        }
        [HttpPost]
        public ActionResult SaveGDMatrix(GLB002 glb002, FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var buids = fc["ddlBU"].ToString().Split(',');
                var locaids = fc["ddlLoca"].ToString().Split(',');
                var cat = Convert.ToInt32(fc["hfcatId"].ToString());
                var code = fc["txtCode"].ToString();
                var isDuplicate = true;
                foreach (var bu in buids)
                {
                    foreach (var loca in locaids)
                    {
                        var isvalid = db.GLB002.Any(x => x.BU == bu && x.Location == loca && x.Code == code && x.Category == cat && x.IsActive == true);

                        if (isvalid == false)
                        {
                            isDuplicate = false;
                            GLB002 objGLB002 = new GLB002();
                            objGLB002.BU = bu;
                            objGLB002.Location = loca;
                            objGLB002.Category = Convert.ToInt32(fc["hfcatId"].ToString());
                            objGLB002.Code = fc["txtCode"].ToString();
                            objGLB002.Description = glb002.Description;
                            objGLB002.IsActive = true;
                            objGLB002.CreatedBy = objClsLoginInfo.UserName;
                            objGLB002.CreatedOn = DateTime.Now;
                            db.GLB002.Add(objGLB002);
                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();
                            //if (glb002.Id > 0)
                            //{
                            //    GLB002 objGLB002 = db.GLB002.Where(x => x.Id == glb002.Id).FirstOrDefault();
                            //    objGLB002.BU = fc["ddlBU"].ToString();
                            //    objGLB002.Location = fc["ddlLoca"].ToString();
                            //    objGLB002.Category = Convert.ToInt32(fc["hfcatId"].ToString());
                            //    objGLB002.Code = fc["txtCode"].ToString();
                            //    objGLB002.Description = glb002.Description;
                            //    objGLB002.EditedBy = objClsLoginInfo.UserName;
                            //    objGLB002.EditedOn = DateTime.Now;
                            //    db.SaveChanges();
                            //    objResponseMsg.Key = true;
                            //    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                            //}
                            //else
                            //{

                            //    GLB002 objGLB002 = new GLB002();
                            //    objGLB002.BU = fc["ddlBU"].ToString();
                            //    objGLB002.Location = fc["ddlLoca"].ToString();
                            //    objGLB002.Category = Convert.ToInt32(fc["hfcatId"].ToString());
                            //    objGLB002.Code = fc["txtCode"].ToString();
                            //    objGLB002.Description = glb002.Description;
                            //    objGLB002.IsActive = true;
                            //    objGLB002.CreatedBy = objClsLoginInfo.UserName;
                            //    objGLB002.CreatedOn = DateTime.Now;
                            //    db.GLB002.Add(objGLB002);
                            //    db.SaveChanges();
                            //    objResponseMsg.Key = true;
                            //    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();
                            //}
                        }
                        //else
                        //{
                        //    objResponseMsg.Key = false;
                        //    objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.Message.ToString();
                        //}
                    }
                }
                if (isDuplicate)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.Message.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteGDMatrix(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                GLB002 objGLB002 = db.GLB002.Where(x => x.Id == Id).FirstOrDefault();
                objGLB002.IsActive = false;
                objGLB002.EditedBy = objClsLoginInfo.UserName;
                objGLB002.EditedOn = DateTime.Now;
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Delete.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateGDMatrix(GLB002 glb002, FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (glb002.Id > 0)
                {
                    GLB002 objGLB002 = db.GLB002.Where(x => x.Id == glb002.Id).FirstOrDefault();
                    //objGLB002.Code = fc["txtCode"].ToString();
                    objGLB002.Description = glb002.Description;
                    objGLB002.EditedBy = objClsLoginInfo.UserName;
                    objGLB002.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #region Export Excell
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, int CategoryId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;

                var lst = db.SP_ATH_GET_GlobalData_DETAILS(1, int.MaxValue, strSortOrder, CategoryId, whereCondition).ToList();
                if (!lst.Any())
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No Data Found";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                var newlst = (from li in lst
                              select new
                              {
                                  BUDesc = li.BUDesc,
                                  LocaDesc = li.LocaDesc,
                                  CatDesc = li.CatDesc,
                                  Code = li.Code,
                                  Description = li.Description,
                                  CreatedBy = li.CreatedBy,
                                  CreatedOn = li.CreatedOn,
                              }).ToList();

                strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion


        #region Import Excel
        public FileResult DownloadSample()
        {
            byte[] fileBytes = System.IO.File.ReadAllBytes(Server.MapPath("~/Areas/Authorization/Views/GlobalData/GlobalDataTemplate.xlsx"));
            string fileName = " UploadTemplate.xlsx";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }
        public ActionResult ReadExcel()
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            if (Request.Files.Count == 0)
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please select a file first!";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            HttpPostedFileBase upload = Request.Files[0];
            if (Path.GetExtension(upload.FileName) == ".xlsx" || Path.GetExtension(upload.FileName) == ".xls")
            {
                ExcelPackage package = new ExcelPackage(upload.InputStream);
                //validation for excell format code by Ajay Chauhan on 30-3-2018
                if (!CheckExcellFormat(package))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Excel format is not valid";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                DataTable dt = Manager.ExcelToDataTable(package);
                return validateAndSave(dt);
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please upload valid excel file!";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }

        }
        //validate mandatory fields and date from excel
        public ActionResult validateAndSave(DataTable dt)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            bool hasError = false;
            if (!dt.Columns.Contains("Error"))
            {
                dt.Columns.Add("Error", typeof(string));
            }
            if (!dt.Columns.Contains("BUCode"))
            {
                dt.Columns.Add("BUCode", typeof(string));
            }
            if (!dt.Columns.Contains("LocationCode"))
            {
                dt.Columns.Add("LocationCode", typeof(string));
            }
            if (!dt.Columns.Contains("CategoryCode"))
            {
                dt.Columns.Add("CategoryCode", typeof(string));
            }
            foreach (DataRow item in dt.Rows)
            {
                string psno = item.Field<string>(3);
                List<string> errors = new List<string>();

                string BU = item.Field<string>(0);
                string Location = item.Field<string>(1);
                string Category = item.Field<string>(2);
                string Code = item.Field<string>(3);
                string Description = item.Field<string>(4);

                if (string.IsNullOrWhiteSpace(BU) || string.IsNullOrWhiteSpace(Location) ||
                    string.IsNullOrWhiteSpace(Category) || string.IsNullOrWhiteSpace(Code) ||
                    string.IsNullOrWhiteSpace(Description))
                {
                    errors.Add("Mandatory fields are required"); hasError = true;
                }
                if (!string.IsNullOrWhiteSpace(BU) && BU.Length > 30)
                {
                    errors.Add("BU have maximum limit of 30 characters"); hasError = true;
                }
                if (!string.IsNullOrWhiteSpace(Location) && Location.Length > 30)
                {
                    errors.Add("Location have maximum limit of 30 characters"); hasError = true;
                }
                if (!string.IsNullOrWhiteSpace(Category) && Category.Length > 50)
                {
                    errors.Add("Category Name have maximum limit of 50 characters"); hasError = true;
                }
                if (!string.IsNullOrWhiteSpace(Code) && Code.Length > 15)
                {
                    errors.Add("Global Data Code have maximum limit of 15 characters"); hasError = true;
                }
                if (!string.IsNullOrWhiteSpace(Description) && Description.Length > 100)
                {
                    errors.Add("Global Data Description have maximum limit of 100 characters"); hasError = true;
                }
                if (!string.IsNullOrWhiteSpace(BU) && BU.ToLower() != "all")
                {
                    var objBU = db.COM002.Where(i => i.t_desc.ToLower() == BU.ToLower() && i.t_dtyp == 2).FirstOrDefault();
                    if (objBU == null) { errors.Add("Invalid BU name"); hasError = true; }
                    else { item["BUCode"] = objBU.t_dimx; }
                }
                if (!string.IsNullOrWhiteSpace(Location) && Location.ToLower() != "all")
                {
                    var objLocation = db.COM002.Where(i => i.t_desc.ToLower() == Location.ToLower() && i.t_dtyp == 1).FirstOrDefault();
                    if (objLocation == null) { errors.Add("Invalid Location name"); hasError = true; }
                    else { item["LocationCode"] = objLocation.t_dimx; }
                }
                if (!string.IsNullOrWhiteSpace(Category))
                {
                    var objCategoty = db.GLB001.Where(i => i.Category.ToLower() == Category.ToLower() && i.IsActive == true).FirstOrDefault();
                    if (objCategoty == null) { errors.Add("Invalid Category Code"); hasError = true; }
                    else { item["CategoryCode"] = objCategoty.Id; }
                    //if (objCategoty != null) { item["CategoryCode"] = objCategoty.Id; }
                }

                if (Code.Contains("-"))
                {
                    errors.Add("Invalid Code"); hasError = true; 
                }

                if (item["BUCode"].ToString() != string.Empty && item["LocationCode"].ToString() != string.Empty && item["CategoryCode"].ToString() != string.Empty && !string.IsNullOrWhiteSpace(Code))
                {
                    var CategoryId = Convert.ToInt32(item["CategoryCode"].ToString());
                    var BUCode = item["BUCode"].ToString();
                    var LocationCode = item["LocationCode"].ToString();

                    if (db.GLB002.Where(i => i.BU == BUCode && i.Location == LocationCode && i.Category == CategoryId && i.Code == Code && i.IsActive == true).Any())
                    {
                        errors.Add("Record already exist"); hasError = true;
                    }
                }

                if (item.Field<string>(0).ToLower() == "all" && item["BUCode"].ToString() == string.Empty && item["CategoryCode"].ToString() != string.Empty && !string.IsNullOrWhiteSpace(Code))
                {
                    var CategoryId = Convert.ToInt32(item["CategoryCode"].ToString());
                    var LocationCode = item["LocationCode"].ToString();

                    if (db.GLB002.Where(i => i.Location == (LocationCode == string.Empty ? i.Location : LocationCode) && i.Category == CategoryId && i.Code == Code && i.IsActive == true).Any())
                    {
                        errors.Add("Record already exist"); hasError = true;
                    }
                }

                if (item.Field<string>(1).ToLower() == "all" && item["LocationCode"].ToString() == string.Empty && item["CategoryCode"].ToString() != string.Empty && !string.IsNullOrWhiteSpace(Code))
                {
                    var CategoryId = Convert.ToInt32(item["CategoryCode"].ToString());
                    var BUCode = item["BUCode"].ToString();

                    if (db.GLB002.Where(i => i.BU == (BUCode == string.Empty ? i.BU : BUCode) && i.Category == CategoryId && i.Code == Code && i.IsActive == true).Any())
                    {
                        errors.Add("Record already exist"); hasError = true;
                    }
                }

                #region Check Duplicate Entry In Excel
                var dv = new DataView(dt);
                dv.RowFilter = "(BU='" + item.Field<string>(0) + "' and Location='" + item.Field<string>(1) + "' and Category='" + item.Field<string>(2) + "' and Code='" + item.Field<string>(3) + "') or (Category='" + item.Field<string>(2) + "' and Code='" + item.Field<string>(3) + "' and (BU='ALL' or Location='ALL') )";
                if (dv != null && dv.Count > 1)
                {
                    errors.Add("Duplicate record in data"); hasError = true;
                }
                #endregion


                if (errors.Count > 0)
                {
                    item["Error"] = string.Join(",", errors.ToArray());
                }
            }
            if (!hasError)
            {
                #region New GlobalData
                DataView dv = new DataView(dt);
                dv.RowFilter = "CategoryCode<>''";
                if (dv.Count > 0)
                {
                    List<GLB002> listGLB002 = new List<GLB002>();
                    foreach (DataRow dr in dv.ToTable().Rows)
                    {
                        var CategoryCode = Convert.ToInt32(dr["CategoryCode"].ToString());
                        var BUCode = dr["BUCode"].ToString();
                        var LocationCode = dr["LocationCode"].ToString();
                        var Code = dr.Field<string>(3);
                        var Description = dr.Field<string>(4);

                        if (dr.Field<string>(0).ToLower() == "all" || dr.Field<string>(1).ToLower() == "all")
                        {
                            List<string> listBU = new List<string>();
                            List<string> listLocation = new List<string>();
                            if (dr.Field<string>(0).ToLower() == "all")
                            {
                                listBU = db.COM002.Where(i => i.t_dtyp == 2 && i.t_dimx != "").ToList().Select(i => i.t_dimx).ToList();
                            }
                            else
                            {
                                listBU.Add(BUCode);
                            }

                            if (dr.Field<string>(1).ToLower() == "all")
                            {
                                listLocation = db.COM002.Where(i => i.t_dtyp == 1 && i.t_dimx != "").ToList().Select(i => i.t_dimx).ToList();
                            }
                            else
                            {
                                listLocation.Add(LocationCode);
                            }
                            foreach (var bu in listBU)
                            {
                                foreach (var loc in listLocation)
                                {
                                    listGLB002.Add(new GLB002
                                    {
                                        BU = bu,
                                        Category = CategoryCode,
                                        CreatedBy = objClsLoginInfo.UserName,
                                        CreatedOn = DateTime.Now,
                                        Code = Code,
                                        Description = Description,
                                        Location = loc,
                                        IsActive = true
                                    });
                                }
                            }
                        }
                        else
                        {
                            listGLB002.Add(new GLB002
                            {
                                BU = BUCode,
                                Category = CategoryCode,
                                CreatedBy = objClsLoginInfo.UserName,
                                CreatedOn = DateTime.Now,
                                Code = Code,
                                Description = Description,
                                Location = LocationCode,
                                IsActive = true
                            });
                        }
                    }
                    if (listGLB002.Count > 0)
                    {
                        db.GLB002.AddRange(listGLB002);
                        db.SaveChanges();
                    }
                }
                #endregion

                #region Create New Category and GlobalData
                dv = new DataView(dt);
                dv.RowFilter = "ISNULL(CategoryCode,'')=''";
                if (dv.Count > 0)
                {
                    List<GLB001> listGLB001 = new List<GLB001>();
                    DataTable dtCategory = dv.ToTable(true, "Category");
                    foreach (DataRow dr in dtCategory.Rows)
                    {
                        var CategoryNew = dr["Category"].ToString();
                        listGLB001.Add(new GLB001
                        {
                            Category = CategoryNew,
                            CreatedBy = objClsLoginInfo.UserName,
                            CreatedOn = DateTime.Now,
                            Description = CategoryNew,
                            IsActive = true
                        });
                    }
                    if (listGLB001.Count > 0)
                    {
                        if (listGLB001.Count > 0)
                        {
                            db.GLB001.AddRange(listGLB001);
                            db.SaveChanges();
                        }
                        List<GLB002> listGLB002 = new List<GLB002>();
                        foreach (DataRow dr in dv.ToTable().Rows)
                        {
                            var CategoryNew = dr["Category"].ToString();
                            var CategoryCode = listGLB001.Where(i => i.Category == CategoryNew && i.IsActive == true).FirstOrDefault().Id;
                            var BUCode = dr["BUCode"].ToString();
                            var LocationCode = dr["LocationCode"].ToString();
                            var Code = dr.Field<string>(3);
                            var Description = dr.Field<string>(4);

                            if (dr.Field<string>(0).ToLower() == "all" || dr.Field<string>(1).ToLower() == "all")
                            {
                                List<string> listBU = new List<string>();
                                List<string> listLocation = new List<string>();
                                if (dr.Field<string>(0).ToLower() == "all")
                                {
                                    listBU = db.COM002.Where(i => i.t_dtyp == 2 && i.t_dimx != "").ToList().Select(i => i.t_dimx).ToList();
                                }
                                else
                                {
                                    listBU.Add(BUCode);
                                }

                                if (dr.Field<string>(1).ToLower() == "all")
                                {
                                    listLocation = db.COM002.Where(i => i.t_dtyp == 1 && i.t_dimx != "").ToList().Select(i => i.t_dimx).ToList();
                                }
                                else
                                {
                                    listLocation.Add(LocationCode);
                                }
                                foreach (var bu in listBU)
                                {
                                    foreach (var loc in listLocation)
                                    {
                                        listGLB002.Add(new GLB002
                                        {
                                            BU = bu,
                                            Category = CategoryCode,
                                            CreatedBy = objClsLoginInfo.UserName,
                                            CreatedOn = DateTime.Now,
                                            Code = Code,
                                            Description = Description,
                                            Location = loc,
                                            IsActive = true
                                        });
                                    }
                                }
                            }
                            else
                            {
                                listGLB002.Add(new GLB002
                                {
                                    BU = BUCode,
                                    Category = CategoryCode,
                                    CreatedBy = objClsLoginInfo.UserName,
                                    CreatedOn = DateTime.Now,
                                    Code = Code,
                                    Description = Description,
                                    Location = LocationCode,
                                    IsActive = true
                                });
                            }
                        }
                        if (listGLB002.Count > 0)
                        {
                            db.GLB002.AddRange(listGLB002);
                            db.SaveChanges();
                        }
                    }
                }
                #endregion

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Global Data Upload Successfully!";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return ExportToExcel(dt);
            }
        }
        //insert column wise data of excel into database
        public ActionResult ExportToExcel(DataTable dt)
        {
            clsHelper.ResponceMsgWithFileName objResponseMsg = new clsHelper.ResponceMsgWithFileName();
            try
            {
                MemoryStream ms = new MemoryStream();
                using (FileStream fs = System.IO.File.OpenRead(Server.MapPath("~/Areas/Authorization/Views/GlobalData/GlobalDataErrorList.xlsx")))
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(fs))
                    {
                        ExcelWorkbook excelWorkBook = excelPackage.Workbook;
                        ExcelWorksheet excelWorksheet = excelWorkBook.Worksheets.First();
                        int i = 2;
                        foreach (DataRow item in dt.Rows)
                        {
                            excelWorksheet.Cells[i, 1].Value = item.Field<string>(0);
                            excelWorksheet.Cells[i, 2].Value = item.Field<string>(1);
                            excelWorksheet.Cells[i, 3].Value = item.Field<string>(2);
                            excelWorksheet.Cells[i, 4].Value = item.Field<string>(3);
                            excelWorksheet.Cells[i, 5].Value = item.Field<string>(4);
                            excelWorksheet.Cells[i, 6].Value = item.Field<string>(5);
                            i++;
                        }
                        excelPackage.SaveAs(ms);
                    }
                }

                ms.Position = 0;

                string fileName;
                string excelFilePath = Helper.ExcelFilePath("ErrorList", out fileName);
                Byte[] bin = ms.ToArray();
                System.IO.File.WriteAllBytes(excelFilePath, bin);

                objResponseMsg.Key = false;
                objResponseMsg.FileName = fileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
    }
}