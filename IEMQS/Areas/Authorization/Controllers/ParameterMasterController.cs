﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.Authorization.Controllers
{
    public class ParameterMasterController : clsBase
    {
        // GET: Authorization/ParameterMaster
        //[AllowAnonymous]
        [SessionExpireFilter]
        //[UserPermissions]
        public ActionResult Index()
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("QMS001");
            return View();
        }

        [HttpPost]
        public ActionResult getData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = "1=1";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere+= " and (c.t_desc like '%" + param.sSearch
                        + "%' or b.t_desc like '%" + param.sSearch
                        + "%' or Project like '%" + param.sSearch
                        + "%' or AutoICL like '%" + param.sSearch
                        + "%' or DependencyApplicable like '%" + param.sSearch
                        + "%' or NonICLApprovalCycle like '%" + param.sSearch
                        + "%' or RepairDesignationToDisableReoffer like '%" + param.sSearch
                        + "%' or RepairDesignationToSendEmail like '%" + param.sSearch
                        + "%' or CreatedBy like '%" + param.sSearch
                        + "%' or CreatedOn like '%" + param.sSearch
                        + "%')";

                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstResult = db.SP_ATH_GET_ParameterMaster
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                           Convert.ToString(GetCodeAndNameByProject(uc.Project)),
                           Convert.ToString(uc.BU),
                           Convert.ToString(uc.Location),
                           Convert.ToString(uc.AutoICL),
                           Convert.ToString(uc.DependencyApplicable),
                           Convert.ToString(uc.NonICLApprovalCycle),
                           Convert.ToString(uc.NDTAutoGeneration),
                           Convert.ToString(uc.RepairDesignationToDisableReoffer),
                           Convert.ToString(uc.RepairDesignationToSendEmail),
                           Convert.ToString(uc.CreatedBy),
                           Convert.ToString(uc.CreatedOn),
                           Convert.ToString(uc.Id),
                           Convert.ToString(uc.Id)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetParameterMaster(int Id)
        {
            QMS001 objQMS001 = new QMS001();
          
            if (Id > 0)
            {
                objQMS001 = db.QMS001.Where(x => x.Id == Id).FirstOrDefault();
                ViewBag.state = "Edit";
            }
            List<string> lstnumbers = clsImplementationEnum.getDdlNumber().ToList();
            ViewBag.lstnumbers = lstnumbers.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.lstyesno = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

            var BU = (from a in db.COM002
                      where a.t_dtyp == 2 && a.t_dimx != ""
                      select new { a.t_dimx, Desc = a.t_desc }).ToList();
            ViewBag.BU = new SelectList(BU, "t_dimx", "Desc");

            var Location = (from a in db.COM002
                            where a.t_dtyp == 1 && a.t_dimx != ""
                            select new { a.t_dimx, Desc = a.t_desc }).ToList();

            ViewBag.Loca = new SelectList(Location, "t_dimx", "Desc");

            var lstProjectDesc = (from a in db.COM001
                                  select new Projects { projectCode = a.t_cprj, projectDescription = a.t_cprj + " - " + a.t_dsca }).ToList();
            ViewBag.Project = new SelectList(lstProjectDesc, "projectCode", "projectDescription");

            return PartialView("AddParameterData", objQMS001);
        }
        [HttpPost]
        public ActionResult SaveParameterMaster(FormCollection fc, QMS001 qms001)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int id = Convert.ToInt32(fc["Id"]);
                var BU = fc["BU"].Split('-')[0].Trim();
                var Location = fc["ddlLoca"].ToString();
                var Project = fc["txtProject"].ToString().Split('-')[0].Trim();
                var isvalid = db.QMS001.Any(x => x.BU == BU && x.Location == Location && x.Project == Project);
                if (isvalid == false)
                {
                    if (id > 0)
                    {
                        bool d1, d2, d3, d4;
                        if (fc["ddlAicl"].ToString() == "Yes")
                        {
                            d1 = true;
                        }
                        else
                        {
                            d1 = false;
                        }
                        if (fc["ddldepapp"].ToString() == "Yes")
                        {
                            d2 = true;
                        }
                        else
                        {
                            d2 = false;
                        }
                        if (fc["ddlNcycle"].ToString() == "Yes")
                        {
                            d3 = true;
                        }
                        else
                        {
                            d3 = false;
                        }
                        if (fc["ddlNauto"].ToString() == "Yes")
                        {
                            d4 = true;
                        }
                        else
                        {
                            d4 = false;
                        }
                        QMS001 objqms001 = db.QMS001.Where(x => x.Id == id).FirstOrDefault();
                        objqms001.BU = BU;
                        objqms001.Location = fc["ddlLoca"].ToString();
                        objqms001.Project = fc["ddlProject"].ToString().Split('-')[0].Trim();
                        objqms001.AutoICL = d1;
                        objqms001.DependencyApplicable = d2;
                        objqms001.NonICLApprovalCycle = d3;
                        objqms001.NDTAutoGeneration = d4;
                        objqms001.RepairDesignationToDisableReoffer = string.IsNullOrWhiteSpace(fc["ddlRepair"].ToString()) ? 0 : Convert.ToInt32(fc["ddlRepair"]); //Convert.ToInt32(fc["ddlRepair"].ToString());
                        objqms001.RepairDesignationToSendEmail = string.IsNullOrWhiteSpace(fc["ddlEmail"].ToString()) ? 0 : Convert.ToInt32(fc["ddlEmail"]);  //Convert.ToInt32(fc["ddlEmail"].ToString());
                        objqms001.IsLTFPS = qms001.IsLTFPS;
                        objqms001.EditedBy = objClsLoginInfo.UserName;
                        objqms001.EditedOn = DateTime.Now;
                        objqms001.RepairDesignationEmailTo = qms001.RepairDesignationEmailTo;
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                    }
                    else
                    {
                        bool d1, d2, d3, d4;
                        if (fc["ddlAicl"].ToString() == "Yes")
                        {
                            d1 = true;
                        }
                        else
                        {
                            d1 = false;
                        }
                        if (fc["ddldepapp"].ToString() == "Yes")
                        {
                            d2 = true;
                        }
                        else
                        {
                            d2 = false;
                        }
                        if (fc["ddlNcycle"].ToString() == "Yes")
                        {
                            d3 = true;
                        }
                        else
                        {
                            d3 = false;
                        }
                        if (fc["ddlNauto"].ToString() == "Yes")
                        {
                            d4 = true;
                        }
                        else
                        {
                            d4 = false;
                        }

                        int val1, val2;
                        if (fc["ddlRepair"].ToString() == "Select" || fc["ddlRepair"].ToString() == "")
                        {
                            val1 = 0;
                        }
                        else
                        {
                            val1 = Convert.ToInt32(fc["ddlRepair"].ToString());
                        }
                        if (fc["ddlEmail"].ToString() == "Select" || fc["ddlEmail"].ToString() == "")
                        {
                            val2 = 0;
                        }
                        else
                        {
                            val2 = Convert.ToInt32(fc["ddlEmail"].ToString());
                        }
                        QMS001 objqms001 = new QMS001();
                        objqms001.BU = BU;
                        objqms001.Location = fc["ddlLoca"].ToString();
                        objqms001.Project = fc["txtProject"].ToString().Split('-')[0].Trim();
                        objqms001.AutoICL = d1;
                        objqms001.DependencyApplicable = d2;
                        objqms001.NonICLApprovalCycle = d3;
                        objqms001.NDTAutoGeneration = d4;
                        objqms001.RepairDesignationToDisableReoffer = val1;
                        objqms001.RepairDesignationToSendEmail = val2;
                        objqms001.IsLTFPS = qms001.IsLTFPS;
                        objqms001.CreatedBy = objClsLoginInfo.UserName;
                        objqms001.CreatedOn = DateTime.Now;
                        objqms001.RepairDesignationEmailTo = qms001.RepairDesignationEmailTo;
                        db.QMS001.Add(objqms001);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.Message.ToString();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteGDMatrix(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {

                QMS001 objQMS001 = db.QMS001.Where(x => x.Id == Id).FirstOrDefault();
                List<QMS010> lstQMS010 = db.QMS010.Where(x => x.Project == objQMS001.Project && x.Location == objClsLoginInfo.Location).ToList();
                if (!lstQMS010.Any())
                {
                    db.QMS001.Remove(objQMS001);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CTQMessages.Delete.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Project can't be delete, as it used in Quality Project";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateGDMatrix(FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int id = Convert.ToInt32(fc["Id"]);
                if (id > 0)
                {
                    bool d1, d2, d3, d4;
                    if (fc["ddlAicl"].ToString() == "Yes")
                    {
                        d1 = true;
                    }
                    else
                    {
                        d1 = false;
                    }
                    if (fc["ddldepapp"].ToString() == "Yes")
                    {
                        d2 = true;
                    }
                    else
                    {
                        d2 = false;
                    }
                    if (fc["ddlNcycle"].ToString() == "Yes")
                    {
                        d3 = true;
                    }
                    else
                    {
                        d3 = false;
                    }
                    if (fc["ddlNauto"].ToString() == "Yes")
                    {
                        d4 = true;
                    }
                    else
                    {
                        d4 = false;
                    }

                    QMS001 objqms001 = db.QMS001.Where(x => x.Id == id).FirstOrDefault();

                    if (db.QMS010.Any(x => x.Project == objqms001.Project && x.Location == objqms001.Location))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Project can't be edit, as Quality Project of this project is already exists";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }

                    objqms001.AutoICL = d1;
                    // objqms001.Location = fc["ddlLoca"].ToString();
                    objqms001.DependencyApplicable = d2;
                    objqms001.NonICLApprovalCycle = d3;
                    objqms001.NDTAutoGeneration = d4;
                    objqms001.RepairDesignationToDisableReoffer = string.IsNullOrWhiteSpace(fc["ddlRepair"].ToString()) ? 0 : Convert.ToInt32(fc["ddlRepair"]); //Convert.ToInt32(fc["ddlRepair"].ToString());
                    objqms001.RepairDesignationToSendEmail = string.IsNullOrWhiteSpace(fc["ddlEmail"].ToString()) ? 0 : Convert.ToInt32(fc["ddlEmail"]);  //Convert.ToInt32(fc["ddlEmail"].ToString());
                    objqms001.RepairDesignationEmailTo = string.IsNullOrWhiteSpace(fc["RepairDesignationEmailTo"].ToString()) ? "" : fc["RepairDesignationEmailTo"].ToString();
                    objqms001.EditedBy = objClsLoginInfo.UserName;
                    objqms001.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult getCodeValue(string project)
        {
            ResponceMsgWithStatus1 objResponseMsg = new ResponceMsgWithStatus1();
            try
            {
                objResponseMsg.projdesc = db.COM001.Where(i => i.t_cprj == project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetProjectWiseBU(string projectcode)
        {
            if (!string.IsNullOrWhiteSpace(projectcode))
            {
                string BU = db.COM001.Where(i => i.t_cprj == projectcode).FirstOrDefault().t_entu;
                bool IsLTFPS;
                if (BU.Equals("02"))
                    IsLTFPS = true;
                else
                    IsLTFPS = false;
                var BUDescription = (from a in db.COM002
                                     where a.t_dtyp == 2 && a.t_dimx == BU
                                     select new { BUDesc = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();

                var response = new
                {
                    Key = true,
                    Value = BUDescription.BUDesc,
                    IsLTFPS = IsLTFPS
                };
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult verifyProject(string project)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                project = project.Split('-')[0].Trim();
                var Exists = db.COM001.Any(x => x.t_cprj == project);
                if (Exists == false)
                {
                    objResponseMsg.Key = false;
                }
                else
                {
                    objResponseMsg.Key = true;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public string GetCodeAndNameByProject(string project)
        {
            var projdesc = db.COM001.Where(i => i.t_cprj == project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
            return projdesc;
        }

        #region Export Excell
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_ATH_GET_ParameterMaster(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = Convert.ToString(GetCodeAndNameByProject(li.Project)),
                                      BU = Convert.ToString(li.BU),
                                      Location = Convert.ToString(li.Location),
                                      AutoICL = Convert.ToString(li.AutoICL),
                                      DependencyApplicable = Convert.ToString(li.DependencyApplicable),
                                      NonICLApprovalCycle = Convert.ToString(li.NonICLApprovalCycle),
                                      NDTAutoGeneration = Convert.ToString(li.NDTAutoGeneration),
                                      RepairDesignationToDisableReoffer = Convert.ToString(li.RepairDesignationToDisableReoffer),
                                      RepairDesignationToSendEmail = Convert.ToString(li.RepairDesignationToSendEmail),
                                      CreatedBy = Convert.ToString(li.CreatedBy),
                                      CreatedOn = Convert.ToString(li.CreatedOn),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion 
    }
    public class ResponceMsgWithStatus1 : clsHelper.ResponseMsg
    {
        public string status;
        public string Revision;
        public string projdesc;

    }
}