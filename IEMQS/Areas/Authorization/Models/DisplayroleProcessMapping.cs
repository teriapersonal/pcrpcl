﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IEMQS.Areas.Authorization.Models
{
    public class DisplayroleProcessMapping
    {
        public int ID { get; set; }
        public string Role { get; set; }
        public string Process { get; set; }
    }
}