﻿using IEMQS.Areas.Authorization.Controllers;
using IEMQS.Areas.MSW.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using IEMQS.Areas.Utility.Models;
using System.Globalization;

namespace IEMQS.Areas.EMT.Controllers
{
    public class MaintainEMTController : clsBase
    {
        // GET: EMT/MaintainEMT
        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }
        [SessionExpireFilter]
        public ActionResult AddEMT(int Id = 0)
        {
            EMT001 objEMT001 = new EMT001();
            var user = objClsLoginInfo.UserName;

            var lstProjectDesc = (from a in db.COM001
                                  select new Projects { projectCode = a.t_cprj, projectDescription = a.t_cprj + " - " + a.t_dsca }).ToList();
            ViewBag.Project = new SelectList(lstProjectDesc, "projectCode", "projectDescription");

            List<string> lstProCat = clsImplementationEnum.getProductCategory().ToList();
            ViewBag.lstProCat = lstProCat.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

            if (Id > 0)
            {
                objEMT001 = db.EMT001.Where(x => x.HeaderId == Id).FirstOrDefault();
                //if (objEMT001.CreatedBy != objClsLoginInfo.UserName)
                //{
                //    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                //}
                var PlanningDinID = db.PDN002.Where(x => x.RefId == Id && x.DocumentNo == objEMT001.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                ViewBag.PlanningDinID = PlanningDinID;

                ViewBag.IsReviseBtnEnabled = Manager.IsReviseEnabled(PlanningDinID, objEMT001.HeaderId, objEMT001.Document);
                ViewBag.DocMessage = clsImplementationMessage.CommonMessages.DocMessage.ToString();
            }
            return View(objEMT001);
        }
        [HttpPost]
        public ActionResult getCodeValue(string project, string approver)
        {
            ResponceMsgWithStatus1 objResponseMsg = new ResponceMsgWithStatus1();
            try
            {
                objResponseMsg.custdesc = Manager.GetCustomerCodeAndNameByProject(project);
                objResponseMsg.projdesc = db.COM001.Where(i => i.t_cprj == project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objResponseMsg.appdesc = db.COM003.Where(i => i.t_psno == approver && i.t_actv == 1).Select(i => i.t_psno + " - " + i.t_name).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ShowHistoryTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();

            model.TimelineTitle = " Initiate Exit Meeting TimeLine";
            model.Title = "PDinDoc";
            if (HeaderId > 0)
            {

                EMT001_Log objEMT001_Log = db.EMT001_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                model.CreatedBy = objEMT001_Log.CreatedBy != null ? Manager.GetUserNameFromPsNo(objEMT001_Log.CreatedBy) : null;
                model.CreatedOn = objEMT001_Log.CreatedOn;
                model.EditedBy = objEMT001_Log.EditedBy != null ? Manager.GetUserNameFromPsNo(objEMT001_Log.EditedBy) : null;
                model.EditedOn = objEMT001_Log.EditedOn;
                model.SubmittedBy = objEMT001_Log.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objEMT001_Log.SubmittedBy) : null;
                model.SubmittedOn = objEMT001_Log.SubmittedOn;

                model.ApprovedBy = objEMT001_Log.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objEMT001_Log.ApprovedBy) : null;
                model.ApprovedOn = objEMT001_Log.ApprovedOn;

            }
            else
            {

                EMT001_Log objEMT001_Log = db.EMT001_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                model.CreatedBy = objEMT001_Log.CreatedBy != null ? Manager.GetUserNameFromPsNo(objEMT001_Log.CreatedBy) : null;
                model.CreatedOn = objEMT001_Log.CreatedOn;
                model.EditedBy = objEMT001_Log.EditedBy != null ? Manager.GetUserNameFromPsNo(objEMT001_Log.EditedBy) : null;
                model.EditedOn = objEMT001_Log.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
        public ActionResult ShowTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();
            model.Title = "PDinDoc";
            model.TimelineTitle = "Initiate Exit Meeting Timeline";

            if (HeaderId > 0)
            {
                EMT001 objEMT001 = db.EMT001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.CreatedBy = objEMT001.CreatedBy != null ? Manager.GetUserNameFromPsNo(objEMT001.CreatedBy) : null;
                model.CreatedOn = objEMT001.CreatedOn;
                model.EditedBy = objEMT001.EditedBy != null ? Manager.GetUserNameFromPsNo(objEMT001.EditedBy) : null;
                model.EditedOn = objEMT001.EditedOn;
                model.SubmittedBy = objEMT001.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objEMT001.SubmittedBy) : null;
                model.SubmittedOn = objEMT001.SubmittedOn;
                model.ApprovedBy = objEMT001.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objEMT001.ApprovedBy) : null;
                model.ApprovedOn = objEMT001.ApprovedOn;

            }
            else
            {
                EMT001 objEMT001 = db.EMT001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.CreatedBy = objEMT001.CreatedBy != null ? Manager.GetUserNameFromPsNo(objEMT001.CreatedBy) : null;
                model.CreatedOn = objEMT001.CreatedOn;
                model.EditedBy = objEMT001.EditedBy != null ? Manager.GetUserNameFromPsNo(objEMT001.EditedBy) : null;
                model.EditedOn = objEMT001.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
        [NonAction]
        public string GetCustomerCodeAndNameByProject(string project)
        {
            return ((from cm004 in db.COM004
                     join cm005 in db.COM005 on cm004.t_cono equals cm005.t_cono
                     join cm006 in db.COM006 on cm004.t_ofbp equals cm006.t_bpid
                     where cm004.t_cprj == project
                     select cm004.t_ofbp + "-" + cm006.t_nama).FirstOrDefault());
        }

        [NonAction]
        public DateTime GetCDDFromProject(string Project)
        {
            try
            {
                var contract = db.COM004.Where(q => q.t_cprj.Equals(Project)).FirstOrDefault().t_cono;
                return db.COM008.Where(q => q.t_cono.Equals(contract)).FirstOrDefault().t_ccdd;
            }
            catch (Exception) { return new DateTime(); }
        }

        [HttpPost]
        public ActionResult GetHeaderData(string project)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                clsManager man = new clsManager();
                string customer = man.GetCustomerCodeAndNameByProject(project);
                string cdd = man.GetContractWiseCdd(project);
                int RevNum;
                var Exists = db.EMT001.Any(x => x.Project == project);
                if (Exists == true)
                {
                    objResponseMsg.Key = true;
                    int? revNo = (from rev in db.TLP001
                                  where rev.Project == project
                                  select rev.RevNo).FirstOrDefault();
                    RevNum = Convert.ToInt32(revNo) + 1;
                }
                else
                {
                    objResponseMsg.Key = false;
                    RevNum = 0;
                }
                objResponseMsg.Value = customer + "|" + cdd + "|" + RevNum.ToString();   //GetCDDFromProject(project).ToString("dd-MMM-yyyy");
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetProjects(string search)
        {
            try
            {
                var items = from li in db.COM001
                            where li.t_cprj.Contains(search) || li.t_dsca.Contains(search)
                            select new
                            {
                                id = li.t_cprj,
                                text = li.t_cprj + "-" + li.t_dsca
                            };
                return Json(items, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult GetHistoryView(int headerid = 0)
        {
            EMT001_Log objEMT001 = new EMT001_Log();
            Session["HeaderId"] = Convert.ToInt32(headerid);
            return PartialView("EMTHistoryGridPartial", objEMT001);
        }
        [HttpPost]
        public JsonResult LoadEMTHeaderHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var user = objClsLoginInfo.UserName;

                string strWhere = string.Empty;

                //if (param.CTQCompileStatus != null && param.CTQCompileStatus.ToUpper() == "PENDING")
                //{
                //    strWhere += "1=1 and status in('" + clsImplementationEnum.CTQStatus.SendForApprovel.GetStringValue() + "')";
                //}
                //else
                //{
                //    strWhere += "1=1";
                //}
                //strWhere += "(CreatedBy= '" + user + "' or  ApprovedBy='" + user +"')";

                strWhere += "HeaderId = '" + Convert.ToInt32(Session["Headerid"]) + "'";

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (Project like '%" + param.sSearch
                         + "%' or Document like '%" + param.sSearch
                         + "%' or Customer like '%" + param.sSearch
                         + "%' or RevNo like '%" + param.sSearch
                         + "%' or CDD like '%" + param.sSearch
                         + "%' or Product like '%" + param.sSearch
                         + "%' or ProcessLicensor like '%" + param.sSearch
                         + "%' or Status like '%" + param.sSearch + "%')";
                }
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + strSortOrder + sortDirection + " ";
                }
                var lstResult = db.SP_EMT_GET_HISTORY_HEADERDETAILS
                                (
                                StartIndex, EndIndex, "", strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(GetCodeAndNameByProject(uc.Project)),
                            Convert.ToString(uc.Document),
                            Convert.ToString(Manager.GetCustomerCodeAndNameByProject(uc.Project)),
                            Convert.ToString(uc.Product),
                            Convert.ToString(uc.ProcessLicensor),
                            Convert.ToString("R"+uc.RevNo),
                            Convert.ToString(uc.Status),
                            Convert.ToString(uc.SubmittedBy),
                            uc.SubmittedOn == null || uc.SubmittedOn.Value == DateTime.MinValue ? "NA" : uc.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                            Convert.ToString(uc.ApprovedBy),
                            uc.ApprovedOn == null || uc.ApprovedOn.Value == DateTime.MinValue ? "NA" : uc.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                            Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.HeaderId),
                               

                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult DeleteHeader(int headerid)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                objResponseMsg = Manager.DeletePDINDocument(headerid, clsImplementationEnum.PlanList.Initiate_Exit_Meeting);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public string GetCodeAndNameByProject(string project)
        {
            var projdesc = db.COM001.Where(i => i.t_cprj == project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
            return projdesc;
        }

        [SessionExpireFilter]
        public ActionResult EMTHistoryDetail(int Id = 0)
        {
            EMT001_Log objEMT001 = new EMT001_Log();
            var user = objClsLoginInfo.UserName;

            var lstProjectDesc = (from a in db.COM001
                                  select new Projects { projectCode = a.t_cprj, projectDescription = a.t_cprj + " - " + a.t_dsca }).ToList();
            ViewBag.Project = new SelectList(lstProjectDesc, "projectCode", "projectDescription");

            List<string> lstProCat = clsImplementationEnum.getProductCategory().ToList();
            ViewBag.lstProCat = lstProCat.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

            if (Id > 0)
            {
                objEMT001 = db.EMT001_Log.Where(x => x.Id == Id).FirstOrDefault();
                //if (objEMT001.CreatedBy != objClsLoginInfo.UserName)
                //{
                //    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                //}
            }
            var urlPrefix = Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.LastIndexOf('=') + 1);
            if (!objClsLoginInfo.GetUserRoleList().Contains("SHOP"))
            {
                ViewBag.RevPrev = (db.EMT001_Log.Any(q => (q.HeaderId == objEMT001.HeaderId && q.RevNo == (objEMT001.RevNo - 1))) ? urlPrefix + db.EMT001_Log.Where(q => (q.HeaderId == objEMT001.HeaderId && q.RevNo == (objEMT001.RevNo - 1))).FirstOrDefault().Id : null);
                ViewBag.RevNext = (db.EMT001_Log.Any(q => (q.HeaderId == objEMT001.HeaderId && q.RevNo == (objEMT001.RevNo + 1))) ? urlPrefix + db.EMT001_Log.Where(q => (q.HeaderId == objEMT001.HeaderId && q.RevNo == (objEMT001.RevNo + 1))).FirstOrDefault().Id : null);
            }
            return View(objEMT001);
        }

        [HttpPost]
        public JsonResult sentForApproval(string Approver, int headerid)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (headerid > 0)
                {
                    Approver = Approver.Split('-')[0].Trim();
                    if (Approver.Trim() == objClsLoginInfo.UserName)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.SMessage.ToString();
                    }
                    else
                    {
                        EMT001 objEMT001 = db.EMT001.Where(x => x.HeaderId == headerid).FirstOrDefault();
                        objEMT001.Status = clsImplementationEnum.CTQStatus.SendForApprovel.GetStringValue();
                        objEMT001.ApprovedBy = Approver.Trim();
                        objEMT001.SubmittedBy = objClsLoginInfo.UserName;
                        objEMT001.SubmittedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.AMessage.ToString();
                        Manager.UpdatePDN002(objEMT001.HeaderId, objEMT001.Status, objEMT001.RevNo, objEMT001.Project, objEMT001.Document);

                        #region Send Notification
                        (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG2.GetStringValue()+","+ clsImplementationEnum.UserRoleName.PLNG1.GetStringValue(),
                                                                objEMT001.Project,
                                                                "",
                                                                "",
                                                                Manager.GetPDINDocumentNotificationMsg(objEMT001.Project, clsImplementationEnum.PlanList.Initiate_Exit_Meeting.GetStringValue(), objEMT001.RevNo.Value.ToString(), objEMT001.Status),
                                                                clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(),
                                                                Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Initiate_Exit_Meeting.GetStringValue(), objEMT001.HeaderId.ToString(), true),
                                                                objEMT001.ApprovedBy);
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveEMTHeader(EMT001 objEMT001)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                string project = objEMT001.Project.Split('-')[0].Trim();
                int? HeaderId = db.EMT001.Where(i => i.Project == project).Select(i => i.HeaderId).FirstOrDefault();

                if (HeaderId > 0)
                {
                    var obj_EMT001 = db.EMT001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    //if (!objEMT001.CDD.HasValue || (objEMT001.CDD.HasValue && objEMT001.CDD.Value.Year <= 1754))
                    //    obj_EMT001.CDD = null;
                    //else
                    //    obj_EMT001.CDD = objEMT001.CDD;
                    //if (string.IsNullOrEmpty(objEMT001.Customer))
                    //    obj_EMT001.Customer = " ";
                    //else
                    //    obj_EMT001.Customer = objEMT001.Customer.Split('-')[0].Trim();
                    //obj_EMT001.Project = project;
                    //string rev = Regex.Replace(objEMT001.RevNo.ToString(), "[^0-9]+", string.Empty);
                    if (objEMT001.Status == clsImplementationEnum.PTMTCTQStatus.Approved.GetStringValue())
                    {

                        obj_EMT001.RevNo = objEMT001.RevNo + 1;
                        obj_EMT001.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                        obj_EMT001.ReturnRemark = null;
                        obj_EMT001.ApprovedOn = null;
                    }
                    else
                    {
                        obj_EMT001.RevNo = Convert.ToInt32(Regex.Replace(objEMT001.RevNo.ToString(), "[^0-9]+", string.Empty));
                        obj_EMT001.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                    }
                    //objEMT001.Product = objEMT001.Product;
                    //obj_EMT001.ProcessLicensor = objEMT001.ProcessLicensor;
                    if (!string.IsNullOrWhiteSpace(objEMT001.ApprovedBy))
                        obj_EMT001.ApprovedBy = objEMT001.ApprovedBy.Split('-')[0].ToString().Trim();
                    else
                        obj_EMT001.ApprovedBy = "";

                    //obj_EMT001.Document = objEMT001.Document;
                    obj_EMT001.EditedBy = objClsLoginInfo.UserName;
                    obj_EMT001.EditedOn = DateTime.Now;
                    obj_EMT001.ReviseRemark = objEMT001.ReviseRemark;
                    db.SaveChanges();
                    objResponseMsg.Status = obj_EMT001.Status;
                    objResponseMsg.RevNo = obj_EMT001.RevNo.ToString();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                    objResponseMsg.HeaderId = obj_EMT001.HeaderId;
                    //var folderPath = "EMT001/" + HeaderId.ToString();
                    //Manager.ManageDocumentsKM(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                    Manager.UpdatePDN002(obj_EMT001.HeaderId, obj_EMT001.Status, obj_EMT001.RevNo, obj_EMT001.Project, obj_EMT001.Document);
                }
                else
                {

                    EMT001 OBJEMT001 = new EMT001();

                    string projectEXIST = db.EMT001.Where(x => x.Project == project).Select(i => i.Project).FirstOrDefault();
                    if (projectEXIST != "")
                    {
                        if (!objEMT001.CDD.HasValue || (objEMT001.CDD.HasValue && objEMT001.CDD.Value.Year <= 1754))
                            OBJEMT001.CDD = null;
                        else
                            OBJEMT001.CDD = objEMT001.CDD;
                        if (string.IsNullOrEmpty(objEMT001.Customer))
                            OBJEMT001.Customer = " ";
                        else
                            OBJEMT001.Customer = objEMT001.Customer.Split('-')[0].Trim();
                        OBJEMT001.Project = project;
                        string rev = Regex.Replace(objEMT001.RevNo.ToString(), "[^0-9]+", string.Empty);
                        OBJEMT001.RevNo = 0;
                        OBJEMT001.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                        OBJEMT001.Product = objEMT001.Product;
                        OBJEMT001.ProcessLicensor = objEMT001.ProcessLicensor;
                        objEMT001.ReviseRemark = objEMT001.ReviseRemark;
                        if (!string.IsNullOrWhiteSpace(objEMT001.ApprovedBy))
                            OBJEMT001.ApprovedBy = objEMT001.ApprovedBy.Split('-')[0].ToString().Trim();
                        else
                            OBJEMT001.ApprovedBy = "";

                        OBJEMT001.Document = objEMT001.Document;
                        OBJEMT001.CreatedBy = objClsLoginInfo.UserName;
                        OBJEMT001.CreatedOn = DateTime.Now;
                        db.EMT001.Add(OBJEMT001);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();
                        objResponseMsg.Status = OBJEMT001.Status;
                        objResponseMsg.RevNo = OBJEMT001.RevNo.ToString();
                        int Headerid = (from a in db.EMT001
                                        where a.Project == project
                                        select a.HeaderId).FirstOrDefault();
                        //var folderPath = "EMT001/" + Headerid.ToString();
                        //Manager.ManageDocumentsKM(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                        objResponseMsg.HeaderId = OBJEMT001.HeaderId;
                        Manager.UpdatePDN002(objEMT001.HeaderId, objEMT001.Status, objEMT001.RevNo, objEMT001.Project, objEMT001.Document);
                    }
                    else
                    {
                        db.SaveChanges();
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Record already Exists..!";
                    }
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult getHeader(string project)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int Headerid = (from a in db.EMT001
                                where a.Project == project
                                select a.HeaderId).FirstOrDefault();
                string status = (from a in db.EMT001
                                 where a.Project == project
                                 select a.Status).FirstOrDefault();
                objResponseMsg.Key = true;
                objResponseMsg.Value = Headerid.ToString() + "|" + status;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult getHeaderId(string project)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int Headerid = (from a in db.EMT001
                                where a.Project == project
                                select a.HeaderId).FirstOrDefault();

                objResponseMsg.Key = true;
                objResponseMsg.Value = Headerid.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [NonAction]
        public string getHeaderID(string proj)
        {
            return (from a in db.EMT001 where a.Project == proj select a.HeaderId).FirstOrDefault().ToString();
        }

        [NonAction]
        public static byte[] FromBase64(string base64EncodedData)
        {
            base64EncodedData = base64EncodedData.Substring(base64EncodedData.IndexOf("base64,") + 7);
            var base64EncodedBytes = Convert.FromBase64String(base64EncodedData);
            return base64EncodedBytes;
        }

        [HttpPost]
        public JsonResult LoadEMTHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var user = objClsLoginInfo.UserName;

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and status in('" + clsImplementationEnum.CTQStatus.DRAFT.GetStringValue() + "','" + clsImplementationEnum.CTQStatus.Returned.GetStringValue() + "')";
                }
                else
                {
                    strWhere += "1=1";
                }
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                //strWhere += " and CreatedBy=" + user;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += "and (Project like '%" + param.sSearch
                         + "%' or Document like '%" + param.sSearch
                         + "%' or Customer like '%" + param.sSearch
                         + "%' or Product like '%" + param.sSearch
                         + "%' or ProcessLicensor like '%" + param.sSearch + "%')";
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_EMT_GET_HEADERDETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(GetCodeAndNameByProject(uc.Project)),
                               Convert.ToString(uc.Document),
                               Convert.ToString(Manager.GetCustomerCodeAndNameByProject(uc.Project)),
                               Convert.ToString(uc.ProcessLicensor),
                               Convert.ToString(uc.CDD.HasValue ? uc.CDD.Value.ToString("yyyy-MM-dd") : " "),
                               Convert.ToString(uc.Product),
                               Convert.ToString(uc.Status),
                               Convert.ToString("R"+uc.RevNo),
                               //"",
                                Convert.ToString(uc.SubmittedBy),
                                uc.SubmittedOn == null || uc.SubmittedOn.Value == DateTime.MinValue ? "NA" : uc.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                Convert.ToString(uc.ApprovedBy),
                                uc.ApprovedOn == null || uc.ApprovedOn.Value == DateTime.MinValue ? "NA" : uc.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                 "<nobr><center>"+"<a title='View' href='"+WebsiteURL+"/EMT/MaintainEMT/AddEMT?Id="+Convert.ToInt32(uc.HeaderId)+"'><i style='' class='iconspace fa fa-eye'></i></a>"+
                                   Helper.GenerateActionIcon(uc.HeaderId,"Delete","Delete Record","fa fa-trash-o", "DeleteDocument("+ uc.HeaderId +",'/EMT/MaintainEMT/DeleteHeader', {headerid:"+uc.HeaderId+"}, 'tblEMTHeader')","",  (( uc.RevNo >0 && uc.Status.ToLower() != clsImplementationEnum.CommonStatus.SendForApprovel.GetStringValue().ToLower()) || ( uc.RevNo == 0 && uc.Status.ToLower() == clsImplementationEnum.CommonStatus.Approved.GetStringValue().ToLower()) ) ? false:true) +
                             "<i class ='iconspace fa fa-clock-o' title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/EMT/MaintainEMT/ShowTimeline?HeaderID=" + Convert.ToInt32(uc.HeaderId) + "')></i>"+
                           (uc.RevNo>0?"<i title='History' class='iconspace fa fa-history' onClick='histroy("+uc.HeaderId+")'></i> ":"<i title='History' class='disabledicon fa fa-history' ></i>" )+
                             "</center></nobr>",
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.CreatedBy==user),
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public string GetAttachments(int headerId)
        {
            var v = clsUploadKM.getDocs("EMT001/" + headerId.ToString());
            string result = "";
            foreach (var item in v)
            {
                result += "<a href=\"" + item.URL + "\">" + item.Name + "</a><hr style=\"margin: 0.4em;\" />";
            }
            result += "";
            return result;
        }

        [HttpPost]
        public string EditAttachments(int headerId)
        {
            var v = clsUploadKM.getDocs("EMT001/" + headerId.ToString());
            string result = "";
            foreach (var item in v)
            {
                result += "<div class=\"row\"><div class=\"col-md-10\"> <a href=\"" + item.URL + "\">" + item.Name + "</a> </div><div class=\"col-md-2\"><a href=\"#\" onclick=\"deleteAttachment('" + headerId + "','" + item.Name + "')\" ><i Title = \"Delete Attachment\" class=\"fa fa-trash-o\" style=\"text-shadow:3px 3px 3px #888888;cursor:pointer;color:black\"></i></a></div></div><hr style=\"margin: 0.4em;\" />";
            }
            result += "<br/><a href=\"#\" class=\"pull-right\" onclick=\"showAddAttachment('" + headerId + "')\">Add New Attachment</a>";
            return result;
        }

        [HttpPost]
        public void DeleteAttachments(int headerId, string filename)
        {
            try
            {
                clsUploadKM.DeleteFile("EMT001/" + headerId.ToString(), filename);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }


        [HttpPost]
        public ActionResult GetHeaderGridDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("GetEMTGridDataPartial");
        }
        [HttpPost]
        public ActionResult ReviseHeader(int strHeaderId, string strRemarks)
        {
            CustomResponceMsg objResponseMsg = new CustomResponceMsg();
            try
            {
                EMT001 objEMT001 = db.EMT001.Where(u => u.HeaderId == strHeaderId).SingleOrDefault();
                if (objEMT001 != null)
                {
                    objEMT001.RevNo = Convert.ToInt32(objEMT001.RevNo) + 1;
                    objEMT001.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                    objEMT001.ReviseRemark = strRemarks;
                    objEMT001.EditedBy = objClsLoginInfo.UserName;
                    objEMT001.EditedOn = DateTime.Now;
                    objEMT001.ReturnRemark = null;
                    objEMT001.ApprovedOn = null;
                    objEMT001.SubmittedBy = null;
                    objEMT001.SubmittedOn = null;
                    db.SaveChanges();
                    Manager.UpdatePDN002(objEMT001.HeaderId, objEMT001.Status, objEMT001.RevNo, objEMT001.Project, objEMT001.Document);

                    var oldFolderPath = "EMT001//" + objEMT001.HeaderId + "//R" + (objEMT001.RevNo - 1);
                    var newFolderPath = "EMT001//" + objEMT001.HeaderId + "//R" + (objEMT001.RevNo);
                    //(new clsFileUpload()).CopyFolderContentsAsync(oldFolderPath, newFolderPath);

                    Utility.Controllers.FileUploadController _obj = new Utility.Controllers.FileUploadController();
                    _obj.CopyDataOnFCSServerAsync(oldFolderPath, newFolderPath, oldFolderPath, objEMT001.HeaderId, newFolderPath, objEMT001.HeaderId, DESServices.CommonService.GetUseIPConfig, DESServices.CommonService.objClsLoginInfo.UserName, 0);

                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderID = objEMT001.HeaderId;
                    objResponseMsg.Status = objEMT001.Status;
                    objResponseMsg.rev = objEMT001.RevNo;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Revision;
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public void UpdateAttachments(string headerId, bool hasAttachments, Dictionary<string, string> Attach)
        {
            Manager.ManageDocumentsKM("EMT001/" + headerId, hasAttachments, Attach, objClsLoginInfo.UserName);
        }

        [HttpPost]
        public ActionResult verifyApprover(string approver)
        {
            ResponceMsgWithStatus1 objResponseMsg = new ResponceMsgWithStatus1();
            try
            {
                approver = approver.Split('-')[0].Trim();
                var Exists = db.COM003.Any(x => x.t_psno == approver && x.t_actv == 1);
                if (Exists == false)
                {
                    objResponseMsg.Key = false;
                }
                else
                {
                    objResponseMsg.Key = true;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult retrack(string headerid)
        {
            ResponceMsgWithStatus1 objResponseMsg = new ResponceMsgWithStatus1();
            try
            {
                int hd = Convert.ToInt32(headerid);
                if (hd > 0)
                {
                    EMT001 objEMT001 = db.EMT001.Where(x => x.HeaderId == hd).FirstOrDefault();
                    objEMT001.Status = clsImplementationEnum.CTQStatus.DRAFT.GetStringValue();
                    objEMT001.SubmittedOn = null;
                    objEMT001.SubmittedBy = null;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    Manager.UpdatePDN002(objEMT001.HeaderId, objEMT001.Status, objEMT001.RevNo, objEMT001.Project, objEMT001.Document);
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #region Export Excel
        //Excel funtionality
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {//header grid data

                    var lst = db.SP_EMT_GET_HEADERDETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      Project = Convert.ToString(GetCodeAndNameByProject(uc.Project)),
                                      Document = Convert.ToString(uc.Document),
                                      Customer = Convert.ToString(Manager.GetCustomerCodeAndNameByProject(uc.Project)),
                                      ProcessLicensor = Convert.ToString(uc.ProcessLicensor),
                                      CDD = Convert.ToString(uc.CDD.HasValue ? uc.CDD.Value.ToString("yyyy-MM-dd") : " "),
                                      Product = Convert.ToString(uc.Product),
                                      Status = Convert.ToString(uc.Status),
                                      RevNo = Convert.ToString("R" + uc.RevNo),
                                      SubmittedBy = Convert.ToString(uc.SubmittedBy),
                                      SubmittedOn = uc.SubmittedOn == null || uc.SubmittedOn.Value == DateTime.MinValue ? "NA" : uc.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      ApprovedBy = uc.ApprovedBy,
                                      ApprovedOn = uc.ApprovedOn == null || uc.ApprovedOn.Value == DateTime.MinValue ? "NA" : uc.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.HISTORYHEADER.GetStringValue())
                {  //history header
                    var lst = db.SP_EMT_GET_HISTORY_HEADERDETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No data available";
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      Project = Convert.ToString(GetCodeAndNameByProject(uc.Project)),
                                      Document = Convert.ToString(uc.Document),
                                      Customer = Convert.ToString(Manager.GetCustomerCodeAndNameByProject(uc.Project)),
                                      ProcessLicensor = Convert.ToString(uc.ProcessLicensor),
                                      CDD = uc.CDD == null || uc.CDD.Value == DateTime.MinValue ? " " : uc.CDD.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      Product = Convert.ToString(uc.Product),
                                      Status = Convert.ToString(uc.Status),
                                      RevNo = Convert.ToString("R" + uc.RevNo),
                                      SubmittedBy = Convert.ToString(uc.SubmittedBy),
                                      SubmittedOn = uc.SubmittedOn == null || uc.SubmittedOn.Value == DateTime.MinValue ? "NA" : uc.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      ApprovedBy = uc.ApprovedBy,
                                      ApprovedOn = uc.ApprovedOn == null || uc.ApprovedOn.Value == DateTime.MinValue ? "NA" : uc.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }


                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generation, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
    public class ResponceMsgWithStatus1 : clsHelper.ResponseMsg
    {
        public string projdesc;
        public string appdesc;
        public string custdesc;
    }
}