﻿
using IEMQS.Areas.MSW.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.CSR.Controllers
{
    public class ApprovePMGCSRController : clsBase
    {
        // GET: CSR/ApprovePMGCSR
        public ActionResult Index(string Project)
        {
            ViewBag.chkProject = Project;
            return View();
        }

        public ActionResult AddUpdatePMGCSR(int HeaderID = 0)
        {
            ViewBag.HeaderID = HeaderID;
            return View();
        }

        [HttpPost]
        public ActionResult LoadCSRListDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_PMGCSRListDataPartial");
        }


        [HttpPost]
        public ActionResult LoadCSRAddUpdateFormPartial(int HeaderID)
        {
            CSM001 objCSM001 = new CSM001();

            string user = objClsLoginInfo.UserName;
            string BU = string.Join(",", db.ATH001.Where(i => i.Employee == user).Select(i => i.BU).ToList());
            List<Projects> project = Manager.getProjectsByUser(user);

            if (HeaderID > 0)
            {
                objCSM001 = db.CSM001.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
                var projectDetails = db.COM001.Where(x => x.t_cprj == objCSM001.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                ViewBag.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
            }
            else
            {
                objCSM001.Status = clsImplementationEnum.CSRStatus.Draft.GetStringValue();
                objCSM001.RevNo = 0;
                var locDescription = (from a in db.COM003
                                      join b in db.COM002 on a.t_loca equals b.t_dimx
                                      where b.t_dtyp == 1 && a.t_actv == 1
                                      && a.t_psno.Equals(user, StringComparison.OrdinalIgnoreCase)
                                      select b.t_dimx + "-" + b.t_desc).FirstOrDefault();

            }
            List<string> lstCsr = clsImplementationEnum.getFunctionCSR().ToList();
            ViewBag.TPIList = lstCsr.Select(i => new CategoryData { Value = i.ToString(), Code = i.ToString(), CategoryDescription = i.ToString() }).ToList();
            return PartialView("_PMGCSRAddUpdateFormPartial", objCSM001);
        }


        [HttpPost]
        public ActionResult GetHeaderData(string project)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                project = project.Split('-')[0].Trim();
                var ExistsPrject = db.COM001.Any(x => x.t_cprj == project);
                if (ExistsPrject == false)
                {
                    objResponseMsg.Key = false;
                }
                else
                {
                    string customer = Manager.GetCustomerCodeAndNameByProject(project);

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = customer;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CSRMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public static string MultiSelectDropdown(List<SelectItemList> list, int rowID, string selectedValue, bool Disabled = false, string OnChangeEvent = "", string OnBlurEvent = "", string ColumnName = "")
        {
            string multipleSelect = string.Empty;
            string[] arrayVal = { };

            try
            {
                if (!string.IsNullOrWhiteSpace(selectedValue))
                {
                    arrayVal = selectedValue.Split(',');
                }


                multipleSelect += "<select data-name='ddlmultiple' name='ddlmultiple" + rowID + "' id='ddlmultiple" + rowID + "' data-lineid='" + rowID + "' multiple='multiple'  style='width: 100 % ' colname='" + ColumnName + "' class='form-control multiselect-drodown personMultiselect' " + (Disabled ? "disabled " : "") + (OnChangeEvent != string.Empty ? "onchange='" + OnChangeEvent + "'" : "") + (OnBlurEvent != string.Empty ? " onblur='" + OnBlurEvent + "'" : "") + " >";
                foreach (var item in list)
                {
                    multipleSelect += "<option value='" + item.id + "' " + ((arrayVal.Contains(item.id)) ? " selected" : "") + " >" + item.text + "</option>";
                }
                multipleSelect += "</select>";
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return multipleSelect;
        }
        [HttpPost]
        public ActionResult getInlineCSRLines(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                string strSortOrder = string.Empty;
                int headerid = Convert.ToInt32(param.Headerid);
                var status = db.CSM001.Where(i => i.HeaderId == headerid).Select(i => i.Status).FirstOrDefault();
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere = " (Description like '%" + param.sSearch
                       + "%' or Project like '%" + param.sSearch
                       + "%' or MOMClause like '%" + param.sSearch
                       + "%' or NextStep like '%" + param.sSearch
                       + "%' or Functions like '%" + param.sSearch
                       + "%' or ResPerson +' - '+ i.t_name like '%" + param.sSearch
                       + "%' or Date like '%" + param.sSearch
                       + "%')";
                }
                else
                {
                    strWhere = "1=1";
                }
                strWhere += " and Groups = '1'";
                var lstResult = db.SP_CSR_GET_LINES
                                (
                                StartIndex, EndIndex, strSortOrder, headerid, strWhere
                                ).ToList();

                int newRecordId = 0;
                var proj = db.CSM001.Where(i => i.HeaderId == headerid).Select(i => i.Project).FirstOrDefault();

                bool isEditable = false;
                CSM001 objCSM001 = db.CSM001.FirstOrDefault(c => c.HeaderId == headerid);
                if (objCSM001 != null)
                {
                    if (objCSM001.Status == clsImplementationEnum.CSRStatus.SendForPMGApproval.GetStringValue())
                        isEditable = true;
                    else
                        isEditable = false;
                }
                List<SelectItemList> lstEmployee = new List<SelectItemList>();
                //if (proj != null)
                //  lstEmployee = getEmployeeAutocompletebyDept1(proj.ToString());

                var newRecord = new[] {
                                    Helper.GenerateHidden(newRecordId, "LineId", ""),
                                    Helper.GenerateHidden(newRecordId, "HeaderId", headerid.ToString()),
                                    Helper.GenerateTextbox(newRecordId, "Description"),
                                    Helper.GenerateTextbox(newRecordId, "MOMClause"),
                                    Helper.GenerateTextbox(newRecordId,"NextStep"),
                                    GenerateAutoCompleteOnBlur(newRecordId,"txtFunctions1","","","",false,"","Functions1")
                                         +  ""  + Helper.GenerateHidden(newRecordId,"Functions1"),
                                    MultiSelectDropdown(lstEmployee,newRecordId,"ResPerson1",false,"","","ResPerson1"),
                                   //Helper.GenerateTextbox(newRecordId,"Functions"),
                                   // Helper.GenerateTextbox(newRecordId,"ResPerson"),
                                   
                                   // Helper.MultiSelectDropdown(lstEmployee,"select",false,  "", "ResPerson"),
                                  // "<select name='ResPerson' id='ResPerson' multiple='multiple' style=' width:100 % ' class='form - control clresp'></select>",
                                    Helper.GenerateTextbox(newRecordId, "Date"),
                                    Helper.GenerateGridButton(newRecordId, "Add", "Add Rocord", "fa fa-plus", "SaveNewRecord();" ),
                                };


                var data = (from uc in lstResult
                            select new[]
                            {
                               Helper.GenerateHidden(uc.LineId, "LineId", Convert.ToString(uc.ROW_NO)),
                               Helper.GenerateHidden(uc.HeaderId, "HeaderId", Convert.ToString(uc.HeaderId)),
                               Helper.GenerateTextbox(uc.LineId, "Description", uc.Description , "UpdateLineDetails(this, "+ uc.LineId +");",(((status == clsImplementationEnum.CSRStatus.SendForPMGApproval.GetStringValue())) ? false : true)),
                               Helper.GenerateTextbox(uc.LineId, "MOMClause", uc.MOMClause , "UpdateLineDetails(this, "+ uc.LineId +");",(((status == clsImplementationEnum.CSRStatus.SendForPMGApproval.GetStringValue())) ? false : true)),
                               Helper.GenerateTextbox(uc.LineId, "NextStep", uc.NextStep , "UpdateLineDetails(this, "+ uc.LineId +");",(((status == clsImplementationEnum.CSRStatus.SendForPMGApproval.GetStringValue()) ) ? false : true)),
                               isEditable ? GenerateAutoCompleteOnBlur(uc.LineId,"txtFunctions1",uc.Functions,"UpdateLineDetails(this,"+ uc.LineId +","+ uc.HeaderId +")","",false,"width: 80px !important;","UpdateLineDetails",uc.Functions)
                               +"" +Helper.GenerateHidden(uc.LineId,"Functions1",uc.Functions) : uc.Functions,
                               MultiSelectDropdown(lstEmployee,uc.LineId,Convert.ToString(uc.ResPerson),(((status == clsImplementationEnum.CSRStatus.SendForPMGApproval.GetStringValue())) ? false : true),"UpdateLineDetails(this, "+ uc.LineId +");","","ResPerson1",uc.Functions),
                               Helper.GenerateTextbox(uc.LineId, "Date",Convert.ToDateTime(uc.Date).ToString("yyyy-MM-dd") , "UpdateLineDetails(this, "+ uc.LineId +");",(((status == clsImplementationEnum.CSRStatus.SendForPMGApproval.GetStringValue())) ? false : true)),
                               HTMLActionString(uc.LineId,"","Delete","Delete Record","fa fa-trash-o","DeleteRecord("+ uc.LineId +");"),
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult getInlineCSRLines2(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                string strSortOrder = string.Empty;
                int headerid = Convert.ToInt32(param.Headerid);
                var status = db.CSM001.Where(i => i.HeaderId == headerid).Select(i => i.Status).FirstOrDefault();
                bool isEditable = false;
                CSM001 objCSM001 = db.CSM001.FirstOrDefault(c => c.HeaderId == headerid);
                if (objCSM001 != null)
                {
                    if (objCSM001.Status == clsImplementationEnum.CSRStatus.SendForPMGApproval.GetStringValue())
                        isEditable = true;
                    else
                        isEditable = false;
                }
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere = " (Description like '%" + param.sSearch
                       + "%' or Project like '%" + param.sSearch
                       + "%' or MOMClause like '%" + param.sSearch
                       + "%' or NextStep like '%" + param.sSearch
                       + "%' or Functions like '%" + param.sSearch
                       + "%' or ResPerson +' - '+ i.t_name like '%" + param.sSearch
                       + "%' or Date like '%" + param.sSearch
                       + "%')";
                }
                else
                {
                    strWhere = "1=1";
                }
                strWhere += " and Groups = '2'";
                var lstResult = db.SP_CSR_GET_LINES
                                (
                                StartIndex, EndIndex, strSortOrder, headerid, strWhere
                                ).ToList();

                int newRecordId = 0;
                var proj = db.CSM001.Where(i => i.HeaderId == headerid).Select(i => i.Project).FirstOrDefault();
                List<SelectItemList> lstEmployee = new List<SelectItemList>();
                //if (proj != null)
                //    lstEmployee = getEmployeeAutocompletebyDept1(proj.ToString());
                var newRecord = new[] {
                                    Helper.GenerateHidden(newRecordId, "LineId", ""),
                                    Helper.GenerateHidden(newRecordId, "HeaderId", headerid.ToString()),
                                    Helper.GenerateTextbox(newRecordId, "Description"),
                                    Helper.GenerateTextbox(newRecordId, "MOMClause"),
                                     Helper.GenerateTextbox(newRecordId,"NextStep"),
                                     GenerateAutoCompleteOnBlur(newRecordId,"txtFunctions2","","","",false,"","Functions2")
                                         +  ""  + Helper.GenerateHidden(newRecordId,"Functions2"),
                                    //Helper.GenerateTextbox(newRecordId,"ResPerson"),
                                    MultiSelectDropdown(lstEmployee,newRecordId,"ResPerson2",false,"","","ResPerson2"),
                                    Helper.GenerateTextbox(newRecordId, "Date"),
                                    Helper.GenerateGridButton(newRecordId, "Add", "Add Rocord", "fa fa-plus", "SaveNewRecord2();" ),
                                };


                //var data = (from uc in lstResult
                //            select new[]
                //            {
                //               Helper.GenerateHidden(uc.LineId, "LineId", Convert.ToString(uc.ROW_NO)),
                //               Helper.GenerateHidden(uc.HeaderId, "HeaderId", Convert.ToString(uc.HeaderId)),
                //               Helper.GenerateTextbox(uc.LineId, "Description", uc.Description , "UpdateLineDetails2(this, "+ uc.LineId +");"),
                //               Helper.GenerateTextbox(uc.LineId, "MOMClause", uc.MOMClause , "UpdateLineDetails2(this, "+ uc.LineId +");"),
                //               Helper.GenerateTextbox(uc.LineId, "NextStep", uc.NextStep , "UpdateLineDetails2(this, "+ uc.LineId +");"),
                //              // GenerateAutoComplete(uc.LineId, "txtDept",uc.Department,"UpdateLineDetails2(this, "+ uc.LineId +");",false,"", "Department" , false) +""+
                //               Helper.GenerateTextbox(uc.LineId,"Functions",uc.Functions , "UpdateLineDetails2(this, "+ uc.LineId +");"),
                //              // GenerateAutoComplete(uc.LineId, "txtEmp",uc.ResponsiblePerson ,"UpdateLineDetails2(this, "+ uc.LineId +");",false,"", "ResponsiblePerson" , false) +""+ 
                //               Helper.GenerateTextbox(uc.LineId,"ResPerson",uc.ResPerson , "UpdateLineDetails2(this, "+ uc.LineId +");"),
                //               Helper.GenerateTextbox(uc.LineId, "Date",Convert.ToDateTime(uc.Date).ToString("yyyy-MM-dd") , "UpdateLineDetails2(this, "+ uc.LineId +");"),
                //               Helper.GenerateGridButton(uc.LineId, "Delete", "Delete Rocord", "fa fa-trash-o", "DeleteRecord2("+ uc.LineId +");"),
                //           }).ToList();


                var data = (from uc in lstResult
                            select new[]
                           {
                               Helper.GenerateHidden(uc.LineId, "LineId", Convert.ToString(uc.ROW_NO)),
                               Helper.GenerateHidden(uc.HeaderId, "HeaderId", Convert.ToString(uc.HeaderId)),
                               Helper.GenerateTextbox(uc.LineId, "Description", uc.Description , "UpdateLineDetails2(this, "+ uc.LineId +");",(((status == clsImplementationEnum.CSRStatus.SendForPMGApproval.GetStringValue())) ? false : true)),
                               Helper.GenerateTextbox(uc.LineId, "MOMClause", uc.MOMClause , "UpdateLineDetails2(this, "+ uc.LineId +");",(((status == clsImplementationEnum.CSRStatus.SendForPMGApproval.GetStringValue())) ? false : true)),
                               Helper.GenerateTextbox(uc.LineId, "NextStep", uc.NextStep , "UpdateLineDetails2(this, "+ uc.LineId +");",(((status == clsImplementationEnum.CSRStatus.SendForPMGApproval.GetStringValue()) ) ? false : true)),
                               isEditable ? GenerateAutoCompleteOnBlur(uc.LineId,"txtFunctions2",uc.Functions,"UpdateLineDetails2(this,"+ uc.LineId +","+ uc.HeaderId +")","",false,"width: 80px !important;","UpdateLineDetails2",uc.Functions)
                               +"" +Helper.GenerateHidden(uc.LineId,"Functions2",uc.Functions) : uc.Functions,
                               MultiSelectDropdown(lstEmployee,uc.LineId,Convert.ToString(uc.ResPerson),(((status == clsImplementationEnum.CSRStatus.SendForPMGApproval.GetStringValue())) ? false : true),"UpdateLineDetails2(this, "+ uc.LineId +");","","ResPerson2",uc.Functions),
                               Helper.GenerateTextbox(uc.LineId, "Date",Convert.ToDateTime(uc.Date).ToString("yyyy-MM-dd") , "UpdateLineDetails2(this, "+ uc.LineId +");",(((status == clsImplementationEnum.CSRStatus.SendForPMGApproval.GetStringValue())) ? false : true)),
                               HTMLActionString(uc.LineId,"","Delete","Delete Record","fa fa-trash-o","DeleteRecord2("+ uc.LineId +");"),
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult getInlineCSRLines3(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                string strSortOrder = string.Empty;
                int headerid = Convert.ToInt32(param.Headerid);
                var status = db.CSM001.Where(i => i.HeaderId == headerid).Select(i => i.Status).FirstOrDefault();
                bool isEditable = false;
                CSM001 objCSM001 = db.CSM001.FirstOrDefault(c => c.HeaderId == headerid);
                if (objCSM001 != null)
                {
                    if (objCSM001.Status == clsImplementationEnum.CSRStatus.SendForPMGApproval.GetStringValue())
                        isEditable = true;
                    else
                        isEditable = false;
                }
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere = " (Description like '%" + param.sSearch
                       + "%' or Project like '%" + param.sSearch
                       + "%' or MOMClause like '%" + param.sSearch
                       + "%' or NextStep like '%" + param.sSearch
                       + "%' or Functions like '%" + param.sSearch
                       + "%' or ResPerson +' - '+ i.t_name like '%" + param.sSearch
                       + "%' or Date like '%" + param.sSearch
                       + "%')";
                }
                else
                {
                    strWhere = "1=1";
                }
                strWhere += " and Groups = '3'";
                var lstResult = db.SP_CSR_GET_LINES
                                (
                                StartIndex, EndIndex, strSortOrder, headerid, strWhere
                                ).ToList();

                var proj = db.CSM001.Where(i => i.HeaderId == headerid).Select(i => i.Project).FirstOrDefault();
                List<SelectItemList> lstEmployee = new List<SelectItemList>();
                //if (proj != null)
                //    lstEmployee = getEmployeeAutocompletebyDept1(proj.ToString());
                int newRecordId = 0;
                var newRecord = new[] {
                                    Helper.GenerateHidden(newRecordId, "LineId", ""),
                                    Helper.GenerateHidden(newRecordId, "HeaderId", headerid.ToString()),
                                    Helper.GenerateTextbox(newRecordId, "Description"),
                                    Helper.GenerateTextbox(newRecordId, "MOMClause"),
                                     Helper.GenerateTextbox(newRecordId,"NextStep"),
                                    GenerateAutoCompleteOnBlur(newRecordId,"txtFunctions3","","","",false,"","Functions3")
                                         +  ""  + Helper.GenerateHidden(newRecordId,"Functions3"),
                                  //  Helper.GenerateTextbox(newRecordId,"ResPerson"),
                                   MultiSelectDropdown(lstEmployee,newRecordId,"ResPerson3",false,"","","ResPerson3"),
                                    Helper.GenerateTextbox(newRecordId, "Date"),
                                    Helper.GenerateGridButton(newRecordId, "Add", "Add Rocord", "fa fa-plus", "SaveNewRecord3();" ),
                                };


                //var data = (from uc in lstResult
                //            select new[]
                //            {
                //               Helper.GenerateHidden(uc.LineId, "LineId", Convert.ToString(uc.ROW_NO)),
                //               Helper.GenerateHidden(uc.HeaderId, "HeaderId", Convert.ToString(uc.HeaderId)),
                //               Helper.GenerateTextbox(uc.LineId, "Description", uc.Description , "UpdateLineDetails3(this, "+ uc.LineId +");"),
                //               Helper.GenerateTextbox(uc.LineId, "MOMClause", uc.MOMClause , "UpdateLineDetails3(this, "+ uc.LineId +");"),
                //               Helper.GenerateTextbox(uc.LineId, "NextStep", uc.NextStep , "UpdateLineDetails3(this, "+ uc.LineId +");"),
                //              // GenerateAutoComplete(uc.LineId, "txtDept",uc.Department,"UpdateLineDetails3(this, "+ uc.LineId +");",false,"", "Department" , false) +""+
                //               Helper.GenerateTextbox(uc.LineId,"Functions",uc.Functions , "UpdateLineDetails3(this, "+ uc.LineId +");"),
                //              // GenerateAutoComplete(uc.LineId, "txtEmp",uc.ResponsiblePerson ,"UpdateLineDetails3(this, "+ uc.LineId +");",false,"", "ResponsiblePerson" , false) +""+ 
                //               Helper.GenerateTextbox(uc.LineId,"ResPerson",uc.ResPerson , "UpdateLineDetails3(this, "+ uc.LineId +");"),
                //               Helper.GenerateTextbox(uc.LineId, "Date",Convert.ToDateTime(uc.Date).ToString("yyyy-MM-dd") , "UpdateLineDetails3(this, "+ uc.LineId +");"),
                //               Helper.GenerateGridButton(uc.LineId, "Delete", "Delete Rocord", "fa fa-trash-o", "DeleteRecord3("+ uc.LineId +");"),
                //           }).ToList();

                var data = (from uc in lstResult
                            select new[]
                            {
                               Helper.GenerateHidden(uc.LineId, "LineId", Convert.ToString(uc.ROW_NO)),
                               Helper.GenerateHidden(uc.HeaderId, "HeaderId", Convert.ToString(uc.HeaderId)),
                               Helper.GenerateTextbox(uc.LineId, "Description", uc.Description , "UpdateLineDetails3(this, "+ uc.LineId +");",(((status == clsImplementationEnum.CSRStatus.SendForPMGApproval.GetStringValue())) ? false : true)),
                               Helper.GenerateTextbox(uc.LineId, "MOMClause", uc.MOMClause , "UpdateLineDetails3(this, "+ uc.LineId +");",(((status == clsImplementationEnum.CSRStatus.SendForPMGApproval.GetStringValue())) ? false : true)),
                               Helper.GenerateTextbox(uc.LineId, "NextStep", uc.NextStep , "UpdateLineDetails3(this, "+ uc.LineId +");",(((status == clsImplementationEnum.CSRStatus.SendForPMGApproval.GetStringValue()) ) ? false : true)),
                               isEditable ? GenerateAutoCompleteOnBlur(uc.LineId,"txtFunctions3",uc.Functions,"UpdateLineDetails3(this,"+ uc.LineId +","+ uc.HeaderId +")","",false,"width: 80px !important;","UpdateLineDetails3",uc.Functions)
                               +"" +Helper.GenerateHidden(uc.LineId,"Functions3",uc.Functions) : uc.Functions,
                               MultiSelectDropdown(lstEmployee,uc.LineId,Convert.ToString(uc.ResPerson),(((status == clsImplementationEnum.CSRStatus.SendForPMGApproval.GetStringValue())) ? false : true),"UpdateLineDetails3(this, "+ uc.LineId +");","","ResPerson3",uc.Functions),
                               Helper.GenerateTextbox(uc.LineId, "Date",Convert.ToDateTime(uc.Date).ToString("yyyy-MM-dd") , "UpdateLineDetails3(this, "+ uc.LineId +");",(((status == clsImplementationEnum.CSRStatus.SendForPMGApproval.GetStringValue())) ? false : true)),
                               HTMLActionString(uc.LineId,"","Delete","Delete Record","fa fa-trash-o","DeleteRecord3("+ uc.LineId +");"),
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult getInlineCSRLines4(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                string strSortOrder = string.Empty;
                int headerid = Convert.ToInt32(param.Headerid);
                var status = db.CSM001.Where(i => i.HeaderId == headerid).Select(i => i.Status).FirstOrDefault();
                bool isEditable = false;
                CSM001 objCSM001 = db.CSM001.FirstOrDefault(c => c.HeaderId == headerid);
                if (objCSM001 != null)
                {
                    if (objCSM001.Status == clsImplementationEnum.CSRStatus.SendForPMGApproval.GetStringValue())
                        isEditable = true;
                    else
                        isEditable = false;
                }
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere = " (Description like '%" + param.sSearch
                       + "%' or Project like '%" + param.sSearch
                       + "%' or MOMClause like '%" + param.sSearch
                       + "%' or NextStep like '%" + param.sSearch
                       + "%' or Functions like '%" + param.sSearch
                       + "%' or ResPerson +' - '+ i.t_name like '%" + param.sSearch
                       + "%' or Date like '%" + param.sSearch
                       + "%')";
                }
                else
                {
                    strWhere = "1=1";
                }
                strWhere += " and Groups = '4'";
                var lstResult = db.SP_CSR_GET_LINES
                                (
                                StartIndex, EndIndex, strSortOrder, headerid, strWhere
                                ).ToList();

                var proj = db.CSM001.Where(i => i.HeaderId == headerid).Select(i => i.Project).FirstOrDefault();
                List<SelectItemList> lstEmployee = new List<SelectItemList>();
                //if (proj != null)
                //    lstEmployee = getEmployeeAutocompletebyDept1(proj.ToString());
                int newRecordId = 0;
                var newRecord = new[] {
                                    Helper.GenerateHidden(newRecordId, "LineId", ""),
                                    Helper.GenerateHidden(newRecordId, "HeaderId", headerid.ToString()),
                                    Helper.GenerateTextbox(newRecordId, "Description"),
                                    Helper.GenerateTextbox(newRecordId, "MOMClause"),
                                     Helper.GenerateTextbox(newRecordId,"NextStep"),
                                      GenerateAutoCompleteOnBlur(newRecordId,"txtFunctions4","","","",false,"","Functions4")
                                         +  ""  + Helper.GenerateHidden(newRecordId,"Functions4"),
                                  //  Helper.GenerateTextbox(newRecordId,"ResPerson"),
                                   MultiSelectDropdown(lstEmployee,newRecordId,"ResPerson4",false,"","","ResPerson4"),
                                    Helper.GenerateTextbox(newRecordId, "Date"),
                                    Helper.GenerateGridButton(newRecordId, "Add", "Add Rocord", "fa fa-plus", "SaveNewRecord4();" ),
                                };


                //var data = (from uc in lstResult
                //            select new[]
                //            {
                //               Helper.GenerateHidden(uc.LineId, "LineId", Convert.ToString(uc.ROW_NO)),
                //               Helper.GenerateHidden(uc.HeaderId, "HeaderId", Convert.ToString(uc.HeaderId)),
                //               Helper.GenerateTextbox(uc.LineId, "Description", uc.Description , "UpdateLineDetails3(this, "+ uc.LineId +");"),
                //               Helper.GenerateTextbox(uc.LineId, "MOMClause", uc.MOMClause , "UpdateLineDetails3(this, "+ uc.LineId +");"),
                //               Helper.GenerateTextbox(uc.LineId, "NextStep", uc.NextStep , "UpdateLineDetails3(this, "+ uc.LineId +");"),
                //              // GenerateAutoComplete(uc.LineId, "txtDept",uc.Department,"UpdateLineDetails3(this, "+ uc.LineId +");",false,"", "Department" , false) +""+
                //               Helper.GenerateTextbox(uc.LineId,"Functions",uc.Functions , "UpdateLineDetails3(this, "+ uc.LineId +");"),
                //              // GenerateAutoComplete(uc.LineId, "txtEmp",uc.ResponsiblePerson ,"UpdateLineDetails3(this, "+ uc.LineId +");",false,"", "ResponsiblePerson" , false) +""+ 
                //               Helper.GenerateTextbox(uc.LineId,"ResPerson",uc.ResPerson , "UpdateLineDetails3(this, "+ uc.LineId +");"),
                //               Helper.GenerateTextbox(uc.LineId, "Date",Convert.ToDateTime(uc.Date).ToString("yyyy-MM-dd") , "UpdateLineDetails3(this, "+ uc.LineId +");"),
                //               Helper.GenerateGridButton(uc.LineId, "Delete", "Delete Rocord", "fa fa-trash-o", "DeleteRecord3("+ uc.LineId +");"),
                //           }).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Helper.GenerateHidden(uc.LineId, "LineId", Convert.ToString(uc.ROW_NO)),
                               Helper.GenerateHidden(uc.HeaderId, "HeaderId", Convert.ToString(uc.HeaderId)),
                               Helper.GenerateTextbox(uc.LineId, "Description", uc.Description , "UpdateLineDetails4(this, "+ uc.LineId +");",(((status == clsImplementationEnum.CSRStatus.SendForPMGApproval.GetStringValue())) ? false : true)),
                               Helper.GenerateTextbox(uc.LineId, "MOMClause", uc.MOMClause , "UpdateLineDetails4(this, "+ uc.LineId +");",(((status == clsImplementationEnum.CSRStatus.SendForPMGApproval.GetStringValue())) ? false : true)),
                               Helper.GenerateTextbox(uc.LineId, "NextStep", uc.NextStep , "UpdateLineDetails4(this, "+ uc.LineId +");",(((status == clsImplementationEnum.CSRStatus.SendForPMGApproval.GetStringValue()) ) ? false : true)),
                               isEditable ? GenerateAutoCompleteOnBlur(uc.LineId,"txtFunctions4",uc.Functions,"UpdateLineDetails4(this,"+ uc.LineId +","+ uc.HeaderId +")","",false,"width: 80px !important;","UpdateLineDetails4",uc.Functions)
                               +"" +Helper.GenerateHidden(uc.LineId,"Functions4",uc.Functions) : uc.Functions,
                               MultiSelectDropdown(lstEmployee,uc.LineId,Convert.ToString(uc.ResPerson),(((status == clsImplementationEnum.CSRStatus.SendForPMGApproval.GetStringValue())) ? false : true),"UpdateLineDetails4(this, "+ uc.LineId +");","","ResPerson4",uc.Functions),
                               Helper.GenerateTextbox(uc.LineId, "Date",Convert.ToDateTime(uc.Date).ToString("yyyy-MM-dd") , "UpdateLineDetails4(this, "+ uc.LineId +");",(((status == clsImplementationEnum.CSRStatus.SendForPMGApproval.GetStringValue())) ? false : true)),
                               HTMLActionString(uc.LineId,"","Delete","Delete Record","fa fa-trash-o","DeleteRecord4("+ uc.LineId +");"),
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }

        public string GenerateAutoCompleteOnBlur(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", string onClickMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", string oldValue = "", bool disabled = false)
        {
            string strAutoComplete = string.Empty;
            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "autocomplete form-control";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick=" + onClickMethod + "" : "";
            strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' data-lineid='" + rowId + "' hdElement='" + hdElementId + "' value='" + inputValue + "' data-oldvalue='" + oldValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + onClickEvent + " " + (disabled ? "disabled" : "") + " />";

            return strAutoComplete;
        }
        [HttpPost]
        public JsonResult GetEmployeeResultFunctions(string Dept)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            List<Employee> lstEmployee = getEmployeeAutocompletebyFunctions(Dept).ToList();
            objResponseMsg.lsttask = lstEmployee;
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public string MultiSelectDropdown(List<SelectItemList> list, int rowID, string selectedValue, bool Disabled = false, string OnChangeEvent = "", string OnBlurEvent = "", string ColumnName = "", string functions = "")
        {
            string multipleSelect = string.Empty;
            string[] arrayVal = { };

            try
            {
                if (!string.IsNullOrWhiteSpace(selectedValue))
                {
                    arrayVal = selectedValue.Split(',');
                }

                // multipleSelect += "<select data-name='ddlmultiple' name='ddlmultiple" + rowID + "' id='ddlmultiple" + rowID + "' data-lineid='" + rowID + "' multiple='multiple'  style='width: 100 % ' colname='" + ColumnName + "' class='form-control multiselect-drodown' " + (Disabled ? "disabled " : "") + (OnChangeEvent != string.Empty ? "onchange=" + OnChangeEvent + "" : "") + (OnBlurEvent != string.Empty ? " onblur='" + OnBlurEvent + "'" : "") + " >";
                multipleSelect += "<select data-name='ddlmultiple' name='ddlmultiple" + rowID + "' id='ddlmultiple" + rowID + "' data-lineid='" + rowID + "' multiple='multiple'  style='width: 100 % ' colname='" + ColumnName + "' class='form-control multiselect-drodown personMultiselect' " + (Disabled ? "disabled " : "") + (OnChangeEvent != string.Empty ? "onchange='" + OnChangeEvent + "'" : "") + (OnBlurEvent != string.Empty ? " onblur='" + OnBlurEvent + "'" : "") + " >";
                //foreach (var item in list)
                //{
                //    multipleSelect += "<option value='" + item.id + "' " + ((arrayVal.Contains(item.id)) ? " selected" : "") + " >" + item.text + "</option>";
                //}
                if (!string.IsNullOrWhiteSpace(functions))
                {
                    List<Employee> lstEmployee = getEmployeeAutocompletebyFunctions(functions).ToList();
                    foreach (var item in lstEmployee)
                    {
                        multipleSelect += "<option value='" + item.psnum + "' " + ((arrayVal.Contains(item.psnum)) ? " selected" : "") + " >" + item.name + "</option>";
                    }
                }
                multipleSelect += "</select>";
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return multipleSelect;
        }
        public List<Employee> getEmployeeAutocompletebyFunctions(string dept)
        {
            List<Employee> lstEmployee = new List<Employee>();
            int[] role = new int[5];
            if (dept == "WE")
            {
                role[0] = 3;
                role[1] = 4;
                role[2] = 5;
            }
            if (dept == "MCC")
            {
                role[0] = 65;
                role[1] = 74;
            }
            if (dept == "PCC")
            {
                role[0] = 32;
                role[1] = 33;
                role[2] = 35;
            }
            if (dept == "NDE")
            {
                role[0] = 16;
                role[1] = 17;
            }
            if (dept == "QA/QC")
            {
                role[0] = 14;
                role[1] = 36;
                role[2] = 9;
                role[3] = 10;
                role[4] = 15;
            }
            if (dept == "PMG")
            {
                role[0] = 1;
                role[1] = 2;
                role[2] = 24;
            }
            if (dept == "DCC")
            {
                role[0] = 6;
                role[1] = 7;
                role[2] = 8;
            }
            lstEmployee = (from a in db.COM003
                           join b in db.ATH001 on a.t_psno equals b.Employee
                           where role.Contains(b.Role)
                           select
                           new Employee { psnum = a.t_psno, name = a.t_psno + " - " + a.t_name }).Distinct().ToList();

            return lstEmployee;
        }
        [HttpPost]
        public ActionResult DeleteLine(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                CSM002 objCSM002 = db.CSM002.Where(x => x.LineId == Id).FirstOrDefault();
                db.CSM002.Remove(objCSM002);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Delete.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateData(int headerId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
                {
                    if (columnName == "Date")
                    {
                        if (CheckDate(columnValue))
                        {
                            db.SP_CSR_MOM_UPDATE_COLUMN(Convert.ToInt32(headerId), columnName, columnValue);
                            objResponseMsg.Key = true;
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "Invalid Date";
                        }
                    }
                    else if (columnName == "ResPerson")
                    {
                        columnValue = columnValue.Split('-')[0].ToString();
                        db.SP_CSR_MOM_UPDATE_COLUMN(Convert.ToInt32(headerId), columnName, columnValue);
                        objResponseMsg.Key = true;

                    }
                    else if (columnName == "txtFunctions")
                    {
                        columnName = "Functions";
                        db.SP_CSR_MOM_UPDATE_COLUMN(Convert.ToInt32(headerId), columnName, columnValue);
                        objResponseMsg.Key = true;

                    }
                    else
                    {
                        db.SP_CSR_MOM_UPDATE_COLUMN(Convert.ToInt32(headerId), columnName, columnValue);
                        objResponseMsg.Key = true;
                    }


                    //db.SP_CSR_MOM_UPDATE_COLUMN(LineId, columnName, columnValue);
                    //ReviseData(headerId);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        protected bool CheckDate(String date)
        {
            try
            {
                DateTime dt = DateTime.Parse(date);
                return true;
            }
            catch
            {
                return false;
            }
        }

        [HttpPost]
        public JsonResult createRevison(int headerid)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                int? revison = (from a in db.CSM001
                                where a.HeaderId == headerid
                                select a.RevNo).FirstOrDefault();
                revison = revison + 1;
                db.SP_CSR_Revision(headerid, revison, objClsLoginInfo.UserName);
                objResponseMsg.Key = true;
                objResponseMsg.Status = "Draft";
                objResponseMsg.Revision = "R" + revison.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReviseData(int headerId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                CSM001 objCSM001 = db.CSM001.Where(x => x.HeaderId == headerId).FirstOrDefault();
                if (objCSM001.Status == clsImplementationEnum.CSRStatus.ApprovedByPMG.GetStringValue())
                {
                    objCSM001.RevNo = Convert.ToInt32(objCSM001.RevNo) + 1;
                    objCSM001.Status = clsImplementationEnum.CSRStatus.Draft.GetStringValue();
                    objCSM001.EditedBy = objClsLoginInfo.UserName;
                    objCSM001.EditedOn = DateTime.Now;
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetEmployeeResult(string term)
        {
            List<Employee> lstEmployee = getEmployeeAutocompletebyDept(term).ToList();
            return Json(lstEmployee, JsonRequestBehavior.AllowGet);
        }
        public List<Employee> getEmployeeAutocompletebyDept(string term)
        {
            List<Employee> lstEmployee = new List<Employee>();
            lstEmployee = (from a in db.COM003
                           where (a.t_actv == 1 && (a.t_psno.Contains(term) ||
                           a.t_name.Contains(term) ||
                           (a.t_psno + " - " + a.t_name).Contains(term)))
                           select
                           new Employee { psnum = a.t_psno, name = a.t_psno + " - " + a.t_name }).ToList();
            return lstEmployee;
        }

        public List<SelectItemList> getEmployeeAutocompletebyDept1(String Prj)
        {
            List<SelectItemList> lstEmployee = new List<SelectItemList>();
            //lstEmployee = (from a in db.COM003
            //               where (a.t_actv == 1
            //               )
            //               select
            //               new SelectItemList { id = a.t_psno, text = a.t_psno + " - " + a.t_name }).ToList();
            //lstEmployee = (from a in db.COM003
            //               where (a.t_actv == 1
            //               )
            //               select
            //               new SelectItemList { id = a.t_psno, text = a.t_psno + " - " + a.t_name }).ToList();

            lstEmployee = db.ATH001.
                 Join(db.COM003, x => x.Employee, y => y.t_psno,
                 (x, y) => new { x, y })
                 .Where(z => z.y.t_actv == 1 && (z.x.Project == Prj || z.x.Project == "ALL"))
                 .Distinct()
                 .Select(z => new SelectItemList
                 {
                     id = z.x.Employee,
                     text = z.x.Employee + " - " + z.y.t_name
                 }).Distinct().ToList();



            return lstEmployee;
        }


        [HttpPost]
        public ActionResult SaveNewLines(FormCollection fc)
        {
            int newRowIndex = 0;
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int headerId = Convert.ToInt32(fc["txtHeaderId"]);
                int hd = Convert.ToInt32(fc["HeaderId0"].ToString());
                //int headerId = Convert.ToInt32(fc["HeaderId" + newRowIndex]);
                CSM001 objCSM001 = db.CSM001.Where(x => x.HeaderId == hd).FirstOrDefault();
                CSM002 objCSM002 = new CSM002();
                objCSM002.HeaderId = hd;
                objCSM002.Project = objCSM001.Project;
                objCSM002.Groups = 1;
                objCSM002.Description = fc["Description" + newRowIndex];
                objCSM002.MOMClause = fc["MOMClause" + newRowIndex];
                objCSM002.NextStep = fc["NextStep" + newRowIndex];
                objCSM002.Functions = fc["Functions" + newRowIndex];
                objCSM002.ResPerson = fc["ddlmultiple0"].ToString().Split('-')[0].Trim();
                objCSM002.Date = Convert.ToDateTime(fc["Date0"].ToString());
                objCSM002.RevNo = 0;
                objCSM002.CreatedBy = objClsLoginInfo.UserName;
                objCSM002.CreatedOn = DateTime.Now;

                db.CSM002.Add(objCSM002);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveNewLines2(FormCollection fc)
        {
            int newRowIndex = 0;
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int headerId = Convert.ToInt32(fc["txtHeaderId"]);
                int hd = Convert.ToInt32(fc["HeaderId0"].ToString());
                //int headerId = Convert.ToInt32(fc["HeaderId" + newRowIndex]);
                CSM001 objCSM001 = db.CSM001.Where(x => x.HeaderId == hd).FirstOrDefault();
                CSM002 objCSM002 = new CSM002();
                objCSM002.HeaderId = hd;
                objCSM002.Project = objCSM001.Project;
                objCSM002.Groups = 2;
                objCSM002.Description = fc["Description" + newRowIndex];
                objCSM002.MOMClause = fc["MOMClause" + newRowIndex];
                objCSM002.NextStep = fc["NextStep" + newRowIndex];
                objCSM002.Functions = fc["Functions2" + newRowIndex];
                objCSM002.ResPerson = fc["ddlmultiple0"].ToString().Split('-')[0].Trim();
                objCSM002.Date = Convert.ToDateTime(fc["Date0"].ToString());
                objCSM002.RevNo = 0;
                objCSM002.CreatedBy = objClsLoginInfo.UserName;
                objCSM002.CreatedOn = DateTime.Now;

                db.CSM002.Add(objCSM002);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult SaveNewLines3(FormCollection fc)
        {
            int newRowIndex = 0;
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int headerId = Convert.ToInt32(fc["txtHeaderId"]);
                int hd = Convert.ToInt32(fc["HeaderId0"].ToString());
                //int headerId = Convert.ToInt32(fc["HeaderId" + newRowIndex]);
                CSM001 objCSM001 = db.CSM001.Where(x => x.HeaderId == hd).FirstOrDefault();
                CSM002 objCSM002 = new CSM002();
                objCSM002.HeaderId = hd;
                objCSM002.Project = objCSM001.Project;
                objCSM002.Groups = 3;
                objCSM002.Description = fc["Description" + newRowIndex];
                objCSM002.MOMClause = fc["MOMClause" + newRowIndex];
                objCSM002.NextStep = fc["NextStep" + newRowIndex];
                objCSM002.Functions = fc["Functions3" + newRowIndex];
                objCSM002.ResPerson = fc["ddlmultiple0"].ToString().Split('-')[0].Trim();
                objCSM002.Date = Convert.ToDateTime(fc["Date0"].ToString());
                objCSM002.RevNo = 0;
                objCSM002.CreatedBy = objClsLoginInfo.UserName;
                objCSM002.CreatedOn = DateTime.Now;

                db.CSM002.Add(objCSM002);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveNewLines4(FormCollection fc)
        {
            int newRowIndex = 0;
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int headerId = Convert.ToInt32(fc["txtHeaderId"]);
                int hd = Convert.ToInt32(fc["HeaderId0"].ToString());
                //int headerId = Convert.ToInt32(fc["HeaderId" + newRowIndex]);
                CSM001 objCSM001 = db.CSM001.Where(x => x.HeaderId == hd).FirstOrDefault();
                CSM002 objCSM002 = new CSM002();
                objCSM002.HeaderId = hd;
                objCSM002.Project = objCSM001.Project;
                objCSM002.Groups = 4;
                objCSM002.Description = fc["Description" + newRowIndex];
                objCSM002.MOMClause = fc["MOMClause" + newRowIndex];
                objCSM002.NextStep = fc["NextStep" + newRowIndex];
                objCSM002.Functions = fc["Functions4" + newRowIndex];
                objCSM002.ResPerson = fc["ddlmultiple0"].ToString().Split('-')[0].Trim();
                objCSM002.Date = Convert.ToDateTime(fc["Date0"].ToString());
                objCSM002.RevNo = 0;
                objCSM002.CreatedBy = objClsLoginInfo.UserName;
                objCSM002.CreatedOn = DateTime.Now;

                db.CSM002.Add(objCSM002);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public class ResponceMsgWithHeaderID : clsHelper.ResponseMsg
        {
            public int HeaderID;
        }
        public class ResponceMsgWithStatus : clsHelper.ResponseMsg
        {
            public string Status;
            public string Revision;
            public string CreatedBy;
            public string projdesc;
            public string appdesc;
            public string app2desc;
            public string customer;
            public List<Employee> lsttask { get; set; }
        }

        [HttpPost]
        //public ActionResult SaveHeader(CSM001 csm001)
        public ActionResult SaveHeader(CSM001 csm001, bool hasAttachments, Dictionary<string, string> Attach)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                string revnum = "";
                if (csm001.HeaderId > 0)
                {
                    CSM001 objcsm001 = db.CSM001.Where(x => x.HeaderId == csm001.HeaderId).FirstOrDefault();
                    objcsm001.Project = csm001.Project;
                    objcsm001.Customer = csm001.Customer;
                    objcsm001.Equipment = csm001.Equipment;
                    objcsm001.PO = csm001.PO;
                    objcsm001.PMGApproveBy = csm001.PMGApproveBy;
                    objcsm001.ApprovedBy = csm001.ApprovedBy.ToString().Split('-')[0].Trim();
                    objcsm001.Approved2By = csm001.Approved2By.ToString().Split('-')[0].Trim();
                    objcsm001.EditedBy = objClsLoginInfo.UserName;
                    objcsm001.EditedOn = DateTime.Now;
                    if (objcsm001.Status == clsImplementationEnum.CSRStatus.ApprovedByPMG.GetStringValue())
                    {

                        objcsm001.RevNo = Convert.ToInt32(objcsm001.RevNo) + 1;
                        revnum = objcsm001.RevNo.ToString();
                        objcsm001.Status = clsImplementationEnum.CSRStatus.Draft.GetStringValue();


                    }
                    else
                    {
                        revnum = objcsm001.RevNo.ToString();
                    }
                    db.SaveChanges();
                    objResponseMsg.HeaderID = objcsm001.HeaderId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CSRMessages.Update.ToString();
                    var folderPath = "CSM001/" + csm001.HeaderId ;
                    Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                }
                else
                {
                    CSM001 objcsm001 = new CSM001();
                    objcsm001.Project = csm001.Project;
                    objcsm001.RevNo = 0;
                    objcsm001.Equipment = csm001.Equipment;
                    objcsm001.PO = csm001.PO;
                    objcsm001.Status = csm001.Status;
                    objcsm001.PMGApproveBy = csm001.PMGApproveBy;
                    objcsm001.ApprovedBy = csm001.ApprovedBy.ToString().Split('-')[0].Trim();
                    objcsm001.Approved2By = csm001.Approved2By.ToString().Split('-')[0].Trim();
                    objcsm001.CreatedBy = objClsLoginInfo.UserName;
                    objcsm001.CreatedOn = DateTime.Now;
                    db.CSM001.Add(objcsm001);
                    db.SaveChanges();
                    objResponseMsg.HeaderID = objcsm001.HeaderId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CSRMessages.Insert.ToString();
                    var folderPath = "CSM001/" + csm001.HeaderId ;
                    Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);

                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [NonAction]
        public static string GenerateGridButton(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";


            htmlControl = "<a id='" + inputID + "' name='" + inputID + "' class='btn btn-outline btn-circle btn-sm blue' " + onClickEvent + " ><i class='" + className + "'></i> " + buttonName + "</a>";

            //htmlControl = "<i  data-modal='' id='" + inputID + "' name='" + inputID + "' style='cursor:Pointer;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";

            return htmlControl;
        }


        public static string GenerateHiddenFor(int rowId, string columnName, string columnValue)
        {
            string htmlControl = "";

            string inputID = columnName + "" + rowId.ToString();
            string inputValue = columnValue;

            htmlControl = "<input type='hidden' id='" + inputID + "' value='" + inputValue + "' name='" + inputID + "' colname='" + columnName + "'/>";

            return htmlControl;
        }


        public string HTMLActionString(int rowId, string status, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";
            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            if (!string.Equals(status, clsImplementationEnum.CSRStatus.SendForPMGApproval.GetStringValue()))
            {
                if (string.Equals(status, clsImplementationEnum.CSRStatus.ApprovedByPMG.GetStringValue()))
                {
                    htmlControl = "";// "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
                }
                else
                {
                    htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
                }
            }

            return htmlControl;
        }


        public static string GenerateTextboxFor(int rowId, string status, string columnName, string columnValue = "", string onBlurMethod = "", string onClickMethod = "", bool isReadOnly = false, string inputStyle = "", string maxLength = "")
        {
            string htmlControl = "";

            string inputID = columnName + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "form-control";

            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onChange='" + onBlurMethod + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";
            if (string.Equals(status, clsImplementationEnum.CSRStatus.SendForPMGApproval.GetStringValue()))
            {
                htmlControl = "<input disabled type='text' id='" + inputID + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " maxlength='" + maxLength + "'  />";
            }
            else
            {
                htmlControl = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + onClickEvent + " maxlength='" + maxLength + "'  />";
            }

            return htmlControl;
        }

        [NonAction]
        public static string GenerateDropdown(int rowId, string status, string columnName, SelectList itemList, string defaultSelectionText = "", string onChangeMethod = "", string OnClickMethod = "", bool isReadOnly = false, string inputStyle = "")
        {
            string selectOptions = "";
            string inputID = columnName + "" + rowId.ToString();
            string inputName = columnName;
            string className = "form-control col-md-3";
            string onChangedEvent = !string.IsNullOrEmpty(onChangeMethod) ? "onchange='" + onChangeMethod + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(OnClickMethod) ? "onclick='" + OnClickMethod + "'" : "";
            if (defaultSelectionText.Length > 0)
            {
                selectOptions += "<option value=''>" + defaultSelectionText + "</option>";
            }

            foreach (var item in itemList)
            {
                if (item.Selected)
                {
                    selectOptions += "<option selected value=" + item.Value + ">" + item.Text + "</option>";
                }
                else
                {
                    selectOptions += "<option value=" + item.Value + ">" + item.Text + "</option>";
                }
            }
            if (string.Equals(status, clsImplementationEnum.CSRStatus.SendForPMGApproval.GetStringValue()))
            {
                return "<select id='" + inputID + "' disabled name='" + inputID + "' " + onChangedEvent + " colname='" + columnName + "' class='" + className + "' style='" + inputStyle + "' >" + selectOptions + "</select>";
            }
            else
            {
                return "<select id='" + inputID + "' name='" + inputID + "' " + onChangedEvent + " colname='" + columnName + "' class='" + className + "' style='" + inputStyle + "'  " + onClickEvent + " >" + selectOptions + "</select>";
            }
        }


        //public ActionResult CSRDetail(int Id = 0)
        //{
        //    CSM001 objCSR001 = new CSM001();
        //    return View(objCSR001);
        //}

        [HttpPost]
        public ActionResult GetHeaderGridDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_PMGCSRListDataPartial");
        }

        [HttpPost]
        public JsonResult LoadCSRHeaderData(JQueryDataTableParamModel param, string Project)
        {
            try
            {
                if (Project != null && Project != "")
                {
                    var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                    var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                    int StartIndex = param.iDisplayStart + 1;
                    int EndIndex = param.iDisplayStart + param.iDisplayLength;
                    var user = objClsLoginInfo.UserName;
                    string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                    string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                    string strSortOrder = string.Empty;
                    string strWhere = string.Empty;
                    if (param.CTQCompileStatus.ToUpper() == "PENDING")
                    {
                        strWhere += "1=1 and status in('" + clsImplementationEnum.CSRStatus.SendForPMGApproval.GetStringValue() + "','" + clsImplementationEnum.CSRStatus.ReturnedByPMG.GetStringValue() + "')";
                    }
                    else
                    {
                        strWhere += "1=1";
                    }
                    strWhere += " and Approved2By=" + user;

                    if (!string.IsNullOrWhiteSpace(sortColumnName))
                    {
                        strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                    }

                    if (!string.IsNullOrWhiteSpace(param.sSearch))
                    {
                        strWhere += "and (Project like '%" + param.sSearch
                            + "%' or Equipment like '%" + param.sSearch
                            + "%' or Customer like '%" + param.sSearch
                            + "%' or RevNo like '%" + param.sSearch
                            + "%' or PO like '%" + param.sSearch
                            + "%' or Status like '%" + param.sSearch + "%')";


                    }
                    {
                        strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                    }
                    strWhere += " and Project='" + Project.ToString() + "'";
                    //strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName);
                    var lstResult = db.SP_CSR_GET_HEADERDETAILS
                                    (
                                    StartIndex, EndIndex, strSortOrder, strWhere
                                    ).ToList();

                    lstResult = lstResult.Where(x => x.Project.Contains(Project)).ToList();

                    var data = (from uc in lstResult
                                select new[]
                               {
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.Equipment),
                               Convert.ToString(uc.Customer),
                               Convert.ToString(uc.PO),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.Status),

                               "<center><a class='btn btn-xs' href='"+WebsiteURL+"/CSR/ApprovePMGCSR/AddUpdatePMGCSR?HeaderID="+Convert.ToInt32(uc.HeaderId)+" &Project=" +Project+ "'><i style='margin-left:5px;' class='fa fa-eye'></i></a>"+
                               (uc.RevNo > 0 ? HTMLActionString(uc.HeaderId,"","History","History Record","fa fa-history","ViewHistoryData("+ Convert.ToInt32(uc.HeaderId) +");") : HTMLActionString(uc.HeaderId,"","History","History Record","fa fa-history disabledicon","ViewHistoryData('"+ Convert.ToInt32(uc.HeaderId) +"');")) +
                               "<i style='margin-left:5px;cursor:pointer;' title='Print Report' class='fa fa-print' onClick='PrintReport("+uc.HeaderId+")'></i></center>"
                           }).ToList();

                    return Json(new
                    {
                        sEcho = Convert.ToInt32(param.sEcho),
                        iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                        iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                        aaData = data
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                    var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                    int StartIndex = param.iDisplayStart + 1;
                    int EndIndex = param.iDisplayStart + param.iDisplayLength;
                    var user = objClsLoginInfo.UserName;
                    string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                    string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                    string strSortOrder = string.Empty;
                    string strWhere = string.Empty;
                    if (param.CTQCompileStatus.ToUpper() == "PENDING")
                    {
                        strWhere += "1=1 and status in('" + clsImplementationEnum.CSRStatus.SendForPMGApproval.GetStringValue() + "','" + clsImplementationEnum.CSRStatus.ReturnedByPMG.GetStringValue() + "')";
                    }
                    else
                    {
                        strWhere += "1=1";
                    }
                    strWhere += " and Approved2By=" + user;

                    if (!string.IsNullOrWhiteSpace(sortColumnName))
                    {
                        strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                    }

                    if (!string.IsNullOrWhiteSpace(param.sSearch))
                    {
                        strWhere += "and (Project like '%" + param.sSearch
                            + "%' or Equipment like '%" + param.sSearch
                            + "%' or Customer like '%" + param.sSearch
                            + "%' or RevNo like '%" + param.sSearch
                            + "%' or PO like '%" + param.sSearch
                            + "%' or Status like '%" + param.sSearch + "%')";


                    }
                    {
                        strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                    }
                    //strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName);
                    var lstResult = db.SP_CSR_GET_HEADERDETAILS
                                    (
                                    StartIndex, EndIndex, strSortOrder, strWhere
                                    ).ToList();

                    var data = (from uc in lstResult
                                select new[]
                               {
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.Equipment),
                               Convert.ToString(uc.Customer),
                               Convert.ToString(uc.PO),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.Status),

                               "<center><a class='btn btn-xs' href='"+WebsiteURL+"/CSR/ApprovePMGCSR/AddUpdatePMGCSR?HeaderID="+Convert.ToInt32(uc.HeaderId)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a>"+
                               (uc.RevNo > 0 ? HTMLActionString(uc.HeaderId,"","History","History Record","fa fa-history","ViewHistoryData("+ Convert.ToInt32(uc.HeaderId) +");") : HTMLActionString(uc.HeaderId,"","History","History Record","fa fa-history disabledicon","ViewHistoryData('"+ Convert.ToInt32(uc.HeaderId) +"');")) +
                               "<i style='margin-left:5px;cursor:pointer;' title='Print Report' class='fa fa-print' onClick='PrintReport("+uc.HeaderId+")'></i></center>"
                           }).ToList();

                    return Json(new
                    {
                        sEcho = Convert.ToInt32(param.sEcho),
                        iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                        iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                        aaData = data
                    }, JsonRequestBehavior.AllowGet);
                }
                    
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }




        [HttpPost]
        public ActionResult SendForApproval(int headerid)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                CSM001 objCSM001 = db.CSM001.Where(x => x.HeaderId == headerid).FirstOrDefault();
                {
                    if (objCSM001 != null)
                    {
                        //objCSM001.Status = clsImplementationEnum.CSRStatus.ApprovedByPMG.GetStringValue();
                        //objCSM001.EditedBy = objClsLoginInfo.UserName;
                        //objCSM001.EditedOn = DateTime.Now;
                        //db.SaveChanges();
                        db.SP_CSR_APPROVE(headerid, objClsLoginInfo.UserName);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Approved Successfully..!!";

                        #region Send Mail
                        Hashtable _ht = new Hashtable();
                        EmailSend _objEmail = new EmailSend();
                        _ht["[Approver]"] = Manager.GetUserNameFromPsNo(objCSM001.Approved2By);
                        _ht["[CSR No]"] = objCSM001.Project;
                        _ht["[Initiator]"] = Manager.GetUserNameFromPsNo(objCSM001.CreatedBy);
                        MAIL001 objTemplateMaster = db.MAIL001.Where(ii => ii.TamplateName == MailTemplates.CSR.CSRApprovedbyPMG).SingleOrDefault();
                        _objEmail.MailToAdd = Manager.GetMailIdFromPsNo(objCSM001.CreatedBy);
                        _ht["[Subject]"] = "CSR " + objCSM001.Project + " is Complete";
                        _objEmail.SendMail(_objEmail, _ht, objTemplateMaster);
                        #endregion

                        CSM002 objCSM002 = db.CSM002.Where(x => x.HeaderId == headerid).FirstOrDefault();
                        #region Send Mail
                        Hashtable _ht1 = new Hashtable();
                        EmailSend _objEmail1 = new EmailSend();
                        _ht1["[Approver]"] = Manager.GetUserNameFromPsNo(objCSM001.Approved2By);
                        _ht1["[CSR No]"] = objCSM001.Project;
                        _ht1["[Initiator]"] = Manager.GetUserNameFromPsNo(objCSM001.CreatedBy);
                        MAIL001 objTemplateMaster1 = db.MAIL001.Where(ii => ii.TamplateName == MailTemplates.CSR.CSRApprovedbyPMGGroup).SingleOrDefault();
                        string[] lstResPerson = objCSM002.ResPerson.Split(',').ToArray();
                        string mailTo = "";
                        foreach (var lst in lstResPerson)
                        {
                            mailTo = mailTo + Manager.GetMailIdFromPsNo(lst) + ";";
                        }
                        _objEmail1.MailToAdd = mailTo;
                        _ht1["[Subject]"] = "CSR " + objCSM001.Project + " is Complete";
                        _objEmail.SendMail(_objEmail1, _ht1, objTemplateMaster1);
                        #endregion

                    }
                    else
                    {
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Details not available for approval.";
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CSRMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult SendForReturn(int headerid, string remarks)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                CSM001 objCSM001 = db.CSM001.Where(x => x.HeaderId == headerid).FirstOrDefault();
                {
                    if (objCSM001 != null)
                    {
                        objCSM001.Status = clsImplementationEnum.CSRStatus.ReturnedByPMG.GetStringValue();
                        objCSM001.EditedBy = objClsLoginInfo.UserName;
                        objCSM001.EditedOn = DateTime.Now;
                        objCSM001.ReturnRemark = remarks;
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Details successfully sent for Return.";

                        #region Send Mail
                        Hashtable _ht = new Hashtable();
                        EmailSend _objEmail = new EmailSend();
                        _ht["[Approver]"] = Manager.GetUserNameFromPsNo(objCSM001.ApprovedBy);
                        _ht["[CSR No]"] = objCSM001.Project;
                        _ht["[Initiator]"] = Manager.GetUserNameFromPsNo(objCSM001.CreatedBy);
                        MAIL001 objTemplateMaster = db.MAIL001.Where(ii => ii.TamplateName == MailTemplates.CSR.CSRReturnedbyPMG).SingleOrDefault();
                        _objEmail.MailToAdd = Manager.GetMailIdFromPsNo(objCSM001.CreatedBy);
                        _ht["[Subject]"] = "CSR " + objCSM001.Project + " Returned";
                        _objEmail.SendMail(_objEmail, _ht, objTemplateMaster);
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Details not available for Return.";
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CSRMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult getCodeValue(string project, string approver, string approver2, string CreatedBy)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                string name = (from a in db.COM003
                               where a.t_psno == CreatedBy && a.t_actv == 1
                               select a.t_name).FirstOrDefault();

                objResponseMsg.CreatedBy = CreatedBy + " - " + name;
                string customer = Manager.GetCustomerCodeAndNameByProject(project);
                objResponseMsg.customer = customer;

                objResponseMsg.projdesc = db.COM001.Where(i => i.t_cprj == project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objResponseMsg.appdesc = db.COM003.Where(i => i.t_psno == approver && i.t_actv == 1).Select(i => i.t_psno + " - " + i.t_name).FirstOrDefault();
                objResponseMsg.app2desc = db.COM003.Where(i => i.t_psno == approver2 && i.t_actv == 1).Select(i => i.t_psno + " - " + i.t_name).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
    }
}