﻿using Excel.Log;
using IEMQSImplementation;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices.ComTypes;
using System.Security.Cryptography;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PTR.Controllers
{
    public class ReportPTRController : clsBase
    {
        // GET: PTR/ReportPTRSaveHardnessHeaderData

        string ControllerURL = "/PTR/ReportPTR";

        public ActionResult Index(int? Id)
        {
            return View();
        }

        #region COVER

        public ActionResult Cover(int? PTRHeaderId)
        {
            #region ViewBag
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstYesNo = new List<string> { "Yes", "No" };
            ViewBag.lstYesNo = lstYesNo.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #endregion

            PTR011 _objPTR011 = new PTR011();
            if (PTRHeaderId.HasValue && db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).Count() > 0)
            {
                _objPTR011 = db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).FirstOrDefault();
                //ViewBag.lstPTR011_Log = db.PTR011_Log.Where(w => w.HeaderId == _objPTR011.HeaderId).ToList();
                _objPTR011.PTR011_Log = db.PTR011_Log.Where(w => w.HeaderId == _objPTR011.HeaderId).ToList();
            }
            else
            {



                PTR001 _lstPTR001 = db.PTR001.Where(x => x.HeaderId == PTRHeaderId).FirstOrDefault();

                _objPTR011.ProjectNo = _lstPTR001.Project;
                _objPTR011.PTRNo = _lstPTR001.PTRNo;
                _objPTR011.TestDescription = _lstPTR001.CouponCategory;
                _objPTR011.MaterialSpecification = _lstPTR001.MaterialSpec;
                _objPTR011.SeamNo = _lstPTR001.SeamNo;
                _objPTR011.PQRNo = _lstPTR001.PQRnO;
                _objPTR011.CustomerInspnAgency = _lstPTR001.InspectionAgency1 + "," + _lstPTR001.InspectionAgency2 + "," + _lstPTR001.InspectionAgency3;
                _objPTR011.WeldingProcess = _lstPTR001.WeldingProcess;
                _objPTR011.FillerWire = _lstPTR001.FillerWire;
                _objPTR011.Position = _lstPTR001.Position;
                _objPTR011.BatchNo = _lstPTR001.BatchNo;
                _objPTR011.BrandName = _lstPTR001.BrandName;
                _objPTR011.WPSNo = _lstPTR001.WPSNo;
                _objPTR011.WelderNo = _lstPTR001.WelderID;
                _objPTR011.CodeSpecification = _lstPTR001.ApplicableCodeSpec;
                _objPTR011.Note1 = "All Instruments & equipments listed above are having valid calibration and traceable to National standards. All units are SI units.";
                _objPTR011.Note2 = "The results relate only to the sample tested. Discipline: Mechanical Testing & Group: Metals & alloys.";

                List<PTR002> _lstPTR002 = db.PTR002.Where(x => x.HeaderId == PTRHeaderId).ToList();
                string strHTDuration = "";
                for (int i = 0; i < _lstPTR002.Count(); i++)
                {

                    string strHTDurationTemp = "";


                    if (!string.IsNullOrWhiteSpace(_lstPTR002[i].HTTemp1) || !string.IsNullOrWhiteSpace(_lstPTR002[i].HTTime1) || !string.IsNullOrWhiteSpace(_lstPTR002[i].HTType1))
                        strHTDurationTemp += "(" + _lstPTR002[i].HTType1 + ") " + _lstPTR002[i].HTTemp1 + "°C / " + _lstPTR002[i].HTTime1 + "Hrs";

                    if (!string.IsNullOrWhiteSpace(_lstPTR002[i].HTTemp2) || !string.IsNullOrWhiteSpace(_lstPTR002[i].HTTime2) || !string.IsNullOrWhiteSpace(_lstPTR002[i].HTType2))
                        strHTDurationTemp += " + " + "(" + _lstPTR002[i].HTType2 + ") " + _lstPTR002[i].HTTemp2 + "°C / " + _lstPTR002[i].HTTime2 + "Hrs";

                    if (!string.IsNullOrWhiteSpace(_lstPTR002[i].HTTemp3) || !string.IsNullOrWhiteSpace(_lstPTR002[i].HTTime3) || !string.IsNullOrWhiteSpace(_lstPTR002[i].HTType3))
                        strHTDurationTemp += " + " + "(" + _lstPTR002[i].HTType3 + ") " + _lstPTR002[i].HTTemp3 + "°C / " + _lstPTR002[i].HTTime3 + "Hrs";

                    if (!string.IsNullOrWhiteSpace(_lstPTR002[i].HTTemp4) || !string.IsNullOrWhiteSpace(_lstPTR002[i].HTTime4) || !string.IsNullOrWhiteSpace(_lstPTR002[i].HTType4))
                        strHTDurationTemp += " + " + "(" + _lstPTR002[i].HTType4 + ") " + _lstPTR002[i].HTTemp4 + "°C / " + _lstPTR002[i].HTTime4 + "Hrs";

                    if (!string.IsNullOrWhiteSpace(_lstPTR002[i].HTTemp5) || !string.IsNullOrWhiteSpace(_lstPTR002[i].HTTime5) || !string.IsNullOrWhiteSpace(_lstPTR002[i].HTType5))
                        strHTDurationTemp += " + " + "(" + _lstPTR002[i].HTType5 + ") " + _lstPTR002[i].HTTemp5 + "°C / " + _lstPTR002[i].HTTime5 + "Hrs";

                    if (!string.IsNullOrWhiteSpace(_lstPTR002[i].HTTemp6) || !string.IsNullOrWhiteSpace(_lstPTR002[i].HTTime6) || !string.IsNullOrWhiteSpace(_lstPTR002[i].HTType6))
                        strHTDurationTemp += " + " + "(" + _lstPTR002[i].HTType6 + ") " + _lstPTR002[i].HTTemp6 + "°C / " + _lstPTR002[i].HTTime6 + "Hrs";

                    if (strHTDurationTemp != "")
                    {
                        if (strHTDuration != "")
                            strHTDuration += ", ";

                        strHTDurationTemp = _lstPTR002[i].CouponID + "- " + strHTDurationTemp;
                    }

                    strHTDuration += strHTDurationTemp;
                    //_objPTR011.HTDuration = _lstPTR002[i].HTTemp1 + "/" + _lstPTR002[i].HTTime1 + "(" + _lstPTR002[i].HTType1 + ")," + _lstPTR002[i].HTTemp2 + "/" + _lstPTR002[i].HTTime2 + "(" + _lstPTR002[i].HTType2 + ")," + _lstPTR002[i].HTTemp3 + "/" + _lstPTR002[i].HTTime3 + "(" + _lstPTR002[i].HTType3 + ")," + _lstPTR002[i].HTTemp4 + "/" + _lstPTR002[i].HTTime4 + "(" + _lstPTR002[i].HTType4 + ")," + _lstPTR002[i].HTTemp5 + "/" + _lstPTR002[i].HTTime5 + "(" + _lstPTR002[i].HTType5 + ")," + _lstPTR002[i].HTTemp6 + "/" + _lstPTR002[i].HTTime6 + "(" + _lstPTR002[i].HTType6 + ")";
                    //db.PTR011.Add(objPTR011);
                }
                _objPTR011.HTDuration = strHTDuration;

                string strPlateNo1 = "", strHeatNo1 = "", strPlateNo2 = "", strHeatNo2 = "";
                List<PTR005> _lstPTR005 = db.PTR005.Where(x => x.HeaderId == PTRHeaderId).ToList();

                for (int i = 0; i < _lstPTR005.Count; i++)
                {
                    if (i == 0)
                    {
                        strPlateNo1 = _lstPTR005[i].PlateNo1;
                        strHeatNo1 = _lstPTR005[i].HeatNo1;

                        strPlateNo2 = _lstPTR005[i].PlateNo2;
                        strHeatNo2 = _lstPTR005[i].HeatNo2;
                    }
                    else
                    {
                        strPlateNo1 += "," + _lstPTR005[i].PlateNo1;
                        strHeatNo1 += "," + _lstPTR005[i].HeatNo1;

                        strPlateNo2 += "," + _lstPTR005[i].PlateNo2;
                        strHeatNo2 += "," + _lstPTR005[i].HeatNo2;
                    }
                }
                //_objPTR011.PlateNo = strPlateNo1 + "#" + strPlateNo2;
                //_objPTR011.HeatNo = strHeatNo1 + "#" + strHeatNo2;
                if (strPlateNo1 != "" && strPlateNo2 != "")
                    _objPTR011.PlateNo = strPlateNo1 + "#" + strPlateNo2;
                else
                    _objPTR011.PlateNo = "NA";
                if (strHeatNo1 != "" && strHeatNo2 != "")
                    _objPTR011.HeatNo = strHeatNo1 + "#" + strHeatNo2;
                else
                    _objPTR011.HeatNo = "NA";

                _objPTR011.Units = "All units are SI units.";

                PTR011_Log objPTR011_Log = new PTR011_Log();

                foreach (PTR010 obj in db.PTR010.ToList())
                {
                    objPTR011_Log = new PTR011_Log();
                    objPTR011_Log.InstrumnetId = obj.InstrumnetId;
                    objPTR011_Log.Description = obj.Description;
                    objPTR011_Log.IdentificationNo = obj.IdentificationNo;
                    objPTR011_Log.InstrumentUsed = "No";
                    _objPTR011.PTR011_Log.Add(objPTR011_Log);

                }
            }

            ViewBag.isEditable = IsPTRReportEditable(PTRHeaderId);

            return PartialView("Cover", _objPTR011);
        }

        public ActionResult SaveCoverHeaderData(PTR011 _obj)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                bool isAdd = false;
                PTR011 _objPTR011 = new PTR011();
                if (_obj.HeaderId == 0) //Add
                    isAdd = true;
                else
                    _objPTR011 = db.PTR011.Where(c => c.HeaderId == _obj.HeaderId).FirstOrDefault();

                _objPTR011.ULRNo = _obj.ULRNo;
                _objPTR011.ReportNo = _obj.ReportNo;
                _objPTR011.DateofPTRreceipt = _obj.DateofPTRreceipt;
                _objPTR011.ProjectNo = _obj.ProjectNo;
                _objPTR011.PTRNo = _obj.PTRNo;
                _objPTR011.TestDescription = _obj.TestDescription;
                _objPTR011.MaterialSpecification = _obj.MaterialSpecification;
                _objPTR011.CouponThickness = _obj.CouponThickness;
                _objPTR011.PlateNo = _obj.PlateNo;
                _objPTR011.HeatNo = _obj.HeatNo;
                _objPTR011.SeamNo = _obj.SeamNo;
                _objPTR011.PQRNo = _obj.PQRNo;
                _objPTR011.TestingDate = _obj.TestingDate;
                _objPTR011.CustomerInspnAgency = _obj.CustomerInspnAgency;
                _objPTR011.WeldingProcess = _obj.WeldingProcess;
                _objPTR011.FillerWire = _obj.FillerWire;
                _objPTR011.Position = _obj.Position;
                _objPTR011.BatchNo = _obj.BatchNo;
                _objPTR011.BrandName = _obj.BrandName;
                _objPTR011.WPSNo = _obj.WPSNo;
                _objPTR011.WelderNo = _obj.WelderNo;
                _objPTR011.HTDuration = _obj.HTDuration;
                _objPTR011.CodeSpecification = _obj.CodeSpecification;
                _objPTR011.Units = _obj.Units;
                _objPTR011.Remarks1 = _obj.Remarks1;
                _objPTR011.Remarks2 = _obj.Remarks2;
                _objPTR011.PTRHeaderId = _obj.PTRHeaderId;
                _objPTR011.IsNABL = _obj.IsNABL;
                _objPTR011.Note1 = _obj.Note1;
                _objPTR011.Note2 = _obj.Note2;

                if (isAdd)
                {
                    _objPTR011.CreatedBy = objClsLoginInfo.UserName;
                    _objPTR011.CreatedOn = DateTime.Now;
                    db.PTR011.Add(_objPTR011);
                }
                else
                {
                    _objPTR011.EditedBy = objClsLoginInfo.UserName;
                    _objPTR011.EditedOn = DateTime.Now;
                }

                db.SaveChanges();

                objResponseMsg.Key = true;
                objResponseMsg.HeaderId = _objPTR011.HeaderId;
            }
            catch (Exception ex)
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
                objResponseMsg.HeaderId = 0;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveCoverLineTable1(List<PTR011_Log> lst, int HeaderId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                List<PTR011_Log> _lstPTR011_Log = new List<PTR011_Log>();
                List<PTR011_Log> _lstDelete = new List<PTR011_Log>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PTR011_Log obj = db.PTR011_Log.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.IdentificationNo = item.IdentificationNo;
                                obj.InstrumentUsed = item.InstrumentUsed;
                            }
                        }
                        else
                        {
                            PTR011_Log obj = new PTR011_Log();
                            obj.HeaderId = item.HeaderId;
                            obj.InstrumnetId = item.InstrumnetId;
                            obj.Description = item.Description;
                            obj.IdentificationNo = item.IdentificationNo;
                            obj.InstrumentUsed = item.InstrumentUsed;
                            _lstPTR011_Log.Add(obj);
                        }
                    }
                    if (_lstPTR011_Log.Count > 0)
                        db.PTR011_Log.AddRange(_lstPTR011_Log);
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    _lstDelete = db.PTR011_Log.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (_lstDelete.Count > 0)
                        db.PTR011_Log.RemoveRange(_lstDelete);

                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Data saved successfully";
                }
            }
            catch (Exception ex)
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
                objResponseMsg.HeaderId = 0;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region IMPACT
        public ActionResult Impact(int? PTRHeaderId)
        {
            #region ViewBag
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstReferenceStandards = new List<string> { "ASTM E23, ASME SEC IIA - SA370", "ASME SECTION IIA - SA370", "ASME SECTION IIA - SA370 Ed2017", "ASTM E23 ", "ASTM E23 Ed-2018", "ISO 148-1" };
            ViewBag.lstReferenceStandards = lstReferenceStandards.Select(x => new ddlValue() { id = x.ToString(), text = x.ToString() }).ToList();

            #endregion

            PTR012 _objPTR012 = new PTR012();
            if (PTRHeaderId.HasValue && db.PTR012.Where(w => w.PTRHeaderId == PTRHeaderId).Count() > 0)
            {
                _objPTR012 = db.PTR012.Where(w => w.PTRHeaderId == PTRHeaderId).FirstOrDefault();
                ViewBag.lstPTR012_Log = db.PTR012_Log.Where(w => w.HeaderId == _objPTR012.HeaderId).ToList();

                if (db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).Count() > 0)
                    _objPTR012.ReportNo = db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).FirstOrDefault().ReportNo;
            }
            else
            {
                _objPTR012.SOPNo = "SOP/MT/001";
                _objPTR012.Note1 = "Sample Size: 10x10x55mm, All dimension are verified and found satisfactory.";
                if (db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).Count() > 0)
                    _objPTR012.ReportNo = db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).FirstOrDefault().ReportNo;
            }

            ViewBag.isEditable = IsPTRReportEditable(PTRHeaderId);
            return PartialView("Impact", _objPTR012);
        }

        public ActionResult SaveImpactHeaderData(PTR012 _obj)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                bool isAdd = false;
                PTR012 _objData = new PTR012();
                if (_obj.HeaderId == 0) //Add
                    isAdd = true;
                else
                    _objData = db.PTR012.Where(c => c.HeaderId == _obj.HeaderId).FirstOrDefault();

                _objData.ReportNo = _obj.ReportNo;
                _objData.RefStd = _obj.RefStd;
                _objData.SOPNo = _obj.SOPNo;
                _objData.RequiredValue = _obj.RequiredValue;
                _objData.TempValue = _obj.TempValue;
                _objData.HumidityValue = _obj.HumidityValue;
                _objData.Remarks1 = _obj.Remarks1;
                _objData.Remarks2 = _obj.Remarks2;
                _objData.PTRHeaderId = _obj.PTRHeaderId;
                _objData.Note1 = _obj.Note1;

                if (isAdd)
                {
                    _objData.CreatedBy = objClsLoginInfo.UserName;
                    _objData.CreatedOn = DateTime.Now;
                    db.PTR012.Add(_objData);
                }
                else
                {
                    _objData.EditedBy = objClsLoginInfo.UserName;
                    _objData.EditedOn = DateTime.Now;
                }

                db.SaveChanges();

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data saved successfully";
                objResponseMsg.HeaderId = _objData.HeaderId;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveImpactLineTable1(List<PTR012_Log> _lst, int HeaderId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                List<PTR012_Log> _lstAdd = new List<PTR012_Log>();
                List<PTR012_Log> _lstDelete = new List<PTR012_Log>();
                if (_lst != null)
                {
                    foreach (var item in _lst)
                    {
                        if (item.LineId > 0)
                        {
                            PTR012_Log _objUpdate = db.PTR012_Log.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (_objUpdate != null)
                            {
                                _objUpdate.SrNo = item.SrNo;
                                _objUpdate.CouponId = item.CouponId;
                                _objUpdate.CouponTitle = item.CouponTitle;
                                _objUpdate.Samplelocation = item.Samplelocation;
                                _objUpdate.Notchlocation = item.Notchlocation;
                                _objUpdate.SampleDirectionNotchPosition = item.SampleDirectionNotchPosition;
                                _objUpdate.Testtemp = item.Testtemp;
                                _objUpdate.AbsorbedEnergyinJoules1 = item.AbsorbedEnergyinJoules1;
                                _objUpdate.AbsorbedEnergyinJoules2 = item.AbsorbedEnergyinJoules2;
                                _objUpdate.AbsorbedEnergyinJoules3 = item.AbsorbedEnergyinJoules3;
                                _objUpdate.Avg = item.Avg;
                                _objUpdate.LEinmm1 = item.LEinmm1;
                                _objUpdate.LEinmm2 = item.LEinmm2;
                                _objUpdate.LEinmm3 = item.LEinmm3;
                                _objUpdate.PS1 = item.PS1;
                                _objUpdate.PS2 = item.PS2;
                                _objUpdate.PS3 = item.PS3;
                                _objUpdate.SpecimenBroken = item.SpecimenBroken;
                                _objUpdate.Results = item.Results;

                                _objUpdate.EditedBy = objClsLoginInfo.UserName;
                                _objUpdate.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PTR012_Log _objAdd = new PTR012_Log();
                            _objAdd.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.SrNo))
                            {
                                _objAdd.SrNo = item.SrNo;
                                _objAdd.HeaderId = item.HeaderId;
                                _objAdd.CouponId = item.CouponId;
                                _objAdd.CouponTitle = item.CouponTitle;
                                _objAdd.Samplelocation = item.Samplelocation;
                                _objAdd.Notchlocation = item.Notchlocation;
                                _objAdd.SampleDirectionNotchPosition = item.SampleDirectionNotchPosition;
                                _objAdd.Testtemp = item.Testtemp;
                                _objAdd.AbsorbedEnergyinJoules1 = item.AbsorbedEnergyinJoules1;
                                _objAdd.AbsorbedEnergyinJoules2 = item.AbsorbedEnergyinJoules2;
                                _objAdd.AbsorbedEnergyinJoules3 = item.AbsorbedEnergyinJoules3;
                                _objAdd.Avg = item.Avg;
                                _objAdd.LEinmm1 = item.LEinmm1;
                                _objAdd.LEinmm2 = item.LEinmm2;
                                _objAdd.LEinmm3 = item.LEinmm3;
                                _objAdd.PS1 = item.PS1;
                                _objAdd.PS2 = item.PS2;
                                _objAdd.PS3 = item.PS3;
                                _objAdd.SpecimenBroken = item.SpecimenBroken;
                                _objAdd.Results = item.Results;

                                _objAdd.CreatedBy = objClsLoginInfo.UserName;
                                _objAdd.CreatedOn = DateTime.Now;
                                _lstAdd.Add(_objAdd);
                            }
                        }
                    }
                    if (_lstAdd.Count > 0)
                        db.PTR012_Log.AddRange(_lstAdd);

                    var LineIds = _lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = _lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    _lstDelete = db.PTR012_Log.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (_lstDelete.Count > 0)
                        db.PTR012_Log.RemoveRange(_lstDelete);
                    db.SaveChanges();
                }
                else
                {
                    _lstDelete = db.PTR012_Log.Where(x => x.HeaderId == HeaderId).ToList();
                    if (_lstDelete.Count > 0)
                        db.PTR012_Log.RemoveRange(_lstDelete);
                    db.SaveChanges();
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data saved successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult impactDetailPartial(clsImpactDetailPara para)
        {
            return View(para);
        }
        [HttpPost]
        public ActionResult impactDetailTRPartial(clsImpactDetailParaTR para)
        {
            List<string> lstSpecimenBroken = new List<string> { "Yes/Yes/Yes", "Yes/Yes/No", "Yes/No/Yes", "No/Yes/Yes", "No/No/Yes", "No/Yes/No", "Yes/No/No", "No/No/No" };
            ViewBag.lstSpecimenBroken = lstSpecimenBroken.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            List<string> lstResult = new List<string> { "Acceptable", "Not Acceptable", "To report" };
            ViewBag.lstResults = lstResult.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            return View(para);
        }
        #endregion

        #region TENSILE
        public ActionResult Tensile(int? PTRHeaderId)
        {
            #region ViewBag
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstReferenceStandards = new List<string> { "ASTM E23, ASME SEC IIA - SA370", "ASTM E8-Ed2016, ASME SEC IIA - SA370 Ed2017, ASME SEC IX-Ed2017", "ASME SECTION IIA - SA370", "ASME SECTION IIA - SA370 Ed2017", "ASTM E8/E8M", "ASTM E8 Ed-2016", "ASTM E8, ASME SEC IIA - SA370", "ASME SEC IX", "ASME SEC IX- Ed2017", "ISO 6892-1", "ISO 6892-2" };
            ViewBag.lstReferenceStandards = lstReferenceStandards.Select(x => new ddlValue() { id = x.ToString(), text = x.ToString() }).ToList();

            List<string> lstRequiredValue = new List<string> { "FLAT", "RT", "HT" };
            ViewBag.lstRequiredValue = lstRequiredValue.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #endregion

            PTR013 _objPTR013 = new PTR013();
            if (PTRHeaderId.HasValue && db.PTR013.Where(w => w.PTRHeaderId == PTRHeaderId).Count() > 0)
            {
                _objPTR013 = db.PTR013.Where(w => w.PTRHeaderId == PTRHeaderId).FirstOrDefault();
                ViewBag.lstPTR013_Log = db.PTR013_Log.Where(w => w.HeaderId == _objPTR013.HeaderId).ToList();

                if (db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).Count() > 0)
                    _objPTR013.ReportNo = db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).FirstOrDefault().ReportNo;
            }
            else
            {
                _objPTR013.SOPNo = "SOP/MT/002";
                _objPTR013.RefStd = "ASTM E23, ASME SEC IIA - SA370";
                _objPTR013.RequireValueddl = "FLAT";
                _objPTR013.Note1 = "Yield Strength is calculated by 0.2% Offset method.(If Reported)";
                _objPTR013.Note2 = "Abbreviations used :- NA:Not Applicable, HT:Hot Tensile, RT: Room Temperature, W.G.L.: Within Gauge Length, BM:Base Metal, L: Longitudinal, TR: Transverse, LAW: Longitudinal All Weld";

                if (db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).Count() > 0)
                    _objPTR013.ReportNo = db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).FirstOrDefault().ReportNo;
            }

            ViewBag.isEditable = IsPTRReportEditable(PTRHeaderId);
            return PartialView("Tensile", _objPTR013);
        }

        public ActionResult SaveTensileHeaderData(PTR013 _obj)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                bool isAdd = false;
                PTR013 _objData = new PTR013();
                if (_obj.HeaderId == 0) //Add
                    isAdd = true;
                else
                    _objData = db.PTR013.Where(c => c.HeaderId == _obj.HeaderId).FirstOrDefault();

                _objData.ReportNo = _obj.ReportNo;
                _objData.RefStd = _obj.RefStd;
                _objData.SOPNo = _obj.SOPNo;
                _objData.RequireValueddl = _obj.RequireValueddl;
                _objData.RequiredValue = _obj.RequiredValue;
                _objData.RequiredValue2 = _obj.RequiredValue2;
                _objData.RequiredValue3 = _obj.RequiredValue3;

                _objData.Identification = _obj.Identification;
                _objData.SpecimenType = _obj.SpecimenType;
                _objData.Location = _obj.Location;
                _objData.Orientation = _obj.Orientation;
                _objData.TestTemperatureinC = _obj.TestTemperatureinC;
                _objData.SoakingTime = _obj.SoakingTime;
                _objData.GaugeDiaORWidth = _obj.GaugeDiaORWidth;
                _objData.Thickness = _obj.Thickness;
                _objData.Area = _obj.Area;
                _objData.GaugeLength = _obj.GaugeLength;
                _objData.YieldLoad = _obj.YieldLoad;
                _objData.UltimateLoad = _obj.UltimateLoad;
                _objData.FinalLength = _obj.FinalLength;
                _objData.FinalDiaWidthThick = _obj.FinalDiaWidthThick;
                _objData.YieldStress = _obj.YieldStress;
                _objData.UTS = _obj.UTS;
                _objData.Elongation = _obj.Elongation;
                _objData.ReductionInArea = _obj.ReductionInArea;
                _objData.FractureLocation = _obj.FractureLocation;
                _objData.FractureType = _obj.FractureType;
                _objData.Result = _obj.Result;

                _objData.TempValue = _obj.TempValue;
                _objData.HumidityValue = _obj.HumidityValue;
                _objData.Remarks1 = _obj.Remarks1;
                _objData.Remarks2 = _obj.Remarks2;
                _objData.Note1 = _obj.Note1;
                _objData.Note2 = _obj.Note2;
                _objData.PTRHeaderId = _obj.PTRHeaderId;

                if (isAdd)
                {
                    _objData.CreatedBy = objClsLoginInfo.UserName;
                    _objData.CreatedOn = DateTime.Now;
                    db.PTR013.Add(_objData);
                }
                else
                {
                    _objData.EditedBy = objClsLoginInfo.UserName;
                    _objData.EditedOn = DateTime.Now;
                }

                db.SaveChanges();

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data saved successfully";
                objResponseMsg.HeaderId = _objData.HeaderId;
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting
                        // the current instance as InnerException
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            //catch (Exception ex)
            //{
            //    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            //    objResponseMsg.Key = false;
            //    objResponseMsg.Value = ex.Message;
            //}
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveTensileLineTable1(List<PTR013_Log> _lst, int HeaderId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                List<PTR013_Log> _lstAdd = new List<PTR013_Log>();
                List<PTR013_Log> _lstDelete = new List<PTR013_Log>();
                if (_lst != null)
                {
                    foreach (var item in _lst)
                    {
                        if (item.LineId > 0)
                        {
                            PTR013_Log _objUpdate = db.PTR013_Log.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (_objUpdate != null)
                            {
                                _objUpdate.HeaderId = item.HeaderId;
                                _objUpdate.ActualValue1 = item.ActualValue1;
                                _objUpdate.ActualValue2 = item.ActualValue2;
                                _objUpdate.ActualValue3 = item.ActualValue3;
                                _objUpdate.ActualValue4 = item.ActualValue4;
                                _objUpdate.ActualValue5 = item.ActualValue5;
                                _objUpdate.ActualValue6 = item.ActualValue6;
                                _objUpdate.ActualValue7 = item.ActualValue7;
                                _objUpdate.ActualValue8 = item.ActualValue8;
                                _objUpdate.ActualValue9 = item.ActualValue9;
                                _objUpdate.ActualValue10 = item.ActualValue10;
                                _objUpdate.ActualValue11 = item.ActualValue11;
                                _objUpdate.ActualValue12 = item.ActualValue12;
                                _objUpdate.ActualValue13 = item.ActualValue13;
                                _objUpdate.ActualValue14 = item.ActualValue14;
                                _objUpdate.ActualValue15 = item.ActualValue15;
                                _objUpdate.ActualValue16 = item.ActualValue16;
                                _objUpdate.ActualValue17 = item.ActualValue17;
                                _objUpdate.ActualValue18 = item.ActualValue18;
                                _objUpdate.ActualValue19 = item.ActualValue19;
                                _objUpdate.ActualValue20 = item.ActualValue20;
                                _objUpdate.ActualValue21 = item.ActualValue21;

                                _objUpdate.EditedBy = objClsLoginInfo.UserName;
                                _objUpdate.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PTR013_Log _objAdd = new PTR013_Log();
                            _objAdd.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.ActualValue1))
                            {
                                _objAdd.HeaderId = item.HeaderId;
                                _objAdd.ActualValue1 = item.ActualValue1;
                                _objAdd.ActualValue2 = item.ActualValue2;
                                _objAdd.ActualValue3 = item.ActualValue3;
                                _objAdd.ActualValue4 = item.ActualValue4;
                                _objAdd.ActualValue5 = item.ActualValue5;
                                _objAdd.ActualValue6 = item.ActualValue6;
                                _objAdd.ActualValue7 = item.ActualValue7;
                                _objAdd.ActualValue8 = item.ActualValue8;
                                _objAdd.ActualValue9 = item.ActualValue9;
                                _objAdd.ActualValue10 = item.ActualValue10;
                                _objAdd.ActualValue11 = item.ActualValue11;
                                _objAdd.ActualValue12 = item.ActualValue12;
                                _objAdd.ActualValue13 = item.ActualValue13;
                                _objAdd.ActualValue14 = item.ActualValue14;
                                _objAdd.ActualValue15 = item.ActualValue15;
                                _objAdd.ActualValue16 = item.ActualValue16;
                                _objAdd.ActualValue17 = item.ActualValue17;
                                _objAdd.ActualValue18 = item.ActualValue18;
                                _objAdd.ActualValue19 = item.ActualValue19;
                                _objAdd.ActualValue20 = item.ActualValue20;
                                _objAdd.ActualValue21 = item.ActualValue21;

                                _objAdd.CreatedBy = objClsLoginInfo.UserName;
                                _objAdd.CreatedOn = DateTime.Now;
                                _lstAdd.Add(_objAdd);
                            }
                        }
                    }
                    if (_lstAdd.Count > 0)
                        db.PTR013_Log.AddRange(_lstAdd);

                    var LineIds = _lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = _lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    _lstDelete = db.PTR013_Log.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (_lstDelete.Count > 0)
                        db.PTR013_Log.RemoveRange(_lstDelete);
                    db.SaveChanges();
                }
                else
                {
                    _lstDelete = db.PTR013_Log.Where(x => x.HeaderId == HeaderId).ToList();
                    if (_lstDelete.Count > 0)
                        db.PTR013_Log.RemoveRange(_lstDelete);
                    db.SaveChanges();
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data saved successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region BEND
        public ActionResult Bend(int? PTRHeaderId)
        {
            #region ViewBag
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstReferenceStandards = new List<string> { "ASME SEC IX Ed-2017", "ASME SEC IX", "ASME SECTION IIA - SA370", "ASME SECTION IIA - SA370 Ed2017", "ASTM E190-14", "ASTM E290-14" };
            ViewBag.lstReferenceStandards = lstReferenceStandards.Select(x => new ddlValue() { id = x.ToString(), text = x.ToString() }).ToList();
            #endregion


            PTR014 _objPTR014 = new PTR014();
            if (PTRHeaderId.HasValue && db.PTR014.Where(w => w.PTRHeaderId == PTRHeaderId).Count() > 0)
            {
                _objPTR014 = db.PTR014.Where(w => w.PTRHeaderId == PTRHeaderId).FirstOrDefault();
                ViewBag.lstPTR014_Log = db.PTR014_Log.Where(w => w.HeaderId == _objPTR014.HeaderId).ToList();
                if (db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).Count() > 0)
                    _objPTR014.ReportNo = db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).FirstOrDefault().ReportNo;
            }
            else
            {
                _objPTR014.SOPNo = "SOP/MT/003";
                _objPTR014.RefStd = "ASME SEC IX Ed-2017";

                if (db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).Count() > 0)
                    _objPTR014.ReportNo = db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).FirstOrDefault().ReportNo;
            }

            ViewBag.isEditable = IsPTRReportEditable(PTRHeaderId);
            return PartialView("Bend", _objPTR014);
        }

        public ActionResult SaveBendHeaderData(PTR014 _obj)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                bool isAdd = false;
                PTR014 _objData = new PTR014();
                if (_obj.HeaderId == 0) //Add
                    isAdd = true;
                else
                    _objData = db.PTR014.Where(c => c.HeaderId == _obj.HeaderId).FirstOrDefault();

                _objData.ReportNo = _obj.ReportNo;
                _objData.RefStd = _obj.RefStd;
                _objData.SOPNo = _obj.SOPNo;
                _objData.TempValue = _obj.TempValue;
                _objData.HumidityValue = _obj.HumidityValue;
                _objData.Remarks1 = _obj.Remarks1;
                _objData.Remarks2 = _obj.Remarks2;
                _objData.PTRHeaderId = _obj.PTRHeaderId;

                if (isAdd)
                {
                    _objData.CreatedBy = objClsLoginInfo.UserName;
                    _objData.CreatedOn = DateTime.Now;
                    db.PTR014.Add(_objData);
                }
                else
                {
                    _objData.EditedBy = objClsLoginInfo.UserName;
                    _objData.EditedOn = DateTime.Now;
                }

                db.SaveChanges();

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data saved successfully";
                objResponseMsg.HeaderId = _objData.HeaderId;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveBendLineTable1(List<PTR014_Log> _lst, int HeaderId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                List<PTR014_Log> _lstAdd = new List<PTR014_Log>();
                List<PTR014_Log> _lstDelete = new List<PTR014_Log>();
                if (_lst != null)
                {
                    foreach (var item in _lst)
                    {
                        if (item.LineId > 0)
                        {
                            PTR014_Log _objUpdate = db.PTR014_Log.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (_objUpdate != null)
                            {
                                _objUpdate.HeaderId = item.HeaderId;
                                _objUpdate.SB1Identification = item.SB1Identification;
                                _objUpdate.SB2Identification = item.SB2Identification;
                                _objUpdate.SB3Identification = item.SB3Identification;
                                _objUpdate.SB4Identification = item.SB4Identification;
                                _objUpdate.SB1Thickness = item.SB1Thickness;
                                _objUpdate.SB2Thickness = item.SB2Thickness;
                                _objUpdate.SB3Thickness = item.SB3Thickness;
                                _objUpdate.SB4Thickness = item.SB4Thickness;
                                _objUpdate.SB1BendRollerDia = item.SB1BendRollerDia;
                                _objUpdate.SB2BendRollerDia = item.SB2BendRollerDia;
                                _objUpdate.SB3BendRollerDia = item.SB3BendRollerDia;
                                _objUpdate.SB4BendRollerDia = item.SB4BendRollerDia;
                                _objUpdate.SB1Direction = item.SB1Direction;
                                _objUpdate.SB2Direction = item.SB2Direction;
                                _objUpdate.SB3Direction = item.SB3Direction;
                                _objUpdate.SB4Direction = item.SB4Direction;
                                _objUpdate.MandrelDia = item.MandrelDia;
                                _objUpdate.BendAngel = item.BendAngel;
                                _objUpdate.Result = item.Result;
                                _objUpdate.SB2Result = item.SB2Result;
                                _objUpdate.SB3Result = item.SB3Result;
                                _objUpdate.SB4Result = item.SB4Result;
                                _objUpdate.CouponId = item.CouponId;
                                _objUpdate.Width1 = item.Width1;
                                _objUpdate.Width2 = item.Width2;
                                _objUpdate.Width3 = item.Width3;
                                _objUpdate.Width4 = item.Width4;
                                _objUpdate.Observation1 = item.Observation1;
                                _objUpdate.Observation2 = item.Observation2;
                                _objUpdate.Observation3 = item.Observation3;
                                _objUpdate.Observation4 = item.Observation4;
                                _objUpdate.EditedBy = objClsLoginInfo.UserName;
                                _objUpdate.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PTR014_Log _objAdd = new PTR014_Log();
                            _objAdd.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.SB1Identification))
                            {
                                _objAdd.HeaderId = item.HeaderId;
                                _objAdd.SB1Identification = item.SB1Identification;
                                _objAdd.SB2Identification = item.SB2Identification;
                                _objAdd.SB3Identification = item.SB3Identification;
                                _objAdd.SB4Identification = item.SB4Identification;
                                _objAdd.SB1Thickness = item.SB1Thickness;
                                _objAdd.SB2Thickness = item.SB2Thickness;
                                _objAdd.SB3Thickness = item.SB3Thickness;
                                _objAdd.SB4Thickness = item.SB4Thickness;
                                _objAdd.SB1BendRollerDia = item.SB1BendRollerDia;
                                _objAdd.SB2BendRollerDia = item.SB2BendRollerDia;
                                _objAdd.SB3BendRollerDia = item.SB3BendRollerDia;
                                _objAdd.SB4BendRollerDia = item.SB4BendRollerDia;
                                _objAdd.SB1Direction = item.SB1Direction;
                                _objAdd.SB2Direction = item.SB2Direction;
                                _objAdd.SB3Direction = item.SB3Direction;
                                _objAdd.SB4Direction = item.SB4Direction;
                                _objAdd.MandrelDia = item.MandrelDia;
                                _objAdd.BendAngel = item.BendAngel;
                                _objAdd.Result = item.Result;
                                _objAdd.SB2Result = item.SB2Result;
                                _objAdd.SB3Result = item.SB3Result;
                                _objAdd.SB4Result = item.SB4Result;
                                _objAdd.CouponId = item.CouponId;
                                _objAdd.Width1 = item.Width1;
                                _objAdd.Width2 = item.Width2;
                                _objAdd.Width3 = item.Width3;
                                _objAdd.Width4 = item.Width4;
                                _objAdd.Observation1 = item.Observation1;
                                _objAdd.Observation2 = item.Observation2;
                                _objAdd.Observation3 = item.Observation3;
                                _objAdd.Observation4 = item.Observation4;
                                _objAdd.CreatedBy = objClsLoginInfo.UserName;
                                _objAdd.CreatedOn = DateTime.Now;
                                _lstAdd.Add(_objAdd);
                            }
                        }
                    }
                    if (_lstAdd.Count > 0)
                        db.PTR014_Log.AddRange(_lstAdd);

                    var LineIds = _lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = _lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    _lstDelete = db.PTR014_Log.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (_lstDelete.Count > 0)
                        db.PTR014_Log.RemoveRange(_lstDelete);
                    db.SaveChanges();
                }
                else
                {
                    _lstDelete = db.PTR014_Log.Where(x => x.HeaderId == HeaderId).ToList();
                    if (_lstDelete.Count > 0)
                        db.PTR014_Log.RemoveRange(_lstDelete);
                    db.SaveChanges();
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data saved successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region HARDNESS
        public ActionResult Hardness(int? PTRHeaderId, string reportNo)
        {
            #region ViewBag
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstHardnessScale = new List<string> { "HV10", "HBW (187.5Kg / 2.5mm)", "HRBW", "HRC", "HV5", "HV1" };
            ViewBag.lstHardnessScale = lstHardnessScale.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            List<string> lstReferenceStandards = new List<string> { "ASTM E92 Ed2017", "ASTM E10 Ed2017", "ASTM E18 Ed2017", "ASTM E384 Ed2017", "ISO 6507-2", "ISO 6506-2", "ISO 6508-2" };
            ViewBag.lstReferenceStandards = lstReferenceStandards.Select(x => new ddlValue() { id = x.ToString(), text = x.ToString() }).ToList();

            List<PTR015> _lstNotes2 = db.PTR015.ToList();
            ViewBag._lstNotes2 = _lstNotes2.Select(x => new SelectListItem() { Value = x.HeaderId.ToString(), Text = x.StandardBlockId.ToString() }).ToList();

            #endregion

            List<clsPTR016> _objPTR016 = new List<clsPTR016>();
            _objPTR016 = (from x in db.PTR016
                          where x.PTRHeaderId == PTRHeaderId
                          select new clsPTR016
                          {
                              HeaderId = x.HeaderId,
                              ReportNo = x.ReportNo,
                              RefStd = x.RefStd,
                              SOPNo = x.SOPNo,
                              HardnessScale = x.HardnessScale,
                              RequiredValue = x.RequiredValue,
                              Result = x.Result,
                              SampleId = x.SampleId,
                              SketchName1 = x.SketchName1,
                              SketchName2 = x.SketchName2,
                              TempValue = x.TempValue,
                              HumidityValue = x.HumidityValue,
                              Notes2 = x.Notes2,
                              Notes3 = x.Notes3,
                              Notes4 = x.Notes4,
                              Remarks1 = x.Remarks1,
                              Remarks2 = x.Remarks2,
                              CreatedBy = x.CreatedBy,
                              CreatedOn = x.CreatedOn,
                              EditedBy = x.EditedBy,
                              EditedOn = x.EditedOn,
                              PTRHeaderId = x.PTRHeaderId,
                              PTR016_Log = db.PTR016_Log.Where(t => t.HeaderId == x.HeaderId).ToList()
                          }).ToList();
            var _sopNo = "SOP/MT/004";
            PTR011 objptr = db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).FirstOrDefault();
            string _reportNo = "";
            if (objptr != null)
                _reportNo = objptr.ReportNo;
            if (_objPTR016.Count > 0)
            {
                _sopNo = _objPTR016.FirstOrDefault().SOPNo;
            }
            if (reportNo != null && reportNo != "")
            {
                _objPTR016.Add(new clsPTR016 { ReportNo = reportNo, HeaderId = 0, PTRHeaderId = PTRHeaderId, SOPNo = _sopNo, PTR016_Log = new List<PTR016_Log>() });
            }
            else if (_objPTR016.Count() == 0)
            {
                _objPTR016.Add(new clsPTR016
                {
                    ReportNo = _reportNo,
                    HeaderId = 0,
                    PTRHeaderId = PTRHeaderId,
                    SOPNo = _sopNo,
                    RefStd = "ASTM E92 Ed2017",
                    HardnessScale = "HV10",
                    Notes2 = "8/2198 (HV10)",
                    PTR016_Log = new List<PTR016_Log>()
                });
            }
            /* if (PTRHeaderId.HasValue && db.PTR016.Where(w => w.PTRHeaderId == PTRHeaderId).Count() > 0)
             {
                 _objPTR016 = db.PTR016.Where(w => w.PTRHeaderId == PTRHeaderId).FirstOrDefault();
                 ViewBag.lstPTR016_Log = db.PTR016_Log.Where(w => w.HeaderId == _objPTR016.HeaderId).ToList();

                 ViewBag._lstWELD = db.PTR016_Log.Where(w => w.HeaderId == _objPTR016.HeaderId && w.RequiredValue == "WELD").ToList();
                 ViewBag._lstHAZ = db.PTR016_Log.Where(w => w.HeaderId == _objPTR016.HeaderId && w.RequiredValue == "HAZ").ToList();
                 ViewBag._lstFUSION_LINE = db.PTR016_Log.Where(w => w.HeaderId == _objPTR016.HeaderId && w.RequiredValue == "FUSION LINE").ToList();
                 ViewBag._lstBASE_METAL = db.PTR016_Log.Where(w => w.HeaderId == _objPTR016.HeaderId && w.RequiredValue == "BASE METAL").ToList();

                 if (db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).Count() > 0)
                     _objPTR016.ReportNo = db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).FirstOrDefault().ReportNo;
             }
             else
             {
                 _objPTR016.SOPNo = "SOP/MT/004";
                 _objPTR016.RefStd = "ASTM E92 Ed2017";
                 _objPTR016.HardnessScale = "HV10";
                 _objPTR016.Notes2 = "8/2198 (HV10)";

                 if (db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).Count() > 0)
                     _objPTR016.ReportNo = db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).FirstOrDefault().ReportNo;
             }*/

            ViewBag.isEditable = IsPTRReportEditable(PTRHeaderId);
            #region Image path
            List<string> _lstPath = new List<string>();
            for (int i = 0; i < _objPTR016.Count; i++)
            {
                long lHeaderId = 0;
                long.TryParse(_objPTR016[i].HeaderId.ToString(), out lHeaderId);
                FCS001 _objFCS = db.FCS001.Where(w => w.TableId == lHeaderId && w.TableName == "PTR016" && w.ViewerId == 1).FirstOrDefault();
                if (_objFCS != null)
                    _lstPath.Add(_objFCS.MainDocumentPath);
                else
                    _lstPath.Add("");
            }
            #endregion
            ViewBag._lstImagePaths = _lstPath;
            return PartialView("Hardness", _objPTR016);
            //return View(_objPTR016);
        }

        public ActionResult SaveHardnessHeaderData(PTR016 _obj)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                bool isAdd = false;
                PTR016 _objData = new PTR016();
                if (_obj.HeaderId == 0) //Add
                    isAdd = true;
                else
                    _objData = db.PTR016.Where(c => c.HeaderId == _obj.HeaderId).FirstOrDefault();

                _objData.ReportNo = _obj.ReportNo;
                _objData.RefStd = _obj.RefStd;
                _objData.SOPNo = _obj.SOPNo;
                _objData.HardnessScale = _obj.HardnessScale;
                _objData.Result = _obj.Result;
                _objData.RequiredValue = _obj.RequiredValue;
                _objData.SampleId = _obj.SampleId;
                _objData.SketchName1 = _obj.SketchName1;
                _objData.SketchName2 = _obj.SketchName2;
                _objData.Notes2 = _obj.Notes2;
                _objData.Notes3 = _obj.Notes3;
                _objData.Notes4 = _obj.Notes4;
                _objData.TempValue = _obj.TempValue;
                _objData.HumidityValue = _obj.HumidityValue;
                _objData.Remarks1 = _obj.Remarks1;
                _objData.Remarks2 = _obj.Remarks2;
                _objData.PTRHeaderId = _obj.PTRHeaderId;

                if (isAdd)
                {
                    _objData.CreatedBy = objClsLoginInfo.UserName;
                    _objData.CreatedOn = DateTime.Now;
                    db.PTR016.Add(_objData);
                }
                else
                {
                    _objData.EditedBy = objClsLoginInfo.UserName;
                    _objData.EditedOn = DateTime.Now;
                }

                db.SaveChanges();

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data saved successfully";
                objResponseMsg.HeaderId = _objData.HeaderId;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveHardnessLineTable1(List<PTR016_Log> _lst, int HeaderId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                List<PTR016_Log> _lstAdd = new List<PTR016_Log>();
                List<PTR016_Log> _lstDelete = new List<PTR016_Log>();
                if (_lst != null)
                {
                    foreach (var item in _lst)
                    {
                        if (item.LineId > 0)
                        {
                            PTR016_Log _objUpdate = db.PTR016_Log.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (_objUpdate != null)
                            {
                                _objUpdate.ActualValue = item.ActualValue;
                                _objUpdate.RequiredValue = item.RequiredValue;
                                _objUpdate.EditedBy = objClsLoginInfo.UserName;
                                _objUpdate.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PTR016_Log _objAdd = new PTR016_Log();
                            _objAdd.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.ActualValue))
                            {
                                _objAdd.HeaderId = item.HeaderId;
                                _objAdd.RequiredValue = item.RequiredValue;
                                _objAdd.RequiredValueId = item.RequiredValueId;
                                _objAdd.ActualValue = item.ActualValue;
                                _objAdd.CreatedBy = objClsLoginInfo.UserName;
                                _objAdd.CreatedOn = DateTime.Now;
                                _lstAdd.Add(_objAdd);
                            }
                        }
                    }
                    if (_lstAdd.Count > 0)
                        db.PTR016_Log.AddRange(_lstAdd);

                    var LineIds = _lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = _lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    _lstDelete = db.PTR016_Log.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (_lstDelete.Count > 0)
                        db.PTR016_Log.RemoveRange(_lstDelete);
                    db.SaveChanges();
                }
                else
                {
                    _lstDelete = db.PTR016_Log.Where(x => x.HeaderId == HeaderId).ToList();
                    if (_lstDelete.Count > 0)
                        db.PTR016_Log.RemoveRange(_lstDelete);
                    db.SaveChanges();
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data saved successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public string FindStandardValueById(int? Id)
        {
            try
            {
                PTR015 _obj = db.PTR015.Where(w => w.HeaderId == Id).FirstOrDefault();
                if (_obj != null)
                    return _obj.StandardValue;
            }
            catch (Exception ex)
            {

                throw;
            }
            return "";
        }
        public ActionResult DeleteHardnessHeaderData(int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {

                var header = db.PTR016.Where(t => t.HeaderId == headerId).FirstOrDefault();
                if (header != null)
                {
                    var logs = db.PTR016_Log.Where(t => t.HeaderId == headerId).ToList();
                    db.PTR016_Log.RemoveRange(logs);
                    db.PTR016.Remove(header);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Data deleted successfully";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Record not found.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region DWT
        public ActionResult DWT(int? PTRHeaderId)
        {
            #region ViewBag
            ViewBag.ControllerURL = ControllerURL;

            List<string> _lstTestStatus = new List<string> { "No Break", "No Test", "Break" };
            ViewBag._lstTestStatus = _lstTestStatus.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            List<string> _lstResult = new List<string> { "Acceptable", "Not Acceptable", "To report" };
            ViewBag._lstResult = _lstResult.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #endregion

            PTR017 _objPTR017 = new PTR017();
            if (PTRHeaderId.HasValue && db.PTR017.Where(w => w.PTRHeaderId == PTRHeaderId).Count() > 0)
            {
                _objPTR017 = db.PTR017.Where(w => w.PTRHeaderId == PTRHeaderId).FirstOrDefault();
                ViewBag.lstPTR017_Log = db.PTR017_Log.Where(w => w.HeaderId == _objPTR017.HeaderId).ToList();

                if (db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).Count() > 0)
                    _objPTR017.ReportNo = db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).FirstOrDefault().ReportNo;
            }
            else
            {
                _objPTR017.SOPNo = "SOP/MT/005";
                _objPTR017.RefStd = "ASTM E208 Ed2017";
                _objPTR017.WeldingConsumablesUsed = "UTP DUR 350";
                _objPTR017.WeldingParameters = "190A";
                _objPTR017.SoakingTimeAtTestTemp = "45 Min";
                _objPTR017.Note2 = "Abbreviations used : T:Thickness of coupon, TR:Transverse, L:longitudinal, N:Normal, B.M : Base Metal, FL: fusion line";

                if (db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).Count() > 0)
                    _objPTR017.ReportNo = db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).FirstOrDefault().ReportNo;
            }

            ViewBag.isEditable = IsPTRReportEditable(PTRHeaderId);
            return PartialView("DWT", _objPTR017);
        }

        public ActionResult SaveDWTHeaderData(PTR017 _obj)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                bool isAdd = false;
                PTR017 _objData = new PTR017();
                if (_obj.HeaderId == 0) //Add
                    isAdd = true;
                else
                    _objData = db.PTR017.Where(c => c.HeaderId == _obj.HeaderId).FirstOrDefault();

                _objData.ReportNo = _obj.ReportNo;
                _objData.RefStd = _obj.RefStd;
                _objData.SOPNo = _obj.SOPNo;
                _objData.AcceptanceCriteria = _obj.AcceptanceCriteria;
                _objData.SampleDimension = _obj.SampleDimension;
                _objData.WeldingConsumablesUsed = _obj.WeldingConsumablesUsed;
                _objData.WeldingParameters = _obj.WeldingParameters;
                _objData.SoakingTimeAtTestTemp = _obj.SoakingTimeAtTestTemp;
                _objData.NDTTemp = _obj.NDTTemp;

                _objData.TempValue = _obj.TempValue;
                _objData.HumidityValue = _obj.HumidityValue;
                _objData.Remarks1 = _obj.Remarks1;
                _objData.Remarks2 = _obj.Remarks2;
                _objData.PTRHeaderId = _obj.PTRHeaderId;
                _objData.Note2 = _obj.Note2;

                if (isAdd)
                {
                    _objData.CreatedBy = objClsLoginInfo.UserName;
                    _objData.CreatedOn = DateTime.Now;
                    db.PTR017.Add(_objData);
                }
                else
                {
                    _objData.EditedBy = objClsLoginInfo.UserName;
                    _objData.EditedOn = DateTime.Now;
                }

                db.SaveChanges();

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data saved successfully";
                objResponseMsg.HeaderId = _objData.HeaderId;
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting
                        // the current instance as InnerException
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            //catch (Exception ex)
            //{
            //    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            //    objResponseMsg.Key = false;
            //    objResponseMsg.Value = ex.Message;
            //}
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveDWTLineTable1(List<PTR017_Log> _lst, int HeaderId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                List<PTR017_Log> _lstAdd = new List<PTR017_Log>();
                List<PTR017_Log> _lstDelete = new List<PTR017_Log>();
                if (_lst != null)
                {
                    foreach (var item in _lst)
                    {
                        if (item.LineId > 0)
                        {
                            PTR017_Log _objUpdate = db.PTR017_Log.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (_objUpdate != null)
                            {
                                _objUpdate.HeaderId = item.HeaderId;
                                _objUpdate.ActualValue1 = item.ActualValue1;
                                _objUpdate.ActualValue2 = item.ActualValue2;
                                _objUpdate.ActualValue3 = item.ActualValue3;
                                _objUpdate.ActualValue4 = item.ActualValue4;
                                _objUpdate.ActualValue5 = item.ActualValue5;
                                _objUpdate.ActualValue6 = item.ActualValue6;
                                _objUpdate.ActualValue7 = item.ActualValue7;
                                _objUpdate.ActualValue8 = item.ActualValue8;
                                _objUpdate.ActualValue9 = item.ActualValue9;
                                _objUpdate.ActualValue10 = item.ActualValue10;
                                _objUpdate.ActualValue11 = item.ActualValue11;
                                _objUpdate.ActualValue12 = item.ActualValue12;
                                _objUpdate.ActualValue13 = item.ActualValue13;
                                _objUpdate.ActualValue14 = item.ActualValue14;
                                _objUpdate.ActualValue15 = item.ActualValue15;
                                _objUpdate.ActualValue16 = item.ActualValue16;
                                _objUpdate.ActualValue17 = item.ActualValue17;
                                _objUpdate.ActualValue18 = item.ActualValue18;
                                _objUpdate.ActualValue19 = item.ActualValue19;
                                _objUpdate.ActualValue20 = item.ActualValue20;

                                _objUpdate.EditedBy = objClsLoginInfo.UserName;
                                _objUpdate.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PTR017_Log _objAdd = new PTR017_Log();
                            _objAdd.HeaderId = item.HeaderId;
                            //if (!string.IsNullOrWhiteSpace(item.ActualValue1))
                            //{
                            _objAdd.HeaderId = item.HeaderId;
                            _objAdd.ActualValue1 = item.ActualValue1;
                            _objAdd.ActualValue2 = item.ActualValue2;
                            _objAdd.ActualValue3 = item.ActualValue3;
                            _objAdd.ActualValue4 = item.ActualValue4;
                            _objAdd.ActualValue5 = item.ActualValue5;
                            _objAdd.ActualValue6 = item.ActualValue6;
                            _objAdd.ActualValue7 = item.ActualValue7;
                            _objAdd.ActualValue8 = item.ActualValue8;
                            _objAdd.ActualValue9 = item.ActualValue9;
                            _objAdd.ActualValue10 = item.ActualValue10;
                            _objAdd.ActualValue11 = item.ActualValue11;
                            _objAdd.ActualValue12 = item.ActualValue12;
                            _objAdd.ActualValue13 = item.ActualValue13;
                            _objAdd.ActualValue14 = item.ActualValue14;
                            _objAdd.ActualValue15 = item.ActualValue15;
                            _objAdd.ActualValue16 = item.ActualValue16;
                            _objAdd.ActualValue17 = item.ActualValue17;
                            _objAdd.ActualValue18 = item.ActualValue18;
                            _objAdd.ActualValue19 = item.ActualValue19;
                            _objAdd.ActualValue20 = item.ActualValue20;

                            _objAdd.CreatedBy = objClsLoginInfo.UserName;
                            _objAdd.CreatedOn = DateTime.Now;
                            _lstAdd.Add(_objAdd);
                            //  }
                        }
                    }
                    if (_lstAdd.Count > 0)
                        db.PTR017_Log.AddRange(_lstAdd);

                    var LineIds = _lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = _lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    _lstDelete = db.PTR017_Log.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (_lstDelete.Count > 0)
                        db.PTR017_Log.RemoveRange(_lstDelete);
                    db.SaveChanges();
                }
                else
                {
                    _lstDelete = db.PTR017_Log.Where(x => x.HeaderId == HeaderId).ToList();
                    if (_lstDelete.Count > 0)
                        db.PTR017_Log.RemoveRange(_lstDelete);
                    db.SaveChanges();
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data saved successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region MACRO - 1
        public ActionResult MACRO(int? PTRHeaderId, string reportNo)
        {
            #region ViewBag
            ViewBag.ControllerURL = ControllerURL;

            List<string> _lstTestRemarks = new List<string> {
                "Sample is polished and etched by Aqua regia and examined for macro at 10X. The observation are as below.",
                "Sample is polished and etched by 2% Nital and examined for macro at 10X. The observation are as below.",
                "Sample is polished and etched by 2% Nital and examined for macro at 10X. The observation are as below.",
                "Sample is polished and etched by 2% Nital and examined for macro at 30X. The observation are as below."
            };

            ViewBag._lstTestRemarks = _lstTestRemarks.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            List<string> lstReferenceStandards = new List<string> { "ASME SEC IX Ed2017", "ASME SEC IX", "ASTM E340-15" };
            ViewBag.lstReferenceStandards = lstReferenceStandards.Select(x => new ddlValue() { id = x.ToString(), text = x.ToString() }).ToList();

            #endregion

            List<clsPTR018> _objPTR018 = new List<clsPTR018>();
            _objPTR018 = (from x in db.PTR018
                          where x.PTRHeaderId == PTRHeaderId
                          select new clsPTR018
                          {
                              HeaderId = x.HeaderId,
                              ReportNo = x.ReportNo,
                              RefStd = x.RefStd,
                              SOPNo = x.SOPNo,
                              SampleId = x.SampleId,
                              TestRemarks = x.TestRemarks,
                              PhotoHeading = x.PhotoHeading,
                              TempValue = x.TempValue,
                              HumidityValue = x.HumidityValue,
                              Remarks1 = x.Remarks1,
                              Remarks2 = x.Remarks2,
                              CreatedBy = x.CreatedBy,
                              CreatedOn = x.CreatedOn,
                              EditedBy = x.EditedBy,
                              EditedOn = x.EditedOn,
                              PTRHeaderId = x.PTRHeaderId,
                              PTR018_Log = db.PTR018_Log.Where(w => w.HeaderId == x.HeaderId).ToList()
                          }).ToList();

            /*if (PTRHeaderId.HasValue && db.PTR018.Where(w => w.PTRHeaderId == PTRHeaderId).Count() > 0)
            {
                _objPTR018 = db.PTR018.Where(w => w.PTRHeaderId == PTRHeaderId).FirstOrDefault();
                ViewBag.lstPTR018_Log = db.PTR018_Log.Where(w => w.HeaderId == _objPTR018.HeaderId).ToList();

                if (db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).Count() > 0)
                    _objPTR018.ReportNo = db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).FirstOrDefault().ReportNo;
            }*/
            var _SOPNo = "SOP/MT/006A";
            var _ReportNo = "";
            if (_objPTR018.Count > 0)
            {
                _SOPNo = _objPTR018.FirstOrDefault().SOPNo;
            }
            if (db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).Count() > 0)
                _ReportNo = db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).FirstOrDefault().ReportNo;

            if (reportNo != null && reportNo != "")
            {
                var log = new List<PTR018_Log>();
                log.Add(new PTR018_Log
                {
                    LineId = 0,
                    ActualValue = "Surface is free from defects."
                });
                log.Add(new PTR018_Log
                {
                    LineId = 0,
                    ActualValue = "Macro Examination is satisfactory."
                });
                log.Add(new PTR018_Log
                {
                    LineId = 0,
                    ActualValue = "Surface is free from defects & No disbonding observed at fusion interface."
                });
                _objPTR018.Add(new clsPTR018 { ReportNo = reportNo, HeaderId = 0, PTRHeaderId = PTRHeaderId, SOPNo = _SOPNo, PTR018_Log = log });
            }
            else if (_objPTR018.Count() == 0)
            {
                var objPTR018 = new clsPTR018();
                objPTR018.PTR018_Log = new List<PTR018_Log>();
                objPTR018.PTRHeaderId = PTRHeaderId;
                objPTR018.SOPNo = "SOP/MT/006A";
                objPTR018.TestRemarks = "Sample is polished and etched by Aqua regia and examined for macro at 10X. The observation are as below.";
                objPTR018.RefStd = "ASME SEC IX Ed2017";
                objPTR018.ReportNo = _ReportNo;

                #region Default line table data display
                objPTR018.PTR018_Log.Add(new PTR018_Log
                {
                    LineId = 0,
                    ActualValue = "Surface is free from defects."
                });
                objPTR018.PTR018_Log.Add(new PTR018_Log
                {
                    LineId = 0,
                    ActualValue = "Macro Examination is satisfactory."
                });
                objPTR018.PTR018_Log.Add(new PTR018_Log
                {
                    LineId = 0,
                    ActualValue = "Surface is free from defects & No disbonding observed at fusion interface."
                });
                _objPTR018.Add(objPTR018);
                #endregion
            }


            #region Image path
            List<string> _lstPath = new List<string>();
            for (int i = 0; i < _objPTR018.Count; i++)
            {
                long lHeaderId = 0;
                long.TryParse(_objPTR018[i].HeaderId.ToString(), out lHeaderId);
                FCS001 _objFCS = db.FCS001.Where(w => w.TableId == lHeaderId && w.TableName == "PTR018" && w.ViewerId == 1).FirstOrDefault();
                if (_objFCS != null)
                    _lstPath.Add(_objFCS.MainDocumentPath);
                else
                    _lstPath.Add("");
            }
            #endregion
            ViewBag._lstImagePaths = _lstPath;

            ViewBag.isEditable = IsPTRReportEditable(PTRHeaderId);
            return PartialView("MACRO", _objPTR018);
        }

        public ActionResult SaveMACROHeaderData(PTR018 _obj)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                bool isAdd = false;
                PTR018 _objData = new PTR018();
                if (_obj.HeaderId == 0) //Add
                    isAdd = true;
                else
                    _objData = db.PTR018.Where(c => c.HeaderId == _obj.HeaderId).FirstOrDefault();

                _objData.ReportNo = _obj.ReportNo;
                _objData.RefStd = _obj.RefStd;
                _objData.SOPNo = _obj.SOPNo;
                _objData.SampleId = _obj.SampleId;
                _objData.TestRemarks = _obj.TestRemarks;
                _objData.PhotoHeading = _obj.PhotoHeading;

                _objData.TempValue = _obj.TempValue;
                _objData.HumidityValue = _obj.HumidityValue;
                _objData.Remarks1 = _obj.Remarks1;
                _objData.Remarks2 = _obj.Remarks2;
                _objData.PTRHeaderId = _obj.PTRHeaderId;

                if (isAdd)
                {
                    _objData.CreatedBy = objClsLoginInfo.UserName;
                    _objData.CreatedOn = DateTime.Now;
                    db.PTR018.Add(_objData);
                }
                else
                {
                    _objData.EditedBy = objClsLoginInfo.UserName;
                    _objData.EditedOn = DateTime.Now;
                }

                db.SaveChanges();

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data saved successfully";
                objResponseMsg.HeaderId = _objData.HeaderId;
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting
                        // the current instance as InnerException
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            //catch (Exception ex)
            //{
            //    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            //    objResponseMsg.Key = false;
            //    objResponseMsg.Value = ex.Message;
            //}
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveMACROLineTable1(List<PTR018_Log> _lst, int HeaderId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                List<PTR018_Log> _lstAdd = new List<PTR018_Log>();
                List<PTR018_Log> _lstDelete = new List<PTR018_Log>();
                if (_lst != null)
                {
                    foreach (var item in _lst)
                    {
                        if (item.LineId > 0)
                        {
                            PTR018_Log _objUpdate = db.PTR018_Log.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (_objUpdate != null)
                            {
                                _objUpdate.HeaderId = item.HeaderId;
                                _objUpdate.ActualValue = item.ActualValue;

                                _objUpdate.EditedBy = objClsLoginInfo.UserName;
                                _objUpdate.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PTR018_Log _objAdd = new PTR018_Log();
                            _objAdd.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.ActualValue))
                            {
                                _objAdd.HeaderId = item.HeaderId;
                                _objAdd.ActualValue = item.ActualValue;

                                _objAdd.CreatedBy = objClsLoginInfo.UserName;
                                _objAdd.CreatedOn = DateTime.Now;
                                _lstAdd.Add(_objAdd);
                            }
                        }
                    }
                    if (_lstAdd.Count > 0)
                        db.PTR018_Log.AddRange(_lstAdd);

                    var LineIds = _lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = _lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    _lstDelete = db.PTR018_Log.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (_lstDelete.Count > 0)
                        db.PTR018_Log.RemoveRange(_lstDelete);
                    db.SaveChanges();
                }
                else
                {
                    _lstDelete = db.PTR018_Log.Where(x => x.HeaderId == HeaderId).ToList();
                    if (_lstDelete.Count > 0)
                        db.PTR018_Log.RemoveRange(_lstDelete);
                    db.SaveChanges();
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data saved successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteMacroHeaderData(int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {

                var header = db.PTR018.Where(t => t.HeaderId == headerId).FirstOrDefault();
                if (header != null)
                {
                    var logs = db.PTR018_Log.Where(t => t.HeaderId == headerId).ToList();
                    db.PTR018_Log.RemoveRange(logs);
                    db.PTR018.Remove(header);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Data deleted successfully";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Record not found.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region MICRO
        public ActionResult Micro(int? PTRHeaderId, string reportNo)
        {
            #region ViewBag
            ViewBag.ControllerURL = ControllerURL;

            List<string> _lstTestRemarks = new List<string> {
                "Sample is polished and etched by Aqua regia and examined for micro. The observations are as below.",
                "Sample is polished and etched by 2% Nital and examined for micro. The observations are as below.",
                "Sample is polished and etched by 10% Oxalic acid (Electrolytic) and examined for micro. The observations are as below.",
                "Sample is polished and etched by 2% Nital & Aqua regia and examined for micro. The observations are as below."
            };

            ViewBag._lstTestRemarks = _lstTestRemarks.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            List<string> lstReferenceStandards = new List<string> { "ASTM E3-11", "ASTM E407", "ASM METAL HANDBOOK VOL-9", "ASTM E112-13", "ASTM E45-18" };
            ViewBag.lstReferenceStandards = lstReferenceStandards.Select(x => new ddlValue() { id = x.ToString(), text = x.ToString() }).ToList();

            List<string> _lstYesNo = new List<string> { "Yes", "No" };
            ViewBag._lstYesNo = _lstYesNo.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #endregion

            List<clsPTR019> _objPTR019 = new List<clsPTR019>();

            _objPTR019 = (from x in db.PTR019
                          where x.PTRHeaderId == PTRHeaderId
                          select new clsPTR019
                          {
                              HeaderId = x.HeaderId,
                              ReportName = x.ReportName,
                              ReportNo = x.ReportNo,
                              RefStd = x.RefStd,
                              SOPNo = x.SOPNo,
                              SampleId = x.SampleId,
                              TestRemarks = x.TestRemarks,
                              IsInclusionRating = x.IsInclusionRating,
                              ThinA = x.ThinA,
                              ThinB = x.ThinB,
                              ThinC = x.ThinC,
                              ThinD = x.ThinD,
                              ThickA = x.ThickA,
                              ThickB = x.ThickB,
                              ThickC = x.ThickC,
                              ThickD = x.ThickD,
                              PhotoHeading1 = x.PhotoHeading1,
                              PhotoHeading2 = x.PhotoHeading2,
                              PhotoHeading3 = x.PhotoHeading3,
                              PhotoHeading4 = x.PhotoHeading4,
                              PhotoHeading5 = x.PhotoHeading5,
                              PhotoHeading6 = x.PhotoHeading6,
                              TempValue = x.TempValue,
                              HumidityValue = x.HumidityValue,
                              Remarks1 = x.Remarks1,
                              Remarks2 = x.Remarks2,
                              CreatedBy = x.CreatedBy,
                              CreatedOn = x.CreatedOn,
                              EditedBy = x.EditedBy,
                              EditedOn = x.EditedOn,
                              PhotoHeading7 = x.PhotoHeading7,
                              PhotoHeading8 = x.PhotoHeading8,
                              PhotoHeading9 = x.PhotoHeading9,
                              PhotoHeading10 = x.PhotoHeading10,
                              PhotoHeading11 = x.PhotoHeading11,
                              PhotoHeading12 = x.PhotoHeading12,
                              PTRHeaderId = x.PTRHeaderId,
                              PTR019_Log = db.PTR019_Log.Where(w => w.HeaderId == x.HeaderId).ToList()
                          }).ToList();
            var _SOPNo = "SOP/MT/006";
            var _ReportNo = "";
            var _ReportName = "";
            if (_objPTR019.Count > 0)
            {
                _SOPNo = _objPTR019.FirstOrDefault().SOPNo;
                _ReportName = _objPTR019.FirstOrDefault().ReportName;
            }
            if (db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).Count() > 0)
            {
                _ReportNo = db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).FirstOrDefault().ReportNo;
            }


            if (reportNo != null && reportNo != "")
            {
                var log = new List<PTR019_Log>();
                log.Add(new PTR019_Log
                {
                    LineId = 0,
                    ActualValue = "Surface is free from defects."
                });
                log.Add(new PTR019_Log
                {
                    LineId = 0,
                    ActualValue = "Micro Examination is satisfactory."
                });
                log.Add(new PTR019_Log
                {
                    LineId = 0,
                    ActualValue = "Weld microstructure shows Dendritic structure of Ferrite in Austenite matrix."
                });
                log.Add(new PTR019_Log
                {
                    LineId = 0,
                    ActualValue = "HAZ & Base metal shows Tempered Martensite and Bainite structure."
                });
                log.Add(new PTR019_Log
                {
                    LineId = 0,
                    ActualValue = "Base metal shows Ferrite & pearlite structure."
                });
                log.Add(new PTR019_Log
                {
                    LineId = 0,
                    ActualValue = "HAZ shows fine structure of Ferrite & pearlite."
                });

                _objPTR019.Add(new clsPTR019 { ReportNo = _ReportNo, ReportName = _ReportName, HeaderId = 0, PTRHeaderId = PTRHeaderId, SOPNo = _SOPNo, PTR019_Log = log });
            }
            else if (_objPTR019.Count() == 0)
            {
                var objPTR019 = new clsPTR019();
                objPTR019.PTR019_Log = new List<PTR019_Log>();
                objPTR019.PTRHeaderId = PTRHeaderId;
                objPTR019.SOPNo = "SOP/MT/006";
                objPTR019.TestRemarks = "Sample is polished and etched by Aqua regia and examined for micro. The observations are as below.";
                objPTR019.RefStd = "ASTM E3-11";
                objPTR019.ReportName = "MICRO,GRAIN SIZE & INCLUSION RATING";
                objPTR019.IsInclusionRating = "No";
                objPTR019.ThinA = "0.0";
                objPTR019.ThinB = "0.0";
                objPTR019.ThinC = "0.0";
                objPTR019.ThinD = "1.0";
                objPTR019.ThickA = "0.0";
                objPTR019.ThickB = "0.0";
                objPTR019.ThickC = "0.0";
                objPTR019.ThickD = "0.0";
                objPTR019.ReportNo = _ReportNo;


                #region Default line table data display

                objPTR019.PTR019_Log.Add(new PTR019_Log
                {
                    LineId = 0,
                    ActualValue = "Surface is free from defects."
                });
                objPTR019.PTR019_Log.Add(new PTR019_Log
                {
                    LineId = 0,
                    ActualValue = "Micro Examination is satisfactory."
                });
                objPTR019.PTR019_Log.Add(new PTR019_Log
                {
                    LineId = 0,
                    ActualValue = "Weld microstructure shows Dendritic structure of Ferrite in Austenite matrix."
                });
                objPTR019.PTR019_Log.Add(new PTR019_Log
                {
                    LineId = 0,
                    ActualValue = "HAZ & Base metal shows Tempered Martensite and Bainite structure."
                });
                objPTR019.PTR019_Log.Add(new PTR019_Log
                {
                    LineId = 0,
                    ActualValue = "Base metal shows Ferrite & pearlite structure."
                });
                objPTR019.PTR019_Log.Add(new PTR019_Log
                {
                    LineId = 0,
                    ActualValue = "HAZ shows fine structure of Ferrite & pearlite."
                });

                _objPTR019.Add(objPTR019);
                #endregion
            }

            ViewBag.isEditable = IsPTRReportEditable(PTRHeaderId);
            return PartialView("Micro", _objPTR019);
        }

        public ActionResult microSketchPartial(clsMicroSkectchPara para)
        {
            return View(para);
        }


        public ActionResult SaveMicroHeaderData(PTR019 _obj)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                bool isAdd = false;
                PTR019 _objData = new PTR019();
                if (_obj.HeaderId == 0) //Add
                    isAdd = true;
                else
                    _objData = db.PTR019.Where(c => c.HeaderId == _obj.HeaderId).FirstOrDefault();

                _objData.ReportName = _obj.ReportName;
                _objData.ReportNo = _obj.ReportNo;
                _objData.RefStd = _obj.RefStd;
                _objData.SOPNo = _obj.SOPNo;
                _objData.SampleId = _obj.SampleId;
                _objData.TestRemarks = _obj.TestRemarks;
                _objData.PhotoHeading1 = _obj.PhotoHeading1;
                _objData.PhotoHeading2 = _obj.PhotoHeading2;
                _objData.PhotoHeading3 = _obj.PhotoHeading3;
                _objData.PhotoHeading4 = _obj.PhotoHeading4;
                _objData.PhotoHeading5 = _obj.PhotoHeading5;
                _objData.PhotoHeading6 = _obj.PhotoHeading6;
                _objData.PhotoHeading7 = _obj.PhotoHeading7;
                _objData.PhotoHeading8 = _obj.PhotoHeading8;
                _objData.PhotoHeading9 = _obj.PhotoHeading9;
                _objData.PhotoHeading10 = _obj.PhotoHeading10;
                _objData.PhotoHeading11 = _obj.PhotoHeading11;
                _objData.PhotoHeading12 = _obj.PhotoHeading12;

                _objData.IsInclusionRating = _obj.IsInclusionRating;

                _objData.ThinA = _obj.ThinA;
                _objData.ThinB = _obj.ThinB;
                _objData.ThinC = _obj.ThinC;
                _objData.ThinD = _obj.ThinD;

                _objData.ThickA = _obj.ThickA;
                _objData.ThickB = _obj.ThickB;
                _objData.ThickC = _obj.ThickC;
                _objData.ThickD = _obj.ThickD;

                _objData.TempValue = _obj.TempValue;
                _objData.HumidityValue = _obj.HumidityValue;
                _objData.Remarks1 = _obj.Remarks1;
                _objData.Remarks2 = _obj.Remarks2;
                _objData.PTRHeaderId = _obj.PTRHeaderId;

                if (isAdd)
                {
                    _objData.CreatedBy = objClsLoginInfo.UserName;
                    _objData.CreatedOn = DateTime.Now;
                    db.PTR019.Add(_objData);
                }
                else
                {
                    _objData.EditedBy = objClsLoginInfo.UserName;
                    _objData.EditedOn = DateTime.Now;
                }

                db.SaveChanges();

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data saved successfully";
                objResponseMsg.HeaderId = _objData.HeaderId;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveMicroLineTable1(List<PTR019_Log> _lst, int HeaderId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                List<PTR019_Log> _lstAdd = new List<PTR019_Log>();
                List<PTR019_Log> _lstDelete = new List<PTR019_Log>();
                if (_lst != null)
                {
                    foreach (var item in _lst)
                    {
                        if (item.LineId > 0)
                        {
                            PTR019_Log _objUpdate = db.PTR019_Log.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (_objUpdate != null)
                            {
                                _objUpdate.HeaderId = item.HeaderId;
                                _objUpdate.ActualValue = item.ActualValue;

                                _objUpdate.EditedBy = objClsLoginInfo.UserName;
                                _objUpdate.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PTR019_Log _objAdd = new PTR019_Log();
                            _objAdd.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.ActualValue))
                            {
                                _objAdd.HeaderId = item.HeaderId;
                                _objAdd.ActualValue = item.ActualValue;

                                _objAdd.CreatedBy = objClsLoginInfo.UserName;
                                _objAdd.CreatedOn = DateTime.Now;
                                _lstAdd.Add(_objAdd);
                            }
                        }
                    }
                    if (_lstAdd.Count > 0)
                        db.PTR019_Log.AddRange(_lstAdd);

                    var LineIds = _lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = _lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    _lstDelete = db.PTR019_Log.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (_lstDelete.Count > 0)
                        db.PTR019_Log.RemoveRange(_lstDelete);
                    db.SaveChanges();
                }
                else
                {
                    _lstDelete = db.PTR019_Log.Where(x => x.HeaderId == HeaderId).ToList();
                    if (_lstDelete.Count > 0)
                        db.PTR019_Log.RemoveRange(_lstDelete);
                    db.SaveChanges();
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data saved successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteMicroHeaderData(int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {

                var header = db.PTR019.Where(t => t.HeaderId == headerId).FirstOrDefault();
                if (header != null)
                {
                    var logs = db.PTR019_Log.Where(t => t.HeaderId == headerId).ToList();
                    db.PTR019_Log.RemoveRange(logs);
                    db.PTR019.Remove(header);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Data deleted successfully";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Record not found.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }




        #endregion

        #region MACRO MLP
        public ActionResult MicroMLP(int? PTRHeaderId, string reportNo)
        {
            #region ViewBag
            ViewBag.ControllerURL = ControllerURL;

            List<string> _lstTestRemarks = new List<string> {
                "Tubes are cut from Quarter section, 04 face of each tubes are polished and etched by 2% Nital and examined for macro at 10X & 30X. The observation are as below.",
                "Tubes are cut from Quarter section, 04 face of each tubes are polished and etched by Aqua Regia and examined for macro at 10X & 30X. The observation are as below.",
                "Tubes are cut from half section, 04 face of each tubes are polished and etched by 2% Nital and examined for macro at 10X & 30X. The observation are as below.",
                "Tubes are cut from half section, 04 face of each tubes are polished and etched by Aqua Regia and examined for macro at 10X & 30X. The observation are as below.",
                "Tubes are cut from half section, 02 face of each tubes are polished and etched by 2% Nital and examined for macro at 10X & 30X. The observation are as below.",
                "Tubes are cut from half section, 02 face of each tubes are polished and etched by Aqua Regia and examined for macro at 10X & 30X. The observation are as below."
            };

            ViewBag._lstTestRemarks = _lstTestRemarks.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            List<string> lstReferenceStandards = new List<string> { "ASME SEC IX- Ed2017", "ASME SEC IX" };
            ViewBag.lstReferenceStandards = lstReferenceStandards.Select(x => new ddlValue() { id = x.ToString(), text = x.ToString() }).ToList();

            List<string> _lstResult = new List<string> { "ACCEPTABLE", "NOT ACCEPTABLE", "To report" };
            ViewBag._lstResult = _lstResult.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #endregion

            List<clsPTR020> _objPTR020 = new List<clsPTR020>();
            _objPTR020 = (from x in db.PTR020
                          where x.PTRHeaderId == PTRHeaderId
                          select new clsPTR020
                          {
                              HeaderId = x.HeaderId,
                              ReportNo = x.ReportNo,
                              RefStd = x.RefStd,
                              SOPNo = x.SOPNo,
                              TestRemarks = x.TestRemarks,
                              LeakPathExamination1 = x.LeakPathExamination1,
                              LeakPathExamination2 = x.LeakPathExamination2,
                              AxialLength1 = x.AxialLength1,
                              AxialLength2 = x.AxialLength2,
                              MaxPenetrationOnTube = x.MaxPenetrationOnTube,
                              PhotoHeading1 = x.PhotoHeading1,
                              PhotoHeading2 = x.PhotoHeading2,
                              Result = x.Result,
                              TempValue = x.TempValue,
                              HumidityValue = x.HumidityValue,
                              Remarks1 = x.Remarks1,
                              Remarks2 = x.Remarks2,
                              CreatedBy = x.CreatedBy,
                              CreatedOn = x.CreatedOn,
                              EditedBy = x.EditedBy,
                              EditedOn = x.EditedOn,
                              PTRHeaderId = x.PTRHeaderId,
                              PTR020_Log = db.PTR020_Log.Where(t => t.HeaderId == x.HeaderId).ToList(),
                              PTR020_Log2 = db.PTR020_Log2.Where(t => t.HeaderId == x.HeaderId).ToList(),
                              PTR020_Log3 = db.PTR020_Log3.Where(t => t.HeaderId == x.HeaderId).ToList(),
                          }).ToList();
            var _SOPNo = "SOP/MT/006A";
            var _ReportNo = string.Empty;
            if (_objPTR020.Count > 0)
            {
                _SOPNo = _objPTR020.FirstOrDefault().SOPNo;
            }
            if (db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).Count() > 0)
                _ReportNo = db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).FirstOrDefault().ReportNo;

            /*if (PTRHeaderId.HasValue && db.PTR020.Where(w => w.PTRHeaderId == PTRHeaderId).Count() > 0)
            {
                _objPTR020 = db.PTR020.Where(w => w.PTRHeaderId == PTRHeaderId).FirstOrDefault();
                ViewBag.lstPTR020_Log = db.PTR020_Log.Where(w => w.HeaderId == _objPTR020.HeaderId).ToList();
                ViewBag.lstPTR020_Log_2 = db.PTR020_Log2.Where(w => w.HeaderId == _objPTR020.HeaderId).ToList();
                ViewBag.lstPTR020_Log_3 = db.PTR020_Log3.Where(w => w.HeaderId == _objPTR020.HeaderId).ToList();

                if (db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).Count() > 0)
                    _objPTR020.ReportNo = db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).FirstOrDefault().ReportNo;
            }*/
            if (reportNo != null && reportNo != "")
            {
                #region Default line table data display
                List<PTR020_Log3> _lst = new List<PTR020_Log3>();
                PTR020_Log3 _obj = new PTR020_Log3();
                _obj.LineId = 0;
                _obj.ActualValue = "Tube sheet & tube wall face found complete fusion of weld deposit.";
                _lst.Add(_obj);
                _obj = new PTR020_Log3();
                _obj.LineId = 0;
                _obj.ActualValue = "Root of joint found complete penetration of the weld deposit.";
                _lst.Add(_obj);
                _obj = new PTR020_Log3();
                _obj.LineId = 0;
                _obj.ActualValue = "Porosity & cracks are not observed.";
                _lst.Add(_obj);
                _obj = new PTR020_Log3();
                _obj.LineId = 0;
                _obj.ActualValue = "MLP & Axial length verified and result is as per table below.";
                _lst.Add(_obj);
                _obj = new PTR020_Log3();
                _obj.LineId = 0;
                _obj.ActualValue = "Macro Examination is satisfactory ";
                _lst.Add(_obj);
                _obj = new PTR020_Log3();
                _obj.LineId = 0;
                _obj.ActualValue = "Macro Examination is not satisfactory ";
                _lst.Add(_obj);
                _obj = new PTR020_Log3();
                _obj.LineId = 0;
                _obj.ActualValue = "Lack of fusion observed.";
                _lst.Add(_obj);

                ViewBag.lstPTR020_Log_3 = _lst;
                #endregion
                _objPTR020.Add(new clsPTR020
                {
                    SOPNo = _SOPNo,
                    TestRemarks = "Tubes are cut from Quarter section, 04 face of each tubes are polished and etched by 2% Nital and examined for macro at 10X & 30X. The observation are as below.",
                    RefStd = "ASME SEC IX- Ed2017",
                    Result = "ACCEPTABLE",
                    ReportNo = _ReportNo,
                    PTRHeaderId = PTRHeaderId,
                    PTR020_Log3 = _lst,
                });

            }
            else if (_objPTR020.Count() == 0)
            {

                #region Default line table data display
                List<PTR020_Log3> _lst = new List<PTR020_Log3>();
                PTR020_Log3 _obj = new PTR020_Log3();
                _obj.LineId = 0;
                _obj.ActualValue = "Tube sheet & tube wall face found complete fusion of weld deposit.";
                _lst.Add(_obj);
                _obj = new PTR020_Log3();
                _obj.LineId = 0;
                _obj.ActualValue = "Root of joint found complete penetration of the weld deposit.";
                _lst.Add(_obj);
                _obj = new PTR020_Log3();
                _obj.LineId = 0;
                _obj.ActualValue = "Porosity & cracks are not observed.";
                _lst.Add(_obj);
                _obj = new PTR020_Log3();
                _obj.LineId = 0;
                _obj.ActualValue = "MLP & Axial length verified and result is as per table below.";
                _lst.Add(_obj);
                _obj = new PTR020_Log3();
                _obj.LineId = 0;
                _obj.ActualValue = "Macro Examination is satisfactory ";
                _lst.Add(_obj);
                _obj = new PTR020_Log3();
                _obj.LineId = 0;
                _obj.ActualValue = "Macro Examination is not satisfactory ";
                _lst.Add(_obj);
                _obj = new PTR020_Log3();
                _obj.LineId = 0;
                _obj.ActualValue = "Lack of fusion observed.";
                _lst.Add(_obj);

                //ViewBag.lstPTR020_Log_3 = _lst;
                #endregion
                _objPTR020.Add(new clsPTR020
                {
                    SOPNo = "SOP/MT/006A",
                    TestRemarks = "Tubes are cut from Quarter section, 04 face of each tubes are polished and etched by 2% Nital and examined for macro at 10X & 30X. The observation are as below.",
                    RefStd = "ASME SEC IX- Ed2017",
                    Result = "ACCEPTABLE",
                    ReportNo = _ReportNo,
                    PTRHeaderId = PTRHeaderId,
                    PTR020_Log3 = _lst
                });
            }

            #region Image path
            List<string> _lstPath1 = new List<string>();

            for (int i = 0; i < _objPTR020.Count; i++)
            {
                long lHeaderId = 0;
                long.TryParse(_objPTR020[i].HeaderId.ToString(), out lHeaderId);
                FCS001 _objFCS = db.FCS001.Where(w => w.TableId == lHeaderId && w.TableName == "PTR020" && w.ViewerId == 1).FirstOrDefault();
                if (_objFCS != null)
                    _lstPath1.Add(_objFCS.MainDocumentPath);
                else
                    _lstPath1.Add("");
            }

            List<string> _lstPath2 = new List<string>();
            for (int i = 0; i < _objPTR020.Count; i++)
            {
                long lHeaderId = 0;
                long.TryParse(_objPTR020[i].HeaderId.ToString(), out lHeaderId);
                FCS001 _objFCS = db.FCS001.Where(w => w.TableId == lHeaderId && w.TableName == "PTR020" && w.ViewerId == 2).FirstOrDefault();
                if (_objFCS != null)
                    _lstPath2.Add(_objFCS.MainDocumentPath);
                else
                    _lstPath2.Add("");
            }
            #endregion

            ViewBag._lstImagePaths1 = _lstPath1;
            ViewBag._lstImagePaths2 = _lstPath2;
            ViewBag.isEditable = IsPTRReportEditable(PTRHeaderId);
            return PartialView("MicroMLP", _objPTR020);
        }

        public ActionResult SaveMACROMLPHeaderData(PTR020 _obj)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                bool isAdd = false;
                PTR020 _objData = new PTR020();
                if (_obj.HeaderId == 0) //Add
                    isAdd = true;
                else
                    _objData = db.PTR020.Where(c => c.HeaderId == _obj.HeaderId).FirstOrDefault();

                _objData.ReportNo = _obj.ReportNo;
                _objData.RefStd = _obj.RefStd;
                _objData.SOPNo = _obj.SOPNo;
                _objData.TestRemarks = _obj.TestRemarks;
                _objData.LeakPathExamination1 = _obj.LeakPathExamination1;
                _objData.LeakPathExamination2 = _obj.LeakPathExamination2;
                _objData.AxialLength1 = _obj.AxialLength1;
                _objData.AxialLength2 = _obj.AxialLength2;
                _objData.MaxPenetrationOnTube = _obj.MaxPenetrationOnTube;
                _objData.PhotoHeading1 = _obj.PhotoHeading1;
                _objData.PhotoHeading2 = _obj.PhotoHeading2;
                _objData.Result = _obj.Result;

                _objData.TempValue = _obj.TempValue;
                _objData.HumidityValue = _obj.HumidityValue;
                _objData.Remarks1 = _obj.Remarks1;
                _objData.Remarks2 = _obj.Remarks2;
                _objData.PTRHeaderId = _obj.PTRHeaderId;

                if (isAdd)
                {
                    _objData.CreatedBy = objClsLoginInfo.UserName;
                    _objData.CreatedOn = DateTime.Now;
                    db.PTR020.Add(_objData);
                }
                else
                {
                    _objData.EditedBy = objClsLoginInfo.UserName;
                    _objData.EditedOn = DateTime.Now;
                }

                db.SaveChanges();

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data saved successfully";
                objResponseMsg.HeaderId = _objData.HeaderId;
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting
                        // the current instance as InnerException
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            //catch (Exception ex)
            //{
            //    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            //    objResponseMsg.Key = false;
            //    objResponseMsg.Value = ex.Message;
            //}
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveMACROMLPLineTable1(List<PTR020_Log> _lst, int HeaderId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                List<PTR020_Log> _lstAdd = new List<PTR020_Log>();
                List<PTR020_Log> _lstDelete = new List<PTR020_Log>();
                if (_lst != null)
                {
                    foreach (var item in _lst)
                    {
                        if (item.LineId > 0)
                        {
                            PTR020_Log _objUpdate = db.PTR020_Log.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (_objUpdate != null)
                            {
                                _objUpdate.HeaderId = item.HeaderId;
                                _objUpdate.TubeNo = item.TubeNo;
                                _objUpdate.FaceA = item.FaceA;
                                _objUpdate.FaceB = item.FaceB;
                                _objUpdate.FaceC = item.FaceC;
                                _objUpdate.FaceD = item.FaceD;
                                _objUpdate.MLP = item.MLP;

                                _objUpdate.EditedBy = objClsLoginInfo.UserName;
                                _objUpdate.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PTR020_Log _objAdd = new PTR020_Log();
                            _objAdd.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.TubeNo))
                            {
                                _objAdd.HeaderId = item.HeaderId;
                                _objAdd.TubeNo = item.TubeNo;
                                _objAdd.FaceA = item.FaceA;
                                _objAdd.FaceB = item.FaceB;
                                _objAdd.FaceC = item.FaceC;
                                _objAdd.FaceD = item.FaceD;
                                _objAdd.MLP = item.MLP;

                                _objAdd.CreatedBy = objClsLoginInfo.UserName;
                                _objAdd.CreatedOn = DateTime.Now;
                                _lstAdd.Add(_objAdd);
                            }
                        }
                    }
                    if (_lstAdd.Count > 0)
                        db.PTR020_Log.AddRange(_lstAdd);

                    var LineIds = _lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = _lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    _lstDelete = db.PTR020_Log.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (_lstDelete.Count > 0)
                        db.PTR020_Log.RemoveRange(_lstDelete);
                    db.SaveChanges();
                }
                else
                {
                    _lstDelete = db.PTR020_Log.Where(x => x.HeaderId == HeaderId).ToList();
                    if (_lstDelete.Count > 0)
                        db.PTR020_Log.RemoveRange(_lstDelete);
                    db.SaveChanges();
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data saved successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveMACROMLPLineTable2(List<PTR020_Log2> _lst, int HeaderId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                List<PTR020_Log2> _lstAdd = new List<PTR020_Log2>();
                List<PTR020_Log2> _lstDelete = new List<PTR020_Log2>();
                if (_lst != null)
                {
                    foreach (var item in _lst)
                    {
                        if (item.LineId > 0)
                        {
                            PTR020_Log2 _objUpdate = db.PTR020_Log2.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (_objUpdate != null)
                            {
                                _objUpdate.HeaderId = item.HeaderId;
                                _objUpdate.TubeNo = item.TubeNo;
                                _objUpdate.FaceA = item.FaceA;
                                _objUpdate.FaceB = item.FaceB;
                                _objUpdate.FaceC = item.FaceC;
                                _objUpdate.FaceD = item.FaceD;
                                _objUpdate.MaxPenetrationOnTube = item.MaxPenetrationOnTube;

                                _objUpdate.EditedBy = objClsLoginInfo.UserName;
                                _objUpdate.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PTR020_Log2 _objAdd = new PTR020_Log2();
                            _objAdd.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.TubeNo))
                            {
                                _objAdd.HeaderId = item.HeaderId;
                                _objAdd.TubeNo = item.TubeNo;
                                _objAdd.FaceA = item.FaceA;
                                _objAdd.FaceB = item.FaceB;
                                _objAdd.FaceC = item.FaceC;
                                _objAdd.FaceD = item.FaceD;
                                _objAdd.MaxPenetrationOnTube = item.MaxPenetrationOnTube;

                                _objAdd.CreatedBy = objClsLoginInfo.UserName;
                                _objAdd.CreatedOn = DateTime.Now;
                                _lstAdd.Add(_objAdd);
                            }
                        }
                    }
                    if (_lstAdd.Count > 0)
                        db.PTR020_Log2.AddRange(_lstAdd);

                    var LineIds = _lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = _lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    _lstDelete = db.PTR020_Log2.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (_lstDelete.Count > 0)
                        db.PTR020_Log2.RemoveRange(_lstDelete);
                    db.SaveChanges();
                }
                else
                {
                    _lstDelete = db.PTR020_Log2.Where(x => x.HeaderId == HeaderId).ToList();
                    if (_lstDelete.Count > 0)
                        db.PTR020_Log2.RemoveRange(_lstDelete);
                    db.SaveChanges();
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data saved successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveMACROMLPLineTable3(List<PTR020_Log3> _lst, int HeaderId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                List<PTR020_Log3> _lstAdd = new List<PTR020_Log3>();
                List<PTR020_Log3> _lstDelete = new List<PTR020_Log3>();
                if (_lst != null)
                {
                    foreach (var item in _lst)
                    {
                        if (item.LineId > 0)
                        {
                            PTR020_Log3 _objUpdate = db.PTR020_Log3.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (_objUpdate != null)
                            {
                                _objUpdate.HeaderId = item.HeaderId;
                                _objUpdate.ActualValue = item.ActualValue;

                                _objUpdate.EditedBy = objClsLoginInfo.UserName;
                                _objUpdate.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PTR020_Log3 _objAdd = new PTR020_Log3();
                            _objAdd.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.ActualValue))
                            {
                                _objAdd.HeaderId = item.HeaderId;
                                _objAdd.ActualValue = item.ActualValue;

                                _objAdd.CreatedBy = objClsLoginInfo.UserName;
                                _objAdd.CreatedOn = DateTime.Now;
                                _lstAdd.Add(_objAdd);
                            }
                        }
                    }
                    if (_lstAdd.Count > 0)
                        db.PTR020_Log3.AddRange(_lstAdd);

                    var LineIds = _lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = _lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    _lstDelete = db.PTR020_Log3.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (_lstDelete.Count > 0)
                        db.PTR020_Log3.RemoveRange(_lstDelete);
                    db.SaveChanges();
                }
                else
                {
                    _lstDelete = db.PTR020_Log3.Where(x => x.HeaderId == HeaderId).ToList();
                    if (_lstDelete.Count > 0)
                        db.PTR020_Log3.RemoveRange(_lstDelete);
                    db.SaveChanges();
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data saved successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeleteMacroMLPHeaderData(int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {

                var header = db.PTR020.Where(t => t.HeaderId == headerId).FirstOrDefault();
                if (header != null)
                {
                    var logs = db.PTR020_Log.Where(t => t.HeaderId == headerId).ToList();
                    var logs2 = db.PTR020_Log2.Where(t => t.HeaderId == headerId).ToList();
                    var logs3 = db.PTR020_Log3.Where(t => t.HeaderId == headerId).ToList();

                    db.PTR020_Log.RemoveRange(logs);
                    db.PTR020_Log2.RemoveRange(logs2);
                    db.PTR020_Log3.RemoveRange(logs3);

                    db.PTR020.Remove(header);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Data deleted successfully";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Record not found.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region TearTest
        public ActionResult TearTest(int? PTRHeaderId, string reportNo)
        {
            #region ViewBag
            ViewBag.ControllerURL = ControllerURL;
            #endregion

            List<PTR021> _objPTR021 = new List<PTR021>();
            _objPTR021 = (from x in db.PTR021
                          where x.PTRHeaderId == PTRHeaderId
                          select x).ToList();

            /*if (PTRHeaderId.HasValue && db.PTR021.Where(w => w.PTRHeaderId == PTRHeaderId).Count() > 0)
            {
                _objPTR021 = db.PTR021.Where(w => w.PTRHeaderId == PTRHeaderId).FirstOrDefault();
                if (db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).Count() > 0)
                    _objPTR021.ReportNo = db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).FirstOrDefault().ReportNo;
            }
            */
            var _SOPNo = string.Empty;
            var _ReportNo = string.Empty;
            if (_objPTR021.Count > 0)
            {
                _SOPNo = _objPTR021.FirstOrDefault().SOPNo;
            }
            if (db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).Count() > 0)
                _ReportNo = db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).FirstOrDefault().ReportNo;

            if (reportNo != null && reportNo != "")
            {
                _objPTR021.Add(new PTR021
                {
                    ReportNo = _ReportNo,
                    SOPNo = _SOPNo,
                    PTRHeaderId = PTRHeaderId,
                    TestRemarks = "Tube was cut in to 4 section with projected tube length. Quarter section was tore as per below sketch.",
                    Observation = "Underside / Root side of weld joint shown adequate & uniform weld penetration. Tear Test are satisfactory."
                });
            }
            else if (_objPTR021.Count() == 0)
            {
                _objPTR021.Add(new PTR021
                {
                    ReportNo = _ReportNo,
                    PTRHeaderId = PTRHeaderId,
                    TestRemarks = "Tube was cut in to 4 section with projected tube length. Quarter section was tore as per below sketch.",
                    Observation = "Underside / Root side of weld joint shown adequate & uniform weld penetration. Tear Test are satisfactory."
                });
            }

            #region Image path
            List<string> _lstPath = new List<string>();
            for (int i = 0; i < _objPTR021.Count; i++)
            {
                long lHeaderId = 0;
                long.TryParse(_objPTR021[i].HeaderId.ToString(), out lHeaderId);
                FCS001 _objFCS = db.FCS001.Where(w => w.TableId == lHeaderId && w.TableName == "PTR021" && w.ViewerId == 2).FirstOrDefault();
                if (_objFCS != null)
                    _lstPath.Add(_objFCS.MainDocumentPath);
                else
                    _lstPath.Add("");
            }
            #endregion

            ViewBag._lstImagePaths = _lstPath;
            ViewBag.isEditable = IsPTRReportEditable(PTRHeaderId);
            return PartialView("TearTest", _objPTR021);
        }

        public ActionResult SaveTearTestHeaderData(PTR021 _obj)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                bool isAdd = false;
                PTR021 _objData = new PTR021();
                if (_obj.HeaderId == 0) //Add
                    isAdd = true;
                else
                    _objData = db.PTR021.Where(c => c.HeaderId == _obj.HeaderId).FirstOrDefault();

                _objData.ReportNo = _obj.ReportNo;
                _objData.RefStd = _obj.RefStd;
                _objData.SOPNo = _obj.SOPNo;
                _objData.SampleId = _obj.SampleId;
                _objData.TestSample = _obj.TestSample;
                _objData.TestRemarks = _obj.TestRemarks;
                _objData.Observation = _obj.Observation;
                _objData.PhotoHeading1 = _obj.PhotoHeading1;
                _objData.PhotoHeading2 = _obj.PhotoHeading2;

                _objData.TempValue = _obj.TempValue;
                _objData.HumidityValue = _obj.HumidityValue;
                _objData.Remarks1 = _obj.Remarks1;
                _objData.Remarks2 = _obj.Remarks2;
                _objData.PTRHeaderId = _obj.PTRHeaderId;
                _objData.PhotoHeading1 = _obj.PhotoHeading1;
                _objData.PhotoHeading2 = _obj.PhotoHeading2;

                if (isAdd)
                {
                    _objData.CreatedBy = objClsLoginInfo.UserName;
                    _objData.CreatedOn = DateTime.Now;
                    db.PTR021.Add(_objData);
                }
                else
                {
                    _objData.EditedBy = objClsLoginInfo.UserName;
                    _objData.EditedOn = DateTime.Now;
                }

                db.SaveChanges();

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data saved successfully";
                objResponseMsg.HeaderId = _objData.HeaderId;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeleteTearHeaderData(int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {

                var header = db.PTR021.Where(t => t.HeaderId == headerId).FirstOrDefault();
                if (header != null)
                {
                    db.PTR021.Remove(header);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Data deleted successfully";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Record not found.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Flattening
        public ActionResult Flattening(int? PTRHeaderId, string reportNo)
        {
            #region ViewBag
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstReferenceStandards = new List<string> { "ASME SEC IIA SA999", "ASME SEC IIA SA1016" };
            ViewBag.lstReferenceStandards = lstReferenceStandards.Select(x => new ddlValue() { id = x.ToString(), text = x.ToString() }).ToList();
            #endregion

            List<clsPTR022> _objPTR022 = new List<clsPTR022>();
            _objPTR022 = (from x in db.PTR022
                          where x.PTRHeaderId == PTRHeaderId
                          select new clsPTR022
                          {
                              HeaderId = x.HeaderId,
                              ReportNo = x.ReportNo,
                              RefStd = x.RefStd,
                              SOPNo = x.SOPNo,
                              SpecifiedWallThickness = x.SpecifiedWallThickness,
                              SpecifiedOutsideDiameter = x.SpecifiedOutsideDiameter,
                              ConstantValue = x.ConstantValue,
                              DistanceBet2Flattening = x.DistanceBet2Flattening,
                              FirstStep = x.FirstStep,
                              FirstStepAcceptance = x.FirstStepAcceptance,
                              SecondStep = x.SecondStep,
                              SecondStepAcceptance = x.SecondStepAcceptance,
                              TempValue = x.TempValue,
                              HumidityValue = x.HumidityValue,
                              Remarks1 = x.Remarks1,
                              Remarks2 = x.Remarks2,
                              CreatedBy = x.CreatedBy,
                              CreatedOn = x.CreatedOn,
                              EditedBy = x.EditedBy,
                              EditedOn = x.EditedOn,
                              Length = x.Length,
                              PTRHeaderId = x.PTRHeaderId,
                              PTR022_Log = db.PTR022_Log.Where(w => w.HeaderId == x.HeaderId).ToList()
                          }).ToList();
            var _SOPNo = "SOP/MT/002";
            var _ReportNo = string.Empty;
            if (_objPTR022.Count > 0)
            {
                _SOPNo = _objPTR022.FirstOrDefault().SOPNo;
            }
            if (db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).Count() > 0)
                _ReportNo = db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).FirstOrDefault().ReportNo;

            /*if (PTRHeaderId.HasValue && db.PTR022.Where(w => w.PTRHeaderId == PTRHeaderId).Count() > 0)
            {

                _objPTR022 = db.PTR022.Where(w => w.PTRHeaderId == PTRHeaderId).FirstOrDefault();
                ViewBag._lstPTR022_Log = db.PTR022_Log.Where(w => w.HeaderId == _objPTR022.HeaderId).ToList();

                if (db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).Count() > 0)
                    _objPTR022.ReportNo = db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).FirstOrDefault().ReportNo;

            }*/
            if (reportNo != null && reportNo != "")
            {
                _objPTR022.Add(new clsPTR022
                {
                    PTRHeaderId = PTRHeaderId,
                    SOPNo = _SOPNo,
                    FirstStepAcceptance = "No cracks or breaks on the inside, out side or end surface of sample shall occur at first step of flattening.",
                    ReportNo = _ReportNo
                });
            }
            else if (_objPTR022.Count() == 0)
            {
                _objPTR022.Add(new clsPTR022
                {
                    SOPNo = "SOP/MT/002",
                    FirstStepAcceptance = "No cracks or breaks on the inside, out side or end surface of sample shall occur at first step of flattening.",
                    ReportNo = _ReportNo,
                    PTRHeaderId = PTRHeaderId
                });
            }

            ViewBag.isEditable = IsPTRReportEditable(PTRHeaderId);
            return PartialView("Flattening", _objPTR022);
        }

        public ActionResult SaveFlatteningHeaderData(PTR022 _obj)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                bool isAdd = false;
                PTR022 _objData = new PTR022();
                if (_obj.HeaderId == 0) //Add
                    isAdd = true;
                else
                    _objData = db.PTR022.Where(c => c.HeaderId == _obj.HeaderId).FirstOrDefault();

                _objData.ReportNo = _obj.ReportNo;
                _objData.RefStd = _obj.RefStd;
                _objData.SOPNo = _obj.SOPNo;
                _objData.Length = _obj.Length;
                _objData.SpecifiedWallThickness = _obj.SpecifiedWallThickness;
                _objData.SpecifiedOutsideDiameter = _obj.SpecifiedOutsideDiameter;
                _objData.ConstantValue = _obj.ConstantValue;
                _objData.DistanceBet2Flattening = _obj.DistanceBet2Flattening;
                _objData.FirstStep = _obj.FirstStep;
                _objData.FirstStepAcceptance = _obj.FirstStepAcceptance;
                _objData.SecondStep = _obj.SecondStep;
                _objData.SecondStepAcceptance = _obj.SecondStepAcceptance;

                _objData.TempValue = _obj.TempValue;
                _objData.HumidityValue = _obj.HumidityValue;
                _objData.Remarks1 = _obj.Remarks1;
                _objData.Remarks2 = _obj.Remarks2;
                _objData.PTRHeaderId = _obj.PTRHeaderId;

                if (isAdd)
                {
                    _objData.CreatedBy = objClsLoginInfo.UserName;
                    _objData.CreatedOn = DateTime.Now;
                    db.PTR022.Add(_objData);
                }
                else
                {
                    _objData.EditedBy = objClsLoginInfo.UserName;
                    _objData.EditedOn = DateTime.Now;
                }

                db.SaveChanges();

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data saved successfully";
                objResponseMsg.HeaderId = _objData.HeaderId;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveFlatteningLineTable1(List<PTR022_Log> _lst, int HeaderId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                List<PTR022_Log> _lstAdd = new List<PTR022_Log>();
                List<PTR022_Log> _lstDelete = new List<PTR022_Log>();
                if (_lst != null)
                {
                    foreach (var item in _lst)
                    {
                        if (item.LineId > 0)
                        {
                            PTR022_Log _objUpdate = db.PTR022_Log.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (_objUpdate != null)
                            {
                                _objUpdate.HeaderId = item.HeaderId;
                                _objUpdate.SampleId = item.SampleId;
                                _objUpdate.FirstStepObservation = item.FirstStepObservation;
                                _objUpdate.SecondStepObservation = item.SecondStepObservation;
                                _objUpdate.Result = item.Result;

                                _objUpdate.EditedBy = objClsLoginInfo.UserName;
                                _objUpdate.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PTR022_Log _objAdd = new PTR022_Log();
                            _objAdd.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.SampleId))
                            {
                                _objAdd.HeaderId = item.HeaderId;
                                _objAdd.SampleId = item.SampleId;
                                _objAdd.FirstStepObservation = item.FirstStepObservation;
                                _objAdd.SecondStepObservation = item.SecondStepObservation;
                                _objAdd.Result = item.Result;

                                _objAdd.CreatedBy = objClsLoginInfo.UserName;
                                _objAdd.CreatedOn = DateTime.Now;
                                _lstAdd.Add(_objAdd);
                            }
                        }
                    }
                    if (_lstAdd.Count > 0)
                        db.PTR022_Log.AddRange(_lstAdd);

                    var LineIds = _lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = _lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    _lstDelete = db.PTR022_Log.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (_lstDelete.Count > 0)
                        db.PTR022_Log.RemoveRange(_lstDelete);
                    db.SaveChanges();
                }
                else
                {
                    _lstDelete = db.PTR022_Log.Where(x => x.HeaderId == HeaderId).ToList();
                    if (_lstDelete.Count > 0)
                        db.PTR022_Log.RemoveRange(_lstDelete);
                    db.SaveChanges();
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data saved successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeleteFlatteningHeaderData(int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {

                var header = db.PTR022.Where(t => t.HeaderId == headerId).FirstOrDefault();
                if (header != null)
                {
                    var logs = db.PTR022_Log.Where(t => t.HeaderId == headerId).ToList();
                    db.PTR022_Log.RemoveRange(logs);
                    db.PTR022.Remove(header);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Data deleted successfully";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Record not found.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Flaring
        public ActionResult Flaring(int? PTRHeaderId, string reportNo)
        {
            #region ViewBag
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstReferenceStandards = new List<string> { "ASME SEC IIA SA999", "ASME SEC IIA SA1016" };
            ViewBag.lstReferenceStandards = lstReferenceStandards.Select(x => new ddlValue() { id = x.ToString(), text = x.ToString() }).ToList();
            #endregion

            List<clsPTR023> _objPTR023 = new List<clsPTR023>();
            _objPTR023 = (from x in db.PTR023
                          where x.PTRHeaderId == PTRHeaderId
                          select new clsPTR023
                          {
                              HeaderId = x.HeaderId,
                              ReportNo = x.ReportNo,
                              RefStd = x.RefStd,
                              SOPNo = x.SOPNo,
                              Length = x.Length,
                              ActualInsideDiameter = x.ActualInsideDiameter,
                              RatioOfActualIDToSpecifiedOD = x.RatioOfActualIDToSpecifiedOD,
                              MinReqExpansionOfTubeID = x.MinReqExpansionOfTubeID,
                              TestRemarks = x.TestRemarks,
                              TempValue = x.TempValue,
                              HumidityValue = x.HumidityValue,
                              Remarks1 = x.Remarks1,
                              Remarks2 = x.Remarks2,
                              CreatedBy = x.CreatedBy,
                              CreatedOn = x.CreatedOn,
                              EditedBy = x.EditedBy,
                              EditedOn = x.EditedOn,
                              PTRHeaderId = x.PTRHeaderId,
                              PTR023_Log = db.PTR023_Log.Where(w => w.HeaderId == x.HeaderId).ToList()
                          }).ToList();
            var _SOPNo = "SOP/MT/002";
            var _ReportNo = string.Empty;
            if (_objPTR023.Count > 0)
            {
                _SOPNo = _objPTR023.FirstOrDefault().SOPNo;
            }
            if (db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).Count() > 0)
                _ReportNo = db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).FirstOrDefault().ReportNo;
            /*if (PTRHeaderId.HasValue && db.PTR023.Where(w => w.PTRHeaderId == PTRHeaderId).Count() > 0)
            {
                _objPTR023 = db.PTR023.Where(w => w.PTRHeaderId == PTRHeaderId).FirstOrDefault();
                ViewBag._lstPTR023_Log = db.PTR023_Log.Where(w => w.HeaderId == _objPTR023.HeaderId).ToList();

                if (db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).Count() > 0)
                    _objPTR023.ReportNo = db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).FirstOrDefault().ReportNo;
            }*/
            if (reportNo != null && reportNo != "")
            {

                _objPTR023.Add(new clsPTR023
                {
                    ReportNo = _ReportNo,
                    PTRHeaderId = PTRHeaderId,
                    SOPNo = _SOPNo,
                    RefStd = "ASME SEC IIA SA999",
                    TestRemarks = "Tube is flared by 60° included angle tool & mouth of tube is expanded up to minimum % of inside diameter specified in Table-5 of SA 1016."
                });


            }
            else if (_objPTR023.Count() == 0)
            {
                _objPTR023.Add(new clsPTR023
                {
                    ReportNo = _ReportNo,
                    PTRHeaderId = PTRHeaderId,
                    SOPNo = "SOP/MT/002",
                    RefStd = "ASME SEC IIA SA999",
                    TestRemarks = "Tube is flared by 60° included angle tool & mouth of tube is expanded up to minimum % of inside diameter specified in Table-5 of SA 1016."
                });

            }

            ViewBag.isEditable = IsPTRReportEditable(PTRHeaderId);
            return PartialView("Flaring", _objPTR023);
        }

        public ActionResult SaveFlaringHeaderData(PTR023 _obj)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                bool isAdd = false;
                PTR023 _objData = new PTR023();
                if (_obj.HeaderId == 0) //Add
                    isAdd = true;
                else
                    _objData = db.PTR023.Where(c => c.HeaderId == _obj.HeaderId).FirstOrDefault();

                _objData.ReportNo = _obj.ReportNo;
                _objData.RefStd = _obj.RefStd;
                _objData.SOPNo = _obj.SOPNo;
                _objData.Length = _obj.Length;
                _objData.Length = _obj.Length;
                _objData.ActualInsideDiameter = _obj.ActualInsideDiameter;
                _objData.RatioOfActualIDToSpecifiedOD = _obj.RatioOfActualIDToSpecifiedOD;
                _objData.MinReqExpansionOfTubeID = _obj.MinReqExpansionOfTubeID;
                _objData.TestRemarks = _obj.TestRemarks;

                _objData.TempValue = _obj.TempValue;
                _objData.HumidityValue = _obj.HumidityValue;
                _objData.Remarks1 = _obj.Remarks1;
                _objData.Remarks2 = _obj.Remarks2;
                _objData.PTRHeaderId = _obj.PTRHeaderId;

                if (isAdd)
                {
                    _objData.CreatedBy = objClsLoginInfo.UserName;
                    _objData.CreatedOn = DateTime.Now;
                    db.PTR023.Add(_objData);
                }
                else
                {
                    _objData.EditedBy = objClsLoginInfo.UserName;
                    _objData.EditedOn = DateTime.Now;
                }

                db.SaveChanges();

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data saved successfully";
                objResponseMsg.HeaderId = _objData.HeaderId;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveFlaringLineTable1(List<PTR023_Log> _lst, int HeaderId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                List<PTR023_Log> _lstAdd = new List<PTR023_Log>();
                List<PTR023_Log> _lstDelete = new List<PTR023_Log>();
                if (_lst != null)
                {
                    foreach (var item in _lst)
                    {
                        if (item.LineId > 0)
                        {
                            PTR023_Log _objUpdate = db.PTR023_Log.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (_objUpdate != null)
                            {
                                _objUpdate.HeaderId = item.HeaderId;
                                _objUpdate.SampleId = item.SampleId;
                                _objUpdate.TestObservation = item.TestObservation;
                                _objUpdate.Result = item.Result;

                                _objUpdate.EditedBy = objClsLoginInfo.UserName;
                                _objUpdate.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PTR023_Log _objAdd = new PTR023_Log();
                            _objAdd.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.SampleId))
                            {
                                _objAdd.HeaderId = item.HeaderId;
                                _objAdd.SampleId = item.SampleId;
                                _objAdd.TestObservation = item.TestObservation;
                                _objAdd.Result = item.Result;

                                _objAdd.CreatedBy = objClsLoginInfo.UserName;
                                _objAdd.CreatedOn = DateTime.Now;
                                _lstAdd.Add(_objAdd);
                            }
                        }
                    }
                    if (_lstAdd.Count > 0)
                        db.PTR023_Log.AddRange(_lstAdd);

                    var LineIds = _lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = _lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    _lstDelete = db.PTR023_Log.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (_lstDelete.Count > 0)
                        db.PTR023_Log.RemoveRange(_lstDelete);
                    db.SaveChanges();
                }
                else
                {
                    _lstDelete = db.PTR023_Log.Where(x => x.HeaderId == HeaderId).ToList();
                    if (_lstDelete.Count > 0)
                        db.PTR023_Log.RemoveRange(_lstDelete);
                    db.SaveChanges();
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data saved successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeleteFlaringHeaderData(int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {

                var header = db.PTR023.Where(t => t.HeaderId == headerId).FirstOrDefault();
                if (header != null)
                {
                    var logs = db.PTR023_Log.Where(t => t.HeaderId == headerId).ToList();
                    db.PTR023_Log.RemoveRange(logs);
                    db.PTR023.Remove(header);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Data deleted successfully";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Record not found.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region PULL-OUT
        public ActionResult PullOut(int? PTRHeaderId)
        {
            #region ViewBag
            ViewBag.ControllerURL = ControllerURL;
            #endregion

            PTR024 _objPTR024 = new PTR024();
            if (PTRHeaderId.HasValue && db.PTR024.Where(w => w.PTRHeaderId == PTRHeaderId).Count() > 0)
            {
                _objPTR024 = db.PTR024.Where(w => w.PTRHeaderId == PTRHeaderId).FirstOrDefault();
                ViewBag._lstPTR024_Log = db.PTR024_Log.Where(w => w.HeaderId == _objPTR024.HeaderId).ToList();

                if (db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).Count() > 0)
                    _objPTR024.ReportNo = db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).FirstOrDefault().ReportNo;
            }
            else
            {
                _objPTR024.SOPNo = "1) SOP/MT/002 2) HE-HZMC-MET-P-069";
                ViewBag._lstPTR024_Log = null;

                if (db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).Count() > 0)
                    _objPTR024.ReportNo = db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).FirstOrDefault().ReportNo;
            }

            ViewBag.isEditable = IsPTRReportEditable(PTRHeaderId);
            return PartialView("PullOut", _objPTR024);
        }

        public ActionResult SavePullOutHeaderData(PTR024 _obj)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                bool isAdd = false;
                PTR024 _objData = new PTR024();
                if (_obj.HeaderId == 0) //Add
                    isAdd = true;
                else
                    _objData = db.PTR024.Where(c => c.HeaderId == _obj.HeaderId).FirstOrDefault();

                _objData.ReportNo = _obj.ReportNo;
                _objData.RefStd = _obj.RefStd;
                _objData.SOPNo = _obj.SOPNo;
                _objData.SampleId = _obj.SampleId;
                _objData.RequiredLoad = _obj.RequiredLoad;
                _objData.CrossSectionArea = _obj.CrossSectionArea;

                _objData.TempValue = _obj.TempValue;
                _objData.HumidityValue = _obj.HumidityValue;
                _objData.Remarks1 = _obj.Remarks1;
                _objData.Remarks2 = _obj.Remarks2;
                _objData.PTRHeaderId = _obj.PTRHeaderId;

                if (isAdd)
                {
                    _objData.CreatedBy = objClsLoginInfo.UserName;
                    _objData.CreatedOn = DateTime.Now;
                    db.PTR024.Add(_objData);
                }
                else
                {
                    _objData.EditedBy = objClsLoginInfo.UserName;
                    _objData.EditedOn = DateTime.Now;
                }

                db.SaveChanges();

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data saved successfully";
                objResponseMsg.HeaderId = _objData.HeaderId;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SavePullOutLineTable1(List<PTR024_Log> _lst, int HeaderId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                List<PTR024_Log> _lstAdd = new List<PTR024_Log>();
                List<PTR024_Log> _lstDelete = new List<PTR024_Log>();
                if (_lst != null)
                {
                    foreach (var item in _lst)
                    {
                        if (item.LineId > 0)
                        {
                            PTR024_Log _objUpdate = db.PTR024_Log.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (_objUpdate != null)
                            {
                                _objUpdate.HeaderId = item.HeaderId;
                                _objUpdate.SrNo = item.SrNo;
                                _objUpdate.HoleTubeNo = item.HoleTubeNo;
                                _objUpdate.TestType = item.TestType;
                                _objUpdate.FinalLoadInN = item.FinalLoadInN;
                                _objUpdate.Strength = item.Strength;
                                _objUpdate.FractureLocation = item.FractureLocation;
                                _objUpdate.Remark = item.Remark;

                                _objUpdate.EditedBy = objClsLoginInfo.UserName;
                                _objUpdate.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PTR024_Log _objAdd = new PTR024_Log();
                            _objAdd.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.SrNo))
                            {
                                _objAdd.HeaderId = item.HeaderId;
                                _objAdd.SrNo = item.SrNo;
                                _objAdd.HoleTubeNo = item.HoleTubeNo;
                                _objAdd.TestType = item.TestType;
                                _objAdd.FinalLoadInN = item.FinalLoadInN;
                                _objAdd.Strength = item.Strength;
                                _objAdd.FractureLocation = item.FractureLocation;
                                _objAdd.Remark = item.Remark;

                                _objAdd.CreatedBy = objClsLoginInfo.UserName;
                                _objAdd.CreatedOn = DateTime.Now;
                                _lstAdd.Add(_objAdd);
                            }
                        }
                    }
                    if (_lstAdd.Count > 0)
                        db.PTR024_Log.AddRange(_lstAdd);

                    var LineIds = _lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = _lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    _lstDelete = db.PTR024_Log.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (_lstDelete.Count > 0)
                        db.PTR024_Log.RemoveRange(_lstDelete);
                    db.SaveChanges();
                }
                else
                {
                    _lstDelete = db.PTR024_Log.Where(x => x.HeaderId == HeaderId).ToList();
                    if (_lstDelete.Count > 0)
                        db.PTR024_Log.RemoveRange(_lstDelete);
                    db.SaveChanges();
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data saved successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region IGC 1
        public ActionResult IGC1(int? PTRHeaderId, string reportNo)
        {
            #region ViewBag
            ViewBag.ControllerURL = ControllerURL;

            List<string> _lstResult = new List<string> { "Acceptable", "Not Acceptable", "To report" };
            ViewBag._lstResult = _lstResult.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            #endregion

            List<PTR025> _objPTR025 = new List<PTR025>();
            _objPTR025 = db.PTR025.Where(w => w.PTRHeaderId == PTRHeaderId).ToList();

            /*if (PTRHeaderId.HasValue && db.PTR025.Where(w => w.PTRHeaderId == PTRHeaderId).Count() > 0)
                _objPTR025 = db.PTR025.Where(w => w.PTRHeaderId == PTRHeaderId).FirstOrDefault();
            */
            var _SOPNo = "SOP/MT/007";
            var _ReportNo = string.Empty;
            if (_objPTR025.Count > 0)
            {
                _SOPNo = _objPTR025.FirstOrDefault().SOPNo;
            }
            if (db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).Count() > 0)
                _ReportNo = db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).FirstOrDefault().ReportNo;

            if (reportNo != null && reportNo != "")
            {
                _objPTR025.Add(new PTR025
                {
                    PTRHeaderId = PTRHeaderId,
                    ReportNo = _ReportNo,
                    SOPNo = _SOPNo,
                    Result = "Acceptable"
                });
            }
            else if (_objPTR025.Count() == 0)
            {

                _objPTR025.Add(new PTR025
                {
                    ReportNo = _ReportNo,
                    PTRHeaderId = PTRHeaderId,
                    SOPNo = "SOP/MT/007",
                    Result = "Acceptable"
                });
            }

            #region Image path
            List<string> _lstPath1 = new List<string>();

            for (int i = 0; i < _objPTR025.Count; i++)
            {
                long lHeaderId = 0;
                long.TryParse(_objPTR025[i].HeaderId.ToString(), out lHeaderId);
                FCS001 _objFCS = db.FCS001.Where(w => w.TableId == lHeaderId && w.TableName == "PTR025" && w.ViewerId == 1).FirstOrDefault();
                if (_objFCS != null)
                    _lstPath1.Add(_objFCS.MainDocumentPath);
                else
                    _lstPath1.Add("");
            }

            List<string> _lstPath2 = new List<string>();
            for (int i = 0; i < _objPTR025.Count; i++)
            {
                long lHeaderId = 0;
                long.TryParse(_objPTR025[i].HeaderId.ToString(), out lHeaderId);
                FCS001 _objFCS = db.FCS001.Where(w => w.TableId == lHeaderId && w.TableName == "PTR025" && w.ViewerId == 2).FirstOrDefault();
                if (_objFCS != null)
                    _lstPath2.Add(_objFCS.MainDocumentPath);
                else
                    _lstPath2.Add("");
            }
            #endregion

            ViewBag._lstImagePaths1 = _lstPath1;
            ViewBag._lstImagePaths2 = _lstPath2;

            ViewBag.isEditable = IsPTRReportEditable(PTRHeaderId);
            return PartialView("IGC1", _objPTR025);
        }

        public ActionResult SaveIGC1HeaderData(PTR025 _obj)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                bool isAdd = false;
                PTR025 _objData = new PTR025();
                if (_obj.HeaderId == 0) //Add
                    isAdd = true;
                else
                    _objData = db.PTR025.Where(c => c.HeaderId == _obj.HeaderId).FirstOrDefault();

                _objData.ReportNo = _obj.ReportNo;
                _objData.RefStd = _obj.RefStd;
                _objData.SOPNo = _obj.SOPNo;
                _objData.SampleId = _obj.SampleId;
                _objData.Length = _obj.Length;
                _objData.Width = _obj.Width;
                _objData.Thickness = _obj.Thickness;
                _objData.RemarksDimensions = _obj.RemarksDimensions;
                _objData.IGCPractice = _obj.IGCPractice;
                _objData.LocationOrientation = _obj.LocationOrientation;
                _objData.Sensitization = _obj.Sensitization;
                _objData.dtBoilingStart = _obj.dtBoilingStart;
                _objData.dtBoilingEnd = _obj.dtBoilingEnd;
                _objData.TotalDurationOfBoilingours = _obj.TotalDurationOfBoilingours;
                _objData.BendTestMandrelDia = _obj.BendTestMandrelDia;
                _objData.AngelOfBend = _obj.AngelOfBend;
                _objData.Magnification = _obj.Magnification;
                _objData.Evaluation = _obj.Evaluation;
                _objData.RemarksDimensions = _obj.RemarksDimensions;
                _objData.PhotoHeading1 = _obj.PhotoHeading1;
                _objData.PhotoHeading2 = _obj.PhotoHeading2;
                _objData.Result = _obj.Result;

                _objData.TempValue = _obj.TempValue;
                _objData.HumidityValue = _obj.HumidityValue;
                _objData.Remarks1 = _obj.Remarks1;
                _objData.Remarks2 = _obj.Remarks2;
                _objData.PTRHeaderId = _obj.PTRHeaderId;

                if (isAdd)
                {
                    _objData.CreatedBy = objClsLoginInfo.UserName;
                    _objData.CreatedOn = DateTime.Now;
                    db.PTR025.Add(_objData);
                }
                else
                {
                    _objData.EditedBy = objClsLoginInfo.UserName;
                    _objData.EditedOn = DateTime.Now;
                }

                db.SaveChanges();

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data saved successfully";
                objResponseMsg.HeaderId = _objData.HeaderId;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeleteIGCHeaderData(int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {

                var header = db.PTR025.Where(t => t.HeaderId == headerId).FirstOrDefault();
                if (header != null)
                {
                    db.PTR025.Remove(header);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Data deleted successfully";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Record not found.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region MACRO General
        public ActionResult MacroGeneral(int? PTRHeaderId)
        {
            #region ViewBag
            ViewBag.ControllerURL = ControllerURL;

            List<string> _lstResult = new List<string> { "Acceptable", "Not Acceptable", "To report" };
            ViewBag._lstResult = _lstResult.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            #endregion

            PTR026 _objPTR026 = new PTR026();
            if (PTRHeaderId.HasValue && db.PTR026.Where(w => w.PTRHeaderId == PTRHeaderId).Count() > 0)
            {
                _objPTR026 = db.PTR026.Where(w => w.PTRHeaderId == PTRHeaderId).FirstOrDefault();
                ViewBag._lstPTR026_Log = db.PTR026_Log.Where(w => w.HeaderId == _objPTR026.HeaderId).ToList();

                //if (db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).Count() > 0)
                //    _objPTR026.ReportNo = db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).FirstOrDefault().ReportNo;
                FCS001 _objFUC = db.FCS001.Where(w => w.TableId == _objPTR026.PTRHeaderId && w.TableName == "PTR026" && w.ViewerId == 1).FirstOrDefault();
                if (_objFUC != null)
                    ViewBag._lstImagePaths = _objFUC.MainDocumentPath;
                else
                    ViewBag._lstImagePaths = "";
            }
            else
            {
                _objPTR026.SopNo = "SOP/MT/006A";
                _objPTR026.WpqResult = "Acceptable";

                PTR001 _lstPTR001 = db.PTR001.Where(x => x.HeaderId == PTRHeaderId).FirstOrDefault();

                _objPTR026.ProjectNo = _lstPTR001.Project;
                _objPTR026.PTRNo = _lstPTR001.PTRNo;
                _objPTR026.TestCoupon = "Welder Performance Qualification";
                _objPTR026.MaterialSpecification = _lstPTR001.MaterialSpec;
                _objPTR026.Thickness = "";
                _objPTR026.PqrNO = _lstPTR001.PQRnO;
                _objPTR026.Process = _lstPTR001.WeldingProcess;
                _objPTR026.InspAgency = _lstPTR001.InspectionAgency1 + "," + _lstPTR001.InspectionAgency2 + "," + _lstPTR001.InspectionAgency3;
                _objPTR026.SampleId = _lstPTR001.LabId;
                _objPTR026.FillerWire = _lstPTR001.FillerWire;
                _objPTR026.Position = _lstPTR001.Position;
                _objPTR026.CodeSpec = _lstPTR001.ApplicableCodeSpec;
                _objPTR026.WqtNo = _lstPTR001.WQTno;
                _objPTR026.WPSNo = _lstPTR001.WPSNo;
                _objPTR026.WelderNo = _lstPTR001.WelderID;
                _objPTR026.PqrNO = _lstPTR001.PQRnO;
                _objPTR026.ReqReceipt = _lstPTR001.LabIdCreatedOn == null ? "" : _lstPTR001.LabIdCreatedOn.Value.ToShortDateString();
                ViewBag._lstImagePaths = "";

                if (db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).Count() > 0)
                    _objPTR026.ReportNo = db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).FirstOrDefault().ReportNo;
            }

            ViewBag.isEditable = IsPTRReportEditable(PTRHeaderId);

            return PartialView("MacroGeneral", _objPTR026);
        }
        public ActionResult SaveMacroGeneralHeaderData(PTR026 _obj)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                bool isAdd = false;
                PTR026 _objData = new PTR026();
                if (_obj.HeaderId == 0) //Add
                    isAdd = true;
                else
                    _objData = db.PTR026.Where(c => c.HeaderId == _obj.HeaderId).FirstOrDefault();

                _objData.ReportNo = _obj.ReportNo;
                _objData.SopNo = _obj.SopNo;
                _objData.ProjectNo = _obj.ProjectNo;
                _objData.ReqReceipt = _obj.ReqReceipt;
                _objData.PTRNo = _obj.PTRNo;
                _objData.TestCoupon = _obj.TestCoupon;
                _objData.WPSNo = _obj.WPSNo;
                _objData.MaterialSpecification = _obj.MaterialSpecification;
                _objData.WelderNo = _obj.WelderNo;
                _objData.Thickness = _obj.Thickness;
                _objData.Process = _obj.Process;
                _objData.InspAgency = _obj.InspAgency;
                _objData.Position = _obj.Position;
                _objData.SampleId = _obj.SampleId;
                _objData.FillerWire = _obj.FillerWire;
                _objData.CodeSpec = _obj.CodeSpec;
                _objData.WqtNo = _obj.WqtNo;
                _objData.JointType = _obj.JointType;
                _objData.PqrNO = _obj.PqrNO;
                _objData.Magnification = _obj.Magnification;
                _objData.EtchingReagent = _obj.EtchingReagent;

                _objData.TypeOfExamination = _obj.TypeOfExamination;
                _objData.NosOfSample = _obj.NosOfSample;
                _objData.WpqResult = _obj.WpqResult;
                _objData.TestingDate = _obj.TestingDate;

                _objData.TempValue = _obj.TempValue;
                _objData.HumidityValue = _obj.HumidityValue;
                _objData.Remarks1 = _obj.Remarks1;
                _objData.Remarks2 = _obj.Remarks2;
                _objData.PTRHeaderId = _obj.PTRHeaderId;

                if (isAdd)
                {
                    _objData.CreatedBy = objClsLoginInfo.UserName;
                    _objData.CreatedOn = DateTime.Now;
                    db.PTR026.Add(_objData);
                }
                else
                {
                    _objData.EditedBy = objClsLoginInfo.UserName;
                    _objData.EditedOn = DateTime.Now;
                }

                db.SaveChanges();

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data saved successfully";
                objResponseMsg.HeaderId = _objData.HeaderId;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SaveMacroGeneralLineTable1(List<PTR026_Log> _lst, int HeaderId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                List<PTR026_Log> _lstAdd = new List<PTR026_Log>();
                List<PTR026_Log> _lstDelete = new List<PTR026_Log>();
                if (_lst != null)
                {
                    foreach (var item in _lst)
                    {
                        if (item.LineId > 0)
                        {
                            PTR026_Log _objUpdate = db.PTR026_Log.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (_objUpdate != null)
                            {
                                _objUpdate.HeaderId = item.HeaderId;
                                _objUpdate.ActualValue = item.ActualValue;

                                _objUpdate.EditedBy = objClsLoginInfo.UserName;
                                _objUpdate.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PTR026_Log _objAdd = new PTR026_Log();
                            _objAdd.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.ActualValue))
                            {
                                _objAdd.HeaderId = item.HeaderId;
                                _objAdd.ActualValue = item.ActualValue;

                                _objAdd.CreatedBy = objClsLoginInfo.UserName;
                                _objAdd.CreatedOn = DateTime.Now;
                                _lstAdd.Add(_objAdd);
                            }
                        }
                    }
                    if (_lstAdd.Count > 0)
                        db.PTR026_Log.AddRange(_lstAdd);

                    var LineIds = _lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = _lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    _lstDelete = db.PTR026_Log.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (_lstDelete.Count > 0)
                        db.PTR026_Log.RemoveRange(_lstDelete);
                    db.SaveChanges();
                }
                else
                {
                    _lstDelete = db.PTR026_Log.Where(x => x.HeaderId == HeaderId).ToList();
                    if (_lstDelete.Count > 0)
                        db.PTR026_Log.RemoveRange(_lstDelete);
                    db.SaveChanges();
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data saved successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region MACRO N FRACTURE
        public ActionResult MacroNFracture(int? PTRHeaderId)
        {
            #region ViewBag
            ViewBag.ControllerURL = ControllerURL;

            List<string> _lstResult = new List<string> { "Acceptable", "Not Acceptable", "To report" };
            ViewBag._lstResult = _lstResult.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            List<string> _lstYesNo = new List<string> { "Yes", "No" };
            ViewBag._lstYesNo = _lstYesNo.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            List<string> _lstResultMacroExamination = new List<string> { "SATISFACTORY", "NOT SATISFACTORY", "NOT APPLICABLE" };
            ViewBag._lstResultMacroExamination = _lstResultMacroExamination.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            #endregion

            PTR027 _objPTR027 = new PTR027();
            if (PTRHeaderId.HasValue && db.PTR027.Where(w => w.PTRHeaderId == PTRHeaderId).Count() > 0)
            {
                _objPTR027 = db.PTR027.Where(w => w.PTRHeaderId == PTRHeaderId).FirstOrDefault();

                if (db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).Count() > 0)
                    _objPTR027.ReportNo = db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).FirstOrDefault().ReportNo;
            }
            else
            {
                _objPTR027.SopNo = "HE-HZMC-MET-P-067";
                _objPTR027.TestCoupon = "Welder Performance Qualification";
                _objPTR027.WPQResult = "Acceptable";
                _objPTR027.MEResult1 = "SATISFACTORY";
                _objPTR027.MEResult2 = "SATISFACTORY";
                _objPTR027.MEResult3 = "SATISFACTORY";
                _objPTR027.MEResult4 = "SATISFACTORY";
                _objPTR027.MEResult5 = "SATISFACTORY";

                _objPTR027.FTDetails1 = "Yes";
                _objPTR027.FTDetails2 = "SATISFACTORY";
                _objPTR027.FTDetails3 = "SATISFACTORY";
                _objPTR027.FTDetails4 = "SATISFACTORY";

                PTR001 _lstPTR001 = db.PTR001.Where(x => x.HeaderId == PTRHeaderId).FirstOrDefault();

                _objPTR027.ProjectNo = _lstPTR001.Project;
                _objPTR027.PTRNo = _lstPTR001.PTRNo;
                //_objPTR027.TestCoupon = _lstPTR001.CouponCategory;
                _objPTR027.MaterialSpecification = _lstPTR001.MaterialSpec;
                _objPTR027.Thickness = "";
                _objPTR027.PqrNO = _lstPTR001.PQRnO;
                _objPTR027.Process = _lstPTR001.WeldingProcess;
                _objPTR027.InspAgency = _lstPTR001.InspectionAgency1 + "," + _lstPTR001.InspectionAgency2 + "," + _lstPTR001.InspectionAgency3;
                _objPTR027.SampleId = _lstPTR001.LabId;
                _objPTR027.FillerWire = _lstPTR001.FillerWire;
                _objPTR027.Position = _lstPTR001.Position;
                _objPTR027.CodeSpec = _lstPTR001.ApplicableCodeSpec;
                _objPTR027.WqtNo = _lstPTR001.WQTno;
                _objPTR027.WPSNo = _lstPTR001.WPSNo;
                _objPTR027.WelderNo = _lstPTR001.WelderID;
                _objPTR027.ReqReceipt = _lstPTR001.LabIdCreatedOn == null ? "" : _lstPTR001.LabIdCreatedOn.Value.ToShortDateString();

                if (db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).Count() > 0)
                    _objPTR027.ReportNo = db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).FirstOrDefault().ReportNo;
            }

            ViewBag.isEditable = IsPTRReportEditable(PTRHeaderId);
            return PartialView("MacroNFracture", _objPTR027);
        }
        public ActionResult SaveMacroNFractureHeaderData(PTR027 _obj)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                bool isAdd = false;
                PTR027 _objData = new PTR027();
                if (_obj.HeaderId == 0) //Add
                    isAdd = true;
                else
                    _objData = db.PTR027.Where(c => c.HeaderId == _obj.HeaderId).FirstOrDefault();

                _objData.ReportNo = _obj.ReportNo;
                _objData.SopNo = _obj.SopNo;
                _objData.ProjectNo = _obj.ProjectNo;
                _objData.ReqReceipt = _obj.ReqReceipt;
                _objData.PTRNo = _obj.PTRNo;
                _objData.TestCoupon = _obj.TestCoupon;
                _objData.WPSNo = _obj.WPSNo;
                _objData.MaterialSpecification = _obj.MaterialSpecification;
                _objData.WelderNo = _obj.WelderNo;
                _objData.Thickness = _obj.Thickness;
                _objData.Process = _obj.Process;
                _objData.InspAgency = _obj.InspAgency;
                _objData.Position = _obj.Position;
                _objData.SampleId = _obj.SampleId;
                _objData.FillerWire = _obj.FillerWire;
                _objData.CodeSpec = _obj.CodeSpec;
                _objData.WqtNo = _obj.WqtNo;
                _objData.JointType = _obj.JointType;
                _objData.PqrNO = _obj.PqrNO;
                _objData.Magnification = _obj.Magnification;
                _objData.EtchingReagent = _obj.EtchingReagent;

                _objData.TypeOfExamination = _obj.TypeOfExamination;
                _objData.NosOfSample = _obj.NosOfSample;
                _objData.WPQResult = _obj.WPQResult;
                _objData.TestingDate = _obj.TestingDate;

                _objData.MERemark1 = _obj.MERemark1;
                _objData.MERemark2 = _obj.MERemark2;
                _objData.MERemark3 = _obj.MERemark3;
                _objData.MERemark4 = _obj.MERemark4;
                _objData.MERemark5 = _obj.MERemark5;

                _objData.MEResult1 = _obj.MEResult1;
                _objData.MEResult2 = _obj.MEResult2;
                _objData.MEResult3 = _obj.MEResult3;
                _objData.MEResult4 = _obj.MEResult4;
                _objData.MEResult5 = _obj.MEResult5;

                _objData.FTDetails1 = _obj.FTDetails1;
                _objData.FTDetails2 = _obj.FTDetails2;
                _objData.FTDetails3 = _obj.FTDetails3;
                _objData.FTDetails4 = _obj.FTDetails4;

                _objData.MacroExamination = _obj.MacroExamination;
                _objData.FractureTest = _obj.FractureTest;

                _objData.TempValue = _obj.TempValue;
                _objData.HumidityValue = _obj.HumidityValue;
                _objData.Remarks1 = _obj.Remarks1;
                _objData.Remarks2 = _obj.Remarks2;
                _objData.PTRHeaderId = _obj.PTRHeaderId;

                if (isAdd)
                {
                    _objData.CreatedBy = objClsLoginInfo.UserName;
                    _objData.CreatedOn = DateTime.Now;
                    db.PTR027.Add(_objData);
                }
                else
                {
                    _objData.EditedBy = objClsLoginInfo.UserName;
                    _objData.EditedOn = DateTime.Now;
                }

                db.SaveChanges();

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data saved successfully";
                objResponseMsg.HeaderId = _objData.HeaderId;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region BEND WQTC
        public ActionResult BendWQTC(int? PTRHeaderId)
        {
            #region ViewBag
            ViewBag.ControllerURL = ControllerURL;

            List<string> _lstResult = new List<string> { "Acceptable", "Not Acceptable", "To report" };
            ViewBag._lstResult = _lstResult.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #endregion

            PTR028 _objPTR028 = new PTR028();
            if (PTRHeaderId.HasValue && db.PTR028.Where(w => w.PTRHeaderId == PTRHeaderId).Count() > 0)
            {
                _objPTR028 = db.PTR028.Where(w => w.PTRHeaderId == PTRHeaderId).FirstOrDefault();
                ViewBag._lstPTR028_Log = db.PTR028_Log.Where(w => w.HeaderId == _objPTR028.HeaderId).ToList();

                /* commented by parita on 28-7-2020 because of make reportNo editable 
                if (db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).Count() > 0)
                    _objPTR028.ReportNo = db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).FirstOrDefault().ReportNo;
                */
            }
            else
            {
                _objPTR028.SopNo = "SOP/MT/003";
                _objPTR028.TestCoupon = "Welder Performance Qualification";

                PTR001 _lstPTR001 = db.PTR001.Where(x => x.HeaderId == PTRHeaderId).FirstOrDefault();

                _objPTR028.ProjectNo = _lstPTR001.Project;
                _objPTR028.PTRNo = _lstPTR001.PTRNo;
                //_objPTR028.TestCoupon = _lstPTR001.CouponCategory;
                _objPTR028.MaterialSpec = _lstPTR001.MaterialSpec;
                _objPTR028.Thickness = "";
                _objPTR028.PqrNo = _lstPTR001.PQRnO;
                _objPTR028.Process = _lstPTR001.WeldingProcess;
                _objPTR028.InspectionAgency = _lstPTR001.InspectionAgency1 + "," + _lstPTR001.InspectionAgency2 + "," + _lstPTR001.InspectionAgency3;
                _objPTR028.SampleId = _lstPTR001.LabId;
                _objPTR028.Fillerwire = _lstPTR001.FillerWire;
                _objPTR028.Position = _lstPTR001.Position;
                _objPTR028.CodeSpec = _lstPTR001.ApplicableCodeSpec;
                _objPTR028.WqtNo = _lstPTR001.WQTno;
                _objPTR028.WpsNo = _lstPTR001.WPSNo;
                _objPTR028.WelderNo = _lstPTR001.WelderID;
                _objPTR028.ReqReceiptDate = _lstPTR001.LabIdCreatedOn;
                _objPTR028.Note1 = "Abbreviations used :- SB = Side Bend, TR = Transverse, T = Thickness of Coupon, t = Thickness of Sample, D: Diameter";
                _objPTR028.Note2 = "Machine Used(Calibrated) : 1) UTM 100T - Machine sr. No.: 10-619, 2) Vernier Caliper : CV-083";
                if (db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).Count() > 0)
                    _objPTR028.ReportNo = db.PTR011.Where(w => w.PTRHeaderId == PTRHeaderId).FirstOrDefault().ReportNo;
            }

            ViewBag.isEditable = IsPTRReportEditable(PTRHeaderId);
            return PartialView("BendWQTC", _objPTR028);
        }

        public ActionResult SaveBendWQTCHeaderData(PTR028 _obj)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                bool isAdd = false;
                PTR028 _objData = new PTR028();
                if (_obj.HeaderId == 0) //Add
                    isAdd = true;
                else
                    _objData = db.PTR028.Where(c => c.HeaderId == _obj.HeaderId).FirstOrDefault();

                _objData.ReportNo = _obj.ReportNo;
                _objData.SopNo = _obj.SopNo;
                _objData.ProjectNo = _obj.ProjectNo;
                _objData.ReqReceiptDate = _obj.ReqReceiptDate;
                _objData.TestingDate = _obj.TestingDate;
                _objData.PTRNo = _obj.PTRNo;
                _objData.TestCoupon = _obj.TestCoupon;
                _objData.WpsNo = _obj.WpsNo;
                _objData.MaterialSpec = _obj.MaterialSpec;
                _objData.WelderNo = _obj.WelderNo;
                _objData.Thickness = _obj.Thickness;
                _objData.Process = _obj.Process;
                _objData.InspectionAgency = _obj.InspectionAgency;
                _objData.Position = _obj.Position;
                _objData.SampleId = _obj.SampleId;
                _objData.Fillerwire = _obj.Fillerwire;
                _objData.CodeSpec = _obj.CodeSpec;
                _objData.WqtNo = _obj.WqtNo;
                _objData.TypeOfJoint = _obj.TypeOfJoint;
                _objData.PqrNo = _obj.PqrNo;

                _objData.TempValue = _obj.TempValue;
                _objData.HumidityValue = _obj.HumidityValue;
                _objData.Remarks1 = _obj.Remarks1;
                _objData.Remarks2 = _obj.Remarks2;
                _objData.PTRHeaderId = _obj.PTRHeaderId;
                _objData.CouponId = _obj.CouponId;
                _objData.Note1 = _obj.Note1;
                _objData.Note2 = _obj.Note2;
                if (isAdd)
                {
                    _objData.CreatedBy = objClsLoginInfo.UserName;
                    _objData.CreatedOn = DateTime.Now;
                    db.PTR028.Add(_objData);
                }
                else
                {
                    _objData.EditedBy = objClsLoginInfo.UserName;
                    _objData.EditedOn = DateTime.Now;
                }

                db.SaveChanges();

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data saved successfully";
                objResponseMsg.HeaderId = _objData.HeaderId;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveBendWQTCLineTable1(List<PTR028_Log> _lst, int HeaderId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                List<PTR028_Log> _lstAdd = new List<PTR028_Log>();
                List<PTR028_Log> _lstDelete = new List<PTR028_Log>();
                if (_lst != null)
                {
                    foreach (var item in _lst)
                    {
                        if (item.LineId > 0)
                        {
                            PTR028_Log _objUpdate = db.PTR028_Log.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (_objUpdate != null)
                            {
                                _objUpdate.HeaderId = item.HeaderId;
                                _objUpdate.SB1Identification = item.SB1Identification;
                                _objUpdate.SB2Identification = item.SB2Identification;
                                _objUpdate.SB3Identification = item.SB3Identification;
                                _objUpdate.SB4Identification = item.SB4Identification;
                                _objUpdate.SB1Thickness = item.SB1Thickness;
                                _objUpdate.SB2Thickness = item.SB2Thickness;
                                _objUpdate.SB3Thickness = item.SB3Thickness;
                                _objUpdate.SB4Thickness = item.SB4Thickness;
                                _objUpdate.SB1BendRollerDia = item.SB1BendRollerDia;
                                _objUpdate.SB2BendRollerDia = item.SB2BendRollerDia;
                                _objUpdate.SB3BendRollerDia = item.SB3BendRollerDia;
                                _objUpdate.SB4BendRollerDia = item.SB4BendRollerDia;
                                _objUpdate.SB1Direction = item.SB1Direction;
                                _objUpdate.SB2Direction = item.SB2Direction;
                                _objUpdate.SB3Direction = item.SB3Direction;
                                _objUpdate.SB4Direction = item.SB4Direction;
                                _objUpdate.MandrelDia = item.MandrelDia;
                                _objUpdate.BendAngel = item.BendAngel;
                                _objUpdate.Result = item.Result;
                                _objUpdate.SB2Result = item.SB2Result;
                                _objUpdate.SB3Result = item.SB3Result;
                                _objUpdate.SB4Result = item.SB4Result;
                                _objUpdate.Width1 = item.Width1;
                                _objUpdate.Width2 = item.Width2;
                                _objUpdate.Width3 = item.Width3;
                                _objUpdate.Width4 = item.Width4;
                                _objUpdate.Observation1 = item.Observation1;
                                _objUpdate.Observation2 = item.Observation2;
                                _objUpdate.Observation3 = item.Observation3;
                                _objUpdate.Observation4 = item.Observation4;
                                _objUpdate.CouponId = item.CouponId;

                                _objUpdate.EditedBy = objClsLoginInfo.UserName;
                                _objUpdate.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PTR028_Log _objAdd = new PTR028_Log();
                            _objAdd.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.SB1Identification))
                            {
                                _objAdd.HeaderId = item.HeaderId;
                                _objAdd.SB1Identification = item.SB1Identification;
                                _objAdd.SB2Identification = item.SB2Identification;
                                _objAdd.SB3Identification = item.SB3Identification;
                                _objAdd.SB4Identification = item.SB4Identification;
                                _objAdd.SB1Thickness = item.SB1Thickness;
                                _objAdd.SB2Thickness = item.SB2Thickness;
                                _objAdd.SB3Thickness = item.SB3Thickness;
                                _objAdd.SB4Thickness = item.SB4Thickness;
                                _objAdd.SB1BendRollerDia = item.SB1BendRollerDia;
                                _objAdd.SB2BendRollerDia = item.SB2BendRollerDia;
                                _objAdd.SB3BendRollerDia = item.SB3BendRollerDia;
                                _objAdd.SB4BendRollerDia = item.SB4BendRollerDia;
                                _objAdd.SB1Direction = item.SB1Direction;
                                _objAdd.SB2Direction = item.SB2Direction;
                                _objAdd.SB3Direction = item.SB3Direction;
                                _objAdd.SB4Direction = item.SB4Direction;
                                _objAdd.MandrelDia = item.MandrelDia;
                                _objAdd.BendAngel = item.BendAngel;
                                _objAdd.Result = item.Result;
                                _objAdd.SB2Result = item.SB2Result;
                                _objAdd.SB3Result = item.SB3Result;
                                _objAdd.SB4Result = item.SB4Result;
                                _objAdd.Width1 = item.Width1;
                                _objAdd.Width2 = item.Width2;
                                _objAdd.Width3 = item.Width3;
                                _objAdd.Width4 = item.Width4;
                                _objAdd.Observation1 = item.Observation1;
                                _objAdd.Observation2 = item.Observation2;
                                _objAdd.Observation3 = item.Observation3;
                                _objAdd.Observation4 = item.Observation4;
                                _objAdd.CouponId = item.CouponId;

                                _objAdd.CreatedBy = objClsLoginInfo.UserName;
                                _objAdd.CreatedOn = DateTime.Now;
                                _lstAdd.Add(_objAdd);
                            }
                        }
                    }
                    if (_lstAdd.Count > 0)
                        db.PTR028_Log.AddRange(_lstAdd);

                    var LineIds = _lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = _lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    _lstDelete = db.PTR028_Log.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (_lstDelete.Count > 0)
                        db.PTR028_Log.RemoveRange(_lstDelete);
                    db.SaveChanges();
                }
                else
                {
                    _lstDelete = db.PTR028_Log.Where(x => x.HeaderId == HeaderId).ToList();
                    if (_lstDelete.Count > 0)
                        db.PTR028_Log.RemoveRange(_lstDelete);
                    db.SaveChanges();
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data saved successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region CHEMICAL OVERLAY1
        public ActionResult ChemicalOverlay(int? PTRHeaderId)
        {
            #region ViewBag
            ViewBag.ControllerURL = ControllerURL;

            List<string> _lstResult = new List<string> { "Acceptable", "Not Acceptable", "To report" };
            ViewBag._lstResult = _lstResult.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            List<string> _lstTestmethod = new List<string> { "ASTM 415- Ed2017", "ASTM 1086- Ed2014", "ASTM E572 - Ed2013", "ASTM E322- Ed2012", "ASTM E1019-Ed2011", "IS 9878 & IS 8811", "ASTM E1476" };
            ViewBag._lstTestmethod = _lstTestmethod.Select(x => new ddlValue() { id = x.ToString(), text = x.ToString() }).ToList();

            List<string> _lstInstrumentUsed = new List<string> { "OES (Sr No.127198/09)", "Alloy Analyser (Sr No.62917)", "Leco CS (Sr No.4107)" };
            ViewBag._lstInstrumentUsed = _lstInstrumentUsed.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            List<string> _lstCRMUsed = new List<string> {
                "IARM 4C", "IARM 6C", "IARM 36B", "B.S.87F", "IARM 35H",
                "502-348","502-809","501-506","501-024","501-503",
                "502-364","BCS/SS 407/2","BCS/SS 405/2","BS CA5A","1173",
                "1763a","Euronorm 379-1","BS 410B","BCS/SS 473","Euronorm 035-2",
                "IARM 35JN","B.S. SS 3951","SRM 1269","Euronorm 292-1","LAS-F"};
            ViewBag._lstCRMUsed = _lstCRMUsed.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #endregion

            PTR029 _objPTR029 = new PTR029();
            if (PTRHeaderId.HasValue && db.PTR029.Where(w => w.PTRHeaderId == PTRHeaderId).Count() > 0)
            {
                _objPTR029 = db.PTR029.Where(w => w.PTRHeaderId == PTRHeaderId).FirstOrDefault();
                ViewBag._lstPTR029_Log = db.PTR029_Log.Where(w => w.HeaderId == _objPTR029.HeaderId).ToList();
            }
            else
            {
                _objPTR029.TestMethod = "ASTM 415- Ed2017";
                _objPTR029.InstrumentUsed = "OES (Sr No.127198/09)";
                _objPTR029.CRMUsed = "IARM 4C";

                PTR011 _lstPTR011 = db.PTR011.Where(x => x.PTRHeaderId == PTRHeaderId).FirstOrDefault();
                if (_lstPTR011 != null)
                {
                    _objPTR029.ProjectNo = _lstPTR011.ProjectNo;
                    _objPTR029.PTRNo = _lstPTR011.PTRNo;
                    _objPTR029.MaterialSpec = _lstPTR011.MaterialSpecification;
                    _objPTR029.SeamNo = _lstPTR011.SeamNo;
                    _objPTR029.PqrNo = _lstPTR011.PQRNo;
                    _objPTR029.FillerWire = _lstPTR011.FillerWire;
                    _objPTR029.Position = _lstPTR011.Position;
                    _objPTR029.BatchNo = _lstPTR011.BatchNo;
                    _objPTR029.BrandName = _lstPTR011.BrandName;
                    _objPTR029.WpsNo = _lstPTR011.WPSNo;
                    _objPTR029.CodeSpec = _lstPTR011.CodeSpecification;
                    _objPTR029.HeatNo = _lstPTR011.HeatNo;
                }
                else
                {
                    PTR001 _lstPTR001 = db.PTR001.Where(x => x.HeaderId == PTRHeaderId).FirstOrDefault();
                    if (_lstPTR001 != null)
                    {
                        _objPTR029.ProjectNo = _lstPTR001.Project;
                        _objPTR029.PTRNo = _lstPTR001.PTRNo;
                        _objPTR029.TestDescription = _lstPTR001.CouponCategory;

                    }
                }
                List<PTR004> ptr004 = new List<PTR004>();
                ptr004 = db.PTR004.Where(x => x.HeaderId == PTRHeaderId && x.TestTypeOrientation == "Mtest34").ToList();
                if (ptr004 != null)
                {
                    if (ptr004.Count() > 0)
                    {
                        _objPTR029.CAResult1 = "% " + ptr004[0].Elements;
                        _objPTR029.CARequirement1 = ptr004[0].AcceptanceCriteria + " Max";
                    }
                    if (ptr004.Count() > 1)
                    {
                        _objPTR029.CAResult2 = "% " + ptr004[1].Elements;
                        _objPTR029.CARequirement2 = ptr004[1].AcceptanceCriteria + " Max";
                    }
                    if (ptr004.Count() > 2)
                    {
                        _objPTR029.CAResult3 = "% " + ptr004[2].Elements;
                        _objPTR029.CARequirement3 = ptr004[2].AcceptanceCriteria + " Max";
                    }
                    if (ptr004.Count() > 3)
                    {
                        _objPTR029.CAResult4 = "% " + ptr004[3].Elements;
                        _objPTR029.CARequirement4 = ptr004[3].AcceptanceCriteria + " Max";
                    }
                    if (ptr004.Count() > 4)
                    {
                        _objPTR029.CAResult5 = "% " + ptr004[4].Elements;
                        _objPTR029.CARequirement5 = ptr004[4].AcceptanceCriteria + " Max";
                    }
                    if (ptr004.Count() > 5)
                    {
                        _objPTR029.CAResult6 = "% " + ptr004[5].Elements;
                        _objPTR029.CARequirement6 = ptr004[5].AcceptanceCriteria + " Max";
                    }
                    if (ptr004.Count() > 6)
                    {
                        _objPTR029.CAResult7 = "% " + ptr004[6].Elements;
                        _objPTR029.CARequirement7 = ptr004[6].AcceptanceCriteria + " Max";
                    }
                    if (ptr004.Count() > 7)
                    {
                        _objPTR029.CAResult8 = "% " + ptr004[7].Elements;
                        _objPTR029.CARequirement8 = ptr004[7].AcceptanceCriteria + " Max";
                    }
                    if (ptr004.Count() > 8)
                    {
                        _objPTR029.CAResult9 = "% " + ptr004[8].Elements;
                        _objPTR029.CARequirement9 = ptr004[8].AcceptanceCriteria + " Max";
                    }
                    if (ptr004.Count() > 9)
                    {
                        _objPTR029.CAResult10 = "% " + ptr004[9].Elements;
                        _objPTR029.CARequirement10 = ptr004[9].AcceptanceCriteria + " Max";
                    }
                    if (ptr004.Count() > 10)
                    {
                        _objPTR029.CAResult11 = "% " + ptr004[10].Elements;
                        _objPTR029.CARequirement11 = ptr004[10].AcceptanceCriteria + " Max";
                    }
                    if (ptr004.Count() > 11)
                    {
                        _objPTR029.CAResult12 = "% " + ptr004[11].Elements;
                        _objPTR029.CARequirement12 = ptr004[11].AcceptanceCriteria + " Max";
                    }

                }
            }
            ViewBag.isEditable = IsPTRReportEditable(PTRHeaderId);
            return PartialView("ChemicalOverlay", _objPTR029);
        }

        public ActionResult SaveChemicalOverlayHeaderData(PTR029 _obj)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                bool isAdd = false;
                PTR029 _objData = new PTR029();
                if (_obj.HeaderId == 0) //Add
                    isAdd = true;
                else
                    _objData = db.PTR029.Where(c => c.HeaderId == _obj.HeaderId).FirstOrDefault();

                _objData.ReportNo = _obj.ReportNo;
                _objData.DateOfRecived = _obj.DateOfRecived;
                _objData.DateOfTesting = _obj.DateOfTesting;

                _objData.NameOfItem = _obj.NameOfItem;
                _objData.Process = _obj.Process;
                _objData.ProjectNo = _obj.ProjectNo;
                _objData.FillerWire = _obj.FillerWire;
                _objData.PTRNo = _obj.PTRNo;
                _objData.Position = _obj.Position;
                _objData.TestDescription = _obj.TestDescription;
                _objData.BrandName = _obj.BrandName;
                _objData.MaterialSpec = _obj.MaterialSpec;
                _objData.BatchNo = _obj.BatchNo;
                _objData.Thickness = _obj.Thickness;
                _objData.WelderId = _obj.WelderId;
                _objData.SeamNo = _obj.SeamNo;
                _objData.HeatNo = _obj.HeatNo;
                _objData.PqrNo = _obj.PqrNo;
                _objData.WpsNo = _obj.WpsNo;
                _objData.CodeSpec = _obj.CodeSpec;
                _objData.htDuration = _obj.htDuration;
                _objData.TestMethod = _obj.TestMethod;

                _objData.CAResult1 = _obj.CAResult1;
                _objData.CAResult2 = _obj.CAResult2;
                _objData.CAResult3 = _obj.CAResult3;
                _objData.CAResult4 = _obj.CAResult4;
                _objData.CAResult5 = _obj.CAResult5;
                _objData.CAResult6 = _obj.CAResult6;
                _objData.CAResult7 = _obj.CAResult7;
                _objData.CAResult8 = _obj.CAResult8;
                _objData.CAResult9 = _obj.CAResult9;
                _objData.CAResult10 = _obj.CAResult10;
                _objData.CAResult11 = _obj.CAResult11;
                _objData.CAResult12 = _obj.CAResult12;

                _objData.CARequirement1 = _obj.CARequirement1;
                _objData.CARequirement2 = _obj.CARequirement2;
                _objData.CARequirement3 = _obj.CARequirement3;
                _objData.CARequirement4 = _obj.CARequirement4;
                _objData.CARequirement5 = _obj.CARequirement5;
                _objData.CARequirement6 = _obj.CARequirement6;
                _objData.CARequirement7 = _obj.CARequirement7;
                _objData.CARequirement8 = _obj.CARequirement8;
                _objData.CARequirement9 = _obj.CARequirement9;
                _objData.CARequirement10 = _obj.CARequirement10;
                _objData.CARequirement11 = _obj.CARequirement11;
                _objData.CARequirement12 = _obj.CARequirement12;

                _objData.InstrumentUsed = _obj.InstrumentUsed;
                _objData.CRMUsed = _obj.CRMUsed;

                _objData.TempValue = _obj.TempValue;
                _objData.HumidityValue = _obj.HumidityValue;
                _objData.Remarks1 = _obj.Remarks1;
                _objData.Remarks2 = _obj.Remarks2;
                _objData.PTRHeaderId = _obj.PTRHeaderId;

                if (isAdd)
                {
                    _objData.CreatedBy = objClsLoginInfo.UserName;
                    _objData.CreatedOn = DateTime.Now;
                    db.PTR029.Add(_objData);
                }
                else
                {
                    _objData.EditedBy = objClsLoginInfo.UserName;
                    _objData.EditedOn = DateTime.Now;
                }

                db.SaveChanges();

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data saved successfully";
                objResponseMsg.HeaderId = _objData.HeaderId;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveChemicalOverlayLineTable1(List<PTR029_Log> _lst, int HeaderId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                List<PTR029_Log> _lstAdd = new List<PTR029_Log>();
                List<PTR029_Log> _lstDelete = new List<PTR029_Log>();
                if (_lst != null)
                {
                    foreach (var item in _lst)
                    {
                        if (item.LineId > 0)
                        {
                            PTR029_Log _objUpdate = db.PTR029_Log.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (_objUpdate != null)
                            {
                                _objUpdate.HeaderId = item.HeaderId;
                                _objUpdate.SrNo = item.SrNo;
                                _objUpdate.HeightFromWeldInt = item.HeightFromWeldInt;
                                _objUpdate.ActualValue1 = item.ActualValue1;
                                _objUpdate.ActualValue2 = item.ActualValue2;
                                _objUpdate.ActualValue3 = item.ActualValue3;
                                _objUpdate.ActualValue4 = item.ActualValue4;
                                _objUpdate.ActualValue5 = item.ActualValue5;
                                _objUpdate.ActualValue6 = item.ActualValue6;
                                _objUpdate.ActualValue7 = item.ActualValue7;
                                _objUpdate.ActualValue8 = item.ActualValue8;
                                _objUpdate.ActualValue9 = item.ActualValue9;
                                _objUpdate.ActualValue10 = item.ActualValue10;
                                _objUpdate.ActualValue11 = item.ActualValue11;
                                _objUpdate.ActualValue12 = item.ActualValue12;
                                _objUpdate.Result = item.Result;

                                _objUpdate.EditedBy = objClsLoginInfo.UserName;
                                _objUpdate.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PTR029_Log _objAdd = new PTR029_Log();
                            _objAdd.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.SrNo))
                            {
                                _objAdd.HeaderId = item.HeaderId;
                                _objAdd.SrNo = item.SrNo;
                                _objAdd.HeightFromWeldInt = item.HeightFromWeldInt;
                                _objAdd.ActualValue1 = item.ActualValue1;
                                _objAdd.ActualValue2 = item.ActualValue2;
                                _objAdd.ActualValue3 = item.ActualValue3;
                                _objAdd.ActualValue4 = item.ActualValue4;
                                _objAdd.ActualValue5 = item.ActualValue5;
                                _objAdd.ActualValue6 = item.ActualValue6;
                                _objAdd.ActualValue7 = item.ActualValue7;
                                _objAdd.ActualValue8 = item.ActualValue8;
                                _objAdd.ActualValue9 = item.ActualValue9;
                                _objAdd.ActualValue10 = item.ActualValue10;
                                _objAdd.ActualValue11 = item.ActualValue11;
                                _objAdd.ActualValue12 = item.ActualValue12;
                                _objAdd.Result = item.Result;

                                _objAdd.CreatedBy = objClsLoginInfo.UserName;
                                _objAdd.CreatedOn = DateTime.Now;
                                _lstAdd.Add(_objAdd);
                            }
                        }
                    }
                    if (_lstAdd.Count > 0)
                        db.PTR029_Log.AddRange(_lstAdd);

                    var LineIds = _lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = _lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    _lstDelete = db.PTR029_Log.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (_lstDelete.Count > 0)
                        db.PTR029_Log.RemoveRange(_lstDelete);
                    db.SaveChanges();
                }
                else
                {
                    _lstDelete = db.PTR029_Log.Where(x => x.HeaderId == HeaderId).ToList();
                    if (_lstDelete.Count > 0)
                        db.PTR029_Log.RemoveRange(_lstDelete);
                    db.SaveChanges();
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data saved successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region CHEMICAL1
        public ActionResult Chemical(int? PTRHeaderId)
        {
            #region ViewBag
            ViewBag.ControllerURL = ControllerURL;

            List<string> _lstResult = new List<string> { "Acceptable", "Not Acceptable", "To report" };
            ViewBag._lstResult = _lstResult.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            List<string> _lstTestmethod = new List<string> { "ASTM 415- Ed2017", "ASTM 1086- Ed2014", "ASTM E572 - Ed2013", "ASTM E322- Ed2012", "ASTM E1019-Ed2011", "IS 9878 & IS 8811", "ASTM E1476" };
            ViewBag._lstTestmethod = _lstTestmethod.Select(x => new ddlValue() { id = x.ToString(), text = x.ToString() }).ToList();

            List<string> _lstInstrumentUsed = new List<string> { "OES (Sr No.127198/09)", "Alloy Analyser (Sr No.62917)", "Leco CS (Sr No.4107)" };
            ViewBag._lstInstrumentUsed = _lstInstrumentUsed.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            List<string> _lstCRMUsed = new List<string> {
                "IARM 4C", "IARM 6C", "IARM 36B", "B.S.87F", "IARM 35H",
                "502-348","502-809","501-506","501-024","501-503",
                "502-364","BCS/SS 407/2","BCS/SS 405/2","BS CA5A","1173",
                "1763a","Euronorm 379-1","BS 410B","BCS/SS 473","Euronorm 035-2",
                "IARM 35JN","B.S. SS 3951","SRM 1269","Euronorm 292-1","LAS-F"};
            ViewBag._lstCRMUsed = _lstCRMUsed.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #endregion

            PTR030 _objPTR030 = new PTR030();
            if (PTRHeaderId.HasValue && db.PTR030.Where(w => w.PTRHeaderId == PTRHeaderId).Count() > 0)
            {
                _objPTR030 = db.PTR030.Where(w => w.PTRHeaderId == PTRHeaderId).FirstOrDefault();
                ViewBag._lstPTR030_Log = db.PTR030_Log.Where(w => w.HeaderId == _objPTR030.HeaderId).ToList();
            }
            else
            {
                _objPTR030.TestMethod = "ASTM 415- Ed2017";
                _objPTR030.InstrumentUsed = "OES (Sr No.127198/09)";
                _objPTR030.CRMUsed = "IARM 4C";

                PTR011 _lstPTR011 = db.PTR011.Where(x => x.PTRHeaderId == PTRHeaderId).FirstOrDefault();
                if (_lstPTR011 != null)
                {
                    _objPTR030.ProjectNo = _lstPTR011.ProjectNo;
                    _objPTR030.PTRNo = _lstPTR011.PTRNo;
                    _objPTR030.MaterialSpec = _lstPTR011.MaterialSpecification;
                    _objPTR030.SeamNo = _lstPTR011.SeamNo;
                    _objPTR030.PqrNo = _lstPTR011.PQRNo;
                    _objPTR030.FillerWire = _lstPTR011.FillerWire;
                    _objPTR030.Position = _lstPTR011.Position;
                    _objPTR030.BatchNo = _lstPTR011.BatchNo;
                    _objPTR030.BrandName = _lstPTR011.BrandName;
                    _objPTR030.WpsNo = _lstPTR011.WPSNo;
                    _objPTR030.CodeSpec = _lstPTR011.CodeSpecification;
                    _objPTR030.HeatNo = _lstPTR011.HeatNo;
                }
                else
                {
                    PTR001 _lstPTR001 = db.PTR001.Where(x => x.HeaderId == PTRHeaderId).FirstOrDefault();
                    if (_lstPTR001 != null)
                    {
                        _objPTR030.ProjectNo = _lstPTR001.Project;
                        _objPTR030.PTRNo = _lstPTR001.PTRNo;
                        _objPTR030.TestDescription = _lstPTR001.CouponCategory;

                    }
                }
                #region Default line table data display

                List<PTR004> ptr004 = new List<PTR004>();
                ptr004 = db.PTR004.Where(x => x.HeaderId == PTRHeaderId).ToList();

                if (ptr004 != null)
                {
                    List<PTR030_Log> ptr030_Log_List = new List<PTR030_Log>();
                    foreach (var item in ptr004)
                    {
                        PTR030_Log ptr030_Log = new PTR030_Log();
                        ptr030_Log.Element = item.Elements;
                        ptr030_Log.SpecificationReq = item.AcceptanceCriteria;
                        ptr030_Log_List.Add(ptr030_Log);
                    }

                    ViewBag._lstPTR030_Log = ptr030_Log_List;
                }

                //PTR029 _obj029 = db.PTR029.Where(w => w.HeaderId == Id).FirstOrDefault();

                //List<PTR030_Log> _lst = new List<PTR030_Log>();

                //PTR030_Log _obj = new PTR030_Log();
                //_obj.LineId = 0;
                //_obj.Element = _obj029.CAResult1;
                //_obj.SpecificationReq = _obj029.CARequirement1;
                //_lst.Add(_obj);

                //_obj = new PTR030_Log();
                //_obj.LineId = 0;
                //_obj.Element = _obj029.CAResult2;
                //_obj.SpecificationReq = _obj029.CARequirement2;
                //_lst.Add(_obj);

                //_obj = new PTR030_Log();
                //_obj.LineId = 0;
                //_obj.Element = _obj029.CAResult3;
                //_obj.SpecificationReq = _obj029.CARequirement3;
                //_lst.Add(_obj);

                //_obj = new PTR030_Log();
                //_obj.LineId = 0;
                //_obj.Element = _obj029.CAResult4;
                //_obj.SpecificationReq = _obj029.CARequirement4;
                //_lst.Add(_obj);

                //_obj = new PTR030_Log();
                //_obj.LineId = 0;
                //_obj.Element = _obj029.CAResult5;
                //_obj.SpecificationReq = _obj029.CARequirement5;
                //_lst.Add(_obj);

                //_obj = new PTR030_Log();
                //_obj.LineId = 0;
                //_obj.Element = _obj029.CAResult6;
                //_obj.SpecificationReq = _obj029.CARequirement6;
                //_lst.Add(_obj);

                //_obj = new PTR030_Log();
                //_obj.LineId = 0;
                //_obj.Element = _obj029.CAResult7;
                //_obj.SpecificationReq = _obj029.CARequirement7;
                //_lst.Add(_obj);

                //_obj = new PTR030_Log();
                //_obj.LineId = 0;
                //_obj.Element = _obj029.CAResult8;
                //_obj.SpecificationReq = _obj029.CARequirement8;
                //_lst.Add(_obj);

                //_obj = new PTR030_Log();
                //_obj.LineId = 0;
                //_obj.Element = _obj029.CAResult9;
                //_obj.SpecificationReq = _obj029.CARequirement9;
                //_lst.Add(_obj);

                //_obj = new PTR030_Log();
                //_obj.LineId = 0;
                //_obj.Element = _obj029.CAResult10;
                //_obj.SpecificationReq = _obj029.CARequirement10;
                //_lst.Add(_obj);

                //_obj = new PTR030_Log();
                //_obj.LineId = 0;
                //_obj.Element = _obj029.CAResult11;
                //_obj.SpecificationReq = _obj029.CARequirement11;
                //_lst.Add(_obj);

                //_obj = new PTR030_Log();
                //_obj.LineId = 0;
                //_obj.Element = _obj029.CAResult12;
                //_obj.SpecificationReq = _obj029.CARequirement12;
                //_lst.Add(_obj);

                //ViewBag.lstPTR030_Log = _lst;
                #endregion
            }
            ViewBag.isEditable = IsPTRReportEditable(PTRHeaderId);
            return PartialView("Chemical", _objPTR030);
        }

        public ActionResult SaveChemical1HeaderData(PTR030 _obj)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                bool isAdd = false;
                PTR030 _objData = new PTR030();
                if (_obj.HeaderId == 0) //Add
                    isAdd = true;
                else
                    _objData = db.PTR030.Where(c => c.HeaderId == _obj.HeaderId).FirstOrDefault();

                _objData.ReportNo = _obj.ReportNo;
                _objData.DateOfRecived = _obj.DateOfRecived;
                _objData.DateOfTesting = _obj.DateOfTesting;

                _objData.NameOfItem = _obj.NameOfItem;
                _objData.Process = _obj.Process;
                _objData.ProjectNo = _obj.ProjectNo;
                _objData.FillerWire = _obj.FillerWire;
                _objData.PTRNo = _obj.PTRNo;
                _objData.Position = _obj.Position;
                _objData.TestDescription = _obj.TestDescription;
                _objData.BrandName = _obj.BrandName;
                _objData.MaterialSpec = _obj.MaterialSpec;
                _objData.BatchNo = _obj.BatchNo;
                _objData.Thickness = _obj.Thickness;
                _objData.WelderId = _obj.WelderId;
                _objData.SeamNo = _obj.SeamNo;
                _objData.HeatNo = _obj.HeatNo;
                _objData.PqrNo = _obj.PqrNo;
                _objData.WpsNo = _obj.WpsNo;
                _objData.CodeSpec = _obj.CodeSpec;
                _objData.htDuration = _obj.htDuration;
                _objData.TestMethod = _obj.TestMethod;

                _objData.ChemicalResultLocation = _obj.ChemicalResultLocation;

                _objData.InstrumentUsed = _obj.InstrumentUsed;
                _objData.CRMUsed = _obj.CRMUsed;

                _objData.TempValue = _obj.TempValue;
                _objData.HumidityValue = _obj.HumidityValue;
                _objData.Remarks1 = _obj.Remarks1;
                _objData.Remarks2 = _obj.Remarks2;
                _objData.PTRHeaderId = _obj.PTRHeaderId;

                if (isAdd)
                {
                    _objData.CreatedBy = objClsLoginInfo.UserName;
                    _objData.CreatedOn = DateTime.Now;
                    db.PTR030.Add(_objData);
                }
                else
                {
                    _objData.EditedBy = objClsLoginInfo.UserName;
                    _objData.EditedOn = DateTime.Now;
                }

                db.SaveChanges();

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data saved successfully";
                objResponseMsg.HeaderId = _objData.HeaderId;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveChemical1LineTable1(List<PTR030_Log> _lst, int HeaderId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                List<PTR030_Log> _lstAdd = new List<PTR030_Log>();
                List<PTR030_Log> _lstDelete = new List<PTR030_Log>();
                if (_lst != null)
                {
                    foreach (var item in _lst)
                    {
                        if (item.LineId > 0)
                        {
                            PTR030_Log _objUpdate = db.PTR030_Log.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (_objUpdate != null)
                            {
                                _objUpdate.HeaderId = item.HeaderId;
                                _objUpdate.Element = item.Element;
                                _objUpdate.SpecificationReq = item.SpecificationReq;
                                _objUpdate.ObservedValue = item.ObservedValue;
                                _objUpdate.TestRemark = item.TestRemark;

                                _objUpdate.EditedBy = objClsLoginInfo.UserName;
                                _objUpdate.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PTR030_Log _objAdd = new PTR030_Log();
                            _objAdd.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.Element))
                            {
                                _objAdd.HeaderId = item.HeaderId;
                                _objAdd.Element = item.Element;
                                _objAdd.SpecificationReq = item.SpecificationReq;
                                _objAdd.ObservedValue = item.ObservedValue;
                                _objAdd.TestRemark = item.TestRemark;

                                _objAdd.CreatedBy = objClsLoginInfo.UserName;
                                _objAdd.CreatedOn = DateTime.Now;
                                _lstAdd.Add(_objAdd);
                            }
                        }
                    }
                    if (_lstAdd.Count > 0)
                        db.PTR030_Log.AddRange(_lstAdd);

                    var LineIds = _lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = _lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    _lstDelete = db.PTR030_Log.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (_lstDelete.Count > 0)
                        db.PTR030_Log.RemoveRange(_lstDelete);
                    db.SaveChanges();
                }
                else
                {
                    _lstDelete = db.PTR030_Log.Where(x => x.HeaderId == HeaderId).ToList();
                    if (_lstDelete.Count > 0)
                        db.PTR030_Log.RemoveRange(_lstDelete);
                    db.SaveChanges();
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data saved successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region IGC2
        public ActionResult IGC2(int? PTRHeaderId)
        {
            #region ViewBag
            ViewBag.ControllerURL = ControllerURL;

            List<string> _lstResult = new List<string> { "Acceptable", "Not Acceptable", "To report" };
            ViewBag._lstResult = _lstResult.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            List<string> _lstInstrumentUsed = new List<string> { "OES (Sr No.127198/09)", "Alloy Analyser (Sr No.62917)", "Leco CS (Sr No.4107)" };
            ViewBag._lstInstrumentUsed = _lstInstrumentUsed.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #endregion

            PTR031 _objPTR031 = new PTR031();
            if (PTRHeaderId.HasValue && db.PTR031.Where(w => w.PTRHeaderId == PTRHeaderId).Count() > 0)
            {
                _objPTR031 = db.PTR031.Where(w => w.PTRHeaderId == PTRHeaderId).FirstOrDefault();
            }
            else
            {
                _objPTR031.TestMethod = "ASTM A 262 PRACTICE 'E'";
                _objPTR031.Result = "Acceptable";
                _objPTR031.MachineInstrumentUsed = "OES (Sr No.127198/09)";

                PTR011 _lstPTR011 = db.PTR011.Where(x => x.PTRHeaderId == PTRHeaderId).FirstOrDefault();
                if (_lstPTR011 != null)
                {
                    _objPTR031.ProjectNo = _lstPTR011.ProjectNo;
                    _objPTR031.PtrNo = _lstPTR011.PTRNo;
                    _objPTR031.MaterialSpec = _lstPTR011.MaterialSpecification;
                    _objPTR031.SeamNo = _lstPTR011.SeamNo;
                    _objPTR031.PqrNo = _lstPTR011.PQRNo;
                    _objPTR031.WeldingProcess = _lstPTR011.WeldingProcess;
                    _objPTR031.FillerWire = _lstPTR011.FillerWire;
                    _objPTR031.Position = _lstPTR011.Position;
                    _objPTR031.BatchNo = _lstPTR011.BatchNo;
                    _objPTR031.BrandName = _lstPTR011.BrandName;
                    _objPTR031.WpsNo = _lstPTR011.WPSNo;
                    _objPTR031.CodeSpec = _lstPTR011.CodeSpecification;
                    _objPTR031.PlatePipeNo = _lstPTR011.PlateNo;
                    _objPTR031.HeatNo = _lstPTR011.HeatNo;
                }
                else
                {
                    PTR001 _lstPTR001 = db.PTR001.Where(x => x.HeaderId == PTRHeaderId).FirstOrDefault();
                    if (_lstPTR001 != null)
                    {
                        _objPTR031.ProjectNo = _lstPTR001.Project;
                        _objPTR031.PtrNo = _lstPTR001.PTRNo;
                    }
                }

            }

            #region Image path
            List<string> _lstPath = new List<string>();
            long lHeaderId = 0;
            long.TryParse(_objPTR031.HeaderId.ToString(), out lHeaderId);
            List<FCS001> _lstFCS = db.FCS001.Where(w => w.TableId == lHeaderId && w.TableName == "PTR031").ToList();
            if (_lstFCS.Count > 0)
            {
                FCS001 _obj1 = _lstFCS.Find(f => f.ViewerId == 1);
                if (_obj1 != null)
                    _lstPath.Add(_obj1.MainDocumentPath);
                else
                    _lstPath.Add("");

                FCS001 _obj2 = _lstFCS.Find(f => f.ViewerId == 2);
                if (_obj2 != null)
                    _lstPath.Add(_obj2.MainDocumentPath);
                else
                    _lstPath.Add("");

                FCS001 _obj3 = _lstFCS.Find(f => f.ViewerId == 3);
                if (_obj3 != null)
                    _lstPath.Add(_obj3.MainDocumentPath);
                else
                    _lstPath.Add("");
            }
            else
            {
                _lstPath.Add("");
                _lstPath.Add("");
                _lstPath.Add("");
            }
            #endregion

            ViewBag._lstImagePaths = _lstPath;

            ViewBag.isEditable = IsPTRReportEditable(PTRHeaderId);
            return PartialView("IGC2", _objPTR031);
        }

        public ActionResult SaveIGC2HeaderData(PTR031 _obj)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                bool isAdd = false;
                PTR031 _objData = new PTR031();
                if (_obj.HeaderId == 0) //Add
                    isAdd = true;
                else
                    _objData = db.PTR031.Where(c => c.HeaderId == _obj.HeaderId).FirstOrDefault();

                _objData.ReportNo = _obj.ReportNo;
                _objData.TestMethod = _obj.TestMethod;
                _objData.TestDate = _obj.TestDate;
                _objData.CouponDiscerption = _obj.CouponDiscerption;
                _objData.CodeSpec = _obj.CodeSpec;
                _objData.ProjectNo = _obj.ProjectNo;
                _objData.PtrNo = _obj.PtrNo;
                _objData.MaterialSpec = _obj.MaterialSpec;
                _objData.WeldingProcess = _obj.WeldingProcess;
                _objData.PlatePipeNo = _obj.PlatePipeNo;
                _objData.FillerWire = _obj.FillerWire;
                _objData.HeatNo = _obj.HeatNo;
                _objData.Position = _obj.Position;
                _objData.SizeThickness = _obj.SizeThickness;
                _objData.BatchNo = _obj.BatchNo;
                _objData.SeamNo = _obj.SeamNo;
                _objData.BrandName = _obj.BrandName;
                _objData.PWHTDetails = _obj.PWHTDetails;
                _objData.PqrNo = _obj.PqrNo;
                _objData.SampleLocation = _obj.SampleLocation;
                _objData.WpsNo = _obj.WpsNo;
                _objData.Sensitization = _obj.Sensitization;
                _objData.BoilingEtchingSolution = _obj.BoilingEtchingSolution;

                _objData.LabId = _obj.LabId;
                _objData.SampleId = _obj.SampleId;
                _objData.SDLength = _obj.SDLength;
                _objData.SDWidth = _obj.SDWidth;
                _objData.SDThickness = _obj.SDThickness;

                _objData.BoilingDuration = _obj.BoilingDuration;
                _objData.AngelOfBend = _obj.AngelOfBend;
                _objData.MandrelDia = _obj.MandrelDia;
                _objData.Magnification = _obj.Magnification;
                _objData.Observation = _obj.Observation;

                _objData.PhotoHeading1 = _obj.PhotoHeading1;
                _objData.PhotoHeading2 = _obj.PhotoHeading2;
                _objData.PhotoHeading3 = _obj.PhotoHeading3;
                _objData.Result = _obj.Result;
                _objData.MachineInstrumentUsed = _obj.MachineInstrumentUsed;

                _objData.TempValue = _obj.TempValue;
                _objData.HumidityValue = _obj.HumidityValue;
                _objData.Remarks1 = _obj.Remarks1;
                _objData.Remarks2 = _obj.Remarks2;
                _objData.PTRHeaderId = _obj.PTRHeaderId;

                if (isAdd)
                {
                    _objData.CreatedBy = objClsLoginInfo.UserName;
                    _objData.CreatedOn = DateTime.Now;
                    db.PTR031.Add(_objData);
                }
                else
                {
                    _objData.EditedBy = objClsLoginInfo.UserName;
                    _objData.EditedOn = DateTime.Now;
                }

                db.SaveChanges();

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data saved successfully";
                objResponseMsg.HeaderId = _objData.HeaderId;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        [HttpPost]
        public ActionResult DeletePhoto(int PhotoNo, int Headerid, string strTableName)
        {
            try
            {
                var Files = (new clsFileUpload()).GetDocuments(strTableName + "/" + Headerid + "/" + PhotoNo);
                foreach (var item in Files)
                {
                    (new clsFileUpload()).DeleteFile(strTableName + "/" + Headerid + "/" + PhotoNo, item.Name);
                }

                return Json(true);
            }
            catch
            {
                return Json(false);
            }
        }
        [HttpPost]
        public ActionResult DeletePhotoHeading(int PhotoNo, int Headerid, string strTableName)
        {
            try
            {
                db.Database.Initialize(force: false);
                System.Data.Common.DbCommand cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = $" UPDATE {strTableName} SET PhotoHeading{PhotoNo} = NULL WHERE HeaderId={Headerid}";
                db.Database.Connection.Open();
                int i = cmd.ExecuteNonQuery();
                return Json(true);
            }
            catch
            {
                return Json(false);
            }
        }

        [NonAction]
        public bool IsPTRReportEditable(int? PTRHeaderId)
        {
            var status = db.PTR001.Where(x => x.HeaderId == PTRHeaderId).Select(x => x.Status).FirstOrDefault();

            if (status == clsImplementationEnum.PTRStatus.Closed.GetStringValue())
            {
                return false;
            }
            else
            {
                return true;
            }
        }


    }
}