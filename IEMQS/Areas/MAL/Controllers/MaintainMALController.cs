﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Mvc;
using System.Globalization;

namespace IEMQS.Areas.MAL.Controllers
{
    public class MaintainMALController : clsBase
    {
        // GET: MAL/MaintainMAL
        #region Header List
        [SessionExpireFilter]
        [AllowAnonymous]
        // [UserPermissions]
        public ActionResult Index()
        {
            ViewBag.Title = "Maintain Milestone Activity ";
            List<string> lstMALProtocolType = clsImplementationEnum.getMALProtocolType().ToList();
            ViewBag.lstMALProtocolType = lstMALProtocolType.AsEnumerable().Select(x => new { Value = x.ToString(), Text = x.ToString() }).ToList();

            return View();
        }

        //load tab wise partial
        [HttpPost]
        public ActionResult LoadDataGridPartial(string status, string title, string indextype)
        {
            ViewBag.Status = status;
            ViewBag.GridTitle = title;
            return PartialView("_GetIndexGridDataPartial");
        }
        //bind datatable for Data Grid
        [HttpPost]
        public JsonResult LoadDataGridData(JQueryDataTableParamModel param)

        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhereCondition = string.Empty;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                strWhereCondition += "1=1 ";
                //search Condition 
                if (param.Status.ToUpper() == "PENDING")
                {
                    strWhereCondition += " and mal000.Status in('" + clsImplementationEnum.CommonStatus.DRAFT.GetStringValue() + "')";
                }
                string[] columnName = {
                                        "Project",
                                        "InspectionActivity",
                                        "SerialNo",
                                        "Status",
                                        "CreatedBy",
                                        "CreatedOn",
                                        "Project +'-'+com1.t_dsca",
                                        "CreatedBy +'-'+com3.t_name"
                                        };
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                var lstResult = db.SP_MAL_ACTIVITY_INDEX_DATA(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();

                var data = (from uc in lstResult
                            select new[]
                            {
                            Convert.ToString(uc.Id),
                            Convert.ToString(uc.Id),
                            Convert.ToString(uc.Project),
                            Convert.ToString(uc.InspectionActivity),
                            Convert.ToString(uc.SerialNo),
                            Convert.ToString(uc.Status),
                            Convert.ToString(uc.CreatedBy),
                            uc.CreatedOn==null || uc.CreatedOn.Value==DateTime.MinValue?"":uc.CreatedOn.Value.ToString("dd/MM/yyyy" ,CultureInfo.InvariantCulture),
                            Helper.GenerateActionIcon(uc.HeaderId, "View", "View Detail", "fa fa-eye", "",WebsiteURL +setURL(uc.InspectionActivity)+"/"+uc.HeaderId+(uc.Status != "Approved" ? "?m=1":""),false)
                            + Helper.GenerateActionIcon(uc.HeaderId, "Print", "Print Detail", "fa fa-print","printReport('"+setPrintURL(uc.InspectionActivity)+"',"+uc.HeaderId+",'"+uc.InspectionActivity+"')","" ,false)

                          }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        [HttpPost]
        public ActionResult ApprovePTR(string strRecordIds)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (strRecordIds != "" && strRecordIds != null)
                {
                    string[] idList = strRecordIds.Split(',');

                    foreach (var id in idList)
                    {
                        int intId = Convert.ToInt32(id);
                        var obj = db.MAL000.Where(x => x.Id == intId).FirstOrDefault();
                        obj.Status = clsImplementationEnum.CommonStatus.Approved.GetStringValue();
                        obj.EditedBy = objClsLoginInfo.UserName;
                        obj.EditedOn = DateTime.Now;
                        db.SaveChanges();
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol approved successfully";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please select record";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        #region setURL
        [HttpPost]
        public string setURL(string type)
        {
            string url = string.Empty;
            if (type == clsImplementationEnum.MALProtocolType.HeatTreatment.GetStringValue()) { url = "/MAL/HeatTreatment/Details"; }
            if (type == clsImplementationEnum.MALProtocolType.NamePlate.GetStringValue()) { url = "/MAL/NamePlate/Details"; }
            if (type == clsImplementationEnum.MALProtocolType.PicklingPassivation.GetStringValue()) { url = "/MAL/PicklingPassivation/Details"; }
            if (type == clsImplementationEnum.MALProtocolType.RubOff.GetStringValue()) { url = "/MAL/RubOff/Details"; }
            if (type == clsImplementationEnum.MALProtocolType.Stenciling.GetStringValue()) { url = "/MAL/Stenciling/Details"; }
            if (type == clsImplementationEnum.MALProtocolType.Blasting.GetStringValue()) { url = "/MAL/Blasting/Details"; }
            if (type == clsImplementationEnum.MALProtocolType.Coating.GetStringValue()) { url = "/MAL/Coating/Details"; }
            if (type == clsImplementationEnum.MALProtocolType.DFT.GetStringValue()) { url = "/MAL/DFT/Details"; }
            if (type == clsImplementationEnum.MALProtocolType.FinalActivities.GetStringValue()) { url = "/MAL/FinalActivities/Details"; }
            if (type == clsImplementationEnum.MALProtocolType.LTCertificate.GetStringValue()) { url = "/MAL/LTCertificate/Details"; }
            if (type == clsImplementationEnum.MALProtocolType.Heliumleaktest.GetStringValue()) { url = "/MAL/Heliumleaktest/Details"; }
            if (type == clsImplementationEnum.MALProtocolType.HydrostaticTest.GetStringValue()) { url = "/MAL/HydrostaticTest/Details"; }
            if (type == clsImplementationEnum.MALProtocolType.PneumaticTest.GetStringValue()) { url = "/MAL/PneumaticTest/Details"; }
            if (type == clsImplementationEnum.MALProtocolType.N2FillingInspection.GetStringValue()) { url = "/MAL/N2FillingInspection/Details"; }
            return url;
        }

        [HttpPost]
        public string setPrintURL(string type)
        {
            string url = string.Empty;
            if (type == clsImplementationEnum.MALProtocolType.HeatTreatment.GetStringValue()) { url = "/MAL/HEATTREATMENT"; }
            if (type == clsImplementationEnum.MALProtocolType.NamePlate.GetStringValue()) { url = "/MAL/NAMEPLATE"; }
            if (type == clsImplementationEnum.MALProtocolType.PicklingPassivation.GetStringValue()) { url = "/MAL/PICKLINGPASSIVATION"; }
            if (type == clsImplementationEnum.MALProtocolType.RubOff.GetStringValue()) { url = "/MAL/RUBOFF"; }
            if (type == clsImplementationEnum.MALProtocolType.Stenciling.GetStringValue()) { url = "/MAL/STENCILING"; }
            if (type == clsImplementationEnum.MALProtocolType.Blasting.GetStringValue()) { url = "/MAL/BLASTING"; }
            if (type == clsImplementationEnum.MALProtocolType.Coating.GetStringValue()) { url = "/MAL/COATING"; }
            if (type == clsImplementationEnum.MALProtocolType.DFT.GetStringValue()) { url = "/MAL/DFT"; }
            if (type == clsImplementationEnum.MALProtocolType.Heliumleaktest.GetStringValue()) { url = "/MAL/HELIUM LEAK TEST REPORT"; }
            if (type == clsImplementationEnum.MALProtocolType.HydrostaticTest.GetStringValue()) { url = "/MAL/HYDROSTATICTEST"; }
            if (type == clsImplementationEnum.MALProtocolType.PneumaticTest.GetStringValue()) { url = "/MAL/PNEUMATICTEST"; }
            if (type == clsImplementationEnum.MALProtocolType.N2FillingInspection.GetStringValue()) { url = "/MAL/N2FillingInspection"; }
            if (type == clsImplementationEnum.MALProtocolType.LTCertificate.GetStringValue()) { url = "/MAL/LTCERTIFICATE"; }
            if (type == clsImplementationEnum.MALProtocolType.FinalActivities.GetStringValue()) { url = "/MAL/FinalActivities"; }

            return url;
        }

        [HttpPost]
        public ActionResult GetInspectionAgency(string type , int headerid)
        {
            string Agency = string.Empty;
            List<string> tpi = new List<string>();
            string qproject = string.Empty;
            if (type == clsImplementationEnum.MALProtocolType.Heliumleaktest.GetStringValue())
            {
                qproject = db.MAL019.Where(x => x.HeaderId == headerid).Select(q => q.QualityProject).FirstOrDefault();
            }
            if (!string.IsNullOrWhiteSpace(qproject))
            {
                string inspectionagency = Manager.GetInspectionAgency(qproject);
                if (!string.IsNullOrWhiteSpace(inspectionagency))
                {
                    string[] arr = inspectionagency.Split(',').ToArray();
                    Agency = string.Join("#", arr.Take(3).ToArray());

                    for (int i = 0; i < arr.Length; i++)
                    {
                        string code = arr[i];
                        var Description = (from glb002 in db.GLB002
                                           join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                                           where glb001.Category.Equals("Inspection Agency", StringComparison.OrdinalIgnoreCase) && glb002.Code.Equals(code) //&& glb002.IsActive == true
                                           select glb002.Description
                                         ).FirstOrDefault();
                        tpi.Add((string.IsNullOrWhiteSpace(Description)) ? arr[i] : Description);
                    }
                }
            }
            var obj = new
            {
                Agency = Agency,
                arr = tpi.Count() > 0? string.Join(",", tpi):""
            };
            return Json(obj, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}