﻿using IEMQS.Areas.Utility.Controllers;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.MAL.Controllers
{
    public class HeatTreatmentController : clsBase
    {
        // GET: MAL/HeatTreatment
        string ControllerURL = "/MAL/HeatTreatment/";
        string Title = "HEAT TREATMENT SUMMARY";
        public ActionResult Details(int? id, int? m)
        {
            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;
            ViewBag.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
            ViewBag.lstMAL013_2 = null;

            List<string> lstdegree = clsImplementationEnum.getDegree().ToList();
            ViewBag.DegreeEnum = lstdegree.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();



            MAL013 objMAL013 = new MAL013();
            if (id > 0)
            {
                objMAL013 = db.MAL013.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;

                List<MAL013_2> lstMAL013_2 = db.MAL013_2.Where(x => x.HeaderId == objMAL013.HeaderId).ToList();
                List<MAL013_3> lstMAL013_3 = db.MAL013_3.Where(x => x.HeaderId == objMAL013.HeaderId).ToList();

                ViewBag.lstMAL013_2 = lstMAL013_2;
                ViewBag.lstMAL013_3 = lstMAL013_3;

                string HeatTreatment = clsImplementationEnum.MALProtocolType.HeatTreatment.GetStringValue();
                var obj = db.MAL000.Where(a => a.HeaderId == objMAL013.HeaderId && a.InspectionActivity == HeatTreatment).FirstOrDefault();
                ViewBag.Status = obj?.Status;
                if (m == 1)
                { ViewBag.isEditable = "true"; }
                else
                { ViewBag.isEditable = "false"; }
            }
            else
            {
                ViewBag.isEditable = "true";
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;
            ViewBag.PrintedBy = objClsLoginInfo.UserName;

            if (id > 0 && objMAL013 != null)
            {
                #region bind lines

                var ddlHTRNoData = db.HTR001.Where(x => x.Project.ToLower() == objMAL013.ProjectNo.ToLower()).Select(x => x.HTRNo).Distinct().ToList();
                List<SelectListItem> listHtrNo = new List<SelectListItem>();

                foreach (var item in ddlHTRNoData)
                {
                    listHtrNo.Add(new SelectListItem() { Text = item, Value = item });
                }
                ViewBag.ddlHTRNo = listHtrNo;

                #endregion
            }

            return View(objMAL013);
        }

        [HttpPost]
        public ActionResult SaveHeatTreatmentHeaderData(MAL013 MAL013, int s = 0)
        {
            MAL013 objMAL013 = new MAL013();
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = MAL013.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    objMAL013 = db.MAL013.FirstOrDefault(c => c.HeaderId == refHeaderId);
                }

                #region Save Data
                objMAL013.ProjectNo = MAL013.ProjectNo;
                objMAL013.Date = MAL013.Date;
                objMAL013.EquipmentNameNumber = MAL013.EquipmentNameNumber;
                objMAL013.InspectionAgency = MAL013.InspectionAgency;
                objMAL013.HeatTreatmentType = MAL013.HeatTreatmentType;
                objMAL013.Client = MAL013.Client;
                objMAL013.PoNo = MAL013.PoNo;
                objMAL013.LoadingTemperature_Degree = MAL013.LoadingTemperature_Degree;
                objMAL013.UnLoadingTemperatureMax_Degree = MAL013.UnLoadingTemperatureMax_Degree;
                objMAL013.RateOfHeatingCoolingPerHourMax_Degree = MAL013.RateOfHeatingCoolingPerHourMax_Degree;
                objMAL013.SoakingTemperature_Degree = MAL013.SoakingTemperature_Degree;
                objMAL013.RateOfHeatingPerHourActual_Degree = MAL013.RateOfHeatingPerHourActual_Degree;
                objMAL013.RateOfCoolingPerHourActual_Degree = MAL013.RateOfCoolingPerHourActual_Degree;

                if (refHeaderId == 0)
                {
                    objMAL013.CreatedBy = objClsLoginInfo.UserName;
                    objMAL013.CreatedOn = DateTime.Now;
                    db.MAL013.Add(objMAL013);
                }
                else
                {
                    objMAL013.EditedBy = objClsLoginInfo.UserName;
                    objMAL013.EditedOn = DateTime.Now;
                }



                db.SaveChanges();

                string HeatTreatment = clsImplementationEnum.MALProtocolType.HeatTreatment.GetStringValue();

                var obj = db.MAL000.Where(a => a.HeaderId == objMAL013.HeaderId && a.InspectionActivity == HeatTreatment).FirstOrDefault();
                if (obj == null)
                {
                    int? srno = db.MAL000.Where(o => o.Project == objMAL013.ProjectNo && o.InspectionActivity == HeatTreatment).OrderByDescending(x => x.SerialNo).Select(a => a.SerialNo).FirstOrDefault();
                    MAL000 objMAL000 = new MAL000();
                    objMAL000.Project = objMAL013.ProjectNo;
                    objMAL000.InspectionActivity = HeatTreatment;
                    objMAL000.SerialNo = Convert.ToInt32((srno >= 0 ? srno : 0) + 1);
                    objMAL000.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                    //objMAL000.LoadingTemperature_Degree = objMAL013.LoadingTemperature_Degree;
                    objMAL000.CreatedBy = objClsLoginInfo.UserName;
                    objMAL000.CreatedOn = DateTime.Now;
                    objMAL000.HeaderId = objMAL013.HeaderId;
                    db.MAL000.Add(objMAL000);
                    db.SaveChanges();
                }
                else
                {
                    if (s == 1)
                    {
                        obj.Status = clsImplementationEnum.CommonStatus.Approved.GetStringValue();
                        obj.EditedBy = objClsLoginInfo.UserName;
                        obj.EditedOn = DateTime.Now;
                        db.SaveChanges();
                    }
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Protocol submitted successfully";
                objResponseMsg.HeaderId = objMAL013.HeaderId;
                #endregion
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        #region Line1 details
        [HttpPost]
        public JsonResult SaveLine1(List<MAL013_2> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<MAL013_2> lstAddMAL013_2 = new List<MAL013_2>();
                List<MAL013_2> lstDeleteMAL013_2 = new List<MAL013_2>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            MAL013_2 obj = db.MAL013_2.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.HeaderId = item.HeaderId;
                                obj.DrgNo = item.DrgNo;
                                obj.RevNo = item.RevNo;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            MAL013_2 obj = new MAL013_2();
                            obj.HeaderId = item.HeaderId;
                            obj.DrgNo = item.DrgNo;
                            obj.RevNo = item.RevNo;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            lstAddMAL013_2.Add(obj);

                        }
                    }
                    if (lstAddMAL013_2.Count > 0)
                    {
                        db.MAL013_2.AddRange(lstAddMAL013_2);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeleteMAL013_2 = db.MAL013_2.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeleteMAL013_2.Count > 0)
                    {
                        db.MAL013_2.RemoveRange(lstDeleteMAL013_2);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeleteMAL013_2 = db.MAL013_2.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeleteMAL013_2.Count > 0)
                    {
                        db.MAL013_2.RemoveRange(lstDeleteMAL013_2);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region Line2 details
        [HttpPost]
        public JsonResult SaveLine2(List<MAL013_3> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<MAL013_3> lstAddMAL013_3 = new List<MAL013_3>();
                List<MAL013_3> lstDeleteMAL013_3 = new List<MAL013_3>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        MAL013_3 obj = db.MAL013_3.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new MAL013_3();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        obj.HeaderId = item.HeaderId;
                        obj.Details = item.Details;
                        obj.SeamNo = item.SeamNo;
                        obj.LoadingTemperature = item.LoadingTemperature;
                        obj.UnLoadingTemperatureMax = item.UnLoadingTemperatureMax;
                        obj.RateOfHeatingCoolingPerHourMax = item.RateOfHeatingCoolingPerHourMax;
                        obj.SoakingTemperature = item.SoakingTemperature;
                        obj.SoakingTime = item.SoakingTime;
                        obj.SoakingTimeActual = item.SoakingTimeActual;
                        obj.RateOfHeatingPerHourActual = item.RateOfHeatingPerHourActual;
                        obj.RateOfCoolingPerHourActual = item.RateOfCoolingPerHourActual;
                        obj.HTRNo = item.HTRNo;
                        obj.GraphNo = item.GraphNo;
                        if (isAdded)
                        {
                            lstAddMAL013_3.Add(obj);
                        }
                    }
                    if (lstAddMAL013_3.Count > 0)
                    {
                        db.MAL013_3.AddRange(lstAddMAL013_3);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeleteMAL013_3 = db.MAL013_3.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeleteMAL013_3.Count > 0)
                    {
                        db.MAL013_3.RemoveRange(lstDeleteMAL013_3);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeleteMAL013_3 = db.MAL013_3.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeleteMAL013_3.Count > 0)
                    {
                        db.MAL013_3.RemoveRange(lstDeleteMAL013_3);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion


        [HttpPost]
        public string ProjectDetails(string ProjectNo)
        {
            if (ProjectNo.Contains("/"))
            {
                string[] arr = ProjectNo.Split('/');
                ProjectNo = arr[0];
            }

            var Data = db.COM001.Where(w => w.t_cprj == ProjectNo).FirstOrDefault();
            string sValue = Data.t_cprj + "-" + Data.t_dsca;
            return sValue;
        }

        [HttpPost]
        public JsonResult GetHTRById(string htrNo)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                //var htrData = db.HTR001.Where(x => x.HTRNo == htrNo).FirstOrDefault();



                var htrData = (from htr001 in db.HTR001
                               join htr002 in db.HTR002 on htr001.HTRNo equals htr002.HTRNo
                               where htr001.HTRNo == htrNo
                               select new HTRData
                               {
                                   HTRNo = htr001.HTRNo,
                                   SoakingTime = htr001.SoakingTimeHH + ":" + htr001.SoakingTimeMM,
                                   Details = htr002.Description,
                                   SeamNo = htr002.SeamNo,
                                   LoadingTemperature = htr001.MaxLoadingTemp,
                                   UnLoadingTemperatureMax = htr001.MaxUnloadingTemp,
                                   RateOfHeatingCoolingPerHourMax = htr001.MaxRateOfHeating,
                                   SoakingTemperature = htr001.MaxSoakingTemperature,
                               }).FirstOrDefault();



                return Json(htrData);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpPost]
        public ActionResult GetHTRNo(string ProjectNo)
        {
            //  var ddlHTRNoData = db.HTR001.Where(x => x.Project.ToLower() == ProjectNo.ToLower()).Select(x => x.HTRNo).Distinct().ToList();
            List<SelectListItem> listHtrNo = new List<SelectListItem>();

            //foreach (var item in ddlHTRNoData)
            //{
            //    listHtrNo.Add(new SelectListItem() { Text = item, Value = item });
            //}
            //return Json(listHtrNo, JsonRequestBehavior.AllowGet);
            QMS010 obj = new QMS010();
            obj= db.QMS010.Where(x => x.QualityProject == ProjectNo.ToLower()).FirstOrDefault();
            //string Porj_QMS010 = db.QMS010.Where(x => x.QualityProject == obj.ProjectNo.ToLower()).Select(x => x.Project).ToString();
            string Porj_QMS010 =Convert.ToString(obj.Project);
            var ddlHTRNoData = db.HTR001.Where(x => x.Project.ToLower() == Porj_QMS010).Select(x => x.HTRNo).Distinct().ToList();
            
            foreach (var item in ddlHTRNoData)
            {
                listHtrNo.Add(new SelectListItem() { Text = item, Value = item });
            }
            return Json(listHtrNo, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult getDrgNo(string ProjectNo)
        {
            
            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(ProjectNo).ToList();
            lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            return Json(lstdrawing.Select(x => new
            {
                Value = x,
                Text = x
            }).ToList(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        //public string GetInspectionAgency(string qproject)
        //{
        //    string Agency = string.Empty;
        //    string inspectionagency = Manager.GetInspectionAgency(qproject);
        //    if (!string.IsNullOrWhiteSpace(inspectionagency))
        //    {
        //        string[] arr = inspectionagency.Split(',').ToArray();
        //        Agency = string.Join("#", arr.Take(3).ToArray());
        //    }
        //    return Agency;
        //}

        public ActionResult GetInspectionAgency(string qproject)
        {
            string Agency = string.Empty;
            List<string> tpi = new List<string>();
            string inspectionagency = Manager.GetInspectionAgency(qproject);
            if (!string.IsNullOrWhiteSpace(inspectionagency))
            {
                string[] arr = inspectionagency.Split(',').ToArray();
                Agency = string.Join("#", arr.Take(3).ToArray());

                for (int i = 0; i < arr.Length; i++)
                {
                    string code = arr[i];
                    var Description = (from glb002 in db.GLB002
                                       join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                                       where glb001.Category.Equals("Inspection Agency", StringComparison.OrdinalIgnoreCase) && glb002.Code.Equals(code) //&& glb002.IsActive == true
                                       select glb002.Description
                                     ).FirstOrDefault();
                    tpi.Add((string.IsNullOrWhiteSpace(Description)) ? arr[i] : Description);
                }
            }
            var obj = new
            {
                Agency = Agency,
                arr = string.Join(",", tpi)
            };
            return Json(obj, JsonRequestBehavior.AllowGet);
        }
    }

    public class HTRData
    {
        public string HTRNo { get; set; }
        public string SoakingTime { get; set; }
        public string RateOfHeatingPerHourActual { get; set; }
        public string RateOfCoolingPerHourActual { get; set; }
        public string GraphNo { get; set; }
        public string Details { get; set; }
        public string SeamNo { get; set; }
        public string LoadingTemperature { get; set; }
        public string UnLoadingTemperatureMax { get; set; }
        public string RateOfHeatingCoolingPerHourMax { get; set; }
        public string SoakingTemperature { get; set; }
        public string SoakingTimeActual { get; set; }
    }

}