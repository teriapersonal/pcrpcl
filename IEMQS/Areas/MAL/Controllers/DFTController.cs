﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQSImplementation;
using static IEMQSImplementation.clsBase;
using static IEMQSImplementation.clsImplementationMessage;
using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
namespace IEMQS.Areas.MAL.Controllers
{
    public class DFTController : clsBase
    {
        // GET: MAL/DFT
        //public ActionResult Details()
        //{
        //    return View();
        //}

        string ControllerURL = "/MAL/DFT/";
        string Title = "DFT REPORT";
        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            MAL010 objMAL010 = new MAL010();
            if (id > 0)
            {
                objMAL010 = db.MAL010.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;

                if (m == 1)
                { ViewBag.isEditable = "true"; }
                else
                { ViewBag.isEditable = "false"; }
            }
            else
            {
                ViewBag.isEditable = "true";
            }

            string DFT = clsImplementationEnum.MALProtocolType.DFT.GetStringValue();

            var obj = db.MAL000.Where(a => a.HeaderId == objMAL010.HeaderId && a.InspectionActivity == DFT).FirstOrDefault();
            ViewBag.Status = obj?.Status;
            ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objMAL010.Project).Select(i => i.t_dsca).FirstOrDefault();
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            #region bind lines
            List<MAL011> lstMAL011 = db.MAL011.Where(x => x.HeaderId == objMAL010.HeaderId).ToList();
            //List<MAL006> lstMAL006 = db.MAL006.Where(x => x.HeaderId == objMAL010.HeaderId).ToList();
            //List<MAL007> lstMAL007 = db.MAL007.Where(x => x.HeaderId == objMAL010.HeaderId).ToList();
            //List<MAL008> lstMAL008 = db.MAL008.Where(x => x.HeaderId == objMAL010.HeaderId).ToList();

            ViewBag.lstMAL011 = lstMAL011;
            //ViewBag.lstMAL006 = lstMAL006;
            //ViewBag.lstMAL007 = lstMAL007;
            //ViewBag.lstMAL008 = lstMAL008;
            #endregion

            if (objMAL010 != null)
            {
                FCS001 _objfcs = db.FCS001.Where(w => w.TableId == objMAL010.HeaderId && w.TableName == "MAL010" && w.ViewerId == 1).FirstOrDefault();
                if (_objfcs != null)
                    ViewBag.FCS_ImagePath = _objfcs.MainDocumentPath;
                else
                    ViewBag.FCS_ImagePath = "";
            }
            else
                ViewBag.FCS_ImagePath = "";

            return View(objMAL010);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(MAL010 MAL010, int s = 0)
        {
            MAL010 objMAL010 = new MAL010();
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = MAL010.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    objMAL010 = db.MAL010.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                }

                #region Save Data
                objMAL010.Project = MAL010.Project;
                objMAL010.DocumentNo = MAL010.DocumentNo;
                objMAL010.ProcedureNo = MAL010.ProcedureNo;
                objMAL010.ReportNo = MAL010.ReportNo;


                if (refHeaderId == 0)
                {
                    objMAL010.CreatedBy = objClsLoginInfo.UserName;
                    objMAL010.CreatedOn = DateTime.Now;
                    db.MAL010.Add(objMAL010);
                }
                else
                {
                    objMAL010.EditedBy = objClsLoginInfo.UserName;
                    objMAL010.EditedOn = DateTime.Now;
                }

                db.SaveChanges();
                //if (db.MAL000.Any(a => a.Id != objMAL010.HeaderId && a.InspectionActivity == "DFT"))
                //{
                //    MAL000 objMAL000 = new MAL000();
                //    objMAL000.Project = objMAL010.Project;
                //    objMAL000.InspectionActivity = "DFT";
                //    //objMAL000.SerialNo = db.MAL000.where
                //    // objMAL000.Status = clsImplementationEnum.CommonStatus.Draft.GetStringValue();
                //    objMAL000.CreatedBy = objClsLoginInfo.UserName;
                //    objMAL000.CreatedOn = DateTime.Now;
                //    //objMAL000.HeaderId = objMAL010.HeaderId;
                //    db.MAL000.Add(objMAL000);
                //    db.SaveChanges();
                //}
                string DFT = clsImplementationEnum.MALProtocolType.DFT.GetStringValue();
                var obj = db.MAL000.Where(a => a.HeaderId == objMAL010.HeaderId && a.InspectionActivity == DFT).FirstOrDefault();
                if (obj == null)
                {
                    int? srno = db.MAL000.Where(o => o.Project == objMAL010.Project && o.InspectionActivity == DFT).OrderByDescending(x => x.SerialNo).Select(a => a.SerialNo).FirstOrDefault();
                    MAL000 objMAL000 = new MAL000();
                    objMAL000.Project = objMAL010.Project;
                    objMAL000.InspectionActivity = DFT;
                    objMAL000.SerialNo = Convert.ToInt32((srno >= 0 ? srno : 0) + 1);
                    objMAL000.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();

                    objMAL000.CreatedBy = objClsLoginInfo.UserName;
                    objMAL000.CreatedOn = DateTime.Now;
                    objMAL000.HeaderId = objMAL010.HeaderId;
                    db.MAL000.Add(objMAL000);
                    db.SaveChanges();
                }
                else
                {
                    if (s == 1)
                    {
                        obj.Status = clsImplementationEnum.CommonStatus.Approved.GetStringValue();
                        obj.EditedBy = objClsLoginInfo.UserName;
                        obj.EditedOn = DateTime.Now;
                        db.SaveChanges();
                    }
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Protocol submitted successfully";
                objResponseMsg.HeaderId = objMAL010.HeaderId;
                #endregion
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Save Line table
        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3(List<MAL011> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<MAL011> lstAddMAL011 = new List<MAL011>();
                List<MAL011> lstDeleteMAL011 = new List<MAL011>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        MAL011 obj = db.MAL011.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        else
                        {
                            obj = new MAL011();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }

                        if (!string.IsNullOrWhiteSpace(item.Orientation10))
                        {
                            obj.SpotNo = item.SpotNo;
                            obj.Orientation10 = item.Orientation10;
                            obj.Orientation20 = item.Orientation20;
                            obj.Orientation30 = item.Orientation30;
                            obj.Orientation190 = item.Orientation190;
                            obj.Orientation290 = item.Orientation290;
                            obj.Orientation390 = item.Orientation390;
                            obj.Orientation1180 = item.Orientation1180;
                            obj.Orientation2180 = item.Orientation2180;
                            obj.Orientation3180 = item.Orientation3180;
                            obj.Orientation1270 = item.Orientation1270;
                            obj.Orientation2270 = item.Orientation2270;
                            obj.Orientation3270 = item.Orientation3270;
                        }
                        if (isAdded)
                        {
                            lstAddMAL011.Add(obj);
                        }
                    }
                    if (lstAddMAL011.Count > 0)
                    {
                        db.MAL011.AddRange(lstAddMAL011);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeleteMAL011 = db.MAL011.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeleteMAL011.Count > 0)
                    {
                        db.MAL011.RemoveRange(lstDeleteMAL011);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeleteMAL011 = db.MAL011.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeleteMAL011.Count > 0)
                    {
                        db.MAL011.RemoveRange(lstDeleteMAL011);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #endregion
        #region Common function
        public UserRoleAccessDetails GetUserAccessRights()
        {
            UserRoleAccessDetails objUserRoleAccessDetails = new UserRoleAccessDetails();

            try
            {
                var role = (from a in db.ATH001
                            join b in db.ATH004 on a.Role equals b.Id
                            where a.Employee.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase)
                            select new { RoleId = a.Role, RoleDesc = b.Role }).ToList();

                if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PROD3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.PROD3.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Initiator.GetStringValue();
                }
                else if (role.Where(i => (i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.QC3.GetStringValue(), StringComparison.OrdinalIgnoreCase) || i.RoleDesc == clsImplementationEnum.UserRoleName.QC2.GetStringValue() || i.RoleDesc == clsImplementationEnum.UserRoleName.QI2.GetStringValue() || i.RoleDesc == clsImplementationEnum.UserRoleName.QI3.GetStringValue())).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.QC3.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Initiator.GetStringValue();
                }
                else
                {
                    objUserRoleAccessDetails.UserRole = string.Empty;
                    objUserRoleAccessDetails.UserDesignation = string.Empty;
                }

            }
            catch
            {

            }
            return objUserRoleAccessDetails;
        }
        #endregion

    }
}