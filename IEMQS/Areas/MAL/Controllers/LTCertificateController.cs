﻿using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace IEMQS.Areas.MAL.Controllers
{
    public class LTCertificateController : clsBase
    {
        // GET: MAL/LTCertificate
        string ControllerURL = "/MAL/LTCertificate/";
        string Title = "L&T Certificate";

        public ActionResult Details(int? id, int? m)
        {
            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;
            ViewBag.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
            List<string> _lstyesnona = clsImplementationEnum.getYesNoNA().ToList();
            ViewBag.YesNoNAEnum = _lstyesnona.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
           //ViewBag.UserRole = objUserRoleAccessDetails.UserRole;
            ViewBag.PrintedBy = objClsLoginInfo.UserName;

            MAL018 objMAL018 = new MAL018();
            if (id > 0)
            {
                objMAL018 = db.MAL018.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;

                string NamePlate = clsImplementationEnum.MALProtocolType.LTCertificate.GetStringValue();
                var obj = db.MAL000.Where(a => a.HeaderId == objMAL018.HeaderId && a.InspectionActivity == NamePlate).FirstOrDefault();
                ViewBag.Status = obj?.Status;
                if (m == 1)
                { ViewBag.isEditable = "true"; }
                else
                { ViewBag.isEditable = "false"; }
            }
            else
            {
                ViewBag.isEditable = "true";
                objMAL018.SpecificationNos = "AS SPECIFIED IN OUR DRAWINGS";
                objMAL018.CustomersDrawingSpecNos = "AS SPECIFIED IN OUR DRAWINGS";
                objMAL018.WeldingMaterialsUsed = "AS SPECIFIED IN OUR DRAWINGS";

                objMAL018.MaterialOfConstruction = "ANNEXURE-I";
                objMAL018.RadiographyTestReports = "ANNEXURE-II";
                objMAL018.Heattreatmentreport = "ANNEXURE-III";
                objMAL018.ProductiontestCouponResults = "ANNEXURE-IV";
                objMAL018.HydrostatiocTestReport = "ANNEXURE-V";
                objMAL018.MarkingOfEquipmentNamePlate = "ANNEXURE-VI";
                objMAL018.PneumaticTestreport = "ANNEXURE-VII";

                objMAL018.FinalDimensionalDetails = "AS SPECIFIED IN OUR DRAWINGS";

                objMAL018.UserName = "KEYURKUMAR PRAJAPATI";
                objMAL018.UserRole = "ASSISTANT GENERAL MANAGER";
                objMAL018.UserNotes1 = "QUALITY CONTROL";
                objMAL018.UserNotes2 = "PROCESS PLANT EQUIPMENT";

                objMAL018.Notes1 = "THE EQUIPMENT DESCRIBED ABOVE WAS INSPECTED BY US AND THE INSPECTING AUTHORITY ACCORDING TO THE APPROVED DRAWING, SPECIFICATION AND CODES AT VARIOUS STAGES OF MANUFACTURE AND FOUND SATISFACTORY.";
                objMAL018.Notes2 = "WE HEREBY CERTIFY THAT THE STATEMENTS MADE IN THIS INSPECTION CERTIFICATE ARE TRUE AND CORRECT.";
            }



            return View(objMAL018);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(MAL018 _objMAL018, int s = 0)
        {
            MAL018 objMAL018 = new MAL018();
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = _objMAL018.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                    objMAL018 = db.MAL018.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                #region Save Data
                objMAL018.InspectionCertiNo = _objMAL018.InspectionCertiNo;

                objMAL018.Date = _objMAL018.Date;
                objMAL018.ProjectNo = _objMAL018.ProjectNo;
                objMAL018.EquipmentDesc = _objMAL018.EquipmentDesc;
                objMAL018.Customer = _objMAL018.Customer;
                objMAL018.CustomerEquiNo = _objMAL018.CustomerEquiNo;
                objMAL018.Project = _objMAL018.Project;
                objMAL018.CustomerPONo = _objMAL018.CustomerPONo;
                objMAL018.CustomerPoDate = _objMAL018.CustomerPoDate;
                objMAL018.InspectionAuthority = _objMAL018.InspectionAuthority;
                objMAL018.YearOfManufacture = _objMAL018.YearOfManufacture;
                objMAL018.EquipmentDesigndTo = _objMAL018.EquipmentDesigndTo;
                objMAL018.ApplicableLTDrawings = _objMAL018.ApplicableLTDrawings;
                objMAL018.SpecificationNos = _objMAL018.SpecificationNos;
                objMAL018.CustomersDrawingSpecNos = _objMAL018.CustomersDrawingSpecNos;
                objMAL018.WeldingMaterialsUsed = _objMAL018.WeldingMaterialsUsed;
                objMAL018.MaterialOfConstruction = _objMAL018.MaterialOfConstruction;
                objMAL018.MaterialOfConstructionddl = _objMAL018.MaterialOfConstructionddl;
                objMAL018.RadiographyTestReports = _objMAL018.RadiographyTestReports;
                objMAL018.RadiographyTestReportsddl = _objMAL018.RadiographyTestReportsddl;
                objMAL018.Heattreatmentreport = _objMAL018.Heattreatmentreport;
                objMAL018.Heattreatmentreportddl = _objMAL018.Heattreatmentreportddl;
                objMAL018.ProductiontestCouponResults = _objMAL018.ProductiontestCouponResults;
                objMAL018.ProductiontestCouponResultsddl = _objMAL018.ProductiontestCouponResultsddl;
                objMAL018.HydrostatiocTestReport = _objMAL018.HydrostatiocTestReport;
                objMAL018.HydrostatiocTestReportddl = _objMAL018.HydrostatiocTestReportddl;
                objMAL018.MarkingOfEquipmentNamePlate = _objMAL018.MarkingOfEquipmentNamePlate;
                
                objMAL018.MarkingOfEquipmentNamePlateddl = _objMAL018.MarkingOfEquipmentNamePlateddl;
                objMAL018.PneumaticTestreport = _objMAL018.PneumaticTestreport;
                objMAL018.PneumaticTestreportddl = _objMAL018.PneumaticTestreportddl;
                objMAL018.FinalDimensionalDetails = _objMAL018.FinalDimensionalDetails;
                objMAL018.BalanceWorkAtSite = _objMAL018.BalanceWorkAtSite;
                objMAL018.Notes1 = _objMAL018.Notes1;
                objMAL018.Notes2 = _objMAL018.Notes2;
                objMAL018.UserName = _objMAL018.UserName;
                objMAL018.UserRole = _objMAL018.UserRole;
                objMAL018.UserNotes1 = _objMAL018.UserNotes1;
                objMAL018.UserNotes2 = _objMAL018.UserNotes2;

                if (refHeaderId == 0)
                {
                    objMAL018.CreatedBy = objClsLoginInfo.UserName;
                    objMAL018.CreatedOn = DateTime.Now;
                    db.MAL018.Add(objMAL018);
                }
                else
                {
                    objMAL018.EditedBy = objClsLoginInfo.UserName;
                    objMAL018.EditedOn = DateTime.Now;
                }

                db.SaveChanges();
                string NamePlate = clsImplementationEnum.MALProtocolType.LTCertificate.GetStringValue();

                var obj = db.MAL000.Where(a => a.HeaderId == objMAL018.HeaderId && a.InspectionActivity == NamePlate).FirstOrDefault();
                if (obj == null)
                {
                    int? srno = db.MAL000.Where(o => o.Project == objMAL018.ProjectNo && o.InspectionActivity == NamePlate).OrderByDescending(x => x.SerialNo).Select(a => a.SerialNo).FirstOrDefault();
                    MAL000 objMAL000 = new MAL000();
                    objMAL000.Project = objMAL018.ProjectNo;
                    objMAL000.InspectionActivity = NamePlate;
                    objMAL000.SerialNo = Convert.ToInt32((srno >= 0 ? srno : 0) + 1);
                    objMAL000.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();

                    objMAL000.CreatedBy = objClsLoginInfo.UserName;
                    objMAL000.CreatedOn = DateTime.Now;
                    objMAL000.HeaderId = objMAL018.HeaderId;
                    db.MAL000.Add(objMAL000);
                    db.SaveChanges();
                }
                else
                {
                    if (s == 1)
                    {
                        obj.Status = clsImplementationEnum.CommonStatus.Approved.GetStringValue();
                        obj.EditedBy = objClsLoginInfo.UserName;
                        obj.EditedOn = DateTime.Now;
                        db.SaveChanges();
                    }
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Protocol submitted successfully";
                objResponseMsg.HeaderId = objMAL018.HeaderId;
                #endregion
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
    }
}