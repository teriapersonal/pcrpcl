﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsBase;

namespace IEMQS.Areas.MAL.Controllers
{
    public class N2FillingInspectionController : clsBase
    {
        // GET: MAL/FinalActivities
        string ControllerURL = "/MAL/N2FillingInspection/";
        string Title = "REPORT FOR N2 FILLING INSPECTION";

        #region Details View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            MAL026 objMAL026 = new MAL026();
            ViewBag.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
            if (id > 0)
            {
                objMAL026 = db.MAL026.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;

                string finalActivities = clsImplementationEnum.MALProtocolType.N2FillingInspection.GetStringValue();
                var obj = db.MAL000.Where(a => a.HeaderId == objMAL026.HeaderId && a.InspectionActivity == finalActivities).FirstOrDefault();
                ViewBag.Status = obj?.Status;
                if (m == 1)
                { ViewBag.isEditable = "true"; }
                else
                { ViewBag.isEditable = "false"; }
            }
            else
            {
                ViewBag.isEditable = "true";
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<MAL027> lstMAL027 = db.MAL027.Where(x => x.HeaderId == objMAL026.HeaderId).ToList();

            ViewBag.lstMAL027 = lstMAL027;

            List<MAL028> lstMAL028 = db.MAL028.Where(x => x.HeaderId == objMAL026.HeaderId).ToList();

            ViewBag.lstMAL028 = lstMAL028;

            #endregion

            if (objMAL026 != null)
            {
                FCS001 _objfcs = db.FCS001.Where(w => w.TableId == objMAL026.HeaderId && w.TableName == "MAL026" && w.ViewerId == 1).FirstOrDefault();
                if (_objfcs != null)
                    ViewBag.FCS_ImagePath1 = _objfcs.MainDocumentPath;
                else
                    ViewBag.FCS_ImagePath1 = "";

                FCS001 _objfcs2 = db.FCS001.Where(w => w.TableId == objMAL026.HeaderId && w.TableName == "MAL026" && w.ViewerId == 2).FirstOrDefault();
                if (_objfcs2 != null)
                    ViewBag.FCS_ImagePath2 = _objfcs2.MainDocumentPath;
                else
                    ViewBag.FCS_ImagePath2 = "";
            }
            else
            {
                ViewBag.FCS_ImagePath1 = "";
                ViewBag.FCS_ImagePath2 = "";
            }

            return View(objMAL026);
        }
        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(MAL026 MAL026, int s = 0)
        {
            MAL026 objMAL026 = new MAL026();
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {

                int? refHeaderId = MAL026.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    objMAL026 = db.MAL026.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                }

                #region Save Data
                objMAL026.QualityProject = MAL026.QualityProject;
                objMAL026.Project = MAL026.Project;
                objMAL026.EquipmentNo = MAL026.EquipmentNo;
                objMAL026.DrawingNo = MAL026.DrawingNo;
                objMAL026.DCRNo = MAL026.DCRNo;
                objMAL026.DrawingRevisionNo = MAL026.DrawingRevisionNo;
                objMAL026.InspectionDate = MAL026.InspectionDate;
                objMAL026.ProtocolNo = MAL026.ProtocolNo;
                objMAL026.PhotographsOfAct1 = MAL026.PhotographsOfAct1;
                objMAL026.PhotographsOfAct2 = MAL026.PhotographsOfAct2;
                objMAL026.PressureGuageDetails = MAL026.PressureGuageDetails;
                objMAL026.CheckPoint1 = MAL026.CheckPoint1;
                objMAL026.CheckPoint2 = MAL026.CheckPoint2;
                objMAL026.CheckPoint3 = MAL026.CheckPoint3;
                objMAL026.CheckPoint4 = MAL026.CheckPoint4;
                objMAL026.CheckPoint5 = MAL026.CheckPoint5;
                objMAL026.CheckPoint6 = MAL026.CheckPoint6;
                objMAL026.QCRemarks = MAL026.QCRemarks;
                objMAL026.Location = objClsLoginInfo.Location;
                objMAL026.Result = MAL026.Result;
                //objMAL026.EditedBy = MAL026.EditedBy;
                //objMAL026.EditedOn = MAL026.EditedOn;
                //objMAL026.EditedBy = objClsLoginInfo.UserName;
                //objMAL026.EditedOn = DateTime.Now;

                if (refHeaderId == 0)
                {
                    objMAL026.CreatedBy = objClsLoginInfo.UserName;
                    objMAL026.CreatedOn = DateTime.Now;
                    db.MAL026.Add(objMAL026);
                }
                else
                {
                    objMAL026.EditedBy = objClsLoginInfo.UserName;
                    objMAL026.EditedOn = DateTime.Now;
                }

                db.SaveChanges();

                string finalActivities = clsImplementationEnum.MALProtocolType.N2FillingInspection.GetStringValue();

                var obj = db.MAL000.Where(a => a.HeaderId == objMAL026.HeaderId && a.InspectionActivity == finalActivities).FirstOrDefault();
                if (obj == null)
                {
                    int? srno = db.MAL000.Where(o => o.Project == objMAL026.Project && o.InspectionActivity == finalActivities).OrderByDescending(x => x.SerialNo).Select(a => a.SerialNo).FirstOrDefault();
                    //int? srno = db.MAL000.Where(o.InspectionActivity == finalActivities).OrderByDescending(x => x.SerialNo).Select(a => a.SerialNo).FirstOrDefault();
                    MAL000 objMAL000 = new MAL000();
                    objMAL000.Project = objMAL026.Project;
                    objMAL000.InspectionActivity = finalActivities;
                    objMAL000.SerialNo = Convert.ToInt32((srno >= 0 ? srno : 0) + 1);
                    objMAL000.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();

                    objMAL000.CreatedBy = objClsLoginInfo.UserName;
                    objMAL000.CreatedOn = DateTime.Now;
                    objMAL000.HeaderId = objMAL026.HeaderId;
                    db.MAL000.Add(objMAL000);
                    db.SaveChanges();
                }
                else
                {
                    if (s == 1)
                    {
                        obj.Status = clsImplementationEnum.CommonStatus.Approved.GetStringValue();
                        obj.EditedBy = objClsLoginInfo.UserName;
                        obj.EditedOn = DateTime.Now;
                        db.SaveChanges();
                    }
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Protocol submitted successfully";
                objResponseMsg.HeaderId = objMAL026.HeaderId;
                #endregion
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    MAL026 objMAL026 = db.MAL026.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objMAL026 != null && string.IsNullOrWhiteSpace(objMAL026.ProtocolNo))
                    {
                        db.MAL026.Remove(objMAL026);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objMAL026.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion



        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<MAL028> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {          

            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<MAL028> lstAddMAL028 = new List<MAL028>();
                List<MAL028> lstDeleteMAL028 = new List<MAL028>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        MAL028 obj = db.MAL028.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        else
                        {
                            obj = new MAL028();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        obj.SrNo = item.SrNo;
                        obj.ReqPressure = item.ReqPressure;
                        obj.ActPressure = item.ActPressure;
                        obj.ReqOxygenlevel = item.ReqOxygenlevel;
                        obj.ActOxygenlevel = item.ActOxygenlevel;
                        obj.Date = item.Date;

                        if (isAdded)
                        {
                            lstAddMAL028.Add(obj);
                        }
                    }
                    if (lstAddMAL028.Count > 0)
                    {
                        db.MAL028.AddRange(lstAddMAL028);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeleteMAL028 = db.MAL028.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeleteMAL028.Count > 0)
                    {
                        db.MAL028.RemoveRange(lstDeleteMAL028);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeleteMAL028 = db.MAL028.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeleteMAL028.Count > 0)
                    {
                        db.MAL028.RemoveRange(lstDeleteMAL028);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2(List<MAL027> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<MAL027> lstAddMAL027 = new List<MAL027>();
                List<MAL027> lstDeleteMAL027 = new List<MAL027>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        MAL027 obj = db.MAL027.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        else
                        {
                            obj = new MAL027();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        obj.GaugeSerialNumber = item.GaugeSerialNumber;
                        obj.GaugeCelebrationDueDate = item.GaugeCelebrationDueDate;
                        if (isAdded)
                        {
                            lstAddMAL027.Add(obj);
                        }
                    }
                    if (lstAddMAL027.Count > 0)
                    {
                        db.MAL027.AddRange(lstAddMAL027);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeleteMAL027 = db.MAL027.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeleteMAL027.Count > 0)
                    {
                        db.MAL027.RemoveRange(lstDeleteMAL027);
                    }
                    db.SaveChanges();

                }
                else
                {
                    lstDeleteMAL027 = db.MAL027.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeleteMAL027.Count > 0)
                    {
                        db.MAL027.RemoveRange(lstDeleteMAL027);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="PhotoNo">1 First Image | 2 Second Image</param>
        /// <param name="Headerid">record Unique Id</param>
        /// <param name="PageType">0 For Details | 1 for Linkage</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DeletePhoto(int PhotoNo, int Headerid, int PageType)
        {
            try
            {
                if (PageType == 0)
                {
                    var Files = (new clsFileUpload()).GetDocuments("MAL026/" + Headerid + "/" + PhotoNo);
                    foreach (var item in Files)
                    {
                        (new clsFileUpload()).DeleteFile("MAL026/" + Headerid + "/" + PhotoNo, item.Name);
                    }
                }
                //else
                //{
                //    var Files = (new clsFileUpload()).GetDocuments("PRL365/" + Headerid + "/" + PhotoNo);
                //    foreach (var item in Files)
                //    {
                //        (new clsFileUpload()).DeleteFile("PRL365/" + Headerid + "/" + PhotoNo, item.Name);
                //    }
                //}
                return Json(true);
            }
            catch (Exception ex)
            {
                return Json(false);
            }
        }


        [HttpPost]
        public string ProjectDetails(string ProjectNo)
        {
            if (ProjectNo != "")
            {
                NDEModels objNDEModels = new NDEModels();
                var objQproject = db.QMS010.Where(x => x.QualityProject == ProjectNo).FirstOrDefault();
                string strMCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;



                if (ProjectNo.Contains("/"))
                {
                    string[] arr = ProjectNo.Split('/');
                    ProjectNo = arr[0];
                }



                var Data = db.COM001.Where(w => w.t_cprj == ProjectNo).FirstOrDefault();
                string sValue = Data.t_cprj + "-" + Data.t_dsca;

                return (sValue + "|" + strMCode);
            }
            else
                return "";
        }

        [HttpPost]
        public ActionResult getDrgNo(string ProjectNo)
        {
            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(ProjectNo).ToList();
            lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            return Json(lstdrawing.Select(x => new
            {
                Value = x,
                Text = x
            }).ToList(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public string GetInspectionAgency(string qproject)
        {
            string Agency = string.Empty;
            string inspectionagency = Manager.GetInspectionAgency(qproject);
            if (!string.IsNullOrWhiteSpace(inspectionagency))
            {
                string[] arr = inspectionagency.Split(',').ToArray();
                Agency = string.Join("#", arr.Take(3).ToArray());
            }
            return Agency;
        }

        #endregion
        [HttpPost]
        public ActionResult SaveN2(string HelpSectionImages)
        {
            string fileSavePath = string.Empty;
            string virtualDirectoryImg = "UploadedFiles";
            string fileName = string.Empty;
            if (HelpSectionImages == "System.Web.HttpPostedFileWrapper")
            {
                if (HttpContext.Request.Files.AllKeys.Any())
                {
                    // Get the uploaded image from the Files collection
                    var httpPostedFile = HttpContext.Request.Files["HelpSectionImages"];
                    fileName = httpPostedFile.FileName;
                    string HeaderId = HttpContext.Request.Form["Headerid"];
                    string Qproject = HttpContext.Request.Form["CheckPoint6"];
                    string ImgName = HttpContext.Request.Form["ImgName"];
                    if (httpPostedFile != null)
                    {
                        // OBtient le path du fichier 
                        if (ImgName == "")
                        {
                            Qproject = Qproject.Replace('/', '_');
                            ImgName = Qproject + "-" + DateTime.Now.ToString("ddMMyyyy HHmmss") + ".png";
                        }

                        fileSavePath = Path.Combine(HttpContext.Server.MapPath("~/Areas/MAL/N2Img/"), ImgName);

                        // Sauvegarde du fichier dans UploadedFiles sur le serveur
                        httpPostedFile.SaveAs(fileSavePath);
                    }

                    return Json(ImgName);
                }
                else
                {
                    return Json("Failed");
                }
            }
            else
            {
                string ImgName = HttpContext.Request.Form["ImgName"];
                var t = HelpSectionImages.Substring(22);  // remove data:image/png;base64,



                byte[] imageBytes = Convert.FromBase64String(t);

                //Save the Byte Array as Image File.
                string filePath = Server.MapPath(ImgName);
                System.IO.File.WriteAllBytes(filePath, imageBytes);
                return Json(ImgName);
            }
        }
        [HttpPost]
        public ActionResult SaveN2Img(string HelpSectionImages)
        {
            string fileSavePath = string.Empty;
            string virtualDirectoryImg = "UploadedFiles";
            string fileName = string.Empty;
            if (HelpSectionImages == "System.Web.HttpPostedFileWrapper")
            {
                if (HttpContext.Request.Files.AllKeys.Any())
                {
                    // Get the uploaded image from the Files collection
                    var httpPostedFile = HttpContext.Request.Files["HelpSectionImages"];
                    fileName = httpPostedFile.FileName;
                    string HeaderId = HttpContext.Request.Form["Headerid"];
                    string Qproject = HttpContext.Request.Form["CheckPoint6"];
                    string ImgName = HttpContext.Request.Form["ImgName"];
                    if (httpPostedFile != null)
                    {
                        // OBtient le path du fichier 
                        if (ImgName == "")
                        {
                            Qproject = Qproject.Replace('/', '_');
                            ImgName = Qproject + "-" + DateTime.Now.ToString("ddMMyyyy HHmmss") + ".png";
                        }

                        fileSavePath = Path.Combine(HttpContext.Server.MapPath("~/Areas/MAL/N2Image/"), ImgName);

                        // Sauvegarde du fichier dans UploadedFiles sur le serveur
                        httpPostedFile.SaveAs(fileSavePath);
                    }

                    return Json(ImgName);
                }
                else
                {
                    return Json("Failed");
                }
            }
            else
            {
                string ImgName = HttpContext.Request.Form["ImgName"];
                var t = HelpSectionImages.Substring(22);  // remove data:image/png;base64,
                byte[] imageBytes = Convert.FromBase64String(t);

                //Save the Byte Array as Image File.
                string filePath = Server.MapPath(ImgName);
                System.IO.File.WriteAllBytes(filePath, imageBytes);
                return Json(ImgName);
            }
        }

        [HttpPost]
        public string SaveImageInSharePoint(string ImagePath, string imageBlob)
        {
            string ImageName = "NewImg.png";
            string Massage = "Saving image Failed";
            var Files = (new clsFileUpload()).GetDocuments(ImagePath);
            if (Files.Length > 0)
            {
                ImageName = Files.FirstOrDefault().Name;
            }
            //imageBlob = imageBlob.Replace("data:image/png;base64,", string.Empty);
            //imageBlob = imageBlob.Replace("data:image/jpg;base64,", string.Empty);
            //imageBlob = imageBlob.Replace("data:image/jpeg;base64,", string.Empty);
            int base64 = imageBlob.IndexOf("base64,");
            imageBlob = imageBlob.Remove(0, base64 + 7);
            //byte[] bytearray = Encoding.UTF8.GetBytes(imageBlob);
            var bytes = Convert.FromBase64String(imageBlob);
            MemoryStream stream = new MemoryStream(bytes);
            //var bytes = Convert.FromBase64String(imageBlob);
            //var contents = new StreamContent(new MemoryStream(bytes));
            clsFileUpload upload = new clsFileUpload();

            bool t = upload.FileUpload(ImageName, ImagePath, stream, "", "", false);
            if (t)
            {
                Massage = "Image Saved";
            }
            return Massage;
        }
    }
}