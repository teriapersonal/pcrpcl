﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsBase;

namespace IEMQS.Areas.MAL.Controllers
{
    public class HeliumleaktestController : clsBase
    {
        // GET: MAL/FinalActivities
        string ControllerURL = "/MAL/Heliumleaktest/";
        string Title = "HELIUM LEAK TEST REPORT";

        #region Details View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            MAL019 objMAL019 = new MAL019();
            ViewBag.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
            if (id > 0)
            {
                objMAL019 = db.MAL019.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;

                string finalActivities = clsImplementationEnum.MALProtocolType.Heliumleaktest.GetStringValue();
                var obj = db.MAL000.Where(a => a.HeaderId == objMAL019.HeaderId && a.InspectionActivity == finalActivities).FirstOrDefault();


                ViewBag.Status = obj?.Status;
                if (m == 1)
                { ViewBag.isEditable = "true"; }
                else
                { ViewBag.isEditable = "false"; }
            }
            else
            {
                ViewBag.isEditable = "true";
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstStageOfInspection = new List<string> { "BEFORE PWHT", "AFTER PWHT", " BEFORE HYDRO", " AFTER HYDRO", "AFTER COMPLETE WELD", "AFTER PARTIAL WELD", "AFTER ROOT RUN WELDING" };

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.StageOfInspection = lstStageOfInspection.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<MAL020> lstMAL020 = db.MAL020.Where(x => x.HeaderId == objMAL019.HeaderId).ToList();

            ViewBag.lstMAL020 = lstMAL020;


            #endregion
            return View(objMAL019);
        }
        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(MAL019 MAL019, int s = 0)
        {
            MAL019 objMAL019 = new MAL019();
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {

                int? refHeaderId = MAL019.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    objMAL019 = db.MAL019.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                }

                #region Save Data

                objMAL019.Project = MAL019.Project;
                //objMAL019.BU = MAL019.BU;
                objMAL019.Location = objClsLoginInfo.Location;
                objMAL019.QualityProject = MAL019.QualityProject;
                objMAL019.EquipmentNo = MAL019.EquipmentNo;
                objMAL019.DrawingNo = MAL019.DrawingNo;
                objMAL019.DrawingRevisionNo = MAL019.DrawingRevisionNo;
                objMAL019.DCRNo = MAL019.DCRNo;
                objMAL019.InspectionDate = MAL019.InspectionDate;
                objMAL019.Project = MAL019.Project;
                objMAL019.ProtocolNo = MAL019.ProtocolNo;
                objMAL019.ProcedureNo = MAL019.ProcedureNo;
                objMAL019.TestDescription = MAL019.TestDescription;
                objMAL019.DateOfTest = MAL019.DateOfTest;
                objMAL019.StageOfInspection = MAL019.StageOfInspection;
                objMAL019.TestTechnique = MAL019.TestTechnique;
                objMAL019.NameOfHeliumLeakDetectorMachine = MAL019.NameOfHeliumLeakDetectorMachine;
                objMAL019.ModelNo = MAL019.ModelNo;
                objMAL019.ReqTestPressure = MAL019.ReqTestPressure;
                objMAL019.ActTestPressure = MAL019.ActTestPressure;
                objMAL019.ReqHoldingTime = MAL019.ReqHoldingTime;
                objMAL019.ActHoldingTime = MAL019.ActHoldingTime;
                objMAL019.ReqTestTemperature = MAL019.ReqTestTemperature;
                objMAL019.ActTestTemperature = MAL019.ActTestTemperature;
                objMAL019.LeakRateMeasuredOnScaleOfHeliumMachine = MAL019.LeakRateMeasuredOnScaleOfHeliumMachine;
                objMAL019.StandardLeakSerialNumber = MAL019.StandardLeakSerialNumber;
                objMAL019.CelebrationDueDate = MAL019.CelebrationDueDate;
                objMAL019.LeakRateOfStandardLeak = MAL019.LeakRateOfStandardLeak;
                objMAL019.BaselineReadingAfterConnectingSnifferProbe = MAL019.BaselineReadingAfterConnectingSnifferProbe;
                objMAL019.AcceptanceCriteria = MAL019.AcceptanceCriteria;
                objMAL019.LeakRateObserved = MAL019.LeakRateObserved;
                objMAL019.AnyDeflectionOnIndicatorOfHeliumLeakDetector = MAL019.AnyDeflectionOnIndicatorOfHeliumLeakDetector;
                objMAL019.ObservationAndRemarks = MAL019.ObservationAndRemarks;
                objMAL019.Result = MAL019.Result;
                //objMAL019.EditedBy = MAL019.EditedBy;
                //objMAL019.EditedOn = MAL019.EditedOn;
                //objMAL019.EditedBy = objClsLoginInfo.UserName;
                //objMAL019.EditedOn = DateTime.Now;

                if (refHeaderId == 0)
                {
                    objMAL019.CreatedBy = objClsLoginInfo.UserName;
                    objMAL019.CreatedOn = DateTime.Now;
                    db.MAL019.Add(objMAL019);
                }
                else
                {
                    objMAL019.EditedBy = objClsLoginInfo.UserName;
                    objMAL019.EditedOn = DateTime.Now;
                }

                db.SaveChanges();

                string finalActivities = clsImplementationEnum.MALProtocolType.Heliumleaktest.GetStringValue();

                var obj = db.MAL000.Where(a => a.HeaderId == objMAL019.HeaderId && a.InspectionActivity == finalActivities).FirstOrDefault();
                if (obj == null)
                {
                    int? srno = db.MAL000.Where(o=> o.InspectionActivity == finalActivities).OrderByDescending(x => x.SerialNo).Select(a => a.SerialNo).FirstOrDefault();
                    MAL000 objMAL000 = new MAL000();
                    objMAL000.Project = objMAL019.Project;
                    objMAL000.InspectionActivity = finalActivities;
                    objMAL000.SerialNo = Convert.ToInt32((srno >= 0 ? srno : 0) + 1);
                    objMAL000.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();

                    objMAL000.CreatedBy = objClsLoginInfo.UserName;
                    objMAL000.CreatedOn = DateTime.Now;
                    objMAL000.HeaderId = objMAL019.HeaderId;
                    db.MAL000.Add(objMAL000);
                    db.SaveChanges();
                }
                else
                {
                    if (s == 1)
                    {
                        obj.Status = clsImplementationEnum.CommonStatus.Approved.GetStringValue();
                        obj.EditedBy = objClsLoginInfo.UserName;
                        obj.EditedOn = DateTime.Now;
                        db.SaveChanges();
                    }
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Protocol submitted successfully";
                objResponseMsg.HeaderId = objMAL019.HeaderId;
                #endregion
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);




            //objResponseMsg.Key = true;
            //        objResponseMsg.Value = "Protocol submitted successfully";
            //        objResponseMsg.HeaderId = objMAL019.HeaderId;
            //                #endregion

            //        else
            //        {
            //            objResponseMsg.Key = false;
            //            // objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
            //        }

            //    }
            //    catch (Exception ex)
            //    {
            //        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            //        objResponseMsg.Key = false;
            //        objResponseMsg.Value = ex.Message;
            //    }
            //    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    MAL019 objMAL019 = db.MAL019.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objMAL019 != null && string.IsNullOrWhiteSpace(objMAL019.ProtocolNo))
                    {
                        db.MAL019.Remove(objMAL019);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objMAL019.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion



        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<MAL020> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<MAL020> lstAddMAL020 = new List<MAL020>();
                List<MAL020> lstDeleteMAL020 = new List<MAL020>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        MAL020 obj = db.MAL020.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        else
                        {
                            obj = new MAL020();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }

                        obj.GaugeSerialNumber = item.GaugeSerialNumber;
                        obj.GaugeCelebrationDueDate = item.GaugeCelebrationDueDate;
                        if (isAdded)
                        {
                            lstAddMAL020.Add(obj);
                        }
                    }
                    if (lstAddMAL020.Count > 0)
                    {
                        db.MAL020.AddRange(lstAddMAL020);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeleteMAL020 = db.MAL020.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeleteMAL020.Count > 0)
                    {
                        db.MAL020.RemoveRange(lstDeleteMAL020);
                    }
                    db.SaveChanges();

                }
                else
                {
                    lstDeleteMAL020 = db.MAL020.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeleteMAL020.Count > 0)
                    {
                        db.MAL020.RemoveRange(lstDeleteMAL020);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);


        }
        #endregion





        [HttpPost]
        public string ProjectDetails(string ProjectNo)
        {
            if (ProjectNo != "")
            {
                NDEModels objNDEModels = new NDEModels();
                var objQproject = db.QMS010.Where(x => x.QualityProject == ProjectNo).FirstOrDefault();
                string strMCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;

                ProjectNo = objQproject.Project;
                var Data = db.COM001.Where(w => w.t_cprj == ProjectNo).FirstOrDefault();
                string sValue = Data.t_cprj + "-" + Data.t_dsca;

                return (Data.t_cprj + "|" + sValue + "|" + strMCode);
            }
            else
                return "";
        }

        [HttpPost]
        public ActionResult getDrgNo(string term, string ProjectNo)
        {
            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(ProjectNo).ToList();
            var lst = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            return Json(lstdrawing.Select(x => new
            {
                Value = x,
                Text = x
            }).ToList(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetInspectionAgency(string qproject)
        {
            string Agency = string.Empty;
            List<string> tpi = new List<string>();
            string inspectionagency = Manager.GetInspectionAgency(qproject);
            if (!string.IsNullOrWhiteSpace(inspectionagency))
            {
                string[] arr = inspectionagency.Split(',').ToArray();
                Agency = string.Join("#", arr.Take(3).ToArray());

                for (int i = 0; i < arr.Length; i++)
                {
                    string code = arr[i];
                    var Description = (from glb002 in db.GLB002
                                       join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                                       where glb001.Category.Equals("Inspection Agency", StringComparison.OrdinalIgnoreCase) && glb002.Code.Equals(code) //&& glb002.IsActive == true
                                       select glb002.Description
                                     ).FirstOrDefault();
                    tpi.Add((string.IsNullOrWhiteSpace(Description)) ? arr[i] : Description);
                }
            }
            var obj = new
            {
                Agency = Agency,
                arr = string.Join(",", tpi)
            };
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}