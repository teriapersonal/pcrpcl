﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.MAL.Controllers
{
    public class PicklingPassivationController : clsBase
    {
        // GET: MAL/PicklingPassivation
        string ControllerURL = "/MAL/PicklingPassivation/";
        string Title = "PICKLING AND PASSIVATION REPORT";

        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;
            ViewBag._lstMAL016_2 = null;
            ViewBag.DrawingNo = null;
            ViewBag.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
            ViewBag.Location = objClsLoginInfo.Location;
            MAL016 objMAL016 = new MAL016();
            if (id > 0)
            {
                objMAL016 = db.MAL016.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;

                ViewBag._lstMAL016_2 = db.MAL016_2.Where(x => x.HeaderId == objMAL016.HeaderId).ToList();
                ViewBag._lstMAL016_3 = db.MAL016_3.Where(w => w.HeaderId == objMAL016.HeaderId).ToList();

                string PicklingPassivation = clsImplementationEnum.MALProtocolType.PicklingPassivation.GetStringValue();
                var obj = db.MAL000.Where(a => a.HeaderId == objMAL016.HeaderId && a.InspectionActivity == PicklingPassivation).FirstOrDefault();
                ViewBag.Status = obj?.Status;

                if (m == 1)
                { ViewBag.isEditable = "true"; }
                else
                { ViewBag.isEditable = "false"; }
            }
            else
            {
                ViewBag.isEditable = "true";
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;
            ViewBag.PrintedBy = objClsLoginInfo.UserName;

            return View(objMAL016);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(MAL016 _objMAL016, int s = 0)
        {
            MAL016 objMAL016 = new MAL016();
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = _objMAL016.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                    objMAL016 = db.MAL016.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                #region Save Data
                objMAL016.ProjectNo = _objMAL016.ProjectNo;
                objMAL016.Date = _objMAL016.Date;
                objMAL016.EquipmentNameNumber = _objMAL016.EquipmentNameNumber;
                objMAL016.InspectionAgency = _objMAL016.InspectionAgency;
                objMAL016.ManufacturingCode = _objMAL016.ManufacturingCode;
                objMAL016.Client = _objMAL016.Client;
                objMAL016.PoNo = _objMAL016.PoNo;
                objMAL016.Stage = _objMAL016.Stage;

                if (refHeaderId == 0)
                {
                    objMAL016.CreatedBy = objClsLoginInfo.UserName;
                    objMAL016.CreatedOn = DateTime.Now;
                    db.MAL016.Add(objMAL016);
                }
                else
                {
                    objMAL016.EditedBy = objClsLoginInfo.UserName;
                    objMAL016.EditedOn = DateTime.Now;
                }

                db.SaveChanges();
                string PicklingPassivation = clsImplementationEnum.MALProtocolType.PicklingPassivation.GetStringValue();

                var obj = db.MAL000.Where(a => a.HeaderId == objMAL016.HeaderId && a.InspectionActivity == PicklingPassivation).FirstOrDefault();
                if (obj == null)
                {
                    int? srno = db.MAL000.Where(o => o.Project == objMAL016.ProjectNo && o.InspectionActivity == PicklingPassivation).OrderByDescending(x => x.SerialNo).Select(a => a.SerialNo).FirstOrDefault();
                    MAL000 objMAL000 = new MAL000();
                    objMAL000.Project = objMAL016.ProjectNo;
                    objMAL000.InspectionActivity = PicklingPassivation;
                    objMAL000.SerialNo = Convert.ToInt32((srno >= 0 ? srno : 0) + 1);
                    objMAL000.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();

                    objMAL000.CreatedBy = objClsLoginInfo.UserName;
                    objMAL000.CreatedOn = DateTime.Now;
                    objMAL000.HeaderId = objMAL016.HeaderId;
                    db.MAL000.Add(objMAL000);
                    db.SaveChanges();
                }
                else
                {
                    if (s == 1)
                    {
                        obj.Status = clsImplementationEnum.CommonStatus.Approved.GetStringValue();
                        obj.EditedBy = objClsLoginInfo.UserName;
                        obj.EditedOn = DateTime.Now;
                        db.SaveChanges();
                    }
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Protocol submitted successfully";
                objResponseMsg.HeaderId = objMAL016.HeaderId;
                #endregion
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveProtocolLine1(List<MAL016_2> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<MAL016_2> lstAddMAL016_2 = new List<MAL016_2>();
                List<MAL016_2> lstDeleteMAL016_2 = new List<MAL016_2>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            MAL016_2 obj = db.MAL016_2.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.HeaderId = item.HeaderId;
                                obj.DrgNo = item.DrgNo;
                                obj.RevNo = item.RevNo;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            MAL016_2 obj = new MAL016_2();
                            if (!string.IsNullOrWhiteSpace(item.DrgNo))
                            {
                                obj.HeaderId = item.HeaderId;
                                obj.DrgNo = item.DrgNo;
                                obj.RevNo = item.RevNo;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddMAL016_2.Add(obj);
                            }
                        }
                    }
                    if (lstAddMAL016_2.Count > 0)
                    {
                        db.MAL016_2.AddRange(lstAddMAL016_2);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeleteMAL016_2 = db.MAL016_2.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeleteMAL016_2.Count > 0)
                    {
                        db.MAL016_2.RemoveRange(lstDeleteMAL016_2);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeleteMAL016_2 = db.MAL016_2.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeleteMAL016_2.Count > 0)
                    {
                        db.MAL016_2.RemoveRange(lstDeleteMAL016_2);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveProtocolLine2(List<MAL016_3> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<MAL016_3> lstAddMAL016_3 = new List<MAL016_3>();
                List<MAL016_3> lstDeleteMAL016_3 = new List<MAL016_3>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        MAL016_3 obj = db.MAL016_3.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.ActualValue = item.ActualValue;
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(item.ActualValue))
                            {
                                obj = new MAL016_3();
                                obj.ActualValue = item.ActualValue;
                                obj.HeaderId = item.HeaderId;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddMAL016_3.Add(obj);
                            }
                        }
                    }
                    if (lstAddMAL016_3.Count > 0)
                    {
                        db.MAL016_3.AddRange(lstAddMAL016_3);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeleteMAL016_3 = db.MAL016_3.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeleteMAL016_3.Count > 0)
                    {
                        db.MAL016_3.RemoveRange(lstDeleteMAL016_3);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeleteMAL016_3 = db.MAL016_3.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeleteMAL016_3.Count > 0)
                    {
                        db.MAL016_3.RemoveRange(lstDeleteMAL016_3);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public string ProjectDetails(string ProjectNo)
        {
            if (ProjectNo != "")
            {
                NDEModels objNDEModels = new NDEModels();
                var objQproject = db.QMS010.Where(x => x.QualityProject == ProjectNo).FirstOrDefault();
                string strMCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;

                if (ProjectNo.Contains("/"))
                {
                    string[] arr = ProjectNo.Split('/');
                    ProjectNo = arr[0];
                }

                var Data = db.COM001.Where(w => w.t_cprj == ProjectNo).FirstOrDefault();
                string sValue = Data.t_cprj + "-" + Data.t_dsca;

                return (sValue + "|" + strMCode);
            }
            else
                return "";
        }

        [HttpPost]
        public ActionResult getDrgNo(string ProjectNo)
        {
            //ProjectNo = "S040166";
            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(ProjectNo).ToList();
            lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            return Json(lstdrawing.Select(x => new
            {
                Value = x,
                Text = x
            }).ToList(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeletePhoto(int PhotoNo, int Headerid, string strTableName)
        {
            try
            {
                var Files = (new clsFileUpload()).GetDocuments(strTableName + "/" + Headerid + "/" + PhotoNo);
                foreach (var item in Files)
                {
                    (new clsFileUpload()).DeleteFile(strTableName + "/" + Headerid + "/" + PhotoNo, item.Name);
                }

                return Json(true);
            }
            catch
            {
                return Json(false);
            }
        }


        [HttpPost]
        //public string GetInspectionAgency(string qproject)
        //{
        //    string Agency = string.Empty;
        //    string inspectionagency = Manager.GetInspectionAgency(qproject);
        //    if (!string.IsNullOrWhiteSpace(inspectionagency))
        //    {
        //        string[] arr = inspectionagency.Split(',').ToArray();
        //        Agency = string.Join("#", arr.Take(3).ToArray());
        //    }
        //    return Agency;
        //}

        public ActionResult GetInspectionAgency(string qproject)
        {
            string Agency = string.Empty;
            List<string> tpi = new List<string>();
            string inspectionagency = Manager.GetInspectionAgency(qproject);
            if (!string.IsNullOrWhiteSpace(inspectionagency))
            {
                string[] arr = inspectionagency.Split(',').ToArray();
                Agency = string.Join("#", arr.Take(3).ToArray());

                for (int i = 0; i < arr.Length; i++)
                {
                    string code = arr[i];
                    var Description = (from glb002 in db.GLB002
                                       join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                                       where glb001.Category.Equals("Inspection Agency", StringComparison.OrdinalIgnoreCase) && glb002.Code.Equals(code) //&& glb002.IsActive == true
                                       select glb002.Description
                                     ).FirstOrDefault();
                    tpi.Add((string.IsNullOrWhiteSpace(Description)) ? arr[i] : Description);
                }
            }
            var obj = new
            {
                Agency = Agency,
                arr = string.Join(",", tpi)
            };
            return Json(obj, JsonRequestBehavior.AllowGet);
        }
    }
}