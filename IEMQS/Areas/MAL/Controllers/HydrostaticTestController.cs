﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsBase;

namespace IEMQS.Areas.MAL.Controllers
{
    public class HydrostaticTestController : clsBase
    {
        // GET: MAL/FinalActivities
        string ControllerURL = "/MAL/HydrostaticTest/";
        string Title = "HYDROSTATIC TEST REPORT";

        #region Details View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            MAL021 objMAL021 = new MAL021();
            MAL023 objMAL023 = new MAL023();
            ViewBag.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
            if (id > 0)
            {
                objMAL021 = db.MAL021.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;

                string finalActivities = clsImplementationEnum.MALProtocolType.HydrostaticTest.GetStringValue();
                var obj = db.MAL000.Where(a => a.HeaderId == objMAL021.HeaderId && a.InspectionActivity == finalActivities).FirstOrDefault();
                ViewBag.Status = obj?.Status;
                if (m == 1)
                { ViewBag.isEditable = "true"; }
                else
                { ViewBag.isEditable = "false"; }
            }
            else
            {
                try
                {
                    objMAL021.ProtocolNo = string.Empty;
                    objMAL021.CreatedBy = objClsLoginInfo.UserName;
                    objMAL021.CreatedOn = DateTime.Now;

                    #region
                    objMAL021.QCRemarks = "NO PRESSURE DROP AND NO LEAKAGE OBSERVED.";

                    db.MAL021.Add(objMAL021);
                    db.SaveChanges();
                    #endregion

                    #region  LOCATION | MAL023
                    List<MAL023> lst = new List<MAL023>();

                    lst.Add(new MAL023
                    {
                        HeaderId = objMAL021.HeaderId,
                        Location = "TOP",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    lst.Add(new MAL023
                    {
                        HeaderId = objMAL021.HeaderId,
                        Location = "MIDDLE",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    lst.Add(new MAL023
                    {
                        HeaderId = objMAL021.HeaderId,
                        Location = "BOTTOM",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    db.MAL023.AddRange(lst);
                    db.SaveChanges();
                    #endregion

                    //db.MAL021.Add(objMAL021);
                    //db.SaveChanges();
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objMAL021.HeaderId;
                ViewBag.isEditable = "true";
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstyesnona = clsImplementationEnum.getYesNoNA().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNoNAEnum = lstyesnona.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<MAL022> lstMAL022 = db.MAL022.Where(x => x.HeaderId == objMAL021.HeaderId).ToList();
            List<MAL023> lstMAL023 = db.MAL023.Where(x => x.HeaderId == objMAL021.HeaderId).ToList();

            ViewBag.lstMAL022 = lstMAL022;
            ViewBag.lstMAL023 = lstMAL023;

            #endregion

            return View(objMAL021);
        }
        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(MAL021 MAL021, int s = 0)
        {
            MAL021 objMAL021 = new MAL021();
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {

                int? refHeaderId = MAL021.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    objMAL021 = db.MAL021.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                }

                #region Save Data
                objMAL021.Location = objClsLoginInfo.Location;
                objMAL021.QualityProject = MAL021.QualityProject;
                objMAL021.Project = MAL021.Project;
                objMAL021.EquipmentNo = MAL021.EquipmentNo;
                objMAL021.DrawingNo = MAL021.DrawingNo;
                objMAL021.DrawingRevisionNo = MAL021.DrawingRevisionNo;
                objMAL021.DCRNo = MAL021.DCRNo;
                objMAL021.InspectionDate = MAL021.InspectionDate;
                objMAL021.ProtocolNo = MAL021.ProtocolNo;
                objMAL021.ProcedureNo = MAL021.ProcedureNo;
                objMAL021.TestDescription = MAL021.TestDescription;
                objMAL021.DateOfTest = MAL021.DateOfTest;
                objMAL021.TestPosition = MAL021.TestPosition;
                objMAL021.TestMedium = MAL021.TestMedium;
                objMAL021.ReqChlorideContentOfWater = MAL021.ReqChlorideContentOfWater;
                objMAL021.ActChlorideContentOfWater = MAL021.ActChlorideContentOfWater;
                objMAL021.ReqTestPressure = MAL021.ReqTestPressure;
                objMAL021.ActTestPressure = MAL021.ActTestPressure;
                objMAL021.ReqHoldingTime = MAL021.ReqHoldingTime;
                objMAL021.ActHoldingTime = MAL021.ActHoldingTime;
                objMAL021.ReqTestTemprature = MAL021.ReqTestTemprature;
                objMAL021.ActTestTemprature = MAL021.ActTestTemprature;
                objMAL021.OutesideCircumferenceInMM = MAL021.OutesideCircumferenceInMM;
                objMAL021.Graph = MAL021.Graph;
                objMAL021.QCRemarks = MAL021.QCRemarks;
                objMAL021.Result = MAL021.Result;
                //objMAL021.EditedBy = MAL021.EditedBy;
                //objMAL021.EditedOn = MAL021.EditedOn;
                //objMAL021.EditedBy = objClsLoginInfo.UserName;
                //objMAL021.EditedOn = DateTime.Now;

                if (refHeaderId == 0)
                {
                    objMAL021.CreatedBy = objClsLoginInfo.UserName;
                    objMAL021.CreatedOn = DateTime.Now;
                    db.MAL021.Add(objMAL021);
                }
                else
                {
                    objMAL021.EditedBy = objClsLoginInfo.UserName;
                    objMAL021.EditedOn = DateTime.Now;
                }

                db.SaveChanges();

                string finalActivities = clsImplementationEnum.MALProtocolType.HydrostaticTest.GetStringValue();

                var obj = db.MAL000.Where(a => a.HeaderId == objMAL021.HeaderId && a.InspectionActivity == finalActivities).FirstOrDefault();
                if (obj == null)
                {
                    int? srno = db.MAL000.Where(o=> o.InspectionActivity == finalActivities).OrderByDescending(x => x.SerialNo).Select(a => a.SerialNo).FirstOrDefault();
                    MAL000 objMAL000 = new MAL000();
                    objMAL000.Project = objMAL021.Project;
                    objMAL000.InspectionActivity = finalActivities;
                    objMAL000.SerialNo = Convert.ToInt32((srno >= 0 ? srno : 0) + 1);
                    objMAL000.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();

                    objMAL000.CreatedBy = objClsLoginInfo.UserName;
                    objMAL000.CreatedOn = DateTime.Now;
                    objMAL000.HeaderId = objMAL021.HeaderId;
                    db.MAL000.Add(objMAL000);
                    db.SaveChanges();
                }
                else
                {
                    if (s == 1)
                    {
                        obj.Status = clsImplementationEnum.CommonStatus.Approved.GetStringValue();
                        obj.EditedBy = objClsLoginInfo.UserName;
                        obj.EditedOn = DateTime.Now;
                        db.SaveChanges();
                    }
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Protocol submitted successfully";
                objResponseMsg.HeaderId = objMAL021.HeaderId;
                #endregion
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    MAL021 objMAL021 = db.MAL021.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objMAL021 != null && string.IsNullOrWhiteSpace(objMAL021.ProtocolNo))
                    {
                        db.MAL021.Remove(objMAL021);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objMAL021.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion



        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<MAL022> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<MAL022> lstAddMAL022 = new List<MAL022>();
                List<MAL022> lstDeleteMAL022 = new List<MAL022>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        MAL022 obj = db.MAL022.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        else
                        {
                            obj = new MAL022();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        obj.GaugeSerialNumber = item.GaugeSerialNumber;
                        obj.GaugeCelebrationDueDate = item.GaugeCelebrationDueDate;
                        if (isAdded)
                        {
                            lstAddMAL022.Add(obj);
                        }
                    }
                    if (lstAddMAL022.Count > 0)
                    {
                        db.MAL022.AddRange(lstAddMAL022);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeleteMAL022 = db.MAL022.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeleteMAL022.Count > 0)
                    {
                        db.MAL022.RemoveRange(lstDeleteMAL022);
                    }
                    db.SaveChanges();

                }
                else
                {
                    lstDeleteMAL022 = db.MAL022.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeleteMAL022.Count > 0)
                    {
                        db.MAL022.RemoveRange(lstDeleteMAL022);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);


        }
        #endregion
        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2(List<MAL023> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<MAL023> lstAddMAL023 = new List<MAL023>();
                List<MAL023> lstDeleteMAL023 = new List<MAL023>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        MAL023 obj = db.MAL023.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        else
                        {
                            obj = new MAL023();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        obj.Location = item.Location;
                        obj.BeforeHydroTest = item.BeforeHydroTest;
                        obj.DuringHydroTest = item.DuringHydroTest;
                        obj.AfterHydroTest = item.AfterHydroTest;

                        if (isAdded)
                        {
                            lstAddMAL023.Add(obj);
                        }
                    }
                    if (lstAddMAL023.Count > 0)
                    {
                        db.MAL023.AddRange(lstAddMAL023);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeleteMAL023 = db.MAL023.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeleteMAL023.Count > 0)
                    {
                        db.MAL023.RemoveRange(lstDeleteMAL023);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeleteMAL023 = db.MAL023.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeleteMAL023.Count > 0)
                    {
                        db.MAL023.RemoveRange(lstDeleteMAL023);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion




        [HttpPost]
        public string ProjectDetails(string ProjectNo)
        {
            if (ProjectNo != "")
            {
                NDEModels objNDEModels = new NDEModels();
                var objQproject = db.QMS010.Where(x => x.QualityProject == ProjectNo).FirstOrDefault();
                string strMCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;



                if (ProjectNo.Contains("/"))
                {
                    string[] arr = ProjectNo.Split('/');
                    ProjectNo = arr[0];
                }



                var Data = db.COM001.Where(w => w.t_cprj == ProjectNo).FirstOrDefault();
                string sValue = Data.t_cprj + "-" + Data.t_dsca;

                return (sValue + "|" + strMCode);
            }
            else
                return "";
        }

        [HttpPost]
        public ActionResult getDrgNo(string ProjectNo)
        {
            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(ProjectNo).ToList();
            lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            return Json(lstdrawing.Select(x => new
            {
                Value = x,
                Text = x
            }).ToList(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public string GetInspectionAgency(string qproject)
        {
            string Agency = string.Empty;
            string inspectionagency = Manager.GetInspectionAgency(qproject);
            if (!string.IsNullOrWhiteSpace(inspectionagency))
            {
                string[] arr = inspectionagency.Split(',').ToArray();
                Agency = string.Join("#", arr.Take(3).ToArray());
            }
            return Agency;
        }

        #endregion
    }
}