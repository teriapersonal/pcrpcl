﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;


namespace IEMQS.Areas.MAL.Controllers
{
    public class NamePlateController : clsBase
    {
        // GET: MAL/NamePlate
        string ControllerURL = "/MAL/NamePlate/";
        string Title = "REPORT FOR NAME PLATE FIXING";

        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;
            ViewBag._lstMAL014_2 = null;
            ViewBag.DrawingNo = null;
            ViewBag.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
            ViewBag.Location = objClsLoginInfo.Location;
            MAL014 objMAL014 = new MAL014();
            if (id > 0)
            {
                objMAL014 = db.MAL014.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;

                ViewBag._lstMAL014_2 = db.MAL014_2.Where(x => x.HeaderId == objMAL014.HeaderId).ToList();

                string NamePlate = clsImplementationEnum.MALProtocolType.NamePlate.GetStringValue();
                var obj = db.MAL000.Where(a => a.HeaderId == objMAL014.HeaderId && a.InspectionActivity == NamePlate).FirstOrDefault();
                ViewBag.Status = obj?.Status;

                if (objMAL014 != null)
                {
                    FCS001 _objfcs = db.FCS001.Where(w => w.TableId == objMAL014.HeaderId && w.TableName == "MAL014" && w.ViewerId == 1).FirstOrDefault();
                    if (_objfcs != null)
                        ViewBag.FCS_ImagePath = _objfcs.MainDocumentPath;
                    else
                        ViewBag.FCS_ImagePath = "";
                }
                else
                    ViewBag.FCS_ImagePath = "";


                if (m == 1)
                { ViewBag.isEditable = "true"; }
                else
                { ViewBag.isEditable = "false"; }
            }
            else
            {
                ViewBag.isEditable = "true";
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;
            ViewBag.PrintedBy = objClsLoginInfo.UserName;

            return View(objMAL014);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(MAL014 _objMAL014, int s = 0)
        {
            MAL014 objMAL014 = new MAL014();
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = _objMAL014.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                    objMAL014 = db.MAL014.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                #region Save Data
                objMAL014.ProjectNo = _objMAL014.ProjectNo;
                objMAL014.Date = _objMAL014.Date;
                objMAL014.EquipmentNameAndNo = _objMAL014.EquipmentNameAndNo;
                objMAL014.InspectionAgency = _objMAL014.InspectionAgency;
                objMAL014.ManufacturingCode = _objMAL014.ManufacturingCode;
                objMAL014.Client = _objMAL014.Client;
                objMAL014.PoNo = _objMAL014.PoNo;
                objMAL014.PhotoHeaderDetails = _objMAL014.PhotoHeaderDetails;

                if (refHeaderId == 0)
                {
                    objMAL014.CreatedBy = objClsLoginInfo.UserName;
                    objMAL014.CreatedOn = DateTime.Now;
                    db.MAL014.Add(objMAL014);
                }
                else
                {
                    objMAL014.EditedBy = objClsLoginInfo.UserName;
                    objMAL014.EditedOn = DateTime.Now;
                }

                db.SaveChanges();
                string NamePlate = clsImplementationEnum.MALProtocolType.NamePlate.GetStringValue();

                var obj = db.MAL000.Where(a => a.HeaderId == objMAL014.HeaderId && a.InspectionActivity == NamePlate).FirstOrDefault();
                if (obj == null)
                {
                    int? srno = db.MAL000.Where(o => o.Project == objMAL014.ProjectNo && o.InspectionActivity == NamePlate).OrderByDescending(x => x.SerialNo).Select(a => a.SerialNo).FirstOrDefault();
                    MAL000 objMAL000 = new MAL000();
                    objMAL000.Project = objMAL014.ProjectNo;
                    objMAL000.InspectionActivity = NamePlate;
                    objMAL000.SerialNo = Convert.ToInt32((srno >= 0 ? srno : 0) + 1);
                    objMAL000.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();

                    objMAL000.CreatedBy = objClsLoginInfo.UserName;
                    objMAL000.CreatedOn = DateTime.Now;
                    objMAL000.HeaderId = objMAL014.HeaderId;
                    db.MAL000.Add(objMAL000);
                    db.SaveChanges();
                }
                else
                {
                    if (s == 1)
                    {
                        obj.Status = clsImplementationEnum.CommonStatus.Approved.GetStringValue();
                        obj.EditedBy = objClsLoginInfo.UserName;
                        obj.EditedOn = DateTime.Now;
                        db.SaveChanges();
                    }
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Protocol submitted successfully";
                objResponseMsg.HeaderId = objMAL014.HeaderId;
                #endregion
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveProtocolLine1(List<MAL014_2> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<MAL014_2> lstAddMAL014_2 = new List<MAL014_2>();
                List<MAL014_2> lstDeleteMAL014_2 = new List<MAL014_2>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            MAL014_2 obj = db.MAL014_2.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.HeaderId = item.HeaderId;
                                obj.DrgNo = item.DrgNo;
                                obj.RevNo = item.RevNo;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            MAL014_2 obj = new MAL014_2();
                            if (!string.IsNullOrWhiteSpace(item.DrgNo))
                            {
                                obj.HeaderId = item.HeaderId;
                                obj.DrgNo = item.DrgNo;
                                obj.RevNo = item.RevNo;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddMAL014_2.Add(obj);
                            }
                        }
                    }
                    if (lstAddMAL014_2.Count > 0)
                    {
                        db.MAL014_2.AddRange(lstAddMAL014_2);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeleteMAL014_2 = db.MAL014_2.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeleteMAL014_2.Count > 0)
                    {
                        db.MAL014_2.RemoveRange(lstDeleteMAL014_2);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeleteMAL014_2 = db.MAL014_2.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeleteMAL014_2.Count > 0)
                    {
                        db.MAL014_2.RemoveRange(lstDeleteMAL014_2);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public string ProjectDetails(string ProjectNo)
        {
            if (ProjectNo != "")
            {
                NDEModels objNDEModels = new NDEModels();
                var objQproject = db.QMS010.Where(x => x.QualityProject == ProjectNo).FirstOrDefault();
                string strMCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;

                if (ProjectNo.Contains("/"))
                {
                    string[] arr = ProjectNo.Split('/');
                    ProjectNo = arr[0];
                }

                var Data = db.COM001.Where(w => w.t_cprj == ProjectNo).FirstOrDefault();
                string sValue = Data.t_cprj + "-" + Data.t_dsca;

                return (sValue + "|" + strMCode);
            }
            else
                return "";
        }

        [HttpPost]
        public ActionResult getDrgNo(string ProjectNo)
        {
           // ProjectNo = "S040166";
            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(ProjectNo).ToList();
            lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            return Json(lstdrawing.Select(x => new
            {
                Value = x,
                Text = x
            }).ToList(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeletePhoto(int PhotoNo, int Headerid, string strTableName)
        {
            try
            {
                var Files = (new clsFileUpload()).GetDocuments(strTableName + "/" + Headerid + "/" + PhotoNo);
                foreach (var item in Files)
                {
                    (new clsFileUpload()).DeleteFile(strTableName + "/" + Headerid + "/" + PhotoNo, item.Name);
                }

                return Json(true);
            }
            catch
            {
                return Json(false);
            }
        }

        [HttpPost]
        //public string GetInspectionAgency(string qproject)
        //{
        //    string Agency = string.Empty;
        //    string inspectionagency = Manager.GetInspectionAgency(qproject);
        //    if (!string.IsNullOrWhiteSpace(inspectionagency))
        //    {
        //        string[] arr = inspectionagency.Split(',').ToArray();
        //        Agency = string.Join("#", arr.Take(3).ToArray());
        //    }
        //    return Agency;
        //}

        public ActionResult GetInspectionAgency(string qproject)
        {
            string Agency = string.Empty;
            List<string> tpi = new List<string>();
            string inspectionagency = Manager.GetInspectionAgency(qproject);
            if (!string.IsNullOrWhiteSpace(inspectionagency))
            {
                string[] arr = inspectionagency.Split(',').ToArray();
                Agency = string.Join("#", arr.Take(3).ToArray());

                for (int i = 0; i < arr.Length; i++)
                {
                    string code = arr[i];
                    var Description = (from glb002 in db.GLB002
                                       join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                                       where glb001.Category.Equals("Inspection Agency", StringComparison.OrdinalIgnoreCase) && glb002.Code.Equals(code) //&& glb002.IsActive == true
                                       select glb002.Description
                                     ).FirstOrDefault();
                    tpi.Add((string.IsNullOrWhiteSpace(Description)) ? arr[i] : Description);
                }
            }

            var obj = new
            {
                Agency = Agency,
                arr = string.Join(",", tpi)
            };
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveNamePlate(string HelpSectionImages)
        {
            string fileSavePath = string.Empty;
            string virtualDirectoryImg = "UploadedFiles";
            string fileName = string.Empty;
            if (HelpSectionImages == "System.Web.HttpPostedFileWrapper")
            {
                if (HttpContext.Request.Files.AllKeys.Any())
                {
                    // Get the uploaded image from the Files collection
                    var httpPostedFile = HttpContext.Request.Files["HelpSectionImages"];
                    fileName = httpPostedFile.FileName;
                    string HeaderId = HttpContext.Request.Form["Headerid"];
                    string Qproject = HttpContext.Request.Form["Qproject"];
                    string ImgName = HttpContext.Request.Form["ImgName"];
                    if (httpPostedFile != null)
                    {
                        // OBtient le path du fichier 
                        if (ImgName == "")
                        {
                            Qproject = Qproject.Replace('/', '_');
                            ImgName = Qproject + "-" + DateTime.Now.ToString("ddMMyyyy HHmmss") + ".png";
                        }

                        fileSavePath = Path.Combine(HttpContext.Server.MapPath("~/Areas/MAL/NameImg/"), ImgName);

                        // Sauvegarde du fichier dans UploadedFiles sur le serveur
                        httpPostedFile.SaveAs(fileSavePath);
                    }

                    return Json(ImgName);
                }
                else
                {
                    return Json("Failed");
                }
            }
            else
            {
                string ImgName = HttpContext.Request.Form["ImgName"];
                var t = HelpSectionImages.Substring(22);  // remove data:image/png;base64,



                byte[] imageBytes = Convert.FromBase64String(t);

                //Save the Byte Array as Image File.
                string filePath = Server.MapPath(ImgName);
                System.IO.File.WriteAllBytes(filePath, imageBytes);
                return Json(ImgName);
            }
        }

        [HttpPost]
        public string SaveImageInSharePoint(string ImagePath, string imageBlob)
        {
            string ImageName = "NewImg.png";
            string Massage = "Saving image Failed";
            var Files = (new clsFileUpload()).GetDocuments(ImagePath);
            if (Files.Length > 0)
            {
                ImageName = Files.FirstOrDefault().Name;
            }
            //imageBlob = imageBlob.Replace("data:image/png;base64,", string.Empty);
            //imageBlob = imageBlob.Replace("data:image/jpg;base64,", string.Empty);
            int base64 = imageBlob.IndexOf("base64,");
            imageBlob = imageBlob.Remove(0, base64 + 7);
            var bytes = Convert.FromBase64String(imageBlob);
            MemoryStream stream = new MemoryStream(bytes);
            //var bytes = Convert.FromBase64String(imageBlob);
            //var contents = new StreamContent(new MemoryStream(bytes));
            clsFileUpload upload = new clsFileUpload();

            bool t = upload.FileUpload(ImageName, ImagePath, stream, "", "", false);
            if (t)
            {
                Massage = "Image Saved";
            }
            return Massage;
        }
    }
}