﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.MAL.Controllers
{
    public class StencilingController : clsBase
    {

        // GET: MAL/Stenciling
        string ControllerURL = "/MAL/Stenciling/";
        string Title = "REPORT FOR STENCILING / MARKING INSPECTION";

        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;
            ViewBag._lstMAL017_2 = null;
            ViewBag.DrawingNo = null;
            ViewBag.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();

            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();


            MAL017 objMAL017 = new MAL017();
            if (id > 0)
            {
                objMAL017 = db.MAL017.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;

                ViewBag._lstMAL017_2 = db.MAL017_2.Where(x => x.HeaderId == objMAL017.HeaderId).ToList();
                ViewBag._lstMAL017_3 = db.MAL017_3.Where(x => x.HeaderId == objMAL017.HeaderId).ToList();
                //ViewBag._lstMAL017_4 = db.MAL017_4.Where(x => x.HeaderId == objMAL017.HeaderId).Select(s => new
                //{
                //    HeaderId = s.HeaderId,
                //    FileName = s.FileName,
                //}); 

                List<FCS001> _objFCS = db.FCS001.Where(w => w.TableId == id.Value && w.TableName == "MAL017").OrderBy(o=>o.ViewerId).ToList();
                if (_objFCS.Count > 0)
                    ViewBag._lstMAL017_4 = _objFCS;
                else
                    ViewBag._lstMAL017_4 = null;

                string Stenciling = clsImplementationEnum.MALProtocolType.Stenciling.GetStringValue();
                var obj = db.MAL000.Where(a => a.HeaderId == objMAL017.HeaderId && a.InspectionActivity == Stenciling).FirstOrDefault();
                ViewBag.Status = obj?.Status;
                if (m == 1)
                { ViewBag.isEditable = "true"; }
                else
                { ViewBag.isEditable = "false"; }
            }
            else
            {
                ViewBag.isEditable = "true";
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;
            ViewBag.PrintedBy = objClsLoginInfo.UserName;

            return View(objMAL017);
        }
      
        [HttpPost]
        public ActionResult SaveProtocolHeaderData(MAL017 _objMAL017, int s = 0)
        {
            MAL017 objMAL017 = new MAL017();
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = _objMAL017.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                    objMAL017 = db.MAL017.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                #region Save Data
                objMAL017.ProjectNo = _objMAL017.ProjectNo;
                objMAL017.Date = _objMAL017.Date;
                objMAL017.EquipmentNameNumber = _objMAL017.EquipmentNameNumber;
                objMAL017.InspectionAgency = _objMAL017.InspectionAgency;
                objMAL017.ManufacturingCode = _objMAL017.ManufacturingCode;
                objMAL017.Client = _objMAL017.Client;
                objMAL017.PoNo = _objMAL017.PoNo;
                objMAL017.IsPhotograph = _objMAL017.IsPhotograph;

                if (refHeaderId == 0)
                {
                    objMAL017.CreatedBy = objClsLoginInfo.UserName;
                    objMAL017.CreatedOn = DateTime.Now;
                    db.MAL017.Add(objMAL017);
                }
                else
                {
                    objMAL017.EditedBy = objClsLoginInfo.UserName;
                    objMAL017.EditedOn = DateTime.Now;
                }
                db.SaveChanges();
                string Stenciling = clsImplementationEnum.MALProtocolType.Stenciling.GetStringValue();

                var obj = db.MAL000.Where(a => a.HeaderId == objMAL017.HeaderId && a.InspectionActivity == Stenciling).FirstOrDefault();
                if (obj == null)
                {
                    int? srno = db.MAL000.Where(o => o.Project == objMAL017.ProjectNo && o.InspectionActivity == Stenciling).OrderByDescending(x => x.SerialNo).Select(a => a.SerialNo).FirstOrDefault();
                    MAL000 objMAL000 = new MAL000();
                    objMAL000.Project = objMAL017.ProjectNo;
                    objMAL000.InspectionActivity = Stenciling;
                    objMAL000.SerialNo = Convert.ToInt32((srno >= 0 ? srno : 0) + 1);
                    objMAL000.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();

                    objMAL000.CreatedBy = objClsLoginInfo.UserName;
                    objMAL000.CreatedOn = DateTime.Now;
                    objMAL000.HeaderId = objMAL017.HeaderId;
                    db.MAL000.Add(objMAL000);
                    db.SaveChanges();
                }
                else
                {
                    if (s == 1)
                    {
                        obj.Status = clsImplementationEnum.CommonStatus.Approved.GetStringValue();
                        obj.EditedBy = objClsLoginInfo.UserName;
                        obj.EditedOn = DateTime.Now;
                        db.SaveChanges();
                    }
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Protocol submitted successfully";
                objResponseMsg.HeaderId = objMAL017.HeaderId;
                #endregion
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveProtocolLine1(List<MAL017_2> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<MAL017_2> lstAddMAL017_2 = new List<MAL017_2>();
                List<MAL017_2> lstDeleteMAL017_2 = new List<MAL017_2>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            MAL017_2 obj = db.MAL017_2.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.HeaderId = item.HeaderId;
                                obj.DrgNo = item.DrgNo;
                                obj.RevNo = item.RevNo;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            MAL017_2 obj = new MAL017_2();
                            if (!string.IsNullOrWhiteSpace(item.DrgNo))
                            {
                                obj.HeaderId = item.HeaderId;
                                obj.DrgNo = item.DrgNo;
                                obj.RevNo = item.RevNo;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddMAL017_2.Add(obj);
                            }
                        }
                    }
                    if (lstAddMAL017_2.Count > 0)
                    {
                        db.MAL017_2.AddRange(lstAddMAL017_2);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeleteMAL017_2 = db.MAL017_2.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeleteMAL017_2.Count > 0)
                    {
                        db.MAL017_2.RemoveRange(lstDeleteMAL017_2);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeleteMAL017_2 = db.MAL017_2.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeleteMAL017_2.Count > 0)
                    {
                        db.MAL017_2.RemoveRange(lstDeleteMAL017_2);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public string ProjectDetails(string ProjectNo)
        {
            if (ProjectNo != "")
            {
                NDEModels objNDEModels = new NDEModels();
                var objQproject = db.QMS010.Where(x => x.QualityProject == ProjectNo).FirstOrDefault();
                string strMCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;

                if (ProjectNo.Contains("/"))
                {
                    string[] arr = ProjectNo.Split('/');
                    ProjectNo = arr[0];
                }

                var Data = db.COM001.Where(w => w.t_cprj == ProjectNo).FirstOrDefault();
                string sValue = Data.t_cprj + "-" + Data.t_dsca;

                return (sValue + "|" + strMCode);
            }
            else
                return "";
        }

        [HttpPost]
        public ActionResult getDrgNo(string ProjectNo)
        {
            //ProjectNo = "S040166";
            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(ProjectNo).ToList();
            lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            return Json(lstdrawing.Select(x => new
            {
                Value = x,
                Text = x
            }).ToList(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeletePhoto(string strTableName, int Headerid, int PhotoNo, string FileName)
        {
            try
            {
                var Files = (new clsFileUpload()).GetDocuments(strTableName + "/" + Headerid + "/" + PhotoNo);

                foreach (var item in Files)
                {
                    if (item.Name == FileName)
                        (new clsFileUpload()).DeleteFile(strTableName + "/" + Headerid + "/" + PhotoNo, item.Name);
                }
                return Json(true);
            }
            catch
            {
                return Json(false);
            }
        }

        //[HttpPost]
        //public ActionResult DeletePhoto(int PhotoNo, int Headerid, string strTableName, string status = "")
        //{
        //    try
        //    {
        //        var Files = (new clsFileUpload()).GetDocuments(strTableName + "/" + Headerid + "/" + PhotoNo);
        //        foreach (var item in Files)
        //        {
        //            (new clsFileUpload()).DeleteFile(strTableName + "/" + Headerid + "/" + PhotoNo, item.Name);
        //        }

        //        return Json(true);
        //    }
        //    catch
        //    {
        //        return Json(false);
        //    }
        //}
        //[HttpPost]
        //public ActionResult DeleteAllPhoto(int PhotoNo, int Headerid, string strTableName, string status)
        //{
        //    try
        //    {
        //        var Files = (new clsFileUpload()).GetDocuments(strTableName + "/" + Headerid + "/" + PhotoNo);
        //        foreach (var item in Files)
        //        {
        //            (new clsFileUpload()).DeleteFile(strTableName + "/" + Headerid + "/" + PhotoNo, item.Name);
        //        }

        //        return Json('Deleted');
        //    }
        //    catch
        //    {
        //        return Json('error');
        //    }
        //}

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2(List<MAL017_3> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<MAL017_3> lstAddMAL017_3 = new List<MAL017_3>();
                List<MAL017_3> lstDeleteMAL017_3 = new List<MAL017_3>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        MAL017_3 obj = db.MAL017_3.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        else
                        {
                            obj = new MAL017_3();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }



                        if (!string.IsNullOrWhiteSpace(item.ActualValue))
                        {
                            obj.ActualValue = item.ActualValue;
                        }
                        if (isAdded)
                        {
                            lstAddMAL017_3.Add(obj);
                        }
                    }
                    if (lstAddMAL017_3.Count > 0)
                    {
                        db.MAL017_3.AddRange(lstAddMAL017_3);
                    }



                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);



                    lstDeleteMAL017_3 = db.MAL017_3.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeleteMAL017_3.Count > 0)
                    {
                        db.MAL017_3.RemoveRange(lstDeleteMAL017_3);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeleteMAL017_3 = db.MAL017_3.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeleteMAL017_3.Count > 0)
                    {
                        db.MAL017_3.RemoveRange(lstDeleteMAL017_3);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        [HttpPost]
        public JsonResult SaveMultipleImages(List<MAL017_4> mAL017_4s = null)
        {
            if (mAL017_4s != null)
            {
                foreach (var item in mAL017_4s)
                {
                    MAL017_4 mAL017_4 = new MAL017_4()
                    {
                        HeaderId = item.HeaderId,
                        FileName = item.FileName,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    };
                    db.MAL017_4.Add(mAL017_4);
                }
                db.SaveChanges();
                return Json("success", JsonRequestBehavior.AllowGet);
            }
            return Json("", JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public string GetInspectionAgency(string qproject)
        {
            string Agency = string.Empty;
            string inspectionagency = Manager.GetInspectionAgency(qproject);
            if (!string.IsNullOrWhiteSpace(inspectionagency))
            {
                string[] arr = inspectionagency.Split(',').ToArray();
                Agency = string.Join("#", arr.Take(3).ToArray());
            }
            return Agency;
        }

    }
}