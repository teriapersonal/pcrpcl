﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQSImplementation;

namespace IEMQS.Areas.PPS.Controllers
{
    public class DesignPlantMasterController : clsBase
    {
        // GET: PPS/DesignPlantMaster
        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }
    }
}