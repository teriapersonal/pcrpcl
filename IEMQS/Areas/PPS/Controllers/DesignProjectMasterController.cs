﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQSImplementation;

namespace IEMQS.Areas.PPS.Controllers
{
    public class DesignProjectMasterController : clsBase
    {
        // GET: PPS/DesignProjectMaster
        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }
    }
}