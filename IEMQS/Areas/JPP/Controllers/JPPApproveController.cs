﻿using IEMQS.Areas.Authorization.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationEnum;

namespace IEMQS.Areas.JPP.Controllers
{
    public class JPPApproveController : clsBase
    {
        // GET: JPP/JPPApprove
        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }
        [SessionExpireFilter]
        public ActionResult JPPApproveDetail(int Id = 0)
        {
            JPP001 objJPP001 = new JPP001();
            string name = (from a in db.COM003
                           where a.t_psno == objClsLoginInfo.UserName && a.t_actv == 1
                           select a.t_name).FirstOrDefault();
            ViewBag.CreatedBy = objClsLoginInfo.UserName + " - " + name;
            var Location = (from a in db.COM002
                            where a.t_dtyp == 1 && a.t_dimx != ""
                            select new { a.t_dimx, Desc = a.t_desc }).ToList();

            ViewBag.Location = new SelectList(Location, "t_dimx", "Desc");
            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.lstyesno = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();
            if (Id > 0)
            {
                string isPCC3 = "";
                var isvalid = objClsLoginInfo.GetUserRoleList().Contains(clsImplementationEnum.UserRoleName.PMG2.ToString());
                if (isvalid == true)
                {
                    isPCC3 = "No";
                }
                else
                {
                    isPCC3 = "Yes";
                }
                ViewBag.PCC3 = isPCC3;
                objJPP001 = db.JPP001.Where(x => x.HeaderId == Id).FirstOrDefault();

                var imbplanname = clsImplementationEnum.PlanList.Improvement_Budget_Plan.GetStringValue();
                var PlanningDinID = db.PDN002.Where(x => x.RefId == objJPP001.HeaderId && x.DocumentNo == objJPP001.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                ViewBag.PlanningDinID = PlanningDinID;
                var imbData = db.PDN002.Where(x => x.HeaderId == PlanningDinID && x.Plan == imbplanname).FirstOrDefault();
                string strRELEASED = clsImplementationEnum.PlanningDinStatus.RELEASED.GetStringValue();
                string strApproved = clsImplementationEnum.CommonStatus.Approved.GetStringValue();
                if (imbData != null)
                {
                    ViewBag.IMBLink = WebsiteURL + "/IMB/Approve/Viewlines?Id=" + imbData.RefId;
                    ViewBag.IMBDocNo = imbData.DocumentNo;

                    if (imbData.Status == strApproved && imbData.IssueStatus == strRELEASED)
                    {
                        ViewBag.showImb = "true";
                    }
                }
                ViewBag.Revoke = Manager.IsRevokeApplicable(PlanningDinID, Id, clsImplementationEnum.PlanList.JPP.GetStringValue());
                ViewBag.Contract = (from jpp1 in db.JPP001
                                    join cm004 in db.COM004 on jpp1.ContractNo equals cm004.t_cono
                                    where cm004.t_cono == objJPP001.ContractNo
                                    select cm004.t_cono + "-" + cm004.t_desc).FirstOrDefault();


                ViewBag.Iproject = (from jpp1 in db.JPP001
                                    join cm004 in db.COM004 on jpp1.ContractNo equals cm004.t_cono
                                    where cm004.t_cono == objJPP001.ContractNo & jpp1.Project== objJPP001.Project
                                    select jpp1.IProject).FirstOrDefault();
                //to resolved approveby value null While Project Status Draft.
                if ((objJPP001.ApprovedBy + "").Trim() != objClsLoginInfo.UserName.Trim())
                {
                    //return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                    ViewBag.ForApprover = false;
                }
                else
                {
                    ViewBag.ForApprover = true;
                }
            }

            return View(objJPP001);
        }
        [HttpPost]
        public ActionResult GetHeaderGridDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_getApproveHTMLPartial");
        }
        public string GetCodeAndNameByProject(string project)
        {
            var projdesc = db.COM001.Where(i => i.t_cprj == project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
            return projdesc;
        }
        [HttpPost]
        public JsonResult GetHeaderDetailsForPrintReport(int HeaderId)
        {
            var objJPP001 = db.JPP001.Where(i => i.HeaderId == HeaderId).Select(i => new { i.HeaderId, i.Project, i.Document, i.RevNo }).FirstOrDefault();
            return Json(objJPP001, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult LoadHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var user = objClsLoginInfo.UserName;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                string strWhere = string.Empty;
                //strWhere += " and headerId = " + param.Headerid;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and status in('" + clsImplementationEnum.CTQStatus.SendForApprovel.GetStringValue() + "')";
                    strWhere += " and ApprovedBy=" + user;
                }
                else
                {
                    strWhere += "1=1";
                }
                //strWhere += " and ApprovedBy=" + user;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {

                    strWhere += " and (Project like '%" + param.sSearch
                         + "%' or Document like '%" + param.sSearch
                         + "%' or Customer like '%" + param.sSearch
                         + "%' or RevNo like '%" + param.sSearch
                         + "%' or CDDDate like '%" + param.sSearch
                         + "%' or Status like '%" + param.sSearch
                         + "%' or (b.t_cono + '-'+ b.t_desc) like '%" + param.sSearch + "%')";
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                var lstResult = db.SP_JPP_GET_HEADERDETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.ContractNo),
                               Convert.ToString(GetCodeAndNameByProject(uc.Project)),
                               Convert.ToString(Manager.GetCustomerCodeAndNameByProject(uc.Project)),
                               Convert.ToString(uc.Document),
                               Convert.ToString(uc.Status),
                               Convert.ToDateTime(uc.CDDDate).ToString("dd/MM/yyyy"),
                               Convert.ToString("R"+uc.RevNo),
                                Convert.ToString(uc.SubmittedBy),
                                Convert.ToString(Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy")),
                                Convert.ToString(uc.ApprovedBy),
                                Convert.ToString(Convert.ToDateTime(uc.ApprovedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.ApprovedOn).ToString("dd/MM/yyyy")),

                                "<center>"
                                +"<a title='View' href='"+WebsiteURL+"/JPP/JPPApprove/JPPApproveDetail?Id="+uc.HeaderId+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a>"
                                +"<a title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/JPP/JPPHeader/ShowTimeline?HeaderID=" + Convert.ToInt32(uc.HeaderId) + "')><i class='fa fa-clock-o' style='margin-left:5px;'></i></a>"
                                +(uc.RevNo > 0 ||(uc.RevNo==0 && uc.Status==clsImplementationEnum.CommonStatus.Approved.GetStringValue()) ? HTMLHistoryString(uc.HeaderId,uc.Status,"History","History","fa fa-history","GetHistoryDetails(\""+ uc.HeaderId +"\");") + "" :  "<i title='History' style = 'margin-left:5px;opacity:0.5' class='fa fa-history'></i>")
                                + "<i style='margin-left:5px;cursor:pointer;' title='Print Report' class='fa fa-print' onClick='PrintReport("+uc.HeaderId+")'></i>"
                                +"</center>",
                               Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public string HTMLHistoryString(int rowId, string status, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";
            htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;margin-left:5px;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
            return htmlControl;
        }
        [HttpPost]
        public ActionResult ApproveSelectedJPP(string strHeaderIds)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string[] arrayHeaderIds = strHeaderIds.Split(',').ToArray();
                for (int i = 0; i < arrayHeaderIds.Length; i++)
                {
                    int HeaderId = Convert.ToInt32(arrayHeaderIds[i]);


                    db.SP_JPP_APPROVE(HeaderId, objClsLoginInfo.UserName);
                    JPP001 objJPP001 = db.JPP001.Where(m => m.HeaderId == HeaderId).FirstOrDefault();
                    int newId = db.JPP001_Log.Where(q => q.HeaderId == HeaderId).OrderByDescending(q => q.ApprovedOn).FirstOrDefault().Id;
                    Manager.AddPDN002(objJPP001.HeaderId, objJPP001.Status, objJPP001.RevNo, objJPP001.Project, objJPP001.Document, PlanList.JPP.GetStringValue(), clsImplementationEnum.PlanList.JPP.GetStringValue());
                    Manager.UpdatePDN002(objJPP001.HeaderId, objJPP001.Status, objJPP001.RevNo, objJPP001.Project, objJPP001.Document, newId);

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PMG3.GetStringValue(),
                                                        objJPP001.Project,
                                                        "",
                                                        "",
                                                        Manager.GetPDINDocumentNotificationMsg(objJPP001.Project, clsImplementationEnum.PlanList.JPP.GetStringValue(), objJPP001.RevNo.Value.ToString(), objJPP001.Status),
                                                        clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(),
                                                        Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.JPP.GetStringValue(), objJPP001.HeaderId.ToString(), false),
                                                        objJPP001.CreatedBy);
                    #endregion
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.QCPMessages.Approve.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult ReturnSelectedTP(string strHeaderIds, string remarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string[] arrayHeaderIds = strHeaderIds.Split(',').ToArray();
                for (int i = 0; i < arrayHeaderIds.Length; i++)
                {
                    int HeaderId = Convert.ToInt32(arrayHeaderIds[i]);
                    JPP001 objJPP001 = db.JPP001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    objJPP001.Status = clsImplementationEnum.CTQStatus.Returned.GetStringValue();
                    objJPP001.ApprovedBy = objClsLoginInfo.UserName;
                    objJPP001.ApprovedOn = DateTime.Now;
                    objJPP001.ReturnRemark = remarks;
                    db.SaveChanges();
                    Manager.AddPDN002(objJPP001.HeaderId, objJPP001.Status, objJPP001.RevNo, objJPP001.Project, objJPP001.Document, PlanList.JPP.GetStringValue(), clsImplementationEnum.PlanList.JPP.GetStringValue());
                    Manager.UpdatePDN002(objJPP001.HeaderId, objJPP001.Status, objJPP001.RevNo, objJPP001.Project, objJPP001.Document);

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PMG3.GetStringValue(),
                                                        objJPP001.Project,
                                                        "",
                                                        "",
                                                        Manager.GetPDINDocumentNotificationMsg(objJPP001.Project, clsImplementationEnum.PlanList.JPP.GetStringValue(), objJPP001.RevNo.Value.ToString(), objJPP001.Status),
                                                        clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(),
                                                        Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.JPP.GetStringValue(), objJPP001.HeaderId.ToString(), false),
                                                        objJPP001.CreatedBy);
                    #endregion
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.QCPMessages.Return.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetHistoryView(string Id)
        {
            ViewBag.Id = Id;
            return PartialView("_JPPHistoryGrid");
        }

        [HttpPost]
        public ActionResult RevokeDocument(int HeaderId, int pdinId, string Remarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (Manager.IsRevokeApplicable(pdinId, HeaderId, clsImplementationEnum.PlanList.JPP.GetStringValue()))
                {
                    objResponseMsg.Key = Manager.RevokePDINDocument(HeaderId, clsImplementationEnum.PlanList.JPP, Remarks);
                    if (objResponseMsg.Key)
                    {
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Revoke;
                    }
                    else
                    {
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.UnSuccess;
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.pdnRevokeMessage;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult LoadHeaderHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                //var user = objClsLoginInfo.UserName;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                int Id = Convert.ToInt32(param.Headerid); 
                JPP001 objJPP001 = new JPP001();
                objJPP001 = db.JPP001.Where(x => x.HeaderId == Id).FirstOrDefault();
                ViewBag.Contract = (from jpp1 in db.JPP001
                                    join cm004 in db.COM004 on jpp1.ContractNo equals cm004.t_cono
                                    where cm004.t_cono == objJPP001.ContractNo
                                    select cm004.t_cono + "-" + cm004.t_desc).FirstOrDefault();


                ViewBag.Iproject = (from jpp1 in db.JPP001
                                    join cm004 in db.COM004 on jpp1.ContractNo equals cm004.t_cono
                                    where cm004.t_cono == objJPP001.ContractNo & jpp1.Project == objJPP001.Project
                                    select jpp1.IProject).FirstOrDefault();
                //strWhere += " and headerId = " + param.Headerid;


                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                string strWhere = "1=1";
                strWhere += " and headerId = " + param.Headerid;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (Project like '%" + param.sSearch
                          + "%' or Document like '%" + param.sSearch
                         + "%' or Document like '%" + param.sSearch
                         + "%' or Customer like '%" + param.sSearch
                         + "%' or RevNo like '%" + param.sSearch
                         + "%' or Status like '%" + param.sSearch + "%')";
                }
                var lstResult = db.SP_JPP_GET_HEADERHISTORYDETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.Project),
                                Convert.ToString(uc.ContractNo),
                               Convert.ToString(GetCodeAndNameByProject(uc.Project)),
                               Convert.ToString(Manager.GetCustomerCodeAndNameByProject(uc.Project)),
                               Convert.ToString(uc.Document),
                               Convert.ToString(uc.Status),
                               Convert.ToDateTime(uc.CDDDate).ToString("dd/MM/yyyy"),
                                Convert.ToString("R"+uc.RevNo),
                                Convert.ToString(uc.SubmittedBy),
                                Convert.ToString(Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy")),
                                Convert.ToString(uc.ApprovedBy),
                                Convert.ToString(Convert.ToDateTime(uc.ApprovedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.ApprovedOn).ToString("dd/MM/yyyy")),
                                //Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.Id)
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult JPPHistoryDetail(int Id = 0)
        {
            JPP001_Log objJPP001_Log = new JPP001_Log();
            if (Id > 0)
            {
                objJPP001_Log = db.JPP001_Log.Where(x => (x.Id == Id)).FirstOrDefault();
            }            
            string name = (from a in db.COM003
                           where a.t_psno == objClsLoginInfo.UserName && a.t_actv == 1
                           select a.t_name).FirstOrDefault();
            var Location = (from a in db.COM002
                            where a.t_dtyp == 1 && a.t_dimx != ""
                            select new { a.t_dimx, Desc = a.t_desc }).ToList();

            ViewBag.Location = new SelectList(Location, "t_dimx", "Desc");
            ViewBag.CreatedBy = objClsLoginInfo.UserName + " - " + name;
            if (objJPP001_Log.ContractNo != null)
            {
                ViewBag.Contract = (from cm004 in db.COM004
                                    join cm005 in db.COM005 on cm004.t_cono equals cm005.t_cono
                                    where cm004.t_cono == objJPP001_Log.ContractNo
                                    select cm004.t_cono + "-" + cm004.t_desc).FirstOrDefault();
            }

            if (objJPP001_Log.IProject != null)
            {
                ViewBag.Iproject = (from jpp1 in db.JPP001
                                    join cm004 in db.COM004 on jpp1.ContractNo equals cm004.t_cono
                                    where cm004.t_cono == objJPP001_Log.ContractNo & jpp1.Project == objJPP001_Log.Project
                                    select jpp1.IProject).FirstOrDefault();
            }
            
            var urlPrefix = Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.LastIndexOf('=') + 1);
            if (!objClsLoginInfo.GetUserRoleList().Contains("SHOP"))
            {
                ViewBag.RevPrev = (db.JPP001_Log.Any(q => (q.HeaderId == objJPP001_Log.HeaderId && q.RevNo == (objJPP001_Log.RevNo - 1))) ? urlPrefix + db.JPP001_Log.Where(q => (q.HeaderId == objJPP001_Log.HeaderId && q.RevNo == (objJPP001_Log.RevNo - 1))).FirstOrDefault().Id : null);
                ViewBag.RevNext = (db.JPP001_Log.Any(q => (q.HeaderId == objJPP001_Log.HeaderId && q.RevNo == (objJPP001_Log.RevNo + 1))) ? urlPrefix + db.JPP001_Log.Where(q => (q.HeaderId == objJPP001_Log.HeaderId && q.RevNo == (objJPP001_Log.RevNo + 1))).FirstOrDefault().Id : null);
            }
            return View(objJPP001_Log);
        }
        [HttpPost]
        public ActionResult GetMajorMaterialConstruction(string Project, string Document, string RevNo, int headerid, int lineid)
        {
            JPP002 objJPP001 = new JPP002();
            var isvalid = db.JPP001.Any(x => x.HeaderId == headerid);
            if (isvalid == true)
            {
                Project = Project.Split('-')[0].Trim();
                var projdesc = db.COM001.Where(i => i.t_cprj == Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                int Headerid = (from a in db.JPP001
                                where a.Project == Project
                                select a.HeaderId).FirstOrDefault();
                Session["headerid"] = Convert.ToInt32(Headerid);
                ViewBag.Project = Project + " - " + projdesc;
                ViewBag.DocNum = Document;
                ViewBag.DocRev = RevNo;
                ViewBag.Headerid = Headerid;
                ViewBag.Lineid = lineid;
            }
            if (lineid > 0)
            {
                objJPP001 = db.JPP002.Where(x => x.LineId == lineid).FirstOrDefault();
            }
            return PartialView("_MajorMaterialConstructionPartial", objJPP001);
        }
        [HttpPost]
        public ActionResult saveMajorMaterialConstruction(FormCollection fc)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                int headerid = Convert.ToInt32(fc["HeaderId0"].ToString());
                var project = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.Project).FirstOrDefault();
                var document = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.Document).FirstOrDefault();
                var revno = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.RevNo).FirstOrDefault();
                JPP002 objJPP002 = new JPP002();
                objJPP002.HeaderId = headerid;
                objJPP002.Project = project;
                objJPP002.Document = document;
                objJPP002.RevNo = revno;
                objJPP002.MajorMaterialofConstruction = fc["MajorMaterialofConstruction0"].ToString();
                objJPP002.CreatedBy = objClsLoginInfo.UserName;
                objJPP002.CreatedOn = DateTime.Now;
                db.JPP002.Add(objJPP002);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Insert.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteMajorMaterialConstruction(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                JPP002 objJPP002 = db.JPP002.Where(x => x.LineId == Id).FirstOrDefault();
                db.JPP002.Remove(objJPP002);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Delete.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetSpecificCustomerRequirement(string Project, string Document, string RevNo, int headerid, int lineid)
        {
            JPP003 objJPP001 = new JPP003();
            var isvalid = db.JPP001.Any(x => x.HeaderId == headerid);
            if (isvalid == true)
            {
                Project = Project.Split('-')[0].Trim();
                var projdesc = db.COM001.Where(i => i.t_cprj == Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                int Headerid = (from a in db.JPP001
                                where a.Project == Project
                                select a.HeaderId).FirstOrDefault();
                Session["headerid"] = Convert.ToInt32(Headerid);
                ViewBag.Project = Project + " - " + projdesc;
                ViewBag.DocNum = Document;
                ViewBag.DocRev = RevNo;
                ViewBag.Headerid = Headerid;
                ViewBag.Lineid = lineid;
                if (lineid > 0)
                {
                    objJPP001 = db.JPP003.Where(x => x.LineId == lineid).FirstOrDefault();
                }
            }
            return PartialView("_SpecificCustomerRequirementPartial", objJPP001);
        }
        [HttpPost]
        public ActionResult saveSpecificCustomerRequirement(FormCollection fc)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                int headerid = Convert.ToInt32(fc["HeaderId0"].ToString());
                var project = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.Project).FirstOrDefault();
                var document = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.Document).FirstOrDefault();
                var revno = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.RevNo).FirstOrDefault();
                JPP003 objJPP003 = new JPP003();
                objJPP003.HeaderId = headerid;
                objJPP003.Project = project;
                objJPP003.Document = document;
                objJPP003.RevNo = revno;
                objJPP003.SpecificCustomerRequirementSalientFeatures = fc["MajorMaterialofConstruction0"].ToString();
                objJPP003.CreatedBy = objClsLoginInfo.UserName;
                objJPP003.CreatedOn = DateTime.Now;
                db.JPP003.Add(objJPP003);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Insert.ToString();

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteSpecificCustomerRequirement(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                JPP003 objJPP003 = db.JPP003.Where(x => x.LineId == Id).FirstOrDefault();
                db.JPP003.Remove(objJPP003);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Delete.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetPaymentTerms(string Project, string Document, string RevNo, int headerid, int lineid)
        {
            JPP004 objJPP001 = new JPP004();
            var isvalid = db.JPP001.Any(x => x.HeaderId == headerid);
            if (isvalid == true)
            {
                Project = Project.Split('-')[0].Trim();
                var projdesc = db.COM001.Where(i => i.t_cprj == Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();

                int Headerid = (from a in db.JPP001
                                where a.Project == Project
                                select a.HeaderId).FirstOrDefault();
                Session["headerid"] = Convert.ToInt32(Headerid);
                ViewBag.Project = Project + " - " + projdesc;
                ViewBag.DocNum = Document;
                ViewBag.DocRev = RevNo;
                ViewBag.Headerid = Headerid;
                ViewBag.Lineid = lineid;
                if (lineid > 0)
                {
                    objJPP001 = db.JPP004.Where(x => x.LineId == lineid).FirstOrDefault();
                }
            }
            return PartialView("_PaymentTermsPartial", objJPP001);
        }
        [HttpPost]
        public ActionResult savePaymentTerms(FormCollection fc)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                int headerid = Convert.ToInt32(fc["HeaderId0"].ToString());
                var project = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.Project).FirstOrDefault();
                var document = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.Document).FirstOrDefault();
                var revno = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.RevNo).FirstOrDefault();
                int msno = Convert.ToInt32(fc["MSNo0"]);
                decimal percentage = Convert.ToDecimal(fc["percentage0"]);

                JPP004 objJPP004 = new JPP004();
                objJPP004.HeaderId = headerid;
                objJPP004.Project = project;
                objJPP004.Document = document;
                objJPP004.RevNo = revno;
                //objJPP004.PaymentTerms = fc["PaymentTerms0"].ToString();
                objJPP004.MSNo = msno;
                objJPP004.Discription = fc["Discription0"].ToString();
                objJPP004.percentage = percentage;
                objJPP004.CreatedBy = objClsLoginInfo.UserName;
                objJPP004.CreatedOn = DateTime.Now;
                db.JPP004.Add(objJPP004);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Insert.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeletePaymentTerms(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                JPP004 objJPP004 = db.JPP004.Where(x => x.LineId == Id).FirstOrDefault();
                db.JPP004.Remove(objJPP004);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Delete.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetDocuments(string Project, string Document, string RevNo, int headerid, int lineid)
        {
            JPP005 objJPP001 = new JPP005();
            var isvalid = db.JPP001.Any(x => x.HeaderId == headerid);
            if (isvalid == true)
            {
                Project = Project.Split('-')[0].Trim();
                var projdesc = db.COM001.Where(i => i.t_cprj == Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();


                int Headerid = (from a in db.JPP001
                                where a.Project == Project
                                select a.HeaderId).FirstOrDefault();
                Session["headerid"] = Convert.ToInt32(Headerid);
                ViewBag.Project = Project + " - " + projdesc;
                ViewBag.DocNum = Document;
                ViewBag.DocRev = RevNo;
                ViewBag.Headerid = Headerid;
                ViewBag.Lineid = lineid;
                if (lineid > 0)
                {
                    objJPP001 = db.JPP005.Where(x => x.LineId == lineid).FirstOrDefault();
                }
            }
            return PartialView("_DocumentsPartial", objJPP001);
        }
        [HttpPost]
        public ActionResult saveDocuments(FormCollection fc)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                if (CheckDate(fc["RequiredDate0"].ToString()) || CheckDate(fc["ExpectedDate0"].ToString()))
                {
                    int headerid = Convert.ToInt32(fc["HeaderId0"].ToString());
                    var objproj = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => new { project = i.Project, document = i.Document, revno = i.RevNo }).FirstOrDefault();
                    var maxseqno = db.JPP005.Where(x => x.HeaderId == headerid).Max(c => c.SeqNo);
                    JPP005 objJPP005 = new JPP005();
                    objJPP005.SeqNo = maxseqno != null ? (maxseqno + 1) : 1;
                    objJPP005.HeaderId = headerid;
                    objJPP005.Project = objproj.project;
                    objJPP005.Document = objproj.document;
                    objJPP005.RevNo = objproj.revno;
                    objJPP005.DocumentsDrawingDescription = fc["DocumentsDrawingDescription0"].ToString();
                    objJPP005.RequiredDate = Convert.ToDateTime(fc["RequiredDate0"].ToString());
                    objJPP005.ExpectedDate = Convert.ToDateTime(fc["ExpectedDate0"].ToString());
                    objJPP005.CreatedBy = objClsLoginInfo.UserName;
                    objJPP005.CreatedOn = DateTime.Now;
                    db.JPP005.Add(objJPP005);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CTQMessages.Insert.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Invalid Date";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteDocuments(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                JPP005 objJPP005 = db.JPP005.Where(x => x.LineId == Id).FirstOrDefault();
                db.JPP005.Remove(objJPP005);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Delete.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetProcurement(string Project, string Document, string RevNo, int headerid, int lineid)
        {
            JPP006 objJPP001 = new JPP006();
            var isvalid = db.JPP001.Any(x => x.HeaderId == headerid);
            if (isvalid == true)
            {
                Project = Project.Split('-')[0].Trim();
                var projdesc = db.COM001.Where(i => i.t_cprj == Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();

                int Headerid = (from a in db.JPP001
                                where a.Project == Project
                                select a.HeaderId).FirstOrDefault();
                Session["headerid"] = Convert.ToInt32(Headerid);
                ViewBag.Project = Project + " - " + projdesc;
                ViewBag.DocNum = Document;
                ViewBag.DocRev = RevNo;
                ViewBag.Headerid = Headerid;
                ViewBag.Lineid = lineid;
                if (lineid > 0)
                {
                    objJPP001 = db.JPP006.Where(x => x.LineId == lineid).FirstOrDefault();
                }
            }
            return PartialView("_ProcurementPartial", objJPP001);
        }
        [HttpPost]
        public ActionResult saveProcurement(FormCollection fc)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                if (CheckDate(fc["RequiredDatetomeetCDD0"].ToString()) || CheckDate(fc["ExpectedDate0"].ToString()))
                {
                    int headerid = Convert.ToInt32(fc["HeaderId0"].ToString());
                    var objproj = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => new { project = i.Project, document = i.Document, revno = i.RevNo }).FirstOrDefault();
                    var maxseqno = db.JPP006.Where(x => x.HeaderId == headerid).Max(c => c.SeqNo);
                    JPP006 objJPP006 = new JPP006();
                    objJPP006.SeqNo = maxseqno != null ? (maxseqno + 1) : 1;
                    objJPP006.HeaderId = headerid;
                    objJPP006.Project = objproj.project;
                    objJPP006.Document = objproj.document;
                    objJPP006.RevNo = objproj.revno;
                    objJPP006.ProcurementMaterialItem = fc["ProcurementMaterialItem0"].ToString();
                    objJPP006.SizeQuantity = fc["SizeQuantity0"].ToString();
                    objJPP006.PONodate = fc["PONodate0"].ToString();
                    objJPP006.Vendor = fc["Vendor0"].ToString();
                    objJPP006.RequiredDatetomeetCDD = Convert.ToDateTime(fc["RequiredDatetomeetCDD0"].ToString());
                    objJPP006.ExpectedDate = Convert.ToDateTime(fc["ExpectedDate0"].ToString());
                    objJPP006.CreatedBy = objClsLoginInfo.UserName;
                    objJPP006.CreatedOn = DateTime.Now;
                    db.JPP006.Add(objJPP006);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CTQMessages.Update.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Invalid Date";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProcurement(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                JPP006 objJPP006 = db.JPP006.Where(x => x.LineId == Id).FirstOrDefault();
                db.JPP006.Remove(objJPP006);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Delete.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetSubContractingPlan(string Project, string Document, string RevNo, int headerid, int lineid)
        {
            JPP007 objJPP001 = new JPP007();
            var isvalid = db.JPP001.Any(x => x.HeaderId == headerid);
            if (isvalid == true)
            {
                Project = Project.Split('-')[0].Trim();
                var projdesc = db.COM001.Where(i => i.t_cprj == Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();

                int Headerid = (from a in db.JPP001
                                where a.Project == Project
                                select a.HeaderId).FirstOrDefault();
                Session["headerid"] = Convert.ToInt32(Headerid);
                ViewBag.Project = Project + " - " + projdesc;
                ViewBag.DocNum = Document;
                ViewBag.DocRev = RevNo;
                ViewBag.Headerid = Headerid;
                ViewBag.Lineid = lineid;
                if (lineid > 0)
                {
                    objJPP001 = db.JPP007.Where(x => x.LineId == lineid).FirstOrDefault();
                }
            }
            return PartialView("_SubcontractingPlanPartial", objJPP001);
        }
        [HttpPost]
        public ActionResult saveSubContractingPlan(FormCollection fc)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                int headerid = Convert.ToInt32(fc["HeaderId0"].ToString());
                var project = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.Project).FirstOrDefault();
                var document = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.Document).FirstOrDefault();
                var revno = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.RevNo).FirstOrDefault();
                JPP007 objJPP007 = new JPP007();
                objJPP007.HeaderId = Convert.ToInt32(fc["HeaderId0"].ToString());
                objJPP007.Project = project;
                objJPP007.Document = document;
                objJPP007.RevNo = revno;
                objJPP007.SubcontractingPlan = fc["SubcontractingPlan0"].ToString();
                objJPP007.Shop = fc["Shop0"].ToString();
                objJPP007.CreatedBy = objClsLoginInfo.UserName;
                objJPP007.CreatedOn = DateTime.Now;
                db.JPP007.Add(objJPP007);
                db.SaveChanges();

                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Insert.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteSubContractingPlan(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                JPP007 objJPP007 = db.JPP007.Where(x => x.LineId == Id).FirstOrDefault();
                db.JPP007.Remove(objJPP007);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Delete.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetSpecialAssemblies(string Project, string Document, string RevNo, int headerid, int lineid)
        {
            JPP008 objJPP001 = new JPP008();
            var isvalid = db.JPP001.Any(x => x.HeaderId == headerid);
            if (isvalid == true)
            {
                Project = Project.Split('-')[0].Trim();
                var projdesc = db.COM001.Where(i => i.t_cprj == Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();

                int Headerid = (from a in db.JPP001
                                where a.Project == Project
                                select a.HeaderId).FirstOrDefault();
                Session["headerid"] = Convert.ToInt32(Headerid);
                ViewBag.Project = Project + " - " + projdesc;
                ViewBag.DocNum = Document;
                ViewBag.DocRev = RevNo;
                ViewBag.Headerid = Headerid;
                ViewBag.Lineid = lineid;
                if (lineid > 0)
                {
                    objJPP001 = db.JPP008.Where(x => x.LineId == lineid).FirstOrDefault();
                }
            }
            return PartialView("_SpecialAssembliesPartial", objJPP001);
        }
        [HttpPost]
        public ActionResult saveSpecialAssemblies(FormCollection fc)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                int headerid = Convert.ToInt32(fc["HeaderId0"].ToString());
                var project = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.Project).FirstOrDefault();
                var document = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.Document).FirstOrDefault();
                var revno = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.RevNo).FirstOrDefault();
                JPP008 objJPP008 = new JPP008();
                objJPP008.HeaderId = Convert.ToInt32(fc["HeaderId0"].ToString());
                objJPP008.Project = project;
                objJPP008.Document = document;
                objJPP008.RevNo = revno;
                objJPP008.SpecialAssembly = fc["SpecialAssembly0"].ToString();
                objJPP008.CreatedBy = objClsLoginInfo.UserName;
                objJPP008.CreatedOn = DateTime.Now;
                db.JPP008.Add(objJPP008);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Insert.ToString();


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteSpecialAssemblies(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                JPP008 objJPP008 = db.JPP008.Where(x => x.LineId == Id).FirstOrDefault();
                db.JPP008.Remove(objJPP008);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Delete.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetdiscussionPoints(string Project, string Document, string RevNo, int headerid, int lineid)
        {
            JPP009 objJPP001 = new JPP009();
            var isvalid = db.JPP001.Any(x => x.HeaderId == headerid);
            if (isvalid == true)
            {
                Project = Project.Split('-')[0].Trim();
                var projdesc = db.COM001.Where(i => i.t_cprj == Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();

                int Headerid = (from a in db.JPP001
                                where a.Project == Project
                                select a.HeaderId).FirstOrDefault();
                Session["headerid"] = Convert.ToInt32(Headerid);
                ViewBag.Project = Project + " - " + projdesc;
                ViewBag.DocNum = Document;
                ViewBag.DocRev = RevNo;
                ViewBag.Headerid = Headerid;
                ViewBag.Lineid = lineid;
                if (lineid > 0)
                {
                    objJPP001 = db.JPP009.Where(x => x.LineId == lineid).FirstOrDefault();
                }
            }
            return PartialView("_DiscussionPointsPartial", objJPP001);
        }
        [HttpPost]
        public ActionResult savediscussionPoints(FormCollection fc)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                int headerid = Convert.ToInt32(fc["HeaderId0"].ToString());
                var project = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.Project).FirstOrDefault();
                var document = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.Document).FirstOrDefault();
                var revno = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.RevNo).FirstOrDefault();
                JPP009 objJPP009 = new JPP009();
                objJPP009.HeaderId = Convert.ToInt32(fc["HeaderId0"].ToString());
                objJPP009.Project = project;
                objJPP009.Document = document;
                objJPP009.RevNo = revno;
                objJPP009.DiscussionPoints = fc["DiscussionPoints0"].ToString();
                objJPP009.CreatedBy = objClsLoginInfo.UserName;
                objJPP009.CreatedOn = DateTime.Now;
                db.JPP009.Add(objJPP009);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Insert.ToString();


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeletediscussionPointsLine(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                JPP009 objJPP009 = db.JPP009.Where(x => x.LineId == Id).FirstOrDefault();
                db.JPP009.Remove(objJPP009);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Delete.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetApproverResult(string term)
        {
            List<ApproverModel> lstApprovers = Manager.GetApproverList("PMG1", objClsLoginInfo.Location, objClsLoginInfo.UserName, term).ToList();
            return Json(lstApprovers, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult verifyApprover(string approver)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                approver = approver.Split('-')[0].Trim();
                var Exists = db.COM003.Any(x => x.t_psno == approver && x.t_actv == 1);
                if (Exists == false)
                {
                    objResponseMsg.Key = false;
                }
                else
                {
                    objResponseMsg.Key = true;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult getHeaderId(string project)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int Headerid = (from a in db.JPP001
                                where a.Project == project
                                select a.HeaderId).FirstOrDefault();
                string status = (from a in db.JPP001
                                 where a.Project == project
                                 select a.Status).FirstOrDefault();
                objResponseMsg.Key = true;
                objResponseMsg.Value = Headerid.ToString() + "|" + status;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult SaveHeader(JPP001 jpp001)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                int hd = Convert.ToInt32(jpp001.HeaderId);
                if (hd > 0)
                {
                    JPP001 objJPP001 = db.JPP001.Where(x => x.HeaderId == hd).FirstOrDefault();
                    if (objJPP001 != null)
                    {
                        objJPP001.EquipmentName = jpp001.EquipmentName;
                        objJPP001.EquipmentNumber = jpp001.EquipmentNumber;
                        //if (objJPP001.Status == clsImplementationEnum.PTMTCTQStatus.Approved.GetStringValue())
                        //{
                        //    objJPP001.RevNo = Convert.ToInt32(objJPP001.RevNo) + 1;
                        //    objJPP001.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                        //}
                        objJPP001.ProjectLocation = jpp001.ProjectLocation;
                        objJPP001.Document = jpp001.Document;
                        if (!jpp001.KOMDate.HasValue || jpp001.KOMDate.ToString() == "01/01/0001")
                        {
                            objJPP001.KOMDate = null;
                        }
                        else
                        {
                            objJPP001.KOMDate = Convert.ToDateTime(jpp001.KOMDate.ToString());
                        }
                        if (!jpp001.ZeroDate.HasValue || jpp001.ZeroDate.ToString() == "01/01/0001")
                        {
                            objJPP001.ZeroDate = null;
                        }
                        else
                        {
                            objJPP001.ZeroDate = Convert.ToDateTime(jpp001.ZeroDate.ToString());
                        }
                        if (!jpp001.CDDDate.HasValue || jpp001.CDDDate.ToString() == "01/01/0001")
                        {
                            objJPP001.CDDDate = null;
                        }
                        else
                        {
                            objJPP001.CDDDate = Convert.ToDateTime(jpp001.CDDDate.ToString());
                        }
                        objJPP001.DeliveryTerms = jpp001.DeliveryTerms;
                        objJPP001.MajorDimension = jpp001.MajorDimension;
                        objJPP001.WeightofEquipment = jpp001.WeightofEquipment;
                        objJPP001.CodeStamping = jpp001.CodeStamping;
                        objJPP001.CodeStampingDesc = jpp001.CodeStampingDesc;
                        objJPP001.Inspection = jpp001.Inspection;
                        objJPP001.DesignManufacturingCodes = jpp001.DesignManufacturingCodes;
                        objJPP001.FabricationHours = jpp001.FabricationHours;
                        objJPP001.McHours = jpp001.McHours;
                        objJPP001.HoursperTon = jpp001.HoursperTon;
                        objJPP001.Throughput = jpp001.Throughput;
                        objJPP001.ThroughputHr = jpp001.ThroughputHr;
                        objJPP001.ManufacturingShops = jpp001.ManufacturingShops;
                        objJPP001.LiquidatedDamages = jpp001.LiquidatedDamages;
                        objJPP001.Bonus = jpp001.Bonus;
                        objJPP001.PerformanceGuarantee = jpp001.PerformanceGuarantee;
                        //if (string.IsNullOrWhiteSpace(jpp001.MilestoneDates.ToString()) || jpp001.MilestoneDates.ToString() == "01/01/0001")
                        //{
                        //    objJPP001.MilestoneDates = null;
                        //}
                        //else
                        //{
                        //    objJPP001.MilestoneDates = jpp001.MilestoneDates;
                        //}
                        objJPP001.MilestoneDates = jpp001.MilestoneDates;
                        objJPP001.Engineering = jpp001.Engineering;
                        objJPP001.FinalDocumentation = jpp001.FinalDocumentation;
                        objJPP001.Transportationfeasibility = jpp001.Transportationfeasibility;
                        objJPP001.EditedBy = objClsLoginInfo.UserName;
                        objJPP001.EditedOn = DateTime.Now;
                        if (!string.IsNullOrWhiteSpace(jpp001.ApprovedBy))
                            objJPP001.ApprovedBy = jpp001.ApprovedBy.Split('-')[0].ToString().Trim();
                        else
                            objJPP001.ApprovedBy = "";
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CTQMessages.Update.ToString();
                        objResponseMsg.status = objJPP001.Status.ToString();
                        objResponseMsg.Revision = "R" + objJPP001.RevNo.ToString();
                        Manager.AddPDN002(objJPP001.HeaderId, objJPP001.Status, objJPP001.RevNo, objJPP001.Project, objJPP001.Document, PlanList.JPP.GetStringValue(), clsImplementationEnum.PlanList.JPP.GetStringValue());
                        Manager.UpdatePDN002(objJPP001.HeaderId, objJPP001.Status, objJPP001.RevNo, objJPP001.Project, objJPP001.Document);
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Process Details not available.";
                    }
                }
                else
                {
                    var project = jpp001.Project.Split('-')[0].ToString().Trim();
                    bool obj = db.JPP001.Where(x => x.Project == project).Any();
                    if (obj == true)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Project already Exists..!";
                    }
                    else
                    {
                        JPP001 objJPP001 = new JPP001();
                        objJPP001.Project = jpp001.Project.Split('-')[0].ToString().Trim();
                        objJPP001.Document = jpp001.Document;
                        objJPP001.Customer = jpp001.Customer.Split('-')[0].ToString().Trim();
                        objJPP001.RevNo = 0;
                        objJPP001.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                        objJPP001.EquipmentName = jpp001.EquipmentName;
                        objJPP001.EquipmentNumber = jpp001.EquipmentNumber;
                        objJPP001.ProjectLocation = jpp001.ProjectLocation;
                        if (!jpp001.KOMDate.HasValue || jpp001.KOMDate.ToString() == "01/01/0001")
                        {
                            objJPP001.KOMDate = null;
                        }
                        else
                        {
                            objJPP001.KOMDate = Convert.ToDateTime(jpp001.KOMDate.ToString());
                        }
                        if (!jpp001.ZeroDate.HasValue || jpp001.ZeroDate.ToString() == "01/01/0001")
                        {
                            objJPP001.ZeroDate = null;
                        }
                        else
                        {
                            objJPP001.ZeroDate = Convert.ToDateTime(jpp001.ZeroDate.ToString());
                        }
                        if (!jpp001.CDDDate.HasValue || jpp001.CDDDate.ToString() == "01/01/0001")
                        {
                            objJPP001.CDDDate = null;
                        }
                        else
                        {
                            objJPP001.CDDDate = Convert.ToDateTime(jpp001.CDDDate.ToString());
                        }
                        objJPP001.DeliveryTerms = jpp001.DeliveryTerms;
                        objJPP001.MajorDimension = jpp001.MajorDimension;
                        objJPP001.WeightofEquipment = jpp001.WeightofEquipment;
                        objJPP001.CodeStamping = jpp001.CodeStamping;
                        objJPP001.HoursperTon = jpp001.HoursperTon;
                        objJPP001.CodeStampingDesc = jpp001.CodeStampingDesc;
                        objJPP001.Inspection = jpp001.Inspection;
                        objJPP001.DesignManufacturingCodes = jpp001.DesignManufacturingCodes;
                        objJPP001.FabricationHours = jpp001.FabricationHours;
                        objJPP001.McHours = jpp001.McHours;
                        objJPP001.Throughput = jpp001.Throughput;
                        objJPP001.ThroughputHr = jpp001.ThroughputHr;
                        objJPP001.ManufacturingShops = jpp001.ManufacturingShops;
                        objJPP001.LiquidatedDamages = jpp001.LiquidatedDamages;
                        objJPP001.Bonus = jpp001.Bonus;
                        objJPP001.PerformanceGuarantee = jpp001.PerformanceGuarantee;
                        //if (string.IsNullOrWhiteSpace(jpp001.MilestoneDates.ToString()) || jpp001.MilestoneDates.ToString() == "01/01/0001")
                        //{
                        //    objJPP001.MilestoneDates = null;
                        //}
                        //else
                        //{
                        //    objJPP001.MilestoneDates = jpp001.MilestoneDates;
                        //}
                        objJPP001.MilestoneDates = jpp001.MilestoneDates;
                        objJPP001.Engineering = jpp001.Engineering;
                        objJPP001.FinalDocumentation = jpp001.FinalDocumentation;
                        objJPP001.Transportationfeasibility = jpp001.Transportationfeasibility;
                        if (!string.IsNullOrWhiteSpace(jpp001.ApprovedBy))
                            objJPP001.ApprovedBy = jpp001.ApprovedBy.Split('-')[0].ToString().Trim();
                        else
                            objJPP001.ApprovedBy = "";
                        objJPP001.CreatedBy = objClsLoginInfo.UserName;
                        objJPP001.CreatedOn = DateTime.Now;
                        db.JPP001.Add(objJPP001);
                        db.SaveChanges();

                        #region Payment Terms Lines
                        if (objJPP001.Project != null)
                        {
                            AutoPopulateList lstpaymentterms = new AutoPopulateList();
                            lstpaymentterms = lstpaymentTermlines(objJPP001.Project);
                            if (lstpaymentterms.Key != false)
                            {
                                if (lstpaymentterms.lstpayment.Count() > 0)
                                {
                                    List<JPP004> lstjpp004 = new List<JPP004>();
                                    var objjpp001 = db.JPP001.Where(x => x.HeaderId == objJPP001.HeaderId).FirstOrDefault();
                                    if (objjpp001 != null)
                                    {
                                        foreach (var item in lstpaymentterms.lstpayment)
                                        {
                                            if (item.MSNo != null && item.Description != null && item.Percentage != null)
                                            {
                                                JPP004 objJPP004 = new JPP004();
                                                objJPP004.HeaderId = objJPP001.HeaderId;
                                                objJPP004.Project = objJPP001.Project;
                                                objJPP004.Document = objjpp001.Document;
                                                objJPP004.RevNo = objjpp001.RevNo;
                                                objJPP004.MSNo = item.MSNo;
                                                objJPP004.percentage = Convert.ToDecimal(item.Percentage);
                                                objJPP004.Discription = item.Description;
                                                objJPP004.CreatedBy = objClsLoginInfo.UserName;
                                                objJPP004.CreatedOn = DateTime.Now;
                                                lstjpp004.Add(objJPP004);
                                            }
                                        }
                                    }

                                    if (lstjpp004.Count() > 0)
                                    {
                                        db.JPP004.AddRange(lstjpp004);
                                        db.SaveChanges();
                                    }
                                }
                            }
                        }
                        #endregion

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CTQMessages.Update.ToString();
                        objResponseMsg.status = objJPP001.Status.ToString();
                        objResponseMsg.Revision = "R" + objJPP001.RevNo.ToString();
                        Manager.AddPDN002(objJPP001.HeaderId, objJPP001.Status, objJPP001.RevNo, objJPP001.Project, objJPP001.Document, PlanList.JPP.GetStringValue(), clsImplementationEnum.PlanList.JPP.GetStringValue());
                        Manager.UpdatePDN002(objJPP001.HeaderId, objJPP001.Status, objJPP001.RevNo, objJPP001.Project, objJPP001.Document);
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please Enter Proper Data..!";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetHeaderData(string project)
        {
            AutoPopulateList objResponseMsg = new AutoPopulateList();
            try
            {
                var ExistsPrject = db.COM001.Any(x => x.t_cprj == project);
                if (ExistsPrject == false)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Invalid Project..! Please Try Again..!";
                }
                else
                {
                    bool obj = db.JPP001.Where(x => x.Project == project).Any();
                    if (obj == true)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Project already Exists..!";
                    }
                    else
                    {
                        AutoPopulateList lstautopopulate = GetUserAccessRights(project);
                        if (lstautopopulate.Key != false)
                        {
                            objResponseMsg.tempZERO = Convert.ToString(lstautopopulate.lstchildAutoPopulate.Zero);
                            objResponseMsg.tempCDD = Convert.ToString(lstautopopulate.lstchildAutoPopulate.CDD);
                            objResponseMsg.lstchildAutoPopulate = lstautopopulate.lstchildAutoPopulate;
                        }

                        int RevNum, docNum;
                        string customer = Manager.GetCustomerCodeAndNameByProject(project);
                        var Exists = db.TLP001.Any(x => x.RevNo == null && x.Project == project);
                        if (Exists == false)
                        {
                            RevNum = 0;
                        }
                        else
                        {
                            int? revNo = (from rev in db.JPP001
                                          where rev.Project == project
                                          select rev.RevNo).FirstOrDefault();
                            RevNum = Convert.ToInt32(revNo) + 1;
                        }

                        docNum = 01;
                        project = project.Split('-')[0].ToString().Trim();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = customer + "|" + RevNum + "|" + docNum;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #region observation 15966 default entry form ln server
        public AutoPopulateList GetUserAccessRights(string project)
        {
            AutoPopulateList lstautopopulate = new AutoPopulateList();
            lstautopopulate.Key = true;
            try
            {
                //string query1 = "select d.t_cprj [Project], a.t_efdt[Zero Date], c.t_ccdd [CDD], b.t_mlsn [MS No.], b.t_dsca [Description], b.t_perc [Percentage], c.t_lqdm [LD], c.t_bons [Bonus], c.t_ctpi [TPI] from " + LNLinkedServer + ".dbo.ttpctm100175 a with(nolock) left join " + LNLinkedServer + ".dbo.tltctm111175 b with(nolock) on a.t_cprj = b.t_cprj left join " + LNLinkedServer + ".dbo.tltctm100175 c with(nolock) on a.t_cono = c.t_cono left join " + LNLinkedServer + ".dbo.ttppdm600175 d with(nolock) on a.t_cprj = d.t_mprj where d.t_cprj ='" + project + "'";
                string query1 = "select a.t_efdt[Zero], c.t_ccdd [CDD], c.t_lqdm [LD], c.t_bons [Bonus], c.t_ctpi [TPI] from " + LNLinkedServer + ".dbo.ttpctm100175 a with(nolock) left join " + LNLinkedServer + ".dbo.tltctm111175 b with(nolock) on a.t_cono = b.t_cono left join " + LNLinkedServer + ".dbo.tltctm100175 c with(nolock) on a.t_cono = c.t_cono left join " + LNLinkedServer + ".dbo.ttppdm600175 d with(nolock) on a.t_cprj = d.t_mprj where d.t_cprj ='" + project + "'";
                lstautopopulate.lstchildAutoPopulate = db.Database.SqlQuery<lstchildAutoPopulate>(query1).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                lstautopopulate.Key = false;
            }
            return lstautopopulate;
        }

        public AutoPopulateList lstpaymentTermlines(string project)
        {
            AutoPopulateList lstpayment = new AutoPopulateList();
            lstpayment.Key = true;
            try
            {
                string query1 = "select b.t_mlsn [MSNo], b.t_dsca [Description], b.t_perc [Percentage] from " + LNLinkedServer + ".dbo.ttpctm100175 a with(nolock) left join " + LNLinkedServer + ".dbo.tltctm111175 b with(nolock) on a.t_cono = b.t_cono left join " + LNLinkedServer + ".dbo.tltctm100175 c with(nolock) on a.t_cono = c.t_cono left join " + LNLinkedServer + ".dbo.ttppdm600175 d with(nolock) on a.t_cprj = d.t_mprj where d.t_cprj ='" + project + "'";
                lstpayment.lstpayment = db.Database.SqlQuery<lstPaymentTerms>(query1).ToList();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                lstpayment.Key = false;
            }
            return lstpayment;
        }

        #endregion

        [HttpPost]
        public ActionResult LoadfrmDPLines(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                var IsDisplayOnly = !string.IsNullOrEmpty(Request["IsDisplayOnly"]) ? Convert.ToBoolean(Request["IsDisplayOnly"].ToString()) : false;

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                string strSortOrder = string.Empty;
                int headerid = Convert.ToInt32(param.Headerid);
                Session["Headerid"] = Convert.ToInt32(param.Headerid);
                var project = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.Project).FirstOrDefault();
                var document = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.Document).FirstOrDefault();
                var revno = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.RevNo).FirstOrDefault();
                var status = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.Status).FirstOrDefault();
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere = " (Project like '%" + param.sSearch
                       + "%' or Document like '%" + param.sSearch
                       + "%' or RevNo like '%" + param.sSearch
                       + "%' or DiscussionPoints like '%" + param.sSearch
                       + "%')";
                }
                else
                {
                    strWhere = "1=1";
                }
                var lstResult = db.SP_JPP_DiscussionPoints
                                (
                                StartIndex, EndIndex, strSortOrder, headerid, strWhere
                                ).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                                    "",
                                    Helper.GenerateHidden(newRecordId, "LineId", ""),
                                    Helper.GenerateHidden(newRecordId, "HeaderId", headerid.ToString()),
                                     Convert.ToString(project),
                                    Convert.ToString(document),
                                    Convert.ToString(revno),
                                    Helper.GenerateTextbox(newRecordId, "DiscussionPoints" , "","",false,"","100"),
                                    Helper.GenerateGridButton(newRecordId, "Add", "Save Rocord", "fa fa-save", "SaveNewRecord9();" ),
                                };
                var data = (from uc in lstResult
                            select new[]
                            {
                               Convert.ToString(uc.ROW_NO),
                               Helper.GenerateHidden(uc.LineId, "LineId", Convert.ToString(uc.LineId)),
                               Helper.GenerateHidden(uc.HeaderId, "HeaderId", Convert.ToString(uc.HeaderId)),
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.Document),
                               Convert.ToString("R" + uc.RevNo),
                               Helper.GenerateTextbox(uc.LineId, "DiscussionPoints",uc.DiscussionPoints
                               , "UpdateLineDetails9(this, "+ uc.LineId +");"
                               ,(status != clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue() ? true : (!IsDisplayOnly ? true: false)), "" , "100"),
                               Helper.GenerateGridButton(uc.LineId, "Delete", "Delete Rocord", "fa fa-trash-o", "DeleteRecord9("+ uc.LineId +");"),
                           }).ToList();

                data.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult LoadfrmDocLines(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                var IsDisplayOnly = !string.IsNullOrEmpty(Request["IsDisplayOnly"]) ? Convert.ToBoolean(Request["IsDisplayOnly"].ToString()) : false;

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                string strSortOrder = string.Empty;
                int headerid = Convert.ToInt32(param.Headerid);
                Session["Headerid"] = Convert.ToInt32(param.Headerid);
                var project = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.Project).FirstOrDefault();
                var document = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.Document).FirstOrDefault();
                var revno = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.RevNo).FirstOrDefault();
                var status = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.Status).FirstOrDefault();

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere = " (Project like '%" + param.sSearch
                       + "%' or Document like '%" + param.sSearch
                       + "%' or RevNo like '%" + param.sSearch
                       + "%' or DocumentsDrawingDescription like '%" + param.sSearch
                       + "%' or RequiredDate like '%" + param.sSearch
                       + "%' or ExpectedDate like '%" + param.sSearch
                       + "%')";
                }
                else
                {
                    strWhere = "1=1";
                }
                var lstResult = db.SP_JPP_GET_Documents
                                (
                                StartIndex, EndIndex, strSortOrder, headerid, strWhere
                                ).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                                    "",
                                    Helper.GenerateHidden(newRecordId, "LineId", ""),
                                    Helper.GenerateHidden(newRecordId, "HeaderId", headerid.ToString()),
                                    Convert.ToString(project),
                                    Convert.ToString(document),
                                    Convert.ToString(revno),
                                    Helper.GenerateTextbox(newRecordId, "DocumentsDrawingDescription" , "","",false,"","200"),
                                    Helper.GenerateTextbox(newRecordId, "RequiredDate"),
                                    Helper.GenerateTextbox(newRecordId, "ExpectedDate"),
                                    Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveNewRecord5();" ),
                                };
                var data = (from uc in lstResult
                            select new[]
                            {
                               //Convert.ToString(uc.ROW_NO),
                               Helper.GenerateNumericTextbox(uc.LineId, "SeqNo", Convert.ToString(uc.SeqNo)
                               , "UpdateLineDetails5(this, " + uc.LineId + ");"
                               ,(status != clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue() ? true : (!IsDisplayOnly ? true: false)),"",false , "2","","intnumber"),
                               Helper.GenerateHidden(uc.LineId, "LineId", Convert.ToString(uc.LineId)),
                               Helper.GenerateHidden(uc.HeaderId, "HeaderId", Convert.ToString(uc.HeaderId)),
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.Document),
                               Convert.ToString("R" + uc.RevNo),
                               Helper.GenerateTextbox(uc.LineId, "DocumentsDrawingDescription",uc.DocumentsDrawingDescription
                               , "UpdateLineDetails5(this, "+ uc.LineId +");"
                               , (status != clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue() ? true : (!IsDisplayOnly ? true: false)), "" , "200"),

                               Helper.GenerateTextbox(uc.LineId, "RequiredDate"
                               , Convert.ToDateTime(uc.RequiredDate).ToString("yyyy-MM-dd")
                               , "UpdateLineDetails5(this, "+ uc.LineId +");"
                               , (status != clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue() ? true : (!IsDisplayOnly ? true: false))),
                               Helper.GenerateTextbox(uc.LineId, "ExpectedDate"
                               ,Convert.ToDateTime(uc.ExpectedDate).ToString("yyyy-MM-dd")
                               , "UpdateLineDetails5(this, "+ uc.LineId +");"
                               , (status != clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue() ? true : (!IsDisplayOnly ? true: false))),
                               Helper.GenerateGridButton(uc.LineId, "Delete", "Delete Rocord", "fa fa-trash-o", "DeleteRecord5("+ uc.LineId +");"),
                           }).ToList();

                data.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }
        [HttpPost]
        public ActionResult UpdateData2(string rowId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                db.SP_JPPLines_UPDATE(Convert.ToInt32(rowId), "JPP002", columnName, columnValue);
                objResponseMsg.Key = true;


            }
            catch
            {
                objResponseMsg.Key = false;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult UpdateData3(string rowId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {

                db.SP_JPPLines_UPDATE(Convert.ToInt32(rowId), "JPP003", "SpecificCustomerRequirementSalientFeatures", columnValue);
                objResponseMsg.Key = true;


            }
            catch
            {
                objResponseMsg.Key = false;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult UpdateData4(string rowId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                db.SP_JPPLines_UPDATE(Convert.ToInt32(rowId), "JPP004", columnName, columnValue);
                objResponseMsg.Key = true;
            }
            catch
            {
                objResponseMsg.Key = false;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult UpdateData5(string rowId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (columnName == "RequiredDate")
                {
                    if (CheckDate(columnValue))
                    {
                        db.SP_JPPLines_UPDATE(Convert.ToInt32(rowId), "JPP005", columnName, columnValue);
                        objResponseMsg.Key = true;
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Invalid Date";
                    }
                }

                else if (columnName == "ExpectedDate")
                {
                    if (CheckDate(columnValue))
                    {
                        db.SP_JPPLines_UPDATE(Convert.ToInt32(rowId), "JPP005", columnName, columnValue);
                        objResponseMsg.Key = true;
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Invalid Date";
                    }
                }
                else
                {
                    db.SP_JPPLines_UPDATE(Convert.ToInt32(rowId), "JPP005", columnName, columnValue);
                    objResponseMsg.Key = true;
                }
            }

            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult UpdateData6(string rowId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (columnName == "RequiredDatetomeetCDD")
                {
                    if (CheckDate(columnValue))
                    {
                        db.SP_JPPLines_UPDATE(Convert.ToInt32(rowId), "JPP006", columnName, columnValue);
                        objResponseMsg.Key = true;
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Invalid Date";
                    }
                }

                else if (columnName == "ExpectedDate")
                {
                    if (CheckDate(columnValue))
                    {
                        db.SP_JPPLines_UPDATE(Convert.ToInt32(rowId), "JPP006", columnName, columnValue);
                        objResponseMsg.Key = true;
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Invalid Date";
                    }
                }
                else
                {
                    db.SP_JPPLines_UPDATE(Convert.ToInt32(rowId), "JPP006", columnName, columnValue);
                    objResponseMsg.Key = true;
                }
            }

            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult UpdateData7(string rowId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                db.SP_JPPLines_UPDATE(Convert.ToInt32(rowId), "JPP007", columnName, columnValue);
                objResponseMsg.Key = true;


            }
            catch
            {
                objResponseMsg.Key = false;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult UpdateData8(string rowId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                db.SP_JPPLines_UPDATE(Convert.ToInt32(rowId), "JPP008", columnName, columnValue);
                objResponseMsg.Key = true;


            }
            catch
            {
                objResponseMsg.Key = false;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult UpdateData9(string rowId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                db.SP_JPPLines_UPDATE(Convert.ToInt32(rowId), "JPP009", columnName, columnValue);
                objResponseMsg.Key = true;


            }
            catch
            {
                objResponseMsg.Key = false;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        protected bool CheckDate(String date)
        {
            try
            {
                DateTime dt = DateTime.Parse(date);
                return true;
            }
            catch
            {
                return false;
            }
        }
        [HttpPost]
        public ActionResult LoadfrmMMCLines(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                var IsDisplayOnly = !string.IsNullOrEmpty(Request["IsDisplayOnly"]) ? Convert.ToBoolean(Request["IsDisplayOnly"].ToString()) : false;

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                string strSortOrder = string.Empty;
                int headerid = Convert.ToInt32(param.Headerid);
                Session["Headerid"] = Convert.ToInt32(param.Headerid);
                var project = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.Project).FirstOrDefault();
                var document = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.Document).FirstOrDefault();
                var revno = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.RevNo).FirstOrDefault();
                var status = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.Status).FirstOrDefault();
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere = " (Project like '%" + param.sSearch
                       + "%' or Document like '%" + param.sSearch
                       + "%' or RevNo like '%" + param.sSearch
                       + "%' or DocumentsDrawingDescription like '%" + param.sSearch
                       + "%')";
                }
                else
                {
                    strWhere = "1=1";
                }
                var lstResult = db.SP_JPP_GET_MajorMaterialConstruction
                                (
                                StartIndex, EndIndex, strSortOrder, headerid, strWhere
                                ).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                                    "",
                                    Helper.GenerateHidden(newRecordId, "LineId", ""),
                                    Helper.GenerateHidden(newRecordId, "HeaderId", headerid.ToString()),
                                    Convert.ToString(project),
                                    Convert.ToString(document),
                                    Convert.ToString("R" + revno),
                                    Helper.GenerateTextbox(newRecordId, "MajorMaterialofConstruction", "","",false,"","200"),
                                    Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveNewRecord2();" ),
                                };
                var data = (from uc in lstResult
                            select new[]
                            {
                               Convert.ToString(uc.ROW_NO),
                               Helper.GenerateHidden(uc.LineId, "LineId", Convert.ToString(uc.LineId)),
                               Helper.GenerateHidden(uc.HeaderId, "HeaderId", Convert.ToString(uc.HeaderId)),
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.Document),
                               Convert.ToString("R" + uc.RevNo),
                               Helper.GenerateTextbox(uc.LineId, "MajorMaterialofConstruction",uc.MajorMaterialofConstruction
                               , "UpdateLineDetails2(this, "+ uc.LineId +");"
                               , (status != clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue() ? true : (!IsDisplayOnly ? true: false)), "" , "200"),
                               Helper.GenerateGridButton(uc.LineId, "Delete", "Delete Rocord", "fa fa-trash-o", "DeleteRecord2("+ uc.LineId +");"),
                           }).ToList();

                data.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult LoadfrmPTLines(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                var IsDisplayOnly = !string.IsNullOrEmpty(Request["IsDisplayOnly"]) ? Convert.ToBoolean(Request["IsDisplayOnly"].ToString()) : false;

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                string strSortOrder = string.Empty;
                string status = "";
                int headerid = Convert.ToInt32(param.Headerid);
                Session["Headerid"] = Convert.ToInt32(param.Headerid);
                var OBJJPP001 = db.JPP001.Where(i => i.HeaderId == headerid).FirstOrDefault();
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere = " (Project like '%" + param.sSearch
                       + "%' or Document like '%" + param.sSearch
                       + "%' or RevNo like '%" + param.sSearch
                       + "%' or PaymentTerms like '%" + param.sSearch
                       + "%' or MSNo like '%" + param.sSearch
                       + "%' or Discription like '%" + param.sSearch
                       + "%' or percentage like '%" + param.sSearch
                       + "%')";
                }
                else
                {
                    strWhere = "1=1";
                }
                var lstResult = db.SP_JPP_GET_PaymentTerms
                                (
                                StartIndex, EndIndex, strSortOrder, headerid, strWhere
                                ).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                                    "",
                                    Helper.GenerateHidden(newRecordId, "LineId", ""),
                                    Helper.GenerateHidden(newRecordId, "HeaderId", headerid.ToString()),
                                    Convert.ToString(OBJJPP001!= null?OBJJPP001.Project:""),
                                    Convert.ToString(OBJJPP001!= null?OBJJPP001.Document:""),
                                    Convert.ToString(OBJJPP001!= null ? "R"+OBJJPP001.RevNo : "R"),
                                    Helper.GenerateTextbox(newRecordId, "PaymentTerms" , "","",false,"","215"),
                                    Helper.GenerateNumericTextbox(newRecordId, "MSNo", "", "",false,"",false , "2","","intnumber"),
                                    Helper.GenerateTextbox(newRecordId, "Discription" , "","",false,"","215"),
                                    Helper.GenerateNumericTextbox(newRecordId, "percentage", "", "",false,"",false , "6","","numeric-only"),
                                    Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveNewRecord4();" ),
                                };
                var data = (from uc in lstResult
                            select new[]
                            {
                               Convert.ToString(uc.ROW_NO),
                               Helper.GenerateHidden(uc.LineId, "LineId", Convert.ToString(uc.LineId)),
                               Helper.GenerateHidden(uc.HeaderId, "HeaderId", Convert.ToString(uc.HeaderId)),
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.Document),
                               Convert.ToString("R" + uc.RevNo),
                               Helper.GenerateTextbox(uc.LineId, "PaymentTerms",uc.PaymentTerms, "UpdateLineDetails4(this, "+ uc.LineId +");",(OBJJPP001 != null ? (OBJJPP001.Status == clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue()? true : (!IsDisplayOnly ? true: false)): false) , "" , "215"),
                               Helper.GenerateNumericTextbox(uc.LineId, "MSNo", Convert.ToString(uc.MSNo), "UpdateLineDetails4(this, " + uc.LineId + ");",(OBJJPP001 != null ? (OBJJPP001.Status == clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue() ? true : false): false),"",(!IsDisplayOnly ? true: false) , "2","","intnumber"),
                               Helper.GenerateTextbox(uc.LineId, "Discription",uc.Discription, "UpdateLineDetails4(this, "+ uc.LineId +");",(OBJJPP001 != null ? (OBJJPP001.Status == clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue() ? true : (!IsDisplayOnly ? true: false)): false) , "" , "215"),
                               Helper.GenerateNumericTextbox(uc.LineId, "percentage", Convert.ToString(uc.percentage), "UpdateLineDetails4(this, " + uc.LineId + ");",(OBJJPP001 != null ? (OBJJPP001.Status == clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue() ? true : false): false),"",(!IsDisplayOnly ? true: false) , "6","","numeric-only"),
                               Helper.GenerateGridButton(uc.LineId, "Delete", "Delete Rocord", "fa fa-trash-o", "DeleteRecord4("+ uc.LineId +");"),
                           }).ToList();

                data.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult LoadfrmProcLines(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                var IsDisplayOnly = !string.IsNullOrEmpty(Request["IsDisplayOnly"]) ? Convert.ToBoolean(Request["IsDisplayOnly"].ToString()) : false;

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                string strSortOrder = string.Empty;
                int headerid = Convert.ToInt32(param.Headerid);
                Session["Headerid"] = Convert.ToInt32(param.Headerid);
                var project = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.Project).FirstOrDefault();
                var document = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.Document).FirstOrDefault();
                var revno = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.RevNo).FirstOrDefault();
                var status = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.Status).FirstOrDefault();
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere = " (Project like '%" + param.sSearch
                       + "%' or Document like '%" + param.sSearch
                       + "%' or RevNo like '%" + param.sSearch
                       + "%' or ProcurementMaterialItem like '%" + param.sSearch
                       + "%' or SizeQuantity like '%" + param.sSearch
                       + "%' or RequiredDatetomeetCDD like '%" + param.sSearch
                       + "%' or PONodate like '%" + param.sSearch
                       + "%' or Vendor like '%" + param.sSearch
                       + "%' or ExpectedDate like '%" + param.sSearch
                       + "%')";
                }
                else
                {
                    strWhere = "1=1";
                }
                var lstResult = db.SP_JPP_GET_Procurement
                                (
                                StartIndex, EndIndex, strSortOrder, headerid, strWhere
                                ).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                    "",
                                    Helper.GenerateHidden(newRecordId, "LineId", ""),
                                    Helper.GenerateHidden(newRecordId, "HeaderId", headerid.ToString()),
                                    Convert.ToString(project),
                                    Convert.ToString(document),
                                    Convert.ToString(revno),
                                    Helper.GenerateTextbox(newRecordId, "ProcurementMaterialItem" , "","",false,"","100"),
                                    Helper.GenerateTextbox(newRecordId, "SizeQuantity" , "","",false,"","50"),
                                    Helper.GenerateTextbox(newRecordId, "RequiredDatetomeetCDD"),
                                    Helper.GenerateTextbox(newRecordId, "PONodate" , "","",false,"","30"),
                                    Helper.GenerateTextbox(newRecordId, "Vendor" , "","",false,"","30"),
                                    Helper.GenerateTextbox(newRecordId, "ExpectedDate"),
                                    Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveNewRecord6();" ),
                                };
                var data = (from uc in lstResult
                            select new[]
                            {
                                  //Convert.ToString(uc.ROW_NO),
                                  Helper.GenerateNumericTextbox(uc.LineId, "SeqNo", Convert.ToString(uc.SeqNo)
                               , "UpdateLineDetails6(this, " + uc.LineId + ");"
                               ,(status != clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue() ? true : (!IsDisplayOnly ? true: false)),"",false , "2","","intnumber"),
                               Helper.GenerateHidden(uc.LineId, "LineId", Convert.ToString(uc.LineId)),
                               Helper.GenerateHidden(uc.HeaderId, "HeaderId", Convert.ToString(uc.HeaderId)),
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.Document),
                               Convert.ToString("R" + uc.RevNo),
                               Helper.GenerateTextbox(uc.LineId, "ProcurementMaterialItem", uc.ProcurementMaterialItem
                               , "UpdateLineDetails6(this, "+ uc.LineId +");"
                               ,(status != clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue() ? true : (!IsDisplayOnly ? true: false)), "" , "100"),

                               Helper.GenerateTextbox(uc.LineId, "SizeQuantity", uc.SizeQuantity
                               , "UpdateLineDetails6(this, "+ uc.LineId +");"
                               ,(status != clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue() ? true : (!IsDisplayOnly ? true: false)), "" , "50"),

                               Helper.GenerateTextbox(uc.LineId, "RequiredDatetomeetCDD", Convert.ToDateTime(uc.RequiredDatetomeetCDD).ToString("yyyy-MM-dd")
                               , "UpdateLineDetails6(this, "+ uc.LineId +");"
                               ,(status != clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue() ? true : (!IsDisplayOnly ? true: false))),

                               Helper.GenerateTextbox(uc.LineId, "PONodate", uc.PONodate , "UpdateLineDetails6(this, "+ uc.LineId +");"
                               ,(status != clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue() ? true : (!IsDisplayOnly ? true: false)), "" , "30"),

                               Helper.GenerateTextbox(uc.LineId, "Vendor", uc.Vendor , "UpdateLineDetails6(this, "+ uc.LineId +");"
                               ,(status != clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue() ? true : (!IsDisplayOnly ? true: false)), "" , "30"),

                               Helper.GenerateTextbox(uc.LineId, "ExpectedDate",  Convert.ToDateTime(uc.ExpectedDate).ToString("yyyy-MM-dd")
                               , "UpdateLineDetails6(this, "+ uc.LineId +");"
                               ,(status != clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue() ? true : (!IsDisplayOnly ? true: false))),

                               Helper.GenerateGridButton(uc.LineId, "Delete", "Delete Rocord", "fa fa-trash-o", "DeleteRecord6("+ uc.LineId +");"),
                           }).ToList();

                data.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult LoadfrmSALines(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                var IsDisplayOnly = !string.IsNullOrEmpty(Request["IsDisplayOnly"]) ? Convert.ToBoolean(Request["IsDisplayOnly"].ToString()) : false;

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                string strSortOrder = string.Empty;
                int headerid = Convert.ToInt32(param.Headerid);
                Session["Headerid"] = Convert.ToInt32(param.Headerid);
                var project = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.Project).FirstOrDefault();
                var document = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.Document).FirstOrDefault();
                var revno = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.RevNo).FirstOrDefault();
                var status = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.Status).FirstOrDefault();
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere = " (Project like '%" + param.sSearch
                       + "%' or Document like '%" + param.sSearch
                       + "%' or RevNo like '%" + param.sSearch
                       + "%' or SpecialAssembly like '%" + param.sSearch
                       + "%')";
                }
                else
                {
                    strWhere = "1=1";
                }
                var lstResult = db.SP_JPP_GET_SpecialAssemblies
                                (
                                StartIndex, EndIndex, strSortOrder, headerid, strWhere
                                ).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                    "",
                                    Helper.GenerateHidden(newRecordId, "LineId", ""),
                                    Helper.GenerateHidden(newRecordId, "HeaderId", headerid.ToString()),
                                   Convert.ToString(project),
                                    Convert.ToString(document),
                                    Convert.ToString(revno),
                                    Helper.GenerateTextbox(newRecordId, "SpecialAssembly" , "","",false,"","200"),
                                    Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveNewRecord8();" ),
                                };
                var data = (from uc in lstResult
                            select new[]
                            {
                                  Convert.ToString(uc.ROW_NO),
                               Helper.GenerateHidden(uc.LineId, "LineId", Convert.ToString(uc.LineId)),
                               Helper.GenerateHidden(uc.HeaderId, "HeaderId", Convert.ToString(uc.HeaderId)),
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.Document),
                               Convert.ToString("R" + uc.RevNo),
                               Helper.GenerateTextbox(uc.LineId, "SpecialAssembly",uc.SpecialAssembly
                               , "UpdateLineDetails8(this, "+ uc.LineId +");"
                               ,(status != clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue() ? true : (!IsDisplayOnly ? true: false)), "" , "200"),
                               Helper.GenerateGridButton(uc.LineId, "Delete", "Delete Rocord", "fa fa-trash-o", "DeleteRecord8("+ uc.LineId +");"),
                           }).ToList();

                data.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult LoadfrmSCRLines(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                var IsDisplayOnly = !string.IsNullOrEmpty(Request["IsDisplayOnly"]) ? Convert.ToBoolean(Request["IsDisplayOnly"].ToString()) : false;

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                string strSortOrder = string.Empty;
                int headerid = Convert.ToInt32(param.Headerid);
                Session["Headerid"] = Convert.ToInt32(param.Headerid);
                var project = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.Project).FirstOrDefault();
                var document = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.Document).FirstOrDefault();
                var revno = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.RevNo).FirstOrDefault();
                var status = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.Status).FirstOrDefault();
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere = " (Project like '%" + param.sSearch
                       + "%' or Document like '%" + param.sSearch
                       + "%' or RevNo like '%" + param.sSearch
                       + "%' or MajorMaterialofConstruction like '%" + param.sSearch
                       + "%')";
                }
                else
                {
                    strWhere = "1=1";
                }
                var lstResult = db.SP_JPP_GET_SpecificCustomerRequirement
                                (
                                StartIndex, EndIndex, strSortOrder, headerid, strWhere
                                ).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                    "",
                                    Helper.GenerateHidden(newRecordId, "LineId", ""),
                                    Helper.GenerateHidden(newRecordId, "HeaderId", headerid.ToString()),
                                    Convert.ToString(project),
                                    Convert.ToString(document),
                                    Convert.ToString("R" + revno),
                                    Helper.GenerateTextbox(newRecordId, "MajorMaterialofConstruction" , "","",false,"","200"),
                                    Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveNewRecord3();" ),
                                };
                var data = (from uc in lstResult
                            select new[]
                            {
                                  Convert.ToString(uc.ROW_NO),
                               Helper.GenerateHidden(uc.LineId, "LineId", Convert.ToString(uc.LineId)),
                               Helper.GenerateHidden(uc.HeaderId, "HeaderId", Convert.ToString(uc.HeaderId)),
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.Document),
                               Convert.ToString("R" + uc.RevNo),
                               Helper.GenerateTextbox(uc.LineId, "MajorMaterialofConstruction",uc.MajorMaterialofConstruction
                               , "UpdateLineDetails3(this, "+ uc.LineId +");"
                               ,(status != clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue() ? true : (!IsDisplayOnly ? true: false)), "" , "200"),
                               Helper.GenerateGridButton(uc.LineId, "Delete", "Delete Rocord", "fa fa-trash-o", "DeleteRecord3("+ uc.LineId +");"),
                           }).ToList();

                data.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult LoadfrmSCPLines(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                var IsDisplayOnly = !string.IsNullOrEmpty(Request["IsDisplayOnly"]) ? Convert.ToBoolean(Request["IsDisplayOnly"].ToString()) : false;

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                string strSortOrder = string.Empty;
                int headerid = Convert.ToInt32(param.Headerid);
                Session["Headerid"] = Convert.ToInt32(param.Headerid);
                var project = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.Project).FirstOrDefault();
                var document = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.Document).FirstOrDefault();
                var revno = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.RevNo).FirstOrDefault();
                var status = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.Status).FirstOrDefault();
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere = " (Project like '%" + param.sSearch
                       + "%' or Document like '%" + param.sSearch
                       + "%' or RevNo like '%" + param.sSearch
                       + "%' or SubcontractingPlan like '%" + param.sSearch
                       + "%')";
                }
                else
                {
                    strWhere = "1=1";
                }
                var lstResult = db.SP_JPP_GET_SubcontractingPlan
                                (
                                StartIndex, EndIndex, strSortOrder, headerid, strWhere
                                ).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                                    "",
                                    Helper.GenerateHidden(newRecordId, "LineId", ""),
                                    Helper.GenerateHidden(newRecordId, "HeaderId", headerid.ToString()),
                                    Convert.ToString(project),
                                    Convert.ToString(document),
                                    Convert.ToString(revno),
                                    Helper.GenerateTextbox(newRecordId, "SubcontractingPlan" , "","",false,"","200"),
                                     Helper.GenerateTextbox(newRecordId, "Shop" , "","",false,"","50"),
                                    Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveNewRecord7();" ),
                                };
                var data = (from uc in lstResult
                            select new[]
                            {
                               Convert.ToString(uc.ROW_NO),
                               Helper.GenerateHidden(uc.LineId, "LineId", Convert.ToString(uc.LineId)),
                               Helper.GenerateHidden(uc.HeaderId, "HeaderId", Convert.ToString(uc.HeaderId)),
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.Document),
                               Convert.ToString("R" + uc.RevNo),
                               Helper.GenerateTextbox(uc.LineId, "SubcontractingPlan",uc.SubcontractingPlan
                               , "UpdateLineDetails7(this, "+ uc.LineId +");"
                               ,(status != clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue() ? true : (!IsDisplayOnly ? true: false)) , "" , "200"),
                                    Helper.GenerateTextbox(uc.LineId, "Shop",uc.Shop
                               , "UpdateLineDetails7(this, "+ uc.LineId +");"
                               ,(status != clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue() ? true : (!IsDisplayOnly ? true: false)), "" , "50" ),
                               Helper.GenerateGridButton(uc.LineId, "Delete", "Delete Rocord", "fa fa-trash-o", "DeleteRecord7("+ uc.LineId +");"),
                           }).ToList();

                data.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult getCodeValue(string project, string approver, string create)
        {
            ResponceMsgWithStatus1 objResponseMsg = new ResponceMsgWithStatus1();
            try
            {
                objResponseMsg.custdesc = Manager.GetCustomerCodeAndNameByProject(project);
                objResponseMsg.createdesc = db.COM003.Where(i => i.t_psno == create && i.t_actv == 1).Select(i => i.t_psno + " - " + i.t_name).FirstOrDefault();
                objResponseMsg.projdesc = db.COM001.Where(i => i.t_cprj == project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objResponseMsg.appdesc = db.COM003.Where(i => i.t_psno == approver && i.t_actv == 1).Select(i => i.t_psno + " - " + i.t_name).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        public JsonResult createRevison(int strHeaderId, string strRemarks)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                int? revison = (from a in db.JPP001
                                where a.HeaderId == strHeaderId
                                select a.RevNo).FirstOrDefault();
                revison = revison + 1;
                db.SP_JPP_Revision(strHeaderId, revison, objClsLoginInfo.UserName, strRemarks);
                objResponseMsg.Key = true;
                objResponseMsg.status = "Draft";
                objResponseMsg.Revision = "R" + revison.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        public ActionResult LoadfrmDPLinesH(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                string strSortOrder = string.Empty;
                int headerid = Convert.ToInt32(param.Headerid);
                var project = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.Project).FirstOrDefault();
                var document = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.Document).FirstOrDefault();
                var revno = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.RevNo).FirstOrDefault();
                var status = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.Status).FirstOrDefault();
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere = " (Project like '%" + param.sSearch
                       + "%' or Document like '%" + param.sSearch
                       + "%' or RevNo like '%" + param.sSearch
                       + "%' or DiscussionPoints like '%" + param.sSearch
                       + "%')";
                }
                else
                {
                    strWhere = "1=1";
                }
                var lstResult = db.SP_JPP_DiscussionPoints
                                (
                                StartIndex, EndIndex, strSortOrder, headerid, strWhere
                                ).ToList();
                var newRecord = new[] {
                                    "",
                                    Convert.ToString(project),
                                    Convert.ToString(document),
                                    Convert.ToString(revno),
                                   ""
                                };
                var data = (from uc in lstResult
                            select new[]
                            {
                               Convert.ToString(uc.ROW_NO),
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.Document),
                               Convert.ToString("R" + uc.RevNo),
                               Convert.ToString(uc.DiscussionPoints)
                          }).ToList();

                data.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult LoadfrmDocLinesH(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                string strSortOrder = string.Empty;
                int headerid = Convert.ToInt32(param.Headerid);
                var project = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.Project).FirstOrDefault();
                var document = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.Document).FirstOrDefault();
                var revno = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.RevNo).FirstOrDefault();
                var status = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.Status).FirstOrDefault();

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere = " (Project like '%" + param.sSearch
                       + "%' or Document like '%" + param.sSearch
                       + "%' or RevNo like '%" + param.sSearch
                       + "%' or DocumentsDrawingDescription like '%" + param.sSearch
                       + "%' or RequiredDate like '%" + param.sSearch
                       + "%' or ExpectedDate like '%" + param.sSearch
                       + "%')";
                }
                else
                {
                    strWhere = "1=1";
                }
                var lstResult = db.SP_JPP_GET_Documents
                                (
                                StartIndex, EndIndex, strSortOrder, headerid, strWhere
                                ).ToList();

                var newRecord = new[] {
                                    "",

                                    Convert.ToString(project),
                                    Convert.ToString(document),
                                    Convert.ToString(revno),
                                   "",
                                   "",
                                   ""
                                };
                var data = (from uc in lstResult
                            select new[]
                            {
                               //Convert.ToString(uc.ROW_NO),
                               Convert.ToString(uc.SeqNo),

                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.Document),
                               Convert.ToString("R" + uc.RevNo),
                               Convert.ToString(uc.DocumentsDrawingDescription),
                               Convert.ToString(Convert.ToDateTime(uc.RequiredDate).ToShortDateString()),
                               Convert.ToString(Convert.ToDateTime(uc.ExpectedDate).ToShortDateString()),
                           }).ToList();

                data.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }
        [HttpPost]
        public ActionResult LoadfrmMMCLinesH(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                string strSortOrder = string.Empty;
                int headerid = Convert.ToInt32(param.Headerid);
                int refid = db.JPP002_Log.Where(i => i.Id == headerid).Select(i => i.RefId).FirstOrDefault();
                var project = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.Project).FirstOrDefault();
                var document = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.Document).FirstOrDefault();
                var revno = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.RevNo).FirstOrDefault();
                var status = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.Status).FirstOrDefault();
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere = " (Project like '%" + param.sSearch
                       + "%' or Document like '%" + param.sSearch
                       + "%' or RevNo like '%" + param.sSearch
                       + "%' or DocumentsDrawingDescription like '%" + param.sSearch
                       + "%')";
                }
                else
                {
                    strWhere = "1=1";
                }
                var lstResult = db.SP_JPP_GET_MajorMaterialConstruction_History
                                (
                                StartIndex, EndIndex, strSortOrder, headerid, strWhere
                                ).ToList();

                var newRecord = new[] {
                                    "",

                                    Convert.ToString(project),
                                    Convert.ToString(document),
                                    Convert.ToString("R" + revno),
                                   ""
                                };
                var data = (from uc in lstResult
                            select new[]
                            {
                               Convert.ToString(uc.ROW_NO),

                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.Document),
                               Convert.ToString("R" + uc.RevNo),
                               Convert.ToString(uc.MajorMaterialofConstruction)
                           }).ToList();

                data.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult LoadfrmPTLinesH(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                string strSortOrder = string.Empty;
                int headerid = Convert.ToInt32(param.Headerid);
                int refid = db.JPP002_Log.Where(i => i.Id == headerid).Select(i => i.RefId).FirstOrDefault();
                var OBJJPP001 = db.JPP001.Where(i => i.HeaderId == headerid).FirstOrDefault();
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere = " (Project like '%" + param.sSearch
                       + "%' or Document like '%" + param.sSearch
                       + "%' or RevNo like '%" + param.sSearch
                       + "%' or PaymentTerms like '%" + param.sSearch
                       + "%' or MSNo like '%" + param.sSearch
                       + "%' or Discription like '%" + param.sSearch
                       + "%' or percentage like '%" + param.sSearch
                       + "%')";
                }
                else
                {
                    strWhere = "1=1";
                }
                var lstResult = db.SP_JPP_GET_PaymentTerms_History
                                (
                                StartIndex, EndIndex, strSortOrder, headerid, strWhere
                                ).ToList();


                var newRecord = new[] {
                                    "",

                                    Convert.ToString(OBJJPP001 != null ? OBJJPP001.Project : ""),
                                    Convert.ToString(OBJJPP001 != null ? OBJJPP001.Document : ""),
                                    Convert.ToString(OBJJPP001 != null? "R" + OBJJPP001.RevNo : ""),
                                    "",
                                    "",
                                    "",
                                    "",
                                };
                var data = (from uc in lstResult
                            select new[]
                            {
                               Convert.ToString(uc.ROW_NO),

                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.Document),
                               Convert.ToString("R" + uc.RevNo),
                               Convert.ToString(uc.PaymentTerms),
                               Convert.ToString(uc.MSNo),
                               Convert.ToString(uc.Discription),
                               Convert.ToString(uc.percentage),
                           }).ToList();

                data.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult LoadfrmProcLinesH(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                string strSortOrder = string.Empty;
                int headerid = Convert.ToInt32(param.Headerid);
                int refid = db.JPP002_Log.Where(i => i.Id == headerid).Select(i => i.RefId).FirstOrDefault();
                var project = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.Project).FirstOrDefault();
                var document = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.Document).FirstOrDefault();
                var revno = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.RevNo).FirstOrDefault();
                var status = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.Status).FirstOrDefault();
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere = " (Project like '%" + param.sSearch
                       + "%' or Document like '%" + param.sSearch
                       + "%' or RevNo like '%" + param.sSearch
                       + "%' or ProcurementMaterialItem like '%" + param.sSearch
                       + "%' or SizeQuantity like '%" + param.sSearch
                       + "%' or RequiredDatetomeetCDD like '%" + param.sSearch
                       + "%' or PONodate like '%" + param.sSearch
                       + "%' or Vendor like '%" + param.sSearch
                       + "%' or ExpectedDate like '%" + param.sSearch
                       + "%')";
                }
                else
                {
                    strWhere = "1=1";
                }
                var lstResult = db.SP_JPP_GET_Procurement_History
                                (
                                StartIndex, EndIndex, strSortOrder, headerid, strWhere
                                ).ToList();

                var newRecord = new[] {
                    "",

                                    Convert.ToString(project),
                                    Convert.ToString(document),
                                    Convert.ToString(revno),
                                   "",
                                   "",
                                   "",
                                   "",
                                   "",
                                   ""

                                };
                var data = (from uc in lstResult
                            select new[]
                            {
                                  //Convert.ToString(uc.ROW_NO),
                                  Convert.ToString(uc.SeqNo),
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.Document),
                               Convert.ToString("R" + uc.RevNo),
                               Convert.ToString(uc.ProcurementMaterialItem),
                               Convert.ToString(uc.SizeQuantity),
                               Convert.ToString(Convert.ToDateTime(uc.RequiredDatetomeetCDD).ToShortDateString()),
                               Convert.ToString(uc.PONodate),
                               Convert.ToString(uc.Vendor),
                               Convert.ToString(Convert.ToDateTime(uc.ExpectedDate).ToShortDateString()),

                           }).ToList();

                data.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult LoadfrmSALinesH(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                string strSortOrder = string.Empty;
                int headerid = Convert.ToInt32(param.Headerid);
                int refid = db.JPP002_Log.Where(i => i.Id == headerid).Select(i => i.RefId).FirstOrDefault();
                var project = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.Project).FirstOrDefault();
                var document = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.Document).FirstOrDefault();
                var revno = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.RevNo).FirstOrDefault();
                var status = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.Status).FirstOrDefault();
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere = " (Project like '%" + param.sSearch
                       + "%' or Document like '%" + param.sSearch
                       + "%' or RevNo like '%" + param.sSearch
                       + "%' or SpecialAssembly like '%" + param.sSearch
                       + "%')";
                }
                else
                {
                    strWhere = "1=1";
                }
                var lstResult = db.SP_JPP_GET_SpecialAssemblies_History
                                (
                                StartIndex, EndIndex, strSortOrder, headerid, strWhere
                                ).ToList();

                var newRecord = new[] {
                    "",
                                     Convert.ToString(project),
                                    Convert.ToString(document),
                                    Convert.ToString(revno),
                                   ""
                                };
                var data = (from uc in lstResult
                            select new[]
                            {
                                  Convert.ToString(uc.ROW_NO),
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.Document),
                               Convert.ToString("R" + uc.RevNo),
                               Convert.ToString(uc.SpecialAssembly)
                           }).ToList();

                data.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult LoadfrmSCRLinesH(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                string strSortOrder = string.Empty;
                int headerid = Convert.ToInt32(param.Headerid);
                int refid = db.JPP002_Log.Where(i => i.Id == headerid).Select(i => i.RefId).FirstOrDefault();
                var project = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.Project).FirstOrDefault();
                var document = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.Document).FirstOrDefault();
                var revno = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.RevNo).FirstOrDefault();
                var status = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.Status).FirstOrDefault();
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere = " (Project like '%" + param.sSearch
                       + "%' or Document like '%" + param.sSearch
                       + "%' or RevNo like '%" + param.sSearch
                       + "%' or MajorMaterialofConstruction like '%" + param.sSearch
                       + "%')";
                }
                else
                {
                    strWhere = "1=1";
                }
                var lstResult = db.SP_JPP_GET_SpecificCustomerRequirement_History
                                (
                                StartIndex, EndIndex, strSortOrder, headerid, strWhere
                                ).ToList();

                var newRecord = new[] {
                    "",

                                    Convert.ToString(project),
                                    Convert.ToString(document),
                                    Convert.ToString("R" + revno),
                                    ""
                                };
                var data = (from uc in lstResult
                            select new[]
                            {
                               Convert.ToString(uc.ROW_NO),
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.Document),
                               Convert.ToString("R" + uc.RevNo),
                             Convert.ToString(uc.MajorMaterialofConstruction),
                           }).ToList();

                data.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult LoadfrmSCPLinesH(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                string strSortOrder = string.Empty;
                int headerid = Convert.ToInt32(param.Headerid);
                int refid = db.JPP002_Log.Where(i => i.Id == headerid).Select(i => i.RefId).FirstOrDefault();
                var project = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.Project).FirstOrDefault();
                var document = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.Document).FirstOrDefault();
                var revno = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.RevNo).FirstOrDefault();
                var status = db.JPP001.Where(i => i.HeaderId == headerid).Select(i => i.Status).FirstOrDefault();
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere = " (Project like '%" + param.sSearch
                       + "%' or Document like '%" + param.sSearch
                       + "%' or RevNo like '%" + param.sSearch
                       + "%' or SubcontractingPlan like '%" + param.sSearch
                       + "%')";
                }
                else
                {
                    strWhere = "1=1";
                }
                var lstResult = db.SP_JPP_GET_SubcontractingPlan_History
                                (
                                StartIndex, EndIndex, strSortOrder, headerid, strWhere
                                ).ToList();
                var newRecord = new[] {
                                    "",
                                    Convert.ToString(project),
                                    Convert.ToString(document),
                                    Convert.ToString(revno),
                                   ""
                                };
                var data = (from uc in lstResult
                            select new[]
                            {
                               Convert.ToString(uc.ROW_NO),
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.Document),
                               Convert.ToString("R" + uc.RevNo),
                               Convert.ToString(uc.SubcontractingPlan),
                                Convert.ToString(uc.Shop),
                           }).ToList();

                data.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_JPP_GET_HEADERDETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = Convert.ToString(GetCodeAndNameByProject(li.Project)),
                                      Customer = Convert.ToString(Manager.GetCustomerCodeAndNameByProject(li.Project)),
                                      Document = Convert.ToString(li.Document),
                                      Status = Convert.ToString(li.Status),
                                      CDDDate = Convert.ToDateTime(li.CDDDate).ToString("dd/MM/yyyy"),
                                      RevNo = Convert.ToString("R" + li.RevNo),
                                      SubmittedBy = Convert.ToString(li.SubmittedBy),
                                      SubmittedOn = Convert.ToString(Convert.ToDateTime(li.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA" : Convert.ToDateTime(li.SubmittedOn).ToString("dd/MM/yyyy")),
                                      ApprovedBy = Convert.ToString(li.ApprovedBy),
                                      ApprovedOn = Convert.ToString(Convert.ToDateTime(li.ApprovedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA" : Convert.ToDateTime(li.ApprovedOn).ToString("dd/MM/yyyy")),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.MajorMaterialConstructionLine.GetStringValue())
                {
                    int headerid = Convert.ToInt32(Session["Headerid"]);

                    var lst = db.SP_JPP_GET_MajorMaterialConstruction(1, int.MaxValue, strSortOrder, headerid, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      ROW_NO = li.ROW_NO,
                                      //Project = li.Project,
                                      //Document = li.Document,
                                      //Revno = Convert.ToString("R" + li.RevNo),
                                      MajorMaterialofConstruction = Convert.ToString(li.MajorMaterialofConstruction)

                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);

                }
                else if (gridType == clsImplementationEnum.GridType.SpecificCustomerRequirementLine.GetStringValue())
                {
                    int headerid = Convert.ToInt32(Session["Headerid"]);
                    var lst = db.SP_JPP_GET_SpecificCustomerRequirement(1, int.MaxValue, strSortOrder, headerid, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      ROW_NO = li.ROW_NO,
                                      //Project = li.Project,
                                      //Document = li.Document,
                                      //Revno = Convert.ToString("R" + li.RevNo),
                                      SpecificCustomerRequirement = Convert.ToString(li.MajorMaterialofConstruction)

                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.PaymentTermsLines.GetStringValue())
                {
                    int headerid = Convert.ToInt32(Session["Headerid"]);
                    var lst = db.SP_JPP_GET_PaymentTerms(1, int.MaxValue, strSortOrder, headerid, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      ROW_NO = li.ROW_NO,
                                      //Project = li.Project,
                                      //Document = li.Document,
                                      //Revno = Convert.ToString("R" + li.RevNo),
                                      MSNO = Convert.ToString(li.MSNo),
                                      Description = Convert.ToString(li.Discription),
                                      percentage = Convert.ToString(li.percentage)

                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.DocumentsDrawingDescriptionLines.GetStringValue())
                {

                    int headerid = Convert.ToInt32(Session["Headerid"]);
                    var lst = db.SP_JPP_GET_Documents(1, int.MaxValue, strSortOrder, headerid, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      ROW_NO = li.ROW_NO,
                                      SeqNo = li.SeqNo,
                                      //Project = li.Project,
                                      //Document = li.Document,
                                      //Revno = Convert.ToString("R" + li.RevNo),
                                      DocumentsDrawingDescription = Convert.ToString(li.DocumentsDrawingDescription),
                                      RequiredDate = Convert.ToDateTime(li.RequiredDate).ToString("dd/MM/yyyy"),
                                      ExpectedDate = Convert.ToDateTime(li.ExpectedDate).ToString("dd/MM/yyyy")


                                  }).ToList();
                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.ProcurementMaterialItemLines.GetStringValue())
                {
                    int headerid = Convert.ToInt32(Session["Headerid"]);
                    var lst = db.SP_JPP_GET_Procurement(1, int.MaxValue, strSortOrder, headerid, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      ROW_NO = li.ROW_NO,
                                      SeqNo = li.SeqNo,
                                      //Project = li.Project,
                                      //Document = li.Document,
                                      //Revno = Convert.ToString("R" + li.RevNo),
                                      ProcurementMaterialItem = Convert.ToString(li.ProcurementMaterialItem),
                                      SizeQuantity = Convert.ToString(li.SizeQuantity),
                                      RequiredDatetomeetCDD = Convert.ToDateTime(li.RequiredDatetomeetCDD).ToString("dd/MM/yyyy"),
                                      PONodate = Convert.ToString(li.PONodate),
                                      Vendor = Convert.ToString(li.Vendor),
                                      ExpectedDate = Convert.ToDateTime(li.ExpectedDate).ToString("dd/MM/yyyy"),

                                  }).ToList();
                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.SubcontractingPlanLines.GetStringValue())
                {
                    int headerid = Convert.ToInt32(Session["Headerid"]);
                    var lst = db.SP_JPP_GET_SubcontractingPlan(1, int.MaxValue, strSortOrder, headerid, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      ROW_NO = li.ROW_NO,
                                      //Project = li.Project,
                                      //Document = li.Document,
                                      //Revno = Convert.ToString("R" + li.RevNo),
                                      SubcontractingPlan = Convert.ToString(li.SubcontractingPlan)

                                  }).ToList();
                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.SpecialAssemblyLines.GetStringValue())
                {

                    int headerid = Convert.ToInt32(Session["Headerid"]);
                    var lst = db.SP_JPP_GET_SpecialAssemblies(1, int.MaxValue, strSortOrder, headerid, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      ROW_NO = li.ROW_NO,
                                      //Project = li.Project,
                                      //Document = li.Document,
                                      //Revno = Convert.ToString("R" + li.RevNo),
                                      SpecialAssembly = Convert.ToString(li.SpecialAssembly)
                                  }).ToList();
                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.DiscussionPointsLines.GetStringValue())
                {

                    int headerid = Convert.ToInt32(Session["Headerid"]);
                    var lst = db.SP_JPP_DiscussionPoints(1, int.MaxValue, strSortOrder, headerid, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      ROW_NO = li.ROW_NO,
                                      //Project = li.Project,
                                      //Document = li.Document,
                                      //Revno = Convert.ToString("R" + li.RevNo),
                                      DiscussionPoints = Convert.ToString(li.DiscussionPoints)
                                  }).ToList();
                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GenerateExcelHistroy(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_JPP_GET_HEADERHISTORYDETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = Convert.ToString(GetCodeAndNameByProject(li.Project)),
                                      Customer = Convert.ToString(Manager.GetCustomerCodeAndNameByProject(li.Project)),
                                      Document = Convert.ToString(li.Document),
                                      Status = Convert.ToString(li.Status),
                                      CDDDate = Convert.ToDateTime(li.CDDDate).ToString("dd/MM/yyyy"),
                                      RevNo = Convert.ToString("R" + li.RevNo),
                                      SubmittedBy = Convert.ToString(li.SubmittedBy),
                                      SubmittedOn = Convert.ToString(Convert.ToDateTime(li.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA" : Convert.ToDateTime(li.SubmittedOn).ToString("dd/MM/yyyy")),
                                      ApprovedBy = Convert.ToString(li.ApprovedBy),
                                      ApprovedOn = Convert.ToString(Convert.ToDateTime(li.ApprovedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA" : Convert.ToDateTime(li.ApprovedOn).ToString("dd/MM/yyyy")),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.MajorMaterialConstructionLine.GetStringValue())
                {
                    int headerid = Convert.ToInt32(Session["Headerid"]);

                    var lst = db.SP_JPP_GET_MajorMaterialConstruction_History(1, int.MaxValue, strSortOrder, headerid, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      ROW_NO = li.ROW_NO,
                                      //Project = li.Project,
                                      //Document = li.Document,
                                      //Revno = Convert.ToString("R" + li.RevNo),
                                      MajorMaterialofConstruction = Convert.ToString(li.MajorMaterialofConstruction)

                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);

                }
                else if (gridType == clsImplementationEnum.GridType.SpecificCustomerRequirementLine.GetStringValue())
                {
                    int headerid = Convert.ToInt32(Session["Headerid"]);
                    var lst = db.SP_JPP_GET_SpecificCustomerRequirement_History(1, int.MaxValue, strSortOrder, headerid, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      ROW_NO = li.ROW_NO,
                                      //Project = li.Project,
                                      //Document = li.Document,
                                      //Revno = Convert.ToString("R" + li.RevNo),
                                      SpecificCustomerRequirement = Convert.ToString(li.MajorMaterialofConstruction)

                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.PaymentTermsLines.GetStringValue())
                {
                    int headerid = Convert.ToInt32(Session["Headerid"]);
                    var lst = db.SP_JPP_GET_PaymentTerms_History(1, int.MaxValue, strSortOrder, headerid, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      ROW_NO = li.ROW_NO,
                                      //Project = li.Project,
                                      //Document = li.Document,
                                      //Revno = Convert.ToString("R" + li.RevNo),
                                      PaymentTerms = Convert.ToString(li.PaymentTerms)

                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.DocumentsDrawingDescriptionLines.GetStringValue())
                {

                    int headerid = Convert.ToInt32(Session["Headerid"]);
                    var lst = db.SP_JPP_GET_Documents_History(1, int.MaxValue, strSortOrder, headerid, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      ROW_NO = li.ROW_NO,
                                      //Project = li.Project,
                                      //Document = li.Document,
                                      //Revno = Convert.ToString("R" + li.RevNo),
                                      DocumentsDrawingDescription = Convert.ToString(li.DocumentsDrawingDescription),
                                      RequiredDate = Convert.ToDateTime(li.RequiredDate).ToString("dd/MM/yyyy"),
                                      ExpectedDate = Convert.ToDateTime(li.ExpectedDate).ToString("dd/MM/yyyy"),


                                  }).ToList();
                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.ProcurementMaterialItemLines.GetStringValue())
                {
                    int headerid = Convert.ToInt32(Session["Headerid"]);
                    var lst = db.SP_JPP_GET_Procurement_History(1, int.MaxValue, strSortOrder, headerid, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      ROW_NO = li.ROW_NO,
                                      //Project = li.Project,
                                      //Document = li.Document,
                                      //Revno = Convert.ToString("R" + li.RevNo),
                                      ProcurementMaterialItem = Convert.ToString(li.ProcurementMaterialItem),
                                      SizeQuantity = Convert.ToString(li.SizeQuantity),
                                      RequiredDatetomeetCDD = Convert.ToDateTime(li.RequiredDatetomeetCDD).ToString("dd/MM/yyyy"),
                                      PONodate = Convert.ToString(li.PONodate),
                                      Vendor = Convert.ToString(li.Vendor),
                                      ExpectedDate = Convert.ToDateTime(li.ExpectedDate).ToString("dd/MM/yyyy"),

                                  }).ToList();
                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.SubcontractingPlanLines.GetStringValue())
                {
                    int headerid = Convert.ToInt32(Session["Headerid"]);
                    var lst = db.SP_JPP_GET_SubcontractingPlan_History(1, int.MaxValue, strSortOrder, headerid, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      ROW_NO = li.ROW_NO,
                                      //Project = li.Project,
                                      //Document = li.Document,
                                      //Revno = Convert.ToString("R" + li.RevNo),
                                      SubcontractingPlan = Convert.ToString(li.SubcontractingPlan)

                                  }).ToList();
                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.SpecialAssemblyLines.GetStringValue())
                {

                    int headerid = Convert.ToInt32(Session["Headerid"]);
                    var lst = db.SP_JPP_GET_SpecialAssemblies_History(1, int.MaxValue, strSortOrder, headerid, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      ROW_NO = li.ROW_NO,
                                      //Project = li.Project,
                                      //Document = li.Document,
                                      //Revno = Convert.ToString("R" + li.RevNo),
                                      SpecialAssembly = Convert.ToString(li.SpecialAssembly)
                                  }).ToList();
                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.DiscussionPointsLines.GetStringValue())
                {

                    int headerid = Convert.ToInt32(Session["Headerid"]);
                    var lst = db.SP_JPP_GET_DiscussionPoints_HISTORY(1, int.MaxValue, strSortOrder, headerid, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      ROW_NO = li.ROW_NO,
                                      //Project = li.Project,
                                      //Document = li.Document,
                                      //Revno = Convert.ToString("R" + li.RevNo),
                                      DiscussionPoints = Convert.ToString(li.DiscussionPoints)
                                  }).ToList();
                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
    }
}

