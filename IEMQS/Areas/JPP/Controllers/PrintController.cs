﻿using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.JPP.Controllers
{
    [SessionExpireFilter]
    public class PrintController : clsBase
    {
        // GET: JPP/Print
        public ActionResult Index(int HeaderId)
        {
            JPP001 objJPP001 = db.JPP001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            var details = getCodeValue(objJPP001.Project, objJPP001.ApprovedBy, objJPP001.CreatedBy);
            objJPP001.Project =  details.projdesc;
            objJPP001.Customer =  details.custdesc;
            objJPP001.CreatedBy = details.createdesc;
            objJPP001.ApprovedBy =  details.appdesc;
            return View(objJPP001);
        }

        public dynamic getCodeValue(string project, string approver, string create)
        {
            JPP001Extend result = new JPP001Extend();
            try
            {
                result.custdesc = Manager.GetCustomerCodeAndNameByProject(project);
                result.createdesc = db.COM003.Where(i => i.t_psno == create && i.t_actv == 1).Select(i => i.t_psno + " - " + i.t_name).FirstOrDefault();
                result.projdesc = db.COM001.Where(i => i.t_cprj == project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                result.appdesc = db.COM003.Where(i => i.t_psno == approver && i.t_actv == 1).Select(i => i.t_psno + " - " + i.t_name).FirstOrDefault();
            }
            catch (Exception ex){ Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }
            return result;

        }

        private class JPP001Extend
        {
            public string custdesc { get; set; }
            public string createdesc { get; set; }
            public string projdesc { get; set; }
            public string appdesc { get; set; }

        }
    }
}