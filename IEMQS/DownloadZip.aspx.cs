﻿using IEMQSImplementation;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IEMQS
{
    public partial class DownloadZip : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string folderPath = "";
            bool includeFromSubfolders = false;
            string Type = "";
            if (Request.QueryString["folderPath"] != null && Request.QueryString["folderPath"].ToString() != string.Empty)
            {
                folderPath = Request.QueryString["folderPath"].ToString();
            }
            if (Request.QueryString["includeFromSubfolders"] != null && Request.QueryString["includeFromSubfolders"].ToString() != string.Empty)
            {
                includeFromSubfolders =Convert.ToBoolean(Request.QueryString["includeFromSubfolders"].ToString());
            }
            if (Request.QueryString["Type"] != null && Request.QueryString["Type"].ToString() != string.Empty)
            {
                Type = Request.QueryString["Type"].ToString();
            }
            DownloadMultipleFileAsZip(folderPath, includeFromSubfolders, Type);
        }
        public void DownloadMultipleFileAsZip(string folderPath, bool includeFromSubfolders = false, string Type = "")
        {
            List<clsFileUpload.ZipFilesEnt> listZipFilesEnt = new List<clsFileUpload.ZipFilesEnt>();
            try
            {
                listZipFilesEnt = (new clsFileUpload(Type)).CreateZipFilelist(folderPath, includeFromSubfolders);
                if (listZipFilesEnt.Count > 0)
                {
                    using (ZipFile zip = new ZipFile())
                    {
                        zip.AlternateEncodingUsage = ZipOption.AsNecessary;
                        //zip.AddDirectoryByName("Files");   //for create folder
                        foreach (clsFileUpload.ZipFilesEnt file in listZipFilesEnt)
                        {
                            //zip.AddDirectoryByName("Files1");
                            zip.AddEntry(file.FileName, file.Bytes);
                        }

                        Response.Clear();
                        Response.BufferOutput = false;
                        string zipName = String.Format("Zip_{0}.zip", DateTime.Now.ToString("yyyy-MMM-dd-HHmmss"));
                        Response.ContentType = "application/zip";
                        Response.AddHeader("content-disposition", "attachment; filename=" + zipName);
                        zip.Save(Response.OutputStream);
                        Response.End();
                    }
                }

            }
            catch (WebException ex)
            {
                throw new Exception((ex.Response as FtpWebResponse).StatusDescription);
            }
        }       
    }
}