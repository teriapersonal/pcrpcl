﻿var Treeview = {
    init: function () {

        $("#m_tree_2").jstree({
            core: {
                themes: {
                    responsive: !1
                }
            }
            , types: {
                default: {
                    icon: "fa fa-folder m--font-warning foldertree"
                }
                , file: {
                    icon: "fa fa-file  m--font-warning"
                }
            }
            , plugins: ["types"]
        }
        ).on('open_node.jstree', function () {
            fnTreeViewIcon();
        }).on('close_node.jstree', function () {
            //ON CLOSE EVENT CALL
        }),
        $("#m_tree_2").on("select_node.jstree", function (e, t) {
            var n = $("#" + t.selected).find("a");
            if ("#" != n.attr("href") && "javascript:;" != n.attr("href") && "" != n.attr("href")) return "_blank" == n.attr("target") && (n.attr("href").target = "_blank"), document.location.href = n.attr("href"), !1
        }
        )

    }
}

;
jQuery(document).ready(function () {
    Treeview.init()

    fnTreeViewIcon();
});

function fnTreeViewIcon() {
    $("#j1_1_anchor .foldertree").removeClass("jstree-icon jstree-themeicon fa fa-folder")
    $("#j1_1_anchor .foldertree").removeClass("jstree-themeicon-custom")

    var treelist = $("#m_tree_2 .foldertree");
    for (var i = 0; i < treelist.length; i++) {
        var id = $(treelist[i]).closest("a").attr("data-id");
        var Level = $(treelist[i]).closest("a").attr("data-Level");
        var sysGen = $(treelist[i]).closest("a").attr("data-sytemgenerate");
        var isprojectright = $(treelist[i]).closest("a").attr("data-IsProjectRight");
        var iswritefolder = $(treelist[i]).closest("a").attr("data-IsWriteFolder");
        var isviewfolder = $(treelist[i]).closest("a").attr("data-IsViewFolder");
        var dataisolevel = $(treelist[i]).closest("a").attr("data-ISOLevel");

        var vParentReadFolder = $(treelist[i]).parent('.redfolder').length;

        if (IsInprocess.toLowerCase() == 'false') {
        }
        else {
            if (parseFloat(id) > 0) {
                if (isviewfolder.toLowerCase() == "false" && iswritefolder.toLowerCase() == "false") {
                }
                else if (isviewfolder.toLowerCase() == "true" && iswritefolder.toLowerCase() == "false") {
                    if (vParentReadFolder == 0) {
                        if ($("a[data-id='" + id + "']").length == 1)
                            $(treelist[i]).closest("a").after("<a href='javascript:;' data-isprojectright='" + isprojectright + "' data-iswritefolder='" + iswritefolder + "' data-isviewfolder='" + isviewfolder + "' data-sytemgenerate='" + sysGen + "' data-Level='" + Level + "' data-id='" + id + "' data-ISOLevel='" + dataisolevel + "' data-toggle='popover'><i class='la la-ellipsis-v text-white'></i></a>");
                    }
                }
                else if (isviewfolder.toLowerCase() == "true" && iswritefolder.toLowerCase() == "true") {
                    if (vParentReadFolder == 0) {
                        if ($("a[data-id='" + id + "']").length == 1)
                            if (Level == "Level4" && sysGen.toLowerCase() == "true") {
                                $(treelist[i]).closest("a").after("<a href='javascript:;' data-sytemgenerate='" + sysGen + "' data-Level='" + Level + "' data-id='" + id + "'></a>");
                            }
                            else {
                                $(treelist[i]).closest("a").after("<a href='javascript:;' data-isprojectright='" + isprojectright + "' data-iswritefolder='" + iswritefolder + "' data-isviewfolder='" + isviewfolder + "' data-sytemgenerate='" + sysGen + "' data-Level='" + Level + "' data-id='" + id + "' data-ISOLevel='" + dataisolevel + "' data-toggle='popover'><i class='la la-ellipsis-v text-white'></i></a>");
                            }
                    }
                }

                else if (Level == "Level4" && sysGen.toLowerCase() == "true") {
                    $(treelist[i]).closest("a").after("<a href='javascript:;' data-sytemgenerate='" + sysGen + "' data-Level='" + Level + "' data-id='" + id + "'></a>");
                    if ($("a[data-id='" + id + "']").length == 1)
                        $(treelist[i]).closest("a").after("<a href='javascript:;' data-isprojectright='" + isprojectright + "' data-iswritefolder='" + iswritefolder + "' data-isviewfolder='" + isviewfolder + "' data-sytemgenerate='" + sysGen + "' data-Level='" + Level + "' data-id='" + id + "' data-ISOLevel='" + dataisolevel + "' data-toggle='popover'><i class='la la-ellipsis-v text-white'></i></a>");
                }
            }
        }
    }
}