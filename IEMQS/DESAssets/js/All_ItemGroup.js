﻿var tblAllItemGroup;
$(document).ready(function () {

    setTimeout(function () {
        if (tblAllItemGroup != null)
            tblAllItemGroup.destroy();
        
        tblAllItemGroup = $('#tblAllItemGroup').DataTable({
            "serverSide": true,
            "processing": true,
            "orderMulti": false,
            "filter": false,
            "sAjaxSource": "/DES/Part/GetAllItemGroupList",
            "iDisplayLength": 10,
            "ordering": true,
            "scrollX": true,
            "responsive": false,
            "language": {
                "infoFiltered": ""
            },
            "fixedColumns": {
                "leftColumns": 1
            },
            //"scroller": {
            //    loadingIndicator: true
            //},
            //"bSort": true,
            //"aaSorting": [[1, 'desc']],
            "order": [[1, 'desc']],
            "columnDefs": [
                { "orderable": false, "targets": 0 }
                //{ "orderable": false, "targets": 5 },
                //{ "orderable": false, "targets": 6 },
                //{ "orderable": false, "targets": 7 },
                //{ "orderable": false, "targets": 8 }
            ],

            "fnServerData": function (sSource, data, fnCallback) {
                data.push({ name: "searchitem", value: $("#txtsearchitemgrp").val() });
                data.push({ name: "materialId", value: $("#txtsearchMaterialId").val() });

                $.ajax({
                    "dataType": "json",
                    "type": "POST",
                    "url": sSource,
                    "data": data,
                    "success": fnCallback
                });

            },
            "aoColumns": [
                {
                    "sTitle": "", "className": "", render: function (data, type, row, meta) {
                        var srt = "";
                        srt += '<a href="javascript:;" onclick="btnItemGroupCopy(this)" data-id="' + row.t_citg + '" title="Select"><i class="fa fa-link m--font-accent"></i> </a>';                        
                        return srt;
                    }
                },
                {
                    "sTitle": "Item Group", "className": "", render: function (data, type, row, meta) {
                        return row.t_citg;
                    }
                },
                {
                    "sTitle": "Description", "className": "", render: function (data, type, row, meta) {
                        return row.t_dsca;
                    }
                },
                {
                    "sTitle": "C", "className": "", render: function (data, type, row, meta) {
                        return row.t_ccur;
                    }
                },
                {
                    "sTitle": "Spec", "className": "", render: function (data, type, row, meta) {
                        return row.t_spec;
                    }
                },
                {
                    "sTitle": "Deli", "className": "", render: function (data, type, row, meta) {
                        return row.t_cdf_deli;
                    }
                },
                {
                    "sTitle": "Enpr", "className": "", render: function (data, type, row, meta) {
                        return row.t_cdf_enpr;
                    }
                },
                {
                    "sTitle": "Pprm", "className": "", render: function (data, type, row, meta) {
                        return row.t_cdf_pprm;
                    }
                },
                {
                    "sTitle": "Pprp", "className": "", render: function (data, type, row, meta) {
                        return row.t_cdf_pprp;
                    }
                },
                {
                    "sTitle": "Pqtm", "className": "", render: function (data, type, row, meta) {
                        return row.t_cdf_pqtm;
                    }
                },
                {
                    "sTitle": "Pqtp", "className": "", render: function (data, type, row, meta) {
                        return row.t_cdf_pqtp;
                    }
                }
            ],

            "initComplete": function () {

            },

            "fnRowCallback": function () {

            }
        });

    }, 500);
    //setTimeout(function () {
    //    tblAllItemGroup.columns.adjust();
    //}, 500);
});



