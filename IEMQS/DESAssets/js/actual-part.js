var tblPartList;
var tblBOMList;
var tblPartBOMList;
var filtercolumnArray = new Array();
var removefiltercolumnArray = new Array();

$(document).ready(function () {
    PartList();
    BOMList();
    PartBOMList();
    // Showcolumns();
});
$("#parttab").on("click", "a[href='#m_PartTabs_3_1']", function () {
    PartList();
});
$("#parttab").on("click", "a[href='#m_PartTabs_3_2']", function () {
    BOMList();
});
$("#parttab").on("click", "a[href='#m_PartTabs_3_3']", function () {
    PartBOMList();
});
function PartList() {
    if (tblPartList != null)
        tblPartList.ajax.reload();
    else
        tblPartList = $('#tblPartList').DataTable({
            "serverSide": true,
            "processing": true,
            "orderMulti": false,
            "filter": true,
            "sAjaxSource": "/DES/Part/Index",
            "iDisplayLength": 10,
            "ordering": true,
            "scrollX": true,
            //"responsive": true,
            "fixedColumns": {
                leftColumns: 3
            },
            "language": {
                "infoFiltered": ""
            },
            //"scroller": {
            //    loadingIndicator: true
            //},
            //"bSort": true,
            //"aaSorting": [[1, 'desc']],
            "order": [[0, 'desc']],
            "columnDefs": [
                { "orderable": true, "targets": 0 },
                { "orderable": true, "targets": 1 },
                { "orderable": true, "targets": 2 },
                { "orderable": false, "targets": 3 },
                { "orderable": false, "targets": 3 },
                { "orderable": true, "targets": 5 },
                { "orderable": true, "visible": true, "targets": 6 },
                { "orderable": false, "visible": true, "targets": 7 },
                { "orderable": true, "visible": true, "targets": 8 },
                { "orderable": true, "visible": true, "targets": 9 },
                { "orderable": true, "visible": true, "targets": 10 },
                { "orderable": true, "visible": true, "targets": 11 },
                { "orderable": true, "visible": true, "targets": 12 },
                { "orderable": false, "visible": true, "targets": 13 },
                { "orderable": false, "visible": true, "targets": 14 },
                { "orderable": false, "visible": true, "targets": 15 },
                { "orderable": false, "visible": true, "targets": 16 },
                { "orderable": true, "visible": true, "targets": 17 },
                { "orderable": false, "visible": true, "targets": 18 },
                { "orderable": false, "visible": true, "targets": 19 },
                { "orderable": false, "visible": true, "targets": 20 },
                { "orderable": false, "visible": true, "targets": 21 },
                { "orderable": true, "visible": true, "targets": 22 },
                { "orderable": false, "visible": true, "targets": 23 },
                { "orderable": false, "visible": true, "targets": 24 },
                { "orderable": false, "visible": true, "targets": 25 },
                { "orderable": true, "visible": true, "targets": 26 },
                { "orderable": false, "visible": true, "targets": 27 },
                { "orderable": true, "visible": true, "targets": 28 },
            ],

            "fnServerData": function (sSource, data, fnCallback) {
                $.ajax({
                    "dataType": "json",
                    "type": "POST",
                    "url": sSource,
                    "data": data,
                    "success": fnCallback
                });

            },
            "aoColumns": [

                {
                    "sTitle": "Actions", "className": "nowrap", render: function (data, type, row, meta) {
                        var srt = "";
                        if (row.Authorized > 0) {
                            if (IsInprocess.toLowerCase() == 'false') {
                            }
                            else {
                                if (row.IsFromLN != true && row.IsFromFKMS != true) {
                                    if (row.Status == "Completed") {
                                        srt += '\n <a href="javascript:;" onclick="PartRevise(this)" data-href="/DES/Part/EditItem?q=' + row.BString + '" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Revise">\n<i class="la la-refresh m--font-brand"></i>\n</a>';
                                    } else {
                                        srt += '\n <a  href="/DES/Part/EditItem?q=' + row.BString + '" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Edit">\n<i class="la la-edit"></i>\n</a>';
                                        if (row.RevNo == 0)
                                            srt += '\n <a  class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete" Onclick="swalPopup(this)">\n<i class="la la-trash"></i>\n</a>';
                                    }

                                }
                            }
                        }
                        srt += '\n <a href="/DES/Part/Detail?q=' + row.QString + '" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="View">\n                          <i class="la la-eye m--font-success"></i>\n                        </a>';
                        srt += '';
                        return srt;
                    }
                },
                {
                    "sTitle": "Type", "className": "", render: function (data, type, row, meta) {
                        var state = "";
                        if (row.Type.toLowerCase() == "part") {
                            state = "brand";
                        }
                        else if (row.Type.toLowerCase() == "plate part") {
                            state = "info";
                        }
                        else if (row.Type.toLowerCase() == "clad part") {
                            state = "accent";
                        }
                        else if (row.Type.toLowerCase() == "jigfix") {
                            state = "success";
                        }
                        return '<span class="m--font-bold m--font-' + state + '">' + row.Type + "</span>"
                    }
                },
                {
                    "sTitle": "Item Id", "className": "", render: function (data, type, row, meta) {

                        var name = "<a  href='/DES/Part/PowerView?q=" + row.QString + "'>" + row.ItemKey + "</a>";
                        return name;
                    }
                },
                {
                    "sTitle": "Rev No.", "className": "", render: function (data, type, row, meta) {
                        return ('R' + row.RevNo);
                    }
                },
                {
                    "sTitle": "Status", "className": "", render: function (data, type, row, meta) {
                        return row.ItemState;
                    }
                },
                {
                    "sTitle": "Material", "className": "", render: function (data, type, row, meta) {
                        return row.Material;
                    }
                },
                {
                    "sTitle": "Item Group", "className": "", render: function (data, type, row, meta) {
                        return row.ItemGroup;
                    }
                },
                {
                    "sTitle": "UOM", "className": "", render: function (data, type, row, meta) {
                        return row.UOM;
                    }
                },
                {
                    "sTitle": "Item Description", "className": "", render: function (data, type, row, meta) {
                        return row.ItemName;
                    }
                },
                {
                    "sTitle": "String2", "className": "", render: function (data, type, row, meta) {
                        return row.String2;
                    }
                },
                {
                    "sTitle": "String3", "className": "", render: function (data, type, row, meta) {
                        return row.String3;
                    }
                },
                {
                    "sTitle": "String4", "className": "", render: function (data, type, row, meta) {
                        return row.String4;
                    }
                },
                {
                    "sTitle": "ARM Set", "className": "", render: function (data, type, row, meta) {
                        return row.ARMSet;
                    }
                },
                {
                    "sTitle": "ARM Rev", "className": "", render: function (data, type, row, meta) {
                        return row.ARMRev;
                    }
                },
                {
                    "sTitle": "Procurement Drg", "className": "", render: function (data, type, row, meta) {
                        return row.ProcurementDrgDocumentNo;
                    }
                },
                {
                    "sTitle": "Item Weight (kg)", "className": "", render: function (data, type, row, meta) {
                        return row.ItemWeight;
                    }
                },
                {
                    "sTitle": "CRS(Yes/No)", "className": "", render: function (data, type, row, meta) {
                        return row.CRSYN;
                    }
                },
                {
                    "sTitle": "Size code", "className": "", render: function (data, type, row, meta) {
                        return row.SizeCode;
                    }
                },
                {
                    "sTitle": "Weight Factor", "className": "", render: function (data, type, row, meta) {
                        return row.WeightFactor;
                    }
                },
                {
                    "sTitle": "Thickness", "className": "", render: function (data, type, row, meta) {
                        return row.Thickness;
                    }
                },
                {
                    "sTitle": "Clad Plate Part Thickness1", "className": "", render: function (data, type, row, meta) {
                        return row.CladPlatePartThickness1;
                    }
                },
                {
                    "sTitle": "IsDoubleClad", "className": "", render: function (data, type, row, meta) {
                        return row.IsDoubleClad;
                    }
                },
                {
                    "sTitle": "Product Form", "className": "", render: function (data, type, row, meta) {
                        return row.ProductForm;
                    }
                },
                {
                    "sTitle": "Item Id Type", "className": "", render: function (data, type, row, meta) {
                        return row.ItemKeyType;
                    }
                },
                {
                    "sTitle": "Order Policy", "className": "", render: function (data, type, row, meta) {
                        return row.OrderPolicy;
                    }
                },
                {
                    "sTitle": "Created By", "className": "", render: function (data, type, row, meta) {
                        return row.CreatedBy;
                    }
                },
                {
                    "sTitle": "Created On", "className": "", render: function (data, type, row, meta) {
                        return row.CreatedOn;
                    }
                },
                {
                    "sTitle": "Edited By", "className": "", render: function (data, type, row, meta) {
                        return row.EditedBy;
                    }
                },
                {
                    "sTitle": "Edited On", "className": "", render: function (data, type, row, meta) {
                        return row.EditedOn;
                    }
                },
            ],

            "initComplete": function () {
                $searchButton = $('<div class="filed-list"><button class="btn btn-secondary active"><i class="la la-ellipsis-v"></i></button ><div class="hover-div"><h5>Searchable fields</h5><ul class="list-inline"><li class="list-inline-item"> Type</li> <li class="list-inline-item"> Item Id </li><li class="list-inline-item">Material</li><li class="list-inline-item"> Item Group </li> <li class="list-inline-item"> Item Description </li><li class="list-inline-item"> String2 </li><li class="list-inline-item"> String3 </li><li class="list-inline-item"> String4 </li><li class="list-inline-item"> ARM Set </li><li class="list-inline-item"> Size Code </li><li class="list-inline-item"> Product Form </li></ul ></div ></div > ');
                $('#tblPartList_wrapper .dataTables_filter').append($searchButton);
                var $filterBoxPartList = $('#tblPartList_wrapper .dataTables_filter input').unbind(), selfPartList = this.api();
                var stoppedTypingPartList;

                $filterBoxPartList.on('keyup', function () {
                    if (stoppedTypingPartList) clearTimeout(stoppedTypingPartList);
                    stoppedTypingPartList = setTimeout(function () {
                        selfPartList.search($filterBoxPartList.val()).draw();
                    }, 500);
                });
            },

            "fnRowCallback": function () {
                tblPartList.columns.adjust();
            }

        });

}
function BOMList() {
    if (tblBOMList != null)
        tblBOMList.ajax.reload();
    else
        tblBOMList = $('#tblBOMList').DataTable({
            "serverSide": true,
            "processing": true,
            "orderMulti": false,
            "filter": true,
            "sAjaxSource": "/DES/Part/BOMIndex",
            "iDisplayLength": 10,
            "ordering": true,
            "scrollX": true,
            //"responsive": true,
            "fixedColumns": {
                leftColumns: 3
            },
            "language": {
                "infoFiltered": ""
            },
            //"scroller": {
            //    loadingIndicator: true
            //},
            //"bSort": true,
            //"aaSorting": [[1, 'desc']],
            "order": [[0, 'desc']],
            "columnDefs": [
                { "orderable": true, "targets": 0 },
                { "orderable": true, "targets": 1 },
                { "orderable": true, "targets": 2 },
                { "orderable": false, "targets": 3 },
                { "orderable": true, "targets": 4 },
                { "orderable": true, "visible": true, "targets": 5 },
                { "orderable": true, "visible": true, "targets": 6 },
                { "orderable": true, "visible": true, "targets": 7 },
                { "orderable": true, "visible": true, "targets": 8 },
                { "orderable": false, "visible": true, "targets": 9 },
                { "orderable": false, "visible": true, "targets": 10 },
                { "orderable": false, "visible": true, "targets": 11 },
                { "orderable": false, "visible": true, "targets": 12 },
                { "orderable": false, "visible": true, "targets": 13 },
                { "orderable": false, "visible": true, "targets": 14 },
                { "orderable": true, "visible": true, "targets": 15 },
                { "orderable": false, "visible": true, "targets": 16 },
                { "orderable": false, "visible": true, "targets": 17 },
                { "orderable": false, "visible": true, "targets": 18 },
                { "orderable": false, "visible": true, "targets": 19 },
                { "orderable": false, "visible": true, "targets": 20 },
                { "orderable": true, "visible": true, "targets": 21 },
                { "orderable": true, "visible": true, "targets": 22 },
                { "orderable": false, "visible": true, "targets": 23 },
                { "orderable": true, "visible": true, "targets": 24 },
                { "orderable": false, "visible": true, "targets": 25 },
                { "orderable": true, "visible": true, "targets": 26 }
            ],

            "fnServerData": function (sSource, data, fnCallback) {
                $.ajax({
                    "dataType": "json",
                    "type": "POST",
                    "url": sSource,
                    "data": data,
                    "success": fnCallback
                });

            },
            "aoColumns": [

                {
                    "sTitle": "Actions", "className": "nowrap", render: function (data, type, row, meta) {
                        var srt = "";
                        if (row.Authorized > 0) {
                            if (IsInprocess.toLowerCase() == 'false') {
                            }
                            else {
                                if (row.IsFromLN != true && row.IsFromFKMS != true)
                                    srt += '\n <a  href="/DES/Part/EditBOM?q=' + row.BString + '" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Edit">\n<i class="la la-edit"></i>\n</a>';
                                //srt += '\n <a  class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete" Onclick="swalPopup(this)">\n<i class="la la-trash"></i>\n</a>';
                            }
                        }
                        srt += '\n <a href="/DES/Part/BOMDetail?q=' + row.BString + '" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="View">\n                          <i class="la la-eye m--font-success"></i>\n                        </a>';
                        srt += '';
                        return srt;
                    }
                },
                {
                    "sTitle": "Parent Item", "className": "", render: function (data, type, row, meta) {
                        return row.PerentItemKey;
                    }
                },
                {
                    "sTitle": "Item Id", "className": "", render: function (data, type, row, meta) {

                        var name = "<a  href='/DES/Part/PowerView?q=" + row.QString + "'>" + row.ItemKey + "</a>";
                        return name;
                    }
                },
                {
                    "sTitle": "Child Part Status", "className": "", render: function (data, type, row, meta) {
                        return row.ItemState;
                    }
                },
                {
                    "sTitle": "Find No.", "className": "", render: function (data, type, row, meta) {

                        return row.FindNumber;
                    }
                },
                {
                    "sTitle": "Ext. Find No.", "className": "", render: function (data, type, row, meta) {
                        var ReExtFindNumbervNo = row.ExtFindNumber || '';
                        return ReExtFindNumbervNo;
                    }
                },
                {
                    "sTitle": "DRG No.", "className": "", render: function (data, type, row, meta) {
                        return row.DRGNoDocumentNo;
                    }
                },
                {
                    "sTitle": "Find No. Desc", "className": "", render: function (data, type, row, meta) {
                        var FindNumberDescription = row.FindNumberDescription || '';
                        return FindNumberDescription;
                    }
                },
                {
                    "sTitle": "Item Description", "className": "", render: function (data, type, row, meta) {
                        return row.ItemName;
                    }
                },
                {
                    "sTitle": "Quantity(For BOM)", "className": "", render: function (data, type, row, meta) {
                        return (row.Quantity || 0);
                    }
                },
                {
                    "sTitle": "UOM", "className": "", render: function (data, type, row, meta) {
                        return row.UOM;
                    }
                },
                {
                    "sTitle": "Length (mm)", "className": "", render: function (data, type, row, meta) {
                        return row.Length;
                    }
                },
                {
                    "sTitle": "Width", "className": "", render: function (data, type, row, meta) {
                        return row.Width;
                    }
                },
                {
                    "sTitle": "Number Of Pieces", "className": "", render: function (data, type, row, meta) {
                        return row.NumberOfPieces;
                    }
                },
                {
                    "sTitle": "BOM Weight (kg)", "className": "", render: function (data, type, row, meta) {
                        return row.BOMWeight;
                    }
                },
                {
                    "sTitle": "Remarks", "className": "", render: function (data, type, row, meta) {
                        return row.Remarks;
                    }
                },
                {
                    "sTitle": "Job Qty", "className": "", render: function (data, type, row, meta) {
                        return row.JobQty;
                    }
                },
                {
                    "sTitle": "Commissioning Spare Qty", "className": "", render: function (data, type, row, meta) {
                        return row.CommissioningSpareQty;
                    }
                },
                {
                    "sTitle": "Mandatory Spare Qty", "className": "", render: function (data, type, row, meta) {
                        return row.MandatorySpareQty;
                    }
                },
                {
                    "sTitle": "Operation Spare Qty", "className": "", render: function (data, type, row, meta) {
                        return row.OperationSpareQty;
                    }
                },
                {
                    "sTitle": "Extra Qty", "className": "", render: function (data, type, row, meta) {
                        return row.ExtraQty;
                    }
                },
                {
                    "sTitle": "BOM Report Size", "className": "", render: function (data, type, row, meta) {
                        return row.BomReportSize;
                    }
                },
                {
                    "sTitle": "Drawing BOM Size", "className": "", render: function (data, type, row, meta) {
                        return row.DrawingBomSize;
                    }
                },
                {
                    "sTitle": "Created By", "className": "", render: function (data, type, row, meta) {
                        return row.CreatedBy;
                    }
                },
                {
                    "sTitle": "Created On", "className": "", render: function (data, type, row, meta) {
                        return row.CreatedOn;
                    }
                },
                {
                    "sTitle": "Edited By", "className": "", render: function (data, type, row, meta) {
                        return row.EditedBy;
                    }
                },
                {
                    "sTitle": "Edited On", "className": "", render: function (data, type, row, meta) {
                        return row.EditedOn;
                    }
                },
            ],

            "initComplete": function () {
                $searchButton = $('<div class="filed-list"><button class="btn btn-secondary active"><i class="la la-ellipsis-v"></i></button ><div class="hover-div"><h5>Searchable fields</h5><ul class="list-inline"><li class="list-inline-item"> Parent Item Id</li> <li class="list-inline-item"> Item Id </li><li class="list-inline-item"> Find No </li><li class="list-inline-item"> Ext Find No </li> <li class="list-inline-item"> Find No Desc</li><li class="list-inline-item"> DRG No</li><li class="list-inline-item"> Item Description </li><li class="list-inline-item"> Remarks </li><li class="list-inline-item"> Bom Report Size </li><li class="list-inline-item"> Drawing Bom Size </li></ul ></div ></div > ');
                $('#tblBOMList_wrapper .dataTables_filter').append($searchButton);

                var $filterBoxBOMList = $('#tblBOMList_wrapper .dataTables_filter input').unbind(), selfBOMList = this.api();
                var stoppedTypingBOMList;

                $filterBoxBOMList.on('keyup', function () {
                    if (stoppedTypingBOMList) clearTimeout(stoppedTypingBOMList);
                    stoppedTypingBOMList = setTimeout(function () {
                        selfBOMList.search($filterBoxBOMList.val()).draw();
                    }, 500);
                });
            },

            "fnRowCallback": function () {
                tblPartList.columns.adjust();
            }

        });
}

function PartBOMList() {
    if (tblPartBOMList != null)
        tblPartBOMList.ajax.reload();
    else
        tblPartBOMList = $('#tblPartBOMList').DataTable({
            "serverSide": true,
            "processing": true,
            "orderMulti": false,
            "filter": true,
            "sAjaxSource": "/DES/Part/ItemBOMIndex",
            "iDisplayLength": 10,
            "ordering": true,
            "scrollX": true,
            //"responsive": true,
            "fixedColumns": {
                leftColumns: 5
            },
            "language": {
                "infoFiltered": ""
            },
            //"scroller": {
            //    loadingIndicator: true
            //},
            //"bSort": true,
            //"aaSorting": [[1, 'desc']],
            "order": [[0, 'desc']],
            "columnDefs": [
                { "orderable": true, "targets": 0 },
                { "orderable": true, "targets": 1 },
                { "orderable": true, "targets": 2 },
                { "orderable": true, "targets": 3 },
                { "orderable": false, "targets": 4 },
                { "orderable": true, "visible": true, "targets": 5 },
                { "orderable": false, "visible": true, "targets": 6 },
                { "orderable": true, "visible": true, "targets": 7 },
                { "orderable": true, "visible": true, "targets": 8 },
                { "orderable": true, "visible": true, "targets": 9 },
                { "orderable": true, "visible": true, "targets": 10 },
                { "orderable": false, "visible": true, "targets": 11 },
                { "orderable": false, "visible": true, "targets": 12 },
                { "orderable": true, "visible": true, "targets": 13 },
                { "orderable": true, "visible": true, "targets": 14 },
                { "orderable": true, "visible": true, "targets": 15 },
                { "orderable": true, "visible": true, "targets": 16 },
                { "orderable": true, "visible": true, "targets": 17 },
                { "orderable": true, "visible": true, "targets": 18 },
                { "orderable": true, "visible": true, "targets": 19 },
                { "orderable": true, "visible": true, "targets": 20 },
                { "orderable": false, "visible": true, "targets": 21 },
                { "orderable": false, "visible": true, "targets": 22 },
                { "orderable": false, "visible": true, "targets": 23 },
                { "orderable": false, "visible": true, "targets": 24 },
                { "orderable": false, "visible": true, "targets": 25 },
                { "orderable": false, "visible": true, "targets": 26 },
                { "orderable": false, "visible": true, "targets": 27 },
                { "orderable": false, "visible": true, "targets": 28 },
                { "orderable": false, "visible": true, "targets": 29 },
                { "orderable": false, "visible": true, "targets": 30 },
                { "orderable": false, "visible": true, "targets": 31 },
                { "orderable": false, "visible": true, "targets": 32 },
                { "orderable": true, "visible": true, "targets": 33 },
                { "orderable": false, "visible": true, "targets": 34 },
                { "orderable": false, "visible": true, "targets": 35 },
                { "orderable": false, "visible": true, "targets": 36 },
                { "orderable": false, "visible": true, "targets": 37 },
                { "orderable": true, "visible": true, "targets": 38 },
                { "orderable": true, "visible": true, "targets": 39 },
                { "orderable": false, "visible": true, "targets": 40 },
                { "orderable": true, "visible": true, "targets": 41 },
                { "orderable": false, "visible": true, "targets": 42 },
                { "orderable": true, "visible": true, "targets": 43 },
            ],

            "fnServerData": function (sSource, data, fnCallback) {
                $.ajax({
                    "dataType": "json",
                    "type": "POST",
                    "url": sSource,
                    "data": data,
                    "success": fnCallback
                });

            },
            "aoColumns": [

                {
                    "sTitle": "Actions", "className": "nowrap", render: function (data, type, row, meta) {
                        var srt = "";

                        srt += '\n <a href="/DES/Part/Detail?q=' + row.QString + '" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="View">\n                          <i class="la la-eye m--font-success"></i>\n                        </a>';
                        srt += '';
                        return srt;
                    }
                },
                {
                    "sTitle": "Type", "className": "", render: function (data, type, row, meta) {
                        var state = "";
                        if (row.Type.toLowerCase() == "part") {
                            state = "brand";
                        }
                        else if (row.Type.toLowerCase() == "plate part") {
                            state = "info";
                        }
                        else if (row.Type.toLowerCase() == "clad part") {
                            state = "accent";
                        }
                        else if (row.Type.toLowerCase() == "jigfix") {
                            state = "success";
                        }
                        return '<span class="m--font-bold m--font-' + state + '">' + row.Type + "</span>"
                    }
                },
                {
                    "sTitle": "Item Id", "className": "", render: function (data, type, row, meta) {

                        var name = "<a  href='/DES/Part/PowerView?q=" + row.QString + "'>" + row.ItemKey + "</a>";
                        return name;
                    }
                },
                {
                    "sTitle": "Parent Item", "className": "", render: function (data, type, row, meta) {
                        return row.PerentItemKey;
                    }
                },
                {
                    "sTitle": "Rev No.", "className": "", render: function (data, type, row, meta) {
                        return ('R' + row.RevNo);
                    }
                },
                {
                    "sTitle": "DRG No.", "className": "", render: function (data, type, row, meta) {
                        return row.DRGNoDocumentNo;
                    }
                },

                {
                    "sTitle": "Child Part Status", "className": "", render: function (data, type, row, meta) {
                        return row.ItemState;
                    }
                },

                {
                    "sTitle": "Find No.", "className": "", render: function (data, type, row, meta) {

                        return row.FindNumber;
                    }
                },
                {
                    "sTitle": "Ext. Find No.", "className": "", render: function (data, type, row, meta) {
                        var ReExtFindNumbervNo = row.ExtFindNumber || '';
                        return ReExtFindNumbervNo;
                    }
                },
                {
                    "sTitle": "Find No. Desc", "className": "", render: function (data, type, row, meta) {
                        var FindNumberDescription = row.FindNumberDescription || '';
                        return FindNumberDescription;
                    }
                },
                {
                    "sTitle": "ARM Set", "className": "", render: function (data, type, row, meta) {
                        return row.ARMSet;
                    }
                },
                {
                    "sTitle": "ARM Rev", "className": "", render: function (data, type, row, meta) {
                        return row.ARMRev;
                    }
                },
                {
                    "sTitle": "Order Policy", "className": "", render: function (data, type, row, meta) {
                        return row.OrderPolicy;
                    }
                },
                {
                    "sTitle": "Product Form", "className": "", render: function (data, type, row, meta) {
                        return row.ProductForm;
                    }
                },
                {
                    "sTitle": "Material", "className": "", render: function (data, type, row, meta) {
                        return row.Material;
                    }
                },
                {
                    "sTitle": "Item Group", "className": "", render: function (data, type, row, meta) {
                        return row.ItemGroup;
                    }
                },
                {
                    "sTitle": "Item Description", "className": "", render: function (data, type, row, meta) {
                        return row.ItemName;
                    }
                },
                {
                    "sTitle": "String2", "className": "", render: function (data, type, row, meta) {
                        return row.String2;
                    }
                },
                {
                    "sTitle": "String3", "className": "", render: function (data, type, row, meta) {
                        return row.String3;
                    }
                },
                {
                    "sTitle": "String4", "className": "", render: function (data, type, row, meta) {
                        return row.String4;
                    }
                },
                {
                    "sTitle": "Procurement Drg", "className": "", render: function (data, type, row, meta) {
                        return row.ProcurementDrgDocumentNo;
                    }
                },

                {
                    "sTitle": "Item Weight (kg)", "className": "", render: function (data, type, row, meta) {
                        return row.ItemWeight;
                    }
                },
                {
                    "sTitle": "BOM Weight (kg)", "className": "", render: function (data, type, row, meta) {
                        return row.BOMWeight;
                    }
                },
                {
                    "sTitle": "Thickness", "className": "", render: function (data, type, row, meta) {
                        return row.Thickness;
                    }
                },
                {
                    "sTitle": "Clad Plate Part Thickness1", "className": "", render: function (data, type, row, meta) {
                        return row.CladPlatePartThickness1;
                    }
                },
                {
                    "sTitle": "IsDoubleClad", "className": "", render: function (data, type, row, meta) {
                        return row.IsDoubleClad;
                    }
                },
                {
                    "sTitle": "Clad Plate Part Thickness2", "className": "", render: function (data, type, row, meta) {
                        return row.CladPlatePartThickness2;
                    }
                },
                {
                    "sTitle": "Clad Specific Gravity2", "className": "", render: function (data, type, row, meta) {
                        return row.CladSpecificGravity2;
                    }
                },
                {
                    "sTitle": "CRS(Yes/No)", "className": "", render: function (data, type, row, meta) {
                        return row.CRSYN;
                    }
                },
                {
                    "sTitle": "Job Qty", "className": "", render: function (data, type, row, meta) {
                        return row.JobQty;
                    }
                },
                {
                    "sTitle": "Commissioning Spare Qty", "className": "", render: function (data, type, row, meta) {
                        return row.CommissioningSpareQty;
                    }
                },
                {
                    "sTitle": "Mandatory Spare Qty", "className": "", render: function (data, type, row, meta) {
                        return row.MandatorySpareQty;
                    }
                },
                {
                    "sTitle": "Extra Qty", "className": "", render: function (data, type, row, meta) {
                        return row.ExtraQty;
                    }
                },
                {
                    "sTitle": "Remarks", "className": "", render: function (data, type, row, meta) {
                        return row.Remarks;
                    }
                },
                {
                    "sTitle": "Quantity", "className": "", render: function (data, type, row, meta) {
                        return (row.Quantity || 0) + " " + row.UOM;
                    }
                },
                {
                    "sTitle": "Length (mm)", "className": "", render: function (data, type, row, meta) {
                        return row.Length;
                    }
                },
                {
                    "sTitle": "Width", "className": "", render: function (data, type, row, meta) {
                        return row.Width;
                    }
                },
                {
                    "sTitle": "Number Of Pieces", "className": "", render: function (data, type, row, meta) {
                        return row.NumberOfPieces;
                    }
                },
                {
                    "sTitle": "BOM Report Size", "className": "", render: function (data, type, row, meta) {
                        return row.BomReportSize;
                    }
                },
                {
                    "sTitle": "Drawing BOM Size", "className": "", render: function (data, type, row, meta) {
                        return row.DrawingBomSize;
                    }
                },
                {
                    "sTitle": "Created By", "className": "", render: function (data, type, row, meta) {
                        return row.CreatedBy;
                    }
                },
                {
                    "sTitle": "Created On", "className": "", render: function (data, type, row, meta) {
                        return row.CreatedOn;
                    }
                },
                {
                    "sTitle": "Edited By", "className": "", render: function (data, type, row, meta) {
                        return row.EditedBy;
                    }
                },
                {
                    "sTitle": "Edited On", "className": "", render: function (data, type, row, meta) {
                        return row.EditedOn;
                    }
                },
            ],

            "initComplete": function () {
                $searchButton = $('<div class="filed-list"><button class="btn btn-secondary active"><i class="la la-ellipsis-v"></i></button ><div class="hover-div"><h5>Searchable fields</h5><ul class="list-inline"><li class="list-inline-item"> Type</li> <li class="list-inline-item"> Item Id </li><li class="list-inline-item"> DRG No </li><li class="list-inline-item"> Parent Item </li> <li class="list-inline-item"> Find No </li><li class="list-inline-item"> Ext Find No </li><li class="list-inline-item"> ARM Set </li><li class="list-inline-item"> Product Form </li><li class="list-inline-item"> Material </li><li class="list-inline-item"> Item Group </li><li class="list-inline-item"> Item Description </li> <li class="list-inline-item"> Procurement Drg </li></ul ></div ></div > ');
                $('#tblPartBOMList_wrapper .dataTables_filter').append($searchButton);
                var $filterBoxPartBOMList = $('#tblPartBOMList_wrapper .dataTables_filter input').unbind(), selfPartBOMList = this.api();
                var stoppedTypingPartBOMList;

                $filterBoxPartBOMList.on('keyup', function () {
                    if (stoppedTypingPartBOMList) clearTimeout(stoppedTypingPartBOMList);
                    stoppedTypingPartBOMList = setTimeout(function () {
                        selfPartBOMList.search($filterBoxPartBOMList.val()).draw();
                    }, 500);
                });
            },

            "fnRowCallback": function () {
                tblPartList.columns.adjust();
            }

        });
}

function PartRevise($this) {
    swal({
        backdrop: false,
        title: "Do you want to create new revision?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            if (isConfirm.value) {
                mApp.blockPage();
                location.href = $($this).attr("data-href");
            }
        });
}

function swalPopup($this) {
    swal({
        title: "Are you sure?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            if (isConfirm.value) {
                funDelete($this);
            }
        });
}

function funDelete($this) {
    var tr = $($this).closest('tr');
    var row = tblPartList.row(tr);
    var data = row.data();
    mApp.blockPage();
    $.ajax({
        type: "GET",
        url: '/DES/Part/Delete',
        data: { Id: data.ItemId },
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        async: true,
        success: function (response) {
            if (response.Status) {
                toastr.success(response.Msg, "Success");
                tblPartList.ajax.reload();
            } else {
                toastr.info(response.Msg, "Info");
            }
            mApp.unblockPage();
        },
        error: function (error) {
            toastr.error("something went wrong try again later", "Error");
            mApp.unblockPage();
        }
    })
}




function Showcolumns() {
    $('.filter-column:checkbox:checked').each(function () {
        var columnid = $(this).attr('data-column');
        if (filtercolumnArray.indexOf(columnid) === -1) {
            filtercolumnArray.push(columnid);
        }
    });



    for (i = 0; i < filtercolumnArray.length; i++) {
        tblPartList.column(filtercolumnArray[i]).visible(true);
    }




}

function funCreateXMLFile() {
    mApp.blockPage();
    $.ajax({
        type: "GET",
        url: '/DES/Part/CreateXML',
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        async: true,
        success: function (response) {
            if (response.Status) {
                toastr.success(response.Msg, "Success");
            } else {
                toastr.info(response.Msg, "Info");
            }
            mApp.unblockPage();
        },
        error: function (error) {
            toastr.error("something went wrong try again later", "Error");
            mApp.unblockPage();
        }
    })
}

function popupWindow(url, title, win, w, h) {
    const y = win.top.outerHeight / 2 + win.top.screenY - (h / 2);
    const x = win.top.outerWidth / 2 + win.top.screenX - (w / 2);
    return win.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + y + ', left=' + x);
}
