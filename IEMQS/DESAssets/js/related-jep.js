var DatatablesBasicBasic = {
    init: function () {
        var e;
        (e = $("#m_table_1")).DataTable({
           
        
            /*  dom: "<'row'<'col-sm-12'tr>>\n\t\t\t<'row datatable-bottom'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",*/

            language: {
                lengthMenu: "Display _MENU_"
            },


            columnDefs: [
              
                {
                    targets: 0,
                    width: '100px',
                    title: "Actions",
                    orderable: !1,
                    render: function (e, a, t, n) {
                        return '\n                       <a href="jep-customer-feedback.html" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" title="Add Customer feedback">\n                          <i class="la la-comment text-info"></i>\n                        </a> \n                        <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Edit">\n                          <i class="la la-edit text-brand"></i>\n                        </a>'
                    }
            }]
        })
    }
};
jQuery(document).ready(function () {
    DatatablesBasicBasic.init()
});