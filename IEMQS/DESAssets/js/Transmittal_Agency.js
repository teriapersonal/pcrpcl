﻿tblAgencyDocs;
var tblJepDoc;
var tblGlobalJepDoc;
var selectedDocIDs = [];
var FinalDoclist = [];

var WeightageList = [];


$(document).ready(function () {


    tblAgencyDocs = $('#tblAgencyDocs').DataTable({
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "sAjaxSource": "/Transmittal/GetAgencyDocList",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        "responsive": false,
        "language": {
            "infoFiltered": ""
        },
        "bRetrieve": true,
        //"scroller": {
        //    loadingIndicator: true
        //},
        //"bSort": true,
        //"aaSorting": [[1, 'desc']],
        "order": [[0, 'desc']],
        "fnServerData": function (sSource, data, fnCallback) {
            data.push({ name: "AgencyID", value: $("#AgencyId").val() });
            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });

        },
        "aoColumns": [
            {
                "sTitle": "Action", "className": "action", render: function (data, type, row, meta) {

                    var srt = '<input type="hidden" name="AgencyDocumentList[' + meta.row + '].AgencyDocListId"  value="' + row.AgencyDocListId + '"/><input type="hidden" name="AgencyDocumentList[' + meta.row + '].GenDocID"  value="' + row.GenDocID + '"/> <input type="hidden" id="JEPPlannedDate_' + row.AgencyDocListId + '"  value="' + row.JEPPlannedDate + '"/>';

                    if (row.GenDocID == null) {
                        srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnDelete" title="Delete"><i class="la la-trash text-danger"></i></a>';
                    }
                    return srt;
                }
            },
            {
                "sTitle": "Doc No", "orderable": true, "className": "", render: function (data, type, row, meta) {
                    return row.DocNo;
                }
            },
            {
                "sTitle": "Rev", "orderable": false, "className": "", render: function (data, type, row, meta) {
                    return "R" + row.RevisionNo;
                }
            },
            {
                "sTitle": "Customer Doc No", "orderable": true, "className": "", render: function (data, type, row, meta) {

                    return row.CustomerDocNo;
                }
            },
            {
                "sTitle": "Customer Doc Rev", "orderable": false, "className": "", render: function (data, type, row, meta) {

                    return row.CustomerDocRevision;
                }
            },
            {
                "sTitle": "Planned Submit Date", "orderable": true, "className": "", render: function (data, type, row, meta) {
                    var str;
                    var PlannedSubmitDate = row.PlannedSubmitDate || "";
                    var docid = row.GenDocID || 0;
                    //if (row.IsPrevRevHasPlanedDate != null && (row.PlannedSubmitDate != null)) {
                    //   str = '<input type="text" name="AgencyDocumentList[' + meta.row + '].PlannedSubmitDate" class="form-control editDoc forCheckReadOnly" id="txtPlnSubDate_' + row.AgencyDocListId + '"  value=' + PlannedSubmitDate + ' readonly="true">';
                    //}
                    //else {
                    if (docid > 0) {
                        str = '<input type="text" name="AgencyDocumentList[' + meta.row + '].PlannedSubmitDate" class="form-control editDoc forCheckReadOnly" readonly="true" id="txtPlnSubDate_' + row.AgencyDocListId + '"  value=' + PlannedSubmitDate + '>';
                    }
                    else {
                        if (row.IsPrevRevHasPlanedDate != null) {
                            str = '<input type="text" name="AgencyDocumentList[' + meta.row + '].PlannedSubmitDate" class="form-control editDoc forCheckReadOnly" readonly="true" id="txtPlnSubDate_' + row.AgencyDocListId + '"  value=' + PlannedSubmitDate + '>';
                        }
                        else {
                            str = '<input type="text" name="AgencyDocumentList[' + meta.row + '].PlannedSubmitDate" class="form-control datepickerPlnSubDate editDoc" id="txtPlnSubDate_' + row.AgencyDocListId + '"  value=' + PlannedSubmitDate + '>';
                        }
                    }
                    // }
                    return str;
                }
            },
            {
                "sTitle": "Planned Approval Date", "orderable": true, "className": "", render: function (data, type, row, meta) {
                    var str;
                    var PlannedApprovalDate = row.PlannedApprovalDate || "";
                    var docid = row.GenDocID || 0;
                    if (docid > 0) {
                        str = '<input type="text" name="AgencyDocumentList[' + meta.row + '].PlannedApprovalDate" class="form-control editDoc forCheckReadOnly"  readonly="true" id="txtPlnAprDate_' + row.AgencyDocListId + '"  value=' + PlannedApprovalDate + '>';
                    }
                    else {
                        if (row.IsPrevRevHasPlanedDate != null || row.RevisionNo != row.MINRevision) {
                            str = '<input type="text" name="AgencyDocumentList[' + meta.row + '].PlannedApprovalDate" class="form-control editDoc forCheckReadOnly"  readonly="true" id="txtPlnAprDate_' + row.AgencyDocListId + '"  value=' + PlannedApprovalDate + '>';
                        }
                        else {
                            str = '<input type="text" name="AgencyDocumentList[' + meta.row + '].PlannedApprovalDate" class="form-control datepickerAprDate editDoc" id="txtPlnAprDate_' + row.AgencyDocListId + '"  value=' + PlannedApprovalDate + '>';
                        }
                    }
                    return str;
                }
            },
            {
                "sTitle": "Weightage", "orderable": true, "className": "", render: function (data, type, row, meta) {
                    var str;

                    var docid = row.GenDocID || 0;
                    if (row.MINRevision == row.RevisionNo) {
                        if (docid > 0) {
                            str = '<input type="text" maxlength="4" class="form-control  allownumericwithdecimals" name="AgencyDocumentList[' + meta.row + '].Weightage" placeholder="Weightage" value="' + (row.Weightage || '') + '"  readonly="true" />';
                        } else {
                            str = '<input type="text" maxlength="4" class="form-control allownumericwithdecimals" name="AgencyDocumentList[' + meta.row + '].Weightage" placeholder="Weightage" value="' + (row.Weightage || '') + '" />';
                        }
                    }
                    else {
                        str = '<input type="text" maxlength="4" class="form-control  allownumericwithdecimals" readonly="true" />';
                    }
                    return str;

                }
            }
        ],

        "initComplete": function () {

            $searchButton = $('<div class="filed-list"><button class="btn btn-secondary active"><i class="la la-ellipsis-v"></i></button ><div class="hover-div"><h5>Searchable fields</h5><ul class="list-inline"><li class="list-inline-item">Doc no</li>  <li class= "list-inline-item" >Customer Doc no</li><li class= "list-inline-item" >Planned submit date</li></ul ></div ></div > ');
            $('.dataTables_filter').append($searchButton);

            setTimeout(function () {
                tblAgencyDocs.columns.adjust();
            }, 500);
            // AddDefaultRow();
        },
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            //$(nRow).attr('data-docid', aData.AgencyDocListId);
            //  AddDefaultRow();

        },
        "preDrawCallback": function () {
            if (String($("#IsCommentChanges").val()) == "true") {
                $("#FormTransmittal").submit();
                $("#IsCommentChanges").val("false");
            }
        }

    });
    BindJEPDoc();
    BindGlobalJEPDoc();
    funGetWeightage();
    //$(".datepickerroc").datepicker({
    //    minDate: 0,
    //    rtl: mUtil.isRTL(),
    //    todayHighlight: !0,
    //    orientation: "bottom left",
    //    format: 'dd/mm/yyyy'
    //});

});

$("body").on("keypress", ".allownumericwithdecimals", function (e) {
    $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
    if ((e.which != 46 || $(this).val().indexOf('.') != -1) && (e.which < 48 || e.which > 57)) {
        return false;
    }
    if ($(this).val().indexOf('.').length > 2) {
        return false;
    }
});

//$(".forCheckReadOnly").on("click",function (){
//    if ($('.forCheckReadOnly').prop('readonly') == true) {
//        $('.forCheckReadOnly').unbind(event);
//    }
//});



function funGetWeightage() {
    $.ajax({
        type: "POST",
        url: '/DES/Transmittal/GetWeightage',
        data: JSON.stringify({ AgencyId: $("#AgencyId").val() }),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        async: true,
        success: function (response) {
            WeightageList = response;
            var tot = 0;
            for (var i = 0; i < WeightageList.length; i++) {
                tot += WeightageList[i].Item2;
            }

            $("#TxtTotalWeightage").val(tot);
        },
        error: function (error) {

        }
    });
}

$("#tblAgencyDocs").on("focusout", "input[name$='Weightage']", function () {

    var id = this.name;
    var tr = $(this).closest('tr');
    var row = tblAgencyDocs.row(tr);
    var data = row.data();

    funCalcTotalWeightage(data.AgencyDocListId, id);
});

function funCalcTotalWeightage(AgencyDocListId, id) {

    for (var i = 0; i < WeightageList.length; i++) {
        if (WeightageList[i].Item1 == AgencyDocListId) {
            WeightageList[i].Item2 = 0;
            break;
        }
    }

    var JEPDocWeightage = $("input[name$='" + id + "']").val();
    if (WeightageList != null && WeightageList != "" && WeightageList.length > 0) {
        var wt = 0, Fwt = 0;
        for (var i = 0; i < WeightageList.length; i++) {
            wt = (parseFloat(wt) + parseFloat(WeightageList[i].Item2))
        }
        var newwt = (parseFloat(wt) + parseFloat(JEPDocWeightage || 0))
        if (newwt > 100) {
            Fwt = (parseFloat(100) - parseFloat(wt));
            if (Fwt < 0)
                Fwt = 0;
            $("input[name$='" + id + "']").val(parseFloat(Fwt || 0))
        } else {
            Fwt = parseFloat(JEPDocWeightage || 0);
            if (Fwt < 0)
                Fwt = 0;
            $("input[name$='" + id + "']").val(parseFloat(Fwt || 0))
        }

        for (var i = 0; i < WeightageList.length; i++) {
            if (WeightageList[i].Item1 == AgencyDocListId) {
                WeightageList[i].Item2 = Fwt;
                break;
            }
        }
    }
    funPrintTotalWeightage();
}

function funPrintTotalWeightage() {
    var tot = 0;
    for (var i = 0; i < WeightageList.length; i++) {
        tot += WeightageList[i].Item2;
    }

    $("#TxtTotalWeightage").val(tot);
}

$("#SearchDoc").click(function (e) {
    if (($("#AgencyId option:selected").val()) > 0) {
        $("#chbHeader").prop("checked", false);
        $("#chbHeaderGlobalDoc").prop("checked", false);
        selectedDocIDs = [];
        FinalDoclist = [];
        tblJepDoc.ajax.reload();
        tblGlobalJepDoc.ajax.reload();
        $("#m_modal_4").modal("show");
    }
    else {
        $("#m_modal_4").modal("hide");
        toastr.info("Please select Agency!", "Info");
    }
});
$("#searchGlobalJEPDoc").click(function () {
    tblGlobalJepDoc.ajax.reload();
    $("#txtProjectNo").val('');
    $("#txtDocumentNo").val('');
});


//function BindAgencyDoc() {
//}

function BindJEPDoc() {
    tblJepDoc = $('#tblJepDoc').DataTable({
        "stateSave": true,
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "sAjaxSource": "/Transmittal/GetJEPDocList",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        "responsive": true,
        "language": {
            "infoFiltered": ""
        },
        "bRetrieve": true,
        //"scroller": {
        //    loadingIndicator: true
        //},
        //"bSort": true,
        //"aaSorting": [[1, 'desc']],
        "order": [[1, 'desc']],
        'columnDefs': [
            {
                'orderable': false,
                'targets': 0,
                'checkboxes': {
                    'selectRow': true
                }
            }],
        'select': {
            'style': 'multi'
        },
        "fnServerData": function (sSource, data, fnCallback) {
            data.push({ name: "AgencyID", value: $("#AgencyId option:selected").val() });
            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });

        },
        "aoColumns": [
            {
                "sTitle": "<label class='m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--success'><input type='checkbox' id='chbHeader' onclick='SelectAllJEPDoc()'></input><span></span></label>", "sDefaultContent": "<input type='checkbox' ></input>", "bSortable": false, "className": "action", "orderable": false, render: function (data, type, row, meta) {
                    var srt = '<label class="m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--success">'
                    var IDIndex = selectedDocIDs.indexOf(row.JEPDocumentDetailsId);
                    if (IDIndex !== -1) {
                        srt += '<input type="checkbox" class="m-checkable" id="IsSelect_' + row.JEPDocumentDetailsId + '"  data-id="' + row.JEPDocumentDetailsId + '" onclick="AddToDocList(this)" checked>';
                    }
                    else {
                        srt += '<input type="checkbox" class="m-checkable" id="IsSelect_' + row.JEPDocumentDetailsId + '"  data-id="' + row.JEPDocumentDetailsId + '" onclick="AddToDocList(this)">';
                    }
                    srt += "<span></span></label>";
                    return srt;
                }
            },
            {
                "sTitle": "Doc No", "className": "", render: function (data, type, row, meta) {
                    //var name = "<strong style='text-transform: capitalize;'>" + row.DocNo + "</strong>";
                    return row.DocNo;
                }
            },
            {
                "sTitle": "Rev", "className": "", render: function (data, type, row, meta) {
                    //var name = "<strong style='text-transform: capitalize;'>" + row.RevisionNo + "</strong>";
                    return "R" + row.RevisionNo;
                }
            },
            {
                "sTitle": "Description", "className": "", render: function (data, type, row, meta) {
                    return row.DocDescription;
                }
            },
            {
                "sTitle": "Planned Submit Date", "className": "", render: function (data, type, row, meta) {
                    return row.PlannedSubmitDate;
                }
            }
        ],

        "initComplete": function () {
            setTimeout(function () {
                tblJepDoc.columns.adjust();
            }, 500);
        },

        "fnRowCallback": function () {
            setTimeout(function () {
                tblJepDoc.columns.adjust();
            }, 500);
        }

    });
}

function BindGlobalJEPDoc() {

    setTimeout(function () {
        tblGlobalJepDoc = $('#tblGlobalJepDoc').DataTable({
            "searching": false,
            "serverSide": true,
            "processing": true,
            "orderMulti": false,
            "filter": true,
            "sAjaxSource": "/Transmittal/GetGlobalJEPDocList",
            "iDisplayLength": 10,
            "ordering": true,
            "scrollX": true,
            "responsive": true,
            "language": {
                "infoFiltered": ""
            },
            "bRetrieve": true,
            //"scroller": {
            //    loadingIndicator: true
            //},
            //"bSort": true,
            //"aaSorting": [[1, 'desc']],
            "order": [[1, 'desc']],
            "columnDefs": [
                { "orderable": false, "targets": 0 }
            ],

            "fnServerData": function (sSource, data, fnCallback) {
                data.push({ name: "AgencyID", value: $("#AgencyId option:selected").val() });
                data.push({ name: "Project", value: $("#txtProjectNo").val() });
                data.push({ name: "DocumentNo", value: $("#txtDocumentNo").val() });
                $.ajax({
                    "dataType": "json",
                    "type": "POST",
                    "url": sSource,
                    "data": data,
                    "success": fnCallback
                });

            },
            "aoColumns": [
                {
                    "sTitle": "<label class='m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--success'><input type='checkbox' id='chbHeaderGlobalDoc' onclick='SelectAllGlobalJEPDoc()'></input><span></span></label>", "sDefaultContent": "<input type='checkbox' ></input>", "bSortable": false, "className": "action", "orderable": false, render: function (data, type, row, meta) {
                        var srt = '<label class="m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--success">'
                        var IDIndex = selectedDocIDs.indexOf(row.JEPDocumentDetailsId);
                        if (IDIndex !== -1) {
                            srt += '<input type="checkbox" class="m-checkable" id="IsSelect_' + row.JEPDocumentDetailsId + '"  data-id="' + row.JEPDocumentDetailsId + '" onclick="AddToDocList(this)" checked>';
                        }
                        else {
                            srt += '<input type="checkbox" class="m-checkable" id="IsSelect_' + row.JEPDocumentDetailsId + '"  data-id="' + row.JEPDocumentDetailsId + '" onclick="AddToDocList(this)">';
                        }
                        srt += "<span></span></label>";
                        return srt;
                    }
                },
                {
                    "sTitle": "Doc No", "className": "", render: function (data, type, row, meta) {
                        //var name = "<strong style='text-transform: capitalize;'>" + row.DocNo + "</strong>";
                        return row.DocNo;
                    }
                },
                {
                    "sTitle": "Rev", "className": "", render: function (data, type, row, meta) {
                        //var name = "<strong style='text-transform: capitalize;'>" + row.RevisionNo + "</strong>";
                        return "R" + row.RevisionNo;
                    }
                },
                {
                    "sTitle": "Description", "className": "", render: function (data, type, row, meta) {
                        return row.DocDescription;
                    }
                }
            ],

            "initComplete": function () {
                tblGlobalJepDoc.columns.adjust();
            },

            "fnRowCallback": function () {
                tblGlobalJepDoc.columns.adjust();
            }


        });
    }, 1000);

}

function refresh() {
    setTimeout(function () {
        tblGlobalJepDoc.columns.adjust();
    }, 1000);
}

$("#btnAddDocList").on("click", function () {
    if (selectedDocIDs.length > 0) {
        mApp.blockPage();
        var FinalDoclist = Array.from(new Set(selectedDocIDs));
        var DocIDs = new Array();

        for (var id in FinalDoclist) {
            DocIDs.push({ JEPDocumentDetailsId: FinalDoclist[id] });
        }

        var model = {};
        model.AgencyId = $("#AgencyId option:selected").val();
        model.IsEmailSentToAgency = "0";
        model.AgencyDocumentList = DocIDs;

        $.ajax({
            type: "POST",
            url: '/DES/Transmittal/AddAgencyDoc',
            data: JSON.stringify({ model: model }),
            dataType: 'json',
            contentType: "application/json;charset=utf-8",
            async: true,
            success: function (response) {
                mApp.unblockPage();
                if (response.Status) {
                    toastr.success(response.Msg, "Success");
                    tblAgencyDocs.ajax.reload();
                    selectedDocIDs = [];
                    funGetWeightage();

                } else {
                    toastr.info(response.Msg, "Info");
                }
            },
            error: function (error) {
                mApp.unblockPage();
                toastr.error("something went wrong try again later", "Info");
            }
        })
    }
    else {
        toastr.info("Please select atleast one document!", "Info");
        //return;
    }
});

$('#tblAgencyDocs').on('click', 'td .btnDelete', function () {
    var tr = $(this).closest('tr');
    var row = tblAgencyDocs.row(tr);
    var data = row.data();
    swal({
        title: "Do you want to delete..?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            if (isConfirm.value) {
                $.ajax({
                    type: "GET",
                    url: '/DES/Transmittal/DeleteAgencyDoc',
                    data: { Id: data.AgencyDocListId },
                    dataType: 'json',
                    contentType: "application/json;charset=utf-8",
                    async: true,
                    success: function (response) {
                        mApp.unblockPage();
                        if (response.Status) {
                            toastr.success(response.Msg, "Success");
                            if (tblAgencyDocs.data().count() == 1) {
                                tblAgencyDocs.ajax.reload();
                            }
                            else {
                                tblAgencyDocs.ajax.reload(null, false);
                            }
                            WeightageList = [];
                            funGetWeightage();
                        } else {
                            toastr.info(response.Msg, "Info");
                        }
                    },
                    error: function (error) {
                        mApp.unblockPage();
                        toastr.error("something went wrong try again later", "Info");
                    }
                });
            }
        });
});

function Datepickeradd() {
    $(".datepickerroc").datepicker({
        minDate: 0,
        rtl: mUtil.isRTL(),
        todayHighlight: !0,
        orientation: "bottom left",
        format: 'dd/mm/yyyy'
    });
}

function ToJavaScriptDate(value) {
    if (value == null || value == "") {
        var today = new Date();
        var date = today.getDate() + '/' + (today.getMonth() + 1) + '/' + today.getFullYear();
        return date;
    }
    else {
        return value;
    }
}

$("#tblAgencyDocs").on("change keyup", "input", function () {
    $("#IsCommentChanges").val("true");
});

function UpdateDocDetail() {
    mApp.blockPage();
    var AgencyId = $("#AgencyId").val();
    if (AgencyId) {
        $.ajax({
            type: "POST",
            url: '/DES/Transmittal/UpdateAgencyDoc',
            data: JSON.stringify({ model: $("#FormTransmittal").serialize() }),
            dataType: 'json',
            contentType: "application/json;charset=utf-8",
            async: true,
            success: function (response) {
                mApp.unblockPage();
                if (response.Status) {
                    //toastr.success(response.Msg, "Success");
                    tblAgencyDocs.ajax.reload();
                } else {
                    toastr.info(response.Msg, "Info");
                }
            },
            error: function (error) {
                mApp.unblockPage();
                toastr.error("something went wrong try again later", "Info");
            }
        });
    }
    else {
        toastr.info("Agency Name is required", "Info");
    }
}

var TransmittalSuccess = function (d) {
    mApp.unblockPage();
    $("#IsCommentChanges").val("false");
    if (d != null) {

        if (d.Status) {
            toastr.success(d.Msg, "Success");
            if (d.Submitted) {
                window.location.href = "/DES/Transmittal";
            }
        } else {
            toastr.info(d.Msg, "Info");
        }

    }
    funGetWeightage();
}

var TransmittalBegin = function (d) {
    mApp.blockPage();
};

//$("#btnSubmitAgencyDoc").on("click", function () {
//    if (($("#AgencyId option:selected").val()) > 0) {
//        window.location.href = "/DES/Transmittal/GenerateTransmittal?AgencyID=" + $("#AgencyId option:selected").val();
//    }
//    else {
//        toastr.error("Please select Agency!", "Error");
//    }
//});

function AddToDocList($this) {
    var data;
    var allSelected;
    var id = $($this).attr("data-id");
    id = parseInt(id);
    var select = $("#IsSelect_" + id).prop("checked");
    var tableID = $("#IsSelect_" + id).closest("table").attr("id");
    var chbID = $("#" + tableID + " th input[type='checkbox']").attr("id");
    if (tableID == "tblJepDoc") {
        //data = tblJepDoc.rows().data();
        var data = tblJepDoc.rows().data()[0]["DocIDs"];
        var DocIdArray = data.split(',');
    }
    else {
        //data = tblGlobalJepDoc.rows().data();
        var data = tblGlobalJepDoc.rows().data()[0]["DocIDs"];
        var DocIdArray = data.split(',');
    }
    if (select == true) {
        selectedDocIDs.push(id);
        selectedDocIDs = Array.from(new Set(selectedDocIDs));
        DocIdArray.forEach(function (item) {
            item = parseInt(item);
            if (!selectedDocIDs.includes(item)) {
                allSelected = false;
            }
        });
        //data.each(function (value, index) {
        //    if (!selectedDocIDs.includes(data[index].JEPDocumentDetailsId)) {
        //        allSelected = false;
        //    }

        //});
        if (allSelected != false) {
            $("#" + chbID).prop("checked", true);
        }

    }
    else {
        var tableID = $("#IsSelect_" + id).closest("table").attr("id");
        var chbID = $("#" + tableID + " th input[type='checkbox']").attr("id");
        $("#" + chbID).prop("checked", false);
        var index = selectedDocIDs.indexOf(id);
        if (index > -1) {
            selectedDocIDs.splice(index, 1);
        }
    }
    selectedDocIDs = Array.from(new Set(selectedDocIDs));
}

function SelectAllJEPDoc() {
    var data = tblJepDoc.rows().data()[0]["DocIDs"];
    var DocIdArray = data.split(',');
    var select = $("#chbHeader").prop("checked");
    if (select == true) {
        //DocIdArray.each(function (value, index) {
        //    $("#IsSelect_" + DocIdArray[index].JEPDocumentDetailsId).prop("checked", true);
        //    selectedDocIDs.push(DocIdArray[index].JEPDocumentDetailsId);
        //});
        DocIdArray.forEach(function (item) {
            item = parseInt(item);
            $("#IsSelect_" + item).prop("checked", true);
            selectedDocIDs.push(item);
        });
    }
    else {
        DocIdArray.forEach(function (item) {
            item = parseInt(item);
            $("#IsSelect_" + item).prop("checked", false);
            var index = selectedDocIDs.indexOf(item);
            if (index > -1) {
                selectedDocIDs.splice(index, 1);
            }
        });
        //DocIdArray.each(function (value, index) {
        //    $("#IsSelect_" + DocIdArray[index].JEPDocumentDetailsId).prop("checked", false);
        //    var index = selectedDocIDs.indexOf(DocIdArray[index].JEPDocumentDetailsId);
        //    if (index > -1) {
        //        selectedDocIDs.splice(index, 1);
        //    }
        //});
    }
    selectedDocIDs = Array.from(new Set(selectedDocIDs));
}

function SelectAllGlobalJEPDoc() {
    var data = tblGlobalJepDoc.rows().data()[0]["DocIDs"];
    var DocIdArray = data.split(',');
    var select = $("#chbHeaderGlobalDoc").prop("checked");
    if (select == true) {
        DocIdArray.forEach(function (item) {
            item = parseInt(item);
            $("#IsSelect_" + item).prop("checked", true);
            selectedDocIDs.push(item);
        });
        //data.each(function (value, index) {
        //    $("#IsSelect_" + data[index].JEPDocumentDetailsId).prop("checked", true);
        //    selectedDocIDs.push(data[index].JEPDocumentDetailsId);
        //});
    }
    else {
        DocIdArray.forEach(function (item) {
            item = parseInt(item);
            $("#IsSelect_" + item).prop("checked", false);
            var index = selectedDocIDs.indexOf(item);
            if (index > -1) {
                selectedDocIDs.splice(index, 1);
            }
        });
        //data.each(function (value, index) {
        //    $("#IsSelect_" + data[index].JEPDocumentDetailsId).prop("checked", false);
        //    var index = selectedDocIDs.indexOf(data[index].JEPDocumentDetailsId);
        //    if (index > -1) {
        //        selectedDocIDs.splice(index, 1);
        //    }
        //});
    }
    selectedDocIDs = Array.from(new Set(selectedDocIDs));
}

$('#AgencyId').on("change", function (e) {
    var strAgencyId = $("#AgencyId option:selected").val();
    $.ajax({
        type: "POST",
        url: '/DES/Agency/CheckAgencyCheckIn',
        data: JSON.stringify({ AgencyId: strAgencyId }),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        async: true,
        success: function (response) {
            mApp.unblockPage();
            if (response.Status) {
                toastr.info(response.Msg, "Info");
                $("#AgencyId").val("");
                tblAgencyDocs.ajax.reload();
            } else {
                tblAgencyDocs.ajax.reload();
            }
        },
        error: function (error) {
            mApp.unblockPage();
            toastr.error("something went wrong try again later", "Info");
        }
    })
});



//function AddDefaultRow() {
//    setTimeout(function () {
//        Datepickeradd();
//        //trLogic();
//    }, 1000);
//}

$("#tblAgencyDocs").on("focus", ".datepickerPlnSubDate", function () {
    var id = this.id;
    var pid = String(id).replace("txtPlnSubDate_", "");

    var jepdate = $("#JEPPlannedDate_" + pid).val() || "";

    var jepdatesplit = String(jepdate).split("/");
    var v = new Date();
    if (jepdate != "")
        v = new Date(jepdatesplit[2] + '-' + jepdatesplit[1] + '-' + jepdatesplit[0]);

    $("#" + id).datepicker({
        startDate: v,
        todayHighlight: !0, orientation: "bottom left", format: "dd/mm/yyyy", autoclose: true,

    }).on('changeDate', function (e) {

        var newDate = $(this).val();

        var jepNewdatesplit = String(newDate).split("/");
        var v = new Date();
        if (newDate != "") {

            v = new Date(jepNewdatesplit[2] + '-' + jepNewdatesplit[1] + '-' + jepNewdatesplit[0]);
            v.setDate(v.getDate() + 100);

            var DD = v.getDate();
            var MM = v.getMonth() + 1;
            var YYYY = v.getFullYear();

            var newPlnAprDate = DD + "/" + MM + "/" + YYYY;
            if ($("#txtPlnAprDate_" + pid)[0].readOnly != true) {
                $("#txtPlnAprDate_" + pid).val(newPlnAprDate);
            }
        }
    });

    //  var jepAPRdate = $("#txtPlnAprDate_" + pid).val() || "";


});




$("#tblAgencyDocs").on("focus", ".datepickerAprDate", function () {
    var id = this.id;
    var pid = String(id).replace("txtPlnAprDate_", "");

    var PlnSubDate = $("#txtPlnSubDate_" + pid).val() || "";

    var jepdatesplit = String(PlnSubDate).split("/");
    var v = new Date();
    if (PlnSubDate != "")
        v = new Date(jepdatesplit[2] + '-' + jepdatesplit[1] + '-' + jepdatesplit[0])
    else {
        toastr.info("Planned Submit Date is required", "Info");
        return;
    }

    $("#" + id).datepicker({
        startDate: v,
        todayHighlight: !0, orientation: "bottom left", format: "dd/mm/yyyy", autoclose: true
    });
});

$("#m_aside_left_minimize_toggle").click(function () {
    if (tblAgencyDocs != null)
        tblAgencyDocs.columns.adjust();
});

$(".agencyDropdown").change(function () {
    funGetWeightage();
});



