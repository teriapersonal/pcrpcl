var tblObjectList;
var filtercolumnArray = new Array();
var removefiltercolumnArray = new Array();

$(document).ready(function () {
    var IsViewOnly = getUrlParameter('IsViewOnly');


    tblObjectList = $('#tblObjectList').DataTable({
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "sAjaxSource": "/DES/Project/Folder",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        "responsive": false,
        //"fixedColumns": {
        //    leftColumns: 5
        //},
        "language": {
            "infoFiltered": ""
        },
        //"scroller": {
        //    loadingIndicator: true
        //},
        //"bSort": true,
        //"aaSorting": [[1, 'desc']],
        "order": [[0, 'desc']],
        "columnDefs": [
            
            //{ "orderable": false, "targets": 3 },
            //{ "orderable": false, "targets": 4 },
            //{ "orderable": false, "targets": 5 },
            //{ "orderable": false, "targets": 6 },
            //{ "orderable": false, "targets": 7 },
            //{ "visible": false, "targets": 15 },
            //{ "visible": false, "targets": 16 },
            //{ "visible": false, "targets": 17 },
            //{ "visible": false, "targets": 18 },
            //{ "visible": false, "targets": 19 },
            //{ "orderable": false, "targets": 20, "visible": false },
            //{ "orderable": false, "targets": 21, "visible": false },
        ],

        "fnServerData": function (sSource, data, fnCallback) {
            data.push({ name: "searchObjectType", value: $("#GetObjectNameList").val() });
            data.push({ name: "searchObjectStatus", value: $("#GetobjectStatusList").val() });
            data.push({ name: "searchObjectName", value: $("#GetObjectName").val() });
            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });

        },
        "aoColumns": [
            {
                "sTitle": "Actions", "className": "nowrap","oderable":false, render: function (data, type, row, meta) {
                    var srt = "";

                    if ((row.Type.toLowerCase() == 'DOC'.toLowerCase())) {
                        //srt += '\n <a href="/DES/DOC/Detail?q=' + row.QString + '"class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="View">\n                          <i class="la la-eye m--font-success"></i>\n                        </a>';
                        //if (row.Authorized > 0) {
                        //    if (row.Status == "Draft" || row.Status == "Created") {
                        //        srt += '<a href="/DES/DOC/Edit?q=' + row.QString + '" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill btnEditDOC" title="Edit"><i class="la la-edit m--font-brand"></i></a>';
                        //        if (row.ENGGRole > 0) {
                        //            srt += '<a class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnDeleteDOC" title="Delete"><i class="la la-trash text-danger"></i></a>';
                        //        }
                        //    }
                        //    else if (row.Status == "Completed") {
                        //        srt += '\n <a href="javascript:;" id="m_sweetalert_new" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill btnRevDOC" title="Create Revision"  onclick="funRevision()">\n                          <i class="la la-refresh m--font-brand"></i>\n                        </a>';
                        //    }
                        //    srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-info  m-btn--icon m-btn--icon-only m-btn--pill" title="Create ROC"><i class="la la-comments m--font-info" onclick="OpenROC(' + row.ObjectId + ')"></i></a>';
                        //}

                        if (IsViewOnly) {
                            srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill  btnViewDOC" title="View"><i class="la la-eye m--font-success"></i></a>';
                        }
                        else {

                            if (row.Authorized > 0) {
                                if (row.Status == "Draft" || row.Status == "Created") {
                                    srt += '<a  class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill btnEditDOC" title="Edit"><i class="la la-edit m--font-brand"></i></a>';
                                    srt += '<a  class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnDeleteDOC" title="Delete"><i class="la la-trash text-danger"></i></a>';
                                }
                                else if (row.Status == "Completed" && row.ENGGRole > 0) {
                                    srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill btnRevDOC"  onclick="funRevision()" title="Create Revision"><i class="la la-refresh m--font-brand"></i></a>';
                                    srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-info  m-btn--icon m-btn--icon-only m-btn--pill" title="Create ROC"><i class="la la-comments m--font-info" onclick="OpenROC(\'' + row.ObjectId + '\',\'' + Project + '\')"></i></a>';
                                }
                                //if (row.ENGGRole > 0) {

                                //}
                            }
                            srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill  btnViewDOC" title="View"><i class="la la-eye m--font-success"></i></a>';
                        }
                    }
                    else if (row.Type.toLowerCase() == 'Part'.toLowerCase()) {
                        if (IsViewOnly) {
                            srt += '<a href="/DES/Part/Detail?q=' + row.GString + '" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="View"> <i class="la la-eye m--font-success"></i></a>';
                        }
                        else {
                            if (row.Authorized > 0) {
                                srt += '\n <a href="/DES/Part/EditItem?q=' + row.GString + '"  data-Id="' + row.ObjectId + '" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="edit">\n                          <i class="la la-edit m--font-brand"></i>\n                </a>';
                            }
                        }
                        //srt += '\n <a href="javascript:;" id="m_sweetalert_new" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Create Revision">\n                          <i class="la la-refresh m--font-brand"></i>\n                        </a>';
                        //srt += '\n <a href="/DES/Part/Detail?q=' + row.QString + '"class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="View">\n                          <i class="la la-eye m--font-success"></i>\n                        </a>';
                        //srt += ' \n <a href="/DES/Part/Delete?q=' + row.ObjectId + '"class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete">\n<i class="la la-trash m--font-danger"></i>\n                        </a>';
                    }
                    else if (row.Type.toLowerCase() == 'DIN'.toLowerCase()) {
                        if (IsViewOnly) {
                            srt += '<a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill  btnViewDIN" title="View"><i class="la la-eye m--font-success"></i></a>';
                        }
                        else {
                            srt += '<a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill  btnViewDIN" title="View"><i class="la la-eye m--font-success"></i></a>';
                            if (row.Authorized > 0) {
                                if (row.Status == "Created" || row.Status == "Draft") {
                                    srt += '<a  class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill btnEditDIN" title="Edit"><i class="la la-edit m--font-brand"></i></a>';

                                    srt += '<a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnDeleteDIN" title="Delete"><i class="la la-trash text-danger"></i></a>';
                                }
                                else if (row.Status == "Completed") {
                                    srt += '<a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill btnRevDIN" title="Create Revision"><i class="la la-refresh m--font-brand"></i></a>';
                                }
                            }
                        }
                    }
                    else if (row.Type.toLowerCase() == 'JEP'.toLowerCase()) {

                        if (IsViewOnly) {
                            srt += '<a href="/DES/JEP/Detail?q=' + row.GString + '" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="View"> <i class="la la-eye m--font-success"></i></a>';
                        }
                        else {

                            if (row.Authorized > 0) {
                                if (row.ENGGRole > 0) {
                                    srt += '<a href="/DES/JEP/GetCustomerFeedback?q=' + row.QString + '" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" title="Add Customer Feedback"><i class="la la-comment text-info"></i></a>';
                                }

                                if (row.Status === 'Draft' || row.Status === 'Created') {
                                    srt += '<a href="/DES/JEP/Edit?q=' + row.QString + '" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Edit"><i class="la la-edit text-brand"></i></a>';
                                }
                            }
                            srt += '<a href="/DES/JEP/Detail?q=' + row.GString + '" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="View"> <i class="la la-eye m--font-success"></i></a>';
                            //srt += '\n <a href="javascript:;" id="m_sweetalert_new" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Create Revision">\n                          <i class="la la-refresh m--font-brand"></i>\n                        </a>';
                            //  srt += '<a href="/DES/JEP/Detail?q=' + row.GString + '" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="View"> <i class="la la-eye m--font-success"></i></a>';
                            //srt += ' \n <a href="/DES/JEP/Delete?q=' + row.ObjectId + '"class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete">\n<i class="la la-trash m--font-danger"></i>\n                        </a>';
                        }
                    }
                    else if (row.Type.toLowerCase() == 'DCR'.toLowerCase()) {
                        if (IsViewOnly) {
                            srt += '<a href="/DES/DCR/Detail?q=' + row.QString + '" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="Review"><i class="la la-eye m--font-success"></i></a>';
                        }
                        else {
                            if (row.Authorized > 0) {
                                if (IsInprocess.toLowerCase() == 'false') {
                                    srt += '<a href="/DES/DCR/Detail?q=' + row.QString + '" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="Review"><i class="la la-eye m--font-success"></i></a>';
                                    srt += '<a href="javascript:;"  class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill btnEditPart"  data-toggle="modal" data-target="#m_modal_4" onclick="GetDCRAndApart(' + row.DCRId + ')"><i class="la la-files-o " title="Affected Part/Doc"></i></a>';
                                }
                                else {
                                    if (row.Status == "Created") {
                                        if (row.CreatedBy == psno) {
                                            srt += '<a href="/DES/DCR/Edit?q=' + row.QString + '" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Edit"><i class="la la-edit text-brand"></i></a>';
                                            srt += '<a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnDeleteDCR" title="Delete"><i class="la la-trash text-danger"></i></a>';
                                            srt += '<a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill btnCancel" title="Cancel"><i class="la la-close text-info"></i></a>';
                                        }
                                    }
                                    else {
                                        srt += '<a href="/DES/DCR/Detail?q=' + row.QString + '" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="Review"><i class="la la-eye m--font-success"></i></a>';
                                        srt += '<a href="javascript:;"  class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill btnEditPart"  data-toggle="modal" data-target="#m_modal_4" onclick="GetDCRAndApart(' + row.DCRId + ')"><i class="la la-files-o " title="Affected Part/Doc"></i></a>';
                                    }
                                }
                            }
                            else {
                                srt += '<a href="/DES/DCR/Detail?q=' + row.QString + '" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="Review"><i class="la la-eye m--font-success"></i></a>';
                            }
                        }
                    }
                    return srt;
                }
            },
            {
                "sTitle": "Object Name", "className": "", "orderable": true, render: function (data, type, row, meta) {
                    var srt = "";
                    if (row.Type.toLowerCase() == 'DOC'.toLowerCase()) {
                        srt = "<i class='flaticon-doc m--font-accent'></i> " + row.ObjectName;
                    }
                    else if (row.Type.toLowerCase() == 'Part'.toLowerCase()) {
                        srt = "<i class='flaticon-edit m--font-accent'></i> " + row.ObjectName;
                    }
                    else {
                        srt = row.ObjectName;
                    }
                    return srt;
                }
            },
            {
                "sTitle": "Rev",  "orderable": false,render: function (data, type, row, meta) {
                    if (row.Type.toLowerCase() == 'DIN'.toLowerCase()) {
                        return row.Rev;
                    }
                    else if (row.Type.toLowerCase() == 'DCR'.toLowerCase() || row.Type.toLowerCase() == 'JEP'.toLowerCase()) {
                        return 'N/A';
                    }
                    else {
                        return 'R' + row.Rev;
                    }

                }
            },

            {
                "sTitle": "Title", "orderable": true, render: function (data, type, row, meta) {
                    return row.Title;
                }
            },
            {
                "sTitle": "Type", "className": "", "orderable": true, render: function (data, type, row, meta) {
                    return row.Type;
                }
            },
            {
                "sTitle": "Related DCR", "className": "", "orderable": false, render: function (data, type, row, meta) {
                    if (row.RelatedDCR != null && row.RelatedDCR != "" && row.RelatedDCR != "N/A") {
                        //var json = $.parseJSON(row.RelatedDCR);
                        ////var json = $.parseJSON(rlDCR);
                        //var res=[];
                        //for (i = 0; i < json.length; i++) {
                        //    res.push(json[i].DCRNo);
                        //}
                        //var result = res.join(',');
                        //return result;
                        return row.RelatedDCR;
                    }
                    else {
                        return row.RelatedDCR;
                    }
                }
            },

            {
                "sTitle": "Related DIN", "className": "", "orderable": false, render: function (data, type, row, meta) {
                    if (row.RelatedDIN != null && row.RelatedDIN != "" && row.RelatedDIN != "N/A") {
                        //var json = $.parseJSON(row.RelatedDIN);  
                        //var res = [];
                        //for (i = 0; i < json.length; i++) {
                        //    res.push(json[i].DINNo);
                        //}
                        //var result = res.join(',');
                        //return result;
                        return row.RelatedDIN;
                    }
                    else {
                        return row.RelatedDIN;
                    }
                }
            },
            {
                "sTitle": "Policy", "className": "", "orderable": false,render: function (data, type, row, meta) {
                    return row.Policy;
                }
            },
            {
                "sTitle": "Status", "className": "", "orderable": true, render: function (data, type, row, meta) {
                    var str = '';
                    if (row.Type.toLowerCase() == 'DCR'.toLowerCase()) {
                        if (row.Status == "Created") {
                            str = 'Draft';
                        }
                        else {
                            str = row.Status;
                        }
                        return str;
                    }
                    else
                        return row.Status;
                }
            },
        ],

        "initComplete": function () {
            $searchButton = $('<div class="filed-list"><button class="btn btn-secondary active"><i class="la la-ellipsis-v"></i></button ><div class="hover-div"><h5>Searchable fields</h5><ul class="list-inline"><li class="list-inline-item">Object Name</li>  <li class= "list-inline-item" >Title</li><li class="list-inline-item">Type</li><li class="list-inline-item">Status</li></ul ></div ></div > ');
            $('.dataTables_filter').append($searchButton);

            var $filterBoxObjectList = $('#tblObjectList_wrapper .dataTables_filter input').unbind(), selfObjectList = this.api();
            var stoppedTypingObjectList;

            $filterBoxObjectList.on('keyup', function () {
                if (stoppedTypingObjectList) clearTimeout(stoppedTypingObjectList);
                stoppedTypingObjectList = setTimeout(function () {
                    selfObjectList.search($filterBoxObjectList.val()).draw();
                }, 500);
            });
        },

        "fnRowCallback": function () {
            tblObjectList.columns.adjust();
        }

    });

    //Showcolumns();
});


var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};

$("#m_aside_left_minimize_toggle").click(function () {
    if (tblObjectList != null)
        tblObjectList.columns.adjust();
});



var DocId;





$('#btnSearch').click(function () {
    mApp.blockPage();
    tblObjectList.ajax.reload();
    mApp.unblockPage();
});

function openPart($this) {
    var partid = $($this).attr("data-Id");
    if (partid != "") {
        mApp.blockPage();
        $.ajax({
            type: "GET",
            url: "/DES/Part/BOMListPopup",
            data: { ItemId: partid },
            dataType: "html",
            success: function (response) {
                $("#divROC").html(response);
                $('#BOMList').modal('show');
                mApp.unblockPage();
            },
            failure: function (response) {
                toastr.error(response.Msg, "Error");
                mApp.unblockPage();
            },
            error: function (response) {
                toastr.error(response.Msg, "Error");
                mApp.unblockPage();
            }
        });
    }
}

$('#tblObjectList').on('click', 'td .btnDeleteDCR', function () {
    var tr = $(this).closest('tr');
    var row = tblObjectList.row(tr);
    var data = row.data();
    swal({
        title: "Do you want to delete..?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            if (isConfirm.value) {
                mApp.blockPage();
                $.ajax({
                    type: "GET",
                    url: '/DES/DCR/Delete',
                    data: { Id: data.ObjectId },
                    dataType: 'json',
                    contentType: "application/json;charset=utf-8",
                    async: true,
                    success: function (response) {
                        mApp.unblockPage();
                        if (response.Status) {
                            toastr.success(response.Msg, "Success");
                            tblObjectList.ajax.reload();
                        } else {
                            toastr.error(response.Msg, "Error");
                        }
                    },
                    error: function (error) {
                        mApp.unblockPage();
                        toastr.error("something went wrong try again later", "Error");
                    }
                });
            }
        });
});

$('#tblObjectList').on('click', 'td .btnCancel', function () {
    var tr = $(this).closest('tr');
    var row = tblObjectList.row(tr);
    var data = row.data();

    swal({
        title: "Do you want to cancel..?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            if (isConfirm.value) {
                mApp.blockPage();
                $.ajax({
                    type: "POST",
                    url: '/DES/DCR/UpdateDCRStatus',
                    data: JSON.stringify({ DCRId: data.ObjectId, Status: 'Cancelled' }),
                    dataType: 'json',
                    contentType: "application/json;charset=utf-8",
                    async: true,
                    success: function (response) {
                        mApp.unblockPage();
                        if (response.Status) {
                            toastr.success(response.MSG, "Success");
                            tblObjectList.ajax.reload();
                        } else {
                            toastr.error(response.MSG, "Error");
                        }
                    },
                    error: function (error) {
                        toastr.error("something went wrong try again later", "Error");
                        mApp.unblockPage();
                    }
                })
            }
        });
});

//DOC

$('#tblObjectList').on('click', 'td .btnRevDOC', function () {
    var tr = $(this).closest('tr');
    var row = tblObjectList.row(tr);
    var data = row.data();
    DocId = data.QString;
});

$('#tblObjectList').on('click', 'td .btnDeleteDOC', function () {
    var tr = $(this).closest('tr');
    var row = tblObjectList.row(tr);
    var data = row.data();
    swal({
        title: "Do you want to delete..?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            if (isConfirm.value) {
                mApp.blockPage();
                $.ajax({
                    type: "GET",
                    url: '/DES/DOC/Delete',
                    data: { Id: data.ObjectId },
                    dataType: 'json',
                    contentType: "application/json;charset=utf-8",
                    async: true,
                    success: function (response) {
                        if (response.Status) {
                            toastr.success(response.Msg, "Success");
                            tblObjectList.ajax.reload();
                            mApp.unblockPage();
                        } else {
                            toastr.error(response.Msg, "Error");
                            mApp.unblockPage();
                        }
                    },
                    error: function (error) {
                        toastr.error("something went wrong try again later", "Error");
                        mApp.unblockPage();
                    }
                });
            }
        });
});

$('#tblObjectList').on('click', 'td .btnEditDOC', function () {
    var tr = $(this).closest('tr');
    var row = tblObjectList.row(tr);
    var data = row.data();
    window.location = "/DES/DOC/Edit?q=" + data.QString;
});

$('#tblObjectList').on('click', 'td .btnViewDOC', function () {
    var tr = $(this).closest('tr');
    var row = tblObjectList.row(tr);
    var data = row.data();
    window.location = "/DES/DOC/Detail?q=" + data.QString;
});

function funRevision() {
    swal({
        title: "Are You Sure?",
        text: "Do you want to create new Revision?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            var rev = "Yes";
            if (isConfirm.value) {
                window.location = "/DES/DOC/Edit?q=" + DocId + "&Revision=" + rev;
            } else {
            }
        });


}

function OpenROC(documentid, Project) {
    mApp.blockPage();
    $.ajax({
        type: "POST",
        url: "/DES/ROC/ShowROCPopup",
        data: {
            documentId: documentid,
            docProject: Project
        },
        dataType: "html",
        success: function (response) {
            $("#divROCShowINDOC").html(response);
            $('#ROCList').modal('show');
            mApp.unblockPage();
        },
        failure: function (response) {
            toastr.error(response.Msg, "Error");
            mApp.unblockPage();
        },
        error: function (response) {
            toastr.error(response.Msg, "Error");
            mApp.unblockPage();
        }
    });
}
//DIN

$('#tblObjectList').on('click', 'td .btnViewDIN', function () {
    var tr = $(this).closest('tr');
    var row = tblObjectList.row(tr);
    var data = row.data();
    window.location = "/DES/DIN/Detail?q=" + data.QString;
});

$('#tblObjectList').on('click', 'td .btnRevDIN', function () {
    var tr = $(this).closest('tr');
    var row = tblObjectList.row(tr);
    var data = row.data();

    swal({
        title: "Do you want to create new Issue?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            if (isConfirm.value) {
                var model = {
                    DINId: data.ObjectId
                };
                mApp.blockPage();
                $.ajax({
                    type: "Post",
                    url: '/DES/DIN/CreateDINRev',
                    data: JSON.stringify(model),
                    dataType: 'json',
                    contentType: "application/json;charset=utf-8",
                    async: true,
                    success: function (response) {
                        if (response.Status) {
                            toastr.success(response.Msg, "Success");
                            tblObjectList.ajax.reload();
                            window.location = response.rurl;
                            mApp.unblockPage();
                        } else {
                            toastr.error(response.Msg, "Error");
                            mApp.unblockPage();
                        }
                    },
                    error: function (error) {
                        toastr.error("something went wrong try again later", "Error");
                        mApp.unblockPage();
                    }
                });
            }
        });
});

$('#tblObjectList').on('click', 'td .btnEditDIN', function () {
    var tr = $(this).closest('tr');
    var row = tblObjectList.row(tr);
    var data = row.data();
    window.location = "/DES/DIN/Edit?q=" + data.QString;
});

$('#tblObjectList').on('click', 'td .btnDeleteDIN', function () {
    var tr = $(this).closest('tr');
    var row = tblObjectList.row(tr);
    var data = row.data();
    swal({
        title: "Do you want to delete?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            if (isConfirm.value) {
                mApp.blockPage();
                $.ajax({
                    type: "GET",
                    url: '/DES/DIN/DeleteDIN',
                    data: { DINId: data.ObjectId },
                    dataType: 'json',
                    contentType: "application/json;charset=utf-8",
                    async: true,
                    success: function (response) {
                        if (response.Status) {
                            toastr.success(response.Msg, "Success");
                            tblObjectList.ajax.reload();
                            mApp.unblockPage();
                        } else {
                            toastr.error(response.Msg, "Error");
                            mApp.unblockPage();
                        }
                    },
                    error: function (error) {
                        toastr.error("something went wrong try again later", "Error");
                        mApp.unblockPage();
                    }
                });
            } else {
                mApp.unblockPage();
            }
        });
});

