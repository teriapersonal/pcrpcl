﻿var tblFunctionRole;

$(document).ready(function () {

    tblFunctionRole = $('#tblFunctionRole').DataTable({
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "sAjaxSource": "/FunctionRole/GetFunctionRoleList",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        //"responsive": true,
        "language": {
            "infoFiltered": ""
        },
        //"scroller": {
        //    loadingIndicator: true
        //},
        //"bSort": true,
        //"aaSorting": [[1, 'desc']],
        "order": [[0, 'desc']],
        "columnDefs": [
            { "orderable": false, "targets": 2 }
        ],

        "fnServerData": function (sSource, data, fnCallback) {

            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });

        },
        "aoColumns": [
            {
                "sTitle": "Function <span class='text-danger'>*</span>", "className": "", render: function (data, type, row, meta) {
                    var Functionname = "<strong style='text-transform: capitalize;'>" + row.t_desc + "</strong>";
                    return Functionname;
                }
            },
            {
                "sTitle": "Role <span class='text-danger'>*</span>", "className": "", render: function (data, type, row, meta) {
                    var Rolenames = "<strong style='text-transform: capitalize;'>" + row.Rolename + "</strong>";
                    return Rolenames;
                }
            },
            {
                "sTitle": "Action", "className": "action", "orderable": false, render: function (data, type, row, meta) {
                    var srt = "";
                    srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill btnEdit" title="Edit"><i class="la la-edit text-info"></i></a>'; /*onclick="GetAllRoleByFunctionId(' + row.FunctionId + ');"*/
                    srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnDelete" title="Delete"><i class="la la-trash text-danger"></i></a>';
                    return srt;
                }
            }
        ],

        "initComplete": function () {
            AddFunRoleRow();
        },

        "fnRowCallback": function () {
            AddFunRoleRow();
            
        }
    });
});


function AddFunRoleRow() {
    setTimeout(function () {
        if ($("#FunctionList_0").length == 0)
            var html = '<tr class="v-top">'
        + '<td><select id="FunctionList_0" class="form-control"><option value="">Select</option>';
        for (let i = 0; i < ddlfunctionlist.length; i++) {
            html += "<option value='" + ddlfunctionlist[i].t_dimx + "'>" + ddlfunctionlist[i].t_desc + "</option>";
        }
        html += '</select></br><label class="text-danger" id="errorFunction_0" style="display:none" >Function is required </label></td> '

        + '<td><select class="form-control" multiple="multiple" id="RoleList_0">';
        for (let i = 0; i < ddlrolelist.length; i++) {
            html += "<option value='" + ddlrolelist[i].Id + "'>" + ddlrolelist[i].Role + "</option>";
        }
        html += '</select></br><label class="text-danger" id="errorRole_0" style="display:none" >Role is required </label></td> '

            + '<td><a href="#" class="btn m-btn btn-success m-btn--pill m-btn--center btn-sm special-add-btn" onclick="AddFunctionRole(0)" title="Add"><i class="fa fa-plus"></i> Add </a></td> </tr>';
        $('#tblFunctionRole tbody').prepend(html);
        $("#RoleList_0").select2();
        $("#RoleList_0").select2({
            placeholder: "-----Select-----"
        });
        $("#FunctionList_0").select2();
    }, 1000);
}

function AddFunctionRole(id) {
    var functionids = $("#FunctionList_" + id).val();
    var roleids = $("#RoleList_" + id).val();
    var Roles = "";
    //if (roleids.length > 0 && functionids > 0) {
        for (var i = 0; i < roleids.length; i++) {
            if (i == 0)
                Roles = roleids[i];
            else
                Roles += "," + roleids[i];
        }
    if ($("#FunctionList_" + id).val() != "" && $("#RoleList_" + id).val() != "") {
            mApp.blockPage();
            $.ajax({
                type: "POST",
                url: '/DES/FunctionRole/AddFunctionRole',
                data: JSON.stringify({ FunctionRoleMappingId: id, FunctionId: functionids, RoleList: Roles }),
                dataType: 'json',
                contentType: "application/json;charset=utf-8",
                async: true,
                success: function (response) {
                    mApp.unblockPage();
                    if (response.Status) {
                        toastr.success(response.Msg, "Success");
                        tblFunctionRole.ajax.reload();
                    } else {
                        toastr.error(response.Msg, "Error");
                    }
                },
                error: function (error) {
                    mApp.unblockPage();
                    toastr.error("something went wrong try again later", "Error");
                }
            });
        }
        else {
            if ($("#FunctionList_" + id).val() == "")
                $("#errorFunction_" + id).show();
            if ($("#RoleList_" + id).val() == "")
                $("#errorRole_" + id).show();
        }   
}

$('#tblFunctionRole').on('click', 'td .btnEdit', function () {
    var RoleArray = "";
    var tr = $(this).closest('tr');
    var row = tblFunctionRole.row(tr);
    var data = row.data();

    var html = '<td><select class="form-control" disabled="disabled" id="FunctionList_' + data.FunctionRoleMappingId + '">';
            for (let i = 0; i < ddlfunctionlist.length; i++) {
                if (ddlfunctionlist[i].t_dimx == data.FunctionId)
                    html += "<option  value='" + ddlfunctionlist[i].t_dimx + "' selected>" + ddlfunctionlist[i].t_desc + "</option>";
                else
                    html += "<option value='" + ddlfunctionlist[i].t_dimx + "'>" + ddlfunctionlist[i].t_desc + "</option>";
            }
            html += '</select></br><label class="text-danger" id="errorFunction_' + data.FunctionRoleMappingId + '" style="display:none" >Function is required </label></td> '

        + '<td><select class="form-control" multiple="multiple" id="RoleList_' + data.FunctionRoleMappingId + '">';
            for (let i = 0; i < ddlrolelist.length; i++) {

                html += "<option value='" + ddlrolelist[i].Id + "'>" + ddlrolelist[i].Role + "</option>";

            }
            html += '</select></br><label class="text-danger" id="errorRole_' + data.FunctionRoleMappingId + '" style="display:none" >Role is required </label></td> '

        + '<td><a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" onclick="AddFunctionRole(' + data.FunctionRoleMappingId + ')" title="Update"><i class="la la-check text-info"></i></a>'
        + '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" onclick="tblFunctionRoleClear()" title="Cancel"><i class="la la-close text-danger"></i></a></td>';

    $(tr).html(html);   
    mApp.blockPage();
    $.ajax({
        type: "GET",
        url: "/DES/FunctionRole/GetRoleByFunctionId",
        data: { FunctionId: data.FunctionId },
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        success: function (resonse) {
            mApp.unblockPage();
            RoleArray = resonse.Msg;
            var SelectedRoleId = RoleArray.split(',');
            var Roleids = [];
            for (var i = 0; i < SelectedRoleId.length; i++) {
                Roleids.push(String(SelectedRoleId[i].trim()));
            }
            $('#RoleList_' + data.FunctionRoleMappingId).select2();
            $('#RoleList_' + data.FunctionRoleMappingId).val(Roleids).trigger('change');
        }
    });

    $("#FunctionList_" + data.FunctionRoleMappingId).focus();

});

function tblFunctionRoleClear() {
    tblFunctionRole.ajax.reload();
}

$('#tblFunctionRole').on('click', 'td .btnDelete', function () {
    var tr = $(this).closest('tr');
    var row = tblFunctionRole.row(tr);
    var data = row.data();
    swal({
        title: "Do you want to delete..?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            if (isConfirm.value) {
                mApp.blockPage();
                $.ajax({
                    type: "GET",
                    url: '/DES/FunctionRole/DeleteFunctionRole',
                    data: { Id: data.FunctionRoleMappingId },
                    dataType: 'json',
                    contentType: "application/json;charset=utf-8",
                    async: true,
                    success: function (response) {
                        mApp.unblockPage();
                        if (response.Status) {
                            toastr.success(response.Msg, "Success");
                            tblFunctionRole.ajax.reload();
                        } else {
                            toastr.error(response.Msg, "Error");
                        }
                    },
                    error: function (error) {
                        mApp.unblockPage();
                        toastr.error("something went wrong try again later", "Error");
                    }
                });
            }
        });
});

$("#m_aside_left_minimize_toggle").click(function () {
    if (tblFunctionRole != null)
        tblFunctionRole.columns.adjust();
});

