var DatatablesBasicBasic = {
    init: function () {
        var e;
        (e = $("#m_table_1")).DataTable({
             scrollY: "40vh",
            scrollX: !0,
            dom: "<'row'<'col-sm-12'tr>>\n\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
            lengthMenu: [5, 10, 25, 50],
            pageLength: 50,
            language: {
                lengthMenu: "Display _MENU_"
            },
          
           
            columnDefs: [{
                targets: -1,
                title: "Actions",
                orderable: !1,
                render: function (e, a, t, n) {
                    return '\n                       <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" title="view">\n                          <i class="la la-eye text-info"></i>\n                        </a> '
                }
            }]
        })
    }
};
jQuery(document).ready(function () {
    DatatablesBasicBasic.init()
     $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $($.fn.dataTable.tables(true)).css('width', '100%');
        $($.fn.dataTable.tables(true)).DataTable().columns.adjust().draw();
    });
});



var DatatablesBasicBasic2 = {
    init: function () {
        var e;
        (e = $("#m_table_2")).DataTable({
             scrollY: "40vh",
            scrollX: !0,
            dom: "<'row'<'col-sm-12'tr>>\n\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
            lengthMenu: [5, 10, 25, 50],
            pageLength: 50,
            language: {
                lengthMenu: "Display _MENU_"
            },
          
           
            columnDefs: [{
                targets: -1,
                title: "Actions",
                orderable: !1,
                render: function (e, a, t, n) {
                    return '\n                       <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" title="view">\n                          <i class="la la-eye text-info"></i>\n                        </a> '
                }
            }]
        })
    }
};
jQuery(document).ready(function () {
    DatatablesBasicBasic2.init()
     $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $($.fn.dataTable.tables(true)).css('width', '100%');
        $($.fn.dataTable.tables(true)).DataTable().columns.adjust().draw();
    });
});
var DatatablesBasicBasic3 = {
    init: function () {
        var e;
        (e = $("#m_table_3")).DataTable({
             scrollY: "40vh",
            scrollX: !0,
            dom: "<'row'<'col-sm-12'tr>>\n\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
            lengthMenu: [5, 10, 25, 50],
            pageLength: 50,
            language: {
                lengthMenu: "Display _MENU_"
            },
          
           
            columnDefs: [{
                targets: -1,
                title: "Actions",
                orderable: !1,
                render: function (e, a, t, n) {
                    return '\n                       <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" title="view">\n                          <i class="la la-eye text-info"></i>\n                        </a> '
                }
            }]
        })
    }
};
jQuery(document).ready(function () {
    DatatablesBasicBasic3.init()
     $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $($.fn.dataTable.tables(true)).css('width', '100%');
        $($.fn.dataTable.tables(true)).DataTable().columns.adjust().draw();
    });
});
var DatatablesBasicBasic4 = {
    init: function () {
        var e;
        (e = $("#m_table_4")).DataTable({
             scrollY: "40vh",
            scrollX: !0,
            dom: "<'row'<'col-sm-12'tr>>\n\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
            lengthMenu: [5, 10, 25, 50],
            pageLength: 50,
            language: {
                lengthMenu: "Display _MENU_"
            },
          
           
            columnDefs: [{
                targets: -1,
                title: "Actions",
                orderable: !1,
                render: function (e, a, t, n) {
                    return '\n                       <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" title="view">\n                          <i class="la la-eye text-info"></i>\n                        </a> '
                }
            }]
        })
    }
};
jQuery(document).ready(function () {
    DatatablesBasicBasic4.init()
     $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $($.fn.dataTable.tables(true)).css('width', '100%');
        $($.fn.dataTable.tables(true)).DataTable().columns.adjust().draw();
    });
});