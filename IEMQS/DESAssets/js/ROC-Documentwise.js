﻿var tblROCList;
var getCurrentUser;
$(document).ready(function () {
    getCurrentUser = $('#CurrentPsNo').val();
    setTimeout(function () {
        tblROCList = $('#tblROCList').DataTable({
            "searching": false,
            "paging": true,
            "info": false,
            "lengthChange": false,
            "serverSide": true,
            "processing": true,
            "orderMulti": false,
            "filter": true,
            "sAjaxSource": "/DES/ROC/GetROCList",
            "iDisplayLength": 10,
            "ordering": true,
            "scrollX": true,
            "responsive": false,
            "language": {
                "infoFiltered": ""
            },
            "columnDefs": [
                { "orderable": false, "targets": 0 },
            ],
            "fnServerData": function (sSource, data, fnCallback) {

                data.push({ name: "DocumentId", value: $("#hdndocumentid").val() });
                data.push({ name: "Project", value: $("#hdnProject").val() });
                $.ajax({
                    "dataType": "json",
                    "type": "POST",
                    "url": sSource,
                    "data": data,
                    "success": fnCallback
                });

            },
            "aoColumns": [
                {
                    "sTitle": "Action", "className": "action ", "orderable": false,  render: function (data, type, row, meta) {
                        var srt = "";
                      //  srt += '';

                      ////  if (row.Status === 'Created') {
                      //      srt += '<a href="/DES/ROC/Edit?q=' + row.QString + '" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Edit" ><i class="la la-edit m--font-brand"></i></a>';
                      // // }
                      // // else {
                      //      srt += '<a href="/DES/ROC/Detail?q=' + row.QString + '" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="View"> <i class="la la-eye m--font-success"></i></a>';
                      //      //srt += '<a href="/DES/ROC/Edit?q=' + row.QString + '&Rev=True" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill"  onclick="funRevision()" title="Create Revision"><i class="la la-refresh m--font-brand"></i></a>';
                      // // }
                        //  srt += '';
                        srt += '';
                        if (row.Status === 'Created') {                    
                            if ((row.IsROCCheckIn == true)) {
                                if (row.ROCCheckInBy == getCurrentUser) {
                                    srt += '<button class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnCheckOut" title="CheckOut"><i class="fa fa-lock m--font-danger"></i></button>';
                                    srt += '<a  class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill btnEditROC" title="Edit"><i class="la la-edit m--font-brand"></i></a>';
                                }
                                else {
                                    srt += '<button class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill m-btn--pill btnCheckIn" title="Check In By ' + row.ROCCheckInByUser + '" ><i class="fa fa-lock-open m--font-success"></i></button>';
                                }
                            }
                            else {
                                srt += '<a  class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill btnEditROC" title="Edit"><i class="la la-edit m--font-brand"></i></a>';
                            }
                        }
                        else {
                            srt += '<a href="/DES/ROC/Detail?q=' + row.QString + '" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="View"> <i class="la la-eye m--font-success"></i></a>';
                            srt += '<a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill btnRev"  title="Create Revision"><i class="la la-refresh m--font-brand"></i></a>';
                        }
                        srt += '<a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill btnRocVersion" title="ROC Versions"><i class="fa fa-search-plus m--font-brand"></i></a>';
                        srt += '';
                        return srt;
                    }
                },
                {
                    "sTitle": "ROC Ref No", "className": "", "orderable": true,  "sWidth": "", render: function (data, type, row, meta) {
                        return row.ROCRefNo;
                    }
                },
                {
                    "sTitle": "Ver", "className": "", "orderable": false, "sWidth": "", render: function (data, type, row, meta) {
                        if (row.Version == 0)
                        {
                            row.Version = 1;
                        }
                        else
                        {
                            row.Version
                        }
                        return row.Version;
                    }
                },
                {
                    "sTitle": "Agency Name ", "className": "", "orderable": true,  "sWidth": "", render: function (data, type, row, meta) {
                        return row.AgencyName;
                    }
                },
                {
                    "sTitle": "Created On", "className": "", "sWidth": "", "orderable": false, render: function (data, type, row, meta) {
                        return row.CreatedOn;
                    }
                }
            ],

            //"initComplete": function () {
            //    $searchButton = $('<div class="filed-list"><button class="btn btn-secondary active"><i class="la la-ellipsis-v"></i></button ><div class="hover-div"><h5>Searchable fields</h5><ul class="list-inline"><li class="list-inline-item">ROC Ref No</li>  <li class= "list-inline-item" >Agency Name</li></ul ></div ></div > ');
            //    $('.dataTables_filter').append($searchButton);
            //},

            "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                tblROCList.columns.adjust();
            }

        });
    }, 1000);
    mApp.unblockPage();
});

function AddROC() {
    mApp.blockPage();
    var AgencyId = $("#ddlAgency").val();
    if (AgencyId) {
        var data = {
            documentId: $("#hdndocumentid").val(),
            AgencyId: $("#ddlAgency").val(),
            Project:$("#hdnProject").val()
        };

        $.ajax({
            url: "/DES/ROC/Add", //If get File Then Document Mapping Add
            data: JSON.stringify(data),
            type: "POST",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            aysnc: true,
            success: function (response) {
                mApp.unblockPage();
                if (response.Status) {
                    toastr.success(response.Msg, "Success");
                    window.location = "/DES/ROC/Edit?q=" + response.ROCId;
                } else {
                    if (response.IsRevised) {
                        toastr.info(response.Msg, "Info");
                    } else {
                        toastr.error(response.Msg, "Something went wrong");
                    }
                }
            },
            error: function (response) {
                mApp.unblockPage();
                toastr.error(response.Msg, "Error");
            }
        });
    }
    else {
        mApp.unblockPage();
        toastr.info("Please select agency", "Info");
    }
}

$("#m_aside_left_minimize_toggle").click(function () {
    if (tblROCList != null)
        tblROCList.columns.adjust();
});


var RocId;
$('#tblROCList').on('click', 'td .btnRev', function () {
    var tr = $(this).closest('tr');
    var row = tblROCList.row(tr);
    var data = row.data();
    //  RocId = data.QString;

    swal({
        title: "Do you want to create new revision?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
    function (isConfirm) {
        var rev = "Yes";
        if (isConfirm.value) {
            RevisionData(data.ROCId, data.ROCRefNo, data.Version)
            //  window.location = "/DES/ROC/Edit?q=" + RocId + "&Rev=" + rev;
        } else {
        }
    });

});

function RevisionData(RocId, ROCRefNo, Version) {
    var nxtVersion = Version + 1;
    var rev = "Yes";
    //var rocIDS = RocId;
    var docMapps = {
        rocId: RocId,
    };

    $.ajax({
        url: "/DES/ROC/GetDataOfROCForRev", //If get File Then Document Mapping Add
        data: JSON.stringify(docMapps),
        type: "POST",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        aysnc: true,
        success: function (response) {

            mApp.unblockPage();
            if (response.Status) {

                if (response.AttachFiles != null) {

                    var Project = response.AtProject;
                    var CurrentLocationPath = response.Path;
                    var concatDocandProject = CurrentLocationPath +"/"+Project.toString().trim() + "-ROC-" + ROCRefNo.toString().trim() + "-" + nxtVersion;
                    var CurrentLocationIp = response.Ip;
                   
                    var FCSurl = CurrentLocationIp + "/api/ReviseData";
                    var docMapp = {
                        newDocPath: concatDocandProject,
                        oldDocPath: response.AttachFiles
                    };

                    if (CurrentLocationIp != "" || CurrentLocationIp != null) {
                        //mApp.blockPage();
                        jQuery.ajax({
                            url: FCSurl, // Node Applictation Redrection
                            data: JSON.stringify(docMapp),
                            contentType: 'application/json; charset=utf-8',
                            dataType: 'json',
                        cache: false,
                            async: true,
                            processData: false,
                            method: 'POST',
                            type: 'POST', // For jQuery < 1.9
                            success: function (response) {
                                if (response.ReviseData) {

                                    if (response.paths != null) {
                                        var _ROCModel = {
                                            ROCId: RocId,
                                            newPathAfterRevise: response.paths
                                        };
                                        $.ajax({
                                            type: "POST",
                                            url: "/DES/ROC/NewRevision", //If get File Then Document Mapping Add
                                            data: JSON.stringify(_ROCModel),
                                            contentType: "application/json;charset=utf-8",
                                            dataType: "json",
                                            aysnc: true,
                                            success: function (response) {
                                                if (response.Status) {
                                                    window.location = "/DES/ROC/Edit?q=" + response.NewROCId;
                                                } else {
                                                    toastr.error(response.Msg, "Not Success");
                                                    mApp.unblockPage();
                                                }
                                            },
                                            error: function (response) {
                                                toastr.error(response.Msg, "Error");
                                                mApp.unblockPage();
                                            }
                                        });


                                        // window.location = "/DES/ROC/Edit?q=" + RocId + "&Rev=" + rev + "&RevPaths=" + response.paths;
                                        //mApp.unblockPage();
                                    }
                                }
                            },
                        }).fail(function (data) {
                            toastr.error("Server response not received", "Error");
                            mApp.unblockPage();
                        });

                        $('#IsRevision').val(false);
                    }
                    else {
                        toastr.error("Ip is not detecting", "Error");
                        mApp.unblockPage();
                    }
                }
            } else {
                toastr.error(response.Msg, "Not Success");
            }
        },
        error: function (response) {
            mApp.unblockPage();
            toastr.error(response.Msg, "Error");
        }
    });

}


$('#tblROCList').on('click', 'td .btnRocVersion', function () {
    
    var tr = $(this).closest('tr');
    var row = tblROCList.row(tr);
    var data = row.data();
    var rocMapping = {
        ROCRefNo: data.ROCRefNo
    };
    mApp.blockPage();
    $.ajax({
        url: '/DES/ROC/GetAllVersion',
        data: JSON.stringify(rocMapping),
        type: 'POST',
        traditional: true,
        contentType: 'application/json; charset=utf-8',
        dataType: "Html",
        success: function (result) {
            mApp.unblockPage();
            $("#modelbodyAllVersion").html(result);
            $("#m_modal_1_2").modal("show");
        },
        error: function (xhr) {
            mApp.unblockPage();
        }
    });
});

$('#tblROCList').on('click', 'td .btnEditROC', function () {
    var tr = $(this).closest('tr');
    var row = tblROCList.row(tr);
    var data = row.data();
    if ((data.IsROCCheckIn == true)) {
        if (data.ROCCheckInBy == getCurrentUser) {
            window.location = "/DES/ROC/Edit?q=" + data.QString;
        }
        else {
            toastr.info("Docuement is already in use by " + data.DocumentCheckinBy, "Info");
        }
    }
    else {
        window.location = "/DES/ROC/Edit?q=" + data.QString;
    }
});

$('#tblROCList').on('click', 'td .btnCheckOut', function () {
    var tr = $(this).closest('tr');
    var row = tblROCList.row(tr);
    var data = row.data();
    var docMapp = {
        ROCId: data.ROCId
    };
    swal({
        title: "Do you want to release ROC?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(function (isConfirm) {
        if (isConfirm.value) {
            mApp.blockPage();
            $.ajax({
                url: "/DES/ROC/UpdateROCCheckOut",
                data: JSON.stringify(docMapp),
                type: "POST",
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                aysnc: true,
                success: function (response) {
                    if (response) {
                        if (response.Status) {
                            toastr.info(response.Msg, "Info");
                            tblROCList.ajax.reload();
                            mApp.unblockPage();
                        }
                    }
                },
                error: function (response) {
                    toastr.error("something went wrong try again later", "Error");
                    mApp.unblockPage();
                }

            });
        }
    });
});


$('#tblROCList').on('click', 'td .btnCheckIn', function () {
    var tr = $(this).closest('tr');
    var row = tblROCList.row(tr);
    var data = row.data();
    if ((data.IsROCCheckIn == true)) {
        if (data.ROCCheckInBy != getCurrentUser) {
            toastr.info("Docuement is already in use by " + data.ROCCheckInByUser, "Info");
        }
    }
});

