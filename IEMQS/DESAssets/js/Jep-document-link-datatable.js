

var DatatablesBasicScrollable = {
    init: function () {
        var e;
        (e = $("#m_table_1")).DataTable({
              searching: false,
          paging:   false,
            ordering: false,

            dom: "<'row'<'col-sm-12'tr>>\n\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
          
            columnDefs: [ {
                targets: -1,
                title: "Actions",
                orderable: !1,
                render: function (e, a, t, n) {
                    return '\n <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="edit">\n                          <i class="la la-edit m--font-brand"></i>\n </a> \n <a href="#" data-toggle="modal" data-target="#m_modal_4" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="Link Document">\n                          <i class="la la-paperclip m--font-success"></i>\n </a> \n<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="View">\n<i class="la la-trash m--font-danger"></i>\n                        </a>'
                }
            }, ]
        })
    }
};
jQuery(document).ready(function () {
    DatatablesBasicScrollable.init()
});