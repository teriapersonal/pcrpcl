﻿var tblShowFilelogs;
$(document).ready(function () {

    tblShowFilelogs = $('#tblNotifications').DataTable({
        "searching": true,
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "sAjaxSource": "/Notification/GetAllNotificationsByPsNo",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        "responsive": false,
        "language": {
            "infoFiltered": ""
        },
        //"fixedColumns": {
        //    "leftColumns": 5
        //},
        "bRetrieve": true,
        //"scroller": {
        //    loadingIndicator: true
        //},
        //"bSort": true,
        //"aaSorting": [[1, 'desc']],
        "order": [[0, 'desc']],
        "columnDefs": [
            {
                "targets": [0],
                "visible": false,
                "searchable": false
            }
            //{ "orderable": false, "targets": 0 },
            //{ "orderable": false, "targets": 3 },
            //{ "orderable": false, "targets": 4 },
            //{ "orderable": false, "targets": 5 }
        ],
        "fnServerData": function (sSource, data, fnCallback) {
            //data.push({ name: "SearchText", value: $("#SearchText").val() });
            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });

        },
        "aoColumns": [

            {
                "sTitle": "Notification Id", "className": "", render: function (data, type, row, meta) {
                    return row.NotificationId;
                }
            },
            {
                "sTitle": "Sr.No.", "className": "", render: function (data, type, row, meta) {
                    return row.SN;
                }
            },
            {
                "sTitle": "Notification Message", "className": "", render: function (data, type, row, meta) {
                    var NotificationMsg = "";
                    NotificationMsg = "<a href='" + row.RedirectionPath + "' class='m - list - timeline__text'>" + row.NotificationMsg + "</a>";
                    return NotificationMsg;
                }
            },
            {
                "sTitle": "Created By", "className": "", render: function (data, type, row, meta) {
                    var CreatedBy = "";
                    CreatedBy = row.CreatedBy;

                    return CreatedBy;

                }
            },
            {
                "sTitle": "Created On", "className": "", render: function (data, type, row, meta) {
                    var Createdon = row.Createdon;
                    return Createdon;

                }
            }
        ],

        "initComplete": function () {
            setTimeout(function () {
                tblShowFilelogs.columns.adjust();
            }, 500);
        },

        "fnRowCallback": function (nRow, aData, iDisplayIndex) {

        }

    });
});


$("#m_aside_left_minimize_toggle").click(function () {
    if (tblShowFilelogs != null)
        tblShowFilelogs.columns.adjust();
});