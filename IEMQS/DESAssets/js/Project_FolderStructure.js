﻿var passParentProjectFolderId;

function funCreateNewFolderModal(EditModeFlag,$this) {
    var Level = $($this).attr("data-level");
    if (Level == "Level0") {
        passParentProjectFolderId = "";
    }

    var valEditModeOnFlag = false;

    if (EditModeFlag == 2) {
        valEditModeOnFlag = true;
    }


    if (passParentProjectFolderId == undefined && passParentProjectFolderId == NaN) {
        passParentProjectFolderId = "";
    }

    $.ajax({
        type: "GET",
        url: "/DES/Project/_partialCreateNewFolderProject",
        dataType: "html",
        data: { "ParentProjectFolderId": parseInt(passParentProjectFolderId), "EditModeOnFlag": valEditModeOnFlag, "ProjectNo": $('#ProjectNo').val() },
        success: function (data) {
            $('#myModalContent').html(data);
            if (EditModeFlag == 1) {
                $('#ParentProjectFolderId').val(parseInt(passParentProjectFolderId));
                $("#ProjectFolderId").val("");
                $("#FolderName").val("");
                $("#Description").val("");
                $("#exampleModalLabel").text("Create New Folder");
            }
            else if (EditModeFlag == 2) {
                $("#exampleModalLabel").text("Edit Folder");
            }

            //$('#BUId').val($('#BUList').val());
            $('#m_select2_modal').modal('show');
            setTimeout(function () {
                $("#FolderName").focus();
            }, 1000);
        },
        error: function () {
            toastr.error("Error occured!!", "Error")
        }
    });
}



////function getValueUsingClass() {
////    debugger
////    var chkArray = [];
////    $("#getCategoryList:checked").each(function () {
////        chkArray.push($(this).val());
////    });
////    var selectedCategoryIds;
////    selectedCategoryIds = chkArray.join(',');
////    $("#CategoryIds").val(selectedCategoryIds);

////    var chkArray = [];
////    $("#getActionList:checked").each(function () {
////        chkArray.push($(this).val());
////    });
////    var selectedActionIds;
////    selectedActionIds = chkArray.join(',');
////    $("#ActionIds").val(selectedActionIds);
////}


function funDelete() {
    swal({
        title: "Are you sure you want to delete this record?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
    }).then(
                                function (isConfirm) {
                                    if (isConfirm.value) {

                                        $.ajax({
                                            cache: false,
                                            type: "GET",
                                            url: "/DES/Project/DeleteFolder",
                                            data: { Id: parseInt(passParentProjectFolderId) },
                                            contentType: "application/json; charset=utf-8",
                                            dataType: "json",
                                            success: function (data) {
                                                mApp.unblockPage();
                                                if (data.Status) {
                                                    toastr.success(data.Msg, "Success");
                                                    setTimeout(function () {// wait for 5 secs(2)
                                                        location.reload(); // then reload the page.(3)
                                                    }, 1000);
                                                } else {
                                                    toastr.error(data.Msg, "Error");
                                                }
                                            },
                                            error: function (xhr, ajaxOptions, thrownError) {
                                                toastr.error(data.Msg, "Error")
                                            }
                                        });
                                    } 
                                });
}

function funSendAccessRequestMail(id) {
    swal({
        title: "Are you sure you want access?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
    }).then(
        function (isConfirm) {
            mApp.blockPage();
                                    if (isConfirm.value) {
                                        $.ajax({
                                            cache: false,
                                            type: "GET",
                                            url: "/DES/Project/SendRequestMail",
                                            data: { Id: parseInt(passParentProjectFolderId) },
                                            contentType: "application/json; charset=utf-8",
                                            dataType: "json",
                                            success: function (data) {
                                                mApp.unblockPage();
                                                if (data.InfoMsg) {
                                                    toastr.info(data.Msg, "Info")
                                                }
                                                else {
                                                    toastr.success(data.Msg, "Success")
                                                }
                                                
                                            },
                                            error: function (xhr, ajaxOptions, thrownError) {
                                                mApp.unblockPage();
                                                toastr.error(data.Msg, "Error")
                                            }
                                        });
                                    } else {
                                        mApp.unblockPage();
                                    }
                                });
}
