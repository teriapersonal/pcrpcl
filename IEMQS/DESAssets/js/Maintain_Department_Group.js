﻿var tblDINGroup;
var departmentId = null;
var isfromPersonSelectedcalled = false;

$(document).ready(function () {
    $("#selecttest").select2();

    tblDINGroup = $('#tblDINGroup').DataTable({
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "sAjaxSource": "/DepartmentGroup/GetDINGroupList",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        "responsive": true,
        "language": {
            "infoFiltered": ""
        },
        //"scroller": {
        //    loadingIndicator: true
        //},
        //"bSort": true,
        //"aaSorting": [[1, 'desc']],
        "order": [[1, 'desc']],
        "columnDefs": [
            { "orderable": false, "targets": 0 },
            { "orderable": false, "targets": 2 },
            { "orderable": false, "targets": 3 },
            { "orderable": false, "targets": 4 },
            { "orderable": false, "targets": 5 }
        ],

        "fnServerData": function (sSource, data, fnCallback) {

            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });

        },

        "aoColumns": [
            {
                "sTitle": "Action", "className": "action", "orderable": false, render: function (data, type, row, meta) {
                    var srt = "";
                    srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill btnEdit" title="Edit"><i class="la la-edit text-info"></i></a>';
                    //srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnDelete" title="Delete"><i class="la la-trash text-danger"></i></a>';
                    return srt;
                    // DES/JEPTemplate/EditGlobal?Id=' + row.JEPTemplateId + '
                }
            },
            {
                "sTitle": "Group Name ", "className": "", render: function (data, type, row, meta) {
                    var name = "<strong style='text-transform: capitalize;'>" + row.GroupName + "</strong>";
                    return name;
                }
            },
            {
                "sTitle": "Created By", "className": "", render: function (data, type, row, meta) {
                    return row.CreatedBy;
                }
            },
            {
                "sTitle": "Created On", "className": "", render: function (data, type, row, meta) {
                    return row.CreatedOn;
                }
            },
            {
                "sTitle": "Edited By", "className": "", render: function (data, type, row, meta) {
                    return row.EditedBy;
                }
            },
            {
                "sTitle": "Edited On", "className": "", render: function (data, type, row, meta) {
                    return row.EditedOn;
                }
            }
        ],

        "initComplete": function () {

        },

        "fnRowCallback": function () {
            tblDINGroup.columns.adjust();
        }

    });

});
//$('#m_modal_DIN').modal({ backdrop: 'static', keyboard: false }); 


$('#tblDINGroup').on('click', 'td .btnEdit', function () {
    var tr = $(this).closest('tr');
    var row = tblDINGroup.row(tr);
    var data = row.data();
    AddGroup(data.DINGroupId);
});

$('#tblDINGroup').on('click', 'td .btnDelete', function () {
    var tr = $(this).closest('tr');
    var row = tblDINGroup.row(tr);
    var data = row.data();
    swal({
        title: "Do you want to delete..?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            if (isConfirm.value) {
                mApp.blockPage();
                $.ajax({
                    type: "GET",
                    url: '/DES/DepartmentGroup/DeleteDINGroup',
                    data: { Id: data.DINGroupId },
                    dataType: 'json',
                    contentType: "application/json;charset=utf-8",
                    async: true,
                    success: function (response) {
                        mApp.unblockPage();
                        if (response.Status) {
                            toastr.success(response.Msg, "Success");
                            tblDINGroup.ajax.reload();
                        } else {
                            toastr.error(response.Msg, "Error");
                        }
                    },
                    error: function (error) {
                        mApp.unblockPage();
                        toastr.error("something went wrong try again later", "Error");
                    }
                });
            }
        });
});

function AddGroup(id) {  
    mApp.blockPage();
    $.ajax({
        url: '/DES/DepartmentGroup/AddGroup',
        type: 'GET',
        traditional: true,
        data: { Id: id },
        contentType: 'application/json; charset=utf-8',
        dataType: "Html",
        success: function (result) {
            mApp.unblockPage();
            $("#modelbodyDIN").html(result);
            $("#m_modal_DIN").modal("show");
            setTimeout(function () {
                $("#GroupName").focus();
            }, 1000);
        },
        error: function (xhr) {
            mApp.unblockPage();
        }
    });
}

function AddDeptPerson(Id) {
    var rowCount = $('#tbl_DeptPerson tr').length;
    //var DeptId = $('#tr_@NoTask').val();
    //var DeptId = "";
    //var PSNoList = "";
    mApp.blockPage();
    $("#del_" + Id).show();
    $("#Add_" + Id).hide();
    //if (DeptId != null && PSNoList != null) {
    $.ajax({
        url: '/DES/DepartmentGroup/AddDeptPerson',
        type: 'GET',
        traditional: true,
        data: { TNo: rowCount },
        contentType: 'application/json; charset=utf-8',
        dataType: "Html",
        success: function (result) {
            mApp.unblockPage();
            $("#tbl_DeptPerson").append(result);
            CalculateDeptPerson();
            $("#del_0").show();
            $("#Add_" + Id).hide();
            PopulatePersondropdown(Id);
            //$("#Add_" + Id).hide();
            //$("#del_" + (parseInt(Id) + 1)).hide();
        },
        error: function (xhr) {
            mApp.unblockPage();
        }
    });
    //}
    // make condition to null data 

}

function PopulatePersondropdown(Id) {
    var id = Id;
    isfromPersonSelectedcalled = false;
    departmentId = null;
    $(".select-Psno").select2({
        placeholder: "-----Select-----",
        //tags: true,
        multiple: true,
        tokenSeparators: [','],
        minimumInputLength: 2,
        minimumResultsForSearch: 10,
        ajax: {
            url: '/DES/DepartmentGroup/GetPsnoNameByNameOrDept',
            dataType: 'json',
            data: function (params) {
                return {
                    q: params.term,
                    DeptId: departmentId,
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return item;
                        return {
                            text: data.text,
                            id: data.id
                        }
                    })
                };
            },
        },

    });
}

$("#modelbodyDIN").on("change", "select[id$='__DeptId']", function () {
    var valDeptId = $(this).val();
    var id = $(this).closest("tr").attr("data-id");
    if (valDeptId != '' && valDeptId != null && isfromPersonSelectedcalled != true) {
        $('#t_depc').val(valDeptId);
        mApp.blockPage();
        $.ajax({
            type: "GET",
            url: "/DES/DepartmentGroup/FunctionByPSNumberList",
            data: { "DeptId": valDeptId },
            datatype: "json",
            async: true,
            success: function (data) {
                mApp.unblockPage();
                $("#DepartmentGroupList_" + id + "__PSNoList").html("");
                var datas = data.list;
                $.each(datas, function (key, entry) {
                    $("#DepartmentGroupList_" + id + "__PSNoList").append($('<option></option>').attr('value', entry.t_psno).text(entry.t_name));
                });
                $("#DepartmentGroupList_" + id + "__PSNoList").select2();
                $("#DepartmentGroupList_" + id + "__PSNoList").select2({
                    placeholder: "-----Select-----"
                });
            },
            error: function () {
                mApp.unblockPage();
                toastr.error("something went wrong try again later", "error");
            }
        });
    }
});

$("#modelbodyDIN").on("change", "select[id$='__PSNoList']", function () {
    //alert("hello");
    var valDeptId = $(this).val();
    var id = $(this).closest("tr").attr("data-id");
    if (valDeptId.length > 0) {
        $.ajax({
            type: "GET",
            url: "/DES/DepartmentGroup/GetDeptIdByPsNo?psno=" + valDeptId,
            //data: JSON.stringify({
            //    "psno": valDeptId
            //}),
            datatype: "json",
            async: true,
            success: function (data) {
                //$('select[id^="#DepartmentGroupList_' + id + '__DeptId"] option:selected').attr("selected", null);
                // $("#DepartmentGroupList_" + id + "__DeptId option[value = " + data + "]").select2("val", "3101");
                //$('#DepartmentGroupList_' + id + '__DeptId').select2('data', { id: '3101', text: '3101 - MANUFACTURING INCHARGE OFFICE' });
                //$("#DepartmentGroupList_" + id + "__DeptId").val(data);
                //debugger;
                // $(".select2-selection__placeholder").val('3101 - MANUFACTURING INCHARGE OFFICE');
                //$("#DepartmentGroupList_" + id + "__DeptId").select2("val", data);
                //$("#DepartmentGroupList_0__DeptId").select2("val", "3101");
                //$("#DepartmentGroupList_" + id + "__DeptId option[value= " + data + "]").attr('selected', 'selected');
                isfromPersonSelectedcalled = true;
                departmentId = data;
                $("#DepartmentGroupList_" + id + "__DeptId").val(data).trigger('change');
                //$("#selecttest").select2("val", "2");
                isfromPersonSelectedcalled = false;
                //debugger;
            },
            error: function () {
                //mApp.unblockPage();
                toastr.error("something went wrong try again later", "error");
            }
        });
    }
    else {
        $("#DepartmentGroupList_" + id + "__DeptId").val(departmentId).trigger('change');
    }
});

//$("#tbl_DeptPerson").on("click", "select[id$='__DeptId'], select[id$='__PSNoList']", function () {
//    var index = ($('#tbl_DeptPerson tr').length - 1);//$(this).closest("tr").attr("data-id");
//    if (($("#DepartmentGroupList_" + index + "__DeptId").val() != "" && $("#DepartmentGroupList_" + index + "__PSNoList").val() != "") || $("#DepartmentGroupList_" + index + "__IsDelete").val() == "true") {
//        AddDeptPerson(Id);
//    }
//});

function CalculateDeptPerson() {
    var rowCount = $('#tbl_DeptPerson tr').length;
    var step = 1;
    for (var i = 0; i < rowCount; i++) {
        //if ($("#DepartmentGroupList_" + i + " __IsAdd).val() !="true"){

        if ($("#DepartmentGroupList_" + i + "__IsDelete").val() != "true") {
            //$("#DepartmentGroupList_" + i + "__OrderNo").val(step);
            step = step + 1;
        }
    }
}

function RemoveDeptPerson($this) {
    swal({
        title: "Do you want to delete..?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            if (isConfirm.value) {
                var tid = $($this).attr("delete-id");
                $("#DepartmentGroupList_" + tid + "__IsDelete").val("true");
                $("#tr_" + tid).hide();
                var rowCount = $('#tbl_DeptPerson tr:visible').length;
                if (!(rowCount > 0)) {
                    AddDeptPerson(Id);
                }
                CalculateDeptPerson();
            }
        });
}

DINBegin = function (data) {
    mApp.blockPage();
    var flag = true;
    var step = [];
    var rowCount = $('#tbl_DeptPerson tr').length;
    for (var i = 0; i < rowCount; i++) {
        $("span[data-valmsg-for='DepartmentGroupList[" + i + "].DeptId']").html("");
        $("span[data-valmsg-for='DepartmentGroupList[" + i + "].PSNoList']").html("");
        if ($("#DepartmentGroupList_" + i + "__IsDelete").val() != "true") {

            if (i != 0 && i == (rowCount - 1)) {
                if ($("#DepartmentGroupList_" + i + "__DeptId").val() != "") {
                    //if (step.indexOf($("#DepartmentGroupList_" + i + "__DeptId").val()) >= 0) {
                    //    flag = false;
                    //    $("span[data-valmsg-for='DepartmentGroupList[" + i + "].DeptId']").html("Department already exist");
                    //}


                    step.push($("#DepartmentGroupList_" + i + "__DeptId").val());

                    if ($("#DepartmentGroupList_" + i + "__PSNoList").val() == "") {
                        mApp.unblockPage();
                        $("span[data-valmsg-for='DepartmentGroupList[" + i + "].PSNoList']").html("Person Name is required");
                        flag = false;
                    }
                }

            } else {
                //if (step.indexOf($("#DepartmentGroupList_" + i + "__DeptId").val()) >= 0) {
                //    flag = false;
                //    $("span[data-valmsg-for='DepartmentGroupList[" + i + "].DeptId']").html("Department already exist");
                //}


                step.push($("#DepartmentGroupList_" + i + "__DeptId").val());

                if ($("#DepartmentGroupList_" + i + "__PSNoList").val() == "") {
                    mApp.unblockPage();
                    $("span[data-valmsg-for='DepartmentGroupList[" + i + "].PSNoList']").html("Person Name is required");
                    flag = false;
                }
                //if ($("#DepartmentGroupList_" + i + "__DeptId").val() == "") {
                //    $("span[data-valmsg-for='DepartmentGroupList[" + i + "].DeptId']").html("Department is required");
                //    flag = false;
                //}
            }
        }

    }
    //if (flag) {
    //    $("#frmAddGlobal").submit();
    //}
    return flag;
};

DINSuccess = function (data) {
    if (data.Status) {
        mApp.unblockPage();
        toastr.success(data.Msg, "Success");
        tblDINGroup.ajax.reload();
        $("#m_modal_DIN").modal("hide");
    } else {
        mApp.unblockPage();
        if (data.Info == "true")
        {
            toastr.info(data.Msg, "Info");
        }
        else
        {
            toastr.error(data.Msg, "Error");
        }
    }
};

//$(document).on("change", "#ddlProjectList", function () {
//    //$("#ddlProjectList").change(function () {
//    var valProject = $('#ddlProjectList').val();

//    if (valProject != null) {
//        mApp.blockPage();
//        $.ajax({
//            type: "POST",
//            url: "/DES/DepartmentGroup/ProjectByGroupList",
//            data: { "Project": valProject },
//            datatype: "json",
//            async: true,
//            success: function (data) {
//                mApp.unblockPage();
//                $("#ddlGroupList").html("");
//                var datas = data.list;
//                $.each(datas, function (key, entry) {
//                    $("#ddlGroupList").append($('<option></option>').attr('value', entry.DINGroupId).text(entry.GroupName));
//                });
//                $("#ddlGroupList").trigger("change");
//            },
//            error: function () {
//                mApp.unblockPage();
//                toastr.error("something went wrong try again later", "error");
//                $("#ddlGroupList").html("");
//            }
//        });
//    }
//});

function fnBtnCopy() {
    if ($('#ddlGroupList').val() != "") {
        mApp.blockPage();
        $.ajax({
            type: "GET",
            url: "/DES/DepartmentGroup/CopyProjectGroupList",
            data: { "Id": $('#ddlGroupList').val() },
            contentType: 'application/json; charset=utf-8',
            dataType: "Html",
            success: function (data) {
                mApp.unblockPage();
                $("#tbl_DeptPerson").html(data);
                //CalculateDocumentList();
            },
            error: function () {
                mApp.unblockPage();
                toastr.error("something went wrong try again later", "error");
            }
        });
    }
    else {
        mApp.unblockPage();
        toastr.info("Please select group", "Info");
    }
}

$("#m_aside_left_minimize_toggle").click(function () {
    if (tblDINGroup != null)
        tblDINGroup.columns.adjust();
});


//$('#ddlFunctionList').change(function () {
//    debugger;
//    //$('#displayproductid').show();
//    var valDeptId = $('#ddlFunctionList').val();

//    if (valDeptId != '') {
//        $('#t_depc').val(valDeptId);
//        mApp.blockPage();
//        $.ajax({
//            type: "GET",
//            url: "/DES/DepartmentGroup/FunctionByPSNumberList",
//            data: { "DeptId": valDeptId },
//            datatype: "json",
//            async: true,
//            success: function (data) {
//                mApp.unblockPage();
//                $("#ddlPSNoList").html("");
//                //$("#ddlJEPTemplateId").html("");
//                $.each(data, function (key, entry) {
//                    $("#ddlPSNoList").append($('<option></option>').attr('value', entry.t_psno).text(entry.t_name));
//                });
//            },
//            error: function () {
//                mApp.unblockPage();
//                toastr.error("something went wrong try again later", "error");
//            }
//        });
//    }
//});
