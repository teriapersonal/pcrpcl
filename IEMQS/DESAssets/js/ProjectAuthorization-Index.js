﻿var tblProjectAuthorization;
$(document).ready(function () {
    setTimeout(function () {
        tblProjectAuthorization = $('#tblProjectAuthorization').DataTable({
            "searching": false,
            "paging": true,
            "info": false,
            "lengthChange": false,
            "serverSide": true,
            "processing": true,
            "orderMulti": false,
            "filter": true,
            "sAjaxSource": "/DES/ProjectAuthorization/GetProjectAuthorizationList",
            "iDisplayLength": 10,
            "ordering": true,
            "scrollX": true,
            "responsive": false,
            "language": {
                "infoFiltered": ""
            },
            "order": [[0, 'desc']],
            "columnDefs": [
                { "orderable": false, "targets": 0 },
                { "orderable": false, "targets": 1 },
                { "orderable": false, "targets": 2 },
                { "orderable": false, "targets": 3 },
                { "orderable": false, "targets": 4 }
            ],
            "fnServerData": function (sSource, data, fnCallback) {

                $.ajax({
                    "dataType": "json",
                    "type": "POST",
                    "url": sSource,
                    "data": data,
                    "success": fnCallback
                });

            },
            "aoColumns": [
                {
                    "sTitle": "Action", "className": "action ", render: function (data, type, row, meta) {
                        var srt = "";

                        srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Approve" onclick=Approve(' + row.ProjectUserFolderAuthorizeId + ')><i class="la la-check-circle text-success"></i></a>';
                        srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Reject" onclick=Reject(' + row.ProjectUserFolderAuthorizeId + ')><i class="la la-close text-danger"></i></a>';
                        srt += "";
                        return srt;
                    }
                },
                {
                    "sTitle": "Project", "className": "", "sWidth": "", render: function (data, type, row, meta) {
                        return row.Project;
                    }
                },
                //{
                //    "sTitle": "PS Number", "className": "", "sWidth": "", render: function (data, type, row, meta) {
                //        return row.PSNumber;
                //    },
                //},
                {
                    "sTitle": "User Name", "className": "", "sWidth": "", render: function (data, type, row, meta) {
                        return row.UserName;
                    }
                },
                {
                    "sTitle": "Department", "className": "", "sWidth": "", render: function (data, type, row, meta) {
                        return row.FunctionId;
                    }
                },
                {
                    "sTitle": "Location", "className": "", "sWidth": "", render: function (data, type, row, meta) {
                        return row.Location;
                    }
                }
            ],

            "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                tblProjectAuthorization.columns.adjust();
            }

        });
    }, 1000);
    mApp.unblockPage();
});

function Approve(Id) {
    mApp.blockPage();
    var data = {
        Id: Id
    };
    $.ajax({
        url: "/DES/ProjectAuthorization/Approve", //If get File Then Document Mapping Add
        data: JSON.stringify(data),
        type: "POST",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        aysnc: true,
        success: function (response) {
            mApp.unblockPage();
            if (response.Status) {
                tblProjectAuthorization.ajax.reload();
                toastr.success(response.Msg, "Success");
            } else {
                toastr.error(response.Msg, "Not Success");
            }
        },
        error: function (response) {
            mApp.unblockPage();
            toastr.error(response.Msg, "Error");
        }
    });
}

function Reject(Id) {
    mApp.blockPage();
    var data = {
        Id: Id
    };
    $.ajax({
        url: "/DES/ProjectAuthorization/Reject", //If get File Then Document Mapping Add
        data: JSON.stringify(data),
        type: "POST",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        aysnc: true,
        success: function (response) {
            mApp.unblockPage();
            if (response.Status) {
                tblProjectAuthorization.ajax.reload();
                toastr.success(response.Msg, "Success");
            } else {
                toastr.error(response.Msg, "Not Success");
            }
        },
        error: function (response) {
            mApp.unblockPage();
            toastr.error(response.Msg, "Error");
        }
    });
}

$("#m_aside_left_minimize_toggle").click(function () {
    if (tblProjectAuthorization != null)
        tblProjectAuthorization.columns.adjust();
});