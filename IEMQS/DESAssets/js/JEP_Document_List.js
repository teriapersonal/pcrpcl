﻿$("#tbl_JEPDoc").on("keyup blur", "input[id$='__DocumentNo'], input[id$='__DocumentTitle']", function () {
    var index = ($('#tbl_JEPDoc tr').length - 1);//$(this).closest("tr").attr("data-id");
    if (($("#JEPTemplateDocList_" + index + "__DocumentNo").val() != "" && $("#JEPTemplateDocList_" + index + "__DocumentTitle").val() != "") || $("#JEPTemplateDocList_" + index + "__IsDelete").val() == "true") {
        AddDocumentList();
    }
});

$(document).ready(function () {
    CalculateDocumentList();
    if ($("#JEPTemplateId").val() != 0) {
        AddDocumentList();
    }
});

var AddDocumentListflag = true;

function AddDocumentList() {
    var rowCount = $('#tbl_JEPDoc tr').length;
    if (AddDocumentListflag) {
        AddDocumentListflag = false;
        mApp.blockPage();
        $.ajax({
            url: '/DES/JEPTemplate/AddDocumentList',
            type: 'GET',
            traditional: true,
            data: { TNo: rowCount },
            contentType: 'application/json; charset=utf-8',
            dataType: "Html",
            success: function (result) {
                mApp.unblockPage();
                AddDocumentListflag = true;
                $("#tbl_JEPDoc").append(result);
                CalculateDocumentList();
            },
            error: function (xhr) {
                mApp.unblockPage();
                AddDocumentListflag = true;
            }
        });
    }
}

function RemoveDocumentList($this) {
    swal({
        title: "Do you want to delete..?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            if (isConfirm.value) {
                var tid = $($this).attr("delete-id");
                $("#JEPTemplateDocList_" + tid + "__IsDelete").val("true");
                $("#tr_" + tid).hide();
                var rowCount = $('#tbl_JEPDoc tr:visible').length;
                if (!(rowCount > 0)) {
                    AddDocumentList();
                }
                CalculateDocumentList();
            }
        });
}

function CalculateDocumentList() {
    var rowCount = $('#tbl_JEPDoc tr').length;
    var step = 1;
    for (var i = 0; i < rowCount; i++) {
        if ($("#JEPTemplateDocList_" + i + "__IsDelete").val() != "true") {
            $("#JEPTemplateDocList_" + i + "__OrderNo").val(step);
            step = step + 1;
        }
    }
}

DocumentListBegin = function (data) {
    var flag = true;
    var step = [];
    var step1 = [];
    var rowCount = $('#tbl_JEPDoc tr').length;
    for (var i = 0; i < (rowCount - 1); i++) {
        $("span[data-valmsg-for='JEPTemplateDocList[" + i + "].DocumentNo']").html("");
        $("span[data-valmsg-for='JEPTemplateDocList[" + i + "].DocumentTitle']").html("");
        if ($("#JEPTemplateDocList_" + i + "__IsDelete").val() != "true") {
            if (step.indexOf($("#JEPTemplateDocList_" + i + "__DocumentNo").val()) >= 0) {
                flag = false;
                $("span[data-valmsg-for='JEPTemplateDocList[" + i + "].DocumentNo']").html("DocumentNo already exist");
            }
            if (step1.indexOf($("#JEPTemplateDocList_" + i + "__DocumentTitle").val()) >= 0) {
                flag = false;
                $("span[data-valmsg-for='JEPTemplateDocList[" + i + "].DocumentTitle']").html("DocumentTitle already exist");
            }

            step.push($("#JEPTemplateDocList_" + i + "__DocumentNo").val());
            step1.push($("#JEPTemplateDocList_" + i + "__DocumentTitle").val());

            if ($("#JEPTemplateDocList_" + i + "__DocumentNo").val() == "" && $("#JEPTemplateDocList_" + i + "__DocumentTitle").val() == "") {
                $("span[data-valmsg-for='JEPTemplateDocList[" + i + "].DocumentNo']").html("DocumentNo is required");
                $("span[data-valmsg-for='JEPTemplateDocList[" + i + "].DocumentTitle']").html("DocumentTitle is required");
                flag = false;
            }
        }
    }
    if (flag) {
        $("#frmAddGlobal").submit();
    }
    return flag;
};

