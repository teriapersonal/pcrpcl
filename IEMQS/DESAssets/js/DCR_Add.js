﻿var Project;
var selectedDocIDs = [];
var selectedPartIDs = [];
var tblJepDoc;
var FinalDoclist = [];
var FinalPartlist = [];
var tblDCRdoc;
var tblGlobalJepDoc;
var tblDocumentList;
var DCRNo;
var tblDocumentFiles;
var tblDCRPart;
var tblAffectedPart;
var Dropzonefile;
var tblDCRHistory;

$(document).ready(function () {
    WizardDemo.init();
    BindJEPDoc();
    BindGlobalJEPDoc();
    GetAffectedDCRDoc();
    BindPART();
    GetAffectedDCRPart();
    getGrid();
    $("#ddlCategoryChange").select2();
    $("#ddlResponsibleDesignEngineer").select2();
    $("#ddlResponsibleSeniorDesignEngineer").select2();
    $("#ContractProjectList").select2();
    //$("#ProjectWiseDOC").select2();
    
});

function funsearchGlobalJEPDoc() {
    tblGlobalJepDoc.ajax.reload();
    setTimeout(function () {
        tblGlobalJepDoc.columns.adjust();
    }, 500);
}

function funOpenHistory(obj) {
    mApp.blockPage();
    $("#m_modal_5").modal("show");
    AllHistory();
    mApp.unblockPage();
}

function AllHistory() {
    if (tblDCRHistory == null) {
        tblDCRHistory = $('#tblDCRHistory').DataTable({
            "serverSide": true,
            "processing": true,
            "orderMulti": false,
            "filter": true,
            "sAjaxSource": "/DES/DCR/GetAllHistory",
            "iDisplayLength": 10,
            "ordering": true,
            "scrollX": true,
            "responsive": false,
            "language": {
                "infoFiltered": ""
            },
            "order": [[2, 'desc']],
            "columnDefs": [
                //{ "orderable": false, "targets": 1 },
                //{ "orderable": false, "targets": 2 }
            ],

            "fnServerData": function (sSource, data, fnCallback) {
                data.push({ name: "DCRId", value: $("#DCRId").val() });
                $.ajax({
                    "dataType": "json",
                    "type": "POST",
                    "url": sSource,
                    "data": data,
                    "success": fnCallback
                });

            },
            "aoColumns": [

                {
                    "sTitle": "History", render: function (data, type, row, meta) {
                        return row.Description
                    }
                },

                {
                    "sTitle": "Created By", "orderable": false, render: function (data, type, row, meta) {
                        return row.CreatedBy
                    }
                },
                {
                    "sTitle": "Created On", render: function (data, type, row, meta) {
                        return row.CreatedOn
                    }
                },
            ],

            "initComplete": function () {

            },

            "fnRowCallback": function () {
                setTimeout(function () {
                    tblDCRHistory.columns.adjust();
                }, 1000);
            }
        });
    }
}

var WizardDemo = function () {
    $("#m_wizard");
    var e, r, i = $("#m_form"), f = $("#FormAddDCR");
    return {
        init: function () {
            var n;
            $("#m_wizard"), i = $("#m_form"), (r = new mWizard("m_wizard", {
                startStep: 1
            })).on("beforeNext", function (r) {

            }), r.on("change", function (e) {
                mUtil.scrollTop()
            }), r.on("change", function (e) {
                1 === e.getStep();
            })
        }
        , Gotonext: function () {
            r.goNext();
            tblDocumentFiles.columns.adjust();
            tblJepDoc.columns.adjust();
            tblGlobalJepDoc.columns.adjust();
            tblDocumentList.columns.adjust();
            tblDCRPart.columns.adjust();
            tblAffectedPart.columns.adjust();

            //setTimeout(function () {
            //    tblDocumentFiles.ajax.reload();
            //    tblJepDoc.ajax.reload();
            //    tblGlobalJepDoc.ajax.reload();
            //    tblDocumentList.ajax.reload();
            //    tblDCRPart.ajax.reload();
            //    tblAffectedPart.ajax.reload();
            //}, 500);

        }
        , GotoBack: function () {
            r.goPrev();
            //e.goNext();
            tblDocumentFiles.columns.adjust();
            tblJepDoc.columns.adjust();
            tblGlobalJepDoc.columns.adjust();
            tblDocumentList.columns.adjust();
            tblDCRPart.columns.adjust();
            tblAffectedPart.columns.adjust();

            //setTimeout(function () {
            //    tblDocumentFiles.ajax.reload();
            //    tblJepDoc.ajax.reload();
            //    tblGlobalJepDoc.ajax.reload();
            //    tblDocumentList.ajax.reload();
            //    tblDCRPart.ajax.reload();
            //    tblAffectedPart.ajax.reload();
            //}, 500);
        }
        , GoTo: function (i) {
            if ($("#DCRId").val() != "") {
                r.goTo(i);
                tblDocumentFiles.columns.adjust();
                tblJepDoc.columns.adjust();
                tblGlobalJepDoc.columns.adjust();
                tblDocumentList.columns.adjust();
                tblDCRPart.columns.adjust();
                tblAffectedPart.columns.adjust();
            }
        }
    }
}();

CKEDITOR.replace('ReasonForChange', {
    skin: 'moono',
    enterMode: CKEDITOR.ENTER_BR,
    shiftEnterMode: CKEDITOR.ENTER_P,
    height: '100px',
    toolbar: [{
        name: 'basicstyles',
        groups: ['basicstyles'],
        items: ['Bold', 'Italic', 'Underline', "-", 'TextColor', 'BGColor']
    },

    {
        name: 'scripts',
        items: ['Subscript', 'Superscript']
    },
    {
        name: 'editing',
        items: ['Scayt']
    }
    ],
    removePlugins: 'elementspath',
    resize_enabled: true,
});

CKEDITOR.config.scayt_autoStartup = true;

CKEDITOR.replace('ChangeFrom', {
    skin: 'moono',
    enterMode: CKEDITOR.ENTER_BR,
    height: '100px',

    shiftEnterMode: CKEDITOR.ENTER_P,

    toolbar: [{
        name: 'basicstyles',
        groups: ['basicstyles'],
        items: ['Bold', 'Italic', 'Underline', "-", 'TextColor', 'BGColor']
    },

    {
        name: 'scripts',
        items: ['Subscript', 'Superscript']
    },
    {
        name: 'editing',
        items: ['Scayt']
    }
    ],
    removePlugins: 'elementspath',
    resize_enabled: true
});

CKEDITOR.replace('ChangeTo', {
    skin: 'moono',
    enterMode: CKEDITOR.ENTER_BR,
    height: '100px',
    shiftEnterMode: CKEDITOR.ENTER_P,
    toolbar: [{
        name: 'basicstyles',
        groups: ['basicstyles'],
        items: ['Bold', 'Italic', 'Underline', "-", 'TextColor', 'BGColor']
    },

    {
        name: 'scripts',
        items: ['Subscript', 'Superscript']
    },
    {
        name: 'editing',
        items: ['Scayt']
    }
    ],
    removePlugins: 'elementspath',
    resize_enabled: true,
});

funSuccess = function (data) {
    if (data.Status) {
        //toastr.success(data.Msg, "Success");
        $('#DCRId').val(data.model.DCRId);
        //DCRId = $('#DCRId').val();
        $('#txtProjectNo').val(Project);
        $('#DCRNo').val(data.model.DCRNo);
        $('#DCRSupportedDocId').val(data.model.DCRSupportedDocId);
        $('#ddGenrateDCRNo').prop('disabled', true);
        WizardDemo.Gotonext();
    } else {
        toastr.error(data.Msg, "Error");
    }
    mApp.unblockPage();
}
var iii = 1;
var myDropzone = new Dropzone('div#m-dropzone-two', {
    timeout: 18000000,
    parallelUploads: 10000,
    maxFilesize: 200000,
    maxFiles: 10,
    init: function () {
        this.on("maxfilesexceeded", function (file) {

            if (this.files.length == 11) {
                // this.removeFile(this.files[0]);
                if (iii == 1) {
                    alert("10 Files can be uploded at once!");
                }
                iii++;
            }
            myDropzone.removeFile(file);
            myDropzone.removeAllFiles();
        });

    },
    queuecomplete: function () {
        iii = 1;
    }

});

myDropzone.on('sending', function (file, xhr, formData, progress, bytesSent) {
    $('#dropzone').text("");
    $('.dz-error-message').css('display', 'none');
    $('.dz-success-mark').css('display', 'block');
    $('.dz-error-mark').css('display', 'none');

    var regex = /\@|\#|\+|\$|\*|\[|\{|\]|\}|\||\\|\<|\,|\>|\?|\/|\"/;
    var fileName = regex.test(file.name);


    if (fileName) {
        toastr.error(file.name + " Contains special characters", "Error");
        myDropzone.removeFile(file);
    }
    else {
        var dotNameSplit = file.name.split('.');
        if (dotNameSplit.length > 2) {
            toastr.error(file.name + " Contains special characters", "Error");
            myDropzone.removeFile(file);
        }
        else {
            if ($('#CurrentLocationIp').val() != "") {
                // if ($('#CurrentLocationIp').val() != "") {
                var CurrentLocationIp = $('#CurrentLocationIp').val();
                var CurrentFCSPPath = $('#FilePath').val();
                var FCSurl = CurrentLocationIp + "/api/Upload";
                var DocumentNo = file.name;
                var DCRNo = $('#DCRNo').val();
                var DocumentPath = DCRNo.toString().trim() + "-" + "DCRSupportDocument";

                formData.append('SavedPath', CurrentFCSPPath);
                formData.append('DocumentPath', DocumentPath.toString());
                formData.append('DocumentFile', file);
                var formate = file.name.substr(file.name.lastIndexOf('.') + 1);
                //Need To get Status and other filed HERE 
                mApp.blockPage();

                window.onbeforeunload = function () {
                    var prevent_leave = true;
                    if (prevent_leave) {
                        return "Your files are not completely uploaded yet...";
                    }
                }

                jQuery.ajax({
                    url: FCSurl,
                    data: formData,
                    enctype: 'multipart/form-data',
                    cache: false,
                    async: true,
                    contentType: false,
                    processData: false,
                    method: 'POST',
                    type: 'POST', // For jQuery < 1.9
                    success: function (response) {
                        mApp.unblockPage();
                        var fname = response.SavedFileName;
                        var path = response.SavedPath + "/" + fname;

                        $.ajax({
                            type: "POST",
                            url: '/DES/DCR/UploadDCRSupportDocs', //If get File Then Document Mapping Add
                            data: JSON.stringify({ DCRId: $("#DCRId").val(), DocumentTitle: DocumentNo, DocumentFormat: formate, DocumentPath: path }),
                            contentType: "application/json;charset=utf-8",
                            dataType: "json",
                            aysnc: true,
                            success: function (response) {
                                if (response.Status) {
                                    toastr.success(response.Msg, "Success");
                                    // getGrid(response.model.DCRId);
                                    //DCRId = response.model.DCRId;
                                    tblDocumentFiles.ajax.reload();
                                    myDropzone.removeFile(file);
                                    mApp.unblockPage();
                                } else {
                                    toastr.error(response.Msg, "Error");
                                    myDropzone.removeFile(file);
                                    mApp.unblockPage();
                                }
                            },
                            error: function (error) {
                                toastr.error("something went wrong try again later", "Error");
                                myDropzone.removeFile(file);
                            }
                        });
                        window.onbeforeunload = function () {
                            var prevent_leave = false;
                            if (prevent_leave) {
                                return "Your files are not completely uploaded...";
                            }
                        }
                    },
                }).fail(function (data) {
                    mApp.unblockPage();
                    toastr.error("Server response not received", "Error");
                    myDropzone.removeFile(file);
                    mApp.unblockPage();
                    window.onbeforeunload = function () {
                        var prevent_leave = false;
                        if (prevent_leave) {
                            return "Your files are not completely uploaded...";
                        }
                    }
                });
            }
            else {
                toastr.error("Ip is not detecting", "Error");
                myDropzone.removeFile(file);
                mApp.unblockPage();
            }
        }

    }
    // }
    //else {
    //    toastr.error("Ip is not detecting", "Error");
    //    myDropzone.removeFile(file);
    //}
});

function BindJEPDoc() {
    tblJepDoc = $('#tblJepDoc').DataTable({
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "sAjaxSource": "/DES/DCR/GetJEPDocList",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        "responsive": true,
        "language": {
            "infoFiltered": ""
        },
        "bRetrieve": true,
        //"scroller": {
        //    loadingIndicator: true
        //},
        //"bSort": true,
        //"aaSorting": [[1, 'desc']],
        "order": [[1, 'desc']],
        'columnDefs': [
            {
                'orderable': false,
                'targets': 0,
                'checkboxes': {
                    'selectRow': true
                }
            }],
        'select': {
            'style': 'multi'
        },
        "fnServerData": function (sSource, data, fnCallback) {
            data.push({ name: "DCRId", value: $("#DCRId").val() });
            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });
        },
        "aoColumns": [
            {
                "sTitle": "<label class='m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--success'><input type='checkbox' id='chbHeaderJEPDoc' onclick='SelectAllJEPDoc()'></input><span></span></label>", "sDefaultContent": "<input type='checkbox' ></input>", "bSortable": false, "className": "action", "orderable": false, render: function (data, type, row, meta) {
                    var srt = '<label class="m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--success">'
                    var IDIndex = selectedDocIDs.indexOf(row.JEPDocumentDetailsId);
                    if (IDIndex !== -1) {
                        srt += '<input type="checkbox" class="m-checkable" id="IsSelect_' + row.JEPDocumentDetailsId + '"  data-id="' + row.JEPDocumentDetailsId + '" onclick="AddToDocList(this)" checked>';
                    }
                    else {
                        srt += '<input type="checkbox" class="m-checkable" id="IsSelect_' + row.JEPDocumentDetailsId + '"  data-id="' + row.JEPDocumentDetailsId + '" onclick="AddToDocList(this)">';
                    }
                    srt += "<span></span></label>";
                    return srt;
                }
            },
            {
                "sTitle": "Doc No", "className": "", render: function (data, type, row, meta) {
                    //var name = "<strong style='text-transform: capitalize;'>" + row.DocumentNo + "</strong>";
                    return row.DocumentNo
                },
            },
            {
                "sTitle": "Rev", "className": "", render: function (data, type, row, meta) {
                    //var name = "<strong style='text-transform: capitalize;'>" + row.DocumentRevision + "</strong>";
                    var srt = '';
                    if (row.DocumentRevision != null || row.DocumentRevision != '') {
                        srt = 'R' + row.DocumentRevision;
                    }
                    return srt;
                },
            },
            {
                "sTitle": "Description", "className": "", render: function (data, type, row, meta) {
                    return row.DocDescription
                }
            },
            {
                "sTitle": "Planned Submit Date", "className": "", render: function (data, type, row, meta) {
                    return row.PlannedSubmitDate
                }
            }
        ],

        "initComplete": function () {
            setTimeout(function () {
                tblJepDoc.columns.adjust();
            }, 500);
        },

        "fnRowCallback": function () {
            setTimeout(function () {
                tblJepDoc.columns.adjust();
            }, 500);
        }
    });
}

function BindGlobalJEPDoc() {
    setTimeout(function () {
        tblGlobalJepDoc = $('#tblGlobalJepDoc').DataTable({
            "searching": false,
            "serverSide": true,
            "processing": true,
            "orderMulti": false,
            "filter": true,
            "sAjaxSource": "/DES/DCR/GetJEPGlobalDocList",
            "iDisplayLength": 10,
            "ordering": true,
            "scrollX": true,
            "responsive": false,
            "language": {
                "infoFiltered": ""
            },
            "bRetrieve": true,
            //"scroller": {
            //    loadingIndicator: true
            //},
            //"bSort": true,
            //"aaSorting": [[1, 'desc']],
            "order": [[1, 'desc']],
            "columnDefs": [
                { "orderable": false, "targets": 0 }
            ],

            "fnServerData": function (sSource, data, fnCallback) {
                data.push({ name: "DocumentNo", value: $("#txtDocumentNo").val() });
                data.push({ name: "Project", value: $("#ContractProjectList").val() });
                data.push({ name: "DCRId", value: $("#DCRId").val() });
                $.ajax({
                    "dataType": "json",
                    "type": "POST",
                    "url": sSource,
                    "data": data,
                    "success": fnCallback
                });
            },
            "aoColumns": [
                {
                    "sTitle": "<label class='m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--success'><input type='checkbox' id='chbHeaderGlobalDoc' onclick='SelectAllGlobalJEPDoc()'></input><span></span></label>", "sDefaultContent": "<input type='checkbox' ></input>", "bSortable": false, "className": "action", "orderable": false, render: function (data, type, row, meta) {
                        var srt = '<label class="m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--success">'
                        var IDIndex = selectedDocIDs.indexOf(row.JEPDocumentDetailsId);
                        if (IDIndex !== -1) {
                            srt += '<input type="checkbox" class="m-checkable" id="IsSelect_' + row.JEPDocumentDetailsId + '"  data-id="' + row.JEPDocumentDetailsId + '" onclick="AddToDocList(this)" checked>';
                        }
                        else {
                            srt += '<input type="checkbox" class="m-checkable" id="IsSelect_' + row.JEPDocumentDetailsId + '"  data-id="' + row.JEPDocumentDetailsId + '" onclick="AddToDocList(this)">';
                        }
                        srt += "<span></span></label>";
                        return srt;
                    }
                },
                {
                    "sTitle": "Doc No", "className": "", render: function (data, type, row, meta) {
                        //var name = "<strong style='text-transform: capitalize;'>" + row.DocumentNo + "</strong>";
                        return row.DocumentNo
                    },
                },
                {
                    "sTitle": "Rev", "className": "", render: function (data, type, row, meta) {
                        //var name = "<strong style='text-transform: capitalize;'>" + row.DocumentRevision + "</strong>";
                        var srt = '';
                        if (row.DocumentRevision != null || row.DocumentRevision != '') {
                            srt = 'R' + row.DocumentRevision;
                        }
                        return srt;
                    },
                },
                {
                    "sTitle": "Description", "className": "", render: function (data, type, row, meta) {
                        return row.DocDescription
                    }
                }
            ],

            "initComplete": function () {
                tblGlobalJepDoc.columns.adjust();
            },

            "fnRowCallback": function () {
                tblGlobalJepDoc.columns.adjust();
            }
        });
    }, 1000);
}

function SelectAllJEPDoc() {
    var data = tblJepDoc.rows().data()[0]["DocIDs"];
    var DocIdArray = data.split(',');
    var select = $("#chbHeaderJEPDoc").prop("checked");
    if (select == true) {
        DocIdArray.forEach(function (item) {
            item = parseInt(item);
            $("#IsSelect_" + item).prop("checked", true);
            selectedDocIDs.push(item);
        });
    }
    else {
        DocIdArray.forEach(function (item) {
            item = parseInt(item);
            $("#IsSelect_" + item).prop("checked", false);
            var index = selectedDocIDs.indexOf(item);
            if (index > -1) {
                selectedDocIDs.splice(index, 1);
            }
        });
    }
    //selectedDocIDs = Array.from(new Set(selectedDocIDs));


    //var data = tblJepDoc.rows().data();
    //var select = $("#chbHeaderJEPDoc").prop("checked");
    //if (select == true) {
    //    data.each(function (value, index) {
    //        $("#IsSelect_" + data[index].JEPDocumentDetailsId).prop("checked", true);
    //        selectedDocIDs.push(data[index].JEPDocumentDetailsId);
    //    });
    //}
    //else {
    //    data.each(function (value, index) {
    //        $("#IsSelect_" + data[index].JEPDocumentDetailsId).prop("checked", false);
    //        var index = selectedDocIDs.indexOf(data[index].JEPDocumentDetailsId);
    //        if (index > -1) {
    //            selectedDocIDs.splice(index, 1);
    //        }
    //    });
    //}
    //selectedDocIDs = Array.from(new Set(selectedDocIDs));
}

function SelectAllGlobalJEPDoc() {
    var data = tblGlobalJepDoc.rows().data()[0]["DocIDs"];
    var DocIdArray = data.split(',');
    var select = $("#chbHeaderGlobalDoc").prop("checked");
    if (select == true) {
        DocIdArray.forEach(function (item) {
            item = parseInt(item);
            $("#IsSelect_" + item).prop("checked", true);
            selectedDocIDs.push(item);
        });
    }
    else {
        DocIdArray.forEach(function (item) {
            item = parseInt(item);
            $("#IsSelect_" + item).prop("checked", false);
            var index = selectedDocIDs.indexOf(item);
            if (index > -1) {
                selectedDocIDs.splice(index, 1);
            }
        });
    }

    //var data = tblGlobalJepDoc.rows().data();
    //var select = $("#chbHeaderGlobalDoc").prop("checked");
    //if (select == true) {
    //    data.each(function (value, index) {
    //        $("#IsSelect_" + data[index].JEPDocumentDetailsId).prop("checked", true);
    //        selectedDocIDs.push(data[index].JEPDocumentDetailsId);
    //    });
    //}
    //else {
    //    data.each(function (value, index) {
    //        $("#IsSelect_" + data[index].JEPDocumentDetailsId).prop("checked", false);
    //        var index = selectedDocIDs.indexOf(data[index].JEPDocumentDetailsId);
    //        if (index > -1) {
    //            selectedDocIDs.splice(index, 1);
    //        }
    //    });
    //}
    //selectedDocIDs = Array.from(new Set(selectedDocIDs));
}

function AddToDocList($this) {
    var data;
    var allSelected;
    var id = $($this).attr("data-id");
    id = parseInt(id, 10);
    var select = $("#IsSelect_" + id).prop("checked");
    var tableID = $("#IsSelect_" + id).closest("table").attr("id");
    var chbID = $("#" + tableID + " th input[type='checkbox']").attr("id");
    if (tableID == "tblJepDoc") {
        data = tblJepDoc.rows().data();
    }
    else {
        data = tblGlobalJepDoc.rows().data();
    }
    if (select == true) {
        selectedDocIDs.push(id);
        selectedDocIDs = Array.from(new Set(selectedDocIDs));
        data.each(function (value, index) {
            if (!selectedDocIDs.includes(data[index].JEPDocumentDetailsId)) {
                allSelected = false;
            }
        });
        if (allSelected != false) {
            $("#" + chbID).prop("checked", true);
        }

    }
    else {
        var tableID = $("#IsSelect_" + id).closest("table").attr("id");
        var chbID = $("#" + tableID + " th input[type='checkbox']").attr("id");
        $("#" + chbID).prop("checked", false);
        var index = selectedDocIDs.indexOf(id);
        if (index > -1) {
            selectedDocIDs.splice(index, 1);
        }
    }
    selectedDocIDs = Array.from(new Set(selectedDocIDs));
}

$("#btnAddDocList").on("click", function () {
    mApp.blockPage();
    if (selectedDocIDs.length > 0) {
        var FinalDoclist = Array.from(new Set(selectedDocIDs));
        var DocIDs = new Array();

        for (var id in FinalDoclist) {
            DocIDs.push({ JEPDocumentDetailsId: FinalDoclist[id] });
        }

        var model = {};
        model.DCRId = $("#DCRId").val();
        model.dCRDocumentList = DocIDs;

        $.ajax({
            type: "POST",
            url: '/DES/DCR/AddDCRdoc',
            data: JSON.stringify({ model: model }),
            dataType: 'json',
            contentType: "application/json;charset=utf-8",
            async: true,
            success: function (response) {
                mApp.unblockPage();
                if (response.Status) {
                    toastr.success(response.Msg, "Success");
                    tblGlobalJepDoc.ajax.reload();
                    tblJepDoc.ajax.reload();
                    tblDocumentList.ajax.reload();
                    selectedDocIDs = [];
                } else {
                    toastr.error(response.Msg, "Error");
                }
            },
            error: function (error) {
                mApp.unblockPage();
                toastr.error("something went wrong try again later", "Error");
            }
        });
        $("#m_modal_4").modal("hide");
    }
    else {
        mApp.unblockPage();
        toastr.info("Please select atleast one document!", "Info");
        //tblDocumentList.ajax.reload();
    }
});

$("#SearchDoc").click(function (e) {
    tblJepDoc.ajax.reload();
    tblGlobalJepDoc.ajax.reload();

    var selectchbHeaderJEPDoc = $("#chbHeaderJEPDoc").prop("checked");
    if (selectchbHeaderJEPDoc) {
        $("#chbHeaderJEPDoc").prop("checked", false);
    }

    var selectchbHeaderGlobalDoc = $("#chbHeaderGlobalDoc").prop("checked");
    if (selectchbHeaderGlobalDoc) {
        $("#chbHeaderGlobalDoc").prop("checked", false);
    }

    selectedDocIDs = [];
    FinalDoclist = [];
    $("#m_modal_4").modal("show");
    setTimeout(function () {
        tblJepDoc.ajax.reload();
    }, 500);
});

$("a[href='#m_tabs_3_1']").click(function () {
    tblJepDoc.ajax.reload();
});

$("a[href='#m_tabs_3_2']").click(function () {
    tblGlobalJepDoc.ajax.reload();
});

$("div[m-wizard-target='m_wizard_form_step_2']").click(function () {
    tblDocumentList.ajax.reload();
});

$("div[m-wizard-target='m_wizard_form_step_3']").click(function () {
    tblAffectedPart.ajax.reload();
});

$("a[data-wizard-action='next']").click(function () {
    tblAffectedPart.ajax.reload();
    tblDocumentList.ajax.reload();
});

function refresh() {
    setTimeout(function () {
        tblGlobalJepDoc.columns.adjust();
    }, 1000);
}

function GetAffectedDCRDoc() {
    setTimeout(function () {
        tblDocumentList = $('#tblDocumentList').DataTable({
            "serverSide": true,
            "processing": true,
            "orderMulti": false,
            "filter": true,
            "sAjaxSource": "/DES/DCR/GetAffectedDCRDoc",
            "iDisplayLength": 10,
            "ordering": true,
            "scrollX": true,
            "language": {
                "infoFiltered": ""
            },
            "bRetrieve": true,
            "order": [[1, 'desc']],
            "columnDefs": [
                { "orderable": false, "targets": 0 }
            ],

            "fnServerData": function (sSource, data, fnCallback) {
                data.push({ name: "DCRId", value: $("#DCRId").val() });
                data.push({ name: "IsInCorporate", value: false });
                $.ajax({
                    "dataType": "json",
                    "type": "POST",
                    "url": sSource,
                    "data": data,
                    "success": fnCallback
                });
            },
            "aoColumns": [
                {
                    "sTitle": "Action", "className": "action", "orderable": false, render: function (data, type, row, meta) {
                        var srt = "";
                        srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-focus m-btn--icon m-btn--icon-only m-btn--pill btnUV" title="Universal Viewer"><i class="la la-desktop m--font-focus"></i></a>';
                        srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill btnDownload" title="Download"><i class="fa fa-download m--font-brand"></i></a>';
                        srt += '<a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill btnEdit" title="Edit"><i class="la la-edit text-info"></i></a>';
                        srt += '<a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnDelete" title="Delete"><i class="la la-trash text-danger"></i></a>';
                        return srt;
                    }
                },
                {
                    "sTitle": "Doc No", "className": "", render: function (data, type, row, meta) {
                        //return row.DocNo
                        var name = "<a target='_blank' href='/DES/DOC/Detail?q=" + row.QString + "'>" + row.DocNo + "</a>";
                        return name;
                    }
                },
                {
                    "sTitle": "Document Description", "className": "", render: function (data, type, row, meta) {
                        return row.DocDescription
                    }
                },
                {
                    "sTitle": "Rev", "className": "", render: function (data, type, row, meta) {
                        var srt = '';
                        if (row.RevisionNo != null || row.RevisionNo != '') {
                            srt = 'R' + row.RevisionNo;
                        }
                        return srt;
                    }
                },
                {
                    "sTitle": "General Remarks", "className": "", render: function (data, type, row, meta) {
                        return row.GeneralRemarks
                    }
                }
            ],

            "initComplete": function () {

            },

            "fnRowCallback": function () {

            }
        });
    }, 1000);
}

$('#tblDocumentList').on('click', 'td .btnDelete', function () {
    var tr = $(this).closest('tr');
    var row = tblDocumentList.row(tr);
    var data = row.data();
    swal({
        title: "Do you want to delete..?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            if (isConfirm.value) {
                mApp.blockPage();
                $.ajax({
                    type: "GET",
                    url: '/DES/DCR/DeleteAffectedDocument',
                    data: { Id: data.DCRAffectedDocumentId },
                    dataType: 'json',
                    contentType: "application/json;charset=utf-8",
                    async: true,
                    success: function (response) {
                        mApp.unblockPage();
                        if (response.Status) {
                            toastr.success(response.Msg, "Success");
                            tblDocumentList.ajax.reload();
                        } else {
                            toastr.error(response.Msg, "Error");
                        }
                    },
                    error: function (error) {
                        mApp.unblockPage();
                        toastr.error("something went wrong try again later", "Error");
                    }
                });
            }
        });
});

$('#tblDocumentList').on('click', 'td .btnEdit', function () {
    var tr = $(this).closest('tr');
    var row = tblDocumentList.row(tr);
    var data = row.data();
    var html = '<td><a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" onclick="UpdateGeneralRemarks(' + data.DCRAffectedDocumentId + ')" title="Edit"><i class="la la-check text-info"></i></a>'
        + '<a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" onclick="tblDocumentListClear()" title="close"><i class="la la-close text-danger"></i></a></td>'
        + '<td>' + data.DocNo + '</td>'
        + '<td>' + data.DocDescription + '</td>'
        + '<td>R' + data.RevisionNo + '</td>'
    if (data.GeneralRemarks != null)
        html += '<td><input type="text" class="form-control" id="GeneralRemarks_' + data.DCRAffectedDocumentId + '" name="GeneralRemarks_' + data.DCRAffectedDocumentId + '" placeholder="General Remarks" value="' + data.GeneralRemarks + '" /></td>';
    else
        html += '<td><input type="text" class="form-control" id="GeneralRemarks_' + data.DCRAffectedDocumentId + '" name="GeneralRemarks_' + data.DCRAffectedDocumentId + '" placeholder="General Remarks"/></td>';
    $(tr).html(html);
    $("#GeneralRemarks_" + data.DCRAffectedDocumentId).focus();
});

function UpdateGeneralRemarks(id) {
    var GeneralRemarks = $('#GeneralRemarks_' + id).val();
    mApp.blockPage();
    $.ajax({
        type: "POST",
        url: '/DES/DCR/AddDCRdoc',
        data: JSON.stringify({ DCRAffectedDocumentId: id, GeneralRemarks: GeneralRemarks }),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        async: true,
        success: function (response) {
            mApp.unblockPage();
            if (response.Status) {
                toastr.success(response.Msg, "Success");
                tblDocumentList.ajax.reload();
            } else {
                toastr.error(response.Msg, "Error");
            }
        },
        error: function (error) {
            mApp.unblockPage();
            toastr.error("something went wrong try again later", "Error");
        }
    });
}

function tblDocumentListClear() {
    tblDocumentList.ajax.reload();
}

function getGrid() {
    mApp.blockPage();
    tblDocumentFiles = $('#tblDocumentFiles').DataTable({
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "sAjaxSource": "/DES/DCR/GetDCRSupportedDocumentList",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        "language": {
            "infoFiltered": ""
        },
        "bRetrieve": true,
        "order": [],
        "columnDefs": [
            { "orderable": false, "targets": 0 },
            {
                "targets": [2],
                "visible": false
            }
        ],

        "fnServerData": function (sSource, data, fnCallback) {
            data.push({ name: "DCRId", value: $('#DCRId').val() });
            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });
        },
        "aoColumns": [
            {
                "sTitle": "Action", "className": "action", "orderable": false, render: function (data, type, row, meta) {
                    var srt = "";
                    srt += '<a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-focus m-btn--icon m-btn--icon-only m-btn--pill btnUV" title="Universal Viewer"> <i class="la la-desktop m--font-focus"></i></a>';
                    srt += '<a href="#"  class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill btnDownload" title="Download"><i class="fa fa-download m--font-brand"></i></a>';
                    srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnDeleteDFM" title="Delete"><i class="la la-trash text-danger"></i></a>';
                    return srt;
                }
            },
            {
                "sTitle": "Document Title", "className": "", render: function (data, type, row, meta) {
                    return row.DocumentTitle
                }
            },
            {
                "sTitle": "Document Format", "className": "", render: function (data, type, row, meta) {
                    return row.DocumentFormat
                }
            }
        ],

        "initComplete": function () {

        },

        "fnRowCallback": function () {

        }
    });
    mApp.unblockPage();
}

var path;
$('#tblDocumentFiles').on('click', 'td .btnDownload', function () {
    var tr = $(this).closest('tr');
    var row = tblDocumentFiles.row(tr);
    var data = row.data();
    var CurrentLocationIp = $('#CurrentLocationIp').val();
    downloadFile(data.DocumentPath, CurrentLocationIp);
});

$('#tblDocumentList').on('click', 'td .btnDownload', function () {
    var tr = $(this).closest('tr');
    var row = tblDocumentList.row(tr);
    var data = row.data();
    var CurrentLocationIp = $('#CurrentLocationIp').val();
    downloadFile(data.MainDocumentPath, CurrentLocationIp);
});

//$('#tblDocumentFiles').on('click', 'td .btnDownload', function () {
//    var CurrentLocationIp = $('#CurrentLocationIp').val();
//    var CurrentAddDocFolder = $('#AddToLocationIp').val();
//    var FCSurl = CurrentLocationIp + "/Download";
//    var tr = $(this).closest('tr');
//    var row = tblDocumentFiles.row(tr);
//    var data = row.data();
//    var DocPathList = [];
//    DocPathList.push(data.DocumentPath);

//    window.open(FCSurl + "?DocList=" + DocPathList, "DownloadWindow", "width=750, height=750, top=0, screenX=1000, screenY=1000");
//});

function BindPART() {
    tblDCRPart = $('#tblDCRPart').DataTable({
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "sAjaxSource": "/DES/DCR/GetPartList",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        "responsive": false,
        "language": {
            "infoFiltered": ""
        },
        "bRetrieve": true,
        "order": [[1, 'desc']],
        'columnDefs': [
            {
                'orderable': false,
                'targets': 0,
                'checkboxes': {
                    'selectRow': true
                }
            }],
        'select': {
            'style': 'multi'
        },
        "fnServerData": function (sSource, data, fnCallback) {
            data.push({ name: "DCRId", value: $('#DCRId').val() });
            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });
        },
        "aoColumns": [
            {
                "sTitle": "<label class='m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--success'><input type='checkbox' id='chbHeaderPart' onclick='SelectAllPart()'></input><span></span></label>", "sDefaultContent": "<input type='checkbox' ></input>", "bSortable": false, "className": "action", "orderable": false, render: function (data, type, row, meta) {
                    var srt = '<label class="m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--success">'
                    var IDIndex = selectedPartIDs.indexOf(row.ItemId);
                    if (IDIndex !== -1) {
                        srt += '<input type="checkbox" class="m-checkable" id="IsSelect_' + row.ItemId + '"  data-id="' + row.ItemId + '" onclick="AddToPartList(this)" checked>';
                    }
                    else {
                        srt += '<input type="checkbox" class="m-checkable" id="IsSelect_' + row.ItemId + '"  data-id="' + row.ItemId + '" onclick="AddToPartList(this)">';
                    }
                    srt += "<span></span></label>";
                    return srt;
                }
            },
            {
                "sTitle": "Type", "className": "", render: function (data, type, row, meta) {
                    //var name = "<strong style='text-transform: capitalize;'>" + row.DocumentNo + "</strong>";
                    return row.Type
                },
            },
            {
                "sTitle": "ItemKey", "className": "", render: function (data, type, row, meta) {
                    //var name = "<strong style='text-transform: capitalize;'>" + row.DocumentRevision + "</strong>";
                    return row.ItemKey
                },
            },
            {
                "sTitle": "Parent Item", "className": "", render: function (data, type, row, meta) {
                    return row.ParentItem
                }
            },
            {
                "sTitle": "ItemName", "className": "", render: function (data, type, row, meta) {
                    return row.ItemName
                }
            },
            {
                "sTitle": "Rev", "className": "", render: function (data, type, row, meta) {
                    return "R" + row.RevNo
                }
            },
            {
                "sTitle": "DRG No", "className": "", render: function (data, type, row, meta) {
                    return row.DocumentNo
                }
            },
            {
                "sTitle": "Material", "className": "", render: function (data, type, row, meta) {
                    return row.Material
                }
            },
            {
                "sTitle": "ItemGroup", "className": "", render: function (data, type, row, meta) {
                    return row.ItemGroup
                }
            },
        ],

        "initComplete": function () {
            setTimeout(function () {
                tblDCRPart.columns.adjust();
            }, 500);
        },

        "fnRowCallback": function () {
            setTimeout(function () {
                tblDCRPart.columns.adjust();
            }, 500);
        }
    });
}

function SelectAllPart() {
    var data = tblDCRPart.rows().data()[0]["ItemIds"];
    var ItemIdArray = data.split(',');
    var select = $("#chbHeaderPart").prop("checked");
    if (select == true) {
        ItemIdArray.forEach(function (item) {
            item = parseInt(item);
            $("#IsSelect_" + item).prop("checked", true);
            selectedPartIDs.push(item);
        });
    }
    else {
        ItemIdArray.forEach(function (item) {
            item = parseInt(item);
            $("#IsSelect_" + item).prop("checked", false);
            var index = selectedPartIDs.indexOf(item);
            if (index > -1) {
                selectedPartIDs.splice(index, 1);
            }
        });
    }


    //var data = tblDCRPart.rows().data();
    //var select = $("#chbHeaderPart").prop("checked");
    //if (select == true) {
    //    data.each(function (value, index) {
    //        $("#IsSelect_" + data[index].ItemId).prop("checked", true);
    //        selectedPartIDs.push(data[index].ItemId);
    //    });
    //}
    //else {
    //    data.each(function (value, index) {
    //        $("#IsSelect_" + data[index].ItemId).prop("checked", false);
    //        var index = selectedPartIDs.indexOf(data[index].ItemId);
    //        if (index > -1) {
    //            selectedPartIDs.splice(index, 1);
    //        }
    //    });
    //}
    //selectedPartIDs = Array.from(new Set(selectedPartIDs));
}

function AddToPartList($this) {
    var data;
    var allSelected;
    var id = $($this).attr("data-id");
    id = parseInt(id, 10);
    var select = $("#IsSelect_" + id).prop("checked");
    var tableID = $("#IsSelect_" + id).closest("table").attr("id");
    var chbID = $("#" + tableID + " th input[type='checkbox']").attr("id");
    data = tblDCRPart.rows().data();
    if (select == true) {
        selectedPartIDs.push(id);
        selectedPartIDs = Array.from(new Set(selectedPartIDs));
        data.each(function (value, index) {
            if (!selectedPartIDs.includes(data[index].ItemId)) {
                allSelected = false;
            }
        });
        if (allSelected != false) {
            $("#" + chbID).prop("checked", true);
        }
    }
    else {
        var tableID = $("#IsSelect_" + id).closest("table").attr("id");
        var chbID = $("#" + tableID + " th input[type='checkbox']").attr("id");
        $("#" + chbID).prop("checked", false);
        var index = selectedPartIDs.indexOf(id);
        if (index > -1) {
            selectedPartIDs.splice(index, 1);
        }
    }
    selectedPartIDs = Array.from(new Set(selectedPartIDs));
}

$("#btnAddPartList").on("click", function () {
    mApp.blockPage();
    if (selectedPartIDs.length > 0) {
        var FinalPartlist = Array.from(new Set(selectedPartIDs));
        var ItemIDs = new Array();

        for (var id in FinalPartlist) {
            ItemIDs.push({ ItemId: FinalPartlist[id] });
        }

        var model = {};
        model.DCRId = $("#DCRId").val();
        model.dCRPartList = ItemIDs;

        $.ajax({
            type: "POST",
            url: '/DES/DCR/AddPart',
            data: JSON.stringify({ model: model }),
            dataType: 'json',
            contentType: "application/json;charset=utf-8",
            async: true,
            success: function (response) {
                mApp.unblockPage();
                if (response.Status) {
                    toastr.success(response.Msg, "Success");
                    tblDCRPart.ajax.reload();
                    tblAffectedPart.ajax.reload();
                    selectedPartIDs = [];
                } else {
                    toastr.error(response.Msg, "Error");
                }
            },
            error: function (error) {
                mApp.unblockPage();
                toastr.error("something went wrong try again later", "Error");
            }
        })
        $("#m_modal_2").modal("hide");
    }
    else {
        mApp.unblockPage();
        toastr.info("Please select atleast one part!", "Info");
    }
});

function GetAffectedDCRPart() {
    setTimeout(function () {
        tblAffectedPart = $('#tblAffectedPart').DataTable({
            "serverSide": true,
            "processing": true,
            "orderMulti": false,
            "filter": true,
            "sAjaxSource": "/DES/DCR/GetDCRAffectedPart",
            "iDisplayLength": 10,
            "ordering": true,
            "scrollX": true,
            "language": {
                "infoFiltered": ""
            },
            "bRetrieve": true,
            "order": [[1, 'desc']],
            "columnDefs": [
                { "orderable": false, "targets": 0 }
            ],

            "fnServerData": function (sSource, data, fnCallback) {
                data.push({ name: "DCRId", value: $("#DCRId").val() });
                data.push({ name: "IsInCorporate", value: false });
                $.ajax({
                    "dataType": "json",
                    "type": "POST",
                    "url": sSource,
                    "data": data,
                    "success": fnCallback
                });
            },
            "aoColumns": [
                {
                    "sTitle": "Action", "className": "action", "orderable": false, render: function (data, type, row, meta) {
                        var srt = "";
                        if (row.DocumentNo != null) {
                            srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-focus m-btn--icon m-btn--icon-only m-btn--pill btnUV" title="Universal Viewer"><i class="la la-desktop m--font-focus"></i></a>';
                        }
                        srt += '<a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnDelete" title="Delete"><i class="la la-trash text-danger"></i></a>';
                        return srt;
                    }
                },
                {
                    "sTitle": "Type", "className": "", render: function (data, type, row, meta) {
                        //var name = "<strong style='text-transform: capitalize;'>" + row.DocumentNo + "</strong>";
                        return row.Type
                    },
                },
                {
                    "sTitle": "ItemKey", "className": "", render: function (data, type, row, meta) {
                        //var name = "<strong style='text-transform: capitalize;'>" + row.DocumentRevision + "</strong>";
                        //return row.ItemKey
                        var name = "<a target='_blank' href='/DES/Part/Detail?q=" + row.QString + "'>" + row.ItemKey + "</a>";
                        return name;
                    },
                },
                {
                    "sTitle": "Parent Item", "className": "", render: function (data, type, row, meta) {
                        return row.Parent_Item
                    }
                },
                {
                    "sTitle": "ItemName", "className": "", render: function (data, type, row, meta) {
                        return row.ItemName
                    }
                },
                {
                    "sTitle": "Rev", "className": "", render: function (data, type, row, meta) {
                        return "R" + row.RevNo
                    }
                },
                {
                    "sTitle": "DRG No", "className": "", render: function (data, type, row, meta) {
                        return row.DocumentNo
                    }
                },
                {
                    "sTitle": "Material", "className": "", render: function (data, type, row, meta) {
                        return row.Material
                    }
                },
                {
                    "sTitle": "ItemGroup", "className": "", render: function (data, type, row, meta) {
                        return row.ItemGroup
                    }
                },
            ],

            "initComplete": function () {

            },

            "fnRowCallback": function () {

            }
        });
    }, 1000);
}

$("#SearchPart").click(function (e) {
    var selectchbHeaderPart = $("#chbHeaderPart").prop("checked");
    if (selectchbHeaderPart) {
        $("#chbHeaderPart").prop("checked", false);
    }
    selectedPartIDs = [];
    FinalPartlist = [];
    tblDCRPart.ajax.reload();
    $("#m_modal_2").modal("show");
});

function UpdateStatus() {
    mApp.blockPage();
    var flag = true;
    if (tblDocumentList.data().length == 0 && tblAffectedPart.data().length == 0) {
        toastr.info("Please select at least one affected Doc and affected Part", "Info");
        flag = false;
        mApp.unblockPage();
    }

    if (flag) {
        $.ajax({
            type: "POST",
            url: '/DES/DCR/UpdateDCRStatus',
            data: JSON.stringify({ DCRId: $('#DCRId').val(), Status: 'InProcess' }),
            dataType: 'json',
            contentType: "application/json;charset=utf-8",
            async: true,
            success: function (response) {
                mApp.unblockPage();
                if (response.Status) {
                    toastr.success(response.MSG, "Success");
                    window.location.href = "/DES/DCR";
                } else {
                    toastr.error(response.MSG, "Error");
                }
            },
            error: function (error) {
                toastr.error("something went wrong try again later", "Error");
                mApp.unblockPage();
            }
        });
    }

}

$('#tblAffectedPart').on('click', 'td .btnDelete', function () {
    var tr = $(this).closest('tr');
    var row = tblAffectedPart.row(tr);
    var data = row.data();
    swal({
        title: "Do you want to delete..?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            if (isConfirm.value) {
                mApp.blockPage();
                $.ajax({
                    type: "GET",
                    url: '/DES/DCR/DeleteAffectedPart',
                    data: { Id: data.DCRAffectedPartsId },
                    dataType: 'json',
                    contentType: "application/json;charset=utf-8",
                    async: true,
                    success: function (response) {
                        mApp.unblockPage();
                        if (response.Status) {
                            toastr.success(response.Msg, "Success");
                            tblAffectedPart.ajax.reload();
                        } else {
                            toastr.error(response.Msg, "Error");
                        }
                    },
                    error: function (error) {
                        mApp.unblockPage();
                        toastr.error("something went wrong try again later", "Error");
                    }
                });
            }
        });
});

$('#tblDocumentFiles').on('click', 'td .btnDeleteDFM', function () {
    var tr = $(this).closest('tr');
    var row = tblDocumentFiles.row(tr);
    var data = row.data();
    var CurrentLocationIp = $('#CurrentLocationIp').val();
    var FCSurl = CurrentLocationIp + "/api/DeleteDoc";
    var documentName = data.DocumentTitle;

    swal({
        title: "Do you want to delete?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            var rev = "Yes";
            if (isConfirm.value) {
                mApp.blockPage();
                var docdelete = {
                    deletePath: data.DocumentPath
                };

                $.ajax({
                    url: FCSurl,
                    data: JSON.stringify(docdelete),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    cache: false,
                    async: true,
                    processData: false,
                    method: 'POST',
                    type: 'POST', // For jQuery < 1.9
                    success: function (response) {
                        if (response) {
                            //toastr.success(response.Msg, "Successfully removed file from server");

                            $.ajax({
                                type: "GET",
                                url: '/DES/DCR/DeleteDocFile',
                                data: { Id: data.DCRSupportedDocId },
                                dataType: 'json',
                                contentType: "application/json;charset=utf-8",
                                async: true,
                                success: function (response) {
                                    toastr.success(documentName + " " + response.Msg, "Success");
                                    tblDocumentFiles.ajax.reload();
                                    mApp.unblockPage();
                                },
                                error: function (error) {
                                    toastr.error("Error while removing file from grid", "Error");
                                    mApp.unblockPage();
                                }
                            })
                        }
                    },
                    error: function (error) {
                        toastr.error("Error while removing file from server", "Error");
                        mApp.unblockPage();
                    }
                })
            } else {
                mApp.unblockPage();
            }
        });
});


$("#m_aside_left_minimize_toggle").click(function () {
    if (tblDocumentFiles != null)
        tblDocumentFiles.columns.adjust();
});

$('#tblDocumentList').on('click', 'td .btnUV', function () {
    var tr = $(this).closest('tr'); 0
    var row = tblDocumentList.row(tr);
    var data = row.data();
    var DocPathList = data.MainDocumentPath.split(",");
    $.each(DocPathList, function (i) {
        UniverSalFile(DocPathList[i]);
    });
});

$('#tblDocumentFiles').on('click', 'td .btnUV', function () {
    var tr = $(this).closest('tr'); 0
    var row = tblDocumentFiles.row(tr);
    var data = row.data();
    var DocPathList = data.DocumentPath.split(",");
    $.each(DocPathList, function (i) {
        UniverSalFile(DocPathList[i]);
    });
});

$('#tblAffectedPart').on('click', 'td .btnUV', function () {
    var tr = $(this).closest('tr'); 0
    var row = tblAffectedPart.row(tr);
    var data = row.data();
    var DocPathList = data.MainDocumentPath.split(",");
    $.each(DocPathList, function (i) {
        UniverSalFile(DocPathList[i]);
    });
});