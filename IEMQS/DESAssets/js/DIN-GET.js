﻿
var tblDIN;
var getCurrentUser;
$(document).ready(function () {
    getCurrentUser = $('#CurrentPsNo').val();
    mApp.blockPage();

    //Get Document Table
    tblDIN = $('#tblDIN').DataTable({
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "sAjaxSource": "/DES/DIN/Index",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        "responsive": false,
        "language": {
            "infoFiltered": ""
        },
        "order": [[0, 'desc']],
        "columnDefs": [
        ],

        "fnServerData": function (sSource, data, fnCallback) {

            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });

        },
        "aoColumns": [
            {
                "sTitle": "Action", "className": "action", "orderable": false, render: function (data, type, row, meta) {
                    var srt = "";

                    srt += '<a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill  btnViewDIN" title="View"><i class="la la-eye m--font-success"></i></a>';
                    if (row.Authorized > 0) {
                        if (row.Status == 'InProcess') {
                        }
                        else {
                            if (row.Status == "Created" || row.Status == "Draft") {
                                srt += '<a  class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill btnEditDIN" title="Edit"><i class="la la-edit m--font-brand"></i></a>';


                                if ((row.IsDINCheckIn == true)) {
                                    if (row.DINCheckInBy == getCurrentUser) {
                                        srt += '<button class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnCheckOut" title="CheckOut"><i class="fa fa-lock m--font-danger"></i></button>';
                                        srt += '<a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnDeleteDIN" title="Delete"><i class="la la-trash text-danger"></i></a>';
                                    }
                                    else {
                                        srt += '<button class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill m-btn--pill btnCheckIn" title="Check In By ' + row.DINCheckInUser + '" ><i class="fa fa-lock-open m--font-success"></i></button>';
                                    }
                                }
                                else {
                                    srt += '<a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnDeleteDIN" title="Delete"><i class="la la-trash text-danger"></i></a>';
                                }
                            }
                            else if (row.Status == "Completed") {
                                srt += '<a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill btnRevDIN" title="Create Issue"><i class="la la-refresh m--font-brand"></i></a>';
                            }
                        }
                    }
                    return srt;
                }
            },
            {
                "sTitle": "DIN No", "className": "", render: function (data, type, row, meta) {
                    return row.DINNo;
                }
            },
            {
                "sTitle": "Project No", "className": "", render: function (data, type, row, meta) {
                    return row.ProjectNo;
                }
            },
            {
                "sTitle": "Contract No", "className": "", render: function (data, type, row, meta) {
                    return row.ContractNo;
                }
            },
            {
                "sTitle": "Project Description", "className": "", "orderable": false, render: function (data, type, row, meta) {
                    return row.ProjectDescription;
                }
            },
            {
                "sTitle": "Customer", "className": "", "orderable": false, render: function (data, type, row, meta) {
                    return row.Customer;
                }
            },
            {
                "sTitle": "DIN Description", "className": "text-wrap custom-table-width", "orderable": false, render: function (data, type, row, meta) {
                    return row.DinDescription;
                }
            },
            {
                "sTitle": "Code Stamp", "className": "", "orderable": false, render: function (data, type, row, meta) {
                    return row.ASMETag;
                }
            },
            {
                "sTitle": "Nuclear Code", "className": "", "orderable": false, render: function (data, type, row, meta) {
                    return row.NuclearASME;
                }
            },
            {
                "sTitle": "Issue No", "className": "", "orderable": true, render: function (data, type, row, meta) {
                    return row.IssueNo;
                }
            },
            {
                "sTitle": "Status", "className": "", "orderable": true, render: function (data, type, row, meta) {
                    return row.Status;
                }
            }
        ],

        "initComplete": function () {
            $searchButton = $('<div class="filed-list"><button class="btn btn-secondary active"><i class="la la-ellipsis-v"></i></button ><div class="hover-div"><h5>Searchable fields</h5><ul class="list-inline"><li class="list-inline-item">DIN No</li>  <li class= "list-inline-item" >Project No</li><li class="list-inline-item">Contract No</li><li class="list-inline-item">Issue No</li></ul ></div ></div > ');
            $('.dataTables_filter').append($searchButton);
        },

        "fnRowCallback": function () {
            tblDIN.columns.adjust();
        }

    });
    mApp.unblockPage();
});



$('#tblDIN').on('click', 'td .btnDeleteDIN', function () {
    var tr = $(this).closest('tr');
    var row = tblDIN.row(tr);
    var data = row.data();
    swal({
        title: "Do you want to delete?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            if (isConfirm.value) {
                mApp.blockPage();
                $.ajax({
                    type: "GET",
                    url: '/DES/DIN/DeleteDIN',
                    data: { DINId: data.DINId },
                    dataType: 'json',
                    contentType: "application/json;charset=utf-8",
                    async: true,
                    success: function (response) {
                        if (response.Status) {
                            toastr.success(response.Msg, "Success");
                            if (tblDIN.data().count() == 1) {
                                tblDIN.ajax.reload();
                            }
                            else {
                                tblDIN.ajax.reload(null, false);
                            }
                            mApp.unblockPage();
                        } else {
                            toastr.error(response.Msg, "Error");
                            mApp.unblockPage();
                        }
                    },
                    error: function (error) {
                        toastr.error("something went wrong try again later", "Error");
                        mApp.unblockPage();
                    }
                });
            } else {
                mApp.unblockPage();
            }
        });
});

$('#tblDIN').on('click', 'td .btnEditDIN', function () {
    var tr = $(this).closest('tr');
    var row = tblDIN.row(tr);
    var data = row.data();

    if ((data.IsDINCheckIn == true)) {
        if (data.DINCheckInBy == getCurrentUser) {
            window.location = "/DES/DIN/Edit?q=" + data.QString;
        }
        else {
            toastr.info("DIN is already in use by " + data.DINCheckInUser, "Info");
        }
    }
    else {
        window.location = "/DES/DIN/Edit?q=" + data.QString;

    }
});

$('#tblDIN').on('click', 'td .btnViewDIN', function () {
    var tr = $(this).closest('tr');
    var row = tblDIN.row(tr);
    var data = row.data();
    window.location = "/DES/DIN/Detail?q=" + data.QString;
});

$("#m_aside_left_minimize_toggle").click(function () {
    if (tblDIN != null)
        tblDIN.columns.adjust();
});

//function funRevision() {
//    swal({
//        title: "Do you want to create new revision?",
//        type: "warning",
//        allowOutsideClick: false,
//        showConfirmButton: true,
//        showCancelButton: true,
//        confirmButtonClass: "btn btn-danger",
//        cancelButtonClass: "btn btn-secondary",
//        confirmButtonText: "Yes",
//        cancelButtonText: "No"
//    }).then(
//        function (isConfirm) {
//            var rev = "Yes";
//            if (isConfirm.value) {
//                window.location = "/DES/DOC/Edit?q=" + DocId + "&Revision=" + rev;
//            } else {
//            }
//        });
//}

$('#tblDIN').on('click', 'td .btnRevDIN', function () {
    var tr = $(this).closest('tr');
    var row = tblDIN.row(tr);
    var data = row.data();

    swal({
        title: "Do you want to create new Issue?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            if (isConfirm.value) {
                var model = {
                    DINId: data.DINId
                };
                mApp.blockPage();
                $.ajax({
                    type: "Post",
                    url: '/DES/DIN/CreateDINRev',
                    data: JSON.stringify(model),
                    dataType: 'json',
                    contentType: "application/json;charset=utf-8",
                    async: true,
                    success: function (response) {
                        if (response.Status) {
                            toastr.success(response.Msg, "Success");
                            tblDIN.ajax.reload();
                            window.location = response.rurl;
                            mApp.unblockPage();
                        } else {
                            toastr.error(response.Msg, "Error");
                            mApp.unblockPage();
                        }
                    },
                    error: function (error) {
                        toastr.error("something went wrong try again later", "Error");
                        mApp.unblockPage();
                    }
                });
            }
        });
});

$('#tblDIN').on('click', 'td .btnCheckOut', function () {
    var tr = $(this).closest('tr');
    var row = tblDIN.row(tr);
    var data = row.data();
    var docMapp = {
        DINId: data.DINId
    };
    swal({
        title: "Do you want to release DIN?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(function (isConfirm) {
        if (isConfirm.value) {
            mApp.blockPage();
            $.ajax({
                url: "/DES/DIN/UpdateDINCheckOut",
                data: JSON.stringify(docMapp),
                type: "POST",
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                aysnc: true,
                success: function (response) {
                    if (response) {
                        if (response.Status) {
                            toastr.info(response.Msg, "Info");
                            tblDIN.ajax.reload();
                            mApp.unblockPage();
                        }
                    }
                },
                error: function (response) {
                    toastr.error("something went wrong try again later", "Error");
                    mApp.unblockPage();
                }

            });
        }
    });
});

$('#tblDIN').on('click', 'td .btnCheckIn', function () {
    var tr = $(this).closest('tr');
    var row = tblDIN.row(tr);
    var data = row.data();
    if ((data.IsDINCheckIn == true)) {
        if (data.DINCheckInBy != getCurrentUser) {
            toastr.info("DIN is already in use by " + data.DINCheckInUser, "Info");
        }
    }
});
