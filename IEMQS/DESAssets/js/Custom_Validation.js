﻿$("body").on("keypress", ".blockSpecialChar", function (e) {
    //Spacial chaer include only  /\:*?"<>|  @ , * ? [ ] # $ { } " .
    //47, 92, 58, 42, 63, 34, 60, 62, 124,64 ,44,42,63,91,93,35,36,123,125,46

    var k;
    document.all ? k = e.keyCode : k = e.which;
    return (k != 47 && k != 92 && k != 58 && k != 42 && k != 63 && k != 34 && k != 60 && k != 62 && k != 124 && k != 64 && k != 44 && k != 42 && k != 63 && k != 91 && k != 93 && k != 35 && k != 36 && k != 123 && k != 125 && k != 46);
});

$("body").on("keypress", ".noSpace", function (e) {
    var k;
    document.all ? k = e.keyCode : k = e.which;
    return !(k == 32);
});


$("body").on("keypress", ".onlyNumeric", function (e) {
    var keyCode = e.which ? e.which : e.keyCode;
    if (!(keyCode >= 48 && keyCode <= 57)) {
        return false;
    }
});

$("body").on("keypress", ".percentage", function (e) {
    $('.numeric').autoNumeric('init', { aPad: "false", vMin: "0", vMax: "100", aForm: "false" });
});



$("body").on("keypress", ".allownumericwithdecimal", function (e) {
    $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
    if ((e.which != 46 || $(this).val().indexOf('.') != -1) && (e.which < 48 || e.which > 57)) {
        return false;
    }
});

function downloadFile(fielpath, CurrentLocationIp) {
    var FCSurl = CurrentLocationIp + "/Download";
    var DocPathList = [];
    DocPathList.push(fielpath);
    popupWindow(FCSurl + "?DocList=" + DocPathList, "DownloadWindow", window, 500, 500);
}
function popupWindow(url, title, win, w, h) {
    const y = win.top.outerHeight / 2 + win.top.screenY - (h / 2);
    const x = win.top.outerWidth / 2 + win.top.screenX - (w / 2);
    return win.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + y + ', left=' + x);
}

function UniverSalFile(DocFilePath) {
    var randomnumber = Math.floor((Math.random() * 100) + 1);
    var EncodeDocFilePath = escape(DocFilePath);
    // popupWindowUV("https://10.7.66.83:96/Home/Index?DocPath=" + EncodeDocFilePath, "UniverSalWindow", window, screen.availWidth, screen.availHeight, randomnumber);
    popupWindowUV("https://phzpdiemqsweb.lthed.com:100/Home/Index?DocPath=" + EncodeDocFilePath, "UniverSalWindow", window, screen.availWidth, screen.availHeight);
}

function UniverSalFileWTAnnotation(DocFilePath, FileId, UserId, DocNo,DocId) {
    var randomnumber = Math.floor((Math.random() * 100) + 1);
    var EncodeDocFilePath = escape(DocFilePath);

    var extension = String(DocFilePath.substr((DocFilePath.lastIndexOf('.') + 1))).toLowerCase();
    
    switch (extension) {
        case 'prt':
        case 'sldprt':
        case 'par':
        case 'stp':
        case 'catpart':
        case 'igs':
        case 'iges':
        case 'jt':
        case 'x_t':
        case '3dxml':
            popupWindowUV("http://phzpdiemqsweb.lthed.com:8444/?DocumentMappingId=" + FileId + "&UserId=" + UserId , "UniverSalWindow", window, screen.availWidth, screen.availHeight, randomnumber);
            break;  
        default:
           // popupWindowUV("https://10.7.66.83:96/Home/Index?DocPath=" + EncodeDocFilePath, "UniverSalWindow", window, screen.availWidth, screen.availHeight, randomnumber);
            popupWindowUV("https://phzpdiemqsweb.lthed.com:100/Home/Index?DocPath=" + EncodeDocFilePath + "&FileId=" + FileId + "&UserID=" + UserId + "&DocNo=" + DocNo + "&DocId=" + DocId , "UniverSalWindow", window, screen.availWidth, screen.availHeight);
            break;
    }

    
}

function popupWindowUV(url, title, win, w, h, no) {
    const y = win.top.outerHeight / 2 + win.top.screenY - (h / 2);
    const x = win.top.outerWidth / 2 + win.top.screenX - (w / 2);
    return win.open(url, '_blank', title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + y + ', left=' + x);

}

$("body").on("keypress", ".alphnumericspacedashunddot", function (e) {
    var k;
    document.all ? k = e.keyCode : k = e.which;
    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57) || k == 45 || k == 95 || k == 46);
});