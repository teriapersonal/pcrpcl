﻿var tblDocumentList;
var tblDeptPersonList;
var tblAllDocs;
var tblAllParts;
var selectedDocIDs = [];
var selectedPartIDs = [];
var tblDCRHistory;
var tblDeptPerson;
var departmentId = null;
var isfromPersonSelectedcalled = false;
var Dropzonefile;
var tblJepDoc;
var tblGlobalJepDoc;
var tblAffectedPart;
var tblDCRPart;

$(document).ready(function () {
    //WizardDemo.init(); 
    BindPART();
    BindJEPDoc();
    BindGlobalJEPDoc();
    GetAffectedDCRDoc();
    GetAffectedDCRPart();
    getGrid();
    getGridtblSupportDocumentFiles();
    bindDeptPersonList();
    $("#selecttest").select2();

    $("#btnAddDocList").on("click", function () {
        mApp.blockPage();
        if (selectedDocIDs.length > 0) {
            var FinalDoclist = Array.from(new Set(selectedDocIDs));
            var DocIDs = new Array();

            for (var id in FinalDoclist) {
                DocIDs.push({ JEPDocumentDetailsId: FinalDoclist[id] });
            }

            var model = {};
            model.DCRId = $("#DCRId").val();
            model.dCRDocumentList = DocIDs;

            $.ajax({
                type: "POST",
                url: '/DES/DCR/AddDCRdoc',
                data: JSON.stringify({ model: model }),
                dataType: 'json',
                contentType: "application/json;charset=utf-8",
                async: true,
                success: function (response) {
                    mApp.unblockPage();
                    if (response.Status) {
                        toastr.success(response.Msg, "Success");
                        tblGlobalJepDoc.ajax.reload();
                        tblJepDoc.ajax.reload();
                        tblDocumentList.ajax.reload();
                        selectedDocIDs = [];
                    } else {
                        toastr.error(response.Msg, "Error");
                    }
                },
                error: function (error) {
                    mApp.unblockPage();
                    toastr.error("something went wrong try again later", "Error");
                }
            });
            $("#m_modal_3").modal("hide");
        }
        else {
            mApp.unblockPage();
            toastr.info("Please select atleast one document!", "Info");
            //tblDocumentList.ajax.reload();
        }
    });

    $("#btnAddPartList").on("click", function () {
        mApp.blockPage();
        if (selectedPartIDs.length > 0) {
            var FinalPartlist = Array.from(new Set(selectedPartIDs));
            var ItemIDs = new Array();

            for (var id in FinalPartlist) {
                ItemIDs.push({ ItemId: FinalPartlist[id] });
            }

            var model = {};
            model.DCRId = $("#DCRId").val();
            model.dCRPartList = ItemIDs;

            $.ajax({
                type: "POST",
                url: '/DES/DCR/AddPart',
                data: JSON.stringify({ model: model }),
                dataType: 'json',
                contentType: "application/json;charset=utf-8",
                async: true,
                success: function (response) {
                    mApp.unblockPage();
                    if (response.Status) {
                        toastr.success(response.Msg, "Success");
                        tblDCRPart.ajax.reload();
                        tblAffectedPart.ajax.reload();
                        selectedPartIDs = [];
                    } else {
                        toastr.error(response.Msg, "Error");
                    }
                },
                error: function (error) {
                    mApp.unblockPage();
                    toastr.error("something went wrong try again later", "Error");
                }
            })
            $("#m_modal_6").modal("hide");
        }
        else {
            mApp.unblockPage();
            toastr.info("Please select atleast one part!", "Info");
        }
    });

    function refresh() {
        setTimeout(function () {
            tblJepDoc.columns.adjust();
            tblGlobalJepDoc.columns.adjust();
        }, 1000);
    }

    //setTimeout(function () {
    //    $(".select-function").select2({ placeholder: "Select" });
    //    $(".select-Psno").select2({ placeholder: "-----Select-----" });
    //}, 1000);
});

//var WizardDemo = function () {
//    $("#m_wizard");
//    var e, r, i = $("#m_form"), f = $("#FormDetailDCR");
//    return {
//        init: function () {
//            var n;
//            $("#m_wizard"), i = $("#m_form"), (r = new mWizard("m_wizard", {
//                startStep: 1
//            })).on("beforeNext", function (r) {

//            }), r.on("change", function (e) {
//                mUtil.scrollTop()
//            }), r.on("change", function (e) {
//                1 === e.getStep();
//            })
//        }
//        , Gotonext: function () {
//            r.goNext();
//            tblDocumentFiles.columns.adjust();
//            tblJepDoc.columns.adjust();
//            tblGlobalJepDoc.columns.adjust();
//            tblDocumentList.columns.adjust();
//            tblDCRPart.columns.adjust();
//            tblAffectedPart.columns.adjust();

//            //setTimeout(function () {
//            //    tblDocumentFiles.ajax.reload();
//            //    tblJepDoc.ajax.reload();
//            //    tblGlobalJepDoc.ajax.reload();
//            //    tblDocumentList.ajax.reload();
//            //    tblDCRPart.ajax.reload();
//            //    tblAffectedPart.ajax.reload();
//            //}, 500);

//        }
//        , GotoBack: function () {
//            r.goPrev();
//            //e.goNext();
//            tblDocumentFiles.columns.adjust();
//            tblJepDoc.columns.adjust();
//            tblGlobalJepDoc.columns.adjust();
//            tblDocumentList.columns.adjust();
//            tblDCRPart.columns.adjust();
//            tblAffectedPart.columns.adjust();

//            //setTimeout(function () {
//            //    tblDocumentFiles.ajax.reload();
//            //    tblJepDoc.ajax.reload();
//            //    tblGlobalJepDoc.ajax.reload();
//            //    tblDocumentList.ajax.reload();
//            //    tblDCRPart.ajax.reload();
//            //    tblAffectedPart.ajax.reload();
//            //}, 500);
//        }
//        , GoTo: function (i) {
//            if ($("#DCRId").val() != "") {
//                r.goTo(i);
//                tblDocumentFiles.columns.adjust();
//                tblJepDoc.columns.adjust();
//                tblGlobalJepDoc.columns.adjust();
//                tblDocumentList.columns.adjust();
//                tblDCRPart.columns.adjust();
//                tblAffectedPart.columns.adjust();
//            }
//        }
//    }
//}();

//CKEDITOR.replace('ReasonForChange', {
//    skin: 'moono',
//    enterMode: CKEDITOR.ENTER_BR,
//    shiftEnterMode: CKEDITOR.ENTER_P,
//    height: '100px',
//    toolbar: [{
//        name: 'basicstyles',
//        groups: ['basicstyles'],
//        items: ['Bold', 'Italic', 'Underline', "-", 'TextColor', 'BGColor']
//    },

//    {
//        name: 'scripts',
//        items: ['Subscript', 'Superscript']
//    },
//    {
//        name: 'editing',
//        items: ['Scayt']
//    }
//    ],
//    removePlugins: 'elementspath',
//    resize_enabled: false,
//});

//CKEDITOR.config.scayt_autoStartup = true;

//CKEDITOR.replace('ChangeFrom', {
//    skin: 'moono',
//    enterMode: CKEDITOR.ENTER_BR,
//    height: '100px',

//    shiftEnterMode: CKEDITOR.ENTER_P,

//    toolbar: [{
//        name: 'basicstyles',
//        groups: ['basicstyles'],
//        items: ['Bold', 'Italic', 'Underline', "-", 'TextColor', 'BGColor']
//    },

//    {
//        name: 'scripts',
//        items: ['Subscript', 'Superscript']
//    },
//    {
//        name: 'editing',
//        items: ['Scayt']
//    }
//    ],
//    removePlugins: 'elementspath',
//    resize_enabled: false
//});

//CKEDITOR.replace('ChangeTo', {
//    skin: 'moono',
//    enterMode: CKEDITOR.ENTER_BR,
//    height: '100px',
//    shiftEnterMode: CKEDITOR.ENTER_P,
//    toolbar: [{
//        name: 'basicstyles',
//        groups: ['basicstyles'],
//        items: ['Bold', 'Italic', 'Underline', "-", 'TextColor', 'BGColor']
//    },

//    {
//        name: 'scripts',
//        items: ['Subscript', 'Superscript']
//    },
//    {
//        name: 'editing',
//        items: ['Scayt']
//    }
//    ],
//    removePlugins: 'elementspath',
//    resize_enabled: false,
//});

funSuccess1 = function (data) {
    if (data.Status) {
        //toastr.success(data.Msg, "Success");
        $('#DCRId').val(data.model.DCRId);
        //DCRId = $('#DCRId').val();
        $('#txtProjectNo').val(Project);
        $('#DCRNo').val(data.model.DCRNo);
        $('#DCRSupportedDocId').val(data.model.DCRSupportedDocId);
        WizardDemo.Gotonext();
    } else {
        toastr.error(data.Msg, "Error");
    }
    mApp.unblockPage();
}

$("#tblDeptPersonList").on("change", "select[id$='__DeptId']", function () {
    var valDeptId = $(this).val();
    var id = $(this).closest("tr").attr("data-id");
    if (valDeptId != '' && valDeptId != null && isfromPersonSelectedcalled != true) {
        $('#t_depc').val(valDeptId);
        mApp.blockPage();
        $.ajax({
            type: "GET",
            url: "/DES/DepartmentGroup/FunctionByPSNumberList",
            data: { "DeptId": valDeptId },
            datatype: "json",
            async: true,
            success: function (data) {
                mApp.unblockPage();
                $("#DepartmentGroupList_" + id + "__PSNoList").html("");
                var datas = data.list;
                $.each(datas, function (key, entry) {
                    $("#DepartmentGroupList_" + id + "__PSNoList").append($('<option></option>').attr('value', entry.t_psno).text(entry.t_name));
                });
                $("#DepartmentGroupList_" + id + "__PSNoList").select2();
                $("#DepartmentGroupList_" + id + "__PSNoList").select2({
                    placeholder: "-----Select-----"
                });
            },
            error: function () {
                mApp.unblockPage();
                toastr.error("something went wrong try again later", "error");
            }
        });
    }
});

$("#tblDeptPersonList").on("change", ".select-Psno", function () {
    //alert("hello");
    var valDeptId = $(this).val();
    var id = $(this).closest("tr").attr("data-id");
    if (valDeptId.length > 0) {
        $.ajax({
            type: "GET",
            url: "/DES/DepartmentGroup/GetDeptIdByPsNo?psno=" + valDeptId,
            //data: JSON.stringify({
            //    "psno": valDeptId
            //}),
            datatype: "json",
            async: true,
            success: function (data) {
                //$('select[id^="#DepartmentGroupList_' + id + '__DeptId"] option:selected').attr("selected", null);
                // $("#DepartmentGroupList_" + id + "__DeptId option[value = " + data + "]").select2("val", "3101");
                //$('#DepartmentGroupList_' + id + '__DeptId').select2('data', { id: '3101', text: '3101 - MANUFACTURING INCHARGE OFFICE' });
                //$("#DepartmentGroupList_" + id + "__DeptId").val(data);
                //debugger;
                // $(".select2-selection__placeholder").val('3101 - MANUFACTURING INCHARGE OFFICE');
                //$("#DepartmentGroupList_" + id + "__DeptId").select2("val", data);
                //$("#DepartmentGroupList_0__DeptId").select2("val", "3101");
                //$("#DepartmentGroupList_" + id + "__DeptId option[value= " + data + "]").attr('selected', 'selected');
                isfromPersonSelectedcalled = true;
                departmentId = data;
                $("#DepartmentGroupList_" + id + "__DeptId").val(data).trigger('change');
                //$("#selecttest").select2("val", "2");
                isfromPersonSelectedcalled = false;
                //debugger;
            },
            error: function () {
                //mApp.unblockPage();
                toastr.error("something went wrong try again later", "error");
            }
        });
    }
    else {
        $("#DepartmentGroupList_" + id + "__DeptId").val(departmentId).trigger('change');
    }
});

function AddDeptPerson(Id) {
    var rowCount = $('#tbl_DeptPerson tr').length;
    mApp.blockPage();
    var _valfunBegin = funBegin();
    if (_valfunBegin) {
        $("#del_" + Id).show();
        $("#Add_" + Id).hide();

        $.ajax({
            url: '/DES/DCR/AddDeptPerson',
            type: 'GET',
            traditional: true,
            data: { TNo: rowCount },
            contentType: 'application/json; charset=utf-8',
            dataType: "Html",
            success: function (result) {
                mApp.unblockPage();
                $("#tbl_DeptPerson").append(result);
                CalculateDeptPerson();
                $("#del_0").show();
                $("#Add_" + Id).hide();
                //$("#Add_" + Id).hide();
                //$("#del_" + (parseInt(Id) + 1)).hide();
                PopulatePersondropdown(Id);
            },
            error: function (xhr) {
                mApp.unblockPage();
            }
        });
    }
    mApp.unblockPage();
}

function PopulatePersondropdown(Id) {
    var id = Id;
    isfromPersonSelectedcalled = false;
    departmentId = null;
    $(".select-Psno").select2({
        placeholder: "-----Select-----",
        //tags: true,
        multiple: true,
        tokenSeparators: [',', ' '],
        minimumInputLength: 2,
        minimumResultsForSearch: 10,
        ajax: {
            url: '/DES/DepartmentGroup/GetPsnoNameByNameOrDept',
            dataType: 'json',
            data: function (params) {
                return {
                    q: params.term,
                    DeptId: departmentId,
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return item;
                        return {
                            text: data.text,
                            id: data.id
                        }
                    })
                };
            },
        },

    });
}

function CalculateDeptPerson() {
    var rowCount = $('#tbl_DeptPerson tr').length;
    var step = 1;
    for (var i = 0; i < rowCount; i++) {
        //if ($("#DepartmentGroupList_" + i + " __IsAdd).val() !="true"){

        if ($("#DepartmentGroupList_" + i + "__IsDelete").val() != "true") {
            //$("#DepartmentGroupList_" + i + "__OrderNo").val(step);
            step = step + 1;
        }
    }
}

function RemoveDeptPerson($this) {
    swal({
        title: "Do you want to delete..?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            if (isConfirm.value) {
                var tid = $($this).attr("delete-id");
                $("#DepartmentGroupList_" + tid + "__IsDelete").val("true");
                $("#tr_" + tid).hide();
                var rowCount = $('#tbl_DeptPerson tr:visible').length;
                if (!(rowCount > 0)) {
                    AddDeptPerson(Id);
                }
                CalculateDeptPerson();
            }
        });
}

$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    if (tblDocumentList != null)
        tblDocumentList.columns.adjust();
    if (tblAffectedPart != null)
        tblAffectedPart.columns.adjust();
    tblDocumentFiles.columns.adjust();
    tblSupportDocumentFiles.columns.adjust();
    if (tblDeptPerson != null)
        tblDeptPerson.columns.adjust();

    $(".select-function").select2({
        placeholder: "Select"
    });
    $(".select-Psno").select2({
        placeholder: "-----Select-----",
        //tags: true,
        multiple: true,
        tokenSeparators: [',', ' '],
        minimumInputLength: 2,
        minimumResultsForSearch: 10,
        ajax: {
            url: '/DES/DepartmentGroup/GetPsnoNameByNameOrDept',
            dataType: 'json',
            data: function (params) {
                return {
                    q: params.term,
                    DeptId: departmentId,
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return item;
                        return {
                            text: data.text,
                            id: data.id
                        }
                    })
                };
            },
        },

    });

    //alert("Hello");
});

function GetAffectedDCRDoc() {
    setTimeout(function () {
        tblDocumentList = $('#tblDocumentList').DataTable({
            "serverSide": true,
            "processing": true,
            "orderMulti": false,
            "filter": true,
            "sAjaxSource": "/DES/DCR/GetAffectedDCRDoc",
            "iDisplayLength": 10,
            "ordering": true,
            "scrollX": true,
            "language": {
                "infoFiltered": ""
            },
            "bRetrieve": true,
            "order": [[1, 'desc']],
            "columnDefs": [
                { "orderable": false, "targets": 0 }
            ],

            "fnServerData": function (sSource, data, fnCallback) {
                data.push({ name: "DCRId", value: $("#DCRId").val() });
                data.push({ name: "IsInCorporate", value: false });
                $.ajax({
                    "dataType": "json",
                    "type": "POST",
                    "url": sSource,
                    "data": data,
                    "success": fnCallback
                });
            },
            "aoColumns": [
                {
                    "sTitle": "Action", "className": "action", "orderable": false, render: function (data, type, row, meta) {
                        var srt = "";
                        srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-focus m-btn--icon m-btn--icon-only m-btn--pill btnUV" title="Universal Viewer"><i class="la la-desktop m--font-focus"></i></a>';
                        srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill btnDownload" title="Download"><i class="fa fa-download m--font-brand"></i></a>';
                        if (($('#ResponsibleDesignEngineer').val() == $('#CurrentUser').val()) && ($('#PolicyStepForView').val() == "Submit") ||
                            ($('#ResponsibleSeniorDesignEngineer').val() == $('#CurrentUser').val()) && ($('#PolicyStepForView').val() == "Evaluate"))
                        {
                            srt += '<a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnDelete" title="Delete"><i class="la la-trash text-danger"></i></a>';
                        }                        
                        return srt;
                    }
                },
                {
                    "sTitle": "Doc No", "className": "", render: function (data, type, row, meta) {
                        //return row.DocNo
                        var name = "<a target='_blank' href='/DES/DOC/Detail?q=" + row.QString + "'>" + row.DocNo + "</a>";
                        return name;
                    }
                },
                {
                    "sTitle": "Document Description", "className": "", render: function (data, type, row, meta) {
                        return row.DocDescription
                    }
                },
                {
                    "sTitle": "Rev", "className": "", render: function (data, type, row, meta) {
                        return "R" + row.RevisionNo
                    }
                },
                {
                    "sTitle": "General Remarks", "className": "", render: function (data, type, row, meta) {
                        return row.GeneralRemarks
                    }
                }
            ],

            "initComplete": function () {

            },

            "fnRowCallback": function () {

            }
        });
    }, 1000);
}

function GetAffectedDCRPart() {
    setTimeout(function () { 
        tblAffectedPart = $('#tblAffectedPart').DataTable({
            "serverSide": true,
            "processing": true,
            "orderMulti": false,
            "filter": true,
            "sAjaxSource": "/DES/DCR/GetDCRAffectedPart",
            "iDisplayLength": 10,
            "ordering": true,
            "scrollX": true,
            "language": {
                "infoFiltered": ""
            },
            "bRetrieve": true,
            "order": [[1, 'desc']],
            "columnDefs": [
                { "orderable": false, "targets": 0 }
            ],

            "fnServerData": function (sSource, data, fnCallback) {
                data.push({ name: "DCRId", value: $("#DCRId").val() });
                data.push({ name: "IsInCorporate", value: false });
                $.ajax({
                    "dataType": "json",
                    "type": "POST",
                    "url": sSource,
                    "data": data,
                    "success": fnCallback
                });
            },
            "aoColumns": [
                {
                    "sTitle": "Action", "className": "action", "orderable": false, render: function (data, type, row, meta) {                       
                        var srt = "";
                        if (row.DocumentNo != null) {
                            srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-focus m-btn--icon m-btn--icon-only m-btn--pill btnUV" title="Universal Viewer"><i class="la la-desktop m--font-focus"></i></a>';
                        }
                        if (($('#ResponsibleDesignEngineer').val() == $('#CurrentUser').val()) && ($('#PolicyStepForView').val() == "Submit") ||
                            ($('#ResponsibleSeniorDesignEngineer').val() == $('#CurrentUser').val()) && ($('#PolicyStepForView').val() == "Evaluate")) {
                            srt += '<a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnDelete" title="Delete"><i class="la la-trash text-danger"></i></a>';
                        }
                        return srt;
                    }
                },
                {
                    "sTitle": "Type", "className": "", render: function (data, type, row, meta) {
                        //var name = "<strong style='text-transform: capitalize;'>" + row.DocumentNo + "</strong>";
                        return row.Type
                    },
                },
                {
                    "sTitle": "ItemKey", "className": "", render: function (data, type, row, meta) {
                        //var name = "<strong style='text-transform: capitalize;'>" + row.DocumentRevision + "</strong>";
                        //return row.ItemKey 
                        var name = "<a target='_blank' href='/DES/Part/Detail?q=" + row.QString + "'>" + row.ItemKey + "</a>";
                        return name;
                    },
                },
                {
                    "sTitle": "Parent Item", "className": "", render: function (data, type, row, meta) {
                        return row.Parent_Item
                    }
                },
                {
                    "sTitle": "ItemName", "className": "", render: function (data, type, row, meta) {
                        return row.ItemName
                    }
                },
                {
                    "sTitle": "Rev", "className": "", render: function (data, type, row, meta) {
                        return "R" + row.RevNo
                    }
                },
                {
                    "sTitle": "DRG No", "className": "", render: function (data, type, row, meta) {
                        return row.DocumentNo
                    }
                },
                {
                    "sTitle": "Material", "className": "", render: function (data, type, row, meta) {
                        return row.Material
                    }
                },
                {
                    "sTitle": "ItemGroup", "className": "", render: function (data, type, row, meta) {
                        return row.ItemGroup
                    }
                },
            ],

            "initComplete": function () {

            },

            "fnRowCallback": function () {

            }
        });
    }, 1000);
}

function getGrid() {
    tblDocumentFiles = $('#tblDocumentFiles').DataTable({
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "sAjaxSource": "/DES/DCR/GetDCRSupportedDocumentList",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        "language": {
            "infoFiltered": ""
        },
        "bRetrieve": true,
        "order": [[1, 'desc']],
        "columnDefs": [
            { "orderable": false, "targets": 0 },
            {
                "targets": [2],
                "visible": false
            }
        ],

        "fnServerData": function (sSource, data, fnCallback) {
            data.push({ name: "DCRId", value: $('#DCRId').val() });
            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });
        },
        "aoColumns": [
            {
                "sTitle": "Action", "className": "action", "orderable": false, render: function (data, type, row, meta) {
                    var srt = "";
                    srt += '<a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-focus m-btn--icon m-btn--icon-only m-btn--pill btnUV" title="Universal Viewer"> <i class="la la-desktop m--font-focus"></i></a>';
                    srt += '<a href="#"  class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill btnDownload" title="Download"><i class="fa fa-download m--font-brand"></i></a>';
                    return srt;
                }
            },
            {
                "sTitle": "Document Title", "className": "", render: function (data, type, row, meta) {
                    return row.DocumentTitle
                }
            },
            {
                "sTitle": "Document Format", "className": "", render: function (data, type, row, meta) {
                    return row.DocumentFormat
                }
            }
        ],

        "initComplete": function () {

        },

        "fnRowCallback": function () {

        }
    });
}

funSuccess = function (data) {
    if (data.Status) {
        //toastr.success(data.Msg, "Success");
        window.location.href = "/DES/DCR";
    }
    else {
        toastr.error(data.Msg, "Error");
        mApp.unblockPage();
    }
}

funSuccessRemark = function (data) {
    if (data.Remark != null) {
        if (data.Status) {
            toastr.success(data.Msg, "Success");
            window.location.href = "/DES/DCR";
        }
        else {
            toastr.error(data.Msg, "Error");
            mApp.unblockPage();
        }
    }
    else {
        toastr.info(data.Msg, "Info");
        mApp.unblockPage();
    }
}

funSuccessReviewRemark = function (data) {
    if (data.Remark) {
        if (data.Status) {
            toastr.success(data.Msg, "Success");
            window.location.href = "/DES/DCR";
        }
        else {
            toastr.error(data.Msg, "Error");
            mApp.unblockPage();
        }
    }
    else {
        toastr.info(data.Msg, "Info");
        mApp.unblockPage();
    }
}

funSuccessReviewCommentRemark = function (data) {
    if (data.Remark) {
        if (data.Status) {
            toastr.success(data.Msg, "Success");
            window.location.href = "/DES/DCR";
        }
        else {
            toastr.error(data.Msg, "Error");
            mApp.unblockPage();
        }
    }
    else {
        toastr.info(data.Msg, "Info");
        mApp.unblockPage();
    }
}

funSuccessReleaseRemark = function (data) {
    if (data.Remark) {
        if (data.Status) {
            toastr.success(data.Msg, "Success");
            window.location.href = "/DES/DCR";
        }
        else {
            toastr.error(data.Msg, "Error");
            mApp.unblockPage();
        }
    }
    else {
        toastr.info(data.Msg, "Info");
        mApp.unblockPage();
    }
}

funBegin = function (data) {
    //var me = event.currentTarget;
    //var data1 = me.data.value;

    mApp.blockPage();

    var temp = $('#tblDeptPersonList tbody tr').length;
    var flag = true;

    if (temp > 1) {

        var step = [];
        var rowCount = $('#tbl_DeptPerson tr').length;
        //for (var i = 0; i < rowCount; i++) {
        //    $("span[data-valmsg-for='DepartmentGroupList[" + i + "].DeptId']").html("");
        //    $("span[data-valmsg-for='DepartmentGroupList[" + i + "].PSNoList']").html("");
        //    if ($("#DepartmentGroupList_" + i + "__IsDelete").val() != "true") {
        //        if (i != 0 && i == (rowCount - 1)) {
        //            if ($("#DepartmentGroupList_" + i + "__DeptId").val() != "") {
        //                if (step.indexOf($("#DepartmentGroupList_" + i + "__DeptId").val()) >= 0) {
        //                    flag = false;
        //                    $("span[data-valmsg-for='DepartmentGroupList[" + i + "].DeptId']").html("Department already exist");
        //                }


        //                step.push($("#DepartmentGroupList_" + i + "__DeptId").val());

        //                if ($("#DepartmentGroupList_" + i + "__PSNoList").val() == "") {
        //                    $("span[data-valmsg-for='DepartmentGroupList[" + i + "].PSNoList']").html("PSNumber is required");
        //                    flag = false;
        //                }
        //            }

        //        } else {
        //            if (step.indexOf($("#DepartmentGroupList_" + i + "__DeptId").val()) >= 0) {
        //                flag = false;
        //                $("span[data-valmsg-for='DepartmentGroupList[" + i + "].DeptId']").html("Department already exist");
        //            }


        //            step.push($("#DepartmentGroupList_" + i + "__DeptId").val());

        //            if ($("#DepartmentGroupList_" + i + "__PSNoList").val() == "") {
        //                $("span[data-valmsg-for='DepartmentGroupList[" + i + "].PSNoList']").html("PSNumber is required");
        //                flag = false;
        //            }
        //            if ($("#DepartmentGroupList_" + i + "__DeptId").val() == "") {
        //                $("span[data-valmsg-for='DepartmentGroupList[" + i + "].DeptId']").html("Department is required");
        //                flag = false;
        //            }
        //        }
        //    }
        //}
    }
    return flag;
    //if (flag) {
    //    debugger
    //    console.log(value[0])
    //    var buttonValue = getParameterByName(value[0].data, "Button");
    //    commonSwalMsg(buttonValue)
    //}
    //return false;

};

funSuccess2 = function (data) {
    if (data.Status) {
        toastr.success(data.Msg, "Success");
        //window.location.href = "/DES/DCR";
        mApp.unblockPage();
    } else {
        toastr.error(data.Msg, "Error");
        mApp.unblockPage();
    }
}

function getParameterByName(data, name) {

    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");

    name = '?' + name; //add ? on front of string for a simplified RegEx search

    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"), results = regex.exec(data);

    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

funBeginEvaluate = function (data) {
    mApp.blockPage();
};

function bindDeptPersonList() {
    setTimeout(function () {

        tblDeptPerson = $('#tblDeptPerson').DataTable({
            "serverSide": true,
            "processing": true,
            "orderMulti": false,
            "filter": true,
            "sAjaxSource": "/DCR/GetDeptPersonGrid",
            "iDisplayLength": 10,
            "ordering": true,
            "scrollX": true,
            "responsive": false,
            "language": {
                "infoFiltered": ""
            },
            //"scroller": {
            //    loadingIndicator: true
            //},
            //"bSort": true,
            //"aaSorting": [[1, 'desc']],
            "order": [[0, 'asc']],
            "columnDefs": [
                //{ "orderable": false, "targets": 4 },
                //{ "orderable": false, "targets": 5 },
                //{ "orderable": false, "targets": 6 },
                //{ "orderable": false, "targets": 7 },
                //{ "orderable": false, "targets": 8 }
            ],

            "fnServerData": function (sSource, data, fnCallback) {
                data.push({ name: "DCRId", value: $("#DCRId").val() });
                data.push({ name: "DCREvaluatId", value: $("#DCREvaluateModel_DCREvaluateId").val() });
                $.ajax({
                    "dataType": "json",
                    "type": "POST",
                    "url": sSource,
                    "data": data,
                    "success": fnCallback
                });

            },
            "aoColumns": [
                {
                    "sTitle": "Department", "className": "", render: function (data, type, row, meta) {
                        return row.DeptName;
                    }
                },
                {
                    "sTitle": "Person", "className": "", render: function (data, type, row, meta) {
                        return row.Person;
                    }
                },
                {
                    "sTitle": "Comments", "className": "text-wrap", render: function (data, type, row, meta) {
                        var str = '';
                        if (row.Remark != null) {
                            str = String(row.Remark).replace(/&lt;/g, '<').replace(/&gt;/g, '>');
                        }
                        return str;
                    }
                },
                {
                    "sTitle": "Commented By", "className": "", render: function (data, type, row, meta) {
                        return row.CreatedBy;
                    }
                },
                {
                    "sTitle": "Commented On", "className": "", render: function (data, type, row, meta) {
                        return row.CreatedOn;
                    }
                },
            ],
        });
    }, 1000);
}

//var edt1=CKEDITOR.replace("DCRReviewModel_Remark", {
//    skin: 'moono',
//    enterMode: CKEDITOR.ENTER_BR,
//    height: '100px',
//    shiftEnterMode: CKEDITOR.ENTER_P,
//    toolbar: [{
//        name: 'basicstyles',
//        groups: ['basicstyles'],
//        items: ['Bold', 'Italic', 'Underline', "-", 'TextColor', 'BGColor']
//    },
//    {
//        name: 'scripts',
//        items: ['Subscript', 'Superscript']
//    },
//{
//    name: 'editing',
//    items: ['Scayt']
//}
//    ],
//    removePlugins: 'elementspath',
//    resize_enabled: false
//});

//var edt2 = CKEDITOR.replace("DCREvaluateModel_Remark", {
//    skin: 'moono',
//    enterMode: CKEDITOR.ENTER_BR,
//    height: '100px',
//    shiftEnterMode: CKEDITOR.ENTER_P,
//    toolbar: [{
//        name: 'basicstyles',
//        groups: ['basicstyles'],
//        items: ['Bold', 'Italic', 'Underline', "-", 'TextColor', 'BGColor']
//    },
//    {
//        name: 'scripts',
//        items: ['Subscript', 'Superscript']
//    },
//{
//    name: 'editing',
//    items: ['Scayt']
//}
//    ],
//    removePlugins: 'elementspath',
//    resize_enabled: false
//});

//var edt3 = CKEDITOR.replace("DCREvaluateCommentModel_Remark", {
//    skin: 'moono',
//    enterMode: CKEDITOR.ENTER_BR,
//    height: '100px',
//    shiftEnterMode: CKEDITOR.ENTER_P,
//    toolbar: [{
//        name: 'basicstyles',
//        groups: ['basicstyles'],
//        items: ['Bold', 'Italic', 'Underline', "-", 'TextColor', 'BGColor']
//    },
//    {
//        name: 'scripts',
//        items: ['Subscript', 'Superscript']
//    },
//{
//    name: 'editing',
//    items: ['Scayt']
//}
//    ],
//    removePlugins: 'elementspath',
//    resize_enabled: false
//});

//var edt4 = CKEDITOR.replace("DCRReleaseModel_Remark", {
//    skin: 'moono',
//    enterMode: CKEDITOR.ENTER_BR,
//    height: '100px',
//    shiftEnterMode: CKEDITOR.ENTER_P,
//    toolbar: [{
//        name: 'basicstyles',
//        groups: ['basicstyles'],
//        items: ['Bold', 'Italic', 'Underline', "-", 'TextColor', 'BGColor']
//    },
//    {
//        name: 'scripts',
//        items: ['Subscript', 'Superscript']
//    },
//{
//    name: 'editing',
//    items: ['Scayt']
//}
//    ],
//    removePlugins: 'elementspath',
//    resize_enabled: false
//});

//var edt5 = CKEDITOR.replace("ReleasedRemark", {
//    skin: 'moono',
//    enterMode: CKEDITOR.ENTER_BR,
//    height: '100px',
//    shiftEnterMode: CKEDITOR.ENTER_P,
//    toolbar: [{
//        name: 'basicstyles',
//        groups: ['basicstyles'],
//        items: ['Bold', 'Italic', 'Underline', "-", 'TextColor', 'BGColor']
//    },
//    {
//        name: 'scripts',
//        items: ['Subscript', 'Superscript']
//    },
//{
//    name: 'editing',
//    items: ['Scayt']
//}
//    ],
//    removePlugins: 'elementspath',
//    resize_enabled: false
//});

var cklist = [];
$('.ckediterbox').each(function () {
    cklist.push(CKEDITOR.replace(this.id, {
        skin: 'moono',
        enterMode: CKEDITOR.ENTER_BR,
        height: '100px',
        shiftEnterMode: CKEDITOR.ENTER_P,
        toolbar: [{
            name: 'basicstyles',
            groups: ['basicstyles'],
            items: ['Bold', 'Italic', 'Underline', "-", 'TextColor', 'BGColor']
        },
        {
            name: 'scripts',
            items: ['Subscript', 'Superscript']
        },
        {
            name: 'editing',
            items: ['Scayt']
        }
        ],
        removePlugins: 'elementspath',
        resize_enabled: false
    }));
});

CKEDITOR.config.scayt_autoStartup = true;

function funReleasedStep() {
    var selectchbHeaderPart = $("#chbHeaderPart").prop("checked");
    if (selectchbHeaderPart) {
        $("#chbHeaderPart").prop("checked", false);
    }
    var selectchbHeaderDoc = $("#chbHeaderDoc").prop("checked");
    if (selectchbHeaderDoc) {
        $("#chbHeaderDoc").prop("checked", false);
    }

    var flag = true;
    var radioValue = $("input[name='ReleasedStepId']:checked").val();
    if (radioValue != '' && radioValue != undefined) {
        mApp.blockPage();

        if (radioValue == 3) {
            if (selectedDocIDs == '' && selectedPartIDs == '') {
                toastr.info("Please select at least one affected Doc and affected Part", "Error");
                flag = false;
                mApp.unblockPage();
            }
        }
        var DCRModel = {};
        DCRModel.DCRId = $("#DCRId").val();
        DCRModel.ReleasedStepId = radioValue;
        DCRModel.Page = 'InCorporate';
        DCRModel.ReleasedRemark = CKEDITOR.instances['ReleasedRemark'].getData();

        if (flag) {
            if ( radioValue == 3 || radioValue == 4) {

                var DCRDocumentModel = {};
                var DCRPartModel = {};

                var FinalDoclist = Array.from(new Set(selectedDocIDs));
                var FinalPartlist = Array.from(new Set(selectedPartIDs));
                var DocIDs = new Array();
                var PartIDs = new Array();

                for (var id in FinalDoclist) {
                    DocIDs.push({ JEPDocumentDetailsId: FinalDoclist[id] });
                }

                for (var id in FinalPartlist) {
                    PartIDs.push({ ItemId: FinalPartlist[id] });
                }

                DCRDocumentModel.DCRId = $("#DCRId").val();
                DCRDocumentModel.dCRDocumentList = DocIDs;

                DCRPartModel.DCRId = $("#DCRId").val();
                DCRPartModel.dCRPartList = PartIDs;

                var json1 = JSON.stringify({ DCRDocumentModel });
                var json2 = JSON.stringify({ DCRPartModel });
                var json3 = JSON.stringify({ DCRModel });
            }

            $.ajax({
                type: "POST",
                url: "/DES/DCR/ReleasedProcess",
                data: { 'DCRDocumentModel': DCRDocumentModel, 'DCRPartModel': DCRPartModel, 'DCRModel': DCRModel },
                datatype: "json",
                async: true,
                success: function (data) {
                    if (data.status) {
                        //for (var i = 0; i < data.result.length; i++) {
                        //    funRevisedDocData(data.result[i]);
                        //}
                        mApp.unblockPage();
                        toastr.success("DCR incorporated successfully", "Success");
                        setTimeout(function () {
                            window.location.href = "/DES/DCR";
                        }, 1000);
                    }
                    else {
                        window.location.href = "/DES/DCR";
                    }
                },
                error: function () {
                    mApp.unblockPage();
                    toastr.error("something went wrong try again later", "error");
                }
            });
        }
    }
    else {
        mApp.unblockPage();
        toastr.info("Please select any one option", "Info");
    }
}

function funRevisedDocData(data) {
    mApp.unblockPage();
    if (data.Status) {
        //$('#DocumentId').val(data.model.DocumentId);
        funSuccessDocID = data.DocumentId;
        funSuccessDocNo = data.DocumentNo;
        funSuccessRevision = data.DocumentRevision;
        funSuccessProject = data.Project;
        funSuccessIsRevision = data.IsRevision;
        var CureentLocationPath = data.FilePath;
        var CurrentLocationIp = data.CurrentLocationIp;
        var FCSurl = CurrentLocationIp + "/api/ReviseData";

        var DocumentRevision = data.DocumentRevision;
        var DocOldRevision = parseInt(DocumentRevision) - 1;
        //If Revision
        if (funSuccessIsRevision == true) {

            var linksToCopy = data.NewDocumentPathForRevison;
            concatDocandProject = CureentLocationPath + "/" + funSuccessProject.toString().trim() + "-" + funSuccessDocNo.toString().trim() + "-R" + DocumentRevision.toString().trim();
            oldPath = funSuccessProject.toString().trim() + "-" + funSuccessDocNo.toString().trim() + "-R" + DocOldRevision.toString().trim();

            var docMapp = {
                newDocPath: concatDocandProject,
                oldDocPath: linksToCopy
            };
            mApp.blockPage();
            jQuery.ajax({
                url: FCSurl, // Node Applictation Redrection
                data: JSON.stringify(docMapp),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                cache: false,
                async: true,
                processData: false,
                method: 'POST',
                type: 'POST', // For jQuery < 1.9
                success: function (response) {
                    mApp.unblockPage();

                },
            }).fail(function (data) {
                toastr.error("Server response not received", "Error");
                mApp.unblockPage();
            });

            //$('#IsRevision').val(false);
        }


        //getGrid(funSuccessDocID);
        //WizardDemo.Gotonext();
    } else {
        toastr.error(data.Msg, "Error");
        mApp.unblockPage();
    }

}

function funPartialPartDOC(obj) {
    $("#m_modal_4").modal("show");
    GetAllDocs();
    GetAllParts();
}

function refresh() {
    setTimeout(function () {
        tblAllParts.ajax.reload();
    }, 1000);
}

function GetAllDocs() {
    setTimeout(function () {
        tblAllDocs = $('#tblAllDocs').DataTable({
            "serverSide": true,
            "processing": true,
            "orderMulti": false,
            "filter": true,
            "sAjaxSource": "/DES/DCR/GetAffectedDCRDoc",
            "iDisplayLength": 10,
            "ordering": true,
            "scrollX": true,
            "language": {
                "infoFiltered": ""
            },
            "bRetrieve": true,
            "order": [[1, 'desc']],
            "columnDefs": [
                {
                    "orderable": false, "targets": 0,
                    'checkboxes': {
                        'selectRow': true
                    }
                }
            ],

            "fnServerData": function (sSource, data, fnCallback) {
                data.push({ name: "DCRId", value: $("#DCRId").val() });
                data.push({ name: "IsInCorporate", value: true });
                $.ajax({
                    "dataType": "json",
                    "type": "POST",
                    "url": sSource,
                    "data": data,
                    "success": fnCallback
                });
            },
            "aoColumns": [
                {
                    "sTitle": "<label class='m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--success'><input type='checkbox' id='chbHeaderDoc' onclick='SelectAllDoc()'></input><span></span></label>", "sDefaultContent": "<input type='checkbox' ></input>", "bSortable": false, "className": "action", "orderable": false, render: function (data, type, row, meta) {
                        var srt = '<label class="m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--success">'
                        var IDIndex = selectedDocIDs.indexOf(row.JEPDocumentDetailsId);
                        if (IDIndex !== -1) {
                            srt += '<input type="checkbox" class="m-checkable" id="IsSelect_' + row.JEPDocumentDetailsId + '"  data-id="' + row.JEPDocumentDetailsId + '" onclick="AddToDocList(this)" checked>';
                        }
                        else {
                            srt += '<input type="checkbox" class="m-checkable" id="IsSelect_' + row.JEPDocumentDetailsId + '"  data-id="' + row.JEPDocumentDetailsId + '" onclick="AddToDocList(this)">';
                        }
                        srt += "<span></span></label>";
                        return srt;
                    }
                },
                {
                    "sTitle": "Document No", "className": "", render: function (data, type, row, meta) {
                        return row.DocNo
                    }
                },
                {
                    "sTitle": "Document Description", "className": "", render: function (data, type, row, meta) {
                        return row.DocDescription
                    }
                },
                {
                    "sTitle": "Rev", "className": "", render: function (data, type, row, meta) {
                        return "R" + row.RevisionNo
                    }
                },
                {
                    "sTitle": "General Remarks", "className": "", render: function (data, type, row, meta) {
                        return row.GeneralRemarks
                    }
                }
            ],

            "initComplete": function () {

            },

            "fnRowCallback": function () {

            }
        });
    }, 1000);
}

function GetAllParts() {
    setTimeout(function () {
        tblAllParts = $('#tblAllParts').DataTable({
            "serverSide": true,
            "processing": true,
            "orderMulti": false,
            "filter": true,
            "sAjaxSource": "/DES/DCR/GetDCRAffectedPart",
            "iDisplayLength": 10,
            "ordering": true,
            "scrollX": true,
            "language": {
                "infoFiltered": ""
            },
            "bRetrieve": true,
            "order": [[1, 'desc']],
            "columnDefs": [
                {
                    "orderable": false, "targets": 0,
                    'checkboxes': {
                        'selectRow': true
                    }
                }
            ],

            "fnServerData": function (sSource, data, fnCallback) {
                data.push({ name: "DCRId", value: $("#DCRId").val() });
                data.push({ name: "IsInCorporate", value: true });
                $.ajax({
                    "dataType": "json",
                    "type": "POST",
                    "url": sSource,
                    "data": data,
                    "success": fnCallback
                });
            },
            "aoColumns": [
                {
                    "sTitle": "<label class='m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--success'><input type='checkbox' id='chbHeaderPart' onclick='SelectAllPart()'></input><span></span></label>", "sDefaultContent": "<input type='checkbox' ></input>", "bSortable": false, "className": "action", "orderable": false, render: function (data, type, row, meta) {
                        var srt = '<label class="m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--success">'
                        var IDIndex = selectedPartIDs.indexOf(row.ItemId);
                        if (IDIndex !== -1) {
                            srt += '<input type="checkbox" class="m-checkable" id="IsSelect_' + row.ItemId + '"  data-id="' + row.ItemId + '" onclick="AddToPartList(this)" checked>';
                        }
                        else {
                            srt += '<input type="checkbox" class="m-checkable" id="IsSelect_' + row.ItemId + '"  data-id="' + row.ItemId + '" onclick="AddToPartList(this)">';
                        }
                        srt += "<span></span></label>";
                        return srt;
                    }
                },
                {
                    "sTitle": "Type", "className": "", render: function (data, type, row, meta) {
                        //var name = "<strong style='text-transform: capitalize;'>" + row.DocumentNo + "</strong>";
                        return row.Type
                    },
                },
                {
                    "sTitle": "ItemKey", "className": "", render: function (data, type, row, meta) {
                        //var name = "<strong style='text-transform: capitalize;'>" + row.DocumentRevision + "</strong>";
                        return row.ItemKey
                    },
                },
                {
                    "sTitle": "Parent Item", "className": "", render: function (data, type, row, meta) {
                        return row.ItemName
                    }
                },
                {
                    "sTitle": "ItemName", "className": "", render: function (data, type, row, meta) {
                        return row.ItemName
                    }
                },
                {
                    "sTitle": "Rev", "className": "", render: function (data, type, row, meta) {
                        return "R" + row.RevNo
                    }
                },
                {
                    "sTitle": "DRG No", "className": "", render: function (data, type, row, meta) {
                        return row.DocumentNo
                    }
                },
                {
                    "sTitle": "Material", "className": "", render: function (data, type, row, meta) {
                        return row.Material
                    }
                },
                {
                    "sTitle": "ItemGroup", "className": "", render: function (data, type, row, meta) {
                        return row.ItemGroup
                    }
                },
            ],

            "initComplete": function () {

            },

            "fnRowCallback": function () {

            }
        });
    }, 1000);
}

function SelectAllDoc() {
    var data = tblAllDocs.rows().data()[0]["DocIDs"];
    var DocIdArray = data.split(',');
    var select = $("#chbHeaderDoc").prop("checked");
    if (select == true) {
        DocIdArray.forEach(function (item) {
            item = parseInt(item);
            $("#IsSelect_" + item).prop("checked", true);
            selectedDocIDs.push(item);
        });
    }
    else {
        DocIdArray.forEach(function (item) {
            item = parseInt(item);
            $("#IsSelect_" + item).prop("checked", false);
            var index = selectedDocIDs.indexOf(item);
            if (index > -1) {
                selectedDocIDs.splice(index, 1);
            }
        });
    }

    //var data = tblAllDocs.rows().data();
    //var select = $("#chbHeader").prop("checked");
    //if (select == true) {
    //    data.each(function (value, index) {
    //        $("#IsSelect_" + data[index].JEPDocumentDetailsId).prop("checked", true);
    //        selectedDocIDs.push(data[index].JEPDocumentDetailsId);
    //    });
    //}
    //else {
    //    data.each(function (value, index) {
    //        $("#IsSelect_" + data[index].JEPDocumentDetailsId).prop("checked", false);
    //        var index = selectedDocIDs.indexOf(data[index].JEPDocumentDetailsId);
    //        if (index > -1) {
    //            selectedDocIDs.splice(index, 1);
    //        }
    //    });
    //}
    //selectedDocIDs = Array.from(new Set(selectedDocIDs));
}

function AddToDocList($this) {
    ;
    var data;
    var allSelected;
    var id = $($this).attr("data-id");
    id = parseInt(id, 10);
    var select = $("#IsSelect_" + id).prop("checked");
    var tableID = $("#IsSelect_" + id).closest("table").attr("id");
    var chbID = $("#" + tableID + " th input[type='checkbox']").attr("id");
    data = tblAllDocs.rows().data();

    if (select == true) {
        selectedDocIDs.push(id);
        selectedDocIDs = Array.from(new Set(selectedDocIDs));
        data.each(function (value, index) {
            if (!selectedDocIDs.includes(data[index].JEPDocumentDetailsId)) {
                allSelected = false;
            }
        });
        if (allSelected != false) {
            $("#" + chbID).prop("checked", true);
        }

    }
    else {
        var tableID = $("#IsSelect_" + id).closest("table").attr("id");
        var chbID = $("#" + tableID + " th input[type='checkbox']").attr("id");
        $("#" + chbID).prop("checked", false);
        var index = selectedDocIDs.indexOf(id);
        if (index > -1) {
            selectedDocIDs.splice(index, 1);
        }
    }
    selectedDocIDs = Array.from(new Set(selectedDocIDs));
}

function SelectAllPart() {
    var data = tblAllParts.rows().data()[0]["ItemIds"];
    var ItemIdArray = data.split(',');
    var select = $("#chbHeaderPart").prop("checked");
    if (select == true) {
        ItemIdArray.forEach(function (item) {
            item = parseInt(item);
            $("#IsSelect_" + item).prop("checked", true);
            selectedPartIDs.push(item);
        });
    }
    else {
        ItemIdArray.forEach(function (item) {
            item = parseInt(item);
            $("#IsSelect_" + item).prop("checked", false);
            var index = selectedPartIDs.indexOf(item);
            if (index > -1) {
                selectedPartIDs.splice(index, 1);
            }
        });
    }

    //var data = tblAllParts.rows().data();
    //var select = $("#chbHeader").prop("checked");
    //if (select == true) {
    //    data.each(function (value, index) {
    //        $("#IsSelect_" + data[index].ItemId).prop("checked", true);
    //        selectedPartIDs.push(data[index].ItemId);
    //    });
    //}
    //else {
    //    data.each(function (value, index) {
    //        $("#IsSelect_" + data[index].ItemId).prop("checked", false);
    //        var index = selectedPartIDs.indexOf(data[index].ItemId);
    //        if (index > -1) {
    //            selectedPartIDs.splice(index, 1);
    //        }
    //    });
    //}
    //selectedPartIDs = Array.from(new Set(selectedPartIDs));
}

function AddToPartList($this) {
    ;
    var data;
    var allSelected;
    var id = $($this).attr("data-id");
    id = parseInt(id, 10);
    var select = $("#IsSelect_" + id).prop("checked");
    var tableID = $("#IsSelect_" + id).closest("table").attr("id");
    var chbID = $("#" + tableID + " th input[type='checkbox']").attr("id");
    data = tblAllParts.rows().data();

    if (select == true) {
        selectedPartIDs.push(id);
        selectedPartIDs = Array.from(new Set(selectedPartIDs));
        data.each(function (value, index) {
            if (!selectedPartIDs.includes(data[index].ItemId)) {
                allSelected = false;
            }
        });
        if (allSelected != false) {
            $("#" + chbID).prop("checked", true);
        }

    }
    else {
        var tableID = $("#IsSelect_" + id).closest("table").attr("id");
        var chbID = $("#" + tableID + " th input[type='checkbox']").attr("id");
        $("#" + chbID).prop("checked", false);
        var index = selectedPartIDs.indexOf(id);
        if (index > -1) {
            selectedPartIDs.splice(index, 1);
        }
    }
    selectedPartIDs = Array.from(new Set(selectedPartIDs));
}

$("#btnAddDocPartList").on("click", function () {
    mApp.blockPage();
    //if (selectedDocIDs.length > 0 && selectedPartIDs.length > 0) {


    //    //var model = {};
    //    //model.DCRId = $("#DCRId").val();
    //    //model.dCRDocumentList = DocIDs;

    //    //$.ajax({
    //    //    type: "POST",
    //    //    url: '/DES/DCR/AddDCRdoc',
    //    //    data: JSON.stringify({ model: model }),
    //    //    dataType: 'json',
    //    //    contentType: "application/json;charset=utf-8",
    //    //    async: true,
    //    //    success: function (response) {
    //    //        mApp.unblockPage();
    //    //        if (response.Status) {
    //    //            toastr.success(response.Msg, "Success");
    //    //            tblGlobalJepDoc.ajax.reload();
    //    //            tblJepDoc.ajax.reload();
    //    //            tblDocumentList.ajax.reload();
    //    //            selectedDocIDs = [];
    //    //        } else {
    //    //            toastr.error(response.Msg, "Error");
    //    //        }
    //    //    },
    //    //    error: function (error) {
    //    //        mApp.unblockPage();
    //    //        toastr.error("something went wrong try again later", "Error");
    //    //    }
    //    //})
    //}
    //else {
    //    mApp.unblockPage();
    //    toastr.info("Please select atleast one document and part!", "Info");
    //    tblDocumentList.ajax.reload();
    //}
    mApp.unblockPage();
});

function funOpenHistory(obj) {
    mApp.blockPage();
    $("#m_modal_5").modal("show");
    AllHistory();
    mApp.unblockPage();
}

function funDCRReport() {
    mApp.blockPage();

    var dat = [];
    dat.push({ Param: "DCRNo", Value: $('#DCRNo').val() });
    ShowReport(dat, "/DES/DCR Report", false, true, 'pdf', $('#DCRNo').val() + '_DCR Report');
    mApp.unblockPage();
}


function AllHistory() {
    if (tblDCRHistory == null) {


        tblDCRHistory = $('#tblDCRHistory').DataTable({
            "serverSide": true,
            "processing": true,
            "orderMulti": false,
            "filter": true,
            "sAjaxSource": "/DES/DCR/GetAllHistory",
            "iDisplayLength": 10,
            "ordering": true,
            "scrollX": true,
            "responsive": false,
            "language": {
                "infoFiltered": ""
            },
            "order": [[2, 'desc']],
            "columnDefs": [
                //{ "orderable": false, "targets": 1 },
                //{ "orderable": false, "targets": 2 }
            ],

            "fnServerData": function (sSource, data, fnCallback) {
                data.push({ name: "DCRId", value: $("#DCRId").val() });
                $.ajax({
                    "dataType": "json",
                    "type": "POST",
                    "url": sSource,
                    "data": data,
                    "success": fnCallback
                });

            },
            "aoColumns": [

                {
                    "sTitle": "History", render: function (data, type, row, meta) {
                        return row.Description
                    }
                },

                {
                    "sTitle": "Created By", "orderable": false, render: function (data, type, row, meta) {
                        return row.CreatedBy
                    }
                },
                {
                    "sTitle": "Created On", render: function (data, type, row, meta) {
                        return row.CreatedOn
                    }
                },
            ],

            "initComplete": function () {

            },

            "fnRowCallback": function () {
                setTimeout(function () {
                    tblDCRHistory.columns.adjust();
                }, 1000);
            }
        });
    }
}
var fnForm1Begin = function () {
    swal({
        title: "Do you want to ",//+ btnName + "?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            if (isConfirm.value) {
                return true
            }
            else {
                return false;
            }
        });
}

function commonSwalMsg(btnName, obj, formid) {
    swal({
        title: "Do you want to " + btnName + "?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            if (isConfirm.value) {
                var flag = funBegin(obj);
                if (flag) {
                    $("#button").val(btnName);
                    //$("#form" + formid).submit();
                    //alert (obj.form.Id)
                    //edt1.updateElement();
                    //edt2.updateElement()
                    //edt3.updateElement()
                    //edt4.updateElement()
                    //edt5.updateElement()
                    for (var i = 0; i < cklist.length; i++) {
                        cklist[i].updateElement();
                    }
                    $(obj.form).submit();
                }
            }
        });
}


$('#tblDocumentList').on('click', 'td .btnUV', function () {
    var tr = $(this).closest('tr'); 0
    var row = tblDocumentList.row(tr);
    var data = row.data();
    var DocPathList = data.MainDocumentPath.split(",");
    $.each(DocPathList, function (i) {
        UniverSalFile(DocPathList[i]);
    });
});

$('#tblDocumentFiles').on('click', 'td .btnUV', function () {
    var tr = $(this).closest('tr'); 0
    var row = tblDocumentFiles.row(tr);
    var data = row.data();
    var DocPathList = data.DocumentPath.split(",");
    $.each(DocPathList, function (i) {
        UniverSalFile(DocPathList[i]);
    });
});

$('#tblAffectedPart').on('click', 'td .btnUV', function () {
    var tr = $(this).closest('tr'); 0
    var row = tblAffectedPart.row(tr);
    var data = row.data();
    var DocPathList = data.MainDocumentPath.split(",");
    $.each(DocPathList, function (i) {
        UniverSalFile(DocPathList[i]);
    });
});

function getGridtblSupportDocumentFiles() {
    mApp.blockPage();
    tblSupportDocumentFiles = $('#tblSupportDocumentFiles').DataTable({
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "sAjaxSource": "/DES/DCR/GetDCRSupportedDocumentList",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        "language": {
            "infoFiltered": ""
        },
        "bRetrieve": true,
        "order": [],
        "columnDefs": [
            { "orderable": false, "targets": 0 },
            {
                "targets": [2],
                "visible": false
            }
        ],

        "fnServerData": function (sSource, data, fnCallback) {
            data.push({ name: "DCRId", value: $('#DCRId').val() });
            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });
        },
        "aoColumns": [
            {
                "sTitle": "Action", "className": "action", "orderable": false, render: function (data, type, row, meta) {
                    var srt = "";
                    srt += '<a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-focus m-btn--icon m-btn--icon-only m-btn--pill btnUV" title="Universal Viewer"> <i class="la la-desktop m--font-focus"></i></a>';
                    srt += '<a href="#"  class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill btnDownload" title="Download"><i class="fa fa-download m--font-brand"></i></a>';
                    srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnDeleteDFM" title="Delete"><i class="la la-trash text-danger"></i></a>';
                    return srt;
                }
            },
            {
                "sTitle": "Document Title", "className": "", render: function (data, type, row, meta) {
                    return row.DocumentTitle
                }
            },
            {
                "sTitle": "Document Format", "className": "", render: function (data, type, row, meta) {
                    return row.DocumentFormat
                }
            }
        ],

        "initComplete": function () {

        },

        "fnRowCallback": function () {

        }
    });
    mApp.unblockPage();
}

var path;
$('#tblSupportDocumentFiles').on('click', 'td .btnDownload', function () {
    var tr = $(this).closest('tr');
    var row = tblSupportDocumentFiles.row(tr);
    var data = row.data();
    var CurrentLocationIp = $('#CurrentLocationIp').val();
    downloadFile(data.DocumentPath, CurrentLocationIp);
});

$('#tblDocumentFiles').on('click', 'td .btnDownload', function () {
    var tr = $(this).closest('tr');
    var row = tblDocumentFiles.row(tr);
    var data = row.data();
    var CurrentLocationIp = $('#CurrentLocationIp').val();
    downloadFile(data.DocumentPath, CurrentLocationIp);
});

$('#tblDocumentList').on('click', 'td .btnDownload', function () {
    var tr = $(this).closest('tr');
    var row = tblDocumentList.row(tr);
    var data = row.data();
    var CurrentLocationIp = $('#CurrentLocationIp').val();
    downloadFile(data.MainDocumentPath, CurrentLocationIp);
});

$('#tblSupportDocumentFiles').on('click', 'td .btnDeleteDFM', function () {
    var tr = $(this).closest('tr');
    var row = tblSupportDocumentFiles.row(tr);
    var data = row.data();
    var CurrentLocationIp = $('#CurrentLocationIp').val();
    var FCSurl = CurrentLocationIp + "/api/DeleteDoc";
    var documentName = data.DocumentTitle;

    swal({
        title: "Do you want to delete?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            var rev = "Yes";
            if (isConfirm.value) {
                mApp.blockPage();
                var docdelete = {
                    deletePath: data.DocumentPath
                };

                $.ajax({
                    url: FCSurl,
                    data: JSON.stringify(docdelete),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    cache: false,
                    async: true,
                    processData: false,
                    method: 'POST',
                    type: 'POST', // For jQuery < 1.9
                    success: function (response) {
                        if (response) {
                            //toastr.success(response.Msg, "Successfully removed file from server");

                            $.ajax({
                                type: "GET",
                                url: '/DES/DCR/DeleteDocFile',
                                data: { Id: data.DCRSupportedDocId },
                                dataType: 'json',
                                contentType: "application/json;charset=utf-8",
                                async: true,
                                success: function (response) {
                                    toastr.success(documentName + response.Msg, "Success");
                                    tblSupportDocumentFiles.ajax.reload();
                                    mApp.unblockPage();
                                },
                                error: function (error) {
                                    toastr.error("Error while removing file from grid", "Error");
                                    mApp.unblockPage();
                                }
                            })
                        }
                    },
                    error: function (error) {
                        toastr.error("Error while removing file from server", "Error");
                        mApp.unblockPage();
                    }
                })
            } else {
                mApp.unblockPage();
            }
        });
});

$('#tblSupportDocumentFiles').on('click', 'td .btnUV', function () {
    var tr = $(this).closest('tr'); 0
    var row = tblSupportDocumentFiles.row(tr);
    var data = row.data();
    var DocPathList = data.DocumentPath.split(",");
    $.each(DocPathList, function (i) {
        UniverSalFile(DocPathList[i]);
    });
});
var iii = 1;
var myDropzone = new Dropzone('div#m-dropzone-two', {
    timeout: 18000000,
    parallelUploads: 10000,
    maxFilesize: 200000,
    maxFiles: 10,
    init: function () {
        this.on("maxfilesexceeded", function (file) {

            if (this.files.length == 11) {
                // this.removeFile(this.files[0]);
                if (iii == 1) {
                    alert("10 Files can be uploded at once!");
                }
                iii++;
            }
            myDropzone.removeFile(file);
            myDropzone.removeAllFiles();
        });

    },
    queuecomplete: function () {
        iii = 1;
    }
});

myDropzone.on('sending', function (file, xhr, formData, progress, bytesSent) {
    $('#dropzone').text("");
    $('.dz-error-message').css('display', 'none');
    $('.dz-success-mark').css('display', 'block');
    $('.dz-error-mark').css('display', 'none');

    var regex = /\@|\#|\+|\$|\*|\[|\{|\]|\}|\||\\|\<|\,|\>|\?|\/|\"/;;
    var fileName = regex.test(file.name);


    if (fileName) {
        toastr.error(file.name + " Contains special characters", "Error");
        myDropzone.removeFile(file);
    }
    else {
        var dotNameSplit = file.name.split('.');
        if (dotNameSplit.length > 2) {
            toastr.error(file.name + " Contains special characters", "Error");
            myDropzone.removeFile(file);
        }
        else {
            if ($('#CurrentLocationIp').val() != "") {
                // if ($('#CurrentLocationIp').val() != "") {
                var CurrentLocationIp = $('#CurrentLocationIp').val();
                var CurrentLocationPath = $('#FilePath').val();
                var FCSurl = CurrentLocationIp + "/api/Upload";
                var DocumentNo = file.name;
                var DCRNo = $('#DCRNo').val();
                var DocumentPath = DCRNo.toString().trim() + "-" + "DCRSupportDocument";

                formData.append('SavedPath', CurrentLocationPath.toString());
                formData.append('DocumentPath', DocumentPath.toString());
                formData.append('DocumentFile', file);
                var formate = file.name.substr(file.name.lastIndexOf('.') + 1);
                //Need To get Status and other filed HERE 
                mApp.blockPage();

                window.onbeforeunload = function () {
                    var prevent_leave = true;
                    if (prevent_leave) {
                        return "Your files are not completely uploaded yet...";
                    }
                }

                jQuery.ajax({
                    url: FCSurl,
                    data: formData,
                    enctype: 'multipart/form-data',
                    cache: false,
                    async: true,
                    contentType: false,
                    processData: false,
                    method: 'POST',
                    type: 'POST', // For jQuery < 1.9
                    success: function (response) {
                        mApp.unblockPage();
                        var fname = response.SavedFileName;
                        var path = response.SavedPath + "/" + fname;

                        $.ajax({
                            type: "POST",
                            url: '/DES/DCR/UploadDCRSupportDocs', //If get File Then Document Mapping Add
                            data: JSON.stringify({ DCRId: $("#DCRId").val(), DocumentTitle: DocumentNo, DocumentFormat: formate, DocumentPath: path }),
                            contentType: "application/json;charset=utf-8",
                            dataType: "json",
                            aysnc: true,
                            success: function (response) {
                                if (response.Status) {
                                    toastr.success(response.Msg, "Success");
                                    // getGrid(response.model.DCRId);
                                    //DCRId = response.model.DCRId;
                                    tblSupportDocumentFiles.ajax.reload();
                                    myDropzone.removeFile(file);
                                    mApp.unblockPage();
                                } else {
                                    toastr.error(response.Msg, "Error");
                                    myDropzone.removeFile(file);
                                    mApp.unblockPage();
                                }
                            },
                            error: function (error) {
                                toastr.error("something went wrong try again later", "Error");
                                myDropzone.removeFile(file);
                            }
                        });
                        window.onbeforeunload = function () {
                            var prevent_leave = false;
                            if (prevent_leave) {
                                return "Your files are not completely uploaded...";
                            }
                        }
                    },
                }).fail(function (data) {
                    mApp.unblockPage();
                    toastr.error("Server response not received", "Error");
                    myDropzone.removeFile(file);
                    mApp.unblockPage();
                    window.onbeforeunload = function () {
                        var prevent_leave = false;
                        if (prevent_leave) {
                            return "Your files are not completely uploaded...";
                        }
                    }
                });
            }
            else {
                toastr.error("Ip is not detecting", "Error");
                myDropzone.removeFile(file);
                mApp.unblockPage();
            }
        }

    }
    // }
    //else {
    //    toastr.error("Ip is not detecting", "Error");
    //    myDropzone.removeFile(file);
    //}
});

function BindJEPDoc() {
    tblJepDoc = $('#tblJepDoc').DataTable({
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "sAjaxSource": "/DES/DCR/GetJEPDocList",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        "responsive": true,
        "language": {
            "infoFiltered": ""
        },
        "bRetrieve": true,
        //"scroller": {
        //    loadingIndicator: true
        //},
        //"bSort": true,
        //"aaSorting": [[1, 'desc']],
        "order": [[1, 'desc']],
        'columnDefs': [
            {
                'orderable': false,
                'targets': 0,
                'checkboxes': {
                    'selectRow': true
                }
            }],
        'select': {
            'style': 'multi'
        },
        "fnServerData": function (sSource, data, fnCallback) {
            data.push({ name: "DCRId", value: $("#DCRId").val() });
            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });
        },
        "aoColumns": [
            {
                "sTitle": "<label class='m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--success'><input type='checkbox' id='chbHeaderJEPDoc' onclick='SelectAllJEPDoc()'></input><span></span></label>", "sDefaultContent": "<input type='checkbox' ></input>", "bSortable": false, "className": "action", "orderable": false, render: function (data, type, row, meta) {
                    var srt = '<label class="m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--success">'
                    var IDIndex = selectedDocIDs.indexOf(row.JEPDocumentDetailsId);
                    if (IDIndex !== -1) {
                        srt += '<input type="checkbox" class="m-checkable" id="IsSelect_' + row.JEPDocumentDetailsId + '"  data-id="' + row.JEPDocumentDetailsId + '" onclick="AddToDocList(this)" checked>';
                    }
                    else {
                        srt += '<input type="checkbox" class="m-checkable" id="IsSelect_' + row.JEPDocumentDetailsId + '"  data-id="' + row.JEPDocumentDetailsId + '" onclick="AddToDocList(this)">';
                    }
                    srt += "<span></span></label>";
                    return srt;
                }
            },
            {
                "sTitle": "Doc No", "className": "", render: function (data, type, row, meta) {
                    //var name = "<strong style='text-transform: capitalize;'>" + row.DocumentNo + "</strong>";
                    return row.DocumentNo
                },
            },
            {
                "sTitle": "Rev", "className": "", render: function (data, type, row, meta) {
                    //var name = "<strong style='text-transform: capitalize;'>" + row.DocumentRevision + "</strong>";
                    var srt = '';
                    if (row.DocumentRevision != null || row.DocumentRevision != '') {
                        srt = 'R' + row.DocumentRevision;
                    }
                    return srt;
                },
            },
            {
                "sTitle": "Description", "className": "", render: function (data, type, row, meta) {
                    return row.DocDescription
                }
            },
            {
                "sTitle": "Planned Submit Date", "className": "", render: function (data, type, row, meta) {
                    return row.PlannedSubmitDate
                }
            }
        ],

        "initComplete": function () {
            setTimeout(function () {
                tblJepDoc.columns.adjust();
            }, 500);
        },

        "fnRowCallback": function () {
            setTimeout(function () {
                tblJepDoc.columns.adjust();
            }, 500);
        }
    });
}

function BindGlobalJEPDoc() {
    setTimeout(function () {
        tblGlobalJepDoc = $('#tblGlobalJepDoc').DataTable({
            "searching": false,
            "serverSide": true,
            "processing": true,
            "orderMulti": false,
            "filter": true,
            "sAjaxSource": "/DES/DCR/GetJEPGlobalDocList",
            "iDisplayLength": 10,
            "ordering": true,
            "scrollX": true,
            "responsive": false,
            "language": {
                "infoFiltered": ""
            },
            "bRetrieve": true,
            //"scroller": {
            //    loadingIndicator: true
            //},
            //"bSort": true,
            //"aaSorting": [[1, 'desc']],
            "order": [[1, 'desc']],
            "columnDefs": [
                { "orderable": false, "targets": 0 }
            ],

            "fnServerData": function (sSource, data, fnCallback) {
                data.push({ name: "DocumentNo", value: $("#txtDocumentNo").val() });
                data.push({ name: "Project", value: $("#ContractProjectList").val() });
                data.push({ name: "DCRId", value: $("#DCRId").val() });
                $.ajax({
                    "dataType": "json",
                    "type": "POST",
                    "url": sSource,
                    "data": data,
                    "success": fnCallback
                });
            },
            "aoColumns": [
                {
                    "sTitle": "<label class='m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--success'><input type='checkbox' id='chbHeaderGlobalDoc' onclick='SelectAllGlobalJEPDoc()'></input><span></span></label>", "sDefaultContent": "<input type='checkbox' ></input>", "bSortable": false, "className": "action", "orderable": false, render: function (data, type, row, meta) {
                        var srt = '<label class="m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--success">'
                        var IDIndex = selectedDocIDs.indexOf(row.JEPDocumentDetailsId);
                        if (IDIndex !== -1) {
                            srt += '<input type="checkbox" class="m-checkable" id="IsSelect_' + row.JEPDocumentDetailsId + '"  data-id="' + row.JEPDocumentDetailsId + '" onclick="AddToDocList(this)" checked>';
                        }
                        else {
                            srt += '<input type="checkbox" class="m-checkable" id="IsSelect_' + row.JEPDocumentDetailsId + '"  data-id="' + row.JEPDocumentDetailsId + '" onclick="AddToDocList(this)">';
                        }
                        srt += "<span></span></label>";
                        return srt;
                    }
                },
                {
                    "sTitle": "Doc No", "className": "", render: function (data, type, row, meta) {
                        //var name = "<strong style='text-transform: capitalize;'>" + row.DocumentNo + "</strong>";
                        return row.DocumentNo
                    },
                },
                {
                    "sTitle": "Rev", "className": "", render: function (data, type, row, meta) {
                        //var name = "<strong style='text-transform: capitalize;'>" + row.DocumentRevision + "</strong>";
                        var srt = '';
                        if (row.DocumentRevision != null || row.DocumentRevision != '') {
                            srt = 'R' + row.DocumentRevision;
                        }
                        return srt;
                    },
                },
                {
                    "sTitle": "Description", "className": "", render: function (data, type, row, meta) {
                        return row.DocDescription
                    }
                }
            ],

            "initComplete": function () {
                setTimeout(function () {
                    tblGlobalJepDoc.columns.adjust();
                }, 500);
            },

            "fnRowCallback": function () {
                setTimeout(function () {
                    tblGlobalJepDoc.columns.adjust();
                }, 500);
            }
        });
    }, 1000);
}

function SelectAllJEPDoc() {
    var data = tblJepDoc.rows().data()[0]["DocIDs"];
    var DocIdArray = data.split(',');
    var select = $("#chbHeaderJEPDoc").prop("checked");
    if (select == true) {
        DocIdArray.forEach(function (item) {
            item = parseInt(item);
            $("#IsSelect_" + item).prop("checked", true);
            selectedDocIDs.push(item);
        });
    }
    else {
        DocIdArray.forEach(function (item) {
            item = parseInt(item);
            $("#IsSelect_" + item).prop("checked", false);
            var index = selectedDocIDs.indexOf(item);
            if (index > -1) {
                selectedDocIDs.splice(index, 1);
            }
        });
    }
}

function SelectAllGlobalJEPDoc() {
    var data = tblGlobalJepDoc.rows().data()[0]["DocIDs"];
    var DocIdArray = data.split(',');
    var select = $("#chbHeaderGlobalDoc").prop("checked");
    if (select == true) {
        DocIdArray.forEach(function (item) {
            item = parseInt(item);
            $("#IsSelect_" + item).prop("checked", true);
            selectedDocIDs.push(item);
        });
    }
    else {
        DocIdArray.forEach(function (item) {
            item = parseInt(item);
            $("#IsSelect_" + item).prop("checked", false);
            var index = selectedDocIDs.indexOf(item);
            if (index > -1) {
                selectedDocIDs.splice(index, 1);
            }
        });
    }
}

function AddToDocList($this) {
    var data;
    var allSelected;
    var id = $($this).attr("data-id");
    id = parseInt(id, 10);
    var select = $("#IsSelect_" + id).prop("checked");
    var tableID = $("#IsSelect_" + id).closest("table").attr("id");
    var chbID = $("#" + tableID + " th input[type='checkbox']").attr("id");
    if (tableID == "tblJepDoc") {
        data = tblJepDoc.rows().data();
    }
    else {
        data = tblGlobalJepDoc.rows().data();
    }
    if (select == true) {
        selectedDocIDs.push(id);
        selectedDocIDs = Array.from(new Set(selectedDocIDs));
        data.each(function (value, index) {
            if (!selectedDocIDs.includes(data[index].JEPDocumentDetailsId)) {
                allSelected = false;
            }
        });
        if (allSelected != false) {
            $("#" + chbID).prop("checked", true);
        }

    }
    else {
        var tableID = $("#IsSelect_" + id).closest("table").attr("id");
        var chbID = $("#" + tableID + " th input[type='checkbox']").attr("id");
        $("#" + chbID).prop("checked", false);
        var index = selectedDocIDs.indexOf(id);
        if (index > -1) {
            selectedDocIDs.splice(index, 1);
        }
    }
    selectedDocIDs = Array.from(new Set(selectedDocIDs));
}



$("#SearchDoc").click(function (e) {
    tblJepDoc.ajax.reload();
    tblGlobalJepDoc.ajax.reload();

    var selectchbHeaderJEPDoc = $("#chbHeaderJEPDoc").prop("checked");
    if (selectchbHeaderJEPDoc) {
        $("#chbHeaderJEPDoc").prop("checked", false);
    }

    var selectchbHeaderGlobalDoc = $("#chbHeaderGlobalDoc").prop("checked");
    if (selectchbHeaderGlobalDoc) {
        $("#chbHeaderGlobalDoc").prop("checked", false);
    }

    selectedDocIDs = [];
    FinalDoclist = [];
    $("#m_modal_3").modal("show");
    setTimeout(function () {
        tblJepDoc.ajax.reload();
    }, 500);
});

function funsearchGlobalJEPDoc() {
    tblGlobalJepDoc.ajax.reload();
    setTimeout(function () {
        tblGlobalJepDoc.columns.adjust();
    }, 500);
}

$("a[href='#m_tabs_3_1_1_1']").click(function () {
    tblJepDoc.ajax.reload();
});

$("a[href='#m_tabs_3_2_2_2']").click(function () {
    tblGlobalJepDoc.ajax.reload();
});

$('#tblDocumentList').on('click', 'td .btnDelete', function () {
    var tr = $(this).closest('tr');
    var row = tblDocumentList.row(tr);
    var data = row.data();
    swal({
        title: "Do you want to delete..?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            if (isConfirm.value) {
                mApp.blockPage();
                $.ajax({
                    type: "GET",
                    url: '/DES/DCR/DeleteAffectedDocument',
                    data: { Id: data.DCRAffectedDocumentId },
                    dataType: 'json',
                    contentType: "application/json;charset=utf-8",
                    async: true,
                    success: function (response) {
                        mApp.unblockPage();
                        if (response.Status) {
                            toastr.success(response.Msg, "Success");
                            tblDocumentList.ajax.reload();
                        } else {
                            toastr.error(response.Msg, "Error");
                        }
                    },
                    error: function (error) {
                        mApp.unblockPage();
                        toastr.error("something went wrong try again later", "Error");
                    }
                });
            }
        });
});


function BindPART() {
    tblDCRPart = $('#tblDCRPart').DataTable({
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "sAjaxSource": "/DES/DCR/GetPartList",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        "responsive": false,
        "language": {
            "infoFiltered": ""
        },
        "bRetrieve": true,
        "order": [[1, 'desc']],
        'columnDefs': [
            {
                'orderable': false,
                'targets': 0,
                'checkboxes': {
                    'selectRow': true
                }
            }],
        'select': {
            'style': 'multi'
        },
        "fnServerData": function (sSource, data, fnCallback) {
            data.push({ name: "DCRId", value: $('#DCRId').val() });
            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });
        },
        "aoColumns": [
            {
                "sTitle": "<label class='m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--success'><input type='checkbox' id='chbHeaderPart' onclick='SelectAllPart()'></input><span></span></label>", "sDefaultContent": "<input type='checkbox' ></input>", "bSortable": false, "className": "action", "orderable": false, render: function (data, type, row, meta) {
                    var srt = '<label class="m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--success">'
                    var IDIndex = selectedPartIDs.indexOf(row.ItemId);
                    if (IDIndex !== -1) {
                        srt += '<input type="checkbox" class="m-checkable" id="IsSelect_' + row.ItemId + '"  data-id="' + row.ItemId + '" onclick="AddToPartList(this)" checked>';
                    }
                    else {
                        srt += '<input type="checkbox" class="m-checkable" id="IsSelect_' + row.ItemId + '"  data-id="' + row.ItemId + '" onclick="AddToPartList(this)">';
                    }
                    srt += "<span></span></label>";
                    return srt;
                }
            },
            {
                "sTitle": "Type", "className": "", render: function (data, type, row, meta) {
                    //var name = "<strong style='text-transform: capitalize;'>" + row.DocumentNo + "</strong>";
                    return row.Type
                },
            },
            {
                "sTitle": "ItemKey", "className": "", render: function (data, type, row, meta) {
                    //var name = "<strong style='text-transform: capitalize;'>" + row.DocumentRevision + "</strong>";
                    return row.ItemKey
                },
            },
            {
                "sTitle": "Parent Item", "className": "", render: function (data, type, row, meta) {
                    return row.ParentItem
                }
            },
            {
                "sTitle": "ItemName", "className": "", render: function (data, type, row, meta) {
                    return row.ItemName
                }
            },
            {
                "sTitle": "Rev", "className": "", render: function (data, type, row, meta) {
                    return "R" + row.RevNo
                }
            },
            {
                "sTitle": "DRG No", "className": "", render: function (data, type, row, meta) {
                    return row.DocumentNo
                }
            },
            {
                "sTitle": "Material", "className": "", render: function (data, type, row, meta) {
                    return row.Material
                }
            },
            {
                "sTitle": "ItemGroup", "className": "", render: function (data, type, row, meta) {
                    return row.ItemGroup
                }
            },
        ],

        "initComplete": function () {
            setTimeout(function () {
                tblDCRPart.columns.adjust();
            }, 500);
        },

        "fnRowCallback": function () {
            setTimeout(function () {
                tblDCRPart.columns.adjust();
            }, 500);
        }
    });
}

function SelectAllPart() {
    var data = tblDCRPart.rows().data()[0]["ItemIds"];
    var ItemIdArray = data.split(',');
    var select = $("#chbHeaderPart").prop("checked");
    if (select == true) {
        ItemIdArray.forEach(function (item) {
            item = parseInt(item);
            $("#IsSelect_" + item).prop("checked", true);
            selectedPartIDs.push(item);
        });
    }
    else {
        ItemIdArray.forEach(function (item) {
            item = parseInt(item);
            $("#IsSelect_" + item).prop("checked", false);
            var index = selectedPartIDs.indexOf(item);
            if (index > -1) {
                selectedPartIDs.splice(index, 1);
            }
        });
    }
}

function AddToPartList($this) {
    var data;
    var allSelected;
    var id = $($this).attr("data-id");
    id = parseInt(id, 10);
    var select = $("#IsSelect_" + id).prop("checked");
    var tableID = $("#IsSelect_" + id).closest("table").attr("id");
    var chbID = $("#" + tableID + " th input[type='checkbox']").attr("id");
    data = tblDCRPart.rows().data();
    if (select == true) {
        selectedPartIDs.push(id);
        selectedPartIDs = Array.from(new Set(selectedPartIDs));
        data.each(function (value, index) {
            if (!selectedPartIDs.includes(data[index].ItemId)) {
                allSelected = false;
            }
        });
        if (allSelected != false) {
            $("#" + chbID).prop("checked", true);
        }
    }
    else {
        var tableID = $("#IsSelect_" + id).closest("table").attr("id");
        var chbID = $("#" + tableID + " th input[type='checkbox']").attr("id");
        $("#" + chbID).prop("checked", false);
        var index = selectedPartIDs.indexOf(id);
        if (index > -1) {
            selectedPartIDs.splice(index, 1);
        }
    }
    selectedPartIDs = Array.from(new Set(selectedPartIDs));
}

//function getaffecteddcrpart() {
//    settimeout(function () {
//        tblaffectedpart = $('#tblaffectedpart').datatable({
//            "serverside": true,
//            "processing": true,
//            "ordermulti": false,
//            "filter": true,
//            "sajaxsource": "/des/dcr/getdcraffectedpart",
//            "idisplaylength": 10,
//            "ordering": true,
//            "scrollx": true,
//            "language": {
//                "infofiltered": ""
//            },
//            "bretrieve": true,
//            "order": [[1, 'desc']],
//            "columndefs": [
//                { "orderable": false, "targets": 0 }
//            ],

//            "fnserverdata": function (ssource, data, fncallback) {
//                data.push({ name: "dcrid", value: $("#dcrid").val() });
//                data.push({ name: "isincorporate", value: false });
//                $.ajax({
//                    "datatype": "json",
//                    "type": "post",
//                    "url": ssource,
//                    "data": data,
//                    "success": fncallback
//                });
//            },
//            "aocolumns": [
//                {
//                    "stitle": "action", "classname": "action", "orderable": false, render: function (data, type, row, meta) {
//                        var srt = "";
//                        if (row.documentno != null) {
//                            srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-focus m-btn--icon m-btn--icon-only m-btn--pill btnuv" title="universal viewer"><i class="la la-desktop m--font-focus"></i></a>';
//                        }
//                        if (($('#responsibledesignengineer').val() == $('#currentuser').val()) && ($('#policystepforview').val() == "submit") ||
//                            ($('#responsibleseniordesignengineer').val() == $('#currentuser').val()) && ($('#policystepforview').val() == "evaluate")) {
//                            srt += '<a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btndelete" title="delete"><i class="la la-trash text-danger"></i></a>';
//                        }
//                        return srt;
//                    }
//                },
//                {
//                    "stitle": "type", "classname": "", render: function (data, type, row, meta) {
//                        //var name = "<strong style='text-transform: capitalize;'>" + row.documentno + "</strong>";
//                        return row.type
//                    },
//                },
//                {
//                    "stitle": "itemkey", "classname": "", render: function (data, type, row, meta) {
//                        //var name = "<strong style='text-transform: capitalize;'>" + row.documentrevision + "</strong>";
//                        //return row.itemkey
//                        var name = "<a target='_blank' href='/des/part/detail?q=" + row.qstring + "'>" + row.itemkey + "</a>";
//                        return name;
//                    },
//                },
//                {
//                    "stitle": "parent item", "classname": "", render: function (data, type, row, meta) {
//                        return row.parent_item
//                    }
//                },
//                {
//                    "stitle": "itemname", "classname": "", render: function (data, type, row, meta) {
//                        return row.itemname
//                    }
//                },
//                {
//                    "stitle": "rev", "classname": "", render: function (data, type, row, meta) {
//                        return "r" + row.revno
//                    }
//                },
//                {
//                    "stitle": "drg no", "classname": "", render: function (data, type, row, meta) {
//                        return row.documentno
//                    }
//                },
//                {
//                    "stitle": "material", "classname": "", render: function (data, type, row, meta) {
//                        return row.material
//                    }
//                },
//                {
//                    "stitle": "itemgroup", "classname": "", render: function (data, type, row, meta) {
//                        return row.itemgroup
//                    }
//                },
//            ],

//            "initcomplete": function () {

//            },

//            "fnrowcallback": function () {

//            }
//        });
//    }, 1000);
//}

$("#SearchPart").click(function (e) {

    var selectchbHeaderPart = $("#chbHeaderPart").prop("checked");
    if (selectchbHeaderPart) {
        $("#chbHeaderPart").prop("checked", false);
    }
    selectedPartIDs = [];
    FinalPartlist = [];
    $("#m_modal_6").modal("show");
    setTimeout(function () {
        tblDCRPart.ajax.reload();
    }, 500);    
});

$('#tblAffectedPart').on('click', 'td .btnDelete', function () {
    var tr = $(this).closest('tr');
    var row = tblAffectedPart.row(tr);
    var data = row.data();
    swal({
        title: "Do you want to delete..?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            if (isConfirm.value) {
                mApp.blockPage();
                $.ajax({
                    type: "GET",
                    url: '/DES/DCR/DeleteAffectedPart',
                    data: { Id: data.DCRAffectedPartsId },
                    dataType: 'json',
                    contentType: "application/json;charset=utf-8",
                    async: true,
                    success: function (response) {
                        mApp.unblockPage();
                        if (response.Status) {
                            toastr.success(response.Msg, "Success");
                            tblAffectedPart.ajax.reload();
                        } else {
                            toastr.error(response.Msg, "Error");
                        }
                    },
                    error: function (error) {
                        mApp.unblockPage();
                        toastr.error("something went wrong try again later", "Error");
                    }
                });
            }
        });
});