﻿
var tblARMSet;
$(document).ready(function () {
    mApp.blockPage();
    tblARMSet = $('#tblARMSet').DataTable({
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "auto-width": false,
        "sAjaxSource": "/ARMSet/Index",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        "responsive": false,
        "language": {
            "infoFiltered": ""
        },
        //"scroller": {
        //    loadingIndicator: true
        //},
        //"bSort": true,
        //"aaSorting": [[1, 'desc']],
        "order": [[0, 'desc']],
        "columnDefs": [

        ],

        "fnServerData": function (sSource, data, fnCallback) {

            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });

        },
        "aoColumns": [

            {
                "sTitle": "ARMSet <span class='text-danger'>*</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", "className": "", render: function (data, type, row, meta) {
                    var ArmSet = "<strong style='text-transform: capitalize;'>" + row.Arms + "</strong>";
                    return ArmSet;
                },
            },
            {
                "sTitle": "Description <span class='text-danger'>*</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", "className": "", render: function (data, type, row, meta) {
                    return row.Desc;
                }
            },
            {
                "sTitle": "Ver <span class='text-danger'>*</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", "className": "", render: function (data, type, row, meta) {
                    return row.Vrsn;
                }
            },
            {
                "sTitle": "ARM Code", "className": "", render: function (data, type, row, meta) {
                    return row.ARMName;
                }
            },
            {
                "sTitle": "File Name &nbsp;&nbsp;&nbsp;", "className": "", render: function (data, type, row, meta) {
                    return row.Fnam;
                }
            },
            {
                "sTitle": "Status", "orderable": false, render: function (data, type, row, meta) {
                    var strHtml = '';
                    var arms = row.Arms;
                    if (row.Status === true) {
                        strHtml = "<a href='javascript:;' onclick='funUpdateArmCodeStatus(1, \"" + arms + "\")'><i class='la la-check'></i></a>";
                    }
                    else {
                        strHtml = "<a href='javascript:;' onclick='funUpdateArmCodeStatus(2,\"" + arms + "\")'><i class='la la-close'></i></a>";
                    }
                    return strHtml;
                }
            },
            {
                "sTitle": "Type", "className": "", render: function (data, type, row, meta) {
                    return row.Type;
                }
            },

            {
                "sTitle": "Created By", "className": "", "orderable": false, render: function (data, type, row, meta) {
                    return row.CreatedBy;
                }
            },
            {
                "sTitle": "Created On", "className": "", "orderable": false, render: function (data, type, row, meta) {
                    return row.CreatedOn;
                }
            },
            {
                "sTitle": "Edited By", "className": "", "orderable": false, render: function (data, type, row, meta) {
                    return row.EditedBy;
                }
            },
            {
                "sTitle": "Edited On", "className": "", "orderable": false, render: function (data, type, row, meta) {
                    return row.EditedOn;
                }
            },
            {
                "sTitle": "Action &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", "className": "action", "orderable": false, render: function (data, type, row, meta) {
                    var srt = "";
                    if (row.IsFromDOC == false) {
                        srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill btnEdit" title="Edit"><i class="la la-edit text-info"></i></a>';
                    }
                    if (IsITRoleGroup == "True") {
                        srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnDelete" title="Delete"><i class="la la-trash text-danger"></i></a>';
                    }
                        return srt;
                }
            }
        ],

        "initComplete": function () {
         AddARMSetRow();
        },

        "fnRowCallback": function () {
           AddARMSetRow();
        }

    });
    mApp.unblockPage();
});

function AddARMSetRow() {
    setTimeout(function () {
        //$("#Arms_0").focus();
        if ($("#Arms_0").length == 0)
            var html = '<tr class="v-top">'
                + '<td><input type="text" maxlength="30" class="form-control blockSpecialChar noSpace" id="Arms_0" name="Arms_0" placeholder="ARMSet" /> <label class="text-danger" id="errorArms_0" style="display:none" >ARMSet is required</label></td> '
                + '<td><input type="text" maxlength="50" class="form-control blockSpecialChar" id="Description_0" name="Description_0" placeholder="Description" /> <label class="text-danger" id="errorDescription_0" style="display:none" >Description is required</label></td> '
                + '<td><input type="text" maxlength="9" class="form-control blockSpecialChar noSpace" id="Vrsn_0" name="Vrsn_0" placeholder="Version" /><label class="text-danger" id="errorVrsn_0" style="display:none" >Version is required</label></td> '
                + '<td><select id="ARMCode_0" class= "form-control"><option value="">Select</option>';
        for (let i = 0; i < ddlobjectlist.length; i++) {
            html += "<option value='" + ddlobjectlist[i].ARMCodeId + "'>" + ddlobjectlist[i].Name + " </option>";
        }
        html += '</select></td> '
            + '<td><input type="text" maxlength="50" class="form-control blockSpecialChar" id="File_0" name="File_0" placeholder="FileName" /></td> '
            + '<td></td> '
            + '<td></td><td></td><td></td><td></td><td></td>'
            + '<td><a href="#" class="btn m-btn btn-success m-btn--pill m-btn--center btn-sm special-add-btn" onclick="AddARMSet(0)" title="Add"><i class="fa fa-plus"></i> Add </a></td> </tr>'
        $('#tblARMSet tbody').prepend(html);
        $("#ARMCode_0").select2();
    }, 1000);
}

function funUpdateArmCodeStatus(UpdateFlag, Arms) {
    var valStatus = false;
    if (UpdateFlag == 2) {
        valStatus = true;
    }
    //var model = { Arms: Arms, Status: valStatus };
    mApp.blockPage();
    $.ajax({
        type: "GET",
        url: '/DES/ARMSet/UpdateArmSetStatus',
        data: { Arms: Arms, Status: valStatus },
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        async: true,
        success: function (response) {
            mApp.unblockPage();
            if (response.Status) {
                toastr.success(response.Msg, "Success");               
                tblARMSet.ajax.reload();
            } else {
                toastr.info(response.Msg, "Info");
            }
        },
        error: function (error) {
            mApp.unblockPage();
            toastr.error("something went wrong try again later", "Error");
        }
    });
}

$('#tblARMSet').on('click', 'td .btnEdit', function () {
    var tr = $(this).closest('tr');
    var row = tblARMSet.row(tr);
    var data = row.data();
    var html =
        '<td><input type="text" maxlength="30" class="form-control blockSpecialChar noSpace" id="Arms_' + data.Arms + '" name="Arms_' + data.Arms + '" placeholder="ARMSet" value="' + data.Arms + '" disabled/> <label class="text-danger" id="errorArms_' + data.Arms + '" style="display:none" >ARMSet is required </label></td> '
        + '<td><input type="text" maxlength="50" class="form-control blockSpecialChar" id="Description_' + data.Arms + '" name="Description_' + data.Arms + '" placeholder="Description"  value="' + data.Desc + '" /> <label class="text-danger" id="errorDescription_' + data.Arms + '" style="display:none" >Description is required </label></td> '
        + '<td><input type="text" maxlength="9" class="form-control blockSpecialChar noSpace" id="Vrsn_' + data.Arms + '" name="Vrsn_' + data.Arms + '" placeholder="Version" value="' + data.Vrsn + '" /><label class="text-danger" id="errorVrsn_' + data.Arms + '" style="display:none" >Version is required </label></td> '
        + '<td><select id="ARMCode_' + data.Arms + '" name="ARMCode_' + data.Arms + '" value="' + data.ARMName + '" class= "form-control">';
    html += "<option value=''>Select</option>'";
    for (let i = 0; i < ddlobjectlist.length; i++) {
        if (ddlobjectlist[i].Name == data.ARMName) {
            html += " <option selected='selected' value='" + ddlobjectlist[i].ARMCodeId + "'>" + ddlobjectlist[i].Name + " </option>";
        }
        else {
            html += "<option value='" + ddlobjectlist[i].ARMCodeId + "'>" + ddlobjectlist[i].Name + "</option>";
        }
    }
    html += '</select></td> '
+ '<td><input type="text" maxlength="50" class="form-control blockSpecialChar" id="File_' + data.Arms + '" name="File_' + data.Arms + '" placeholder="File" value="' + data.Fnam + '" /></td> '
    html += "<td>" + (data.Status ? "<i class='la la-check'></i >" : "<i class='la la-close'></i >") +"</td>";
    html += '<td></td><td></td><td></td><td></td><td></td>';
    html += '<td><a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" data-arm=' + data.Arms + ' onclick="AddARMSet(this)" title="Edit"><i class="la la-check text-info"></i></a>'
    html += ' <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" onclick="tblARMSetReload()" title="close"><i class="la la-close text-danger"></i></a></td>';

    $(tr).html(html);
    $("#Description_" + data.Arms).focus();
    $("#ARMCode_" + data.Arms).select2();
});

function tblARMSetReload() {
    tblARMSet.ajax.reload();
}

function AddARMSet(ids) {
    var id;
    if (ids == 0) {
        id = 0;
    }
    else {
        id = $(ids).attr("data-arm");
    }
   
    $("#errorArms_" + id).hide();
    $("#errorDescription_" + id).hide();
    $("#errorVrsn_" + id).hide();
    var Arms = $("#Arms_" + id).val();
    var Desc = $("#Description_" + id).val();
    var Vrsn = $("#Vrsn_" + id).val();
    var ArmCode = $("#ARMCode_" + id).val();
    var Fnam = $("#File_" + id).val();
    var Status = $("#Status_" + id).prop("checked");
    if (Arms != "" && Desc != "" && Vrsn != "") {
        mApp.blockPage();
        $.ajax({
            type: "POST",
            url: '/DES/ARMSet/AddEdit',
            data: JSON.stringify({ Arms: id, Arms: Arms, Desc: Desc, Vrsn: Vrsn, ArmCode: ArmCode, Fnam: Fnam, Status: Status }),
            dataType: 'json',
            contentType: "application/json;charset=utf-8",
            async: true,
            success: function (response) {
                mApp.unblockPage();
                if (response.Status) {
                    toastr.success(response.Msg, "Success");
                    tblARMSet.ajax.reload();
                } else {
                    toastr.info(response.Msg, "Info");
                }
            },
            error: function (error) {
                mApp.unblockPage();
                toastr.error("something went wrong try again later", "Error");
            }
        });
    } else {
        if (Arms == "")
            $("#errorArms_" + id).show();
        if (Desc == "")
            $("#errorDescription_" + id).show();
        if (Vrsn == "")
            $("#errorVrsn_" + id).show();
    }

}

$('#tblARMSet').on('click', 'td .btnDelete', function () {
    var tr = $(this).closest('tr');
    var row = tblARMSet.row(tr);
    var data = row.data();

    swal({
        title: "Do you want to delete..?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            if (isConfirm.value) {
                mApp.blockPage();
                $.ajax({
                    type: "GET",
                    url: '/DES/ARMSet/DeleteArmSet',
                    data: { Arms: data.Arms },
                    dataType: 'json',
                    contentType: "application/json;charset=utf-8",
                    async: true,
                    success: function (response) {
                        mApp.unblockPage();
                        if (response.Status) {
                            toastr.success(response.Msg, "Success");
                            if (tblARMSet.data().count() == 1) {
                                tblARMSet.ajax.reload();
                            }
                            else {
                                tblARMSet.ajax.reload(null, false);
                            }
                          
                        } else {
                            toastr.info(response.Msg, "Info");
                        }
                    },
                    error: function (error) {
                        mApp.unblockPage();
                        toastr.error("something went wrong try again later", "Error");
                    }
                });
            }
        });
});

$("#m_aside_left_minimize_toggle").click(function () {
    if (tblARMSet != null)
        tblARMSet.columns.adjust();
});