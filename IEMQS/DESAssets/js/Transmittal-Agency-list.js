var DatatablesBasicScrollable = {
    init: function () {
        var e;
        (e = $("#m_table_1")).DataTable({
            dom: "<'row'<'col-sm-12'tr>>\n\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
   scrollY: "30vh",
            scrollX: !0,
               order: [[1, "desc"]],
          
            columnDefs: [ {
                targets: 0,
                title: "Actions",
                orderable: !1,
                render: function (e, a, t, n) {
                    return '\n <a href="Generate-Transmittal.html" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="Generate Transmittal">\n                          <i class="la la-external-link m--font-success"></i>\n                        </a> \n <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Edit">\n                          <i class="la la-edit  m--font-brand"></i>\n                        </a>  \n <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="View">\n<i class="la la-trash m--font-danger"></i>\n                        </a> '
                }
            }, ]
        })
    }
};
jQuery(document).ready(function () {
    DatatablesBasicScrollable.init()
});