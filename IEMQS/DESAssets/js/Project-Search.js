﻿

$(document).ready(function () {
    $("#txtProjectSearch").autocomplete({
        source: function (request, response) {
            $.get("/DES/Project/ProjectSearch", {
                Status: $("#ddlProjectStatus").val(), key: request.term
            }, function (data) {
                
                if (data.length > 0) {
                    response(data);
                }
                else {
                    toastr.error("Project Name is not found", "Error");
                   // location.href = "/DES/Dashboard";
                }
                
            });
        },
        minLength: 3
    });
});

$('#txtProjectSearch').on("keypress", function (e) {
    if (e.keyCode == 13) {
        SearchProject();
    }
});

function SearchProject() {
    var ProjectSearch = $("#txtProjectSearch").val();
    if (ProjectSearch) {
        $("#txtProjectSearch").val('');
        location.href = "/DES/Project/Details?ProjectNo=" + ProjectSearch;
    }
    else {
        toastr.error("Please enter project name", "Error");
    }
    
}