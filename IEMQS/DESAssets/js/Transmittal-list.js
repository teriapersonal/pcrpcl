var DatatablesBasicScrollable1 = {
    init: function () {
        var e;
        (e = $("#m_table_2")).DataTable({
            dom: "<'row'<'col-sm-12'tr>>\n\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
   scrollY: "30vh",
            scrollX: !0,
               order: [[1, "desc"]],
          
            columnDefs: [ {
                targets: 0,
                title: "Actions",
                orderable: !1,
                render: function (e, a, t, n) {
                    return '\n <a href="" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="Send Email">\n                          <i class="la la-envelope m--font-success"></i>\n                        </a>\n <a href="Generate-Transmittal.html" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" title="View">\n                          <i class="la la-eye m--font-info"></i>\n                        </a>  \n <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Edit">\n                          <i class="la la-edit  m--font-brand"></i>\n                        </a> '
                }
            }, ]
        })
    }
};
jQuery(document).ready(function () {
    DatatablesBasicScrollable1.init()
    $('a[data-toggle="tab"]').on( 'shown.bs.tab', function (e) {
     $($.fn.dataTable.tables( true ) ).css('width', '100%');
        $($.fn.dataTable.tables( true ) ).DataTable().columns.adjust().draw();
    });
});