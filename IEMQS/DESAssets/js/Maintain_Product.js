﻿var tblProduct;
$(document).ready(function () {

    tblProduct = $('#tblProduct').DataTable({
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "sAjaxSource": "/Product/GetProductList",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        "responsive": true,
        "language": {
            "infoFiltered": ""
        },
        //"scroller": {
        //    loadingIndicator: true
        //},
        //"bSort": true,
        //"aaSorting": [[1, 'desc']],
        "order": [[0, 'desc']],
        "columnDefs": [
            { "orderable": false, "targets": 2 },
            { "orderable": false, "targets": 3 },
            { "orderable": false, "targets": 4 },
            { "orderable": false, "targets": 5 },
            { "orderable": false, "targets": 6 }
        ],

        "fnServerData": function (sSource, data, fnCallback) {

            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });

        },
        "aoColumns": [
            {
                "sTitle": "BU / PBU <span class='text-danger'>*</span>", "className": "", render: function (data, type, row, meta) {
                    var BU = "<strong style='text-transform: capitalize;'>" + row.BU + "-" + row.PBU + "</strong>";
                    return BU;
                }
            },
            {
                "sTitle": "Product <span class='text-danger'>*</span>", "className": "", render: function (data, type, row, meta) {
                    var ProductName = "<strong style='text-transform: capitalize;'>" + row.Product + "</strong>";
                    return ProductName;
                },
            },
            {
                "sTitle": "Description <span class='text-danger'>*</span>", "className": "", render: function (data, type, row, meta) {                   
                    return row.Description;
                },
            },
            {
                "sTitle": "Created By", "className": "", render: function (data, type, row, meta) {
                    return row.CreatedBy;
                }
            },
            {
                "sTitle": "Created On", "className": "", render: function (data, type, row, meta) {
                    return row.CreatedOn;
                }
            },
            {
                "sTitle": "Edited By", "className": "", render: function (data, type, row, meta) {
                    return row.EditedBy;
                }
            },
            {
                "sTitle": "Edited On", "className": "", render: function (data, type, row, meta) {
                    return row.EditedOn;
                }
            },
            {
                "sTitle": "Action", "className": "action", "orderable": false, render: function (data, type, row, meta) {
                    var srt = "";
                    srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill btnEdit" title="Edit"><i class="la la-edit text-info"></i></a>';
                    srt += '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnDelete" title="Delete"><i class="la la-trash text-danger"></i></a>';
                    return srt;
                }
            }
        ],

        "initComplete": function () {
            AddProRow();
        },

        "fnRowCallback": function () {
            AddProRow();
            
        }
    });
});

function AddProRow() {
    setTimeout(function () {
        $("#BU_0").focus();
        if ($("#BU_0").length == 0)
            var html = '<tr class="v-top">'
            + '<td><select id="BU_0" class= "form-control"><option value="">Select</option>';
        for (let i = 0; i < ddlbulist.length; i++) {
            html += "<option value='" + ddlbulist[i].BUId + "'>" + ddlbulist[i].BU + "</option>";
        }
        html += '</select><br><label class="text-danger" id="errorBU_0" style="display:none" > BU / PBU is required </label></td> '
            + '<td><input type="text" maxlength="100" class="form-control" id="ProductName_0" Name="ProductName_0" placeholder="Product Name" /><label class="text-danger" id="errorProductName_0" style="display:none" > Product Name is required </label></td> '
            + '<td><input type="text" maxlength="100" class="form-control" id="Description_0" Name="Description_0" placeholder="Description" /><label class="text-danger" id="errorDescription_0" style="display:none" > Description is required </label></td> '
            + '<td></td><td></td><td></td><td></td>'
            + '<td><a href="#" class="btn m-btn btn-success m-btn--pill m-btn--center btn-sm special-add-btn" onclick="AddProduct(0)" title="Add"><i class="fa fa-plus"></i> Add </a></td> </tr>'
        $('#tblProduct tbody').prepend(html);
        $("#BU_0").select2();
    }, 1000);
}

function AddProduct(id) {
    $("#errorBU_" + id).hide();
    $("#errorProductName_" + id).hide();
    $("#errorDescription_0" + id).hide();
    var bu = $("#BU_" + id).val();
    var name = $("#ProductName_" + id).val();
    var desc = $("#Description_" + id).val();   
    if (bu != "" && name != "" && desc != "") {
        mApp.blockPage();
        $.ajax({
            type: "POST",
            url: '/DES/Product/AddProduct',
            data: JSON.stringify({ ProductId: id, Product: name, BUId: bu, Description : desc }),
            dataType: 'json',
            contentType: "application/json;charset=utf-8",
            async: true,
            success: function (response) {
                mApp.unblockPage();
                if (response.Status) {
                    toastr.success(response.Msg, "Success");
                    tblProduct.ajax.reload();
                } else {
                    toastr.info(response.Msg, "Info");
                }
            },
            error: function (error) {
                mApp.unblockPage();
                toastr.error("something went wrong try again later", "Error");
            }
        });
    } else {              
        if (bu == "")
            $("#errorBU_" + id).show();
        if (name == "")
            $("#errorProductName_" + id).show();
        if (desc == "")
            $("#errorDescription_" + id).show();
    }
}

$('#tblProduct').on('click', 'td .btnEdit', function () {
    var tr = $(this).closest('tr');
    var row = tblProduct.row(tr);
    var data = row.data();
    var html = '<td><select id="BU_' + data.ProductId + '" name="BU_' + data.ProductId + '" value="' + data.BU + '" class= "form-control">';
        for (let i = 0; i < ddlbulist.length; i++) {
            if (ddlbulist[i].BUId == data.BUId)
                html += "<option selected='selected' value='" + ddlbulist[i].BUId + "'>" + ddlbulist[i].BU + "</option>";
            else
                html += "<option value='" + ddlbulist[i].BUId + "'>" + ddlbulist[i].BU + "</option>";
        }
        html += '</select><label class="text-danger" id="errorBU" style="display:none" > BU/PBU is required </label></td> '
            + '<td><input type="text" maxlength="100" class="form-control" id="ProductName_' + data.ProductId + '" name="ProductName_' + data.ProductId + '" placeholder="Product Name" value="' + data.Product + '" /> <label class="text-danger" id="errorProductName_' + data.ProductId + '" style="display:none" > Product Name is required </label></td> '
            + '<td><input type="text" maxlength="100" class="form-control" id="Description_' + data.ProductId + '" name="Description_' + data.ProductId + '" placeholder="Description" value="' + data.Description + '" /> <label class="text-danger" id="errorDescription_' + data.ProductId + '" style="display:none" > Description is required </label></td> '        
            + '<td></td><td></td><td></td><td></td>'
            + '<td><a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" onclick="AddProduct(' + data.ProductId + ')" title="Update"><i class="la la-check text-info"></i></a>'
            + '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" onclick="tblProductClear()" title="Cancel"><i class="la la-close text-danger"></i></a></td>'
    $(tr).html(html);
    $("#BU_" + data.ProductId).focus();
    $("#BU_" + data.ProductId).select2();
});

function tblProductClear() {
    tblProduct.ajax.reload();
}

$('#tblProduct').on('click', 'td .btnDelete', function () {
    var tr = $(this).closest('tr');
    var row = tblProduct.row(tr);
    var data = row.data();
    swal({
        title: "Do you want to delete..?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            if (isConfirm.value) {
                mApp.blockPage();
                $.ajax({
                    type: "GET",
                    url: '/DES/Product/DeleteProduct',
                    data: { Id: data.ProductId },
                    dataType: 'json',
                    contentType: "application/json;charset=utf-8",
                    async: true,
                    success: function (response) {
                        mApp.unblockPage();
                        if (response.Status) {
                            toastr.success(response.Msg, "Success");
                            if (tblProduct.data().count() == 1) {
                                tblProduct.ajax.reload();
                            }
                            else {
                                tblProduct.ajax.reload(null, false);
                            }
                        } else {
                            toastr.info(response.Msg, "Info");
                        }
                    },
                    error: function (error) {
                        mApp.unblockPage();
                        toastr.error("something went wrong try again later", "Error");
                    }
                });
            }
        });
});

$("#m_aside_left_minimize_toggle").click(function () {
    if (tblProduct != null)
        tblProduct.columns.adjust();
});
