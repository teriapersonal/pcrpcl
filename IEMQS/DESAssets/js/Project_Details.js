var tblProjectAccessList;
$(document).ready(function () {

    tblProjectAccessList = $('#tblProjectAccessList').DataTable({
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "sAjaxSource": "/DES/Project/Details",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        //"responsive": true,
        "language": {
            "infoFiltered": ""
        },
        //"scroller": {
        //    loadingIndicator: true
        //},
        //"bSort": true,
        //"aaSorting": [[1, 'desc']],
        "order": [[0, 'desc']],
        "columnDefs": [
        ],

        "fnServerData": function (sSource, data, fnCallback) {
            data.push({ name: "ProjectNo", value: $("#ModelProject").text() });
            data.push({ name: "ContractNo", value: $("#ModelContract").text() });
            data.push({ name: "BUCode", value: $("#ModelBUCode").text() });
            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });

        },
        "aoColumns": [
            {
                "sTitle": "EmployeeName", "className": "","sWidth":"20%", render: function (data, type, row, meta) {
                    return row.EmpName;
                }
            },
             {
                 "sTitle": "Roles", "className": "", "sWidth": "80%", render: function (data, type, row, meta) {
                     return row.Roles;
                 }
             }
        ],

        "initComplete": function () {

        },

        "fnRowCallback": function () {
            tblProjectAccessList.columns.adjust();
        }

    });
});

function funUpdateDetailsModal(EditModeFlag, $this) {
    mApp.blockPage();
    $.ajax({
        type: "GET",
        url: "/DES/Project/Edit",
        dataType: "html",
        data: { "ProjectNo": $('#ModelProject').text() },
        success: function (data) {
            mApp.unblockPage();
            $('#myModalContent1').html(data);
            $('#m_select2_modal1').modal('show');
            setTimeout(function () {
                $("#FolderName").focus();
            }, 1000);

        },
        error: function () {
            mApp.unblockPage();
            toastr.error("Error occured!!", "Error")
        }
    });

}

$("#m_aside_left_minimize_toggle").click(function () {
    if (tblProjectAccessList != null)
        tblProjectAccessList.columns.adjust();
});



//var DatatablesBasicScrollable = {
//    init: function () {
//        var e;
//        (e = $("#m_table_1, #m_table_2")).DataTable({
           
// dom: "<'row'<'col-sm-12'tr>>\n\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
//            searching: false,
//          paging:   false,
//              order: [[1, "desc"]],
//           /* headerCallback: function (e, a, t, n, s) {
//                e.getElementsByTagName("th")[0].innerHTML = '\n                    <label class="m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--success">\n                        <input type="checkbox" value="" class="m-group-checkable">\n                        <span></span>\n                    </label>'
//            },*/
//            columnDefs: [ /*{
//                targets: 0,
//                width: "30px",
//                className: "dt-right",
//                orderable: !1,
//                render: function (e, a, t, n) {
//                    return '\n                        <label class="m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--success">\n                            <input type="checkbox" value="" class="m-checkable">\n                            <span></span>\n                        </label>'
//                }
//            }*/]
//        })/*,e.on("change", ".m-group-checkable", function () {
//            var e = $(this).closest("table").find("td:first-child .m-checkable"),
//                a = $(this).is(":checked");
//            $(e).each(function () {
//                a ? ($(this).prop("checked", !0), $(this).closest("tr").addClass("active")) : ($(this).prop("checked", !1), $(this).closest("tr").removeClass("active"))
//            })
//        }), e.on("change", "tbody tr .m-checkbox", function () {
//            $(this).parents("tr").toggleClass("active")
//        })*/
//    }
//};
//jQuery(document).ready(function () {
//    DatatablesBasicScrollable.init()
//});


