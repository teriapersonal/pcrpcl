﻿var armxhrChange = function () {
    getDecOfARM();
}

var armxhr = {
    valueField: 'Arms',
    labelField: 'Desc',
    searchField: ['Desc'],
    options: [],
    load: function (query, callback) {
        if (!query.length) return callback();
        $.ajax({
            url: '/DES/Part/GetAllARM',
            type: 'GET',
            dataType: 'json',
            data: {
                arm: query,
            },
            error: function () {
                callback();
            },
            success: function (res) {
                callback(res);
            }
        });
    },
    onChange: armxhrChange
}

var ProcurementDrgDocumentxhr = {
    valueField: 'Id',
    labelField: 'Lookup',
    searchField: ['Lookup'],
    options: [],
    load: function (query, callback) {
        if (!query.length) return callback();
        $.ajax({
            url: '/DES/Part/GetProcurementDrawing',
            type: 'GET',
            dataType: 'json',
            data: {
                DocNo: query,
                Contract: $("#Contract").val()
            },
            error: function () {
                callback();
            },
            success: function (res) {
                callback(res);
            }
        });
    }
}


var SizeCodeChange = function () {
    getSizeCode();
}

var SizeCodexhr = {
    valueField: 'SizeCode',
    labelField: 'SizeCode',
    searchField: ['SizeCode'],
    options: [],
    load: function (query, callback) {
        if (!query.length) return callback();
        $.ajax({
            url: '/DES/Part/GetAllSizeCode',
            type: 'GET',
            dataType: 'json',
            data: {
                SizeCode: query,
            },
            error: function () {
                callback();
            },
            success: function (res) {
                callback(res);
            }
        });
    },
    onChange: SizeCodeChange
}

var MaterialChange = function () {
    GetMaterialChange();
}
var Materialxhr = {
    valueField: 'Value',
    labelField: 'Text',
    searchField: ['Text'],
    options: [],
    load: function (query, callback) {
        if (!query.length) return callback();
        $.ajax({
            url: '/DES/Part/GetMaterialByProductFormCode',
            type: 'GET',
            dataType: 'json',
            data: {
                Material: query,
                ProductForm: $('#ProductForm').val()
            },
            error: function () {
                callback();
            },
            success: function (res) {
                callback(res);
            }
        });
    },
    onChange: MaterialChange
}


$(document).ready(function () {
    $("#ItemKeyType").selectize().change();
    $('#OrderPolicy').selectize();
    $("#Type").selectize().change();
    $('#ARMSet').selectize(armxhr);
    $('#ProductFormCodeId').selectize();
    $('#ProcurementDrgDocumentId').selectize(ProcurementDrgDocumentxhr);
    $('#Material').selectize(Materialxhr);
    $('#SizeCode').selectize(SizeCodexhr);
    $("#ItemGroup").selectize();
    $("#IsDoubleClad").selectize().change();

    CalcAll();
    ItemIdExist();
});

function ItemIdExist() {
    var id = $("#ItemId").val() || "0";
    if (parseFloat(id) > 0) {
        var ItemKeyType = $("#ItemKeyType").selectize();
        var selectizeItemKeyType = ItemKeyType[0].selectize;
        selectizeItemKeyType.disable()
        var OrderPolicy = $("#OrderPolicy").selectize();
        var selectizeOrderPolicy = OrderPolicy[0].selectize;
        selectizeOrderPolicy.disable()
        var Type = $("#Type").selectize();
        var selectizeType = Type[0].selectize;
        selectizeType.disable()
        var ProductFormCodeId = $("#ProductFormCodeId").selectize();
        var selectizeProductFormCodeId = ProductFormCodeId[0].selectize;
        selectizeProductFormCodeId.disable()
        $("#ItemKey").attr("disabled", "disabled");
    } else {
        var ItemKeyType = $("#ItemKeyType").selectize();
        var selectizeItemKeyType = ItemKeyType[0].selectize;
        selectizeItemKeyType.enable()
        var OrderPolicy = $("#OrderPolicy").selectize();
        var selectizeOrderPolicy = OrderPolicy[0].selectize;
        selectizeOrderPolicy.enable()
        var Type = $("#Type").selectize();
        var selectizeType = Type[0].selectize;
        selectizeType.enable()
        var ProductFormCodeId = $("#ProductFormCodeId").selectize();
        var selectizeProductFormCodeId = ProductFormCodeId[0].selectize;
        selectizeProductFormCodeId.enable()
        $("#ItemKey").removeAttr("disabled");
    }
}

$("#ProductFormCodeId").change(function () {
    getProductFormDescriptions();
});
function getProductFormDescriptions() {
    if ($("#ProductFormCodeId").val() != "") {
        mApp.blockPage();
        $.ajax({
            type: "GET",
            url: "/DES/Part/GetProductFomDesc",
            data: { productFormId: $("#ProductFormCodeId").val() },
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            aysnc: true,
            success: function (response) {
                mApp.unblockPage();
                $('#String2').val(response.Description1);
                $('#String3').val(response.Description2);
                $('#String4').val(response.Description3);
                $('#HdnString2').val(response.Description1);
                $('#HdnString3').val(response.Description2);
                $('#HdnString4').val(response.Description3);
                $('#ItemName').val(response.ProductForm)
                $('#ProductForm').val(response.ProductForm)
                BOMReportDrawingStrChange();
                var Type = $("#Type").selectize();
                var selectizeType = Type[0].selectize;
                selectizeType.destroy();

                var v = '';
                $.each(response.TypeList, function (i, v1) {
                    v += "<option value='" + v1.Value + "'>" + v1.Text + "</option>";
                });

                $("#Type").html(v);
                $("#Type").selectize().change();
                var Material = $('#Material').selectize(Materialxhr);
                var selectizeMaterial = Material[0].selectize;
                selectizeMaterial.setValue("", false);
                selectizeMaterial.clearOptions()
                CleanDetails();
            },
            error: function (response) {
                mApp.unblockPage();
            }
        });
    } else {
        $('#String2').val("");
        $('#String3').val("");
        $('#String4').val("");
        $('#HdnString2').val("");
        $('#HdnString3').val("");
        $('#HdnString4').val("");
        $('#ItemName').val("");
        $('#ProductForm').val("");
        var v = '';
        var Type = $("#Type").selectize();
        var selectizeType = Type[0].selectize;
        selectizeType.destroy();
        $("#Type").html(v);
        $("#Type").selectize().change();
        var Material = $('#Material').selectize(Materialxhr);
        var selectizeMaterial = Material[0].selectize;
        selectizeMaterial.setValue("", false);
        selectizeMaterial.clearOptions()
        CleanDetails();
    }
}


function CleanDetails() {


    var ProcurementDrgDocumentId = $('#ProcurementDrgDocumentId').selectize(ProcurementDrgDocumentxhr);
    var selectizeProcurementDrgDocumentId = ProcurementDrgDocumentId[0].selectize;
    selectizeProcurementDrgDocumentId.setValue("", true);
    var ARMSet = $('#ARMSet').selectize(armxhr);
    var selectizeARMSet = ARMSet[0].selectize;
    selectizeARMSet.setValue("", true);
    $("#ARMRev").val("");
    var SizeCode = $('#SizeCode').selectize(SizeCodexhr);
    var selectizeSizeCode = SizeCode[0].selectize;
    selectizeSizeCode.setValue("", true);
    $("#Thickness").val("");
    $("#CladPlatePartThickness1").val("");
    $("#CladSpecificGravity1").val("");

    var IsDoubleClad = $("#IsDoubleClad").selectize();
    var selectizeIsDoubleClad = IsDoubleClad[0].selectize;
    selectizeIsDoubleClad.setValue("No", false);

    $("#CladPlatePartThickness2").val("");
    $("#CladSpecificGravity2").val("");
    $("#ItemWeight").val("");
    $("#UOM").val("");
    $("#CRSYN").val("No");
    $("#Density").val("");
    $("#WeightFactor").val("");
    $("#ManufactureItem").val("");
    $("#DoNotReferExistingItemkey").val("false");
    $("#DoNotCheckProductFormCode").val("false");
}

function getStringByProductForm() {
    if ($("#ProductFormCodeId").val() != "") {
        mApp.blockPage();
        $.ajax({
            type: "GET",
            url: "/DES/Part/GetProductFomDesc",
            data: { productFormId: $("#ProductFormCodeId").val() },
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            aysnc: true,
            success: function (response) {
                mApp.unblockPage();
                $('#String2').val(response.Description1);
                $('#String3').val(response.Description2);
                $('#String4').val(response.Description3);
                $('#HdnString2').val(response.Description1);
                $('#HdnString3').val(response.Description2);
                $('#HdnString4').val(response.Description3);
                $('#ProductForm').val(response.ProductForm)
                BOMReportDrawingStrChange();
            },
            error: function (response) {
                mApp.unblockPage();
            }
        });
    } else {
        $('#String2').val("");
        $('#String3').val("");
        $('#String4').val("");
        $('#HdnString2').val("");
        $('#HdnString3').val("");
        $('#HdnString4').val("");
        $('#ProductForm').val("");
    }
}

function getTypeByProductForm(Selected) {
    if ($("#ProductFormCodeId").val() != "") {
        mApp.blockPage();
        $.ajax({
            type: "GET",
            url: "/DES/Part/GetProductFomDesc",
            data: { productFormId: $("#ProductFormCodeId").val() },
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            aysnc: true,
            success: function (response) {
                mApp.unblockPage();

                var Type = $("#Type").selectize();
                var selectizeType = Type[0].selectize;
                selectizeType.destroy();

                var v = '';
                $.each(response.TypeList, function (i, v1) {
                    if (v1.Value == Selected)
                        v += "<option selected='selected' value='" + v1.Value + "'>" + v1.Text + "</option>";
                    else
                        v += "<option value='" + v1.Value + "'>" + v1.Text + "</option>";
                });

                $("#Type").html(v);
                $("#Type").selectize().change();

            },
            error: function (response) {
                mApp.unblockPage();
            }
        });
    } else {

        var v = '';
        var Type = $("#Type").selectize();
        var selectizeType = Type[0].selectize;
        selectizeType.destroy();
        $("#Type").html(v);
        $("#Type").selectize().change();
    }
}

$("#Type").change(function () {

    $("#Thickness").removeAttr('disabled');
    $("#CladPlatePartThickness1").removeAttr('disabled');
    //$("#IsDoubleClad").removeAttr('disabled');
    var IsDoubleClad = $("#IsDoubleClad").selectize();
    var selectizeIsDoubleClad = IsDoubleClad[0].selectize;
    selectizeIsDoubleClad.enable()
    $("#CladPlatePartThickness2").removeAttr('disabled');
    $("#CladSpecificGravity2").removeAttr('disabled');
    //$("#SizeCode").removeAttr('disabled');
    var SizeCode = $("#SizeCode").selectize(SizeCodexhr);
    var selectizeSizeCode = SizeCode[0].selectize;
    selectizeSizeCode.enable()

    if ($("#Type").val() == Part) {
        $("#Thickness").attr('disabled', 'disabled');
        $("#CladPlatePartThickness1").attr('disabled', 'disabled');
        //$("#IsDoubleClad").attr('disabled', 'disabled');
        var IsDoubleClad = $("#IsDoubleClad").selectize();
        var selectizeIsDoubleClad = IsDoubleClad[0].selectize;
        selectizeIsDoubleClad.setValue("No", false);
        selectizeIsDoubleClad.disable()
        $("#CladPlatePartThickness2").attr('disabled', 'disabled');
        $("#CladSpecificGravity2").attr('disabled', 'disabled');
        $("#Thickness").val("");
        $("#CladPlatePartThickness1").val("");
        $("#CladPlatePartThickness2").val("");
        $("#CladSpecificGravity2").val("");
    }
    else if ($("#Type").val() == Plate_Part) {
        $("#CladPlatePartThickness1").attr('disabled', 'disabled');
        //$("#IsDoubleClad").attr('disabled', 'disabled');
        var IsDoubleClad = $("#IsDoubleClad").selectize();
        var selectizeIsDoubleClad = IsDoubleClad[0].selectize;
        selectizeIsDoubleClad.setValue("No", false);
        selectizeIsDoubleClad.disable()
        $("#CladPlatePartThickness2").attr('disabled', 'disabled');
        $("#CladSpecificGravity2").attr('disabled', 'disabled');
        $("#CladPlatePartThickness1").val("");
        $("#CladPlatePartThickness2").val("");
        $("#CladSpecificGravity2").val("");
        //$("#SizeCode").val("");
        //$("#SizeCode").attr('disabled', 'disabled');
        var SizeCode = $("#SizeCode").selectize(SizeCodexhr);
        var selectizeSizeCode = SizeCode[0].selectize;
        selectizeSizeCode.destroy();
        $("#SizeCode").val("");
        var SizeCode = $("#SizeCode").selectize(SizeCodexhr);
        var selectizeSizeCode = SizeCode[0].selectize;
        selectizeSizeCode.disable();
        $("#WeightFactor").val("");
    }
    else if ($("#Type").val() == Clad_Part) {
        //$("#SizeCode").val("");
        //$("#SizeCode").attr('disabled', 'disabled');
        var SizeCode = $("#SizeCode").selectize(SizeCodexhr);
        var selectizeSizeCode = SizeCode[0].selectize;
        selectizeSizeCode.destroy();
        $("#SizeCode").val("");
        var SizeCode = $("#SizeCode").selectize(SizeCodexhr);
        var selectizeSizeCode = SizeCode[0].selectize;
        selectizeSizeCode.disable();
        $("#WeightFactor").val("");
    }
    else if ($("#Type").val() == Jigfix) {
        $("#Thickness").attr('disabled', 'disabled');
        $("#CladPlatePartThickness1").attr('disabled', 'disabled');
        //$("#IsDoubleClad").attr('disabled', 'disabled');
        var IsDoubleClad = $("#IsDoubleClad").selectize();
        var selectizeIsDoubleClad = IsDoubleClad[0].selectize;
        selectizeIsDoubleClad.disable()
        $("#CladPlatePartThickness2").attr('disabled', 'disabled');
        $("#CladSpecificGravity2").attr('disabled', 'disabled');
        $("#Thickness").val("");
        $("#CladPlatePartThickness1").val("");
        $("#CladPlatePartThickness2").val("");
        $("#CladSpecificGravity2").val("");
    }
    $("#IsDoubleClad").change();
    getPolicyType();
    CalcAll();
});



$("#IsDoubleClad").change(function () {
    if ($("#IsDoubleClad").val() == "No") {
        $("#CladPlatePartThickness2").attr("disabled", "disabled");
        $("#CladSpecificGravity2").attr("disabled", "disabled");
        $("#CladPlatePartThickness2").val("");
        $("#CladSpecificGravity2").val("");
    } else {
        $("#CladPlatePartThickness2").removeAttr("disabled");
        $("#CladSpecificGravity2").removeAttr("disabled");
    }
    CalcAll();
});


function getPolicyType() {

    getValueOfTpye = $("#Type").val()

    if (getValueOfTpye != "") {
        $.ajax({
            type: "GET",
            url: "/DES/Part/GePolicyByType", //If get File Then Document Mapping Add
            data: {
                Type: getValueOfTpye
            },
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            aysnc: true,
            success: function (response) {

                $('#Policy').val(response.PolicyName);
                $('#PolicyId').val(response.PolicyId);

            },
            error: function (response) {

            }
        });
    } else {
        $('#Policy').val("");
        $('#PolicyId').val("");
    }
}

function getSizeCode() {


    if ($("#SizeCode").val() != "") {
        $.ajax({
            type: "GET",
            url: "/DES/Part/GetSizeCodeDetailsById", //If get File Then Document Mapping Add
            data: {
                Id: $("#SizeCode").val()
            },
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            aysnc: true,
            success: function (response) {
                var wf = parseFloat(response.WeightFactor).toFixed(2);
                $('#WeightFactor').val(wf);



                swal({
                    backdrop: false,
                    title: "You want to use String2, String3 and String4 base on Size code?",
                    type: "info",
                    allowOutsideClick: false,
                    showConfirmButton: true,
                    showCancelButton: true,
                    confirmButtonClass: "btn btn-danger",
                    cancelButtonClass: "btn btn-secondary",
                    confirmButtonText: "Yes",
                    cancelButtonText: "No"
                }).then(
                    function (isConfirm) {
                        if (isConfirm.value) {
                            $('#String2').val(response.Description1);
                            $('#String3').val(response.Description2);
                            $('#String4').val(response.Description3);
                            $('#HdnString2').val(response.Description1);
                            $('#HdnString3').val(response.Description2);
                            $('#HdnString4').val(response.Description3);
                        }
                    });




                CalcAll();
            },
            error: function (response) {

            }
        });
    } else {
        $('#WeightFactor').val("");
        CalcAll();
    }
}


function GetMaterialChange() {
    
    if ($("#Material").val() != "") {
        mApp.blockPage();
        $.ajax({
            type: "GET",
            url: "/DES/Part/GetItemGroupFromMaterial",
            data: {
                Material: $("#Material").val(),
                ProductForm: $('#ProductForm').val(),
                ItemGroup: $('#HidenItemGrouop').val()
            },
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            aysnc: true,
            success: function (response) {
                var ds = parseFloat(response.Density).toFixed(2);
                $("#Density").val(ds);
                var csg = parseFloat(response.CladSpecificGravity1).toFixed(2);
                $("#CladSpecificGravity1").val(csg);
                var ItemGroup = $('#ItemGroup').selectize();
                var selectizeItemGroup = ItemGroup[0].selectize;
                selectizeItemGroup.destroy()
                var v = '';
                if (response.ItemGroupUOM != null && response.ItemGroupUOM != "") {
                    //var l = response.ItemGroupUOM.length;
                    //if (l > 1)
                    //    v = "<option value>Select</option>";
                    $.each(response.ItemGroupUOM, function (i, v1) {
                        if (v1.Selected)
                            v += "<option selected='selected' value='" + v1.t_citg + "'>" + v1.t_deca + "</option>";
                        else
                            v += "<option value='" + v1.t_citg + "'>" + v1.t_deca + "</option>";
                    });
                }
                $("#ItemGroup").html(v);
                $("#ItemGroup").selectize().change();
                mApp.unblockPage();
            },
            error: function (response) {
                mApp.unblockPage();
            }
        });
    } else {
        $("#Density").val("");
        $("#CladSpecificGravity1").val("");
        var ItemGroup = $('#ItemGroup').selectize();
        var selectizeItemGroup = ItemGroup[0].selectize;
        selectizeItemGroup.destroy()
        var v = '';
        v = "<option value>Select</option>";
        $("#ItemGroup").html(v);
        $("#ItemGroup").selectize().change();
    }
}


$("#ItemGroup").on("change", function () {
    if ($("#ItemGroup").val() != "") {

        $.ajax({
            url: '/DES/Part/GetItemGroupDetail',
            type: 'GET',
            traditional: true,
            data: { ItemGroup: $("#ItemGroup").val() },
            contentType: 'application/json; charset=utf-8',
            dataType: "json",
            success: function (result) {
                mApp.unblockPage();
                $("#UOM").val(result.t_cuni);
                $("#ManufactureItem").val(result.t_kitm);
                CalcAll();
            },
            error: function (xhr) {
                mApp.unblockPage();
            }
        });
    } else {
        $("#UOM").val("");
        $("#ManufactureItem").val("");
        CalcAll();
    }
});

function GetItemGroup(selected, Material) {   
    if (Material != "") {
        $.ajax({
            type: "GET",
            url: "/DES/Part/GetItemGroupFromMaterial",
            data: {
                Material: Material,
                ProductForm: $('#ProductForm').val()
            },
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            aysnc: true,
            success: function (response) {
                var ItemGroup = $('#ItemGroup').selectize();
                var selectizeItemGroup = ItemGroup[0].selectize;
                selectizeItemGroup.destroy()
                var v = '';
                if (response.ItemGroupUOM != null && response.ItemGroupUOM != "") {
                    var l = response.ItemGroupUOM.length;
                    if (l > 1)
                        v = "<option value>Select</option>";
                    $.each(response.ItemGroupUOM, function (i, v1) {
                        if (selected == v1.t_citg)
                            v += "<option selected='selected' value='" + v1.t_citg + "'>" + v1.t_deca + "</option>";
                        else
                            v += "<option value='" + v1.t_citg + "'>" + v1.t_deca + "</option>";
                    });
                }
                $("#ItemGroup").html(v);
                $("#ItemGroup").selectize()
            },
            error: function (response) {

            }
        });
    } else {
        var ItemGroup = $('#ItemGroup').selectize();
        var selectizeItemGroup = ItemGroup[0].selectize;
        selectizeItemGroup.destroy()
        var v = '';
        v = "<option value>Select</option>";
        $("#ItemGroup").html(v);
        $("#ItemGroup").selectize();
    }
}


$("#ItemKeyType").change(function () {
    if ($("#ItemKeyType").val() == AutoGenerate) {
        $("#ItemName").attr("readonly", "readonly");
        $("#ItemKey").attr("readonly", "readonly");
        $("#ItemKey").attr("tabindex", "-1");
        var id = $("#ItemId").val() || "0";
        if (parseFloat(id) == 0) {
            $("#ItemKey").val("");
        }
    }
    else if ($("#ItemKeyType").val() == AutoGenerate_FindNo) {
        $("#ItemKey").attr("readonly", "readonly");
        $("#ItemKey").attr("tabindex", "-1");
        var id = $("#ItemId").val() || "0";
        if (parseFloat(id) == 0) {
            $("#ItemKey").val("");
        }
    }
    else if ($("#ItemKeyType").val() == UserDefined) {
        $("#ItemKey").removeAttr("readonly");
        $("#ItemKey").removeAttr("tabindex");
        $("#ItemName").removeAttr("readonly");
    }
});

$("#ItemKey").on("focusout", function () {

    var keyattr = $("#ItemKey").attr('readonly') || "";

    if ($("#ItemKey").val() != "" && keyattr == "") {
        mApp.blockPage();
        $.ajax({
            url: '/DES/Part/GetUserDefinedItemKey',
            type: 'GET',
            traditional: true,
            data: { ItemKey: $("#ItemKey").val(), Contract: $("#Contract").val() },
            contentType: 'application/json; charset=utf-8',
            dataType: "json",
            success: function (result) {
                mApp.unblockPage();


                if (result != '' && result != null) {


                    $("#ItemId").val(result.ItemId);

                    var ItemKeyType = $('#ItemKeyType').selectize();
                    var selectizeItemKeyType = ItemKeyType[0].selectize;
                    selectizeItemKeyType.setValue(result.ItemKeyType, false);
                    var OrderPolicy = $('#OrderPolicy').selectize();
                    var selectizeOrderPolicy = OrderPolicy[0].selectize;
                    selectizeOrderPolicy.setValue(result.OrderPolicy, false);

                    var ProductFormCodeId = $('#ProductFormCodeId').selectize();
                    var selectizeProductFormCodeId = ProductFormCodeId[0].selectize;
                    selectizeProductFormCodeId.setValue(result.ProductFormCodeId, true);
                    $("#ItemKey").val(result.ItemKey);
                    getTypeByProductForm(result.Type);
                    $("#PolicyId").val(result.PolicyId);
                    $("#Policy").val(result.Policy);
                    $("#RevNo").val(result.RevNo);
                    $("#txtRevNo").val('R' + result.RevNo);

                    var Materialoptions = [{ Value: result.Material, Text: result.Material }]
                    var Material = $('#Material').selectize(Materialxhr);
                    var selectizeMaterial = Material[0].selectize;
                    selectizeMaterial.addOption(Materialoptions);
                    selectizeMaterial.setValue(result.Material, true);

                    GetItemGroup(result.ItemGroup, result.Material)

                    if (parseFloat(result.ProcurementDrgDocumentId) > 0) {
                        var ProcurementDrgDocumentIdoptions = [{ Id: result.ProcurementDrgDocumentId, Lookup: result.ProcurementDrgDocumentNo }]
                        var ProcurementDrgDocumentId = $('#ProcurementDrgDocumentId').selectize(ProcurementDrgDocumentxhr);
                        var selectizeProcurementDrgDocumentId = ProcurementDrgDocumentId[0].selectize;
                        selectizeProcurementDrgDocumentId.addOption(ProcurementDrgDocumentIdoptions);
                        selectizeProcurementDrgDocumentId.setValue(result.ProcurementDrgDocumentId, true);
                    } else {
                        var ProcurementDrgDocumentId = $('#ProcurementDrgDocumentId').selectize(ProcurementDrgDocumentxhr);
                        var selectizeProcurementDrgDocumentId = ProcurementDrgDocumentId[0].selectize;
                        selectizeProcurementDrgDocumentId.setValue("", true);
                    }

                    $("#ItemName").val(result.ItemName);
                    $("#String2").val(result.String2);
                    $("#String3").val(result.String3);
                    $("#String4").val(result.String4);
                    $("#HdnString2").val(result.HdnString2);
                    $("#HdnString3").val(result.HdnString3);
                    $("#HdnString4").val(result.HdnString4);

                    if (String(result.ARMSet || "") != "") {
                        var ARMSetoptions = [{ Arms: result.ARMSet, Desc: result.ARMSet }]
                        var ARMSet = $('#ARMSet').selectize(armxhr);
                        var selectizeARMSet = ARMSet[0].selectize;
                        selectizeARMSet.addOption(ARMSetoptions);
                        selectizeARMSet.setValue(result.ARMSet, true);
                        $("#ARMRev").val(result.ARMRev);
                    } else {
                        var ARMSet = $('#ARMSet').selectize(armxhr);
                        var selectizeARMSet = ARMSet[0].selectize;
                        selectizeARMSet.setValue("", true);
                        $("#ARMRev").val("");
                    }

                    if (String(result.SizeCode || "") != "") {
                        var SizeCodeoptions = [{ SizeCode: result.SizeCode, SizeCode: result.SizeCode }]
                        var SizeCode = $('#SizeCode').selectize(SizeCodexhr);
                        var selectizeSizeCode = SizeCode[0].selectize;
                        selectizeSizeCode.addOption(SizeCodeoptions);
                        selectizeSizeCode.setValue(result.SizeCode, true);
                    } else {
                        var SizeCode = $('#SizeCode').selectize(SizeCodexhr);
                        var selectizeSizeCode = SizeCode[0].selectize;
                        selectizeSizeCode.setValue("", true);
                    }


                    $("#Thickness").val(result.Thickness);
                    $("#CladPlatePartThickness1").val(result.CladPlatePartThickness1);
                    $("#CladSpecificGravity1").val(result.CladSpecificGravity1);
                    var isDbl = (result.IsDoubleClad || "No");
                    var IsDoubleClad = $("#IsDoubleClad").selectize();
                    var selectizeIsDoubleClad = IsDoubleClad[0].selectize;
                    selectizeIsDoubleClad.setValue(isDbl, false);

                    $("#CladPlatePartThickness2").val(result.CladPlatePartThickness2);
                    $("#CladSpecificGravity2").val(result.CladSpecificGravity2);
                    $("#ItemWeight").val(result.ItemWeight);
                    $("#UOM").val(result.UOM);
                    $("#CRSYN").val(result.CRSYN);
                    $("#Density").val(result.Density);
                    $("#WeightFactor").val(result.WeightFactor);
                    $("#ManufactureItem").val(result.ManufactureItem);
                    $("#ProductForm").val(result.ProductForm);
                    $("#DoNotReferExistingItemkey").val("false");
                    $("#DoNotCheckProductFormCode").val("false");
                    $("#ActionForPart").val(result.ActionForPart);
                    $("#btnSubmit").val(result.ActionForPart);
                    ItemIdExist();
                }
            },
            error: function (xhr) {
                mApp.unblockPage();
            }
        });
    }
});

function getDecOfARM() {
    var aid = $("#ARMSet").val();
    var docMapp = {
        Id: $("#ARMSet").val()
    };
    if (aid != "") {
        $.ajax({
            type: "GET",
            url: "/DES/Part/GetARMSetDetailsById", //If get File Then Document Mapping Add
            data: docMapp,
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            aysnc: true,
            success: function (response) {

                $('#ARMRev').val(response.Vrsn);
            },
            error: function (response) {

            }
        });
    } else {
        $('#ARMRev').val("");
    }
}



$("#ProcurementDrgDocumentId").change(function () {
    if ($("#ProcurementDrgDocumentId").val() != "") {
        $("#String2").val("");
        $("#String3").val("");
        $("#String4").val("");
        var v = $("#ProcurementDrgDocumentId option:selected").text();
        v = "Refer " + v;
        $("#String2").val(v);
    } else {
        getStringByProductForm();
    }
});

$("#String2, #String3, #String4").on("keyup", function (e) {
    var code = e.keyCode || e.which;
    if (String(code) != '9') {
        if (String(this.id) == "String2")
            $("#HdnString2").val("");
        if (String(this.id) == "String3")
            $("#HdnString3").val("");
        if (String(this.id) == "String4")
            $("#HdnString4").val("");

        CalcAll();
        BOMReportDrawingStrChange();
    }

});



$("#Thickness,#ProcurementDrgDocumentId,#CladPlatePartThickness1,#CladPlatePartThickness2,#WeightFactor,#ItemWeight,#CladSpecificGravity2").on("keyup change", function () {
    CalcAll();
    BOMReportDrawingStrChange();
});





function CalcAll() {
    $("#ItemWeight").removeAttr("title");
    $("#ItemWeight").removeAttr("readonly");
    $("#ItemWeight").removeAttr("tabindex");

    var UOM = $("#UOM").val();
    var type = $("#Type").val();




    var Density = parseFloat($("#Density").val() || 0);
    var Thickness = parseFloat($("#Thickness").val() || 0);
    var CladPlatePartThickness1 = parseFloat($("#CladPlatePartThickness1").val() || 0);
    var CladPlatePartThickness2 = parseFloat($("#CladPlatePartThickness2").val() || 0);
    var WeightFactor = parseFloat($("#WeightFactor").val() || 0);
    var isdou = $("#IsDoubleClad").val();



    if (type == Plate_Part) {
        //NumberOfPieces = JobQty;
        // $("#BOMModel_NumberOfPieces").val(JobQty);
        if (UOM == "kg") {
            var PartWeight = parseFloat(Thickness * Density).toFixed(2);
            $("#ItemWeight").val(parseFloat(PartWeight).toFixed(2));
            $("#ItemWeight").attr("readonly", "readonly");
            $("#ItemWeight").attr("tabindex", "-1");
        }

    }
    else if (type == Clad_Part) {
        if (UOM == "kg") {
            var CladSpecificGravity1 = parseFloat($("#CladSpecificGravity1").val() || 0);
            var CladSpecificGravity2 = parseFloat($("#CladSpecificGravity2").val() || 0);

            var PartWeight = parseFloat((Thickness * Density) + (CladPlatePartThickness1 * CladSpecificGravity1)).toFixed(2);
            if (isdou == "Yes") {
                PartWeight = parseFloat(parseFloat(PartWeight) + parseFloat((CladPlatePartThickness2 * CladSpecificGravity2))).toFixed(2);
            }


            $("#ItemWeight").val(parseFloat(PartWeight).toFixed(2));
            $("#ItemWeight").attr("readonly", "readonly")
            $("#ItemWeight").attr("tabindex", "-1");
        }

    }
    else if (type == Part) {
        //$("#BOMModel_NumberOfPieces").val("");
        //NumberOfPieces = "";
        if (UOM == "m") {
            $("#ItemWeight").attr("title", "Note that the user has to enter kg/m in weight of the part");

            if (WeightFactor > 0) {
                var PartWeight = parseFloat(WeightFactor * Density).toFixed(2);
                $("#ItemWeight").val(parseFloat(PartWeight).toFixed(2));
                $("#ItemWeight").attr("readonly", "readonly")
                $("#ItemWeight").attr("tabindex", "-1");
            }


        }
        else if (UOM == "m2") {
            $("#ItemWeight").attr("title", "Note that the user has to enter kg/m2 in weight of the part");


        } else if (UOM == "kg") {
            $("#ItemWeight").attr("title", "Note that the user has to enter kg/m in weight of the part");

            if (WeightFactor > 0) {
                var PartWeight = parseFloat(WeightFactor * Density).toFixed(2);
                $("#ItemWeight").val(parseFloat(PartWeight).toFixed(2));
                $("#ItemWeight").attr("readonly", "readonly")
                $("#ItemWeight").attr("tabindex", "-1");
            }

        }



    }
    else if (type == Jigfix) {
        //$("#BOMModel_NumberOfPieces").val("");
        //NumberOfPieces = "";
    }



}


function BOMReportDrawingStrChange() {
    if ($("#ProcurementDrgDocumentId").val() == "") {

        var s1 = $("#HdnString2").val() || "";
        var s2 = $("#HdnString3").val() || "";
        var s3 = $("#HdnString4").val() || "";
        var ProcDrgNo = $("#ProcurementDrgDocumentId option:selected").text() || "";

        if (PartFixList != null && PartFixList.length > 0) {
            for (var i = 0; i < PartFixList.length; i++) {
                var data = $("#" + PartFixList[i].Description).val() || "";
                if (data != "") {
                    var regExp = new RegExp(PartFixList[i].Lookup, 'gi');
                    s1 = String(s1).replace(regExp, data);
                    s2 = String(s2).replace(regExp, data);
                    s3 = String(s3).replace(regExp, data);
                }
            }
        }
        if (ProcDrgNo != "") {
            s1 = String(s1).replace(/#ProcDrgNo#/gi, ProcDrgNo);
            s2 = String(s2).replace(/#ProcDrgNo#/gi, ProcDrgNo);
            s3 = String(s3).replace(/#ProcDrgNo#/gi, ProcDrgNo);
        }
        if (s1 != "")
            $("#String2").val(s1);
        if (s2 != "")
            $("#String3").val(s2);
        if (s3 != "")
            $("#String4").val(s3);



    }
}







//End--
var funPartSuccess = function (data) {

    if (data.Status) {
        location.href = data.RedirectAction;
    } else {
        mApp.unblockPage();
        if (data.ConfirmExistingItemkey) {
            toastr.info(data.Msg, "Info")
            BindExistingItemkeyData(data.ExistingItemkeyData)

        } else if (data.ConfirmProductFormCode) {
            swal({
                backdrop: false,
                title: data.Msg,
                type: "info",
                allowOutsideClick: false,
                showConfirmButton: true,
                showCancelButton: true,
                confirmButtonClass: "btn btn-danger",
                cancelButtonClass: "btn btn-secondary",
                confirmButtonText: "Yes",
                cancelButtonText: "No"
            }).then(
                function (isConfirm) {
                    if (isConfirm.value) {
                        $("#DoNotCheckProductFormCode").val("true");
                        $("#FormPart").submit();
                    }
                });
        } else if (data.RevisionConfirm) {
            swal({
                backdrop: false,
                title: data.Msg,
                type: "info",
                allowOutsideClick: false,
                showConfirmButton: true,
                showCancelButton: true,
                confirmButtonClass: "btn btn-danger",
                cancelButtonClass: "btn btn-secondary",
                confirmButtonText: "Yes",
                cancelButtonText: "No"
            }).then(
                function (isConfirm) {
                    if (isConfirm.value) {
                        $("#ConfirmRevision").val("true");
                        $("#FormPart").submit();
                    }
                });
        } else {
            toastr.info(data.Msg, "Info")
        }

    }
}

var funPartBegin = function () {
    $("#FormPart").valid();

    if ($("#ItemKeyType").val() == UserDefined) {
        if ($("#ItemKey").val() == "") {
            $('span[data-valmsg-for="ItemKey"]').html("Item Key is required")
            $("#ItemKey").focus();
            return false;
        }
    }

    if ($("#ItemKeyType").val() == AutoGenerate) {
        if ($("#OrderPolicy").val() == Standard) {
            toastr.info("For Standard part you can not use Auto Generate Item Id Type", "Info")
            return false;
        }
        if (parseInt($("#ManufactureItem").val()) == parseInt(Make)) {
            toastr.info("For Manufacture Item you can not use Auto Generate Item Id Type", "Info")
            return false;
        }
    }

    mApp.blockPage();
    return true;
}
var tblExistingItemkeyData;
function BindExistingItemkeyData(data) {
    var htmltable = "";
    if (tblExistingItemkeyData != null) {
        tblExistingItemkeyData.destroy();
    }
    $("#tbodyExistingItemkeyData").html("");
    if ($("#ItemKeyType").val() == UserDefined)
        $("#modelFooterExistingItemkeyData").show();
    else
        $("#modelFooterExistingItemkeyData").hide();
    $("#ModalExistingItemkeyData").modal("show");
    if (data.length > 0) {
        for (var i = 0; i < data.length; i++) {
            htmltable += "<tr>"
            htmltable += "<td>"
            htmltable += '<a  href="/DES/Part/EditItem?q=' + data[i].BString + '" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Edit">\n<i class="la la-edit"></i>\n</a>';
            htmltable += ' <a href="/DES/Part/Detail?q=' + data[i].QString + '" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="View">\n                          <i class="la la-eye m--font-success"></i>\n                        </a>';
            htmltable += "</td>"
            htmltable += '<td>' + data[i].Type + '</td>'
            htmltable += "<td><a  href='/DES/Part/PowerView?q=" + data[i].QString + "'>" + data[i].ItemKey + "</a></td>";
            htmltable += '<td>R' + data[i].RevNo + '</td>'
            htmltable += '<td>' + (data[i].Material || "") + '</td>'
            htmltable += '<td>' + (data[i].ItemGroup || "") + '</td>'
            htmltable += '<td>' + (data[i].UOM || "") + '</td>'
            htmltable += '<td>' + (data[i].ItemName || "") + '</td>'
            htmltable += '<td>' + (data[i].String2 || "") + '</td>'
            htmltable += '<td>' + (data[i].String3 || "") + '</td>'
            htmltable += '<td>' + (data[i].String4 || "") + '</td>'
            htmltable += '<td>' + (data[i].ARMSet || "") + '</td>'
            htmltable += '<td>' + (data[i].ARMRev || "") + '</td>'
            htmltable += '<td>' + (data[i].ProcurementDrgDocumentNo || "") + '</td>'
            htmltable += '<td>' + (data[i].ItemWeight || "") + '</td>'
            htmltable += '<td>' + (data[i].CRSYN || "") + '</td>'
            htmltable += '<td>' + (data[i].SizeCode || "") + '</td>'
            htmltable += '<td>' + (data[i].WeightFactor || "") + '</td>'
            htmltable += '<td>' + (data[i].Thickness || "") + '</td>'
            htmltable += '<td>' + (data[i].CladPlatePartThickness1 || "") + '</td>'
            htmltable += '<td>' + (data[i].IsDoubleClad || "") + '</td>'
            htmltable += '<td>' + (data[i].ProductForm || "") + '</td>'
            htmltable += '<td>' + (data[i].ItemKeyType || "") + '</td>'
            htmltable += '<td>' + (data[i].OrderPolicy || "") + '</td>'
            htmltable += '<td>' + (data[i].CreatedBy || "") + '</td>'
            htmltable += '<td>' + (data[i].CreatedOn || "") + '</td>'
            htmltable += '<td>' + (data[i].EditedBy || "") + '</td>'
            htmltable += '<td>' + (data[i].EditedOn || "") + '</td>'
            htmltable += "</tr>"
        }

    }
    $("#tbodyExistingItemkeyData").html(htmltable);
    tblExistingItemkeyData = $("#tblExistingItemkeyData").DataTable({ "scrollX": true });
    setTimeout(function () { tblExistingItemkeyData.columns.adjust(); }, 2000)
}
function ContinueWithNewItemId() {
    $("#DoNotReferExistingItemkey").val("true");
    $("#FormPart").submit();
}


function GetMatrialdata(selected, typeselected, ProductFormCodeId) {

    if (ProductFormCodeId != "") {
        mApp.blockPage();
        $.ajax({
            type: "GET",
            url: "/DES/Part/GetProductFomDesc",
            data: { productFormId: ProductFormCodeId },
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            aysnc: true,
            success: function (response) {
                mApp.unblockPage();
                var v = '';
                $.each(response.MaterialList, function (i, v1) {
                    if (selected == v1.Value)
                        v += "<option selected='selected' value='" + v1.Value + "'>" + v1.Text + "</option>";
                    else
                        v += "<option value='" + v1.Value + "'>" + v1.Text + "</option>";
                });

                $("#Material").html(v);
                $("#Material").select2({
                    placeholder: "Select"
                });
                var v = '';
                $.each(response.TypeList, function (i, v1) {
                    if (typeselected == v1.Value)
                        v += "<option selected='selected' value='" + v1.Value + "'>" + v1.Text + "</option>";
                    else
                        v += "<option value='" + v1.Value + "'>" + v1.Text + "</option>";
                });

                $("#Type").html(v);
                $("#Type").change();
            },
            error: function (response) {
                mApp.unblockPage();
            }
        });
    } else {
        $('#String2').val("");
        $('#String3').val("");
        $('#String4').val("");
        $('#HdnString2').val("");
        $('#HdnString3').val("");
        $('#HdnString4').val("");
        $("#BOMModel_BomReportSize").val("");
        $("#BOMModel_DrawingBomSize").val("");
        $("#BOMModel_HdnBomReportSize").val("");
        $("#BOMModel_HdnDrawingBomSize").val("");
        $('#ItemName').val("")
        var v = '';
        $("#Material").html(v);
        $("#Material").select2({
            placeholder: "Select"
        });
        $("#Type").html(v);
        $("#Type").change();
    }
}









//$('#SizeCode').change(function () {
//    getSizeCode();
//});



// On Arm Change Get Description
//$("#ARMSet").change(function () {
//    getDecOfARM();
//});

//Get Policy

//Get Description Of Arm




function AllMaterial() {
    var val = $('#Material').val();
    var productfrm = $('#ProductFormCodeId').val();
    if (productfrm != "") {
        mApp.blockPage();
        $.ajax({
            url: '/DES/Part/GetAllMaterial',
            type: 'GET',
            data: { Val: val, ProductFormCodeId: productfrm },
            traditional: true,
            contentType: 'application/json; charset=utf-8',
            dataType: "Html",
            success: function (result) {
                mApp.unblockPage();
                $("#modelbodyMaterial").html(result);
                $("#m_modal_2_4").modal("show");
            },
            error: function (xhr) {
                mApp.unblockPage();
            }
        });
    }
    else {
        mApp.unblockPage();
        toastr.info("Please select ProductForm", "Info");
    }
}

function btnMaterialCopy($this) {
    var id = $($this).attr('data-id');
    var desc = $($this).attr('data-desc') || "";
    var IGroup = $($this).attr('data-IGroup') || "";
    if (desc == "") {
        desc = id;
    } else {
        desc = desc + " (" + id + ")";
    }
    var Materialoptions = [{ Value: id, Text: desc }]
    var Material = $('#Material').selectize(Materialxhr);
    $('#HidenItemGrouop').val(IGroup);
    var selectizeMaterial = Material[0].selectize;
    selectizeMaterial.addOption(Materialoptions);
    selectizeMaterial.setValue(id, false);

    $("#m_modal_2_4").modal("hide");
}

function AllItemGrp() {
    var val = $('#ItemGroup').val();
    var material = $('#Material').val();
    if (material != "") {
        mApp.blockPage();
        $.ajax({
            url: '/DES/Part/GetAllItemGroup',
            type: 'GET',
            data: { Val: val, MaterialId: material },
            traditional: true,
            contentType: 'application/json; charset=utf-8',
            dataType: "Html",
            success: function (result) {
                mApp.unblockPage();
                $("#modelbodyItemGrp").html(result);
                $("#m_modal_2_2").modal("show");
            },
            error: function (xhr) {
                mApp.unblockPage();
            }
        });
    }
    else {
        mApp.unblockPage();
        toastr.info("Please select Material", "Info");
    }
}

function btnItemGroupCopy($this) {
    var id = $($this).attr('data-id');

    var ItemGroup = $('#ItemGroup').selectize();
    var selectizeItemGroup = ItemGroup[0].selectize;
    selectizeItemGroup.setValue(id, false)
    $("#m_modal_2_2").modal("hide");
}

function AllARMSet() {
    var val = $('#ARMSet').val();
    mApp.blockPage();
    $.ajax({
        url: '/DES/Part/GetAllARMSet',
        type: 'GET',
        data: { Val: val },
        traditional: true,
        contentType: 'application/json; charset=utf-8',
        dataType: "Html",
        success: function (result) {
            mApp.unblockPage();
            $("#modelbodyARMCode").html(result);
            $("#m_modal_2_1").modal("show");
        },
        error: function (xhr) {
            mApp.unblockPage();
        }
    });
}

function btnARMSetCopy($this) {
    var id = $($this).attr('data-id');
    var ARMSetoptions = [{ Arms: id, Desc: id }]
    var ARMSet = $('#ARMSet').selectize(armxhr);
    var selectizeARMSet = ARMSet[0].selectize;
    selectizeARMSet.addOption(ARMSetoptions);
    selectizeARMSet.setValue(id, false);
    $("#m_modal_2_1").modal("hide");
}

function AllSizeCode() {
    var val = $('#SizeCode').val();
    mApp.blockPage();
    $.ajax({
        url: '/DES/Part/GetAllSize',
        type: 'GET',
        data: { Val: val },
        traditional: true,
        contentType: 'application/json; charset=utf-8',
        dataType: "Html",
        success: function (result) {
            mApp.unblockPage();
            $("#modelbodySizeCode").html(result);
            $("#m_modal_2_3").modal("show");
        },
        error: function (xhr) {
            mApp.unblockPage();
        }
    });
}

function btnSizeCodeCopy($this) {
    var id = $($this).attr('data-id');

    var SizeCodeoptions = [{ SizeCode: id, SizeCode: id }]
    var SizeCode = $('#SizeCode').selectize(SizeCodexhr);
    var selectizeSizeCode = SizeCode[0].selectize;
    selectizeSizeCode.addOption(SizeCodeoptions);
    selectizeSizeCode.setValue(id, false);
    $("#m_modal_2_3").modal("hide");
}


function ClearCache() {
    mApp.blockPage()
    $.ajax({
        type: "GET",
        url: "/DES/Part/ClearCache", //If get File Then Document Mapping Add
        data: {},
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        aysnc: true,
        success: function (response) {
            mApp.unblockPage();
            toastr.success("Cache clear successfully.", "Success");
        },
        error: function (response) {
            mApp.unblockPage();
        }
    });

}

