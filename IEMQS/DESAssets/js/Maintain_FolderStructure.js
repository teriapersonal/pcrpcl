﻿
$(document).ready(function () {
    $("#BUList").focus();
    $("#BUList")[0].selectedIndex = 0;
    $("#displayProjectId").hide();
});

var passParentFolderId;

function funCreateNewFolderModal(EditModeFlag, $this) {
    var Level = $($this).attr("data-level");
    if (Level == "Level0") {
        passParentFolderId = "";
    }

    var valEditModeOnFlag = false;

    if (EditModeFlag == 2) {
        valEditModeOnFlag = true;
    }
    if (passParentFolderId == undefined || passParentFolderId == NaN) {
        passParentFolderId = "";
    }
    mApp.blockPage();

    $.ajax({
        type: "GET",
        url: "/DES/Parameter/_partialCreateNewFolder",
        dataType: "html",
        data: { "BUId": $('#BUList').val(), "ParentFolderId": parseInt(passParentFolderId), "EditModeOnFlag": valEditModeOnFlag },
        success: function (data) {
            mApp.unblockPage();
            $('#myModalContent').html(data);
            if (EditModeFlag == 1) {
                $('#ParentFolderId').val(parseInt(passParentFolderId));
                $("#FolderId").val("");
                $("#FolderName").val("");
                $("#Description").val("");
                $("#exampleModalLabel").text("Create New Folder");
                if ($("#ParentFolderId").val() > 0) {
                    $("#divFunction").hide();
                }
            }
            else if (EditModeFlag == 2) {
                $("#exampleModalLabel").text("Edit Folder");
                if ($("#FunctionIds").val() != "") {
                    var selectedValuesFunctionId = ($("#FunctionIds").val()).split(',');
                    var Funids = [];
                    for (var i = 0; i < selectedValuesFunctionId.length; i++) {
                        Funids.push(String(selectedValuesFunctionId[i].trim()));
                    }
                    $('#RoleGroupList').select2();
                    $('#RoleGroupList').val(Funids).trigger('change');
                }
                else {
                    $("#divFunction").hide();
                }
            }

            $('#BUId').val($('#BUList').val());
            $('#m_select2_modal').modal('show');
            setTimeout(function () {
                $("#FolderName").focus();
            }, 1000);

        },
        error: function () {
            mApp.unblockPage();
            toastr.error("Error occured!!", "Error");
        }
    });

}

$("#BUList").change(function () {
    $('#displayProjectId').show();
    var valBUId = $('#BUList').val();

    if (valBUId != undefined && valBUId != NaN && valBUId != '') {
        $('#BUId').val(valBUId);
        mApp.blockPage();
        $.ajax({
            type: "POST",
            url: "/DES/Parameter/_partialFolderTreeView",
            data: { "BUId": parseInt(valBUId) },
            dataType: "html",
            async: false,
            success: function (data) {
                mApp.unblockPage();
                $("#m_tree_View").html(data);
            },
            error: function () {
                mApp.unblockPage();
                toastr.error("Error occured!!", "Error")
            }
        });
    }
    else {
        $("#displayProjectId").hide();
        $("#m_tree_2").hide();
    }
});

function getValueUsingClass() {
    var chkArray = [];
    $("#getCategoryList:checked").each(function () {  //#getCategoryList:checked"
        chkArray.push($(this).val());
    });
    var selectedCategoryIds;
    selectedCategoryIds = chkArray.join(',');
    $("#CategoryIds").val(selectedCategoryIds);

    var chkArray = [];
    $("#getActionList:checked").each(function () {  //#getActionList:checked"
        chkArray.push($(this).val());
    });
    var selectedActionIds;
    selectedActionIds = chkArray.join(',');
    $("#ActionIds").val(selectedActionIds);

    var selectedFunctionIds;
    selectedFunctionIds = JSON.stringify($("#selectize").val());
    $("#FunctionIds").val(selectedFunctionIds);
}

function funDelete() {
    swal({
        title: "Do you want to delete..?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
    }).then(
        function (isConfirm) {
            if (isConfirm.value) {
                mApp.blockPage();
                $.ajax({
                    cache: false,
                    type: "GET",
                    url: "/DES/Parameter/DeleteFolder",
                    data: { Id: parseInt(passParentFolderId) },
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        mApp.unblockPage();
                        toastr.success(data.Msg, "Success");
                        $("#BUList").change();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        mApp.unblockPage();
                        toastr.error(data.Msg, "Error");
                    }
                });
            } else {
            }
        });






    //debugger
    //swal({
    //    title: "Are you sure you want to delete this record?",
    //    type: "warning",
    //    showCancelButton: true,
    //    confirmButtonText: "Yes",
    //    confirmButtonClass: "btn btn-danger",
    //}, function () {
    //    $.ajax({
    //        type: "GET",
    //        url: '/DES/Parameter/DeleteFolder',
    //        data: { "Id": parseInt(passParentFolderId) },
    //        dataType: 'json',
    //        contentType: "application/json;charset=utf-8",
    //        async: true,
    //        success: function (response) {
    //            if (response.Status) {
    //                toastr.success(response.Msg, "Success");
    //            } else {
    //                toastr.error(response.Msg, "Error");
    //            }
    //        },
    //        error: function (error) {
    //            toastr.error("something went wrong try again later", "Error");
    //        }
    //    })
    //});
}

