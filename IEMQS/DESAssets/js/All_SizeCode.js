﻿var tblAllSizeCode;
$(document).ready(function () {

    tblAllSizeCode = $('#tblAllSizeCode').DataTable({
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": false,
        "sAjaxSource": "/DES/Part/GetAllSizeCodeList",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        "responsive": false,
        "language": {
            "infoFiltered": ""
        },
        //"scroller": {
        //    loadingIndicator: true
        //},
        //"bSort": true,
        //"aaSorting": [[1, 'desc']],
        "order": [[1, 'desc']],
        "columnDefs": [
            { "orderable": false, "targets": 0 }
            //{ "orderable": false, "targets": 5 },
            //{ "orderable": false, "targets": 6 },
            //{ "orderable": false, "targets": 7 },
            //{ "orderable": false, "targets": 8 }
        ],

        "fnServerData": function (sSource, data, fnCallback) {
            data.push({ name: "searchsizecode", value: $("#txtsearchsizecode").val() });
            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });

        },
        "aoColumns": [
            {
                "sTitle": "", "className": "", render: function (data, type, row, meta) {
                    var srt = "";
                    srt += '<a href="javascript:;" onclick="btnSizeCodeCopy(this)" data-id="' + row.SizeCode + '"><i class="fa fa-link m--font-accent" title="Select"></i> </a>';
                    return srt;
                }
            },
            {
                "sTitle": "SizeCode", "className": "", render: function (data, type, row, meta) {
                    return row.SizeCode;
                }
            },           
            {
                "sTitle": "String-2", "className": "", render: function (data, type, row, meta) {
                    return row.Description1;
                }
            },
            {
                "sTitle": "String-3", "className": "", render: function (data, type, row, meta) {
                    return row.Description2;
                }
            },
            {
                "sTitle": "String-4", "className": "", render: function (data, type, row, meta) {
                    return row.Description3;
                }
            },
            {
                "sTitle": "Weight Factor", "className": "", render: function (data, type, row, meta) {
                    return row.WeightFactor;
                }
            }            
        ],

        "initComplete": function () {

        },

        "fnRowCallback": function () {

        }
    });
    setTimeout(function () {
        tblAllSizeCode.columns.adjust(); 
    }, 500);
});



