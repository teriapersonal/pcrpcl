﻿var selectedDocIDs = [];
var FinalDoclist = [];
var tblJepDocumentDetails;
var tblJepDoc;
var tblGlobalJepDoc;
//Wizard
var WizardDemo = function () {
    $("#m_wizard");
    var e, r, i = $("#m_form"), f = $("#FormDINAttr");
    return {
        init: function () {
            var n;
            $("#m_wizard"), i = $("#m_form"), (r = new mWizard("m_wizard", {
                startStep: 1
            })).on("beforeNext", function (r) {
                //!0 !== e.form() && r.stop()

            }), r.on("change", function (e) {
                mUtil.scrollTop()
            }), r.on("change", function (e) {
                1 === e.getStep();

            })
        }

        , Gotonext: function () {
            r.goNext();
        }
        , GotoBack: function () {
            r.goPrev();
            e.goNext();
        }
        , GoTo: function (i) {
            if ($("#DINId").val() != "") {
                r.goTo(i);
                BindJepDocumentDetails();
                BindJEPDoc();
                BindGlobalJEPDoc();
                referesh();
            }
        }

    }
}();

$(document).ready(function () {
    WizardDemo.init();
    // BindJepDocumentDetails();
});



$('#openPop').click(function () {


    var selectchbHeaderPart = $("#chbHeader").prop("checked");
    if (selectchbHeaderPart) {
        $("#chbHeader").prop("checked", false);
    }

    var selectchbHeaderParts = $("#chbHeaderGlobalDoc").prop("checked");
    if (selectchbHeaderParts) {
        $("#chbHeaderGlobalDoc").prop("checked", false);
    }


    selectedDocIDs = [];
    FinalDoclist = [];


    $("#txtProjectNo").val('');
    $("#txtDocumentNo").val('');

    setTimeout(function () {
        tblJepDoc.columns.adjust();
    }, 1000);
});

$('#globalTab').click(function () {
    setTimeout(function () {
        tblGlobalJepDoc.columns.adjust();
    }, 1000);
});

function ajaxValidate() {
    mApp.blockPage();
}
//If AJAX.Begin Success then
funSuccess = function (data) {

    if (data.Status) {
        $('#DINId').val(data.status);
        $("#DINNo").prop("readonly", true);
        toastr.success(data.Msg, "Success");
        BindJepDocumentDetails();
        BindJEPDoc();
        BindGlobalJEPDoc();
        referesh();
        WizardDemo.Gotonext();
        mApp.unblockPage();
    }
    else {
        toastr.error(data.Msg, "Error");
        mApp.unblockPage();
    }
}

function referesh() {
    setTimeout(function () {
        tblJepDocumentDetails.columns.adjust();
    }, 1000);

}

//Bind JEP
function BindJEPDoc() {
    mApp.blockPage();
    if (tblJepDoc == null) {
        tblJepDoc = $('#tblJepDoc').DataTable({
            "serverSide": true,
            "processing": true,
            "orderMulti": false,
            "filter": true,
            "sAjaxSource": "/DES/DIN/GetJEPDocList",
            "iDisplayLength": 10,
            "ordering": true,
            "scrollX": true,
            "responsive": false,
            "language": {
                "infoFiltered": ""
            },
            "bRetrieve": true,
            "order": [[0, 'desc']],
            "columnDefs": [

            ],

            "fnServerData": function (sSource, data, fnCallback) {
                data.push({ name: "DINId", value: $("#DINId").val() });
                $.ajax({
                    "dataType": "json",
                    "type": "POST",
                    "url": sSource,
                    "data": data,
                    "success": fnCallback
                });

            },
            "aoColumns": [
                {
                    "sTitle": "<label class='m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--success'><input type='checkbox' id='chbHeader' onclick='SelectAllJEPDoc()'></input><span></span></label>", "sDefaultContent": "<input type='checkbox' ></input>", "bSortable": false, "className": "action", "orderable": false, render: function (data, type, row, meta) {
                        var srt = '<label class="m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--success">'
                        var IDIndex = selectedDocIDs.indexOf(row.JEPDocumentDetailsId);
                        if (IDIndex !== -1) {
                            srt += '<input type="checkbox" class="m-checkable" id="IsSelect_' + row.JEPDocumentDetailsId + '"  data-id="' + row.JEPDocumentDetailsId + '" onclick="AddToDocList(this)" checked>';
                        }
                        else {
                            srt += '<input type="checkbox" class="m-checkable" id="IsSelect_' + row.JEPDocumentDetailsId + '"  data-id="' + row.JEPDocumentDetailsId + '" onclick="AddToDocList(this)">';
                        }
                        srt += "<span></span></label>";
                        return srt;
                    }
                },
                {
                    "sTitle": "Doc No", "className": "", render: function (data, type, row, meta) {
                        return row.DocNo;
                    }
                },
                {
                    "sTitle": "Revision", "className": "", render: function (data, type, row, meta) {
                        if (row.RevisionNo != null) {
                            return "R" + row.RevisionNo;
                        }
                        else {
                            return "";
                        }
                    }
                },
                {
                    "sTitle": "Description", "className": "", render: function (data, type, row, meta) {
                        return row.DocDescription;
                    }
                },
                //{
                //    "sTitle": "Status", "className": "", render: function (data, type, row, meta) {
                //        return row.Status;
                //    }
                //},
                {
                    "sTitle": "Planned Submit Date", "className": "text-left", render: function (data, type, row, meta) {
                        return row.PlannedDate;
                    }
                }
            ],

            "initComplete": function () {

                mApp.unblockPage();
            },

            "fnRowCallback": function () {
                setTimeout(function () {
                    tblJepDoc.columns.adjust();
                }, 1000);

            }

        });
    }
    mApp.unblockPage();
}

//Bind Globel JEP Doc
function BindGlobalJEPDoc() {
    mApp.blockPage();
    tblGlobalJepDoc = $('#tblGlobalJepDoc').DataTable({
        "searching": false,
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "sAjaxSource": "/DES/DIN/GetGlobalJEPDocList",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        "responsive": false,
        "language": {
            "infoFiltered": ""
        },
        "bRetrieve": true,
        "order": [[1, 'desc']],
        "columnDefs": [

        ],

        "fnServerData": function (sSource, data, fnCallback) {
            data.push({ name: "Project", value: $("#ContractProjectList").val() });
            data.push({ name: "DocumentNo", value: $("#txtDocumentNo").val() });
            data.push({ name: "DINId", value: $("#DINId").val() });
            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });

        },
        "aoColumns": [
            {
                "sTitle": "<label class='m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--success'><input type='checkbox' id='chbHeaderGlobalDoc' onclick='SelectAllGlobalJEPDoc()'></input><span></span></label>", "sDefaultContent": "<input type='checkbox' ></input>", "bSortable": false, "className": "action", "orderable": false, render: function (data, type, row, meta) {
                    var srt = '<label class="m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--success">'
                    var IDIndex = selectedDocIDs.indexOf(row.JEPDocumentDetailsId);
                    if (IDIndex !== -1) {
                        srt += '<input type="checkbox" class="m-checkable" id="IsSelect_' + row.JEPDocumentDetailsId + '"  data-id="' + row.JEPDocumentDetailsId + '" onclick="AddToDocList(this)" checked>';
                    }
                    else {
                        srt += '<input type="checkbox" class="m-checkable" id="IsSelect_' + row.JEPDocumentDetailsId + '"  data-id="' + row.JEPDocumentDetailsId + '" onclick="AddToDocList(this)">';
                    }
                    srt += "<span></span></label>";
                    return srt;
                }
            },
            {
                "sTitle": "Doc No", "className": "", render: function (data, type, row, meta) {
                    return row.DocNo;
                }
            },
            {
                "sTitle": "Revision", "className": "", render: function (data, type, row, meta) {
                    return "R" + row.RevisionNo;
                }
            },
            {
                "sTitle": "Description", "className": "text-left", render: function (data, type, row, meta) {
                    return row.DocDescription;
                }
            }
            //{
            //    "sTitle": "Status", "className": "text-left", render: function (data, type, row, meta) {
            //        return row.Status;
            //    }
            //}
        ],

        "initComplete": function () {
            //tblGlobalJepDoc.columns.adjust();
        },

        "fnRowCallback": function () {
            setTimeout(function () {
                tblGlobalJepDoc.columns.adjust();
            }, 1000);
        }


    });
    mApp.unblockPage();
}

//Bind Jep Document Details
function BindJepDocumentDetails() {
    var dinVal = $('#DINId').val();
    mApp.blockPage();
    tblJepDocumentDetails = $('#tblJepDocumentDetails').DataTable({
        "searching": false,
        "serverSide": true,
        "processing": true,
        "orderMulti": false,
        "filter": true,
        "sAjaxSource": "/DES/DIN/GetJepDocumentDetails",
        "iDisplayLength": 10,
        "ordering": true,
        "scrollX": true,
        "responsive": false,
        "language": {
            "infoFiltered": ""
        },
        "bRetrieve": true,
        "order": [[0, 'desc']],
        "columnDefs": [
        ],

        "fnServerData": function (sSource, data, fnCallback) {
            data.push({ name: "DINId", value: dinVal });
            $.ajax({
                "dataType": "json",
                "type": "POST",
                "url": sSource,
                "data": data,
                "success": fnCallback
            });

        },
        "aoColumns": [
            {
                "sTitle": "Action", "className": "action", "orderable": false, render: function (data, type, row, meta) {
                    var srt = "";
                    if (row.Authorized > 0) {
                        //srt += '<a href="javascript:;#"  class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill btnCreateDCR" title="Create DCR"><i class="la la-external-link text-success"></i></a>';

                        if (row.AddeddocStatus != "NotReleased") {
                            srt += '<a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-focus m-btn--icon m-btn--icon-only m-btn--pill btnUV" title="Universal Viewer"> <i class="la la-desktop m--font-focus"></i></a>';
                        }
                        if (row.DINStatus == "Created" || row.DINStatus == "Draft") {
                            srt += ' <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnDelete" title="Delete"><i class="la la-trash text-danger"></i></a>';
                        }
                    }
                    return srt;
                }
            },
            {
                "sTitle": "<i class='fa fa-arrows-alt'></i>&nbsp;&nbsp;&nbsp;", "orderable": false, render: function (data, type, row, meta) {

                    if (row.AddeddocStatus == "NotReleased") {
                        return "";
                    }
                    else {
                        if (row.DocNo != null || row.DocNo != 'undefined') {
                            // return "<i class='fa fa-arrow-up btnRevisionClick' disabled style='color: " + row.colorStatus + " '></i>";
                            if (row.colorStatus == "green") {
                                return "<i class='fa fa-arrow-up btnRevisionClick' title='Latest Revision is already Attached & Carry forward' disabled style='color: " + row.colorStatus + " '></i>";
                            }
                            else if (row.colorStatus == "yellow") {
                                return "<i class='fa fa-arrow-up btnRevisionClick' title='Document is under revision (Not yet Released)' disabled style='color: " + row.colorStatus + " '></i>";
                            }
                            else if (row.colorStatus == "red") {
                                return "<i class='fa fa-arrow-up btnRevisionClick' title='One new revision in Release State exists in IEMQS but not yet added in DIN' disabled style='color: " + row.colorStatus + " '></i>";
                            }
                            else if (row.colorStatus == "purple") {
                                return "<i class='fa fa-arrow-up btnRevisionClick' title='Latest Revision Available in IEMQS' disabled style='color: " + row.colorStatus + " '></i>";
                            }
                        }
                        else {
                            if (row.colorStatus == "green") {
                                return "<i class='fa fa-arrow-up btnRevisionClick' style='cursor:pointer; title='Latest Revision is already Attached & Carry forward' style='color: " + row.colorStatus + " '></i>";
                            }
                            else if (row.colorStatus == "yellow") {
                                return "<i class='fa fa-arrow-up btnRevisionClick' style='cursor:pointer; title='Document is under revision (Not yet Released)' style='color: " + row.colorStatus + " '></i>";
                            }
                            else if (row.colorStatus == "red") {
                                return "<i class='fa fa-arrow-up btnRevisionClick' style='cursor:pointer; title='One new revision in Release State exists in IEMQS but not yet added in DIN' style='color: " + row.colorStatus + " '></i>";
                            }
                            else if (row.colorStatus == "purple") {
                                return "<i class='fa fa-arrow-up btnRevisionClick' style='cursor:pointer; title='Latest Revision Available in IEMQS' style='color: " + row.colorStatus + " '></i>";
                            }
                            //return "<i class='fa fa-arrow-up btnRevisionClick' style='cursor:pointer; color: " + row.colorStatus + " '></i>";
                        }
                    }

                }
            },

            {
                "sTitle": "Serial No", "orderable": false, render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },

            {
                "sTitle": "Doc No", render: function (data, type, row, meta) {
                    //var strHtml = '<i class="flaticon-doc m--font-accent"></i>' + row.DocumentNo + '';
                    //return row.DocNo;
                    var name = "";
                    if (row.DocumentId != null) {
                        //var name = "<a target='_blank' href='/DES/DOC/Detail?q=" + row.QString + "'>" + row.DocNo + "</a>";
                        //var name = "<a target='_blank' href='/DES/DIN/DocumentData?DocumentNo=" + row.DocNo + "&DinID=" + dinVal+"'>" + row.DocNo + "</a>";
                        name = "<a href='javascript:;' onclick = 'RedirectToDocument(\"" + row.DocNo + "\")'>" + row.DocNo + "</a>";
                    } 
                    else {
                         name = row.DocNo;
                    }
                    return name;
                }
            },
            {
                "sTitle": "Rev", "orderable": false, render: function (data, type, row, meta) {
                    if (row.RevisionNo != null) {
                        return "R" + row.RevisionNo;
                    }
                    else {
                        return "";
                    }
                }
            },
            {
                "sTitle": "Document Description", "className": "text-wrap custom-table-width", render: function (data, type, row, meta) {
                    return row.DocDescription;
                }
            },
            {
                "sTitle": "Status", render: function (data, type, row, meta) {
                    var strHtml = '';
                    if (row.AddeddocStatus == "NotReleased") {
                        strHtml = " <label style='color:red;'>Not Relesed</label>";
                    }
                    else if (row.AddeddocStatus == "CarryForward") {
                        strHtml = " <label style='color:green;'>Carry Forward</label>";
                    }
                    else if (row.AddeddocStatus == "Added") {
                        strHtml = " <label style='color:blue;'>Added</label>";
                    }
                    else if (row.AddeddocStatus == "Revised") {
                        strHtml = " <label style='color:red;'>Revised</label>";
                    }
                    else {
                        strHtml = row.AddeddocStatus;
                    }
                    return strHtml;
                }
            },

            //   NotRelesed
            {
                "sTitle": "Remarks&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", "orderable": false, render: function (data, type, row, meta) {
                    var strHtml = '';
                    if (row.GeneralRemarks == null || row.GeneralRemarks == "undifined") {
                        strHtml = "<textarea name='GenralRemark' type='text' id='GenralRemark" + row.DocumentId + "'  class='form-control m-input  m-input--square GenralRemark'   />";
                    }
                    else {
                        strHtml = "<textarea name='GenralRemark'  type='text' id='GenralRemark" + row.DocumentId + "'  class='form-control m-input  m-input--square GenralRemark'  >" + row.GeneralRemarks + "</textarea>";
                    }
                    return strHtml;
                }
            },
            {
                "sTitle": "DocumentId", "orderable": false, visible: false, render: function (data, type, row, meta) {
                    return row.DocumentId;
                }
            },
            {
                "sTitle": "Related DCR", "orderable": false, render: function (data, type, row, meta) {
                    var strHtml = '';
                    var str = [];
                    if (row.DCRNo != null) {
                        var i;
                        var getDCR = row.DCRNo.split(',');
                        for (i = 0; i < getDCR.length; i++) {
                            var getValue = getDCR[i].split('__');
                            // str.push("<a href='/DES/DCR Report/?DCRId=" + getValue[1] + "'>" + getValue[0] + "</a>");
                            str.push('<a href="#" onclick="openDCRReport(\'' + getValue[1] + '\',\'' + getValue[0] + '\')">' + getValue[0] + '</a>');
                        }
                        strHtml = str.join(", ");
                    }
                    else {
                        strHtml = "N/A";
                    }
                    return strHtml;
                }
            },
            {
                "sTitle": "State", "orderable": true, render: function (data, type, row, meta) {
                    return row.DOCStatus;
                }
            },
        ],

        "initComplete": function () {
            setTimeout(function () {
                tblJepDocumentDetails.columns.adjust();
            }, 1000);
            mApp.unblockPage();
        },

        "fnRowCallback": function () {
            setTimeout(function () {
                tblJepDocumentDetails.columns.adjust();
            }, 1000);
            mApp.unblockPage();
        }
    });

    setTimeout(function () {
        tblJepDocumentDetails.columns.adjust();
    }, 1000);
    mApp.unblockPage();
}

function openDCRReport(DCRId, DCRNo) {
    var dat = [];
    dat.push({ Param: "DCRNo", Value: DCRNo });
    ShowReport(dat, "/DES/DCR Report", false, true, 'pdf', DCRNo);
}

function AddToDocList($this) {
    var data;
    var allSelected;
    var DocIdArray;

    var id = $($this).attr("data-id");
    id = parseInt(id);
    var select = $("#IsSelect_" + id).prop("checked");
    var tableID = $("#IsSelect_" + id).closest("table").attr("id");
    var chbID = $("#" + tableID + " th input[type='checkbox']").attr("id");
    if (tableID == "tblJepDoc") {
        data = tblJepDoc.rows().data()[0]["DocIDs"];
        DocIdArray = data.split(',');
    }
    else {
        var data = tblGlobalJepDoc.rows().data()[0]["DocIDs"];
        DocIdArray = data.split(',');
    }
    if (select == true) {
        selectedDocIDs.push(id);
        selectedDocIDs = Array.from(new Set(selectedDocIDs));
        DocIdArray.forEach(function (item) {
            item = parseInt(item);
            if (!selectedDocIDs.includes(item)) {
                allSelected = false;
            }
        });
        if (allSelected != false) {
            $("#" + chbID).prop("checked", true);
        }

    }
    else {
        var tableID = $("#IsSelect_" + id).closest("table").attr("id");
        var chbID = $("#" + tableID + " th input[type='checkbox']").attr("id");
        $("#" + chbID).prop("checked", false);
        var index = selectedDocIDs.indexOf(id);
        if (index > -1) {
            selectedDocIDs.splice(index, 1);
        }
    }
    selectedDocIDs = Array.from(new Set(selectedDocIDs));
}


function SelectAllJEPDoc() {
    var data = tblJepDoc.rows().data()[0]["DocIDs"];
    var DocIdArray = data.split(',');
    var select = $("#chbHeader").prop("checked");
    if (select == true) {
        DocIdArray.forEach(function (item) {
            item = parseInt(item);
            $("#IsSelect_" + item).prop("checked", true);
            selectedDocIDs.push(item);
        });
    }
    else {
        DocIdArray.forEach(function (item) {
            item = parseInt(item);
            $("#IsSelect_" + item).prop("checked", false);
            var index = selectedDocIDs.indexOf(item);
            if (index > -1) {
                selectedDocIDs.splice(index, 1);
            }
        });
    }
    selectedDocIDs = Array.from(new Set(selectedDocIDs));
}

function SelectAllGlobalJEPDoc() {
    var data = tblGlobalJepDoc.rows().data()[0]["DocIDs"];
    var DocIdArray = data.split(',');
    var select = $("#chbHeaderGlobalDoc").prop("checked");
    if (select == true) {
        DocIdArray.forEach(function (item) {
            item = parseInt(item);
            $("#IsSelect_" + item).prop("checked", true);
            selectedDocIDs.push(item);
        });
    }
    else {
        DocIdArray.forEach(function (item) {
            item = parseInt(item);
            $("#IsSelect_" + item).prop("checked", false);
            var index = selectedDocIDs.indexOf(item);
            if (index > -1) {
                selectedDocIDs.splice(index, 1);
            }
        });
    }
}

$("#btnAddDocList").on("click", function () {
    if (selectedDocIDs.length > 0) {
        var FinalDoclist = Array.from(new Set(selectedDocIDs));
        var DocId = new Array();

        for (var id in FinalDoclist) {
            DocId.push({ DOCID: FinalDoclist[id] });
        }
        var model = {};
        model.DocumentIdList = DocId;
        model.DINId = $('#DINId').val();
        mApp.blockPage();
        $.ajax({
            type: "POST",
            url: '/DES/DIN/AddDINDoc',
            data: JSON.stringify({ model: model }),
            dataType: 'json',
            contentType: "application/json;charset=utf-8",
            async: true,
            success: function (response) {
                if (response.Status) {
                    toastr.success(response.Msg, "Success");
                    tblJepDocumentDetails.ajax.reload();
                    tblGlobalJepDoc.ajax.reload();
                    tblJepDoc.ajax.reload();
                    selectedDocIDs = [];
                    FinalDoclist = [];
                    mApp.unblockPage();
                } else {
                    toastr.error(response.Msg, "Error");
                    mApp.unblockPage();
                }
            },
            error: function (error) {
                toastr.error("something went wrong try again later", "Error");
                mApp.unblockPage();
            }
        })
    }
    else {
        toastr.error("Please select atleast one document!", "Error");
        //return;
    }
});

//setup before functions
var typingTimer;                //timer identifier
var doneTypingInterval = 500;  //time in ms (3 seconds)

$('#tblJepDocumentDetails').on('blur', 'td .GenralRemark', function () {
    var tr = $(this).closest('tr');
    var row = tblJepDocumentDetails.row(tr);
    var data = row.data();
    var DINRefDocumentId = data.DINRefDocumentId;
    var getJEPDocumentDetailsId = data.DocumentId;

    clearTimeout(typingTimer);
    if ($('#GenralRemark' + getJEPDocumentDetailsId).val()) {
        typingTimer = setTimeout(doneTyping, doneTypingInterval);
    }

    function doneTyping() {

        var getCountOfText = $('#GenralRemark' + getJEPDocumentDetailsId).val().length;
        if (getCountOfText < 501) {
            var Remarkdata = {
                GeneralRemarks: $('#GenralRemark' + getJEPDocumentDetailsId).val(),
                DINRefDocumentId: DINRefDocumentId
            };
            DINRefDocumentId = DINRefDocumentId;
            mApp.blockPage();
            $.ajax({
                type: "POST",
                url: '/DES/DIN/UpdateGenralRemark',
                data: JSON.stringify(Remarkdata),
                dataType: 'json',
                contentType: "application/json;charset=utf-8",
                async: true,
                success: function (response) {
                    if (response.Status) {
                        // toastr.success(response.Msg, "Success");
                        $('#btnSubmit').prop('disabled', false);
                        tblJepDocumentDetails.ajax.reload();
                        mApp.blockPage();
                    } else {
                        toastr.error(response.Msg, "Error");
                        mApp.unblockPage();
                    }
                },
                error: function (error) {
                    toastr.error("something went wrong try again later", "Error");
                    mApp.unblockPage();
                }
            })
        }
        else {
            toastr.info("Remark text must be less then 500 character", "Info");
            $('#GenralRemark' + getJEPDocumentDetailsId).val('');
        }
    }
});





$('#tblJepDocumentDetails').on('click', 'td .btnDelete', function () {
    var tr = $(this).closest('tr');
    var row = tblJepDocumentDetails.row(tr);
    var data = row.data();

    swal({
        title: "Do you want to delete?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            var rev = "Yes";
            if (isConfirm.value) {
                mApp.blockPage();
                $.ajax({
                    type: "GET",
                    url: '/DES/DIN/DeleteDINRefDocument',
                    data: { Id: data.DINRefDocumentId },
                    dataType: 'json',
                    contentType: "application/json;charset=utf-8",
                    async: true,
                    success: function (response) {
                        if (response.Status) {
                            toastr.success(response.Msg, "Success");
                            if (tblJepDocumentDetails.data().count() == 1) {
                                tblJepDocumentDetails.ajax.reload();
                            }
                            else {
                                tblJepDocumentDetails.ajax.reload(null, false);
                            }
                            tblJepDoc.ajax.reload();
                            tblGlobalJepDoc.ajax.reload();
                            mApp.unblockPage();
                        } else {
                            toastr.error(response.Msg, "Error");
                            mApp.unblockPage();
                        }
                    },
                    error: function (error) {
                        toastr.error("something went wrong try again later", "Error");
                        mApp.unblockPage();
                    }
                })
            } else {
            }
        });
});

$('#btnSubmit').click(function () {
    //mApp.blockPage();
    var model = {
        DINId: $('#DINId').val(),
        DistributionGroupId: $('#DistributionGroupId').val(),
        Department: $('#Department').val(),
    };

    swal({
        title: "Do you want to Release DIN?",
        type: "warning",
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger",
        cancelButtonClass: "btn btn-secondary",
        confirmButtonText: "Yes",
        cancelButtonText: "No"
    }).then(
        function (isConfirm) {
            var rev = "Yes";
            if (isConfirm.value) {
                mApp.blockPage();
                $.ajax({
                    type: "POST",
                    url: '/DES/DIN/UpdatePolicy',
                    data: JSON.stringify(model),
                    dataType: 'json',
                    contentType: "application/json;charset=utf-8",
                    async: true,
                    success: function (response) {
                        if (response.Status) {
                            toastr.success(response.Msg, "Success");
                            tblJepDocumentDetails.ajax.reload();
                            selectedDocIDs = [];

                            //var AckModel = {
                            //    DINId: $('#DINId').val(),
                            //    DINGroupId: $('#DistributionGroupId').val(),
                            //    Department: $('#Department').val(),
                            //};

                            //$.ajax({
                            //    type: "POST",
                            //    url: '/DES/DIN/AddDINACK',
                            //    data: JSON.stringify(AckModel),
                            //    dataType: 'json',
                            //    contentType: "application/json;charset=utf-8",
                            //    async: true,
                            //    success: function (response) {
                            //        if (response.Status) {
                            //            // toastr.success(response.Msg, "Success");
                            //            mApp.unblockPage();
                            //        } else {
                            //            toastr.error(response.Msg, "Error");
                            //            mApp.unblockPage();
                            //        }
                            //    },
                            //    error: function (error) {
                            //        toastr.error("something went wrong try again later", "Error");
                            //        mApp.unblockPage();
                            //    }
                            //})



                            mApp.unblockPage();
                            if (response.RedirectAction != undefined || response.RedirectAction != null) {
                                window.location.href = response.RedirectAction;
                            }
                        } else {
                            if (response.IsInfo) {
                                toastr.info(response.Msg, "Info");
                                mApp.unblockPage();
                            }
                            else {
                                toastr.error(response.Msg, "Error");
                                mApp.unblockPage();
                            }
                        }
                    },
                    error: function (error) {
                        toastr.error("something went wrong try again later", "Error");
                        mApp.unblockPage();
                    }
                })
            } else {
            }
        });
});

$('#tblJepDocumentDetails').on('click', 'td .btnRevisionClick', function () {
    var tr = $(this).closest('tr');
    var row = tblJepDocumentDetails.row(tr);
    var data = row.data();
    var DocDetail = {
        DocumentId: data.DocumentId,
        DocumentNo: data.DocNo,
        DINRefDocumentId: data.DINRefDocumentId
    };
    mApp.blockPage();
    $.ajax({
        type: "POST",
        url: '/DES/DIN/UpdateGird',
        data: JSON.stringify(DocDetail),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        async: true,
        success: function (response) {
            if (response.Status) {
                if (InfoMSGS = "true") {
                    toastr.info(response.Msg, "Info");
                }
                else {
                    toastr.success(response.Msg, "Success");
                }
                tblJepDocumentDetails.ajax.reload();
                mApp.unblockPage();
            } else {
                toastr.info(response.Msg, "Info");
                tblJepDocumentDetails.ajax.reload();
                mApp.unblockPage();
            }
        },
        error: function (error) {
            toastr.error("something went wrong try again later", "Error");
            mApp.unblockPage();
        }

    });
});

$(document).ready(function () {
    $('.groupDrpID').select2();
});

$('.newPop').click(function () {
    var dbGroupId = { DistributionGroupId: $('#DistributionGroupId').val() };
    if ($('#DistributionGroupId').val() != '') {
        mApp.blockPage();

        $.ajax({
            url: '/DES/DIN/GetDatabyGroupId',
            data: JSON.stringify(dbGroupId),
            type: 'POST',
            traditional: true,
            contentType: 'application/json; charset=utf-8',
            dataType: "Html",
            success: function (result) {
                mApp.unblockPage();
                $('#modelbodyAllGroupData').html(result);
                $('#m_modal_1_2').modal("show");
            },
            error: function (xhr) {
                mApp.unblockPage();
            }
        });
    }
    else {
        toastr.info("Please select Distribution Group", "Info");
    }

});


$("#m_aside_left_minimize_toggle").click(function () {
    if (tblJepDoc != null)
        tblJepDoc.columns.adjust();
});

$("#m_aside_left_minimize_toggle").click(function () {
    if (tblGlobalJepDoc != null)
        tblGlobalJepDoc.columns.adjust();
});

$("#m_aside_left_minimize_toggle").click(function () {
    if (tblJepDocumentDetails != null)
        tblJepDocumentDetails.columns.adjust();
});

function btnBindGlobalJEPDoc() {
    $("#ContractProjectList").val($("#ProjectNo").val());
    tblGlobalJepDoc.ajax.reload();
    $("#ContractProjectList").select2();
    $("#txtDocumentNo").val("");
    setTimeout(function () {
        tblGlobalJepDoc.columns.adjust();
    }, 500);
}

function btnSearchGlobalJEPDoc() {
    tblGlobalJepDoc.ajax.reload();
    setTimeout(function () {
        tblGlobalJepDoc.columns.adjust();
    }, 500);
}
$("#txtDocumentNo").keyup(function (event) {
    if (event.keyCode === 13) {
        btnSearchGlobalJEPDoc();
    }
});

$('#btnSaveAsDraft').click(function () {
    var model = { DINId: $('#DINId').val() };
    mApp.blockPage();

    $.ajax({
        url: '/DES/DIN/SaveAsDraft',
        data: JSON.stringify(model),
        type: 'POST',
        traditional: true,
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        success: function (response) {
            mApp.unblockPage();
            if (response.Status) {
                // toastr.info(response.Msg, "Info");
                if (response.RedirectAction != undefined || response.RedirectAction != null) {
                    window.location.href = response.RedirectAction;
                }
            }
        },
        error: function (xhr) {
            mApp.unblockPage();
        }
    });
});

$('#tblJepDocumentDetails').on('click', 'td .btnUV', function () {
    var tr = $(this).closest('tr'); 0
    var row = tblJepDocumentDetails.row(tr);
    var data = row.data();
    var DocPathList = data.MainDocumentPath.split(",");
    $.each(DocPathList, function (i) {
        UniverSalFile(DocPathList[i]);
    });
});

//when open popup then it will call
var tblDistributeDoc;
$('#openDistributePop').click(function () {

    var model = { DINId: $('#DINId').val() };
    mApp.blockPage();

    $.ajax({
        url: '/DES/DIN/GetDistributeDOCData',
        data: JSON.stringify(model),
        type: 'POST',
        traditional: true,
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        success: function (response) {
            mApp.unblockPage();
            if (response.Status) {

                if (response.Data != null) {
                    $("#m_modal_5").modal("show");
                    if (tblDistributeDoc != null) {
                        //$('#tblDistributeDoc').DataTable().empty();
                        tblDistributeDoc.destroy();
                        $('#tblDistributeDoc').empty();
                        //$('#tblDistributeDoc').DataTable().clear().destroy();
                    }
                    var htmlstr = "";
                    htmlstr = "<thead><tr><th>Document</th><th>Document Revision</th>";
                    htmlstr += "<th style='display:none'>DINId</th><th style='display:none'>JEPDocumentDetailsId</th>";
                    $.each(response.Data[0], function (key, values) {  //For loop to create dynamic header 
                        htmlstr += "<th>" + values.DepartmentName + "<br/>" + values.GetFirstPsNumber + "</th>";
                        htmlstr += "<th> 'E' Copies </th>";
                    });
                    //$.each(response.Data[0], function (key, values) {  //For loop to create dynamic header 
                    //    htmlstr += "<th> ElectronicCopys </th>";
                    //});
                    htmlstr += " </tr></thead>";
                    $.each(response.Data, function (key, value) { // loop through create rows 
                        htmlstr += "<tr><td>" + value[0].DocuemntNo + "</td><td>R" + value[0].DocumentRev + "</td>";
                        htmlstr += "<td style='display:none'>" + value[0].DINId + "</td><td style='display:none' >" + value[0].JEPDocumentDetailsId + "</td>";
                        for (var i = 0; i < response.Data[0].length; i++) { // loop through create rows of text box to create dynamic took lenght 

                            if (value[i].IsElectronicCopys == true) {
                                htmlstr += " <td><input type='text'  maxlength='4' class='form-control blockWords getClass' id='text_" + value[i].DINDistributeDOCId + "' name=" + value[i].DINId + "-" + value[i].JEPDocumentDetailsId + " value=" + value[i].DeptWiseDOCCopys + " disabled /></td>";
                                htmlstr += " <td><input type='checkbox' class='m-checkbox m-checkbox--solid m-checkbox--successl  getCheckBoxClass'  id='check_" + value[i].DINDistributeDOCId + "' name=" + value[i].DINId + "-" + value[i].JEPDocumentDetailsId + " checked  ></td>";

                            }
                            else {
                                htmlstr += " <td><input type='text'  maxlength='4' class='form-control blockWords getClass' id='text_" + value[i].DINDistributeDOCId + "' name=" + value[i].DINId + "-" + value[i].JEPDocumentDetailsId + " value=" + value[i].DeptWiseDOCCopys + " /></td>";
                                htmlstr += " <td><input type='checkbox' class='m-checkbox m-checkbox--solid m-checkbox--success  getCheckBoxClass'  id='check_" + value[i].DINDistributeDOCId + "' name=" + value[i].DINId + "-" + value[i].JEPDocumentDetailsId + "  ></td>";

                            }
                        }

                        //for (var i = 0; i < response.Data[0].length; i++) { // loop through create rows of text box to create dynamic took lenght 

                        //    }
                    });

                    htmlstr += "</tr>";

                    $(htmlstr).appendTo("#tblDistributeDoc");
                    tblDistributeDoc = $("#tblDistributeDoc").DataTable({
                        "bSort": false,
                        "filter": true,
                        "scrollX": true,
                        "initComplete": function () {
                            setTimeout(function () {
                                tblDistributeDoc.columns.adjust();
                            }, 1000);
                        },
                    });
                }
                else {
                    toastr.info(response.Msg, "Info");
                    $("#m_modal_5").modal("hide");
                }
            }
        },
        error: function (xhr) {
            mApp.unblockPage();
        }
    });

});


//validation to check you can not add words

$("body").on("keypress", ".blockWords", function (e) {
    // parseInt($(this).val());
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        $("#errmsg").html("Digits Only").show().fadeOut("slow");
        return false;
    }
});


$("body").on("focusout", ".blockWords", function (e) {
    // parseInt($(this).val());
    if ($(this).val() != "") {
        $(this).val(parseInt($(this).val()));
    }
    else {
        $(this).val('0');
    }

});

//when add textbox then it will call on focusout

$('#tblDistributeDoc').on('focusout', 'td .getClass', function (e) {
    var getValue = e.target.name.split('-');
    //herte will  get Data like 1,2,3

    if (e.target.value == "") {
        e.target.value == "0";
    }

    var model = {
        DINDistributeDOCId: e.target.id.replace('text_', ''),
        DINId: getValue[0],   //Is Din Id
        JEPDocumentDetailsId: getValue[1], //Is JEP JEPDocumentDetailsId Id
        DeptWiseDOCCopys: e.target.value  //GetUser Input Value
    };

    $.ajax({
        url: '/DES/DIN/AddDocDistribute',
        data: JSON.stringify(model),
        type: 'POST',
        traditional: true,
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        success: function (response) {
            mApp.unblockPage();
            if (response.Status) {
                // toastr.info(response.Msg, "Info");
                mApp.unblockPage();
            }
        },
        error: function (xhr) {
            mApp.unblockPage();
        }
    });
});

$('#tblDistributeDoc').on('focusout', 'td .getCheckBoxClass', function (e) {
    var getValue = e.target.name.split('-');
    //herte will  get Data like 1,2,3

    if (e.target.value == "") {
        e.target.value == "0";
    }
    if ($('#' + e.target.id + '').is(':checked') == true) {
        $('#text_' + e.target.id.replace('check_', '') + '').prop('disabled', true);
        $('#text_' + e.target.id.replace('check_', '') + '').val('0');
    }
    else {
        $('#text_' + e.target.id.replace('check_', '') + '').prop('disabled', false);
    }
    var model = {
        DINDistributeDOCId: e.target.id.replace('check_', ''),
        DINId: getValue[0],   //Is Din Id
        JEPDocumentDetailsId: getValue[1], //Is JEP JEPDocumentDetailsId Id
        IsElectronicCopys: $('#' + e.target.id + '').is(':checked')  //GetUser Input Value
    };

    $.ajax({
        url: '/DES/DIN/AddDocElectronicDistribute',
        data: JSON.stringify(model),
        type: 'POST',
        traditional: true,
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        success: function (response) {
            mApp.unblockPage();
            if (response.Status) {
                // toastr.info(response.Msg, "Info");
                mApp.unblockPage();
            }
        },
        error: function (xhr) {
            mApp.unblockPage();
        }
    });
});

$('#DistributionGroupId').select2();



function RedirectToDocument(DocumentNo) {
    var model = {
        DINId: $('#DINId').val(),
        DocumentNo: DocumentNo
    };

    $.ajax({
        type: "Post",
        url: '/DES/DIN/IsDocumentComplete',
        data: JSON.stringify(model),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        async: true,
        success: function (response) {
            if (response) {
                if (response.Status) {
                    if (response.RedirectAction != undefined || response.RedirectAction != null) {
                        if (response.RedirectAction == "True") {

                            swal({
                                title: "Document " + DocumentNo + " is in Complete stage, Do you want to Revise document",
                                type: "warning",
                                allowOutsideClick: false,
                                showConfirmButton: true,
                                showCancelButton: true,
                                confirmButtonClass: "btn btn-danger",
                                cancelButtonClass: "btn btn-secondary",
                                confirmButtonText: "Yes",
                                cancelButtonText: "No"
                            }).then(
                                function (isConfirm) {
                                    var rev = "Yes";
                                    if (isConfirm.value) {
                                        mApp.blockPage();
                                        $.ajax({
                                            type: "Post",
                                            url: '/DES/DIN/DocumentData',
                                            data: JSON.stringify(model),
                                            dataType: 'json',
                                            contentType: "application/json;charset=utf-8",
                                            async: true,
                                            success: function (response) {
                                                if (response) {
                                                    if (response.Status) {
                                                        mApp.unblockPage();
                                                        if (response.RedirectAction != undefined || response.RedirectAction != null) {
                                                            window.open(response.RedirectAction, '_blank');

                                                        }
                                                    }
                                                    else {
                                                        mApp.unblockPage();
                                                        toastr.info(response.Msg, "Info");
                                                    }
                                                }
                                                else {
                                                    toastr.error(response.Msg, "Error");
                                                    mApp.unblockPage();
                                                }
                                            },
                                            error: function (error) {
                                                toastr.error("something went wrong try again later", "Error");
                                                mApp.unblockPage();
                                            }
                                        })
                                    } else {
                                    }
                                });
                        }
                        else {
                            if (response.RedirectAction != undefined || response.RedirectAction != null) {
                                window.open(response.RedirectAction, '_blank');

                            }
                        }
                    }
                }
                else {
                    mApp.unblockPage();
                    toastr.info(response.Msg, "Info");
                }
            }
            else {
                toastr.error(response.Msg, "Error");
                mApp.unblockPage();
            }
        },
        error: function (error) {
            toastr.error("something went wrong try again later", "Error");
            mApp.unblockPage();
        }
    })




}