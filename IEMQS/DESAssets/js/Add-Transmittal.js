var DatatablesBasicBasic = {
    init: function () {
        var e;
        (e = $("#m_table_1")).DataTable({
            paging: false,
            responsive: !0,
           dom: "<'row'<'col-sm-12'tr>>\n\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
            
            language: {
                lengthMenu: "Display _MENU_"
            },
       
            columnDefs: [{
                 targets:0,
                title: "Actions",
                  width: "50px",
                orderable: !1,
                render: function (e, a, t, n) {
                    return '\n <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="View">\n<i class="la la-trash m--font-danger"></i>\n                        </a> '
                }
            }]
            
        })
    }
};
jQuery(document).ready(function () {
    DatatablesBasicBasic.init()
});