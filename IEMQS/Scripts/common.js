﻿var ServerSideError = "Error in Server Side";
var ClientSideError = "Error in Script";
var string = "";
var classname = "";
var tables = null;


$(document).ready(function () {
    $('body').click(function (evt) {
        if (evt.target.className === "iconspace fa fa-filter fltbtn") {
            return false;
        }
        else {
            $(".dvfilters").hide();
        }
    });
    if (localStorage.getItem('sidebar_closed') === '1' && App.getViewPort().width >= App.getResponsiveBreakpoint('md')) {
        $('body').addClass('page-sidebar-closed');
        $('.page-sidebar-menu').addClass('page-sidebar-menu-closed');
    }
    $('body').on('click', '.sidebar-toggler', function (e) {
        // local store sidebar show/hide status
        if ($('body').hasClass("page-sidebar-closed")) {
            localStorage.setItem("sidebar_closed", '0');
        } else {
            localStorage.setItem("sidebar_closed", '1');
        }
        $(window).trigger('resize');
    });

    $(document).on('init.dt', function (e, settings) {
        //console.log(e);
        var api = new $.fn.dataTable.Api(settings);
        //console.log('New DataTable created:', api.table().node());
        var tableId = $(api.table().node()).attr("id");
        $("#" + tableId + "_wrapper,#" + tableId + "").find(".dataTables_filter input[type=text],.dataTables_filter input[type=search]")
            .unbind() // Unbind previous default bindings
            .bind("input keydown", function (e) { // Bind our desired behavior
                var item = $(this);
                var prevVal = item.attr("data-prevVal");
                prevVal = prevVal == null ? "" : prevVal;
                searchTerm = $(item).val();
                if (e.keyCode == 13 || e.keyCode == 9) {
                    // Call the API search function
                    if (prevVal != searchTerm) {
                        item.attr("data-prevVal", searchTerm);
                        api.search(searchTerm).draw();
                    }
                }
                return;
            });
    });
});
//function for load master dropdown

//Develop By Jaydeep Sakariya
function LoadDropDown(url, objControl, lbloptional, data) {
    //$.get(url, data, function (data) {
    //    var option = ((lbloptional === undefined || lbloptional === null) ? "" : "<option value='' selected='selected'>--Select " + lbloptional + "--</option>");
    //    $.each(data, function (index, object) {
    //        option += "<option value='" + object.id + "'>" + object.text + "</option>";
    //    });
    //    $(objControl).html(option);



    //});

    $.ajax({
        url: WebsiteURL + url,
        type: "GET",
        data: data,
        async: false,
        success: function (data) {
            var option = ((lbloptional === undefined || lbloptional === null) ? "" : "<option value='' selected='selected'>--Select " + lbloptional + "--</option>");
            $.each(data, function (index, object) {
                option += "<option value='" + object.id + "'>" + object.text + "</option>";
            });
            $(objControl).html(option);
        }
    })

};
//Develop By Jaydeep Sakariya
function LoadSelect2DropDown(url, objControl, placeHolder, condParam, minimumInputLength) {
    if (minimumInputLength === undefined)
        minimumInputLength = 1;
    objControl.select2({
        placeholder: placeHolder,
        delay: 250,
        allowClear: true,
        minimumInputLength: minimumInputLength,
        ajax: {
            url: WebsiteURL + url,
            type: "POST",
            dataType: 'json',
            data: function (params) {

                var query = {
                    search: params.term,
                    page: params.page,
                    param: condParam
                }

                // Query paramters will be ?search=[term]&page=[page]
                return query;
            },
            processResults: function (data) {
                return {
                    results: data
                };
            }
        }
    });
};

function Select2Selected(objControl, id, text) {
    objControl.select2({
        data: [
            {
                id: id,
                text: text
            }
        ]
    });
};


function Select2Edit(url, objControl, caption, id, text, param) {

    $.ajax({
        url: WebsiteURL + url,
        type: "GET",
        data: param,
        async: false,
        success: function (data) {
            var option = "<option value='" + id + "' selected='selected'>" + text + "</option>";
            $.each(data, function (index, object) {
                option += "<option value='" + object.id + "'>" + object.text + "</option>";
            });
            $(objControl).html(option);

            objControl.select2({
                placeholder: caption,
                delay: 250,
                allowClear: true,
                minimumInputLength: 1
            });
        }
    })

};

function Select2EditWithSource(objControl, sourceList, caption, value) {
    var option = "";//"<option value='" + id + "' selected='selected'>" + text + "</option>";
    $.each(sourceList, function (index, object) {
        option += "<option value='" + object.id + "'>" + object.text + "</option>";
    });
    $(objControl).html(option);

    objControl.select2({
        placeholder: caption,
    });
    objControl.each(function (index, item) {
        var val = $(item).data("value") ? $(item).data("value").toString() : ""; // when value is numaric then it can't be split
        $(item).val(val ? val.split(",") : []).trigger("change");
    });
    if (value)
        $(objControl).val(value).trigger("change");
};

function LoadPreValueSelect2(url, objControl, caption, param) {
    $.ajax({
        url: WebsiteURL + url,
        type: "GET",
        data: param,
        async: false,
        success: function (data) {
            var option = "";
            $.each(data, function (index, object) {
                option += "<option value='" + object.id + "'>" + object.text + "</option>";
            });
            $(objControl).html(option);

            objControl.select2({
                placeholder: caption

            });
        }
    })

};

function LoadMultiSelect2OnEdit(strPosition) {
    var arrPositions;
    if (strPosition.length > 0) {
        arrPositions = strPosition.split(',');
    }
    return arrPositions;
}

function FillAutoCompleteForEnum(element, sourceList, hdElement) {
    element.autocomplete({
        source: function (request, response) {
            var array = $.map(sourceList, function (item) {
                return {
                    label: item.Text,
                    value: item.Text,
                    id: item.Value
                }
            });
            response($.ui.autocomplete.filter(array, request.term));
        },
        autoFocus: true,
        minLength: 0,
        select: function (event, ui) {
            $(this).val(ui.item.id);
            $(hdElement).val(ui.item.id);
            //HighlightInValidControls();
        },
        change: function (event, ui) {
            if (ui.item == null) {
                element.val('');
                element.focus();
                //HighlightInValidControls();
            }
        }
    }).focus(function () {
        $(this).data("uiAutocomplete").search($(this).val());
        $(this).autocomplete("search");
    });
}

function FillAutoComplete(element, sourceList, hdElement) {
    element.autocomplete({
        source: function (request, response) {
            var array = $.map(sourceList, function (item) {
                return {
                    label: item.CatDesc,
                    value: item.CatDesc,
                    id: item.CatID
                }
            });
            response($.ui.autocomplete.filter(array, request.term));
        },
        autoFocus: true,
        minLength: 0,
        select: function (event, ui) {
            $(this).val(ui.item.id);
            $(hdElement).val(ui.item.id);
            HighlightInValidControls();
        },
        change: function (event, ui) {
            if (ui.item == null) {
                element.val('');
                element.focus();
                $(hdElement).val("");
                HighlightInValidControls();
            }
        }
    }).focus(function () {
        $(this).data("uiAutocomplete").search($(this).val());
        $(this).autocomplete("search");
    });
}

function FillAutoCompleteWithArray(element, sourceList, hdElement, fnCallback) {
    element.autocomplete({
        source: function (request, response) {
            var array = $.map(sourceList, function (item) {
                return {
                    label: item,
                    value: item,
                    id: item
                }
            });
            response($.ui.autocomplete.filter(array, request.term));
        },
        autoFocus: true,
        minLength: 0,
        select: function (event, ui) {
            $(this).val(ui.item.id);
            if (hdElement != undefined)
                $(hdElement).val(ui.item.id);
            if (fnCallback != undefined && typeof fnCallback == "function")
                fnCallback(ui.item)
            HighlightInValidControls();
        },
        change: function (event, ui) {
            if (ui.item == null) {
                $(this).val('');
                $(this).focus();
                HighlightInValidControls();
            }
        }
    }).focus(function () {
        $(this).data("uiAutocomplete").search($(this).val());
        $(this).autocomplete("search");
    });
}

function FillAutoCompleteWithArrayNoFocus(element, sourceList, hdElement, fnCallback) {
    return element.autocomplete({
        source: function (request, response) {
            var array = $.map(sourceList, function (item) {
                return {
                    label: item,
                    value: item,
                    id: item
                }
            });
            response($.ui.autocomplete.filter(array, request.term));
        },
        autoFocus: true,
        minLength: 0,
        select: function (event, ui) {
            $(this).val(ui.item.id);
            if (hdElement != undefined)
                $(hdElement).val(ui.item.id);
            if (fnCallback != undefined && typeof fnCallback == "function")
                fnCallback(ui.item)
            HighlightInValidControls();
        },
        change: function (event, ui) {
            if (ui.item == null) {
                $(this).val('');
                $(this).focus();
                HighlightInValidControls();
            }
        }
    });
}

function HighlightInValidControls() {
    $('.error').focus(function () {
        $(this).parent().find('.tooltip').removeClass('hidetooltip').addClass('showtooltip');
    });

    $('.error').blur(function () {
        $(this).parent().find('.tooltip').removeClass('showtooltip').addClass('hidetooltip');
    });

    $('select.error').each(function () {
        //console.log($(this).parent());

        $(this).parent().find('.select2').addClass('error');
    });

    HighlightInValidateSelect2();
}

function HighlightInValidateSelect2() {
    $('.select2-search__field').not("#divLinkedProject *").focus(function () {
        ValidateSearch($(this));
    });

    $('.select2-search__field').not("#divLinkedProject *").blur(function () {
        ValidateSearch($(this));
    });

}
function ValidateSearch(search__field) {
    var $ul = search__field.closest('.select2-selection__rendered');
    var $select2 = search__field.closest('.select2');

    //var hasError = $ul.find('.select2-selection__choice').length;
    //if (hasError == 0) {
    //    $select2.addClass('error');
    //    $select2.parent().find('.tooltip').removeClass('hidetooltip').addClass('showtooltip');
    //}
    //else {
    //    $select2.removeClass('error');
    //    $select2.parent().find('.tooltip').removeClass('showtooltip').addClass('hidetooltip');
    //}
    var notrequired = $('.select2-search__field').closest('.select2').parent().find('.select2-hidden-accessible').hasClass('notrequired');
    if (!notrequired) {
        var selectedItemCnt = $ul.find('.select2-selection__choice').length;
        if (selectedItemCnt == 0) {
            $select2.addClass('error');
            $select2.parent().find('.tooltip').removeClass('hidetooltip').addClass('showtooltip');
        }
        else {
            $select2.removeClass('error');
            $select2.parent().find('.tooltip').removeClass('showtooltip').addClass('hidetooltip');
        }
    }
    else {
        $select2.removeClass('error');
        $select2.parent().find('.tooltip').removeClass('showtooltip').addClass('hidetooltip');
    }
}

function ShowTimeline(url) {
    $.ajax({
        url: WebsiteURL + url,
        dataType: "html",
        type: "GET",
        delay: 250,
        async: false,
        success: function (result) {
            bootbox.dialog({
                title: "Timeline",
                message: result,
                size: 'large'
            });
        }
    });
}


function ReviseHeader(id, path, revElement, hdRevElement, spnStatus, callbackfun) {
    var strHtml = "<scr" + "ipt>$(document).ready(function(){"
        + "$('input[maxlength],textarea[maxlength]').maxlength({alwaysShow: true,});"
        + "$('#frmHeaderRevise').validate({"
        + " rules: {"
        + "txtReviseRemark: {"
        + "required: true,"
        + "},"
        + "messages: {"
        + "txtReviseRemark: {"
        + "required: 'Revise Remark is required'"
        + "}"
        + "}"
        + "}"
        + "});"
        + "});</scr" + "ipt>"
        + " <div>"
        + "<form method='post' id='frmHeaderRevise' class='form-horizontal' action=''>"
        + "<div class='form-group'>"
        + "<div class='col-md-12'>"
        + "<br /><label class='control-label'><b>Revise Remark</b><span class='required'> * </span></label>"
        + "<div class=''>"
        + "<textarea type='text' value='' style='width:100%' name='txtReviseRemark' id='txtReviseRemark' maxlength='200' class='form-control col-md-3'></textarea>"
        + "</div>"
        + "</div>"
        + "</div>"
        + "</form>"
        + "</div>";

    bootbox.confirm({
        title: 'Revise Document',
        message: 'Do you want to Revise Document?' + strHtml,
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result) {
                //debugger;
                if ($('#frmHeaderRevise').valid()) {
                    var Remarks = $('#txtReviseRemark').val();
                    $.ajax({
                        url: WebsiteURL + path,
                        type: "POST",
                        datatype: 'json',
                        data: { strHeaderId: id, strRemarks: $('#txtReviseRemark').val() },
                        success: function (data) {
                            if (data.Key) {
                                revElement.val("R" + data.rev);
                                hdRevElement.val(data.rev);
                                spnStatus.text(data.Status);
                                DisplayNotiMessage("success", data.Value, "Success");
                                if ($('#ReviseRemark') != null && $('#ReviseRemark') != undefined && $('#ReviseRemark') != 'undefined') {
                                    $('#ReviseRemark').val(Remarks);
                                    $('#ReviseRemark').css({ "display": "inline-block", })
                                }
                                if (callbackfun != null && callbackfun != undefined && callbackfun != 'undefined') {
                                    callbackfun();
                                }
                            }
                            else {
                                DisplayNotiMessage("error", data.Value, "Error");
                                return false;
                            }
                        },
                        error: function () {
                        }
                    });

                }
                else {
                    return false;
                }
            }
            //else { return false; }
        }
    });
}


function DeleteDocument(headerId, path, param, tblId) {

    bootbox.dialog({
        title: "Delete Confirmation",
        message: "Do you want to delete?",
        size: "medium",
        buttons: {
            Yes: {
                Label: "Yes",
                className: 'btn green',
                callback: function () {
                    $.ajax({
                        url: WebsiteURL + path,
                        type: "POST",
                        data: param,
                        beforeSend: function () {
                            showLoading(true);
                        },
                        complete: function () {
                            showLoading(false);
                        },
                        success: function (res) {
                            if (res.Key == true) {
                                DisplayNotiMessage("success", res.Value, "Success");
                            }
                            else {
                                if (res.dataValue == "warning") {
                                    DisplayNotiMessage("warning", res.Value, "Warning");
                                }
                                else {
                                    DisplayNotiMessage("error", res.Value, "Error");
                                }

                            }
                            $("#" + tblId).DataTable().ajax.reload();
                        }
                    });
                }
            },
            Cancel: {
                label: 'No',
                className: 'btn red',
                callback: function () { }
            }

        }
    });
}

function ReturnHeader(id, path, callbackfun) {
    var strHtml = "<scr" + "ipt>$(document).ready(function(){"
        + "$('input[maxlength],textarea[maxlength]').maxlength({alwaysShow: true,});"
        + "$('#frmHeaderReturn').validate({"
        + " rules: {"
        + "txtReturnRemark: {"
        + "required: true,"
        + "},"
        + "messages: {"
        + "txtReturnRemark: {"
        + "required: 'Revise Remark is required'"
        + "}"
        + "}"
        + "}"
        + "});"
        + "});</scr" + "ipt>"
        + " <div>"
        + "<form method='post' id='frmHeaderReturn' class='form-horizontal' action=''>"
        + "<div class='form-group'>"
        + "<div class='col-md-12'>"
        + "<br /><label class='control-label'><b>Return Remark</b><span class='required'> * </span></label>"
        + "<div class=''>"
        + "<textarea type='text' value='' style='width:100%' name='txtReturnRemark' id='txtReturnRemark' maxlength='200' class='form-control col-md-3'></textarea>"
        + "</div>"
        + "</div>"
        + "</div>"
        + "</form>"
        + "</div>";

    bootbox.confirm({
        title: 'Revise Document',
        message: 'Do you want to Return?' + strHtml,
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result) {
                if ($('#frmHeaderReturn').valid()) {
                    var Remarks = $('#txtReturnRemark').val();
                    $.ajax({
                        url: WebsiteURL + path,
                        type: "POST",
                        datatype: 'json',
                        data: { HeaderId: id, strRemarks: $('#txtReturnRemark').val() },
                        success: function (data) {
                            if (data.Key) {
                                DisplayNotiMessage("success", data.Value, "Success");

                                if (callbackfun != null && callbackfun != undefined && callbackfun != 'undefined') {
                                    callbackfun();
                                }
                            }
                            else {
                                DisplayNotiMessage("error", data.Value, "Error");
                                return false;
                            }
                        },
                        error: function () {
                        }
                    });

                }
                else {
                    return false;
                }
            }
        }
    });
}

function ViewHistoryForProjectPLN(HeaderId, Role, URL, Plan) {
    $.ajax({
        type: "POST",
        url: WebsiteURL + URL,
        datatype: 'html',
        async: false,
        beforeSend: function () {
            showLoading(true);
        },
        complete: function () {
            showLoading(false);
        },
        data: { strRole: Role, "HeaderId": HeaderId },
        success: function (result) {
            bootbox.dialog({
                title: 'History of ' + Plan,
                message: result,
                size: 'large',
                buttons: {
                    'cancel': {
                        label: 'Close',
                        className: 'btn btnclose'
                    }
                }
            });
        },
        error: function () {
        }
    });
}


function RevokeDoument(Url, HeaderId, pdinId, RedirectURL) {
    var strHtml = "<scr" + "ipt>$(document).ready(function(){"
        + "$('#frmRevokeRemarks').validate({"
        + " rules: {"
        + "txtRemarks: {"
        + "required: true,"
        + "},"
        + "messages: {"
        + "txtRemarks: {"
        + "required: 'Remark is required'"
        + "}"
        + "}"
        + "}"
        + "});"
        + "});</scr" + "ipt>"
        + " <div>"
        + "<form method='post' id='frmRevokeRemarks' class='form-horizontal' action=''>"
        + "<div class='form-group'>"
        + "<div class='col-md-12'>"
        + "<label class='control-label'>Remarks<span class='required'> * </span></label>"
        + "<div class=''>"
        + "<textarea value='' name='txtRemarks' id='txtRemarks' class='form-control col-md-12'></textarea>"
        + "</div>"
        + "</div>"
        + "</div>"
        + "</form>"
        + "</div>";


    bootbox.dialog({
        title: "Revoke Confirmation",
        message: 'Do you want to revoke document?' + strHtml,
        size: "medium",
        buttons: {
            Yes: {
                Label: "Yes",
                className: 'btn green',
                callback: function () {
                    if ($('#frmRevokeRemarks').valid()) {
                        $.ajax({
                            type: "POST",
                            url: WebsiteURL + Url,
                            datatype: 'json',
                            async: false,
                            beforeSend: function () {
                                showLoading(true);
                            },
                            complete: function () {
                                showLoading(false);
                            },
                            data: { HeaderId: HeaderId, pdinId: pdinId, Remarks: $("#txtRemarks").val() },
                            success: function (data) {
                                if (data.Key) {
                                    DisplayNotiMessage("success", data.Value, "Success");
                                    //  window.location.href = '/PLN/ApproveReferenceSketch/Index';
                                    window.setTimeout("window.location.href ='" + WebsiteURL + RedirectURL + "'", 2000);
                                }
                                else {
                                    DisplayNotiMessage("error", data.Value, "Error");
                                }
                            },
                            error: function () {
                            }
                        });
                        return true;
                    }
                    else {
                        return false;
                    }
                }
            },
            Cancel: {
                label: 'No',
                className: 'btn red',
                callback: function () { }
            }

        }
    });
}


function DisabledReadOnlyDropdownOpen() {
    $('select[readonly]').on('mousedown', function (e) {
        e.preventDefault();
        this.blur();
        window.focus();
    });
}

var TableDatatablesFixedHeader = function () {
    var oTable;

    function initFixHeaderTable(table) {
        //var table = $('#sample_3');

        var fixedHeaderOffset = 0;
        if (App.getViewPort().width < App.getResponsiveBreakpoint('md')) {
            if ($('.page-header').hasClass('page-header-fixed-mobile')) {
                fixedHeaderOffset = $('.page-header').outerHeight(true);
            }
        } else if ($('.page-header').hasClass('navbar-fixed-top')) {
            fixedHeaderOffset = $('.page-header').outerHeight(true);
        } else if ($('body').hasClass('page-header-fixed')) {
            fixedHeaderOffset = 64; // admin 5 fixed height
        }

        oTable = table.dataTable({
            "bDestroy": true,
            // setup rowreorder extension: http://datatables.net/extensions/fixedheader/
            fixedHeader: {
                header: true,
                headerOffset: fixedHeaderOffset
            },
            searching: false,
            paging: false,
            "bSort": false
        });
    }

    function disabledFixHeader() {
        if (oTable != null && oTable != undefined && oTable != "undefined") {
            oTable.fnDestroy();
        }
    }

    return {
        //main function to initiate the module
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }
        },
        enabled: function (table) {
            initFixHeaderTable(table);
        },
        disabled: function () {
            disabledFixHeader();
        }
        ,
        getoTable: function () {
            console.log(oTable);
        }
    };
}();


var InlineGridEdit = function () {
    var edittbl;
    var btnAddNew;
    //var contentdiv;
    var editetr = {};
    function Initialize(btnid, tblid, dv) {
        edittbl = $(dv).find($(tblid));
        btnAddNew = $(dv).find($(btnid));
        //contentdiv = dv;   
        //console.log(edittbl);
        //console.log(btnAddNew);
        editetr = {};
        btnAddNew.unbind("click");
        btnAddNew.click(function () {
            InsertNewRecordInline($(this), tbl);
        });
        EditLineEnable();
        SetNumericField();
        SetAddNewIntialStage();
    }
    function InsertNewRecordInline(obj, tbl) {
        $('input[maxlength]').maxlength({
            alwaysShow: true,
        });
        tbl.find("tbody tr:first").toggle();
        tbl.find("tbody tr:first select:first").focus();
        $(obj).html('<i class="fa fa-times"></i> Cancel');
        if (tbl.find("tbody tr:first").is(":visible")) {
            $(obj).val("Cancel");
            var delayedFn, blurredFrom;
            tbl.find("tbody tr:first").on('blur', 'a', function (event) {
                blurredFrom = event.delegateTarget;
                delayedFn = setTimeout(function () { SaveLineRecord(0); }, 0);
            });
            tbl.find("tbody tr:first").on('focus', 'a', function (event) {
                if (blurredFrom === event.delegateTarget) {
                    clearTimeout(delayedFn);
                }
            });

            if (editetr.id != null && editetr.id != undefined && editetr.id != "undefined") {
                var $tr = $("[data-id='" + editetr.id + "']");
                $tr.html(editetr.text);
            }
            EditLineEnable();
        }
        else {
            $(obj).val("New");
            $(obj).html('<i class="fa fa-plus"></i> New');
        }
    }

    function EditLineEnable() {
        //remove previous click event
        $(edittbl).find(".editline").unbind("click");

        //add new click event
        $(edittbl).find(".editline").click(function () {

            //If any rows already in edit mode then set as normal row
            edittbl.find('tbody tr').not("tbody tr:first").each(function () {

                $trold = $(this);
                //set readonly or disabled property to all element
                $trold.find('input,select,textarea').attr("readonly", "readonly");
                //$tr.find('input,select,textarea').attr("disabled", "disabled");                
                //find any div edit with editmode. If any then hide update and cancel button and show edit button
                if ($trold.find('.editmode').length > 0) {
                    $trold.find('.editmode').hide();
                    $trold.find('.editline').show();
                }
            })

            var $tr = $(this).closest("tr");

            EditLine($(this), $tr.data("hid"), $tr.data("id"));

        });
    }

    function EditLine(btnEdit, headerId, lineId) {
        //debugger;
        var $tr = $(btnEdit).closest("tr");
        editetr.text = $tr.html();
        editetr.id = $tr.data("id");
        var $td = $(btnEdit).closest("td");
        //console.log($tr);
        //remove readonly attribute for row so user can edit data.
        $tr.find("input,select,textarea").each(function () {
            $(this).removeAttr("readonly");
            $(this).removeAttr("disabled");
        });
        $('input[maxlength]').maxlength({
            alwaysShow: true,
        });


        //check there is any div exist with update and cancel button div. If exist then show div otherwise create new div for update and cancel button.
        if ($td.find('.editmode').length == 0) {
            var editdiv = "<div class='editmode' style='display:inline;'><i id='Update" + lineId + "' name='Update" + lineId + "' style='cursor: pointer;' title='Update Record' class='fa fa-floppy-o iconspace ' onclick='UpdateLine(this," + lineId + ");'></i>";
            editdiv += "<i id='btnCancel" + lineId + "' name='btnCancel" + lineId + "' style='cursor: pointer;' title='Cancel Edit' class='fa fa-ban iconspace canceledit'></i></div> "
            $td.append(editdiv);
        }
        else {
            $td.find('.editmode').show();
        }
        //hide edit button while enable edit mode
        $(btnEdit).hide();

        SetAddNewIntialStage();
        CancelEditEnable();
        SetNumericField();
    }

    function CancelEditEnable() {
        $(edittbl).find(".canceledit").unbind("click");
        $(edittbl).find(".canceledit").click(function () {
            CancelEditLine($(this));
        });
    }
    function CancelEditLine(btnCancel) {
        var $tr = $(btnCancel).closest("tr");
        $tr.html(editetr.text);
        editetr = {};
        SetAddNewIntialStage();
        EditLineEnable();
    }

    function SetAddNewIntialStage() {
        btnAddNew.html('<i class="fa fa-plus"></i> New');
        if (edittbl.find("tbody tr:first").is(":visible")) {
            edittbl.find("tbody tr:first").toggle();
        }
    }

    function SetNumericField() {
        jQuery(function ($) {
            $('.numeric').autoNumeric('init', { aPad: "fase", vMax: "1000000000", aForm: "false" });
            $('.numeric18').autoNumeric('init', { aPad: "fase", vMax: "999999999999999999", aForm: "false" });
            $('.numeric4').autoNumeric('init', { aPad: "fase", vMax: "9999", aForm: "false" });
        });
    }

    return {
        init: function (btnid, tblid, dv) {
            Initialize(btnid, tblid, dv);
        }
    }
}();


function FileUploadEnt(DivId, ControlID) {
    this.ControlID = ControlID;
    this.ActionURL = '';
    this.FolderPath = '';
    this.Type = '';
    this.ShowDeleteAction = false;
    this.MaxChunkSizeInMB = 15;
    this.ShowAttachmentBtn = true;
    this.ChunkIntervalInSec = 1;
    this.IsDisabled = null;
    this.ReloadAfterUpload = null;
    this.IsRequired = false;
    this.AllowFileExtension = '';
    this.MaxFileUploadSizeInMB = null;
    this.SuccessCallbackFunction = '';
    this.LoadCallbackFunction = null;
    this.DivId = DivId;
    this.LoadInPopup = false;
    this.PopupTitle = "File Upload";
    this.ShowDownloadBtn = true;
    this.ShowUploadBtn = true;
    this.IsMultipleUploaded = true;
    this.OrderBy = '';
    this.FilesDisplayFromURL = '';
    this.DeleteSuccessCallbackFunction = '';
    this.DocumentLoadCallbackFunction = '';
    this.OtherFolderPath = '';
    this.RemoveOnlyCreatedByMe = false;
    this.HideAttachbtnForSignleUpload = false;
}

function LoadFileUploadPartialView(objFile) {
    if (objFile.ControlID == undefined || objFile.ControlID == null || objFile.ControlID == "") {
        DisplayNotiMessage("Error", "ControlId not define", "File Upload");
        return;
    }
    //if (folderpath == undefined || folderpath == null || folderpath == "") {
    //    DisplayNotiMessage("Error", "Folder Path Not Define", "File Upload");return;
    //}
    if (objFile.SuccessCallbackFunction === undefined || objFile.SuccessCallbackFunction === null) {
        objFile.SuccessCallbackFunction = "";
    }
    if (objFile.DocumentLoadCallbackFunction === undefined || objFile.DocumentLoadCallbackFunction === null) {
        objFile.DocumentLoadCallbackFunction = "";
    }

    $.ajax({
        url: WebsiteURL + "/Utility/FileUpload/LoadFileUploadPartial",
        type: "POST",
        datatype: "html",
        beforeSend: function () {
            showLoading(true);
        },
        complete: function () {
            showLoading(false);
        },
        data: {
            controlid: objFile.ControlID, folderpath: objFile.FolderPath, maxfileuploadsizeinmb: objFile.MaxFileUploadSizeInMB,
            maxchunksizeinmb: objFile.MaxChunkSizeInMB, chunkintervalinsec: objFile.ChunkIntervalInSec,
            showdeleteaction: objFile.ShowDeleteAction, showattachmentbtn: objFile.ShowAttachmentBtn,
            isdisabled: objFile.IsDisabled, reloadafterupload: objFile.ReloadAfterUpload, isrequired: objFile.IsRequired,
            actionurl: objFile.ActionURL, type: objFile.type, allowfileextension: objFile.AllowFileExtension,
            successcallbackfunction: objFile.SuccessCallbackFunction, showdownloadbtn: objFile.ShowDownloadBtn,
            ismultipleuploaded: objFile.IsMultipleUploaded, orderby: objFile.OrderBy, filesdisplayfromurl: objFile.FilesDisplayFromURL,
            deletesuccesscallbackfunction: objFile.DeleteSuccessCallbackFunction, otherfolderpath: objFile.OtherFolderPath,
            removeonlycreatedbyme: objFile.RemoveOnlyCreatedByMe, documentloadcallbackfunction: objFile.DocumentLoadCallbackFunction,
            hideattachbtnforsignleupload: objFile.HideAttachbtnForSignleUpload
        },
        success: function (data) {
            debugger;
            if (objFile.LoadInPopup) {
                bootbox.dialog({
                    title: objFile.PopupTitle,
                    message: data,
                    size: "large",
                    buttons: {
                        Cancel: {
                            label: 'Close',
                            className: 'btn red',
                            callback: function () { }
                        }
                    }
                });
                if (!objFile.ShowUploadBtn) {
                    $('#btnUploadAll' + objFile.ControlID).hide()
                }
            }
            else {
                $("#" + objFile.DivId).html(data);
                if (!objFile.ShowUploadBtn) {
                    $('#btnUploadAll' + objFile.ControlID).hide()
                }
            }

            if (objFile.LoadCallbackFunction != null && objFile.LoadCallbackFunction != undefined && typeof objFile.LoadCallbackFunction === 'function') {
                objFile.LoadCallbackFunction();
            }
        },
        error: function () {
            response = "";
        }
    });
}
// ----- Write A Content Exist PDF File ----- //
function FileUploadWriteContentOnPDF(filenames, UploadFolderPath, content, posX, posY, rotation, align, position) {
    $.ajax({
        type: "POST",
        url: WebsiteURL + "/Utility/FileUpload/WriteContentOnPDF",
        beforeSend: function () { showLoading(true); },
        complete: function () { showLoading(false); },
        data: { filenames: filenames, UploadFolderPath: UploadFolderPath, content: content, posX: posX, posY: posY, rotation: rotation, align: align, position: position },
        success: function (data) {
            if (!data.Key) {
                DisplayNotiMessage("error", data.Value, "Error")
            }
            //else { DisplayNotiMessage("success", data.Value, "Success") }
        },
        error: function () {
        }
    });
}
// --------------------------------------------//
//function LoadFileUploadPartialView(divid, loadinpopup, controlid, folderpath, maxfileuploadsizeinmb, maxchunksizeinmb,
//                                    chunkintervalinsec, showdeleteaction, showattachmentbtn,
//                                    isdisabled, reloadafterupload, isrequired, actionurl, type, allowfileextension,
//                                    uploadsuccesscallbackfunction, loadcallbackfunction) {
//    if (controlid == undefined || controlid == null || controlid == "") {
//        DisplayNotiMessage("Error", "ControlId not define", "File Upload");
//        return;
//    }
//    //if (folderpath == undefined || folderpath == null || folderpath == "") {
//    //    DisplayNotiMessage("Error", "Folder Path Not Define", "File Upload");
//    //    return;
//    //}
//    if (uploadsuccesscallbackfunction === undefined || uploadsuccesscallbackfunction === null) {
//        uploadsuccesscallbackfunction = "";
//    }
//    $.ajax({
//        url: WebsiteURL + "/Utility/FileUpload/LoadFileUploadPartial",
//        type: "POST",
//        datatype: "html",
//        beforeSend: function () {
//            showLoading(true);
//        },
//        complete: function () {
//            showLoading(false);
//        },
//        data: {
//            controlid: controlid, folderpath: folderpath, maxfileuploadsizeinmb: maxfileuploadsizeinmb, maxchunksizeinmb: maxchunksizeinmb,
//            chunkintervalinsec: chunkintervalinsec, showdeleteaction: showdeleteaction, showattachmentbtn: showattachmentbtn,
//            isdisabled: isdisabled, reloadafterupload: reloadafterupload, isrequired: isrequired, actionurl: actionurl, type: type,
//            allowfileextension: allowfileextension, successcallbackfunction: uploadsuccesscallbackfunction
//        },
//        success: function (data) {
//            if (loadinpopup) {
//                bootbox.dialog({
//                    title: "File Upload",
//                    message: data,
//                    size: "large",
//                    buttons: {
//                        Cancel: {
//                            label: 'Close',
//                            className: 'btn red',
//                            callback: function () { }
//                        }
//                    }
//                });
//            }
//            else {
//                $("#" + divid).html(data);
//            }
//            if (loadcallbackfunction != null && loadcallbackfunction != undefined && typeof loadcallbackfunction === 'function') {
//                loadcallbackfunction();
//            }
//        },
//        error: function () {
//            response = "";
//        }
//    });
//}


function OpenProtocol(url, protocolType, protocolId) {

    /*$.ajax({
        type: "POST",
        url: WebsiteURL + "/Utility/General/ViewProtocolByPROD",
        data: { ProtocolType: protocolType, ProtocolId: protocolId },
        success: function (result) {
        },
        error: function () {
        }
    });*/

    window.open(url, "_blank");
}
function OpenAttachmentPopUp(Id, isEditable, path) {
    //var objFileUploadLine = new FileUploadEnt('dvFileUploadLineID' + Id, 'FileUploadLineID' + Id);
    //objFileUploadLine.ShowDeleteAction = isEditable;
    //objFileUploadLine.ShowAttachmentBtn = isEditable;
    //objFileUploadLine.FolderPath = path;
    //objFileUploadLine.LoadInPopup = true;
    //LoadFileUploadPartialView(objFileUploadLine);

    var vPath = path.split('/');
    var vTabelName = vPath[0];
    var vFinalPath = vPath[0] + '//' + Id + '//1';

    var objFile = {
        TableName: vTabelName,
        TableId: Id,
        Path: vFinalPath,
        ControlID: 'FileUploadLineID' + Id,
        ViewerId: 1,
        IsMultipleUploaded: true,
        ShowDeleteAction: isEditable,
        ShowDownloadBtn: false,
        ShowAttachmentBtn: isEditable
    };

    Load_File_Upload_Partial_View(objFile, 'dvFileUploadLineID' + Id, true);

}
function ConvertDDMMYYYYDateFormat(date) {
    // Opera 8.0+
    var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;

    // Firefox 1.0+
    var isFirefox = typeof InstallTrigger !== 'undefined';

    // Safari 3.0+ "[object HTMLElementConstructor]"
    var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification));

    // Internet Explorer 6-11
    var isIE = /*@cc_on!@*/false || !!document.documentMode;

    // Edge 20+
    var isEdge = !isIE && !!window.StyleMedia;

    // Chrome 1 - 71
    var isChrome = !!window.chrome && (!!window.chrome.webstore || !!window.chrome.runtime);

    // Blink engine detection
    var isBlink = (isChrome || isOpera) && !!window.CSS;
    var returndate = "";
    if (date.length > 0) {
        var splitDate = date.replace(new RegExp('-', 'g'), "/").split("/");

        if (isFirefox) {
            if (splitDate.length > 0) {
                returndate = splitDate[2] + "/" + splitDate[1] + "/" + splitDate[0];
            }
        }
        else if (isChrome) {
            if (splitDate.length > 0) {
                returndate = splitDate[2] + "/" + splitDate[1] + "/" + splitDate[0];
            }
        }
    }
    console.log(returndate);
    return returndate;
}

function DownloadAllDocuments(btnDownloadAll, folderPath, Type, includeFromSubfolders) {
    bootbox.dialog({
        title: "Download Confirmation",
        message: 'Do you want to download all attachment?',
        size: "medium",
        buttons: {
            Yes: {
                label: 'Yes',
                className: 'btn btn-primary',
                callback: function () {
                    var fp = [];
                    $(btnDownloadAll).closest('.formupload').find(".tbldocuments").find('tbody tr').each(function () {
                        debugger
                        console.log($(this).attr("data-fp"));
                        if (fp.length == 0) { fp.push($(this).attr("data-fp")) }
                        else {
                            if (!checkItemExistInArray($(this).attr("data-fp"), fp)) {
                                fp.push($(this).attr("data-fp"))
                            }
                        }
                    });
                    //console.log(fp);
                    //console.log(fp.join(","));
                    var finalFolderPath = fp.join(",");
                    if (Type === undefined || Type == null) {
                        Type = "";
                    }
                    if (includeFromSubfolders === undefined || includeFromSubfolders == null) {
                        includeFromSubfolders = false;
                    }
                    window.open(WebsiteURL + '/Utility/FileUpload/DownloadZipFile?FolderPath=' + finalFolderPath + '&Type=' + Type + '&includeFromSubfolders=' + includeFromSubfolders, "_blank");
                }
            },
            Cancel: {
                label: 'No',
                className: 'btn red',
                callback: function () { }
            }
        }
    });
}

function DestroyAutoComplete(element) {
    var attr = $(element).attr('autocomplete');
    if (typeof attr !== typeof undefined && attr !== false) {
        $(element).autocomplete("destroy");
        $(element).removeData('autocomplete');
        $(element).unbind("focus");
    }
}

function FillAutoCompleteWithCallBack(element, sourceList, hdElement, fnCallback) {
    element.autocomplete({
        source: function (request, response) {
            var array = $.map(sourceList, function (item) {
                return {
                    label: item.CatDesc,
                    value: item.CatID,
                    id: item.CatID
                }
            });
            response($.ui.autocomplete.filter(array, request.term));
        },
        autoFocus: true,
        minLength: 0,
        select: function (event, ui) {
            event.preventDefault();
            $(this).val(ui.item.label);
            $(hdElement).val(ui.item.value);

            //if (fnCallback != null && fnCallback != undefined && fnCallback != 'undefined') {
            //    fnCallback();
            //}
        },
        change: function (event, ui) {
            if (ui.item == null) {
                element.val('');
                element.focus();
                $(hdElement).val('');
            }
            if (fnCallback != null && fnCallback != undefined && fnCallback != 'undefined') {
                fnCallback();
            }
        }
    }).focus(function () {
        $(this).data("uiAutocomplete").search($(this).val());
        $(this).autocomplete("search");
    });
}
function FillAutoCompleteWithCallBackNoFocus(element, sourceList, hdElement, fnCallback) {
    return element.autocomplete({
        source: function (request, response) {
            var array = $.map(sourceList, function (item) {
                return {
                    label: item.CatDesc,
                    value: item.CatID,
                    id: item.CatID
                }
            });
            response($.ui.autocomplete.filter(array, request.term));
        },
        autoFocus: true,
        minLength: 0,
        select: function (event, ui) {
            event.preventDefault();
            $(this).val(ui.item.label);
            if (hdElement !== null && hdElement !== undefined)
                $(hdElement).val(ui.item.value);
        },
        change: function (event, ui) {
            if (ui.item == null) {
                element.val('');
                element.focus();
                if (hdElement !== null && hdElement !== undefined)
                    $(hdElement).val('');
            }
            if (fnCallback != null && fnCallback != undefined && fnCallback != 'undefined') {
                fnCallback();
            }
        }
    });
}

function FillAutoCompleteWithCallBackParam(element, sourceList, hdElement, fnCallback) {
    element.autocomplete({
        source: function (request, response) {
            var array = $.map(sourceList, function (item) {
                return {
                    label: item.CatDesc,
                    value: item.CatID,
                    id: item.CatID
                }
            });
            response($.ui.autocomplete.filter(array, request.term));
        },
        autoFocus: true,
        minLength: 0,
        select: function (event, ui) {
            event.preventDefault();
            $(this).val(ui.item.label);
            $(hdElement).val(ui.item.value);

            if (fnCallback != null && fnCallback != undefined && fnCallback != 'undefined') {
                fnCallback();
            }
        },
        change: function (event, ui) {
            if (ui.item == null) {
                element.val('');
                element.focus();
                $(hdElement).val('');

                if (fnCallback != null && fnCallback != undefined && fnCallback != 'undefined') {
                    fnCallback();
                }
            }
        }
    }).focus(function () {
        $(this).data("uiAutocomplete").search($(this).val());
    });
}
function IsQueryString() {
    var url = location.href;
    var strpath = url.replace(WebsiteURL + "/", "").replace("#", "");
    $.ajax({
        type: "POST",
        url: WebsiteURL + "/MyDashboard/Dashboard/IsQueryString",
        datatype: 'json',
        data: { strpath: strpath },
        success: function (result) {
            if (result.Key) {
                $("#liFav").hide();
            }
            if (result.ActionKey) {
                $("#Bookmark").addClass("removeBookMark").removeClass("addBookMark");
                $("#Bookmark").html('<i class="fa fa-star bookmarkicon" ></i>')
                $("#IconBookmarkId").val(result.HeaderId)
            }
            else {
                $("#Bookmark").addClass("addBookMark").removeClass("removeBookMark");
                $("#Bookmark").html('<i class="fa fa-star-o nobookmark" ></i>')
                $("#IconBookmarkId").val("");
            }
        }
    });
}

function DeleteFavoritesProcess(id) {
    $.ajax({
        type: "POST",
        url: WebsiteURL + "/MyDashboard/Dashboard/DeleteFavoritesProcess",
        datatype: 'json',
        data: { id: parseInt(id) },
        async: false,
        //beforeSend: function () {
        //    //showLoading(true);
        //},
        //complete: function () {
        //    showLoading(false);
        //},
        success: function (result) {
            var strpath = location.href.replace(WebsiteURL + "/", "").replace("#", "");
            if (strpath == "MyDashboard/Dashboard/Index" || strpath == "MyDashboard/Dashboard/") {
                LoadFavoriteTabPartial();

            }
            IsQueryString()
            //$("#Bookmark").addClass("addBookMark").removeClass("removeBookMark");
            //$("#IconBookmark").addClass("nobookmark").removeClass("bookmarkicon");
            //$("#IconBookmarkId").val("");
        },
        error: function () {
        }
    });
}
function addFavorites(url) {
    var strpath = url.replace(WebsiteURL + "/", "").replace("#", "");
    $.ajax({
        url: WebsiteURL + "/Utility/General/addFavoritesProcess",
        type: "POST",
        datatype: "json",
        async: false,
        beforeSend: function () {
            // showLoading(true);
        },
        complete: function () {
            showLoading(false);
        },
        data: { strpath: strpath },
        success: function (data) {
            if (data.Key) {

                $("#Bookmark").addClass("removeBookMark").removeClass("addBookMark");
                $("#Bookmark").html('<i class="fa fa-star bookmarkicon" ></i>')
                $("#IconBookmarkId").val(data.HeaderId)
                if (strpath == "MyDashboard/Dashboard/Index" || strpath == "MyDashboard/Dashboard/") {
                    LoadFavoriteTabPartial();
                }
            }
            else {
                DisplayNotiMessage("info", data.Value, "info");
            }
        },
        error: function () {
            // DisplayNotiMessage("error", "Error occured please try again", "Error");
        }

    });
}
function resetLocalStorage(url) {
    localStorage.setItem("selectedmenu", null);
    location.href = WebsiteURL + url;
}

function ConvertDDMMYYYYToYYYYMMDDDateFormat(date) {
    var returndate = "";
    var splitDate = date.replace(new RegExp('-', 'g'), "/").split("/");
    returndate = splitDate[2] + "/" + splitDate[1] + "/" + splitDate[0];
    return returndate;
}

function UpdateAnyTable(TableName, PrimaryId, PrimaryColumnName, UpdateColumnName, UpdateColumnValue, SetNullForBlank) {
    if (SetNullForBlank == undefined || SetNullForBlank == null) {
        SetNullForBlank = true;
    }
    $.ajax({
        url: WebsiteURL + "/Utility/General/UpdateAnyTable",
        type: "POST",
        datatype: "json",
        async: false,
        beforeSend: function () {
            showLoading(true);
        },
        complete: function () {
            showLoading(false);
        },
        data: { TableName: TableName, PrimaryId: parseInt(PrimaryId), PrimaryColumnName: PrimaryColumnName, UpdateColumnName: UpdateColumnName, UpdateColumnValue: UpdateColumnValue, SetNullForBlank: SetNullForBlank },
        success: function (data) {
            if (!data.Key) {
                DisplayNotiMessage("info", data.Value, "Information")
            }
            //else { DisplayNotiMessage("success", data.Value, "Success") }
        },
        error: function () {
            DisplayNotiMessage("error", "Error occured please try again", "Error");
        }
    });
}

function DDMMYYYYDateCompare(FromDate, ToDate) {
    var fdate = ConvertDDMMYYYYToYYYYMMDDDateFormat(FromDate)
    var tdate = ConvertDDMMYYYYToYYYYMMDDDateFormat(ToDate);
    if (fdate != "" && tdate != "") {
        var fnew = new Date(fdate);
        var tnew = new Date(tdate);
        if (fnew > tnew) {
            DisplayNotiMessage("info", "From date must be less than or equal to todate!", "Information")
            return false;
        }
        else {
            return true;
        }
    }
    else {
        return true;
    }
}

function LoadHTMLPartialView(url, divid, status) {
    $("#" + divid).empty();

    $.ajax({
        type: "POST",
        url: WebsiteURL + url,
        data: { status: status },
        datatype: 'html',
        beforeSend: function () {
            showLoading(true);
        },
        complete: function () {
            showLoading(false);
        },
        success: function (result) {
            $("#" + divid).html(result);
        },
        error: function () {
        }
    });
}

oDTDefLanguage = {
    "aria": {
        "sortAscending": ": activate to sort column ascending",
        "sortDescending": ": activate to sort column descending"
    },
    "emptyTable": "No data available in table",
    "info": "Showing _START_ to _END_ of _TOTAL_ entries",
    "infoEmpty": "No entries found",
    "infoFiltered": "(filtered1 from _MAX_ total entries)",
    "lengthMenu": "_MENU_ entries",
    "search": "Search:",
    "zeroRecords": "No matching records found"
}
function LoadInlineGridTables(tableid, newbtnid, Columns, AjaxSource, parameterArray, extrafn) {
    var table = $('#' + tableid);
    var oTable = table.dataTable({
        "language": oDTDefLanguage,
        "bProcessing": false,
        "bServerSide": true,
        "stateSave": false,
        buttons: [],
        "bDestroy": true,
        responsive: true,
        "order": [
            [0, 'asc']
        ],
        "lengthMenu": [
            [5, 10, 25, 50, 100],
            [5, 10, 25, 50, 100]
        ],
        "pageLength": 10,
        'iDisplayLength': 10,
        "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
        "sAjaxSource": WebsiteURL + AjaxSource + GetQueryStringParameterList(parameterArray),
        "fnServerParams": function (aoData) {
            aoData.push({ "name": "type", "value": "sdf" }
            );
        },
        "fnServerData": function (sSource, aoData, fnCallback) {
            $.ajax({
                'dataType': 'json',
                'type': 'POST',
                'url': sSource,
                'data': aoData,
                'success': function (data) {
                    fnCallback(data);
                },
                'async': false
            });
            $('input[maxlength]').maxlength({
                alwaysShow: true,
            });
            ResetAddRowLine(tableid, newbtnid);
            SetControlNumeric();
            if (extrafn && (typeof extrafn == "function")) {
                extrafn();
            }
        },
        "columns": Columns
    });
}

function LoadGridTables(tableid, Columns, AjaxSource, parameterArray, extrafn, rowDataCallBack) {
    var table = $('#' + tableid);
    var oTable = table.dataTable({
        "language": oDTDefLanguage,
        "bProcessing": false,
        "bServerSide": true,
        "stateSave": false,
        buttons: [],
        "bDestroy": true,
        responsive: true,
        "order": [
            [0, 'asc']
        ],
        "lengthMenu": [
            [5, 10, 25, 50, 100],
            [5, 10, 25, 50, 100]
        ],
        "pageLength": 10,
        'iDisplayLength': 10,
        "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
        "sAjaxSource": WebsiteURL + AjaxSource + GetQueryStringParameterList(parameterArray),
        "fnServerParams": function (aoData) {
            aoData.push({ "name": "type", "value": "sdf" }
            );
        },
        "fnServerData": function (sSource, aoData, fnCallback) {
            $.ajax({
                'dataType': 'json',
                'type': 'POST',
                'url': sSource,
                'data': aoData,
                'success': function (data) {
                    fnCallback(data);
                },
                'async': false
            });
            if (extrafn && (typeof extrafn == "function")) {
                extrafn(aoData);
            }
        },
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            if (rowDataCallBack && (typeof rowDataCallBack == "function")) {
                rowDataCallBack(nRow, aData);
            }
        },
        "columns": Columns
    });
}

function getCheckedFromTable(table, isCheckedAll) {
    var arrayHeaderIds = new Array();
    if (isCheckedAll && $('#example-select-all', '#' + table).is(':checked')) {
        arrayHeaderIds.push($('#example-select-all', '#' + table).val());
    } else {
        $('input[class="clsCheckbox"]:checked', '#' + table + ' tbody').each(function () {
            var headerId = $(this).val();
            if (headerId != '')
                arrayHeaderIds.push(headerId);
        });
    }
    if (arrayHeaderIds.length > 0)
        return arrayHeaderIds.join(",");
    return "";
}
function GetQueryStringParameterList(list) {
    var q = "";
    for (var i = 0; i < parameterList.length; i++) {
        for (var j = 0; j < list.length; j++) {
            if (list[j] == parameterList[i]["id"]) {
                if (q.length == 0) {
                    q = "?" + parameterList[i]["id"] + "=" + parameterList[i]["value"];
                }
                else {
                    q = q + "&" + parameterList[i]["id"] + "=" + parameterList[i]["value"];
                }
            }
        }
    }
    //console.log(q);
    return q;
}

// Created by Ravi Makwana for create Textbox on column rander
// How to use: find example in MCR/_getLineGridDataPartial.cshtml
function ColumnTextBoxRender(rowId, columnName, columnValue, onBlurMethod, maxlength, disabled, isReadOnly, inputStyle, role) {
    columnValue = columnValue ? columnValue : ""; // replace null with empty space char
    if (columnValue.indexOf("<input") != -1 || columnValue.indexOf("<select") != -1) // already textbox
        return columnValue;
    var htmlControl = "";
    var inputID = columnName + rowId;
    var MaxCharcters = maxlength ? "maxlength='" + maxlength + "'" : "";
    var onBlurEvent = onBlurMethod ? "onblur='" + onBlurMethod + "'" : "";
    var userRole = role ? "role = '" + role + "'" : "";
    htmlControl = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' name='" + inputID + "' colname='" + columnName + "' " + MaxCharcters + " value='" + columnValue + "' " + userRole + " class='form-control' style='" + inputStyle + "' " + onBlurEvent + " " + (disabled ? "disabled" : "") + " />";
    return htmlControl;
}

function FillAutoCompleteUsingAjax(element, url, hdElement, autoFocus, parameterArray, extrafn, minLength, isDirectValue, fnResponseMapper) {
    if (autoFocus == null || autoFocus == undefined || autoFocus == "undefined") {
        autoFocus = false;
    }
    if (minLength == null || minLength == undefined || minLength == "undefined") {
        minLength = 0;
    }
    if (parameterArray == null || parameterArray == undefined || parameterArray == "undefined") {
        parameterArray = [];
    }
    element.autocomplete({
        source: function (request, response) {
            $.ajax({
                url: WebsiteURL + url + GetExtraParameters(parameterArray, isDirectValue),
                dataType: "json",
                type: "POST",
                delay: 250,
                async: false,
                data: {
                    term: request.term
                },
                success: function (data) {
                    if (fnResponseMapper && typeof (fnResponseMapper) === "function") {
                        response(fnResponseMapper(data));
                    } else {
                        response($.map(data, function (item) {
                            return { label: item.Text, value: item.Text, id: item.Value };
                        }));
                    }
                    if (data.length > 0) {
                        isSelected = true;
                    }
                    else {
                        isSelected = false;
                    }
                }
            });
        },
        autoFocus: autoFocus,
        minLength: minLength,
        select: function (event, ui) {
            $(this).val(ui.item.id);
            $(hdElement).val(ui.item.id);
            //HighlightInValidControls();
            if (extrafn && (typeof extrafn == "function")) {
                extrafn();
            }
        },
        change: function (event, ui) {
            if (ui.item == null) {
                $(hdElement).val("");
                element.val('');
                element.focus();
                if (extrafn && (typeof extrafn == "function")) {
                    extrafn();
                }
                //HighlightInValidControls();               
            }
        }
    }).focus(function () {
        if ($(this).data("uiAutocomplete") != null && $(this).data("uiAutocomplete") != undefined) {
            $(this).data("uiAutocomplete").search($(this).val());
            //$(this).autocomplete("search");
        }
    });
}

function FillAutoCompleteForSubCategory(element, Key, hdElement, BU, Location, ShowOnTextbox) {
    $(element).autocomplete({
        source: function (request, response) {
            $.ajax({
                url: WebsiteURL + "/Utility/General/GetSubCatagoryForAutoComplete",    //GetProjectResult",
                dataType: "json",
                type: "POST",
                delay: 250,
                async: false,
                data: {
                    term: request.term,
                    key: Key,
                    BU: typeof (BU) === "string" ? BU : $(BU).val().split("-")[0].trim(),
                    Loc: typeof (Location) === "string" ? Location : $(Location).val().split("-")[0].trim(),
                },
                success: function (data) {
                    if (ShowOnTextbox == "id") {
                        response($.map(data, function (item) {
                            return { label: item.label, value: item.id, id: item.value };
                        }));
                    }
                    else
                        response(data);
                    if (data.length > 0) {
                        isSelected = true;
                    }
                    else {
                        isSelected = false;
                    }
                }
            });
        },
        minLength: 0,
        autoFocus: true,
        change: function (event, ui) {
            if (ui.item == null) {
                $(this).val('');
                $(hdElement).val('');
            }
        },
        select: function (event, ui) {
            if (ui.item) {
                $(hdElement).val(ui.item.id);
            }
        },
    }).focus(function () {
        $(this).data("uiAutocomplete").search($(this).val());
        $(this).autocomplete("search");
    });
}
function GetExtraParameters(list, isDirectValue) {
    var q = "";
    if (!isDirectValue) {
        for (var i = 0; i < list.length; i++) {
            if (q.length == 0) {
                q = "?" + list[i]["id"] + "=" + $(list[i]["value"]).val();
            }
            else {
                q = q + "&" + list[i]["id"] + "=" + $(list[i]["value"]).val();
            }
        }
    }
    else {
        for (var i = 0; i < list.length; i++) {
            if (q.length == 0) {
                q = "?" + list[i]["id"] + "=" + list[i]["value"];
            }
            else {
                q = q + "&" + list[i]["id"] + "=" + list[i]["value"];
            }
        }
    }
    //console.log(q);
    return q;
}
function SaveLineData(formdata, URL, table, btnNew) {
    var tabledata = $("#" + table).find("input,select,textarea,hidden").serializeArray();
    if (tabledata != null && tabledata != undefined) {
        $.each(tabledata, function (key, input) {
            formdata.append(input.name, input.value);
        });
    }
    $.ajax({
        url: WebsiteURL + URL,
        type: "POST",
        data: formdata,
        async: false,
        contentType: false,
        processData: false,
        success: function (res) {
            if (res.Key) {
                DisplayNotiMessage("success", res.Value, "Success");
                $('#' + table).DataTable().ajax.reload();
                ShowNewLineRecord($('#' + btnNew));
            }
            else
                DisplayNotiMessage("error", res.Value, "Error");
        },
        error: function (data) { }
    });
}

function SetControlNumeric() {
    $(".numeric-only").autoNumeric('init', { aSep: '', vMax: "9999999.99" });
    $('input[maxlength],textarea[maxlength]').maxlength({
        alwaysShow: true,
    });
}

function ChecklistGridValidate() {
    $('#tblPlanChecklist tbody tr td select').each(function () {
        $this = $(this);
        if ($this.attr('colname') != null && $this.attr('colname') == ("Yes_No") && $this.val() == "") {
            $this.attr("required", "required");
        }
    });
}

function SetCategoryEffact(cat) {
    $("#ddlCategory").val(cat);

    $("#WidthOD").removeAttr("readonly");
    $("#Thickness").removeAttr("readonly");

    if (cat == "Structural") {
        $("#txtMaterial").attr("placeholder", "Select");
        $("#txtMaterial").val('');
        $("#ddlMaterial").val('');
        $(".col-md-12 #structural-type").css('display', 'block');
        $(".col-md-12 #div-size").css('display', 'block');
        $(".col-md-12 #pipe-normal-bore").css('display', 'none');
        $(".col-md-12 #pipe-schedule").css('display', 'none');
        $("#Unit").val('');
        $("#WidthOD").removeAttr("required");
        $("#Thickness").removeAttr("required");
        $("#WidthOD").val('');
        $("#Thickness").val('');
        $("#WidthOD").prop("readonly", true);
        $("#Thickness").prop("readonly", true);
        FillMaterial(1);
    }
    else if (cat == "Pipe-Tube") {
        $("#txtMaterial").attr("placeholder", "Select");
        $("#txtMaterial").val('');
        $("#ddlMaterial").val('');
        $(".col-md-12 #structural-type").css('display', 'none');
        $(".col-md-12 #div-size").css('display', 'none');
        $(".col-md-12 #pipe-normal-bore").css('display', 'block');
        $(".col-md-12 #pipe-schedule").css('display', 'block');
        $("#WidthOD").removeAttr("required");
        $("#Thickness").removeAttr("required");
        $("#WidthOD").val('');
        $("#Thickness").val('');
        $("#WidthOD").prop("readonly", true);
        $("#Thickness").prop("readonly", true);
        FillMaterial(2);
    }
    else {
        $("#txtMaterial").attr("placeholder", "");
        //  $("#txtMaterial").val('');
        //  $("#ddlMaterial").val('');
        $(".col-md-12 #structural-type").css('display', 'none');
        $(".col-md-12 #div-size").css('display', 'none');
        $(".col-md-12 #pipe-normal-bore").css('display', 'none');
        $(".col-md-12 #pipe-schedule").css('display', 'none');
        $("#WidthOD").attr("required", "required");
        $("#Thickness").attr("required", "required");
        if ($("#txtMaterial").data('ui-autocomplete') != undefined) {
            $("#txtMaterial").autocomplete("destroy");
            $("#txtMaterial").removeData('autocomplete');
            $("#txtMaterial").val("");
            $("#ddlMaterial").val("");
        }
    }
    if (cat == "Rod" || cat == "Wire-Rod") {
        $("#WidthOD").removeAttr("required");
        $("#WidthOD").val('');
        $("#WidthOD").prop("readonly", true);

    }

    if (cat == "Structural" || cat == "Pipe-Tube" || cat == "Rod" || cat == "Sq.Bar") {
        $("#Unit").val('m');
        $("#Unit2").val('m');
    }
    else if (cat == "Plate" || cat == "Sheet") {
        $("#Unit").val('m2');
        $("#Unit2").val('m2');
    }
    else if (cat == "Wire-Rod") {
        $("#Unit").val('Kg');
        $("#Unit2").val('Kg');
    }
    else if (cat == "Fastner") {
        $("#Unit").val('Nos');
        $("#Unit2").val('Nos');
        $("#WidthOD").removeAttr("required");
        $("#Thickness").removeAttr("required");
        $("#WidthOD").prop("readonly", true);
        $("#Thickness").prop("readonly", true);
    }
}

function FillMaterial(id) {
    $.ajax({
        url: WebsiteURL + "/Utility/General/GetMaterialFromCategory?Id=" + id,
        type: "GET",
        async: false,
        beforeSend: function () {
            showLoading(true);
        },
        complete: function () {
            $("#txtMaterial").focus();
            showLoading(false);
        },
        datatype: "html",
        success: function (data) {
            if (data != null && data.length > 0) {
                FillAutoComplete($("#txtMaterial"), data, $("#ddlMaterial"));
                //alert('hi');
            }
        },
        error: function () {
            setTimeout(DisplayNotiMessage("error", "Unexpected error for getting materials", "Error"), 1000);
        }
    });
}

function MaterialChange(category, material) {
    if (material != "") {
        if (category == "Structural" && material != "") {
            GetWeight(material, "Structural");
            $("#StructuralType").val(material.split('-')[0].trim());
            $("#Size").val(material.split('-')[1].trim());
        }
        else if (category == "Pipe-Tube" && material != "") {
            GetWeight(material, "Pipe-Tube");
            $("#PipeNormalBore").val(material.split('-')[0].trim());
            $("#PipeSchedule").val(material.split('-')[1].trim());
        }
        else {
            $("#ddlMaterial").val(material);
            $("#CalculatedWeight").val('');

        }
        if ($("#txtMaterialType").val() != "") {
            SetValueOfDensity($("#txtMaterialType").val())
        }
    }
    else {
        $("#ddlMaterial").val("");
        $("#CalculatedWeight").val('');
    }

    if ($("#Qty").val() != "") {
        //   OnTextChange();
    }
}

function GetWeight(material, type) {
    if (material != "") {
        $.ajax({
            url: WebsiteURL + "/Utility/General/GetWeightValue",
            type: "POST",
            datatype: "html",
            data: { material: material, type: type },
            success: function (data) {
                if (data != null) {
                    $("#CalculatedWeight").val(data);

                }
            },
            error: function () {
                setTimeout(DisplayNotiMessage("error", "Unexpected error for getting weight", "Error"), 1000);
            }
        });
    }
}

function IsReUse(isreuse) {
    if (isreuse) {
        $("#ReqWt").val("0");
        $('#txtReUse').val("True");
    }
    else {
        if ($("#Wt").val() != "") { $("#ReqWt").val($("#Wt").val()); } else { $("#ReqWt").val("0"); }
        $('#txtReUse').val("False");
    }
}

function OnTextChange() {
    var isError = false;
    var category = $("#txtCategory").val();
    var QTyFXR = parseFloat($("#QtyofFixture").val());
    var Length = parseFloat($("#LengthOD").val());
    var Width = parseFloat($("#WidthOD").val());
    var Thickness = parseFloat($("#Thickness").val());
    var Density = parseFloat($("#Density").val());
    var Weight = parseFloat($("#CalculatedWeight").val());


    var Qty = parseFloat($("#Qty").val());
    if (category == "") {
        isError = true;
        setTimeout(DisplayNotiMessage("error", "Please select category.", "Error"), 1000);
    }
    if (isNaN(QTyFXR)) {
        isError = true;
        setTimeout(DisplayNotiMessage("error", "Quantity of fixture is required for material calculations.", "Error"), 1000);
    }
    if (isNaN(Length)) {
        isError = true;
        setTimeout(DisplayNotiMessage("error", "Length is required for material calculations.", "Error"), 1000);
    }
    if (category == "Structural" || category == "Pipe-Tube" || category == "Rod" || category == "Wire-Rod" || category == "Fastner") {

    }
    else {
        if (isNaN(Width)) {
            isError = true;
            setTimeout(DisplayNotiMessage("error", "Width is required for material calculations.", "Error"), 1000);
        }
    }
    if (category == "Structural" || category == "Pipe-Tube" || category == "Fastner") { }
    else {
        if (isNaN(Thickness)) {
            isError = true;
            setTimeout(DisplayNotiMessage("error", "Thickness is required for material calculations.", "Error"), 1000);
        }
    }
    if (isNaN(Qty)) {
        isError = true;
        setTimeout(DisplayNotiMessage("error", "Qty is required for material calculations.", "Error"), 1000);
    }

    if ((category == "Structural" || category == "Pipe-Tube") && isNaN(Weight)) {
        isError = true;
        setTimeout(DisplayNotiMessage("error", "Material is required for material calculations.", "Error"), 1000);
    }

    if (!isError) {
        CalculateWeight();
        CalculateArea();
        Reuse();
    }
}

function CalculateWeight() {
    var category = $("#ddlCategory").val();
    var QTyFXR = parseFloat($("#QtyofFixture").val());
    var Length = parseFloat($("#LengthOD").val());
    var Width = parseFloat($("#WidthOD").val());
    var Thickness = parseFloat($("#Thickness").val());
    var Qty = parseFloat($("#Qty").val());
    var Density = parseFloat($("#Density").val());

    if (category == "Fastner") {
        $("#Wt").val("0.00");
    }
    else if (category == "Sheet") {

        var formula = (((((Length * Width) * Thickness) * Qty) / 1000000) * Density);
        $("#Wt").val(formula.toFixed(2));
    }
    else if (category == "Structural" || category == "Pipe-Tube") {
        var Weight = parseFloat($("#CalculatedWeight").val());

        var formula = ((((Length / 1000) * Qty) * Weight) * QTyFXR);
        $("#Wt").val(formula.toFixed(2));

    }
    else if (category == "Plate" || category == "Sq.Bar") {
        var formula = (((((((3.141592654 * (Length - Thickness)) * Width) * Thickness) * Qty) / 1000000) * Density) * QTyFXR);
        $("#Wt").val(formula.toFixed(2));
    }
    else if (category == "Rod") {
        var formula = ((3.141592654 / 4) * (Thickness * Thickness * Length * Qty * Density * QTyFXR)) / (1000000);
        $("#Wt").val(formula.toFixed(2));
    }
}

function CalculateArea() {
    var category = $("#ddlCategory").val();
    var QTyFXR = parseFloat($("#QtyofFixture").val());
    var Length = parseFloat($("#LengthOD").val());
    var Width = parseFloat($("#WidthOD").val());
    var Thickness = parseFloat($("#Thickness").val());
    var Qty = parseFloat($("#Qty").val());
    var Density = parseFloat($("#Density").val());

    if (category == "Structural" || category == "Pipe-Tube" || category == "Rod" || category == "Sq.Bar") {
        var formula = (((Qty * Length) * QTyFXR) / 1000);
        $("#Area").val(formula.toFixed(2));
    }
    else if (category == "Sheet") {
        var formula = (((Length * Width) * Qty) / 1000000);
        $("#Area").val(formula.toFixed(2));
    }
    else if (category == "Plate") {
        var formula = ((((Length * Width) * Qty) * QTyFXR) / 1000000);
        $("#Area").val(formula.toFixed(2));
    }
    else if (category == "Wire-Rod") {
        var formula = ((((((((3.141592654 / 4) * Thickness) * Thickness) * Length) * Qty) * Density) / 1000000) * QTyFXR);
        $("#Area").val(formula.toFixed(2));
        $("#Wt").val(formula.toFixed(2));
    }
    else if (category == "Fastner") {
        var formula = (Qty * QTyFXR);
        $("#Area").val(formula.toFixed(2));
    }
}

function Reuse() {
    if ($('#txtReUse').val() == "False") {
        if ($("#Wt").val() != "") {
            $("#ReqWt").val($("#Wt").val());
        } else {
            $("#ReqWt").val("0");
        }
    }
    else {
        $("#ReqWt").val("0");
    }
}

function SetCalculateValue(actions) {
    if (actions != 'add') {
        OnTextChange();
    }
}

function OnMaterialTypeBlur(materialTye) {
    if ($(materialTye).val() != "") {
        SetValueOfDensity($(materialTye).val());
    }
    else {
        $("#Density").val('');
        var Weight = parseFloat($("#CalculatedWeight").val());
        if (isNaN(Weight)) {
            setTimeout(DisplayNotiMessage("error", "Please select material type for density calculation.", "Error"), 1000);
            $("#ddlMaterialType").val('');
            $(materialTye).val('');
        }
    }
}

function SetValueOfDensity(name) {
    if (name != "") {
        $.ajax({
            url: WebsiteURL + "/Utility/General/GetDensityValue",
            type: "POST",
            data: { name: name },
            datatype: "html",
            success: function (data) {
                if (data != null) {
                    $("#Density").val(data);
                }
            },
            error: function () {
                setTimeout(DisplayNotiMessage("error", "Unexpected error for getting density value", "Error"), 1000);
            }
        });
    }
}

function getData(lineID, headerID, processtype) {
    $.ajax({
        type: "POST",
        url: WebsiteURL + "/Utility/General/getFXRData",
        datatype: "Json",
        async: false,
        data: { HeaderId: headerID, LineID: lineID, processType: processtype },
        success: function (result) {
            if (result == "") {
                //$("#QtyofFixture").val('')
                //$("#QtyofFixture").prop( "disabled", false );


            }
            else {
                //$("#QtyofFixture").val(result)
                //$("#QtyofFixture").prop( "disabled", true );
                $("#QtyofFixture").attr("readonly", "readonly");
            }
        },
        error: function () {
        }
    });
}

function FillControl(strAction, category, material, materialType, unit, unit2, structureType, pipeandbore, pipeShcedule, headerId) {
    $("#strAction").val(strAction);
    $("#txtCategory").val(category);
    $("#ddlCategory").val(category);
    $("#txtMaterial").val(material);
    $("#ddlMaterial").val(material);
    $("#txtMaterialType").val(materialType);
    $("#ddlMaterialType").val(materialType);
    $("#Unit").val(unit);
    $("#Unit2").val(unit2);
    //$("#txtStructuralType").val(structureType);
    //$("#ddlStructuralType").val(structureType);
    //$("#txtPipeNormalBore").val(pipeandbore);
    //$("#ddlPipeNormalBore").val(pipeandbore);
    //$("#txtPipeSchedule").val(pipeShcedule);
    //$("#ddlPipeSchedule").val(pipeShcedule);


    if (headerId > 0) {
        if ($("#txtMaterial").val() != "") {
            if ($("#txtCategory").val() == "Structural" && $("#txtMaterial").val() != "") {
                GetWeight($("#txtMaterial").val(), "Structural");
                $("#WidthOD").removeAttr("required");
                $("#Thickness").removeAttr("required");
                $("#WidthOD").val('0');
                $("#Thickness").val('0');
                $("#WidthOD").prop("readonly", true);
                $("#Thickness").prop("readonly", true);
                FillMaterial(1);

            }
            else if ($("#txtCategory").val() == "Pipe-Tube" && $("#txtMaterial").val() != "") {
                GetWeight($("#txtMaterial").val(), "Pipe-Tube");
                $("#WidthOD").removeAttr("required");
                $("#Thickness").removeAttr("required");
                $("#WidthOD").val('0');
                $("#Thickness").val('0');
                $("#WidthOD").prop("readonly", true);
                $("#Thickness").prop("readonly", true);
                FillMaterial(2);
            }
            else {
                $("#ddlMaterial").val($("#txtMaterial").val());
                $("#CalculatedWeight").val('');
            }

            if ($("#txtCategory").val() == "Wire-Rod") {
                $("#WidthOD").removeAttr("required");
                $("#WidthOD").prop("readonly", true);
            }
            else if ($("#txtCategory").val() == "Rod") {
                $("#WidthOD").removeAttr("required");
                $("#WidthOD").prop("readonly", true);
            }
            else if ($("#txtCategory").val() == "Fastner") {
                $("#WidthOD").removeAttr("required");
                $("#Thickness").removeAttr("required");
                $("#WidthOD").prop("readonly", true);
                $("#Thickness").prop("readonly", true);
            }
        }
        else {
            $("#ddlMaterial").val("");
            $("#CalculatedWeight").val('');
        }

        if ($("#txtMaterialType").val() != "") {
            SetValueOfDensity($("#txtMaterialType").val())
        }
    }
}

function daydiff(first, second) {
    var oneDay = 24 * 60 * 60 * 1000;
    return Math.round(Math.abs((first.getTime() - second.getTime()) / (oneDay)));
}

function IsValidDate(fxrReqdate, matReqdate) {
    var a = fxrReqdate.split('/');
    var b = matReqdate.split('/');

    var first = new Date(a[2] + "," + a[1] + "," + a[0]);
    var second = new Date(b[2] + "," + b[1] + "," + b[0]);

    if (second >= first) {
        return false;
    }
    else {
        var days = daydiff(first, second);
        if (days < 15) {
            return false;
        }
        else {
            return true;
        }
    }
}

function EnableDisableAutoComplete(cnt, IsDisable) {
    if (IsDisable === null || IsDisable === undefined) {
        IsDisable = false;
    }
    if (IsDisable) {
        $("#" + cnt).autocomplete("disable");
    }
    else {
        $("#" + cnt).autocomplete("enable");
    }
}

function EnableDisableMultipleAutoComplete(cnt, IsDisable) {
    if (IsDisable === null || IsDisable === undefined) {
        IsDisable = false;
    }
    if (IsDisable) {
        $(cnt).autocomplete("disable");
    }
    else {
        $(cnt).autocomplete("enable");
    }
}
//function DestroyAutoComplete(cnt) {
//    $("#" + cnt).autocomplete("destroy");
//    $("#" + cnt).unbind("focus");
//}

function ReloadDatatables(tbl) {
    $('#' + tbl).DataTable().ajax.reload();
}

function ColorServer(color) {
    $.getJSON(WebsiteURL + '/manifest.json', function (data) {
        for (r in data) {
            var key = r;
            var val = data[r];
            if (key == "application_server_color") {
                $("#" + color).css({ color: val });
                break;
            }
        }
    });
}

function CheckIsPostValidation(table, action, headerid, isapprover) {
    var validate = false;
    $.ajax({
        url: WebsiteURL + '/Utility/General/CheckSubmit',
        data: { headerid: headerid, table: table, action: action, isapprover: isapprover },
        type: 'POST',
        async: false,
        success: function (data) {
            if (data.Key) {
                validate = data.Key;
            }
            else {
                validate = data.Key;
                DisplayNotiMessage("error", data.Value, "Error");
            }
        }
    });
    return validate
}
function checkItemExistInArray(target, array) {
    for (var i = 0; i < array.length; i++) {
        if (array[i] === target) {
            return true;
        }
    }
    return false;
}


/*Start : Functions For Utility Grid*/

function SaveUtilityGridData(formdata, URL, table, UtilityParams) {

    $.ajax({
        url: WebsiteURL + URL,
        type: "POST",
        data: formdata,
        async: false,
        contentType: false,
        processData: false,
        success: function (res) {
            if (res.Key) {
                if (UtilityParams != "" && UtilityParams != null && UtilityParams != undefined) {
                    UtilityParams.dtReload();
                }
                else {
                    $('#' + table).DataTable().ajax.reload();
                }
                DisplayNotiMessage("success", res.Value, "Success");
                if (table != "" && table != null && table != undefined) {
                    if ($('#btnNew_' + table).html() == '<i class="fa fa-times"></i> Cancel')
                        $('#btnNew_' + table).click();
                }
            }
            else {
                DisplayNotiMessage("error", res.Value, "Error");
                if (table != "" && table != null && table != undefined) {
                    $('#' + table).DataTable().columns.adjust();
                }
            }
        },
        error: function (data) { }
    });
}

function DeleteUtilityGridData(Id, URL, UtilityParams, table) {
    bootbox.dialog({
        message: "Do you want to delete?",
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success',
                callback: function () {
                    $.ajax({
                        url: WebsiteURL + URL,
                        type: "POST",
                        data: { Id: Id },
                        beforeSend: function () {
                            showLoading(true);
                        },
                        complete: function () {
                            showLoading(false);
                        },
                        success: function (data) {

                            if (data.Key == true) {
                                DisplayNotiMessage("success", data.Value, "Success");
                            }
                            else {
                                DisplayNotiMessage("error", data.Value, "Error");
                            }
                            if (UtilityParams != "" && UtilityParams != null && UtilityParams != undefined) {
                                UtilityParams.dtReload();
                            }
                            else {
                                if (table != "" && table != null && table != undefined) {
                                    $('#' + table).DataTable().ajax.reload();
                                }
                            }
                        },
                        error: function (data) {
                            DisplayNotiMessage("error", data.Value, "Error");
                        }
                    });
                }
            },
            cancel: {
                label: 'No',
                className: 'btn-danger',
                callback: function () { }
            },
        }

    });
}

function OpenURL(url) {
    showLoading(true);
    window.location = url;
}
/*End : Functions For Utility Grid*/


function CopyWQRCertificate(headerId, srcwqtno, certificatetype, location) {
    $.ajax({
        url: WebsiteURL + "/WQ/WPQCertificate/CopyCertificate",
        type: "POST",
        data: { wqtno: srcwqtno, location: location },
        dataType: "html",
        async: false,
        success: function (data) {
            bootbox.dialog({
                title: "Copy Certificate",
                message: data,
                size: "medium",
                buttons: {
                    Copy: {
                        Label: "Copy",
                        className: 'btn green',
                        callback: function () {
                            if ($("#frmCopy").valid()) {
                                var destwqtno = $("#txtToWQT").val();
                                $.ajax({
                                    url: WebsiteURL + "/WQ/WPQCertificate/CopyToDestination",
                                    type: "POST",
                                    data: { headerId: headerId, srcwqtno: srcwqtno, destwqtno: destwqtno, type: certificatetype },
                                    dataType: "json",
                                    success: function (data) {

                                        if (data.Key) {
                                            DisplayNotiMessage("success", data.Value, "Success");
                                            var url = "";
                                            if (certificatetype == "wpq") {
                                                url = "/WQ/WPQCertificate/certificate";
                                            }
                                            if (certificatetype == "wopq") {
                                                url = "/WQ/WPQCertificate/WOPQCertificate";
                                            }
                                            if (certificatetype == "tss") {
                                                url = "/WQ/TSSCertificate/TSSCertificate";
                                            }
                                            window.open(WebsiteURL + url + "?wqtno=" + data.wqtno + "&l=" + location);
                                        }
                                        else {
                                            DisplayNotiMessage("error", data.Value, "Error");
                                        }
                                    }
                                });
                            }
                            else {
                                return false;
                            }
                        }
                    },
                    Cancel: {
                        label: 'Close',
                        className: 'btn btnclose',
                        callback: function () { }
                    }
                }
            })
        }
    });
}

//developed by Nikita Vibhandik
function LoadMultiSelectwithCheckbox(url, objControl, caption, param, isDestroy, selected) {
    if (isDestroy != false) {
        objControl.multiselect('destroy');
    }
    $.ajax({
        url: WebsiteURL + url,
        type: "POST",
        data: param,
        async: false,
        dataType: "json",
        success: function (data) {
            var ischeckselected = false;
            var option = "";

            if (selected != undefined && selected != null && selected != "") {
                var selectedoption = selected.split(",");
                ischeckselected = true;
            }
            $.each(data, function (index, object) {
                if (ischeckselected) {
                    if (selectedoption.indexOf(object.id) > -1) {
                        option += "<option value='" + object.id + "' selected>" + object.text + "</option>";
                    }
                    else {
                        option += "<option value='" + object.id + "' >" + object.text + "</option>"
                    }
                }
                else {
                    option += "<option value='" + object.id + "'>" + object.text + "</option>";
                }
            });
            objControl.html(option);
            objControl.multiselect({
                placeholder: caption,
                search: true,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                includeSelectAllOption: true,
            });

            if (data.length == 0) {
                objControl.multiselect("disable");
            }
        }
    })
};

function MultiselectCCss() {
    $(".multiselect").css("width", "100%");
    $(".multiselect").parent().closest('div').css("width", "100%")
    $(".multiselect").css("overflow", "hidden");
}
//
//Created By Anil Hadiyal
function CheckIsMobileDevice() {
    if (!navigator.userAgent.match(/Android/i)
        && !navigator.userAgent.match(/webOS/i)
        && !navigator.userAgent.match(/iPhone/i)
        && !navigator.userAgent.match(/iPad/i)
        && !navigator.userAgent.match(/iPod/i)
        && !navigator.userAgent.match(/BlackBerry/i)
        && !navigator.userAgent.match(/Windows Phone/i)) {
        return false;
    }
    else {
        return true;
    }
}

function ResetAddRowInline(table, $btnname) {
    table.find("tbody tr:first").hide();
    table.find("tbody tr:first").css("background-color", "#fbffb2");
    $btnname.html('<i class="fa fa-plus"></i> New');
    clearFirstRowInline(table.find("tbody tr:first"));
}

function clearFirstRowInline(objFirstLine) {
    $('input[type!="hidden"]', objFirstLine).val('');
}

function ShowNewInlineRecord(obj, $table, $btnname) {
    $table.find("tbody tr:first").toggle();

    if ($table.find("tbody tr:first").is(":visible")) {
        $(obj).html('<i class="fa fa-times"></i> Cancel');
    }
    else {
        $(obj).html('<i class="fa fa-plus"></i> New');
        clearFirstRowInline($table.find("tbody tr:first"));
    }
}

function LoadMultiSelectwithCheckboxusingArray(sourceList, objControl, caption) {

    var option = "";
    $.each(sourceList, function (index, object) {
        option += "<option value='" + object.id + "'>" + object.text + "</option>";
    });
    $(objControl).html(option);
    objControl.multiselect({
        placeholder: caption,
        search: true,
        enableFiltering: true,
        enableCaseInsensitiveFiltering: true,
        includeSelectAllOption: true
    });
}

function LoadSelect2usingArray(sourceList, objControl, caption, param) {
    var option = "";
    $.each(sourceList, function (index, object) {
        option += "<option value='" + object.id + "'>" + object.text + "</option>";
    });

    $(objControl).html(option);

    objControl.select2({
        placeholder: caption
    });

};

var uri = 'data:application/vnd.ms-excel;base64,'
    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
    , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
    , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
function Export(table, name) {
    var ctx = { worksheet: name || 'Worksheet', table: table }
    window.location.href = uri + base64(format(template, ctx))
}


function CreateSwitch(element, hdElement) {
    hdElement.val(element.bootstrapSwitch('state'));
    element.on('switchChange.bootstrapSwitch', function (event, state) {
        var x = $(this).data('on-text');
        var y = $(this).data('off-text');
        if (element.is(':checked')) {
            hdElement.val(true);
        } else {
            hdElement.val(false);
        }
    });
}

function OpenNDERequestDetails(_ReqId) {
    var url = "/NDE/NDE_REQUEST_DETAILS";
    var data = [];

    data.push({ Param: "RequestId", Value: _ReqId });

    ShowReport(data, url);

}

function AddNextRowWMI($this, focuscolumn, focuswithintable, gridId, hdnRowID, tblId, $txtfocusColumn, paramList, aftercallbackfn, isnew, beforecallbackfn) {
    //debugger
    var prevRow = parseInt($("#" + hdnRowID).val());
    var nextRow = parseInt($("#" + hdnRowID).val()) + 1;

    var flag = true;
    if (isnew != true && ($this != null && $($this).val() != "" && $this != undefined) && (beforecallbackfn != null && beforecallbackfn != undefined)) {
        flag = beforecallbackfn($this); /* before adding new row, execute other call back function*/
    }
    if (flag) {
        if (GetParamCondition(paramList, gridId, prevRow) || isnew == true) {
            $("#Delete_" + gridId + "_" + prevRow).show();
            if (aftercallbackfn != null && aftercallbackfn != undefined) {
                $("#" + tblId).find('select,input').removeClass('islastrow');
                $("#" + tblId + " tr").removeClass('islastrow');
                $("#" + tblId).append(aftercallbackfn(nextRow));
            }
            //$("#" + $txtfocusColumn + "_" + gridId + "_" + nextRow).focus();
        }
    }
    else {
        if ($this != null && $this != undefined) {
            $($this).focus();
        }
        if (!focuswithintable && focuscolumn != null && isnew != true) {
            //$("#" + focuscolumn).focus();
        }
    }
    //debugger
    if (isnew != true && ($this != null && $this != undefined) && (focuscolumn != null && focuscolumn != undefined)) {
        var row_index = $($this).parent().parent().index();
        if ($($this).val() == "") {
            if (focuswithintable) {
                var cell = focuscolumn.split(',');
                if ($("#" + tblId + " tbody>tr:eq(" + (row_index + 1) + ")").find('td.' + cell[0]).has("input[type=text]").length == 0) {
                    if ($("#" + tblId + " tbody>tr:eq(" + (row_index + 2) + ")").find('td.' + cell[0]).find("input").length > 0) {
                        //$("#" + tblId + " tbody>tr:eq(" + (row_index + 2) + ")").find('td.' + cell[0]).find("input").focus();
                    }
                    else {
                        //$("#" + cell[1]).focus();
                    }
                }
                else {
                    //$("#" + tblId + " tbody>tr:eq(" + (row_index + 1) + ")").find('td.' + cell[0]).find("input").focus();
                }
            }
            else {
                //$("#" + focuscolumn).focus()
            }
        }
    }
    $('input[maxlength]').maxlength({ alwaysShow: true }); //[10/09/2020 Added by Jagruti - Jayamsoft - Only for WMI Module]
}

function GetParamCondition(list, gridid, rowid, tblId, $this, $appendlocation) {
    var q = true;
    if (($this != null && $this != "" && $this != undefined) && ($appendlocation != "" && $appendlocation != undefined)) {
        $row = $("#" + tblId).find('tr.' + $appendlocation.split("__")[0]).last();
        $cells = $row.find("td input");
        $cells.each(function () {
            flg = $(this).val();
            if (q == true && (flg == "" || flg == null || flg == undefined || flg.legth == 0)) {
                q = false;
                return false;
            }
        });
    }
    else {
        for (var j = 0; j < list.length; j++) {
            var flg = "";
            flg = $("#" + list[j] + "_" + gridid + "_" + rowid).val();
            if (flg == "" || flg == null || flg == undefined || flg.legth == 0) {
                q = false;
                break;
            }
        }
    }
    return q;
}

function OpenQAClearanceReports(reporttype, pLocation, pFromQualityProject, pToQualityProject, pFromSeamNo, pToSeamNo, DateFrom, DateTo, pFromStage, pToStage, pFromStageSeq, pToStageSeq, pWeldingProcess, pSeamApprovalStatus, pInstrument, pPrintBy, pInspectionRemark, pPrintTPI1, pPrintTPI2, pPrintTPI3) {

    var dat = [];
    var url = "";

    if (reporttype == 'Chemical') {
        url = "/IPI/QC/CHEMICAL_ANALYSIS_REPORT";
        dat.push({ Param: "Location", Value: pLocation });
        dat.push({ Param: "QualityProject_f", Value: pFromQualityProject });
        dat.push({ Param: "QualityProject_t", Value: pToQualityProject });
        dat.push({ Param: "SeamNo_f", Value: pFromSeamNo });
        dat.push({ Param: "SeamNo_t", Value: pToSeamNo });
        dat.push({ Param: "InspectionDate_f", Value: DateFrom });
        dat.push({ Param: "InspectionDate_t", Value: DateTo });
        dat.push({ Param: "StageCode_f", Value: pFromStage });
        dat.push({ Param: "StageCode_t", Value: pToStage });
        dat.push({ Param: "Seqn_f", Value: pFromStageSeq });
        dat.push({ Param: "Seqn_t", Value: pToStageSeq });
        dat.push({ Param: "WeldingProcess", Value: pWeldingProcess });
        dat.push({ Param: "SeamResult", Value: pSeamApprovalStatus });
        dat.push({ Param: "TestInstrument", Value: pInstrument });
        dat.push({ Param: "PrintBy", Value: pPrintBy });
        dat.push({ Param: "InspectionRemark", Value: pInspectionRemark });
        dat.push({ Param: "PrintTPI1", Value: pPrintTPI1 });
        dat.push({ Param: "PrintTPI2", Value: pPrintTPI2 });
        dat.push({ Param: "PrintTPI3", Value: pPrintTPI3 });
    }
    else if (reporttype == 'Ferrite') {
        url = "/IPI/QC/FERRITE_REPORT";
        dat.push({ Param: "Location", Value: pLocation });
        dat.push({ Param: "QualityProject_f", Value: pFromQualityProject });
        dat.push({ Param: "QualityProject_t", Value: pToQualityProject });
        dat.push({ Param: "SeamNo_f", Value: pFromSeamNo });
        dat.push({ Param: "SeamNo_t", Value: pToSeamNo });
        dat.push({ Param: "InspectionDate_f", Value: DateFrom });
        dat.push({ Param: "InspectionDate_t", Value: DateTo });
        dat.push({ Param: "StageCode_f", Value: pFromStage });
        dat.push({ Param: "StageCode_t", Value: pToStage });
        dat.push({ Param: "Seqn_f", Value: pFromStageSeq });
        dat.push({ Param: "Seqn_t", Value: pToStageSeq });
        dat.push({ Param: "WeldingProcess", Value: pWeldingProcess });
        dat.push({ Param: "SeamResult", Value: pSeamApprovalStatus });
        dat.push({ Param: "TestInstrument", Value: pInstrument });
        dat.push({ Param: "PrintBy", Value: pPrintBy });
        dat.push({ Param: "InspectionRemark", Value: pInspectionRemark });
        dat.push({ Param: "PrintTPI1", Value: pPrintTPI1 });
        dat.push({ Param: "PrintTPI2", Value: pPrintTPI2 });
        dat.push({ Param: "PrintTPI3", Value: pPrintTPI3 });
    }
    else if (reporttype == 'Hardness') {
        url = "/IPI/QC/HARDNESS_REPORT";
        dat.push({ Param: "Location", Value: pLocation });
        dat.push({ Param: "QualityProject_f", Value: pFromQualityProject });
        dat.push({ Param: "QualityProject_t", Value: pToQualityProject });
        dat.push({ Param: "SeamNo_f", Value: pFromSeamNo });
        dat.push({ Param: "SeamNo_t", Value: pToSeamNo });
        dat.push({ Param: "InspectionDate_f", Value: DateFrom });
        dat.push({ Param: "InspectionDate_t", Value: DateTo });
        dat.push({ Param: "StageCode_f", Value: pFromStage });
        dat.push({ Param: "StageCode_t", Value: pToStage });
        dat.push({ Param: "Seqn_f", Value: pFromStageSeq });
        dat.push({ Param: "Seqn_t", Value: pToStageSeq });
        dat.push({ Param: "SeamResult", Value: pSeamApprovalStatus });
        dat.push({ Param: "TestInstrument", Value: pInstrument });
        dat.push({ Param: "PrintBy", Value: pPrintBy });
        dat.push({ Param: "InspectionRemark", Value: pInspectionRemark });
        dat.push({ Param: "PrintTPI1", Value: pPrintTPI1 });
        dat.push({ Param: "PrintTPI2", Value: pPrintTPI2 });
        dat.push({ Param: "PrintTPI3", Value: pPrintTPI3 });
    }
    else if (reporttype == 'PMI') {
        url = "/IPI/QC/PMI_TEST_REPORT";
        dat.push({ Param: "Location", Value: pLocation });
        dat.push({ Param: "QualityProject_f", Value: pFromQualityProject });
        dat.push({ Param: "QualityProject_t", Value: pToQualityProject });
        dat.push({ Param: "SeamNo_f", Value: pFromSeamNo });
        dat.push({ Param: "SeamNo_t", Value: pToSeamNo });
        dat.push({ Param: "InspectionDate_f", Value: DateFrom });
        dat.push({ Param: "InspectionDate_t", Value: DateTo });
        dat.push({ Param: "StageCode_f", Value: pFromStage });
        dat.push({ Param: "StageCode_t", Value: pToStage });
        dat.push({ Param: "Seqn_f", Value: pFromStageSeq });
        dat.push({ Param: "Seqn_t", Value: pToStageSeq });
        dat.push({ Param: "SeamResult", Value: pSeamApprovalStatus });
        dat.push({ Param: "TestInstrument", Value: pInstrument });
        dat.push({ Param: "PrintBy", Value: pPrintBy });
        dat.push({ Param: "InspectionRemark", Value: pInspectionRemark });
        dat.push({ Param: "PrintTPI1", Value: pPrintTPI1 });
        dat.push({ Param: "PrintTPI2", Value: pPrintTPI2 });
        dat.push({ Param: "PrintTPI3", Value: pPrintTPI3 });
    }
    ShowReport(dat, url);

}

function OpenProtocolReports(pProtocolType, pProtocolId, pHeaderId, pIterationNo, pRequestNoSequence) {

    var dat = [];
    var url = "";
    if (pProtocolType == 'OtherFiles') {
        url = "QMS050/" + pHeaderId + "/" + pIterationNo + "/" + pRequestNoSequence;
        OpenAttachmentPopUp(0, false, url);
    }
    else {
        url = "/IPI/Protocol/" + pProtocolType;
        dat.push({ Param: "HeaderId", Value: pProtocolId });
        ShowReport(dat, url);
    }

}

function Load_File_Upload_Partial_View(objFile, DivId, LoadInPopup) {
    if (objFile.ControlID == undefined || objFile.ControlID == null || objFile.ControlID == "") {
        DisplayNotiMessage("Error", "ControlId not define", "File Upload");
        return;
    }

    if (objFile.Path == undefined || objFile.Path == null || objFile.Path == "") {
        DisplayNotiMessage("Error", "Path is not define", "File Upload");
        return;
    }

    if (objFile.TableName == undefined || objFile.TableName == null || objFile.TableName == "") {
        DisplayNotiMessage("Error", "TableName is not define", "File Upload");
        return;
    }

    //if (objFile.TableId == undefined || objFile.TableId == null || objFile.TableId == "") {
    //    DisplayNotiMessage("Error", "TableId is not define", "File Upload");
    //    return;
    //}

    if (objFile.SuccessCallbackFunction === undefined || objFile.SuccessCallbackFunction === null) {
        objFile.SuccessCallbackFunction = "";
    }

    if (LoadInPopup === undefined || LoadInPopup === null || LoadInPopup === "")
        LoadInPopup = false;

    $.ajax({
        type: 'GET',
        url: WebsiteURL + '/Utility/FileUpload/LoadpartialViewDocumentUpload',
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        data: objFile,
        beforeSend: function () {
            showLoading(true);
        },
        complete: function () {
            showLoading(false);
        },
        success: function (data) {
            if (LoadInPopup) {
                bootbox.dialog({
                    title: objFile.PopupTitle,
                    message: data,
                    size: "large",
                    buttons: {
                        Cancel: {
                            label: 'Close',
                            className: 'btn red',
                            callback: function () { }
                        }
                    }
                });
            }
            else {
                var vControlName = '#' + DivId;
                $(vControlName).html(data);
                //$('#div_drop_zone_1').html(data);
            }

            //if (objFile.LoadCallbackFunction != null && objFile.LoadCallbackFunction != undefined && typeof objFile.LoadCallbackFunction === 'function') {
            //    objFile.LoadCallbackFunction();
            //}
        },
        error: function (xs, status) {
            console.log(status);
            showLoading(false);
        }
    });
}

//FCS START
function fn_view_document_UV(Path) {
    if (Path != '' && Path != null && Path != undefined) {
        Path = '\\' + Path;
        var randomnumber = Math.floor((Math.random() * 100) + 1);
        popupWindowUV("https://phzpdiemqsweb.lthed.com:100/Home/Index?DocPath=" + Path, "UniverSalWindow", window, screen.availWidth, screen.availHeight, randomnumber);
    }
    else {
        DisplayNotiMessage("error", "No Attachment available", "Error");
    }
}

function popupWindowUV(url, title, win, w, h, no) {
    const y = win.top.outerHeight / 2 + win.top.screenY - (h / 2);
    const x = win.top.outerWidth / 2 + win.top.screenX - (w / 2);
    return win.open(url, '_blank', title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + y + ', left=' + x);
}

function fn_download_document(filepath, CurrentLocationIp) {
    if (filepath != undefined && filepath != '' && filepath != null) {
        var FCSurl = CurrentLocationIp + "/Download";
        var DocPathList = [];
        DocPathList.push('\\' + filepath);
        popupWindow(FCSurl + "?DocList=" + DocPathList, "DownloadWindow", window, 500, 500);
    }
    else {
        DisplayNotiMessage("error", "File Not Found!!", "Error");
    }
}

function popupWindow(url, title, win, w, h) {
    const y = win.top.outerHeight / 2 + win.top.screenY - (h / 2);
    const x = win.top.outerWidth / 2 + win.top.screenX - (w / 2);
    return win.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + y + ', left=' + x);
}

//FCS END


//PPS

function PPSFillAutoComplete(element, sourceList, hdElement) {
    element.autocomplete({
        source: function (request, response) {
            var array = $.map(sourceList, function (item) {
                return {
                    label: item.CatDesc,
                    value: item.CatDesc,
                    //id: item.CatID
                    id: item.CatId
                }
            });
            response($.ui.autocomplete.filter(array, request.term));
        },
        autoFocus: true,
        minLength: 0,
        select: function (event, ui) {
            $(this).val(ui.item.id);
            $(hdElement).val(ui.item.id);
            HighlightInValidControls();
        },
        change: function (event, ui) {
            if (ui.item == null) {
                element.val('');
                element.focus();
                $(hdElement).val("");
                HighlightInValidControls();
            }
        }
    }).focus(function () {
        $(this).data("uiAutocomplete").search($(this).val());
        $(this).autocomplete("search");
    });
}


function Load_PPSFile_Upload_Partial_View(objFile, DivId, LoadInPopup) {
    if (objFile.ControlID == undefined || objFile.ControlID == null || objFile.ControlID == "") {
        DisplayNotiMessage("Error", "ControlId not define", "File Upload");
        return;
    }

    if (objFile.Path == undefined || objFile.Path == null || objFile.Path == "") {
        DisplayNotiMessage("Error", "Path is not define", "File Upload");
        return;
    }

    if (objFile.TableName == undefined || objFile.TableName == null || objFile.TableName == "") {
        DisplayNotiMessage("Error", "TableName is not define", "File Upload");
        return;
    }

    //if (objFile.TableId == undefined || objFile.TableId == null || objFile.TableId == "") {
    //    DisplayNotiMessage("Error", "TableId is not define", "File Upload");
    //    return;
    //}

    if (objFile.SuccessCallbackFunction === undefined || objFile.SuccessCallbackFunction === null) {
        objFile.SuccessCallbackFunction = "";
    }

    if (LoadInPopup === undefined || LoadInPopup === null || LoadInPopup === "")
        LoadInPopup = false;

    $.ajax({
        type: 'GET',
        url: WebsiteURL + '/PPS/PPSFileUpload/LoadpartialViewDocumentUpload',
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        data: objFile,
        beforeSend: function () {
            showLoading(true);
        },
        complete: function () {
            showLoading(false);
        },
        success: function (data) {
            if (LoadInPopup) {
                bootbox.dialog({
                    title: objFile.PopupTitle,
                    message: data,
                    size: "large",
                    buttons: {
                        Cancel: {
                            label: 'Close',
                            className: 'btn red',
                            callback: function () { }
                        }
                    }
                });
            }
            else {
                var vControlName = '#' + DivId;
                $(vControlName).html(data);
                //$('#div_drop_zone_1').html(data);
            }

            //if (objFile.LoadCallbackFunction != null && objFile.LoadCallbackFunction != undefined && typeof objFile.LoadCallbackFunction === 'function') {
            //    objFile.LoadCallbackFunction();
            //}
        },
        error: function (xs, status) {
            console.log(status);
            showLoading(false);
        }
    });
}

//PPS END