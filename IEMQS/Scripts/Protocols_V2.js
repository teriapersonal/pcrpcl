﻿var parameterList = [];
var userAssignrole;

function FillProtocolAutoComplete(element, sourceList, hdElement, isEditable, callbackfn, extraPara) {
    element.autocomplete({
        source: function (request, response) {
            var array = $.map(sourceList, function (item) {
                return {
                    label: item.CategoryDescription,
                    value: item.CategoryDescription,
                    id: item.Code
                }
            });
            response($.ui.autocomplete.filter(array, request.term));
        },
        autoFocus: true,
        async: false,
        minLength: 0,
        select: function (event, ui) {
            $(element).val(ui.item.value);
        },
        change: function (event, ui) {
            if (ui.item == null) {
                $(this).val('');
                $(this).focus();
            }
            else {
                if ($(element).val() != ui.item.value) {
                    $(this).val('');
                    $(this).focus();
                }
            }
            if (callbackfn != null && callbackfn != undefined) {
                callbackfn($(this).val(), extraPara[0]);
            }

        }
    }).focus(function () {
        $(this).data("uiAutocomplete").search($(this).val());
        $(this).autocomplete("search");
    });
}

function showCladtripTable(selectedVal, dvID, btnAddPhotoUpload) {
    if (selectedVal == "Yes") {
        $("#" + dvID).show();
        $("#" + btnAddPhotoUpload).show();
    }
    else {
        $("#" + dvID).hide();
        $("#" + btnAddPhotoUpload).hide();
    }
}
function LoadProtocolLineTables(tableid, newbtnid, Columns, AjaxSource, parameterArray, extrafn) {
    var table = $('#' + tableid);
    var oTable = table.dataTable({
        "language": {
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "emptyTable": "No data available in table",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "infoEmpty": "No entries found",
            "infoFiltered": "(filtered1 from _MAX_ total entries)",
            "lengthMenu": "_MENU_ entries",
            "search": "Search:",
            "zeroRecords": "No matching records found"
        },
        "bProcessing": false,
        "bServerSide": true,
        "stateSave": false,
        buttons: [],
        "bDestroy": true,
        responsive: true,
        "order": [
            [0, 'asc']
        ],
        "lengthMenu": [
            [5, 10, 25, 50, 100],
            [5, 10, 25, 50, 100]
        ],
        "pageLength": 10,
        'iDisplayLength': 10,
        "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
        "sAjaxSource": WebsiteURL + AjaxSource + GetQueryStringParameterList(parameterArray),
        "fnServerParams": function (aoData) {
            aoData.push({ "name": "type", "value": "sdf" }
            );
        },
        "fnServerData": function (sSource, aoData, fnCallback) {
            $.ajax({
                'dataType': 'json',
                'type': 'POST',
                'url': sSource,
                'data': aoData,
                'success': function (data) {
                    fnCallback(data);

                    if (data.outby != null && data.outby != undefined && data.outby != "undefined") {
                        SetOutByRow(tableid);
                    }

                    var parentdvid = $('#' + tableid).parents('div.inlineparentdiv').attr('id');

                    InlineGridProtocolEdit.init($('#' + newbtnid), $('#' + tableid), $('#' + parentdvid));
                },
                'async': false
            });
            $('input[maxlength]').maxlength({
                alwaysShow: true,
            });
            ResetAddRowLine(tableid, newbtnid);
            SetControlNumeric();
            if (extrafn && (typeof extrafn == "function")) {
                extrafn();
            }
        },
        createdRow: function (row, data, dataIndex) {
            $(row).attr("data-id", data[2]);
            $(row).attr("data-hid", data[1]);
            $(row).attr("data-gid", data[0]);
        },
        "aoColumns": Columns
    });

}

function SetOutByRow(tableid) {
    $("#" + tableid + " tr:last").each(function () {
        jQuery.each($('td', this), function () {
            $(this).addClass("datatableFooter");
            $(this).attr("bSortable", false);
        });
    });
}

function SetControlNumeric() {
    $(".numeric-only").autoNumeric('init', { aSep: '', vMax: "9999999.99" });
    $('.number-only').autoNumeric('init', { vMin: '0', vMax: '9999999999', aSep: '' });
    $('input[maxlength],textarea[maxlength]').maxlength({
        alwaysShow: true,
    });
}

function GetQueryStringParameterList(list) {
    var q = "";
    for (var i = 0; i < parameterList.length; i++) {
        for (var j = 0; j < list.length; j++) {
            if (list[j] == parameterList[i]["id"]) {
                if (q.length == 0) {
                    q = "?" + parameterList[i]["id"] + "=" + parameterList[i]["value"];
                }
                else {
                    q = q + "&" + parameterList[i]["id"] + "='" + parameterList[i]["value"] + "'";
                }
            }
        }
    }
    //console.log(q);
    return q;
}

function DisbaleControls(UserRole, isDisbale) {
    if ((UserRole.toUpperCase() == 'PROD3')) {
        $("[role=QC]").prop("disabled", isDisbale);
    }
    else {
        $("[role=PROD]").prop("disabled", isDisbale);
    }
}

function ResetAddRowLine(table, btnNew) {
    $("#" + table).find("tbody tr:first").hide();
    $("#" + table).find("tbody tr:first").css("background-color", "#fbffb2");
    $("#" + btnNew).html('<i class="fa fa-plus"></i> New');
    clearFirstRowLine($("#" + table).find("tbody tr:first"));
}

function clearFirstRowLine(objFirstLine) {
    $('input[type!="hidden"]', objFirstLine).val('');
}

function ShowNewLineRecord(obj, table) {
    $('#' + table).find("tbody tr:first").toggle();
    $('#' + table).find("tbody tr:first select:first").focus();
    if ($('#' + table).find("tbody tr:first").is(":visible")) {
        $(obj).html('<i class="fa fa-times"></i> Cancel');

        var $tr = $('#' + table).find("tbody tr:first");
        $tr.css('background-color', '#ffffff');
        $tr.find("input,select,textarea").each(function () {
            if ((userAssignrole.toUpperCase() == 'PROD3')) {
                if ($(this).attr("role").toUpperCase() == "PROD") {
                    $(this).removeAttr("readonly");
                    $(this).removeAttr("disabled");
                    $(this).addClass("PRODTextbox");
                    $(this).addClass("EditableTextColor");
                }
                else {
                    $(this).addClass("QCTextbox");
                    $(this).prop('readonly', true);
                    $(this).addClass("NonEditableTextColor");
                }

            }
            else {
                if ($(this).attr("role").toUpperCase() == "QC") {
                    $(this).removeAttr("readonly");
                    $(this).removeAttr("disabled");
                    $(this).addClass("EditableTextColor");
                    $(this).addClass("QCTextbox");
                }
                else {
                    $(this).addClass("PRODTextbox");
                    $(this).prop('readonly', true);
                    $(this).addClass("NonEditableTextColor");
                }
            }
        });
    }
    else {
        $(obj).html('<i class="fa fa-plus"></i> New');
        clearFirstRowLine($('#' + table).find("tbody tr:first"));
    }
}

function DeleteProtocolLineWithConfirmation(rowID, headerId, Action, table) {
    bootbox.dialog({
        title: "Delete Confirmation",
        message: "Do you want to delete?",
        size: "medium",
        buttons: {
            Yes: {
                Label: "Yes",
                className: 'btn green',
                callback: function () {
                    DeleteProtocolLine(rowID, headerId, Action, table);
                }
            },
            Cancel: {
                label: 'No',
                className: 'btn red',
                callback: function () { }
            }
        }
    });
}

function DeleteProtocolLine(rowID, headerId, Action, table) {
    $.ajax({
        type: "POST",
        url: WebsiteURL + Action,
        datatype: 'json',
        async: false,
        data: { lineId: rowID, headerId: headerId },
        success: function (result) {
            if (result.Key) {
                DisplayNotiMessage("success", result.Value, "Success");
                $('#' + table).DataTable().ajax.reload();
            }
        },
        error: function () {
            alert('error...');
        }
    });
}

function DeleteProtocolHeaderData(HeaderId, URL) {
    var formdata = new FormData();
    formdata.append("MainHeaderId", HeaderId);

    $.ajax({
        url: WebsiteURL + URL,
        type: "POST",
        data: formdata,
        async: false,
        contentType: false,
        processData: false,
        success: function (res) {

        },
        error: function (data) { }
    });
}

function SaveProtocolLineData(formdata, URL, table, btnNew) {
    var tabledata = $("#" + table).find("input,select,textarea,hidden").serializeArray();
    if (tabledata != null && tabledata != undefined) {
        $.each(tabledata, function (key, input) {
            formdata.append(input.name, input.value);
        });
    }

    //$("#" + table + " tbody>tr:first").find('input,select,textarea').each(function () {
    //    //console.log($(this).attr("name"));
    //    if ($(this).attr("type") == "text" || $(this).attr("type") == "select" || $(this).attr("type") == "textarea") {
    //        formdata.append($(this).attr("name"), $(this).val())
    //    }
    //    else if ((type == "checkbox" || type == "radio")) {
    //        formdata.append($(this).attr("name"), $(this).is(":checked"))
    //    }
    //})

    $.ajax({
        url: WebsiteURL + URL,
        type: "POST",
        data: formdata,
        async: false,
        contentType: false,
        processData: false,
        success: function (res) {
            if (res.Key && res.HeaderId > 0) {
                DisplayNotiMessage("success", res.Value, "Success");
                $('#' + table).DataTable().ajax.reload();
                ShowNewLineRecord($('#' + btnNew));
            }
            else
                DisplayNotiMessage("error", res.Value, "Error");
        },
        error: function (data) { }
    });
}

function SaveProtocolHeaderData(frmProtocolHeader, HeaderId, URL, userRole, isSubmit, aftercallbackfn) {
    if ($("#" + frmProtocolHeader).valid()) {
        //debugger
        DisbaleControls(userRole, false);
        var frmHeaderValues = $("#" + frmProtocolHeader).serializeArray();
        DisbaleControls(userRole, true);

        var formdata = new FormData();
        if (frmHeaderValues != null && frmHeaderValues != undefined) {
            $.each(frmHeaderValues, function (key, input) {
                formdata.append(input.name, input.value);
            });
            formdata.append("MainHeaderId", HeaderId);
            $.ajax({
                url: WebsiteURL + URL,
                type: "POST",
                data: formdata,
                contentType: false,
                processData: false,
                beforeSend: function () {
                    showLoading(true);
                },
                complete: function () {
                    showLoading(false);
                },
                success: function (res) {
                    if (res.Key && res.HeaderId > 0) {
                        $('#HeaderId').val(res.HeaderId);
                        if (isSubmit) {
                            DisplayNotiMessage("success", res.Value, "Success");
                        }
                        else {
                            DisplayNotiMessage("success", "Protocol saved successfully", "Success");
                        }
                        if (aftercallbackfn != null && aftercallbackfn != undefined) {
                            aftercallbackfn();
                        }
                    }
                    else
                        DisplayNotiMessage("error", res.Value, "Error");
                },
                error: function (data) { }
            });
        }
    }
    else {
        DisplayNotiMessage("info", "All mandatory (*) fields are required", "Info");
    }
}

function SetAccessControlForRole(UserRole) {
    $(".menu-toggler").trigger('click');
    userAssignrole = UserRole;
    if ((UserRole.toUpperCase() == 'PROD3')) {
        $("[role=QC]").prop("disabled", true);
        $("[role=QC]").addClass("NonEditableTextColor");
        $("[role=PROD]").addClass("EditableTextColor");
        if ($('#btncal1').closest('div.date-picker').find("[role=QC]").length > 0) {
            $('#btncal1').prop("disabled", true)
            $('.calender1').prop("disabled", true)

        }
        $("select[role='QC']").attr("style", "pointer-events: none;");
    }
    else if ((UserRole.toUpperCase() == 'QC3')) {
        $("[role=PROD]").prop("disabled", true);
        $("[role=QC]").addClass("EditableTextColor");
        $("[role=PROD]").addClass("NonEditableTextColor");
        if ($('#btncal1').closest('div.date-picker').find("[role=PROD]").length > 0) {
            $('#btncal1').prop("disabled", true); $('.calender1').prop("disabled", true)
        }
        $("select[role='PROD']").attr("style", "pointer-events: none;");
    }
    else {
        $("[role=PROD]").prop("disabled", true);
        $("[role=QC]").prop("disabled", true);
        $("[role=QC]").addClass("EditableTextColor");
        $("[role=PROD]").addClass("EditableTextColor");
        if ($('#btncal1').closest('div.date-picker').find("[role=QC]").length > 0) {
            $('#btncal1').prop("disabled", true); $('.calender1').prop("disabled", true);
        }
        if ($('#btncal1').closest('div.date-picker').find("[role=PROD]").length > 0) {
            $('#btncal1').prop("disabled", true); $('.calender1').prop("disabled", true)
        }
    }

    $("[role=QC]").addClass("QCTextbox");
    $("[role=PROD]").addClass("PRODTextbox");
}

function totalOvality(txt) {

    var variable = 0;
    var maxval;
    var minval;
    var Ovality
    var data = [];
    $(".calc").each(function () {
        if ($(this).val() != '') {
            variable = parseFloat($(this).val()).toFixed(2);
            data.push(variable);
        }
    });
    if (data.length > 0) {
        data = $.map(data, function (o) { return o; });
        maxval = Math.max.apply(this, data);
        minval = Math.min.apply(this, data);
        Ovality = maxval - minval;
        $("#" + txt).val(Ovality);
    }
    else {
        $("#" + txt).val("");
    }
}
function totalOvalityCustom(clsSelector, resid) {

    var variable = 0;
    var maxval;
    var minval;
    var Ovality
    var data = [];
    $("." + clsSelector).each(function () {
        if ($(this).val() != '') {
            variable = parseFloat($(this).val()).toFixed(2);
            data.push(variable);
        }
    });
    if (data.length > 0) {
        data = $.map(data, function (o) { return o; });
        maxval = Math.max.apply(this, data);
        minval = Math.min.apply(this, data);
        Ovality = maxval - minval;
        $("#" + resid).val(Ovality);
    }
    else {
        $("#" + resid).val("");
    }
}
function calculateMaxMin(cls, txt) {
    var variable = 0;
    var maxval;
    var minval;
    var Ovality
    var data = [];
    $("." + cls).each(function () {
        if ($(this).val() != '') {
            variable = parseFloat($(this).val()).toFixed(2);
            data.push(variable);
        }
    });
    if (data.length > 0) {
        data = $.map(data, function (o) { return o; });
        maxval = Math.max.apply(this, data);
        minval = Math.min.apply(this, data);
        Ovality = maxval - minval;
        $("#" + txt).val(Ovality);
    }
    else {
        $("#" + txt).val("");
    }
}

function calculateRowWiseMaxMin(txt, rowid) {
    var variable = 0;
    var maxval;
    var minval;
    var Ovality
    var data = [];

    $("." + rowid).each(function () {
        if ($(this).val() != '') {
            variable = parseFloat($(this).val()).toFixed(2);
            data.push(variable);
        }
    });
    if (data.length > 0) {
        data = $.map(data, function (o) { return o; });
        maxval = Math.max.apply(this, data);
        minval = Math.min.apply(this, data);
        Ovality = maxval - minval;
        $("#" + txt + rowid).val(Ovality);
    }
    else {
        $("#" + txt + rowid).val("");
    }

}
function calculatetblRowWiseMaxMin(tablename, txt) {
    $('#' + tablename + ' tr').each(function () {
        rowid = $(this).attr("id");
        calculateRowWiseMaxMin(txt, rowid)
    });
}


function calculatetableWiseMaxMin(classname, txt, lastindex) {
    for (var j = 1; j <= parseInt(lastindex); j++) {
        calculateMaxMin(classname + j, txt + j);
    }
}


function SetDynamicRowspan(classname, txt, lastindex) {
    for (var j = 1; j <= parseInt(lastindex); j++) {
        var count = $('.' + classname + j).length;
        $("#" + txt + j).remove("rowspan").attr("rowspan", count);
    }
}

function SetEditable(isEdit) {
    if (isEdit.toLowerCase() == "true") { }
    else {
        $('div.dvDisable :input')
            .not($('.dataTables_length').find('input,select,search,textarea'))
            .not($('.dataTables_filter').find('input,select,search,textarea'))
            .prop('disabled', true);
        $('div.dvDisable a')
            .not($('.dataTables_paginate').find('input,select,search,textarea'))
            .not("#btnPrint")
            .hide();
    }
}
function SetEditableDetails(isEdit, isQCRemarkEditable) {
    if (isEdit.toLowerCase() == "true") { }
    else {
        if (isQCRemarkEditable == "true") {
            $('form.form-horizontal :input')
                .not("#QCRemarks")
                .not(".clsQCAttend")
                .not("#btnSaveDraft,#btnSave")
                .not($('.dataTables_length').find('input,select,search,textarea'))
                .not($('.dataTables_filter').find('input,select,search,textarea'))
                .prop('readonly', true);
            $('form.form-horizontal a')
                .not("#QCRemarks")
                .not(".clsQCAttend")
                .not($('.dataTables_paginate').find('input,select,search,textarea'))
                .not("#btnPrint")
                .hide();
            $("select[role='QC']").attr("style", "pointer-events: none;");
        }
        else {
            $('form.form-horizontal :input')
                .not($('.dataTables_length').find('input,select,search,textarea'))
                .not($('.dataTables_filter').find('input,select,search,textarea'))
                .prop('disabled', true);
            $('form.form-horizontal a')
                .not($('.dataTables_paginate').find('input,select,search,textarea'))
                .not("#btnPrint")
                .hide();
        }
    }
}
function PrintReport(url, HeaderId) {
    var dat = [];
    dat.push({ Param: "HeaderId", Value: HeaderId });
    ShowReport(dat, url);
    return false;
}

//function SaveProtocolLine(formdata,URL, table, btnNew) {
//    //var formdata = new FormData();    
//    $("#" + table + " tbody>tr:first").find('input,select,textarea').each(function () {
//        //console.log($(this).attr("name"));
//        if ($(this).attr("type") == "text" || $(this).attr("type") == "select" || $(this).attr("type") == "textarea") {
//            formdata.append($(this).attr("name"), $(this).val())
//        }
//        else if ((type == "checkbox" || type == "radio")) {
//            formdata.append($(this).attr("name"), $(this).is(":checked"))
//        }
//    })
//    SaveProtocolLineData(formdata, URL, table, btnNew);
//}

var InlineGridProtocolEdit = function () {
    var edittbl;
    var btnAddNew;
    //var contentdiv;
    var editetr = {};
    function Initialize(btnid, tblid, dv) {
        edittbl = $(dv).find($(tblid));
        btnAddNew = $(dv).find($(btnid));

        //contentdiv = dv;   
        //console.log(edittbl);
        //console.log(btnAddNew);
        editetr = {};
        btnAddNew.unbind("click");
        btnAddNew.click(function () {
            //InsertNewRecordInline($(this), tbl);
        });
        EditLineEnable();
        SetNumericField();
        SetAddNewIntialStage();
    }
    function InsertNewRecordInline(obj, tbl) {
        $('input[maxlength]').maxlength({
            alwaysShow: true,
        });
        tbl.find("tbody tr:first").toggle();
        tbl.find("tbody tr:first select:first").focus();
        $(obj).html('<i class="fa fa-times"></i> Cancel');
        if (tbl.find("tbody tr:first").is(":visible")) {
            $(obj).val("Cancel");
            var delayedFn, blurredFrom;
            tbl.find("tbody tr:first").on('blur', 'a', function (event) {
                blurredFrom = event.delegateTarget;
                delayedFn = setTimeout(function () { SaveLineRecord(0); }, 0);
            });
            tbl.find("tbody tr:first").on('focus', 'a', function (event) {
                if (blurredFrom === event.delegateTarget) {
                    clearTimeout(delayedFn);
                }
            });

            if (editetr.id != null && editetr.id != undefined && editetr.id != "undefined") {
                var $tr = $("[data-id='" + editetr.id + "']");
                $tr.html(editetr.text);
            }
            EditLineEnable();
        }
        else {
            $(obj).val("New");
            $(obj).html('<i class="fa fa-plus"></i> New');
        }
    }

    function EditLineEnable() {
        //remove previous click event
        $(edittbl).find(".editline").unbind("click");

        //add new click event
        $(edittbl).find(".editline").click(function () {
            //If any rows already in edit mode then set as normal row
            edittbl.find('tbody tr').not("tbody tr:first").each(function () {
                $trold = $(this);
                //set readonly or disabled property to all element
                $trold.find('input,select,textarea').removeClass('PRODTextbox QCTextbox EditableTextColor NonEditableTextColor');
                $trold.find('input,select,textarea').attr("readonly", "readonly");

                //$tr.find('input,select,textarea').attr("disabled", "disabled");                
                //find any div edit with editmode. If any then hide update and cancel button and show edit button
                if ($trold.find('.editmode').length > 0) {
                    $trold.find('.editmode').hide();
                    $trold.find('.editline').show();
                }
            })

            var $tr = $(this).closest("tr");

            edittbl = $(this).closest('table');
            EditLine($(this), $tr.data("hid"), $tr.data("id"), $tr.data("gid"));
        });
    }

    function EditLine(btnEdit, headerId, lineId, gridId) {
        var $tr = $(btnEdit).closest("tr");
        editetr.text = $tr.html();
        editetr.id = $tr.data("id");
        var $td = $(btnEdit).closest("td");
        //console.log($tr);
        //remove readonly attribute for row so user can edit data.
        $tr.find("input,select,textarea").each(function () {
            if ((userAssignrole.toUpperCase() == 'PROD3')) {
                if ($(this).attr("role").toUpperCase() == "PROD") {
                    $(this).removeAttr("readonly");
                    $(this).removeAttr("disabled");
                    $(this).addClass("PRODTextbox");
                    $(this).addClass("EditableTextColor");
                }
                else {
                    $(this).addClass("QCTextbox");
                    $(this).prop('readonly', true);
                    $(this).addClass("NonEditableTextColor");
                }

            }
            else {
                if ($(this).attr("role").toUpperCase() == "QC") {
                    $(this).removeAttr("readonly");
                    $(this).removeAttr("disabled");
                    $(this).addClass("EditableTextColor");
                    $(this).addClass("QCTextbox");
                }
                else {
                    $(this).addClass("PRODTextbox");
                    $(this).prop('readonly', true);
                    $(this).addClass("NonEditableTextColor");
                }
            }
            //$(this).removeAttr("readonly");
            //$(this).removeAttr("disabled");
        });
        $('input[maxlength]').maxlength({
            alwaysShow: true,
        });


        //check there is any div exist with update and cancel button div. If exist then show div otherwise create new div for update and cancel button.
        if ($td.find('.editmode').length == 0) {
            var editdiv = "<div class='editmode' style='display:inline;'><i id='Update" + lineId + "' name='Update" + lineId + "' style='cursor: pointer;' title='Update Record' class='fa fa-floppy-o iconspace ' onclick='UpdateProtocolLine(" + lineId + "," + gridId + ");'></i>";
            editdiv += "<i id='btnCancel" + lineId + "' name='btnCancel" + lineId + "' style='cursor: pointer;' title='Cancel Edit' class='fa fa-ban iconspace canceledit'></i></div> "
            $td.append(editdiv);
        }
        else {
            $td.find('.editmode').show();
        }
        //hide edit button while enable edit mode
        $(btnEdit).hide();

        SetAddNewIntialStage();
        CancelEditEnable();
        SetNumericField();
    }

    function CancelEditEnable() {
        $(edittbl).find(".canceledit").unbind("click");
        $(edittbl).find(".canceledit").click(function () {
            CancelEditLine($(this));
        });
    }
    function CancelEditLine(btnCancel) {
        var $tr = $(btnCancel).closest("tr");
        $tr.html(editetr.text);
        editetr = {};
        SetAddNewIntialStage();
        EditLineEnable();
    }

    function SetAddNewIntialStage() {
        btnAddNew.html('<i class="fa fa-plus"></i> New');
        if (edittbl.find("tbody tr:first").is(":visible")) {
            edittbl.find("tbody tr:first").toggle();
        }
    }

    function SetNumericField() {
        jQuery(function ($) {
            $('.numeric').autoNumeric('init', { aPad: "fase", vMax: "1000000000", aForm: "false" });
            $('.numeric18').autoNumeric('init', { aPad: "fase", vMax: "999999999999999999", aForm: "false" });
            $('.numeric4').autoNumeric('init', { aPad: "fase", vMax: "9999", aForm: "false" });
        });
    }

    return {
        init: function (btnid, tblid, dv) {
            Initialize(btnid, tblid, dv);
        }
    }
}();

/* Start :  New Grid Implementation */
function ExpandCollapsChild(id, obj) {
    var src = $(obj).attr("src");
    if (src == WebsiteURL + "/Images/details_open.png") {
        $(obj).attr("src", WebsiteURL + "/Images/details_close.png");
    }
    else if (src == WebsiteURL + "/Images/details_close.png") {
        $(obj).attr("src", WebsiteURL + "/Images/details_open.png");
    }
    $('span.' + id).parent().parent().toggle();
}
function CollapseEvent(divId, $this) {
    var el = $("#" + divId);
    if ($($this).hasClass("collapsepanel")) {
        $($this).removeClass("collapsepanel").addClass("expandpanel");
        el.slideUp(200);
    } else {
        $($this).removeClass("expandpanel").addClass("collapsepanel");
        el.slideDown(200);
    }
}

function AddNextRow($this, focuscolumn, focuswithintable, gridId, hdnRowID, tblId, $txtfocusColumn, paramList, aftercallbackfn, isnew, beforecallbackfn) {
    //debugger
    var prevRow = parseInt($("#" + hdnRowID).val());
    var nextRow = parseInt($("#" + hdnRowID).val()) + 1;

    var flag = true;
    if (isnew != true && ($this != null && $($this).val() != "" && $this != undefined) && (beforecallbackfn != null && beforecallbackfn != undefined)) {
        flag = beforecallbackfn($this); /* before adding new row, execute other call back function*/
    }
    if (flag) {
        if (GetParamCondition(paramList, gridId, prevRow) || isnew == true) {
            $("#Delete_" + gridId + "_" + prevRow).show();
            if (aftercallbackfn != null && aftercallbackfn != undefined) {
                $("#" + tblId).find('select,input').removeClass('islastrow');
                $("#" + tblId + " tr").removeClass('islastrow');
                $("#" + tblId).append(aftercallbackfn(nextRow));
            }
            $("#" + $txtfocusColumn + "_" + gridId + "_" + nextRow).focus();
        }
    }
    else {
        if ($this != null && $this != undefined) {
            $($this).focus();
        }
        if (!focuswithintable && focuscolumn != null && isnew != true) {
            $("#" + focuscolumn).focus();
        }
    }
    $('input[maxlength]').maxlength({
        alwaysShow: true,
    });
    $(".date-picker").datepicker({
        orientation: "auto", autoclose: true, format: "dd/mm/yyyy"
    }).on('changeDate', function (e) {

    });
    $('.datepicker').datepicker({
        orientation: "auto", autoclose: true, format: "dd/mm/yyyy"
    });
    jQuery(function ($) {
        $('.numeric').autoNumeric('init', { aPad: "fase", vMax: "1000000000", aForm: "false" });
        $('.numeric18').autoNumeric('init', { aPad: "fase", vMax: "999999999999999999", aForm: "false" });
        $('.numeric4').autoNumeric('init', { aPad: "fase", vMax: "9999", aForm: "false" });
    });
    //debugger
    if (isnew != true && ($this != null && $this != undefined) && (focuscolumn != null && focuscolumn != undefined)) {
        var row_index = $($this).parent().parent().index();
        if ($($this).val() == "") {
            if (focuswithintable) {
                var cell = focuscolumn.split(',');
                if ($("#" + tblId + " tbody>tr:eq(" + (row_index + 1) + ")").find('td.' + cell[0]).has("input[type=text]").length == 0) {
                    if ($("#" + tblId + " tbody>tr:eq(" + (row_index + 2) + ")").find('td.' + cell[0]).find("input").length > 0) {
                        $("#" + tblId + " tbody>tr:eq(" + (row_index + 2) + ")").find('td.' + cell[0]).find("input").focus();
                    }
                    else {
                        $("#" + cell[1]).focus();
                    }
                }
                else {
                    $("#" + tblId + " tbody>tr:eq(" + (row_index + 1) + ")").find('td.' + cell[0]).find("input").focus();
                }
            }
            else {
                $("#" + focuscolumn).focus()
            }
        }
    }
}

function AddNextGroupRow($this, focuscolumn, focuswithintable, gridId, hdnRowID, tblId, $txtfocusColumn, paramList, aftercallbackfn, isnew, appendlocation) {
    //debugger
    var prevRow = parseInt($("#" + hdnRowID).val());
    var nextRow = parseInt($("#" + hdnRowID).val()) + 1;
    if (isnew == true || GetParamCondition(paramList, gridId, prevRow, tblId, $this, appendlocation)) {
        if (aftercallbackfn != null && aftercallbackfn != undefined) {
            if (appendlocation != undefined && appendlocation != "" && appendlocation != null) {
               
                $row = $("#" + tblId).find('tr.' + appendlocation.split("__")[0]).last();
                $row.find("td.DeleteAction").find("i").show();
                $row.removeClass('islastrow');
                $row.find('select,input').removeClass('islastrow');
                $('#' + appendlocation).closest('tr').after(aftercallbackfn(nextRow, appendlocation));
            }
        }
        else {
            $("#Delete_" + gridId + "_" + prevRow).show();
            $("#" + tblId).find('select,input').removeClass('islastrow');
            $("#" + tblId + " tr").removeClass('islastrow');
            $("#" + tblId).append(aftercallbackfn(nextRow));
        }
        $("#" + $txtfocusColumn + "_" + gridId + "_" + nextRow).focus();
    }
    //debugger
    if (isnew != true && ($this != null && $this != undefined) && (focuscolumn != null && focuscolumn != undefined) && (focuscolumn != null && focuscolumn != undefined)) {
        
        var row_index = $($this).parent().parent().index();
        if ($($this).val() == "") {
            //debugger

            if (focuswithintable) {
                var cell = focuscolumn.split(',');
                if ($("#" + tblId + " tbody>tr:eq(" + (row_index + 1) + ")").find('td.' + cell[0]).has("input[type=text]").length == 0) {
                    if ($("#" + tblId + " tbody>tr:eq(" + (row_index + 2) + ")").find('td.' + cell[0]).find("input").length > 0) {
                        $("#" + tblId + " tbody>tr:eq(" + (row_index + 2) + ")").find('td.' + cell[0]).find("input").focus();
                    }
                    else {
                        $("#" + cell[1]).focus();
                    }
                }
                else {
                    $("#" + tblId + " tbody>tr:eq(" + (row_index + 1) + ")").find('td.' + cell[0]).find("input").focus();
                }
            }
        }
        else {

            $("#" + focuscolumn).focus()
        }
    }

}

function AddNextRowNotes($this, focuscolumn, focuswithintable, gridId, hdnRowID, tblId, $txtfocusColumn, paramList, aftercallbackfn, isnew, beforecallbackfn,groupid) {
    var prevRow = parseInt($("#" + hdnRowID).val());
    var nextRow = parseInt($("#" + hdnRowID).val()) + 1;
    var prevRowId = prevRow;
    var nextRowId = nextRow;
    if (groupid != undefined && groupid != null && groupid != "") {
        prevRowId = groupid + "_" + prevRow;
        nextRowId = groupid + "_" + nextRow;
    }
    var flag = true;
    if (isnew != true && ($this != null && $($this).val() != "" && $this != undefined) && (beforecallbackfn != null && beforecallbackfn != undefined)) {
        flag = beforecallbackfn($this); /* before adding new row, execute other call back function*/
    }
    if (flag) {
        if (GetParamCondition(paramList, gridId, prevRowId) || isnew == true) {
            $("#Delete_" + gridId + "_" + prevRowId).show();
            if (aftercallbackfn != null && aftercallbackfn != undefined) {
                $("#" + tblId).find('select,input').removeClass('islastrow');
                $("#" + tblId + " tr").removeClass('islastrow');
                $("#" + tblId).append(aftercallbackfn(nextRow, groupid));
            }
            $("#" + $txtfocusColumn + "_" + gridId + "_" + nextRowId).focus();
        }
    }
    else {
        if ($this != null && $this != undefined) {
            $($this).focus();
        }
        if (!focuswithintable && focuscolumn != null && isnew != true) {
            $("#" + focuscolumn).focus();
        }
    }
    $('input[maxlength]').maxlength({
        alwaysShow: true,
    });
    $(".date-picker").datepicker({
        orientation: "auto", autoclose: true, format: "dd/mm/yyyy"
    }).on('changeDate', function (e) {

    });
    $('.datepicker').datepicker({
        orientation: "auto", autoclose: true, format: "dd/mm/yyyy"
    });

    //debugger
    if (isnew != true && ($this != null && $this != undefined) && (focuscolumn != null && focuscolumn != undefined)) {
        var row_index = $($this).parent().parent().index();
        if ($($this).val() == "") {
            if (focuswithintable) {
                var cell = focuscolumn.split(',');
                if ($("#" + tblId + " tbody>tr:eq(" + (row_index + 1) + ")").find('td.' + cell[0]).has("input[type=text]").length == 0) {
                    if ($("#" + tblId + " tbody>tr:eq(" + (row_index + 2) + ")").find('td.' + cell[0]).find("input").length > 0) {
                        $("#" + tblId + " tbody>tr:eq(" + (row_index + 2) + ")").find('td.' + cell[0]).find("input").focus();
                    }
                    else {
                        $("#" + cell[1]).focus();
                    }
                }
                else {
                    $("#" + tblId + " tbody>tr:eq(" + (row_index + 1) + ")").find('td.' + cell[0]).find("input").focus();
                }
            }
            else {
                $("#" + focuscolumn).focus()
            }
        }
    }
}

/* check validation for mentioned array columns*/
function GetParamCondition(list, gridid, rowid, tblId, $this, $appendlocation) {
    var q = true;
    if (($this != null && $this != "" && $this != undefined) && ($appendlocation != "" && $appendlocation != undefined)) {
        $row = $("#" + tblId).find('tr.' + $appendlocation.split("__")[0]).last();
        $cells = $row.find("td input").not("readonly");
        $cells.each(function () {
            if (!$(this).is(":disabled") && $(this).attr('readonly') != 'true' && $(this).attr('readonly') != 'readonly' ) {
                flg = $(this).val();
                if (q == true && (flg == "" || flg == null || flg == undefined || flg.legth == 0)) {
                    q = false;
                    return false;
                }
            }
        });
    }
    else {
        for (var j = 0; j < list.length; j++) {
            var flg = "";
            flg = $("#" + list[j] + "_" + gridid + "_" + rowid).val();
            if (flg == "" || flg == null || flg == undefined || flg.legth == 0) {
                q = false;
                break;
            }
        }
    }
    return q;
}

/*Save as Draft Protocol Line*/
function SaveAsDraftProtocolLine(data, URL, gridId,aftercallbackfn) {
    $.ajax({
        url: WebsiteURL + URL,
        type: "POST",
        dataType: 'json',
        data: data,
        contentType: "application/json; charset=utf-8",
        beforeSend: function () {
            showLoading(true);
        },
        complete: function () {
            showLoading(false);
        },
        success: function (data) {

        }
    });
}

/*Save as Draft Protocol Line*/

/* Delete single record in html table */
function DeleteProtocolLine(obj) {
    $(obj).parent().parent().remove();
}
/* Delete multiple record in html table according to matching column */
function DeleteProtocolGroupLine(selectedValue, matchColumn, tblId) {
    var selectedValue = $("#" + selectedValue).val();
    $("#" + tblId + " tbody td").find('input[colname="' + matchColumn + '"]').each(function (i, el) {
        if ($(this).val() == selectedValue) {
            $(this).parents("tr").remove();
        }
    });
}

function DeleteNewTable(tblid, id) {
    $("#tblProtocolLines" + tblid).find('tbody[data-group=' + id + ']').remove();
}
function PeakInOutValue($this, colname) {
    if ($($this).val() != "" && $($this).val() != null && $($this).val() != undefined) {
        $($this).closest('tr').find('input[colname="' + colname + '"]').val("NA");
    }
}

function GetColumnTextBoxRender(rowId, columnName, columnValue, onBlurMethod, maxlength, disabled, isReadOnly, inputStyle, role, gridId, classname, isblurevent, isautofocus) {
  
    columnValue = columnValue ? columnValue : ""; // replace null with empty space char
    if (columnValue.indexOf("<input") != -1 || columnValue.indexOf("<select") != -1) // already textbox
        return columnValue;
    var htmlControl = "";
    var inputID = columnName + "_" + gridId + "_" + rowId;
    var MaxCharcters = maxlength ? "maxlength='" + maxlength + "'" : "";
    var onBlurEvent = isblurevent ? (onBlurMethod ? "onblur=\"" + onBlurMethod + "\"" : "") : (onBlurMethod ? "onchange=\"" + onBlurMethod + "\"" : "");
    var htmlRole = role ? "role = '" + role + "'" : "";
    var inlineClass = " NonEditableTextColor " + classname;
    var disabledTxt = true;
    if (userRole == 'QC3') {
        inlineClass = " EditableTextColor";
        disabledTxt = false;
    }
    else if (userRole == 'PROD3') {
        inlineClass = " EditableTextColor";
        disabledTxt = false;
    }
    else {
        disabledTxt = true;
    }
    if (role == 'QC')
        inlineClass += " QCTextbox";
    if (role == 'PROD')
        inlineClass += " PRODTextbox"
    htmlControl = "<input type=\"text\"  lineid=" + rowId + "  class=\"form-control islastrow " + inlineClass + "\" id=\"" + inputID + "\"  name=\"" + inputID + "\"  colname=\"" + columnName + "\" " + MaxCharcters + " value=\"" + columnValue + "\" " + htmlRole
      + " style=\"" + inputStyle + "\"  " + onBlurEvent + " "
      + (disabledTxt ? "disabled " : " ") +"   " + (isReadOnly ? "readonly=true  tabindex=\"-1\"" : "") 
        //+ (isautofocus ? "autofocus" : "")
      + " />";
    return htmlControl;
}


function GetColumnDatePickerRender(rowId, columnName, columnValue, onBlurMethod, maxlength, disabled, isReadOnly, inputStyle, role, gridId, classname, isblurevent) {
    columnValue = columnValue ? columnValue : ""; // replace null with empty space char
    if (columnValue.indexOf("<input") != -1 || columnValue.indexOf("<select") != -1) // already textbox
        return columnValue;
    var htmlControl = "";
    var inputID = columnName + "_" + gridId + "_" + rowId;
    var MaxCharcters = maxlength ? "maxlength='" + maxlength + "'" : "";
    var onBlurEvent = isblurevent ? (onBlurMethod ? "onblur=\"" + onBlurMethod + "\"" : "") : (onBlurMethod ? "onchange=\"" + onBlurMethod + "\"" : "");
    var htmlRole = role ? "role = '" + role + "'" : "";
    var inlineClass = " NonEditableTextColor " + classname;
    var disabledTxt = true;

    if (userRole == 'QC3') {
        inlineClass = " EditableTextColor";
        disabledTxt = false;
    }
    else if (userRole == 'PROD3') {
        inlineClass = " EditableTextColor";
        disabledTxt = false;
    }
    if (role == 'QC')
        inlineClass += " QCTextbox";
    if (role == 'PROD')
        inlineClass += " PRODTextbox"
    htmlControl = '<div id ="datetime1' + inputID + '" class="date date-picker" style="display:inline-block;height: 10px !important;" data-date-format="dd/mm/yyyy">'
        + "<input lineid=" + rowId + "  class=\"form-control islastrow " + inlineClass + "\" id=\"" + inputID + "\"  name=\"" + inputID + "\" colname=\"" + columnName + "\" readonly asp-format='{0:dd/MM/yyyy}' value=\"" + columnValue + "\" " + htmlRole + " style=\"width: 150px !important;" + inputStyle + "\"  /> "
        + '<span class="input-group-btn" style="top: -26px;float: right;margin-right: -4px !important;">'
        + '<button class="btn default btncalender"  id="btncal1' + inputID + '"  type="button" tabindex="-1">'
        + '<i class="fa fa-calendar"></i>'
        + '</button>'
        + '</span></div>'
        + '<script>$("#datetime1' + inputID + '").datepicker({    orientation: "auto", autoclose: true, format: "dd/mm/yyyy"}).on("changeDate", function (e)'
        + '{ ' + onBlurMethod + ' });</script>';
    return htmlControl;
}

function GetColumnDropdownRender(lst, rowId, columnName, columnValue, onBlurMethod, maxlength, disabled, isReadOnly, inputStyle, role, gridId, classname,IsValidateDrgNo) {
    columnValue = columnValue ? columnValue : ""; // replace null with empty space char
    if (columnValue.indexOf("<input") != -1 || columnValue.indexOf("<select") != -1) // already textbox
        return columnValue;
    var htmlControl = "";
    var inputID = columnName + "_" + gridId + "_" + rowId;
    var MaxCharcters = maxlength ? "maxlength='" + maxlength + "'" : "";
    if (IsValidateDrgNo == null || IsValidateDrgNo == undefined)
        IsValidateDrgNo = false;

    if (columnName == "DrawingNo" && IsValidateDrgNo) {
        onBlurMethod = onBlurMethod + " " + "validateDrawingNo(" + rowId + ")";
    }
    var onChangedEvent = onBlurMethod ? "onchange=\"" + onBlurMethod + "\"" : "";
    var htmlRole = role ? "role = '" + role + "'" : "";
    var inlineClass = "form-control  NonEditableTextColor " + classname;
    var disabledTxt = true;

    if (userRole == 'QC3') {
        inlineClass = " EditableTextColor";
        disabledTxt = false;
    }
    else if (userRole == 'PROD3') {
        inlineClass = " EditableTextColor";
        disabledTxt = false;
    }
    if (role == 'QC')
        inlineClass += " QCTextbox";
    if (role == 'PROD')
        inlineClass += " PRODTextbox"
    var selectOptions = "<option value=''></option>";
    if (lst != null && lst != undefined && lst != "undefined") {
        $.each(lst, function (key, value) {
            selectOptions += "<option value='" + value.Value + "' >" + value.Text + "</option>";
        });
    }

    htmlControl = "<select lineid=" + rowId + " id='" + inputID + "' name='" + inputID + "' " + (isReadOnly ? "disabled='disabled'" : "") + " " + onChangedEvent + " colname='" + columnName + "' " + htmlRole + " class='form-control islastrow " + inlineClass + "' style='" + inputStyle + "' >" + selectOptions + "</select>";
    return htmlControl;
}

function GetColumnHiddenRender(rowId, columnName, columnValue, role, gridId, classname) {
    columnValue = columnValue ? columnValue : ""; // replace null with empty space char
    if (columnValue.indexOf("<input") != -1 || columnValue.indexOf("<select") != -1) // already textbox
        return columnValue;
    var htmlControl = "";
    var inputID = columnName + "_" + gridId + "_" + rowId;

    htmlControl = "<input type=\"hidden\" data-attr\"columnValue\"  class=\"form-control islastrow\"  id=\"" + inputID + "\"  name=\"" + inputID + "\" colname=\"" + columnName + "\" value=\"" + columnValue + "\") />";
    return htmlControl;
}

function CheckSeamInSeamList($this, qualityproject, location) {
    var flg = false;
    var columnValue = $($this).val();
    $.ajax({
        type: "POST",
        url: WebsiteURL + "/Utility/General/CheckSeamInSeamList",
        datatype: 'json',
        async: false,
        data: { seamno: columnValue, qualityproject: qualityproject, location: location },
        success: function (result) {
            if (result == "True") {
                flg = true;
            }
            else {
                DisplayNotiMessage("info", "Seam is invalid", "Info");
                $($this).val('');
                flg = false;
            }
        },
        error: function () {
            alert('error...');
        }
    });
    return flg;
}
// DrawingRevisionNo Code for numeric
$("body").on("keypress", ".onlyNumeric", function (e) {
    var keyCode = e.which ? e.which : e.keyCode;
    if (!(keyCode >= 48 && keyCode <= 57)) {
        return false;
    }
});

/* End :  New Grid Implementation */