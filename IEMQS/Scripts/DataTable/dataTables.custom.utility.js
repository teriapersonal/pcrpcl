﻿// ----------------------------------------------- //
// ---------------- How To Use ------------------- //
/*  
 *  below js must be included after the datatable.min.js
    <script src="~/Scripts/Column-Filter.js"></script>
    <script src="~/Scripts/DataTable/dataTables.custom.fixedColumn.js"></script>
    <script src="~/Scripts/DataTable/dataTables.custom.utility.js"></script>
    
 *  below function must be execute before datatable initialize
    var settings = {
        sTableID: 'tblTableID', //Table id                                                  | Required
        sUrl: '/LoadDatatableData', // Fetch Data Url without host address                  | Required
        bIsPaging: false, //Paginate                                                        | Default: true
        iPageLength: 100, // Set Default Page Length on page load                           | Default: 10
        bIsLazyLoading: false, //Lazy Loading load data on scroll                           | Default: true
        bIsSearchFilter: true, //Is Column Level Filter Implement                           | Default: false
        bIsResponsive: false, //Is Responsive Grid implement (addClass control to set default expand)| Default: true
        iResponsiveOnWindowWidth: 600, // Turn on Responsive and ignore FixedColumn on load window Width| Default: undefined
        bIsFixedColumn: true, // Freeze Column from Start 2 and End 1                       | Default: false
        iFCLeft: 1, // Freeze Number of Columns from start to freeze (Only for Fixed Column)| Default: 2                  
        iFCRight: 0, // Freeze Number of Columns from end to freeze (Only for Fixed Column) | Default: 1
        bIsFirstRowHidden:true, //Is table first row hidden (Only for Fixed Column)         | Default:false
        bIsFirstColumnCheckbox:true, //Is table first column checkbox (Only for Fixed Column)| Default:false
        bIsIgnoreHiddenColumns:false, //Is count Hidden Columns for fixed column            | Default:true
        bIsAllCheckbox:true, //Is table has select all checkbox                             | Default:false
        sSelectAllSelector:'#tblTable_wrapper .example-select-all', //select all checkbox selector | Default: '#'+sTableID+'_wrapper #example-select-all'
        aiLengthMenu:[10,100], //add Length Menu options                                    | Default:[5, 10, 25, 50, 100]
        sSortOrder:'desc', // Order by Column on sorting                                    | Default:'asc'
        aaSort:[[1,sSortOrder]], //set default soring on page load (addClass 'sorting' in th to default order)| Default:aaSort:[[th.sorting,sSortOrder]]
        aServerParams: [{ "name": "Parameter", "value": "filterData" },{ "name": "Parameter", "selector": "#inputselector" },...], // Set Paraments on send data request| Default:[]
        iScrollY: 500, // Hieght of table and add scroll                                    | Default:false or bIsLazyLoading == true then default 300
        bScrollX: true, // set Overflow-x auto. add scroll on bottom of table               | Default: bIsResponsive == false
        bIsResizable: false, // Column width can change by dragging                         | Default: true
        bIsWrap: false, // Is Body content wrappeble                                        | Default: true
        bIsWrapHeader: false, // Is Header content wrappeble                                | Default: true
        sBtnDivSelector: '#Div.tools', // add grid button on this selector div              | Default: undefined
        bBtnColVis: true, // add button to show and hide column                             | Default: false
        bBtnMaximize: false, // add button to Zoom Panel                                    | Default: true
        fnServerDataCallback: function(){ // code }, // function will excute after data bind| Default: undefined
        fnCreateRow: function(){ // code }, // function will excute after fnRowCallback     | Default: undefined
        aoCustomSetting: [{prop:"bServerSide",value:false}] // override any default setting | Default: []
        aoColumns: [{"bSortable": "false", "sClass": "hide", "name": "UserId","ColName": "emp.UserId", "maxlength": 50},...], | Default: undefined
        iMaximizeTopPosition:100 //Used for display fullscreen Top position while in Modal popup    | Default: undefined
        iMaximizeLeftPosition:100 //Used for display fullscreen Left position while in Modal popup    | Default: undefined

        oSRE:{ // Single Row Editable or Inline Editable
            sUrl: "/GetInlineEditableRow", // SRE fetch inline editable row                 | Required
            sCallback: 'CallAfterLoadSRE', //it will call after SRE return                  | Required
            iSelectedHeight: 68, // set minimum height on editable stage.(required whem fixed column implemente)| Default: 0
            iRowIDAt: 1, // Index of Main Primary Column for filter on select               | Default: 0
            sPrimaryColName: "HeaderId", // Name of Main Primary Column for filter on select use while table bind using data attribute  | Default: ""
            sOtherColName: "LineId",
            sEditTriggerClass: 'fa-edit', //when this is undefined then it will trigger on row click | Default: undefined
            bIsAddBtnNew: false, Add New Button in BtnDivSelector | Default: true
            bIsUpdateFromServer: false // this will send IsReadOnly Param true and retrive only data after change row selection | Default: true
        }
    }

    initDTUtility(settings); 
    or
    $('#tblTableID').dtUtility(settings);

    [Note]:
    =======

 *  it will only integrate with datatable table.

 *  if bIsFixedColumn is true and not set iFCLeft or iFCRight is not set then default will taken
    Default setting { iFCLeft: 2, iFCRight: 1} 

 *   bIsFirstColumnCheckbox flag only used for 
    If your grid containing first column as checkbox and it's in FixedColumn area
    and replace $("#TableID") to $("#TableID_wrapper") where you fetch checked checkbox value
    do you have checkbox on header for checked all checkbox? so please add this class or id example-select-all

 *  When your used SRE for add new record
    please add bellow line on SaveData button click or Save Action
     $('#'+TableId+'_wrapper').find("thead:eq(1) .dataTables_sizing").html('') // it will remove copy of new record textbox
 
 *  When SRE call on Tr click 
    and you don't want to excute SRE on perticular column/Td
    add class 'SRE_disabled' in Td
    aoColumns: [{ "sClass": "SRE_disabled", "name": "Action" }];
    
 *  When your using LazyLoading
    please use Params.dtReload() to reload grid
    this will auto scroll to before reload postion
    
 *  When in your grid contain checkbox column
    better do not use lazy Loading.
    because when you scroll it will remove after new data load

 *  When your used fixedColumn & SRE
    if in your fixed td content is empty then 
    please add " " or "&nbsp" in SRE Readonly Response
    to resolved all TR height alignment

 *  When your table has select all checkbox & set bIsAllCheckbox true then
    it will send bCheckAll True if SelectAll Checkbox checked
    on serverside you must return checkedIds as below example
    Use Param.dtGetAllChecked() to get checked checkbox value
    if SelectAll Checkbox checked then it will return value from SelectAll Checkbox
    ServerSide Example: 
    var CheckedIds = "";
    if (param.bCheckedAll && lstResult.Any())
        CheckedIds = String.Join(",", db.SP_NAME(1, 0, "", strWhereCondition).Select(s => s.Id).ToList());
    return Json(new
    {
        .......,
        checkedIds = CheckedIds
    }, JsonRequestBehavior.AllowGet);

  * When your serverParam comes from dynamic inputs use selector instand if value
    aServerParams: [{ "name": "Parameter", "selector": "#inputselector" },...]

*/
// ----------------------------------------------- //

// ----- Parameterized Function ----- //
jQuery.fn.dtUtility = function (Params) {
    try {
        Params.sTableID = this[0].id;
        initDTUtility(Params);
        return this;
    } catch (e) {
        console.log(e);
        return false;
    }
};

function initDTUtility(Params) {

    try {
        // ----- Validate Requirement ----- //
        if (!Params.sTableID) {
            console.error("dataTables.custom.utility.js \nTableID is missing in initDTUtility({TableID:''})");
            return;
        }
        if ($('#' + Params.sTableID).length < 1) {
            console.log("dataTables.custom.utility.js \n" + Params.sTableID + " table not found!");
            return;
        }
        if (!Params.sUrl) {
            console.error("dataTables.custom.utility.js \nAjax Source Url is missing in initDTUtility({sUrl:''})");
            return;
        }
        if (Params.oSRE && !Params.oSRE.sUrl) {
            console.error("dataTables.custom.utility.js \nSingel Row Editable request url is missing in initDTUtility({ oSRE:{sUrl:''}})");
            return;
        }

        // ----- Set Default value ----- //
        {
            Params.Wrapper = "#" + Params.sTableID + "_wrapper";
            Params.Table = $('#' + Params.sTableID);

            if (Params.bIsPaging == false || Params.bIsLazyLoading == undefined) {
                Params.bIsLazyLoading = true;
            }
            if (Params.iResponsiveOnWindowWidth > 200 && Params.iResponsiveOnWindowWidth >= window.outerWidth) {
                Params.iFCLeft = 0;
                Params.iFCRight = 0;
                Params.bIsFixedColumn = false;
            }
            if (Params.bIsLazyLoading == true) {
                Params.aiLengthMenu = [];
                Params.iPageLength = 100;
            }
            if (Params.bIsLazyLoading == true || Params.bIsFixedHeader == true) {
                if (Params.iScrollY == undefined)
                    Params.iScrollY = $(window).height() > 500 ? $(window).height() - 260 : 500;
            }
            if (Params.bIsSearchFilter == undefined) {
                Params.bIsSearchFilter = false;
            }
            if (Params.iFCLeft > 0 || Params.iFCRight > 0) {
                Params.bIsFixedColumn = true;
                Params.iFCLeft = Params.iFCLeft || 0;
                Params.iFCRight = Params.iFCRight || 0;
            }
            if (Params.bIsFixedColumn == true) {
                Params.bIsResponsive = false; // responsive grid will not support
                if (Params.iFCLeft == undefined)
                    Params.iFCLeft = 2;
                if (Params.iFCRight == undefined)
                    Params.iFCRight = 1;
                if (Params.bIsIgnoreHiddenColumns == undefined)
                    Params.bIsIgnoreHiddenColumns = true;
            } else if (Params.bIsFixedColumn == undefined) {
                Params.bIsFixedColumn = false;
            }
            if (Params.bIsResponsive == undefined) {
                Params.bIsResponsive = true;
            }
            if (Params.iPageLength == undefined) {
                Params.iPageLength = 10;
            }
            if (Params.bIsPaging == undefined) {
                Params.bIsPaging = true;
            }
            if (Params.aiLengthMenu == undefined) {
                Params.aiLengthMenu = [
                    [5, 10, 25, 50, 100],
                    [5, 10, 25, 50, 100]
                ];
            } else if (Params.aiLengthMenu) {
                Params.aiLengthMenu = [Params.aiLengthMenu, Params.aiLengthMenu];
            }
            if (Params.aaSort == undefined) {
                Params.aaSort = [
                    [$('th.sorting:first', Params.Table).length ? $('th.sorting:first', Params.Table).index() : 0,
                        Params.sSortOrder ? Params.sSortOrder : 'asc']
                ];
            }
            if (Params.aServerParams == undefined) {
                Params.aServerParams = [];
            }
            if (Params.aoCustomSetting == undefined) {
                Params.aoCustomSetting = [];
            }
            if (Params.iScrollY == undefined) {
                Params.iScrollY = false;
            }
            if (Params.bScrollX == undefined) {
                Params.bScrollX = Params.bIsResponsive == false;
            }
            if (Params.bIsWrap == undefined) {
                Params.bIsWrap = Params.oSRE ? false : true;
            }
            if (Params.bIsWrapHeader == undefined) {
                Params.bIsWrapHeader = true;
            }
            if (Params.bIsResizable == undefined) {
                Params.bIsResizable = true;
            }
            if (Params.bIsAllCheckbox == undefined) {
                Params.bIsAllCheckbox = false;
            } else if (!Params.sSelectAllSelector) {
                Params.sSelectAllSelector = Params.Wrapper + ' #example-select-all';
            }

            if (Params.sBtnDivSelector == undefined || Params.bBtnColVis == undefined) {
                Params.bBtnColVis = false;
            }
            if (Params.sBtnDivSelector && Params.bBtnMaximize == undefined) {
                Params.bBtnMaximize = true;
            }
            Params.IsTableAsync = false;
        }

        // ---- Set DataTable Settings ----- //
        Params.TableSetting = { //default and required for all  
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "_MENU_ entries",
                "search": "Search:",
                "zeroRecords": Params.bIsLazyLoading ? "Please wait" : "No matching records found",
                'processing': 'Loading...'
            },
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "buttons": [],
            "order": Params.aaSort,
            "sAjaxSource": WebsiteURL + Params.sUrl,
            "fnServerParams": function (aoData) {
                aoData.push({ "name": "type", "value": "sdf" });
                if (aoData[4].value == -1) // default iDisplayLength is -1 when Paging false
                    aoData[4].value = 0;
                else if (Params.bIsLazyLoading == true && aoData[4].value < 15) // petch for fetch 50 record on Lazy Loading first call
                    aoData[4].value = 50;
                if (Params.aServerParams) {
                    for (var i in Params.aServerParams) {
                        aoData.push({ "name": Params.aServerParams[i].name, "value": (Params.aServerParams[i].selector ? $(Params.aServerParams[i].selector).val(): Params.aServerParams[i].value) });
                    }
                }
                if (Params.bIsAllCheckbox)
                    aoData.push({ "name": "bCheckedAll", "value": $(Params.sSelectAllSelector).is(':checked') });
            },
            "fnServerData": function (sSource, aoData, fnCallback) {
                $.ajax({
                    'dataType': 'json',
                    'type': 'POST',
                    'url': sSource,
                    'data': aoData,
                    'success': function (data) {
                        if (Params.oSRE)
                            $('.dataTables_scrollBody,.DTFC_LeftBodyWrapper,.DTFC_LeftBodyLiner,.DTFC_RightBodyWrapper,.DTFC_RightBodyLiner', Params.Wrapper).css('height', '');
                        fnCallback(data);
                        if (Params.sBtnDivSelector) { // for export excel
                            $(".txtStrWhere", Params.sBtnDivSelector).val(data.whereCondition);
                            $(".txtStrSort", Params.sBtnDivSelector).val(data.strSortOrder);
                        }
                        if (Params.fnServerDataCallback && (typeof Params.fnServerDataCallback == "function")) {
                            Params.fnServerDataCallback(aoData, data);
                        }
                        if (Params.bIsAllCheckbox) {
                            $(Params.sSelectAllSelector).val(data.checkedIds);
                            if (data.checkedIds)
                                $('input[class="clsCheckbox"]', Params.Wrapper).prop('checked', true);
                        }
                    },
                    'async': Params.IsTableAsync
                });
            },
            "createdRow": function (row, data, dataIndex) {
                if (Params.oSRE) {
                    if (!(data instanceof Array)) {
                        if (Params.oSRE.sPrimaryColName) {
                            $(row).attr("data-id", data["" + Params.oSRE.sPrimaryColName + ""]);
                        }
                        if (Params.oSRE.sOtherColName) {
                            $(row).attr("data-ocol", Params.oSRE.sOtherColName);
                            var splitId = Params.oSRE.sOtherColName.split(",");
                            for (var i = 0; i < splitId.length; i++) {
                                if (splitId[i] != null && splitId[i] != undefined && splitId[i] != "") {                                    
                                    $(row).attr("data-" + splitId[i], data["" + splitId[i] + ""]);
                                }
                            }
                        }
                    }
                    else {
                        $(row).attr("data-id", data[Params.oSRE.iRowIDAt || 0]);
                    }
                }
                if (Params.fnCreateRow && typeof (Params.fnCreateRow) == "function") {
                    Params.fnCreateRow(row, data, dataIndex);
                }
            }
        };

        if (Params.aoColumns) {
            $.each(Params.aoColumns, function (idx, col) {
                if (col.maxlength > 0 && !col.render) { // create list of column with Max Length Property
                    col.render = function (data, type, row, meta) {
                        if (data && data.length > col.maxlength)
                            return "<span title='" + data + "'>" + data.substr(0, col.maxlength - 3) + "...</span>";
                        else
                            return data;
                    };
                }
            });
            Params.TableSetting.columns = Params.aoColumns;
        }

        if (!Params.bIsPaging)
            Params.TableSetting.paging = false;
        else {
            Params.TableSetting.pageLength = Params.iPageLength;
            if (Params.aiLengthMenu)
                Params.TableSetting.lengthMenu = Params.aiLengthMenu;
        }

        if (Params.bIsResponsive) {
            if ($('th.control:first', Params.Table).index() > 0) { // set expand button on column. this will only needed when first column is hidden
                Params.TableSetting.responsive = {
                    details: { type: 'column', target: $('th.control:first', Params.Table).index() }
                };
            } else
                Params.TableSetting.responsive = true;
            Params.oSRE = false; // responsive grid will not support
        }

        if (Params.iScrollY) {
            Params.TableSetting.scrollY = Params.iScrollY; // Vertical scrollable 
            if (Params.bIsLazyLoading)
                Params.TableSetting.scroller = { loadingIndicator: true, boundaryScale: 1, displayBuffer: 4 };
        }
        if (Params.bScrollX) {
            Params.TableSetting.scrollX = Params.bScrollX; // Horizantal scrollable 
        }

        if (Params.bIsResizable) {
            $('th', Params.Table).on('mousedown', function (e) {
                Params.mouseDownAt = (e.offsetX > $(this).width() + 20) ? { x: e.pageX, y: e.pageY, id: this } : undefined;
            });
            $('th', Params.Table).on('mouseup', function (e) {
                if (Params.mouseDownAt && Params.mouseDownAt.x != e.pageX) {
                    var THWidth = $(Params.mouseDownAt.id).width() + e.pageX - Params.mouseDownAt.x;
                    $('th:nth-child(' + ($(Params.mouseDownAt.id).index() + 1) + ')', Params.Wrapper).css({ 'min-width': THWidth, 'max-width': THWidth });
                    Params.Table.DataTable().columns.adjust();
                }
            });
        }
        if (Params.bIsFixedColumn) {
            //initDataTableFixedColumn({ sTableID: Params.sTableID, iLeftCols: Params.iFCLeft, iRightCols: Params.iFCRight, bIsWrap: Params.oSRE ? false : true, bIsFirstRowHidden: Params.bIsFirstRowHidden, bIsFirstColumnCheckbox: Params.bIsFirstColumnCheckbox, bIsForceToSet: true, bIsSearchFilter: Params.bIsSearchFilter, bIsLazyLoading: Params.bIsLazyLoading });  // from dataTables.custom.fixedColumn.js
            Params.bIsForceToSet = true;
            // ----- Find Hidden Columns and increse Cols length ----- //
            if (Params.bIsIgnoreHiddenColumns) {
                Params.bIsIgnoreHiddenColumns = false;
                var cntTableCols = Params.aoColumns.length;
                for (var i = 0; i < Params.iFCLeft ; i++) {
                    if (i < cntTableCols && !$('#' + Params.sTableID + ' tr:first th:eq(' + i + ')').is(":visible"))
                        Params.iFCLeft++;
                }
                for (var i = cntTableCols - 1; i >= cntTableCols - Params.iFCRight; i--) {
                    if (!$('#' + Params.sTableID + ' tr:first th:eq(' + i + ')').is(":visible"))
                        Params.iFCRight++;
                }
            }
            Params.TableSetting.fixedColumns = {
                leftColumns: Params.iFCLeft,
                rightColumns: Params.iFCRight
            };
            Params.iLeftCols = Params.iFCLeft;
            Params.iRightCols = Params.iFCRight;

            if (IsBrowser("ie,edge,firefox"))
                $("#" + Params.sTableID).css("width", "99.9%");
        }

        Params.Table.one('preInit.dt', function (e, settings) {
            showLoading(true);
        });

        Params.Table.one('init.dt', function (e, settings, json) {
            injectFixedColumnCss(Params.sTableID, Params.bIsWrap, Params.bIsWrapHeader);
            setTimeout(function () {
                Params.IsTableAsync = true;
                if (Params.bIsFixedColumn) {
                    setFixedColumns(Params);
                } else {
                    if (Params.bIsSearchFilter) { // implement Column Level Filter
                        setTimeout(function () {
                            InitSearchFilterForFixedColumn(Params);
                            // ----- remove original search boxs and initalize event ----- //
                            FixedTableSearchIntialze(Params.sTableID);
                            $('#' + Params.sTableID).DataTable().columns.adjust();
                        }, 1000);
                    }
                    // ----- First Column is checkbox ----- //
                    if (Params.bIsFirstColumnCheckbox == true && Params.bIsAllCheckbox != true) {
                        $(Params.sSelectAllSelector).on('click', function () {
                            var rows = Params.Table.dataTable().api().rows({ 'search': 'applied' }).nodes();
                            $('input[class="clsCheckbox"]', rows).prop('checked', this.checked);
                        });
                        $('#' + Params.sTableID + ' tbody').on('change', 'input[class="clsCheckbox"]', function () {
                            if (!this.checked) {
                                var el = $(Params.sSelectAllSelector).get(0);
                                if (el && el.checked && ('indeterminate' in el)) {
                                    el.indeterminate = true;
                                    el.checked = false;
                                }
                            }
                        });
                    }
                }
                if (Params.oSRE) { // add Single/Inline Row Editable                    
                    $(Params.Wrapper).attr("data-sreurl", Params.oSRE.sUrl);
                    $(Params.Wrapper).attr("data-srecallback", Params.oSRE.sCallback);
                    if (Params.oSRE.bIsUpdateFromServer != false)
                        $(Params.Wrapper).attr("data-sreisupdatefromserver", true);
                    if (Params.oSRE.sEditTriggerClass)
                        SREbindClickTd(Params.sTableID, Params.oSRE.sEditTriggerClass, SRELoadEditableRow);// on click make it inline editable
                    else
                        SREbindClickTr(Params.sTableID, SRELoadEditableRow); // on Row click make it inline editable                    
                    if (Params.oSRE.iSelectedHeight > 0 && $("#" + Params.sTableID + "_SRE_style").length == 0) { // set adjust height of all fixed and non-fixed columns on select row
                        $(Params.Wrapper).attr("data-sreselectedheight", Params.oSRE.iSelectedHeight);
                        $(Params.Wrapper).parent().prepend("<style id='" + Params.sTableID + "_SRE_style'> \
                             " + Params.Wrapper + " .selected{ height:" + Params.oSRE.iSelectedHeight + "px;} \
                                td.sorting:after, td.sorting_asc:after, td.sorting_desc:after, td.sorting_asc_disabled:after, td.sorting_desc_disabled:after {visibility: hidden;} \
                             " + Params.Wrapper + " .select2-selection{ max-height:50px;overflow-x:auto; } \
                             " + Params.Wrapper + " .select2-container--open{ z-index:9995; } </style>");
                    }
                    if (Params.sBtnDivSelector && Params.oSRE.bIsAddBtnNew != false && $('#btnNew_' + Params.sTableID).length == 0)
                        $(Params.sBtnDivSelector).append('<div class="btn-group"><a style="margin-right:5px;margin-left:5px;" id="btnNew_' + Params.sTableID + '" class="btn btn-circle white btn-outline btn-sm" onclick="SRENewRecord(this,\'' + Params.sTableID + '\')"><i class="fa fa-plus"></i> New</a></div>');
                }
                if (Params.bIsResizable) {
                    var cssResize = Params.Wrapper + ' thead tr:not(.mycustomfilter) th{ user-select: none; position: relative; }' + Params.Wrapper + ' thead tr:not(.mycustomfilter) th:before{ cursor: col-resize; content: " "; height: 95%; right: 0px; width: 15px; top: 0px; position: absolute; }';
                    if ($("#" + Params.sTableID + "_SRE_style").length == 0)
                        $(Params.Wrapper).parent().prepend("<style id='" + Params.sTableID + "_SRE_style'> " + cssResize + " </style>");
                    else
                        $("#" + Params.sTableID + "_SRE_style").append(cssResize);
                }
                if (Params.sBtnDivSelector) { // Add Grid Buttons
                    $('.dt-btn', Params.sBtnDivSelector).remove();
                    $(Params.sBtnDivSelector).append('<div class="btn-group dt-btn"></div>');
                    if (Params.bBtnColVis) {
                        var buttons = new $.fn.dataTable.Buttons(Params.Table, {
                            buttons: [
                                    { extend: 'colvis', className: 'btn btn-circle white btn-outline btn-sm' }
                            ]
                        }).container().appendTo($('.dt-btn', Params.sBtnDivSelector));
                    }
                    if (Params.bBtnMaximize) {
                        var TopPosition = Params.iMaximizeTopPosition != undefined && Params.iMaximizeTopPosition != "" && Params.iMaximizeTopPosition != "undefined" ? Params.iMaximizeTopPosition : -87;
                        var LeftPosition = Params.iMaximizeLeftPosition != undefined && Params.iMaximizeLeftPosition != "" && Params.iMaximizeLeftPosition != "undefined" ? Params.iMaximizeLeftPosition : -351;
                        $('<label onclick="TablePortletMaximize(this,' + TopPosition + ',' + LeftPosition + ');" class="btn btn-circle white btn-outline btn-sm" title="Panel Maximize"><i class="fa fa-expand"></i></label>').appendTo($('.dt-btn', Params.sBtnDivSelector));
                    }
                }
                if (Params.bIsAllCheckbox) {
                    $(Params.sSelectAllSelector).on('click', function () {
                        if (this.checked) {
                            Params.Table.DataTable().ajax.reload(function () {
                                var rows = Params.Table.dataTable().api().rows({ 'search': 'applied' }).nodes();
                                $('input[class="clsCheckbox"]', rows).prop('checked', true);
                            });
                        } else {
                            this.value = '';
                            var rows = Params.Table.dataTable().api().rows({ 'search': 'applied' }).nodes();
                            $('input[class="clsCheckbox"]', rows).prop('checked', false);
                        }
                    });
                    $(Params.Wrapper + ' tbody').on('change', 'input[class="clsCheckbox"]', function () {
                        if (!this.checked) {
                            var el = $(Params.sSelectAllSelector).get(0);
                            if (el && el.checked && ('indeterminate' in el)) {
                                el.indeterminate = true;
                                el.checked = false;
                            }
                        }
                    });
                }
                if (!Params.bIsSearchFilter) {
                    showLoading(false);
                }
            }, 10);
            setTimeout(function () { $('#' + Params.sTableID).DataTable().columns.adjust(); }, 1000); // to resolve column alignment with header after init
        });

        if (Params.aoCustomSetting) {
            $.each(Params.aoCustomSetting, function (idx, item) {
                Params.TableSetting[item.prop] = item.value;
            });
        }
        // ----- DataTableInit ----- //
        Params.Table.dataTable(Params.TableSetting);

        Params.dtReload = function () { // use this reload funtion to set scroll at row position
            var oDTTable = Params.Table.DataTable();
            var oDTPage = oDTTable.scroller.page();
            var start_row = oDTPage ? oDTPage.start : 0;
            oDTTable.ajax.reload(function () {
                oDTTable.scroller().scrollToRow(start_row, false);
            }, false);
        };

        Params.dtGetAllChecked = function () { // use this reload funtion to set scroll at row position
            var checkedValues = new Array();
            if (Params.bIsAllCheckbox && $(Params.sSelectAllSelector).is(':checked')) {
                checkedValues.push($(Params.sSelectAllSelector).val());
            } else {
                $('input[class="clsCheckbox"]:checked', Params.Wrapper + ' tbody').each(function () {
                    var headerId = $(this).val();
                    if (headerId != '')
                        checkedValues.push(headerId);
                });
            }
            if (checkedValues.length > 0)
                return checkedValues.join(",");
            return "";
        };
    } catch (e) {
        console.error(e);
    }

} // Init Ends

// ----- Table Panel Maximize Feature ----- //
function TablePortletMaximize(portlet,TopPosition,LeftPosition) {
    var panel = $(portlet).closest(".portlet");
    var pnlpos = $(panel).offset();
    if (panel.hasClass('portlet-maximize')) { // Panel Minimize
        $(portlet).html('<i class="fa fa-expand"></i>').prop('title', "Panel Maximize");
        panel.animate({
            width: "100%",
            height: "100%",
            top: pnlpos.top * -1,
            left: pnlpos.left * -1
        }, 1000);
        setTimeout(function () {
            panel.removeClass('portlet-maximize');
            $('html,body').css("overflow", "").animate({ scrollTop: $(panel).offset().top - 70 }, 1000); // auto main scrollbar;
            if ($('.bootbox.modal').length > 0) {
                $('.bootbox.modal').css("overflow-y", "");
                panel.css("position", "");
            }
        }, 1000);
    } else { // Panel Maximize
        $(portlet).html('<i class="fa fa-compress"></i>').prop('title', "Panel Minimize");
        $('html,body').animate({ scrollTop: 0 }, 1000).css("overflow", "hidden"); // hide main scrollbar
        if ($('.bootbox.modal').length > 0) { //  if table in Modal Popup
            $('.bootbox.modal').animate({ scrollTop: 0 }, 1000).attr("style", $('.bootbox.modal').attr("style") + "; overflow-y:hidden !important"); // used this cuz JQuery.css funtion not add "!important" in property
            panel.css("position", "absolute");
            panel.addClass('portlet-maximize').animate({
                width: '100vw',
                height: '100vh',
                top: TopPosition,//-87,
                left: LeftPosition //-351
            }, 1000);
        } else {
            panel.addClass('portlet-maximize').animate({
                width: '100vw',
                height: '100vh',
                top: pnlpos.top * -1,
                left: pnlpos.left * -1,
            }, 1000);
        }
    }
    setTimeout(function () {
        $('.dataTable[id]', panel).DataTable().columns.adjust();
    }, 1000);
}

//--------------- Singel Line Editable Row -------------------//

var SRE_CMD_Avoid = "##AVOID##";
function SREbindClickTr(TableId, callback) {
    $('#' + TableId + '_wrapper tbody').on('click', 'tr', function (e) {
        if (!$(e.target).closest('td').hasClass('SRE_disabled'))
            callback(TableId, this);
    });
}
function SREbindClickTd(TableId, triggerClass, callback) {
    $('#' + TableId + '_wrapper tbody').on('click', 'a,i,button,input[type=button]', function (e) {
        if ($(this).hasClass(triggerClass)) {
            callback(TableId, this);
        }
    });
}
function SRELoadEditableRow(TableId, element) {
    var Tr = $(element).closest("tr");
    if (!Tr.data("id"))
        return;
    if (!$(Tr).hasClass('selected')) {
        if ($('#btnNew_' + TableId).length > 0) {
            $('#btnNew_' + TableId).html('<i class="fa fa-plus"></i> New');
            $('#' + TableId + '_wrapper').find("thead").find("tr[data-id=0]").remove();
        }
        SREresetRowAll(TableId);

        var rowIndex = $(Tr).index();
        var $MainTR = $('#' + TableId + '_wrapper').find("tbody:eq(0) tr:eq(" + rowIndex + ")");
        var $FDLTR = $('.DTFC_LeftBodyWrapper', '#' + TableId + '_wrapper').find("tbody tr:eq(" + rowIndex + ")");
        var $FDRTR = $('.DTFC_RightBodyWrapper', '#' + TableId + '_wrapper').find("tbody tr:eq(" + rowIndex + ")");

        $MainTR.css('height', '');
        $MainTR.addClass('selected');
        $FDLTR.addClass('selected');
        $FDRTR.addClass('selected');
        $FDLTR.removeAttr('style');
        $FDRTR.removeAttr('style');
        $('td.sorting_1', $MainTR).removeClass('sorting_1');
        $('td.sorting_1', $FDLTR).removeClass('sorting_1');
        $('td.sorting_1', $FDRTR).removeClass('sorting_1');

        var jsondata = {};
        jsondata["id"] = $MainTR.data("id");
        var OtherColumnParam = $MainTR.data("ocol");
        if (OtherColumnParam != null && OtherColumnParam != undefined && OtherColumnParam != "undefined") {
            var splitId = OtherColumnParam.split(",");
            for (var i = 0; i < splitId.length; i++) {
                if (splitId[i] != null && splitId[i] != undefined && splitId[i] != "") {
                    jsondata["" + splitId[i] + ""] = $MainTR.data("" + splitId[i].toLowerCase() + "");
                }
            }
        }

        $.ajax({
            'dataType': 'json',
            'type': 'POST',
            'url': WebsiteURL + $('#' + TableId + "_wrapper").data("sreurl"),
            'data': jsondata,
            beforeSend: function () {
                showLoading(true);
            },
            complete: function () {
                showLoading(false);
            },
            //'async': false,
            'success': function (data) {
                if (!$MainTR.hasClass('selected')) // no action if row selection changed.
                    return;
                var LenFCL = $($FDLTR).find('td').length;
                var LenFCR = $($FDRTR).find('td').length;
                var LenAll = data.aaData[0].length;
                if (!(data.aaData[0] instanceof Array)) {
                    var TableID = TableId;
                    LenAll = $('#' + TableID).DataTable().columns()[0].length; //Object.keys(data.aaData[0]).length;
                    for (var idx = 0; idx < LenAll; idx++) {
                        var colname = $('#' + TableID).DataTable().columns().settings()[0].aoColumns[idx].mData;
                        var datacolname = $('#' + TableID).DataTable().columns().settings()[0].aoColumns[idx].data;
                        var editrendercallback = $('#' + TableID).DataTable().columns().settings()[0].aoColumns[idx].editrendercallback;
                        var rendercallback = $('#' + TableID).DataTable().columns().settings()[0].aoColumns[idx].rendercallback;
                        var ismanualrendercolumn = false;
                        var actions = "";
                        if (editrendercallback != null && editrendercallback != undefined && editrendercallback != "undefined") {
                            var isSameFunction = false;
                            if (rendercallback != null && rendercallback != undefined && rendercallback != "undefined" && rendercallback == editrendercallback) {
                                isSameFunction = true;
                            }
                            ismanualrendercolumn = true;
                            var fn = window[editrendercallback];
                            if (typeof fn === "function") {
                                if (isSameFunction) {
                                    // First parameter : Data   Send paramter : iseditmode or not
                                    actions = eval(editrendercallback)(data.aaData[0], true);
                                }
                                else {
                                    actions = eval(editrendercallback)(data.aaData[0]);
                                }
                            }
                            else {
                                console.log(editrendercallback + " function does not exist.")
                            }
                        }
                        else if (datacolname == null || datacolname == undefined) {
                            ismanualrendercolumn = true;
                            actions = "";
                        }

                        if (data.aaData[0]["" + colname + ""] == SRE_CMD_Avoid)
                            return;
                        if (LenFCL > idx) {
                            if (ismanualrendercolumn) {
                                $($FDLTR).find('td:eq(' + idx + ')').html(actions);
                                $($MainTR).find('td:eq(' + idx + ')').html('');
                            }
                            else {
                                $($FDLTR).find('td:eq(' + idx + ')').html(data.aaData[0]["" + colname + ""]);
                                $($MainTR).find('td:eq(' + idx + ')').html('');
                            }
                        }
                        else if (LenAll - LenFCR <= idx) {
                            if (ismanualrendercolumn) {
                                $($FDRTR).find('td:eq(' + (LenAll - LenFCR - idx) + ')').html(actions);
                                $($MainTR).find('td:eq(' + idx + ')').html('');
                            }
                            else {
                                $($FDRTR).find('td:eq(' + (LenAll - LenFCR - idx) + ')').html(data.aaData[0]["" + colname + ""]);
                                $($MainTR).find('td:eq(' + idx + ')').html('');
                            }
                        }
                        else {
                            if (ismanualrendercolumn) {
                                $($MainTR).find('td:eq(' + idx + ')').html(actions);
                            }
                            else {
                                $($MainTR).find('td:eq(' + idx + ')').html(data.aaData[0]["" + colname + ""]);
                            }
                        }

                    }
                }
                else {
                    $.each(data.aaData[0], function (idx, item) {
                        if (item == SRE_CMD_Avoid)
                            return;
                        if (LenFCL > idx) {
                            $($FDLTR).find('td:eq(' + idx + ')').html(item);
                            $($MainTR).find('td:eq(' + idx + ')').html('');
                        }
                        else if (LenAll - LenFCR <= idx) {
                            $($FDRTR).find('td:eq(' + (LenAll - LenFCR - idx) + ')').html(item);
                            $($MainTR).find('td:eq(' + idx + ')').html('');
                        }
                        else
                            $($MainTR).find('td:eq(' + idx + ')').html(item);
                    });
                }
                $FDLTR.css("height", $MainTR.css('height'));
                $FDRTR.css("height", $MainTR.css('height'));
                if (typeof (window[$('#' + TableId + "_wrapper").data("srecallback")]) == "function")
                    window[$('#' + TableId + "_wrapper").data("srecallback")]($MainTR.data("id"));

                if ($('#' + TableId + '_wrapper').find("tbody:eq(0) > tr").length < 5) // to avoid vertical scroll add addition height when record less then 5
                    $('.dataTables_scrollBody,.DTFC_LeftBodyWrapper,.DTFC_LeftBodyLiner,.DTFC_RightBodyWrapper,.DTFC_RightBodyLiner', '#' + TableId + '_wrapper').css('height', $('#' + TableId).height() + 18);


                $('#' + TableId).DataTable().columns.adjust();
            }
        });
    }
}

function SREresetRowAll(TableID) {
    $("tr.selected", '#' + TableID + '_wrapper tbody:eq(0)').each(function (idx, Tr) {
        $(Tr).removeClass('selected');
        if ($(Tr).data('id') == "0") {
            $(Tr).remove();
            return;
        }
        var $MainTR = $(Tr);
        var rowIndex = $MainTR.index();
        var $FDLTR = $('.DTFC_LeftBodyWrapper', '#' + TableID + '_wrapper').find("tbody tr:eq(" + rowIndex + ")");
        var $FDRTR = $('.DTFC_RightBodyWrapper', '#' + TableID + '_wrapper').find("tbody tr:eq(" + rowIndex + ")");
        $($FDLTR).removeClass('selected');
        $($FDRTR).removeClass('selected');

        var jsondata = {};
        jsondata["id"] = $MainTR.data("id");
        jsondata["isReadOnly"] = true;
        var OtherColumnParam = $MainTR.data("ocol");
        if (OtherColumnParam != null && OtherColumnParam != undefined && OtherColumnParam != "undefined") {
            var splitId = OtherColumnParam.split(",");
            for (var i = 0; i < splitId.length; i++) {
                if (splitId[i] != null && splitId[i] != undefined && splitId[i] != "") {
                    jsondata["" + splitId[i] + ""] = $MainTR.data("" + splitId[i].toLowerCase() + "");
                }
            }
        }

        if ($('#' + TableID + "_wrapper").data("sreisupdatefromserver")) {
            $.ajax({
                'dataType': 'json',
                'type': 'POST',
                'url': WebsiteURL + $('#' + TableID + "_wrapper").data("sreurl"),
                'data': jsondata,
                beforeSend: function () {
                    showLoading(true);
                },
                complete: function () {
                    showLoading(false);
                },
                //'async': false,
                'success': function (data) {
                    var LenFCL = $($FDLTR).find('td').length;
                    var LenFCR = $($FDRTR).find('td').length;
                    var LenAll = data.aaData[0].length;
                    var oDTTable = $('#' + TableID).DataTable();
                    if (!(data.aaData[0] instanceof Array)) {
                        LenAll = $('#' + TableID).DataTable().columns()[0].length; //Object.keys(data.aaData[0]).length;
                        for (var i = 0; i < LenAll; i++) {
                            var colname = $('#' + TableID).DataTable().columns().settings()[0].aoColumns[i].mData;
                            var rendercallback = $('#' + TableID).DataTable().columns().settings()[0].aoColumns[i].rendercallback;
                            var editrendercallback = $('#' + TableID).DataTable().columns().settings()[0].aoColumns[i].editrendercallback;
                            var datacolname = $('#' + TableID).DataTable().columns().settings()[0].aoColumns[i].data;
                            if (data.aaData[0]["" + colname + ""] == SRE_CMD_Avoid)
                                return;
                            var td = $($MainTR).find('td:eq(' + i + ')');
                            if (rendercallback != null && rendercallback != undefined && rendercallback != "undefined") {
                                var isSameFunction = false;
                                if (editrendercallback != null && editrendercallback != undefined && editrendercallback != "undefined" && rendercallback == editrendercallback) {
                                    isSameFunction = true;
                                }
                                var fn = window[rendercallback];
                                if (typeof fn === "function") {
                                    var actions = "";
                                    if (isSameFunction) {
                                        // First parameter : Data   Send paramter : iseditmode or not
                                        actions = eval(rendercallback)(data.aaData[0], false);
                                    }
                                    else {
                                        actions = eval(rendercallback)(data.aaData[0]);
                                    }
                                    oDTTable.cell(td).data(actions);
                                } else {
                                    console.log(rendercallback + " function does not exist.")
                                }
                            }
                            else if (datacolname == null || datacolname == undefined) {
                                oDTTable.cell(td).data("");;
                            }
                            else {
                                oDTTable.cell(td).data(data.aaData[0]["" + colname + ""]);
                            }
                            if (LenFCL > i) {
                                $($FDLTR).find('td:eq(' + i + ')').html(td.html());
                                td.html('');
                            }
                            else if (LenAll - LenFCR <= i) {
                                $($FDRTR).find('td:eq(' + (LenAll - LenFCR - i) + ')').html(td.html());
                                td.html('');
                            }
                        }
                    }
                    else {
                        $.each(data.aaData[0], function (idx, item) {
                            if (item == SRE_CMD_Avoid)
                                return;
                            var td = $($MainTR).find('td:eq(' + idx + ')');
                            oDTTable.cell(td).data(item);
                            if (LenFCL > idx) {
                                $($FDLTR).find('td:eq(' + idx + ')').html(td.html());
                                td.html('');
                            }
                            else if (LenAll - LenFCR <= idx) {
                                $($FDRTR).find('td:eq(' + (LenAll - LenFCR - idx) + ')').html(td.html());
                                td.html('');
                            }
                        });
                    }
                    $FDLTR.css("height", $MainTR.css('height'));
                    $FDRTR.css("height", $MainTR.css('height'));
                    $('.dataTables_scrollBody,.DTFC_LeftBodyWrapper,.DTFC_LeftBodyLiner,.DTFC_RightBodyWrapper,.DTFC_RightBodyLiner', '#' + TableID + '_wrapper').css('height', '');

                    oDTTable.columns.adjust();
                }
            });
        } else {
            SRESetTextOnTD($MainTR);
        }

    });
}

function SRESetTextOnTD(Tr) {
    var rowIndex = Tr.index();
    var TableID = Tr.closest('table[id]').attr('id');
    var $FDLTR = $('.DTFC_LeftBodyWrapper', '#' + TableID + '_wrapper').find("tbody tr:eq(" + rowIndex + ")");
    var $FDRTR = $('.DTFC_RightBodyWrapper', '#' + TableID + '_wrapper').find("tbody tr:eq(" + rowIndex + ")");
    var LenFCL = $FDLTR.find('td').length;
    var LenFCR = $FDRTR.find('td').length;
    var LenAll = Tr.find('td').length;

    var oDTTable = $('#' + TableID).DataTable();
    $("td", Tr).each(function (idx, td) { // make it input to plain text
        var cell = oDTTable.cell(td);
        var targetTd = td;
        if (LenFCL > idx) {
            targetTd = $FDLTR.find('td:eq(' + idx + ')');
        } else if (LenAll - LenFCR <= idx) {
            targetTd = $FDRTR.find('td:eq(' + idx + ')');
        }
        var input = $("input,textarea", targetTd);
        var select = $("select", targetTd);
        var radio = $("input[type=radio]:selected", targetTd);
        var checkbox = $("input[type=checkbox]", targetTd);
        var isUpdated = false;
        if (select.length) {
            var selMulti = $.map(select.find("option:selected"), function (el, i) {
                return $(el).text();
            });
            cell.data(selMulti.join(", "));
            isUpdated = true;
        }
        else if (checkbox.length) {
            var selMulti = $.map(checkbox, function (el, i) {
                if ($(el).is(":checked"))
                    return $(el).val() && $(el).val() != 'on' ? $(el).val() : "True";
            });
            cell.data(selMulti.join(", "));
            isUpdated = true;
        }
        else if (radio.length) {
            cell.data(radio.val());
            isUpdated = true;
        }
        else if (input.length) {
            if (input.attr('type') == "date") { // Format the date DD/MM/YYYY
                var dateValue = input.val().match(/^(\d{4})-(\d{1,2})-(\d{1,2})$/);
                if (dateValue)
                    cell.data(dateValue[3] + "/" + dateValue[2] + "/" + dateValue[1]);
                else
                    cell.data(input.val());
            } else
                cell.data(input.val());
            isUpdated = true;
        }
        if (isUpdated == true) {
            if (LenFCL > idx || LenAll - LenFCR <= idx) {
                $(targetTd).html($(td).html());
                $(td).html('');
            }
        }

    });
    $('.dataTables_scrollBody,.DTFC_LeftBodyWrapper,.DTFC_LeftBodyLiner,.DTFC_RightBodyWrapper,.DTFC_RightBodyLiner', '#' + TableID + '_wrapper').css('height', '');

}
function SRENewRecord(obj, TableID) {
    var Wrapper = '#' + TableID + '_wrapper';
    $('.dataTables_scrollBody,.DTFC_LeftBodyWrapper,.DTFC_LeftBodyLiner,.DTFC_RightBodyWrapper,.DTFC_RightBodyLiner', Wrapper).css('height', '');
    if ($(obj).html() != '<i class="fa fa-times"></i> Cancel') {

        $(obj).html('<i class="fa fa-times"></i> Cancel');
        $(Wrapper).find("thead").each(function (idx, item) {
            if (idx % 2 != 0)
                return;
            var htmlTds = "";
            $(Wrapper).find("thead:eq(" + idx + ") tr:first th").each(function (i, th) {
                htmlTds += '<td class="' + $(th).attr('class') + '"></td>';
            });
            $(item).append("<tr data-id='0'>" + htmlTds + "</tr>");
        });
        SREresetRowAll(TableID);

        var $MainTR = $(Wrapper).find("thead:eq(0) tr[data-id=0]");
        var $FDLTR = $(".DTFC_LeftHeadWrapper", Wrapper).find("thead tr[data-id=0]");
        var $FDRTR = $(".DTFC_RightHeadWrapper", Wrapper).find("thead tr[data-id=0]");
        sreselectedheight = $(Wrapper).data("sreselectedheight");

        $MainTR.css("height", sreselectedheight);
        $FDLTR.css("height", sreselectedheight);
        $FDRTR.css("height", sreselectedheight);
        $MainTR.find('td.sorting').removeClass('sorting');
        $FDLTR.find('td.sorting').removeClass('sorting');
        $FDRTR.find('td.sorting').removeClass('sorting');

        $.ajax({
            'dataType': 'json',
            'type': 'POST',
            'url': WebsiteURL + $(Wrapper).data("sreurl"),
            'data': { id: $MainTR.data("id") },
            beforeSend: function () {
                showLoading(true);
            },
            complete: function () {
                showLoading(false);
            },
            'async': true,
            'success': function (data) {
                var LenFCL = $($FDLTR).find('td').length;
                var LenFCR = $($FDRTR).find('td').length;
                var LenAll = data.aaData[0].length;
                var oDTTable = $('#' + TableID).DataTable();
                if (!(data.aaData[0] instanceof Array)) {
                    LenAll = $('#' + TableID).DataTable().columns()[0].length; //Object.keys(data.aaData[0]).length;                    
                    for (var i = 0; i < LenAll; i++) {

                        var colname = $('#' + TableID).DataTable().columns().settings()[0].aoColumns[i].mData;
                        var rendercallback = $('#' + TableID).DataTable().columns().settings()[0].aoColumns[i].rendercallback;
                        var editrendercallback = $('#' + TableID).DataTable().columns().settings()[0].aoColumns[i].editrendercallback;
                        var datacolname = $('#' + TableID).DataTable().columns().settings()[0].aoColumns[i].data;
                        if (data.aaData[0]["" + colname + ""] == SRE_CMD_Avoid)
                            return;
                        var td = $($MainTR).find('td:eq(' + i + ')');
                        if (rendercallback != null && rendercallback != undefined && rendercallback != "undefined") {
                            var isSameFunction = false;
                            if (editrendercallback != null && editrendercallback != undefined && editrendercallback != "undefined" && rendercallback == editrendercallback) {
                                isSameFunction = true;
                            }
                            var fn = window[rendercallback];
                            if (typeof fn === "function") {
                                var actions = "";
                                if (isSameFunction) {
                                    // First parameter : Data   Send paramter : iseditmode or not
                                    actions = eval(rendercallback)(data.aaData[0], false);
                                }
                                else {
                                    actions = eval(rendercallback)(data.aaData[0]);
                                }
                                $($MainTR).find('td:eq(' + i + ')').html(actions);
                            } else {
                                console.log(rendercallback + " function does not exist.")
                            }
                        }
                        else if (datacolname == null || datacolname == undefined) {
                            $($MainTR).find('td:eq(' + i + ')').html("");;
                        }
                        else {
                            $($MainTR).find('td:eq(' + i + ')').html(data.aaData[0]["" + colname + ""]);
                        }
                        if (LenFCL > i) {
                            $($FDLTR).find('td:eq(' + i + ')').html(td.html());
                            td.html('');
                        }
                        else if (LenAll - LenFCR <= i) {
                            $($FDRTR).find('td:eq(' + (LenAll - LenFCR - i) + ')').html(td.html());
                            td.html('');
                        }
                    }
                }
                else {
                    $.each(data.aaData[0], function (idx, item) {
                        if (item == SRE_CMD_Avoid)
                            return;
                        if (LenFCL > idx)
                            $($FDLTR).find('td:eq(' + idx + ')').html(item);
                        else if (LenAll - LenFCR <= idx)
                            $($FDRTR).find('td:eq(' + (LenAll - LenFCR - idx) + ')').html(item);
                        else
                            $($MainTR).find('td:eq(' + idx + ')').html(item);
                    });
                }
                if (typeof (window[$(Wrapper).data("srecallback")]) == "function")
                    window[$(Wrapper).data("srecallback")]($MainTR.data("id"));

                $('#' + TableID).DataTable().columns.adjust();
            }
        });
        //SRELoadEditableRow(table,  $('#' + table + '_wrapper').find("thead").find("tr:last")[0])
    }
    else {
        $(obj).html('<i class="fa fa-plus"></i> New');
        $(Wrapper).find("thead").find("tr[data-id=0]").remove();
        $('#' + TableID).DataTable().columns.adjust();
    }
}
//------------------------------------------------------------//