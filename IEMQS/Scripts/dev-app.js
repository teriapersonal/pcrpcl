﻿var draggedHtml = "";
var eleId = 1;
var btnEleID = 1;
var elements = {};
var selectedElement;
var selectedElementType;
var selectedButton;
var data = [{ Value: "1", Text: "Option1" }, { Value: "2", Text: "Option2" }, { Value: "3", Text: "Option3" }];

var jsonData = [];

function AddElement(type, IsNewBtn) {
    var properties = {
        textbox: { Id: "textbox" + eleId, labelText: "Untitled", labelId: "label" + eleId, defaultValue: "", element: "textbox", placeholder: "", type: "text", maxlength: "", colname: "", required: false, disabled: false },
        textarea: { Id: "textarea" + eleId, labelText: "Untitled", labelId: "label" + eleId, defaultValue: "", element: "textbox", placeholder: "", maxlength: "", colname: "", required: false, disabled: false, rows: "" },
        button: { Id: "button" + btnEleID, labelText: "Button" + btnEleID, type: "BUTTON", isNewWindows: false, targetUrl: "", disabled: false },
        autocomplete: { Id: "autocomplete" + eleId, labelText: "Untitled", labelId: "label" + eleId, defaultValue: "", element: "autocomplete", placeholder: "", maxlength: "", colname: "", required: false, disabled: false, data: data },
    };

    if (IsNewBtn) {
        elements[selectedElement].push(properties[type]);
    }
    else {
        elements["div" + eleId] = [properties[type]];
        eleId++;
    }
    if (type == "button")
        btnEleID++;
}

var ElementType = {
    textbox: {
        value: 0, name: "textbox", prefix: "txt"
    },
    textarea: { value: 1, name: "textarea", prefix: "txt" },
    button: { value: 2, name: "button", prefix: "btn" },
    checkbox: { value: 2, name: "checkbox", prefix: "chk" },
    autocomplete: {
        value: 2, name: "autocomplete", prefix: "txt"
    },
    dropdown: { value: 2, name: "dropdown", prefix: "btn" },
    div: { value: 2, name: "div", prefix: "dv" },
    headerwithstatus: { value: 2, name: "HeaderWithStatus", prefix: "hws" },
    tabs: { value: 2, name: "tabs", prefix: "tbs" }
};

var ElementProperties = {
    textbox: {
        labelText: {
            name: "labelText"
        },
        defaultValue: {
            name: "defaultValue"
        },
        placeholder: {
            name: "placeholder"
        },
        type: {
            name: "type"
        },
        maxlength: {
            name: "maxlength"
        },
        colname: {
            name: "colname"
        },
        required: {
            name: "required"
        },
        disabled: {
            name: "disabled"
        },
        isdate: {
            name: "isdate"
        }
    },
    textarea: {
        labelText: { name: "labelText" },
        defaultValue: {
            name: "defaultValue"
        },
        placeholder: {
            name: "placeholder"
        },
        maxlength: {
            name: "maxlength"
        },
        colname: {
            name: "colname"
        },
        required: {
            name: "required"
        },
        disabled: {
            name: "disabled"
        },
        rows: {
            name: "rows"
        }
    },
    button: {
        labelText: {
            name: "labelText"
        },
        type: {
            name: "type"
        },
        isNewWindows: {
            name: "isNewWindows"
        },
        targetUrl: {
            name: "targetUrl"
        },
        disabled: {
            name: "disabled"
        }
    },
}

//$('body').on('click', function (e) {
//    //only buttons
//    if ($(e.target).data('toggle') !== 'popover'
//        && $(e.target).parents('.popover.in').length === 0) {
//        closePopover();
//    }
//});

$(function () {
    $(".clsdraggable").draggable({
        revert: "valid",
        cursor: "move",
        helper: 'clone',
        start: function (event, ui) {
        },
        drag: function (event, ui) {

        },
        stop: function (event, ui) {

        }
    });
    CreateDroppable();
    $('body').on('focus', ".date-picker", function () {
        $(this).datepicker({
            orientation: "auto", autoclose: true
        });
    });
    $('body').on('keypress', ".numeric-only", function () {
        $(this).autoNumeric('init');
    });
    $('body').on('keypress', ".Db-Colname", function (e) {
        if (e.which === 32)
            return false;
    });
});

(function (a) {
    a.fn.replaceTagName = function (f) {
        var g = [],
            h = this.length;
        while (h--) {
            var k = document.createElement(f),
                b = this[h],
                d = b.attributes;
            for (var c = d.length - 1; c >= 0; c--) {
                var j = d[c];
                k.setAttribute(j.name, j.value)
            }
            k.innerHTML = b.innerHTML;
            a(b).after(k).remove();
            g[h - 1] = k
        }
        return a(g)
    }
})(window.jQuery);

function CreateDraggable() {
    $(".childdraggable").draggable();
}

function CreateDroppable() {
    $("#droppable,.droppable").droppable({
        helper: 'clone',
        greedy: true,
        tolerance: "pointer",
        drop: function (event, ui) {            
            var htmlAppend = '';
            if ($("#hdnElementID").val() != '' && $("#hdnElementID").val() != null && $("#hdnElementID").val() != undefined) {
                eleId = parseInt($("#hdnElementID").val()) + 1;
                $("#hdnElementID").val(eleId);
            }
            else {
                $("#hdnElementID").val(eleId);
            }

            var type = ui.draggable.attr('data-type');
            if (type != null && type != undefined && type != "") {
                var colSize = ui.draggable.attr('data-colSize');
                htmlAppend += CreateDynamicElement(type, colSize);
            }
            else {
                htmlAppend = ui.draggable.context.outerHTML;
                $(ui.draggable.remove());
            }
            //console.log(htmlAppend);
            var $element = $(htmlAppend);


            $(this).append($element);
            $element.removeAttr('style');

            CreateDroppable();
            CreateDraggable();
            MakeControlSelectable();

            //if (type == "autocomplete") {
            //    EnabledAutoComplete("txt" + eleId, "datatxt" + eleId, "hdntxt" + eleId);
            //}

            $(".droppable").sortable();
            $(".droppable").disableSelection();
            
            $('input:text').attr("autocomplete", "off")
            if ($(event.target).hasClass("droppabletabs")) {
                var TotalHeight = 0;
                $(event.target).find('.childdraggable').each(function () { TotalHeight += parseFloat($(this).outerHeight()); });
                console.log(TotalHeight);
                $(event.target).height(TotalHeight+50);
            }           
            AddElement(type);
        },
        over: function (event, ui) {
            //console.log(ui);
        }
    });
}

function MakeControlSelectable() {
    $('.childdraggable,.childdroppable,.droppable').click(function (e) {
        //console.log($(this));
        $('.childdraggable,.childdroppable,.droppable').removeClass('hasFocus');
        $(this).addClass('hasFocus');
        e.stopPropagation();
    });

    $('.childdraggable').hover(function () {
        $(this).addClass('hoverComponent');
    }, function () {
        $(this).removeClass('hoverComponent');
    });
}

function appendTextBox() {
    var htmlAppend = '<div id="div' + eleId + '" class="col-md-12 childdraggable" data-elemType="textbox" onclick="ApplyPropertiesDialog(this);" data-toggle="popover">' +
                '<label class="control-label" id="label' + eleId + '">Untitled</label>' +
                '<div class="">' +
                '<input type="text" id="textbox' + eleId + '" value="" name="txt' + eleId + '" class="form-control control"/>' +
                '</div></div>'
    return htmlAppend;
}

function appendTextArea() {
    var htmlAppend = '<div  id="div' + eleId + '" class="col-md-12 childdraggable" data-elemType="textarea" onclick="ApplyPropertiesDialog(this);" data-toggle="popover">' +
                '<label class="control-label" id="label' + eleId + '">Untitled</label>' +
                '<div class="">' +
                '<textarea id="textarea' + eleId + '" class="form-control control" id="txt' + eleId + '" value="" name="txt' + eleId + '" rows="2"></textarea>' +
                '</div></div>';
    return htmlAppend;
}

function appendButton() {
    var htmlAppend = '<div id="div' + eleId + '" class="col-md-12 childdraggable" id="button' + eleId + '" onclick="ApplyPropertiesDialog(this);" data-toggle="popover" data-elemType="button">' +
                    '<button class="btn btn-primary control" id="button' + btnEleID + '" onclick="SelectButton(this);">Button' + btnEleID + '<i class="glyphicon glyphicon-remove btnRemove" onclick="btnRemove(this)"></i></button>' +
                    '</div>';
    return htmlAppend;
}

function appendNewButton() {
    var btn = '\t<button class="btn btn-primary control" id="button' + btnEleID + '" onclick="SelectButton(this);">Button' + btnEleID + '<i class="glyphicon glyphicon-remove btnRemove" onclick="btnRemove(this)"></i></button>';
    $("#" + selectedElement).append(btn);
    AddElement("button", true);
}

function appendCheckbox() {
    var htmlAppend = '<div id="div' + eleId + '" class="col-md-12 childdraggable" data-elemType="checkbox">'
                    + '<label class="control-label">Untitled</label>'
                    + '<div class="">'
                    + '<label class="mt-checkbox mt-checkbox-outline" for="chk' + eleId + '">'
                    + '<input class="clsReadOnly" id="chk' + eleId + '" name="chk' + eleId + '" value="true" readonly="readonly" type="checkbox">'
                    + '<input name="hdnchk' + eleId + '" value="false" type="hidden">'
                    + '<span></span>'
                    + '</label></div></div>';
    return htmlAppend;
}

function appendAutoComplete() {
    var htmlAppend = '<div id="div' + eleId + '" class="col-md-12 childdraggable" data-elemType="autocomplete" onclick="ApplyPropertiesDialog(this);" data-toggle="popover">' +
                '<label class="control-label" id="label' + eleId + '">Untitled</label>' +
                '<div class="">' +
                '<input type="text" id="autocomplete' + eleId + '" value="" name="txt' + eleId + '" class="form-control" />' +
                '<input id="hdnautocomplete' + eleId + '" name="hdnautocomplete' + eleId + '" value="" type="hidden">' +
                '</div>';
    htmlAppend += "</div>";
    jsonData.push({ key: "tbltxt" + eleId, data: data });
    var Script = "";
    Script += "<script>";
    Script += "FillAutoCompleteDynamic($('#autocomplete" + eleId + "'), " + JSON.stringify(data) + ", $('#hdnautocomplete" + eleId + "'));";
    Script += "<\/script>";
    htmlAppend += Script;
    return htmlAppend;
}

function SelectButton(e) {
    selectedButton = $(e).attr("id");
    setPropertiesDialog($(e).parent());
}

function AddNewTr(e) {
    var cur = $("#hdnSelected").val();
    var table = "tbl" + cur;
    var cnt = $("#" + table + " tr").length;
    var tr = '<tr><td><input type="text" value="' + cnt + '" class="form-control" /></td><td><input type="text" value="Option' + cnt + '" class="form-control" /></td><td><a class="glyphicon glyphicon-remove deleteRow" aria-hidden="true" onclick="deleteRow(this)"></a></td></tr>';
    $('#' + table).append(tr);
    var json = tableToJson(table);

    DestroyAutoComplete($("#" + cur));
    FillAutoCompleteDynamic($("#" + cur), json, $("#hdn" + cur), changeDefaultValue);
    FillAutoCompleteDynamic($('#' + table).find("#txtAutoCompleteDefaultValue"), json, $('#' + table).find("#hdntxtAutoCompleteDefaultValue"));
    UpdateJson(jsonData, json, table);
}

function deleteRow(e) {
    var table = $(e).closest("table").attr("id");
    var rowCnt = $("#" + table + " tbody tr").length;
    if (rowCnt > 1)
        $(e).closest("tr").remove();
    var json = tableToJson(table);
    var cur = $("#hdnSelected").val();
    FillAutoCompleteDynamic($("#" + cur), json, $("#hdn" + cur), changeDefaultValue);
    FillAutoCompleteDynamic($('#' + table).find("#txtAutoCompleteDefaultValue"), json, $('#' + table).find("#hdntxtAutoCompleteDefaultValue"));
    UpdateJson(jsonData, json, table);
}

function UpdateJson(json, newJson, table) {
    jsonToTable(newJson, table);
    var newJsondata = [];
    $.each(jsonData, function (i, item) {
        if (item.key == table)
            newJsondata.push({
                key: item.key, data: newJson
            });
        else
            newJsondata.push({
                key: item.key, data: item.data
            });
    })
    jsonData = newJsondata;
}

function Onkeyup(e) {
    var table = $(e).closest("table").attr("id");
    var json = tableToJson(table);
    var cur = $("#hdnSelected").val();
    FillAutoCompleteDynamic($("#" + cur), json, $("#hdn" + cur), changeDefaultValue);
    FillAutoCompleteDynamic($('#' + table).find("#txtAutoCompleteDefaultValue"), json, $('#' + table).find("#hdntxtAutoCompleteDefaultValue"));
    UpdateJson(jsonData, json, table);
}

function tableToJson(table) {
    var jsonData = [];
    $("#" + table + " tr").each(function (i) {
        if (i == 0) return;
        var id = $.trim($(this).find("input").eq(0).val());
        var text = $.trim($(this).find("input").eq(1).val());
        jsonData.push({ Value: id, Text: text });
    });
    return jsonData;
}

function jsonToTable(json, table) {
    var tr;
    $('#' + table + " tbody").empty();
    for (var i = 0; i < json.length; i++) {
        tr = $('<tr/>');
        tr.append("<td> <input type='text' value=" + json[i].Value + " class='form-control' onkeyup='Onkeyup(this)'/></td>");
        tr.append("<td> <input type='text' value=" + json[i].Text + " class='form-control' onkeyup='Onkeyup(this)'/></td>");
        tr.append("<td><a class='glyphicon glyphicon-remove' aria-hidden='true' onclick='deleteRow(this)'></a></td>");
        $('#' + table).append(tr);
    }
}

function getRowByKey(json, key) {
    var data = [];
    $.each(json, function (i, row) {
        if (row.Id == key) {
            data = row;
        }
    })
    return data;
}

function getDataByKey(json, table) {
    var data;
    $.each(json, function (i, row) {
        if (row.key == table) {
            data = row;
        }
    })
    return data;
}

function appendDropdown() {
    var htmlAppend = '<div class="col-md-12 childdraggable" data-elemType="dropdown">' +
                '<label class="control-label" id="lbl' + eleId + '">Untitled</label>' +
                '<div class="">' +
                '<select id="ddl' + eleId + '" name="ddl' + eleId + '" class="form-control">' +
                '<option value="">Select</option>';
    for (var i = 0; i < data.length; i++) {
        htmlAppend += "<option value='" + data[i].Value + "'>" + data[i].Text + "</option>";
    }
    htmlAppend += '</select></div></div>';
    return htmlAppend;
}

function btnRemove(e) {
    $(e).parent().remove();
    var btn = $(e).parent().attr("id");
    $.each(elements[selectedElement], function (i, row) {
        if (row.Id == btn) {
            elements[selectedElement].splice(i, 1);
            return false;
        }
    })
}

function GetColumnHtml(noOfCols) {
    var colSize = 12 / noOfCols;
    var htmlAppend = '<div class="row childdroppable" id="dv' + eleId + '">';
    for (var i = 1; i <= noOfCols; i++) {
        //htmlAppend += '<div class="col-md-' + colSize + ' clsContainer droppable"  onmouseover="fnmouseover(event,this)" onmouseout="fnmouseDown(this)"></div>'
        htmlAppend += '<div class="col-md-' + colSize + ' clsContainer droppable" data-colSize="' + colSize + '" data-elemType="div"></div>'
    }
    htmlAppend += '</div>';
    return htmlAppend;
}

function CreateDynamicElement(type, noOfCols) {
    if (type == "textbox") {
        return appendTextBox();
    }
    else if (type == "textarea") {
        return appendTextArea();
    }
    else if (type == "button") {
        return appendButton();
    }
    else if (type == "checkbox") {
        return appendCheckbox();
    }
    else if (type == "dropdown") {
        return appendDropdown();
    }
    else if (type == "autocomplete") {
        return appendAutoComplete();
    }
    else if (type == "column") {
        return GetColumnHtml(noOfCols)
    }
    else if (type == "HeaderWithStatus") {
        return CreatePageHeaderWithStatus()
    }
    else if (type == "tabs") {
        return CreateTabs()
    }
}

function FillAutoCompleteDynamic(element, sourceList, hdElement, fnCallback) {
    if (element.hasClass('ui-autocomplete')) {
        element.autocomplete('destroy')
    }
    element.autocomplete({
        source: function (request, response) {
            var array = $.map(sourceList, function (item) {
                return {
                    label: item.Text,
                    value: item.Text,
                    id: item.Value
                }
            });
            response($.ui.autocomplete.filter(array, request.term));
        },
        autoFocus: true,
        minLength: 0,
        select: function (event, ui) {
            $(this).val(ui.item.label);
            $(hdElement).val(ui.item.id);
            //HighlightInValidControls();
            if (fnCallback != null && fnCallback != undefined && fnCallback != 'undefined') {
                fnCallback();
            }
        },
        change: function (event, ui) {
            if (ui.item == null) {
                element.val('');
                element.focus();
                $(hdElement).val('');
            }
        }
    }).focus(function () {
        $(this).data("uiAutocomplete").search($(this).val());
        $(this).autocomplete("search");
    });
}

function changeDefaultValue() {
    var defaultText = $("#txtAutoCompleteDefaultValue").val();
    var defaultValue = $("#hdntxtAutoCompleteDefaultValue").val();
    var cur = $("#hdnSelected").val();
    $("#" + cur).val(defaultText);
    $("#hdn" + cur).val(defaultValue);
}

$('#removeMenu').click(function () {
    var $removeelem = $(".hasFocus", "#droppable");
    var type = $removeelem.attr("data-elemType");
    var newColSize;
    if (type == "div") {
        var currentSize = parseInt($removeelem.attr("data-colSize"));
        var $Elem;
        if ($removeelem.next("[data-elemType='div']").length > 0) {
            var $Elem = $removeelem.next("[data-elemType='div']");
        }
        else if ($removeelem.prev("[data-elemType='div']").length > 0) {
            var $Elem = $removeelem.prev("[data-elemType='div']");
        }
    }

    $removeelem.fadeOut(700, function () {
        $(this).remove();
        if ($Elem != null && $Elem != undefined && $Elem.length > 0) {
            var elemColSize = $Elem.attr("data-colSize");
            if ($Elem.hasClass("col-md-" + elemColSize)) {
                $Elem.removeClass("col-md-" + elemColSize);
                newColSize = parseInt(elemColSize) + currentSize;
                $Elem.attr("data-colSize", newColSize);
                $Elem.addClass("col-md-" + newColSize);
            }
        }
    });
});

function setPropertiesDialog(e) {
    selectedElement = $(e).attr("id");
    selectedElementType = $(e).attr("data-elemType");
    var type = $(e).attr("data-elemType");
    var id = $(e).attr('id');
    if (id != null)
        var index = id.replace(type, "")

    var propertyData = elements[selectedElement];
    var popoverId = $(e).attr("aria-describedby");
    var $popoverElement = $("#" + popoverId);

    //var labelId = $this.find("label").attr('id');
    //if (type == ElementType.textbox.name)
    //    var id = $this.find("input").attr('id');
    //else if (type == ElementType.textarea.name)
    //    var id = $this.find("textarea").attr('id');
    //else if (type == ElementType.button.name)
    //    var id = $this.attr('id');
    //else if (type == ElementType.autocomplete.name)
    //    var id = $this.find("input").attr('id');

    //var tagName = $("#" + id).prop("tagName");
    //$("#hdnSelected").val(id);

    //var defaultText = $("#" + id).val();
    //var placeholder = $("#" + id).attr("placeholder");
    //var textboxType = "";
    //if ($("#" + id).hasClass("numeric-only"))
    //    textboxType = "number";
    //else if ($("#" + id).hasClass("date-picker"))
    //    textboxType = "date";
    //else
    //    textboxType = "text";
    //var maxlength = $("#" + id).attr("maxlength");
    //var rows = $("#" + id).attr("rows");
    //var colname = $("#" + id).attr("colname");
    //var required = $("#" + id).prop('required');
    //var disabled = $("#" + id).attr("disabled") == "disabled" ? true : false;

    if (type == ElementType.textbox.name) {
        $popoverElement.find("#txtTextboxLabel").val(propertyData.labelText);
        $popoverElement.find("#txtTextboxDefaultValue").val(propertyData.defaultValue);
        $popoverElement.find("#txtTextboxPlaceholder").val(propertyData.placeholder);
        $popoverElement.find("#txtTextboxInputType").val(propertyData.type);
        $popoverElement.find("#txtTextboxMaxLength").val(propertyData.maxlength);
        $popoverElement.find("#txtTextboxDBColumnName").val(propertyData.colname);
        $popoverElement.find("#chkTextboxIsRequired").prop("checked", propertyData.required);
        $popoverElement.find("#chkTextboxIsDisabled").prop("checked", propertyData.disabled);
    }
    else if (type == ElementType.textarea.name) {
        $popoverElement.find("#txtTextareaLabel").val(propertyData.labelText);
        $popoverElement.find("#txtTextareaDefaultValue").val(propertyData.defaultText);
        $popoverElement.find("#txtTextareaPlaceholder").val(propertyData.placeholder);
        $popoverElement.find("#txtTextareaMaxLength").val(propertyData.maxlength);
        $popoverElement.find("#txtTextareaRows").val(propertyData.rows);
        $popoverElement.find("#txtTextareaDBColumnName").val(propertyData.colname);
        $popoverElement.find("#chkTextareaIsRequired").prop("checked", propertyData.required);
        $popoverElement.find("#chkTextareaIsDisabled").prop("checked", propertyData.disabled);
    }
    else if (type == ElementType.button.name) {
        if (selectedButton == null || selectedButton == undefined)
            selectedButton = $("#" + selectedElement).find(".control:first").attr("id");
        propertyData = getRowByKey(elements[selectedElement], selectedButton);
        $popoverElement.find("#txtButtonLabel").val(propertyData.labelText);
        $popoverElement.find("#txtButtonDefaultValue").val(propertyData.type);
        $popoverElement.find("#chkButtonIsDisabled").attr("checked", propertyData.disabled);
        $popoverElement.find("#txtButtonTargetUrl").val(propertyData.targetUrl);
        if (propertyData.type == "A")
            $popoverElement.find("#chkButtonIsNewWindow").prop("disabled", false);
        $popoverElement.find("#chkButtonIsNewWindow").prop("checked", propertyData.isNewWindows);

    }
    else if (type == ElementType.autocomplete.name) {
        $popoverElement.find('table').attr("id", "tbl" + id)
        var table = $popoverElement.find('table').attr("id");
        $popoverElement.find("#txtAutoCompleteLabel").val(propertyData.labelText);
        $popoverElement.find("[name='txtAutoCompleteL']").val(propertyData.defaultText);
        $popoverElement.find("#txtAutoCompletePlaceholder").val(propertyData.placeholder);
        $popoverElement.find("#txtAutoCompleteMaxLength").val(propertyData.maxlength);
        $popoverElement.find("#txtAutoCompleteDBColumnName").val(propertyData.colname);
        $popoverElement.find("#chkAutoCompleteIsRequired").prop("checked", propertyData.required);
        $popoverElement.find("#chkAutoCompleteIsDisabled").prop("checked", propertyData.disabled);
        var json = getDataByKey(jsonData, table);
        jsonToTable(json, table);
        FillAutoCompleteDynamic($popoverElement.find("#txtAutoCompleteDefaultValue"), json, $popoverElement.find("#hdntxtAutoCompleteDefaultValue"), changeDefaultValue);
        //var defaultValue = $("#hdn" + id).val();
        $popoverElement.find("#txtAutoCompleteDefaultValue").val(propertyData.defaultText);
        $popoverElement.find("#hdntxtAutoCompleteDefaultValue").val(propertyData.defaultValue)
    }
}

function ApplyPropertiesDialog(e) {
    var type = $(e).attr("data-elemType");
    $(".popover").remove();
    if (type == ElementType.textbox.name) {
        $("[data-elemType='" + ElementType.textbox.name + "']").popover({
            trigger: 'manual',
            html: true,
            placement: "bottom",
            content: function () {
                return $('#dvTextboxProperties').html();
            }
        }).on("click", function (e) {
            if (e.target.tagName != "INPUT") {
                $(this).popover('show');
                setPropertiesDialog(this);
                $("[data-toggle='popover']").not(this).popover('hide');
            }
        })
    }
    else if (type == ElementType.textarea.name) {
        $("[data-elemType='" + ElementType.textarea.name + "']").popover({
            trigger: 'manual',
            html: true,
            placement: "bottom",
            content: function () {
                return $('#dvTextareaProperties').html();
            }
        }).on("click", function (e) {
            if (e.target.tagName != "TEXTAREA") {
                $(this).popover('show');
                setPropertiesDialog(this)
                $("[data-toggle='popover']").not(this).popover('hide');
            }
        })
    }
    else if (type == ElementType.button.name) {
        $("[data-elemType='" + ElementType.button.name + "']").popover({
            trigger: 'manual',
            html: true,
            placement: "bottom",
            content: function () {
                return $('#dvButtonProperties').html();
            }
        }).on("click", function () {
            $(this).popover('show');
            setPropertiesDialog(this);
            $("[data-toggle='popover']").not(this).popover('hide');
        })
    }
    else if (type == ElementType.autocomplete.name) {
        $("[data-elemType='" + ElementType.autocomplete.name + "']").popover({
            trigger: 'manual',
            html: true,
            placement: "bottom",
            content: function () {
                return $('#dvAutoCompleteProperties').html();
            }
        }).on("click", function (e) {
            if (e.target.tagName != "INPUT") {
                $(this).popover('show');
                setPropertiesDialog(this);
                $("[data-toggle='popover']").not(this).popover('hide');
            }
        })
    }
    else if (type == ElementType.tabs.name) {
        $("[data-elemType='" + ElementType.tabs.name + "']").on("click", ".tabbable-line", function (event) {
            if (!$(event.target).is('a')) {
                if (!$(this).data("bs.popover") || !$(this).attr('data-popoverAttached')) {
                    $(this).popover('destroy').popover({
                        placement: 'bottom',
                        trigger: 'manual',
                        html: true,
                        content: function () {
                            return $('#dvTabsProperties').html();
                        }
                    });
                    $(this).attr('data-popoverAttached', true);
                }
                $(this).popover('show').on('shown.bs.popover', function () {
                    SetTabNames($(this));
                });
            }
        });
    }
    else if (type == ElementType.headerwithstatus.name) {
        $("[data-elemType='" + ElementType.headerwithstatus.name + "']").on("click", "", function (event) {
            var title = $(this).find(".page-title").html();
            var Status = $(this).find("#spnStatusLabel").html();
            if (!$(this).data("bs.popover") || !$(this).attr('data-popoverAttached')) {
                $(this).popover('destroy').popover({
                    placement: 'bottom',
                    trigger: 'manual',
                    html: true,
                    content: function () {
                        return $('#dvHeaderStatusProperties').html();
                    }
                });
                $(this).attr('data-popoverAttached', true);
            }
            $(this).popover('show').on('shown.bs.popover', function () {
                var popid = $(this).attr("aria-describedby");
                $("#" + popid).find("#txtHeaderText").val(title);
                $("#" + popid).find("#txtStatusText").val(Status);
            });
        });
    }    
}

function UpdateJsonByKey(json, key, value) {
    var newJson = [];
    $.each(json, function (i, row) {
        if (row[selectedElement]) {
            row[selectedElement][key] = value;
            newJson.push(row)
        }
        else {
            newJson.push(row);
        }
    });
    elements = newJson;
}

function changeTextBoxAttribute(e) {
    var target = $(e).attr("target");
    var labelId = $("#" + selectedElement).find("label").attr("id");
    var id = $("#" + selectedElement).find(".control").attr("id");
    var value = $(e).val();
    var isChecked = $(e).prop("checked");
    if (target == ElementProperties.textbox.labelText.name)
        $("#" + labelId).html(value);
    else if (target == ElementProperties.textbox.defaultValue.name)
        $("#" + id).val(e.value);
    else if (target == ElementProperties.textbox.placeholder.name)
        $("#" + id).attr("placeholder", e.value);
    else if (target == ElementProperties.textbox.type.name) {
        if (e.value == "number") {
            $("#" + id).addClass("numeric-only").removeClass("date-picker");
            $("#txtTextboxDefaultValue").addClass("numeric-only").removeClass("date-picker");
        }
        else if (e.value == "date") {
            $("#" + id).addClass("date-picker").removeClass("numeric-only");
            $("#txtTextboxDefaultValue").addClass("date-picker").removeClass("numeric-only");
        }
        else {
            $("#" + id).removeClass("numeric-only").removeClass("date-picker");
            $("#txtTextboxDefaultValue").removeClass("numeric-only").removeClass("date-picker");
        }
    }
    else if (target == ElementProperties.textbox.maxlength.name) {
        $("#" + id).attr("maxlength", e.value);
        $("#txtTexboxDefaultValue").attr("maxlength", e.value);
    }
    else if (target == ElementProperties.textbox.colname.name)
        $("#" + id).attr("colname", e.value);
    else if (target == ElementProperties.textbox.required.name) {
        value = isChecked;
        if (isChecked)
            $("#" + labelId).append('<span class="required" aria-required="true"> *</span>');
        else
            $("#" + labelId).find("span").remove();
        $("#" + id).attr("required", isChecked);
    }
    else if (target == ElementProperties.textbox.disabled.name) {
        value = isChecked;
        $("#" + id).attr("disabled", $(e).prop("checked"));
    }
    UpdateJsonByKey(elements, target, value);
}

function changeTextareaAttribute(e) {
    var target = $(e).attr("target");
    var labelId = $("#" + selectedElement).find("label").attr("id");
    var id = $("#" + selectedElement).find(".control").attr("id");
    var value = $(e).val();
    var isChecked = $(e).prop("checked");

    if (target == ElementProperties.textarea.labelText.name)
        $("#" + labelId).text(e.value);
    else if (target == ElementProperties.textarea.defaultValue.name)
        $("#" + id).val(e.value);
    else if (target == ElementProperties.textarea.placeholder.name)
        $("#" + id).attr("placeholder", e.value);
    else if (target == ElementProperties.textarea.maxlength.name) {
        $("#" + id).attr("maxlength", e.value);
        $("#txtTexareaDefaultValue").attr("maxlength", e.value);
    }
    else if (target == ElementProperties.textarea.rows.name) {
        $("#" + id).attr("rows", e.value);
        $("#txtTextareaRows").attr("rows", e.value);
    }
    else if (target == ElementProperties.textarea.colname.name)
        $("#" + id).attr("colname", e.value);
    else if (target == ElementProperties.textarea.required.name) {
        value = isChecked;
        if (isChecked)
            $("#" + labelId).append('<span class="required" aria-required="true"> *</span>');
        else
            $("#" + labelId).find("span").remove();
        $("#" + id).attr("required", isChecked);
    }
    else if (target == ElementProperties.textarea.disabled.name) {
        value = isChecked;
        $("#" + id).attr("disabled", $(e).prop("checked"));
    }
    UpdateJsonByKey(elements, target, value);
}

function changeButtonAttribute(e) {
    var target = $(e).attr("target");
    var id = $("#" + selectedElement).find(".control").attr("id");
    var lblTxt = $("#" + id).prop("tagName") == "INPUT" ? $("#" + id).val() : $("#" + id).text();
    var value = $(e).val();
    var isChecked = $(e).prop("checked");
    if (target == ElementProperties.button.labelText.name)
        $("#" + id).text(e.value);
    else if (target == ElementProperties.button.type.name) {
        if (e.value == "A") {
            $("#chkButtonIsNewWindow").prop("disabled", false)
            $("#" + id).replaceTagName('a')
        }
        else {
            $("#chkButtonIsNewWindow").prop("disabled", true);
            $("#chkButtonIsNewWindow").prop("checked", false);
            $("#txtButtonTargetUrl").val("");
            $("#" + id).replaceTagName('button')
        }
    }
    else if (target == ElementProperties.button.isNewWindows.name) {
        value = isChecked;
        var isDisabled = $(e).prop("checked");
        $("#txtButtonTargetUrl").val("");
        $("#txtButtonTargetUrl").prop("disabled", !isDisabled)
        $("#" + id).attr("target", "_blank");
    }
    else if (target == ElementProperties.button.targetUrl.name) {
        $("#" + id).attr("href", "http://" + e.value);
    }
    else if (target == ElementProperties.button.disabled.name) {
        value = isChecked;
        $("#" + id).attr("disabled", $(e).prop("checked"));
    }
    UpdateJsonByKey(elements, target, value);
}

function changeAutoCompleteAttribute(e) {
    var target = $(e).attr("target");
    var id = $("#hdnSelected").val();
    var labelId = id.replace("txt", "");
    if (target == ElementProperties.textbox.label.name)
        $("#lbl" + labelId).html(e.value);
        //else if (target == ElementProperties.textbox.defaultValue.name) {
        //    $("#" + id).val(e.value);
        //}
    else if (target == ElementProperties.textbox.placeholder.name)
        $("#" + id).attr("placeholder", e.value);
    else if (target == ElementProperties.textbox.maxlength.name) {
        $("#" + id).attr("maxlength", e.value);
        $("#txtAutoCompleteDefaultValue").attr("maxlength", e.value);
    }
    else if (target == ElementProperties.textbox.colname.name)
        $("#" + id).attr("colname", e.value);
    else if (target == ElementProperties.textbox.required.name) {
        var isChecked = $(e).prop("checked");
        if (isChecked)
            $("#lbl" + labelId).append('<span class="required" aria-required="true"> *</span>');
        else
            $("#lbl" + labelId).find("span").remove();
        $("#" + id).attr("required", isChecked);
    }
    else if (target == ElementProperties.textbox.disabled.name)
        $("#" + id).attr("disabled", $(e).prop("checked"));
}

function closePopover() {
    $("[data-toggle='popover']").popover('hide');
}

//#region Header With Status Start
function CreatePageHeaderWithStatus() {
    var htmlAppend = "<div class=\"col-md-12 childdraggable\" data-elemType=\"HeaderWithStatus\" onclick=\"ApplyPropertiesDialog(this);\"><div class=\"mt-element-ribbon bg-grey-steel\" style=\"height:55px;margin-bottom:10px !important;\">" +
         "<div class=\"ribbon ribbon-left\" style=\"background-color:transparent !important;\">" +
         "    <span class=\"page-title\">Header Title</span>" +
         "</div>" +
         "        <div class=\"ribbon ribbon-right ribbon-color-warning\">" +
         "            <span style=\"font-weight:bolder\">" +
         "    <span id=\"spnStatusLabel\">Status</span> : <span id=\"spnStatus\"></span>" +
         "            </span>" +
         "        </div>" +
         "</div>";
    return htmlAppend;
}

function txtHeaderText_Change(txt) {
    var popid = GetPopOverId(txt);
    if ($(txt).hasClass("headertext")) {
        $("[aria-describedby='" + popid + "']").find(".page-title").html($(txt).val())
        $("[aria-describedby='" + popid + "']").attr("data-header", $(txt).val());
    }
    else if ($(txt).hasClass("statustext")) {
        $("[aria-describedby='" + popid + "']").find("#spnStatusLabel").html($(txt).val())
        $("[aria-describedby='" + popid + "']").attr("data-status", $(txt).val());
    }

}

function GetPopOverId(cnt) {
    var popid = $(cnt).closest(".popover").attr("id");
    return popid;
}

function GetPopOverControl(cnt) {
    var popover = $(cnt).closest(".popover");
    return popover;
}

function GetPopOverContainer(cnt) {
    var popid = GetPopOverId(cnt);
    var container = $("[aria-describedby='" + popid + "']");
    return container;
}

function ClosePopup(popdv) {
    debugger
    var popid = $(popdv).closest(".popover");
    $(popid).popover('destroy');
}
//#endregion Header With Status End

//#region Tabs Start
function CreateTabs() {
    var htmlAppend = "<div class=\"col-md-12 childdraggable\" data-elemType=\"tabs\">" +
                    "   <div class=\"portlet light\">" +
                    "       <div class=\"portlet-title tabbable-line\" data-elemType=\"tabs\" onclick=\"ApplyPropertiesDialog(this);\">" +
                    "           <ul class=\"nav nav-tabs\">";
    htmlAppend += CreateSingleTabli(1, true);
    htmlAppend += CreateSingleTabli(2);
    htmlAppend += "             </ul>" +
                    "       </div>" +
                    "       <div class=\"portlet-body\">" +
                    "           <div class=\"tab-content\">";
    htmlAppend += CreateSingleTabContent(1, true);
    htmlAppend += CreateSingleTabContent(2);
    htmlAppend += "             </div>" +
                    "       </div>" +
                    "   </div>" +
                    "</div>";
    //console.log(htmlAppend)
    return htmlAppend;
}

function SetTabNames(dv) {
    var popid = $(dv).attr("aria-describedby");
    var dymanictabs = $("#" + popid).find("#dymanictabs");
    dymanictabs.html("");
    $(dv).find('.nav-tabs li').each(function () {
        var tabid = $(this).find('a').attr("data-tabid");
        var tabname = $(this).find('a').text();
        var tab = DynamicTabNameTextbox(tabid, tabname);
        dymanictabs.append(tab);
    });
}

function CreateSingleTabli(id, isactive, name) {
    if (isactive === undefined || isactive == null) {
        isactive = false;
    }
    if (name === undefined || name == null) {
        name = "Tab" + id;
    }
    var htmlAppend = "<li class=\"" + (isactive ? "active" : "") + "\">" +
                    "   <a data-toggle=\"tab\" data-href=\"#portlet_tab" + id + "\" data-tabid=\"" + id + "\" aria-expanded=\"false\" onclick=\"ChangeTabs(this)\">" + name + "</a>" +
                    "</li>";
    return htmlAppend
}

function CreateSingleTabContent(id, isactive) {
    if (isactive === undefined || isactive == null) {
        isactive = false;
    }
    var htmlAppend = "<div class=\"tab-pane droppabletabs " + (isactive ? "active" : "") + "  clsContainer droppable\" id=\"portlet_tab" + id + "\"></div>";
    return htmlAppend
}

function ChangeTabs(a) {
    $(a).closest('.nav-tabs').find("li").removeClass("active");
    $(a).closest("li").addClass("active");
    $(a).closest('.portlet').find(".tab-content").find(".tab-pane").removeClass("active");
    $(a).closest('.portlet').find(".tab-content").find("[id='portlet_tab" + $(a).attr("data-tabid") + "']").addClass("active");
}

function AddNewTab(btn) {
    var txtnew = $(btn).closest("#dymanictabsAddNew").find(".newtabname");
    var tabname = txtnew.val();
    var $container = GetPopOverContainer(btn);
    var totalTabs = $container.find('.nav-tabs li').length;
    var li = CreateSingleTabli(totalTabs + 1, false, tabname);
    var content = CreateSingleTabContent(totalTabs + 1);
    $container.find('.nav-tabs').append(li);
    $container.closest("[data-elemtype='" + ElementType.tabs.name + "']").find('.tab-content').append(content);
    txtnew.val("");
    SetTabNames($container)
}

function DynamicTabNameTextbox(id, name) {
    var htmlAppend = "<div class=\"col-md-12 tabname\" id=\"tabName" + id + "\" data-tabid=\"" + id + "\">" +
     "   <input type=\"text\" id=\"btnTabName" + id + "\" onkeyup=\"btnTabName_keyup(this)\" class=\"form-control newtabname\" style=\"width:80%;display:inline;\" value=\"" + name + "\" />" +
     "   <a onclick=\"RemoveTab(this)\" class=\"btn btn-circle btn-default\"><i class=\"fa fa-minus\"></i></a>" +
     "</div>";
    return htmlAppend;
}

function btnTabName_keyup(btn) {
    var tabid = $(btn).closest(".tabname").attr("data-tabid");
    var $container = GetPopOverContainer(btn);
    $container.find(".nav-tabs a[data-tabid=" + tabid + "]").text($(btn).val())
}

function RemoveTab(btn) {
    var $container = GetPopOverContainer(btn);
    var tabid = $(btn).closest(".tabname").attr("data-tabid");
    $container.find(".nav-tabs li a[data-tabid=" + tabid + "]").parent().remove();
    var InitialTabId = 1;
    $container.find(".nav-tabs li a").each(function () {
        var tabname = $(this).attr("data-tabid", InitialTabId);
        InitialTabId = InitialTabId + 1;
    });
    SetTabNames($container)
}

function ddlTabAlignment_Change(ddl) {
    var $container = GetPopOverContainer(ddl);
    console.log($container)
    console.log($(ddl).val().toLowerCase())
    if ($(ddl).val().toLowerCase() == "right") {
        $container.find("ul").removeClass("pull-left pull-right").addClass("pull-right");
    }
    else if ($(ddl).val().toLowerCase() == "left") {
        $container.find("ul").removeClass("pull-left pull-right").addClass("pull-left");
    }
}
//#endregion Tabs End