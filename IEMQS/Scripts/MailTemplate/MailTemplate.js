﻿$(document).ready(function () {
    var table = $('#templateData');
    var oTable = $('#templateData').dataTable({
        "language": {
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "emptyTable": "No data available in table",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "infoEmpty": "No entries found",
            "infoFiltered": "(filtered1 from _MAX_ total entries)",
            "lengthMenu": "_MENU_ entries",
            "search": "Search:",
            "zeroRecords": "No matching records found"
        },
        "bProcessing": true,
        "bServerSide": true,
        "stateSave": false,
        "bDestroy": true,
        buttons: [],
        responsive: true,
        "order": [
            [0, 'asc']
        ],
        "lengthMenu": [
            [5, 10, 25, 50, 100],
            [5, 10, 25, 50, 100]
        ],
        "pageLength": 10,
        'iDisplayLength': 10,
        "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
        "sAjaxSource": WebsiteURL + "/Utility/MailTamplateMaster/LoadDataTable",
        "fnServerParams": function (aoData) {
            aoData.push({ "name": "type", "value": "sdf" }

                        );
        },
        "fnServerData": function (sSource, aoData, fnCallback) {
            $.ajax({
                'dataType': 'json',
                'type': 'POST',
                'url': sSource,
                'data': aoData,
                //  'success': fnCallback,
                'success': function (data) {
                    fnCallback(data);
                    $(".txtStrWhere", "#dvExportHeader").val(data.whereCondition);
                    $(".txtStrSort", "#dvExportHeader").val(data.strSortOrder);
                },
                'async': false
            });
        },
        "columns":
    [
        {
            "name": "TamplateName",
            "sName": "TamplateName",
            "bSearchable": true,
            "bSortable": true,
            "ColName": "TamplateName"
        },

          {
              "name": "CCEmail",
              "sName": "CCEmail",
              "bSearchable": true,
              "bSortable": true,
              "ColName": "CCEmail"
          },
           {
               "name": "BCCEmail",
              "sName": "BCCEmail",
              "bSearchable": true,
               "bSortable": true,
               "ColName": "BCCEmail"
           },
           {
               "name": "Subject",
               "sName": "Subject",
               "bSearchable": true,
               "bSortable": true,
               "ColName": "Subject"
           },
            {
                "name": "Description",
                "sName": "Description",
                "bSearchable": true,
                "bSortable": true,
                "ColName": "Description"
            },
             {
                 "bSearchable": false,
                 "bSortable": false,
                 "sClass": "columnAlignCenter",
             },
             {
                 "bSearchable": false,
                 "bSortable": false,
                 "sClass": "columnAlignCenter",
             }
    ]
    });
    table.dtSearchFilter();

    $('#btnNewTemplate').click(function () {
        SaveTemplate(0);
    });

});
function SaveTemplate(templateId) {
    $.ajax({
        url: WebsiteURL +  "/Utility/MailTamplateMaster/TemplateCreatePartial",
        type: "POST",
        datatype: "html",
        async: false,
        beforeSend: function () {
            showLoading(true);
        },
        complete: function () {
            showLoading(false);
        },
        data: { templateId: templateId },
        success: function (data) {
            debugger;
            bootbox.dialog({
                title: "Maintain Template",
                message: data,
                buttons: {
                    'confirm': {
                        label: 'Save',
                        className: 'btn save',
                        callback: function () {
                            if ($("#frmTemplate").valid()) {
                                var formdata = new FormData();
                                var frmlinesValues = $("#frmTemplate").serializeArray();
                                $.each(frmlinesValues, function (key, input) {
                                    formdata.append(input.name, input.value);
                                });

                                $.ajax({
                                    url: WebsiteURL +  "/Utility/MailTamplateMaster/Save",
                                    type: "POST",
                                    contentType: false,
                                    processData: false,
                                    datatype: "json",
                                    data: formdata,
                                    success: function (data) {
                                        if (data.Key == true) {
                                            $("#templateData").DataTable().ajax.reload();
                                            DisplayNotiMessage("success", "Template saved successfully", "Success");
                                        }
                                        else {
                                            DisplayNotiMessage("error", data.Value, "Error");
                                        }
                                    }
                                });
                            }
                            else {
                                return false;
                            }
                        }
                    },
                    'cancel': {
                        label: 'Close',
                        className: 'btn btnclose'
                    }
                }

            })
        },
        error: function () {
            DisplayNotiMessage("error", "Error occured please try again", "Error");
        }
    });
}

function EditTemplate(templateId) {
    bootbox.confirm("Do you want to edit this template.?", function (result) {
        if (result) {
            bootbox.hideAll();
            SaveTemplate(templateId);
        }
    });
}
function DeleteTemplate(templateId) {

    bootbox.confirm("Do you want to delete this template?", function (result) {
        if (result) {
            $.ajax({
                url: WebsiteURL +  "/Utility/MailTamplateMaster/DeleteTemplate",
                type: "POST",
                datatype: "json",
                data: { templateId: templateId },
                success: function (data) {
                    if (data.Key == true) {
                        $("#templateData").DataTable().ajax.reload();
                        DisplayNotiMessage("success", "Template Deleted successfully", "Success");
                    }
                    else {
                        DisplayNotiMessage("error", data.Value, "Error");
                    }
                }
            });
        }
    });
}
function ActiveTemplate(templateId) {

    bootbox.confirm("Do you want to Active/Deactive this template?", function (result) {
        if (result) {
            $.ajax({
                url: WebsiteURL +  "/Utility/MailTamplateMaster/ActiveDeactive",
                type: "POST",
                datatype: "json",
                data: { templateId: templateId },
                success: function (data) {
                    if (data.Key == true) {
                        $("#templateData").DataTable().ajax.reload();
                        DisplayNotiMessage("success", data.Value, "Success");
                    }
                    else {
                        DisplayNotiMessage("error", data.Value, "Error");
                    }
                }
            });
        }
    });

}


function EditTemplateBody(templateId, templateName) {
    $.ajax({
        url: WebsiteURL +  "/Utility/MailTamplateMaster/TemplateBodyEditPartial",
        type: "POST",
        datatype: "html",
        async: false,
        beforeSend: function () {
            showLoading(true);
        },
        complete: function () {
            showLoading(false);
        },
        data: { templateId: templateId },
        success: function (data) {
            bootbox.dialog({
                title: "Maintain Body For " + templateName,
                message: data,
                size:'large',
                buttons: {
                    'confirm': {
                        label: 'Save',
                        className: 'btn green',
                        callback: function () {
                            if ($("#TemplateBodyform").valid()) {
                                var formdata = new FormData();
                                var frmlinesValues = $("#TemplateBodyform").serializeArray();
                                $.each(frmlinesValues, function (key, input) {
                                    formdata.append(input.name, input.value);
                                });

                                $.ajax({
                                    url: WebsiteURL +  "/Utility/MailTamplateMaster/SaveTemplate",
                                    type: "POST",
                                    contentType: false,
                                    processData: false,
                                    datatype: "json",
                                    data: formdata,
                                    success: function (data) {
                                        if (data.Key == true) {
                                            $("#templateData").DataTable().ajax.reload();
                                            DisplayNotiMessage("success", "Template body saved successfully", "Success");
                                        }
                                        else {
                                            DisplayNotiMessage("error", data.Value, "Error");
                                        }
                                    }
                                });
                            }
                            else {
                                return false;
                            }
                        }
                    },
                    'cancel': {
                        label: 'Cancel',
                        className: 'btn green'
                    }
                }

            })
        },
        error: function () {
            DisplayNotiMessage("error", "Error occured please try again", "Error");
        }
    });
}