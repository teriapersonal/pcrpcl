var Login = function() {
    var handleLogin = function() {
        $('.login-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                Username: {
                    required: true
                },
                Password: {
                    required: true
                },
                remember: {
                    required: false
                }
            },

            messages: {
                Username: {
                    required: "Username is required."
                },
                Password: {
                    required: "Password is required."
                }
            },
            invalidHandler: function(event, validator) { //display error alert on form submit   
                $('.alert-danger', $('.login-form')).show();
            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            errorPlacement: function(error, element) {
                error.insertAfter(element.closest('.input-icon'));
            },
            submitHandler: function(form) {
                var formData = new FormData();
                var frmValues = $('#frmLogin').serializeArray();
                $.each(frmValues, function (key, input) {
                    formData.append(input.name, input.value);
                });
                $.ajax({
                    url: '/Authentication/Authenticate/Login',
                    data: formData,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    async:false,
                    beforeSend: function () {
                        showLoading(true);
                    },
                    complete: function () {
                        showLoading(false);
                    },
                    success: function (data) {
                        if (data.Key) {
                            window.location.href = "/MyDashboard/Dashboard/Index";
                        }
                        else {
                            alert(data.Value);
                        }
                    }
                });
            }
        });
        $('.login-form input').keypress(function(e) {
            if (e.which == 13) {
                if ($('.login-form').validate().form()) {
                    $('.login-form').submit(); //form validation success, call ajax form submit
                }
                return false;
            }
        });
        $('.forget-form input').keypress(function(e) {
            if (e.which == 13) {
                if ($('.forget-form').validate().form()) {
                    $('.forget-form').submit();
                }
                return false;
            }
        });
        $('#forget-password').click(function(){
            $('.login-form').hide();
            $('.forget-form').show();
        });

        $('#back-btn').click(function(){
            $('.login-form').show();
            $('.forget-form').hide();
        });
    }

    return {
        //main function to initiate the module
        init: function () {
            handleLogin();            
            $('.forget-form').hide();
        }

    };

}();
jQuery(document).ready(function() {
    Login.init();
});
