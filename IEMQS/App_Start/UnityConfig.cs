using IEMQS.DESServices;
using IEMQS.PPSServices;
using System;

using Unity;

namespace IEMQS
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public static class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container =
          new Lazy<IUnityContainer>(() =>
          {
              var container = new UnityContainer();
              RegisterTypes(container);
              return container;
          });

        /// <summary>
        /// Configured Unity Container.
        /// </summary>
        public static IUnityContainer Container => container.Value;
        #endregion

        /// <summary>
        /// Registers the type mappings with the Unity container.
        /// </summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>
        /// There is no need to register concrete types such as controllers or
        /// API controllers (unless you want to change the defaults), as Unity
        /// allows resolving a concrete type even if it was not previously
        /// registered.
        /// </remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            // NOTE: To load from web.config uncomment the line below.
            // Make sure to add a Unity.Configuration to the using statements.
            // container.LoadConfiguration();

            // TODO: Register your type's mappings here.
            // container.RegisterType<IProductRepository, ProductRepository>();
            container.RegisterType<IBUService, BUService>();
            container.RegisterType<IObjectService, ObjectService>();
            container.RegisterType<ICategoryActionService, CategoryActionService>();
            container.RegisterType<ICommonService, CommonService>();
            container.RegisterType<IPolicyService, PolicyService>();
            container.RegisterType<IObjectPolicyMappingService, ObjectPolicyMappingService>();
            container.RegisterType<IFolderStructureService, FolderStructureService>();
            container.RegisterType<IAgencyService, AgencyService>();
            container.RegisterType<IProjectService, ProjectService>();
            container.RegisterType<IProductFormCodeService, ProductFormCodeService>();
            container.RegisterType<IGlobalJEPTemplateService, GlobalJEPTemplateService>();
            container.RegisterType<IPartService, PartService>();
            container.RegisterType<IProductService, ProductService>();
            container.RegisterType<IAgencyEmailService, AgencyEmailService>();
            container.RegisterType<IROCService, ROCService>();
            container.RegisterType<ITransmittalService, TransmittalService>();
            container.RegisterType<IFunctionRoleService, FunctionRoleService>();
            container.RegisterType<IDepartmentGroupService, DepartmentGroupService>();
            container.RegisterType<IARMService, ARMService>();
            container.RegisterType<IDINService, DINService>();
            container.RegisterType<IDOCService, DOCService>();
            container.RegisterType<IDOCFileService, DOCFileService>();
            container.RegisterType<ISolrDocumentIndexService, SolrDocumentIndexService>();
            container.RegisterType<IProjectAuthorizationService, ProjectAuthorizationService>();
            container.RegisterType<IPartTypeService, PartTypeService>();
            container.RegisterType<INotificationService, NotificationService>();
            container.RegisterType<IDCRService, DCRService>();
            container.RegisterType<IPartSheetService, PartSheetService>();
            container.RegisterType<IJEPService, JEPService>();
            container.RegisterType<ISizeCodeService, SizeCodeService>();
            container.RegisterType<IMaterialService, MaterialService>();
            container.RegisterType<IItemGroupService, ItemGroupService>();
            container.RegisterType<IHBOMService, HBOMService>();



            #region PPS
            //container.RegisterType<IDesignMCSService, DesignMCSService>();


            #endregion


        }
    }
}