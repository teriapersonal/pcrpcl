﻿using System.Web;
using System.Web.Optimization;

namespace IEMQS
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            #region Style Bundle
            bundles.Add(new StyleBundle("~/Content/css").Include(
                                        "~/Content/bootstrap.css",
                                        "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/font-awesome/css").Include(
                                        "~/Content/ThemeCss/font-awesome/css/font-awesome.min.css"));

            bundles.Add(new StyleBundle("~/Content/bootstrap/css").Include(
                                        "~/Content/ThemeCss/bootstrap/css/bootstrap.min.css"));

            bundles.Add(new StyleBundle("~/Content/bootstrap-switch/css").Include(
                                        "~/Content/ThemeCss/bootstrap-switch/css/bootstrap-switch.css"));

            bundles.Add(new StyleBundle("~/Content/select2/css").Include(
                                        "~/Content/ThemeCss/select2/css/select2.min.css",
                                        "~/Content/ThemeCss/select2/css/select2-bootstrap.min.css"));

            bundles.Add(new StyleBundle("~/Content/Theme/css").Include(
                                        "~/Content/ThemeCss/Css/components.min.css",
                                        "~/Content/ThemeCss/Css/plugins.min.css"));

            bundles.Add(new StyleBundle("~/Content/ThemeCssUI/css").Include("~/Content/jquery-ui.min.css"));

            bundles.Add(new StyleBundle("~/Content/Datatable/css").Include(
                                        "~/Content/ThemeCss/Datatable/datatables.min.css",
                                        "~/Content/ThemeCss/Datatable/datatables.bootstrap.css",
                                        "~/Content/ThemeCss/select2/css/select2-bootstrap.min.css"
                                        //, "~/Content/DataTable/fixedColumn.custom.dataTables.css"
                                        ));

            bundles.Add(new StyleBundle("~/Content/datetimepicker/css").Include(
                "~/Content/DateTimePicker/clockface.css",
                "~/Content/DateTimePicker/bootstrap-datetimepicker.min.css",
                "~/Content/DateTimePicker/bootstrap-timepicker.min.css",
                "~/Content/DateTimePicker/bootstrap-datepicker3.min.css",
                "~/Content/DateTimePicker/daterangepicker.min.css"
            ));

            bundles.Add(new StyleBundle("~/Content/PageLevelDatatable/css").Include(
                                        "~/Content/Datatable/jquery.dataTables.min.css",
                                        "~/Content/Datatable/responsive.dataTables.min.css",
                                        "~/Content/Datatable/buttons.dataTables.min.css",
                                        "~/Content/Datatable/select.dataTables.min.css"));

            bundles.Add(new StyleBundle("~/Content/layout2/css").Include(
                                        "~/Content/ThemeCss/layoutCss/layout.min.css",
                                        "~/Content/ThemeCss/layoutCss/blue.min.css",
                                        "~/Content/ThemeCss/layoutCss/custom.min.css",
                                        "~/Content/cw-charcount.css"));

            bundles.Add(new StyleBundle("~/Content/style").Include("~/Content/ThemeCss/style.css", new CssRewriteUrlTransform()));

            bundles.Add(new StyleBundle("~/Content/jQuery-File-Upload").Include(
                                        "~/Content/jQuery.FileUpload/css/jquery.fileupload.css",
                                        "~/Content/jQuery.FileUpload/css/jquery.fileupload-ui.css",
                                        "~/Content/blueimp-gallery2/css/blueimp-gallery.css",
                                        "~/Content/blueimp-gallery2/css/blueimp-gallery-video.css",
                                        "~/Content/blueimp-gallery2/css/blueimp-gallery-indicator.css"));

            bundles.Add(new StyleBundle("~/Content/Common/css").Include(
                                        "~/Content/ThemeCss/toastr.min.css",
                                        "~/Content/ThemeCss/layoutCss/custom.css",
                                        "~/Content/ThemeCss/layoutCss/custom.min.css"));

            bundles.Add(new StyleBundle("~/Content/datepicker/css").Include(
                                       "~/Content/DateTimePicker/daterangepicker.min.css",
                                       "~/Content/DateTimePicker/bootstrap-datepicker3.min.css"));
            #endregion

            #region Script Bundle
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include("~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include("~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include("~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                                        "~/Scripts/bootstrap.js",
                                        "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap/js").Include(
                                        "~/Scripts/ThemeScripts/Plugins/bootstrap/bootstrap.min.js",
                                        "~/Scripts/bootstrap-maxlength.js"));

            bundles.Add(new ScriptBundle("~/bundles/ThemeJs/js").Include("~/Scripts/ThemeScripts/Plugins/jquery.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/ThemeJsUI/js").Include("~/Scripts/jquery-ui.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jquery-slimscroll/js").Include(
                                        "~/Scripts/ThemeScripts/Plugins/jquery-slimscroll/jquery.slimscroll.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jquery.blockui/js").Include(
                                        "~/Scripts/ThemeScripts/Plugins/jquery.blockui.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap-switch/js").Include(
                                        "~/Scripts/ThemeScripts/Plugins/bootstrap-switch/bootstrap-switch.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jquery-validation/js").Include(
                                         "~/Scripts/ThemeScripts/Plugins/jquery-validation/jquery.validate.min.js",
                                         "~/Scripts/ThemeScripts/Plugins/jquery-validation/additional-methods.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/select2/js").Include(
                                        "~/Scripts/select2/js/select2.full.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/backstretch/js").Include(
                                        "~/Scripts/ThemeScripts/Plugins/backstretch/jquery.backstretch.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/datetimepicker/js").Include(
                                      "~/Scripts/DatetimePicker/moment.min.js",
                             "~/Scripts/DatetimePicker/daterangepicker.min.js",
                        "~/Scripts/DatetimePicker/bootstrap-datepicker.min.js",
                        "~/Scripts/DatetimePicker/bootstrap-timepicker.min.js",
                    "~/Scripts/DatetimePicker/bootstrap-datetimepicker.min.js",
                                       "~/Scripts/DatetimePicker/clockface.js",
                "~/Scripts/DatetimePicker/components-date-time-pickers.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/datatable/js").Include(
                                        "~/Scripts/ThemeScripts/Datatable/datatable.js",
                                        "~/Scripts/ThemeScripts/Datatable/datatables.min.js",
                                        "~/Scripts/ThemeScripts/Datatable/datatables.bootstrap.js",
                                        "~/Scripts/DataTable/dataTables.custom.fixedColumn.js",
                                        "~/Scripts/DataTable/dataTables.custom.utility.js",
                                        "~/Scripts/DataTable/dataTables.buttons.min.js",
                                        "~/Scripts/DataTable/jszip.min.js",
                                        "~/Scripts/DataTable/buttons.html5.min.js",
                                        "~/Scripts/Column-Filter.js"
                                        ));

            bundles.Add(new ScriptBundle("~/bundles/jQuery-File-Upload").Include(
                                        //<!-- The Templates plugin is included to render the upload/download listings -->
                                        "~/Scripts/jQuery.FileUpload/vendor/jquery.ui.widget.js",
                                          "~/Scripts/jQuery.FileUpload/tmpl.min.js",
                                        //<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
                                        "~/Scripts/jQuery.FileUpload/load-image.all.min.js",
                                        //<!-- The Canvas to Blob plugin is included for image resizing functionality -->
                                        "~/Scripts/jQuery.FileUpload/canvas-to-blob.min.js",
                                        //"~/Scripts/file-upload/jquery.blueimp-gallery.min.js",
                                        //<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
                                        "~/Scripts/jQuery.FileUpload/jquery.iframe-transport.js",
                                        //<!-- The basic File Upload plugin -->
                                        "~/Scripts/jQuery.FileUpload/jquery.fileupload.js",
                                        //<!-- The File Upload processing plugin -->
                                        "~/Scripts/jQuery.FileUpload/jquery.fileupload-process.js",
                                        //<!-- The File Upload image preview & resize plugin -->
                                        "~/Scripts/jQuery.FileUpload/jquery.fileupload-image.js",
                                        //<!-- The File Upload audio preview plugin -->
                                        "~/Scripts/jQuery.FileUpload/jquery.fileupload-audio.js",
                                        //<!-- The File Upload video preview plugin -->
                                        "~/Scripts/jQuery.FileUpload/jquery.fileupload-video.js",
                                        //<!-- The File Upload validation plugin -->
                                        "~/Scripts/jQuery.FileUpload/jquery.fileupload-validate.js",
                                        //!-- The File Upload user interface plugin -->
                                        "~/Scripts/jQuery.FileUpload/jquery.fileupload-ui.js",
                                        //Blueimp Gallery 2 
                                        "~/Scripts/blueimp-gallery2/js/blueimp-gallery.js",
                                        "~/Scripts/blueimp-gallery2/js/blueimp-gallery-video.js",
                                        "~/Scripts/blueimp-gallery2/js/blueimp-gallery-indicator.js",
                                        "~/Scripts/blueimp-gallery2/js/jquery.blueimp-gallery.js"));

            bundles.Add(new ScriptBundle("~/bundles/Blueimp-Gallerry2").Include(//Blueimp Gallery 2 
                                        "~/Scripts/blueimp-gallery2/js/blueimp-gallery.js",
                                        "~/Scripts/blueimp-gallery2/js/blueimp-gallery-video.js",
                                        "~/Scripts/blueimp-gallery2/js/blueimp-gallery-indicator.js",
                                        "~/Scripts/blueimp-gallery2/js/jquery.blueimp-gallery.js"));

            bundles.Add(new ScriptBundle("~/bundles/charts/js").Include(
                                         "~/Content/assets/global/plugins/flot/jquery.flot.min.js",
                                         "~/Content/assets/global/plugins/flot/jquery.flot.resize.min.js",
                                         "~/Content/assets/global/plugins/flot/jquery.flot.categories.min.js",
                                         "~/Content/assets/global/plugins/flot/jquery.flot.pie.min.js",
                                         "~/Content/assets/global/plugins/flot/jquery.flot.stack.min.js",
                                         "~/Content/assets/global/plugins/flot/jquery.flot.crosshair.min.js",
                                         "~/Content/assets/global/plugins/flot/jquery.flot.axislabels.js",
                                         "~/Content/assets/pages/scripts/charts-flotcharts.js"));

            bundles.Add(new ScriptBundle("~/bundles/Main").Include("~/Scripts/Main.js"));

            bundles.Add(new ScriptBundle("~/bundles/Common/js").Include(
                                        "~/Scripts/ThemeScripts/toastr.min.js",
                                        "~/Scripts/Main.js",
                                        "~/Scripts/common.js",
                                        "~/Scripts/autoNumeric-1.9.41.js"));

            bundles.Add(new ScriptBundle("~/bundles/CommonFooter/js").Include(
                                        "~/Scripts/ThemeScripts/app.min.js",
                                        "~/Scripts/ThemeScripts/layout.js",
                                        "~/Scripts/ThemeScripts/demo.js",
                                        "~/Scripts/ThemeScripts/quick-sidebar.js",
                                        "~/Scripts/ThemeScripts/quick-nav.js"));

            bundles.Add(new ScriptBundle("~/bundles/Dashboard/js").Include(
                                        "~/Content/assets/global/plugins/flot/jquery.flot.min.js",
                                        "~/Content/assets/global/plugins/flot/jquery.flot.resize.min.js",
                                        "~/Content/assets/global/plugins/flot/jquery.flot.categories.min.js",
                                        "~/Content/assets/global/plugins/flot/jquery.flot.pie.min.js",
                                        "~/Content/assets/global/plugins/flot/jquery.flot.stack.min.js",
                                        "~/Content/assets/global/plugins/flot/jquery.flot.crosshair.min.js",
                                        "~/Content/assets/global/plugins/flot/jquery.flot.axislabels.js",
                                        "~/Content/assets/pages/scripts/charts-flotcharts.js",
                                        "~/Content/assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js",
                                        "~/Content/assets/global/plugins/jquery.sparkline.min.js",
                                        "~/Content/assets/global/scripts/app.js",
                                        "~/Content/assets/pages/scripts/dashboard.js",
                                        "~/Content/assets/global/plugins/bootstrap-multiselect/js/bootstrap-multiselect.js",
                                        "~/Content/assets/pages/scripts/components-bootstrap-multiselect.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/datepicker/js").Include(
                                       "~/Scripts/DatetimePicker/moment.min.js",
                                       "~/Scripts/DatetimePicker/bootstrap-datepicker.min.js"));
            bundles.Add(new ScriptBundle("~/bundles/bootstrapmultiselect/js").Include(
                "~/Content/assets/global/plugins/bootstrap-multiselect/js/bootstrap-multiselect.js",
                "~/Scripts/ThemeScripts/app.min.js",
                "~/Content/assets/pages/scripts/components-bootstrap-multiselect.min.js",
                "~/Scripts/bootbox.js",
                "~/Scripts/ThemeScripts/jquery-validate.bootstrap-tooltip.js"));
            #endregion
        }
    }

    //public class CssUrlTransformWrapper : IItemTransform
    //{
    //    private readonly CssRewriteUrlTransform _cssRewriteUrlTransform;
    //    public CssUrlTransformWrapper()
    //    {
    //        _cssRewriteUrlTransform = new CssRewriteUrlTransform();
    //    }
    //    public string Process(string includedVirtualPath, string input)
    //    {
    //        return _cssRewriteUrlTransform.Process("~" + VirtualPathUtility.ToAbsolute(includedVirtualPath), input);
    //    }
    //}
}
