﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Threading.Tasks;
using System.Configuration;
using IEMQSImplementation.ILN.Persistence;
using IEMQSImplementation.ILN.Persistence.Enums;
using Newtonsoft.Json;
using System.Data.SqlClient;
using IEMQS.ILN.Models;
using IEMQSImplementation;
using IEMQSImplementation.PCR;
using System.Text;
using System.Xml;
using System.IO;
using System.Xml.Serialization;
using Formatting = Newtonsoft.Json.Formatting;

namespace IEMQS.ILN.Framework
{
    public class DataAdapter
    {

        const string ProjectItemFetchSuccess = "Project item data successfully fetch from LN.";
        const string NoRecordsFoundsInProjectItemFetch = "No new item found in LN.";
        const string PCRDataUpdated = "PCR data updated successfully!";
        const string PCRGenerateSuccess = "PCR generated successfully!";
        const string PCRReleaseSuccess = "PCR released successfully!";
        const string PCRRetractSuccess = "PCR retracted successfully!";
        const string NonPlateItemRestrict = "You can not update non-plate item details!";
        const string BulkUpdatePCRDetails = "PCR type, date, location and W/C successfully update in bulk!";
        const string PCRRequirementDateInvalid = "Please select PCR requirement date greater than today!";
        const string MaterialPlannerPCRDataUpdated = "Material planner PCR data updated successfully!";
        const string MaterialPlannerPCRForReleaseDataUpdated = "Material planner PCR For Release data updated successfully!";
        const string UnAthorizedForUpdateData = "You are un-authorized for update data!";
        const string MaterialPlannerPCRNotFound = "Material planner PCR details not found!";
        const string PLTType = "PLT";
        const string MaterialPlannerPCRAction = "MaterialPlannerPCR";
        const string GeneratePCRAction = "GeneratePCR";
        const string QCPCRAction = "QCPCR";
        const string PCLGenerateSuccess = "PCL generated successfully!";
        const string PCLReleaseSuccess = "PCL released successfully!";
        const string PCLNotGenerated = "PCL not generated yet!";
        const string PCRNotGenerated = "PCR not generated yet!";
        const string PCLNotInGeneratedStatus = "PCL is not in PCL Generated status!";
        const string NonPlateReturnBalanceDistribution = "You can not submit return balance distribution for non-plate";
        const string ReturnBalanceDistributionSave = "Return balance distribution save successfully!";
        const string ReturnBalanceDistributionUpdate = "Return balance distribution updated successfully!";
        const string ReturnBalanceDistributionNotExist = "Return balance distribution line not exist!";
        const string ReturnBalanceDistributionDelete = "Return balance distribution delete successfully!";
        const string ReturnBalanceDistributionMaxLinesMaintain = "Return balance distribution maximun lines already maintain!";
        const string PCLApprovedByQASuccess = "PCL Approved successfully!";
        const string SomeDataInvalidForUpdate = "Some Data Is Invalid For Update!";
        const string StockNotAttached = "Stock not attached yet!";
        const string StockUnbindSuccess = "Stock unbind successfully!";
        const string StockUnbindInReleaseToMaterialPlannerStatus = "You Can Only Unbind Stock In Release To Material Planner Status!";
        const string PCRReturnSuccess = "PCR Return Successfully!";
        const string StockUnlinkSuccess = "Stock Unlinked Successfully!";
        const string RowDeleteSuccess = "Row Deleted Successfully!";
        const string ValidateSuccess = "Validated Successfully!";
        const string OperationSuccess = "Operation Completed Successfully!";
        const string ApproveRowSuccess = "The Row has been Approved!";
        const string RejectRowSuccess = "The Row has been Rejected!";

        private List<T> ConvertListOfDictionaryToListOfClassObject<T>(List<IDictionary<string, object>> lstDictionary)
        {
            string json = JsonConvert.SerializeObject(lstDictionary, Formatting.Indented);////convert to json
            return JsonConvert.DeserializeObject<List<T>>(json);////convert json to list of class            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="procedureName"></param>
        /// <param name="tableName"></param>
        /// <param name="tabularColumns"></param>
        /// <param name="isLNTable"></param>
        /// <param name="indexName"></param>
        /// <param name="indexColumnNames"></param>
        /// <param name="viewColumnNames"></param>
        /// <param name="viewColumnValues"></param>
        /// <param name=""></param>
        /// <param name="navigatorIndex"></param>
        /// <param name="navigatorType"></param>
        /// <returns></returns>
        public async Task<List<List<IDictionary<string, object>>>> GetMultiOccurenceData(
            string userId,
            string procedureName,
            string tableName,
            string tabularColumns,
            bool isLNTable,
            string indexName,
            string indexColumnNames,
            string viewColumnNames,
            string viewColumnValues,
            int? navigatorIndex,
            string navigatorType = null
            )
        {
            DataConnectionParams<DynamicParameter> connectionParams;
            DataConnection dataConnection;
            List<List<IDictionary<string, object>>> managedLists;
            try
            {
                dataConnection = new DataConnection(ConfigurationManager.ConnectionStrings["IEMQS"].ConnectionString);
                connectionParams = new DataConnectionParams<DynamicParameter>(DataConnectionTypeEnum.StoredProcedure)
                {
                    Query = procedureName,
                    Params = new List<DynamicParameter>()
                        {
                            new DynamicParameter() { DbType = DbType.String , Name = "UserId", Value = userId },
                            new DynamicParameter() { DbType = DbType.String, Name = "TableName", Value = tableName },
                            new DynamicParameter() { DbType = DbType.Boolean, Name = "IsLNTable", Value = isLNTable },
                            new DynamicParameter() { DbType = DbType.String, Name = "IndexName", Value = indexName },
                            new DynamicParameter() { DbType = DbType.String, Name = "IndexColumnNames", Value = indexColumnNames },
                            new DynamicParameter() { DbType = DbType.String, Name = "ViewColumnNames", Value = viewColumnNames },
                            new DynamicParameter() { DbType = DbType.String, Name = "ViewColumnValues", Value = viewColumnValues },
                            new DynamicParameter() { DbType = DbType.String, Name = "NavigatorType", Value = navigatorType },
                            new DynamicParameter() { DbType = DbType.Int32, Name = "NavigatorIndex", Value = navigatorIndex },
                            new DynamicParameter() { DbType = DbType.String, Name = "TabularColumnNames", Value = tabularColumns }
                        }
                };
                managedLists = await dataConnection.QueryDynamicMultipleAsync(connectionParams);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return managedLists;
        }


        public async Task<List<IDictionary<string, object>>> GetProjectList(
            string userId)
        {
            DataConnectionParams<DynamicParameter> connectionParams;
            DataConnection dataConnection;
            List<List<IDictionary<string, object>>> projectList;
            try
            {
                dataConnection = new DataConnection(ConfigurationManager.ConnectionStrings["IEMQS"].ConnectionString);

                connectionParams = new DataConnectionParams<DynamicParameter>(DataConnectionTypeEnum.StoredProcedure)
                {
                    Query = "[dbo].[SP_PCR_Get_ProjectList]",
                    Params = new List<DynamicParameter>()
                        {
                            new DynamicParameter() { DbType = DbType.String , Name = "PSNO", Value = userId }
                        }
                };
                projectList = await dataConnection.QueryDynamicMultipleAsync(connectionParams);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return projectList[0];
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="project"></param>
        /// <param name="element"></param>
        /// <param name="startIndex"></param>
        /// <param name="endIndex"></param>
        /// <param name="sortOrder"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// 

        #region Generate PCR Actions
        public async Task<List<SP_PCR_GET_PCR_DATA_Result>> GetPCRData(
            string project,
            string sortOrder = null,
            int? startIndex = null,
            int? endIndex = null
            )
        {
            DataConnectionParams connectionParams;
            DataConnection dataConnection;
            IEnumerable<SP_PCR_GET_PCR_DATA_Result> managedList;
            try
            {
                dataConnection = new DataConnection(ConfigurationManager.ConnectionStrings["IEMQS"].ConnectionString);
                connectionParams = new DataConnectionParams(DataConnectionTypeEnum.StoredProcedure)
                {
                    Query = "[dbo].[SP_PCR_GET_PCR_DATA]"
                };
                connectionParams.Params = new
                {
                    Project = project,
                    StartIndex = startIndex,
                    EndIndex = endIndex,
                    SortingFields = sortOrder
                };
                managedList = await dataConnection.QueryAsync<SP_PCR_GET_PCR_DATA_Result>(connectionParams);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return managedList.ToList();
        }

        public async Task<MaintainPCRResult> GetMaintainPCRHeaderData(
          string pcrNumber,
          int? state,
          string userId
           )
        {
            DataConnectionParams<DynamicParameter> connectionParams;
            DataConnection dataConnection;
            MaintainPCRResult result = new MaintainPCRResult();
            List<List<IDictionary<string, object>>> keyValuePairs;
            try
            {
                dataConnection = new DataConnection(ConfigurationManager.ConnectionStrings["IEMQS"].ConnectionString);
                connectionParams = new DataConnectionParams<DynamicParameter>(DataConnectionTypeEnum.StoredProcedure)
                {
                    Query = "[dbo].[SP_PCR_GET_MAINTAIN_PCR_HEADER_DATA]",
                    Params = new List<DynamicParameter>()
                        {
                            new DynamicParameter() { DbType = DbType.String, Name = "PCRNumber", Value = pcrNumber },
                            new DynamicParameter() { DbType = DbType.Int32, Name = "State", Value = state },
                            new DynamicParameter() { DbType = DbType.String, Name = "PSNo", Value = userId }
                        }
                };
                keyValuePairs = await dataConnection.QueryDynamicMultipleAsync(connectionParams);
                // 0 PCRNumber list
                // 1  States
                // 2  Header

                List<IDictionary<string, object>> lstDict = keyValuePairs[0];
                List<string> lst = new List<string>();
                foreach (IDictionary<string, object> dict in lstDict)
                {
                    lst.Add(dict["PCRNumber"].ToString());
                }
                result.PCRNumbers = lst;
                result.States = ConvertListOfDictionaryToListOfClassObject<MAINTAIN_PCR_STATES>(keyValuePairs[1])[0];
                result.HeaderResult = ConvertListOfDictionaryToListOfClassObject<MAINTAIN_PCR_HEADER_Result>(keyValuePairs[2])[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public async Task<MaintainPCRResult> GetMaintainPCRLineData(
         string pcrNumber,
         int? state,
         string userId
          )
        {
            DataConnectionParams<DynamicParameter> connectionParams;
            DataConnection dataConnection;
            MaintainPCRResult result = new MaintainPCRResult();
            List<List<IDictionary<string, object>>> keyValuePairs;
            try
            {
                dataConnection = new DataConnection(ConfigurationManager.ConnectionStrings["IEMQS"].ConnectionString);
                connectionParams = new DataConnectionParams<DynamicParameter>(DataConnectionTypeEnum.StoredProcedure)
                {
                    Query = "[dbo].[SP_PCR_GET_MAINTAIN_PCR_LINE_DATA]",
                    Params = new List<DynamicParameter>()
                        {
                            new DynamicParameter() { DbType = DbType.String, Name = "PCRNumber", Value = pcrNumber },
                            new DynamicParameter() { DbType = DbType.Int32, Name = "State", Value = state },
                            new DynamicParameter() { DbType = DbType.String, Name = "PSNo", Value = userId }
                        }
                };
                keyValuePairs = await dataConnection.QueryDynamicMultipleAsync(connectionParams);
                result.LineResult = ConvertListOfDictionaryToListOfClassObject<MAINTAIN_PCR_LINE_Result>(keyValuePairs[0]);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public async Task<ActionOutput> FetchData(string projectValue, string userId)
        {
            ActionOutput output = new ActionOutput();
            DataConnection dataConnection;
            try
            {
                dataConnection = new DataConnection(ConfigurationManager.ConnectionStrings["IEMQS"].ConnectionString);
                DataConnectionParams connectionParams = new DataConnectionParams(DataConnectionTypeEnum.StoredProcedure)
                {
                    Query = "[dbo].[SP_PCR_GENERATE_PCR_ACTIONS_FETCH_DATA]",

                    Params = new
                    {
                        Project = projectValue,
                        PSNo = userId
                    }
                };
                output = new ActionOutput();
                List<int> queryResult = (await dataConnection.QueryAsync<int>(connectionParams)).ToList();
                if (queryResult != null && queryResult.Count == 1)
                {
                    int recordsAffected = queryResult.First();
                    if (recordsAffected > 0)
                    {
                        output.Result = OutputType.Success;
                        output.Message = ProjectItemFetchSuccess;
                    }
                    else
                    {
                        output.Result = OutputType.Warning;
                        output.Message = NoRecordsFoundsInProjectItemFetch;
                    }
                }

            }
            catch (SqlException ex)
            {
                output.Result = OutputType.Error;
                output.Message = ex.Message;
            }
            catch (Exception ex)
            {
                output.Result = OutputType.Error;
                output.Message = ex.Message;
            }
            return output;
        }

        public async Task<ActionOutput> ValidateGeneratePCRBudgetLines(string projectIds, string elementIds, string budgetLineIds, string childItemIds, string seperator)
        {
            ActionOutput output = new ActionOutput();
            DataConnection dataConnection;
            try
            {
                dataConnection = new DataConnection(ConfigurationManager.ConnectionStrings["IEMQS"].ConnectionString);
                DataConnectionParams connectionParams = new DataConnectionParams(DataConnectionTypeEnum.StoredProcedure)
                {
                    Query = "[dbo].[SP_PCR_Validate_Generate_PCR]",

                    Params = new
                    {
                        ProjectIds = projectIds,
                        ElementIds = elementIds,
                        BudgetLineIds = budgetLineIds,
                        ChildItemIds = childItemIds,
                        Seperator = seperator
                    }
                };

                List<int> queryResult = (await dataConnection.QueryAsync<int>(connectionParams)).ToList();
                if (queryResult != null && queryResult.Count == 1)
                {
                    int result = queryResult.First();
                    if (result == 1)
                    {
                        output.Result = OutputType.Success;
                    }
                    else
                    {
                        output.Result = OutputType.Warning;
                    }
                }
            }
            catch (Exception ex)
            {
                output.Result = OutputType.Error;
                output.Message = ex.Message;
            }
            return output;
        }

        public async Task<ActionOutput> GeneratePlateCuttingRequest(string Ids, string seperator, string userId)
        {
            ActionOutput output = new ActionOutput();
            DataConnection dataConnection;
            try
            {
                dataConnection = new DataConnection(ConfigurationManager.ConnectionStrings["IEMQS"].ConnectionString);
                DataConnectionParams connectionParams = new DataConnectionParams(DataConnectionTypeEnum.StoredProcedure)
                {
                    Query = "[dbo].[SP_PCR_Generate_Plate_Cutting_Request]",

                    Params = new
                    {
                        Ids,
                        Seperator = seperator,
                        PSNo = userId
                    }
                };
                List<int> queryResult = (await dataConnection.QueryAsync<int>(connectionParams)).ToList();
                if (queryResult != null && queryResult.Count == 1)
                {
                    int result = queryResult.First();
                    if (result == 1)
                    {
                        output.Result = OutputType.Success;
                        output.Message = PCRGenerateSuccess;
                    }
                    else
                    {
                        output.Result = OutputType.Warning;

                    }
                }
            }
            catch (SqlException ex)
            {
                output.Result = OutputType.Error;
                output.Message = ex.Message;
            }
            catch (Exception ex)
            {
                output.Result = OutputType.Error;
                output.Message = ex.Message;
            }
            return output;
        }

        public async Task<ActionOutput> RetractPlateCuttingRequest(int Id, string project, string element, string userId)
        {
            ActionOutput output = new ActionOutput();
            DataConnection dataConnection;
            try
            {
                dataConnection = new DataConnection(ConfigurationManager.ConnectionStrings["IEMQS"].ConnectionString);
                DataConnectionParams connectionParams = new DataConnectionParams(DataConnectionTypeEnum.StoredProcedure)
                {
                    Query = "[dbo].[SP_PCR_Retract_Plate_Cutting_Request]",

                    Params = new
                    {
                        Id,
                        Project = project,
                        Element = element,
                        PSNo = userId
                    }
                };
                List<int> queryResult = (await dataConnection.QueryAsync<int>(connectionParams)).ToList();
                if (queryResult != null && queryResult.Count == 1)
                {
                    int result = queryResult.First();
                    if (result == 1)
                    {
                        output.Result = OutputType.Success;
                        output.Message = PCRRetractSuccess;
                    }
                    else
                    {
                        output.Result = OutputType.Warning;
                    }
                }
            }
            catch (SqlException ex)
            {
                output.Result = OutputType.Error;
                output.Message = ex.Message;
            }
            catch (Exception ex)
            {
                output.Result = OutputType.Error;
                output.Message = ex.Message;
            }
            return output;
        }
        #endregion

        #region Generate PCL Actions
        public async Task<List<SP_PCR_GET_MATERIAL_PLANNER_DATA_Result>> GetMaterialPlannerData(
            string sortOrder = null,
            //string filter = null,
            int? startIndex = null,
            int? endIndex = null
            )
        {
            DataConnectionParams connectionParams;
            DataConnection dataConnection;
            IEnumerable<SP_PCR_GET_MATERIAL_PLANNER_DATA_Result> managedList;
            try
            {
                dataConnection = new DataConnection(ConfigurationManager.ConnectionStrings["IEMQS"].ConnectionString);
                connectionParams = new DataConnectionParams(DataConnectionTypeEnum.StoredProcedure)
                {
                    Query = "[dbo].[SP_PCR_GET_MATERIAL_PLANNER_DATA]"
                };
                connectionParams.Params = new
                {
                    StartIndex = startIndex,
                    EndIndex = endIndex,
                    SortingFields = sortOrder
                };
                managedList = await dataConnection.QueryAsync<SP_PCR_GET_MATERIAL_PLANNER_DATA_Result>(connectionParams);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return managedList.ToList();
        }

        public async Task<ActionOutput> ValidateGeneratePCL(int Id)
        {
            ActionOutput output = new ActionOutput();
            DataConnection dataConnection;
            try
            {
                dataConnection = new DataConnection(ConfigurationManager.ConnectionStrings["IEMQS"].ConnectionString);
                DataConnectionParams connectionParams = new DataConnectionParams(DataConnectionTypeEnum.StoredProcedure)
                {
                    Query = "[dbo].[SP_PCR_Validate_Generate_PCL]",

                    Params = new
                    {
                        Id
                    }
                };
                output = new ActionOutput();
                List<int> queryResult = (await dataConnection.QueryAsync<int>(connectionParams)).ToList();
                if (queryResult != null && queryResult.Count == 1)
                {
                    int result = queryResult.First();
                    if (result == 1)
                    {
                        output.Result = OutputType.Success;
                    }
                    else
                    {
                        output.Result = OutputType.Warning;
                    }
                }

            }
            catch (SqlException ex)
            {
                output.Result = OutputType.Error;
                output.Message = ex.Message;
            }
            catch (Exception ex)
            {
                output.Result = OutputType.Error;
                output.Message = ex.Message;
            }
            return output;
        }

        public async Task<ActionOutput> GeneratePlateCuttingLayout(int Id, string userId)
        {
            ActionOutput output = new ActionOutput();
            DataConnection dataConnection;
            try
            {
                dataConnection = new DataConnection(ConfigurationManager.ConnectionStrings["IEMQS"].ConnectionString);
                DataConnectionParams connectionParams = new DataConnectionParams(DataConnectionTypeEnum.StoredProcedure)
                {
                    Query = "[dbo].[SP_PCR_Generate_PCL]",

                    Params = new
                    {
                        Id,
                        PSNo = userId
                    }
                };
                List<int> queryResult = (await dataConnection.QueryAsync<int>(connectionParams)).ToList();
                if (queryResult != null && queryResult.Count == 1)
                {
                    int result = queryResult.First();
                    if (result == 1)
                    {
                        output.Result = OutputType.Success;
                        output.Message = PCLGenerateSuccess;
                    }
                    else
                    {
                        output.Result = OutputType.Warning;

                    }
                }
            }
            catch (SqlException ex)
            {
                output.Result = OutputType.Error;
                output.Message = ex.Message;
            }
            catch (Exception ex)
            {
                output.Result = OutputType.Error;
                output.Message = ex.Message;
            }
            return output;
        }

        public async Task<ActionOutput> ValidateReturnPlateCuttingRequest(int Id)
        {
            ActionOutput output = new ActionOutput();
            DataConnection dataConnection;
            try
            {
                dataConnection = new DataConnection(ConfigurationManager.ConnectionStrings["IEMQS"].ConnectionString);
                DataConnectionParams connectionParams = new DataConnectionParams(DataConnectionTypeEnum.StoredProcedure)
                {
                    Query = "[dbo].[SP_PCR_Validate_Return_PCR]",

                    Params = new
                    {
                        Id
                    }
                };
                output = new ActionOutput();
                List<int> queryResult = (await dataConnection.QueryAsync<int>(connectionParams)).ToList();
                if (queryResult != null && queryResult.Count == 1)
                {
                    int result = queryResult.First();
                    if (result == 1)
                    {
                        output.Result = OutputType.Success;
                    }
                    else
                    {
                        output.Result = OutputType.Warning;
                    }
                }

            }
            catch (SqlException ex)
            {
                output.Result = OutputType.Error;
                output.Message = ex.Message;
            }
            catch (Exception ex)
            {
                output.Result = OutputType.Error;
                output.Message = ex.Message;
            }
            return output;
        }

        public async Task<ActionOutput> ReturnPlateCuttingRequest(int Id, string pcrPlannerRemarks, string userId)
        {
            ActionOutput output = new ActionOutput();
            DataConnection dataConnection;
            try
            {
                dataConnection = new DataConnection(ConfigurationManager.ConnectionStrings["IEMQS"].ConnectionString);
                DataConnectionParams connectionParams = new DataConnectionParams(DataConnectionTypeEnum.StoredProcedure)
                {
                    Query = "[dbo].[SP_PCR_Return_PCR]",

                    Params = new
                    {
                        Id,
                        Remarks = pcrPlannerRemarks,
                        PSNo = userId
                    }
                };
                output = new ActionOutput();
                List<int> queryResult = (await dataConnection.QueryAsync<int>(connectionParams)).ToList();
                if (queryResult != null && queryResult.Count == 1)
                {
                    int result = queryResult.First();
                    if (result == 1)
                    {
                        output.Result = OutputType.Success;
                        output.Message = PCRReturnSuccess;
                    }
                    else
                    {
                        output.Result = OutputType.Warning;
                    }
                }

            }
            catch (SqlException ex)
            {
                output.Result = OutputType.Error;
                output.Message = ex.Message;
            }
            catch (Exception ex)
            {
                output.Result = OutputType.Error;
                output.Message = ex.Message;
            }
            return output;
        }

        public async Task<ActionOutput> UnlinkStock(int Id, string userId)
        {
            ActionOutput output = new ActionOutput();
            DataConnection dataConnection;
            try
            {
                dataConnection = new DataConnection(ConfigurationManager.ConnectionStrings["IEMQS"].ConnectionString);
                DataConnectionParams connectionParams = new DataConnectionParams(DataConnectionTypeEnum.StoredProcedure)
                {
                    Query = "[dbo].[SP_PCR_Unlink_Stock]",

                    Params = new
                    {
                        Id,
                        PSNo = userId
                    }
                };
                output = new ActionOutput();
                List<int> queryResult = (await dataConnection.QueryAsync<int>(connectionParams)).ToList();
                if (queryResult != null && queryResult.Count == 1)
                {
                    int result = queryResult.First();
                    if (result == 1)
                    {
                        output.Result = OutputType.Success;
                        output.Message = StockUnlinkSuccess;
                    }
                    else
                    {
                        output.Result = OutputType.Warning;

                    }
                }

            }
            catch (SqlException ex)
            {
                output.Result = OutputType.Error;
                output.Message = ex.Message;
            }
            catch (Exception ex)
            {
                output.Result = OutputType.Error;
                output.Message = ex.Message;
            }
            return output;
        }

        public async Task<ActionOutput> ValidateStockNumber(
           int Id,
           string stockNumber,
           int stockRevision,
           int useAlternateStock,
           string psNo)
        {
            ActionOutput output = new ActionOutput();
            DataConnection dataConnection;
            DataConnectionParams<DynamicParameter> dataConnectionParams;
            List<List<IDictionary<string, object>>> lstlstDict;
            try
            {
                dataConnection = new DataConnection(ConfigurationManager.ConnectionStrings["IEMQS"].ConnectionString);
                
                
                dataConnectionParams = new DataConnectionParams<DynamicParameter>(DataConnectionTypeEnum.StoredProcedure)
                {
                    Params = new List<DynamicParameter>()
                        {
                            new DynamicParameter() { DbType = DbType.Int32, Name = "PSNo", Value = psNo },
                            new DynamicParameter() { DbType = DbType.Int32, Name = "Id", Value = Id },
                            new DynamicParameter() { DbType = DbType.String, Name = "StockNumber", Value = stockNumber },
                            new DynamicParameter() { DbType = DbType.Int32, Name = "StockRevision", Value = stockRevision },
                            new DynamicParameter() { DbType = DbType.Int32, Name = "UseAlternateStock", Value = useAlternateStock }

                        },
                    Query = "[dbo].[SP_PCR_Validate_StockNumber]"
                };
                lstlstDict = (await dataConnection.QueryDynamicMultipleAsync(dataConnectionParams)).ToList();
                IDictionary<string, object> dict = lstlstDict.FirstOrDefault().FirstOrDefault();
                int status = Convert.ToInt32(dict["Status"].ToString());
                if (status == 1)
                {
                    output.Result = OutputType.Success;
                }
                else
                {
                    output.Result = OutputType.Warning;
                    output.Message = Convert.ToString(dict["MESSAGE"]);
                }

            }
            catch (SqlException ex)
            {
                output.Result = OutputType.Error;
                output.Message = ex.Message;
            }
            catch (Exception ex)
            {
                output.Result = OutputType.Error;
                output.Message = ex.Message;
            }
            return output;
        }

        #endregion

        #region Get LookUp Data
        public async Task<List<SP_PCR_LOOK_UP>> GetWorkCenterWithLocation(int showAll)
        {
            DataConnectionParams connectionParams;
            DataConnection dataConnection;
            List<SP_PCR_LOOK_UP> wcListResult;
            try
            {
                dataConnection = new DataConnection(ConfigurationManager.ConnectionStrings["IEMQS"].ConnectionString);
                connectionParams = new DataConnectionParams(DataConnectionTypeEnum.StoredProcedure)
                {
                    Query = "[dbo].[SP_PCR_Get_WorkCenter_With_Location]",
                    Params = new
                    {
                        ShowAll = showAll
                    }

                };
                wcListResult = (await dataConnection.QueryAsync<SP_PCR_LOOK_UP>(connectionParams)).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return wcListResult;
        }

        public async Task<List<SP_PCR_LOOK_UP>> GetEnumData(string userId)
        {
            DataConnectionParams connectionParams;
            DataConnection dataConnection;
            List<SP_PCR_LOOK_UP> wcListResult;
            try
            {
                dataConnection = new DataConnection(ConfigurationManager.ConnectionStrings["IEMQS"].ConnectionString);
                connectionParams = new DataConnectionParams(DataConnectionTypeEnum.StoredProcedure)
                {
                    Query = "[dbo].[SP_PCR_GET_ENUM_LIST]",
                    Params = new
                    {
                        PSNo = userId
                    }

                };
                wcListResult = (await dataConnection.QueryAsync<SP_PCR_LOOK_UP>(connectionParams)).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return wcListResult;
        }

        public async Task<List<SP_PCR_LOOK_UP>> GetPCLNumberList()
        {
            DataConnectionParams connectionParams;
            DataConnection dataConnection;
            List<SP_PCR_LOOK_UP> wcListResult;
            try
            {
                dataConnection = new DataConnection(ConfigurationManager.ConnectionStrings["IEMQS"].ConnectionString);
                connectionParams = new DataConnectionParams(DataConnectionTypeEnum.StoredProcedure)
                {
                    Query = "[dbo].[SP_PCR_Get_PCL_Number_List]"

                };
                wcListResult = (await dataConnection.QueryAsync<SP_PCR_LOOK_UP>(connectionParams)).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return wcListResult;
        }

        public async Task<List<SP_PCR_LOOK_UP>> GetLocationWithAddress()
        {
            DataConnectionParams connectionParams;
            DataConnection dataConnection;
            IEnumerable<SP_PCR_LOOK_UP> locationAddressList;
            try
            {
                dataConnection = new DataConnection(ConfigurationManager.ConnectionStrings["IEMQS"].ConnectionString);
                connectionParams = new DataConnectionParams(DataConnectionTypeEnum.StoredProcedure)
                {
                    Query = "[dbo].[SP_PCR_Get_Location_With_Address]"
                };

                locationAddressList = await dataConnection.QueryAsync<SP_PCR_LOOK_UP>(connectionParams);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return locationAddressList.ToList();
        }

        public async Task<List<SP_PCR_LOOK_UP>> GetPlannerNames(string userId)
        {
            DataConnectionParams connectionParams;
            DataConnection dataConnection;
            List<SP_PCR_LOOK_UP> plannerNamesResult;
            try
            {
                dataConnection = new DataConnection(ConfigurationManager.ConnectionStrings["IEMQS"].ConnectionString);
                connectionParams = new DataConnectionParams(DataConnectionTypeEnum.StoredProcedure)
                {
                    Query = "[dbo].[SP_PCR_Get_PlannerNames]",
                    Params = new
                    {
                        PSNo = userId
                    }

                };
                plannerNamesResult = (await dataConnection.QueryAsync<SP_PCR_LOOK_UP>(connectionParams)).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return plannerNamesResult;
        }
        #endregion

        #region Release PCL Actions
        public async Task<List<RELEASE_PCL_DATA_Result>> GetReleasePCLData(
            string userId
            )
        {
            DataConnectionParams connectionParams;
            DataConnection dataConnection;
            IEnumerable<RELEASE_PCL_DATA_Result> managedList;
            try
            {
                dataConnection = new DataConnection(ConfigurationManager.ConnectionStrings["IEMQS"].ConnectionString);
                connectionParams = new DataConnectionParams(DataConnectionTypeEnum.StoredProcedure)
                {
                    Query = "[dbo].[SP_PCR_Get_ReleasePCL_Data]"
                };
                connectionParams.Params = new
                {
                    PSNo = userId
                };
                managedList = await dataConnection.QueryAsync<RELEASE_PCL_DATA_Result>(connectionParams);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return managedList.ToList();
        }

        public async Task<ActionOutput> ReleasePlateCuttingLayout(string userName, int Id, bool UpdateScrap)
        {
            ActionOutput output = new ActionOutput();
            DataConnection dataConnection;
            try
            {
                dataConnection = new DataConnection(ConfigurationManager.ConnectionStrings["IEMQS"].ConnectionString);
                DataConnectionParams connectionParams = new DataConnectionParams(DataConnectionTypeEnum.StoredProcedure)
                {
                    Query = "[dbo].[SP_PCR_ReleasePCL]",

                    Params = new
                    {
                        Id = Id,
                        PSNo = userName,
                        UpdateScrap = UpdateScrap
                    }
                };
                List<int> queryResult = (await dataConnection.QueryAsync<int>(connectionParams)).ToList();
                if (queryResult != null && queryResult.Count == 1)
                {
                    int result = queryResult.First();
                    if (result == 1)
                    {
                        output.Result = OutputType.Success;
                        output.Message = PCLReleaseSuccess;
                    }
                    else
                    {
                        output.Result = OutputType.Warning;

                    }
                }
            }
            catch (SqlException ex)
            {
                output.Result = OutputType.Error;
                output.Message = ex.Message;
            }
            catch (Exception ex)
            {
                output.Result = OutputType.Error;
                output.Message = ex.Message;
            }
            return output;
        }

        public async Task<ActionOutput> ModifyPlateCuttingLayout(int Id, string remarkReason, string pclModifiedRemark, string userName)
        {
            ActionOutput output = new ActionOutput();
            DataConnection dataConnection;
            try
            {
                dataConnection = new DataConnection(ConfigurationManager.ConnectionStrings["IEMQS"].ConnectionString);
                DataConnectionParams connectionParams = new DataConnectionParams(DataConnectionTypeEnum.StoredProcedure)
                {
                    Query = "[dbo].[SP_PCR_ModifyPCL]",

                    Params = new
                    {
                        Id,
                        RemarkReason= remarkReason,
                        @PCLModifiedRemark= pclModifiedRemark,
                        PSNo = userName
                    }
                };
                List<int> queryResult = (await dataConnection.QueryAsync<int>(connectionParams)).ToList();
                if (queryResult != null && queryResult.Count == 1)
                {
                    int result = queryResult.First();
                    if (result == 1)
                    {
                        output.Result = OutputType.Success;
                        output.Message = "Plate cutting layout modified successfully";
                    }
                    else
                    {
                        output.Result = OutputType.Warning;
                    }
                }
            }
            catch (SqlException ex)
            {
                output.Result = OutputType.Error;
                output.Message = ex.Message;
            }
            catch (Exception ex)
            {
                output.Result = OutputType.Error;
                output.Message = ex.Message;
            }
            return output;
        }

        #endregion

        #region Return Distribution Actions

        public async Task<List<RETURN_BALANCE_DISTRIBUTION_Result>> GetReturnDistributionData(
           string userId,
           int id
           )
        {
            DataConnectionParams connectionParams;
            DataConnection dataConnection;
            IEnumerable<RETURN_BALANCE_DISTRIBUTION_Result> managedList;
            try
            {
                dataConnection = new DataConnection(ConfigurationManager.ConnectionStrings["IEMQS"].ConnectionString);
                connectionParams = new DataConnectionParams(DataConnectionTypeEnum.StoredProcedure)
                {
                    Query = "[dbo].[SP_PCR_Get_ReturnDistribution_Data]"
                };
                connectionParams.Params = new
                {
                    PSNo = userId,
                    Id = id
                };
                managedList = await dataConnection.QueryAsync<RETURN_BALANCE_DISTRIBUTION_Result>(connectionParams);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return managedList.ToList();
        }

        public async Task<RETURN_BALANCE_DISTRIBUTION_Result> GetOriginalReturnDistributionData(
         string userId,
         int id
         )
        {
            DataConnectionParams connectionParams;
            DataConnection dataConnection;
            IEnumerable<RETURN_BALANCE_DISTRIBUTION_Result> managedList;
            try
            {
                dataConnection = new DataConnection(ConfigurationManager.ConnectionStrings["IEMQS"].ConnectionString);
                connectionParams = new DataConnectionParams(DataConnectionTypeEnum.StoredProcedure)
                {
                    Query = "[dbo].[SP_PCR_Get_OriginalReturnDistribution_Data]"
                };
                connectionParams.Params = new
                {
                    PSNo = userId,
                    Id = id
                };
                managedList = await dataConnection.QueryAsync<RETURN_BALANCE_DISTRIBUTION_Result>(connectionParams);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return managedList.ToList().First();
        }

        public async Task<ActionOutput> ValidateReturnDistributionArea(string pclNumber, int Revision, float TotalArea)
        {
            ActionOutput output = new ActionOutput();
            DataConnection dataConnection;
            try
            {
                dataConnection = new DataConnection(ConfigurationManager.ConnectionStrings["IEMQS"].ConnectionString);
                DataConnectionParams connectionParams = new DataConnectionParams(DataConnectionTypeEnum.StoredProcedure)
                {
                    Query = "[dbo].[SP_PCR_Validate_RD_Area]",

                    Params = new
                    {
                        PCLNumber = pclNumber,
                        revision = Revision,
                        totalArea = TotalArea
                    }
                };
                output = new ActionOutput();
                List<int> queryResult = (await dataConnection.QueryAsync<int>(connectionParams)).ToList();
                if (queryResult != null && queryResult.Count == 1)
                {
                    int result = queryResult.First();
                    if (result == 1)
                    {
                        output.Result = OutputType.Success;
                    }
                    else
                    {
                        output.Result = OutputType.Warning;
                    }
                }

            }
            catch (SqlException ex)
            {
                output.Result = OutputType.Error;
                output.Message = ex.Message;
            }
            catch (Exception ex)
            {
                output.Result = OutputType.Error;
                output.Message = ex.Message;
            }
            return output;
        }

        #endregion

        #region PCL WorkFlow Actions
        public async Task<List<PCL_WORKFLOW_Result.Columns>> GetPCLWorkflowData(
           string userId,
           string screenName
           )
        {
            DataConnectionParams connectionParams;
            DataConnection dataConnection;
            IEnumerable<PCL_WORKFLOW_Result.Columns> managedList;
            try
            {
                dataConnection = new DataConnection(ConfigurationManager.ConnectionStrings["IEMQS"].ConnectionString);
                connectionParams = new DataConnectionParams(DataConnectionTypeEnum.StoredProcedure)
                {
                    Query = "[dbo].[SP_PCR_GET_PCL_Workflow_DATA]"
                };
                connectionParams.Params = new
                {
                    PSNo = userId,
                    ScreenName = screenName
                };
                managedList = await dataConnection.QueryAsync<PCL_WORKFLOW_Result.Columns>(connectionParams);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return managedList.ToList();
        }

        public async Task<ActionOutput> ApproveOrRejectRow(string userId, int id, string currentScreenName, bool isApprove)
        {
            {
                ActionOutput output = new ActionOutput();
                DataConnection dataConnection;
                DataConnectionParams<DynamicParameter> dataConnectionParams;
                List<List<IDictionary<string, object>>> lstlstDict;
                try
                {
                    dataConnection = new DataConnection(ConfigurationManager.ConnectionStrings["IEMQS"].ConnectionString);
                    string procedureName = "[dbo].[SP_PCR_Approve_Reject_PCL_Workflow]";

                    dataConnectionParams = new DataConnectionParams<DynamicParameter>(DataConnectionTypeEnum.StoredProcedure)
                    {
                        Params = new List<DynamicParameter>()
                        {
                            new DynamicParameter() { DbType = DbType.String, Name = "PSNo", Value = userId },
                            new DynamicParameter() { DbType = DbType.Int32, Name = "Id", Value = id },
                            new DynamicParameter() { DbType = DbType.String, Name = "CurrentScreen", Value = currentScreenName },
                            new DynamicParameter() { DbType = DbType.Boolean, Name = "Approve", Value = isApprove },
                        },
                        Query = procedureName
                    };
                    lstlstDict = (await dataConnection.QueryDynamicMultipleAsync(dataConnectionParams)).ToList();
                    IDictionary<string, object> dict = lstlstDict.FirstOrDefault().FirstOrDefault();
                    int status = Convert.ToInt32(dict["Status"].ToString());
                    if (status == 1)
                    {
                        output.Result = OutputType.Success;
                        if (isApprove == true)
                        {
                            output.Message = ApproveRowSuccess;
                        }
                        else
                        {
                            output.Message = RejectRowSuccess;
                        }
                    }
                    else
                    {
                        output.Result = OutputType.Warning;
                        output.Message = Convert.ToString(dict["MESSAGE"]);
                    }

                }
                catch (SqlException ex)
                {
                    output.Result = OutputType.Error;
                    output.Message = ex.Message;
                }
                catch (Exception ex)
                {
                    output.Result = OutputType.Error;
                    output.Message = ex.Message;
                }
                return output;
            }
        }

        #endregion
        public async Task<ActionOutput> BeforeReWrite<T>(
            clsImplementationEnum.Callers caller,
            List<T> values,
            string userName)
        {
            ActionOutput output = new ActionOutput();
            DataConnection dataConnection;
            DataConnectionParams<DynamicParameter> dataConnectionParams;
            List<List<IDictionary<string, object>>> lstlstDict; //will see later
            try
            {
                dataConnection = new DataConnection(ConfigurationManager.ConnectionStrings["IEMQS"].ConnectionString);
                string procedureName = string.Empty;
                switch (caller)
                {
                    case clsImplementationEnum.Callers.GeneratePCR:
                        {
                            procedureName = "[dbo].[]";//TBA
                            break;
                        }
                    case clsImplementationEnum.Callers.MaterialPlanner:
                        {
                            procedureName = "[dbo].[SP_PCR_Validate_Update_Material_Planner_Data]";//TBA
                            break;
                        }
                }
                dataConnectionParams = new DataConnectionParams<DynamicParameter>(DataConnectionTypeEnum.StoredProcedure)
                {
                    Params = new List<DynamicParameter>()
                        {
                            new DynamicParameter() { DbType = DbType.String, Name = "UserId", Value = userName },
                            new DynamicParameter() { DbType = DbType.Xml, Name = "Data", Value = GetXml(values) }
                        },
                    Query = procedureName
                };
                lstlstDict = (await dataConnection.QueryDynamicMultipleAsync(dataConnectionParams)).ToList();
                IDictionary<string, object> dict = lstlstDict.FirstOrDefault().FirstOrDefault();
                int status = Convert.ToInt32(dict["Status"].ToString());
                if (status == 1)
                {
                    output.Result = OutputType.Success;
                }
                else
                {
                    output.Result = OutputType.Warning;
                    output.Message = Convert.ToString(dict["MESSAGE"]);
                }

            }
            catch (SqlException ex)
            {
                output.Result = OutputType.Error;
                output.Message = ex.Message;
            }
            catch (Exception ex)
            {
                output.Result = OutputType.Error;
                output.Message = ex.Message;
            }
            return output;
        }

        private string GetXml<T>(List<T> values)
        {
            var emptyNamepsaces = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
            var serializer = new XmlSerializer(values.GetType());
            var settings = new XmlWriterSettings
            {
                Indent = true,
                OmitXmlDeclaration = true
            };

            using (var stream = new StringWriter())
            using (var writer = XmlWriter.Create(stream, settings))
            {
                serializer.Serialize(writer, values, emptyNamepsaces);
                var temp = stream.ToString();
                return stream.ToString();
            }
        }

        public async Task<ActionOutput> AddOrEditRows<T>(
            clsImplementationEnum.Callers caller,
            List<T> values,
            string userName,
            int mode,
            bool isDuplicate
            )
        {
            ActionOutput output = new ActionOutput();
            DataConnection dataConnection;
            DataConnectionParams<DynamicParameter> dataConnectionParams;
            try
            {
                dataConnection = new DataConnection(ConfigurationManager.ConnectionStrings["IEMQS"].ConnectionString);
                string procedureName = string.Empty;
                switch (caller)
                {
                    case clsImplementationEnum.Callers.GeneratePCR:
                        {
                            procedureName = "[dbo].[SP_PCR_Insert_Update_PCR_Data]";
                            break;
                        }
                    case clsImplementationEnum.Callers.MaterialPlanner:
                        {
                            procedureName = "[dbo].[SP_PCR_Insert_Update_Material_Planner_Data]"; // To Change Name
                            break;
                        }
                    case clsImplementationEnum.Callers.ReleasePCL:
                        {
                            procedureName = "[dbo].[SP_PCR_Update_Release_PCL_Data]";
                            break;
                        }
                    case clsImplementationEnum.Callers.MaintainReturnDistribution:
                        {
                            procedureName = "[dbo].[SP_PCR_Insert_Update_Delete_ReturnDistribution_Data]";
                            break;
                        }
                }
                dataConnectionParams = new DataConnectionParams<DynamicParameter>(DataConnectionTypeEnum.StoredProcedure)
                {
                    Params = new List<DynamicParameter>()
                        {
                            new DynamicParameter() { DbType = DbType.String, Name = "UserId", Value = userName },
                            new DynamicParameter() { DbType = DbType.Int32 , Name = "Mode", Value = mode },
                            new DynamicParameter() { DbType = DbType.Xml, Name = "Data", Value = GetXml(values) },
                            new DynamicParameter() { DbType = DbType.Boolean, Name = "IsDuplicate", Value = isDuplicate },
                        },
                    Query = procedureName
                };
                var response = (await dataConnection.ExecuteQueryWithDynamicAsync(dataConnectionParams)).ToList();
                if (response != null && response.Count == 1)
                {
                    int result = response.First();
                    if (result == 1)
                    {
                        output.Result = OutputType.Success;
                        output.Message = OperationSuccess;
                    }
                    else
                    {
                        output.Result = OutputType.Warning;
                    }
                }

            }
            catch (SqlException ex)
            {
                output.Result = OutputType.Error;
                output.Message = ex.Message;
            }
            catch (Exception ex)
            {
                output.Result = OutputType.Error;
                output.Message = ex.Message;
            }
            return output;
        }


        public async Task<List<Stock>> GetStockNumberList(string psNo, int pcrLineId)
        {
            DataConnectionParams connectionParams;
            DataConnection dataConnection;
            IEnumerable<Stock> managedList;
            try
            {
                dataConnection = new DataConnection(ConfigurationManager.ConnectionStrings["IEMQS"].ConnectionString);
                connectionParams = new DataConnectionParams(DataConnectionTypeEnum.StoredProcedure)
                {
                    Query = "[dbo].[SP_PCR_Get_StockNumberList]"
                };
                connectionParams.Params = new
                {
                    PSNo = psNo,
                    Id = pcrLineId
                };
                managedList = await dataConnection.QueryAsync<Stock>(connectionParams);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return managedList.ToList();


        }
    }
}