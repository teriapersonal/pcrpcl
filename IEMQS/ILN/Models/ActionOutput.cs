﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IEMQS.ILN.Models
{
    public class ActionOutput
    {
        public OutputType Result;
        public string Message;
    }
    public enum OutputType
    {
        Success = 0,
        Warning = 1,
        Error = 2

    }
}