﻿using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace IEMQS.WCFService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "IEMQSService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select IEMQSService.svc or IEMQSService.svc.cs at the Solution Explorer and start debugging.
    public class IEMQSService : IIEMQSService
    {
        public IEMQSEntitiesContext db = new IEMQSEntitiesContext();
        public string DoWork()
        {
            return "Test Ok";
        }

        public string GetWinNotification(string psno)
        {
            var count =  db.GLB010.Where(x => x.PSNo == psno && x.IsRead == false).Count();
            string Message = string.Empty;
            if (count > 0)
            { 
                Message = "You have " + count.ToString() + " pending notification.";
            }
            return Message;
        }
    }
}
