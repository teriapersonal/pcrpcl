//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class HTC002
    {
        public int LineId { get; set; }
        public int HeaderId { get; set; }
        public string Project { get; set; }
        public string Document { get; set; }
        public Nullable<int> RevNo { get; set; }
        public string SectionDescription { get; set; }
        public string FurnaceCharge { get; set; }
        public Nullable<int> Length { get; set; }
        public Nullable<int> Width { get; set; }
        public Nullable<int> Height { get; set; }
        public Nullable<decimal> Weight { get; set; }
        public string Furnace { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public Nullable<bool> IsHTRCreated { get; set; }
        public Nullable<int> HTRHeaderId { get; set; }
        public string IdenticalHTRHeaderIds { get; set; }
    
        public virtual HTC001 HTC001 { get; set; }
    }
}
