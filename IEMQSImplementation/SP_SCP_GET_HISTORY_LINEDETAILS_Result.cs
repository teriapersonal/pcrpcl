//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    
    public partial class SP_SCP_GET_HISTORY_LINEDETAILS_Result
    {
        public Nullable<long> ROW_NO { get; set; }
        public Nullable<int> TotalCount { get; set; }
        public int Id { get; set; }
        public Nullable<int> RefId { get; set; }
        public int LineId { get; set; }
        public int HeaderId { get; set; }
        public string Project { get; set; }
        public string Document { get; set; }
        public Nullable<int> RevNo { get; set; }
        public string ReferenceDrawing { get; set; }
        public string ScopeOfWork { get; set; }
        public string EstFor { get; set; }
        public Nullable<int> Weight { get; set; }
        public Nullable<int> RCCHours { get; set; }
        public Nullable<System.DateTime> ExpMatDelDate { get; set; }
        public Nullable<System.DateTime> ReqComDate { get; set; }
        public string SubConTo { get; set; }
        public string DeliverTo { get; set; }
        public string TaskManager { get; set; }
        public Nullable<bool> IsLNLine { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public string TaskDescription { get; set; }
    }
}
