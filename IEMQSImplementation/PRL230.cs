//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class PRL230
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PRL230()
        {
            this.PRL231 = new HashSet<PRL231>();
            this.PRL232 = new HashSet<PRL232>();
        }
    
        public int HeaderId { get; set; }
        public string ProtocolNo { get; set; }
        public string Project { get; set; }
        public string BU { get; set; }
        public string Location { get; set; }
        public string QualityProject { get; set; }
        public string EquipmentNo { get; set; }
        public string DrawingNo { get; set; }
        public Nullable<int> DrawingRevisionNo { get; set; }
        public string DCRNo { get; set; }
        public Nullable<System.DateTime> InspectionDate { get; set; }
        public string SeamNo { get; set; }
        public string StageCode { get; set; }
        public Nullable<int> StageSequence { get; set; }
        public Nullable<int> IterationNo { get; set; }
        public Nullable<int> RequestNoSequence { get; set; }
        public Nullable<long> RequestNo { get; set; }
        public string InspectionAgency { get; set; }
        public Nullable<System.DateTime> OfferDate { get; set; }
        public Nullable<int> ICLRevNo { get; set; }
        public string BoltHolesStraddled { get; set; }
        public string TolBoltHoles { get; set; }
        public string ActBoltHoles { get; set; }
        public string MaxTiltIn { get; set; }
        public string TolMaxTiltIn { get; set; }
        public string ActMaxTiltIn { get; set; }
        public string ReqOffset { get; set; }
        public string ActOffsetMin { get; set; }
        public string ActOffsetMax { get; set; }
        public string DisBetweenPairNozzles { get; set; }
        public string ReqDisBetween { get; set; }
        public string ActDisBetween { get; set; }
        public string ReqOrientationOfNozzle { get; set; }
        public string CircDistFrom { get; set; }
        public string ReqReqOrientation { get; set; }
        public string ActReqOrientation { get; set; }
        public string ElevationOfNozzleFrom { get; set; }
        public string ReqElevationOfNozzleFrom { get; set; }
        public string ActElevationOfNozzleFrom { get; set; }
        public string HeightFromVessel { get; set; }
        public string ReqHeightFrom { get; set; }
        public string ActheightFrom { get; set; }
        public string OffsetDistanceOfNozzles { get; set; }
        public string ReqOffsetDistanceOfNozzles { get; set; }
        public string ActOffsetDistanceOfNozzles { get; set; }
        public string CheckPoint1 { get; set; }
        public string CheckPoint2 { get; set; }
        public Nullable<System.DateTime> CheckPoint2_2 { get; set; }
        public string CheckPoint4 { get; set; }
        public string CheckPoint5 { get; set; }
        public string CheckPoint6 { get; set; }
        public string CheckPoint7 { get; set; }
        public string QCRemarks { get; set; }
        public string Result { get; set; }
        public string OutOfRoundness { get; set; }
        public string OutByOutOfRoundness { get; set; }
        public string ReqCladStripingWidth { get; set; }
        public string ReqCladStripingDepth { get; set; }
        public string ReqCladStripingOffset { get; set; }
        public string Note1 { get; set; }
        public string Note2 { get; set; }
        public string Note3 { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public string ReqFilledBy { get; set; }
        public Nullable<System.DateTime> ReqFilledOn { get; set; }
        public string ActFilledBy { get; set; }
        public Nullable<System.DateTime> ActFilledOn { get; set; }
        public string InspectedBy { get; set; }
        public Nullable<System.DateTime> InspectedOn { get; set; }
        public string HeightFromDrop { get; set; }
        public string SpotNoClad { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRL231> PRL231 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRL232> PRL232 { get; set; }
    }
}
