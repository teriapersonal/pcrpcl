//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    
    public partial class SP_MOM_GET_PROJECTWISE_MOM_INDEX_Result
    {
        public Nullable<long> ROW_NO { get; set; }
        public Nullable<int> TotalCount { get; set; }
        public string PROJECT { get; set; }
        public string CUSTOMER { get; set; }
        public Nullable<System.DateTime> CDD { get; set; }
        public Nullable<int> MOMCOUNT { get; set; }
        public string LASTMOMDATE { get; set; }
        public string PROJECTDESCRIPTION { get; set; }
        public Nullable<System.DateTime> ZERODATE { get; set; }
    }
}
