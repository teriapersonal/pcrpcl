//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class GLB004
    {
        public int LineId { get; set; }
        public int Id { get; set; }
        public string NumberGroup { get; set; }
        public string BU { get; set; }
        public string Location { get; set; }
        public string Series { get; set; }
        public string Description { get; set; }
        public Nullable<long> FirstFreeNo { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
    
        public virtual GLB003 GLB003 { get; set; }
    }
}
