//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class PRL194_6
    {
        public int LineId { get; set; }
        public Nullable<int> HeaderId { get; set; }
        public string SrNo { get; set; }
        public string CrownThicknessReadings1 { get; set; }
        public string CrownThicknessReadings2 { get; set; }
        public string CrownThicknessReadings3 { get; set; }
        public string CrownThicknessReadings4 { get; set; }
        public string CrownThicknessReadings5 { get; set; }
        public string CrownThicknessReadings6 { get; set; }
        public string CrownThicknessReadings7 { get; set; }
        public string CrownThicknessReadings8 { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
    
        public virtual PRL190 PRL190 { get; set; }
    }
}
