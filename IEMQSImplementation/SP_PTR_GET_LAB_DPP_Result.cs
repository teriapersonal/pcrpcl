//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    
    public partial class SP_PTR_GET_LAB_DPP_Result
    {
        public Nullable<long> ROW_NO { get; set; }
        public Nullable<int> TotalCount { get; set; }
        public int HeaderId { get; set; }
        public string PTRNo { get; set; }
        public string LabId { get; set; }
        public string CouponCategory { get; set; }
        public Nullable<System.DateTime> RequestedDate { get; set; }
        public Nullable<System.DateTime> CouponReceiptDate { get; set; }
        public Nullable<System.DateTime> TestCompletionDate { get; set; }
        public Nullable<int> DayTaken { get; set; }
    }
}
