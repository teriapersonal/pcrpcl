//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    
    public partial class SP_QMS_GET_SEAM_FOR_INSPECTION_CERTIFICATE_GENERATE_Result
    {
        public string QualityProject { get; set; }
        public string SeamNo { get; set; }
        public string StageCode { get; set; }
        public string NDETechniqueNo { get; set; }
        public Nullable<int> NDETechniqueRevisionNo { get; set; }
        public Nullable<long> RowNo { get; set; }
    }
}
