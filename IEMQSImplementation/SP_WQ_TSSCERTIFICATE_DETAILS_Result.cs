//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    
    public partial class SP_WQ_TSSCERTIFICATE_DETAILS_Result
    {
        public Nullable<long> ROW_NO { get; set; }
        public Nullable<int> TotalCount { get; set; }
        public int Id { get; set; }
        public string MaintainedBy { get; set; }
        public Nullable<System.DateTime> MaintainedOn { get; set; }
        public string Location { get; set; }
        public string SpecificationNotes { get; set; }
        public string WQTNo { get; set; }
        public string WelderName { get; set; }
        public string Shop { get; set; }
        public string WelderStampNo { get; set; }
        public Nullable<System.DateTime> DateOfWelding { get; set; }
        public string WPSNo { get; set; }
        public string JoinType { get; set; }
        public bool TestCoupon { get; set; }
        public string WeldingProcessEsQualified { get; set; }
        public string WeldingTypeQualified { get; set; }
        public string WeldingType { get; set; }
        public Nullable<System.DateTime> ValueUpto { get; set; }
        public string Remarks { get; set; }
        public Nullable<System.DateTime> LastWeldDate { get; set; }
        public string JointType { get; set; }
    }
}
