//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    
    public partial class SP_PLN_Get_Checklist_Result
    {
        public string Plan { get; set; }
        public int Id { get; set; }
        public string RelatedTo { get; set; }
        public string CheckPoint { get; set; }
        public Nullable<int> Yes_No { get; set; }
        public Nullable<bool> IsLatest { get; set; }
    }
}
