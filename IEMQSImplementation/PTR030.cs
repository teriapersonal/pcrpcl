//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class PTR030
    {
        public int HeaderId { get; set; }
        public string ReportNo { get; set; }
        public Nullable<System.DateTime> DateOfRecived { get; set; }
        public Nullable<System.DateTime> DateOfTesting { get; set; }
        public string NameOfItem { get; set; }
        public string Process { get; set; }
        public string ProjectNo { get; set; }
        public string FillerWire { get; set; }
        public string PTRNo { get; set; }
        public string Position { get; set; }
        public string TestDescription { get; set; }
        public string BrandName { get; set; }
        public string MaterialSpec { get; set; }
        public string BatchNo { get; set; }
        public string Thickness { get; set; }
        public string WelderId { get; set; }
        public string SeamNo { get; set; }
        public string HeatNo { get; set; }
        public string PqrNo { get; set; }
        public string WpsNo { get; set; }
        public string CodeSpec { get; set; }
        public string htDuration { get; set; }
        public string TestMethod { get; set; }
        public string ChemicalResultLocation { get; set; }
        public string InstrumentUsed { get; set; }
        public string CRMUsed { get; set; }
        public string TempValue { get; set; }
        public string HumidityValue { get; set; }
        public string Remarks1 { get; set; }
        public string Remarks2 { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public Nullable<int> PTRHeaderId { get; set; }
    }
}
