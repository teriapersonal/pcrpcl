//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class PTR012_Log
    {
        public int LineId { get; set; }
        public Nullable<int> HeaderId { get; set; }
        public string SrNo { get; set; }
        public string CouponId { get; set; }
        public string Samplelocation { get; set; }
        public string Notchlocation { get; set; }
        public string SampleDirectionNotchPosition { get; set; }
        public string Testtemp { get; set; }
        public string AbsorbedEnergyinJoules1 { get; set; }
        public string AbsorbedEnergyinJoules2 { get; set; }
        public string AbsorbedEnergyinJoules3 { get; set; }
        public string Avg { get; set; }
        public string LEinmm1 { get; set; }
        public string LEinmm2 { get; set; }
        public string LEinmm3 { get; set; }
        public string PS1 { get; set; }
        public string PS2 { get; set; }
        public string PS3 { get; set; }
        public string SpecimenBroken { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public string Results { get; set; }
        public string CouponTitle { get; set; }
    
        public virtual PTR012 PTR012 { get; set; }
    }
}
