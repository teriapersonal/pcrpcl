//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class QMS052
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public QMS052()
        {
            this.QMS052_1 = new HashSet<QMS052_1>();
        }
    
        public int HardnessId { get; set; }
        public Nullable<int> HeaderId { get; set; }
        public Nullable<int> RefRequestId { get; set; }
        public string Instrument { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public string Inspectedby { get; set; }
        public Nullable<System.DateTime> InspectedOn { get; set; }
    
        public virtual QMS040 QMS040 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<QMS052_1> QMS052_1 { get; set; }
        public virtual QMS050 QMS050 { get; set; }
    }
}
