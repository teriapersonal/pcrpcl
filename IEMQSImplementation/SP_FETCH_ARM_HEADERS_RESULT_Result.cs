//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    
    public partial class SP_FETCH_ARM_HEADERS_RESULT_Result
    {
        public Nullable<long> ROW_NO { get; set; }
        public Nullable<int> TotalCount { get; set; }
        public int HeaderId { get; set; }
        public string Project { get; set; }
        public string Status { get; set; }
        public string ContractNo { get; set; }
        public string Customer { get; set; }
        public Nullable<int> RevNo { get; set; }
        public string ARMSet { get; set; }
        public string ARMType { get; set; }
        public string ARMDesc { get; set; }
        public string ARMRevision { get; set; }
        public string ReasonOfRevsion { get; set; }
        public Nullable<int> DocNo { get; set; }
        public string SubmittedBy { get; set; }
        public Nullable<System.DateTime> SubmittedOn { get; set; }
        public string ApprovedBy { get; set; }
        public Nullable<System.DateTime> ApprovedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public string ReviewedBy { get; set; }
        public Nullable<System.DateTime> ReviewedOn { get; set; }
    }
}
