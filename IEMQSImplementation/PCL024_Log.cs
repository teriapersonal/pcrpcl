//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class PCL024_Log
    {
        public int Id { get; set; }
        public int RefId { get; set; }
        public int LineId { get; set; }
        public int HeaderId { get; set; }
        public string Project { get; set; }
        public string Document { get; set; }
        public string ReactorID { get; set; }
        public Nullable<double> ReactorThk1 { get; set; }
        public Nullable<double> ReactorTLTL { get; set; }
        public Nullable<double> ReactorLg { get; set; }
        public Nullable<double> ReactorFabWt { get; set; }
        public Nullable<double> ReactorHydroWt { get; set; }
        public Nullable<double> ReactorMaxShellWd { get; set; }
        public bool ReactoreDesigned { get; set; }
        public bool ShellConst1Seg { get; set; }
        public bool ShellConst2Seg { get; set; }
        public bool ShellConst3Seg { get; set; }
        public bool ShellConst4Seg { get; set; }
        public bool TypeRollCold { get; set; }
        public bool TypeRollWarm { get; set; }
        public bool TypeRollHot { get; set; }
        public bool GasCuttingPreHeat { get; set; }
        public bool GasCuttingPreHeatNR { get; set; }
        public bool CSeamWEPPlate { get; set; }
        public bool CSeamWEPAfter { get; set; }
        public bool PTCRequiredforLS { get; set; }
        public bool PTCRequiredforCS { get; set; }
        public bool PTCRequiredforDend { get; set; }
        public bool PTCRequiredforDendShell { get; set; }
        public bool PTCRequiredforSpool { get; set; }
        public bool ISRRequiredforLS { get; set; }
        public bool ISRRequiredforCS { get; set; }
        public bool ISRRequiredforNozzle { get; set; }
        public bool ISRRequiredforCone { get; set; }
        public bool ISRRequiredforHead { get; set; }
        public bool PTMTAfterWelding { get; set; }
        public bool PTMTAfterOverlay { get; set; }
        public bool PTMTAfterPWHT { get; set; }
        public bool PTMTAfterHydro { get; set; }
        public bool RTAfterWelding { get; set; }
        public bool RTAfterOverlay { get; set; }
        public bool RTAfterPWHT { get; set; }
        public bool RTAfterHydro { get; set; }
        public bool UTAfterRecordable { get; set; }
        public bool UTAfterConventional { get; set; }
        public bool UTAfterWelding { get; set; }
        public bool UTAfterOverlay { get; set; }
        public bool UTAfterPWHT { get; set; }
        public bool UTAfterHydro { get; set; }
        public bool FerAfterWelding { get; set; }
        public bool FerAfterOverlay { get; set; }
        public bool FerAfterPWHT { get; set; }
        public bool FerAfterHydro { get; set; }
        public bool SOTESSCSingle { get; set; }
        public bool SOTESSCDouble { get; set; }
        public bool SOTESSCClade { get; set; }
        public bool SOTESSCOlayNR { get; set; }
        public bool DCPetalWCrown { get; set; }
        public bool DC4PetalWoCrown { get; set; }
        public bool DCSinglePeice { get; set; }
        public bool DCTwoHalves { get; set; }
        public bool DRCFormedPetals { get; set; }
        public bool DRCFormedSetup { get; set; }
        public bool DRCFormedWelded { get; set; }
        public bool DRCFormedOutside { get; set; }
        public bool DOTESSCSingle { get; set; }
        public bool DOTESSCDouble { get; set; }
        public bool DOTESSCClade { get; set; }
        public bool DOTESSCOlayNR { get; set; }
        public bool SVJYring { get; set; }
        public bool SVJBuildup { get; set; }
        public bool SVJLapJoint { get; set; }
        public bool YRTForgedShell { get; set; }
        public bool YRTForgedSeg { get; set; }
        public bool YRTReadimade { get; set; }
        public bool YRTPlate { get; set; }
        public bool YRTNR { get; set; }
        public bool TSRWeldBuildup { get; set; }
        public bool TSRForgedShell { get; set; }
        public bool TSRPlateWOlay { get; set; }
        public bool TSRPlate { get; set; }
        public bool TSRSSPlate { get; set; }
        public bool TSRNoofGussets { get; set; }
        public bool TSRNR { get; set; }
        public bool NFFRaised { get; set; }
        public bool NFFGroove { get; set; }
        public bool NFFOther { get; set; }
        public bool NGFBfPWHT { get; set; }
        public bool NGFAfPWHT { get; set; }
        public bool NGFNR { get; set; }
        public bool SpoolMatSS { get; set; }
        public bool SpoolMatLAS { get; set; }
        public bool SpoolMatCS { get; set; }
        public bool SpoolMatOth { get; set; }
        public string SpoolMatOthVal { get; set; }
        public bool JSInConnel { get; set; }
        public bool JSBiMetalic { get; set; }
        public bool JSCladRestoration { get; set; }
        public bool SSWBfPWHT { get; set; }
        public bool SSWAfPWHT { get; set; }
        public bool SSWBoth { get; set; }
        public bool VessleSSkirt { get; set; }
        public bool VessleSSupp { get; set; }
        public bool VessleSLegs { get; set; }
        public bool VessleSSaddle { get; set; }
        public bool SkirtMadein1 { get; set; }
        public bool SkirtMadein2 { get; set; }
        public bool FPNWeldNuts { get; set; }
        public bool FPNLeaveRow { get; set; }
        public bool PWHTSinglePiece { get; set; }
        public bool PWHTLSRJoint { get; set; }
        public double PWHTNoSec { get; set; }
        public bool PTCMTCptc { get; set; }
        public bool PTCMTCmtc { get; set; }
        public bool PTCMTCptcSim { get; set; }
        public bool PTCMTCmtcSim { get; set; }
        public bool SaddlesTrans { get; set; }
        public bool SaddlesAddit { get; set; }
        public bool PWHTinFurnacePFS { get; set; }
        public bool PWHTinFurnaceHFS1 { get; set; }
        public bool PWHTinFurnaceLEMF { get; set; }
        public string PWHTinFurnaceName { get; set; }
        public bool SectionWtMore230 { get; set; }
        public bool SectionWt200To230 { get; set; }
        public bool SectionWt150To200 { get; set; }
        public bool SectionWtLess150 { get; set; }
        public bool SectionWtSingle { get; set; }
        public bool SectionWt2Sec { get; set; }
        public Nullable<double> SectionWtNoSec { get; set; }
        public bool ARMRequired { get; set; }
        public bool ARMRT { get; set; }
        public bool ARMPWHT { get; set; }
        public bool ARMSB { get; set; }
        public bool CLRHLess30ppm { get; set; }
        public bool CLRHMore30ppm { get; set; }
        public Nullable<double> CLRHppm { get; set; }
        public string CLRHOther { get; set; }
        public bool TemplateForReq { get; set; }
        public bool TemplateForNReq { get; set; }
        public bool N2FillReq { get; set; }
        public bool N2FillNotReq { get; set; }
        public bool N2FillOther { get; set; }
        public string N2FillOtherVal { get; set; }
        public bool ESTPlate { get; set; }
        public bool ESTShell { get; set; }
        public bool ESTSection { get; set; }
        public bool ESTEquip { get; set; }
        public bool ISTPlate { get; set; }
        public bool ISTShell { get; set; }
        public bool ISTSection { get; set; }
        public bool ISTEquip { get; set; }
        public bool ISTAfBfPWHT { get; set; }
        public bool ISTAfHydro { get; set; }
        public bool CDNSS { get; set; }
        public bool CDNLAS { get; set; }
        public bool CDNCS { get; set; }
        public bool CDNNotReq { get; set; }
        public bool MaxSBLess1_38 { get; set; }
        public bool MaxSBLess1_78 { get; set; }
        public bool MaxSBLess2_14 { get; set; }
        public bool MaxSBLess4 { get; set; }
        public bool MaxSBMore4 { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public string ApprovedBy { get; set; }
        public Nullable<System.DateTime> ApprovedOn { get; set; }
        public string TypeRollWarmVal { get; set; }
        public string TypeRollHotVal { get; set; }
        public string TSRNoofGussetsVal { get; set; }
        public string SaddlesTransVal { get; set; }
        public string SaddlesAdditVal { get; set; }
        public Nullable<bool> ISRNA { get; set; }
        public Nullable<bool> RTAfterNA { get; set; }
        public Nullable<bool> TOFDAfterWelding { get; set; }
        public Nullable<bool> TOFDAfterOverlay { get; set; }
        public Nullable<bool> TOFDAfterPWHT { get; set; }
        public Nullable<bool> TOFDAfterHydro { get; set; }
        public Nullable<bool> TOFDNA { get; set; }
        public string TypeRollHotmin { get; set; }
    }
}
