//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class QMS071
    {
        public int LineId { get; set; }
        public int SpotId { get; set; }
        public int RequestId { get; set; }
        public string QualityProject { get; set; }
        public string Project { get; set; }
        public string BU { get; set; }
        public string Location { get; set; }
        public string SeamNo { get; set; }
        public string StageCode { get; set; }
        public Nullable<int> StageSequence { get; set; }
        public Nullable<int> IterationNo { get; set; }
        public Nullable<long> RequestNo { get; set; }
        public Nullable<int> RequestNoSequence { get; set; }
        public Nullable<int> SpotNumber { get; set; }
        public Nullable<int> ProbeAngle { get; set; }
        public string ScanningDirection { get; set; }
        public Nullable<double> EffReflectorSize { get; set; }
        public Nullable<int> Amplitude { get; set; }
        public Nullable<int> BeamPath { get; set; }
        public Nullable<int> XAt { get; set; }
        public Nullable<int> YfromReferenceLine { get; set; }
        public Nullable<double> MinDepth { get; set; }
        public Nullable<double> MaxDepth { get; set; }
        public Nullable<int> Length { get; set; }
        public string LineRemark { get; set; }
        public string CommonRemark { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
    }
}
