//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    
    public partial class SP_FKMS_FR_GET_ALLOCATION_DETAILS_Result
    {
        public Nullable<long> ROW_NO { get; set; }
        public Nullable<int> TotalCount { get; set; }
        public string Project { get; set; }
        public int NodeId { get; set; }
        public int RefLineId { get; set; }
        public string Description { get; set; }
        public string NodeName { get; set; }
        public string NodeKey { get; set; }
        public string FixtureNo { get; set; }
        public string FixtureName { get; set; }
        public Nullable<int> AllocatedQty { get; set; }
        public Nullable<int> RequestedQty { get; set; }
        public Nullable<int> CompletedQty { get; set; }
        public Nullable<int> TotalQty { get; set; }
        public string Remarks { get; set; }
        public string NodeColor { get; set; }
        public string DeliverStatus { get; set; }
        public Nullable<System.DateTime> WorkCompletioncommittedbyLEMF { get; set; }
        public Nullable<int> OverallCompletedQty { get; set; }
        public Nullable<int> ReceivedQty { get; set; }
    }
}
