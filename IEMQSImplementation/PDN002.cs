//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class PDN002
    {
        public int LineId { get; set; }
        public int HeaderId { get; set; }
        public string Project { get; set; }
        public int IssueNo { get; set; }
        public string Description { get; set; }
        public string DocumentNo { get; set; }
        public Nullable<System.DateTime> TargetDate { get; set; }
        public Nullable<int> RevNo { get; set; }
        public Nullable<int> RefId { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditeOn { get; set; }
        public string Plan { get; set; }
        public string Status { get; set; }
        public string Department { get; set; }
        public Nullable<int> RefHistoryId { get; set; }
        public Nullable<bool> IsApplicable { get; set; }
        public string IssueStatus { get; set; }
    
        public virtual PDN001 PDN001 { get; set; }
    }
}
