//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class PRO130
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PRO130()
        {
            this.PRO131 = new HashSet<PRO131>();
        }
    
        public int HeaderId { get; set; }
        public string ProtocolNo { get; set; }
        public string InspectionCertificateNo { get; set; }
        public string MethodOfTest { get; set; }
        public string AnalyserMake { get; set; }
        public string AnalyserModel { get; set; }
        public string OperatorName { get; set; }
        public string Remarks { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public string MATERIAL { get; set; }
        public string PROCEDUREREVNO { get; set; }
        public string PMIREPORTNO { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRO131> PRO131 { get; set; }
    }
}
