//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class PRO235
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PRO235()
        {
            this.PRO236 = new HashSet<PRO236>();
        }
    
        public int HeaderId { get; set; }
        public string ProtocolNo { get; set; }
        public string ActualCfOfShell { get; set; }
        public string ReqElevation { get; set; }
        public string ToleranceReqArcLength { get; set; }
        public string ReqProjection { get; set; }
        public string ReqTilt { get; set; }
        public string ReqRootGap { get; set; }
        public string ReqRootFace { get; set; }
        public string ReqWepAngle { get; set; }
        public string ReqSize { get; set; }
        public string CheckPoint1 { get; set; }
        public string CheckPoint2 { get; set; }
        public string CheckPoint3 { get; set; }
        public string CheckPoint4 { get; set; }
        public string CheckPoint6 { get; set; }
        public string CheckPoint7 { get; set; }
        public Nullable<System.DateTime> CheckPoint7_2 { get; set; }
        public string QCRemarks { get; set; }
        public string Result { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRO236> PRO236 { get; set; }
    }
}
