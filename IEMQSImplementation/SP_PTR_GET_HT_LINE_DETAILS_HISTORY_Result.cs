//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    
    public partial class SP_PTR_GET_HT_LINE_DETAILS_HISTORY_Result
    {
        public Nullable<long> ROW_NO { get; set; }
        public Nullable<int> TotalCount { get; set; }
        public int Id { get; set; }
        public int RefId { get; set; }
        public int LineId { get; set; }
        public int HeaderId { get; set; }
        public string Project { get; set; }
        public string BU { get; set; }
        public string Location { get; set; }
        public Nullable<int> RevNo { get; set; }
        public string PTRNo { get; set; }
        public Nullable<int> CouponNo { get; set; }
        public string CouponID { get; set; }
        public string HTType1 { get; set; }
        public string HTTemp1 { get; set; }
        public string HTTime1 { get; set; }
        public string HTType2 { get; set; }
        public string HTTemp2 { get; set; }
        public string HTTime2 { get; set; }
        public string HTType3 { get; set; }
        public string HTTemp3 { get; set; }
        public string HTTime3 { get; set; }
        public string HTType4 { get; set; }
        public string HTTemp4 { get; set; }
        public string HTTime4 { get; set; }
        public string HTType5 { get; set; }
        public string HTTemp5 { get; set; }
        public string HTTime5 { get; set; }
        public string HTType6 { get; set; }
        public string HTTemp6 { get; set; }
        public string HTTime6 { get; set; }
    }
}
