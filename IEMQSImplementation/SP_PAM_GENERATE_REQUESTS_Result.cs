//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    
    public partial class SP_PAM_GENERATE_REQUESTS_Result
    {
        public Nullable<long> ROW_NO { get; set; }
        public Nullable<int> TotalCount { get; set; }
        public int Id { get; set; }
        public string ContractNo { get; set; }
        public string ConDesc { get; set; }
        public string Customer { get; set; }
        public string CustomerDesc { get; set; }
        public Nullable<System.DateTime> RequestedOn { get; set; }
        public Nullable<System.DateTime> POExpectedOn { get; set; }
        public string RequestedBy { get; set; }
        public string BUHead { get; set; }
        public string Status { get; set; }
        public string Comments { get; set; }
    }
}
