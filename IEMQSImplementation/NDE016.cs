//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class NDE016
    {
        public int SubLineId { get; set; }
        public int LineId { get; set; }
        public int HeaderId { get; set; }
        public string Project { get; set; }
        public string BU { get; set; }
        public string Location { get; set; }
        public int RevNo { get; set; }
        public int ZoneNo { get; set; }
        public int GroupNo { get; set; }
        public string Angle { get; set; }
        public string DepthCoverage { get; set; }
        public int FocalDepth { get; set; }
        public int TCGIndicationAmplitude { get; set; }
        public int StartElement { get; set; }
        public int NoofElements { get; set; }
        public Nullable<decimal> AngularIncrementalChange { get; set; }
        public string FocalPlane { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public Nullable<double> ReferencegaindB { get; set; }
    
        public virtual NDE015 NDE015 { get; set; }
    }
}
