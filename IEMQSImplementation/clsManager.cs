﻿using IEMQSImplementation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace IEMQSImplementation
{
    public partial class clsManager : clsBase
    {
        /// <summary>
        /// Check for page access using login name and contoller name
        /// </summary>
        /// <param name="currentArea"></param>
        /// <param name="currentController"></param>
        /// <param name="currentAction"></param>
        /// <param name="currentuser"></param>
        /// <returns></returns>
        public int GetpageAccess(string currentArea, string currentController, string currentAction, string currentuser)
        {
            int Count = 0;
            System.Data.Entity.Core.Objects.ObjectResult<int?> obj = db.SP_ATH_VALIDATE_PROCESS_PSNO(currentuser, currentArea, currentController, currentAction);

            if (obj != null)
            {
                string strOutput = obj.FirstOrDefault().Value.ToString();

                int.TryParse(strOutput, out Count);
            }
            return Count;
        }
        public string Momname(int MOMNo)
        {
            return db.MOM001.Where(x => x.MOMNo == MOMNo).Select(x => x.Venue).FirstOrDefault();
        }
        public string GetUserNameFromPsNo(string psno)
        {
            return db.COM003.Where(x => x.t_psno == psno && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
        }

        public string GetUserNameFromPsNoForDIN(string psno)
        {
            return db.COM003.Where(x => x.t_psno == psno).Select(x => x.t_name).FirstOrDefault();
        }

        public string GetMailIdFromPsNo(string psno)
        {
            return db.COM007.Where(x => x.t_emno == psno).Select(x => x.t_mail).FirstOrDefault();
        }
        public string GetStatusFromHeaderId(int headerId)
        {
            return db.IMB001.Where(x => x.HeaderId == headerId).Select(x => x.Status).FirstOrDefault();
        }
        
        public string GetISFromPSNO(string psno)
        {
            return db.COM003.Where(x => x.t_psno == psno && x.t_actv == 1).Select(x => x.t_iasp).FirstOrDefault();
        }

        public void AlphaNumericalSort(string[] ar)
        {
            Regex rgx = new Regex("([^0-9]*)([0-9]+)");
            Array.Sort(ar, (a, b) =>
            {
                var ma = rgx.Matches(a.Replace("/", ""));
                var mb = rgx.Matches(b.Replace("/", ""));
                for (int i = 0; i < ma.Count; ++i)
                {
                    int ret = ma[i].Groups[1].Value.CompareTo(mb[i].Groups[1].Value);
                    if (ret != 0)
                        return ret;

                    ret = int.Parse(ma[i].Groups[2].Value) - int.Parse(mb[i].Groups[2].Value);
                    if (ret != 0)
                        return ret;
                }

                return 0;
            });
        }

        public string MakeDefaultWhere(string UserName, string strBUColumnName = "", string strLocColumnName = "")
        {
            string strWhere = string.Empty;
            var lstBusLoc = db.ATH001.Where(x => x.Employee == UserName).Select(x => new { x.BU, x.Location }).Distinct().ToList();
            if (lstBusLoc != null && lstBusLoc.Count > 0)
            {
                string strBus = string.Join(",", lstBusLoc.Select(x => x.BU).Distinct().ToList()).ToString();
                string strLoc = string.Join(",", lstBusLoc.Select(x => x.Location).Distinct().ToList()).ToString();
                if (strBus.Length > 0)
                {
                    if (!string.IsNullOrWhiteSpace(strBUColumnName))
                    {
                        strWhere += MakeStringInCondition(strBUColumnName, strBus);
                    }
                    else
                    {
                        strWhere += MakeStringInCondition("BU", strBus);
                    }
                }
                if (strLoc.Length > 0)
                {
                    if (!string.IsNullOrWhiteSpace(strLocColumnName))
                    {
                        strWhere += MakeStringInCondition(strLocColumnName, strLoc);
                    }
                    else
                    {
                        strWhere += MakeStringInCondition("Location", strLoc);
                    }
                }
            }
            return strWhere;
        }

        public string MakeDefaultLEMWhere(string UserName, string strBUColumnName = "", string strLocColumnName = "")
        {
            string strWhere = string.Empty;
            var lstBusLoc = db.ATH001.Where(x => x.Employee == UserName).Select(x => new { x.BU, x.Location }).Distinct().ToList();
            if (lstBusLoc != null && lstBusLoc.Count > 0)
            {
                string strBus = string.Empty + "," + string.Join(",", lstBusLoc.Select(x => x.BU).Distinct().ToList()).ToString();
                string strLoc = string.Join(",", lstBusLoc.Select(x => x.Location).Distinct().ToList()).ToString();
                if (lstBusLoc.Any(x => x.Location == "HZW"))
                {
                    strLoc += "," + "LEM";
                }
                if (strBus.Length > 0)
                {
                    if (!string.IsNullOrWhiteSpace(strBUColumnName))
                    {
                        strWhere += MakeStringInCondition(strBUColumnName, strBus);
                    }
                    else
                    {
                        strWhere += MakeStringInCondition("BU", strBus);
                    }
                }
                if (strLoc.Length > 0)
                {
                    if (!string.IsNullOrWhiteSpace(strLocColumnName))
                    {
                        strWhere += MakeStringInCondition(strLocColumnName, strLoc);
                    }
                    else
                    {
                        strWhere += MakeStringInCondition("Location", strLoc);
                    }
                }
            }
            return strWhere;
        }
        public string MakeStringInCondition(string columnName, string stringValues)
        {
            string strStringFormat = string.Empty;
            string[] arrIntValues = stringValues.Split(',').ToArray();
            StringBuilder sb = new StringBuilder();
            List<string> lstValues = new List<string>();
            if (arrIntValues.Length > 0)
            {
                sb.Append(" and " + columnName + " IN");
                sb.Append("(");
                for (int i = 0; i < arrIntValues.Length; i++)
                {
                    string value = "'" + arrIntValues[i] + "'";
                    lstValues.Add(value);
                }
                sb.Append(string.Join(",", lstValues.Select(n => n.ToString()).ToArray()));
                sb.Append(")");
            }
            strStringFormat = sb.ToString();
            return strStringFormat;
        }

        public bool CheckDate(String date)
        {
            try
            {
                DateTime dt = DateTime.Parse(date);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public string CheckListLineOperationYesNo(string Yes_No, int LineId, int HeaderId, bool IsDisable, string ActualId)
        {
            List<string> lstEncoderType = clsImplementationEnum.getChecklistYesNo().ToList();
            string ddl = string.Empty;
            ddl = "<select class='form-control yesno' id='ddlYesNo' name='ddlYesNo' sHeaderId='" + HeaderId.ToString() + "' sLineId='" + LineId.ToString() + "' sactualid='" + ActualId.ToString() + "' class='remarks' " + (IsDisable == true ? "disabled" : "") + ">";
            lstEncoderType.ForEach(x => { ddl += "<option value='" + x.ToString() + "' " + (Yes_No.ToLower() == x.ToString().ToLower() ? " selected " : "") + ">" + x.ToString() + "</option>"; });
            ddl += "</select>";
            return ddl;
        }
        public string CheckListLineOperationRemarks(string Remarks, int LineId, int HeaderId, bool IsDisable)
        {
            return "<input class='form-control col-md-3 remarks' id='txtRemark" + LineId.ToString() + "' maxlength='100' name='txtRemark" + LineId.ToString() + "' value='" + Remarks + "' type='text' " + (IsDisable == true ? "disabled" : "") + " />";
        }

        public string GetStageType(string BU, string Location, string StageCode)
        {
            QMS002 objQMS002 = db.QMS002.FirstOrDefault(x => x.BU == BU && x.Location == Location && x.StageCode == StageCode);
            if (objQMS002 != null)
            {
                return objQMS002.StageType;
            }
            return "";
        }

        //Observation 15957 on 02-08-2018 by Ajay Chauhan
        public List<clsNDETechniques> getPTMTNDETechniques(string project, string qualityProject, string BU, string Location)
        {
            List<clsNDETechniques> lstNDETechniques = new List<clsNDETechniques>();
            string TecStatus = clsImplementationEnum.NDETechniqueStatus.Approved.GetStringValue();
            lstNDETechniques.AddRange(db.NDE007_Log.Where(x => x.Status == TecStatus && x.QualityProject == qualityProject && x.Project == project && x.BU == BU && x.Location == Location).Select(x => new clsNDETechniques { TechniqueName = "PT", TechniqueNo = x.PTTechNo, TechniqueDisplayNo = x.TechniqueSheetNo, TechniqueRev = x.RevNo, TechniqueUrl = WebsiteURL + "/NDE/PTTechniqueMaster/ViewHistory?id=" + x.Id }).ToList());
            lstNDETechniques.AddRange(db.NDE008_Log.Where(x => x.Status == TecStatus && x.QualityProject == qualityProject && x.Project == project && x.BU == BU && x.Location == Location).Select(x => new clsNDETechniques { TechniqueName = "MT", TechniqueNo = x.MTTechNo, TechniqueDisplayNo = x.TechniqueSheetNo, TechniqueRev = x.RevNo, TechniqueUrl = WebsiteURL + "/NDE/MaintainMTTechnique/ViewHistory?id=" + x.Id }).ToList());
            return lstNDETechniques;
        }

        public List<clsNDETechniques> getNDETechniques(string project, string qualityProject, string stageCode, string BU, string Location)
        {

            //stageCode = stageCode.Split('-')[0].Trim();

            List<clsNDETechniques> lstNDETechniques = new List<clsNDETechniques>();
            List<QMS002> lstQMS002 = db.QMS002.Where(x => x.BU == BU && x.Location == Location && x.StageCode == stageCode).ToList();
            if (lstQMS002 != null && lstQMS002.Count > 0)
            {
                List<string> lstStageType = new List<string>();
                string TecStatus = clsImplementationEnum.NDETechniqueStatus.Approved.GetStringValue();
                foreach (QMS002 objQMS002 in lstQMS002)
                {
                    lstStageType.Add(objQMS002.StageType);
                }
                foreach (string strStageType in lstStageType)
                {
                    switch (strStageType.ToUpper())
                    {
                        case "PT":
                            lstNDETechniques = db.NDE007_Log.Where(x => x.Status == TecStatus && x.QualityProject == qualityProject && x.Project == project && x.BU == BU && x.Location == Location).Select(x => new clsNDETechniques { TechniqueName = "PT", TechniqueNo = x.PTTechNo, TechniqueDisplayNo = x.TechniqueSheetNo, TechniqueRev = x.RevNo, TechniqueUrl = WebsiteURL + "/NDE/PTTechniqueMaster/ViewHistory?id=" + x.Id }).ToList();
                            break;
                        case "MT":
                            lstNDETechniques = db.NDE008_Log.Where(x => x.Status == TecStatus && x.QualityProject == qualityProject && x.Project == project && x.BU == BU && x.Location == Location).Select(x => new clsNDETechniques { TechniqueName = "MT", TechniqueNo = x.MTTechNo, TechniqueDisplayNo = x.TechniqueSheetNo, TechniqueRev = x.RevNo, TechniqueUrl = WebsiteURL + "/NDE/MaintainMTTechnique/ViewHistory?id=" + x.Id }).ToList();
                            break;
                        case "RT":
                            lstNDETechniques = db.NDE009_Log.Where(x => x.Status == TecStatus && x.QualityProject == qualityProject && x.Project == project && x.BU == BU && x.Location == Location).Select(x => new clsNDETechniques { TechniqueName = "RT", TechniqueNo = x.RTTechNo, TechniqueDisplayNo = x.TechniqueSheetNo, TechniqueRev = x.RevNo, TechniqueUrl = WebsiteURL + "/NDE/MaintainRTTechnique/ViewHistory?id=" + x.Id }).ToList();
                            break;
                        case "UT":
                            List<clsNDETechniques> lstNDETechniquesAscan = db.NDE010_Log.Where(x => x.Status == TecStatus && x.QualityProject == qualityProject && x.Project == project && x.BU == BU && x.Location == Location).Select(x => new clsNDETechniques { TechniqueName = "ASCAN UT", TechniqueNo = x.UTTechNo, TechniqueDisplayNo = x.TechniqueSheetNo, TechniqueRev = x.RevNo, TechniqueUrl = WebsiteURL + "/NDE/MaintainScanUTTech/ViewLogDetails?Id=" + x.Id }).ToList();

                            if (lstNDETechniquesAscan != null && lstNDETechniquesAscan.Count > 0)
                                lstNDETechniques.AddRange(lstNDETechniquesAscan);

                            List<clsNDETechniques> lstNDETechniquesTOFD = db.NDE012_Log.Where(x => x.Status == TecStatus && x.QualityProject == qualityProject && x.Project == project && x.BU == BU && x.Location == Location).Select(x => new clsNDETechniques { TechniqueName = "TOFD UT", TechniqueNo = x.UTTechNo, TechniqueDisplayNo = x.TechniqueSheetNo, TechniqueRev = x.RevNo, TechniqueUrl = WebsiteURL + "/NDE/MaintainTOFDUTTech/ViewLogDetails?ID=" + x.Id }).ToList();

                            if (lstNDETechniquesTOFD != null && lstNDETechniquesTOFD.Count > 0)
                                lstNDETechniques.AddRange(lstNDETechniquesTOFD);

                            List<clsNDETechniques> lstNDETechniquesPAUT = db.NDE014_Log.Where(x => x.Status == TecStatus && x.QualityProject == qualityProject && x.Project == project && x.BU == BU && x.Location == Location).Select(x => new clsNDETechniques { TechniqueName = "PAUT", TechniqueNo = x.UTTechNo, TechniqueDisplayNo = x.TechniqueSheetNo, TechniqueRev = x.RevNo, TechniqueUrl = WebsiteURL + "/NDE/MaintainPAUTTechnique/ViewHistory?id=" + x.Id }).ToList();

                            if (lstNDETechniquesPAUT != null && lstNDETechniquesPAUT.Count > 0)
                                lstNDETechniques.AddRange(lstNDETechniquesPAUT);

                            break;
                        default:
                            break;
                    }
                }
            }

            return lstNDETechniques;
        }

        public string MakeHTMLDropDownControl(string classname, string controlName, string controlId, string controlValue, List<Tuple<string, string>> lstBindsOptions, bool isDefaultOption = false, int id = 0)
        {
            string strSelect = "<select name=\"" + controlName + "\" class=\"" + classname + "\" id=\"" + controlId + "\" spara=\"" + id + "\">";
            if (isDefaultOption)
            {
                strSelect += "<option value=\"\">Select</option>";
            }
            foreach (Tuple<string, string> item in lstBindsOptions)
            {

                if (controlValue == item.Item1)
                {
                    strSelect += "<option selected value=\"" + item.Item1 + "\">" + item.Item2 + "</option>";
                }
                else
                {
                    strSelect += "<option value=\"" + item.Item1 + "\">" + item.Item2 + "</option>";
                }
            }
            strSelect += "</select>";
            return strSelect;
        }

        public bool IsReviseEnabled(int PlaningDinId, int HeaderId, string DocumentNo)
        {
            var objPDN002 = db.PDN002.Where(x => x.HeaderId == PlaningDinId && x.RefId == HeaderId && x.DocumentNo == DocumentNo).FirstOrDefault();
            if (objPDN002 != null && objPDN002.IssueStatus != null)
            {
                if (objPDN002.IssueStatus.ToLower() == clsImplementationEnum.PlanningDinStatus.RELEASED.GetStringValue().ToLower())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public bool IsDeleteEnabled(PDN002 objPDN002)
        {
            //var objPDN002 = db.PDN002.Where(x => x.HeaderId == PlaningDinId && x.RefId == HeaderId && x.Plan == plan).FirstOrDefault();
            if (objPDN002 != null && objPDN002.IssueStatus != null)
            {
                if ((objPDN002.IssueStatus.ToLower() == clsImplementationEnum.PlanningDinStatus.RELEASED.GetStringValue().ToLower()
                    && objPDN002.Status == clsImplementationEnum.CommonStatus.Approved.GetStringValue())
                    ||
                    (objPDN002.Status == clsImplementationEnum.CommonStatus.SendForApprovel.GetStringValue())
                    )
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }

        public string MakeDatatableForSearch(string jsonSearchFilter)
        {
            string strWhereCondition = string.Empty;
            try
            {
                List<SearchFilter> searchflt = new JavaScriptSerializer().Deserialize<List<SearchFilter>>(jsonSearchFilter);
                foreach (var item in searchflt)
                {
                    if (!string.IsNullOrWhiteSpace(item.Value) && item.Value != "false")
                    {
                        //if (item.ColumnName.Contains(',')) // Multiple Table Field on signle Column and make it with OR operation
                        //{
                        //    strWhereCondition += " and ( 1=0 ";
                        //    foreach (var Cname in item.ColumnName.Split(','))
                        //    {
                        //        //if
                        //            strWhereCondition += MakeDatatableSearchWithMultipleCondition(item.FilterType, Cname, item.Value, item.DataType, "or");
                        //        //else
                        //        //    strWhereCondition += clsImplementationEnum.MakeDatatableSearchWithMultipleConditionForNumeric(item.FilterType, (item.DataType == "date" ? "CONVERT(date, " + Cname + ")" : Cname), item.Value, "or");
                        //    }
                        //    strWhereCondition += " ) ";
                        //}
                        //else
                        //{
                        if (item.ColumnName == "Revision" || item.ColumnName == "RevNo" || item.ColumnName == "SeamListRev" || item.ColumnName == "QualityIdRev" )
                        {
                            item.Value = item.Value.Replace("R", "");
                            item.Value = item.Value.Replace("r", "");
                        }
                        strWhereCondition += MakeDatatableSearchWithMultipleCondition(item.FilterType, item.ColumnName, item.Value, item.DataType);

                        /*if (item.DataType == "string")
                            strWhereCondition += clsImplementationEnum.MakeDatatableSearchWithMultipleCondition(item.FilterType, item.ColumnName, item.Value);
                        else
                            strWhereCondition += clsImplementationEnum.MakeDatatableSearchWithMultipleConditionForNumeric(item.FilterType, (item.DataType == "date" ? "CONVERT(date, " + item.ColumnName + ")" : item.ColumnName), item.Value);*/
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }
            return strWhereCondition;
        }

        public List<CategoryData> GetSubCatagorywithoutBULocation(string Key)
        {
            List<CategoryData> lstGLB002 = null;
            lstGLB002 = (from glb002 in db.GLB002
                         join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                         where glb001.Category.Equals(Key, StringComparison.OrdinalIgnoreCase) && glb002.IsActive == true //&& strLoc.Equals(glb002.Location)
                         select glb002).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Description }).Distinct().ToList();
            return lstGLB002;
        }
        /*
        public  string MakeDatatableSearchWithMultipleCondition(string filterstatus, string column_name, string column_value, string operation = "and")
        {
            string strLikeFormat = string.Empty;
            StringBuilder sb = new StringBuilder();
            if (operation == "or")
                sb.Append(" " + operation + " ");
            else
                sb.Append(" " + operation + " ( ");
            if (filterstatus == clsImplementationEnum.NumaricValidation.Equal.GetStringValue())
            {

                sb.Append(" " + column_name + " = '" + column_value + "' ");
            }

            else if (filterstatus == clsImplementationEnum.NumaricValidation.DoesNotEqual.GetStringValue())
            {
                sb.Append(" " + column_name + " != '" + column_value + "' ");

            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.StartsWith.GetStringValue())
            {
                sb.Append(" " + column_name + " like '" + column_value + "%'");

            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.GreaterThenorEquals.GetStringValue())
            {
                sb.Append(" " + column_name + " >= '" + column_value + "' ");

            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.LessThenorEquals.GetStringValue())
            {
                sb.Append(" " + column_name + " <= '" + column_value + "' ");

            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.GreaterThen.GetStringValue())
            {
                sb.Append(" " + column_name + " > '" + column_value + "' ");

            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.LessThen.GetStringValue())
            {
                sb.Append(" " + column_name + " < '" + column_value + "' ");

            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.Contains.GetStringValue())
            {
                sb.Append(" " + column_name + " like '%" + column_value + "%'");

            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.DoesNotContains.GetStringValue())
            {
                sb.Append(" " + column_name + " not like '%" + column_value + "%'");

            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.EndsWith.GetStringValue())
            {
                sb.Append(" " + column_name + " like '%" + column_value + "'");

            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.DoesNotEndsWith.GetStringValue())
            {
                sb.Append(" " + column_name + " not like '%" + column_value + "'");
            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.IsEmpty.GetStringValue())
            {
                sb.Append(" " + column_name + " is null   ");

            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.IsNotEmpty.GetStringValue())
            {
                sb.Append(" " + column_name + " is not null ");

            }
            else
            {
                sb.Append(" " + column_name + " like  '%" + column_value + "%'");
            }
            if (operation != "or")
            {
                sb.Append(") ");
            }
            strLikeFormat = sb.ToString();
            return strLikeFormat;
        }
        public  string MakeDatatableSearchWithMultipleConditionForNumeric(string filterstatus, string column_name, string column_value, string operation = "and")
        {
            string strLikeFormat = string.Empty;
            StringBuilder sb = new StringBuilder();
            sb.Append(" " + operation + " ( ");
            if (filterstatus == clsImplementationEnum.NumaricValidation.Equal.GetStringValue())
            {

                sb.Append(" " + column_name + " = '" + column_value + "' ");
            }

            else if (filterstatus == clsImplementationEnum.NumaricValidation.DoesNotEqual.GetStringValue())
            {
                sb.Append(" " + column_name + " != '" + column_value + "' ");

            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.StartsWith.GetStringValue())
            {
                sb.Append(" " + column_name + " like '" + column_value + "%'");

            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.GreaterThenorEquals.GetStringValue())
            {
                sb.Append(" " + column_name + " >= '" + column_value + "' ");

            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.LessThenorEquals.GetStringValue())
            {
                sb.Append(" " + column_name + " <= '" + column_value + "' ");

            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.GreaterThen.GetStringValue())
            {
                sb.Append(" " + column_name + " > '" + column_value + "' ");

            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.LessThen.GetStringValue())
            {
                sb.Append(" " + column_name + " < '" + column_value + "' ");

            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.Contains.GetStringValue())
            {
                sb.Append(" " + column_name + " like '%" + column_value + "%'");

            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.DoesNotContains.GetStringValue())
            {
                sb.Append(" " + column_name + " not like '%" + column_value + "%'");

            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.EndsWith.GetStringValue())
            {
                sb.Append(" " + column_name + " like '%" + column_value + "'");

            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.DoesNotEndsWith.GetStringValue())
            {
                sb.Append(" " + column_name + " not like '%" + column_value + "'");
            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.IsEmpty.GetStringValue())
            {
                sb.Append(" " + column_name + " is null   ");

            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.IsNotEmpty.GetStringValue())
            {
                sb.Append(" " + column_name + " is not null ");

            }
            else
            {
                sb.Append(" " + column_name + " = '" + column_value + "' ");
            }
            sb.Append(") ");
            strLikeFormat = sb.ToString();
            return strLikeFormat;
        }
        */
        public string MakeDatatableSearchWithMultipleCondition(string filterstatus, string column_name, string column_value, string column_type = "string", string operation = "and")
        {
            string strLikeFormat = string.Empty;
            StringBuilder sb = new StringBuilder();
            if (column_type == "date" || column_type == "daterange")
                column_name = "CONVERT(date, " + column_name + ")";
            sb.Append(" " + operation + " ( " + column_name + " ");
            if (filterstatus == clsImplementationEnum.NumaricValidation.Equal.GetStringValue())
            {
                sb.Append("= '" + column_value + "' ");
            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.DoesNotEqual.GetStringValue())
            {
                sb.Append("!= '" + column_value + "' ");
            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.StartsWith.GetStringValue())
            {
                sb.Append("like '" + column_value.Replace("%", "[%]").Replace("_", "[_]") + "%' ");
            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.GreaterThenorEquals.GetStringValue())
            {
                sb.Append(">= '" + column_value + "' ");
            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.LessThenorEquals.GetStringValue())
            {
                sb.Append("<= '" + column_value + "' ");
            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.GreaterThen.GetStringValue())
            {
                sb.Append("> '" + column_value + "' ");
            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.LessThen.GetStringValue())
            {
                sb.Append("< '" + column_value + "' ");
            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.Contains.GetStringValue())
            {
                sb.Append("like '%" + column_value.Replace("%", "[%]").Replace("_", "[_]") + "%'");
            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.DoesNotContains.GetStringValue())
            {
                sb.Append("not like '%" + column_value.Replace("%", "[%]").Replace("_", "[_]") + "%'");
            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.EndsWith.GetStringValue())
            {
                sb.Append("like '%" + column_value.Replace("%", "[%]").Replace("_", "[_]") + "'");
            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.DoesNotEndsWith.GetStringValue())
            {
                sb.Append("not like '%" + column_value.Replace("%", "[%]").Replace("_", "[_]") + "'");
            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.IsEmpty.GetStringValue())
            {
                sb.Append("is null OR " + column_name + " = '' ");
            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.IsNotEmpty.GetStringValue())
            {
                sb.Append("is not null AND " + column_name + " != '' ");
            }
            else // default by data type
            {
                if (column_value == null)
                {
                    sb.Append("is null");
                }
                else
                {
                    if (column_type == "string")
                    {
                        sb.Append("like '%" + column_value.Replace("%", "[%]").Replace("_", "[_]") + "%'");
                    }
                    else if (column_type == "daterange")
                    {
                        if (!string.IsNullOrWhiteSpace(column_value))
                        {
                            String[] slit = { "to" };
                            string[] date = column_value.Split(slit, StringSplitOptions.RemoveEmptyEntries);
                            string datefrom = "CONVERT(date, '" + getDateTimeyyyyMMdd(date[0]) + "')";
                            string dateto = "CONVERT(date, '" + getDateTimeyyyyMMdd(date[1]) + "')";
                            sb.Append(" >=  " + datefrom + " and " + column_name + " <= " + dateto);
                        }
                    }
                    else if (column_type == "fromtoRange")
                    {
                        if (!string.IsNullOrWhiteSpace(column_value))
                        {
                            String[] slit = { "||" };
                            string[] FromToValue = column_value.Split(slit, StringSplitOptions.RemoveEmptyEntries);
                            string strfrom = FromToValue[0];
                            string strto = FromToValue[1];
                            sb.Append("  between  '" + strfrom + "' and '" + strto + "'  OR " + column_name + "  between  '" + strto + "' and '" + strfrom + "'");
                        }
                    }
                    else if (column_type == "Select2")
                    {
                        if (!string.IsNullOrWhiteSpace(column_value))
                        {
                            sb.Append(" in (select value from dbo.fn_Split('" + column_value + "',','))");
                        }
                    }
                    else
                        sb.Append("= '" + column_value + "' ");
                }
            }

            sb.Append(") ");
            strLikeFormat = sb.ToString();
            return strLikeFormat;
        }

        public string getDateTimeyyyyMMdd(string date)
        {
            string returnDate = null;
            if (!string.IsNullOrEmpty(date) && !string.IsNullOrWhiteSpace(date))
            {
                string[] arr = date.Trim().Split('/');
                returnDate = Convert.ToInt32(arr[2]) + "-" + Convert.ToInt32(arr[1]) + "-" + Convert.ToInt32(arr[0]);
            }
            return returnDate;
        }
        public void UpdateIssueNo(PDN002 objPDN002, int? rev, string status, int refhistoryid = 0, bool IsIssueUpdate = false)
        {
            objPDN002.RevNo = rev;
            objPDN002.Status = status;
            if (IsIssueUpdate)
            {
                PDN002 fetchStatus = db.PDN002.Where(x => x.RefHistoryId == refhistoryid && x.RefId == objPDN002.RefId && x.DocumentNo == objPDN002.DocumentNo).FirstOrDefault();
                if (fetchStatus != null)
                {
                    objPDN002.RefHistoryId = refhistoryid;
                    objPDN002.IssueStatus = fetchStatus.IssueStatus;
                }
            }
        }
        #region Protocol Methods
        public bool IsSeamNP(string qualityproject, string project, string location, string seamno)
        {
            bool flag = false;
            var NonPressurePartSeam = Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.NonPressurePartSeam.GetStringValue()).Split(',').ToArray();
            //x.Location == location &&
            if (db.QMS012.Where(x => x.QualityProject == qualityproject && x.Project == project && x.SeamNo == seamno && NonPressurePartSeam.Contains(x.SeamCategory)).Any())
            {
                flag = true;
            }
            return flag;
        }
        // #NewProtocolLinkage
        public int LinkProtocolWithStage(string ProtocolType, string ProtocolNo, clsHelper.ProtocolHeaderData HeaderData, int? ProtocolHeaderId = null, string role = "")
        {
            int returnValue = 0;
            if (ProtocolType == clsImplementationEnum.ProtocolType.SETUP_AND_DIMENSION_REPORT_FOR_ROLLED_SHELL_LONGITUDINAL_SEAM.GetStringValue())
            {
                #region Add New Entry in PRL001 For PROTOCOL001
                PRL001 objPRL001 = new PRL001();
                objPRL001.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL001.QualityProject = HeaderData.QualityProject;
                objPRL001.EquipmentNo = HeaderData.EquipmentNo;
                objPRL001.DrawingNo = HeaderData.DrawingNo;
                objPRL001.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL001.DCRNo = HeaderData.DCRNo;
                objPRL001.SeamNo = HeaderData.SeamNo;
                objPRL001.InspectionAgency = HeaderData.InspectionAgency;
                objPRL001.Project = HeaderData.Project;
                objPRL001.BU = HeaderData.BU;
                objPRL001.Location = HeaderData.Location;
                objPRL001.StageCode = HeaderData.StageCode;
                objPRL001.StageSequence = HeaderData.StageSequence;
                objPRL001.CreatedBy = objClsLoginInfo.UserName;
                objPRL001.CreatedOn = DateTime.Now;
                db.PRL001.Add(objPRL001);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL001

                    PRO001 objPRO001 = db.PRO001.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO001 != null)
                    {
                        CopyProtocolData("PRO001", "PRL001", "PRL001.HeaderId=" + objPRL001.HeaderId + " AND PRO001.HeaderId=" + objPRO001.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO002", "PRL002", "HeaderId=" + objPRO001.HeaderId, "LineId,EditedBy,EditedOn", objPRL001.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO003", "PRL003", "HeaderId=" + objPRO001.HeaderId, "LineId,EditedBy,EditedOn", objPRL001.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                    }
                    #endregion
                }
                returnValue = objPRL001.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.SETUP_AND_DIMENSION_REPORT_FOR_CIRCUMFERENTIAL_SEAM.GetStringValue())
            {
                #region Add New Entry in PRL005 For PROTOCOL002
                PRL005 objPRL005 = new PRL005();
                objPRL005.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL005.QualityProject = HeaderData.QualityProject;
                objPRL005.EquipmentNo = HeaderData.EquipmentNo;
                objPRL005.DrawingNo = HeaderData.DrawingNo;
                objPRL005.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL005.DCRNo = HeaderData.DCRNo;
                objPRL005.SeamNo = HeaderData.SeamNo;
                objPRL005.InspectionAgency = HeaderData.InspectionAgency;
                objPRL005.Project = HeaderData.Project;
                objPRL005.BU = HeaderData.BU;
                objPRL005.Location = HeaderData.Location;
                objPRL005.StageCode = HeaderData.StageCode;
                objPRL005.StageSequence = HeaderData.StageSequence;
                objPRL005.CreatedBy = objClsLoginInfo.UserName;
                objPRL005.CreatedOn = DateTime.Now;
                db.PRL005.Add(objPRL005);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL002

                    PRO005 objPRO005 = db.PRO005.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO005 != null)
                    {
                        CopyProtocolData("PRO005", "PRL005", "PRL005.HeaderId=" + objPRL005.HeaderId + " AND PRO005.HeaderId=" + objPRO005.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO006", "PRL006", "HeaderId=" + objPRO005.HeaderId, "LineId,EditedBy,EditedOn", objPRL005.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO007", "PRL007", "HeaderId=" + objPRO005.HeaderId, "LineId,EditedBy,EditedOn", objPRL005.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO008", "PRL008", "HeaderId=" + objPRO005.HeaderId, "LineId,EditedBy,EditedOn", objPRL005.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO009_1", "PRL009_1", "HeaderId=" + objPRO005.HeaderId, "LineId,EditedBy,EditedOn", objPRL005.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO009_2", "PRL009_2", "HeaderId=" + objPRO005.HeaderId, "LineId,EditedBy,EditedOn", objPRL005.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                    }
                    #endregion
                }
                returnValue = objPRL005.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.SETUP_INSPECTION_REPORT_FOR_FORMED.GetStringValue())
            {
                #region Add New Entry in PRL010 For PROTOCOL003
                PRL010 objPRL010 = new PRL010();
                objPRL010.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL010.QualityProject = HeaderData.QualityProject;
                objPRL010.EquipmentNo = HeaderData.EquipmentNo;
                objPRL010.DrawingNo = HeaderData.DrawingNo;
                objPRL010.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL010.DCRNo = HeaderData.DCRNo;
                objPRL010.SeamNo = HeaderData.SeamNo;
                objPRL010.InspectionAgency = HeaderData.InspectionAgency;
                objPRL010.Project = HeaderData.Project;
                objPRL010.BU = HeaderData.BU;
                objPRL010.Location = HeaderData.Location;
                objPRL010.StageCode = HeaderData.StageCode;
                objPRL010.StageSequence = HeaderData.StageSequence;
                objPRL010.CreatedBy = objClsLoginInfo.UserName;
                objPRL010.CreatedOn = DateTime.Now;
                db.PRL010.Add(objPRL010);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL003

                    PRO010 objPRO010 = db.PRO010.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO010 != null)
                    {
                        CopyProtocolData("PRO010", "PRL010", "PRL010.HeaderId=" + objPRL010.HeaderId + " AND PRO010.HeaderId=" + objPRO010.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO011", "PRL011", "HeaderId=" + objPRO010.HeaderId, "LineId,EditedBy,EditedOn", objPRL010.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO012", "PRL012", "HeaderId=" + objPRO010.HeaderId, "LineId,EditedBy,EditedOn", objPRL010.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO013", "PRL013", "HeaderId=" + objPRO010.HeaderId, "LineId,EditedBy,EditedOn", objPRL010.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO014_1", "PRL014_1", "HeaderId=" + objPRO010.HeaderId, "LineId,EditedBy,EditedOn", objPRL010.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO014_2", "PRL014_2", "HeaderId=" + objPRO010.HeaderId, "LineId,EditedBy,EditedOn", objPRL010.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                    }
                    #endregion
                }
                returnValue = objPRL010.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.SETUP_AND_DIMENSION_REPORT_FOR_NOZZLE.GetStringValue())
            {
                #region Add New Entry in PRL015 For PROTOCOL004
                PRL015 objPRL015 = new PRL015();
                objPRL015.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL015.QualityProject = HeaderData.QualityProject;
                objPRL015.EquipmentNo = HeaderData.EquipmentNo;
                objPRL015.DrawingNo = HeaderData.DrawingNo;
                objPRL015.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL015.DCRNo = HeaderData.DCRNo;
                objPRL015.SeamNo = HeaderData.SeamNo;
                objPRL015.InspectionAgency = HeaderData.InspectionAgency;
                objPRL015.Project = HeaderData.Project;
                objPRL015.BU = HeaderData.BU;
                objPRL015.Location = HeaderData.Location;
                objPRL015.StageCode = HeaderData.StageCode;
                objPRL015.StageSequence = HeaderData.StageSequence;
                objPRL015.CreatedBy = objClsLoginInfo.UserName;
                objPRL015.CreatedOn = DateTime.Now;
                db.PRL015.Add(objPRL015);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL004

                    PRO015 objPRO015 = db.PRO015.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO015 != null)
                    {
                        CopyProtocolData("PRO015", "PRL015", "PRL015.HeaderId=" + objPRL015.HeaderId + " AND PRO015.HeaderId=" + objPRO015.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");
                    }
                    #endregion
                }
                returnValue = objPRL015.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_REPORT_FOR_ROLLED_SHELL_LONGITUDINAL_SEAM.GetStringValue())
            {
                #region Add New Entry in PRL020 For PROTOCOL005
                PRL020 objPRL020 = new PRL020();
                objPRL020.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL020.QualityProject = HeaderData.QualityProject;
                objPRL020.EquipmentNo = HeaderData.EquipmentNo;
                objPRL020.DrawingNo = HeaderData.DrawingNo;
                objPRL020.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL020.DCRNo = HeaderData.DCRNo;
                objPRL020.SeamNo = HeaderData.SeamNo;
                objPRL020.InspectionAgency = HeaderData.InspectionAgency;
                objPRL020.Project = HeaderData.Project;
                objPRL020.BU = HeaderData.BU;
                objPRL020.Location = HeaderData.Location;
                objPRL020.StageCode = HeaderData.StageCode;
                objPRL020.StageSequence = HeaderData.StageSequence;
                objPRL020.CreatedBy = objClsLoginInfo.UserName;
                objPRL020.CreatedOn = DateTime.Now;
                db.PRL020.Add(objPRL020);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL005

                    PRO020 objPRO020 = db.PRO020.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO020 != null)
                    {
                        CopyProtocolData("PRO020", "PRL020", "PRL020.HeaderId=" + objPRL020.HeaderId + " AND PRO020.HeaderId=" + objPRO020.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO021", "PRL021", "HeaderId=" + objPRO020.HeaderId, "LineId,EditedBy,EditedOn", objPRL020.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO022", "PRL022", "HeaderId=" + objPRO020.HeaderId, "LineId,EditedBy,EditedOn", objPRL020.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO023", "PRL023", "HeaderId=" + objPRO020.HeaderId, "LineId,EditedBy,EditedOn", objPRL020.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                    }
                    #endregion
                }
                returnValue = objPRL020.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_INSPECTION_REPORT_FOR_FORMED_D_END_CONE.GetStringValue())
            {
                #region Add New Entry in PRL025 For PROTOCOL006
                PRL025 objPRL025 = new PRL025();
                objPRL025.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL025.QualityProject = HeaderData.QualityProject;
                objPRL025.EquipmentNo = HeaderData.EquipmentNo;
                objPRL025.DrawingNo = HeaderData.DrawingNo;
                objPRL025.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL025.DCRNo = HeaderData.DCRNo;
                objPRL025.SeamNo = HeaderData.SeamNo;
                objPRL025.InspectionAgency = HeaderData.InspectionAgency;
                objPRL025.Project = HeaderData.Project;
                objPRL025.BU = HeaderData.BU;
                objPRL025.Location = HeaderData.Location;
                objPRL025.StageCode = HeaderData.StageCode;
                objPRL025.StageSequence = HeaderData.StageSequence;
                objPRL025.CreatedBy = objClsLoginInfo.UserName;
                objPRL025.CreatedOn = DateTime.Now;
                db.PRL025.Add(objPRL025);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL006

                    PRO025 objPRO025 = db.PRO025.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO025 != null)
                    {
                        CopyProtocolData("PRO025", "PRL025", "PRL025.HeaderId=" + objPRL025.HeaderId + " AND PRO025.HeaderId=" + objPRO025.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO026", "PRL026", "HeaderId=" + objPRO025.HeaderId, "LineId,EditedBy,EditedOn", objPRL025.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO027", "PRL027", "HeaderId=" + objPRO025.HeaderId, "LineId,EditedBy,EditedOn", objPRL025.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO028", "PRL028", "HeaderId=" + objPRO025.HeaderId, "LineId,EditedBy,EditedOn", objPRL025.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO029", "PRL029", "HeaderId=" + objPRO025.HeaderId, "LineId,EditedBy,EditedOn", objPRL025.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);

                    }
                    #endregion
                }
                returnValue = objPRL025.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.WELD_VISUAL_AND_DIMENSION_REPORT_FOR_NOZZLE.GetStringValue())
            {
                #region Add New Entry in PRL030 For PROTOCOL007
                PRL030 objPRL030 = new PRL030();
                objPRL030.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL030.QualityProject = HeaderData.QualityProject;
                objPRL030.EquipmentNo = HeaderData.EquipmentNo;
                objPRL030.DrawingNo = HeaderData.DrawingNo;
                objPRL030.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL030.DCRNo = HeaderData.DCRNo;
                objPRL030.SeamNo = HeaderData.SeamNo;
                objPRL030.InspectionAgency = HeaderData.InspectionAgency;
                objPRL030.Project = HeaderData.Project;
                objPRL030.BU = HeaderData.BU;
                objPRL030.Location = HeaderData.Location;
                objPRL030.StageCode = HeaderData.StageCode;
                objPRL030.StageSequence = HeaderData.StageSequence;
                objPRL030.CreatedBy = objClsLoginInfo.UserName;
                objPRL030.CreatedOn = DateTime.Now;
                db.PRL030.Add(objPRL030);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL007

                    PRO030 objPRO030 = db.PRO030.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO030 != null)
                    {
                        CopyProtocolData("PRO030", "PRL030", "PRL030.HeaderId=" + objPRL030.HeaderId + " AND PRO030.HeaderId=" + objPRO030.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");
                    }
                    #endregion
                }
                returnValue = objPRL030.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORT_FOR_SQUARE_TYPE.GetStringValue())
            {
                #region Add New Entry in PRL035 For PROTOCOL008
                PRL035 objPRL035 = new PRL035();
                objPRL035.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL035.QualityProject = HeaderData.QualityProject;
                objPRL035.EquipmentNo = HeaderData.EquipmentNo;
                objPRL035.DrawingNo = HeaderData.DrawingNo;
                objPRL035.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL035.DCRNo = HeaderData.DCRNo;
                objPRL035.SeamNo = HeaderData.SeamNo;
                objPRL035.InspectionAgency = HeaderData.InspectionAgency;
                objPRL035.Project = HeaderData.Project;
                objPRL035.BU = HeaderData.BU;
                objPRL035.Location = HeaderData.Location;
                objPRL035.StageCode = HeaderData.StageCode;
                objPRL035.StageSequence = HeaderData.StageSequence;
                objPRL035.CreatedBy = objClsLoginInfo.UserName;
                objPRL035.CreatedOn = DateTime.Now;
                db.PRL035.Add(objPRL035);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL008

                    PRO035 objPRO035 = db.PRO035.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO035 != null)
                    {
                        CopyProtocolData("PRO035", "PRL035", "PRL035.HeaderId=" + objPRL035.HeaderId + " AND PRO035.HeaderId=" + objPRO035.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");
                    }
                    #endregion
                }
                returnValue = objPRL035.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_REPORT_FOR_CIRCUMFERENTIAL_SEAM.GetStringValue())
            {
                #region Add New Entry in PRL040 For PROTOCOL009
                PRL040 objPRL040 = new PRL040();
                objPRL040.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL040.QualityProject = HeaderData.QualityProject;
                objPRL040.EquipmentNo = HeaderData.EquipmentNo;
                objPRL040.DrawingNo = HeaderData.DrawingNo;
                objPRL040.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL040.DCRNo = HeaderData.DCRNo;
                objPRL040.SeamNo = HeaderData.SeamNo;
                objPRL040.InspectionAgency = HeaderData.InspectionAgency;
                objPRL040.Project = HeaderData.Project;
                objPRL040.BU = HeaderData.BU;
                objPRL040.Location = HeaderData.Location;
                objPRL040.StageCode = HeaderData.StageCode;
                objPRL040.StageSequence = HeaderData.StageSequence;
                objPRL040.CreatedBy = objClsLoginInfo.UserName;
                objPRL040.CreatedOn = DateTime.Now;
                db.PRL040.Add(objPRL040);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL009

                    PRO040 objPRO040 = db.PRO040.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO040 != null)
                    {
                        CopyProtocolData("PRO040", "PRL040", "PRL040.HeaderId=" + objPRL040.HeaderId + " AND PRO040.HeaderId=" + objPRO040.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO041", "PRL041", "HeaderId=" + objPRO040.HeaderId, "LineId,EditedBy,EditedOn", objPRL040.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO042", "PRL042", "HeaderId=" + objPRO040.HeaderId, "LineId,EditedBy,EditedOn", objPRL040.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO043", "PRL043", "HeaderId=" + objPRO040.HeaderId, "LineId,EditedBy,EditedOn", objPRL040.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO044_1", "PRL044_1", "HeaderId=" + objPRO040.HeaderId, "LineId,EditedBy,EditedOn", objPRL040.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO044_2", "PRL044_2", "HeaderId=" + objPRO040.HeaderId, "LineId,EditedBy,EditedOn", objPRL040.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);

                    }
                    #endregion
                }
                returnValue = objPRL040.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.SETUP_AND_DIMENSION_REPORT_FOR_FORMED_ELBOW_LONG_SEAMS.GetStringValue())
            {
                #region Add New Entry in PRL045 For PROTOCOL010
                PRL045 objPRL045 = new PRL045();
                objPRL045.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL045.QualityProject = HeaderData.QualityProject;
                objPRL045.EquipmentNo = HeaderData.EquipmentNo;
                objPRL045.DrawingNo = HeaderData.DrawingNo;
                objPRL045.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL045.DCRNo = HeaderData.DCRNo;
                objPRL045.SeamNo = HeaderData.SeamNo;
                objPRL045.InspectionAgency = HeaderData.InspectionAgency;
                objPRL045.Project = HeaderData.Project;
                objPRL045.BU = HeaderData.BU;
                objPRL045.Location = HeaderData.Location;
                objPRL045.StageCode = HeaderData.StageCode;
                objPRL045.StageSequence = HeaderData.StageSequence;
                objPRL045.CreatedBy = objClsLoginInfo.UserName;
                objPRL045.CreatedOn = DateTime.Now;
                db.PRL045.Add(objPRL045);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL010

                    PRO045 objPRO045 = db.PRO045.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO045 != null)
                    {
                        CopyProtocolData("PRO045", "PRL045", "PRL045.HeaderId=" + objPRL045.HeaderId + " AND PRO045.HeaderId=" + objPRO045.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");
                    }
                    #endregion
                }
                returnValue = objPRL045.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.WELD_VISUAL_AND_DIMENSION_REPORT_FOR_SPOOL_TYPE1.GetStringValue())
            {
                #region Add New Entry in PRL050 For PROTOCOL011
                PRL050 objPRL050 = new PRL050();
                objPRL050.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL050.QualityProject = HeaderData.QualityProject;
                objPRL050.EquipmentNo = HeaderData.EquipmentNo;
                objPRL050.DrawingNo = HeaderData.DrawingNo;
                objPRL050.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL050.DCRNo = HeaderData.DCRNo;
                objPRL050.SeamNo = HeaderData.SeamNo;
                objPRL050.InspectionAgency = HeaderData.InspectionAgency;
                objPRL050.Project = HeaderData.Project;
                objPRL050.BU = HeaderData.BU;
                objPRL050.Location = HeaderData.Location;
                objPRL050.StageCode = HeaderData.StageCode;
                objPRL050.StageSequence = HeaderData.StageSequence;
                objPRL050.CreatedBy = objClsLoginInfo.UserName;
                objPRL050.CreatedOn = DateTime.Now;
                db.PRL050.Add(objPRL050);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL011

                    PRO050 objPRO050 = db.PRO050.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO050 != null)
                    {
                        CopyProtocolData("PRO050", "PRL050", "PRL050.HeaderId=" + objPRL050.HeaderId + " AND PRO050.HeaderId=" + objPRO050.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");
                    }
                    #endregion
                }
                returnValue = objPRL050.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.WELD_VISUAL_AND_DIMENSION_REPORT_FOR_SPOOL_TYPE2.GetStringValue())
            {
                #region Add New Entry in PRL055 For PROTOCOL012
                PRL055 objPRL055 = new PRL055();
                objPRL055.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL055.QualityProject = HeaderData.QualityProject;
                objPRL055.EquipmentNo = HeaderData.EquipmentNo;
                objPRL055.DrawingNo = HeaderData.DrawingNo;
                objPRL055.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL055.DCRNo = HeaderData.DCRNo;
                objPRL055.SeamNo = HeaderData.SeamNo;
                objPRL055.InspectionAgency = HeaderData.InspectionAgency;
                objPRL055.Project = HeaderData.Project;
                objPRL055.BU = HeaderData.BU;
                objPRL055.Location = HeaderData.Location;
                objPRL055.StageCode = HeaderData.StageCode;
                objPRL055.StageSequence = HeaderData.StageSequence;
                objPRL055.CreatedBy = objClsLoginInfo.UserName;
                objPRL055.CreatedOn = DateTime.Now;
                db.PRL055.Add(objPRL055);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL012

                    PRO055 objPRO055 = db.PRO055.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO055 != null)
                    {
                        CopyProtocolData("PRO055", "PRL055", "PRL055.HeaderId=" + objPRL055.HeaderId + " AND PRO055.HeaderId=" + objPRO055.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");
                    }
                    #endregion
                }
                returnValue = objPRL055.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue())
            {
                #region Add New Entry in PRL060 For PROTOCOL013
                PRL060 objPRL060 = new PRL060();
                objPRL060.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL060.QualityProject = HeaderData.QualityProject;
                objPRL060.EquipmentNo = HeaderData.EquipmentNo;
                objPRL060.DrawingNo = HeaderData.DrawingNo;
                objPRL060.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL060.DCRNo = HeaderData.DCRNo;
                objPRL060.SeamNo = HeaderData.SeamNo;
                objPRL060.InspectionAgency = HeaderData.InspectionAgency;
                objPRL060.Project = HeaderData.Project;
                objPRL060.BU = HeaderData.BU;
                objPRL060.Location = HeaderData.Location;
                objPRL060.StageCode = HeaderData.StageCode;
                objPRL060.StageSequence = HeaderData.StageSequence;
                objPRL060.CreatedBy = objClsLoginInfo.UserName;
                objPRL060.CreatedOn = DateTime.Now;
                db.PRL060.Add(objPRL060);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL013

                    PRO060 objPRO060 = db.PRO060.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO060 != null)
                    {
                        CopyProtocolData("PRO060", "PRL060", "PRL060.HeaderId=" + objPRL060.HeaderId + " AND PRO060.HeaderId=" + objPRO060.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO061", "PRL061", "HeaderId=" + objPRO060.HeaderId, "LineId,EditedBy,EditedOn", objPRL060.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                    }
                    #endregion
                }
                returnValue = objPRL060.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_BEFORE_OVERLAY_FOR_SQUARE_TYPE_NUB.GetStringValue())
            {
                #region Add New Entry in PRL065 For PROTOCOL014
                PRL065 objPRL065 = new PRL065();
                objPRL065.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL065.QualityProject = HeaderData.QualityProject;
                objPRL065.EquipmentNo = HeaderData.EquipmentNo;
                objPRL065.DrawingNo = HeaderData.DrawingNo;
                objPRL065.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL065.DCRNo = HeaderData.DCRNo;
                objPRL065.SeamNo = HeaderData.SeamNo;
                objPRL065.InspectionAgency = HeaderData.InspectionAgency;
                objPRL065.Project = HeaderData.Project;
                objPRL065.BU = HeaderData.BU;
                objPRL065.Location = HeaderData.Location;
                objPRL065.StageCode = HeaderData.StageCode;
                objPRL065.StageSequence = HeaderData.StageSequence;
                objPRL065.CreatedBy = objClsLoginInfo.UserName;
                objPRL065.CreatedOn = DateTime.Now;
                db.PRL065.Add(objPRL065);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL014

                    PRO065 objPRO065 = db.PRO065.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO065 != null)
                    {
                        CopyProtocolData("PRO065", "PRL065", "PRL065.HeaderId=" + objPRL065.HeaderId + " AND PRO065.HeaderId=" + objPRO065.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");
                    }
                    #endregion
                }
                returnValue = objPRL065.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_AFTER_OVERLAY_FOR_TAPER_TYPE_NUB.GetStringValue())
            {

                #region Add New Entry in PRL070 For PROTOCOL015
                PRL070 objPRL070 = new PRL070();
                objPRL070.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL070.QualityProject = HeaderData.QualityProject;
                objPRL070.EquipmentNo = HeaderData.EquipmentNo;
                objPRL070.DrawingNo = HeaderData.DrawingNo;
                objPRL070.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL070.DCRNo = HeaderData.DCRNo;
                objPRL070.SeamNo = HeaderData.SeamNo;
                objPRL070.InspectionAgency = HeaderData.InspectionAgency;
                objPRL070.Project = HeaderData.Project;
                objPRL070.BU = HeaderData.BU;
                objPRL070.Location = HeaderData.Location;
                objPRL070.StageCode = HeaderData.StageCode;
                objPRL070.StageSequence = HeaderData.StageSequence;
                objPRL070.CreatedBy = objClsLoginInfo.UserName;
                objPRL070.CreatedOn = DateTime.Now;
                db.PRL070.Add(objPRL070);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL015

                    PRO070 objPRO070 = db.PRO070.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO070 != null)
                    {
                        CopyProtocolData("PRO070", "PRL070", "PRL070.HeaderId=" + objPRL070.HeaderId + " AND PRO070.HeaderId=" + objPRO070.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");
                    }
                    #endregion
                }
                returnValue = objPRL070.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.TSR_DIMENSION_INSPECTION_REPORTS.GetStringValue())
            {

                #region Add New Entry in PRL075 For PROTOCOL016
                PRL075 objPRL075 = new PRL075();
                objPRL075.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL075.QualityProject = HeaderData.QualityProject;
                objPRL075.EquipmentNo = HeaderData.EquipmentNo;
                objPRL075.DrawingNo = HeaderData.DrawingNo;
                objPRL075.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL075.DCRNo = HeaderData.DCRNo;
                objPRL075.SeamNo = HeaderData.SeamNo;
                objPRL075.InspectionAgency = HeaderData.InspectionAgency;
                objPRL075.Project = HeaderData.Project;
                objPRL075.BU = HeaderData.BU;
                objPRL075.Location = HeaderData.Location;
                objPRL075.StageCode = HeaderData.StageCode;
                objPRL075.StageSequence = HeaderData.StageSequence;
                objPRL075.CreatedBy = objClsLoginInfo.UserName;
                objPRL075.CreatedOn = DateTime.Now;
                db.PRL075.Add(objPRL075);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL016

                    PRO075 objPRO075 = db.PRO075.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO075 != null)
                    {
                        CopyProtocolData("PRO075", "PRL075", "PRL075.HeaderId=" + objPRL075.HeaderId + " AND PRO075.HeaderId=" + objPRO075.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");
                    }
                    #endregion
                }
                returnValue = objPRL075.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.WELD_VISUAL_AND_DIMENSION_INSPECTION_REPORT_FOR_SUPPORT_BRACKET.GetStringValue())
            {

                #region Add New Entry in PRL080 For PROTOCOL017
                PRL080 objPRL080 = new PRL080();
                objPRL080.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL080.QualityProject = HeaderData.QualityProject;
                objPRL080.EquipmentNo = HeaderData.EquipmentNo;
                objPRL080.DrawingNo = HeaderData.DrawingNo;
                objPRL080.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL080.DCRNo = HeaderData.DCRNo;
                objPRL080.SeamNo = HeaderData.SeamNo;
                objPRL080.InspectionAgency = HeaderData.InspectionAgency;
                objPRL080.Project = HeaderData.Project;
                objPRL080.BU = HeaderData.BU;
                objPRL080.Location = HeaderData.Location;
                objPRL080.StageCode = HeaderData.StageCode;
                objPRL080.StageSequence = HeaderData.StageSequence;
                objPRL080.CreatedBy = objClsLoginInfo.UserName;
                objPRL080.CreatedOn = DateTime.Now;
                db.PRL080.Add(objPRL080);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL017

                    PRO080 objPRO080 = db.PRO080.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO080 != null)
                    {
                        CopyProtocolData("PRO080", "PRL080", "PRL080.HeaderId=" + objPRL080.HeaderId + " AND PRO080.HeaderId=" + objPRO080.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO081", "PRL081", "HeaderId=" + objPRO080.HeaderId, "LineId,EditedBy,EditedOn", objPRL080.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO082", "PRL082", "HeaderId=" + objPRO080.HeaderId, "LineId,EditedBy,EditedOn", objPRL080.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                    }
                    #endregion
                }
                returnValue = objPRL080.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.RECORD_FOR_TEMPORARY_ATTACHMENTS_LOCATION.GetStringValue())
            {

                #region Add New Entry in PRL085 For PROTOCOL018
                PRL085 objPRL085 = new PRL085();
                objPRL085.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL085.QualityProject = HeaderData.QualityProject;
                objPRL085.EquipmentNo = HeaderData.EquipmentNo;
                objPRL085.DrawingNo = HeaderData.DrawingNo;
                objPRL085.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL085.DCRNo = HeaderData.DCRNo;
                objPRL085.SeamNo = HeaderData.SeamNo;
                objPRL085.InspectionAgency = HeaderData.InspectionAgency;
                objPRL085.Project = HeaderData.Project;
                objPRL085.BU = HeaderData.BU;
                objPRL085.Location = HeaderData.Location;
                objPRL085.StageCode = HeaderData.StageCode;
                objPRL085.StageSequence = HeaderData.StageSequence;
                objPRL085.CreatedBy = objClsLoginInfo.UserName;
                objPRL085.CreatedOn = DateTime.Now;
                db.PRL085.Add(objPRL085);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL018

                    PRO085 objPRO085 = db.PRO085.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO085 != null)
                    {
                        CopyProtocolData("PRO085", "PRL085", "PRL085.HeaderId=" + objPRL085.HeaderId + " AND PRO085.HeaderId=" + objPRO085.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO086", "PRL086", "HeaderId=" + objPRO085.HeaderId, "LineId,EditedBy,EditedOn", objPRL085.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                    }
                    #endregion
                }
                returnValue = objPRL085.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_INSPECTION_OF_SKIRT_TEMPLATE.GetStringValue())
            {

                #region Add New Entry in PRL090 For PROTOCOL019
                PRL090 objPRL090 = new PRL090();
                objPRL090.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL090.QualityProject = HeaderData.QualityProject;
                objPRL090.EquipmentNo = HeaderData.EquipmentNo;
                objPRL090.DrawingNo = HeaderData.DrawingNo;
                objPRL090.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL090.DCRNo = HeaderData.DCRNo;
                objPRL090.SeamNo = HeaderData.SeamNo;
                objPRL090.InspectionAgency = HeaderData.InspectionAgency;
                objPRL090.Project = HeaderData.Project;
                objPRL090.BU = HeaderData.BU;
                objPRL090.Location = HeaderData.Location;
                objPRL090.StageCode = HeaderData.StageCode;
                objPRL090.StageSequence = HeaderData.StageSequence;
                objPRL090.CreatedBy = objClsLoginInfo.UserName;
                objPRL090.CreatedOn = DateTime.Now;
                db.PRL090.Add(objPRL090);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL019

                    PRO090 objPRO090 = db.PRO090.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO090 != null)
                    {
                        CopyProtocolData("PRO090", "PRL090", "PRL090.HeaderId=" + objPRL090.HeaderId + " AND PRO090.HeaderId=" + objPRO090.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");
                    }
                    #endregion
                }
                returnValue = objPRL090.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_INSPECTION_OF_SKIRT_ASSEMBLY.GetStringValue())
            {

                #region Add New Entry in PRL095 For PROTOCOL020
                PRL095 objPRL095 = new PRL095();
                objPRL095.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL095.QualityProject = HeaderData.QualityProject;
                objPRL095.EquipmentNo = HeaderData.EquipmentNo;
                objPRL095.DrawingNo = HeaderData.DrawingNo;
                objPRL095.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL095.DCRNo = HeaderData.DCRNo;
                objPRL095.SeamNo = HeaderData.SeamNo;
                objPRL095.InspectionAgency = HeaderData.InspectionAgency;
                objPRL095.Project = HeaderData.Project;
                objPRL095.BU = HeaderData.BU;
                objPRL095.Location = HeaderData.Location;
                objPRL095.StageCode = HeaderData.StageCode;
                objPRL095.StageSequence = HeaderData.StageSequence;
                objPRL095.CreatedBy = objClsLoginInfo.UserName;
                objPRL095.CreatedOn = DateTime.Now;
                db.PRL095.Add(objPRL095);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL020

                    PRO095 objPRO095 = db.PRO095.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO095 != null)
                    {
                        CopyProtocolData("PRO095", "PRL095", "PRL095.HeaderId=" + objPRL095.HeaderId + " AND PRO095.HeaderId=" + objPRO095.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO096", "PRL096", "HeaderId=" + objPRO095.HeaderId, "LineId,EditedBy,EditedOn", objPRL095.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO097", "PRL097", "HeaderId=" + objPRO095.HeaderId, "LineId,EditedBy,EditedOn", objPRL095.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO098", "PRL098", "HeaderId=" + objPRO095.HeaderId, "LineId,EditedBy,EditedOn", objPRL095.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO099", "PRL099", "HeaderId=" + objPRO095.HeaderId, "LineId,EditedBy,EditedOn", objPRL095.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                    }
                    #endregion
                }
                returnValue = objPRL095.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_OF_HARDNESS_TEST.GetStringValue())
            {

                #region Add New Entry in PRL100 For PROTOCOL021
                PRL100 objPRL100 = new PRL100();
                objPRL100.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL100.QualityProject = HeaderData.QualityProject;
                objPRL100.EquipmentNo = HeaderData.EquipmentNo;
                objPRL100.DrawingNo = HeaderData.DrawingNo;
                objPRL100.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL100.DCRNo = HeaderData.DCRNo;
                objPRL100.SeamNo = HeaderData.SeamNo;
                objPRL100.InspectionAgency = HeaderData.InspectionAgency;
                objPRL100.Project = HeaderData.Project;
                objPRL100.BU = HeaderData.BU;
                objPRL100.Location = HeaderData.Location;
                objPRL100.StageCode = HeaderData.StageCode;
                objPRL100.StageSequence = HeaderData.StageSequence;
                objPRL100.CreatedBy = objClsLoginInfo.UserName;
                objPRL100.CreatedOn = DateTime.Now;
                db.PRL100.Add(objPRL100);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL021

                    PRO100 objPRO100 = db.PRO100.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO100 != null)
                    {
                        CopyProtocolData("PRO100", "PRL100", "PRL100.HeaderId=" + objPRL100.HeaderId + " AND PRO100.HeaderId=" + objPRO100.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO101", "PRL101", "HeaderId=" + objPRO100.HeaderId, "LineId,EditedBy,EditedOn", objPRL100.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                    }
                    #endregion
                }
                returnValue = objPRL100.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.AIR_TEST_OF_REINFORCEMENT_PADS_OF_NOZZLE.GetStringValue())
            {

                #region Add New Entry in PRL105 For PROTOCOL022
                PRL105 objPRL105 = new PRL105();
                objPRL105.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL105.QualityProject = HeaderData.QualityProject;
                objPRL105.EquipmentNo = HeaderData.EquipmentNo;
                objPRL105.DrawingNo = HeaderData.DrawingNo;
                objPRL105.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL105.DCRNo = HeaderData.DCRNo;
                objPRL105.SeamNo = HeaderData.SeamNo;
                objPRL105.InspectionAgency = HeaderData.InspectionAgency;
                objPRL105.Project = HeaderData.Project;
                objPRL105.BU = HeaderData.BU;
                objPRL105.Location = HeaderData.Location;
                objPRL105.StageCode = HeaderData.StageCode;
                objPRL105.StageSequence = HeaderData.StageSequence;
                objPRL105.CreatedBy = objClsLoginInfo.UserName;
                objPRL105.CreatedOn = DateTime.Now;
                db.PRL105.Add(objPRL105);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL022

                    PRO105 objPRO105 = db.PRO105.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO105 != null)
                    {
                        CopyProtocolData("PRO105", "PRL105", "PRL105.HeaderId=" + objPRL105.HeaderId + " AND PRO105.HeaderId=" + objPRO105.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO106", "PRL106", "HeaderId=" + objPRO105.HeaderId, "LineId,EditedBy,EditedOn", objPRL105.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                    }
                    #endregion
                }
                returnValue = objPRL105.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.HELIUM_LEAK_TEST_REPORT.GetStringValue())
            {

                #region Add New Entry in PRL110 For PROTOCOL023
                PRL110 objPRL110 = new PRL110();
                objPRL110.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL110.QualityProject = HeaderData.QualityProject;
                objPRL110.EquipmentNo = HeaderData.EquipmentNo;
                objPRL110.DrawingNo = HeaderData.DrawingNo;
                objPRL110.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL110.DCRNo = HeaderData.DCRNo;
                objPRL110.SeamNo = HeaderData.SeamNo;
                objPRL110.InspectionAgency = HeaderData.InspectionAgency;
                objPRL110.Project = HeaderData.Project;
                objPRL110.BU = HeaderData.BU;
                objPRL110.Location = HeaderData.Location;
                objPRL110.StageCode = HeaderData.StageCode;
                objPRL110.StageSequence = HeaderData.StageSequence;
                objPRL110.CreatedBy = objClsLoginInfo.UserName;
                objPRL110.CreatedOn = DateTime.Now;
                db.PRL110.Add(objPRL110);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL023

                    PRO110 objPRO110 = db.PRO110.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO110 != null)
                    {
                        CopyProtocolData("PRO110", "PRL110", "PRL110.HeaderId=" + objPRL110.HeaderId + " AND PRO110.HeaderId=" + objPRO110.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO111", "PRL111", "HeaderId=" + objPRO110.HeaderId, "LineId,EditedBy,EditedOn", objPRL110.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                    }
                    #endregion
                }
                returnValue = objPRL110.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.HYDROSTATIC_TEST_REPORT.GetStringValue())
            {

                #region Add New Entry in PRL115 For PROTOCOL024
                PRL115 objPRL115 = new PRL115();
                objPRL115.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL115.QualityProject = HeaderData.QualityProject;
                objPRL115.EquipmentNo = HeaderData.EquipmentNo;
                objPRL115.DrawingNo = HeaderData.DrawingNo;
                objPRL115.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL115.DCRNo = HeaderData.DCRNo;
                objPRL115.SeamNo = HeaderData.SeamNo;
                objPRL115.InspectionAgency = HeaderData.InspectionAgency;
                objPRL115.Project = HeaderData.Project;
                objPRL115.BU = HeaderData.BU;
                objPRL115.Location = HeaderData.Location;
                objPRL115.StageCode = HeaderData.StageCode;
                objPRL115.StageSequence = HeaderData.StageSequence;
                objPRL115.CreatedBy = objClsLoginInfo.UserName;
                objPRL115.CreatedOn = DateTime.Now;
                db.PRL115.Add(objPRL115);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL024

                    PRO115 objPRO115 = db.PRO115.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO115 != null)
                    {
                        CopyProtocolData("PRO115", "PRL115", "PRL115.HeaderId=" + objPRL115.HeaderId + " AND PRO115.HeaderId=" + objPRO115.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO116", "PRL116", "HeaderId=" + objPRO115.HeaderId, "LineId,EditedBy,EditedOn", objPRL115.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                    }
                    #endregion
                }
                returnValue = objPRL115.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.PNEUMATIC_TEST_REPORT.GetStringValue())
            {

                #region Add New Entry in PRL120 For PROTOCOL025
                PRL120 objPRL120 = new PRL120();
                objPRL120.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL120.QualityProject = HeaderData.QualityProject;
                objPRL120.EquipmentNo = HeaderData.EquipmentNo;
                objPRL120.DrawingNo = HeaderData.DrawingNo;
                objPRL120.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL120.DCRNo = HeaderData.DCRNo;
                objPRL120.SeamNo = HeaderData.SeamNo;
                objPRL120.InspectionAgency = HeaderData.InspectionAgency;
                objPRL120.Project = HeaderData.Project;
                objPRL120.BU = HeaderData.BU;
                objPRL120.Location = HeaderData.Location;
                objPRL120.StageCode = HeaderData.StageCode;
                objPRL120.StageSequence = HeaderData.StageSequence;
                objPRL120.CreatedBy = objClsLoginInfo.UserName;
                objPRL120.CreatedOn = DateTime.Now;
                db.PRL120.Add(objPRL120);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL025

                    PRO120 objPRO120 = db.PRO120.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO120 != null)
                    {
                        CopyProtocolData("PRO120", "PRL120", "PRL120.HeaderId=" + objPRL120.HeaderId + " AND PRO120.HeaderId=" + objPRO120.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO121", "PRL121", "HeaderId=" + objPRO120.HeaderId, "LineId,EditedBy,EditedOn", objPRL120.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                    }
                    #endregion
                }
                returnValue = objPRL120.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_N2_FILLING_INSPECTION.GetStringValue())
            {

                #region Add New Entry in PRL125 For PROTOCOL026
                PRL125 objPRL125 = new PRL125();
                objPRL125.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL125.QualityProject = HeaderData.QualityProject;
                objPRL125.EquipmentNo = HeaderData.EquipmentNo;
                objPRL125.DrawingNo = HeaderData.DrawingNo;
                objPRL125.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL125.DCRNo = HeaderData.DCRNo;
                objPRL125.SeamNo = HeaderData.SeamNo;
                objPRL125.InspectionAgency = HeaderData.InspectionAgency;
                objPRL125.Project = HeaderData.Project;
                objPRL125.BU = HeaderData.BU;
                objPRL125.Location = HeaderData.Location;
                objPRL125.StageCode = HeaderData.StageCode;
                objPRL125.StageSequence = HeaderData.StageSequence;
                objPRL125.CreatedBy = objClsLoginInfo.UserName;
                objPRL125.CreatedOn = DateTime.Now;
                db.PRL125.Add(objPRL125);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL026

                    PRO125 objPRO125 = db.PRO125.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO125 != null)
                    {
                        CopyProtocolData("PRO125", "PRL125", "PRL125.HeaderId=" + objPRL125.HeaderId + " AND PRO125.HeaderId=" + objPRO125.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");
                    }
                    #endregion
                }
                returnValue = objPRL125.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_POSITIVE_MATERIAL_IDENTIFICATION.GetStringValue())
            {
                #region Add New Entry in PRL130 For PROTOCOL027
                PRL130 objPRL130 = new PRL130();
                objPRL130.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL130.QualityProject = HeaderData.QualityProject;
                objPRL130.EquipmentNo = HeaderData.EquipmentNo;
                objPRL130.DrawingNo = HeaderData.DrawingNo;
                objPRL130.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL130.DCRNo = HeaderData.DCRNo;
                objPRL130.SeamNo = HeaderData.SeamNo;
                objPRL130.InspectionAgency = HeaderData.InspectionAgency;
                objPRL130.Project = HeaderData.Project;
                objPRL130.BU = HeaderData.BU;
                objPRL130.Location = HeaderData.Location;
                objPRL130.StageCode = HeaderData.StageCode;
                objPRL130.StageSequence = HeaderData.StageSequence;
                objPRL130.CreatedBy = objClsLoginInfo.UserName;
                objPRL130.CreatedOn = DateTime.Now;
                db.PRL130.Add(objPRL130);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL027

                    PRO130 objPRO130 = db.PRO130.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO130 != null)
                    {
                        CopyProtocolData("PRO130", "PRL130", "PRL130.HeaderId=" + objPRL130.HeaderId + " AND PRO130.HeaderId=" + objPRO130.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO131", "PRL131", "HeaderId=" + objPRO130.HeaderId, "LineId,EditedBy,EditedOn", objPRL130.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                    }
                    #endregion
                }
                returnValue = objPRL130.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_OF_FINAL_MACHINING_INSPECTION_OF_TUBESHEET.GetStringValue())
            {
                #region Add New Entry in PRL135 For PROTOCOL028
                PRL135 objPRL135 = new PRL135();
                objPRL135.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL135.QualityProject = HeaderData.QualityProject;
                objPRL135.EquipmentNo = HeaderData.EquipmentNo;
                objPRL135.DrawingNo = HeaderData.DrawingNo;
                objPRL135.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL135.DCRNo = HeaderData.DCRNo;
                objPRL135.SeamNo = HeaderData.SeamNo;
                objPRL135.InspectionAgency = HeaderData.InspectionAgency;
                objPRL135.Project = HeaderData.Project;
                objPRL135.BU = HeaderData.BU;
                objPRL135.Location = HeaderData.Location;
                objPRL135.StageCode = HeaderData.StageCode;
                objPRL135.StageSequence = HeaderData.StageSequence;
                objPRL135.CreatedBy = objClsLoginInfo.UserName;
                objPRL135.CreatedOn = DateTime.Now;
                db.PRL135.Add(objPRL135);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL028

                    PRO135 objPRO135 = db.PRO135.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO135 != null)
                    {
                        CopyProtocolData("PRO135", "PRL135", "PRL135.HeaderId=" + objPRL135.HeaderId + " AND PRO135.HeaderId=" + objPRO135.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO136", "PRL136", "HeaderId=" + objPRO135.HeaderId, "LineId,EditedBy,EditedOn", objPRL135.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                    }
                    #endregion
                }
                returnValue = objPRL135.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_OF_ARM_REVIEW.GetStringValue())
            {
                #region Add New Entry in PRL140 For PROTOCOL029
                PRL140 objPRL140 = new PRL140();
                objPRL140.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL140.QualityProject = HeaderData.QualityProject;
                objPRL140.EquipmentNo = HeaderData.EquipmentNo;
                objPRL140.DrawingNo = HeaderData.DrawingNo;
                objPRL140.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL140.DCRNo = HeaderData.DCRNo;
                objPRL140.SeamNo = HeaderData.SeamNo;
                objPRL140.InspectionAgency = HeaderData.InspectionAgency;
                objPRL140.Project = HeaderData.Project;
                objPRL140.BU = HeaderData.BU;
                objPRL140.Location = HeaderData.Location;
                objPRL140.StageCode = HeaderData.StageCode;
                objPRL140.StageSequence = HeaderData.StageSequence;
                objPRL140.CreatedBy = objClsLoginInfo.UserName;
                objPRL140.CreatedOn = DateTime.Now;
                db.PRL140.Add(objPRL140);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL029

                    PRO140 objPRO140 = db.PRO140.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO140 != null)
                    {
                        CopyProtocolData("PRO140", "PRL140", "PRL140.HeaderId=" + objPRL140.HeaderId + " AND PRO140.HeaderId=" + objPRO140.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO141", "PRL141", "HeaderId=" + objPRO140.HeaderId, "LineId,EditedBy,EditedOn", objPRL140.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                    }
                    #endregion
                }
                returnValue = objPRL140.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_INSPECTION_REPORT_FOR_OVERLAY_ON_SHELL_DEND.GetStringValue())
            {
                #region Add New Entry in PRL145 For PROTOCOL030
                PRL145 objPRL145 = new PRL145();
                objPRL145.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL145.QualityProject = HeaderData.QualityProject;
                objPRL145.EquipmentNo = HeaderData.EquipmentNo;
                objPRL145.DrawingNo = HeaderData.DrawingNo;
                objPRL145.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL145.DCRNo = HeaderData.DCRNo;
                objPRL145.SeamNo = HeaderData.SeamNo;
                objPRL145.InspectionAgency = HeaderData.InspectionAgency;
                objPRL145.Project = HeaderData.Project;
                objPRL145.BU = HeaderData.BU;
                objPRL145.Location = HeaderData.Location;
                objPRL145.StageCode = HeaderData.StageCode;
                objPRL145.StageSequence = HeaderData.StageSequence;
                objPRL145.CreatedBy = objClsLoginInfo.UserName;
                objPRL145.CreatedOn = DateTime.Now;
                db.PRL145.Add(objPRL145);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL030

                    PRO145 objPRO145 = db.PRO145.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO145 != null)
                    {
                        CopyProtocolData("PRO145", "PRL145", "PRL145.HeaderId=" + objPRL145.HeaderId + " AND PRO145.HeaderId=" + objPRO145.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");
                    }
                    #endregion
                }
                returnValue = objPRL145.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_VISUAL_AND_DIM_OF_TAILING_LUG.GetStringValue())
            {
                #region Add New Entry in PRL150 For PROTOCOL031
                PRL150 objPRL150 = new PRL150();
                objPRL150.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL150.QualityProject = HeaderData.QualityProject;
                objPRL150.EquipmentNo = HeaderData.EquipmentNo;
                objPRL150.DrawingNo = HeaderData.DrawingNo;
                objPRL150.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL150.DCRNo = HeaderData.DCRNo;
                objPRL150.SeamNo = HeaderData.SeamNo;
                objPRL150.InspectionAgency = HeaderData.InspectionAgency;
                objPRL150.Project = HeaderData.Project;
                objPRL150.BU = HeaderData.BU;
                objPRL150.Location = HeaderData.Location;
                objPRL150.StageCode = HeaderData.StageCode;
                objPRL150.StageSequence = HeaderData.StageSequence;
                objPRL150.CreatedBy = objClsLoginInfo.UserName;
                objPRL150.CreatedOn = DateTime.Now;
                db.PRL150.Add(objPRL150);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL031

                    PRO150 objPRO150 = db.PRO150.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO150 != null)
                    {
                        CopyProtocolData("PRO150", "PRL150", "PRL150.HeaderId=" + objPRL150.HeaderId + " AND PRO150.HeaderId=" + objPRO150.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO151", "PRL151", "HeaderId=" + objPRO150.HeaderId, "LineId,EditedBy,EditedOn", objPRL150.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                    }
                    #endregion
                }
                returnValue = objPRL150.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.OVERLAY_THICKNESS_MEASUREMENT_REPORT_FOR_SHELL_DEND_LONG_SEAM_CIRC_SEAM.GetStringValue())
            {
                #region Add New Entry in PRL155 For PROTOCOL032
                PRL155 objPRL155 = new PRL155();
                objPRL155.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL155.QualityProject = HeaderData.QualityProject;
                objPRL155.EquipmentNo = HeaderData.EquipmentNo;
                objPRL155.DrawingNo = HeaderData.DrawingNo;
                objPRL155.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL155.DCRNo = HeaderData.DCRNo;
                objPRL155.SeamNo = HeaderData.SeamNo;
                objPRL155.InspectionAgency = HeaderData.InspectionAgency;
                objPRL155.Project = HeaderData.Project;
                objPRL155.BU = HeaderData.BU;
                objPRL155.Location = HeaderData.Location;
                objPRL155.StageCode = HeaderData.StageCode;
                objPRL155.StageSequence = HeaderData.StageSequence;
                objPRL155.CreatedBy = objClsLoginInfo.UserName;
                objPRL155.CreatedOn = DateTime.Now;
                db.PRL155.Add(objPRL155);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL032

                    PRO155 objPRO155 = db.PRO155.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO155 != null)
                    {
                        CopyProtocolData("PRO155", "PRL155", "PRL155.HeaderId=" + objPRL155.HeaderId + " AND PRO155.HeaderId=" + objPRO155.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO156", "PRL156", "HeaderId=" + objPRO155.HeaderId, "LineId,EditedBy,EditedOn", objPRL155.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                    }
                    #endregion
                }
                returnValue = objPRL155.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.OVERLAY_THICKNESS_MEASUREMENT_REPORT_FOR_NOZZLE_PIPE_ELBOW.GetStringValue())
            {
                #region Add New Entry in PRL160 For PROTOCOL033
                PRL160 objPRL160 = new PRL160();
                objPRL160.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL160.QualityProject = HeaderData.QualityProject;
                objPRL160.EquipmentNo = HeaderData.EquipmentNo;
                objPRL160.DrawingNo = HeaderData.DrawingNo;
                objPRL160.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL160.DCRNo = HeaderData.DCRNo;
                objPRL160.SeamNo = HeaderData.SeamNo;
                objPRL160.InspectionAgency = HeaderData.InspectionAgency;
                objPRL160.Project = HeaderData.Project;
                objPRL160.BU = HeaderData.BU;
                objPRL160.Location = HeaderData.Location;
                objPRL160.StageCode = HeaderData.StageCode;
                objPRL160.StageSequence = HeaderData.StageSequence;
                objPRL160.CreatedBy = objClsLoginInfo.UserName;
                objPRL160.CreatedOn = DateTime.Now;
                db.PRL160.Add(objPRL160);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL033

                    PRO160 objPRO160 = db.PRO160.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO160 != null)
                    {
                        CopyProtocolData("PRO160", "PRL160", "PRL160.HeaderId=" + objPRL160.HeaderId + " AND PRO160.HeaderId=" + objPRO160.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO161", "PRL161", "HeaderId=" + objPRO160.HeaderId, "LineId,EditedBy,EditedOn", objPRL160.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                    }
                    #endregion
                }
                returnValue = objPRL160.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.THICKNESS_REPORT_FOR_EQUIPMENT.GetStringValue())
            {
                #region Add New Entry in PRL165 For PROTOCOL034
                PRL165 objPRL165 = new PRL165();
                objPRL165.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL165.QualityProject = HeaderData.QualityProject;
                objPRL165.EquipmentNo = HeaderData.EquipmentNo;
                objPRL165.DrawingNo = HeaderData.DrawingNo;
                objPRL165.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL165.DCRNo = HeaderData.DCRNo;
                objPRL165.SeamNo = HeaderData.SeamNo;
                objPRL165.InspectionAgency = HeaderData.InspectionAgency;
                objPRL165.Project = HeaderData.Project;
                objPRL165.BU = HeaderData.BU;
                objPRL165.Location = HeaderData.Location;
                objPRL165.StageCode = HeaderData.StageCode;
                objPRL165.StageSequence = HeaderData.StageSequence;
                objPRL165.CreatedBy = objClsLoginInfo.UserName;
                objPRL165.CreatedOn = DateTime.Now;
                db.PRL165.Add(objPRL165);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL034

                    PRO165 objPRO165 = db.PRO165.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO165 != null)
                    {
                        CopyProtocolData("PRO165", "PRL165", "PRL165.HeaderId=" + objPRL165.HeaderId + " AND PRO165.HeaderId=" + objPRO165.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO166", "PRL166", "HeaderId=" + objPRO165.HeaderId, "LineId,EditedBy,EditedOn", objPRL165.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                    }
                    #endregion
                }
                returnValue = objPRL165.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.NOZZLE_CUTOUT_MARKING_ON_SHELL_CONE_DEND.GetStringValue())
            {
                #region Add New Entry in PRL170 For PROTOCOL035
                PRL170 objPRL170 = new PRL170();
                objPRL170.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL170.QualityProject = HeaderData.QualityProject;
                objPRL170.EquipmentNo = HeaderData.EquipmentNo;
                objPRL170.DrawingNo = HeaderData.DrawingNo;
                objPRL170.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL170.DCRNo = HeaderData.DCRNo;
                objPRL170.SeamNo = HeaderData.SeamNo;
                objPRL170.InspectionAgency = HeaderData.InspectionAgency;
                objPRL170.Project = HeaderData.Project;
                objPRL170.BU = HeaderData.BU;
                objPRL170.Location = HeaderData.Location;
                objPRL170.StageCode = HeaderData.StageCode;
                objPRL170.StageSequence = HeaderData.StageSequence;
                objPRL170.CreatedBy = objClsLoginInfo.UserName;
                objPRL170.CreatedOn = DateTime.Now;
                db.PRL170.Add(objPRL170);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL035

                    PRO170 objPRO170 = db.PRO170.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO170 != null)
                    {
                        CopyProtocolData("PRO170", "PRL170", "PRL170.HeaderId=" + objPRL170.HeaderId + " AND PRO170.HeaderId=" + objPRO170.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO171", "PRL171", "HeaderId=" + objPRO170.HeaderId, "LineId,EditedBy,EditedOn", objPRL170.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                    }
                    #endregion
                }
                returnValue = objPRL170.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.SETUP_AND_DIMENSION_REPORT_FOR_ROLLED_SHELL_LONGITUDINAL_SEAM_V2.GetStringValue())
            {
                #region Add New Entry in PRL175 For PROTOCOL036
                PRL175 objPRL175 = new PRL175();
                objPRL175.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL175.QualityProject = HeaderData.QualityProject;
                objPRL175.EquipmentNo = HeaderData.EquipmentNo;
                objPRL175.DrawingNo = HeaderData.DrawingNo;
                objPRL175.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL175.DCRNo = HeaderData.DCRNo;
                objPRL175.SeamNo = HeaderData.SeamNo;
                objPRL175.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL175.Project = HeaderData.Project;
                objPRL175.BU = HeaderData.BU;
                objPRL175.Location = HeaderData.Location;
                objPRL175.StageCode = HeaderData.StageCode;
                objPRL175.StageSequence = HeaderData.StageSequence;
                objPRL175.ICLRevNo = HeaderData.ICLRevNo;
                objPRL175.CreatedBy = objClsLoginInfo.UserName;
                objPRL175.CreatedOn = DateTime.Now;

                #region CIRCUMFERENCE MEASUREMENT REQ
                objPRL175.PRL178.Add(new PRL178
                {
                    Location = "TOP",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                objPRL175.PRL178.Add(new PRL178
                {
                    Location = "MIDDLE",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL175.PRL178.Add(new PRL178
                {
                    Location = "BOTTOM",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                #region TOTAL SHELL HEIGHT
                objPRL175.PRL179.Add(new PRL179
                {
                    Orientation = "AT 0°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL175.PRL179.Add(new PRL179
                {
                    Orientation = "AT 90°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL175.PRL179.Add(new PRL179
                {
                    Orientation = "AT 180°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL175.PRL179.Add(new PRL179
                {
                    Orientation = "AT 270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region OUT OF ROUNDNESS 
                objPRL175.PRL179_2.Add(new PRL179_2
                {
                    Orientation = "0°-180° ",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL175.PRL179_2.Add(new PRL179_2
                {
                    Orientation = "22.5°-202.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL175.PRL179_2.Add(new PRL179_2
                {
                    Orientation = "45°-225°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL175.PRL179_2.Add(new PRL179_2
                {
                    Orientation = "67.5°-247.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL175.PRL179_2.Add(new PRL179_2
                {
                    Orientation = "90°-270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL175.PRL179_2.Add(new PRL179_2
                {
                    Orientation = "112.5°-292.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL175.PRL179_2.Add(new PRL179_2
                {
                    Orientation = "135°-315°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL175.PRL179_2.Add(new PRL179_2
                {
                    Orientation = "157.5°-337.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                db.PRL175.Add(objPRL175);
                db.SaveChanges();

                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL036

                    PRO175 objPRO175 = db.PRO175.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO175 != null)
                    {
                        CopyProtocolData("PRO175", "PRL175", "PRL175.HeaderId=" + objPRL175.HeaderId + " AND PRO175.HeaderId=" + objPRO175.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO176", "PRL176", "HeaderId=" + objPRO175.HeaderId, "LineId,EditedBy,EditedOn", objPRL175.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO177", "PRL177", "HeaderId=" + objPRO175.HeaderId, "LineId,EditedBy,EditedOn", objPRL175.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO178", "PRL178", "HeaderId=" + objPRO175.HeaderId, "LineId,EditedBy,EditedOn", objPRL175.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO179", "PRL179", "HeaderId=" + objPRO175.HeaderId, "LineId,EditedBy,EditedOn", objPRL175.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO179_2", "PRL179_2", "HeaderId=" + objPRO175.HeaderId, "LineId,EditedBy,EditedOn", objPRL175.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO179_3", "PRL179_3", "HeaderId=" + objPRO175.HeaderId, "LineId,EditedBy,EditedOn", objPRL175.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO179_4", "PRL179_4", "HeaderId=" + objPRO175.HeaderId, "LineId,EditedBy,EditedOn", objPRL175.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO179_5", "PRL179_5", "HeaderId=" + objPRO175.HeaderId, "LineId,EditedBy,EditedOn", objPRL175.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO179_6", "PRL179_6", "HeaderId=" + objPRO175.HeaderId, "LineId,EditedBy,EditedOn", objPRL175.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                    }
                    #endregion
                }
                returnValue = objPRL175.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.SETUP_AND_DIMENSION_REPORT_FOR_CIRCUMFERENTIAL_SEAM_V2.GetStringValue())
            {
                #region Add New Entry in PRL180 For PROTOCOL037
                PRL180 objPRL180 = new PRL180();
                objPRL180.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL180.QualityProject = HeaderData.QualityProject;
                objPRL180.EquipmentNo = HeaderData.EquipmentNo;
                objPRL180.DrawingNo = HeaderData.DrawingNo;
                objPRL180.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL180.DCRNo = HeaderData.DCRNo;
                objPRL180.SeamNo = HeaderData.SeamNo;
                objPRL180.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL180.Project = HeaderData.Project;
                objPRL180.BU = HeaderData.BU;
                objPRL180.Location = HeaderData.Location;
                objPRL180.StageCode = HeaderData.StageCode;
                objPRL180.StageSequence = HeaderData.StageSequence;
                objPRL180.ICLRevNo = HeaderData.ICLRevNo;
                objPRL180.CreatedBy = objClsLoginInfo.UserName;
                objPRL180.CreatedOn = DateTime.Now;

                #region  TOTAL HEIGHT REQ | PRO182
                objPRL180.PRL182.Add(new PRL182
                {
                    RequiredValue = "At 0°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL180.PRL182.Add(new PRL182
                {
                    RequiredValue = "At 90°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL180.PRL182.Add(new PRL182
                {
                    RequiredValue = "At 180°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL180.PRL182.Add(new PRL182
                {
                    RequiredValue = "At 270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region OUT OF ROUNDNESS | PRO183
                objPRL180.PRL183.Add(new PRL183
                {
                    Orientation = "0°-180°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL180.PRL183.Add(new PRL183
                {
                    Orientation = "22.5°-202.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL180.PRL183.Add(new PRL183
                {
                    Orientation = "45°-225°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL180.PRL183.Add(new PRL183
                {
                    Orientation = "67.5°-247.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL180.PRL183.Add(new PRL183
                {
                    Orientation = "90°-270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL180.PRL183.Add(new PRL183
                {
                    Orientation = "112.5°-292.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL180.PRL183.Add(new PRL183
                {
                    Orientation = "135°-315°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL180.PRL183.Add(new PRL183
                {
                    Orientation = "157.5°-337.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                db.PRL180.Add(objPRL180);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL037

                    PRO180 objPRO180 = db.PRO180.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO180 != null)
                    {
                        CopyProtocolData("PRO180", "PRL180", "PRL180.HeaderId=" + objPRL180.HeaderId + " AND PRO180.HeaderId=" + objPRO180.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO181", "PRL181", "HeaderId=" + objPRO180.HeaderId, "LineId,EditedBy,EditedOn", objPRL180.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO182", "PRL182", "HeaderId=" + objPRO180.HeaderId, "LineId,EditedBy,EditedOn", objPRL180.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO183", "PRL183", "HeaderId=" + objPRO180.HeaderId, "LineId,EditedBy,EditedOn", objPRL180.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO184", "PRL184", "HeaderId=" + objPRO180.HeaderId, "LineId,EditedBy,EditedOn", objPRL180.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO184_2", "PRL184_2", "HeaderId=" + objPRO180.HeaderId, "LineId,EditedBy,EditedOn", objPRL180.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO184_3", "PRL184_3", "HeaderId=" + objPRO180.HeaderId, "LineId,EditedBy,EditedOn", objPRL180.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO184_4", "PRL184_4", "HeaderId=" + objPRO180.HeaderId, "LineId,EditedBy,EditedOn", objPRL180.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO184_5", "PRL184_5", "HeaderId=" + objPRO180.HeaderId, "LineId,EditedBy,EditedOn", objPRL180.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO184_6", "PRL184_6", "HeaderId=" + objPRO180.HeaderId, "LineId,EditedBy,EditedOn", objPRL180.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO184_7", "PRL184_7", "HeaderId=" + objPRO180.HeaderId, "LineId,EditedBy,EditedOn", objPRL180.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO184_8", "PRL184_8", "HeaderId=" + objPRO180.HeaderId, "LineId,EditedBy,EditedOn", objPRL180.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                    }
                    #endregion
                }
                returnValue = objPRL180.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.SETUP_INSPECTION_REPORT_FOR_FORMED_HEMISPHERICAL_DEND_WITHOUT_CROWN_AND_TIER.GetStringValue())
            {
                #region Add New Entry in PRL185 For PROTOCOL038
                PRL185 objPRL185 = new PRL185();
                objPRL185.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL185.QualityProject = HeaderData.QualityProject;
                objPRL185.EquipmentNo = HeaderData.EquipmentNo;
                objPRL185.DrawingNo = HeaderData.DrawingNo;
                objPRL185.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL185.DCRNo = HeaderData.DCRNo;
                objPRL185.SeamNo = HeaderData.SeamNo;
                objPRL185.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL185.Project = HeaderData.Project;
                objPRL185.BU = HeaderData.BU;
                objPRL185.Location = HeaderData.Location;
                objPRL185.StageCode = HeaderData.StageCode;
                objPRL185.StageSequence = HeaderData.StageSequence;
                objPRL185.ICLRevNo = HeaderData.ICLRevNo;
                objPRL185.CreatedBy = objClsLoginInfo.UserName;
                objPRL185.CreatedOn = DateTime.Now;

                #region DIMENSION
                objPRL185.OverCrowingRemarks = "TOL: 1¼ (1.25) % OF NOMINAL I/S DIA.";
                objPRL185.UnderCrowingRemarks = "TOL:5/8 (0.625) % OF NOMINAL I/S DIA.";
                #endregion

                #region OVALITY
                objPRL185.PRL186.Add(new PRL186
                {
                    Orientation = "0°-180° ",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL185.PRL186.Add(new PRL186
                {
                    Orientation = "22.5°-202.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL185.PRL186.Add(new PRL186
                {
                    Orientation = "45°-225°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL185.PRL186.Add(new PRL186
                {
                    Orientation = "67.5°-247.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL185.PRL186.Add(new PRL186
                {
                    Orientation = "90°-270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL185.PRL186.Add(new PRL186
                {
                    Orientation = "112.5°-292.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL185.PRL186.Add(new PRL186
                {
                    Orientation = "135°-315°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL185.PRL186.Add(new PRL186
                {
                    Orientation = "157.5°-337.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region  THICKNESS
                objPRL185.PRL187.Add(new PRL187
                {
                    ThiknessColumn1 = "PETAL 1",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL185.PRL187.Add(new PRL187
                {
                    ThiknessColumn1 = "PETAL 2",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL185.PRL187.Add(new PRL187
                {
                    ThiknessColumn1 = "PETAL 3",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL185.PRL187.Add(new PRL187
                {
                    ThiknessColumn1 = "PETAL 4",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region DETAIL THICKNESS REPORT
                objPRL185.PRL189_4.Add(new PRL189_4
                {
                    Description = "PETAL 1",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL185.PRL189_4.Add(new PRL189_4
                {
                    Description = "PETAL 2",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL185.PRL189_4.Add(new PRL189_4
                {
                    Description = "PETAL 3",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL185.PRL189_4.Add(new PRL189_4
                {
                    Description = "PETAL 4",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                db.PRL185.Add(objPRL185);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL038

                    PRO185 objPRO185 = db.PRO185.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO185 != null)
                    {
                        CopyProtocolData("PRO185", "PRL185", "PRL185.HeaderId=" + objPRO185.HeaderId + " AND PRO185.HeaderId=" + objPRO185.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO186", "PRL186", "HeaderId=" + objPRO185.HeaderId, "LineId,EditedBy,EditedOn", objPRO185.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO187", "PRL187", "HeaderId=" + objPRO185.HeaderId, "LineId,EditedBy,EditedOn", objPRO185.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO188", "PRL188", "HeaderId=" + objPRO185.HeaderId, "LineId,EditedBy,EditedOn", objPRO185.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO189", "PRL189", "HeaderId=" + objPRO185.HeaderId, "LineId,EditedBy,EditedOn", objPRO185.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO189_2", "PRL189_2", "HeaderId=" + objPRO185.HeaderId, "LineId,EditedBy,EditedOn", objPRO185.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO189_3", "PRL189_3", "HeaderId=" + objPRO185.HeaderId, "LineId,EditedBy,EditedOn", objPRO185.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO189_4", "PRL189_4", "HeaderId=" + objPRO185.HeaderId, "LineId,EditedBy,EditedOn", objPRO185.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO189_5", "PRL189_5", "HeaderId=" + objPRO185.HeaderId, "LineId,EditedBy,EditedOn", objPRO185.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO189_6", "PRL189_6", "HeaderId=" + objPRO185.HeaderId, "LineId,EditedBy,EditedOn", objPRO185.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        //CopyProtocolLinesData("PRO184_7", "PRL184_7", "HeaderId=" + objPRO185.HeaderId, "LineId,EditedBy,EditedOn", objPRO185.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        //CopyProtocolLinesData("PRO184_8", "PRL184_8", "HeaderId=" + objPRO185.HeaderId, "LineId,EditedBy,EditedOn", objPRO185.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                    }
                    #endregion
                }
                returnValue = objPRL185.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.SETUP_INSPECTION_REPORT_FOR_FORMED_HEMISPHERICAL_DEND_WITH_CROWN_AND_TIER.GetStringValue())
            {
                #region Add New Entry in PRL190 For PROTOCOL039
                PRL190 objPRL190 = new PRL190();
                objPRL190.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL190.QualityProject = HeaderData.QualityProject;
                objPRL190.EquipmentNo = HeaderData.EquipmentNo;
                objPRL190.DrawingNo = HeaderData.DrawingNo;
                objPRL190.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL190.DCRNo = HeaderData.DCRNo;
                objPRL190.SeamNo = HeaderData.SeamNo;
                objPRL190.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL190.Project = HeaderData.Project;
                objPRL190.BU = HeaderData.BU;
                objPRL190.Location = HeaderData.Location;
                objPRL190.StageCode = HeaderData.StageCode;
                objPRL190.StageSequence = HeaderData.StageSequence;
                objPRL190.ICLRevNo = HeaderData.ICLRevNo;
                objPRL190.CreatedBy = objClsLoginInfo.UserName;
                objPRL190.CreatedOn = DateTime.Now;

                #region DIMENSIONS OF CROWN REMARK

                objPRL190.OverCrowingRemark = "TOL: 1¼ (1.25) % OF NOMINAL I/S DIA.";

                objPRL190.UnderCrowingRemark = "TOL:5/8 (0.625) % OF NOMINAL I/S DIA.";

                #endregion

                #region OVALITY 
                objPRL190.PRL191.Add(new PRL191
                {
                    Orientation = "0°-180° ",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL190.PRL191.Add(new PRL191
                {
                    Orientation = "22.5°-202.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL190.PRL191.Add(new PRL191
                {
                    Orientation = "45°-225°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL190.PRL191.Add(new PRL191
                {
                    Orientation = "67.5°-247.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL190.PRL191.Add(new PRL191
                {
                    Orientation = "90°-270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL190.PRL191.Add(new PRL191
                {
                    Orientation = "112.5°-292.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL190.PRL191.Add(new PRL191
                {
                    Orientation = "135°-315°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL190.PRL191.Add(new PRL191
                {
                    Orientation = "157.5°-337.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region THICKNESS 
                objPRL190.PRL192.Add(new PRL192
                {
                    ThiknessColumn1 = "PETAL 1 ",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL190.PRL192.Add(new PRL192
                {
                    ThiknessColumn1 = "PETAL 2",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL190.PRL192.Add(new PRL192
                {
                    ThiknessColumn1 = "PETAL 3",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL190.PRL192.Add(new PRL192
                {
                    ThiknessColumn1 = "PETAL 4",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region DESCRIPTION 
                objPRL190.PRL194_5.Add(new PRL194_5
                {
                    Description = "PETAL 1 ",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL190.PRL194_5.Add(new PRL194_5
                {
                    Description = "PETAL 2",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL190.PRL194_5.Add(new PRL194_5
                {
                    Description = "PETAL 3",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL190.PRL194_5.Add(new PRL194_5
                {
                    Description = "PETAL 4",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                db.PRL190.Add(objPRL190);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL039

                    PRO190 objPRO190 = db.PRO190.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO190 != null)
                    {
                        CopyProtocolData("PRO190", "PRL190", "PRL190.HeaderId=" + objPRO190.HeaderId + " AND PRO190.HeaderId=" + objPRO190.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO191", "PRL191", "HeaderId=" + objPRO190.HeaderId, "LineId,EditedBy,EditedOn", objPRO190.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO192", "PRL192", "HeaderId=" + objPRO190.HeaderId, "LineId,EditedBy,EditedOn", objPRO190.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO193", "PRL193", "HeaderId=" + objPRO190.HeaderId, "LineId,EditedBy,EditedOn", objPRO190.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO194", "PRL194", "HeaderId=" + objPRO190.HeaderId, "LineId,EditedBy,EditedOn", objPRO190.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO194_2", "PRL194_2", "HeaderId=" + objPRO190.HeaderId, "LineId,EditedBy,EditedOn", objPRO190.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO194_3", "PRL194_3", "HeaderId=" + objPRO190.HeaderId, "LineId,EditedBy,EditedOn", objPRO190.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO194_4", "PRL194_4", "HeaderId=" + objPRO190.HeaderId, "LineId,EditedBy,EditedOn", objPRO190.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO194_5", "PRL194_5", "HeaderId=" + objPRO190.HeaderId, "LineId,EditedBy,EditedOn", objPRO190.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO194_6", "PRL194_6", "HeaderId=" + objPRO190.HeaderId, "LineId,EditedBy,EditedOn", objPRO190.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO194_7", "PRL194_7", "HeaderId=" + objPRO190.HeaderId, "LineId,EditedBy,EditedOn", objPRO190.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO194_8", "PRL194_8", "HeaderId=" + objPRO190.HeaderId, "LineId,EditedBy,EditedOn", objPRO190.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO194_9", "PRL194_9", "HeaderId=" + objPRO190.HeaderId, "LineId,EditedBy,EditedOn", objPRO190.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                    }
                    #endregion
                }
                returnValue = objPRL190.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.SETUP_INSPECTION_REPORT_FOR_FORMED_ELLIPSOIDAL_TORISPHERICAL_D_END_WITHOUT_CROWN_AND_TIER.GetStringValue())
            {
                #region Add New Entry in PRL195 For PROTOCOL040
                PRL195 objPRL195 = new PRL195();
                objPRL195.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL195.QualityProject = HeaderData.QualityProject;
                objPRL195.EquipmentNo = HeaderData.EquipmentNo;
                objPRL195.DrawingNo = HeaderData.DrawingNo;
                objPRL195.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL195.DCRNo = HeaderData.DCRNo;
                objPRL195.SeamNo = HeaderData.SeamNo;
                objPRL195.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL195.Project = HeaderData.Project;
                objPRL195.BU = HeaderData.BU;
                objPRL195.Location = HeaderData.Location;
                objPRL195.StageCode = HeaderData.StageCode;
                objPRL195.StageSequence = HeaderData.StageSequence;
                objPRL195.ICLRevNo = HeaderData.ICLRevNo;
                objPRL195.CreatedBy = objClsLoginInfo.UserName;
                objPRL195.CreatedOn = DateTime.Now;

                #region DIMENSION
                objPRL195.RemOverCrowing = "TOL: 1¼ (1.25) % OF NOMINAL I/S DIA.";
                objPRL195.RemUnderCrowing = "TOL:5/8 (0.625) % OF NOMINAL I/S DIA.";
                #endregion

                #region OVALITY
                objPRL195.PRL196.Add(new PRL196
                {
                    Orientation = "0°-180° ",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL195.PRL196.Add(new PRL196
                {
                    Orientation = "22.5°-202.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL195.PRL196.Add(new PRL196
                {
                    Orientation = "45°-225°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL195.PRL196.Add(new PRL196
                {
                    Orientation = "67.5°-247.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL195.PRL196.Add(new PRL196
                {
                    Orientation = "90°-270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL195.PRL196.Add(new PRL196
                {
                    Orientation = "112.5°-292.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL195.PRL196.Add(new PRL196
                {
                    Orientation = "135°-315°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL195.PRL196.Add(new PRL196
                {
                    Orientation = "157.5°-337.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region  THICKNESS
                objPRL195.PRL197.Add(new PRL197
                {
                    ThiknessColumn1 = "PETAL 1",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL195.PRL197.Add(new PRL197
                {
                    ThiknessColumn1 = "PETAL 2",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL195.PRL197.Add(new PRL197
                {
                    ThiknessColumn1 = "PETAL 3",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL195.PRL197.Add(new PRL197
                {
                    ThiknessColumn1 = "PETAL 4",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region DETAIL THICKNESS REPORT
                objPRL195.PRL199_4.Add(new PRL199_4
                {
                    Description = "PETAL 1",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL195.PRL199_4.Add(new PRL199_4
                {
                    Description = "PETAL 2",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL195.PRL199_4.Add(new PRL199_4
                {
                    Description = "PETAL 3",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL195.PRL199_4.Add(new PRL199_4
                {
                    Description = "PETAL 4",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                db.PRL195.Add(objPRL195);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL040

                    PRO195 objPRO195 = db.PRO195.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO195 != null)
                    {
                        CopyProtocolData("PRO195", "PRL195", "PRL195.HeaderId=" + objPRO195.HeaderId + " AND PRO195.HeaderId=" + objPRO195.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO196", "PRL196", "HeaderId=" + objPRO195.HeaderId, "LineId,EditedBy,EditedOn", objPRO195.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO197", "PRL197", "HeaderId=" + objPRO195.HeaderId, "LineId,EditedBy,EditedOn", objPRO195.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO198", "PRL198", "HeaderId=" + objPRO195.HeaderId, "LineId,EditedBy,EditedOn", objPRO195.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO199", "PRL199", "HeaderId=" + objPRO195.HeaderId, "LineId,EditedBy,EditedOn", objPRO195.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO199_2", "PRL199_2", "HeaderId=" + objPRO195.HeaderId, "LineId,EditedBy,EditedOn", objPRO195.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO199_3", "PRL199_3", "HeaderId=" + objPRO195.HeaderId, "LineId,EditedBy,EditedOn", objPRO195.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO199_4", "PRL199_4", "HeaderId=" + objPRO195.HeaderId, "LineId,EditedBy,EditedOn", objPRO195.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO199_5", "PRL199_5", "HeaderId=" + objPRO195.HeaderId, "LineId,EditedBy,EditedOn", objPRO195.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO199_6", "PRL199_6", "HeaderId=" + objPRO195.HeaderId, "LineId,EditedBy,EditedOn", objPRO195.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                    }
                    #endregion
                }
                returnValue = objPRL195.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.SETUP_INSPECTION_REPORT_FOR_FORMED_ELLIPSOIDAL_TORISPHERICAL_D_END_WITH_CROWN_AND_TIER.GetStringValue())
            {
                #region Add New Entry in PRL200 For PROTOCOL041
                PRL200 objPRL200 = new PRL200();
                objPRL200.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL200.QualityProject = HeaderData.QualityProject;
                objPRL200.EquipmentNo = HeaderData.EquipmentNo;
                objPRL200.DrawingNo = HeaderData.DrawingNo;
                objPRL200.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL200.DCRNo = HeaderData.DCRNo;
                objPRL200.SeamNo = HeaderData.SeamNo;
                objPRL200.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL200.Project = HeaderData.Project;
                objPRL200.BU = HeaderData.BU;
                objPRL200.Location = HeaderData.Location;
                objPRL200.StageCode = HeaderData.StageCode;
                objPRL200.StageSequence = HeaderData.StageSequence;
                objPRL200.ICLRevNo = HeaderData.ICLRevNo;
                objPRL200.CreatedBy = objClsLoginInfo.UserName;
                objPRL200.CreatedOn = DateTime.Now;

                #region DIMENSION              
                objPRL200.OverCrowingRemarks = "TOL: 1¼ (1.25) % OF NOMINAL I/S DIA.";
                objPRL200.UnderCrowingRemarks = "TOL:5/8 (0.625) % OF NOMINAL I/S DIA.";
                #endregion

                #region OVALITY
                objPRL200.PRL201.Add(new PRL201
                {
                    Orientation = "0°-180° ",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL200.PRL201.Add(new PRL201
                {
                    Orientation = "22.5°-202.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL200.PRL201.Add(new PRL201
                {
                    Orientation = "45°-225°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL200.PRL201.Add(new PRL201
                {
                    Orientation = "67.5°-247.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL200.PRL201.Add(new PRL201
                {
                    Orientation = "90°-270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL200.PRL201.Add(new PRL201
                {
                    Orientation = "112.5°-292.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL200.PRL201.Add(new PRL201
                {
                    Orientation = "135°-315°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL200.PRL201.Add(new PRL201
                {
                    Orientation = "157.5°-337.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region  THICKNESS
                objPRL200.PRL202.Add(new PRL202
                {
                    ThiknessColumn1 = "PETAL 1",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL200.PRL202.Add(new PRL202
                {
                    ThiknessColumn1 = "PETAL 2",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL200.PRL202.Add(new PRL202
                {
                    ThiknessColumn1 = "PETAL 3",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL200.PRL202.Add(new PRL202
                {
                    ThiknessColumn1 = "PETAL 4",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region DETAIL THICKNESS REPORT
                objPRL200.PRL204_5.Add(new PRL204_5
                {
                    Description = "PETAL 1",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL200.PRL204_5.Add(new PRL204_5
                {
                    Description = "PETAL 2",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL200.PRL204_5.Add(new PRL204_5
                {
                    Description = "PETAL 3",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL200.PRL204_5.Add(new PRL204_5
                {
                    Description = "PETAL 4",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                db.PRL200.Add(objPRL200);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL041

                    PRO200 objPRO200 = db.PRO200.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO200 != null)
                    {
                        CopyProtocolData("PRO200", "PRL200", "PRL200.HeaderId=" + objPRO200.HeaderId + " AND PRO200.HeaderId=" + objPRO200.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO201", "PRL201", "HeaderId=" + objPRO200.HeaderId, "LineId,EditedBy,EditedOn", objPRO200.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO202", "PRL202", "HeaderId=" + objPRO200.HeaderId, "LineId,EditedBy,EditedOn", objPRO200.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO203", "PRL203", "HeaderId=" + objPRO200.HeaderId, "LineId,EditedBy,EditedOn", objPRO200.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO204", "PRL204", "HeaderId=" + objPRO200.HeaderId, "LineId,EditedBy,EditedOn", objPRO200.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO204_2", "PRL204_2", "HeaderId=" + objPRO200.HeaderId, "LineId,EditedBy,EditedOn", objPRO200.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO204_3", "PRL204_3", "HeaderId=" + objPRO200.HeaderId, "LineId,EditedBy,EditedOn", objPRO200.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO204_4", "PRL204_4", "HeaderId=" + objPRO200.HeaderId, "LineId,EditedBy,EditedOn", objPRO200.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO204_5", "PRL204_5", "HeaderId=" + objPRO200.HeaderId, "LineId,EditedBy,EditedOn", objPRO200.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO204_6", "PRL204_6", "HeaderId=" + objPRO200.HeaderId, "LineId,EditedBy,EditedOn", objPRO200.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO204_7", "PRL204_7", "HeaderId=" + objPRO200.HeaderId, "LineId,EditedBy,EditedOn", objPRO200.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO204_8", "PRL204_8", "HeaderId=" + objPRO200.HeaderId, "LineId,EditedBy,EditedOn", objPRO200.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO204_9", "PRL204_9", "HeaderId=" + objPRO200.HeaderId, "LineId,EditedBy,EditedOn", objPRO200.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                    }
                    #endregion
                }
                returnValue = objPRL200.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.SETUP_DIMENSION_REPORT_FOR_NOZZLE.GetStringValue())
            {
                #region Add New Entry in PRL205 For PROTOCOL042
                PRL205 objPRL205 = new PRL205();
                objPRL205.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL205.QualityProject = HeaderData.QualityProject;
                objPRL205.EquipmentNo = HeaderData.EquipmentNo;
                objPRL205.DrawingNo = HeaderData.DrawingNo;
                objPRL205.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL205.DCRNo = HeaderData.DCRNo;
                objPRL205.SeamNo = HeaderData.SeamNo;
                objPRL205.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL205.Project = HeaderData.Project;
                objPRL205.BU = HeaderData.BU;
                objPRL205.Location = HeaderData.Location;
                objPRL205.StageCode = HeaderData.StageCode;
                objPRL205.StageSequence = HeaderData.StageSequence;
                objPRL205.ICLRevNo = HeaderData.ICLRevNo;
                objPRL205.CreatedBy = objClsLoginInfo.UserName;
                objPRL205.CreatedOn = DateTime.Now;

                #region OUT OF ROUNDNESS
                objPRL205.PRL207.Add(new PRL207
                {
                    Orient = "0°-180° ",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL205.PRL207.Add(new PRL207
                {
                    Orient = "22.5°-202.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL205.PRL207.Add(new PRL207
                {
                    Orient = "45°-225°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL205.PRL207.Add(new PRL207
                {
                    Orient = "67.5°-247.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL205.PRL207.Add(new PRL207
                {
                    Orient = "90°-270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL205.PRL207.Add(new PRL207
                {
                    Orient = "112.5°-292.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL205.PRL207.Add(new PRL207
                {
                    Orient = "135°-315°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL205.PRL207.Add(new PRL207
                {
                    Orient = "157.5°-337.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                db.PRL205.Add(objPRL205);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL042

                    PRO205 objPRO205 = db.PRO205.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO205 != null)
                    {
                        CopyProtocolData("PRO205", "PRL205", "PRL205.HeaderId=" + objPRO205.HeaderId + " AND PRO205.HeaderId=" + objPRO205.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO206", "PRL206", "HeaderId=" + objPRO205.HeaderId, "LineId,EditedBy,EditedOn", objPRO205.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO207", "PRL207", "HeaderId=" + objPRO205.HeaderId, "LineId,EditedBy,EditedOn", objPRO205.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO208", "PRL208", "HeaderId=" + objPRO205.HeaderId, "LineId,EditedBy,EditedOn", objPRO205.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO209", "PRL209", "HeaderId=" + objPRO205.HeaderId, "LineId,EditedBy,EditedOn", objPRO205.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO209_2", "PRL209_2", "HeaderId=" + objPRO205.HeaderId, "LineId,EditedBy,EditedOn", objPRO205.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                    }
                    #endregion
                }
                returnValue = objPRL205.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_REPORT_FOR_ROLLED_SHELL_LONGITUDINAL_SEAM_V2.GetStringValue())
            {
                #region Add New Entry in PRL210 For PROTOCOL043
                PRL210 objPRL210 = new PRL210();
                objPRL210.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL210.QualityProject = HeaderData.QualityProject;
                objPRL210.EquipmentNo = HeaderData.EquipmentNo;
                objPRL210.DrawingNo = HeaderData.DrawingNo;
                objPRL210.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL210.DCRNo = HeaderData.DCRNo;
                objPRL210.SeamNo = HeaderData.SeamNo;
                objPRL210.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL210.Project = HeaderData.Project;
                objPRL210.BU = HeaderData.BU;
                objPRL210.Location = HeaderData.Location;
                objPRL210.StageCode = HeaderData.StageCode;
                objPRL210.StageSequence = HeaderData.StageSequence;
                objPRL210.ICLRevNo = HeaderData.ICLRevNo;
                objPRL210.CreatedBy = objClsLoginInfo.UserName;
                objPRL210.CreatedOn = DateTime.Now;

                #region OUT OF ROUNDNESS
                objPRL210.PRL211.Add(new PRL211
                {
                    Orientation = "0°-180° ",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL210.PRL211.Add(new PRL211
                {
                    Orientation = "22.5°-205.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL210.PRL211.Add(new PRL211
                {
                    Orientation = "45°-225°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL210.PRL211.Add(new PRL211
                {
                    Orientation = "67.5°-247.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL210.PRL211.Add(new PRL211
                {
                    Orientation = "90°-270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL210.PRL211.Add(new PRL211
                {
                    Orientation = "112.5°-292.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL210.PRL211.Add(new PRL211
                {
                    Orientation = "135°-315°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL210.PRL211.Add(new PRL211
                {
                    Orientation = "157.5°-337.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region CIRCUMFERENCE MEASUREMENT
                objPRL210.PRL212.Add(new PRL212
                {
                    Location = "TOP",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL210.PRL212.Add(new PRL212
                {
                    Location = "MIDDLE",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL210.PRL212.Add(new PRL212
                {
                    Location = "BOTTOM",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region TOTAL SHELL HEIGHT
                objPRL210.PRL213.Add(new PRL213
                {
                    RequiredValue = "AT 0°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL210.PRL213.Add(new PRL213
                {
                    RequiredValue = "AT 90°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL210.PRL213.Add(new PRL213
                {
                    RequiredValue = "AT 180°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL210.PRL213.Add(new PRL213
                {
                    RequiredValue = "AT 270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                db.PRL210.Add(objPRL210);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL043

                    PRO210 objPRO210 = db.PRO210.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO210 != null)
                    {
                        CopyProtocolData("PRO210", "PRL210", "PRL210.HeaderId=" + objPRO210.HeaderId + " AND PRO210.HeaderId=" + objPRO210.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO211", "PRL211", "HeaderId=" + objPRO210.HeaderId, "LineId,EditedBy,EditedOn", objPRO210.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO212", "PRL212", "HeaderId=" + objPRO210.HeaderId, "LineId,EditedBy,EditedOn", objPRO210.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO213", "PRL213", "HeaderId=" + objPRO210.HeaderId, "LineId,EditedBy,EditedOn", objPRO210.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO214", "PRL214", "HeaderId=" + objPRO210.HeaderId, "LineId,EditedBy,EditedOn", objPRO210.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO214_2", "PRL214_2", "HeaderId=" + objPRO210.HeaderId, "LineId,EditedBy,EditedOn", objPRO210.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                    }
                    #endregion
                }
                returnValue = objPRL210.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_REPORT_FOR_CIRCUMFERENTIAL_SEAM_V2.GetStringValue())
            {
                #region Add New Entry in PRL215 For PROTOCOL044
                PRL215 objPRL215 = new PRL215();
                objPRL215.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL215.QualityProject = HeaderData.QualityProject;
                objPRL215.EquipmentNo = HeaderData.EquipmentNo;
                objPRL215.DrawingNo = HeaderData.DrawingNo;
                objPRL215.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL215.DCRNo = HeaderData.DCRNo;
                objPRL215.SeamNo = HeaderData.SeamNo;
                objPRL215.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL215.Project = HeaderData.Project;
                objPRL215.BU = HeaderData.BU;
                objPRL215.Location = HeaderData.Location;
                objPRL215.StageCode = HeaderData.StageCode;
                objPRL215.StageSequence = HeaderData.StageSequence;
                objPRL215.ICLRevNo = HeaderData.ICLRevNo;
                objPRL215.CreatedBy = objClsLoginInfo.UserName;
                objPRL215.CreatedOn = DateTime.Now;

                #region TOTAL HEIGHT
                objPRL215.PRL217.Add(new PRL217
                {
                    Orientation = "0°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL215.PRL217.Add(new PRL217
                {
                    Orientation = "90°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL215.PRL217.Add(new PRL217
                {
                    Orientation = "180°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL215.PRL217.Add(new PRL217
                {
                    Orientation = "270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                #region OUT OF ROUNDNESS
                objPRL215.PRL218.Add(new PRL218
                {
                    Orientation = "0°-180° ",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL215.PRL218.Add(new PRL218
                {
                    Orientation = "22.5°-202.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL215.PRL218.Add(new PRL218
                {
                    Orientation = "45°-225°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL215.PRL218.Add(new PRL218
                {
                    Orientation = "67.5°-247.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL215.PRL218.Add(new PRL218
                {
                    Orientation = "90°-270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL215.PRL218.Add(new PRL218
                {
                    Orientation = "112.5°-292.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL215.PRL218.Add(new PRL218
                {
                    Orientation = "135°-315°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL215.PRL218.Add(new PRL218
                {
                    Orientation = "157.5°-337.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                db.PRL215.Add(objPRL215);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL044

                    PRO215 objPRO215 = db.PRO215.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO215 != null)
                    {
                        CopyProtocolData("PRO215", "PRL215", "PRL215.HeaderId=" + objPRO215.HeaderId + " AND PRO215.HeaderId=" + objPRO215.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO216", "PRL216", "HeaderId=" + objPRO215.HeaderId, "LineId,EditedBy,EditedOn", objPRO215.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO217", "PRL217", "HeaderId=" + objPRO215.HeaderId, "LineId,EditedBy,EditedOn", objPRO215.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO218", "PRL218", "HeaderId=" + objPRO215.HeaderId, "LineId,EditedBy,EditedOn", objPRO215.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO219", "PRL219", "HeaderId=" + objPRO215.HeaderId, "LineId,EditedBy,EditedOn", objPRO215.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO219_2", "PRL219_2", "HeaderId=" + objPRO215.HeaderId, "LineId,EditedBy,EditedOn", objPRO215.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO219_3", "PRL219_3", "HeaderId=" + objPRO215.HeaderId, "LineId,EditedBy,EditedOn", objPRO215.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO219_4", "PRL219_4", "HeaderId=" + objPRO215.HeaderId, "LineId,EditedBy,EditedOn", objPRO215.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO219_5", "PRL219_5", "HeaderId=" + objPRO215.HeaderId, "LineId,EditedBy,EditedOn", objPRO215.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                    }
                    #endregion
                }
                returnValue = objPRL215.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_INSPECTION_REPORT_FOR_FORMED_HEMISPHERICAL_D_END.GetStringValue())
            {
                #region Add New Entry in PRL220 For PROTOCOL045
                PRL220 objPRL220 = new PRL220();
                objPRL220.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL220.QualityProject = HeaderData.QualityProject;
                objPRL220.EquipmentNo = HeaderData.EquipmentNo;
                objPRL220.DrawingNo = HeaderData.DrawingNo;
                objPRL220.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL220.DCRNo = HeaderData.DCRNo;
                objPRL220.SeamNo = HeaderData.SeamNo;
                objPRL220.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL220.Project = HeaderData.Project;
                objPRL220.BU = HeaderData.BU;
                objPRL220.Location = HeaderData.Location;
                objPRL220.StageCode = HeaderData.StageCode;
                objPRL220.StageSequence = HeaderData.StageSequence;
                objPRL220.ICLRevNo = HeaderData.ICLRevNo;
                objPRL220.CreatedBy = objClsLoginInfo.UserName;
                objPRL220.CreatedOn = DateTime.Now;

                #region TOTAL HEIGHT
                objPRL220.PRL223.Add(new PRL223
                {
                    TotalHeight1 = "0°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL220.PRL223.Add(new PRL223
                {
                    TotalHeight1 = "90°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL220.PRL223.Add(new PRL223
                {
                    TotalHeight1 = "180°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL220.PRL223.Add(new PRL223
                {
                    TotalHeight1 = "270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                #region OVALITY
                objPRL220.PRL221.Add(new PRL221
                {
                    Orientation = "0°-180°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL220.PRL221.Add(new PRL221
                {
                    Orientation = "22.5°-202.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL220.PRL221.Add(new PRL221
                {
                    Orientation = "45°-225°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL220.PRL221.Add(new PRL221
                {
                    Orientation = "67.5°-247.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL220.PRL221.Add(new PRL221
                {
                    Orientation = "90°-270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL220.PRL221.Add(new PRL221
                {
                    Orientation = "112.5°-292.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL220.PRL221.Add(new PRL221
                {
                    Orientation = "135°-315°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL220.PRL221.Add(new PRL221
                {
                    Orientation = "157.5°-337.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                db.PRL220.Add(objPRL220);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL045

                    PRO220 objPRO220 = db.PRO220.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO220 != null)
                    {
                        CopyProtocolData("PRO220", "PRL220", "PRL220.HeaderId=" + objPRO220.HeaderId + " AND PRO220.HeaderId=" + objPRO220.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO221", "PRL221", "HeaderId=" + objPRO220.HeaderId, "LineId,EditedBy,EditedOn", objPRO220.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO222", "PRL222", "HeaderId=" + objPRO220.HeaderId, "LineId,EditedBy,EditedOn", objPRO220.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO223", "PRL223", "HeaderId=" + objPRO220.HeaderId, "LineId,EditedBy,EditedOn", objPRO220.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO224", "PRL224", "HeaderId=" + objPRO220.HeaderId, "LineId,EditedBy,EditedOn", objPRO220.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO224_2", "PRL224_2", "HeaderId=" + objPRO220.HeaderId, "LineId,EditedBy,EditedOn", objPRO220.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO224_3", "PRL224_3", "HeaderId=" + objPRO220.HeaderId, "LineId,EditedBy,EditedOn", objPRO220.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                    }
                    #endregion
                }
                returnValue = objPRL220.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_INSPECTION_REPORT_FOR_ELLIPSOIDAL_TORISPHERICAL_FORMED_D_END.GetStringValue())
            {
                #region Add New Entry in PRL225 For PROTOCOL046
                PRL225 objPRL225 = new PRL225();
                objPRL225.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL225.QualityProject = HeaderData.QualityProject;
                objPRL225.EquipmentNo = HeaderData.EquipmentNo;
                objPRL225.DrawingNo = HeaderData.DrawingNo;
                objPRL225.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL225.DCRNo = HeaderData.DCRNo;
                objPRL225.SeamNo = HeaderData.SeamNo;
                objPRL225.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL225.Project = HeaderData.Project;
                objPRL225.BU = HeaderData.BU;
                objPRL225.Location = HeaderData.Location;
                objPRL225.StageCode = HeaderData.StageCode;
                objPRL225.StageSequence = HeaderData.StageSequence;
                objPRL225.ICLRevNo = HeaderData.ICLRevNo;
                objPRL225.CreatedBy = objClsLoginInfo.UserName;
                objPRL225.CreatedOn = DateTime.Now;

                #region TOTAL HEIGHT
                objPRL225.PRL228.Add(new PRL228
                {
                    TotalHeight1 = "0°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL225.PRL228.Add(new PRL228
                {
                    TotalHeight1 = "90°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL225.PRL228.Add(new PRL228
                {
                    TotalHeight1 = "180°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL225.PRL228.Add(new PRL228
                {
                    TotalHeight1 = "270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                #region OVALITY
                objPRL225.PRL226.Add(new PRL226
                {
                    Orientation = "0°-180°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL225.PRL226.Add(new PRL226
                {
                    Orientation = "22.5°-202.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL225.PRL226.Add(new PRL226
                {
                    Orientation = "45°-225°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL225.PRL226.Add(new PRL226
                {
                    Orientation = "67.5°-247.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL225.PRL226.Add(new PRL226
                {
                    Orientation = "90°-270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL225.PRL226.Add(new PRL226
                {
                    Orientation = "112.5°-292.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL225.PRL226.Add(new PRL226
                {
                    Orientation = "135°-315°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL225.PRL226.Add(new PRL226
                {
                    Orientation = "157.5°-337.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                db.PRL225.Add(objPRL225);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL046
                    PRO225 objPRO225 = db.PRO225.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO225 != null)
                    {
                        CopyProtocolData("PRO225", "PRL225", "PRL225.HeaderId=" + objPRO225.HeaderId + " AND PRO225.HeaderId=" + objPRO225.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO226", "PRL226", "HeaderId=" + objPRO225.HeaderId, "LineId,EditedBy,EditedOn", objPRO225.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO227", "PRL227", "HeaderId=" + objPRO225.HeaderId, "LineId,EditedBy,EditedOn", objPRO225.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO228", "PRL228", "HeaderId=" + objPRO225.HeaderId, "LineId,EditedBy,EditedOn", objPRO225.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO229", "PRL229", "HeaderId=" + objPRO225.HeaderId, "LineId,EditedBy,EditedOn", objPRO225.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO229_2", "PRL229_2", "HeaderId=" + objPRO225.HeaderId, "LineId,EditedBy,EditedOn", objPRO225.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO229_3", "PRL229_3", "HeaderId=" + objPRO225.HeaderId, "LineId,EditedBy,EditedOn", objPRO225.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                    }
                    #endregion
                }
                returnValue = objPRL225.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.WELD_VISUAL_AND_DIMENSION_REPORT_FOR_NOZZLE_V2.GetStringValue())
            {
                #region Add New Entry in PRL230 For PROTOCOL047
                PRL230 objPRL230 = new PRL230();
                objPRL230.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL230.QualityProject = HeaderData.QualityProject;
                objPRL230.EquipmentNo = HeaderData.EquipmentNo;
                objPRL230.DrawingNo = HeaderData.DrawingNo;
                objPRL230.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL230.DCRNo = HeaderData.DCRNo;
                objPRL230.SeamNo = HeaderData.SeamNo;
                objPRL230.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL230.Project = HeaderData.Project;
                objPRL230.BU = HeaderData.BU;
                objPRL230.Location = HeaderData.Location;
                objPRL230.StageCode = HeaderData.StageCode;
                objPRL230.StageSequence = HeaderData.StageSequence;
                objPRL230.ICLRevNo = HeaderData.ICLRevNo;
                objPRL230.CreatedBy = objClsLoginInfo.UserName;
                objPRL230.CreatedOn = DateTime.Now;

                #region OUT OF ROUNDNESS
                objPRL230.PRL231.Add(new PRL231
                {
                    Orientation = "0°-180° ",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL230.PRL231.Add(new PRL231
                {
                    Orientation = "22.5°-202.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL230.PRL231.Add(new PRL231
                {
                    Orientation = "45°-225°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL230.PRL231.Add(new PRL231
                {
                    Orientation = "67.5°-247.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL230.PRL231.Add(new PRL231
                {
                    Orientation = "90°-270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL230.PRL231.Add(new PRL231
                {
                    Orientation = "112.5°-292.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL230.PRL231.Add(new PRL231
                {
                    Orientation = "135°-315°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL230.PRL231.Add(new PRL231
                {
                    Orientation = "157.5°-337.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                db.PRL230.Add(objPRL230);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL047
                    PRO230 objPRO230 = db.PRO230.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO230 != null)
                    {
                        CopyProtocolData("PRO230", "PRL230", "PRL230.HeaderId=" + objPRO230.HeaderId + " AND PRO230.HeaderId=" + objPRO230.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO231", "PRL231", "HeaderId=" + objPRO230.HeaderId, "LineId,EditedBy,EditedOn", objPRO230.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO232", "PRL232", "HeaderId=" + objPRO230.HeaderId, "LineId,EditedBy,EditedOn", objPRO230.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                    }
                    #endregion
                }
                returnValue = objPRL230.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.SETUP_AND_DIMENSION_REPORT_FOR_ATTACHMENT.GetStringValue())
            {
                #region Add New Entry in PRL235 For PROTOCOL048
                PRL235 objPRL235 = new PRL235();
                objPRL235.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL235.QualityProject = HeaderData.QualityProject;
                objPRL235.EquipmentNo = HeaderData.EquipmentNo;
                objPRL235.DrawingNo = HeaderData.DrawingNo;
                objPRL235.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL235.DCRNo = HeaderData.DCRNo;
                objPRL235.SeamNo = HeaderData.SeamNo;
                objPRL235.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL235.Project = HeaderData.Project;
                objPRL235.BU = HeaderData.BU;
                objPRL235.Location = HeaderData.Location;
                objPRL235.StageCode = HeaderData.StageCode;
                objPRL235.StageSequence = HeaderData.StageSequence;
                objPRL235.ICLRevNo = HeaderData.ICLRevNo;
                objPRL235.CreatedBy = objClsLoginInfo.UserName;
                objPRL235.CreatedOn = DateTime.Now;

                db.PRL235.Add(objPRL235);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL048
                    PRO235 objPRO235 = db.PRO235.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO235 != null)
                    {
                        CopyProtocolData("PRO235", "PRL235", "PRL235.HeaderId=" + objPRO235.HeaderId + " AND PRO235.HeaderId=" + objPRO235.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO236", "PRL236", "HeaderId=" + objPRO235.HeaderId, "LineId,EditedBy,EditedOn", objPRO235.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                    }
                    #endregion
                }
                returnValue = objPRL235.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_REPORT_FOR_ATTACHMENT.GetStringValue())
            {
                #region Add New Entry in PRL240 For PROTOCOL049
                PRL240 objPRL240 = new PRL240();
                objPRL240.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL240.QualityProject = HeaderData.QualityProject;
                objPRL240.EquipmentNo = HeaderData.EquipmentNo;
                objPRL240.DrawingNo = HeaderData.DrawingNo;
                objPRL240.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL240.DCRNo = HeaderData.DCRNo;
                objPRL240.SeamNo = HeaderData.SeamNo;
                objPRL240.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL240.Project = HeaderData.Project;
                objPRL240.BU = HeaderData.BU;
                objPRL240.Location = HeaderData.Location;
                objPRL240.StageCode = HeaderData.StageCode;
                objPRL240.StageSequence = HeaderData.StageSequence;
                objPRL240.ICLRevNo = HeaderData.ICLRevNo;
                objPRL240.CreatedBy = objClsLoginInfo.UserName;
                objPRL240.CreatedOn = DateTime.Now;

                db.PRL240.Add(objPRL240);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL049
                    PRO240 objPRO240 = db.PRO240.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO240 != null)
                    {
                        CopyProtocolData("PRO240", "PRL240", "PRL240.HeaderId=" + objPRO240.HeaderId + " AND PRO240.HeaderId=" + objPRO240.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO241", "PRL241", "HeaderId=" + objPRO240.HeaderId, "LineId,EditedBy,EditedOn", objPRO240.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                    }
                    #endregion
                }
                returnValue = objPRL240.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_REPORT_BEFORE_WELD_OVERALY_ON_SHELL.GetStringValue())
            {
                #region Add New Entry in PRL245 For PROTOCOL050
                PRL245 objPRL245 = new PRL245();
                objPRL245.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL245.QualityProject = HeaderData.QualityProject;
                objPRL245.EquipmentNo = HeaderData.EquipmentNo;
                objPRL245.DrawingNo = HeaderData.DrawingNo;
                objPRL245.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL245.DCRNo = HeaderData.DCRNo;
                objPRL245.SeamNo = HeaderData.SeamNo;
                objPRL245.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL245.Project = HeaderData.Project;
                objPRL245.BU = HeaderData.BU;
                objPRL245.Location = HeaderData.Location;
                objPRL245.StageCode = HeaderData.StageCode;
                objPRL245.StageSequence = HeaderData.StageSequence;
                objPRL245.ICLRevNo = HeaderData.ICLRevNo;
                objPRL245.CreatedBy = objClsLoginInfo.UserName;
                objPRL245.CreatedOn = DateTime.Now;

                #region OUT OF ROUNDNESS
                objPRL245.PRL246.Add(new PRL246
                {
                    Orientation = "0°-180° ",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL245.PRL246.Add(new PRL246
                {
                    Orientation = "22.5°-202.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL245.PRL246.Add(new PRL246
                {
                    Orientation = "45°-225°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL245.PRL246.Add(new PRL246
                {
                    Orientation = "67.5°-247.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL245.PRL246.Add(new PRL246
                {
                    Orientation = "90°-270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL245.PRL246.Add(new PRL246
                {
                    Orientation = "112.5°-292.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL245.PRL246.Add(new PRL246
                {
                    Orientation = "135°-315°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL245.PRL246.Add(new PRL246
                {
                    Orientation = "157.5°-337.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region CIRCUMFERENCE MEASUREMENT
                objPRL245.PRL247.Add(new PRL247
                {
                    Location = "TOP",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL245.PRL247.Add(new PRL247
                {
                    Location = "MIDDLE",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL245.PRL247.Add(new PRL247
                {
                    Location = "BOTTOM",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region TOTAL LENGTH
                objPRL245.PRL248.Add(new PRL248
                {
                    RequiredValue = "AT 0°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL245.PRL248.Add(new PRL248
                {
                    RequiredValue = "AT 90°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL245.PRL248.Add(new PRL248
                {
                    RequiredValue = "AT 180°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL245.PRL248.Add(new PRL248
                {
                    RequiredValue = "AT 270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                db.PRL245.Add(objPRL245);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL050
                    PRO245 objPRO245 = db.PRO245.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO245 != null)
                    {
                        CopyProtocolData("PRO245", "PRL245", "PRL245.HeaderId=" + objPRO245.HeaderId + " AND PRO245.HeaderId=" + objPRO245.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO246", "PRL246", "HeaderId=" + objPRO245.HeaderId, "LineId,EditedBy,EditedOn", objPRO245.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO247", "PRL247", "HeaderId=" + objPRO245.HeaderId, "LineId,EditedBy,EditedOn", objPRO245.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO248", "PRL248", "HeaderId=" + objPRO245.HeaderId, "LineId,EditedBy,EditedOn", objPRO245.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);

                    }
                    #endregion
                }
                returnValue = objPRL245.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_REPORT_AFTER_WELD_OVERALY_ON_SHELL.GetStringValue())
            {
                #region Add New Entry in PRL250 For PROTOCOL051
                PRL250 objPRL250 = new PRL250();
                objPRL250.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL250.QualityProject = HeaderData.QualityProject;
                objPRL250.EquipmentNo = HeaderData.EquipmentNo;
                objPRL250.DrawingNo = HeaderData.DrawingNo;
                objPRL250.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL250.DCRNo = HeaderData.DCRNo;
                objPRL250.SeamNo = HeaderData.SeamNo;
                objPRL250.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL250.Project = HeaderData.Project;
                objPRL250.BU = HeaderData.BU;
                objPRL250.Location = HeaderData.Location;
                objPRL250.StageCode = HeaderData.StageCode;
                objPRL250.StageSequence = HeaderData.StageSequence;
                objPRL250.ICLRevNo = HeaderData.ICLRevNo;
                objPRL250.CreatedBy = objClsLoginInfo.UserName;
                objPRL250.CreatedOn = DateTime.Now;

                #region OUT OF ROUNDNESS
                objPRL250.PRL251.Add(new PRL251
                {
                    Orientation = "0°-180° ",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL250.PRL251.Add(new PRL251
                {
                    Orientation = "22.5°-205.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL250.PRL251.Add(new PRL251
                {
                    Orientation = "45°-225°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL250.PRL251.Add(new PRL251
                {
                    Orientation = "67.5°-247.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL250.PRL251.Add(new PRL251
                {
                    Orientation = "90°-270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL250.PRL251.Add(new PRL251
                {
                    Orientation = "112.5°-292.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL250.PRL251.Add(new PRL251
                {
                    Orientation = "135°-315°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL250.PRL251.Add(new PRL251
                {
                    Orientation = "157.5°-337.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region CIRCUMFERENCE MEASUREMENT
                objPRL250.PRL252.Add(new PRL252
                {
                    Location = "TOP",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL250.PRL252.Add(new PRL252
                {
                    Location = "MIDDLE",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL250.PRL252.Add(new PRL252
                {
                    Location = "BOTTOM",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region TOTAL SHELL HEIGHT
                objPRL250.PRL253.Add(new PRL253
                {
                    RequiredValue = "AT 0°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL250.PRL253.Add(new PRL253
                {
                    RequiredValue = "AT 90°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL250.PRL253.Add(new PRL253
                {
                    RequiredValue = "AT 180°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL250.PRL253.Add(new PRL253
                {
                    RequiredValue = "AT 270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                db.PRL250.Add(objPRL250);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL051
                    PRO250 objPRO250 = db.PRO250.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO250 != null)
                    {
                        CopyProtocolData("PRO250", "PRL250", "PRL250.HeaderId=" + objPRO250.HeaderId + " AND PRO250.HeaderId=" + objPRO250.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO251", "PRL251", "HeaderId=" + objPRO250.HeaderId, "LineId,EditedBy,EditedOn", objPRO250.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO252", "PRL252", "HeaderId=" + objPRO250.HeaderId, "LineId,EditedBy,EditedOn", objPRO250.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO253", "PRL253", "HeaderId=" + objPRO250.HeaderId, "LineId,EditedBy,EditedOn", objPRO250.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO254", "PRL254", "HeaderId=" + objPRO250.HeaderId, "LineId,EditedBy,EditedOn", objPRO250.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);

                    }
                    #endregion
                }
                returnValue = objPRL250.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.SETUP_AND_DIMENSION_REPORT_FOR_PEPE_TO_PIPE_PIEPE_TO_FLANGE_PIPE_TO_ELBOW_SEAM_PIPE_TO_FORGING_FORGING_TO_FLANGE_ELBOW_TO_FLANGE.GetStringValue())
            {
                #region Add New Entry in PRL255 For PROTOCOL052
                PRL255 objPRL255 = new PRL255();
                objPRL255.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL255.QualityProject = HeaderData.QualityProject;
                objPRL255.EquipmentNo = HeaderData.EquipmentNo;
                objPRL255.DrawingNo = HeaderData.DrawingNo;
                objPRL255.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL255.DCRNo = HeaderData.DCRNo;
                objPRL255.SeamNo = HeaderData.SeamNo;
                objPRL255.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL255.Project = HeaderData.Project;
                objPRL255.BU = HeaderData.BU;
                objPRL255.Location = HeaderData.Location;
                objPRL255.StageCode = HeaderData.StageCode;
                objPRL255.StageSequence = HeaderData.StageSequence;
                objPRL255.ICLRevNo = HeaderData.ICLRevNo;
                objPRL255.CreatedBy = objClsLoginInfo.UserName;
                objPRL255.CreatedOn = DateTime.Now;



                #region TOTAL LENGTH
                objPRL255.PRL256.Add(new PRL256
                {
                    RequiredValue = "AT 0°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL255.PRL256.Add(new PRL256
                {
                    RequiredValue = "AT 90°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL255.PRL256.Add(new PRL256
                {
                    RequiredValue = "AT 180°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL255.PRL256.Add(new PRL256
                {
                    RequiredValue = "AT 270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                db.PRL255.Add(objPRL255);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL052
                    PRO255 objPRO255 = db.PRO255.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO255 != null)
                    {
                        CopyProtocolData("PRO255", "PRL255", "PRL255.HeaderId=" + objPRO255.HeaderId + " AND PRO255.HeaderId=" + objPRO255.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO256", "PRL256", "HeaderId=" + objPRO255.HeaderId, "LineId,EditedBy,EditedOn", objPRO255.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO257", "PRL257", "HeaderId=" + objPRO255.HeaderId, "LineId,EditedBy,EditedOn", objPRO255.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO258", "PRL258", "HeaderId=" + objPRO255.HeaderId, "LineId,EditedBy,EditedOn", objPRO255.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO259", "PRL259", "HeaderId=" + objPRO255.HeaderId, "LineId,EditedBy,EditedOn", objPRO255.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO259_2", "PRL259_2", "HeaderId=" + objPRO255.HeaderId, "LineId,EditedBy,EditedOn", objPRO255.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO259_3", "PRL259_3", "HeaderId=" + objPRO255.HeaderId, "LineId,EditedBy,EditedOn", objPRO255.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO259_4", "PRL259_4", "HeaderId=" + objPRO255.HeaderId, "LineId,EditedBy,EditedOn", objPRO255.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);

                    }
                    #endregion
                }
                returnValue = objPRL255.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_REPORT_FOR_PEPE_TO_PIPE_PIEPE_TO_FLANGE_PIPE_TO_ELBOW_SEAM_PIPE_TO_FORGING_FORGING_TO_FLANGE_ELBOW_TO_FLANGE.GetStringValue())
            {
                #region Add New Entry in PRL260 For PROTOCOL053
                PRL260 objPRL260 = new PRL260();
                objPRL260.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL260.QualityProject = HeaderData.QualityProject;
                objPRL260.EquipmentNo = HeaderData.EquipmentNo;
                objPRL260.DrawingNo = HeaderData.DrawingNo;
                objPRL260.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL260.DCRNo = HeaderData.DCRNo;
                objPRL260.SeamNo = HeaderData.SeamNo;
                objPRL260.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL260.Project = HeaderData.Project;
                objPRL260.BU = HeaderData.BU;
                objPRL260.Location = HeaderData.Location;
                objPRL260.StageCode = HeaderData.StageCode;
                objPRL260.StageSequence = HeaderData.StageSequence;
                objPRL260.ICLRevNo = HeaderData.ICLRevNo;
                objPRL260.CreatedBy = objClsLoginInfo.UserName;
                objPRL260.CreatedOn = DateTime.Now;



                #region TOTAL LENGTH
                objPRL260.PRL261.Add(new PRL261
                {
                    RequiredValue = "AT 0°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL260.PRL261.Add(new PRL261
                {
                    RequiredValue = "AT 90°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL260.PRL261.Add(new PRL261
                {
                    RequiredValue = "AT 180°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL260.PRL261.Add(new PRL261
                {
                    RequiredValue = "AT 270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                db.PRL260.Add(objPRL260);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL053
                    PRO260 objPRO260 = db.PRO260.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO260 != null)
                    {
                        CopyProtocolData("PRO260", "PRL260", "PRL260.HeaderId=" + objPRO260.HeaderId + " AND PRO260.HeaderId=" + objPRO260.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO261", "PRL261", "HeaderId=" + objPRO260.HeaderId, "LineId,EditedBy,EditedOn", objPRO260.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO262", "PRL262", "HeaderId=" + objPRO260.HeaderId, "LineId,EditedBy,EditedOn", objPRO260.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO263", "PRL263", "HeaderId=" + objPRO260.HeaderId, "LineId,EditedBy,EditedOn", objPRO260.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO264", "PRL264", "HeaderId=" + objPRO260.HeaderId, "LineId,EditedBy,EditedOn", objPRO260.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO264_2", "PRL264_2", "HeaderId=" + objPRO260.HeaderId, "LineId,EditedBy,EditedOn", objPRO260.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);


                    }
                    #endregion
                }
                returnValue = objPRL260.HeaderId;
            }

            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_SETUP_AND_DIMENSION_INSPECTION_OF_TSR.GetStringValue())
            {
                #region Add New Entry in PRL265 For PROTOCOL054
                PRL265 objPRL265 = new PRL265();
                objPRL265.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL265.QualityProject = HeaderData.QualityProject;
                objPRL265.EquipmentNo = HeaderData.EquipmentNo;
                objPRL265.DrawingNo = HeaderData.DrawingNo;
                objPRL265.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL265.DCRNo = HeaderData.DCRNo;
                objPRL265.SeamNo = HeaderData.SeamNo;
                objPRL265.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL265.Project = HeaderData.Project;
                objPRL265.BU = HeaderData.BU;
                objPRL265.Location = HeaderData.Location;
                objPRL265.StageCode = HeaderData.StageCode;
                objPRL265.StageSequence = HeaderData.StageSequence;
                objPRL265.ICLRevNo = HeaderData.ICLRevNo;
                objPRL265.CreatedBy = objClsLoginInfo.UserName;
                objPRL265.CreatedOn = DateTime.Now;



                #region ORIENTATION 
                objPRL265.PRL266.Add(new PRL266
                {
                    Orientation = "0°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL265.PRL266.Add(new PRL266
                {
                    Orientation = "30°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL265.PRL266.Add(new PRL266
                {
                    Orientation = "60°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL265.PRL266.Add(new PRL266
                {
                    Orientation = "90°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL265.PRL266.Add(new PRL266
                {
                    Orientation = "120°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL265.PRL266.Add(new PRL266
                {
                    Orientation = "150°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL265.PRL266.Add(new PRL266
                {
                    Orientation = "180°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL265.PRL266.Add(new PRL266
                {
                    Orientation = "210°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL265.PRL266.Add(new PRL266
                {
                    Orientation = "240°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL265.PRL266.Add(new PRL266
                {
                    Orientation = "270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL265.PRL266.Add(new PRL266
                {
                    Orientation = "300°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL265.PRL266.Add(new PRL266
                {
                    Orientation = "330°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                db.PRL265.Add(objPRL265);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL054
                    PRO265 objPRO265 = db.PRO265.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO265 != null)
                    {
                        CopyProtocolData("PRO265", "PRL265", "PRL265.HeaderId=" + objPRO265.HeaderId + " AND PRO265.HeaderId=" + objPRO265.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO266", "PRL266", "HeaderId=" + objPRO265.HeaderId, "LineId,EditedBy,EditedOn", objPRO265.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO267", "PRL267", "HeaderId=" + objPRO265.HeaderId, "LineId,EditedBy,EditedOn", objPRO265.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO268", "PRL268", "HeaderId=" + objPRO265.HeaderId, "LineId,EditedBy,EditedOn", objPRO265.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);

                    }
                    #endregion
                }
                returnValue = objPRL265.HeaderId;
            }

            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_VISUAL_AND_DIMENSION_INSPECTION_OF_TSR.GetStringValue())
            {
                #region Add New Entry in PRL270 For PROTOCOL055
                PRL270 objPRL270 = new PRL270();
                objPRL270.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL270.QualityProject = HeaderData.QualityProject;
                objPRL270.EquipmentNo = HeaderData.EquipmentNo;
                objPRL270.DrawingNo = HeaderData.DrawingNo;
                objPRL270.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL270.DCRNo = HeaderData.DCRNo;
                objPRL270.SeamNo = HeaderData.SeamNo;
                objPRL270.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL270.Project = HeaderData.Project;
                objPRL270.BU = HeaderData.BU;
                objPRL270.Location = HeaderData.Location;
                objPRL270.StageCode = HeaderData.StageCode;
                objPRL270.StageSequence = HeaderData.StageSequence;
                objPRL270.ICLRevNo = HeaderData.ICLRevNo;
                objPRL270.CreatedBy = objClsLoginInfo.UserName;
                objPRL270.CreatedOn = DateTime.Now;



                #region ORIENTATION 
                objPRL270.PRL271.Add(new PRL271
                {
                    Orientation = "0°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL270.PRL271.Add(new PRL271
                {
                    Orientation = "30°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL270.PRL271.Add(new PRL271
                {
                    Orientation = "60°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL270.PRL271.Add(new PRL271
                {
                    Orientation = "90°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL270.PRL271.Add(new PRL271
                {
                    Orientation = "120°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL270.PRL271.Add(new PRL271
                {
                    Orientation = "150°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL270.PRL271.Add(new PRL271
                {
                    Orientation = "180°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL270.PRL271.Add(new PRL271
                {
                    Orientation = "210°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL270.PRL271.Add(new PRL271
                {
                    Orientation = "240°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL270.PRL271.Add(new PRL271
                {
                    Orientation = "270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL270.PRL271.Add(new PRL271
                {
                    Orientation = "300°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL270.PRL271.Add(new PRL271
                {
                    Orientation = "330°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                db.PRL270.Add(objPRL270);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL055
                    PRO270 objPRO270 = db.PRO270.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO270 != null)
                    {
                        CopyProtocolData("PRO270", "PRL270", "PRL270.HeaderId=" + objPRO270.HeaderId + " AND PRO270.HeaderId=" + objPRO270.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO271", "PRL271", "HeaderId=" + objPRO270.HeaderId, "LineId,EditedBy,EditedOn", objPRO270.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);

                    }
                    #endregion
                }
                returnValue = objPRL270.HeaderId;
            }

            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_VISUAL_AND_DIMENSION_REPORT_AFTER_MACHINING_FOR_GASKET_FACE_AND_OTHER_DIMENSIONS_OF_NOZZLE.GetStringValue())
            {
                #region Add New Entry in PRL275 For PROTOCOL056
                PRL275 objPRL275 = new PRL275();
                objPRL275.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL275.QualityProject = HeaderData.QualityProject;
                objPRL275.EquipmentNo = HeaderData.EquipmentNo;
                objPRL275.DrawingNo = HeaderData.DrawingNo;
                objPRL275.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL275.DCRNo = HeaderData.DCRNo;
                objPRL275.SeamNo = HeaderData.SeamNo;
                objPRL275.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL275.Project = HeaderData.Project;
                objPRL275.BU = HeaderData.BU;
                objPRL275.Location = HeaderData.Location;
                objPRL275.StageCode = HeaderData.StageCode;
                objPRL275.StageSequence = HeaderData.StageSequence;
                objPRL275.ICLRevNo = HeaderData.ICLRevNo;
                objPRL275.CreatedBy = objClsLoginInfo.UserName;
                objPRL275.CreatedOn = DateTime.Now;



                #region DESCRIPTION
                objPRL275.PRL276.Add(new PRL276
                {
                    SrNo = 1,
                    Description = "Identification",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL275.PRL276.Add(new PRL276
                {
                    SrNo = 2,
                    Description = "Visual inspection",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL275.PRL276.Add(new PRL276
                {
                    SrNo = 3,
                    Description = "ID before O/L",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL275.PRL276.Add(new PRL276
                {
                    SrNo = 4,
                    Description = "ID after O/L",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL275.PRL276.Add(new PRL276
                {
                    SrNo = 5,
                    Description = "ID Surface finish",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL275.PRL276.Add(new PRL276
                {
                    SrNo = 6,
                    Description = "Gasket face step OD",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });
                objPRL275.PRL276.Add(new PRL276
                {
                    SrNo = 7,
                    Description = "Gasket height",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });
                objPRL275.PRL276.Add(new PRL276
                {
                    SrNo = 8,
                    Description = "Gasket face finish",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });
                objPRL275.PRL276.Add(new PRL276
                {
                    SrNo = 9,
                    Description = "Gasket step OD radius Top",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });
                objPRL275.PRL276.Add(new PRL276
                {
                    SrNo = 10,
                    Description = "Gasket step OD radius Bottom",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });
                objPRL275.PRL276.Add(new PRL276
                {
                    SrNo = 11,
                    Description = "Groove width",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });
                objPRL275.PRL276.Add(new PRL276
                {
                    SrNo = 12,
                    Description = "Groove depth",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });
                objPRL275.PRL276.Add(new PRL276
                {
                    SrNo = 13,
                    Description = "Groove corner radius",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });
                objPRL275.PRL276.Add(new PRL276
                {
                    SrNo = 14,
                    Description = "Groove angle",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });
                objPRL275.PRL276.Add(new PRL276
                {
                    SrNo = 15,
                    Description = "Groove taper finish",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });
                objPRL275.PRL276.Add(new PRL276
                {
                    SrNo = 16,
                    Description = "Groove face finish",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });
                objPRL275.PRL276.Add(new PRL276
                {
                    SrNo = 17,
                    Description = "Gasket groove P.C.D",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });
                objPRL275.PRL276.Add(new PRL276
                {
                    SrNo = 18,
                    Description = "Radius Gasket face to ID",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });
                objPRL275.PRL276.Add(new PRL276
                {
                    SrNo = 19,
                    Description = "Final Visual inspection after O/L",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });
                objPRL275.PRL276.Add(new PRL276
                {
                    SrNo = 20,
                    Description = "O/L Thk.",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });
                #endregion

                db.PRL275.Add(objPRL275);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL056
                    PRO275 objPRO275 = db.PRO275.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO275 != null)
                    {
                        CopyProtocolData("PRO275", "PRL275", "PRL275.HeaderId=" + objPRO275.HeaderId + " AND PRO275.HeaderId=" + objPRO275.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO276", "PRL276", "HeaderId=" + objPRO275.HeaderId, "LineId,EditedBy,EditedOn", objPRO275.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);

                    }
                    #endregion
                }
                returnValue = objPRL275.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.BEFORE_OVERLAY_THICKNESS_MEASUREMENT_REPORT_FOR_SHELL_HEAD_LONG_SEAM_CIRC_SEAM.GetStringValue())
            {
                #region Add New Entry in PRL280 For PROTOCOL057
                PRL280 objPRL280 = new PRL280();
                objPRL280.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL280.QualityProject = HeaderData.QualityProject;
                objPRL280.EquipmentNo = HeaderData.EquipmentNo;
                objPRL280.DrawingNo = HeaderData.DrawingNo;
                objPRL280.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL280.DCRNo = HeaderData.DCRNo;
                objPRL280.SeamNo = HeaderData.SeamNo;
                objPRL280.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL280.Project = HeaderData.Project;
                objPRL280.BU = HeaderData.BU;
                objPRL280.Location = HeaderData.Location;
                objPRL280.StageCode = HeaderData.StageCode;
                objPRL280.StageSequence = HeaderData.StageSequence;
                objPRL280.ICLRevNo = HeaderData.ICLRevNo;
                objPRL280.CreatedBy = objClsLoginInfo.UserName;
                objPRL280.CreatedOn = DateTime.Now;

                db.PRL280.Add(objPRL280);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL057
                    PRO280 objPRO280 = db.PRO280.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO280 != null)
                    {
                        CopyProtocolData("PRO280", "PRL280", "PRL280.HeaderId=" + objPRO280.HeaderId + " AND PRO280.HeaderId=" + objPRO280.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO281", "PRL281", "HeaderId=" + objPRO280.HeaderId, "LineId,EditedBy,EditedOn", objPRO280.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);

                    }
                    #endregion
                }
                returnValue = objPRL280.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.OVERLAY_THICKNESS_MEASUREMENT_REPORT_FOR_SHELL_HEAD_LONG_SEAM_CIRC_SEAM.GetStringValue())
            {
                #region Add New Entry in PRL285 For PROTOCOL058
                PRL285 objPRL285 = new PRL285();
                objPRL285.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL285.QualityProject = HeaderData.QualityProject;
                objPRL285.EquipmentNo = HeaderData.EquipmentNo;
                objPRL285.DrawingNo = HeaderData.DrawingNo;
                objPRL285.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL285.DCRNo = HeaderData.DCRNo;
                objPRL285.SeamNo = HeaderData.SeamNo;
                objPRL285.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL285.Project = HeaderData.Project;
                objPRL285.BU = HeaderData.BU;
                objPRL285.Location = HeaderData.Location;
                objPRL285.StageCode = HeaderData.StageCode;
                objPRL285.StageSequence = HeaderData.StageSequence;
                objPRL285.ICLRevNo = HeaderData.ICLRevNo;
                objPRL285.CreatedBy = objClsLoginInfo.UserName;
                objPRL285.CreatedOn = DateTime.Now;



                db.PRL285.Add(objPRL285);
                db.SaveChanges();

                #region DATA FETCH FROM BEFORE PROTOCOL
                //if (objPRL285 != null)
                //{
                //    PRL280 _obj280 = db.PRL280.Where(w => w.SeamNo == objPRL285.SeamNo).FirstOrDefault();
                //    if (_obj280 != null)
                //    {
                //        List<PRL281> _lstPRL281 = db.PRL281.Where(w => w.HeaderId == _obj280.HeaderId).ToList();
                //        if (_lstPRL281.Count() > 0)
                //        {
                //            for (int i = 0; i < _lstPRL281.Count; i++)
                //            {
                //                PRL286 objPRL286Copy = new PRL286();
                //                objPRL286Copy.SrNo = _lstPRL281[i].SrNo;
                //                objPRL286Copy.HeaderId = objPRL285.HeaderId;
                //                objPRL286Copy.Elevation = _lstPRL281[i].Elevation;
                //                objPRL286Copy.Orientation = _lstPRL281[i].Orientation;
                //                objPRL286Copy.ReqBeforeOverlayThk = _lstPRL281[i].ReqThickness;
                //                objPRL286Copy.ActBeforeOverlayThk = _lstPRL281[i].ActThickness;
                //                objPRL286Copy.ReqAfterOverlayThk = "";
                //                objPRL286Copy.ActAfterOverlayThk = "";
                //                objPRL286Copy.ReqOverlayThk = "";
                //                objPRL286Copy.Remarks = "";
                //                objPRL286Copy.CreatedBy = objClsLoginInfo.UserName;
                //                objPRL286Copy.CreatedOn = DateTime.Now;

                //                db.PRL286.Add(objPRL286Copy);
                //            }
                //            db.SaveChanges();
                //        }
                //    }
                //}
                #endregion

                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL058
                    PRO285 objPRO285 = db.PRO285.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO285 != null)
                    {
                        CopyProtocolData("PRO285", "PRL285", "PRL285.HeaderId=" + objPRO285.HeaderId + " AND PRO285.HeaderId=" + objPRO285.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO286", "PRL286", "HeaderId=" + objPRO285.HeaderId, "LineId,EditedBy,EditedOn", objPRO285.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);

                    }
                    #endregion
                }
                returnValue = objPRL285.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.SETUP_AND_DIMENSION_REPORT_FOR_LONGITUDINAL_SEAM_OF_RING_IN_SEGMENT_STAGE.GetStringValue())
            {
                #region Add New Entry in PRL290 For PROTOCOL059
                PRL290 objPRL290 = new PRL290();
                objPRL290.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL290.QualityProject = HeaderData.QualityProject;
                objPRL290.EquipmentNo = HeaderData.EquipmentNo;
                objPRL290.DrawingNo = HeaderData.DrawingNo;
                objPRL290.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL290.DCRNo = HeaderData.DCRNo;
                objPRL290.SeamNo = HeaderData.SeamNo;
                objPRL290.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL290.Project = HeaderData.Project;
                objPRL290.BU = HeaderData.BU;
                objPRL290.Location = HeaderData.Location;
                objPRL290.StageCode = HeaderData.StageCode;
                objPRL290.StageSequence = HeaderData.StageSequence;
                objPRL290.ICLRevNo = HeaderData.ICLRevNo;
                objPRL290.CreatedBy = objClsLoginInfo.UserName;
                objPRL290.CreatedOn = DateTime.Now;

                #region  Orientation

                objPRL290.PRL291.Add(new PRL291
                {
                    Orientation = "0°-180° ",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL290.PRL291.Add(new PRL291
                {
                    Orientation = "45°-225°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL290.PRL291.Add(new PRL291
                {
                    Orientation = "90°-270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL290.PRL291.Add(new PRL291
                {
                    Orientation = "135°-315°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                db.PRL290.Add(objPRL290);
                db.SaveChanges();


                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL059
                    PRO290 objPRO290 = db.PRO290.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO290 != null)
                    {
                        CopyProtocolData("PRO290", "PRL290", "PRL290.HeaderId=" + objPRO290.HeaderId + " AND PRO290.HeaderId=" + objPRO290.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO291", "PRL291", "HeaderId=" + objPRO290.HeaderId, "LineId,EditedBy,EditedOn", objPRO290.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO292", "PRL292", "HeaderId=" + objPRO290.HeaderId, "LineId,EditedBy,EditedOn", objPRO290.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO293", "PRL293", "HeaderId=" + objPRO290.HeaderId, "LineId,EditedBy,EditedOn", objPRO290.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);


                    }
                    #endregion
                }
                returnValue = objPRL290.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_REPORT_FOR_LONGITUDINAL_SEAM_OF_RING.GetStringValue())
            {
                #region Add New Entry in PRL295 For PROTOCOL060
                PRL295 objPRL295 = new PRL295();
                objPRL295.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL295.QualityProject = HeaderData.QualityProject;
                objPRL295.EquipmentNo = HeaderData.EquipmentNo;
                objPRL295.DrawingNo = HeaderData.DrawingNo;
                objPRL295.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL295.DCRNo = HeaderData.DCRNo;
                objPRL295.SeamNo = HeaderData.SeamNo;
                objPRL295.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL295.Project = HeaderData.Project;
                objPRL295.BU = HeaderData.BU;
                objPRL295.Location = HeaderData.Location;
                objPRL295.StageCode = HeaderData.StageCode;
                objPRL295.StageSequence = HeaderData.StageSequence;
                objPRL295.ICLRevNo = HeaderData.ICLRevNo;
                objPRL295.CreatedBy = objClsLoginInfo.UserName;
                objPRL295.CreatedOn = DateTime.Now;

                #region  Orientation

                objPRL295.PRL296.Add(new PRL296
                {
                    Orientation = "0°-180° ",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL295.PRL296.Add(new PRL296
                {
                    Orientation = "45°-225°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL295.PRL296.Add(new PRL296
                {
                    Orientation = "90°-270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL295.PRL296.Add(new PRL296
                {
                    Orientation = "135°-315°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                #region ORIENTATION FLATNESS  
                objPRL295.PRL298.Add(new PRL298
                {
                    OrientationFlatness = "0°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL295.PRL298.Add(new PRL298
                {
                    OrientationFlatness = "45°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL295.PRL298.Add(new PRL298
                {
                    OrientationFlatness = "90°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL295.PRL298.Add(new PRL298
                {
                    OrientationFlatness = "135°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL295.PRL298.Add(new PRL298
                {
                    OrientationFlatness = "180°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL295.PRL298.Add(new PRL298
                {
                    OrientationFlatness = "225°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                }); objPRL295.PRL298.Add(new PRL298
                {
                    OrientationFlatness = "270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                }); objPRL295.PRL298.Add(new PRL298
                {
                    OrientationFlatness = "315°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                db.PRL295.Add(objPRL295);
                db.SaveChanges();


                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL060
                    PRO295 objPRO295 = db.PRO295.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO295 != null)
                    {
                        CopyProtocolData("PRO295", "PRL295", "PRL295.HeaderId=" + objPRO295.HeaderId + " AND PRO295.HeaderId=" + objPRO295.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO296", "PRL296", "HeaderId=" + objPRO295.HeaderId, "LineId,EditedBy,EditedOn", objPRO295.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO297", "PRL297", "HeaderId=" + objPRO295.HeaderId, "LineId,EditedBy,EditedOn", objPRO295.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO298", "PRL298", "HeaderId=" + objPRO295.HeaderId, "LineId,EditedBy,EditedOn", objPRO295.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);


                    }
                    #endregion
                }
                returnValue = objPRL295.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_SETUP_AND_DIMENSION_OF_TAILING_LUG.GetStringValue())
            {
                #region Add New Entry in PRL300 For PROTOCOL061
                PRL300 objPRL300 = new PRL300();
                objPRL300.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL300.QualityProject = HeaderData.QualityProject;
                objPRL300.EquipmentNo = HeaderData.EquipmentNo;
                objPRL300.DrawingNo = HeaderData.DrawingNo;
                objPRL300.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL300.DCRNo = HeaderData.DCRNo;
                objPRL300.SeamNo = HeaderData.SeamNo;
                objPRL300.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL300.Project = HeaderData.Project;
                objPRL300.BU = HeaderData.BU;
                objPRL300.Location = HeaderData.Location;
                objPRL300.StageCode = HeaderData.StageCode;
                objPRL300.StageSequence = HeaderData.StageSequence;
                objPRL300.ICLRevNo = HeaderData.ICLRevNo;
                objPRL300.CreatedBy = objClsLoginInfo.UserName;
                objPRL300.CreatedOn = DateTime.Now;

                #region DIMENSION 
                objPRL300.PRL301.Add(new PRL301
                {
                    Dimension = "A",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL300.PRL301.Add(new PRL301
                {
                    Dimension = "B",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL300.PRL301.Add(new PRL301
                {
                    Dimension = "C",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL300.PRL301.Add(new PRL301
                {
                    Dimension = "D",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL300.PRL301.Add(new PRL301
                {
                    Dimension = "E",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL300.PRL301.Add(new PRL301
                {
                    Dimension = "F",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL300.PRL301.Add(new PRL301
                {
                    Dimension = "G",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL300.PRL301.Add(new PRL301
                {
                    Dimension = "H",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion



                db.PRL300.Add(objPRL300);
                db.SaveChanges();


                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL061
                    PRO300 objPRO300 = db.PRO300.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO300 != null)
                    {
                        CopyProtocolData("PRO300", "PRL300", "PRL300.HeaderId=" + objPRO300.HeaderId + " AND PRO300.HeaderId=" + objPRO300.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO301", "PRL301", "HeaderId=" + objPRO300.HeaderId, "LineId,EditedBy,EditedOn", objPRO300.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO302", "PRL302", "HeaderId=" + objPRO300.HeaderId, "LineId,EditedBy,EditedOn", objPRO300.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO303", "PRL303", "HeaderId=" + objPRO300.HeaderId, "LineId,EditedBy,EditedOn", objPRO300.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);


                    }
                    #endregion
                }
                returnValue = objPRL300.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_VISUAL_AND_DIMENSION_OF_TAILING_LUG.GetStringValue())
            {
                #region Add New Entry in PRL305 For PROTOCOL062
                PRL305 objPRL305 = new PRL305();
                objPRL305.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL305.QualityProject = HeaderData.QualityProject;
                objPRL305.EquipmentNo = HeaderData.EquipmentNo;
                objPRL305.DrawingNo = HeaderData.DrawingNo;
                objPRL305.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL305.DCRNo = HeaderData.DCRNo;
                objPRL305.SeamNo = HeaderData.SeamNo;
                objPRL305.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL305.Project = HeaderData.Project;
                objPRL305.BU = HeaderData.BU;
                objPRL305.Location = HeaderData.Location;
                objPRL305.StageCode = HeaderData.StageCode;
                objPRL305.StageSequence = HeaderData.StageSequence;
                objPRL305.ICLRevNo = HeaderData.ICLRevNo;
                objPRL305.CreatedBy = objClsLoginInfo.UserName;
                objPRL305.CreatedOn = DateTime.Now;

                #region DIMENSION 
                objPRL305.PRL306.Add(new PRL306
                {
                    Dimension = "A",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL305.PRL306.Add(new PRL306
                {
                    Dimension = "B",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL305.PRL306.Add(new PRL306
                {
                    Dimension = "C",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL305.PRL306.Add(new PRL306
                {
                    Dimension = "D",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL305.PRL306.Add(new PRL306
                {
                    Dimension = "E",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL305.PRL306.Add(new PRL306
                {
                    Dimension = "F",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL305.PRL306.Add(new PRL306
                {
                    Dimension = "G",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL305.PRL306.Add(new PRL306
                {
                    Dimension = "H",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                db.PRL305.Add(objPRL305);
                db.SaveChanges();


                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL062
                    PRO305 objPRO305 = db.PRO305.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO305 != null)
                    {
                        CopyProtocolData("PRO305", "PRL305", "PRL305.HeaderId=" + objPRO305.HeaderId + " AND PRO305.HeaderId=" + objPRO305.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO306", "PRL306", "HeaderId=" + objPRO305.HeaderId, "LineId,EditedBy,EditedOn", objPRO305.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);

                    }
                    #endregion
                }
                returnValue = objPRL305.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.SETUP_AND_DIMENSION_REPORT_FOR_MTB_BLOCK_OR_PTC_LONGITUDINAL_SEAM.GetStringValue())
            {
                #region Add New Entry in PRL310 For PROTOCOL063
                PRL310 objPRL310 = new PRL310();
                objPRL310.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL310.QualityProject = HeaderData.QualityProject;
                objPRL310.EquipmentNo = HeaderData.EquipmentNo;
                objPRL310.DrawingNo = HeaderData.DrawingNo;
                objPRL310.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL310.DCRNo = HeaderData.DCRNo;
                objPRL310.SeamNo = HeaderData.SeamNo;
                objPRL310.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL310.Project = HeaderData.Project;
                objPRL310.BU = HeaderData.BU;
                objPRL310.Location = HeaderData.Location;
                objPRL310.StageCode = HeaderData.StageCode;
                objPRL310.StageSequence = HeaderData.StageSequence;
                objPRL310.ICLRevNo = HeaderData.ICLRevNo;
                objPRL310.CreatedBy = objClsLoginInfo.UserName;
                objPRL310.CreatedOn = DateTime.Now;



                db.PRL310.Add(objPRL310);
                db.SaveChanges();


                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL063
                    PRO310 objPRO310 = db.PRO310.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO310 != null)
                    {
                        CopyProtocolData("PRO310", "PRL310", "PRL310.HeaderId=" + objPRO310.HeaderId + " AND PRO310.HeaderId=" + objPRO310.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO311", "PRL311", "HeaderId=" + objPRO310.HeaderId, "LineId,EditedBy,EditedOn", objPRO310.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO312", "PRL312", "HeaderId=" + objPRO310.HeaderId, "LineId,EditedBy,EditedOn", objPRO310.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);

                    }
                    #endregion
                }
                returnValue = objPRL310.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.SETUP_AND_DIMENSION_REPORT_FOR_LONGITUDINAL_SEAM_OF_TUBESHEET_AND_BAFFLE.GetStringValue())
            {
                #region Add New Entry in PRL315 For PROTOCOL064
                PRL315 objPRL315 = new PRL315();
                objPRL315.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL315.QualityProject = HeaderData.QualityProject;
                objPRL315.EquipmentNo = HeaderData.EquipmentNo;
                objPRL315.DrawingNo = HeaderData.DrawingNo;
                objPRL315.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL315.DCRNo = HeaderData.DCRNo;
                objPRL315.SeamNo = HeaderData.SeamNo;
                objPRL315.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL315.Project = HeaderData.Project;
                objPRL315.BU = HeaderData.BU;
                objPRL315.Location = HeaderData.Location;
                objPRL315.StageCode = HeaderData.StageCode;
                objPRL315.StageSequence = HeaderData.StageSequence;
                objPRL315.ICLRevNo = HeaderData.ICLRevNo;
                objPRL315.CreatedBy = objClsLoginInfo.UserName;
                objPRL315.CreatedOn = DateTime.Now;

                #region  Orientation
                objPRL315.PRL316.Add(new PRL316
                {
                    Orientation = "0°-180° ",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL315.PRL316.Add(new PRL316
                {
                    Orientation = "45°-225°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL315.PRL316.Add(new PRL316
                {
                    Orientation = "90°-270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL315.PRL316.Add(new PRL316
                {
                    Orientation = "135°-315°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                #region ORIENTATION FLATNESS 
                objPRL315.PRL318.Add(new PRL318
                {
                    Location = "0°-180°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL315.PRL318.Add(new PRL318
                {
                    Location = "45°-225°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL315.PRL318.Add(new PRL318
                {
                    Location = "90°-270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL315.PRL318.Add(new PRL318
                {
                    Location = "135°-315°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                db.PRL315.Add(objPRL315);
                db.SaveChanges();


                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL064
                    PRO315 objPRO315 = db.PRO315.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO315 != null)
                    {
                        CopyProtocolData("PRO315", "PRL315", "PRL315.HeaderId=" + objPRO315.HeaderId + " AND PRO315.HeaderId=" + objPRO315.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO316", "PRL316", "HeaderId=" + objPRO315.HeaderId, "LineId,EditedBy,EditedOn", objPRO315.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO317", "PRL317", "HeaderId=" + objPRO315.HeaderId, "LineId,EditedBy,EditedOn", objPRO315.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO318", "PRL318", "HeaderId=" + objPRO315.HeaderId, "LineId,EditedBy,EditedOn", objPRO315.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO319", "PRL319", "HeaderId=" + objPRO315.HeaderId, "LineId,EditedBy,EditedOn", objPRO315.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                    }
                    #endregion
                }
                returnValue = objPRL315.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_REPORT_FOR_LONGITUDINAL_SEAM_OF_TUBESHEET_AND_BAFFLE.GetStringValue())
            {
                #region Add New Entry in PRL320 For PROTOCOL065
                PRL320 objPRL320 = new PRL320();
                objPRL320.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL320.QualityProject = HeaderData.QualityProject;
                objPRL320.EquipmentNo = HeaderData.EquipmentNo;
                objPRL320.DrawingNo = HeaderData.DrawingNo;
                objPRL320.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL320.DCRNo = HeaderData.DCRNo;
                objPRL320.SeamNo = HeaderData.SeamNo;
                objPRL320.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL320.Project = HeaderData.Project;
                objPRL320.BU = HeaderData.BU;
                objPRL320.Location = HeaderData.Location;
                objPRL320.StageCode = HeaderData.StageCode;
                objPRL320.StageSequence = HeaderData.StageSequence;
                objPRL320.ICLRevNo = HeaderData.ICLRevNo;
                objPRL320.CreatedBy = objClsLoginInfo.UserName;
                objPRL320.CreatedOn = DateTime.Now;

                #region  Orientation
                objPRL320.PRL321.Add(new PRL321
                {
                    Orientation = "0°-180° ",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL320.PRL321.Add(new PRL321
                {
                    Orientation = "45°-225°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL320.PRL321.Add(new PRL321
                {
                    Orientation = "90°-270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL320.PRL321.Add(new PRL321
                {
                    Orientation = "135°-315°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                #region ORIENTATION FLATNESS 
                objPRL320.PRL322.Add(new PRL322
                {
                    Location = "0°-180°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL320.PRL322.Add(new PRL322
                {
                    Location = "45°-225°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL320.PRL322.Add(new PRL322
                {
                    Location = "90°-270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL320.PRL322.Add(new PRL322
                {
                    Location = "135°-315°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                db.PRL320.Add(objPRL320);
                db.SaveChanges();


                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL065
                    PRO320 objPRO320 = db.PRO320.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO320 != null)
                    {
                        CopyProtocolData("PRO320", "PRL320", "PRL320.HeaderId=" + objPRO320.HeaderId + " AND PRO320.HeaderId=" + objPRO320.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO321", "PRL321", "HeaderId=" + objPRO320.HeaderId, "LineId,EditedBy,EditedOn", objPRO320.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO322", "PRL322", "HeaderId=" + objPRO320.HeaderId, "LineId,EditedBy,EditedOn", objPRO320.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO323", "PRL323", "HeaderId=" + objPRO320.HeaderId, "LineId,EditedBy,EditedOn", objPRO320.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                    }
                    #endregion
                }
                returnValue = objPRL320.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_OF_FINAL_MACHINING_INSPECTION_OF_TUBESHEET_V2.GetStringValue())
            {
                #region Add New Entry in PRL325 For PROTOCOL066
                PRL325 objPRL325 = new PRL325();
                objPRL325.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL325.QualityProject = HeaderData.QualityProject;
                objPRL325.EquipmentNo = HeaderData.EquipmentNo;
                objPRL325.DrawingNo = HeaderData.DrawingNo;
                objPRL325.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL325.DCRNo = HeaderData.DCRNo;
                objPRL325.SeamNo = HeaderData.SeamNo;
                objPRL325.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL325.Project = HeaderData.Project;
                objPRL325.BU = HeaderData.BU;
                objPRL325.Location = HeaderData.Location;
                objPRL325.StageCode = HeaderData.StageCode;
                objPRL325.StageSequence = HeaderData.StageSequence;
                objPRL325.ICLRevNo = HeaderData.ICLRevNo;
                objPRL325.CreatedBy = objClsLoginInfo.UserName;
                objPRL325.CreatedOn = DateTime.Now;

                #region  Characteristics | PRO326
                objPRL325.PRL326.Add(new PRL326
                {
                    SrNo = 1,
                    Characteristics = "IDENTIFICATION",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL325.PRL326.Add(new PRL326
                {
                    SrNo = 2,
                    Characteristics = "VISUAL INSPECTION",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL325.PRL326.Add(new PRL326
                {
                    SrNo = 3,
                    Characteristics = "LAYOUT OF TUBE HOLES AS PER DRG.",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL325.PRL326.Add(new PRL326
                {
                    SrNo = 4,
                    Characteristics = "TOTAL NO OF TUBE HOLES.",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL325.PRL326.Add(new PRL326
                {
                    SrNo = 5,
                    Characteristics = "GO GAUGING OF HOLES.",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL325.PRL326.Add(new PRL326
                {
                    SrNo = 6,
                    Characteristics = "NO GO GAUGING OF HOLES.",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });
                objPRL325.PRL326.Add(new PRL326
                {
                    SrNo = 7,
                    Characteristics = "O T L OF TUBE HOLES",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });
                objPRL325.PRL326.Add(new PRL326
                {
                    SrNo = 8,
                    Characteristics = "VISUAL INSPECTION OF TUBE HOLE FINISH.",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });
                objPRL325.PRL326.Add(new PRL326
                {
                    SrNo = 9,
                    Characteristics = "BOTH SIDE HOLES SHARP CORNER DEBARRING.",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });
                objPRL325.PRL326.Add(new PRL326
                {
                    SrNo = 10,
                    Characteristics = " \"J\" PREPARATION",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });
                objPRL325.PRL326.Add(new PRL326
                {
                    SrNo = 11,
                    Characteristics = "\"J\" GROOVE DEPTH",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });
                objPRL325.PRL326.Add(new PRL326
                {
                    SrNo = 12,
                    Characteristics = " J' GROOVE  WIDTH AT TOP",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });
                objPRL325.PRL326.Add(new PRL326
                {
                    SrNo = 13,
                    Characteristics = "GO GAUGE  FOR \"J' GROOVES",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });
                objPRL325.PRL326.Add(new PRL326
                {
                    SrNo = 14,
                    Characteristics = "NO GO GAUGE FOR \"J' GROOVES",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });
                objPRL325.PRL326.Add(new PRL326
                {
                    SrNo = 15,
                    Characteristics = "TUBE HOLES SIZE",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });
                objPRL325.PRL326.Add(new PRL326
                {
                    SrNo = 16,
                    Characteristics = "STEPS IN TUBE HOLES AS PER DRAWING",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });
                objPRL325.PRL326.Add(new PRL326
                {
                    SrNo = 17,
                    Characteristics = "TUBE HOLES PITCH",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });
                objPRL325.PRL326.Add(new PRL326
                {
                    SrNo = 18,
                    Characteristics = "TUBE HOLES  LIGAMENT.",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });
                objPRL325.PRL326.Add(new PRL326
                {
                    SrNo = 19,
                    Characteristics = "TOTAL THICKNESS",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });
                objPRL325.PRL326.Add(new PRL326
                {
                    SrNo = 20,
                    Characteristics = "TUBE SHEET OD",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });
                objPRL325.PRL326.Add(new PRL326
                {
                    SrNo = 21,
                    Characteristics = "TUBE SHEET THICKNESS AFTER M/C",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });
                objPRL325.PRL326.Add(new PRL326
                {
                    SrNo = 22,
                    Characteristics = "CHANNEL SIDE TUBE SHEET LIP I.D",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });
                objPRL325.PRL326.Add(new PRL326
                {
                    SrNo = 23,
                    Characteristics = "SHELL SIDE TUBE SHEET LIP I.D",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });
                objPRL325.PRL326.Add(new PRL326
                {
                    SrNo = 24,
                    Characteristics = "WEP OF LIP AS PER DRAWING",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });
                objPRL325.PRL326.Add(new PRL326
                {
                    SrNo = 25,
                    Characteristics = "STEPS ON TUBESHEET THICKNESS AS PER DRW",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });
                objPRL325.PRL326.Add(new PRL326
                {
                    SrNo = 26,
                    Characteristics = "NUMBER OF THREADED HOLES FOR TIE ROD",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });
                objPRL325.PRL326.Add(new PRL326
                {
                    SrNo = 27,
                    Characteristics = "POSITION OF THREADED HOLES FOR TIE ROD",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });
                objPRL325.PRL326.Add(new PRL326
                {
                    SrNo = 28,
                    Characteristics = "GO - NOGO GAUGING FOR THREADED HOLES",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });
                objPRL325.PRL326.Add(new PRL326
                {
                    SrNo = 29,
                    Characteristics = "EXPANSION GROOVE LENGTH DIMENSIONS",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });
                objPRL325.PRL326.Add(new PRL326
                {
                    SrNo = 30,
                    Characteristics = "EXPANSION DIAMETER DIMENSIONS",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });
                objPRL325.PRL326.Add(new PRL326
                {
                    SrNo = 31,
                    Characteristics = "NUMBER OF IMPINGEMENT HOLES",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });
                objPRL325.PRL326.Add(new PRL326
                {
                    SrNo = 32,
                    Characteristics = "DIMENSIONS OF IMPINGEMENT HOLES",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });
                objPRL325.PRL326.Add(new PRL326
                {
                    SrNo = 33,
                    Characteristics = "OUTER MOST HOLES TO TUBESHEET OD DISTANCE",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });

                #endregion

                db.PRL325.Add(objPRL325);
                db.SaveChanges();


                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL066
                    PRO325 objPRO325 = db.PRO325.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO325 != null)
                    {
                        CopyProtocolData("PRO325", "PRL325", "PRL325.HeaderId=" + objPRO325.HeaderId + " AND PRO325.HeaderId=" + objPRO325.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO326", "PRL326", "HeaderId=" + objPRO325.HeaderId, "LineId,EditedBy,EditedOn", objPRO325.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO327", "PRL327", "HeaderId=" + objPRO325.HeaderId, "LineId,EditedBy,EditedOn", objPRO325.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO328", "PRL328", "HeaderId=" + objPRO325.HeaderId, "LineId,EditedBy,EditedOn", objPRO325.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);

                    }
                    #endregion
                }
                returnValue = objPRL325.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_NOZZLE_CUT_OUT_MARKING_ON_SHELL_HEAD.GetStringValue())
            {
                #region Add New Entry in PRL330 For PROTOCOL067
                PRL330 objPRL330 = new PRL330();
                objPRL330.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL330.QualityProject = HeaderData.QualityProject;
                objPRL330.EquipmentNo = HeaderData.EquipmentNo;
                objPRL330.DrawingNo = HeaderData.DrawingNo;
                objPRL330.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL330.DCRNo = HeaderData.DCRNo;
                objPRL330.SeamNo = HeaderData.SeamNo;
                objPRL330.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL330.Project = HeaderData.Project;
                objPRL330.BU = HeaderData.BU;
                objPRL330.Location = HeaderData.Location;
                objPRL330.StageCode = HeaderData.StageCode;
                objPRL330.StageSequence = HeaderData.StageSequence;
                objPRL330.ICLRevNo = HeaderData.ICLRevNo;
                objPRL330.CreatedBy = objClsLoginInfo.UserName;
                objPRL330.CreatedOn = DateTime.Now;

                db.PRL330.Add(objPRL330);
                db.SaveChanges();

                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL067
                    PRO330 objPRO330 = db.PRO330.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO330 != null)
                    {
                        CopyProtocolData("PRO330", "PRL330", "PRL330.HeaderId=" + objPRO330.HeaderId + " AND PRO330.HeaderId=" + objPRO330.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO331", "PRL331", "HeaderId=" + objPRO330.HeaderId, "LineId,EditedBy,EditedOn", objPRO330.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                    }
                    #endregion
                }
                returnValue = objPRL330.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.AIR_TEST_REPORT_OF_REINFORCEMENT_PADS_COVERING_WELD_SEAMS.GetStringValue())
            {
                #region Add New Entry in PRL335 For PROTOCOL068
                PRL335 objPRL335 = new PRL335();
                objPRL335.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL335.QualityProject = HeaderData.QualityProject;
                objPRL335.EquipmentNo = HeaderData.EquipmentNo;
                objPRL335.DrawingNo = HeaderData.DrawingNo;
                objPRL335.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL335.DCRNo = HeaderData.DCRNo;
                objPRL335.SeamNo = HeaderData.SeamNo;
                objPRL335.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL335.Project = HeaderData.Project;
                objPRL335.BU = HeaderData.BU;
                objPRL335.Location = HeaderData.Location;
                objPRL335.StageCode = HeaderData.StageCode;
                objPRL335.StageSequence = HeaderData.StageSequence;
                objPRL335.ICLRevNo = HeaderData.ICLRevNo;
                objPRL335.CreatedBy = objClsLoginInfo.UserName;
                objPRL335.CreatedOn = DateTime.Now;

                #region Notes
                objPRL335.Notes = " 1.AIR TEST OF REINFORCEMENT PAD CHECKED AND FOUND SATISFACTORY. \r\n 2.COMPLETE WELD JOINTS EXAMINED WITH SOAP SOLUTION TO ENSURE NO LEAKAGE.";
                #endregion

                db.PRL335.Add(objPRL335);
                db.SaveChanges();

                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL068
                    PRO335 objPRO335 = db.PRO335.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO335 != null)
                    {
                        CopyProtocolData("PRO335", "PRL335", "PRL335.HeaderId=" + objPRO335.HeaderId + " AND PRO335.HeaderId=" + objPRO335.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO336", "PRL336", "HeaderId=" + objPRO335.HeaderId, "LineId,EditedBy,EditedOn", objPRO335.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO337", "PRL337", "HeaderId=" + objPRO335.HeaderId, "LineId,EditedBy,EditedOn", objPRO335.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                    }
                    #endregion
                }
                returnValue = objPRL335.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.AIR_TEST_REPORT_OF_REINFORCEMENT_PADS_OF_NOZZLE.GetStringValue())
            {
                #region Add New Entry in PRL340 For PROTOCOL069
                PRL340 objPRL340 = new PRL340();
                objPRL340.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL340.QualityProject = HeaderData.QualityProject;
                objPRL340.EquipmentNo = HeaderData.EquipmentNo;
                objPRL340.DrawingNo = HeaderData.DrawingNo;
                objPRL340.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL340.DCRNo = HeaderData.DCRNo;
                objPRL340.SeamNo = HeaderData.SeamNo;
                objPRL340.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL340.Project = HeaderData.Project;
                objPRL340.BU = HeaderData.BU;
                objPRL340.Location = HeaderData.Location;
                objPRL340.StageCode = HeaderData.StageCode;
                objPRL340.StageSequence = HeaderData.StageSequence;
                objPRL340.ICLRevNo = HeaderData.ICLRevNo;
                objPRL340.CreatedBy = objClsLoginInfo.UserName;
                objPRL340.CreatedOn = DateTime.Now;

                #region NOTES
                objPRL340.Notes = " 1. AIR TEST OF REINFORCEMENT PAD CHECKED AND FOUND SATISFACTORY. \r\n 2.COMPLETE WELD JOINTS EXAMINED WITH SOAP SOLUTION TO ENSURE NO LEAKAGE.";

                #endregion

                db.PRL340.Add(objPRL340);
                db.SaveChanges();


                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL069
                    PRO340 objPRO340 = db.PRO340.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO340 != null)
                    {
                        CopyProtocolData("PRO340", "PRL340", "PRL340.HeaderId=" + objPRO340.HeaderId + " AND PRO340.HeaderId=" + objPRO340.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO341", "PRL341", "HeaderId=" + objPRO340.HeaderId, "LineId,EditedBy,EditedOn", objPRO340.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO342", "PRL342", "HeaderId=" + objPRO340.HeaderId, "LineId,EditedBy,EditedOn", objPRO340.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);

                    }
                    #endregion
                }
                returnValue = objPRL340.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.HELIUM_LEAK_TEST_REPORT_V2.GetStringValue())
            {
                #region Add New Entry in PRL345 For PROTOCOL070
                PRL345 objPRL345 = new PRL345();
                objPRL345.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL345.QualityProject = HeaderData.QualityProject;
                objPRL345.EquipmentNo = HeaderData.EquipmentNo;
                objPRL345.DrawingNo = HeaderData.DrawingNo;
                objPRL345.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL345.DCRNo = HeaderData.DCRNo;
                objPRL345.SeamNo = HeaderData.SeamNo;
                objPRL345.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL345.Project = HeaderData.Project;
                objPRL345.BU = HeaderData.BU;
                objPRL345.Location = HeaderData.Location;
                objPRL345.StageCode = HeaderData.StageCode;
                objPRL345.StageSequence = HeaderData.StageSequence;
                objPRL345.ICLRevNo = HeaderData.ICLRevNo;
                objPRL345.CreatedBy = objClsLoginInfo.UserName;
                objPRL345.CreatedOn = DateTime.Now;



                db.PRL345.Add(objPRL345);
                db.SaveChanges();


                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL070
                    PRO345 objPRO345 = db.PRO345.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO345 != null)
                    {
                        CopyProtocolData("PRO345", "PRL345", "PRL345.HeaderId=" + objPRO345.HeaderId + " AND PRO345.HeaderId=" + objPRO345.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO346", "PRL346", "HeaderId=" + objPRO345.HeaderId, "LineId,EditedBy,EditedOn", objPRO345.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);

                    }
                    #endregion
                }
                returnValue = objPRL345.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.HYDROSTATIC_TEST_REPORT_V2.GetStringValue())
            {
                #region Add New Entry in PRL350 For PROTOCOL071
                PRL350 objPRL350 = new PRL350();
                objPRL350.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL350.QualityProject = HeaderData.QualityProject;
                objPRL350.EquipmentNo = HeaderData.EquipmentNo;
                objPRL350.DrawingNo = HeaderData.DrawingNo;
                objPRL350.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL350.DCRNo = HeaderData.DCRNo;
                objPRL350.SeamNo = HeaderData.SeamNo;
                objPRL350.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL350.Project = HeaderData.Project;
                objPRL350.BU = HeaderData.BU;
                objPRL350.Location = HeaderData.Location;
                objPRL350.StageCode = HeaderData.StageCode;
                objPRL350.StageSequence = HeaderData.StageSequence;
                objPRL350.ICLRevNo = HeaderData.ICLRevNo;
                objPRL350.CreatedBy = objClsLoginInfo.UserName;
                objPRL350.CreatedOn = DateTime.Now;

                #region 
                objPRL350.QCRemarks = "NO PRESSURE DROP AND NO LEAKAGE OBSERVED.";
                #endregion

                #region  LOCATION 
                objPRL350.PRL352.Add(new PRL352
                {
                    Location = "TOP",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL350.PRL352.Add(new PRL352
                {
                    Location = "MIDDLE",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL350.PRL352.Add(new PRL352
                {
                    Location = "BOTTOM",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                db.PRL350.Add(objPRL350);
                db.SaveChanges();


                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL071
                    PRO350 objPRO350 = db.PRO350.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO350 != null)
                    {
                        CopyProtocolData("PRO350", "PRL350", "PRL350.HeaderId=" + objPRO350.HeaderId + " AND PRO350.HeaderId=" + objPRO350.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO351", "PRL351", "HeaderId=" + objPRO350.HeaderId, "LineId,EditedBy,EditedOn", objPRO350.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO352", "PRL352", "HeaderId=" + objPRO350.HeaderId, "LineId,EditedBy,EditedOn", objPRO350.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);

                    }
                    #endregion
                }
                returnValue = objPRL350.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.PNEUMATIC_TEST_REPORT_V2.GetStringValue())
            {
                #region Add New Entry in PRL355 For PROTOCOL072
                PRL355 objPRL355 = new PRL355();
                objPRL355.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL355.QualityProject = HeaderData.QualityProject;
                objPRL355.EquipmentNo = HeaderData.EquipmentNo;
                objPRL355.DrawingNo = HeaderData.DrawingNo;
                objPRL355.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL355.DCRNo = HeaderData.DCRNo;
                objPRL355.SeamNo = HeaderData.SeamNo;
                objPRL355.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL355.Project = HeaderData.Project;
                objPRL355.BU = HeaderData.BU;
                objPRL355.Location = HeaderData.Location;
                objPRL355.StageCode = HeaderData.StageCode;
                objPRL355.StageSequence = HeaderData.StageSequence;
                objPRL355.ICLRevNo = HeaderData.ICLRevNo;
                objPRL355.CreatedBy = objClsLoginInfo.UserName;
                objPRL355.CreatedOn = DateTime.Now;

                #region NOTES
                objPRL355.QCRemarks = "NO PRESSURE DROP AND NO LEAKAGE OBSERVED.";
                #endregion

                db.PRL355.Add(objPRL355);
                db.SaveChanges();


                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL072
                    PRO355 objPRO355 = db.PRO355.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO355 != null)
                    {
                        CopyProtocolData("PRO355", "PRL355", "PRL355.HeaderId=" + objPRO355.HeaderId + " AND PRO355.HeaderId=" + objPRO355.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO356", "PRL356", "HeaderId=" + objPRO355.HeaderId, "LineId,EditedBy,EditedOn", objPRO355.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                    }
                    #endregion
                }
                returnValue = objPRL355.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_CUSO4_TEST.GetStringValue())
            {
                #region Add New Entry in PRL360 For PROTOCOL073
                PRL360 objPRL360 = new PRL360();
                objPRL360.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL360.QualityProject = HeaderData.QualityProject;
                objPRL360.EquipmentNo = HeaderData.EquipmentNo;
                objPRL360.DrawingNo = HeaderData.DrawingNo;
                objPRL360.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL360.DCRNo = HeaderData.DCRNo;
                objPRL360.SeamNo = HeaderData.SeamNo;
                objPRL360.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL360.Project = HeaderData.Project;
                objPRL360.BU = HeaderData.BU;
                objPRL360.Location = HeaderData.Location;
                objPRL360.StageCode = HeaderData.StageCode;
                objPRL360.StageSequence = HeaderData.StageSequence;
                objPRL360.ICLRevNo = HeaderData.ICLRevNo;
                objPRL360.CreatedBy = objClsLoginInfo.UserName;
                objPRL360.CreatedOn = DateTime.Now;



                db.PRL360.Add(objPRL360);
                db.SaveChanges();


                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL073
                    PRO360 objPRO360 = db.PRO360.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO360 != null)
                    {
                        CopyProtocolData("PRO360", "PRL360", "PRL360.HeaderId=" + objPRO360.HeaderId + " AND PRO360.HeaderId=" + objPRO360.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO361", "PRL361", "HeaderId=" + objPRO360.HeaderId, "LineId,EditedBy,EditedOn", objPRO360.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);


                    }
                    #endregion
                }
                returnValue = objPRL360.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_N2_FILLING_INSPECTION_V2.GetStringValue())
            {
                #region Add New Entry in PRL365 For PROTOCOL074
                PRL365 objPRL365 = new PRL365();
                objPRL365.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL365.QualityProject = HeaderData.QualityProject;
                objPRL365.EquipmentNo = HeaderData.EquipmentNo;
                objPRL365.DrawingNo = HeaderData.DrawingNo;
                objPRL365.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL365.DCRNo = HeaderData.DCRNo;
                objPRL365.SeamNo = HeaderData.SeamNo;
                objPRL365.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL365.Project = HeaderData.Project;
                objPRL365.BU = HeaderData.BU;
                objPRL365.Location = HeaderData.Location;
                objPRL365.StageCode = HeaderData.StageCode;
                objPRL365.StageSequence = HeaderData.StageSequence;
                objPRL365.ICLRevNo = HeaderData.ICLRevNo;
                objPRL365.CreatedBy = objClsLoginInfo.UserName;
                objPRL365.CreatedOn = DateTime.Now;

                db.PRL365.Add(objPRL365);
                db.SaveChanges();

                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL074
                    PRO365 objPRO365 = db.PRO365.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO365 != null)
                    {
                        CopyProtocolData("PRO365", "PRL365", "PRL365.HeaderId=" + objPRO365.HeaderId + " AND PRO365.HeaderId=" + objPRO365.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO366", "PRL366", "HeaderId=" + objPRO365.HeaderId, "LineId,EditedBy,EditedOn", objPRO365.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO367", "PRL367", "HeaderId=" + objPRO365.HeaderId, "LineId,EditedBy,EditedOn", objPRO365.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);


                    }
                    #endregion
                }
                returnValue = objPRL365.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.SETUP_AND_DIMENSION_REPORT_FOR_FORMED_CONE.GetStringValue())
            {
                #region Add New Entry in PRL370 For PROTOCOL075
                PRL370 objPRL370 = new PRL370();
                objPRL370.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL370.QualityProject = HeaderData.QualityProject;
                objPRL370.EquipmentNo = HeaderData.EquipmentNo;
                objPRL370.DrawingNo = HeaderData.DrawingNo;
                objPRL370.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL370.DCRNo = HeaderData.DCRNo;
                objPRL370.SeamNo = HeaderData.SeamNo;
                objPRL370.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL370.Project = HeaderData.Project;
                objPRL370.BU = HeaderData.BU;
                objPRL370.Location = HeaderData.Location;
                objPRL370.StageCode = HeaderData.StageCode;
                objPRL370.StageSequence = HeaderData.StageSequence;
                objPRL370.ICLRevNo = HeaderData.ICLRevNo;
                objPRL370.CreatedBy = objClsLoginInfo.UserName;
                objPRL370.CreatedOn = DateTime.Now;




                #region TYPE
                objPRL370.RemarksInsideCirc2 = "NOTE : IN CASE OF CONE CIRCUMFERENCE TO BE CHECKED IN CLOCK WISE AND ANTI CLOCK WISE THAN AVERAGE SHALL BE REPORTED  \n ABBREVIATION  \n C.W: CLOCK WISE,  \n A.C.W: ANTI CLOCK WISE,  \n AVG: AVERAGE";
                #endregion

                #region OVALITY
                objPRL370.PRL371.Add(new PRL371
                {
                    Orientation = "0°-180° ",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL370.PRL371.Add(new PRL371
                {
                    Orientation = "22.5°-202.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL370.PRL371.Add(new PRL371
                {
                    Orientation = "45°-225°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL370.PRL371.Add(new PRL371
                {
                    Orientation = "67.5°-247.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL370.PRL371.Add(new PRL371
                {
                    Orientation = "90°-270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL370.PRL371.Add(new PRL371
                {
                    Orientation = "112.5°-292.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL370.PRL371.Add(new PRL371
                {
                    Orientation = "135°-315°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL370.PRL371.Add(new PRL371
                {
                    Orientation = "157.5°-337.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion


                #region OVERALL HEIGHT OF CONE
                objPRL370.PRL372.Add(new PRL372
                {
                    Orientation = "0°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL370.PRL372.Add(new PRL372
                {
                    Orientation = "90°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL370.PRL372.Add(new PRL372
                {
                    Orientation = "180°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL370.PRL372.Add(new PRL372
                {
                    Orientation = "270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                #region  THICKNESS  READINGS AT TRIMMING LINE
                objPRL370.PRL373.Add(new PRL373
                {
                    SrNo = 1,
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL370.PRL373.Add(new PRL373
                {
                    SrNo = 2,
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL370.PRL373.Add(new PRL373
                {
                    SrNo = 3,
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL370.PRL373.Add(new PRL373
                {
                    SrNo = 4,
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL370.PRL373.Add(new PRL373
                {
                    SrNo = 5,
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL370.PRL373.Add(new PRL373
                {
                    SrNo = 6,
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL370.PRL373.Add(new PRL373
                {
                    SrNo = 7,
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL370.PRL373.Add(new PRL373
                {
                    SrNo = 8,
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL370.PRL373.Add(new PRL373
                {
                    SrNo = 9,
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL370.PRL373.Add(new PRL373
                {
                    SrNo = 10,
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL370.PRL373.Add(new PRL373
                {
                    SrNo = 11,
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL370.PRL373.Add(new PRL373
                {
                    SrNo = 12,
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion


                #region DETAIL THICKNESS REPORT
                objPRL370.PRL374.Add(new PRL374
                {
                    Description = "PETAL 1",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL370.PRL374.Add(new PRL374
                {
                    Description = "PETAL 2",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL370.PRL374.Add(new PRL374
                {
                    Description = "PETAL 3",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL370.PRL374.Add(new PRL374
                {
                    Description = "PETAL 4",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region  " e " VALUE TEMPLATE READING AS APPLICABLE
                objPRL370.PRL374_2.Add(new PRL374_2
                {
                    ReqAtTop = "PETAL-1",
                    ReqAtMiddle = "PETAL-1",
                    ReqAtBottom = "PETAL-1",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL370.PRL374_2.Add(new PRL374_2
                {
                    ReqAtTop = "PETAL-2",
                    ReqAtMiddle = "PETAL-2",
                    ReqAtBottom = "PETAL-2",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL370.PRL374_2.Add(new PRL374_2
                {
                    ReqAtTop = "PETAL-3",
                    ReqAtMiddle = "PETAL-3",
                    ReqAtBottom = "PETAL-3",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL370.PRL374_2.Add(new PRL374_2
                {
                    ReqAtTop = "PETAL-4",
                    ReqAtMiddle = "PETAL-4",
                    ReqAtBottom = "PETAL-4",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL370.PRL374_2.Add(new PRL374_2
                {
                    ReqAtTop = "PETAL-5",
                    ReqAtMiddle = "PETAL-5",
                    ReqAtBottom = "PETAL-5",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL370.PRL374_2.Add(new PRL374_2
                {
                    ReqAtTop = "PETAL-6",
                    ReqAtMiddle = "PETAL-6",
                    ReqAtBottom = "PETAL-6",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL370.PRL374_2.Add(new PRL374_2
                {
                    ReqAtTop = "PETAL-7",
                    ReqAtMiddle = "PETAL-7",
                    ReqAtBottom = "PETAL-7",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL370.PRL374_2.Add(new PRL374_2
                {
                    ReqAtTop = "PETAL-8",
                    ReqAtMiddle = "PETAL-8",
                    ReqAtBottom = "PETAL-8",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });


                #endregion

                #region  GAP BY USING 2D PROFILE TEMPLATE
                objPRL370.PRL374_7.Add(new PRL374_7
                {
                    PetalNo = "1",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL370.PRL374_7.Add(new PRL374_7
                {
                    PetalNo = "2",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL370.PRL374_7.Add(new PRL374_7
                {
                    PetalNo = "3",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL370.PRL374_7.Add(new PRL374_7
                {
                    PetalNo = "4",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL370.PRL374_7.Add(new PRL374_7
                {
                    PetalNo = "5",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL370.PRL374_7.Add(new PRL374_7
                {
                    PetalNo = "6",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL370.PRL374_7.Add(new PRL374_7
                {
                    PetalNo = "7",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL370.PRL374_7.Add(new PRL374_7
                {
                    PetalNo = "8",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                #region ACTUAL GAP BY USING KNUCKLE RADIUS TEMPLATE 

                objPRL370.PRL374_8.Add(new PRL374_8
                {
                    PetalNo = "1",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL370.PRL374_8.Add(new PRL374_8
                {
                    PetalNo = "2",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL370.PRL374_8.Add(new PRL374_8
                {
                    PetalNo = "3",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL370.PRL374_8.Add(new PRL374_8
                {
                    PetalNo = "4",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL370.PRL374_8.Add(new PRL374_8
                {
                    PetalNo = "5",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL370.PRL374_8.Add(new PRL374_8
                {
                    PetalNo = "6",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL370.PRL374_8.Add(new PRL374_8
                {
                    PetalNo = "7",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL370.PRL374_8.Add(new PRL374_8
                {
                    PetalNo = "8",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion



                db.PRL370.Add(objPRL370);
                db.SaveChanges();

                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL075
                    PRO370 objPRO370 = db.PRO370.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO370 != null)
                    {
                        CopyProtocolData("PRO370", "PRL370", "PRL370.HeaderId=" + objPRO370.HeaderId + " AND PRO370.HeaderId=" + objPRO370.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO371", "PRL371", "HeaderId=" + objPRO370.HeaderId, "LineId,EditedBy,EditedOn", objPRO370.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO372", "PRL372", "HeaderId=" + objPRO370.HeaderId, "LineId,EditedBy,EditedOn", objPRO370.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO373", "PRL373", "HeaderId=" + objPRO370.HeaderId, "LineId,EditedBy,EditedOn", objPRO370.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO374", "PRL374", "HeaderId=" + objPRO370.HeaderId, "LineId,EditedBy,EditedOn", objPRO370.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO374_2", "PRL374_2", "HeaderId=" + objPRO370.HeaderId, "LineId,EditedBy,EditedOn", objPRO370.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO374_3", "PRL374_3", "HeaderId=" + objPRO370.HeaderId, "LineId,EditedBy,EditedOn", objPRO370.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO374_4", "PRL374_4", "HeaderId=" + objPRO370.HeaderId, "LineId,EditedBy,EditedOn", objPRO370.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO374_5", "PRL374_5", "HeaderId=" + objPRO370.HeaderId, "LineId,EditedBy,EditedOn", objPRO370.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO374_6", "PRL374_6", "HeaderId=" + objPRO370.HeaderId, "LineId,EditedBy,EditedOn", objPRO370.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO374_7", "PRL374_7", "HeaderId=" + objPRO370.HeaderId, "LineId,EditedBy,EditedOn", objPRO370.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO374_8", "PRL374_8", "HeaderId=" + objPRO370.HeaderId, "LineId,EditedBy,EditedOn", objPRO370.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO374_9", "PRL374_9", "HeaderId=" + objPRO370.HeaderId, "LineId,EditedBy,EditedOn", objPRO370.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO374_10", "PRL374_10", "HeaderId=" + objPRO370.HeaderId, "LineId,EditedBy,EditedOn", objPRO370.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO374_11", "PRL374_11", "HeaderId=" + objPRO370.HeaderId, "LineId,EditedBy,EditedOn", objPRO370.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                    }
                    #endregion
                }
                returnValue = objPRL370.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_REPORT_FOR_FORMED_CONE.GetStringValue())
            {
                #region Add New Entry in PRL375 For PROTOCOL076
                PRL375 objPRL375 = new PRL375();
                objPRL375.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL375.QualityProject = HeaderData.QualityProject;
                objPRL375.EquipmentNo = HeaderData.EquipmentNo;
                objPRL375.DrawingNo = HeaderData.DrawingNo;
                objPRL375.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL375.DCRNo = HeaderData.DCRNo;
                objPRL375.SeamNo = HeaderData.SeamNo;
                objPRL375.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL375.Project = HeaderData.Project;
                objPRL375.BU = HeaderData.BU;
                objPRL375.Location = HeaderData.Location;
                objPRL375.StageCode = HeaderData.StageCode;
                objPRL375.StageSequence = HeaderData.StageSequence;
                objPRL375.ICLRevNo = HeaderData.ICLRevNo;
                objPRL375.CreatedBy = objClsLoginInfo.UserName;
                objPRL375.CreatedOn = DateTime.Now;


                #region TYPE
                objPRL375.RemarksInsideCirc2 = "NOTE : IN CASE OF CONE CIRCUMFERENCE TO BE CHECKED IN CLOCK WISE AND ANTI CLOCK WISE THAN AVERAGE SHALL BE REPORTED  \n ABBREVIATION  \n C.W: CLOCK WISE,  \n A.C.W: ANTI CLOCK WISE,  \n AVG: AVERAGE";
                #endregion

                #region OVALITY
                objPRL375.PRL376.Add(new PRL376
                {
                    Orientation = "0°-180° ",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL375.PRL376.Add(new PRL376
                {
                    Orientation = "22.5°-202.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL375.PRL376.Add(new PRL376
                {
                    Orientation = "45°-225°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL375.PRL376.Add(new PRL376
                {
                    Orientation = "67.5°-247.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL375.PRL376.Add(new PRL376
                {
                    Orientation = "90°-270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL375.PRL376.Add(new PRL376
                {
                    Orientation = "112.5°-292.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL375.PRL376.Add(new PRL376
                {
                    Orientation = "135°-315°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL375.PRL376.Add(new PRL376
                {
                    Orientation = "157.5°-337.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion


                #region  OVERALL HEIGHT OF CONE
                objPRL375.PRL377.Add(new PRL377
                {
                    Orientation = "0°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL375.PRL377.Add(new PRL377
                {
                    Orientation = "90°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL375.PRL377.Add(new PRL377
                {
                    Orientation = "180°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL375.PRL377.Add(new PRL377
                {
                    Orientation = "270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region  " e " VALUE TEMPLATE READING AS APPLICABLE
                objPRL375.PRL378.Add(new PRL378
                {
                    ReqAtTop = "PETAL-1",
                    ReqAtMiddle = "PETAL-1",
                    ReqAtBottom = "PETAL-1",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL375.PRL378.Add(new PRL378
                {
                    ReqAtTop = "SEAM",
                    ReqAtMiddle = "SEAM",
                    ReqAtBottom = "SEAM",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL375.PRL378.Add(new PRL378
                {
                    ReqAtTop = "PETAL: 2",
                    ReqAtMiddle = "PETAL: 2",
                    ReqAtBottom = "PETAL: 2",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL375.PRL378.Add(new PRL378
                {
                    ReqAtTop = "SEAM",
                    ReqAtMiddle = "SEAM",
                    ReqAtBottom = "SEAM",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL375.PRL378.Add(new PRL378
                {
                    ReqAtTop = "PETAL: 3",
                    ReqAtMiddle = "PETAL: 3",
                    ReqAtBottom = "PETAL: 3",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL375.PRL378.Add(new PRL378
                {
                    ReqAtTop = "SEAM",
                    ReqAtMiddle = "SEAM",
                    ReqAtBottom = "SEAM",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL375.PRL378.Add(new PRL378
                {
                    ReqAtTop = "PETAL: 4",
                    ReqAtMiddle = "PETAL: 4",
                    ReqAtBottom = "PETAL: 4",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL375.PRL378.Add(new PRL378
                {
                    ReqAtTop = "SEAM",
                    ReqAtMiddle = "SEAM",
                    ReqAtBottom = "SEAM",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL375.PRL378.Add(new PRL378
                {
                    ReqAtTop = "PETAL: 5",
                    ReqAtMiddle = "PETAL: 5",
                    ReqAtBottom = "PETAL: 5",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL375.PRL378.Add(new PRL378
                {
                    ReqAtTop = "SEAM",
                    ReqAtMiddle = "SEAM",
                    ReqAtBottom = "SEAM",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL375.PRL378.Add(new PRL378
                {
                    ReqAtTop = "PETAL: 6",
                    ReqAtMiddle = "PETAL: 6",
                    ReqAtBottom = "PETAL: 6",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL375.PRL378.Add(new PRL378
                {
                    ReqAtTop = "SEAM",
                    ReqAtMiddle = "SEAM",
                    ReqAtBottom = "SEAM",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL375.PRL378.Add(new PRL378
                {
                    ReqAtTop = "PETAL: 7",
                    ReqAtMiddle = "PETAL: 7",
                    ReqAtBottom = "PETAL: 7",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL375.PRL378.Add(new PRL378
                {
                    ReqAtTop = "SEAM",
                    ReqAtMiddle = "SEAM",
                    ReqAtBottom = "SEAM",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL375.PRL378.Add(new PRL378
                {
                    ReqAtTop = "PETAL: 8",
                    ReqAtMiddle = "PETAL: 8",
                    ReqAtBottom = "PETAL: 8",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL375.PRL378.Add(new PRL378
                {
                    ReqAtTop = "SEAM",
                    ReqAtMiddle = "SEAM",
                    ReqAtBottom = "SEAM",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region ACTUAL GAP BY USING KNUCKLE RADIUS TEMPLATE 
                objPRL375.PRL379_3.Add(new PRL379_3
                {
                    PetalNo = "1",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL375.PRL379_3.Add(new PRL379_3
                {
                    PetalNo = "2",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL375.PRL379_3.Add(new PRL379_3
                {
                    PetalNo = "3",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL375.PRL379_3.Add(new PRL379_3
                {
                    PetalNo = "4",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL375.PRL379_3.Add(new PRL379_3
                {
                    PetalNo = "5",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL375.PRL379_3.Add(new PRL379_3
                {
                    PetalNo = "6",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL375.PRL379_3.Add(new PRL379_3
                {
                    PetalNo = "7",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL375.PRL379_3.Add(new PRL379_3
                {
                    PetalNo = "8",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion


                db.PRL375.Add(objPRL375);
                db.SaveChanges();


                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL076
                    PRO375 objPRO375 = db.PRO375.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO375 != null)
                    {
                        CopyProtocolData("PRO375", "PRL375", "PRL375.HeaderId=" + objPRO375.HeaderId + " AND PRO375.HeaderId=" + objPRO375.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO376", "PRL376", "HeaderId=" + objPRO375.HeaderId, "LineId,EditedBy,EditedOn", objPRO375.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO377", "PRL377", "HeaderId=" + objPRO375.HeaderId, "LineId,EditedBy,EditedOn", objPRO375.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO378", "PRL378", "HeaderId=" + objPRO375.HeaderId, "LineId,EditedBy,EditedOn", objPRO375.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO379", "PRL379", "HeaderId=" + objPRO375.HeaderId, "LineId,EditedBy,EditedOn", objPRO375.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO379_2", "PRL379_2", "HeaderId=" + objPRO375.HeaderId, "LineId,EditedBy,EditedOn", objPRO375.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO379_3", "PRL379_3", "HeaderId=" + objPRO375.HeaderId, "LineId,EditedBy,EditedOn", objPRO375.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO379_4", "PRL379_4", "HeaderId=" + objPRO375.HeaderId, "LineId,EditedBy,EditedOn", objPRO375.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);


                    }
                    #endregion
                }
                returnValue = objPRL375.HeaderId;
            }

            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_VISUAL_AND_DIMENSION_INSPECTION_FOR_SQUARE_TYPE_NUB_BEFORE_OVERLAY.GetStringValue())
            {
                #region Add New Entry in PRL380 For PROTOCOL077
                PRL380 objPRL380 = new PRL380();
                objPRL380.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL380.QualityProject = HeaderData.QualityProject;
                objPRL380.EquipmentNo = HeaderData.EquipmentNo;
                objPRL380.DrawingNo = HeaderData.DrawingNo;
                objPRL380.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL380.DCRNo = HeaderData.DCRNo;
                objPRL380.SeamNo = HeaderData.SeamNo;
                objPRL380.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL380.Project = HeaderData.Project;
                objPRL380.BU = HeaderData.BU;
                objPRL380.Location = HeaderData.Location;
                objPRL380.StageCode = HeaderData.StageCode;
                objPRL380.StageSequence = HeaderData.StageSequence;
                objPRL380.ICLRevNo = HeaderData.ICLRevNo;
                objPRL380.CreatedBy = objClsLoginInfo.UserName;
                objPRL380.CreatedOn = DateTime.Now;



                #region  Orientation
                objPRL380.PRL381.Add(new PRL381
                {

                    Orientation = "0º",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL380.PRL381.Add(new PRL381
                {

                    Orientation = "45º",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL380.PRL381.Add(new PRL381
                {

                    Orientation = "90º",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL380.PRL381.Add(new PRL381
                {

                    Orientation = "135º",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL380.PRL381.Add(new PRL381
                {

                    Orientation = "180º",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL380.PRL381.Add(new PRL381
                {

                    Orientation = "225º",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL380.PRL381.Add(new PRL381
                {

                    Orientation = "270º",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL380.PRL381.Add(new PRL381
                {

                    Orientation = "315º",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });


                #endregion


                #region  OrientationReqNubID
                objPRL380.PRL382.Add(new PRL382
                {

                    OrientationReqNubID = "0°-180°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL380.PRL382.Add(new PRL382
                {

                    OrientationReqNubID = "22.5°-202.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL380.PRL382.Add(new PRL382
                {

                    OrientationReqNubID = "45°-225°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL380.PRL382.Add(new PRL382
                {

                    OrientationReqNubID = "67.5°-247.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL380.PRL382.Add(new PRL382
                {

                    OrientationReqNubID = "90°-270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL380.PRL382.Add(new PRL382
                {

                    OrientationReqNubID = "112.5°-292.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL380.PRL382.Add(new PRL382
                {

                    OrientationReqNubID = "135°-315°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL380.PRL382.Add(new PRL382
                {

                    OrientationReqNubID = "157.5°-337.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });


                #endregion

                #region  OrientationAllowedShellOutOfRoundness 
                objPRL380.PRL383.Add(new PRL383
                {

                    OrientationAllowedShellOutOfRoundness = "0°-180°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL380.PRL383.Add(new PRL383
                {

                    OrientationAllowedShellOutOfRoundness = "22.5°-202.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL380.PRL383.Add(new PRL383
                {

                    OrientationAllowedShellOutOfRoundness = "45°-225°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL380.PRL383.Add(new PRL383
                {

                    OrientationAllowedShellOutOfRoundness = "67.5°-247.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL380.PRL383.Add(new PRL383
                {

                    OrientationAllowedShellOutOfRoundness = "90°-270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL380.PRL383.Add(new PRL383
                {

                    OrientationAllowedShellOutOfRoundness = "112.5°-292.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL380.PRL383.Add(new PRL383
                {

                    OrientationAllowedShellOutOfRoundness = "135°-315°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL380.PRL383.Add(new PRL383
                {

                    OrientationAllowedShellOutOfRoundness = "157.5°-337.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });


                #endregion

                #region  OtherDimensions 
                objPRL380.PRL384.Add(new PRL384
                {

                    OtherDimensions = "TOP SIDE CORNER RADIUS",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL380.PRL384.Add(new PRL384
                {

                    OtherDimensions = "BOTTOM SIDE CORNER RADIUS",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL380.PRL384.Add(new PRL384
                {

                    OtherDimensions = "SURFACE FINISH ON MACHINED SURFACES",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                db.PRL380.Add(objPRL380);
                db.SaveChanges();


                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL077
                    PRO380 objPRO380 = db.PRO380.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO380 != null)
                    {
                        CopyProtocolData("PRO380", "PRL380", "PRL380.HeaderId=" + objPRO380.HeaderId + " AND PRO380.HeaderId=" + objPRO380.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO381", "PRL381", "HeaderId=" + objPRO380.HeaderId, "LineId,EditedBy,EditedOn", objPRO380.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO382", "PRL382", "HeaderId=" + objPRO380.HeaderId, "LineId,EditedBy,EditedOn", objPRO380.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO383", "PRL383", "HeaderId=" + objPRO380.HeaderId, "LineId,EditedBy,EditedOn", objPRO380.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO384", "PRL384", "HeaderId=" + objPRO380.HeaderId, "LineId,EditedBy,EditedOn", objPRO380.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);

                    }
                    #endregion
                }
                returnValue = objPRL380.HeaderId;
            }

            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_VISUAL_AND_DIMENSION_INSPECTION_FOR_SQUARE_TYPE_NUB_AFTER_OVERLAY.GetStringValue())
            {
                #region Add New Entry in PRL385 For PROTOCOL078
                PRL385 objPRL385 = new PRL385();
                objPRL385.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL385.QualityProject = HeaderData.QualityProject;
                objPRL385.EquipmentNo = HeaderData.EquipmentNo;
                objPRL385.DrawingNo = HeaderData.DrawingNo;
                objPRL385.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL385.DCRNo = HeaderData.DCRNo;
                objPRL385.SeamNo = HeaderData.SeamNo;
                objPRL385.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL385.Project = HeaderData.Project;
                objPRL385.BU = HeaderData.BU;
                objPRL385.Location = HeaderData.Location;
                objPRL385.StageCode = HeaderData.StageCode;
                objPRL385.StageSequence = HeaderData.StageSequence;
                objPRL385.ICLRevNo = HeaderData.ICLRevNo;
                objPRL385.CreatedBy = objClsLoginInfo.UserName;
                objPRL385.CreatedOn = DateTime.Now;


                #region  OrientationReqNubID 
                objPRL385.PRL387.Add(new PRL387
                {

                    OrientationReqNubID = "0°-180°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL385.PRL387.Add(new PRL387
                {

                    OrientationReqNubID = "22.5°-202.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL385.PRL387.Add(new PRL387
                {

                    OrientationReqNubID = "45°-225°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL385.PRL387.Add(new PRL387
                {

                    OrientationReqNubID = "67.5°-247.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL385.PRL387.Add(new PRL387
                {

                    OrientationReqNubID = "90°-270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL385.PRL387.Add(new PRL387
                {

                    OrientationReqNubID = "112.5°-292.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL385.PRL387.Add(new PRL387
                {

                    OrientationReqNubID = "135°-315°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL385.PRL387.Add(new PRL387
                {

                    OrientationReqNubID = "157.5°-337.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });


                #endregion

                #region  OrientationAllowedShellOutOfRoundness
                objPRL385.PRL388.Add(new PRL388
                {

                    OrientationAllowedShellOutOfRoundness = "0°-180°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL385.PRL388.Add(new PRL388
                {

                    OrientationAllowedShellOutOfRoundness = "22.5°-202.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL385.PRL388.Add(new PRL388
                {

                    OrientationAllowedShellOutOfRoundness = "45°-225°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL385.PRL388.Add(new PRL388
                {

                    OrientationAllowedShellOutOfRoundness = "67.5°-247.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL385.PRL388.Add(new PRL388
                {

                    OrientationAllowedShellOutOfRoundness = "90°-270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL385.PRL388.Add(new PRL388
                {

                    OrientationAllowedShellOutOfRoundness = "112.5°-292.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL385.PRL388.Add(new PRL388
                {

                    OrientationAllowedShellOutOfRoundness = "135°-315°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL385.PRL388.Add(new PRL388
                {

                    OrientationAllowedShellOutOfRoundness = "157.5°-337.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });


                #endregion

                #region  OtherDimensions 
                objPRL385.PRL389.Add(new PRL389
                {

                    OtherDimensions = "DEPTH OF BARRIER LAYER FROM OVERLAY ON NUB FACE (TOP SIDE )",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL385.PRL389.Add(new PRL389
                {

                    OtherDimensions = "DEPTH OF BARRIER LAYER FROM OVERLAY ON NUB FACE  (BOTTOM SIDE )",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL385.PRL389.Add(new PRL389
                {

                    OtherDimensions = "DEPTH OF BARRIER LAYER FROM OVERLAY ON SHELL I.D. (TOP SIDE )",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL385.PRL389.Add(new PRL389
                {

                    OtherDimensions = "DEPTH OF BARRIER LAYER FROM OVERLAY ON SHELL I.D. (BOTTOM SIDE )",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL385.PRL389.Add(new PRL389
                {

                    OtherDimensions = "RADIUS ON BARRIER LAYER (TOP SIDE)",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL385.PRL389.Add(new PRL389
                {

                    OtherDimensions = "RADIUS ON BARRIER LAYER (BOTTOM SIDE)",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL385.PRL389.Add(new PRL389
                {

                    OtherDimensions = "SURFACE FINISH ON MACHINED SURFACES",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });


                #endregion

                db.PRL385.Add(objPRL385);
                db.SaveChanges();

                #region Default data
                //if (objPRL385 != null)
                //{
                //PRL380 _obj380 = db.PRL380.Where(w => w.SeamNo == objPRL385.SeamNo).FirstOrDefault();
                //if (_obj380 != null)
                //{
                //    List<PRL381> _lstPRL381 = db.PRL381.Where(w => w.HeaderId == _obj380.HeaderId).ToList();
                //    if (_lstPRL381.Count() > 0)
                //    {
                //        for (int i = 0; i < _lstPRL381.Count; i++)
                //        {
                //            PRL386 objPRL386Copy = new PRL386();
                //            objPRL386Copy.HeaderId = objPRL385.HeaderId;
                //            objPRL386Copy.Orientation = _lstPRL381[i].Orientation;
                //            objPRL386Copy.ActTolReqElevationOfNubFrom = "";
                //            objPRL386Copy.ActTolReqElevationOfTopSide = "";
                //            objPRL386Copy.ActTolReqNubWidth = "";
                //            objPRL386Copy.ActTolReqNubHeight = "";
                //            objPRL386Copy.A1DimensionsBefore = _lstPRL381[i].A1DimensionsChecked;
                //            objPRL386Copy.A2DimensionsBefore = _lstPRL381[i].A2DimensionsChecked;
                //            objPRL386Copy.B1DimensionsBefore = _lstPRL381[i].B1DimensionsChecked;
                //            objPRL386Copy.B2DimensionsBefore = _lstPRL381[i].B2DimensionsChecked;
                //            objPRL386Copy.C1DimensionsBefore = _lstPRL381[i].C1DimensionsChecked;
                //            objPRL386Copy.C2DimensionsBefore = _lstPRL381[i].C2DimensionsChecked;
                //            objPRL386Copy.AA1DimensionsAfter = "";
                //            objPRL386Copy.AA2DimensionsAfter = "";
                //            objPRL386Copy.BB1DimensionsAfter = "";
                //            objPRL386Copy.BB2DimensionsAfter = "";
                //            objPRL386Copy.CC1DimensionsAfter = "";
                //            objPRL386Copy.CC2DimensionsAfter = "";
                //            objPRL386Copy.A1NubTopSide = "";
                //            objPRL386Copy.A2NubTopSide = "";
                //            objPRL386Copy.B1NubID = "";
                //            objPRL386Copy.B2NubID = "";
                //            objPRL386Copy.C1NubBottomSide = "";
                //            objPRL386Copy.C2NubBottomSide = "";
                //            objPRL386Copy.CreatedBy = objClsLoginInfo.UserName;
                //            objPRL386Copy.CreatedOn = DateTime.Now;
                //            db.PRL386.Add(objPRL386Copy);
                //        }
                //        db.SaveChanges();
                //    }
                //}
                //}
                #endregion

                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL078
                    PRO385 objPRO385 = db.PRO385.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO385 != null)
                    {
                        CopyProtocolData("PRO385", "PRL385", "PRL385.HeaderId=" + objPRO385.HeaderId + " AND PRO385.HeaderId=" + objPRO385.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO386", "PRL386", "HeaderId=" + objPRO385.HeaderId, "LineId,EditedBy,EditedOn", objPRO385.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO387", "PRL387", "HeaderId=" + objPRO385.HeaderId, "LineId,EditedBy,EditedOn", objPRO385.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO388", "PRL388", "HeaderId=" + objPRO385.HeaderId, "LineId,EditedBy,EditedOn", objPRO385.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO389", "PRL389", "HeaderId=" + objPRO385.HeaderId, "LineId,EditedBy,EditedOn", objPRO385.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);

                    }
                    #endregion
                }
                returnValue = objPRL385.HeaderId;
            }

            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_VISUAL_AND_DIMENSION_INSPECTION_FOR_TAPER_TYPE_NUB_BEFORE_OVERLAY.GetStringValue())
            {
                #region Add New Entry in PRL390 For PROTOCOL079
                PRL390 objPRL390 = new PRL390();
                objPRL390.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL390.QualityProject = HeaderData.QualityProject;
                objPRL390.EquipmentNo = HeaderData.EquipmentNo;
                objPRL390.DrawingNo = HeaderData.DrawingNo;
                objPRL390.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL390.DCRNo = HeaderData.DCRNo;
                objPRL390.SeamNo = HeaderData.SeamNo;
                objPRL390.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL390.Project = HeaderData.Project;
                objPRL390.BU = HeaderData.BU;
                objPRL390.Location = HeaderData.Location;
                objPRL390.StageCode = HeaderData.StageCode;
                objPRL390.StageSequence = HeaderData.StageSequence;
                objPRL390.ICLRevNo = HeaderData.ICLRevNo;
                objPRL390.CreatedBy = objClsLoginInfo.UserName;
                objPRL390.CreatedOn = DateTime.Now;



                #region  Orientation 

                objPRL390.PRL391.Add(new PRL391
                {

                    Orientation = "0º",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL390.PRL391.Add(new PRL391
                {

                    Orientation = "45º",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL390.PRL391.Add(new PRL391
                {

                    Orientation = "90º",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL390.PRL391.Add(new PRL391
                {

                    Orientation = "135º",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL390.PRL391.Add(new PRL391
                {

                    Orientation = "180º",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL390.PRL391.Add(new PRL391
                {

                    Orientation = "225º",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL390.PRL391.Add(new PRL391
                {

                    Orientation = "270º",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL390.PRL391.Add(new PRL391
                {

                    Orientation = "315º",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });


                #endregion


                #region  OrientationReqNubID 
                objPRL390.PRL392.Add(new PRL392
                {

                    OrientationReqNubID = "0°-180°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL390.PRL392.Add(new PRL392
                {

                    OrientationReqNubID = "22.5°-202.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL390.PRL392.Add(new PRL392
                {

                    OrientationReqNubID = "45°-225°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL390.PRL392.Add(new PRL392
                {

                    OrientationReqNubID = "67.5°-247.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL390.PRL392.Add(new PRL392
                {

                    OrientationReqNubID = "90°-270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL390.PRL392.Add(new PRL392
                {

                    OrientationReqNubID = "112.5°-292.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL390.PRL392.Add(new PRL392
                {

                    OrientationReqNubID = "135°-315°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL390.PRL392.Add(new PRL392
                {

                    OrientationReqNubID = "157.5°-337.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });


                #endregion

                #region  OrientationAllowedShellOutOfRoundness 
                objPRL390.PRL393.Add(new PRL393
                {

                    OrientationAllowedShellOutOfRoundness = "0°-180°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL390.PRL393.Add(new PRL393
                {

                    OrientationAllowedShellOutOfRoundness = "22.5°-202.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL390.PRL393.Add(new PRL393
                {

                    OrientationAllowedShellOutOfRoundness = "45°-225°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL390.PRL393.Add(new PRL393
                {

                    OrientationAllowedShellOutOfRoundness = "67.5°-247.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL390.PRL393.Add(new PRL393
                {

                    OrientationAllowedShellOutOfRoundness = "90°-270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL390.PRL393.Add(new PRL393
                {

                    OrientationAllowedShellOutOfRoundness = "112.5°-292.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL390.PRL393.Add(new PRL393
                {

                    OrientationAllowedShellOutOfRoundness = "135°-315°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL390.PRL393.Add(new PRL393
                {

                    OrientationAllowedShellOutOfRoundness = "157.5°-337.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });

                #endregion


                #region  OtherDimensions 
                objPRL390.PRL394.Add(new PRL394
                {

                    OtherDimensions = "TOP SIDE CORNER RADIUS",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL390.PRL394.Add(new PRL394
                {

                    OtherDimensions = "BOTTOM SIDE CORNER RADIUS",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL390.PRL394.Add(new PRL394
                {

                    OtherDimensions = "SURFACE FINISH ON MACHINED SURFACES",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL390.PRL394.Add(new PRL394
                {

                    OtherDimensions = "TAPER ANGLE",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                db.PRL390.Add(objPRL390);
                db.SaveChanges();


                #endregion


                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL079
                    PRO390 objPRO390 = db.PRO390.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO390 != null)
                    {
                        CopyProtocolData("PRO390", "PRL390", "PRL390.HeaderId=" + objPRO390.HeaderId + " AND PRO390.HeaderId=" + objPRO390.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO391", "PRL391", "HeaderId=" + objPRO390.HeaderId, "LineId,EditedBy,EditedOn", objPRO390.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO392", "PRL392", "HeaderId=" + objPRO390.HeaderId, "LineId,EditedBy,EditedOn", objPRO390.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO393", "PRL393", "HeaderId=" + objPRO390.HeaderId, "LineId,EditedBy,EditedOn", objPRO390.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO394", "PRL394", "HeaderId=" + objPRO390.HeaderId, "LineId,EditedBy,EditedOn", objPRO390.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);

                    }
                    #endregion
                }
                returnValue = objPRL390.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_VISUAL_AND_DIMENSION_INSPECTION_FOR_TAPER_TYPE_NUB_AFTER_OVERLAY.GetStringValue())
            {
                #region Add New Entry in PRL395 For PROTOCOL080
                PRL395 objPRL395 = new PRL395();
                objPRL395.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL395.QualityProject = HeaderData.QualityProject;
                objPRL395.EquipmentNo = HeaderData.EquipmentNo;
                objPRL395.DrawingNo = HeaderData.DrawingNo;
                objPRL395.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL395.DCRNo = HeaderData.DCRNo;
                objPRL395.SeamNo = HeaderData.SeamNo;
                objPRL395.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL395.Project = HeaderData.Project;
                objPRL395.BU = HeaderData.BU;
                objPRL395.Location = HeaderData.Location;
                objPRL395.StageCode = HeaderData.StageCode;
                objPRL395.StageSequence = HeaderData.StageSequence;
                objPRL395.ICLRevNo = HeaderData.ICLRevNo;
                objPRL395.CreatedBy = objClsLoginInfo.UserName;
                objPRL395.CreatedOn = DateTime.Now;


                #region  OrientationReqNubID 
                objPRL395.PRL397.Add(new PRL397
                {

                    OrientationReqNubID = "0°-180°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL395.PRL397.Add(new PRL397
                {

                    OrientationReqNubID = "22.5°-202.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL395.PRL397.Add(new PRL397
                {

                    OrientationReqNubID = "45°-225°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL395.PRL397.Add(new PRL397
                {

                    OrientationReqNubID = "67.5°-247.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL395.PRL397.Add(new PRL397
                {

                    OrientationReqNubID = "90°-270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL395.PRL397.Add(new PRL397
                {

                    OrientationReqNubID = "112.5°-292.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL395.PRL397.Add(new PRL397
                {

                    OrientationReqNubID = "135°-315°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL395.PRL397.Add(new PRL397
                {

                    OrientationReqNubID = "157.5°-337.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });


                #endregion

                #region  OrientationAllowedShellOutOfRoundness 
                objPRL395.PRL398.Add(new PRL398
                {

                    OrientationAllowedShellOutOfRoundness = "0°-180°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL395.PRL398.Add(new PRL398
                {

                    OrientationAllowedShellOutOfRoundness = "22.5°-202.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL395.PRL398.Add(new PRL398
                {

                    OrientationAllowedShellOutOfRoundness = "45°-225°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL395.PRL398.Add(new PRL398
                {

                    OrientationAllowedShellOutOfRoundness = "67.5°-247.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL395.PRL398.Add(new PRL398
                {

                    OrientationAllowedShellOutOfRoundness = "90°-270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL395.PRL398.Add(new PRL398
                {

                    OrientationAllowedShellOutOfRoundness = "112.5°-292.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL395.PRL398.Add(new PRL398
                {

                    OrientationAllowedShellOutOfRoundness = "135°-315°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL395.PRL398.Add(new PRL398
                {

                    OrientationAllowedShellOutOfRoundness = "157.5°-337.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });


                #endregion

                #region  OtherDimensions | PRL395
                objPRL395.PRL399.Add(new PRL399
                {

                    OtherDimensions = "DEPTH OF BARRIER LAYER FROM OVERLAY ON NUB FACE (TOP SIDE )",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL395.PRL399.Add(new PRL399
                {

                    OtherDimensions = "DEPTH OF BARRIER LAYER FROM OVERLAY ON NUB FACE  (BOTTOM SIDE )",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL395.PRL399.Add(new PRL399
                {

                    OtherDimensions = "DEPTH OF BARRIER LAYER FROM OVERLAY ON SHELL I.D. (TOP SIDE )",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL395.PRL399.Add(new PRL399
                {

                    OtherDimensions = "DEPTH OF BARRIER LAYER FROM OVERLAY ON SHELL I.D. (BOTTOM SIDE )",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                objPRL395.PRL399.Add(new PRL399
                {

                    OtherDimensions = "RADIUS ON BARRIER LAYER (TOP SIDE)",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                objPRL395.PRL399.Add(new PRL399
                {

                    OtherDimensions = "RADIUS ON BARRIER LAYER (BOTTOM SIDE)",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                objPRL395.PRL399.Add(new PRL399
                {

                    OtherDimensions = "TAPER ANGLE",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                objPRL395.PRL399.Add(new PRL399
                {

                    OtherDimensions = "SURFACE FINISH ON MACHINED SURFACES",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });



                #endregion

                db.PRL395.Add(objPRL395);
                db.SaveChanges();

                #region Default Data
                //if (objPRL395 != null)
                //{
                //    PRL390 _obj390 = db.PRL390.Where(w => w.SeamNo == objPRL395.SeamNo).FirstOrDefault();
                //    if (_obj390 != null)
                //    {
                //        List<PRL391> _lstPRL391 = db.PRL391.Where(w => w.HeaderId == _obj390.HeaderId).ToList();
                //        if (_lstPRL391.Count() > 0)
                //        {
                //            for (int i = 0; i < _lstPRL391.Count; i++)
                //            {
                //                PRL396 objPRL396Copy = new PRL396();
                //                objPRL396Copy.HeaderId = objPRL395.HeaderId;
                //                objPRL396Copy.Orientation = _lstPRL391[i].Orientation;
                //                objPRL396Copy.ActTolReqElevationOfNubFrom = "";
                //                objPRL396Copy.ActTolReqElevationOfTopSide = "";
                //                objPRL396Copy.ActTolReqNubWidth = "";
                //                objPRL396Copy.ActTolReqNubHeight = "";
                //                objPRL396Copy.A1DimensionsBefore = _lstPRL391[i].A1DimensionsChecked;
                //                objPRL396Copy.A2DimensionsBefore = _lstPRL391[i].A2DimensionsChecked;
                //                objPRL396Copy.B1DimensionsBefore = _lstPRL391[i].B1DimensionsChecked;
                //                objPRL396Copy.B2DimensionsBefore = _lstPRL391[i].B2DimensionsChecked;
                //                objPRL396Copy.C1DimensionsBefore = _lstPRL391[i].C1DimensionsChecked;
                //                objPRL396Copy.C2DimensionsBefore = _lstPRL391[i].C2DimensionsChecked;
                //                objPRL396Copy.AA1DimensionsAfter = "";
                //                objPRL396Copy.AA2DimensionsAfter = "";
                //                objPRL396Copy.BB1DimensionsAfter = "";
                //                objPRL396Copy.BB2DimensionsAfter = "";
                //                objPRL396Copy.CC1DimensionsAfter = "";
                //                objPRL396Copy.CC2DimensionsAfter = "";
                //                objPRL396Copy.A1NubTopSide = "";
                //                objPRL396Copy.A2NubTopSide = "";
                //                objPRL396Copy.B1NubID = "";
                //                objPRL396Copy.B2NubID = "";
                //                objPRL396Copy.C1NubBottomSide = "";
                //                objPRL396Copy.C2NubBottomSide = "";
                //                objPRL396Copy.CreatedBy = objClsLoginInfo.UserName;
                //                objPRL396Copy.CreatedOn = DateTime.Now;
                //                db.PRL396.Add(objPRL396Copy);
                //            }
                //            db.SaveChanges();
                //        }
                //    }
                //}
                #endregion

                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL080
                    PRO395 objPRO395 = db.PRO395.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO395 != null)
                    {
                        CopyProtocolData("PRO395", "PRL395", "PRL395.HeaderId=" + objPRO395.HeaderId + " AND PRO395.HeaderId=" + objPRO395.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO396", "PRL396", "HeaderId=" + objPRO395.HeaderId, "LineId,EditedBy,EditedOn", objPRO395.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO397", "PRL397", "HeaderId=" + objPRO395.HeaderId, "LineId,EditedBy,EditedOn", objPRO395.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO398", "PRL398", "HeaderId=" + objPRO395.HeaderId, "LineId,EditedBy,EditedOn", objPRO395.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO399", "PRL399", "HeaderId=" + objPRO395.HeaderId, "LineId,EditedBy,EditedOn", objPRO395.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);

                    }
                    #endregion
                }
                returnValue = objPRL395.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_VISUAL_AND_DIMENSION_INSPECTION_FOR_NUB_ON_D_END_BEFORE_OVERLAY.GetStringValue())
            {
                #region Add New Entry in PRL400 For PROTOCOL081
                PRL400 objPRL400 = new PRL400();
                objPRL400.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL400.QualityProject = HeaderData.QualityProject;
                objPRL400.EquipmentNo = HeaderData.EquipmentNo;
                objPRL400.DrawingNo = HeaderData.DrawingNo;
                objPRL400.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL400.DCRNo = HeaderData.DCRNo;
                objPRL400.SeamNo = HeaderData.SeamNo;
                objPRL400.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL400.Project = HeaderData.Project;
                objPRL400.BU = HeaderData.BU;
                objPRL400.Location = HeaderData.Location;
                objPRL400.StageCode = HeaderData.StageCode;
                objPRL400.StageSequence = HeaderData.StageSequence;
                objPRL400.ICLRevNo = HeaderData.ICLRevNo;
                objPRL400.CreatedBy = objClsLoginInfo.UserName;
                objPRL400.CreatedOn = DateTime.Now;

                #region  Orientation
                objPRL400.PRL401.Add(new PRL401
                {
                    Orientation = "0º",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL400.PRL401.Add(new PRL401
                {
                    Orientation = "45º",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL400.PRL401.Add(new PRL401
                {
                    Orientation = "90º",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL400.PRL401.Add(new PRL401
                {
                    Orientation = "135º",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL400.PRL401.Add(new PRL401
                {
                    Orientation = "180º",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL400.PRL401.Add(new PRL401
                {
                    Orientation = "225º",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL400.PRL401.Add(new PRL401
                {
                    Orientation = "270º",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL400.PRL401.Add(new PRL401
                {
                    Orientation = "315º",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });


                #endregion

                #region  OrientationReqNubID
                objPRL400.PRL402.Add(new PRL402
                {

                    OrientationReqNubID = "0°-180°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL400.PRL402.Add(new PRL402
                {

                    OrientationReqNubID = "22.5°-202.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL400.PRL402.Add(new PRL402
                {

                    OrientationReqNubID = "45°-225°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL400.PRL402.Add(new PRL402
                {

                    OrientationReqNubID = "67.5°-247.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL400.PRL402.Add(new PRL402
                {

                    OrientationReqNubID = "90°-270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL400.PRL402.Add(new PRL402
                {

                    OrientationReqNubID = "112.5°-292.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL400.PRL402.Add(new PRL402
                {

                    OrientationReqNubID = "135°-315°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL400.PRL402.Add(new PRL402
                {

                    OrientationReqNubID = "157.5°-337.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });


                #endregion


                #region  OtherDimensions
                objPRL400.PRL403.Add(new PRL403
                {

                    OtherDimensions = "SURFACE FINISH ON MACHINED SURFACES",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL400.PRL403.Add(new PRL403
                {

                    OtherDimensions = "LEVEL OF D'END TRIMMING LINE / REF. LINE / WEP",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL400.PRL403.Add(new PRL403
                {

                    OtherDimensions = "CONCENTRICITY OF NUB TO D'END OPEN END",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                db.PRL400.Add(objPRL400);
                db.SaveChanges();


                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL081
                    PRO400 objPRO400 = db.PRO400.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO400 != null)
                    {
                        CopyProtocolData("PRO400", "PRL400", "PRL400.HeaderId=" + objPRO400.HeaderId + " AND PRO400.HeaderId=" + objPRO400.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO401", "PRL401", "HeaderId=" + objPRO400.HeaderId, "LineId,EditedBy,EditedOn", objPRO400.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO402", "PRL402", "HeaderId=" + objPRO400.HeaderId, "LineId,EditedBy,EditedOn", objPRO400.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO403", "PRL403", "HeaderId=" + objPRO400.HeaderId, "LineId,EditedBy,EditedOn", objPRO400.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);


                    }
                    #endregion
                }
                returnValue = objPRL400.HeaderId;
            }

            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_VISUAL_AND_DIMENSION_INSPECTION_FOR_NUB_ON_D_END_AFTER_OVERLAY.GetStringValue())
            {
                #region Add New Entry in PRL405 For PROTOCOL082
                PRL405 objPRL405 = new PRL405();
                objPRL405.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL405.QualityProject = HeaderData.QualityProject;
                objPRL405.EquipmentNo = HeaderData.EquipmentNo;
                objPRL405.DrawingNo = HeaderData.DrawingNo;
                objPRL405.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL405.DCRNo = HeaderData.DCRNo;
                objPRL405.SeamNo = HeaderData.SeamNo;
                objPRL405.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL405.Project = HeaderData.Project;
                objPRL405.BU = HeaderData.BU;
                objPRL405.Location = HeaderData.Location;
                objPRL405.StageCode = HeaderData.StageCode;
                objPRL405.StageSequence = HeaderData.StageSequence;
                objPRL405.ICLRevNo = HeaderData.ICLRevNo;
                objPRL405.CreatedBy = objClsLoginInfo.UserName;
                objPRL405.CreatedOn = DateTime.Now;



                #region  OrientationReqNubID 

                objPRL405.PRL407.Add(new PRL407
                {

                    OrientationReqNubID = "0°-180°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL405.PRL407.Add(new PRL407
                {

                    OrientationReqNubID = "22.5°-202.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL405.PRL407.Add(new PRL407
                {

                    OrientationReqNubID = "45°-225°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL405.PRL407.Add(new PRL407
                {

                    OrientationReqNubID = "67.5°-247.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL405.PRL407.Add(new PRL407
                {

                    OrientationReqNubID = "90°-270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL405.PRL407.Add(new PRL407
                {

                    OrientationReqNubID = "112.5°-292.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL405.PRL407.Add(new PRL407
                {

                    OrientationReqNubID = "135°-315°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL405.PRL407.Add(new PRL407
                {

                    OrientationReqNubID = "157.5°-337.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });


                #endregion


                #region  OtherDimensions 

                objPRL405.PRL408.Add(new PRL408
                {

                    OtherDimensions = "SURFACE FINISH ON MACHINED SURFACES",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL405.PRL408.Add(new PRL408
                {

                    OtherDimensions = "LEVEL OF D'END TRIMMING LINE / REF. LINE / WEP",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL405.PRL408.Add(new PRL408
                {

                    OtherDimensions = "CONCENTRICITY OF NUB TO D'END OPEN END",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL405.PRL408.Add(new PRL408
                {

                    OtherDimensions = "DEPTH OF BARRIER LAYER FROM OVERLAY ON NUB FACE (TOP SIDE )",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL405.PRL408.Add(new PRL408
                {

                    OtherDimensions = "DEPTH OF BARRIER LAYER FROM OVERLAY ON NUB FACE  (I.D. SIDE )",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                objPRL405.PRL408.Add(new PRL408
                {

                    OtherDimensions = "DEPTH OF BARRIER LAYER FROM OVERLAY ON D'END I.D. (TOP SIDE )",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL405.PRL408.Add(new PRL408
                {

                    OtherDimensions = "DEPTH OF BARRIER LAYER FROM OVERLAY ON D'END I.D. (BOTTOM SIDE )",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL405.PRL408.Add(new PRL408
                {

                    OtherDimensions = "RADIUS ON BARRIER LAYER (TOP SIDE)",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL405.PRL408.Add(new PRL408
                {

                    OtherDimensions = "RADIUS ON BARRIER LAYER (BOTTOM SIDE)",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });


                #endregion


                db.PRL405.Add(objPRL405);
                db.SaveChanges();

                #region Default Data

                //if (objPRL405 != null)
                //{

                //    PRL400 _obj400 = db.PRL400.Where(w => w.SeamNo == objPRL405.SeamNo).FirstOrDefault();
                //    if (_obj400 != null)
                //    {

                //        List<PRL401> _lstPRL401 = db.PRL401.Where(w => w.HeaderId == _obj400.HeaderId).ToList();

                //        if (_lstPRL401.Count() > 0)
                //        {

                //            for (int i = 0; i < _lstPRL401.Count; i++)
                //            {
                //                PRL406 objPRL406Copy = new PRL406();

                //                objPRL406Copy.HeaderId = objPRL405.HeaderId;


                //                objPRL406Copy.Orientation = _lstPRL401[i].Orientation;
                //                objPRL406Copy.ActTolReqElevationOfNubFrom = "";
                //                objPRL406Copy.ActDimentionOfNubId = "";
                //                objPRL406Copy.ElevationOfNubBeforeOvarlay = _lstPRL401[i].ActADimension;
                //                objPRL406Copy.DimensionOfNubIdBeforeOvarlay = _lstPRL401[i].ActBDimension;
                //                objPRL406Copy.OvarlayThicknessNubTopSide = "";
                //                objPRL406Copy.OvarlayThicknessNubIdSide = "";
                //                objPRL406Copy.CreatedBy = objClsLoginInfo.UserName;
                //                objPRL406Copy.CreatedOn = DateTime.Now;

                //                db.PRL406.Add(objPRL406Copy);

                //            }
                //            db.SaveChanges();
                //        }
                //    }
                //}
                #endregion


                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL082
                    PRO405 objPRO405 = db.PRO405.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO405 != null)
                    {
                        CopyProtocolData("PRO405", "PRL405", "PRL405.HeaderId=" + objPRO405.HeaderId + " AND PRO405.HeaderId=" + objPRO405.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO406", "PRL406", "HeaderId=" + objPRO405.HeaderId, "LineId,EditedBy,EditedOn", objPRO405.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO407", "PRL407", "HeaderId=" + objPRO405.HeaderId, "LineId,EditedBy,EditedOn", objPRO405.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO408", "PRL408", "HeaderId=" + objPRO405.HeaderId, "LineId,EditedBy,EditedOn", objPRO405.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);

                    }
                    #endregion
                }
                returnValue = objPRL405.HeaderId;
            }

            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_VISUAL_AND_DIMENSION_INSPECTION_OF_SKIRT_ASSEMBLY.GetStringValue())
            {
                #region Add New Entry in PRL410 For PROTOCOL083
                PRL410 objPRL410 = new PRL410();
                objPRL410.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL410.QualityProject = HeaderData.QualityProject;
                objPRL410.EquipmentNo = HeaderData.EquipmentNo;
                objPRL410.DrawingNo = HeaderData.DrawingNo;
                objPRL410.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL410.DCRNo = HeaderData.DCRNo;
                objPRL410.SeamNo = HeaderData.SeamNo;
                objPRL410.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL410.Project = HeaderData.Project;
                objPRL410.BU = HeaderData.BU;
                objPRL410.Location = HeaderData.Location;
                objPRL410.StageCode = HeaderData.StageCode;
                objPRL410.StageSequence = HeaderData.StageSequence;
                objPRL410.ICLRevNo = HeaderData.ICLRevNo;
                objPRL410.CreatedBy = objClsLoginInfo.UserName;
                objPRL410.CreatedOn = DateTime.Now;

                #region  CIRCUMFERENCE MEASUREMENT | PRL410
                objPRL410.PRL411.Add(new PRL411
                {

                    Location = "TOP",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL410.PRL411.Add(new PRL411
                {

                    Location = "BOTTOM",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region  TOTAL SKIRT HEIGHT | PRL410
                objPRL410.PRL412.Add(new PRL412
                {

                    Orientation = "0°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL410.PRL412.Add(new PRL412
                {

                    Orientation = "45°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL410.PRL412.Add(new PRL412
                {

                    Orientation = "90°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL410.PRL412.Add(new PRL412
                {

                    Orientation = "135°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL410.PRL412.Add(new PRL412
                {

                    Orientation = "180°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL410.PRL412.Add(new PRL412
                {

                    Orientation = "225°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL410.PRL412.Add(new PRL412
                {

                    Orientation = "270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL410.PRL412.Add(new PRL412
                {

                    Orientation = "315°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region  OUT OF ROUNDNESS | PRL410
                objPRL410.PRL413.Add(new PRL413
                {

                    Orientation = "0°-180°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL410.PRL413.Add(new PRL413
                {

                    Orientation = "22.5°-205.5",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL410.PRL413.Add(new PRL413
                {

                    Orientation = "45°-225°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL410.PRL413.Add(new PRL413
                {

                    Orientation = "67.5°-247.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL410.PRL413.Add(new PRL413
                {

                    Orientation = "90°-270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL410.PRL413.Add(new PRL413
                {

                    Orientation = "112.5°-292.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL410.PRL413.Add(new PRL413
                {

                    Orientation = "135°-315°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL410.PRL413.Add(new PRL413
                {

                    Orientation = "157.5°-337.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region  CHARACTERISTICS / DESCRIPTION | PRL410
                objPRL410.PRL414_2.Add(new PRL414_2
                {
                    SrNo = 1,
                    Characteristics = "ELEVATION OF REF. LINE FROM B.T.L",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL410.PRL414_2.Add(new PRL414_2
                {
                    SrNo = 2,
                    Characteristics = "DIAMETER OF TAILING LUG HOLES",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL410.PRL414_2.Add(new PRL414_2
                {
                    SrNo = 3,
                    Characteristics = "DISTANCE BETWEEN BOTH TAILING LUGS",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL410.PRL414_2.Add(new PRL414_2
                {
                    SrNo = 4,
                    Characteristics = "TAILING LUG THICKNESS WITH CHICK PLATES",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                #region  HOLE TO HOLE1 | PRL410
                objPRL410.PRL414_3.Add(new PRL414_3
                {



                    HoleToHole1 = "H1",
                    HoleToHole2 = "H17",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL410.PRL414_3.Add(new PRL414_3
                {



                    HoleToHole1 = "H2",
                    HoleToHole2 = "H18",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL410.PRL414_3.Add(new PRL414_3
                {



                    HoleToHole1 = "H3",
                    HoleToHole2 = "H19",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL410.PRL414_3.Add(new PRL414_3
                {



                    HoleToHole1 = "H4",
                    HoleToHole2 = "H20",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL410.PRL414_3.Add(new PRL414_3
                {



                    HoleToHole1 = "H5",
                    HoleToHole2 = "H21",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL410.PRL414_3.Add(new PRL414_3
                {



                    HoleToHole1 = "H6",
                    HoleToHole2 = "H22",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now



                });
                objPRL410.PRL414_3.Add(new PRL414_3
                {



                    HoleToHole1 = "H7",
                    HoleToHole2 = "H23",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL410.PRL414_3.Add(new PRL414_3
                {



                    HoleToHole1 = "H8",
                    HoleToHole2 = "H24",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL410.PRL414_3.Add(new PRL414_3
                {



                    HoleToHole1 = "H9",
                    HoleToHole2 = "H25",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL410.PRL414_3.Add(new PRL414_3
                {



                    HoleToHole1 = "H10",
                    HoleToHole2 = "H26",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL410.PRL414_3.Add(new PRL414_3
                {



                    HoleToHole1 = "H11",
                    HoleToHole2 = "H27",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL410.PRL414_3.Add(new PRL414_3
                {



                    HoleToHole1 = "H12",
                    HoleToHole2 = "H28",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL410.PRL414_3.Add(new PRL414_3
                {



                    HoleToHole1 = "H13",
                    HoleToHole2 = "H29",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL410.PRL414_3.Add(new PRL414_3
                {



                    HoleToHole1 = "H14",
                    HoleToHole2 = "H30",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL410.PRL414_3.Add(new PRL414_3
                {



                    HoleToHole1 = "H15",
                    HoleToHole2 = "H31",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL410.PRL414_3.Add(new PRL414_3
                {



                    HoleToHole1 = "H16",
                    HoleToHole2 = "H32",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });



                #endregion

                #region  HOLE1 | PRL410
                objPRL410.PRL414_4.Add(new PRL414_4
                {

                    Hole1 = "H1",
                    Hole2 = "H17",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL410.PRL414_4.Add(new PRL414_4
                {

                    Hole1 = "H2",
                    Hole2 = "H18",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL410.PRL414_4.Add(new PRL414_4
                {

                    Hole1 = "H3",
                    Hole2 = "H19",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL410.PRL414_4.Add(new PRL414_4
                {

                    Hole1 = "H4",
                    Hole2 = "H20",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL410.PRL414_4.Add(new PRL414_4
                {

                    Hole1 = "H5",
                    Hole2 = "H21",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL410.PRL414_4.Add(new PRL414_4
                {

                    Hole1 = "H6",
                    Hole2 = "H22",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL410.PRL414_4.Add(new PRL414_4
                {

                    Hole1 = "H7",
                    Hole2 = "H23",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL410.PRL414_4.Add(new PRL414_4
                {

                    Hole1 = "H8",
                    Hole2 = "H24",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL410.PRL414_4.Add(new PRL414_4
                {

                    Hole1 = "H9",
                    Hole2 = "H25",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL410.PRL414_4.Add(new PRL414_4
                {

                    Hole1 = "H10",
                    Hole2 = "H26",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL410.PRL414_4.Add(new PRL414_4
                {

                    Hole1 = "H11",
                    Hole2 = "H27",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL410.PRL414_4.Add(new PRL414_4
                {

                    Hole1 = "H12",
                    Hole2 = "H28",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL410.PRL414_4.Add(new PRL414_4
                {

                    Hole1 = "H13",
                    Hole2 = "H29",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL410.PRL414_4.Add(new PRL414_4
                {

                    Hole1 = "H14",
                    Hole2 = "H30",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL410.PRL414_4.Add(new PRL414_4
                {

                    Hole1 = "H15",
                    Hole2 = "H31",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL410.PRL414_4.Add(new PRL414_4
                {

                    Hole1 = "H16",
                    Hole2 = "H32",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                #region  OrientationDistance | PRL410
                objPRL410.PRL414_5.Add(new PRL414_5
                {

                    OrientationDistance = "0°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL410.PRL414_5.Add(new PRL414_5
                {

                    OrientationDistance = "45°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL410.PRL414_5.Add(new PRL414_5
                {

                    OrientationDistance = "90°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL410.PRL414_5.Add(new PRL414_5
                {

                    OrientationDistance = "135°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL410.PRL414_5.Add(new PRL414_5
                {

                    OrientationDistance = "180°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL410.PRL414_5.Add(new PRL414_5
                {

                    OrientationDistance = "225°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL410.PRL414_5.Add(new PRL414_5
                {

                    OrientationDistance = "270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL410.PRL414_5.Add(new PRL414_5
                {

                    OrientationDistance = "315°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion



                db.PRL410.Add(objPRL410);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL083
                    PRO410 objPRO410 = db.PRO410.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO410 != null)
                    {
                        CopyProtocolData("PRO410", "PRL410", "PRL410.HeaderId=" + objPRO410.HeaderId + " AND PRO410.HeaderId=" + objPRO410.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO411", "PRL411", "HeaderId=" + objPRO410.HeaderId, "LineId,EditedBy,EditedOn", objPRO410.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO412", "PRL412", "HeaderId=" + objPRO410.HeaderId, "LineId,EditedBy,EditedOn", objPRO410.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO413", "PRL413", "HeaderId=" + objPRO410.HeaderId, "LineId,EditedBy,EditedOn", objPRO410.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO414", "PRL414", "HeaderId=" + objPRO410.HeaderId, "LineId,EditedBy,EditedOn", objPRO410.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO414_2", "PRL414_2", "HeaderId=" + objPRO410.HeaderId, "LineId,EditedBy,EditedOn", objPRO410.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO414_3", "PRL414_3", "HeaderId=" + objPRO410.HeaderId, "LineId,EditedBy,EditedOn", objPRO410.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO414_4", "PRL414_4", "HeaderId=" + objPRO410.HeaderId, "LineId,EditedBy,EditedOn", objPRO410.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO414_5", "PRL414_5", "HeaderId=" + objPRO410.HeaderId, "LineId,EditedBy,EditedOn", objPRO410.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO414_6", "PRL414_6", "HeaderId=" + objPRO410.HeaderId, "LineId,EditedBy,EditedOn", objPRO410.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO414_7", "PRL414_7", "HeaderId=" + objPRO410.HeaderId, "LineId,EditedBy,EditedOn", objPRO410.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);


                    }
                    #endregion
                }
                returnValue = objPRL410.HeaderId;
            }

            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_VISUAL_AND_DIMENSION_OF_SKIRT_TEMPLATE.GetStringValue())
            {
                #region Add New Entry in PRL415 For PROTOCOL084
                PRL415 objPRL415 = new PRL415();
                objPRL415.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL415.QualityProject = HeaderData.QualityProject;
                objPRL415.EquipmentNo = HeaderData.EquipmentNo;
                objPRL415.DrawingNo = HeaderData.DrawingNo;
                objPRL415.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL415.DCRNo = HeaderData.DCRNo;
                objPRL415.SeamNo = HeaderData.SeamNo;
                objPRL415.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL415.Project = HeaderData.Project;
                objPRL415.BU = HeaderData.BU;
                objPRL415.Location = HeaderData.Location;
                objPRL415.StageCode = HeaderData.StageCode;
                objPRL415.StageSequence = HeaderData.StageSequence;
                objPRL415.ICLRevNo = HeaderData.ICLRevNo;
                objPRL415.CreatedBy = objClsLoginInfo.UserName;
                objPRL415.CreatedOn = DateTime.Now;

                #region  INSPECTION DESCREPTION | PRL416
                objPRL415.PRL416.Add(new PRL416
                {
                    Description = "OUTSIDE  DIAMETER",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL416.Add(new PRL416
                {
                    Description = "BOLT CIRCLE DIAMETER",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL416.Add(new PRL416
                {
                    Description = "INSIDE  DIAMETER",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL416.Add(new PRL416
                {
                    Description = "BOLT HOLE DIAMETER",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL416.Add(new PRL416
                {
                    Description = "ORIENTATION OF NOTCHES",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL416.Add(new PRL416
                {
                    Description = "NO.OF BOLT HOLES",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL416.Add(new PRL416
                {
                    Description = "BOLT HOLE PITCH",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL416.Add(new PRL416
                {
                    Description = "THICKNESS OF TEMPLATE RING",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });
                objPRL415.PRL416.Add(new PRL416
                {
                    Description = "HEIGHT OF TEMPLATE",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL416.Add(new PRL416
                {
                    Description = "ALIGNMENT OF HOLES",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL416.Add(new PRL416
                {
                    Description = "DIAMETER OF ALIGNMENT PIN",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL416.Add(new PRL416
                {
                    Description = "FLATNESS OF TEMPLATE(IF APPLICABLE)",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });


                #endregion

                #region BOLT CIRCLE DIAMETER HOLE TO HOLE  | PRL417
                objPRL415.PRL417.Add(new PRL417
                {

                    HoleToHole1 = "H1",
                    HoleToHole2 = "H17",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL417.Add(new PRL417
                {

                    HoleToHole1 = "H2",
                    HoleToHole2 = "H18",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL417.Add(new PRL417
                {

                    HoleToHole1 = "H3",
                    HoleToHole2 = "H19",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL417.Add(new PRL417
                {

                    HoleToHole1 = "H4",
                    HoleToHole2 = "H20",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL417.Add(new PRL417
                {

                    HoleToHole1 = "H5",
                    HoleToHole2 = "H21",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL417.Add(new PRL417
                {

                    HoleToHole1 = "H6",
                    HoleToHole2 = "H22",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });
                objPRL415.PRL417.Add(new PRL417
                {

                    HoleToHole1 = "H7",
                    HoleToHole2 = "H23",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL417.Add(new PRL417
                {

                    HoleToHole1 = "H8",
                    HoleToHole2 = "H24",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL417.Add(new PRL417
                {

                    HoleToHole1 = "H9",
                    HoleToHole2 = "H25",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL417.Add(new PRL417
                {

                    HoleToHole1 = "H10",
                    HoleToHole2 = "H26",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL417.Add(new PRL417
                {

                    HoleToHole1 = "H11",
                    HoleToHole2 = "H27",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL417.Add(new PRL417
                {

                    HoleToHole1 = "H12",
                    HoleToHole2 = "H28",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL417.Add(new PRL417
                {

                    HoleToHole1 = "H13",
                    HoleToHole2 = "H29",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL417.Add(new PRL417
                {

                    HoleToHole1 = "H14",
                    HoleToHole2 = "H30",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL417.Add(new PRL417
                {

                    HoleToHole1 = "H15",
                    HoleToHole2 = "H31",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL417.Add(new PRL417
                {

                    HoleToHole1 = "H16",
                    HoleToHole2 = "H32",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                #region BOLT HOLE PITCH DIMENSION | PRL418
                objPRL415.PRL418.Add(new PRL418
                {

                    HoleToHole1 = "H1",
                    HoleToHole2 = "H2",
                    HoleToHole3 = "H17",
                    HoleToHole4 = "H18",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL418.Add(new PRL418
                {

                    HoleToHole1 = "H2",
                    HoleToHole2 = "H3",
                    HoleToHole3 = "H18",
                    HoleToHole4 = "H19",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL418.Add(new PRL418
                {

                    HoleToHole1 = "H3",
                    HoleToHole2 = "H4",
                    HoleToHole3 = "H19",
                    HoleToHole4 = "H20",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL418.Add(new PRL418
                {

                    HoleToHole1 = "H4",
                    HoleToHole2 = "H5",
                    HoleToHole3 = "H20",
                    HoleToHole4 = "H21",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL418.Add(new PRL418
                {

                    HoleToHole1 = "H5",
                    HoleToHole2 = "H6",
                    HoleToHole3 = "H21",
                    HoleToHole4 = "H22",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL418.Add(new PRL418
                {

                    HoleToHole1 = "H6",
                    HoleToHole2 = "H7",
                    HoleToHole3 = "H22",
                    HoleToHole4 = "H23",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });
                objPRL415.PRL418.Add(new PRL418
                {

                    HoleToHole1 = "H7",
                    HoleToHole2 = "H8",
                    HoleToHole3 = "H23",
                    HoleToHole4 = "H24",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL418.Add(new PRL418
                {

                    HoleToHole1 = "H8",
                    HoleToHole2 = "H9",
                    HoleToHole3 = "H24",
                    HoleToHole4 = "H25",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL418.Add(new PRL418
                {

                    HoleToHole1 = "H9",
                    HoleToHole2 = "H10",
                    HoleToHole3 = "H25",
                    HoleToHole4 = "H26",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL418.Add(new PRL418
                {

                    HoleToHole1 = "H10",
                    HoleToHole2 = "H11",
                    HoleToHole3 = "H26",
                    HoleToHole4 = "H27",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL418.Add(new PRL418
                {

                    HoleToHole1 = "H11",
                    HoleToHole2 = "H12",
                    HoleToHole3 = "H27",
                    HoleToHole4 = "H28",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL418.Add(new PRL418
                {

                    HoleToHole1 = "H12",
                    HoleToHole2 = "H13",
                    HoleToHole3 = "H28",
                    HoleToHole4 = "H29",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL418.Add(new PRL418
                {

                    HoleToHole1 = "H13",
                    HoleToHole2 = "H14",
                    HoleToHole3 = "H29",
                    HoleToHole4 = "H30",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL418.Add(new PRL418
                {

                    HoleToHole1 = "H14",
                    HoleToHole2 = "H15",
                    HoleToHole3 = "H30",
                    HoleToHole4 = "H31",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL418.Add(new PRL418
                {

                    HoleToHole1 = "H15",
                    HoleToHole2 = "H16",
                    HoleToHole3 = "H31",
                    HoleToHole4 = "H32",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL418.Add(new PRL418
                {

                    HoleToHole1 = "H16",
                    HoleToHole2 = "H17",
                    HoleToHole3 = "H32",
                    HoleToHole4 = "H1",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                #region BOLT HOLE DIAMETER | PRL419
                objPRL415.PRL419.Add(new PRL419
                {

                    Hole1 = "H1",
                    Hole2 = "H17",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419.Add(new PRL419
                {

                    Hole1 = "H2",
                    Hole2 = "H18",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419.Add(new PRL419
                {

                    Hole1 = "H3",
                    Hole2 = "H19",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419.Add(new PRL419
                {

                    Hole1 = "H4",
                    Hole2 = "H20",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419.Add(new PRL419
                {

                    Hole1 = "H5",
                    Hole2 = "H21",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419.Add(new PRL419
                {

                    Hole1 = "H6",
                    Hole2 = "H22",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });
                objPRL415.PRL419.Add(new PRL419
                {

                    Hole1 = "H7",
                    Hole2 = "H23",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419.Add(new PRL419
                {

                    Hole1 = "H8",
                    Hole2 = "H24",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419.Add(new PRL419
                {

                    Hole1 = "H9",
                    Hole2 = "H25",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419.Add(new PRL419
                {

                    Hole1 = "H10",
                    Hole2 = "H26",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419.Add(new PRL419
                {

                    Hole1 = "H11",
                    Hole2 = "H27",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419.Add(new PRL419
                {

                    Hole1 = "H12",
                    Hole2 = "H28",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419.Add(new PRL419
                {

                    Hole1 = "H13",
                    Hole2 = "H29",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419.Add(new PRL419
                {

                    Hole1 = "H14",
                    Hole2 = "H30",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419.Add(new PRL419
                {

                    Hole1 = "H15",
                    Hole2 = "H31",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419.Add(new PRL419
                {

                    Hole1 = "H16",
                    Hole2 = "H32",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                #region OVERALL HEIGHT OF TEMPLATE ORIENTATION | PRL419_2
                objPRL415.PRL419_2.Add(new PRL419_2
                {

                    Orientation = "0°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_2.Add(new PRL419_2
                {

                    Orientation = "90°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_2.Add(new PRL419_2
                {

                    Orientation = "180°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_2.Add(new PRL419_2
                {

                    Orientation = "270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });


                #endregion

                #region  ORIENTATION | PRL419_3
                objPRL415.PRL419_3.Add(new PRL419_3
                {

                    Orientation = "0°-180°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_3.Add(new PRL419_3
                {

                    Orientation = "45°-225°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_3.Add(new PRL419_3
                {

                    Orientation = "90°-270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_3.Add(new PRL419_3
                {

                    Orientation = "135° - 315°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });


                #endregion

                #region DIMNSION OF ONE HOLE FROM EACH HOLES | PRL419_4
                objPRL415.PRL419_4.Add(new PRL419_4
                {

                    HoleToHole1 = "A1",
                    HoleToHole2 = "A2",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_4.Add(new PRL419_4
                {

                    HoleToHole1 = "A2",
                    HoleToHole2 = "A3",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_4.Add(new PRL419_4
                {

                    HoleToHole1 = "A3",
                    HoleToHole2 = "A4",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_4.Add(new PRL419_4
                {

                    HoleToHole1 = "A4",
                    HoleToHole2 = "A5",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_4.Add(new PRL419_4
                {

                    HoleToHole1 = "A5",
                    HoleToHole2 = "A6",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_4.Add(new PRL419_4
                {

                    HoleToHole1 = "A6",
                    HoleToHole2 = "A7",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });
                objPRL415.PRL419_4.Add(new PRL419_4
                {

                    HoleToHole1 = "A7",
                    HoleToHole2 = "A8",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_4.Add(new PRL419_4
                {

                    HoleToHole1 = "A8",
                    HoleToHole2 = "A9",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_4.Add(new PRL419_4
                {

                    HoleToHole1 = "A9",
                    HoleToHole2 = "A10",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_4.Add(new PRL419_4
                {

                    HoleToHole1 = "A10",
                    HoleToHole2 = "A11",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_4.Add(new PRL419_4
                {

                    HoleToHole1 = "A11",
                    HoleToHole2 = "A12",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_4.Add(new PRL419_4
                {

                    HoleToHole1 = "A12",
                    HoleToHole2 = "A13",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_4.Add(new PRL419_4
                {

                    HoleToHole1 = "A13",
                    HoleToHole2 = "A14",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_4.Add(new PRL419_4
                {

                    HoleToHole1 = "A14",
                    HoleToHole2 = "A15",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_4.Add(new PRL419_4
                {

                    HoleToHole1 = "A15",
                    HoleToHole2 = "A16",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_4.Add(new PRL419_4
                {

                    HoleToHole1 = "A16",
                    HoleToHole2 = "A17",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_4.Add(new PRL419_4
                {

                    HoleToHole1 = "A17",
                    HoleToHole2 = "A18",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_4.Add(new PRL419_4
                {

                    HoleToHole1 = "A18",
                    HoleToHole2 = "A19",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_4.Add(new PRL419_4
                {

                    HoleToHole1 = "A19",
                    HoleToHole2 = "A20",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_4.Add(new PRL419_4
                {

                    HoleToHole1 = "A20",
                    HoleToHole2 = "A21",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_4.Add(new PRL419_4
                {

                    HoleToHole1 = "A21",
                    HoleToHole2 = "A22",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_4.Add(new PRL419_4
                {

                    HoleToHole1 = "A22",
                    HoleToHole2 = "A23",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_4.Add(new PRL419_4
                {

                    HoleToHole1 = "A23",
                    HoleToHole2 = "A24",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_4.Add(new PRL419_4
                {

                    HoleToHole1 = "A24",
                    HoleToHole2 = "A25",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_4.Add(new PRL419_4
                {
                    HoleToHole1 = "A25",
                    HoleToHole2 = "A26",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_4.Add(new PRL419_4
                {

                    HoleToHole1 = "A26",
                    HoleToHole2 = "A27",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_4.Add(new PRL419_4
                {

                    HoleToHole1 = "A27",
                    HoleToHole2 = "A28",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_4.Add(new PRL419_4
                {

                    HoleToHole1 = "A28",
                    HoleToHole2 = "A29",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_4.Add(new PRL419_4
                {
                    HoleToHole1 = "A29",
                    HoleToHole2 = "A30",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_4.Add(new PRL419_4
                {

                    HoleToHole1 = "A30",
                    HoleToHole2 = "A31",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_4.Add(new PRL419_4
                {

                    HoleToHole1 = "A31",
                    HoleToHole2 = "A32",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_4.Add(new PRL419_4
                {

                    HoleToHole1 = "A32",
                    HoleToHole2 = "A1",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                #region DIMNSION OF ONE HOLE FROM EACH HOLES | PRL419_5
                objPRL415.PRL419_5.Add(new PRL419_5
                {

                    HoleToHole1 = "B1",
                    HoleToHole2 = "B2",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_5.Add(new PRL419_5
                {

                    HoleToHole1 = "B2",
                    HoleToHole2 = "B3",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_5.Add(new PRL419_5
                {

                    HoleToHole1 = "B3",
                    HoleToHole2 = "B4",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_5.Add(new PRL419_5
                {

                    HoleToHole1 = "B4",
                    HoleToHole2 = "B5",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_5.Add(new PRL419_5
                {

                    HoleToHole1 = "B5",
                    HoleToHole2 = "B6",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_5.Add(new PRL419_5
                {

                    HoleToHole1 = "B6",
                    HoleToHole2 = "B7",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now

                });
                objPRL415.PRL419_5.Add(new PRL419_5
                {

                    HoleToHole1 = "B7",
                    HoleToHole2 = "B8",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_5.Add(new PRL419_5
                {

                    HoleToHole1 = "B8",
                    HoleToHole2 = "B9",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_5.Add(new PRL419_5
                {

                    HoleToHole1 = "B9",
                    HoleToHole2 = "B10",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_5.Add(new PRL419_5
                {

                    HoleToHole1 = "B10",
                    HoleToHole2 = "B11",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_5.Add(new PRL419_5
                {

                    HoleToHole1 = "B11",
                    HoleToHole2 = "B12",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_5.Add(new PRL419_5
                {

                    HoleToHole1 = "B12",
                    HoleToHole2 = "B13",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_5.Add(new PRL419_5
                {

                    HoleToHole1 = "B13",
                    HoleToHole2 = "B14",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_5.Add(new PRL419_5
                {

                    HoleToHole1 = "B14",
                    HoleToHole2 = "B15",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_5.Add(new PRL419_5
                {

                    HoleToHole1 = "B15",
                    HoleToHole2 = "B16",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_5.Add(new PRL419_5
                {

                    HoleToHole1 = "B16",
                    HoleToHole2 = "B17",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_5.Add(new PRL419_5
                {

                    HoleToHole1 = "B17",
                    HoleToHole2 = "B18",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_5.Add(new PRL419_5
                {

                    HoleToHole1 = "B18",
                    HoleToHole2 = "B19",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_5.Add(new PRL419_5
                {

                    HoleToHole1 = "B19",
                    HoleToHole2 = "B20",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_5.Add(new PRL419_5
                {

                    HoleToHole1 = "B20",
                    HoleToHole2 = "B21",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_5.Add(new PRL419_5
                {

                    HoleToHole1 = "B21",
                    HoleToHole2 = "B22",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_5.Add(new PRL419_5
                {

                    HoleToHole1 = "B22",
                    HoleToHole2 = "B23",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_5.Add(new PRL419_5
                {

                    HoleToHole1 = "B23",
                    HoleToHole2 = "B24",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_5.Add(new PRL419_5
                {

                    HoleToHole1 = "B24",
                    HoleToHole2 = "B25",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_5.Add(new PRL419_5
                {
                    HoleToHole1 = "B25",
                    HoleToHole2 = "B26",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_5.Add(new PRL419_5
                {

                    HoleToHole1 = "B26",
                    HoleToHole2 = "B27",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_5.Add(new PRL419_5
                {

                    HoleToHole1 = "B27",
                    HoleToHole2 = "B28",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_5.Add(new PRL419_5
                {

                    HoleToHole1 = "B28",
                    HoleToHole2 = "B29",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_5.Add(new PRL419_5
                {
                    HoleToHole1 = "B29",
                    HoleToHole2 = "B30",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_5.Add(new PRL419_5
                {

                    HoleToHole1 = "B30",
                    HoleToHole2 = "B31",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_5.Add(new PRL419_5
                {

                    HoleToHole1 = "B31",
                    HoleToHole2 = "B32",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL415.PRL419_5.Add(new PRL419_5
                {

                    HoleToHole1 = "B32",
                    HoleToHole2 = "B1",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion



                db.PRL415.Add(objPRL415);
                db.SaveChanges();

                #endregion


                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL084
                    PRO415 objPRO415 = db.PRO415.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO415 != null)
                    {
                        CopyProtocolData("PRO415", "PRL415", "PRL415.HeaderId=" + objPRO415.HeaderId + " AND PRO415.HeaderId=" + objPRO415.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO416", "PRL416", "HeaderId=" + objPRO415.HeaderId, "LineId,EditedBy,EditedOn", objPRO415.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO417", "PRL417", "HeaderId=" + objPRO415.HeaderId, "LineId,EditedBy,EditedOn", objPRO415.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO418", "PRL418", "HeaderId=" + objPRO415.HeaderId, "LineId,EditedBy,EditedOn", objPRO415.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO419", "PRL419", "HeaderId=" + objPRO415.HeaderId, "LineId,EditedBy,EditedOn", objPRO415.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO419_2", "PRL419_2", "HeaderId=" + objPRO415.HeaderId, "LineId,EditedBy,EditedOn", objPRO415.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO419_3", "PRL419_3", "HeaderId=" + objPRO415.HeaderId, "LineId,EditedBy,EditedOn", objPRO415.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO419_4", "PRL419_4", "HeaderId=" + objPRO415.HeaderId, "LineId,EditedBy,EditedOn", objPRO415.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO419_5", "PRL419_5", "HeaderId=" + objPRO415.HeaderId, "LineId,EditedBy,EditedOn", objPRO415.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);

                    }
                    #endregion
                }
                returnValue = objPRL415.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_WELD_VISUAL_AND_DIMENSION_REPORT_FOR_SPOOL_TYPE_1_BOTH_SIDE_FLANGES.GetStringValue())
            {
                #region Add New Entry in PRL420 For PROTOCOL085
                PRL420 objPRL420 = new PRL420();
                objPRL420.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL420.QualityProject = HeaderData.QualityProject;
                objPRL420.EquipmentNo = HeaderData.EquipmentNo;
                objPRL420.DrawingNo = HeaderData.DrawingNo;
                objPRL420.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL420.DCRNo = HeaderData.DCRNo;
                objPRL420.SeamNo = HeaderData.SeamNo;
                objPRL420.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL420.Project = HeaderData.Project;
                objPRL420.BU = HeaderData.BU;
                objPRL420.Location = HeaderData.Location;
                objPRL420.StageCode = HeaderData.StageCode;
                objPRL420.StageSequence = HeaderData.StageSequence;
                objPRL420.ICLRevNo = HeaderData.ICLRevNo;
                objPRL420.CreatedBy = objClsLoginInfo.UserName;
                objPRL420.CreatedOn = DateTime.Now;


                #endregion

                db.PRL420.Add(objPRL420);
                db.SaveChanges();


                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL085
                    PRO420 objPRO420 = db.PRO420.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO420 != null)
                    {
                        CopyProtocolData("PRO420", "PRL420", "PRL420.HeaderId=" + objPRO420.HeaderId + " AND PRO420.HeaderId=" + objPRO420.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO421", "PRL421", "HeaderId=" + objPRO420.HeaderId, "LineId,EditedBy,EditedOn", objPRO420.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);


                    }
                    #endregion
                }
                returnValue = objPRL420.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_WELD_VISUAL_AND_DIMENSION_FOR_SPOOL_TYPE_2_ONE_SIDE_FLANGE.GetStringValue())
            {
                #region Add New Entry in PRL425 For PROTOCOL086
                PRL425 objPRL425 = new PRL425();
                objPRL425.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL425.QualityProject = HeaderData.QualityProject;
                objPRL425.EquipmentNo = HeaderData.EquipmentNo;
                objPRL425.DrawingNo = HeaderData.DrawingNo;
                objPRL425.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL425.DCRNo = HeaderData.DCRNo;
                objPRL425.SeamNo = HeaderData.SeamNo;
                objPRL425.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL425.Project = HeaderData.Project;
                objPRL425.BU = HeaderData.BU;
                objPRL425.Location = HeaderData.Location;
                objPRL425.StageCode = HeaderData.StageCode;
                objPRL425.StageSequence = HeaderData.StageSequence;
                objPRL425.ICLRevNo = HeaderData.ICLRevNo;
                objPRL425.CreatedBy = objClsLoginInfo.UserName;
                objPRL425.CreatedOn = DateTime.Now;




                db.PRL425.Add(objPRL425);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL086
                    PRO425 objPRO425 = db.PRO425.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO425 != null)
                    {
                        CopyProtocolData("PRO425", "PRL425", "PRL425.HeaderId=" + objPRO425.HeaderId + " AND PRO425.HeaderId=" + objPRO425.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO426", "PRL426", "HeaderId=" + objPRO425.HeaderId, "LineId,EditedBy,EditedOn", objPRO425.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);


                    }
                    #endregion
                }
                returnValue = objPRL425.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_WELD_VISUAL_AND_DIMENSION_REPORT_FOR_SPOOL_TYPE_3_BOTH_SIDE_FLANGES.GetStringValue())
            {
                #region Add New Entry in PRL430 For PROTOCOL087
                PRL430 objPRL430 = new PRL430();
                objPRL430.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL430.QualityProject = HeaderData.QualityProject;
                objPRL430.EquipmentNo = HeaderData.EquipmentNo;
                objPRL430.DrawingNo = HeaderData.DrawingNo;
                objPRL430.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL430.DCRNo = HeaderData.DCRNo;
                objPRL430.SeamNo = HeaderData.SeamNo;
                objPRL430.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL430.Project = HeaderData.Project;
                objPRL430.BU = HeaderData.BU;
                objPRL430.Location = HeaderData.Location;
                objPRL430.StageCode = HeaderData.StageCode;
                objPRL430.StageSequence = HeaderData.StageSequence;
                objPRL430.ICLRevNo = HeaderData.ICLRevNo;
                objPRL430.CreatedBy = objClsLoginInfo.UserName;
                objPRL430.CreatedOn = DateTime.Now;




                db.PRL430.Add(objPRL430);
                db.SaveChanges();

                #endregion


                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL087
                    PRO430 objPRO430 = db.PRO430.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO430 != null)
                    {
                        CopyProtocolData("PRO430", "PRL430", "PRL430.HeaderId=" + objPRO430.HeaderId + " AND PRO430.HeaderId=" + objPRO430.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO431", "PRL431", "HeaderId=" + objPRO430.HeaderId, "LineId,EditedBy,EditedOn", objPRO430.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);


                    }
                    #endregion
                }
                returnValue = objPRL430.HeaderId;
            }

            else if (ProtocolType == clsImplementationEnum.ProtocolType.AFTER_PWHTFINAL_REPORT_FOR_VISUAL_AND_DIMENSION_AFTER_MACHINING_OF_GASKET_FACE_AND_OTHER_DIMENSIONS_OF_FLANGE.GetStringValue())
            {
                #region Add New Entry in PRL435 For PROTOCOL088
                PRL435 objPRL435 = new PRL435();
                objPRL435.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL435.QualityProject = HeaderData.QualityProject;
                objPRL435.EquipmentNo = HeaderData.EquipmentNo;
                objPRL435.DrawingNo = HeaderData.DrawingNo;
                objPRL435.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL435.DCRNo = HeaderData.DCRNo;
                objPRL435.SeamNo = HeaderData.SeamNo;
                objPRL435.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL435.Project = HeaderData.Project;
                objPRL435.BU = HeaderData.BU;
                objPRL435.Location = HeaderData.Location;
                objPRL435.StageCode = HeaderData.StageCode;
                objPRL435.StageSequence = HeaderData.StageSequence;
                objPRL435.ICLRevNo = HeaderData.ICLRevNo;
                objPRL435.CreatedBy = objClsLoginInfo.UserName;
                objPRL435.CreatedOn = DateTime.Now;

                #region  MACHINING | PRL437
                objPRL435.PRL437.Add(new PRL437
                {
                    SrNo = "1",
                    Description = "Identification",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL435.PRL437.Add(new PRL437
                {
                    SrNo = "2",
                    Description = "Visual inspection",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL435.PRL437.Add(new PRL437
                {
                    SrNo = "3",
                    Description = "Gasket face step OD",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL435.PRL437.Add(new PRL437
                {
                    SrNo = "4",
                    Description = "Gasket height",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL435.PRL437.Add(new PRL437
                {
                    SrNo = "5",
                    Description = "Gasket face finish",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL435.PRL437.Add(new PRL437
                {
                    SrNo = "6",
                    Description = "Gasket step OD radius Top",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL435.PRL437.Add(new PRL437
                {
                    SrNo = "7",
                    Description = "Gasket step OD radius Bottom",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL435.PRL437.Add(new PRL437
                {
                    SrNo = "8",
                    Description = "Groove width",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL435.PRL437.Add(new PRL437
                {
                    SrNo = "9",
                    Description = "Groove depth",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL435.PRL437.Add(new PRL437
                {
                    SrNo = "10",
                    Description = "Groove corner radius",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL435.PRL437.Add(new PRL437
                {
                    SrNo = "11",
                    Description = "Groove angle",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL435.PRL437.Add(new PRL437
                {
                    SrNo = "12",
                    Description = "Groove taper finish",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL435.PRL437.Add(new PRL437
                {
                    SrNo = "13",
                    Description = "Groove face finish",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL435.PRL437.Add(new PRL437
                {
                    SrNo = "14",
                    Description = "Gasket groove P.C.D",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL435.PRL437.Add(new PRL437
                {
                    SrNo = "15",
                    Description = "Final Visual inspection after O/L",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                db.PRL435.Add(objPRL435);
                db.SaveChanges();

                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL088
                    PRO435 objPRO435 = db.PRO435.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO435 != null)
                    {
                        CopyProtocolData("PRO435", "PRL435", "PRL435.HeaderId=" + objPRO435.HeaderId + " AND PRO435.HeaderId=" + objPRO435.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");
                        CopyProtocolLinesData("PRO436", "PRL436", "HeaderId=" + objPRO435.HeaderId, "LineId,EditedBy,EditedOn", objPRO435.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO437", "PRL437", "HeaderId=" + objPRO435.HeaderId, "LineId,EditedBy,EditedOn", objPRO435.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                    }
                    #endregion
                }
                returnValue = objPRL435.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.AFTER_PWHTFINAL_REPORT_FOR_VISUAL_AND_DIMENSION_AFTER_MACHINING_OF_GASKET_FACE_AND_OTHER_DIMENSIONS_OF_NOZZLE.GetStringValue())
            {
                #region Add New Entry in PRL440 For PROTOCOL089
                PRL440 objPRL440 = new PRL440();
                objPRL440.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL440.QualityProject = HeaderData.QualityProject;
                objPRL440.EquipmentNo = HeaderData.EquipmentNo;
                objPRL440.DrawingNo = HeaderData.DrawingNo;
                objPRL440.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL440.DCRNo = HeaderData.DCRNo;
                objPRL440.SeamNo = HeaderData.SeamNo;
                objPRL440.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL440.Project = HeaderData.Project;
                objPRL440.BU = HeaderData.BU;
                objPRL440.Location = HeaderData.Location;
                objPRL440.StageCode = HeaderData.StageCode;
                objPRL440.StageSequence = HeaderData.StageSequence;
                objPRL440.ICLRevNo = HeaderData.ICLRevNo;
                objPRL440.CreatedBy = objClsLoginInfo.UserName;
                objPRL440.CreatedOn = DateTime.Now;

                #region  MACHINING | PRL442
                objPRL440.PRL442.Add(new PRL442
                {
                    SrNo = "1",
                    Description = "Identification",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL440.PRL442.Add(new PRL442
                {
                    SrNo = "2",
                    Description = "Visual inspection",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL440.PRL442.Add(new PRL442
                {
                    SrNo = "3",
                    Description = "ID before O/L",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL440.PRL442.Add(new PRL442
                {
                    SrNo = "4",
                    Description = "ID after O/L",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL440.PRL442.Add(new PRL442
                {
                    SrNo = "5",
                    Description = "ID Machining up to length",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL440.PRL442.Add(new PRL442
                {
                    SrNo = "6",
                    Description = "ID Surface finish",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL440.PRL442.Add(new PRL442
                {
                    SrNo = "7",
                    Description = "Gasket face step OD",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL440.PRL442.Add(new PRL442
                {
                    SrNo = "8",
                    Description = "Gasket height",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL440.PRL442.Add(new PRL442
                {
                    SrNo = "9",
                    Description = "Gasket face finish",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL440.PRL442.Add(new PRL442
                {
                    SrNo = "10",
                    Description = "Gasket step OD radius Top",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL440.PRL442.Add(new PRL442
                {
                    SrNo = "11",
                    Description = "Gasket step OD radius Bottom",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL440.PRL442.Add(new PRL442
                {
                    SrNo = "12",
                    Description = "Groove width",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL440.PRL442.Add(new PRL442
                {
                    SrNo = "13",
                    Description = "Groove depth",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL440.PRL442.Add(new PRL442
                {
                    SrNo = "14",
                    Description = "Groove corner radius",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL440.PRL442.Add(new PRL442
                {
                    SrNo = "15",
                    Description = "Groove angle",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL440.PRL442.Add(new PRL442
                {
                    SrNo = "16",
                    Description = "Groove taper finish",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL440.PRL442.Add(new PRL442
                {
                    SrNo = "17",
                    Description = "Groove face finish",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL440.PRL442.Add(new PRL442
                {
                    SrNo = "18",
                    Description = "Gasket groove P.C.D",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL440.PRL442.Add(new PRL442
                {
                    SrNo = "19",
                    Description = "Radius Gasket face to ID",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL440.PRL442.Add(new PRL442
                {
                    SrNo = "20",
                    Description = "O/L Thk.",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL440.PRL442.Add(new PRL442
                {
                    SrNo = "21",
                    Description = "ID to Back face radius",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL440.PRL442.Add(new PRL442
                {
                    SrNo = "22",
                    Description = "Back face height",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL440.PRL442.Add(new PRL442
                {
                    SrNo = "23",
                    Description = "Back face visual",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL440.PRL442.Add(new PRL442
                {
                    SrNo = "24",
                    Description = "Final Visual inspection after O/L",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                db.PRL440.Add(objPRL440);
                db.SaveChanges();

                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL089
                    PRO440 objPRO440 = db.PRO440.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO440 != null)
                    {
                        CopyProtocolData("PRO440", "PRL440", "PRL440.HeaderId=" + objPRO440.HeaderId + " AND PRO440.HeaderId=" + objPRO440.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");
                        CopyProtocolLinesData("PRO441", "PRL441", "HeaderId=" + objPRO440.HeaderId, "LineId,EditedBy,EditedOn", objPRO440.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO442", "PRL442", "HeaderId=" + objPRO440.HeaderId, "LineId,EditedBy,EditedOn", objPRO440.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                    }
                    #endregion
                }
                returnValue = objPRL440.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.INITIAL_MACHINING_VISUAL_AND_DIMENSION_REPORT_FOR_GASKET_FACE_AND_OTHER_DIMENSIONS_OF_FLANGE.GetStringValue())
            {
                #region Add New Entry in PRL445 For PROTOCOL090
                PRL445 objPRL445 = new PRL445();
                objPRL445.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL445.QualityProject = HeaderData.QualityProject;
                objPRL445.EquipmentNo = HeaderData.EquipmentNo;
                objPRL445.DrawingNo = HeaderData.DrawingNo;
                objPRL445.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL445.DCRNo = HeaderData.DCRNo;
                objPRL445.SeamNo = HeaderData.SeamNo;
                objPRL445.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL445.Project = HeaderData.Project;
                objPRL445.BU = HeaderData.BU;
                objPRL445.Location = HeaderData.Location;
                objPRL445.StageCode = HeaderData.StageCode;
                objPRL445.StageSequence = HeaderData.StageSequence;
                objPRL445.ICLRevNo = HeaderData.ICLRevNo;
                objPRL445.CreatedBy = objClsLoginInfo.UserName;
                objPRL445.CreatedOn = DateTime.Now;

                #region  MACHINING | PRO447
                objPRL445.PRL447.Add(new PRL447
                {
                    SrNo = "1",
                    Description = "Identification",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL445.PRL447.Add(new PRL447
                {
                    SrNo = "2",
                    Description = "Visual inspection",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL445.PRL447.Add(new PRL447
                {
                    SrNo = "3",
                    Description = "Gasket face step OD",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL445.PRL447.Add(new PRL447
                {
                    SrNo = "4",
                    Description = "Gasket height",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL445.PRL447.Add(new PRL447
                {
                    SrNo = "5",
                    Description = "Gasket face finish",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL445.PRL447.Add(new PRL447
                {
                    SrNo = "6",
                    Description = "Gasket step OD radius Top",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL445.PRL447.Add(new PRL447
                {
                    SrNo = "7",
                    Description = "Gasket step OD radius Bottom",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL445.PRL447.Add(new PRL447
                {
                    SrNo = "8",
                    Description = "Groove width",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL445.PRL447.Add(new PRL447
                {
                    SrNo = "9",
                    Description = "Groove depth",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL445.PRL447.Add(new PRL447
                {
                    SrNo = "10",
                    Description = "Groove corner radius",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL445.PRL447.Add(new PRL447
                {
                    SrNo = "11",
                    Description = "Groove angle",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL445.PRL447.Add(new PRL447
                {
                    SrNo = "12",
                    Description = "Groove taper finish",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL445.PRL447.Add(new PRL447
                {
                    SrNo = "13",
                    Description = "Groove face finish",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL445.PRL447.Add(new PRL447
                {
                    SrNo = "14",
                    Description = "Gasket groove P.C.D",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL445.PRL447.Add(new PRL447
                {
                    SrNo = "15",
                    Description = "Final Visual inspection after O/L",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                db.PRL445.Add(objPRL445);
                db.SaveChanges();

                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL090
                    PRO445 objPRO445 = db.PRO445.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO445 != null)
                    {
                        CopyProtocolData("PRO445", "PRL445", "PRL445.HeaderId=" + objPRO445.HeaderId + " AND PRO445.HeaderId=" + objPRO445.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");
                        CopyProtocolLinesData("PRO446", "PRL446", "HeaderId=" + objPRO445.HeaderId, "LineId,EditedBy,EditedOn", objPRO445.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO447", "PRL447", "HeaderId=" + objPRO445.HeaderId, "LineId,EditedBy,EditedOn", objPRO445.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                    }
                    #endregion
                }
                returnValue = objPRL445.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.INITIAL_MACHINING_VISUAL_AND_DIMENSION_REPORT_FOR_GASKET_FACE_AND_OTHER_DIMENSIONS_OF_NOZZLE.GetStringValue())
            {
                #region Add New Entry in PRL445 For PROTOCOL091
                PRL450 objPRL450 = new PRL450();
                objPRL450.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL450.QualityProject = HeaderData.QualityProject;
                objPRL450.EquipmentNo = HeaderData.EquipmentNo;
                objPRL450.DrawingNo = HeaderData.DrawingNo;
                objPRL450.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL450.DCRNo = HeaderData.DCRNo;
                objPRL450.SeamNo = HeaderData.SeamNo;
                objPRL450.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL450.Project = HeaderData.Project;
                objPRL450.BU = HeaderData.BU;
                objPRL450.Location = HeaderData.Location;
                objPRL450.StageCode = HeaderData.StageCode;
                objPRL450.StageSequence = HeaderData.StageSequence;
                objPRL450.ICLRevNo = HeaderData.ICLRevNo;
                objPRL450.CreatedBy = objClsLoginInfo.UserName;
                objPRL450.CreatedOn = DateTime.Now;

                #region  MACHINING | PRL452
                objPRL450.PRL452.Add(new PRL452
                {
                    SrNo = "1",
                    Description = "Identification",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL450.PRL452.Add(new PRL452
                {
                    SrNo = "2",
                    Description = "Visual inspection",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL450.PRL452.Add(new PRL452
                {
                    SrNo = "3",
                    Description = "ID before O/L",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL450.PRL452.Add(new PRL452
                {
                    SrNo = "4",
                    Description = "ID after O/L",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL450.PRL452.Add(new PRL452
                {
                    SrNo = "5",
                    Description = "ID Machining up to length",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL450.PRL452.Add(new PRL452
                {
                    SrNo = "6",
                    Description = "ID Surface finish",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL450.PRL452.Add(new PRL452
                {
                    SrNo = "7",
                    Description = "Gasket face step OD",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL450.PRL452.Add(new PRL452
                {
                    SrNo = "8",
                    Description = "Gasket height",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL450.PRL452.Add(new PRL452
                {
                    SrNo = "9",
                    Description = "Gasket face finish",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL450.PRL452.Add(new PRL452
                {
                    SrNo = "10",
                    Description = "Gasket step OD radius Top",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL450.PRL452.Add(new PRL452
                {
                    SrNo = "11",
                    Description = "Gasket step OD radius Bottom",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL450.PRL452.Add(new PRL452
                {
                    SrNo = "12",
                    Description = "Groove width",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL450.PRL452.Add(new PRL452
                {
                    SrNo = "13",
                    Description = "Groove depth",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL450.PRL452.Add(new PRL452
                {
                    SrNo = "14",
                    Description = "Groove corner radius",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL450.PRL452.Add(new PRL452
                {
                    SrNo = "15",
                    Description = "Groove angle",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL450.PRL452.Add(new PRL452
                {
                    SrNo = "16",
                    Description = "Groove taper finish",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL450.PRL452.Add(new PRL452
                {
                    SrNo = "17",
                    Description = "Groove face finish",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL450.PRL452.Add(new PRL452
                {
                    SrNo = "18",
                    Description = "Gasket groove P.C.D",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL450.PRL452.Add(new PRL452
                {
                    SrNo = "19",
                    Description = "Radius Gasket face to ID",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL450.PRL452.Add(new PRL452
                {
                    SrNo = "20",
                    Description = "O/L Thk.",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL450.PRL452.Add(new PRL452
                {
                    SrNo = "21",
                    Description = "ID to Back face radius",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL450.PRL452.Add(new PRL452
                {
                    SrNo = "22",
                    Description = "Back face height",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL450.PRL452.Add(new PRL452
                {
                    SrNo = "23",
                    Description = "Back face visual",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL450.PRL452.Add(new PRL452
                {
                    SrNo = "24",
                    Description = "Final Visual inspection after O/L",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                db.PRL450.Add(objPRL450);
                db.SaveChanges();

                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL091
                    PRO450 objPRO450 = db.PRO450.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO450 != null)
                    {
                        CopyProtocolData("PRO450", "PRL450", "PRL450.HeaderId=" + objPRO450.HeaderId + " AND PRO450.HeaderId=" + objPRO450.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");
                        CopyProtocolLinesData("PRO451", "PRL451", "HeaderId=" + objPRO450.HeaderId, "LineId,EditedBy,EditedOn", objPRO450.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO452", "PRL452", "HeaderId=" + objPRO450.HeaderId, "LineId,EditedBy,EditedOn", objPRO450.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                    }
                    #endregion
                }
                returnValue = objPRL450.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_WATER_BOX_MACHINING_INSPECTION_R1.GetStringValue())
            {
                #region Add New Entry in PRL455 For PROTOCOL092
                PRL455 objPRL455 = new PRL455();
                objPRL455.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL455.QualityProject = HeaderData.QualityProject;
                objPRL455.EquipmentNo = HeaderData.EquipmentNo;
                objPRL455.DrawingNo = HeaderData.DrawingNo;
                objPRL455.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL455.DCRNo = HeaderData.DCRNo;
                objPRL455.SeamNo = HeaderData.SeamNo;
                objPRL455.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL455.Project = HeaderData.Project;
                objPRL455.BU = HeaderData.BU;
                objPRL455.Location = HeaderData.Location;
                objPRL455.StageCode = HeaderData.StageCode;
                objPRL455.StageSequence = HeaderData.StageSequence;
                objPRL455.ICLRevNo = HeaderData.ICLRevNo;
                objPRL455.CreatedBy = objClsLoginInfo.UserName;
                objPRL455.CreatedOn = DateTime.Now;

                #region  MACHINING | PRL457
                objPRL455.PRL457.Add(new PRL457
                {
                    SrNo = "1",
                    Description = "Identification",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL455.PRL457.Add(new PRL457
                {
                    SrNo = "2",
                    Description = "Visual inspection",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL455.PRL457.Add(new PRL457
                {
                    SrNo = "3",
                    Description = "Bolt hole size",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL455.PRL457.Add(new PRL457
                {
                    SrNo = "4",
                    Description = "Total nos. of bolt holes",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL455.PRL457.Add(new PRL457
                {
                    SrNo = "5",
                    Description = "Bolt holes layout as per drg.",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL455.PRL457.Add(new PRL457
                {
                    SrNo = "6",
                    Description = "Bolt holes surface finish",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL455.PRL457.Add(new PRL457
                {
                    SrNo = "7",
                    Description = "Bolt hole  equal pitch",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL455.PRL457.Add(new PRL457
                {
                    SrNo = "8",
                    Description = "Bolt hole to bolt hole distance A",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL455.PRL457.Add(new PRL457
                {
                    SrNo = "9",
                    Description = "Bolt hole to bolt hole distance B",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL455.PRL457.Add(new PRL457
                {
                    SrNo = "10",
                    Description = "Bolt hole to bolt hole distance C",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL455.PRL457.Add(new PRL457
                {
                    SrNo = "11",
                    Description = "Bolt hole to bolt hole distance D",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL455.PRL457.Add(new PRL457
                {
                    SrNo = "12",
                    Description = "Bolt holes ref distance",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL455.PRL457.Add(new PRL457
                {
                    SrNo = "13",
                    Description = "Out side flange edge to bolt hole centre distance",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL455.PRL457.Add(new PRL457
                {
                    SrNo = "14",
                    Description = "Flange thickness",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL455.PRL457.Add(new PRL457
                {
                    SrNo = "15",
                    Description = "Surface Finish of flange",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL455.PRL457.Add(new PRL457
                {
                    SrNo = "16",
                    Description = "Vent holes as per drg.",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL455.PRL457.Add(new PRL457
                {
                    SrNo = "17",
                    Description = "Vent holes dimensions",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL455.PRL457.Add(new PRL457
                {
                    SrNo = "18",
                    Description = "Deburring",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region HOLE TO HOLE LIGAMENT | PRL458
                for (int i = 1; i < 180; i++)
                {
                    PRL458 obj = new PRL458();
                    if (i != 179)
                    {
                        obj.ReqHoleNoToHoleNo1 = (i + " to " + (i + 1));
                        obj.CreatedBy = objClsLoginInfo.UserName;
                        obj.CreatedOn = DateTime.Now;
                    }
                    else
                    {
                        obj.ReqHoleNoToHoleNo1 = (i + " to 1");
                        obj.CreatedBy = objClsLoginInfo.UserName;
                        obj.CreatedOn = DateTime.Now;
                    }
                    //System.Threading.Thread.Sleep(100);

                    objPRL455.PRL458.Add(obj);
                }
                #endregion

                #region OUT SIDE FRAME EDGE TO HOLE CENTER LIGAMENT & FRAME THICKNESS | PRL459
                for (int i = 1; i < 180; i++)
                {
                    PRL459 obj = new PRL459();
                    obj.ReqHoleNo = Convert.ToString(i);
                    obj.CreatedBy = objClsLoginInfo.UserName;
                    obj.CreatedOn = DateTime.Now;
                    objPRL455.PRL459.Add(obj);
                }
                #endregion

                db.PRL455.Add(objPRL455);
                db.SaveChanges();

                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL092
                    PRO455 objPRO455 = db.PRO455.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO455 != null)
                    {
                        CopyProtocolData("PRO455", "PRL455", "PRL455.HeaderId=" + objPRO455.HeaderId + " AND PRO455.HeaderId=" + objPRO455.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");
                        CopyProtocolLinesData("PRO456", "PRL456", "HeaderId=" + objPRO455.HeaderId, "LineId,EditedBy,EditedOn", objPRO455.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO457", "PRL457", "HeaderId=" + objPRO455.HeaderId, "LineId,EditedBy,EditedOn", objPRO455.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO458", "PRL458", "HeaderId=" + objPRO455.HeaderId, "LineId,EditedBy,EditedOn", objPRO455.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO459", "PRL459", "HeaderId=" + objPRO455.HeaderId, "LineId,EditedBy,EditedOn", objPRO455.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                    }
                    #endregion
                }
                returnValue = objPRL455.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_SCREW_PLUG_ACME_HEADER_MACHINING_INSPECTION.GetStringValue())
            {
                #region Add New Entry in PRL460 For PROTOCOL093
                PRL460 objPRL460 = new PRL460();
                objPRL460.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL460.QualityProject = HeaderData.QualityProject;
                objPRL460.EquipmentNo = HeaderData.EquipmentNo;
                objPRL460.DrawingNo = HeaderData.DrawingNo;
                objPRL460.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL460.DCRNo = HeaderData.DCRNo;
                objPRL460.SeamNo = HeaderData.SeamNo;
                objPRL460.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL460.Project = HeaderData.Project;
                objPRL460.BU = HeaderData.BU;
                objPRL460.Location = HeaderData.Location;
                objPRL460.StageCode = HeaderData.StageCode;
                objPRL460.StageSequence = HeaderData.StageSequence;
                objPRL460.ICLRevNo = HeaderData.ICLRevNo;
                objPRL460.CreatedBy = objClsLoginInfo.UserName;
                objPRL460.CreatedOn = DateTime.Now;

                #region  FLANGE & ID MACHINING  | PRL462
                objPRL460.PRL462.Add(new PRL462
                {
                    SrNo = "1",
                    Description = "Identifiaction",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL462.Add(new PRL462
                {
                    SrNo = "2",
                    Description = "Channel header ID",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL462.Add(new PRL462
                {
                    SrNo = "3",
                    Description = "Groove OD",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL462.Add(new PRL462
                {
                    SrNo = "4",
                    Description = "Groove ID",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL462.Add(new PRL462
                {
                    SrNo = "5",
                    Description = "Taper",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL462.Add(new PRL462
                {
                    SrNo = "6",
                    Description = "Radius",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL462.Add(new PRL462
                {
                    SrNo = "7",
                    Description = "Surface finish in groove",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL462.Add(new PRL462
                {
                    SrNo = "8",
                    Description = "ID Finish",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL462.Add(new PRL462
                {
                    SrNo = "9",
                    Description = "Gasket fgroove step face finish",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL462.Add(new PRL462
                {
                    SrNo = "10",
                    Description = "Gasket face to groove depth",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL462.Add(new PRL462
                {
                    SrNo = "11",
                    Description = "Gasket face to Taper length",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL462.Add(new PRL462
                {
                    SrNo = "12",
                    Description = "Open end to Gasket face height",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL462.Add(new PRL462
                {
                    SrNo = "13",
                    Description = "Chanel header ID",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region ACME ID MACHINING | PRL463
                objPRL460.PRL463.Add(new PRL463
                {
                    SrNo = "1",
                    Description = "Identifiaction",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL463.Add(new PRL463
                {
                    SrNo = "2",
                    Description = "Front side relief dia.",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL463.Add(new PRL463
                {
                    SrNo = "3",
                    Description = "Step ID depth",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL463.Add(new PRL463
                {
                    SrNo = "4",
                    Description = "Radius",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL463.Add(new PRL463
                {
                    SrNo = "5",
                    Description = "Back end relief",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL463.Add(new PRL463
                {
                    SrNo = "6",
                    Description = "Relief groove width",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL463.Add(new PRL463
                {
                    SrNo = "7",
                    Description = "Relief groove Distance from front face",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL463.Add(new PRL463
                {
                    SrNo = "8",
                    Description = "Groove both side radius",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL463.Add(new PRL463
                {
                    SrNo = "9",
                    Description = "Step ID",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL463.Add(new PRL463
                {
                    SrNo = "10",
                    Description = "Step ID  Distance from front face",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL463.Add(new PRL463
                {
                    SrNo = "11",
                    Description = "Front corner radius",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL463.Add(new PRL463
                {
                    SrNo = "12",
                    Description = "Step ID depth",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL463.Add(new PRL463
                {
                    SrNo = "13",
                    Description = "End coner radius",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL463.Add(new PRL463
                {
                    SrNo = "14",
                    Description = "Gasket groove depth",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL463.Add(new PRL463
                {
                    SrNo = "15",
                    Description = "Gasket groove ID",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL463.Add(new PRL463
                {
                    SrNo = "16",
                    Description = "Radius",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL463.Add(new PRL463
                {
                    SrNo = "17",
                    Description = "Gasket groove OD",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL463.Add(new PRL463
                {
                    SrNo = "18",
                    Description = "Dia.Ø  depth",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL463.Add(new PRL463
                {
                    SrNo = "19",
                    Description = "Finish",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL463.Add(new PRL463
                {
                    SrNo = "20",
                    Description = "Channel header open end face to serrated gasket",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL463.Add(new PRL463
                {
                    SrNo = "21",
                    Description = "ID",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL463.Add(new PRL463
                {
                    SrNo = "22",
                    Description = "ID Length",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL463.Add(new PRL463
                {
                    SrNo = "23",
                    Description = "Groove ID",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL463.Add(new PRL463
                {
                    SrNo = "24",
                    Description = "Dia.Ø width",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL463.Add(new PRL463
                {
                    SrNo = "25",
                    Description = "Finish",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL463.Add(new PRL463
                {
                    SrNo = "26",
                    Description = "Gasket face finish",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL463.Add(new PRL463
                {
                    SrNo = "27",
                    Description = "Ref.OD up to mm",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region ACME THREADING | PRL464
                objPRL460.PRL464.Add(new PRL464
                {
                    SrNo = "1",
                    Description = "Threading pitch",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL464.Add(new PRL464
                {
                    SrNo = "2",
                    Description = "Angle 29° ACME CLASS 2G STD,ASME B1.5",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL464.Add(new PRL464
                {
                    SrNo = "3",
                    Description = "Threading depth from threading ID",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL464.Add(new PRL464
                {
                    SrNo = "4",
                    Description = "Minor dia.",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL464.Add(new PRL464
                {
                    SrNo = "5",
                    Description = "Pitch circle dia.",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL464.Add(new PRL464
                {
                    SrNo = "6",
                    Description = "Thread length",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL464.Add(new PRL464
                {
                    SrNo = "7",
                    Description = "Thread Go template",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL464.Add(new PRL464
                {
                    SrNo = "8",
                    Description = "Threading Not go template",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region TLR BRACKET | PRL464_2
                objPRL460.PRL464_2.Add(new PRL464_2
                {
                    SrNo = "1",
                    Description = "Bracket height from job center",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL464_2.Add(new PRL464_2
                {
                    SrNo = "2",
                    Description = "Bracket width",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL464_2.Add(new PRL464_2
                {
                    SrNo = "3",
                    Description = "Bracket both side finish",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL464_2.Add(new PRL464_2
                {
                    SrNo = "4",
                    Description = "Bracket Length",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL464_2.Add(new PRL464_2
                {
                    SrNo = "5",
                    Description = "Both side angle",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL464_2.Add(new PRL464_2
                {
                    SrNo = "6",
                    Description = "Channel header open end face to bracket face dis.",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL464_2.Add(new PRL464_2
                {
                    SrNo = "7",
                    Description = "_Nos.  UNC X Deep , Drill hole deep (Part-)",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL464_2.Add(new PRL464_2
                {
                    SrNo = "8",
                    Description = "PCD",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL464_2.Add(new PRL464_2
                {
                    SrNo = "9",
                    Description = "Nos. holes Go, No go",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL464_2.Add(new PRL464_2
                {
                    SrNo = "10",
                    Description = "Distance from front face od to item",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL464_2.Add(new PRL464_2
                {
                    SrNo = "11",
                    Description = "Between two holes pitch",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL460.PRL464_2.Add(new PRL464_2
                {
                    SrNo = "12",
                    Description = "Between two holes pitch",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                db.PRL460.Add(objPRL460);
                db.SaveChanges();

                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL093
                    PRO460 objPRO460 = db.PRO460.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO460 != null)
                    {
                        CopyProtocolData("PRO460", "PRL460", "PRL460.HeaderId=" + objPRO460.HeaderId + " AND PRO460.HeaderId=" + objPRO460.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");
                        CopyProtocolLinesData("PRO461", "PRL461", "HeaderId=" + objPRO460.HeaderId, "LineId,EditedBy,EditedOn", objPRO460.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO462", "PRL462", "HeaderId=" + objPRO460.HeaderId, "LineId,EditedBy,EditedOn", objPRO460.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO463", "PRL463", "HeaderId=" + objPRO460.HeaderId, "LineId,EditedBy,EditedOn", objPRO460.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO464", "PRL464", "HeaderId=" + objPRO460.HeaderId, "LineId,EditedBy,EditedOn", objPRO460.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO464_2", "PRL464_2", "HeaderId=" + objPRO460.HeaderId, "LineId,EditedBy,EditedOn", objPRO460.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                    }
                    #endregion
                }
                returnValue = objPRL460.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.FINAL_ASSEMBLY_INSPECTION_OF_ROUGH_LIQUID_DISTRIBUTION_TRAY.GetStringValue())
            {
                #region Add New Entry in PRL465 For PROTOCOL094
                PRL465 objPRL465 = new PRL465();
                objPRL465.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL465.QualityProject = HeaderData.QualityProject;
                objPRL465.EquipmentNo = HeaderData.EquipmentNo;
                objPRL465.DrawingNo = HeaderData.DrawingNo;
                objPRL465.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL465.DCRNo = HeaderData.DCRNo;
                objPRL465.SeamNo = HeaderData.SeamNo;
                objPRL465.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL465.Project = HeaderData.Project;
                objPRL465.BU = HeaderData.BU;
                objPRL465.Location = HeaderData.Location;
                objPRL465.StageCode = HeaderData.StageCode;
                objPRL465.StageSequence = HeaderData.StageSequence;
                objPRL465.ICLRevNo = HeaderData.ICLRevNo;
                objPRL465.CreatedBy = objClsLoginInfo.UserName;
                objPRL465.CreatedOn = DateTime.Now;

                #region PRO467
                objPRL465.PRL467.Add(new PRL467
                {
                    SrNo = "1",
                    Description = "TRAY OD",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL465.PRL467.Add(new PRL467
                {
                    SrNo = "2",
                    Description = "HEIGHT OF RLDT",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL465.PRL467.Add(new PRL467
                {
                    SrNo = "3",
                    Description = "GAP BETWEEN TRAY PANELS TO PANELS AT SUPPORT BEAM",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL465.PRL467.Add(new PRL467
                {
                    SrNo = "4",
                    Description = "GAP BETWEEN TRAY PANELS TO TRAY PANELS AT BACKING BAR",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL465.PRL467.Add(new PRL467
                {
                    SrNo = "5",
                    Description = "LEVELNESS OF TRAY",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL465.PRL467.Add(new PRL467
                {
                    SrNo = "6",
                    Description = "DISTANCE BETWEEN RLDT BOTTOM SURFACE TO VLDT TOP SURFACE",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL465.PRL467.Add(new PRL467
                {
                    SrNo = "7",
                    Description = "SUPPORT ROD HOLES (QTY)",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL465.PRL467.Add(new PRL467
                {
                    SrNo = "8",
                    Description = "SUPPORT ROD HOLES (DIAMETER & PCD)",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL465.PRL467.Add(new PRL467
                {
                    SrNo = "9",
                    Description = "SUPPORT ROD HOLES (ORIENTATION)",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL465.PRL467.Add(new PRL467
                {
                    SrNo = "10",
                    Description = "HOLES IN TRAY PANELS (DIAMETER)",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL465.PRL467.Add(new PRL467
                {
                    SrNo = "11",
                    Description = "HOLES IN TRAY PANELS (QTY)",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL465.PRL467.Add(new PRL467
                {
                    SrNo = "12",
                    Description = "HOLES IN TRAY PANELS (PITCH)",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL465.PRL467.Add(new PRL467
                {
                    SrNo = "13",
                    Description = "BANDING PLATE HOLE (HOLE TO HOLE DISTANCE)",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL465.PRL467.Add(new PRL467
                {
                    SrNo = "14",
                    Description = "LIFTING LUG (SIZE & DIA)",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL465.PRL467.Add(new PRL467
                {
                    SrNo = "15",
                    Description = "LIFTING LUG (QTY)",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL465.PRL467.Add(new PRL467
                {
                    SrNo = "16",
                    Description = "GAP BETWEEN END OF SPLICE PLATE & BEAM FLANGE",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL465.PRL467.Add(new PRL467
                {
                    SrNo = "17",
                    Description = "VERIFY TRAY PERFORATIONS ARE NOT DIRECTLY ABOVE THE VLDT RISER ASSEMBLY",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL465.PRL467.Add(new PRL467
                {
                    SrNo = "18",
                    Description = "J-CLIPS (DIMENSION & QTY)",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL465.PRL467.Add(new PRL467
                {
                    SrNo = "19",
                    Description = "J-CLIPS (LOCATION)",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL465.PRL467.Add(new PRL467
                {
                    SrNo = "20",
                    Description = "OTHER DIMENSIONS",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL465.PRL467.Add(new PRL467
                {
                    SrNo = "21",
                    Description = "IDENTIFICATION & MATCH MARKING",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL465.PRL467.Add(new PRL467
                {
                    SrNo = "22",
                    Description = "WELD & OVERALL VISUAL",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                db.PRL465.Add(objPRL465);
                db.SaveChanges();

                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL094
                    PRO465 objPRO465 = db.PRO465.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO465 != null)
                    {
                        CopyProtocolData("PRO465", "PRL465", "PRL465.HeaderId=" + objPRO465.HeaderId + " AND PRO470.HeaderId=" + objPRO465.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO466", "PRL466", "HeaderId=" + objPRO465.HeaderId, "LineId,EditedBy,EditedOn", objPRO465.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO467", "PRL467", "HeaderId=" + objPRO465.HeaderId, "LineId,EditedBy,EditedOn", objPRO465.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO468", "PRL468", "HeaderId=" + objPRO465.HeaderId, "LineId,EditedBy,EditedOn", objPRO465.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                    }
                    #endregion
                }
                returnValue = objPRL465.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.FINAL_ASSEMBLY_INSPECTION_OF_VAPOUR_LIQUID_DISTRIBUTION_TRAY.GetStringValue())
            {
                #region Add New Entry in PRL470 For PROTOCOL095
                PRL470 objPRL470 = new PRL470();
                objPRL470.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL470.QualityProject = HeaderData.QualityProject;
                objPRL470.EquipmentNo = HeaderData.EquipmentNo;
                objPRL470.DrawingNo = HeaderData.DrawingNo;
                objPRL470.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL470.DCRNo = HeaderData.DCRNo;
                objPRL470.SeamNo = HeaderData.SeamNo;
                objPRL470.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL470.Project = HeaderData.Project;
                objPRL470.BU = HeaderData.BU;
                objPRL470.Location = HeaderData.Location;
                objPRL470.StageCode = HeaderData.StageCode;
                objPRL470.StageSequence = HeaderData.StageSequence;
                objPRL470.ICLRevNo = HeaderData.ICLRevNo;
                objPRL470.CreatedBy = objClsLoginInfo.UserName;
                objPRL470.CreatedOn = DateTime.Now;

                #region PRO472
                objPRL470.PRL472.Add(new PRL472
                {
                    SrNo = "1",
                    Description = "TRAY OD",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL470.PRL472.Add(new PRL472
                {
                    SrNo = "2",
                    Description = "HEIGHT OF VLDT",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL470.PRL472.Add(new PRL472
                {
                    SrNo = "3",
                    Description = "SUPPORT BEAM TO BEAM DISTANCE",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL470.PRL472.Add(new PRL472
                {
                    SrNo = "4",
                    Description = "GAP BETWEEN TRAY PANELS TO PANELS AT SUPPORT BEAM ",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL470.PRL472.Add(new PRL472
                {
                    SrNo = "5",
                    Description = "GAP BETWEEN TRAY PANELS TO TRAY PANELS AT BACKING BAR",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL470.PRL472.Add(new PRL472
                {
                    SrNo = "6",
                    Description = "LEVELNESS OF TRAY",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL470.PRL472.Add(new PRL472
                {
                    SrNo = "7",
                    Description = "LIFT CHANNEL WIDTH & HEIGHT",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL470.PRL472.Add(new PRL472
                {
                    SrNo = "8",
                    Description = "SUPPORT ROD HOLES (QTY)",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL470.PRL472.Add(new PRL472
                {
                    SrNo = "9",
                    Description = "SUPPORT ROD HOLES (DIAMETER & PCD)",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL470.PRL472.Add(new PRL472
                {
                    SrNo = "10",
                    Description = "SUPPORT ROD HOLES (ORIENTATION)",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL470.PRL472.Add(new PRL472
                {
                    SrNo = "11",
                    Description = "UNIFLOW DISTRIBUTION IN TRAY PANELS (PITCH)",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL470.PRL472.Add(new PRL472
                {
                    SrNo = "12",
                    Description = "UNIFLOW DISTRIBUTION IN TRAY PANELS (QTY)",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL470.PRL472.Add(new PRL472
                {
                    SrNo = "13",
                    Description = "UNIFLOW DISTRIBUTION IN TRAY PANELS (ORIENTATION)",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL470.PRL472.Add(new PRL472
                {
                    SrNo = "14",
                    Description = "UNIFLOW DISTRIBUTION IN TRAY PANELS (VAPOR INLET)",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL470.PRL472.Add(new PRL472
                {
                    SrNo = "15",
                    Description = "UNIFLOW DISTRIBUTION IN TRAY PANELS (TOTAL HEIGHT FROM TRAY)",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL470.PRL472.Add(new PRL472
                {
                    SrNo = "16",
                    Description = "UNIFLOW DISTRIBUTION IN TRAY PANELS (SPRAY DEVICE TO ORIFICE PLATE SPACING)",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL470.PRL472.Add(new PRL472
                {
                    SrNo = "17",
                    Description = "UNIFLOW DISTRIBUTION IN TRAY PANELS (LOCATION)",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL470.PRL472.Add(new PRL472
                {
                    SrNo = "18",
                    Description = "BANDING PLATE SLOT",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL470.PRL472.Add(new PRL472
                {
                    SrNo = "19",
                    Description = "LIFTING LUG  (SIZE & DIA)",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL470.PRL472.Add(new PRL472
                {
                    SrNo = "20",
                    Description = "LIFTING LUG (QTY)",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL470.PRL472.Add(new PRL472
                {
                    SrNo = "21",
                    Description = "VERIFY CLEARANCE BETWEEN OD OF DOWNCOMER AND OUTER EDGE OF BANDING PLATE",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL470.PRL472.Add(new PRL472
                {
                    SrNo = "22",
                    Description = "J-CLIPS (DIMENSION & QTY)",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL470.PRL472.Add(new PRL472
                {
                    SrNo = "23",
                    Description = "J-CLIPS (LOCATION)",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL470.PRL472.Add(new PRL472
                {
                    SrNo = "24",
                    Description = "OTHER DIMENSIONS",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL470.PRL472.Add(new PRL472
                {
                    SrNo = "25",
                    Description = "IDENTIFICATION & MATCH MARKING",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL470.PRL472.Add(new PRL472
                {
                    SrNo = "26",
                    Description = "WELD & OVERALL VISUAL",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                db.PRL470.Add(objPRL470);
                db.SaveChanges();

                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL095
                    PRO470 objPRO470 = db.PRO470.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO470 != null)
                    {
                        CopyProtocolData("PRO470", "PRL470", "PRL470.HeaderId=" + objPRO470.HeaderId + " AND PRO470.HeaderId=" + objPRO470.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO471", "PRL471", "HeaderId=" + objPRO470.HeaderId, "LineId,EditedBy,EditedOn", objPRO470.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO472", "PRL472", "HeaderId=" + objPRO470.HeaderId, "LineId,EditedBy,EditedOn", objPRO470.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO473", "PRL473", "HeaderId=" + objPRO470.HeaderId, "LineId,EditedBy,EditedOn", objPRO470.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                    }
                    #endregion
                }
                returnValue = objPRL470.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.FINAL_DIMENSION_INSPECTION_OF_LATTICE_BEAMS.GetStringValue())
            {
                #region Add New Entry in PRL475 For PROTOCOL096
                PRL475 objPRL475 = new PRL475();
                objPRL475.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL475.QualityProject = HeaderData.QualityProject;
                objPRL475.EquipmentNo = HeaderData.EquipmentNo;
                objPRL475.DrawingNo = HeaderData.DrawingNo;
                objPRL475.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL475.DCRNo = HeaderData.DCRNo;
                objPRL475.SeamNo = HeaderData.SeamNo;
                objPRL475.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL475.Project = HeaderData.Project;
                objPRL475.BU = HeaderData.BU;
                objPRL475.Location = HeaderData.Location;
                objPRL475.StageCode = HeaderData.StageCode;
                objPRL475.StageSequence = HeaderData.StageSequence;
                objPRL475.ICLRevNo = HeaderData.ICLRevNo;
                objPRL475.CreatedBy = objClsLoginInfo.UserName;
                objPRL475.CreatedOn = DateTime.Now;

                #region PRO477
                objPRL475.PRL477.Add(new PRL477
                {
                    SrNo = "1",
                    Description = "TOP FLANGE (Length)",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL475.PRL477.Add(new PRL477
                {
                    SrNo = "2",
                    Description = "TOP FLANGE (Width)",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL475.PRL477.Add(new PRL477
                {
                    SrNo = "3",
                    Description = "TOP FLANGE (Thickness)",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL475.PRL477.Add(new PRL477
                {
                    SrNo = "4",
                    Description = "TOP FLANGE (Notch)",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL475.PRL477.Add(new PRL477
                {
                    SrNo = "5",
                    Description = "BOTTOM FLANGE (Length)",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL475.PRL477.Add(new PRL477
                {
                    SrNo = "6",
                    Description = "BOTTOM FLANGE (Width)",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL475.PRL477.Add(new PRL477
                {
                    SrNo = "7",
                    Description = "BOTTOM FLANGE (Thickness)",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL475.PRL477.Add(new PRL477
                {
                    SrNo = "8",
                    Description = "BEAM HEIGHT",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL475.PRL477.Add(new PRL477
                {
                    SrNo = "9",
                    Description = "WEB (Length)",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL475.PRL477.Add(new PRL477
                {
                    SrNo = "10",
                    Description = "WEB (Thickness)",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL475.PRL477.Add(new PRL477
                {
                    SrNo = "11",
                    Description = "SLOTS IN WEB (Slot Qty)",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL475.PRL477.Add(new PRL477
                {
                    SrNo = "12",
                    Description = "SLOTS IN WEB (Distance from CL)",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL475.PRL477.Add(new PRL477
                {
                    SrNo = "13",
                    Description = "SLOTS IN WEB (Elevation from bottom flange)",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL475.PRL477.Add(new PRL477
                {
                    SrNo = "14",
                    Description = "SLOTS IN WEB (Distance between slots)",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL475.PRL477.Add(new PRL477
                {
                    SrNo = "15",
                    Description = "TRIANGULAR CUTOUTS IN WEB (Dimension)",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL475.PRL477.Add(new PRL477
                {
                    SrNo = "16",
                    Description = "TRIANGULAR CUTOUTS IN WEB (Pitch)",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL475.PRL477.Add(new PRL477
                {
                    SrNo = "17",
                    Description = "VENT HOLES IN WEB (Diameter)",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL475.PRL477.Add(new PRL477
                {
                    SrNo = "18",
                    Description = "VENT HOLES IN WEB (Qty)",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL475.PRL477.Add(new PRL477
                {
                    SrNo = "19",
                    Description = "VENT HOLES IN WEB (Location)",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL475.PRL477.Add(new PRL477
                {
                    SrNo = "20",
                    Description = "CAMBER",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL475.PRL477.Add(new PRL477
                {
                    SrNo = "21",
                    Description = "SWEEP",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL475.PRL477.Add(new PRL477
                {
                    SrNo = "22",
                    Description = "LEVLNESS OF EACH SEATING SURFACE",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL475.PRL477.Add(new PRL477
                {
                    SrNo = "23",
                    Description = "MAXIMUM OUT OF SQUARENESS OF TOP FLANGE ACROSS WIDTH",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                db.PRL475.Add(objPRL475);
                db.SaveChanges();

                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL096
                    PRO475 objPRO475 = db.PRO475.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO475 != null)
                    {
                        CopyProtocolData("PRO475", "PRL475", "PRL475.HeaderId=" + objPRO475.HeaderId + " AND PRO475.HeaderId=" + objPRO475.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO476", "PRL476", "HeaderId=" + objPRO475.HeaderId, "LineId,EditedBy,EditedOn", objPRO475.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO477", "PRL477", "HeaderId=" + objPRO475.HeaderId, "LineId,EditedBy,EditedOn", objPRO475.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO478", "PRL478", "HeaderId=" + objPRO475.HeaderId, "LineId,EditedBy,EditedOn", objPRO475.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                    }
                    #endregion
                }
                returnValue = objPRL475.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.FINAL_DIMENSION_AND_VISUAL_INSPECTION_REPORT.GetStringValue())
            {
                #region Add New Entry in PRL480 For PROTOCOL098
                PRL480 objPRL480 = new PRL480();
                objPRL480.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRL480.QualityProject = HeaderData.QualityProject;
                objPRL480.EquipmentNo = HeaderData.EquipmentNo;
                objPRL480.DrawingNo = HeaderData.DrawingNo;
                objPRL480.DrawingRevisionNo = HeaderData.DrawingRevisionNo;
                objPRL480.DCRNo = HeaderData.DCRNo;
                objPRL480.SeamNo = HeaderData.SeamNo;
                objPRL480.InspectionAgency = GetInspectionAgency(HeaderData.QualityProject);
                objPRL480.Project = HeaderData.Project;
                objPRL480.BU = HeaderData.BU;
                objPRL480.Location = HeaderData.Location;
                //objPRL480.StageCode = HeaderData.StageCode;
                objPRL480.StageSequence = HeaderData.StageSequence;
                objPRL480.ICLRevNo = HeaderData.ICLRevNo;

                //   objPRL480.LTProjectNo = string.Empty;
                objPRL480.CreatedBy = objClsLoginInfo.UserName;
                objPRL480.CreatedOn = DateTime.Now;
                #region  CIRCUMFERENCE MEASUREMENT
                objPRL480.PRL482.Add(new PRL482
                {
                    Location = "SHELL-1",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                objPRL480.PRL482.Add(new PRL482
                {
                    Location = "SHELL-2",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL480.PRL482.Add(new PRL482
                {
                    Location = "SHELL-3",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL480.PRL482.Add(new PRL482
                {
                    Location = "SHELL-4",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL480.PRL482.Add(new PRL482
                {
                    Location = "SHELL-5",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL480.PRL482.Add(new PRL482
                {
                    Location = "SHELL-6",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL480.PRL482.Add(new PRL482
                {
                    Location = "SHELL-7",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL480.PRL482.Add(new PRL482
                {
                    Location = "SHELL-8",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL480.PRL482.Add(new PRL482
                {
                    Location = "SHELL-9",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL480.PRL482.Add(new PRL482
                {
                    Location = "SHELL-10",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL480.PRL482.Add(new PRL482
                {
                    Location = "SHELL-11",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL480.PRL482.Add(new PRL482
                {
                    Location = "SHELL-12",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL480.PRL482.Add(new PRL482
                {
                    Location = "SHELL-13",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL480.PRL482.Add(new PRL482
                {
                    Location = "SHELL-14",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL480.PRL482.Add(new PRL482
                {
                    Location = "SHELL-15",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL480.PRL482.Add(new PRL482
                {
                    Location = "SHELL-16",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL480.PRL482.Add(new PRL482
                {
                    Location = "SHELL-17",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL480.PRL482.Add(new PRL482
                {
                    Location = "SHELL-18",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL480.PRL482.Add(new PRL482
                {
                    Location = "SHELL-19",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL480.PRL482.Add(new PRL482
                {
                    Location = "SHELL-20",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL480.PRL482.Add(new PRL482
                {
                    Location = "SHELL-21",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region LENGTH DIMENSION
                objPRL480.PRL483.Add(new PRL483
                {
                    Orientation = "AT 0°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL480.PRL483.Add(new PRL483
                {
                    Orientation = "AT 45°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL480.PRL483.Add(new PRL483
                {
                    Orientation = "AT 90°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL480.PRL483.Add(new PRL483
                {
                    Orientation = "AT 135°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL480.PRL483.Add(new PRL483
                {
                    Orientation = "AT 180°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL480.PRL483.Add(new PRL483
                {
                    Orientation = "AT 225°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL480.PRL483.Add(new PRL483
                {
                    Orientation = "AT 270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL480.PRL483.Add(new PRL483
                {
                    Orientation = "AT 315°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region Notes

                objPRL480.PRL484.Add(new PRL484 { NotesType = "LENGTH DIMENSION", Notes = "1) ALL DIMENSIONS ARE IN 'MM' UNLESS SPECIFIED.", CreatedBy = objClsLoginInfo.UserName, CreatedOn = DateTime.Now });
                objPRL480.PRL484.Add(new PRL484 { NotesType = "LENGTH DIMENSION", Notes = "2) DIMENSIONS OF OPEN END WEP CHECKED &FOUND SATISFACTORY.", CreatedBy = objClsLoginInfo.UserName, CreatedOn = DateTime.Now });
                objPRL480.PRL484.Add(new PRL484 { NotesType = "LENGTH DIMENSION", Notes = "3) FINAL VISUAL INSPECTION OF INSIDE & OUTSIDE SURFACE AND ALL WELDS CARRIED OUT AND FOUND SATISFACTORY.", CreatedBy = objClsLoginInfo.UserName, CreatedOn = DateTime.Now });
                objPRL480.PRL484.Add(new PRL484 { NotesType = "LENGTH DIMENSION", Notes = "4) MEASURING AND TESTING EQUIPMENT SERIAL NUMBER &CALIBRATION DUE DATE :", CreatedBy = objClsLoginInfo.UserName, CreatedOn = DateTime.Now });
                objPRL480.PRL484.Add(new PRL484 { NotesType = "LENGTH DIMENSION", Notes = "5) FINAL VISUAL INSPECTION OF SEAM AND SURFACE IS CHECKED AND FOUND SATISFACTORY.", CreatedBy = objClsLoginInfo.UserName, CreatedOn = DateTime.Now });
                objPRL480.PRL484.Add(new PRL484 { NotesType = "LENGTH DIMENSION", Notes = "6) REINFORCEMENT OF WELD JOINTS IS CHECKED AND FOUND WITHIN LIMITS AS PER TABLE 6.6 OF ASME SEC VIII DIV 2.", CreatedBy = objClsLoginInfo.UserName, CreatedOn = DateTime.Now });
                objPRL480.PRL484.Add(new PRL484 { NotesType = "LENGTH DIMENSION", Notes = "7) OFFSET IN BUTT WELD JOINTS IS CHECKED AND FOUND WITHIN LIMITS AS PER TABLE 6.4 OF ASME SEC VIII DIV 2.", CreatedBy = objClsLoginInfo.UserName, CreatedOn = DateTime.Now });
                objPRL480.PRL484.Add(new PRL484 { NotesType = "LENGTH DIMENSION", Notes = "8) MAXIMUM DEVIATION FROM TRUE CIRCULAR FORM 'e' ON SHELL CHECKED WITH TEMPLATE HAVING CHORD LENGTH OF____MM AND FOUND ____MM MAXIMUM(ALLOWED ___MM MAX.).", CreatedBy = objClsLoginInfo.UserName, CreatedOn = DateTime.Now });
                objPRL480.PRL484.Add(new PRL484 { NotesType = "LENGTH DIMENSION", Notes = "9) MAXIMUM DEVIATION FROM TRUE CIRCULAR FORM 'e' IN HEAD CHECKED WITH TEMPLATE HAVING CHORD LENGTH OF ____MM AND FOUND____MM MAXIMUM(ALLOWED ___MM MAX.).", CreatedBy = objClsLoginInfo.UserName, CreatedOn = DateTime.Now });
                objPRL480.PRL484.Add(new PRL484 { NotesType = "LENGTH DIMENSION", Notes = "10) MAXIMUM PEAKING HEIGHT(DP) AT CATEGORY 'A' JOINTS IN HEAD CHEKED WITH TEMPLATE HAVING CHORD LENGTH OF _____MM AND FOUND______MM MAXIMUM.", CreatedBy = objClsLoginInfo.UserName, CreatedOn = DateTime.Now });
                objPRL480.PRL484.Add(new PRL484 { NotesType = "LENGTH DIMENSION", Notes = "11) OUT OF ROUNDNESS OF SHELL CHECKED AND FOUND _____MM MAXIMUM.,  this remarks shall be editable at time of ICL approval also and at time of Atending entry also", CreatedBy = objClsLoginInfo.UserName, CreatedOn = DateTime.Now });
                #endregion

                /*

                #region DIMENSION OF WELDED TRAY SUPPORT RING 
                objPRL480.PRL484_6.Add(new PRL484_6
                {
                    TsrOrientation = "0°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL480.PRL484_6.Add(new PRL484_6
                {
                    TsrOrientation = "30°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL480.PRL484_6.Add(new PRL484_6
                {
                    TsrOrientation = "60°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL480.PRL484_6.Add(new PRL484_6
                {
                    TsrOrientation = "90°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL480.PRL484_6.Add(new PRL484_6
                {
                    TsrOrientation = "120°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL480.PRL484_6.Add(new PRL484_6
                {
                    TsrOrientation = "150°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL480.PRL484_6.Add(new PRL484_6
                {
                    TsrOrientation = "180°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL480.PRL484_6.Add(new PRL484_6
                {
                    TsrOrientation = "210°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL480.PRL484_6.Add(new PRL484_6
                {
                    TsrOrientation = "240°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL480.PRL484_6.Add(new PRL484_6
                {
                    TsrOrientation = "270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL480.PRL484_6.Add(new PRL484_6
                {
                    TsrOrientation = "300°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL480.PRL484_6.Add(new PRL484_6
                {
                    TsrOrientation = "330°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion
                */

                #region OUTOFROUNDNESS

                objPRL480.OutOfRoundnessAt0 = "0°-180°";
                objPRL480.OutOfRoundnessAt22 = "22.5°-202.5°";
                objPRL480.OutOfRoundnessAt45 = "45°-225°";
                objPRL480.OutOfRoundnessAt67 = "67.5°-247.5°";
                objPRL480.OutOfRoundnessAt90 = "90°-270°";
                objPRL480.OutOfRoundnessAt112 = "112.5°-292.5°";
                objPRL480.OutOfRoundnessAt135 = "135°-315°";
                objPRL480.OutOfRoundnessAt157 = "157.5°-337.5°";

                #endregion



                #region DIMENSION OF NUB ON BOTTOM HEAD
                objPRL480.PRL484_7.Add(new PRL484_7
                {
                    Orientation = "0°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL480.PRL484_7.Add(new PRL484_7
                {
                    Orientation = "45°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL480.PRL484_7.Add(new PRL484_7
                {
                    Orientation = "90°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL480.PRL484_7.Add(new PRL484_7
                {
                    Orientation = "135°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL480.PRL484_7.Add(new PRL484_7
                {
                    Orientation = "180°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL480.PRL484_7.Add(new PRL484_7
                {
                    Orientation = "225°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL480.PRL484_7.Add(new PRL484_7
                {
                    Orientation = "270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRL480.PRL484_7.Add(new PRL484_7
                {
                    Orientation = "315°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                db.PRL480.Add(objPRL480);
                db.SaveChanges();
                #endregion

                if (ProtocolHeaderId.HasValue)
                {
                    #region Copy All Data from PRO to PRL For PROTOCOL098
                    PRO480 objPRO480 = db.PRO480.Where(c => c.HeaderId == ProtocolHeaderId).FirstOrDefault();
                    if (objPRO480 != null)
                    {
                        CopyProtocolData("PRO480", "PRL480", "PRL480.HeaderId=" + objPRO480.HeaderId + " AND PRO480.HeaderId=" + objPRO480.HeaderId, "HeaderId,CreatedBy,CreatedOn,EditedBy,EditedOn");

                        CopyProtocolLinesData("PRO476", "PRL476", "HeaderId=" + objPRO480.HeaderId, "LineId,EditedBy,EditedOn", objPRO480.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO477", "PRL477", "HeaderId=" + objPRO480.HeaderId, "LineId,EditedBy,EditedOn", objPRO480.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO478", "PRL478", "HeaderId=" + objPRO480.HeaderId, "LineId,EditedBy,EditedOn", objPRO480.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO484_1", "PRL484_1", "HeaderId=" + objPRO480.HeaderId, "LineId,EditedBy,EditedOn", objPRO480.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO484_2", "PRL484_2", "HeaderId=" + objPRO480.HeaderId, "LineId,EditedBy,EditedOn", objPRO480.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO484_3", "PRL484_3", "HeaderId=" + objPRO480.HeaderId, "LineId,EditedBy,EditedOn", objPRO480.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO484_4", "PRL484_4", "HeaderId=" + objPRO480.HeaderId, "LineId,EditedBy,EditedOn", objPRO480.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO484_5", "PRL484_5", "HeaderId=" + objPRO480.HeaderId, "LineId,EditedBy,EditedOn", objPRO480.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO484_5_1", "PRL484_5_1", "HeaderId=" + objPRO480.HeaderId, "LineId,EditedBy,EditedOn", objPRO480.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO484_6", "PRL484_6", "HeaderId=" + objPRO480.HeaderId, "LineId,EditedBy,EditedOn", objPRO480.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO484_6_1", "PRL484_6_1", "HeaderId=" + objPRO480.HeaderId, "LineId,EditedBy,EditedOn", objPRO480.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO484_7", "PRL484_7", "HeaderId=" + objPRO480.HeaderId, "LineId,EditedBy,EditedOn", objPRO480.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO484_8", "PRL484_8", "HeaderId=" + objPRO480.HeaderId, "LineId,EditedBy,EditedOn", objPRO480.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO484_9", "PRL484_9", "HeaderId=" + objPRO480.HeaderId, "LineId,EditedBy,EditedOn", objPRO480.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO484_10", "PRL484_10", "HeaderId=" + objPRO480.HeaderId, "LineId,EditedBy,EditedOn", objPRO480.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO484_11", "PRL484_11", "HeaderId=" + objPRO480.HeaderId, "LineId,EditedBy,EditedOn", objPRO480.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO484_11_1", "PRL484_11_1", "HeaderId=" + objPRO480.HeaderId, "LineId,EditedBy,EditedOn", objPRO480.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);
                        CopyProtocolLinesData("PRO484_12", "PRL484_12", "HeaderId=" + objPRO480.HeaderId, "LineId,EditedBy,EditedOn", objPRO480.HeaderId, objClsLoginInfo.UserName, ProtocolType, role);


                    }
                    #endregion
                }
                returnValue = objPRL480.HeaderId;
            }
            return returnValue;
        }


        // #NewProtocolLinkage
        public bool DelinkProtocolWithStage(int ProtocolHeaderId, string ProtocolType)
        {
            try
            {
                if (ProtocolType == clsImplementationEnum.ProtocolType.SETUP_AND_DIMENSION_REPORT_FOR_ROLLED_SHELL_LONGITUDINAL_SEAM.GetStringValue())
                {
                    #region Delink with PRL001 header and its lines data For PROTOCOL001

                    PRL001 objPRL001 = db.PRL001.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL001 != null)
                    {
                        if (objPRL001.PRL002.Any())
                        {
                            db.PRL002.RemoveRange(objPRL001.PRL002.ToList());
                        }
                        if (objPRL001.PRL003.Any())
                        {
                            db.PRL003.RemoveRange(objPRL001.PRL003.ToList());
                        }
                        db.PRL001.Remove(objPRL001);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.SETUP_AND_DIMENSION_REPORT_FOR_CIRCUMFERENTIAL_SEAM.GetStringValue())
                {
                    #region Delink with PRL005 header and its lines data For PROTOCOL002

                    PRL005 objPRL005 = db.PRL005.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL005 != null)
                    {
                        if (objPRL005.PRL006.Any())
                        {
                            db.PRL006.RemoveRange(objPRL005.PRL006.ToList());
                        }
                        if (objPRL005.PRL007.Any())
                        {
                            db.PRL007.RemoveRange(objPRL005.PRL007.ToList());
                        }
                        if (objPRL005.PRL008.Any())
                        {
                            db.PRL008.RemoveRange(objPRL005.PRL008.ToList());
                        }
                        if (objPRL005.PRL009_1.Any())
                        {
                            db.PRL009_1.RemoveRange(objPRL005.PRL009_1.ToList());
                        }
                        if (objPRL005.PRL009_2.Any())
                        {
                            db.PRL009_2.RemoveRange(objPRL005.PRL009_2.ToList());
                        }
                        db.PRL005.Remove(objPRL005);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.SETUP_INSPECTION_REPORT_FOR_FORMED.GetStringValue())
                {
                    #region Delink with PRL010 header and its lines data For PROTOCOL003

                    PRL010 objPRL010 = db.PRL010.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL010 != null)
                    {
                        if (objPRL010.PRL011.Any())
                        {
                            db.PRL011.RemoveRange(objPRL010.PRL011.ToList());
                        }
                        if (objPRL010.PRL012.Any())
                        {
                            db.PRL012.RemoveRange(objPRL010.PRL012.ToList());
                        }
                        if (objPRL010.PRL013.Any())
                        {
                            db.PRL013.RemoveRange(objPRL010.PRL013.ToList());
                        }
                        if (objPRL010.PRL014_1.Any())
                        {
                            db.PRL014_1.RemoveRange(objPRL010.PRL014_1.ToList());
                        }
                        if (objPRL010.PRL014_2.Any())
                        {
                            db.PRL014_2.RemoveRange(objPRL010.PRL014_2.ToList());
                        }
                        db.PRL010.Remove(objPRL010);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.SETUP_AND_DIMENSION_REPORT_FOR_NOZZLE.GetStringValue())
                {
                    #region Delink with PRL015 header and its lines data For PROTOCOL004

                    PRL015 objPRL015 = db.PRL015.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL015 != null)
                    {
                        db.PRL015.Remove(objPRL015);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_REPORT_FOR_ROLLED_SHELL_LONGITUDINAL_SEAM.GetStringValue())
                {
                    #region Delink with PRL020 header and its lines data For PROTOCOL005

                    PRL020 objPRL020 = db.PRL020.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL020 != null)
                    {
                        if (objPRL020.PRL021.Any())
                        {
                            db.PRL021.RemoveRange(objPRL020.PRL021.ToList());
                        }
                        if (objPRL020.PRL022.Any())
                        {
                            db.PRL022.RemoveRange(objPRL020.PRL022.ToList());
                        }
                        if (objPRL020.PRL023.Any())
                        {
                            db.PRL023.RemoveRange(objPRL020.PRL023.ToList());
                        }
                        db.PRL020.Remove(objPRL020);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_INSPECTION_REPORT_FOR_FORMED_D_END_CONE.GetStringValue())
                {
                    #region Delink with PRL025 header and its lines data For PROTOCOL006

                    PRL025 objPRL025 = db.PRL025.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL025 != null)
                    {
                        if (objPRL025.PRL026.Any())
                        {
                            db.PRL026.RemoveRange(objPRL025.PRL026.ToList());
                        }
                        if (objPRL025.PRL027.Any())
                        {
                            db.PRL027.RemoveRange(objPRL025.PRL027.ToList());
                        }
                        if (objPRL025.PRL028.Any())
                        {
                            db.PRL028.RemoveRange(objPRL025.PRL028.ToList());
                        }
                        if (objPRL025.PRL029.Any())
                        {
                            db.PRL029.RemoveRange(objPRL025.PRL029.ToList());
                        }
                        db.PRL025.Remove(objPRL025);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.WELD_VISUAL_AND_DIMENSION_REPORT_FOR_NOZZLE.GetStringValue())
                {
                    #region Delink with PRL030 header and its lines data For PROTOCOL007

                    PRL030 objPRL030 = db.PRL030.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL030 != null)
                    {
                        db.PRL030.Remove(objPRL030);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORT_FOR_SQUARE_TYPE.GetStringValue())
                {
                    #region Delink with PRL035 header and its lines data For PROTOCOL008

                    PRL035 objPRL035 = db.PRL035.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL035 != null)
                    {
                        db.PRL035.Remove(objPRL035);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_REPORT_FOR_CIRCUMFERENTIAL_SEAM.GetStringValue())
                {
                    #region Delink with PRL040 header and its lines data For PROTOCOL009

                    PRL040 objPRL040 = db.PRL040.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL040 != null)
                    {
                        if (objPRL040.PRL041.Any())
                        {
                            db.PRL041.RemoveRange(objPRL040.PRL041.ToList());
                        }
                        if (objPRL040.PRL042.Any())
                        {
                            db.PRL042.RemoveRange(objPRL040.PRL042.ToList());
                        }
                        if (objPRL040.PRL043.Any())
                        {
                            db.PRL043.RemoveRange(objPRL040.PRL043.ToList());
                        }
                        if (objPRL040.PRL044_1.Any())
                        {
                            db.PRL044_1.RemoveRange(objPRL040.PRL044_1.ToList());
                        }
                        if (objPRL040.PRL044_2.Any())
                        {
                            db.PRL044_2.RemoveRange(objPRL040.PRL044_2.ToList());
                        }
                        db.PRL040.Remove(objPRL040);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.SETUP_AND_DIMENSION_REPORT_FOR_FORMED_ELBOW_LONG_SEAMS.GetStringValue())
                {
                    #region Delink with PRL045 header and its lines data For PROTOCOL010

                    PRL045 objPRL045 = db.PRL045.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL045 != null)
                    {
                        db.PRL045.Remove(objPRL045);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.WELD_VISUAL_AND_DIMENSION_REPORT_FOR_SPOOL_TYPE1.GetStringValue())
                {
                    #region Delink with PRL050 header and its lines data For PROTOCOL011

                    PRL050 objPRL050 = db.PRL050.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL050 != null)
                    {
                        db.PRL050.Remove(objPRL050);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.WELD_VISUAL_AND_DIMENSION_REPORT_FOR_SPOOL_TYPE2.GetStringValue())
                {
                    #region Delink with PRL055 header and its lines data For PROTOCOL012

                    PRL055 objPRL055 = db.PRL055.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL055 != null)
                    {
                        db.PRL055.Remove(objPRL055);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue())
                {
                    #region Delink with PRL060 header and its lines data For PROTOCOL013

                    PRL060 objPRL060 = db.PRL060.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL060 != null)
                    {
                        if (objPRL060.PRL061.Any())
                        {
                            db.PRL061.RemoveRange(objPRL060.PRL061.ToList());
                        }
                        db.PRL060.Remove(objPRL060);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_BEFORE_OVERLAY_FOR_SQUARE_TYPE_NUB.GetStringValue())
                {
                    #region Delink with PRL065 header and its lines data For PROTOCOL014

                    PRL065 objPRL065 = db.PRL065.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL065 != null)
                    {
                        db.PRL065.Remove(objPRL065);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_AFTER_OVERLAY_FOR_TAPER_TYPE_NUB.GetStringValue())
                {
                    #region Delink with PRL070 header and its lines data For PROTOCOL015

                    PRL070 objPRL070 = db.PRL070.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL070 != null)
                    {
                        db.PRL070.Remove(objPRL070);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.TSR_DIMENSION_INSPECTION_REPORTS.GetStringValue())
                {
                    #region Delink with PRL075 header and its lines data For PROTOCOL016

                    PRL075 objPRL075 = db.PRL075.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL075 != null)
                    {
                        db.PRL075.Remove(objPRL075);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.WELD_VISUAL_AND_DIMENSION_INSPECTION_REPORT_FOR_SUPPORT_BRACKET.GetStringValue())
                {
                    #region Delink with PRL080 header and its lines data For PROTOCOL017

                    PRL080 objPRL080 = db.PRL080.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL080 != null)
                    {
                        if (objPRL080.PRL081.Any())
                        {
                            db.PRL081.RemoveRange(objPRL080.PRL081.ToList());
                        }
                        if (objPRL080.PRL082.Any())
                        {
                            db.PRL082.RemoveRange(objPRL080.PRL082.ToList());
                        }
                        db.PRL080.Remove(objPRL080);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.RECORD_FOR_TEMPORARY_ATTACHMENTS_LOCATION.GetStringValue())
                {
                    #region Delink with PRL085 header and its lines data For PROTOCOL018

                    PRL085 objPRL085 = db.PRL085.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL085 != null)
                    {
                        if (objPRL085.PRL086.Any())
                        {
                            db.PRL086.RemoveRange(objPRL085.PRL086.ToList());
                        }
                        db.PRL085.Remove(objPRL085);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_INSPECTION_OF_SKIRT_TEMPLATE.GetStringValue())
                {
                    #region Delink with PRL090 header and its lines data For PROTOCOL019

                    PRL090 objPRL090 = db.PRL090.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL090 != null)
                    {
                        db.PRL090.Remove(objPRL090);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_INSPECTION_OF_SKIRT_ASSEMBLY.GetStringValue())
                {
                    #region Delink with PRL095 header and its lines data For PROTOCOL020

                    PRL095 objPRL095 = db.PRL095.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL095 != null)
                    {
                        if (objPRL095.PRL096.Any())
                        {
                            db.PRL096.RemoveRange(objPRL095.PRL096.ToList());
                        }
                        if (objPRL095.PRL097.Any())
                        {
                            db.PRL097.RemoveRange(objPRL095.PRL097.ToList());
                        }
                        if (objPRL095.PRL098.Any())
                        {
                            db.PRL098.RemoveRange(objPRL095.PRL098.ToList());
                        }
                        if (objPRL095.PRL099.Any())
                        {
                            db.PRL099.RemoveRange(objPRL095.PRL099.ToList());
                        }
                        db.PRL095.Remove(objPRL095);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_OF_HARDNESS_TEST.GetStringValue())
                {
                    #region Delink with PRL100 header and its lines data For PROTOCOL021

                    PRL100 objPRL100 = db.PRL100.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL100 != null)
                    {
                        if (objPRL100.PRL101.Any())
                        {
                            db.PRL101.RemoveRange(objPRL100.PRL101.ToList());
                        }
                        db.PRL100.Remove(objPRL100);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.AIR_TEST_OF_REINFORCEMENT_PADS_OF_NOZZLE.GetStringValue())
                {
                    #region Delink with PRL105 header and its lines data For PROTOCOL022

                    PRL105 objPRL105 = db.PRL105.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL105 != null)
                    {
                        if (objPRL105.PRL106.Any())
                        {
                            db.PRL106.RemoveRange(objPRL105.PRL106.ToList());
                        }
                        db.PRL105.Remove(objPRL105);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.HELIUM_LEAK_TEST_REPORT.GetStringValue())
                {
                    #region Delink with PRL110 header and its lines data For PROTOCOL023

                    PRL110 objPRL110 = db.PRL110.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL110 != null)
                    {
                        if (objPRL110.PRL111.Any())
                        {
                            db.PRL111.RemoveRange(objPRL110.PRL111.ToList());
                        }
                        db.PRL110.Remove(objPRL110);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.HYDROSTATIC_TEST_REPORT.GetStringValue())
                {
                    #region Delink with PRL115 header and its lines data For PROTOCOL024

                    PRL115 objPRL115 = db.PRL115.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL115 != null)
                    {
                        if (objPRL115.PRL116.Any())
                        {
                            db.PRL116.RemoveRange(objPRL115.PRL116.ToList());
                        }
                        db.PRL115.Remove(objPRL115);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.PNEUMATIC_TEST_REPORT.GetStringValue())
                {
                    #region Delink with PRL120 header and its lines data For PROTOCOL025

                    PRL120 objPRL120 = db.PRL120.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL120 != null)
                    {
                        if (objPRL120.PRL121.Any())
                        {
                            db.PRL121.RemoveRange(objPRL120.PRL121.ToList());
                        }
                        db.PRL120.Remove(objPRL120);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_N2_FILLING_INSPECTION.GetStringValue())
                {
                    #region Delink with PRL125 header and its lines data For PROTOCOL026

                    PRL125 objPRL125 = db.PRL125.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL125 != null)
                    {
                        db.PRL125.Remove(objPRL125);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_POSITIVE_MATERIAL_IDENTIFICATION.GetStringValue())
                {
                    #region Delink with PRL130 header and its lines data For PROTOCOL027

                    PRL130 objPRL130 = db.PRL130.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL130 != null)
                    {
                        if (objPRL130.PRL131.Any())
                        {
                            db.PRL131.RemoveRange(objPRL130.PRL131.ToList());
                        }
                        db.PRL130.Remove(objPRL130);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_OF_FINAL_MACHINING_INSPECTION_OF_TUBESHEET.GetStringValue())
                {
                    #region Delink with PRL135 header and its lines data For PROTOCOL028

                    PRL135 objPRL135 = db.PRL135.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL135 != null)
                    {
                        if (objPRL135.PRL136.Any())
                        {
                            db.PRL136.RemoveRange(objPRL135.PRL136.ToList());
                        }
                        db.PRL135.Remove(objPRL135);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_OF_ARM_REVIEW.GetStringValue())
                {
                    #region Delink with PRL140 header and its lines data For PROTOCOL029

                    PRL140 objPRL140 = db.PRL140.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL140 != null)
                    {
                        if (objPRL140.PRL141.Any())
                        {
                            db.PRL141.RemoveRange(objPRL140.PRL141.ToList());
                        }
                        db.PRL140.Remove(objPRL140);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_INSPECTION_REPORT_FOR_OVERLAY_ON_SHELL_DEND.GetStringValue())
                {
                    #region Delink with PRL145 header and its lines data For PROTOCOL030

                    PRL145 objPRL145 = db.PRL145.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL145 != null)
                    {
                        db.PRL145.Remove(objPRL145);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_VISUAL_AND_DIM_OF_TAILING_LUG.GetStringValue())
                {
                    #region Delink with PRL150 header and its lines data For PROTOCOL031

                    PRL150 objPRL150 = db.PRL150.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL150 != null)
                    {
                        if (objPRL150.PRL151.Any())
                        {
                            db.PRL151.RemoveRange(objPRL150.PRL151.ToList());
                        }
                        db.PRL150.Remove(objPRL150);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.OVERLAY_THICKNESS_MEASUREMENT_REPORT_FOR_SHELL_DEND_LONG_SEAM_CIRC_SEAM.GetStringValue())
                {
                    #region Delink with PRL155 header and its lines data For PROTOCOL032

                    PRL155 objPRL155 = db.PRL155.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL155 != null)
                    {
                        if (objPRL155.PRL156.Any())
                        {
                            db.PRL156.RemoveRange(objPRL155.PRL156.ToList());
                        }
                        db.PRL155.Remove(objPRL155);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.OVERLAY_THICKNESS_MEASUREMENT_REPORT_FOR_NOZZLE_PIPE_ELBOW.GetStringValue())
                {
                    #region Delink with PRL160 header and its lines data For PROTOCOL033

                    PRL160 objPRL160 = db.PRL160.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL160 != null)
                    {
                        if (objPRL160.PRL161.Any())
                        {
                            db.PRL161.RemoveRange(objPRL160.PRL161.ToList());
                        }
                        db.PRL160.Remove(objPRL160);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.THICKNESS_REPORT_FOR_EQUIPMENT.GetStringValue())
                {
                    #region Delink with PRL165 header and its lines data For PROTOCOL034

                    PRL165 objPRL165 = db.PRL165.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL165 != null)
                    {
                        if (objPRL165.PRL166.Any())
                        {
                            db.PRL166.RemoveRange(objPRL165.PRL166.ToList());
                        }
                        db.PRL165.Remove(objPRL165);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.NOZZLE_CUTOUT_MARKING_ON_SHELL_CONE_DEND.GetStringValue())
                {
                    #region Delink with PRL170 header and its lines data For PROTOCOL035

                    PRL170 objPRL170 = db.PRL170.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL170 != null)
                    {
                        if (objPRL170.PRL171.Any())
                        {
                            db.PRL171.RemoveRange(objPRL170.PRL171.ToList());
                        }
                        db.PRL170.Remove(objPRL170);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.SETUP_AND_DIMENSION_REPORT_FOR_ROLLED_SHELL_LONGITUDINAL_SEAM_V2.GetStringValue())
                {
                    #region Delink with PRL175 header and its lines data For PROTOCOL036

                    PRL175 objPRL175 = db.PRL175.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL175 != null)
                    {
                        if (objPRL175.PRL176.Any())
                        {
                            db.PRL176.RemoveRange(objPRL175.PRL176.ToList());
                        }
                        if (objPRL175.PRL177.Any())
                        {
                            db.PRL177.RemoveRange(objPRL175.PRL177.ToList());
                        }
                        if (objPRL175.PRL178.Any())
                        {
                            db.PRL178.RemoveRange(objPRL175.PRL178.ToList());
                        }
                        if (objPRL175.PRL179.Any())
                        {
                            db.PRL179.RemoveRange(objPRL175.PRL179.ToList());
                        }
                        if (objPRL175.PRL179_2.Any())
                        {
                            db.PRL179_2.RemoveRange(objPRL175.PRL179_2.ToList());
                        }
                        if (objPRL175.PRL179_3.Any())
                        {
                            db.PRL179_3.RemoveRange(objPRL175.PRL179_3.ToList());
                        }
                        if (objPRL175.PRL179_4.Any())
                        {
                            db.PRL179_4.RemoveRange(objPRL175.PRL179_4.ToList());
                        }
                        if (objPRL175.PRL179_5.Any())
                        {
                            db.PRL179_5.RemoveRange(objPRL175.PRL179_5.ToList());
                        }
                        if (objPRL175.PRL179_6.Any())
                        {
                            db.PRL179_6.RemoveRange(objPRL175.PRL179_6.ToList());
                        }

                        db.PRL175.Remove(objPRL175);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.SETUP_AND_DIMENSION_REPORT_FOR_CIRCUMFERENTIAL_SEAM_V2.GetStringValue())
                {
                    #region Delink with PRL180 header and its lines data For PROTOCOL037

                    PRL180 objPRL180 = db.PRL180.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL180 != null)
                    {
                        if (objPRL180.PRL181.Any())
                        {
                            db.PRL181.RemoveRange(objPRL180.PRL181.ToList());
                        }
                        if (objPRL180.PRL182.Any())
                        {
                            db.PRL182.RemoveRange(objPRL180.PRL182.ToList());
                        }
                        if (objPRL180.PRL183.Any())
                        {
                            db.PRL183.RemoveRange(objPRL180.PRL183.ToList());
                        }
                        if (objPRL180.PRL184.Any())
                        {
                            db.PRL184.RemoveRange(objPRL180.PRL184.ToList());
                        }
                        if (objPRL180.PRL184_2.Any())
                        {
                            db.PRL184_2.RemoveRange(objPRL180.PRL184_2.ToList());
                        }
                        if (objPRL180.PRL184_3.Any())
                        {
                            db.PRL184_3.RemoveRange(objPRL180.PRL184_3.ToList());
                        }
                        if (objPRL180.PRL184_4.Any())
                        {
                            db.PRL184_4.RemoveRange(objPRL180.PRL184_4.ToList());
                        }
                        if (objPRL180.PRL184_5.Any())
                        {
                            db.PRL184_5.RemoveRange(objPRL180.PRL184_5.ToList());
                        }
                        if (objPRL180.PRL184_6.Any())
                        {
                            db.PRL184_6.RemoveRange(objPRL180.PRL184_6.ToList());
                        }
                        if (objPRL180.PRL184_7.Any())
                        {
                            db.PRL184_7.RemoveRange(objPRL180.PRL184_7.ToList());
                        }
                        if (objPRL180.PRL184_8.Any())
                        {
                            db.PRL184_8.RemoveRange(objPRL180.PRL184_8.ToList());
                        }

                        db.PRL180.Remove(objPRL180);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.SETUP_INSPECTION_REPORT_FOR_FORMED_HEMISPHERICAL_DEND_WITHOUT_CROWN_AND_TIER.GetStringValue())
                {
                    #region Delink with PRL185 header and its lines data For PROTOCOL038

                    PRL185 objPRL185 = db.PRL185.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL185 != null)
                    {
                        if (objPRL185.PRL186.Any())
                        {
                            db.PRL186.RemoveRange(objPRL185.PRL186.ToList());
                        }
                        if (objPRL185.PRL187.Any())
                        {
                            db.PRL187.RemoveRange(objPRL185.PRL187.ToList());
                        }
                        if (objPRL185.PRL188.Any())
                        {
                            db.PRL188.RemoveRange(objPRL185.PRL188.ToList());
                        }
                        if (objPRL185.PRL189.Any())
                        {
                            db.PRL189.RemoveRange(objPRL185.PRL189.ToList());
                        }
                        if (objPRL185.PRL189_2.Any())
                        {
                            db.PRL189_2.RemoveRange(objPRL185.PRL189_2.ToList());
                        }
                        if (objPRL185.PRL189_3.Any())
                        {
                            db.PRL189_3.RemoveRange(objPRL185.PRL189_3.ToList());
                        }
                        if (objPRL185.PRL189_4.Any())
                        {
                            db.PRL189_4.RemoveRange(objPRL185.PRL189_4.ToList());
                        }
                        if (objPRL185.PRL189_5.Any())
                        {
                            db.PRL189_5.RemoveRange(objPRL185.PRL189_5.ToList());
                        }
                        if (objPRL185.PRL189_6.Any())
                        {
                            db.PRL189_6.RemoveRange(objPRL185.PRL189_6.ToList());
                        }


                        db.PRL185.Remove(objPRL185);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.SETUP_INSPECTION_REPORT_FOR_FORMED_HEMISPHERICAL_DEND_WITH_CROWN_AND_TIER.GetStringValue())
                {
                    #region Delink with PRL190 header and its lines data For PROTOCOL039

                    PRL190 objPRL190 = db.PRL190.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL190 != null)
                    {
                        if (objPRL190.PRL191.Any())
                        {
                            db.PRL191.RemoveRange(objPRL190.PRL191.ToList());
                        }
                        if (objPRL190.PRL192.Any())
                        {
                            db.PRL192.RemoveRange(objPRL190.PRL192.ToList());
                        }
                        if (objPRL190.PRL193.Any())
                        {
                            db.PRL193.RemoveRange(objPRL190.PRL193.ToList());
                        }
                        if (objPRL190.PRL194.Any())
                        {
                            db.PRL194.RemoveRange(objPRL190.PRL194.ToList());
                        }
                        if (objPRL190.PRL194_2.Any())
                        {
                            db.PRL194_2.RemoveRange(objPRL190.PRL194_2.ToList());
                        }
                        if (objPRL190.PRL194_3.Any())
                        {
                            db.PRL194_3.RemoveRange(objPRL190.PRL194_3.ToList());
                        }
                        if (objPRL190.PRL194_4.Any())
                        {
                            db.PRL194_4.RemoveRange(objPRL190.PRL194_4.ToList());
                        }
                        if (objPRL190.PRL194_5.Any())
                        {
                            db.PRL194_5.RemoveRange(objPRL190.PRL194_5.ToList());
                        }
                        if (objPRL190.PRL194_6.Any())
                        {
                            db.PRL194_6.RemoveRange(objPRL190.PRL194_6.ToList());
                        }
                        if (objPRL190.PRL194_7.Any())
                        {
                            db.PRL194_7.RemoveRange(objPRL190.PRL194_7.ToList());
                        }
                        if (objPRL190.PRL194_8.Any())
                        {
                            db.PRL194_8.RemoveRange(objPRL190.PRL194_8.ToList());
                        }
                        if (objPRL190.PRL194_9.Any())
                        {
                            db.PRL194_9.RemoveRange(objPRL190.PRL194_9.ToList());
                        }

                        db.PRL190.Remove(objPRL190);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.SETUP_INSPECTION_REPORT_FOR_FORMED_ELLIPSOIDAL_TORISPHERICAL_D_END_WITHOUT_CROWN_AND_TIER.GetStringValue())
                {
                    #region Delink with PRL195 header and its lines data For PROTOCOL040

                    PRL195 objPRL195 = db.PRL195.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL195 != null)
                    {
                        if (objPRL195.PRL196.Any())
                        {
                            db.PRL196.RemoveRange(objPRL195.PRL196.ToList());
                        }
                        if (objPRL195.PRL197.Any())
                        {
                            db.PRL197.RemoveRange(objPRL195.PRL197.ToList());
                        }
                        if (objPRL195.PRL198.Any())
                        {
                            db.PRL198.RemoveRange(objPRL195.PRL198.ToList());
                        }
                        if (objPRL195.PRL199.Any())
                        {
                            db.PRL199.RemoveRange(objPRL195.PRL199.ToList());
                        }
                        if (objPRL195.PRL199_2.Any())
                        {
                            db.PRL199_2.RemoveRange(objPRL195.PRL199_2.ToList());
                        }
                        if (objPRL195.PRL199_3.Any())
                        {
                            db.PRL199_3.RemoveRange(objPRL195.PRL199_3.ToList());
                        }
                        if (objPRL195.PRL199_4.Any())
                        {
                            db.PRL199_4.RemoveRange(objPRL195.PRL199_4.ToList());
                        }
                        if (objPRL195.PRL199_5.Any())
                        {
                            db.PRL199_5.RemoveRange(objPRL195.PRL199_5.ToList());
                        }
                        if (objPRL195.PRL199_6.Any())
                        {
                            db.PRL199_6.RemoveRange(objPRL195.PRL199_6.ToList());
                        }

                        db.PRL195.Remove(objPRL195);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.SETUP_INSPECTION_REPORT_FOR_FORMED_ELLIPSOIDAL_TORISPHERICAL_D_END_WITH_CROWN_AND_TIER.GetStringValue())
                {
                    #region Delink with PRL200 header and its lines data For PROTOCOL041

                    PRL200 objPRL200 = db.PRL200.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL200 != null)
                    {
                        if (objPRL200.PRL201.Any())
                        {
                            db.PRL201.RemoveRange(objPRL200.PRL201.ToList());
                        }
                        if (objPRL200.PRL202.Any())
                        {
                            db.PRL202.RemoveRange(objPRL200.PRL202.ToList());
                        }
                        if (objPRL200.PRL203.Any())
                        {
                            db.PRL203.RemoveRange(objPRL200.PRL203.ToList());
                        }
                        if (objPRL200.PRL204.Any())
                        {
                            db.PRL204.RemoveRange(objPRL200.PRL204.ToList());
                        }
                        if (objPRL200.PRL204_2.Any())
                        {
                            db.PRL204_2.RemoveRange(objPRL200.PRL204_2.ToList());
                        }
                        if (objPRL200.PRL204_3.Any())
                        {
                            db.PRL204_3.RemoveRange(objPRL200.PRL204_3.ToList());
                        }
                        if (objPRL200.PRL204_4.Any())
                        {
                            db.PRL204_4.RemoveRange(objPRL200.PRL204_4.ToList());
                        }
                        if (objPRL200.PRL204_5.Any())
                        {
                            db.PRL204_5.RemoveRange(objPRL200.PRL204_5.ToList());
                        }
                        if (objPRL200.PRL204_6.Any())
                        {
                            db.PRL204_6.RemoveRange(objPRL200.PRL204_6.ToList());
                        }
                        if (objPRL200.PRL204_7.Any())
                        {
                            db.PRL204_7.RemoveRange(objPRL200.PRL204_7.ToList());
                        }
                        if (objPRL200.PRL204_8.Any())
                        {
                            db.PRL204_8.RemoveRange(objPRL200.PRL204_8.ToList());
                        }
                        if (objPRL200.PRL204_9.Any())
                        {
                            db.PRL204_9.RemoveRange(objPRL200.PRL204_9.ToList());
                        }

                        db.PRL200.Remove(objPRL200);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.SETUP_INSPECTION_REPORT_FOR_FORMED_ELLIPSOIDAL_TORISPHERICAL_D_END_WITH_CROWN_AND_TIER.GetStringValue())
                {
                    #region Delink with PRL205 header and its lines data For PROTOCOL042

                    PRL205 objPRL205 = db.PRL205.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL205 != null)
                    {
                        if (objPRL205.PRL206.Any())
                        {
                            db.PRL206.RemoveRange(objPRL205.PRL206.ToList());
                        }
                        if (objPRL205.PRL207.Any())
                        {
                            db.PRL207.RemoveRange(objPRL205.PRL207.ToList());
                        }
                        if (objPRL205.PRL208.Any())
                        {
                            db.PRL208.RemoveRange(objPRL205.PRL208.ToList());
                        }
                        if (objPRL205.PRL209.Any())
                        {
                            db.PRL209.RemoveRange(objPRL205.PRL209.ToList());
                        }
                        if (objPRL205.PRL209_2.Any())
                        {
                            db.PRL209_2.RemoveRange(objPRL205.PRL209_2.ToList());
                        }

                        db.PRL205.Remove(objPRL205);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_REPORT_FOR_ROLLED_SHELL_LONGITUDINAL_SEAM_V2.GetStringValue())
                {
                    #region Delink with PRL210 header and its lines data For PROTOCOL043

                    PRL210 objPRL210 = db.PRL210.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL210 != null)
                    {
                        if (objPRL210.PRL211.Any())
                        {
                            db.PRL211.RemoveRange(objPRL210.PRL211.ToList());
                        }
                        if (objPRL210.PRL212.Any())
                        {
                            db.PRL212.RemoveRange(objPRL210.PRL212.ToList());
                        }
                        if (objPRL210.PRL213.Any())
                        {
                            db.PRL213.RemoveRange(objPRL210.PRL213.ToList());
                        }
                        if (objPRL210.PRL214.Any())
                        {
                            db.PRL214.RemoveRange(objPRL210.PRL214.ToList());
                        }
                        if (objPRL210.PRL214_2.Any())
                        {
                            db.PRL214_2.RemoveRange(objPRL210.PRL214_2.ToList());
                        }

                        db.PRL210.Remove(objPRL210);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_REPORT_FOR_CIRCUMFERENTIAL_SEAM_V2.GetStringValue())
                {
                    #region Delink with PRL215 header and its lines data For PROTOCOL044

                    PRL215 objPRL215 = db.PRL215.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL215 != null)
                    {
                        if (objPRL215.PRL216.Any())
                        {
                            db.PRL216.RemoveRange(objPRL215.PRL216.ToList());
                        }
                        if (objPRL215.PRL217.Any())
                        {
                            db.PRL217.RemoveRange(objPRL215.PRL217.ToList());
                        }
                        if (objPRL215.PRL218.Any())
                        {
                            db.PRL218.RemoveRange(objPRL215.PRL218.ToList());
                        }
                        if (objPRL215.PRL219.Any())
                        {
                            db.PRL219.RemoveRange(objPRL215.PRL219.ToList());
                        }
                        if (objPRL215.PRL219_2.Any())
                        {
                            db.PRL219_2.RemoveRange(objPRL215.PRL219_2.ToList());
                        }
                        if (objPRL215.PRL219_3.Any())
                        {
                            db.PRL219_3.RemoveRange(objPRL215.PRL219_3.ToList());
                        }
                        if (objPRL215.PRL219_4.Any())
                        {
                            db.PRL219_4.RemoveRange(objPRL215.PRL219_4.ToList());
                        }
                        if (objPRL215.PRL219_5.Any())
                        {
                            db.PRL219_5.RemoveRange(objPRL215.PRL219_5.ToList());
                        }

                        db.PRL215.Remove(objPRL215);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_INSPECTION_REPORT_FOR_FORMED_HEMISPHERICAL_D_END.GetStringValue())
                {
                    #region Delink with PRL220 header and its lines data For PROTOCOL045

                    PRL220 objPRL220 = db.PRL220.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL220 != null)
                    {
                        if (objPRL220.PRL221.Any())
                        {
                            db.PRL221.RemoveRange(objPRL220.PRL221.ToList());
                        }
                        if (objPRL220.PRL222.Any())
                        {
                            db.PRL222.RemoveRange(objPRL220.PRL222.ToList());
                        }
                        if (objPRL220.PRL223.Any())
                        {
                            db.PRL223.RemoveRange(objPRL220.PRL223.ToList());
                        }
                        if (objPRL220.PRL224.Any())
                        {
                            db.PRL224.RemoveRange(objPRL220.PRL224.ToList());
                        }
                        if (objPRL220.PRL224_2.Any())
                        {
                            db.PRL224_2.RemoveRange(objPRL220.PRL224_2.ToList());
                        }
                        if (objPRL220.PRL224_3.Any())
                        {
                            db.PRL224_3.RemoveRange(objPRL220.PRL224_3.ToList());
                        }

                        db.PRL220.Remove(objPRL220);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_INSPECTION_REPORT_FOR_ELLIPSOIDAL_TORISPHERICAL_FORMED_D_END.GetStringValue())
                {
                    #region Delink with PRL225 header and its lines data For PROTOCOL046

                    PRL225 objPRL225 = db.PRL225.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL225 != null)
                    {
                        if (objPRL225.PRL226.Any())
                        {
                            db.PRL226.RemoveRange(objPRL225.PRL226.ToList());
                        }
                        if (objPRL225.PRL227.Any())
                        {
                            db.PRL227.RemoveRange(objPRL225.PRL227.ToList());
                        }
                        if (objPRL225.PRL228.Any())
                        {
                            db.PRL228.RemoveRange(objPRL225.PRL228.ToList());
                        }
                        if (objPRL225.PRL229.Any())
                        {
                            db.PRL229.RemoveRange(objPRL225.PRL229.ToList());
                        }
                        if (objPRL225.PRL229_2.Any())
                        {
                            db.PRL229_2.RemoveRange(objPRL225.PRL229_2.ToList());
                        }
                        if (objPRL225.PRL229_3.Any())
                        {
                            db.PRL229_3.RemoveRange(objPRL225.PRL229_3.ToList());
                        }

                        db.PRL225.Remove(objPRL225);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.WELD_VISUAL_AND_DIMENSION_REPORT_FOR_NOZZLE_V2.GetStringValue())
                {
                    #region Delink with PRL230 header and its lines data For PROTOCOL047

                    PRL230 objPRL230 = db.PRL230.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL230 != null)
                    {
                        if (objPRL230.PRL231.Any())
                        {
                            db.PRL231.RemoveRange(objPRL230.PRL231.ToList());
                        }
                        if (objPRL230.PRL232.Any())
                        {
                            db.PRL232.RemoveRange(objPRL230.PRL232.ToList());
                        }

                        db.PRL230.Remove(objPRL230);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.SETUP_AND_DIMENSION_REPORT_FOR_ATTACHMENT.GetStringValue())
                {
                    #region Delink with PRL235 header and its lines data For PROTOCOL048

                    PRL235 objPRL235 = db.PRL235.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL235 != null)
                    {
                        if (objPRL235.PRL236.Any())
                        {
                            db.PRL236.RemoveRange(objPRL235.PRL236.ToList());
                        }

                        db.PRL235.Remove(objPRL235);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_REPORT_FOR_ATTACHMENT.GetStringValue())
                {
                    #region Delink with PRL240 header and its lines data For PROTOCOL049

                    PRL240 objPRL240 = db.PRL240.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL240 != null)
                    {
                        if (objPRL240.PRL241.Any())
                        {
                            db.PRL241.RemoveRange(objPRL240.PRL241.ToList());
                        }

                        db.PRL240.Remove(objPRL240);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_REPORT_BEFORE_WELD_OVERALY_ON_SHELL.GetStringValue())
                {
                    #region Delink with PRL245 header and its lines data For PROTOCOL050

                    PRL245 objPRL245 = db.PRL245.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL245 != null)
                    {
                        if (objPRL245.PRL246.Any())
                        {
                            db.PRL246.RemoveRange(objPRL245.PRL246.ToList());
                        }
                        if (objPRL245.PRL247.Any())
                        {
                            db.PRL247.RemoveRange(objPRL245.PRL247.ToList());
                        }
                        if (objPRL245.PRL248.Any())
                        {
                            db.PRL248.RemoveRange(objPRL245.PRL248.ToList());
                        }

                        db.PRL245.Remove(objPRL245);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_REPORT_AFTER_WELD_OVERALY_ON_SHELL.GetStringValue())
                {
                    #region Delink with PRL250 header and its lines data For PROTOCOL051

                    PRL250 objPRL250 = db.PRL250.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL250 != null)
                    {
                        if (objPRL250.PRL251.Any())
                        {
                            db.PRL251.RemoveRange(objPRL250.PRL251.ToList());
                        }
                        if (objPRL250.PRL252.Any())
                        {
                            db.PRL252.RemoveRange(objPRL250.PRL252.ToList());
                        }
                        if (objPRL250.PRL253.Any())
                        {
                            db.PRL253.RemoveRange(objPRL250.PRL253.ToList());
                        }
                        if (objPRL250.PRL254.Any())
                        {
                            db.PRL254.RemoveRange(objPRL250.PRL254.ToList());
                        }

                        db.PRL250.Remove(objPRL250);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.SETUP_AND_DIMENSION_REPORT_FOR_PEPE_TO_PIPE_PIEPE_TO_FLANGE_PIPE_TO_ELBOW_SEAM_PIPE_TO_FORGING_FORGING_TO_FLANGE_ELBOW_TO_FLANGE.GetStringValue())
                {
                    #region Delink with PRL255 header and its lines data For PROTOCOL052

                    PRL255 objPRL255 = db.PRL255.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL255 != null)
                    {
                        if (objPRL255.PRL256.Any())
                        {
                            db.PRL256.RemoveRange(objPRL255.PRL256.ToList());
                        }
                        if (objPRL255.PRL257.Any())
                        {
                            db.PRL257.RemoveRange(objPRL255.PRL257.ToList());
                        }
                        if (objPRL255.PRL258.Any())
                        {
                            db.PRL258.RemoveRange(objPRL255.PRL258.ToList());
                        }
                        if (objPRL255.PRL259.Any())
                        {
                            db.PRL259.RemoveRange(objPRL255.PRL259.ToList());
                        }
                        if (objPRL255.PRL259_2.Any())
                        {
                            db.PRL259_2.RemoveRange(objPRL255.PRL259_2.ToList());
                        }
                        if (objPRL255.PRL259_3.Any())
                        {
                            db.PRL259_3.RemoveRange(objPRL255.PRL259_3.ToList());
                        }
                        if (objPRL255.PRL259_4.Any())
                        {
                            db.PRL259_4.RemoveRange(objPRL255.PRL259_4.ToList());
                        }

                        db.PRL255.Remove(objPRL255);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_REPORT_FOR_PEPE_TO_PIPE_PIEPE_TO_FLANGE_PIPE_TO_ELBOW_SEAM_PIPE_TO_FORGING_FORGING_TO_FLANGE_ELBOW_TO_FLANGE.GetStringValue())
                {
                    #region Delink with PRL260 header and its lines data For PROTOCOL053

                    PRL260 objPRL260 = db.PRL260.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL260 != null)
                    {
                        if (objPRL260.PRL261.Any())
                        {
                            db.PRL261.RemoveRange(objPRL260.PRL261.ToList());
                        }
                        if (objPRL260.PRL262.Any())
                        {
                            db.PRL262.RemoveRange(objPRL260.PRL262.ToList());
                        }
                        if (objPRL260.PRL263.Any())
                        {
                            db.PRL263.RemoveRange(objPRL260.PRL263.ToList());
                        }
                        if (objPRL260.PRL264.Any())
                        {
                            db.PRL264.RemoveRange(objPRL260.PRL264.ToList());
                        }
                        if (objPRL260.PRL264_2.Any())
                        {
                            db.PRL264_2.RemoveRange(objPRL260.PRL264_2.ToList());
                        }

                        db.PRL260.Remove(objPRL260);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_SETUP_AND_DIMENSION_INSPECTION_OF_TSR.GetStringValue())
                {
                    #region Delink with PRL265 header and its lines data For PROTOCOL054

                    PRL265 objPRL265 = db.PRL265.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL265 != null)
                    {
                        if (objPRL265.PRL266.Any())
                        {
                            db.PRL266.RemoveRange(objPRL265.PRL266.ToList());
                        }
                        if (objPRL265.PRL267.Any())
                        {
                            db.PRL267.RemoveRange(objPRL265.PRL267.ToList());
                        }
                        if (objPRL265.PRL268.Any())
                        {
                            db.PRL268.RemoveRange(objPRL265.PRL268.ToList());
                        }

                        db.PRL265.Remove(objPRL265);
                        db.SaveChanges();
                    }

                    #endregion
                }

                else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_VISUAL_AND_DIMENSION_INSPECTION_OF_TSR.GetStringValue())
                {
                    #region Delink with PRL270 header and its lines data For PROTOCOL055

                    PRL270 objPRL270 = db.PRL270.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL270 != null)
                    {
                        if (objPRL270.PRL271.Any())
                        {
                            db.PRL271.RemoveRange(objPRL270.PRL271.ToList());
                        }


                        db.PRL270.Remove(objPRL270);
                        db.SaveChanges();
                    }

                    #endregion
                }

                else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_VISUAL_AND_DIMENSION_REPORT_AFTER_MACHINING_FOR_GASKET_FACE_AND_OTHER_DIMENSIONS_OF_NOZZLE.GetStringValue())
                {
                    #region Delink with PRL275 header and its lines data For PROTOCOL056

                    PRL275 objPRL275 = db.PRL275.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL275 != null)
                    {
                        if (objPRL275.PRL276.Any())
                        {
                            db.PRL276.RemoveRange(objPRL275.PRL276.ToList());
                        }

                        db.PRL275.Remove(objPRL275);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.BEFORE_OVERLAY_THICKNESS_MEASUREMENT_REPORT_FOR_SHELL_HEAD_LONG_SEAM_CIRC_SEAM.GetStringValue())
                {
                    #region Delink with PRL280 header and its lines data For PROTOCOL057

                    PRL280 objPRL280 = db.PRL280.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL280 != null)
                    {
                        if (objPRL280.PRL281.Any())
                        {
                            db.PRL281.RemoveRange(objPRL280.PRL281.ToList());
                        }
                        db.PRL280.Remove(objPRL280);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.OVERLAY_THICKNESS_MEASUREMENT_REPORT_FOR_SHELL_HEAD_LONG_SEAM_CIRC_SEAM.GetStringValue())
                {
                    #region Delink with PRL285 header and its lines data For PROTOCOL058

                    PRL285 objPRL285 = db.PRL285.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL285 != null)
                    {
                        if (objPRL285.PRL286.Any())
                        {
                            db.PRL286.RemoveRange(objPRL285.PRL286.ToList());
                        }
                        db.PRL285.Remove(objPRL285);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.SETUP_AND_DIMENSION_REPORT_FOR_LONGITUDINAL_SEAM_OF_RING_IN_SEGMENT_STAGE.GetStringValue())
                {
                    #region Delink with PRL290 header and its lines data For PROTOCOL059

                    PRL290 objPRL290 = db.PRL290.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL290 != null)
                    {
                        if (objPRL290.PRL291.Any())
                        {
                            db.PRL291.RemoveRange(objPRL290.PRL291.ToList());
                        }
                        if (objPRL290.PRL292.Any())
                        {
                            db.PRL292.RemoveRange(objPRL290.PRL292.ToList());
                        }
                        if (objPRL290.PRL293.Any())
                        {
                            db.PRL293.RemoveRange(objPRL290.PRL293.ToList());
                        }
                        db.PRL290.Remove(objPRL290);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_REPORT_FOR_LONGITUDINAL_SEAM_OF_RING.GetStringValue())
                {
                    #region Delink with PRL295 header and its lines data For PROTOCOL060

                    PRL295 objPRL295 = db.PRL295.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL295 != null)
                    {
                        if (objPRL295.PRL296.Any())
                        {
                            db.PRL296.RemoveRange(objPRL295.PRL296.ToList());
                        }
                        if (objPRL295.PRL297.Any())
                        {
                            db.PRL297.RemoveRange(objPRL295.PRL297.ToList());
                        }
                        if (objPRL295.PRL298.Any())
                        {
                            db.PRL298.RemoveRange(objPRL295.PRL298.ToList());
                        }
                        db.PRL295.Remove(objPRL295);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_SETUP_AND_DIMENSION_OF_TAILING_LUG.GetStringValue())
                {
                    #region Delink with PRL300 header and its lines data For PROTOCOL061

                    PRL300 objPRL300 = db.PRL300.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL300 != null)
                    {
                        if (objPRL300.PRL301.Any())
                        {
                            db.PRL301.RemoveRange(objPRL300.PRL301.ToList());
                        }
                        if (objPRL300.PRL302.Any())
                        {
                            db.PRL302.RemoveRange(objPRL300.PRL302.ToList());
                        }
                        if (objPRL300.PRL303.Any())
                        {
                            db.PRL303.RemoveRange(objPRL300.PRL303.ToList());
                        }
                        db.PRL300.Remove(objPRL300);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_VISUAL_AND_DIMENSION_OF_TAILING_LUG.GetStringValue())
                {
                    #region Delink with PRL305 header and its lines data For PROTOCOL062

                    PRL305 objPRL305 = db.PRL305.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL305 != null)
                    {
                        if (objPRL305.PRL306.Any())
                        {
                            db.PRL306.RemoveRange(objPRL305.PRL306.ToList());
                        }

                        db.PRL305.Remove(objPRL305);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.SETUP_AND_DIMENSION_REPORT_FOR_MTB_BLOCK_OR_PTC_LONGITUDINAL_SEAM.GetStringValue())
                {
                    #region Delink with PRL310 header and its lines data For PROTOCOL063

                    PRL310 objPRL310 = db.PRL310.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL310 != null)
                    {
                        if (objPRL310.PRL311.Any())
                        {
                            db.PRL311.RemoveRange(objPRL310.PRL311.ToList());
                        }
                        if (objPRL310.PRL312.Any())
                        {
                            db.PRL312.RemoveRange(objPRL310.PRL312.ToList());
                        }

                        db.PRL310.Remove(objPRL310);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.SETUP_AND_DIMENSION_REPORT_FOR_LONGITUDINAL_SEAM_OF_TUBESHEET_AND_BAFFLE.GetStringValue())
                {
                    #region Delink with PRL315 header and its lines data For PROTOCOL064

                    PRL315 objPRL315 = db.PRL315.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL315 != null)
                    {
                        if (objPRL315.PRL316.Any())
                        {
                            db.PRL316.RemoveRange(objPRL315.PRL316.ToList());
                        }
                        if (objPRL315.PRL317.Any())
                        {
                            db.PRL317.RemoveRange(objPRL315.PRL317.ToList());
                        }
                        if (objPRL315.PRL318.Any())
                        {
                            db.PRL318.RemoveRange(objPRL315.PRL318.ToList());
                        }
                        if (objPRL315.PRL319.Any())
                        {
                            db.PRL319.RemoveRange(objPRL315.PRL319.ToList());
                        }
                        db.PRL315.Remove(objPRL315);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_REPORT_FOR_LONGITUDINAL_SEAM_OF_TUBESHEET_AND_BAFFLE.GetStringValue())
                {
                    #region Delink with PRL320 header and its lines data For PROTOCOL065

                    PRL320 objPRL320 = db.PRL320.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL320 != null)
                    {
                        if (objPRL320.PRL321.Any())
                        {
                            db.PRL321.RemoveRange(objPRL320.PRL321.ToList());
                        }
                        if (objPRL320.PRL322.Any())
                        {
                            db.PRL322.RemoveRange(objPRL320.PRL322.ToList());
                        }
                        if (objPRL320.PRL323.Any())
                        {
                            db.PRL323.RemoveRange(objPRL320.PRL323.ToList());
                        }

                        db.PRL320.Remove(objPRL320);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_OF_FINAL_MACHINING_INSPECTION_OF_TUBESHEET_V2.GetStringValue())
                {
                    #region Delink with PRL325 header and its lines data For PROTOCOL066

                    PRL325 objPRL325 = db.PRL325.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL325 != null)
                    {
                        if (objPRL325.PRL326.Any())
                        {
                            db.PRL326.RemoveRange(objPRL325.PRL326.ToList());
                        }
                        if (objPRL325.PRL327.Any())
                        {
                            db.PRL327.RemoveRange(objPRL325.PRL327.ToList());
                        }
                        if (objPRL325.PRL328.Any())
                        {
                            db.PRL328.RemoveRange(objPRL325.PRL328.ToList());
                        }

                        db.PRL325.Remove(objPRL325);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_NOZZLE_CUT_OUT_MARKING_ON_SHELL_HEAD.GetStringValue())
                {
                    #region Delink with PRL330 header and its lines data For PROTOCOL067

                    PRL330 objPRL330 = db.PRL330.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL330 != null)
                    {
                        if (objPRL330.PRL331.Any())
                        {
                            db.PRL331.RemoveRange(objPRL330.PRL331.ToList());
                        }
                        db.PRL330.Remove(objPRL330);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.AIR_TEST_REPORT_OF_REINFORCEMENT_PADS_COVERING_WELD_SEAMS.GetStringValue())
                {
                    #region Delink with PRL335 header and its lines data For PROTOCOL068

                    PRL335 objPRL335 = db.PRL335.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL335 != null)
                    {
                        if (objPRL335.PRL336.Any())
                        {
                            db.PRL336.RemoveRange(objPRL335.PRL336.ToList());
                        }
                        if (objPRL335.PRL337.Any())
                        {
                            db.PRL337.RemoveRange(objPRL335.PRL337.ToList());
                        }
                        db.PRL335.Remove(objPRL335);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.AIR_TEST_REPORT_OF_REINFORCEMENT_PADS_OF_NOZZLE.GetStringValue())
                {
                    #region Delink with PRL340 header and its lines data For PROTOCOL069

                    PRL340 objPRL340 = db.PRL340.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL340 != null)
                    {
                        if (objPRL340.PRL341.Any())
                        {
                            db.PRL341.RemoveRange(objPRL340.PRL341.ToList());
                        }
                        if (objPRL340.PRL342.Any())
                        {
                            db.PRL342.RemoveRange(objPRL340.PRL342.ToList());
                        }

                        db.PRL340.Remove(objPRL340);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.HELIUM_LEAK_TEST_REPORT_V2.GetStringValue())
                {
                    #region Delink with PRL345 header and its lines data For PROTOCOL070

                    PRL345 objPRL345 = db.PRL345.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL345 != null)
                    {
                        if (objPRL345.PRL346.Any())
                        {
                            db.PRL346.RemoveRange(objPRL345.PRL346.ToList());
                        }


                        db.PRL345.Remove(objPRL345);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.HYDROSTATIC_TEST_REPORT_V2.GetStringValue())
                {
                    #region Delink with PRL350 header and its lines data For PROTOCOL071

                    PRL350 objPRL350 = db.PRL350.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL350 != null)
                    {
                        if (objPRL350.PRL351.Any())
                        {
                            db.PRL351.RemoveRange(objPRL350.PRL351.ToList());
                        }
                        if (objPRL350.PRL352.Any())
                        {
                            db.PRL352.RemoveRange(objPRL350.PRL352.ToList());
                        }

                        db.PRL350.Remove(objPRL350);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.PNEUMATIC_TEST_REPORT_V2.GetStringValue())
                {
                    #region Delink with PRL355 header and its lines data For PROTOCOL072

                    PRL355 objPRL355 = db.PRL355.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL355 != null)
                    {
                        if (objPRL355.PRL356.Any())
                        {
                            db.PRL356.RemoveRange(objPRL355.PRL356.ToList());
                        }

                        db.PRL355.Remove(objPRL355);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_CUSO4_TEST.GetStringValue())
                {
                    #region Delink with PRL360 header and its lines data For PROTOCOL073

                    PRL360 objPRL360 = db.PRL360.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL360 != null)
                    {
                        if (objPRL360.PRL361.Any())
                        {
                            db.PRL361.RemoveRange(objPRL360.PRL361.ToList());
                        }


                        db.PRL360.Remove(objPRL360);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_N2_FILLING_INSPECTION_V2.GetStringValue())
                {
                    #region Delink with PRL365 header and its lines data For PROTOCOL074

                    PRL365 objPRL365 = db.PRL365.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL365 != null)
                    {
                        if (objPRL365.PRL366.Any())
                        {
                            db.PRL366.RemoveRange(objPRL365.PRL366.ToList());
                        }
                        if (objPRL365.PRL367.Any())
                        {
                            db.PRL367.RemoveRange(objPRL365.PRL367.ToList());
                        }

                        db.PRL365.Remove(objPRL365);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.SETUP_AND_DIMENSION_REPORT_FOR_FORMED_CONE.GetStringValue())
                {
                    #region Delink with PRL370 header and its lines data For PROTOCOL075

                    PRL370 objPRL370 = db.PRL370.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL370 != null)
                    {
                        if (objPRL370.PRL371.Any())
                        {
                            db.PRL371.RemoveRange(objPRL370.PRL371.ToList());
                        }
                        if (objPRL370.PRL372.Any())
                        {
                            db.PRL372.RemoveRange(objPRL370.PRL372.ToList());
                        }
                        if (objPRL370.PRL373.Any())
                        {
                            db.PRL373.RemoveRange(objPRL370.PRL373.ToList());
                        }
                        if (objPRL370.PRL374.Any())
                        {
                            db.PRL374.RemoveRange(objPRL370.PRL374.ToList());
                        }
                        if (objPRL370.PRL374_2.Any())
                        {
                            db.PRL374_2.RemoveRange(objPRL370.PRL374_2.ToList());
                        }
                        if (objPRL370.PRL374_3.Any())
                        {
                            db.PRL374_3.RemoveRange(objPRL370.PRL374_3.ToList());
                        }
                        if (objPRL370.PRL374_4.Any())
                        {
                            db.PRL374_4.RemoveRange(objPRL370.PRL374_4.ToList());
                        }
                        if (objPRL370.PRL374_5.Any())
                        {
                            db.PRL374_5.RemoveRange(objPRL370.PRL374_5.ToList());
                        }
                        if (objPRL370.PRL374_6.Any())
                        {
                            db.PRL374_6.RemoveRange(objPRL370.PRL374_6.ToList());
                        }
                        if (objPRL370.PRL374_7.Any())
                        {
                            db.PRL374_7.RemoveRange(objPRL370.PRL374_7.ToList());
                        }
                        if (objPRL370.PRL374_8.Any())
                        {
                            db.PRL374_8.RemoveRange(objPRL370.PRL374_8.ToList());
                        }
                        if (objPRL370.PRL374_9.Any())
                        {
                            db.PRL374_9.RemoveRange(objPRL370.PRL374_9.ToList());
                        }
                        if (objPRL370.PRL374_10.Any())
                        {
                            db.PRL374_10.RemoveRange(objPRL370.PRL374_10.ToList());
                        }
                        if (objPRL370.PRL374_11.Any())
                        {
                            db.PRL374_11.RemoveRange(objPRL370.PRL374_11.ToList());
                        }
                        db.PRL370.Remove(objPRL370);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_REPORT_FOR_FORMED_CONE.GetStringValue())
                {
                    #region Delink with PRL375 header and its lines data For PROTOCOL076

                    PRL375 objPRL375 = db.PRL375.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL375 != null)
                    {
                        if (objPRL375.PRL376.Any())
                        {
                            db.PRL376.RemoveRange(objPRL375.PRL376.ToList());
                        }
                        if (objPRL375.PRL377.Any())
                        {
                            db.PRL377.RemoveRange(objPRL375.PRL377.ToList());
                        }
                        if (objPRL375.PRL378.Any())
                        {
                            db.PRL378.RemoveRange(objPRL375.PRL378.ToList());
                        }
                        if (objPRL375.PRL379.Any())
                        {
                            db.PRL379.RemoveRange(objPRL375.PRL379.ToList());
                        }
                        if (objPRL375.PRL379_2.Any())
                        {
                            db.PRL379_2.RemoveRange(objPRL375.PRL379_2.ToList());
                        }
                        if (objPRL375.PRL379_3.Any())
                        {
                            db.PRL379_3.RemoveRange(objPRL375.PRL379_3.ToList());
                        }
                        if (objPRL375.PRL379_4.Any())
                        {
                            db.PRL379_4.RemoveRange(objPRL375.PRL379_4.ToList());
                        }

                        db.PRL375.Remove(objPRL375);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_VISUAL_AND_DIMENSION_INSPECTION_FOR_SQUARE_TYPE_NUB_BEFORE_OVERLAY.GetStringValue())
                {
                    #region Delink with PRL380 header and its lines data For PROTOCOL077

                    PRL380 objPRL380 = db.PRL380.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL380 != null)
                    {
                        if (objPRL380.PRL381.Any())
                        {
                            db.PRL381.RemoveRange(objPRL380.PRL381.ToList());
                        }
                        if (objPRL380.PRL382.Any())
                        {
                            db.PRL382.RemoveRange(objPRL380.PRL382.ToList());
                        }
                        if (objPRL380.PRL383.Any())
                        {
                            db.PRL383.RemoveRange(objPRL380.PRL383.ToList());
                        }
                        if (objPRL380.PRL384.Any())
                        {
                            db.PRL384.RemoveRange(objPRL380.PRL384.ToList());
                        }

                        db.PRL380.Remove(objPRL380);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_VISUAL_AND_DIMENSION_INSPECTION_FOR_SQUARE_TYPE_NUB_AFTER_OVERLAY.GetStringValue())
                {
                    #region Delink with PRL385 header and its lines data For PROTOCOL078

                    PRL385 objPRL385 = db.PRL385.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL385 != null)
                    {
                        if (objPRL385.PRL386.Any())
                        {
                            db.PRL386.RemoveRange(objPRL385.PRL386.ToList());
                        }
                        if (objPRL385.PRL387.Any())
                        {
                            db.PRL387.RemoveRange(objPRL385.PRL387.ToList());
                        }
                        if (objPRL385.PRL388.Any())
                        {
                            db.PRL388.RemoveRange(objPRL385.PRL388.ToList());
                        }
                        if (objPRL385.PRL389.Any())
                        {
                            db.PRL389.RemoveRange(objPRL385.PRL389.ToList());
                        }

                        db.PRL385.Remove(objPRL385);
                        db.SaveChanges();
                    }

                    #endregion
                }

                else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_VISUAL_AND_DIMENSION_INSPECTION_FOR_TAPER_TYPE_NUB_BEFORE_OVERLAY.GetStringValue())
                {
                    #region Delink with PRL390 header and its lines data For PROTOCOL079

                    PRL390 objPRL390 = db.PRL390.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL390 != null)
                    {
                        if (objPRL390.PRL391.Any())
                        {
                            db.PRL391.RemoveRange(objPRL390.PRL391.ToList());
                        }
                        if (objPRL390.PRL392.Any())
                        {
                            db.PRL392.RemoveRange(objPRL390.PRL392.ToList());
                        }
                        if (objPRL390.PRL393.Any())
                        {
                            db.PRL393.RemoveRange(objPRL390.PRL393.ToList());
                        }
                        if (objPRL390.PRL394.Any())
                        {
                            db.PRL394.RemoveRange(objPRL390.PRL394.ToList());
                        }

                        db.PRL390.Remove(objPRL390);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_VISUAL_AND_DIMENSION_INSPECTION_FOR_TAPER_TYPE_NUB_AFTER_OVERLAY.GetStringValue())
                {
                    #region Delink with PRL395 header and its lines data For PROTOCOL080

                    PRL395 objPRL395 = db.PRL395.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL395 != null)
                    {
                        if (objPRL395.PRL396.Any())
                        {
                            db.PRL396.RemoveRange(objPRL395.PRL396.ToList());
                        }
                        if (objPRL395.PRL397.Any())
                        {
                            db.PRL397.RemoveRange(objPRL395.PRL397.ToList());
                        }
                        if (objPRL395.PRL398.Any())
                        {
                            db.PRL398.RemoveRange(objPRL395.PRL398.ToList());
                        }
                        if (objPRL395.PRL399.Any())
                        {
                            db.PRL399.RemoveRange(objPRL395.PRL399.ToList());
                        }

                        db.PRL395.Remove(objPRL395);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_VISUAL_AND_DIMENSION_INSPECTION_FOR_NUB_ON_D_END_BEFORE_OVERLAY.GetStringValue())
                {
                    #region Delink with PRL400 header and its lines data For PROTOCOL081

                    PRL400 objPRL400 = db.PRL400.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL400 != null)
                    {
                        if (objPRL400.PRL401.Any())
                        {
                            db.PRL401.RemoveRange(objPRL400.PRL401.ToList());
                        }
                        if (objPRL400.PRL402.Any())
                        {
                            db.PRL402.RemoveRange(objPRL400.PRL402.ToList());
                        }
                        if (objPRL400.PRL403.Any())
                        {
                            db.PRL403.RemoveRange(objPRL400.PRL403.ToList());
                        }


                        db.PRL400.Remove(objPRL400);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_VISUAL_AND_DIMENSION_INSPECTION_FOR_NUB_ON_D_END_AFTER_OVERLAY.GetStringValue())
                {
                    #region Delink with PRL405 header and its lines data For PROTOCOL082

                    PRL405 objPRL405 = db.PRL405.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL405 != null)
                    {
                        if (objPRL405.PRL406.Any())
                        {
                            db.PRL406.RemoveRange(objPRL405.PRL406.ToList());
                        }
                        if (objPRL405.PRL407.Any())
                        {
                            db.PRL407.RemoveRange(objPRL405.PRL407.ToList());
                        }

                        if (objPRL405.PRL408.Any())
                        {
                            db.PRL408.RemoveRange(objPRL405.PRL408.ToList());
                        }

                        db.PRL405.Remove(objPRL405);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_VISUAL_AND_DIMENSION_INSPECTION_OF_SKIRT_ASSEMBLY.GetStringValue())
                {
                    #region Delink with PRL410 header and its lines data For PROTOCOL083

                    PRL410 objPRL410 = db.PRL410.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL410 != null)
                    {
                        if (objPRL410.PRL411.Any())
                        {
                            db.PRL411.RemoveRange(objPRL410.PRL411.ToList());
                        }
                        if (objPRL410.PRL412.Any())
                        {
                            db.PRL412.RemoveRange(objPRL410.PRL412.ToList());
                        }
                        if (objPRL410.PRL413.Any())
                        {
                            db.PRL413.RemoveRange(objPRL410.PRL413.ToList());
                        }
                        if (objPRL410.PRL414.Any())
                        {
                            db.PRL414.RemoveRange(objPRL410.PRL414.ToList());
                        }
                        if (objPRL410.PRL414_2.Any())
                        {
                            db.PRL414_2.RemoveRange(objPRL410.PRL414_2.ToList());
                        }
                        if (objPRL410.PRL414_3.Any())
                        {
                            db.PRL414_3.RemoveRange(objPRL410.PRL414_3.ToList());
                        }
                        if (objPRL410.PRL414_4.Any())
                        {
                            db.PRL414_4.RemoveRange(objPRL410.PRL414_4.ToList());
                        }
                        if (objPRL410.PRL414_5.Any())
                        {
                            db.PRL414_5.RemoveRange(objPRL410.PRL414_5.ToList());
                        }
                        if (objPRL410.PRL414_6.Any())
                        {
                            db.PRL414_6.RemoveRange(objPRL410.PRL414_6.ToList());
                        }
                        if (objPRL410.PRL414_7.Any())
                        {
                            db.PRL414_7.RemoveRange(objPRL410.PRL414_7.ToList());
                        }


                        db.PRL410.Remove(objPRL410);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_VISUAL_AND_DIMENSION_OF_SKIRT_TEMPLATE.GetStringValue())
                {
                    #region Delink with PRL415 header and its lines data For PROTOCOL084

                    PRL415 objPRL415 = db.PRL415.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL415 != null)
                    {
                        if (objPRL415.PRL416.Any())
                        {
                            db.PRL416.RemoveRange(objPRL415.PRL416.ToList());
                        }
                        if (objPRL415.PRL417.Any())
                        {
                            db.PRL417.RemoveRange(objPRL415.PRL417.ToList());
                        }
                        if (objPRL415.PRL418.Any())
                        {
                            db.PRL418.RemoveRange(objPRL415.PRL418.ToList());
                        }
                        if (objPRL415.PRL419.Any())
                        {
                            db.PRL419.RemoveRange(objPRL415.PRL419.ToList());
                        }
                        if (objPRL415.PRL419_2.Any())
                        {
                            db.PRL419_2.RemoveRange(objPRL415.PRL419_2.ToList());
                        }
                        if (objPRL415.PRL419_3.Any())
                        {
                            db.PRL419_3.RemoveRange(objPRL415.PRL419_3.ToList());
                        }
                        if (objPRL415.PRL419_4.Any())
                        {
                            db.PRL419_4.RemoveRange(objPRL415.PRL419_4.ToList());
                        }
                        if (objPRL415.PRL419_5.Any())
                        {
                            db.PRL419_5.RemoveRange(objPRL415.PRL419_5.ToList());
                        }


                        db.PRL415.Remove(objPRL415);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_WELD_VISUAL_AND_DIMENSION_REPORT_FOR_SPOOL_TYPE_1_BOTH_SIDE_FLANGES.GetStringValue())
                {
                    #region Delink with PRL420 header and its lines data For PROTOCOL085

                    PRL420 objPRL420 = db.PRL420.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL420 != null)
                    {
                        if (objPRL420.PRL421.Any())
                        {
                            db.PRL421.RemoveRange(objPRL420.PRL421.ToList());
                        }


                        db.PRL420.Remove(objPRL420);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_WELD_VISUAL_AND_DIMENSION_FOR_SPOOL_TYPE_2_ONE_SIDE_FLANGE.GetStringValue())
                {
                    #region Delink with PRL425 header and its lines data For PROTOCOL086

                    PRL425 objPRL425 = db.PRL425.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL425 != null)
                    {
                        if (objPRL425.PRL426.Any())
                        {
                            db.PRL426.RemoveRange(objPRL425.PRL426.ToList());
                        }


                        db.PRL425.Remove(objPRL425);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_WELD_VISUAL_AND_DIMENSION_REPORT_FOR_SPOOL_TYPE_3_BOTH_SIDE_FLANGES.GetStringValue())
                {
                    #region Delink with PRL430 header and its lines data For PROTOCOL087

                    PRL430 objPRL430 = db.PRL430.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL430 != null)
                    {
                        if (objPRL430.PRL431.Any())
                        {
                            db.PRL431.RemoveRange(objPRL430.PRL431.ToList());
                        }


                        db.PRL430.Remove(objPRL430);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.AFTER_PWHTFINAL_REPORT_FOR_VISUAL_AND_DIMENSION_AFTER_MACHINING_OF_GASKET_FACE_AND_OTHER_DIMENSIONS_OF_FLANGE.GetStringValue())
                {
                    #region Delink with PRL435 header and its lines data For PROTOCOL088

                    PRL435 objPRL435 = db.PRL435.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL435 != null)
                    {
                        if (objPRL435.PRL436.Any())
                        {
                            db.PRL436.RemoveRange(objPRL435.PRL436.ToList());
                        }
                        if (objPRL435.PRL437.Any())
                        {
                            db.PRL437.RemoveRange(objPRL435.PRL437.ToList());
                        }

                        db.PRL435.Remove(objPRL435);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.AFTER_PWHTFINAL_REPORT_FOR_VISUAL_AND_DIMENSION_AFTER_MACHINING_OF_GASKET_FACE_AND_OTHER_DIMENSIONS_OF_NOZZLE.GetStringValue())
                {
                    #region Delink with PRL440 header and its lines data For PROTOCOL089

                    PRL440 objPRL440 = db.PRL440.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL440 != null)
                    {
                        if (objPRL440.PRL441.Any())
                        {
                            db.PRL441.RemoveRange(objPRL440.PRL441.ToList());
                        }
                        if (objPRL440.PRL442.Any())
                        {
                            db.PRL442.RemoveRange(objPRL440.PRL442.ToList());
                        }

                        db.PRL440.Remove(objPRL440);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.INITIAL_MACHINING_VISUAL_AND_DIMENSION_REPORT_FOR_GASKET_FACE_AND_OTHER_DIMENSIONS_OF_FLANGE.GetStringValue())
                {
                    #region Delink with PRL445 header and its lines data For PROTOCOL090

                    PRL445 objPRL445 = db.PRL445.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL445 != null)
                    {
                        if (objPRL445.PRL446.Any())
                        {
                            db.PRL446.RemoveRange(objPRL445.PRL446.ToList());
                        }
                        if (objPRL445.PRL447.Any())
                        {
                            db.PRL447.RemoveRange(objPRL445.PRL447.ToList());
                        }

                        db.PRL445.Remove(objPRL445);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.INITIAL_MACHINING_VISUAL_AND_DIMENSION_REPORT_FOR_GASKET_FACE_AND_OTHER_DIMENSIONS_OF_NOZZLE.GetStringValue())
                {
                    #region Delink with PRL450 header and its lines data For PROTOCOL091

                    PRL450 objPRL450 = db.PRL450.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL450 != null)
                    {
                        if (objPRL450.PRL451.Any())
                        {
                            db.PRL451.RemoveRange(objPRL450.PRL451.ToList());
                        }
                        if (objPRL450.PRL452.Any())
                        {
                            db.PRL452.RemoveRange(objPRL450.PRL452.ToList());
                        }

                        db.PRL450.Remove(objPRL450);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_WATER_BOX_MACHINING_INSPECTION_R1.GetStringValue())
                {
                    #region Delink with PRL455 header and its lines data For PROTOCOL092

                    PRL455 objPRL455 = db.PRL455.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL455 != null)
                    {
                        if (objPRL455.PRL456.Any())
                        {
                            db.PRL456.RemoveRange(objPRL455.PRL456.ToList());
                        }
                        if (objPRL455.PRL457.Any())
                        {
                            db.PRL457.RemoveRange(objPRL455.PRL457.ToList());
                        }
                        if (objPRL455.PRL458.Any())
                        {
                            db.PRL458.RemoveRange(objPRL455.PRL458.ToList());
                        }
                        if (objPRL455.PRL459.Any())
                        {
                            db.PRL459.RemoveRange(objPRL455.PRL459.ToList());
                        }

                        db.PRL455.Remove(objPRL455);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_SCREW_PLUG_ACME_HEADER_MACHINING_INSPECTION.GetStringValue())
                {
                    #region Delink with PRL460 header and its lines data For PROTOCOL093

                    PRL460 objPRL460 = db.PRL460.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL460 != null)
                    {
                        if (objPRL460.PRL461.Any())
                        {
                            db.PRL461.RemoveRange(objPRL460.PRL461.ToList());
                        }
                        if (objPRL460.PRL462.Any())
                        {
                            db.PRL462.RemoveRange(objPRL460.PRL462.ToList());
                        }
                        if (objPRL460.PRL463.Any())
                        {
                            db.PRL463.RemoveRange(objPRL460.PRL463.ToList());
                        }
                        if (objPRL460.PRL464.Any())
                        {
                            db.PRL464.RemoveRange(objPRL460.PRL464.ToList());
                        }
                        if (objPRL460.PRL464_2.Any())
                        {
                            db.PRL464_2.RemoveRange(objPRL460.PRL464_2.ToList());
                        }

                        db.PRL460.Remove(objPRL460);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.FINAL_ASSEMBLY_INSPECTION_OF_ROUGH_LIQUID_DISTRIBUTION_TRAY.GetStringValue())
                {
                    #region Delink with PRL465 header and its lines data For PROTOCOL094

                    PRL465 objPRL465 = db.PRL465.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL465 != null)
                    {
                        if (objPRL465.PRL466.Any())
                        {
                            db.PRL466.RemoveRange(objPRL465.PRL466.ToList());
                        }
                        if (objPRL465.PRL467.Any())
                        {
                            db.PRL467.RemoveRange(objPRL465.PRL467.ToList());
                        }
                        if (objPRL465.PRL468.Any())
                        {
                            db.PRL468.RemoveRange(objPRL465.PRL468.ToList());
                        }

                        db.PRL465.Remove(objPRL465);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.FINAL_ASSEMBLY_INSPECTION_OF_VAPOUR_LIQUID_DISTRIBUTION_TRAY.GetStringValue())
                {
                    #region Delink with PRL470 header and its lines data For PROTOCOL095

                    PRL470 objPRL470 = db.PRL470.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL470 != null)
                    {
                        if (objPRL470.PRL471.Any())
                        {
                            db.PRL471.RemoveRange(objPRL470.PRL471.ToList());
                        }
                        if (objPRL470.PRL472.Any())
                        {
                            db.PRL472.RemoveRange(objPRL470.PRL472.ToList());
                        }
                        if (objPRL470.PRL473.Any())
                        {
                            db.PRL473.RemoveRange(objPRL470.PRL473.ToList());
                        }

                        db.PRL470.Remove(objPRL470);
                        db.SaveChanges();
                    }

                    #endregion
                }
                else if (ProtocolType == clsImplementationEnum.ProtocolType.FINAL_DIMENSION_INSPECTION_OF_LATTICE_BEAMS.GetStringValue())
                {
                    #region Delink with PRL475 header and its lines data For PROTOCOL096

                    PRL475 objPRL475 = db.PRL475.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL475 != null)
                    {
                        if (objPRL475.PRL476.Any())
                        {
                            db.PRL476.RemoveRange(objPRL475.PRL476.ToList());
                        }
                        if (objPRL475.PRL477.Any())
                        {
                            db.PRL477.RemoveRange(objPRL475.PRL477.ToList());
                        }
                        if (objPRL475.PRL478.Any())
                        {
                            db.PRL478.RemoveRange(objPRL475.PRL478.ToList());
                        }

                        db.PRL475.Remove(objPRL475);
                        db.SaveChanges();
                    }

                    #endregion
                }

                else if (ProtocolType == clsImplementationEnum.ProtocolType.FINAL_DIMENSION_AND_VISUAL_INSPECTION_REPORT.GetStringValue())
                {
                    #region Delink with PRL480 header and its lines data For PROTOCOL098

                    PRL480 objPRL480 = db.PRL480.FirstOrDefault(c => c.HeaderId == ProtocolHeaderId);
                    if (objPRL480 != null)
                    {
                        if (objPRL480.PRL484_12.Any()) { db.PRL484_12.RemoveRange(objPRL480.PRL484_12.ToList()); }
                        if (objPRL480.PRL484_11_1.Any()) { db.PRL484_11_1.RemoveRange(objPRL480.PRL484_11_1.ToList()); }
                        if (objPRL480.PRL484_11.Any()) { db.PRL484_11.RemoveRange(objPRL480.PRL484_11.ToList()); }
                        if (objPRL480.PRL484_10.Any()) { db.PRL484_11.RemoveRange(objPRL480.PRL484_11.ToList()); }
                        if (objPRL480.PRL484_9.Any()) { db.PRL484_9.RemoveRange(objPRL480.PRL484_9.ToList()); }
                        if (objPRL480.PRL484_8.Any()) { db.PRL484_8.RemoveRange(objPRL480.PRL484_8.ToList()); }
                        if (objPRL480.PRL484_7.Any()) { db.PRL484_7.RemoveRange(objPRL480.PRL484_7.ToList()); }
                        if (objPRL480.PRL484_6_1.Any()) { db.PRL484_6_1.RemoveRange(objPRL480.PRL484_6_1.ToList()); }
                        if (objPRL480.PRL484_6.Any()) { db.PRL484_6.RemoveRange(objPRL480.PRL484_6.ToList()); }
                        if (objPRL480.PRL484_5_1.Any()) { db.PRL484_5_1.RemoveRange(objPRL480.PRL484_5_1.ToList()); }
                        if (objPRL480.PRL484_5.Any()) { db.PRL484_5.RemoveRange(objPRL480.PRL484_5.ToList()); }
                        if (objPRL480.PRL484_4.Any()) { db.PRL484_4.RemoveRange(objPRL480.PRL484_4.ToList()); }
                        if (objPRL480.PRL484_3.Any()) { db.PRL484_3.RemoveRange(objPRL480.PRL484_3.ToList()); }
                        if (objPRL480.PRL484_2.Any()) { db.PRL484_2.RemoveRange(objPRL480.PRL484_2.ToList()); }
                        if (objPRL480.PRL484_1.Any()) { db.PRL484_1.RemoveRange(objPRL480.PRL484_1.ToList()); }
                        if (objPRL480.PRL484.Any()) { db.PRL484.RemoveRange(objPRL480.PRL484.ToList()); }
                        if (objPRL480.PRL483.Any()) { db.PRL483.RemoveRange(objPRL480.PRL483.ToList()); }
                        if (objPRL480.PRL482.Any()) { db.PRL482.RemoveRange(objPRL480.PRL482.ToList()); }
                        if (objPRL480.PRL481.Any()) { db.PRL481.RemoveRange(objPRL480.PRL481.ToList()); }

                        db.PRL480.Remove(objPRL480);
                        db.SaveChanges();
                    }

                    #endregion
                }

                return true;
            }
            catch
            {
                return false;
            }

        }

        public string GetInspectionAgency(string qproject)
        {
            var lstQMS025 = db.QMS025.Where(x => x.QualityProject == qproject).ToList();
            List<string> lsTPIAgency = new List<string>();
            lsTPIAgency.AddRange(lstQMS025.Where(x => !string.IsNullOrWhiteSpace(x.FirstTPIAgency)).Select(x => x.FirstTPIAgency).Distinct().ToList());
            lsTPIAgency.AddRange(lstQMS025.Where(x => !string.IsNullOrWhiteSpace(x.SecondTPIAgency)).Select(x => x.SecondTPIAgency).Distinct().ToList());
            lsTPIAgency.AddRange(lstQMS025.Where(x => !string.IsNullOrWhiteSpace(x.ThirdTPIAgency)).Select(x => x.ThirdTPIAgency).Distinct().ToList());
            lsTPIAgency.AddRange(lstQMS025.Where(x => !string.IsNullOrWhiteSpace(x.ForthTPIAgency)).Select(x => x.ForthTPIAgency).Distinct().ToList());
            lsTPIAgency.AddRange(lstQMS025.Where(x => !string.IsNullOrWhiteSpace(x.FifthTPIAgency)).Select(x => x.FifthTPIAgency).Distinct().ToList());
            lsTPIAgency.AddRange(lstQMS025.Where(x => !string.IsNullOrWhiteSpace(x.SixthTPIAgency)).Select(x => x.SixthTPIAgency).Distinct().ToList());
            string InspectionAgency = string.Join(",", lsTPIAgency.Distinct().ToList());
            return InspectionAgency;
        }
        public bool CopyProtocolData(string FromTable, string ToTable, string WhereCondition, string SkipColumns)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(WhereCondition))
                    WhereCondition = "1=1";

                db.SP_IPI_PROTOCOL_COPY_DATA("dbo", FromTable, ToTable, WhereCondition, SkipColumns);

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool CopyProtocolLinesData(string FromTable, string ToTable, string WhereCondition, string SkipColumns, int newHeaderId, string Username, string ProtocolType, string role = "")
        {
            try
            {
                if (string.IsNullOrWhiteSpace(WhereCondition))
                    WhereCondition = "1=1";
                using (var db = new IEMQSEntitiesContext())
                {
                    db.SP_IPI_PROTOCOL_COPY_LINES_DATA(newHeaderId, "dbo", FromTable, ToTable, WhereCondition, SkipColumns, Username, ProtocolType, role);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        public string FetchStageType(string stageCode, string bu, string location)
        {
            string stagetype = string.Empty;
            stagetype = db.QMS002.Where(x => x.StageCode == stageCode && x.BU == bu && x.Location == location).Select(x => x.StageType).FirstOrDefault();
            return stagetype;
        }

        public bool IsProtocolFilledByProduction(int ProtocolId, string ProtocolType, string BU, string Location)
        {
            try
            {
                bool? result = db.SP_IPI_PROTOCOL_IS_LAST_FILLED_BY_USERROLE(ProtocolId, ProtocolType, BU, Location, clsImplementationEnum.UserRoleName.PROD3.GetStringValue()).FirstOrDefault();
                if (result.HasValue)
                    return result.Value;
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        public bool IsProtocolFilledByQC(int ProtocolId, string ProtocolType, string BU, string Location)
        {
            try
            {
                bool? result = db.SP_IPI_PROTOCOL_IS_LAST_FILLED_BY_USERROLE(ProtocolId, ProtocolType, BU, Location, clsImplementationEnum.UserRoleName.QC3.GetStringValue()).FirstOrDefault();
                if (result.HasValue)
                    return result.Value;
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        public PRL001 CopyData_PROTOCOL001(PRO001 objPRO001, PRL001 objPRL001)
        {
            if (objPRO001 != null)
            {
                #region Copy data from PRO001 to PRL001

                objPRL001.ProtocolNo = objPRO001.ProtocolNo;
                objPRL001.IdentificationMarking = objPRO001.IdentificationMarking;
                objPRL001.PtcRequired = objPRO001.PtcRequired;
                objPRL001.WepUTMTPTCUSO4Clear = objPRO001.WepUTMTPTCUSO4Clear;
                objPRL001.SeamNoPunchingAtEvery900mm = objPRO001.SeamNoPunchingAtEvery900mm;
                objPRL001.ExtentOfNDTRT = objPRO001.ExtentOfNDTRT;
                objPRL001.ExtentOfNDTUT = objPRO001.ExtentOfNDTUT;
                objPRL001.ExtentOfNDTTOFD = objPRO001.ExtentOfNDTTOFD;
                objPRL001.WEPAngleis = objPRO001.WEPAngleis;
                objPRL001.WEPAngleos = objPRO001.WEPAngleos;
                objPRL001.TacksFreeFromCrack = objPRO001.TacksFreeFromCrack;
                objPRL001.RequriedRootGap = objPRO001.RequriedRootGap;
                objPRL001.RequriedRootFace = objPRO001.RequriedRootFace;
                objPRL001.ActMaxRootGap = objPRO001.ActMaxRootGap;
                objPRL001.ActMaxRootFace = objPRO001.ActMaxRootFace;
                objPRL001.ActMinRootGap = objPRO001.ActMinRootGap;
                objPRL001.ActMinRootFace = objPRO001.ActMinRootFace;
                objPRL001.ShellThiknessRequired = objPRO001.ShellThiknessRequired;
                objPRL001.OffsetAllowed = objPRO001.OffsetAllowed;
                objPRL001.RrequriedActual1 = objPRO001.RrequriedActual1;
                objPRL001.ActMaxActual1 = objPRO001.ActMaxActual1;
                objPRL001.ActMinActual1 = objPRO001.ActMinActual1;
                objPRL001.RrequiredActual2 = objPRO001.RrequiredActual2;
                objPRL001.ActMaxActual2 = objPRO001.ActMaxActual2;
                objPRL001.ActMinActual2 = objPRO001.ActMinActual2;
                objPRL001.RequriedActual3 = objPRO001.RequriedActual3;
                objPRL001.ActMaxActual3 = objPRO001.ActMaxActual3;
                objPRL001.ActMinActual3 = objPRO001.ActMinActual3;
                objPRL001.RrequriedActual4 = objPRO001.RrequriedActual4;
                objPRL001.ActMaxActual4 = objPRO001.ActMaxActual4;
                objPRL001.ActMinActual4 = objPRO001.ActMinActual4;
                objPRL001.RequredActual5 = objPRO001.RequredActual5;
                objPRL001.ActMaxActual5 = objPRO001.ActMaxActual5;
                objPRL001.ActMinActual5 = objPRO001.ActMinActual5;
                objPRL001.TotalShellHeightRequried = objPRO001.TotalShellHeightRequried;
                objPRL001.ReqIdCFTop = objPRO001.ReqIdCFTop;
                objPRL001.ReqIdCFMiddle = objPRO001.ReqIdCFMiddle;
                objPRL001.ReqIdCFBottom = objPRO001.ReqIdCFBottom;
                objPRL001.ActReqIdCFTop = objPRO001.ActReqIdCFTop;
                objPRL001.ActReqIdCFMiddle = objPRO001.ActReqIdCFMiddle;
                objPRL001.ActReqIdCFBottom = objPRO001.ActReqIdCFBottom;
                objPRL001.ReqODCFTop = objPRO001.ReqODCFTop;
                objPRL001.ReqODCFTopMiddle = objPRO001.ReqODCFTopMiddle;
                objPRL001.ReqODCFTopBottom = objPRO001.ReqODCFTopBottom;
                objPRL001.ActReqODCFTop = objPRO001.ActReqODCFTop;
                objPRL001.ActReqODCFTopMiddle = objPRO001.ActReqODCFTopMiddle;
                objPRL001.ActReqODCFTopBottom = objPRO001.ActReqODCFTopBottom;
                objPRL001.ActualAt0 = objPRO001.ActualAt0;
                objPRL001.ActualAt90 = objPRO001.ActualAt90;
                objPRL001.ActualAt180 = objPRO001.ActualAt180;
                objPRL001.ActualAt270 = objPRO001.ActualAt270;
                objPRL001.SeamNo1 = objPRO001.SeamNo1;
                objPRL001.SeamNo1Required = objPRO001.SeamNo1Required;
                objPRL001.SeamNo1Actual = objPRO001.SeamNo1Actual;
                objPRL001.SeamNo1Remark = objPRO001.SeamNo1Remark;
                objPRL001.SeamNo2 = objPRO001.SeamNo2;
                objPRL001.SeamNo2Required = objPRO001.SeamNo2Required;
                objPRL001.SeamNo2Actual = objPRO001.SeamNo2Actual;
                objPRL001.SeamNo2Remark = objPRO001.SeamNo2Remark;
                objPRL001.SeamNo3 = objPRO001.SeamNo3;
                objPRL001.SeamNo3Required = objPRO001.SeamNo3Required;
                objPRL001.SeamNo3Actual = objPRO001.SeamNo3Actual;
                objPRL001.SeamNo3Remark = objPRO001.SeamNo3Remark;
                objPRL001.NoteLine1 = objPRO001.NoteLine1;
                objPRL001.NoteLine2_1 = objPRO001.NoteLine2_1;
                objPRL001.NoteLine2_2 = objPRO001.NoteLine2_2;
                objPRL001.NoteLine3 = objPRO001.NoteLine3;
                objPRL001.NoteLine4 = objPRO001.NoteLine4;
                objPRL001.NoteLine5 = objPRO001.NoteLine5;
                objPRL001.NoteLine6 = objPRO001.NoteLine6;
                objPRL001.NoteLine7 = objPRO001.NoteLine7;
                objPRL001.NoteLine9 = objPRO001.NoteLine9;
                objPRL001.NoteLine10 = objPRO001.NoteLine10;
                objPRL001.NoteLine11 = objPRO001.NoteLine11;
                objPRL001.NoteLine12 = objPRO001.NoteLine12;
                objPRL001.NoteLine13 = objPRO001.NoteLine13;
                objPRL001.NoteLine14 = objPRO001.NoteLine14;
                objPRL001.ReqTopID = objPRO001.ReqTopID;
                objPRL001.ActTopID1 = objPRO001.ActTopID1;
                objPRL001.ActTopID2 = objPRO001.ActTopID2;
                objPRL001.ActTopID3 = objPRO001.ActTopID3;
                objPRL001.ActTopID4 = objPRO001.ActTopID4;
                objPRL001.ActTopID5 = objPRO001.ActTopID5;
                objPRL001.ActTopID6 = objPRO001.ActTopID6;
                objPRL001.ActTopID7 = objPRO001.ActTopID7;
                objPRL001.ActTopID8 = objPRO001.ActTopID8;
                objPRL001.ActTopID9 = objPRO001.ActTopID9;
                objPRL001.ReqBotID = objPRO001.ReqBotID;
                objPRL001.ActBottomID1 = objPRO001.ActBottomID1;
                objPRL001.ActBottomID2 = objPRO001.ActBottomID2;
                objPRL001.ActBottomID3 = objPRO001.ActBottomID3;
                objPRL001.ActBottomID4 = objPRO001.ActBottomID4;
                objPRL001.ActBottomID5 = objPRO001.ActBottomID5;
                objPRL001.ActBottomID6 = objPRO001.ActBottomID6;
                objPRL001.ActBottomID7 = objPRO001.ActBottomID7;
                objPRL001.ActBottomID8 = objPRO001.ActBottomID8;
                objPRL001.ActBottomID9 = objPRO001.ActBottomID9;
                objPRL001.Remarks = objPRO001.Remarks;
                objPRL001.ProfileMeasurementTol = objPRO001.ProfileMeasurementTol;
                objPRL001.ProfileMeasurementSeamNo1 = objPRO001.ProfileMeasurementSeamNo1;
                objPRL001.ProfileMeasurementSeamNo2 = objPRO001.ProfileMeasurementSeamNo2;
                objPRL001.ProfileMeasurementSeamNo3 = objPRO001.ProfileMeasurementSeamNo3;
                objPRL001.TopPeakIn1 = objPRO001.TopPeakIn1;
                objPRL001.TopPeakIn2 = objPRO001.TopPeakIn2;
                objPRL001.TopPeakIn3 = objPRO001.TopPeakIn3;
                objPRL001.TopPeakOut1 = objPRO001.TopPeakOut1;
                objPRL001.TopPeakOut2 = objPRO001.TopPeakOut2;
                objPRL001.TopPeakOut3 = objPRO001.TopPeakOut3;
                objPRL001.MidPeakIn1 = objPRO001.MidPeakIn1;
                objPRL001.MidPeakIn2 = objPRO001.MidPeakIn2;
                objPRL001.MidPeakIn3 = objPRO001.MidPeakIn3;
                objPRL001.MidPeakOut1 = objPRO001.MidPeakOut1;
                objPRL001.MidPeakOut2 = objPRO001.MidPeakOut2;
                objPRL001.MidPeakOut3 = objPRO001.MidPeakOut3;
                objPRL001.BottomPeakIn1 = objPRO001.BottomPeakIn1;
                objPRL001.BottomPeakIn2 = objPRO001.BottomPeakIn2;
                objPRL001.BottomPeakIn3 = objPRO001.BottomPeakIn3;
                objPRL001.BottomPeakOut1 = objPRO001.BottomPeakOut1;
                objPRL001.BottomPeakOut2 = objPRO001.BottomPeakOut2;
                objPRL001.BottomPeakOut3 = objPRO001.BottomPeakOut3;
                objPRL001.ProfileMeasurementRemarks = objPRO001.ProfileMeasurementRemarks;
                objPRL001.ChordLengthReq = objPRO001.ChordLengthReq;
                objPRL001.ChordLengthAct = objPRO001.ChordLengthAct;
                objPRL001.RadiusReq = objPRO001.RadiusReq;
                objPRL001.RadiusAct = objPRO001.RadiusAct;
                objPRL001.GapAllowable1 = objPRO001.GapAllowable1;
                objPRL001.GapAllowable2 = objPRO001.GapAllowable2;
                objPRL001.GapAct = objPRO001.GapAct;
                objPRL001.EValueTemplateInspectionRemarks = objPRO001.EValueTemplateInspectionRemarks;
                objPRL001.InspectionRemark = objPRO001.InspectionRemark;
                objPRL001.WidthRequiredMax = objPRO001.WidthRequiredMax;
                objPRL001.WidthRequiredMin = objPRO001.WidthRequiredMin;
                objPRL001.DepthRequriedMax = objPRO001.DepthRequriedMax;
                objPRL001.DepthRequriedMin = objPRO001.DepthRequriedMin;
                objPRL001.Note1 = objPRO001.Note1;
                objPRL001.Note2 = objPRO001.Note2;
                objPRL001.Note3 = objPRO001.Note3;
                #endregion

                #region Copy data from PRO002 to PRL002
                var lstPRO002 = objPRO001.PRO002.ToList();
                if (lstPRO002.Count > 0)
                {
                    List<PRL002> lstPRL002 = lstPRO002.Select(c => new PRL002()
                    {
                        HeaderId = objPRL001.HeaderId,
                        SpotNo = c.SpotNo,
                        SpotWidth = c.SpotWidth,
                        Remarks = c.Remarks,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    }).ToList();

                    db.PRL002.AddRange(lstPRL002);
                }
                #endregion

                #region Copy data from PRO003 to PRL003
                var lstPRO003 = objPRO001.PRO003.ToList();
                if (lstPRO003.Count > 0)
                {
                    List<PRL003> lstPRL003 = lstPRO003.Select(c => new PRL003()
                    {
                        HeaderId = objPRL001.HeaderId,
                        SpotNo = c.SpotNo,
                        SpotDepth = c.SpotDepth,
                        Remarks = c.Remarks,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    }).ToList();

                    db.PRL003.AddRange(lstPRL003);
                }
                #endregion

                db.SaveChanges();

            }

            return objPRL001;

        }

        public PRL005 CopyData_PROTOCOL002(PRO005 objPRO005, PRL005 objPRL005)
        {
            if (objPRO005 != null)
            {
                #region Copy data from PRO005 to PRL005

                objPRL005.ProtocolNo = objPRO005.ProtocolNo;
                objPRL005.IdentificationMarking = objPRO005.IdentificationMarking;
                objPRL005.PtcRequired = objPRO005.PtcRequired;
                objPRL005.ITNo = objPRO005.ITNo;
                objPRL005.HeatNo = objPRO005.HeatNo;
                objPRL005.WepUtClear = objPRO005.WepUtClear;
                objPRL005.WepMtClear = objPRO005.WepMtClear;
                objPRL005.WepPtClear = objPRO005.WepPtClear;
                objPRL005.WepCuso4Clear = objPRO005.WepCuso4Clear;
                objPRL005.ExtentOfRT = objPRO005.ExtentOfRT;
                objPRL005.ExtentOfUT = objPRO005.ExtentOfUT;
                objPRL005.WEPAngleis = objPRO005.WEPAngleis;
                objPRL005.WEPAngleos = objPRO005.WEPAngleos;
                objPRL005.TacksFreeFromCrack = objPRO005.TacksFreeFromCrack;
                objPRL005.RequriedRootGap = objPRO005.RequriedRootGap;
                objPRL005.RequriedRootFace = objPRO005.RequriedRootFace;
                objPRL005.RequiredRemark = objPRO005.RequiredRemark;
                objPRL005.ActMaxRootGap = objPRO005.ActMaxRootGap;
                objPRL005.ActMaxRootFace = objPRO005.ActMaxRootFace;
                objPRL005.ActMaxRemark = objPRO005.ActMaxRemark;
                objPRL005.ActMinRootGap = objPRO005.ActMinRootGap;
                objPRL005.ActMinRootFace = objPRO005.ActMinRootFace;
                objPRL005.ActMinRemark = objPRO005.ActMinRemark; ;
                objPRL005.TotalHeightRequired = objPRO005.TotalHeightRequired;
                objPRL005.HeightAt0 = objPRO005.HeightAt0;
                objPRL005.HeightAt45 = objPRO005.HeightAt45;
                objPRL005.HeightAt90 = objPRO005.HeightAt90;
                objPRL005.HeightAt135 = objPRO005.HeightAt135; ;
                objPRL005.HeightAt180 = objPRO005.HeightAt180;
                objPRL005.HeightAt225 = objPRO005.HeightAt225;
                objPRL005.HeightAt270 = objPRO005.HeightAt270;
                objPRL005.HeightAt315 = objPRO005.HeightAt315;
                objPRL005.TTLToTopShellWEPRequiredLength = objPRO005.TTLToTopShellWEPRequiredLength;
                objPRL005.Top0 = objPRO005.Top0;
                objPRL005.Top45 = objPRO005.Top45;
                objPRL005.Top90 = objPRO005.Top90;
                objPRL005.Top135 = objPRO005.Top135;
                objPRL005.Top180 = objPRO005.Top180;
                objPRL005.Top225 = objPRO005.Top225;
                objPRL005.Top270 = objPRO005.Top270;
                objPRL005.Top315 = objPRO005.Top315;
                objPRL005.Note2 = objPRO005.Note2;
                objPRL005.Note4 = objPRO005.Note4;
                objPRL005.Note51 = objPRO005.Note51;
                objPRL005.Note52 = objPRO005.Note52;
                objPRL005.Note6 = objPRO005.Note6;
                objPRL005.Note7 = objPRO005.Note7;
                objPRL005.Note8 = objPRO005.Note8;
                objPRL005.Note9 = objPRO005.Note9;
                objPRL005.Note10 = objPRO005.Note10;
                objPRL005.AllowableOffset = objPRO005.AllowableOffset;
                objPRL005.AlignmentReadingsRemark = objPRO005.AlignmentReadingsRemark;
                objPRL005.CladStripingWidthMax = objPRO005.CladStripingWidthMax;
                objPRL005.CladStripingWidthMin = objPRO005.CladStripingWidthMin;
                objPRL005.CladStripingDepthMax = objPRO005.CladStripingDepthMax;
                objPRL005.CladStripingDepthMin = objPRO005.CladStripingDepthMin;
                objPRL005.Note1 = objPRO005.Note1;
                objPRL005.FNote1 = objPRO005.FNote1;
                objPRL005.FNote2 = objPRO005.FNote2;
                objPRL005.FNote3 = objPRO005.FNote3;
                objPRL005.SeamNoPunchingATEvery900MM = objPRO005.SeamNoPunchingATEvery900MM;
                objPRL005.TopRemarks = objPRO005.TopRemarks;
                objPRL005.CreatedBy = objClsLoginInfo.UserName;
                objPRL005.CreatedOn = DateTime.Now;

                #endregion

                #region Copy data from PRO006 to PRL006
                var lstPRO006 = objPRO005.PRO006.ToList();
                if (lstPRO006.Count > 0)
                {
                    List<PRL006> lstPRL006 = lstPRO006.Select(c => new PRL006()
                    {
                        HeaderId = objPRL005.HeaderId,
                        SpotNo = c.SpotNo,
                        Offset = c.Offset,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    }).ToList();

                    db.PRL006.AddRange(lstPRL006);
                }
                #endregion

                #region Copy data from PRO007 to PRL007
                var lstPRO007 = objPRO005.PRO007.ToList();
                if (lstPRO007.Count > 0)
                {
                    List<PRL007> lstPRL007 = lstPRO007.Select(c => new PRL007()
                    {
                        HeaderId = objPRL005.HeaderId,
                        SeamNo = c.SeamNo,
                        ReqOrien = c.ReqOrien,
                        ReqArcLength1 = c.ReqArcLength1,
                        ReqArcLength2 = c.ReqArcLength2,
                        ReqArcLength3 = c.ReqArcLength3,
                        ActualArc1 = c.ActualArc1,
                        ActualArc2 = c.ActualArc2,
                        ActualArc3 = c.ActualArc3,
                        Remark = c.Remark,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    }).ToList();

                    db.PRL007.AddRange(lstPRL007);
                }
                #endregion

                #region Copy data from PRO008 to PRL008
                var lstPRO008 = objPRO005.PRO008.ToList();
                if (lstPRO008.Count > 0)
                {
                    List<PRL008> lstPRL008 = lstPRO008.Select(c => new PRL008()
                    {
                        HeaderId = objPRL005.HeaderId,
                        Location = c.Location,
                        Remark = c.Remark,
                        At0 = c.At0,
                        At90 = c.At90,
                        At180 = c.At180,
                        At270 = c.At270,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    }).ToList();

                    db.PRL008.AddRange(lstPRL008);
                }
                #endregion

                #region Copy data from PRO009_1 to PRL009_1
                var lstPRO009_1 = objPRO005.PRO009_1.ToList();
                if (lstPRO009_1.Count > 0)
                {
                    List<PRL009_1> lstPRL009_1 = lstPRO009_1.Select(c => new PRL009_1()
                    {
                        HeaderId = objPRL005.HeaderId,
                        SpotNo = c.SpotNo,
                        Width = c.Width,
                        Remarks = c.Remarks,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    }).ToList();

                    db.PRL009_1.AddRange(lstPRL009_1);
                }
                #endregion

                #region Copy data from PRO009_2 to PRL009_2
                var lstPRO009_2 = objPRO005.PRO009_2.ToList();
                if (lstPRO009_2.Count > 0)
                {
                    List<PRL009_2> lstPRL009_2 = lstPRO009_2.Select(c => new PRL009_2()
                    {
                        HeaderId = objPRL005.HeaderId,
                        SpotNo = c.SpotNo,
                        Depth = c.Depth,
                        Remarks = c.Remarks,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    }).ToList();

                    db.PRL009_2.AddRange(lstPRL009_2);
                }
                #endregion

                db.SaveChanges();
            }
            return objPRL005;
        }

        //#region Maintain Protocol Detail Page
        public int LinkProtocolDetailWithStage(string ProtocolType, string ProtocolNo)
        {
            int returnValue = 0;
            if (ProtocolType == clsImplementationEnum.ProtocolType.SETUP_AND_DIMENSION_REPORT_FOR_ROLLED_SHELL_LONGITUDINAL_SEAM.GetStringValue())
            {
                #region Add New Entry in PRO001 For PROTOCOL001
                PRO001 objPRO001 = new PRO001();
                objPRO001.ProtocolNo = string.Empty;
                objPRO001.CreatedBy = objClsLoginInfo.UserName;
                objPRO001.CreatedOn = DateTime.Now;
                db.PRO001.Add(objPRO001);
                db.SaveChanges();
                #endregion

                returnValue = objPRO001.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.SETUP_AND_DIMENSION_REPORT_FOR_CIRCUMFERENTIAL_SEAM.GetStringValue())
            {
                #region Add New Entry in PRO005 For PROTOCOL002
                PRO005 objPRO005 = new PRO005();
                objPRO005.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO005.CreatedBy = objClsLoginInfo.UserName;
                objPRO005.CreatedOn = DateTime.Now;
                db.PRO005.Add(objPRO005);
                db.SaveChanges();
                #endregion

                returnValue = objPRO005.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.SETUP_INSPECTION_REPORT_FOR_FORMED.GetStringValue())
            {
                #region Add New Entry in PRO010 For PROTOCOL003
                PRO010 objPRO010 = new PRO010();
                objPRO010.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO010.CreatedBy = objClsLoginInfo.UserName;
                objPRO010.CreatedOn = DateTime.Now;
                db.PRO010.Add(objPRO010);
                db.SaveChanges();
                #endregion

                returnValue = objPRO010.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.SETUP_AND_DIMENSION_REPORT_FOR_NOZZLE.GetStringValue())
            {
                #region Add New Entry in PRO015 For PROTOCOL004
                PRO015 objPRO015 = new PRO015();
                objPRO015.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO015.CreatedBy = objClsLoginInfo.UserName;
                objPRO015.CreatedOn = DateTime.Now;
                db.PRO015.Add(objPRO015);
                db.SaveChanges();
                #endregion

                returnValue = objPRO015.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_REPORT_FOR_ROLLED_SHELL_LONGITUDINAL_SEAM.GetStringValue())
            {
                #region Add New Entry in PRO020 For PROTOCOL005
                PRO020 objPRO020 = new PRO020();
                objPRO020.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO020.CreatedBy = objClsLoginInfo.UserName;
                objPRO020.CreatedOn = DateTime.Now;
                db.PRO020.Add(objPRO020);
                db.SaveChanges();
                #endregion

                returnValue = objPRO020.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_INSPECTION_REPORT_FOR_FORMED_D_END_CONE.GetStringValue())
            {
                #region Add New Entry in PRO025 For PROTOCOL006
                PRO025 objPRO025 = new PRO025();
                objPRO025.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO025.CreatedBy = objClsLoginInfo.UserName;
                objPRO025.CreatedOn = DateTime.Now;
                db.PRO025.Add(objPRO025);
                db.SaveChanges();
                #endregion

                returnValue = objPRO025.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.WELD_VISUAL_AND_DIMENSION_REPORT_FOR_NOZZLE.GetStringValue())
            {
                #region Add New Entry in PRO030 For PROTOCOL007
                PRO030 objPRO030 = new PRO030();
                objPRO030.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO030.CreatedBy = objClsLoginInfo.UserName;
                objPRO030.CreatedOn = DateTime.Now;
                db.PRO030.Add(objPRO030);
                db.SaveChanges();
                #endregion

                returnValue = objPRO030.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORT_FOR_SQUARE_TYPE.GetStringValue())
            {
                #region Add New Entry in PRO035 For PROTOCOL008
                PRO035 objPRO035 = new PRO035();
                objPRO035.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO035.CreatedBy = objClsLoginInfo.UserName;
                objPRO035.CreatedOn = DateTime.Now;
                db.PRO035.Add(objPRO035);
                db.SaveChanges();
                #endregion

                returnValue = objPRO035.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_REPORT_FOR_CIRCUMFERENTIAL_SEAM.GetStringValue())
            {
                #region Add New Entry in PRO040 For PROTOCOL009
                PRO040 objPRO040 = new PRO040();
                objPRO040.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO040.CreatedBy = objClsLoginInfo.UserName;
                objPRO040.CreatedOn = DateTime.Now;
                db.PRO040.Add(objPRO040);
                db.SaveChanges();
                #endregion

                returnValue = objPRO040.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.SETUP_AND_DIMENSION_REPORT_FOR_FORMED_ELBOW_LONG_SEAMS.GetStringValue())
            {
                #region Add New Entry in PRO045 For PROTOCOL010
                PRO045 objPRO045 = new PRO045();
                objPRO045.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO045.CreatedBy = objClsLoginInfo.UserName;
                objPRO045.CreatedOn = DateTime.Now;
                db.PRO045.Add(objPRO045);
                db.SaveChanges();
                #endregion

                returnValue = objPRO045.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.WELD_VISUAL_AND_DIMENSION_REPORT_FOR_SPOOL_TYPE1.GetStringValue())
            {
                #region Add New Entry in PRO050 For PROTOCOL011
                PRO050 objPRO050 = new PRO050();
                objPRO050.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO050.CreatedBy = objClsLoginInfo.UserName;
                objPRO050.CreatedOn = DateTime.Now;
                db.PRO050.Add(objPRO050);
                db.SaveChanges();
                #endregion

                returnValue = objPRO050.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.WELD_VISUAL_AND_DIMENSION_REPORT_FOR_SPOOL_TYPE2.GetStringValue())
            {
                #region Add New Entry in PRO055 For PROTOCOL012
                PRO055 objPRO055 = new PRO055();
                objPRO055.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO055.CreatedBy = objClsLoginInfo.UserName;
                objPRO055.CreatedOn = DateTime.Now;
                db.PRO055.Add(objPRO055);
                db.SaveChanges();
                #endregion

                returnValue = objPRO055.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue())
            {
                #region Add New Entry in PRO060 For PROTOCOL013
                PRO060 objPRO060 = new PRO060();
                objPRO060.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO060.CreatedBy = objClsLoginInfo.UserName;
                objPRO060.CreatedOn = DateTime.Now;
                db.PRO060.Add(objPRO060);
                db.SaveChanges();
                #endregion

                returnValue = objPRO060.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_BEFORE_OVERLAY_FOR_SQUARE_TYPE_NUB.GetStringValue())
            {
                #region Add New Entry in PRO065 For PROTOCOL014
                PRO065 objPRO065 = new PRO065();
                objPRO065.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO065.CreatedBy = objClsLoginInfo.UserName;
                objPRO065.CreatedOn = DateTime.Now;
                db.PRO065.Add(objPRO065);
                db.SaveChanges();
                #endregion

                returnValue = objPRO065.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_AFTER_OVERLAY_FOR_TAPER_TYPE_NUB.GetStringValue())
            {
                #region Add New Entry in PRO070 For PROTOCOL015
                PRO070 objPRO070 = new PRO070();
                objPRO070.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO070.CreatedBy = objClsLoginInfo.UserName;
                objPRO070.CreatedOn = DateTime.Now;
                db.PRO070.Add(objPRO070);
                db.SaveChanges();
                #endregion

                returnValue = objPRO070.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.TSR_DIMENSION_INSPECTION_REPORTS.GetStringValue())
            {
                #region Add New Entry in PRO075 For PROTOCOL016
                PRO075 objPRO075 = new PRO075();
                objPRO075.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO075.CreatedBy = objClsLoginInfo.UserName;
                objPRO075.CreatedOn = DateTime.Now;
                db.PRO075.Add(objPRO075);
                db.SaveChanges();
                #endregion

                returnValue = objPRO075.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.WELD_VISUAL_AND_DIMENSION_INSPECTION_REPORT_FOR_SUPPORT_BRACKET.GetStringValue())
            {
                #region Add New Entry in PRO080 For PROTOCOL017
                PRO080 objPRO080 = new PRO080();
                objPRO080.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO080.CreatedBy = objClsLoginInfo.UserName;
                objPRO080.CreatedOn = DateTime.Now;
                db.PRO080.Add(objPRO080);
                db.SaveChanges();
                #endregion

                returnValue = objPRO080.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.RECORD_FOR_TEMPORARY_ATTACHMENTS_LOCATION.GetStringValue())
            {
                #region Add New Entry in PRO085 For PROTOCOL018
                PRO085 objPRO085 = new PRO085();
                objPRO085.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO085.CreatedBy = objClsLoginInfo.UserName;
                objPRO085.CreatedOn = DateTime.Now;
                db.PRO085.Add(objPRO085);
                db.SaveChanges();
                #endregion

                returnValue = objPRO085.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_INSPECTION_OF_SKIRT_TEMPLATE.GetStringValue())
            {
                #region Add New Entry in PRO090 For PROTOCOL019
                PRO090 objPRO090 = new PRO090();
                objPRO090.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO090.CreatedBy = objClsLoginInfo.UserName;
                objPRO090.CreatedOn = DateTime.Now;
                db.PRO090.Add(objPRO090);
                db.SaveChanges();
                #endregion

                returnValue = objPRO090.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_INSPECTION_OF_SKIRT_ASSEMBLY.GetStringValue())
            {

                #region Add New Entry in PRO095 For PROTOCOL020
                PRO095 objPRO095 = new PRO095();
                objPRO095.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO095.CreatedBy = objClsLoginInfo.UserName;
                objPRO095.CreatedOn = DateTime.Now;
                db.PRO095.Add(objPRO095);
                db.SaveChanges();
                #endregion

                returnValue = objPRO095.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_OF_HARDNESS_TEST.GetStringValue())
            {

                #region Add New Entry in PRO100 For PROTOCOL021
                PRO100 objPRO100 = new PRO100();
                objPRO100.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO100.CreatedBy = objClsLoginInfo.UserName;
                objPRO100.CreatedOn = DateTime.Now;
                db.PRO100.Add(objPRO100);
                db.SaveChanges();
                #endregion

                returnValue = objPRO100.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.AIR_TEST_OF_REINFORCEMENT_PADS_OF_NOZZLE.GetStringValue())
            {

                #region Add New Entry in PRO105 For PROTOCOL022
                PRO105 objPRO105 = new PRO105();
                objPRO105.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO105.CreatedBy = objClsLoginInfo.UserName;
                objPRO105.CreatedOn = DateTime.Now;
                db.PRO105.Add(objPRO105);
                db.SaveChanges();
                #endregion

                returnValue = objPRO105.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.HELIUM_LEAK_TEST_REPORT.GetStringValue())
            {

                #region Add New Entry in PRO110 For PROTOCOL023
                PRO110 objPRO110 = new PRO110();
                objPRO110.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO110.CreatedBy = objClsLoginInfo.UserName;
                objPRO110.CreatedOn = DateTime.Now;
                db.PRO110.Add(objPRO110);
                db.SaveChanges();
                #endregion

                returnValue = objPRO110.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.HYDROSTATIC_TEST_REPORT.GetStringValue())
            {

                #region Add New Entry in PRO115 For PROTOCOL024
                PRO115 objPRO115 = new PRO115();
                objPRO115.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO115.CreatedBy = objClsLoginInfo.UserName;
                objPRO115.CreatedOn = DateTime.Now;
                db.PRO115.Add(objPRO115);
                db.SaveChanges();
                #endregion

                returnValue = objPRO115.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.PNEUMATIC_TEST_REPORT.GetStringValue())
            {

                #region Add New Entry in PRO120 For PROTOCOL025
                PRO120 objPRO120 = new PRO120();
                objPRO120.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO120.CreatedBy = objClsLoginInfo.UserName;
                objPRO120.CreatedOn = DateTime.Now;
                db.PRO120.Add(objPRO120);
                db.SaveChanges();
                #endregion

                returnValue = objPRO120.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_N2_FILLING_INSPECTION.GetStringValue())
            {

                #region Add New Entry in PRO125 For PROTOCOL026
                PRO125 objPRO125 = new PRO125();
                objPRO125.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO125.CreatedBy = objClsLoginInfo.UserName;
                objPRO125.CreatedOn = DateTime.Now;
                db.PRO125.Add(objPRO125);
                db.SaveChanges();
                #endregion

                returnValue = objPRO125.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_POSITIVE_MATERIAL_IDENTIFICATION.GetStringValue())
            {
                #region Add New Entry in PRO130 For PROTOCOL027
                PRO130 objPRO130 = new PRO130();
                objPRO130.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO130.CreatedBy = objClsLoginInfo.UserName;
                objPRO130.CreatedOn = DateTime.Now;
                db.PRO130.Add(objPRO130);
                db.SaveChanges();
                #endregion

                returnValue = objPRO130.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_OF_FINAL_MACHINING_INSPECTION_OF_TUBESHEET.GetStringValue())
            {
                #region Add New Entry in PRO135 For PROTOCOL028
                PRO135 objPRO135 = new PRO135();
                objPRO135.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO135.CreatedBy = objClsLoginInfo.UserName;
                objPRO135.CreatedOn = DateTime.Now;
                db.PRO135.Add(objPRO135);
                db.SaveChanges();
                #endregion

                returnValue = objPRO135.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_OF_ARM_REVIEW.GetStringValue())
            {
                #region Add New Entry in PRO140 For PROTOCOL029
                PRO140 objPRO140 = new PRO140();
                objPRO140.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO140.CreatedBy = objClsLoginInfo.UserName;
                objPRO140.CreatedOn = DateTime.Now;
                db.PRO140.Add(objPRO140);
                db.SaveChanges();
                #endregion

                returnValue = objPRO140.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_INSPECTION_REPORT_FOR_OVERLAY_ON_SHELL_DEND.GetStringValue())
            {
                #region Add New Entry in PRO145 For PROTOCOL030
                PRO145 objPRO145 = new PRO145();
                objPRO145.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO145.CreatedBy = objClsLoginInfo.UserName;
                objPRO145.CreatedOn = DateTime.Now;
                db.PRO145.Add(objPRO145);
                db.SaveChanges();
                #endregion

                returnValue = objPRO145.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_VISUAL_AND_DIM_OF_TAILING_LUG.GetStringValue())
            {
                #region Add New Entry in PRO150 For PROTOCOL031
                PRO150 objPRO150 = new PRO150();
                objPRO150.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO150.CreatedBy = objClsLoginInfo.UserName;
                objPRO150.CreatedOn = DateTime.Now;
                db.PRO150.Add(objPRO150);
                db.SaveChanges();
                #endregion

                returnValue = objPRO150.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.OVERLAY_THICKNESS_MEASUREMENT_REPORT_FOR_SHELL_DEND_LONG_SEAM_CIRC_SEAM.GetStringValue())
            {
                #region Add New Entry in PRO155 For PROTOCOL032
                PRO155 objPRO155 = new PRO155();
                objPRO155.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO155.CreatedBy = objClsLoginInfo.UserName;
                objPRO155.CreatedOn = DateTime.Now;
                db.PRO155.Add(objPRO155);
                db.SaveChanges();
                #endregion

                returnValue = objPRO155.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.OVERLAY_THICKNESS_MEASUREMENT_REPORT_FOR_NOZZLE_PIPE_ELBOW.GetStringValue())
            {
                #region Add New Entry in PRO160 For PROTOCOL033
                PRO160 objPRO160 = new PRO160();
                objPRO160.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO160.CreatedBy = objClsLoginInfo.UserName;
                objPRO160.CreatedOn = DateTime.Now;
                db.PRO160.Add(objPRO160);
                db.SaveChanges();
                #endregion

                returnValue = objPRO160.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.THICKNESS_REPORT_FOR_EQUIPMENT.GetStringValue())
            {
                #region Add New Entry in PRO165 For PROTOCOL034
                PRO165 objPRO165 = new PRO165();
                objPRO165.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO165.CreatedBy = objClsLoginInfo.UserName;
                objPRO165.CreatedOn = DateTime.Now;
                db.PRO165.Add(objPRO165);
                db.SaveChanges();
                #endregion

                returnValue = objPRO165.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.NOZZLE_CUTOUT_MARKING_ON_SHELL_CONE_DEND.GetStringValue())
            {
                #region Add New Entry in PRO170 For PROTOCOL035
                PRO170 objPRO170 = new PRO170();
                objPRO170.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO170.CreatedBy = objClsLoginInfo.UserName;
                objPRO170.CreatedOn = DateTime.Now;
                db.PRO170.Add(objPRO170);
                db.SaveChanges();
                #endregion

                returnValue = objPRO170.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.SETUP_AND_DIMENSION_REPORT_FOR_ROLLED_SHELL_LONGITUDINAL_SEAM_V2.GetStringValue())
            {
                #region Add New Entry in PRO175 For PROTOCOL036
                PRO175 objPRO175 = new PRO175();
                objPRO175.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO175.CreatedBy = objClsLoginInfo.UserName;
                objPRO175.CreatedOn = DateTime.Now;

                #region  CIRCUMFERENCE MEASUREMENT
                objPRO175.PRO178.Add(new PRO178
                {
                    Location = "TOP",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                objPRO175.PRO178.Add(new PRO178
                {
                    Location = "MIDDLE",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRO175.PRO178.Add(new PRO178
                {
                    Location = "BOTTOM",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region TOTAL SHELL HEIGHT
                objPRO175.PRO179.Add(new PRO179
                {
                    Orientation = "AT 0°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRO175.PRO179.Add(new PRO179
                {
                    Orientation = "AT 90°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRO175.PRO179.Add(new PRO179
                {
                    Orientation = "AT 180°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRO175.PRO179.Add(new PRO179
                {
                    Orientation = "AT 270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region OUT OF ROUNDNESS 
                objPRO175.PRO179_2.Add(new PRO179_2
                {
                    Orientation = "0°-180° ",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRO175.PRO179_2.Add(new PRO179_2
                {
                    Orientation = "22.5°-202.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRO175.PRO179_2.Add(new PRO179_2
                {
                    Orientation = "45°-225°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRO175.PRO179_2.Add(new PRO179_2
                {
                    Orientation = "67.5°-247.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRO175.PRO179_2.Add(new PRO179_2
                {
                    Orientation = "90°-270°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRO175.PRO179_2.Add(new PRO179_2
                {
                    Orientation = "112.5°-292.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRO175.PRO179_2.Add(new PRO179_2
                {
                    Orientation = "135°-315°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                objPRO175.PRO179_2.Add(new PRO179_2
                {
                    Orientation = "157.5°-337.5°",
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                db.PRO175.Add(objPRO175);
                db.SaveChanges();

                #endregion

                returnValue = objPRO175.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.SETUP_AND_DIMENSION_REPORT_FOR_CIRCUMFERENTIAL_SEAM_V2.GetStringValue())
            {
                #region Add New Entry in PRO180 For PROTOCOL037
                PRO180 objPRO180 = new PRO180();
                objPRO180.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO180.CreatedBy = objClsLoginInfo.UserName;
                objPRO180.CreatedOn = DateTime.Now;
                db.PRO180.Add(objPRO180);
                db.SaveChanges();

                #endregion

                returnValue = objPRO180.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.SETUP_INSPECTION_REPORT_FOR_FORMED_HEMISPHERICAL_DEND_WITHOUT_CROWN_AND_TIER.GetStringValue())
            {
                #region Add New Entry in PRO185 For PROTOCOL038
                PRO185 objPRO185 = new PRO185();
                objPRO185.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO185.CreatedBy = objClsLoginInfo.UserName;
                objPRO185.CreatedOn = DateTime.Now;
                db.PRO185.Add(objPRO185);
                db.SaveChanges();

                #endregion

                returnValue = objPRO185.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.SETUP_INSPECTION_REPORT_FOR_FORMED_HEMISPHERICAL_DEND_WITH_CROWN_AND_TIER.GetStringValue())
            {
                #region Add New Entry in PRO190 For PROTOCOL039
                PRO190 objPRO190 = new PRO190();
                objPRO190.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO190.CreatedBy = objClsLoginInfo.UserName;
                objPRO190.CreatedOn = DateTime.Now;
                db.PRO190.Add(objPRO190);
                db.SaveChanges();

                #endregion

                returnValue = objPRO190.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.SETUP_INSPECTION_REPORT_FOR_FORMED_ELLIPSOIDAL_TORISPHERICAL_D_END_WITHOUT_CROWN_AND_TIER.GetStringValue())
            {
                #region Add New Entry in PRO195 For PROTOCOL040
                PRO195 objPRO195 = new PRO195();
                objPRO195.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO195.CreatedBy = objClsLoginInfo.UserName;
                objPRO195.CreatedOn = DateTime.Now;
                db.PRO195.Add(objPRO195);
                db.SaveChanges();
                #endregion

                returnValue = objPRO195.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.SETUP_INSPECTION_REPORT_FOR_FORMED_ELLIPSOIDAL_TORISPHERICAL_D_END_WITH_CROWN_AND_TIER.GetStringValue())
            {
                #region Add New Entry in PRO200 For PROTOCOL041
                PRO200 objPRO200 = new PRO200();
                objPRO200.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO200.CreatedBy = objClsLoginInfo.UserName;
                objPRO200.CreatedOn = DateTime.Now;
                db.PRO200.Add(objPRO200);
                db.SaveChanges();
                #endregion

                returnValue = objPRO200.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.SETUP_DIMENSION_REPORT_FOR_NOZZLE.GetStringValue())
            {
                #region Add New Entry in PRO205 For PROTOCOL042
                PRO205 objPRO205 = new PRO205();
                objPRO205.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO205.CreatedBy = objClsLoginInfo.UserName;
                objPRO205.CreatedOn = DateTime.Now;
                db.PRO205.Add(objPRO205);
                db.SaveChanges();
                #endregion

                returnValue = objPRO205.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_REPORT_FOR_ROLLED_SHELL_LONGITUDINAL_SEAM_V2.GetStringValue())
            {
                #region Add New Entry in PRO210 For PROTOCOL043
                PRO210 objPRO210 = new PRO210();
                objPRO210.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO210.CreatedBy = objClsLoginInfo.UserName;
                objPRO210.CreatedOn = DateTime.Now;
                db.PRO210.Add(objPRO210);
                db.SaveChanges();
                #endregion

                returnValue = objPRO210.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_REPORT_FOR_CIRCUMFERENTIAL_SEAM_V2.GetStringValue())
            {
                #region Add New Entry in PRO215 For PROTOCOL044
                PRO215 objPRO215 = new PRO215();
                objPRO215.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO215.CreatedBy = objClsLoginInfo.UserName;
                objPRO215.CreatedOn = DateTime.Now;
                db.PRO215.Add(objPRO215);
                db.SaveChanges();
                #endregion

                returnValue = objPRO215.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_INSPECTION_REPORT_FOR_FORMED_HEMISPHERICAL_D_END.GetStringValue())
            {
                #region Add New Entry in PRO220 For PROTOCOL045
                PRO220 objPRO220 = new PRO220();
                objPRO220.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO220.CreatedBy = objClsLoginInfo.UserName;
                objPRO220.CreatedOn = DateTime.Now;
                db.PRO220.Add(objPRO220);
                db.SaveChanges();
                #endregion

                returnValue = objPRO220.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_INSPECTION_REPORT_FOR_ELLIPSOIDAL_TORISPHERICAL_FORMED_D_END.GetStringValue())
            {
                #region Add New Entry in PRO225 For PROTOCOL046
                PRO225 objPRO225 = new PRO225();
                objPRO225.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO225.CreatedBy = objClsLoginInfo.UserName;
                objPRO225.CreatedOn = DateTime.Now;
                db.PRO225.Add(objPRO225);
                db.SaveChanges();
                #endregion

                returnValue = objPRO225.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.WELD_VISUAL_AND_DIMENSION_REPORT_FOR_NOZZLE_V2.GetStringValue())
            {
                #region Add New Entry in PRO230 For PROTOCOL047
                PRO230 objPRO230 = new PRO230();
                objPRO230.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO230.CreatedBy = objClsLoginInfo.UserName;
                objPRO230.CreatedOn = DateTime.Now;
                db.PRO230.Add(objPRO230);
                db.SaveChanges();
                #endregion

                returnValue = objPRO230.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.SETUP_AND_DIMENSION_REPORT_FOR_ATTACHMENT.GetStringValue())
            {
                #region Add New Entry in PRO235 For PROTOCOL048
                PRO235 objPRO235 = new PRO235();
                objPRO235.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO235.CreatedBy = objClsLoginInfo.UserName;
                objPRO235.CreatedOn = DateTime.Now;
                db.PRO235.Add(objPRO235);
                db.SaveChanges();
                #endregion

                returnValue = objPRO235.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_REPORT_FOR_ATTACHMENT.GetStringValue())
            {
                #region Add New Entry in PRO240 For PROTOCOL049
                PRO240 objPRO240 = new PRO240();
                objPRO240.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO240.CreatedBy = objClsLoginInfo.UserName;
                objPRO240.CreatedOn = DateTime.Now;
                db.PRO240.Add(objPRO240);
                db.SaveChanges();
                #endregion

                returnValue = objPRO240.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_REPORT_BEFORE_WELD_OVERALY_ON_SHELL.GetStringValue())
            {
                #region Add New Entry in PRO245 For PROTOCOL050
                PRO245 objPRO245 = new PRO245();
                objPRO245.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO245.CreatedBy = objClsLoginInfo.UserName;
                objPRO245.CreatedOn = DateTime.Now;
                db.PRO245.Add(objPRO245);
                db.SaveChanges();
                #endregion

                returnValue = objPRO245.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_REPORT_AFTER_WELD_OVERALY_ON_SHELL.GetStringValue())
            {
                #region Add New Entry in PRO250 For PROTOCOL051
                PRO250 objPRO250 = new PRO250();
                objPRO250.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO250.CreatedBy = objClsLoginInfo.UserName;
                objPRO250.CreatedOn = DateTime.Now;
                db.PRO250.Add(objPRO250);
                db.SaveChanges();
                #endregion

                returnValue = objPRO250.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.SETUP_AND_DIMENSION_REPORT_FOR_PEPE_TO_PIPE_PIEPE_TO_FLANGE_PIPE_TO_ELBOW_SEAM_PIPE_TO_FORGING_FORGING_TO_FLANGE_ELBOW_TO_FLANGE.GetStringValue())
            {
                #region Add New Entry in PRO255 For PROTOCOL052
                PRO255 objPRO255 = new PRO255();
                objPRO255.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO255.CreatedBy = objClsLoginInfo.UserName;
                objPRO255.CreatedOn = DateTime.Now;
                db.PRO255.Add(objPRO255);
                db.SaveChanges();
                #endregion

                returnValue = objPRO255.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_REPORT_FOR_PEPE_TO_PIPE_PIEPE_TO_FLANGE_PIPE_TO_ELBOW_SEAM_PIPE_TO_FORGING_FORGING_TO_FLANGE_ELBOW_TO_FLANGE.GetStringValue())
            {
                #region Add New Entry in PRO260 For PROTOCOL053
                PRO260 objPRO260 = new PRO260();
                objPRO260.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO260.CreatedBy = objClsLoginInfo.UserName;
                objPRO260.CreatedOn = DateTime.Now;
                db.PRO260.Add(objPRO260);
                db.SaveChanges();
                #endregion

                returnValue = objPRO260.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_SETUP_AND_DIMENSION_INSPECTION_OF_TSR.GetStringValue())
            {
                #region Add New Entry in PRO265 For PROTOCOL054
                PRO265 objPRO265 = new PRO265();
                objPRO265.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO265.CreatedBy = objClsLoginInfo.UserName;
                objPRO265.CreatedOn = DateTime.Now;
                db.PRO265.Add(objPRO265);
                db.SaveChanges();
                #endregion

                returnValue = objPRO265.HeaderId;
            }

            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_VISUAL_AND_DIMENSION_INSPECTION_OF_TSR.GetStringValue())
            {
                #region Add New Entry in PRO270 For PROTOCOL055
                PRO270 objPRO270 = new PRO270();
                objPRO270.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO270.CreatedBy = objClsLoginInfo.UserName;
                objPRO270.CreatedOn = DateTime.Now;
                db.PRO270.Add(objPRO270);
                db.SaveChanges();
                #endregion

                returnValue = objPRO270.HeaderId;
            }

            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_VISUAL_AND_DIMENSION_REPORT_AFTER_MACHINING_FOR_GASKET_FACE_AND_OTHER_DIMENSIONS_OF_NOZZLE.GetStringValue())
            {
                #region Add New Entry in PRO275 For PROTOCOL056
                PRO275 objPRO275 = new PRO275();
                objPRO275.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO275.CreatedBy = objClsLoginInfo.UserName;
                objPRO275.CreatedOn = DateTime.Now;
                db.PRO275.Add(objPRO275);
                db.SaveChanges();
                #endregion

                returnValue = objPRO275.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.BEFORE_OVERLAY_THICKNESS_MEASUREMENT_REPORT_FOR_SHELL_HEAD_LONG_SEAM_CIRC_SEAM.GetStringValue())
            {
                #region Add New Entry in PRO280 For PROTOCOL057
                PRO280 objPRO280 = new PRO280();
                objPRO280.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO280.CreatedBy = objClsLoginInfo.UserName;
                objPRO280.CreatedOn = DateTime.Now;
                db.PRO280.Add(objPRO280);
                db.SaveChanges();
                #endregion

                returnValue = objPRO280.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.OVERLAY_THICKNESS_MEASUREMENT_REPORT_FOR_SHELL_HEAD_LONG_SEAM_CIRC_SEAM.GetStringValue())
            {
                #region Add New Entry in PRO285 For PROTOCOL058
                PRO285 objPRO285 = new PRO285();
                objPRO285.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO285.CreatedBy = objClsLoginInfo.UserName;
                objPRO285.CreatedOn = DateTime.Now;
                db.PRO285.Add(objPRO285);
                db.SaveChanges();
                #endregion

                returnValue = objPRO285.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.SETUP_AND_DIMENSION_REPORT_FOR_LONGITUDINAL_SEAM_OF_RING_IN_SEGMENT_STAGE.GetStringValue())
            {
                #region Add New Entry in PRO290 For PROTOCOL059
                PRO290 objPRO290 = new PRO290();
                objPRO290.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO290.CreatedBy = objClsLoginInfo.UserName;
                objPRO290.CreatedOn = DateTime.Now;
                db.PRO290.Add(objPRO290);
                db.SaveChanges();
                #endregion

                returnValue = objPRO290.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_REPORT_FOR_LONGITUDINAL_SEAM_OF_RING.GetStringValue())
            {
                #region Add New Entry in PRO295 For PROTOCOL060
                PRO295 objPRO295 = new PRO295();
                objPRO295.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO295.CreatedBy = objClsLoginInfo.UserName;
                objPRO295.CreatedOn = DateTime.Now;
                db.PRO295.Add(objPRO295);
                db.SaveChanges();
                #endregion

                returnValue = objPRO295.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_SETUP_AND_DIMENSION_OF_TAILING_LUG.GetStringValue())
            {
                #region Add New Entry in PRO300 For PROTOCOL061
                PRO300 objPRO300 = new PRO300();
                objPRO300.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO300.CreatedBy = objClsLoginInfo.UserName;
                objPRO300.CreatedOn = DateTime.Now;
                db.PRO300.Add(objPRO300);
                db.SaveChanges();
                #endregion

                returnValue = objPRO300.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_VISUAL_AND_DIMENSION_OF_TAILING_LUG.GetStringValue())
            {
                #region Add New Entry in PRO305 For PROTOCOL062
                PRO305 objPRO305 = new PRO305();
                objPRO305.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO305.CreatedBy = objClsLoginInfo.UserName;
                objPRO305.CreatedOn = DateTime.Now;
                db.PRO305.Add(objPRO305);
                db.SaveChanges();
                #endregion

                returnValue = objPRO305.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.SETUP_AND_DIMENSION_REPORT_FOR_MTB_BLOCK_OR_PTC_LONGITUDINAL_SEAM.GetStringValue())
            {
                #region Add New Entry in PRO310 For PROTOCOL063
                PRO310 objPRO310 = new PRO310();
                objPRO310.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO310.CreatedBy = objClsLoginInfo.UserName;
                objPRO310.CreatedOn = DateTime.Now;
                db.PRO310.Add(objPRO310);
                db.SaveChanges();
                #endregion

                returnValue = objPRO310.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.SETUP_AND_DIMENSION_REPORT_FOR_LONGITUDINAL_SEAM_OF_TUBESHEET_AND_BAFFLE.GetStringValue())
            {
                #region Add New Entry in PRO315 For PROTOCOL064
                PRO315 objPRO315 = new PRO315();
                objPRO315.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO315.CreatedBy = objClsLoginInfo.UserName;
                objPRO315.CreatedOn = DateTime.Now;
                db.PRO315.Add(objPRO315);
                db.SaveChanges();
                #endregion

                returnValue = objPRO315.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_REPORT_FOR_LONGITUDINAL_SEAM_OF_TUBESHEET_AND_BAFFLE.GetStringValue())
            {
                #region Add New Entry in PRO320 For PROTOCOL065
                PRO320 objPRO320 = new PRO320();
                objPRO320.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO320.CreatedBy = objClsLoginInfo.UserName;
                objPRO320.CreatedOn = DateTime.Now;
                db.PRO320.Add(objPRO320);
                db.SaveChanges();
                #endregion

                returnValue = objPRO320.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_OF_FINAL_MACHINING_INSPECTION_OF_TUBESHEET_V2.GetStringValue())
            {
                #region Add New Entry in PRO325 For PROTOCOL066
                PRO325 objPRO325 = new PRO325();
                objPRO325.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO325.CreatedBy = objClsLoginInfo.UserName;
                objPRO325.CreatedOn = DateTime.Now;
                db.PRO325.Add(objPRO325);
                db.SaveChanges();
                #endregion

                returnValue = objPRO325.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_NOZZLE_CUT_OUT_MARKING_ON_SHELL_HEAD.GetStringValue())
            {
                #region Add New Entry in PRO330 For PROTOCOL067
                PRO330 objPRO330 = new PRO330();
                objPRO330.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO330.CreatedBy = objClsLoginInfo.UserName;
                objPRO330.CreatedOn = DateTime.Now;
                db.PRO330.Add(objPRO330);
                db.SaveChanges();
                #endregion

                returnValue = objPRO330.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.AIR_TEST_REPORT_OF_REINFORCEMENT_PADS_COVERING_WELD_SEAMS.GetStringValue())
            {
                #region Add New Entry in PRO335 For PROTOCOL068
                PRO335 objPRO335 = new PRO335();
                objPRO335.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO335.CreatedBy = objClsLoginInfo.UserName;
                objPRO335.CreatedOn = DateTime.Now;
                db.PRO335.Add(objPRO335);
                db.SaveChanges();
                #endregion

                returnValue = objPRO335.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.AIR_TEST_REPORT_OF_REINFORCEMENT_PADS_OF_NOZZLE.GetStringValue())
            {
                #region Add New Entry in PRO340 For PROTOCOL069
                PRO340 objPRO340 = new PRO340();
                objPRO340.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO340.CreatedBy = objClsLoginInfo.UserName;
                objPRO340.CreatedOn = DateTime.Now;
                db.PRO340.Add(objPRO340);
                db.SaveChanges();
                #endregion

                returnValue = objPRO340.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.HELIUM_LEAK_TEST_REPORT_V2.GetStringValue())
            {
                #region Add New Entry in PRO345 For PROTOCOL070
                PRO345 objPRO345 = new PRO345();
                objPRO345.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO345.CreatedBy = objClsLoginInfo.UserName;
                objPRO345.CreatedOn = DateTime.Now;
                db.PRO345.Add(objPRO345);
                db.SaveChanges();
                #endregion

                returnValue = objPRO345.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.HYDROSTATIC_TEST_REPORT_V2.GetStringValue())
            {
                #region Add New Entry in PRO350 For PROTOCOL071
                PRO350 objPRO350 = new PRO350();
                objPRO350.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO350.CreatedBy = objClsLoginInfo.UserName;
                objPRO350.CreatedOn = DateTime.Now;
                db.PRO350.Add(objPRO350);
                db.SaveChanges();
                #endregion

                returnValue = objPRO350.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.PNEUMATIC_TEST_REPORT_V2.GetStringValue())
            {
                #region Add New Entry in PRO355 For PROTOCOL072
                PRO355 objPRO355 = new PRO355();
                objPRO355.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO355.CreatedBy = objClsLoginInfo.UserName;
                objPRO355.CreatedOn = DateTime.Now;
                db.PRO355.Add(objPRO355);
                db.SaveChanges();
                #endregion

                returnValue = objPRO355.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_CUSO4_TEST.GetStringValue())
            {
                #region Add New Entry in PRO360 For PROTOCOL073
                PRO360 objPRO360 = new PRO360();
                objPRO360.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO360.CreatedBy = objClsLoginInfo.UserName;
                objPRO360.CreatedOn = DateTime.Now;
                db.PRO360.Add(objPRO360);
                db.SaveChanges();
                #endregion

                returnValue = objPRO360.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_N2_FILLING_INSPECTION_V2.GetStringValue())
            {
                #region Add New Entry in PRO365 For PROTOCOL074
                PRO365 objPRO365 = new PRO365();
                objPRO365.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO365.CreatedBy = objClsLoginInfo.UserName;
                objPRO365.CreatedOn = DateTime.Now;
                db.PRO365.Add(objPRO365);
                db.SaveChanges();
                #endregion

                returnValue = objPRO365.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.SETUP_AND_DIMENSION_REPORT_FOR_FORMED_CONE.GetStringValue())
            {
                #region Add New Entry in PRO370 For PROTOCOL075
                PRO370 objPRO370 = new PRO370();
                objPRO370.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO370.CreatedBy = objClsLoginInfo.UserName;
                objPRO370.CreatedOn = DateTime.Now;
                db.PRO370.Add(objPRO370);
                db.SaveChanges();
                #endregion

                returnValue = objPRO370.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_REPORT_FOR_FORMED_CONE.GetStringValue())
            {
                #region Add New Entry in PRO375 For PROTOCOL076
                PRO375 objPRO375 = new PRO375();
                objPRO375.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO375.CreatedBy = objClsLoginInfo.UserName;
                objPRO375.CreatedOn = DateTime.Now;
                db.PRO375.Add(objPRO375);
                db.SaveChanges();
                #endregion

                returnValue = objPRO375.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_VISUAL_AND_DIMENSION_INSPECTION_FOR_SQUARE_TYPE_NUB_BEFORE_OVERLAY.GetStringValue())
            {
                #region Add New Entry in PRO380 For PROTOCOL077
                PRO380 objPRO380 = new PRO380();
                objPRO380.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO380.CreatedBy = objClsLoginInfo.UserName;
                objPRO380.CreatedOn = DateTime.Now;
                db.PRO380.Add(objPRO380);
                db.SaveChanges();
                #endregion

                returnValue = objPRO380.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_VISUAL_AND_DIMENSION_INSPECTION_FOR_SQUARE_TYPE_NUB_AFTER_OVERLAY.GetStringValue())
            {
                #region Add New Entry in PRO385 For PROTOCOL078
                PRO385 objPRO385 = new PRO385();
                objPRO385.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO385.CreatedBy = objClsLoginInfo.UserName;
                objPRO385.CreatedOn = DateTime.Now;
                db.PRO385.Add(objPRO385);
                db.SaveChanges();
                #endregion

                returnValue = objPRO385.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_VISUAL_AND_DIMENSION_INSPECTION_FOR_TAPER_TYPE_NUB_BEFORE_OVERLAY.GetStringValue())
            {
                #region Add New Entry in PRO390 For PROTOCOL079
                PRO390 objPRO390 = new PRO390();
                objPRO390.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO390.CreatedBy = objClsLoginInfo.UserName;
                objPRO390.CreatedOn = DateTime.Now;
                db.PRO390.Add(objPRO390);
                db.SaveChanges();
                #endregion

                returnValue = objPRO390.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_VISUAL_AND_DIMENSION_INSPECTION_FOR_TAPER_TYPE_NUB_AFTER_OVERLAY.GetStringValue())
            {
                #region Add New Entry in PRO395 For PROTOCOL080
                PRO395 objPRO395 = new PRO395();
                objPRO395.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO395.CreatedBy = objClsLoginInfo.UserName;
                objPRO395.CreatedOn = DateTime.Now;
                db.PRO395.Add(objPRO395);
                db.SaveChanges();
                #endregion

                returnValue = objPRO395.HeaderId;
            }

            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_VISUAL_AND_DIMENSION_INSPECTION_FOR_NUB_ON_D_END_BEFORE_OVERLAY.GetStringValue())
            {
                #region Add New Entry in PRO400 For PROTOCOL081
                PRO400 objPRO400 = new PRO400();
                objPRO400.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO400.CreatedBy = objClsLoginInfo.UserName;
                objPRO400.CreatedOn = DateTime.Now;
                db.PRO400.Add(objPRO400);
                db.SaveChanges();
                #endregion

                returnValue = objPRO400.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_VISUAL_AND_DIMENSION_INSPECTION_FOR_NUB_ON_D_END_AFTER_OVERLAY.GetStringValue())
            {
                #region Add New Entry in PRO405 For PROTOCOL082
                PRO405 objPRO405 = new PRO405();
                objPRO405.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO405.CreatedBy = objClsLoginInfo.UserName;
                objPRO405.CreatedOn = DateTime.Now;
                db.PRO405.Add(objPRO405);
                db.SaveChanges();
                #endregion

                returnValue = objPRO405.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_VISUAL_AND_DIMENSION_INSPECTION_OF_SKIRT_ASSEMBLY.GetStringValue())
            {
                #region Add New Entry in PRO410 For PROTOCOL083
                PRO410 objPRO410 = new PRO410();
                objPRO410.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO410.CreatedBy = objClsLoginInfo.UserName;
                objPRO410.CreatedOn = DateTime.Now;
                db.PRO410.Add(objPRO410);
                db.SaveChanges();
                #endregion

                returnValue = objPRO410.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_VISUAL_AND_DIMENSION_OF_SKIRT_TEMPLATE.GetStringValue())
            {
                #region Add New Entry in PRO415 For PROTOCOL084
                PRO415 objPRO415 = new PRO415();
                objPRO415.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO415.CreatedBy = objClsLoginInfo.UserName;
                objPRO415.CreatedOn = DateTime.Now;
                db.PRO415.Add(objPRO415);
                db.SaveChanges();
                #endregion

                returnValue = objPRO415.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_WELD_VISUAL_AND_DIMENSION_REPORT_FOR_SPOOL_TYPE_1_BOTH_SIDE_FLANGES.GetStringValue())
            {
                #region Add New Entry in PRO420 For PROTOCOL085
                PRO420 objPRO420 = new PRO420();
                objPRO420.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO420.CreatedBy = objClsLoginInfo.UserName;
                objPRO420.CreatedOn = DateTime.Now;
                db.PRO420.Add(objPRO420);
                db.SaveChanges();
                #endregion

                returnValue = objPRO420.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_WELD_VISUAL_AND_DIMENSION_FOR_SPOOL_TYPE_2_ONE_SIDE_FLANGE.GetStringValue())
            {
                #region Add New Entry in PRO425 For PROTOCOL086
                PRO425 objPRO425 = new PRO425();
                objPRO425.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO425.CreatedBy = objClsLoginInfo.UserName;
                objPRO425.CreatedOn = DateTime.Now;
                db.PRO425.Add(objPRO425);
                db.SaveChanges();
                #endregion

                returnValue = objPRO425.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_WELD_VISUAL_AND_DIMENSION_REPORT_FOR_SPOOL_TYPE_3_BOTH_SIDE_FLANGES.GetStringValue())
            {
                #region Add New Entry in PRO430 For PROTOCOL087
                PRO430 objPRO430 = new PRO430();
                objPRO430.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO430.CreatedBy = objClsLoginInfo.UserName;
                objPRO430.CreatedOn = DateTime.Now;
                db.PRO430.Add(objPRO430);
                db.SaveChanges();
                #endregion

                returnValue = objPRO430.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.AFTER_PWHTFINAL_REPORT_FOR_VISUAL_AND_DIMENSION_AFTER_MACHINING_OF_GASKET_FACE_AND_OTHER_DIMENSIONS_OF_FLANGE.GetStringValue())
            {
                #region Add New Entry in PRO435 For PROTOCOL088
                PRO435 objPRO435 = new PRO435();
                objPRO435.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO435.CreatedBy = objClsLoginInfo.UserName;
                objPRO435.CreatedOn = DateTime.Now;
                db.PRO435.Add(objPRO435);
                db.SaveChanges();
                #endregion

                returnValue = objPRO435.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.AFTER_PWHTFINAL_REPORT_FOR_VISUAL_AND_DIMENSION_AFTER_MACHINING_OF_GASKET_FACE_AND_OTHER_DIMENSIONS_OF_NOZZLE.GetStringValue())
            {
                #region Add New Entry in PRO440 For PROTOCOL089
                PRO440 objPRO440 = new PRO440();
                objPRO440.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO440.CreatedBy = objClsLoginInfo.UserName;
                objPRO440.CreatedOn = DateTime.Now;
                db.PRO440.Add(objPRO440);
                db.SaveChanges();
                #endregion

                returnValue = objPRO440.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.INITIAL_MACHINING_VISUAL_AND_DIMENSION_REPORT_FOR_GASKET_FACE_AND_OTHER_DIMENSIONS_OF_FLANGE.GetStringValue())
            {
                #region Add New Entry in PRO445 For PROTOCOL090
                PRO445 objPRO445 = new PRO445();
                objPRO445.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO445.CreatedBy = objClsLoginInfo.UserName;
                objPRO445.CreatedOn = DateTime.Now;
                db.PRO445.Add(objPRO445);
                db.SaveChanges();
                #endregion

                returnValue = objPRO445.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.INITIAL_MACHINING_VISUAL_AND_DIMENSION_REPORT_FOR_GASKET_FACE_AND_OTHER_DIMENSIONS_OF_NOZZLE.GetStringValue())
            {
                #region Add New Entry in PRO450 For PROTOCOL091
                PRO450 objPRO450 = new PRO450();
                objPRO450.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO450.CreatedBy = objClsLoginInfo.UserName;
                objPRO450.CreatedOn = DateTime.Now;
                db.PRO450.Add(objPRO450);
                db.SaveChanges();
                #endregion

                returnValue = objPRO450.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_WATER_BOX_MACHINING_INSPECTION_R1.GetStringValue())
            {
                #region Add New Entry in PRO455 For PROTOCOL092
                PRO455 objPRO455 = new PRO455();
                objPRO455.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO455.CreatedBy = objClsLoginInfo.UserName;
                objPRO455.CreatedOn = DateTime.Now;
                db.PRO455.Add(objPRO455);
                db.SaveChanges();
                #endregion

                returnValue = objPRO455.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.REPORT_FOR_SCREW_PLUG_ACME_HEADER_MACHINING_INSPECTION.GetStringValue())
            {
                #region Add New Entry in PRO460 For PROTOCOL093
                PRO460 objPRO460 = new PRO460();
                objPRO460.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO460.CreatedBy = objClsLoginInfo.UserName;
                objPRO460.CreatedOn = DateTime.Now;
                db.PRO460.Add(objPRO460);
                db.SaveChanges();
                #endregion

                returnValue = objPRO460.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.FINAL_ASSEMBLY_INSPECTION_OF_ROUGH_LIQUID_DISTRIBUTION_TRAY.GetStringValue())
            {
                #region Add New Entry in PRO465 For PROTOCOL094
                PRO465 objPRO465 = new PRO465();
                objPRO465.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO465.CreatedBy = objClsLoginInfo.UserName;
                objPRO465.CreatedOn = DateTime.Now;
                db.PRO465.Add(objPRO465);
                db.SaveChanges();
                #endregion

                returnValue = objPRO465.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.FINAL_ASSEMBLY_INSPECTION_OF_VAPOUR_LIQUID_DISTRIBUTION_TRAY.GetStringValue())
            {
                #region Add New Entry in PRO470 For PROTOCOL095
                PRO470 objPRO470 = new PRO470();
                objPRO470.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO470.CreatedBy = objClsLoginInfo.UserName;
                objPRO470.CreatedOn = DateTime.Now;
                db.PRO470.Add(objPRO470);
                db.SaveChanges();
                #endregion

                returnValue = objPRO470.HeaderId;
            }
            else if (ProtocolType == clsImplementationEnum.ProtocolType.FINAL_DIMENSION_INSPECTION_OF_LATTICE_BEAMS.GetStringValue())
            {
                #region Add New Entry in PRO475 For PROTOCOL096
                PRO475 objPRO475 = new PRO475();
                objPRO475.ProtocolNo = !string.IsNullOrWhiteSpace(ProtocolNo) ? ProtocolNo : string.Empty;
                objPRO475.CreatedBy = objClsLoginInfo.UserName;
                objPRO475.CreatedOn = DateTime.Now;
                db.PRO475.Add(objPRO475);
                db.SaveChanges();
                #endregion

                returnValue = objPRO475.HeaderId;
            }
            return returnValue;
        }

        #endregion

        public List<EmployeeList> GetDepartmentRoleWiseEmployee(string Location, string Department, string Role)
        {
            List<EmployeeList> lstEmployee = new List<EmployeeList>();
            try
            {
                List<string> listDepartment = Department.ToLower().Split(',').ToList();
                List<string> listRole = Role.ToLower().Split(',').ToList();
                if (Location != string.Empty)
                {
                    lstEmployee = (from ath1 in db.ATH001
                                   join com3 in db.COM003 on ath1.Employee equals com3.t_psno
                                   join ath4 in db.ATH004 on ath1.Role equals ath4.Id
                                   where (ath1.Location.Equals(Location))
                                        && !string.IsNullOrEmpty(com3.t_depc)
                                        && listRole.Contains(ath4.Role.ToString().ToLower())
                                        && (!string.IsNullOrEmpty(Department) ? listDepartment.Contains(com3.t_depc.ToString().ToLower()) : com3.t_depc == com3.t_depc)
                                        && com3.t_actv == 1
                                   select
                                   new EmployeeList { isActive = com3.t_actv, psno = com3.t_psno, name = com3.t_name, psnoname = com3.t_psno + " - " + com3.t_name, department = com3.t_depc, location = ath1.Location }).Distinct().ToList();
                }
                else
                {
                    lstEmployee = (from ath1 in db.ATH001
                                   join com3 in db.COM003 on ath1.Employee equals com3.t_psno
                                   join ath4 in db.ATH004 on ath1.Role equals ath4.Id
                                   where !string.IsNullOrEmpty(com3.t_depc)
                                        && listRole.Contains(ath4.Role.ToString().ToLower())
                                        && (!string.IsNullOrEmpty(Department) ? listDepartment.Contains(com3.t_depc.ToString().ToLower()) : com3.t_depc == com3.t_depc)
                                        && com3.t_actv == 1
                                   select
                                   new EmployeeList { isActive = com3.t_actv, psno = com3.t_psno, name = com3.t_name, psnoname = com3.t_psno + " - " + com3.t_name, department = com3.t_depc, location = "" }).Distinct().ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return lstEmployee;
        }

        public bool IsChildPartCleared(string Project, string QualityProject, string BU, string Location, string PartNo)
        {
            bool allCleared = true;
            var ChildParts = db.SP_IPI_GETHBOMLIST(Project, PartNo).ToList();
            var ClearedStatus = clsImplementationEnum.PartlistOfferInspectionStatus.CLEARED.GetStringValue();
            foreach (var childPart in ChildParts)
            {
                // check ICL for part exists or not
                if (db.QMS036.Any(c => c.Project == childPart.Project && c.QualityProject == QualityProject && c.PartNo == childPart.FindNo && c.ParentPartNo == childPart.ParentPart && c.ChildPartNo == childPart.Part))
                {
                    // check part exists for inspection
                    if (!db.QMS045.Any(c => c.Project == childPart.Project && c.QualityProject == QualityProject && c.PartNo == childPart.FindNo && c.ParentPartNo == childPart.ParentPart && c.ChildPartNo == childPart.Part))
                    {
                        allCleared = false;
                        break;
                    }

                    // check part inspection status cleared or not
                    if (db.QMS045.Any(c => c.Project == childPart.Project && c.QualityProject == QualityProject && c.PartNo == childPart.FindNo && c.ParentPartNo == childPart.ParentPart && c.ChildPartNo == childPart.Part && (c.InspectionStatus == null || c.InspectionStatus != ClearedStatus)))
                    {
                        allCleared = false;
                        break;
                    }
                }

            }
            return allCleared;
        }

        public bool IsPARTFullyCleared(string Project, string QualityProject, string BU, string Location, string PartNo)
        {
            bool allCleared = true;
            var clearedStatus = clsImplementationEnum.PartlistOfferInspectionStatus.CLEARED.GetStringValue();
            if (db.QMS045.Any(c => c.Project == Project && c.QualityProject == QualityProject && c.BU == BU && c.Location == Location && c.PartNo == PartNo && (c.InspectionStatus == null || c.InspectionStatus != clearedStatus)))
            {
                allCleared = false;
            }
            return allCleared;
        }
        public bool IsSEAMFullyCleared(string Project, string QualityProject, string BU, string Location, string SeamNo)
        {
            bool allCleared = true;
            var clearedStatus = clsImplementationEnum.SeamListInspectionStatus.Cleared.GetStringValue();
            if (db.QMS040.Any(c => c.Project == Project && c.QualityProject == QualityProject && c.BU == BU && c.Location == Location && c.SeamNo == SeamNo && (c.InspectionStatus == null || c.InspectionStatus != clearedStatus)))
            {
                allCleared = false;
            }
            return allCleared;
        }

        public bool ReadyToOffer_PART(QMS035 objQMS035, bool fromICLApproved = false)
        {
            bool PartOffered = false;
            var readyToOfferStatus = clsImplementationEnum.PartlistOfferInspectionStatus.READY_TO_OFFER.GetStringValue();
            var clearedStatus = clsImplementationEnum.PartlistOfferInspectionStatus.CLEARED.GetStringValue();
            string ApprovedStatus = clsImplementationEnum.ICLPartStatus.Approved.GetStringValue();

            var ReadytoOfferStages = db.QMS045.Where(i => i.QualityProject.Equals(objQMS035.QualityProject, StringComparison.OrdinalIgnoreCase)
                                                 && i.Project == objQMS035.Project && i.BU == objQMS035.BU && i.PartNo == objQMS035.PartNo && i.ParentPartNo == objQMS035.ParentPartNo && i.Location == objQMS035.Location && i.InspectionStatus.Equals(readyToOfferStatus)).ToList();
            foreach (var readyToOfferStage in ReadytoOfferStages)
            {
                readyToOfferStage.InspectionStatus = null;
                //readyToOfferStage.Quantity = null;
                readyToOfferStage.BalanceQuantity = null;
                db.SaveChanges();
            }

            int maxOfferedSequenceNo = GetMaxOfferedSequenceNo_PART(objQMS035.QualityProject, objQMS035.BU, objQMS035.Location, objQMS035.PartNo, objQMS035.ParentPartNo);

            bool isApplicableForRTO = true;
            //if (maxOfferedSequenceNo == 0)
            //{
            //    if (MakeSpecialStageRTO(objQMS035))
            //    {
            //        isApplicableForRTO = false;
            //    }
            //    else
            //    {
            //        isApplicableForRTO = IsPartApplicableForRTO(objQMS035.Project, objQMS035.QualityProject, objQMS035.ChildPartNo);
            //    }

            //    //if (isApplicableForRTO)
            //    //{
            //    //    // get seams which are associated with this parts and ready to offer
            //    //    List<string> SeamList = db.QMS012_Log.Where(c => c.QualityProject == objQMS035.QualityProject && c.Project == objQMS035.Project && c.Status == ApprovedStatus).ToList().Where(w => w.Position.Split(',').Contains(objQMS035.PartNo)).Select(s => s.SeamNo).Distinct().ToList();
            //    //    foreach (var seam in SeamList)
            //    //    {
            //    //        QMS030 objQMS030 = db.QMS030.FirstOrDefault(c => c.Project == objQMS035.Project && c.QualityProject == objQMS035.QualityProject && c.SeamNo == seam && c.Status == ApprovedStatus);
            //    //        if (objQMS030 != null)
            //    //        {
            //    //            ReadyToOffer_SEAM(objQMS030);
            //    //        }
            //    //    }
            //    //}
            //}
            //else
            //{
            //    if (!fromICLApproved)
            //    {
            //        isApplicableForRTO = false;
            //    }
            //}

            if (isApplicableForRTO && !db.QMS045.Any(c => c.QualityProject.Equals(objQMS035.QualityProject, StringComparison.OrdinalIgnoreCase)
                                                     && c.Project == objQMS035.Project && c.BU == objQMS035.BU && c.PartNo == objQMS035.PartNo && c.ParentPartNo == objQMS035.ParentPartNo && c.Location == objQMS035.Location
                                                     && c.InspectionStatus != clearedStatus && c.StageSequence <= maxOfferedSequenceNo))
            {

                var QMS045SeqObj = db.QMS045.Where(i => i.QualityProject.Equals(objQMS035.QualityProject, StringComparison.OrdinalIgnoreCase)
                                                 && i.Project == objQMS035.Project && i.BU == objQMS035.BU && i.PartNo == objQMS035.PartNo && i.ParentPartNo == objQMS035.ParentPartNo && i.Location == objQMS035.Location && (i.StageSequence > maxOfferedSequenceNo || (i.StageSequence == maxOfferedSequenceNo && (i.InspectionStatus == null || i.InspectionStatus.Equals(readyToOfferStatus))))).OrderBy(o => o.StageSequence).FirstOrDefault();

                if (QMS045SeqObj != null)
                {
                    int LowerSequence = QMS045SeqObj.StageSequence.Value;

                    var LowerSeqStages = db.QMS045.Where(i => i.QualityProject.Equals(objQMS035.QualityProject, StringComparison.OrdinalIgnoreCase)
                                                         && i.Project == objQMS035.Project && i.BU == objQMS035.BU && i.PartNo == objQMS035.PartNo && i.ParentPartNo == objQMS035.ParentPartNo && i.Location == objQMS035.Location && i.StageSequence == LowerSequence && (i.InspectionStatus == null || i.InspectionStatus.Equals(readyToOfferStatus))).ToList();
                    foreach (var lowerStage in LowerSeqStages)
                    {
                        lowerStage.InspectionStatus = clsImplementationEnum.PartlistOfferInspectionStatus.READY_TO_OFFER.GetStringValue();
                        lowerStage.Quantity = objQMS035.Quantity;
                        lowerStage.BalanceQuantity = objQMS035.Quantity;
                        db.SaveChanges();
                        PartOffered = true;
                    }
                }
            }
            else
            {
                var RTOStages = db.QMS045.Where(i => i.QualityProject.Equals(objQMS035.QualityProject, StringComparison.OrdinalIgnoreCase)
                                                 && i.Project == objQMS035.Project && i.BU == objQMS035.BU && i.PartNo == objQMS035.PartNo && i.ParentPartNo == objQMS035.ParentPartNo && i.Location == objQMS035.Location
                                                 && i.InspectionStatus == null && i.StageSequence == maxOfferedSequenceNo).ToList();
                foreach (var RTOStage in RTOStages)
                {
                    RTOStage.InspectionStatus = clsImplementationEnum.PartlistOfferInspectionStatus.READY_TO_OFFER.GetStringValue();
                    RTOStage.BalanceQuantity = objQMS035.Quantity;
                    db.SaveChanges();
                    PartOffered = true;
                }
            }

            return PartOffered;
        }

        public bool ReadyToOffer_SEAM(QMS030 objQMS030, bool isSpecialStageCleared = false)
        {
            bool SeamOffered = false;
            var readyToOfferStatus = clsImplementationEnum.PartlistOfferInspectionStatus.READY_TO_OFFER.GetStringValue();
            var clearedStatus = clsImplementationEnum.PartlistOfferInspectionStatus.CLEARED.GetStringValue();
            int maxOfferedSequenceNo = GetMaxOfferedSequenceNo(objQMS030.QualityProject, objQMS030.BU, objQMS030.Location, objQMS030.SeamNo);

            var ReadytoOfferStages = db.QMS040.Where(i => i.QualityProject.Equals(objQMS030.QualityProject, StringComparison.OrdinalIgnoreCase)
                                                 && i.Project == objQMS030.Project && i.BU == objQMS030.BU && i.SeamNo == objQMS030.SeamNo && i.Location == objQMS030.Location
                                                 && i.InspectionStatus.Equals(readyToOfferStatus)).ToList();
            foreach (var readyToOfferStage in ReadytoOfferStages)
            {
                readyToOfferStage.InspectionStatus = null;
                db.SaveChanges();
            }

            bool isApplicableForRTO = true;
            //if (maxOfferedSequenceNo == 0 && !isSpecialStageCleared)
            //{
            //    isApplicableForRTO = IsSeamApplicableForRTO(objQMS030.Project, objQMS030.QualityProject, objQMS030.BU, objQMS030.Location, objQMS030.SeamNo);
            //}

            if (isApplicableForRTO && !db.QMS040.Any(c => c.QualityProject.Equals(objQMS030.QualityProject, StringComparison.OrdinalIgnoreCase)
                                                     && c.Project == objQMS030.Project && c.BU == objQMS030.BU && c.SeamNo == objQMS030.SeamNo && c.Location == objQMS030.Location
                                                     && c.InspectionStatus != clearedStatus && c.InspectionStatus != null && c.StageSequence <= maxOfferedSequenceNo))
            {

                var QMS040SeqObj = db.QMS040.Where(i => i.QualityProject.Equals(objQMS030.QualityProject, StringComparison.OrdinalIgnoreCase)
                                                     && i.Project == objQMS030.Project && i.BU == objQMS030.BU && i.SeamNo == objQMS030.SeamNo && i.Location == objQMS030.Location && (i.StageSequence > maxOfferedSequenceNo || (i.StageSequence == maxOfferedSequenceNo && (i.InspectionStatus == null || i.InspectionStatus.Equals(readyToOfferStatus))))).OrderBy(o => o.StageSequence).FirstOrDefault();

                if (QMS040SeqObj != null)
                {
                    int LowerSequence = QMS040SeqObj.StageSequence.Value;
                    var LowerSeqStages = db.QMS040.Where(i => i.QualityProject.Equals(objQMS030.QualityProject, StringComparison.OrdinalIgnoreCase)
                                                     && i.Project == objQMS030.Project && i.BU == objQMS030.BU && i.SeamNo == objQMS030.SeamNo && i.Location == objQMS030.Location && i.StageSequence == LowerSequence && (i.InspectionStatus == null || i.InspectionStatus.Equals(readyToOfferStatus))).ToList();
                    foreach (var lowerStage in LowerSeqStages)
                    {
                        lowerStage.InspectionStatus = clsImplementationEnum.PartlistOfferInspectionStatus.READY_TO_OFFER.GetStringValue();
                        db.SaveChanges();
                        SeamOffered = true;
                    }
                }

            }
            else
            {
                var RTOStages = db.QMS040.Where(i => i.QualityProject.Equals(objQMS030.QualityProject, StringComparison.OrdinalIgnoreCase)
                                                 && i.Project == objQMS030.Project && i.BU == objQMS030.BU && i.SeamNo == objQMS030.SeamNo && i.Location == objQMS030.Location
                                                 && i.InspectionStatus == null && i.StageSequence == maxOfferedSequenceNo).ToList();
                foreach (var RTOStage in RTOStages)
                {
                    RTOStage.InspectionStatus = clsImplementationEnum.PartlistOfferInspectionStatus.READY_TO_OFFER.GetStringValue();
                    db.SaveChanges();
                    SeamOffered = true;
                }
            }

            return SeamOffered;
        }

        public bool IsPartApplicableForRTO(string Project, string QualityProject, string PartCode)
        {
            bool ApplicableForRTO = true;
            string ApprovedStatus = clsImplementationEnum.ICLPartStatus.Approved.GetStringValue();
            string ClearedStatus = clsImplementationEnum.PartlistOfferInspectionStatus.CLEARED.GetStringValue();

            var ChildParts = db.SP_IPI_GETHBOMLIST(Project, PartCode).ToList();
            // check child part exists or not
            foreach (var childPart in ChildParts)
            {
                // check ICL for part exists or not
                if (db.QMS036.Any(c => c.Project == childPart.Project && c.QualityProject == QualityProject && c.PartNo == childPart.FindNo && c.ParentPartNo == childPart.ParentPart && c.ChildPartNo == childPart.Part))
                {
                    // check part exists for inspection
                    if (!db.QMS045.Any(c => c.Project == childPart.Project && c.QualityProject == QualityProject && c.PartNo == childPart.FindNo && c.ParentPartNo == childPart.ParentPart && c.ChildPartNo == childPart.Part))
                    {
                        ApplicableForRTO = false;
                        break;
                    }

                    // check part inspection status cleared or not
                    if (db.QMS045.Any(c => c.Project == childPart.Project && c.QualityProject == QualityProject && c.PartNo == childPart.FindNo && c.ParentPartNo == childPart.ParentPart && c.ChildPartNo == childPart.Part && (c.InspectionStatus == null || c.InspectionStatus != ClearedStatus)))
                    {
                        ApplicableForRTO = false;
                        break;
                    }
                }

                // check seam exists in seam list and inspection cleared or not
                //List<string> SeamList = db.QMS012_Log.Where(c => c.QualityProject == QualityProject && c.Project == childPart.Project && c.Status == ApprovedStatus).ToList().Where(w => w.Position.Split(',').Contains(childPart.FindNo)).Select(s => s.SeamNo).Distinct().ToList();
                List<string> SeamList = db.QMS012_Log.Where(c => c.QualityProject == QualityProject && c.Project == childPart.Project && c.Status == ApprovedStatus && c.AssemblyNo == PartCode).Select(s => s.SeamNo).Distinct().ToList();
                foreach (var seam in SeamList)
                {
                    // check ICL for seam exists or not
                    if (db.QMS031.Any(c => c.Project == childPart.Project && c.QualityProject == QualityProject && c.SeamNo == seam))
                    {
                        // check seam exists for inspection
                        if (!db.QMS040.Any(c => c.Project == childPart.Project && c.QualityProject == QualityProject && c.SeamNo == seam))
                        {
                            ApplicableForRTO = false;
                            break;
                        }

                        // check seam inspection status cleared or not
                        if (db.QMS040.Any(c => c.Project == childPart.Project && c.QualityProject == QualityProject && c.SeamNo == seam && (c.InspectionStatus == null || c.InspectionStatus != ClearedStatus)))
                        {
                            ApplicableForRTO = false;
                            break;
                        }
                    }
                }

                if (!ApplicableForRTO)
                    break;

            }

            return ApplicableForRTO;

        }

        public string TVLIntervention(string intervention)
        {
            if (!string.IsNullOrWhiteSpace(intervention))
            {
                if (intervention.ToUpper() == "E" || intervention.ToUpper() == "EMPTY")
                {
                    intervention = string.Empty;
                }
            }
            return intervention;
        }
        public bool IsSeamApplicableForRTO(string Project, string QualityProject, string BU, string Location, string SeamNo)
        {
            bool ApplicableForRTO = true;
            string ApprovedStatus = clsImplementationEnum.ICLPartStatus.Approved.GetStringValue();
            string ClearedStatus = clsImplementationEnum.PartlistOfferInspectionStatus.CLEARED.GetStringValue();

            // get parts which are associated with seam
            QMS012 objQMS012 = db.QMS012.FirstOrDefault(c => c.Project == Project && c.QualityProject == QualityProject && c.SeamNo == SeamNo);
            if (objQMS012 != null)
            {
                if (!Manager.IsChildPartCleared(Project, QualityProject, BU, Location, objQMS012.AssemblyNo))
                {
                    ApplicableForRTO = false;
                }
                //foreach (var item in objQMS012.Position.Split(','))
                //{
                //    // check ICL for part exists or not
                //    if (db.QMS036.Any(c => c.Project == objQMS012.Project && c.QualityProject == objQMS012.QualityProject && c.PartNo == item))
                //    {
                //        QMS036 objQMS036 = db.QMS036.FirstOrDefault(c => c.Project == objQMS012.Project && c.QualityProject == objQMS012.QualityProject && c.PartNo == item);

                //        // check part exists for inspection
                //        if (!db.QMS045.Any(c => c.Project == objQMS036.Project && c.QualityProject == objQMS036.QualityProject && c.PartNo == item))
                //        {
                //            ApplicableForRTO = false;
                //            break;
                //        }

                //        // check part inspection status cleared or not
                //        if (db.QMS045.Any(c => c.Project == objQMS036.Project && c.QualityProject == objQMS036.QualityProject && c.PartNo == item && (c.InspectionStatus == null || c.InspectionStatus != ClearedStatus)))
                //        {
                //            ApplicableForRTO = false;
                //            break;
                //        }
                //    }

                //}
                //}
            }
            else
            {
                ApplicableForRTO = false;
            }

            return ApplicableForRTO;
        }

        public bool MakeSpecialStageRTO(QMS035 objQMS035)
        {
            bool offered = false;
            string specialStage = Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.SpecialStageCodeForRTO.GetStringValue());
            var readyToOfferStatus = clsImplementationEnum.PartlistOfferInspectionStatus.READY_TO_OFFER.GetStringValue();

            QMS045 objQMS045 = db.QMS045.Where(i => i.QualityProject.Equals(objQMS035.QualityProject, StringComparison.OrdinalIgnoreCase) && i.StageCode.Equals(specialStage, StringComparison.OrdinalIgnoreCase)
                                                 && i.Project == objQMS035.Project && i.BU == objQMS035.BU && i.PartNo == objQMS035.PartNo && i.Location == objQMS035.Location).OrderBy(o => o.StageSequence).FirstOrDefault();

            if (objQMS045 != null)
            {
                objQMS045.InspectionStatus = readyToOfferStatus;
                objQMS045.Quantity = objQMS035.Quantity;
                objQMS045.BalanceQuantity = objQMS035.Quantity;
                db.SaveChanges();
                offered = true;
            }

            return offered;
        }

        public void ConcatString(ref string OriginalString, string AddString, string ConcateChar = ",")
        {
            if (string.IsNullOrEmpty(OriginalString))
            {
                OriginalString = AddString;
            }
            else
            {
                OriginalString += ConcateChar + AddString;
            }
        }

        public clsHelper.ResponseMsg IsApplicableForOffer(int OfferHeaderId, string InspectionFor, bool isLTFPS = false)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string InspectionStatus = string.Empty;

                if (isLTFPS)
                {
                    InspectionStatus = db.LTF002.FirstOrDefault(c => c.LineId == OfferHeaderId).InspectionStatus;
                }
                else
                {
                    if (InspectionFor == clsImplementationEnum.InspectionFor.SEAM.GetStringValue())
                    {
                        InspectionStatus = db.QMS040.FirstOrDefault(c => c.HeaderId == OfferHeaderId).InspectionStatus;
                    }
                    else
                    {
                        InspectionStatus = db.QMS045.FirstOrDefault(c => c.HeaderId == OfferHeaderId).InspectionStatus;
                    }
                }

                if (Convert.ToString(InspectionStatus) == clsImplementationEnum.SeamListInspectionStatus.ReadyToOffer.GetStringValue() || Convert.ToString(InspectionStatus) == clsImplementationEnum.PartlistOfferInspectionStatus.PARTIALLY_OFFERED.GetStringValue())
                {
                    objResponseMsg.Key = true;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "This stage has been already offered";
                }

                return objResponseMsg;
            }
            catch (Exception ex)
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error Occures Please try again";
                return objResponseMsg;
            }
        }

        public clsHelper.ResponseMsg IsApplicableForAttend(int RequestId, string InspectionFor, string RequestType)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string ResultStatus = string.Empty;

                if (InspectionFor == clsImplementationEnum.InspectionFor.SEAM.GetStringValue())
                {
                    if (RequestType == "NDE")
                    {
                        ResultStatus = db.QMS060.FirstOrDefault(c => c.RequestId == RequestId).RequestStatus;
                    }
                    else
                    {
                        ResultStatus = db.QMS050.FirstOrDefault(c => c.RequestId == RequestId).TestResult;
                    }
                }
                else
                {
                    if (RequestType == "NDE")
                    {
                        ResultStatus = db.QMS065.FirstOrDefault(c => c.RequestId == RequestId).RequestStatus;
                    }
                    else
                    {
                        ResultStatus = db.QMS055.FirstOrDefault(c => c.RequestId == RequestId).TestResult;
                    }
                }


                if (ResultStatus == null || ResultStatus == clsImplementationEnum.NDESeamRequestStatus.NotAttended.GetStringValue())
                {
                    objResponseMsg.Key = true;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "This request has been already attended";
                }

                return objResponseMsg;
            }
            catch (Exception ex)
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error Occures Please try again";
                return objResponseMsg;
            }
        }

        public string GetFixtureNo(string Project, int? FXRSrNo)
        {
            return "FIX-" + Project + "-" + FXRSrNo.ToString().PadLeft(3, '0');
        }
        public string GetItemNo(string Project, int? FXRSrNo)
        {
            return "FXR-" + Project.Substring(Project.Length - 4) + "-" + FXRSrNo.ToString().PadLeft(3, '0');
        }

        public bool Add(string ActionType, string Action)
        {
            try
            {
                ATH024 objATH024 = new ATH024();
                objATH024.ActionType = ActionType;
                objATH024.Action = Action;
                objATH024.ActionDate = DateTime.Now;
                objATH024.ActionBy = objClsLoginInfo.UserName;
                objATH024.Browser = System.Web.HttpContext.Current.Request.Browser.Browser;
                objATH024.IPAddress = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];

                db.ATH024.Add(objATH024);
                db.SaveChanges();
            }
            catch
            {
                return false;
            }
            return true;
        }

        public void ScurveHTC(string project, string location)
        {
            try
            {
                var objpdn001 = db.PDN001.Where(x => x.Project == project && x.Location == location).FirstOrDefault();

                if (objpdn001 != null)
                {
                    var lstscu002 = db.SCU002.Where(x => x.Project == project && x.location == objpdn001.Location).ToList();

                    if (lstscu002.Count() > 1)
                    {
                        var ApprovedStatus = clsImplementationEnum.CommonStatus.Approved.GetStringValue();
                        string strHTR = "HTR";

                        string zerodate = Getzerodate(objpdn001.Project);

                        int planqty = 0;
                        int actqty = 0;
                        string projectarray = string.Empty;

                        if (objpdn001.IProject != null)
                            projectarray = objpdn001.Project + "," + objpdn001.IProject;
                        else
                            projectarray = objpdn001.Project;

                        var IProjects = projectarray.Split(',').Distinct().ToList();

                        foreach (var item in IProjects)
                        {
                            var lsthtr001 = db.HTR001.Where(x => x.Project == item && x.Location == objpdn001.Location).ToList();
                            planqty = planqty + lsthtr001.Count();
                            actqty = actqty + lsthtr001.Where(x => x.Status == ApprovedStatus).Count();
                        }

                        SCU002 objSCU002 = null;
                        SCU003 objSCU003 = null;
                        SCU003_Log objSCU003_log = null;

                        objSCU002 = lstscu002.Where(x => x.DoumentNo == strHTR).FirstOrDefault();
                        if (objSCU002 != null)
                        {
                            objSCU002.PlannedQty = planqty;
                            objSCU002.ActualQty = actqty;
                            objSCU002.EditedOn = DateTime.Now;
                            objSCU002.EditedBy = objClsLoginInfo.UserName;
                        }
                        else
                        {
                            objSCU002 = new SCU002();
                            objSCU002.Project = objpdn001.Project;
                            objSCU002.location = objpdn001.Location;
                            objSCU002.CDD = objpdn001.CDD;
                            objSCU002.ZeroDate = Convert.ToDateTime(zerodate);
                            objSCU002.DoumentNo = strHTR;
                            objSCU002.Phase = 2;
                            objSCU002.DoumentDesc = strHTR;
                            objSCU002.Days = 180;
                            objSCU002.PlannedQty = planqty;
                            objSCU002.ActualQty = actqty;
                            objSCU002.HrsPerDoc = 1;
                            objSCU002.CreatedOn = DateTime.Now;
                            objSCU002.CreatedBy = objClsLoginInfo.UserName;
                            db.SCU002.Add(objSCU002);
                        }

                        objSCU003 = db.SCU003.Where(c => c.Project == objSCU002.Project && c.DoumentNo == strHTR).FirstOrDefault();
                        if (objSCU003 != null)
                        {
                            objSCU003.ActualQty = objSCU002.ActualQty;
                            objSCU003.EditedOn = DateTime.Now;
                            objSCU003.EditedBy = objClsLoginInfo.UserName;
                        }
                        else
                        {
                            objSCU003 = new SCU003();
                            objSCU003.Project = objSCU002.Project;
                            objSCU003.location = objSCU002.location;
                            objSCU003.DoumentNo = objSCU002.DoumentNo;
                            objSCU003.ActualQtyInterval = Convert.ToDouble(GetActualQtyInterval(zerodate, objSCU002.Days));
                            objSCU003.ActualQty = objSCU002.ActualQty;
                            objSCU003.phase = objSCU002.Phase;
                            objSCU003.CreatedOn = DateTime.Now;
                            objSCU003.CreatedBy = objClsLoginInfo.UserName;
                            db.SCU003.Add(objSCU003);
                        }

                        objSCU003_log = db.SCU003_Log.Where(c => c.Project == objSCU002.Project && c.DoumentNo == strHTR).FirstOrDefault();
                        if (objSCU003_log != null)
                        {
                            objSCU003_log.ActualQty = objSCU002.ActualQty;
                            objSCU003_log.EditedOn = DateTime.Now;
                            objSCU003_log.EditedBy = objClsLoginInfo.UserName;
                        }
                        else
                        {
                            objSCU003_log = new SCU003_Log();
                            objSCU003_log.Project = objSCU002.Project;
                            objSCU003_log.location = objSCU002.location;
                            objSCU003_log.DoumentNo = objSCU002.DoumentNo;
                            objSCU003_log.ActualQtyInterval = Convert.ToDouble(GetActualQtyInterval(zerodate, objSCU002.Days));
                            objSCU003_log.ActualQty = objSCU002.ActualQty;
                            objSCU003_log.phase = objSCU002.Phase;
                            objSCU003_log.CreatedOn = DateTime.Now;
                            objSCU003_log.CreatedBy = objClsLoginInfo.UserName;
                            db.SCU003_Log.Add(objSCU003_log);
                        }
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string Getzerodate(string Project)
        {
            string result = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(Project))
                {
                    string query = " select cast(ttpctm100175.t_efdt as varchar(50)) " +
                                    " from " + LNLinkedServer + ".dbo.ttpctm100175 as ttpctm100175 " +
                                    " INNER JOIN " + LNLinkedServer + ".dbo.ttpctm110175 as ttpctm110175 " +
                                    " on ttpctm100175.t_cono=ttpctm110175.t_cono " +
                                    " where ttpctm110175.t_cprj = '" + Project + "'";

                    var zero = db.Database.SqlQuery<string>(query).FirstOrDefault();
                    if (zero != null)
                    {
                        result = zero;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        public int GetActualQtyInterval(string zerodate, double days)
        {
            int ActualQtyInterval = 0;
            try
            {
                if (!string.IsNullOrEmpty(zerodate))
                {
                    string dur1 = FN_GET_ACTUALQTY(zerodate, zerodate, days);

                    string zerodate2 = string.Format("{0:MMM dd yyyy hh:mmtt}", DateTime.Now);
                    string dur2 = FN_GET_ACTUALQTY(zerodate, zerodate2, 0);

                    int duration1 = Convert.ToInt32(dur1);
                    int duration2 = Convert.ToInt32(dur2);

                    if (duration1 > duration2)
                        ActualQtyInterval = duration1 > 0 ? duration1 : 0;
                    else
                        ActualQtyInterval = duration2 > 0 ? duration2 : 0;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return ActualQtyInterval;
        }

        public string FN_GET_ACTUALQTY(string zerodate1, string zerodate2, double days)
        {
            int day = Convert.ToInt32(days);
            return db.Database.SqlQuery<string>("SELECT dbo.FN_GET_ACTUALQTY('" + zerodate1 + "','" + zerodate2 + "','" + day + "')").FirstOrDefault();
        }

        public string GET_PART_DESCRIPTION_FROM_HBOM(string Project, string PartCode)
        {
            return db.Database.SqlQuery<string>("SELECT dbo.FN_COMMON_GET_PART_DESCCRIPTION_HBOM('" + Project + "','" + PartCode + "')").FirstOrDefault();
        }


        public bool SetLoginSessionDetails(string UserName, ref string Msg, clsImplementationEnum.UserActionType LoginType)
        {
            bool Status = false;
            try
            {
                string strReleased = clsImplementationEnum.AppVersionStatus.Released.GetStringValue();
                //COM003 objCOM003 = db.COM003.Where(x => x.t_psno == objclsLogin.Username && x.t_actv == 1).FirstOrDefault();
                var objCOM003 = db.SP_GET_LOGIN_DETAILS(UserName).FirstOrDefault();
                if (!string.IsNullOrEmpty(objCOM003.Msg))
                {
                    Msg = objCOM003.Msg;
                }

                if (Msg.Length == 0)
                {
                    #region Set Login Session
                    System.Web.HttpContext.Current.Session["userid"] = objCOM003.t_psno;
                    clsLoginInfo objclsLoginInfo = new clsLoginInfo();
                    objclsLoginInfo.UserName = objCOM003.t_psno;
                    objclsLoginInfo.Name = objCOM003.t_name;
                    objclsLoginInfo.Location = objCOM003.t_loca;
                    objclsLoginInfo.Department = objCOM003.t_depc;
                    objclsLoginInfo.DepartmentName = objCOM003.Department;
                    objclsLoginInfo.LocationName = objCOM003.Location;
                    objclsLoginInfo.Empname = objCOM003.t_name + " " + "(" + objCOM003.t_init + ")";
                    objclsLoginInfo.Initial = objCOM003.t_init;
                    objclsLoginInfo.Emailid = objCOM003.EmailId;
                    objclsLoginInfo.Designation = objCOM003.t_desi;
                    objclsLoginInfo.ListRoles = db.SP_GET_USER_ROLE_NAME_LIST(objCOM003.t_psno).ToList();// objclsLoginInfo.GetUserRoleList();
                    objclsLoginInfo.UserRoles = string.Join(",", objclsLoginInfo.ListRoles);
                    objclsLoginInfo.IsRoleFound = objclsLoginInfo.ListRoles.Count > 0;
                    objclsLoginInfo.Version = objCOM003.AppVersion;
                    var objConfig = db.CONFIG.Where(x => x.Key == "DefaultMenuOrder").Select(i => i.Value).FirstOrDefault();
                    objclsLoginInfo.IsUserMenu = objConfig;
                    objclsLoginInfo.AppVersionId = objclsLoginInfo.AppVersionId;

                    if (objclsLoginInfo.IsUserMenu == "0")
                    {
                        objclsLoginInfo.listMenuResult = db.SP_ATH_INS_DEL_GET_USER_WISE_ACCESS_MENU(objclsLoginInfo.UserName).Where(x => x.IsDisplayInMenu == true).ToList();
                    }

                    System.Web.HttpContext.Current.Session[clsImplementationMessage.Session.LoginInfo] = objclsLoginInfo;
                    if (LoginType == clsImplementationEnum.UserActionType.Login)
                    {
                        Add(clsImplementationEnum.UserActionType.Login.GetStringValue(), "Login Successful");
                    }
                    else if (LoginType == clsImplementationEnum.UserActionType.Login)
                    {
                        Add(clsImplementationEnum.UserActionType.LoginAs.GetStringValue(), objClsLoginInfo.UserName + " Login as " + UserName + " - " + objCOM003.t_name);
                    }
                    Status = true;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Status;
        }

        public string GetSCUDashboardLink(string Project = "")
        {
            var PowerBIReportURL = db.CONFIG.Where(i => i.Key == "PowerBIReportURL").FirstOrDefault().Value;
            var isRolePLNG3 = objClsLoginInfo.ListRoles.Contains(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue());
            PowerBIReportURL += "Power%20BI%20Reports/IEMQS/SCU/" + (isRolePLNG3 ? "S%20Curve%20For%20Planner" : "S%20Curve") + "?rs:embed=true" + (!string.IsNullOrEmpty(Project) ? "&filter=ProjectList/Project%20eq%20%27" + Project + "%27" : "");
            return PowerBIReportURL;
        }

        public bool IsUserInRole(string UserPSNo, string[] MatchRoles)
        {
            var UserRoles = (from a in db.ATH001
                             join b in db.ATH004 on a.Role equals b.Id
                             where a.Employee.Equals(UserPSNo, StringComparison.OrdinalIgnoreCase) && MatchRoles.Contains(b.Role)
                             select new { RoleId = a.Role, RoleDesc = b.Role }).ToList();

            bool isRole = false;
            if (UserRoles.Count() > 0)
            {
                isRole = true;
            }

            return isRole;
        }

    }
    public class clsNDETechniques
    {
        public string TechniqueName { get; set; }
        public string TechniqueNo { get; set; }
        public int? TechniqueRev { get; set; }
        public string TechniqueUrl { get; set; }
        public string TechniqueDisplayNo { get; set; }
    }
    public class SearchFilter
    {
        public string ColumnName { get; set; }
        private string _Value { get; set; }
        public string Value { get { return _Value; } set { _Value = value == null ? null : value.Replace("'", "''"); } }
        public string FilterType { get; set; }
        public string DataType { get; set; }
    }
    public class AccessRight
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
    public class EmployeeList
    {
        public int isActive { get; set; }
        public string psno { get; set; }
        public string name { get; set; }
        public string psnoname { get; set; }
        public string department { get; set; }
        public string location { get; set; }
    }
}
