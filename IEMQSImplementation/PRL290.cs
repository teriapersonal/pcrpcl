//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class PRL290
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PRL290()
        {
            this.PRL291 = new HashSet<PRL291>();
            this.PRL292 = new HashSet<PRL292>();
            this.PRL293 = new HashSet<PRL293>();
        }
    
        public int HeaderId { get; set; }
        public string ProtocolNo { get; set; }
        public string Project { get; set; }
        public string BU { get; set; }
        public string Location { get; set; }
        public string QualityProject { get; set; }
        public string EquipmentNo { get; set; }
        public string DrawingNo { get; set; }
        public Nullable<int> DrawingRevisionNo { get; set; }
        public string DCRNo { get; set; }
        public Nullable<System.DateTime> InspectionDate { get; set; }
        public string SeamNo { get; set; }
        public string StageCode { get; set; }
        public Nullable<int> StageSequence { get; set; }
        public Nullable<int> IterationNo { get; set; }
        public Nullable<int> RequestNoSequence { get; set; }
        public Nullable<long> RequestNo { get; set; }
        public string InspectionAgency { get; set; }
        public Nullable<System.DateTime> OfferDate { get; set; }
        public Nullable<int> ICLRevNo { get; set; }
        public string ReqOutsideDiameter { get; set; }
        public string ReqInsideDiameter { get; set; }
        public string ReqRootGap { get; set; }
        public string ReqRootFace { get; set; }
        public string ReqTopSideWepAngle { get; set; }
        public string ReqBottomSideWepAngle { get; set; }
        public string ReqOffset { get; set; }
        public string CheckPoint1 { get; set; }
        public string CheckPoint2 { get; set; }
        public string CheckPoint3 { get; set; }
        public string CheckPoint4 { get; set; }
        public string CheckPoint5 { get; set; }
        public string CheckPoint6 { get; set; }
        public string CheckPoint8 { get; set; }
        public Nullable<System.DateTime> CheckPoint8_2 { get; set; }
        public string QCRemarks { get; set; }
        public string Result { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public string ReqFilledBy { get; set; }
        public Nullable<System.DateTime> ReqFilledOn { get; set; }
        public string ActFilledBy { get; set; }
        public Nullable<System.DateTime> ActFilledOn { get; set; }
        public string InspectedBy { get; set; }
        public Nullable<System.DateTime> InspectedOn { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRL291> PRL291 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRL292> PRL292 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRL293> PRL293 { get; set; }
    }
}
