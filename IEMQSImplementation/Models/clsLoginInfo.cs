﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMQSImplementation.Models
{
    [Serializable]
    public class clsLoginInfo
    {
        private string _Name;
        private string _UserName;
        private string _ProcessIds;
        private string _Location;
        private string _Department;
        private string _Version;
        private string _DepartmentName;
        private string _Emailid;
        private string _LocationName;
        private string _Initial;
        private string _Designation;
        private string _Empname;
        private string _UserRoles;
        private List<string> _rolelist;
        private List<SP_ATH_INS_DEL_GET_USER_WISE_ACCESS_MENU_Result> _listMenuResult;
        private string _IsUserMenu;
        private int _AppVersionId;
        public string Name { get { return _Name; } set { _Name = value; } }
        public string UserName { get { return _UserName; } set { _UserName = value; } }
        public string ProcessIds { get { return _ProcessIds; } set { _ProcessIds = value; } }
        public string Location { get { return _Location; } set { _Location = value; } }
        public string Department { get { return _Department; } set { _Department = value; } }
        public string Version { get { return _Version; } set { _Version = value; } }
        public string DepartmentName { get { return _DepartmentName; } set { _DepartmentName = value; } }
        public string Emailid { get { return _Emailid; } set { _Emailid = value; } }
        public string LocationName { get { return _LocationName; } set { _LocationName = value; } }
        public string Initial { get { return _Initial; } set { _Initial = value; } }
        public string Empname { get { return _Empname; } set { _Empname = value; } }
        public string Designation { get { return _Designation; } set { _Designation = value; } }
        public string UserRoles { get { return _UserRoles; } set { _UserRoles = value; } }
        public List<string> ListRoles { get { return _rolelist; } set { _rolelist = value; } }
        public bool IsRoleFound;
        public List<SP_ATH_INS_DEL_GET_USER_WISE_ACCESS_MENU_Result> listMenuResult { get { return _listMenuResult; } set { _listMenuResult = value; } }
        public string IsUserMenu { get { return _IsUserMenu; } set { _IsUserMenu = value; } }
        public int AppVersionId { get { return _AppVersionId; } set { _AppVersionId = value; } }

        public string GetUserRole()
        {
            string userRole = "";
            if (System.Web.HttpContext.Current.Session[clsImplementationMessage.Session.LoginInfo] != null)
            {
                var objClsLogin = (clsLoginInfo)System.Web.HttpContext.Current.Session[clsImplementationMessage.Session.LoginInfo];
                userRole = objClsLogin.UserRoles;
            }
            else
            {
                var rolelist = GetUserRoleList();
                IsRoleFound = rolelist.Count > 0;
                userRole = string.Join(",", rolelist);
            }
            return userRole;
        }
        public List<string> GetUserRoleList()
        {
            List<string> rolelist = new List<string>();
            if (System.Web.HttpContext.Current.Session[clsImplementationMessage.Session.LoginInfo] != null)
            {
                var objClsLogin = (clsLoginInfo)System.Web.HttpContext.Current.Session[clsImplementationMessage.Session.LoginInfo];
                rolelist = objClsLogin.ListRoles;
            }
            else
            {
                IEMQSEntitiesContext db = new IEMQSEntitiesContext();
                rolelist = (from ath1 in db.ATH001
                            join ath4 in db.ATH004 on ath1.Role equals ath4.Id
                            where ath1.Employee == UserName
                            select ath4.Role
                           ).Distinct().ToList();
                IsRoleFound = rolelist.Count > 0;
            }
            return rolelist;
        }
    }

    public class FunctionModel
    {
        public int t_dtyp { get; set; }
        public string t_dimx { get; set; }
        public string t_desc { get; set; }
    }
}
