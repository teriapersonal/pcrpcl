﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace IEMQSImplementation.Models
{
   public class MailConfiguration
    {
        public MAIL001 MAIL001 { get; set; }
        [AllowHtml]
        [UIHint("tinymce_jquery_full")]
        [Required(ErrorMessage = "Required")]
        public string ContentBody { get; set; }
        public string TemplateName { get; set; }
    }
}
