//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class WPS025
    {
        public int Id { get; set; }
        public string QualityProject { get; set; }
        public string Project { get; set; }
        public string BU { get; set; }
        public string Location { get; set; }
        public string SeamNo { get; set; }
        public string WeldingProcess { get; set; }
        public string WelderPSNO { get; set; }
        public string PrintedBy { get; set; }
        public Nullable<System.DateTime> PrintedOn { get; set; }
        public string WPSNumber { get; set; }
        public Nullable<int> WPSRevNo { get; set; }
        public string WeldLayer { get; set; }
        public string JointType { get; set; }
        public Nullable<System.DateTime> ValidUpto { get; set; }
        public string WelderStamp { get; set; }
        public Nullable<int> RefTokenId { get; set; }
    }
}
