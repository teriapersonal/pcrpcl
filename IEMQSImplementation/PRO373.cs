//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class PRO373
    {
        public int LineId { get; set; }
        public Nullable<int> HeaderId { get; set; }
        public Nullable<int> SrNo { get; set; }
        public string AtTopThickness { get; set; }
        public string AtBottomThickness { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
    
        public virtual PRO370 PRO370 { get; set; }
    }
}
