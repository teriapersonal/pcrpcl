﻿using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace IEMQSImplementation.ILN.Persistence
{
    public sealed class DataConnection 
    {
        private readonly SqlConnection connection;

        /// <summary>
        /// DataConnection
        /// </summary>
        /// <param name="providedConnectionString"></param>
        public DataConnection(string providedConnectionString)
        {
            connection = connection ?? new SqlConnection(providedConnectionString);
        }

        /// <summary>
        /// ExecuteAsync
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public async Task<int> ExecuteAsync(DataConnectionParams parameter)
        {
            var executeAsync = connection.ExecuteAsync(parameter.Query, parameter.Params, commandType: parameter.QueryType);
            return await PerformOperationAsync(executeAsync);
        }

        /// <summary>
        /// ExecuteQueryWithDynamicAsync
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public async Task<IEnumerable<int>> ExecuteQueryWithDynamicAsync<T>(DataConnectionParams<T> parameter)
        {
            var executeAsync = connection.QueryAsync<int>(parameter.Query, parameter.Params, commandType: parameter.QueryType);
            return await PerformOperationAsync(executeAsync);
        }

        /// <summary>
        /// ExecuteBoolAsync
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public async Task<bool> ExecuteBoolAsync(DataConnectionParams parameter)
        {
            var executeResult = await ExecuteAsync(parameter);
            return executeResult > 0;
        }

        /// <summary>
        /// ExecuteScalarAsync
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public async Task<object> ExecuteScalarAsync(DataConnectionParams parameter)
        {
            var executeScalarAsync = connection.ExecuteScalarAsync(parameter.Query, parameter.Params, commandType: parameter.QueryType);
            return await PerformOperationAsync(executeScalarAsync);
        }

        /// <summary>
        /// QueryAsync
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public async Task<IEnumerable<T>> QueryAsync<T>(DataConnectionParams parameter)
        {
            var queryAsync = connection.QueryAsync<T>(parameter.Query, parameter.Params, commandType: parameter.QueryType);
            return await PerformOperationAsync(queryAsync);
        }

        

        /// <summary>
        /// QueryDynamicMultipleAsync
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public async Task<List<List<IDictionary<string, object>>>> QueryDynamicMultipleAsync(DataConnectionParams<DynamicParameter> parameter)
        {
            var queryAsync = connection.QueryMultipleAsync(parameter.Query, parameter.Params as DynamicParameters, null, null, parameter.QueryType);
            var gridReader = await PerformOperationAsync(queryAsync, false);
            List<List<IDictionary<string, object>>> managedLists = new List<List<IDictionary<string, object>>>();
            while (gridReader.IsConsumed == false)
            {
                var task = gridReader.ReadAsync<dynamic>();
                //var closeConnection = gridReader.IsConsumed;
                var dynamicTable = (await PerformOperationAsync(task, false)).AsList();
                List<IDictionary<string, object>> managedList = new List<IDictionary<string, object>>();
                foreach (var row in dynamicTable)
                {
                    managedList.Add(row as IDictionary<string, object>);
                }

                managedLists.Add(managedList);
            }
            connection.Close();

            return managedLists;
        }       

        private async Task<T> PerformOperationAsync<T>(Task<T> task, bool closeConnection = true)
        {
            if (connection.State == ConnectionState.Closed)
                await connection.OpenAsync();

            var result = await task;
            if(closeConnection)
                connection.Close();
            return result;
        }
    }
}
