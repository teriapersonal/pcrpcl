﻿using Dapper;
using IEMQSImplementation.ILN.Persistence.Enums;
using System.Collections.Generic;

namespace IEMQSImplementation.ILN.Persistence
{
    public class DataConnectionParams<T> : DataConnectionParams
    {

        public DataConnectionParams(DataConnectionTypeEnum queryType = DataConnectionTypeEnum.StoredProcedure) : base(queryType)
        {

        }

        public override object Params
        {
            get
            {
                return _Params as DynamicParameters;
            }
            set
            {
                if (typeof(T) == typeof(DynamicParameter))
                {
                    List<DynamicParameter> parametersList = value as List<DynamicParameter>;
                    DynamicParameters dynamicParameters = new DynamicParameters();
                    foreach (DynamicParameter dynamicParameter in parametersList)
                    {
                        dynamicParameters.Add(dynamicParameter.Name, dynamicParameter.Value, dynamicParameter.DbType);
                    }
                    _Params = dynamicParameters;
                }
                else if (typeof(T) == typeof(object))
                {
                    _Params = value;
                }
            }
        }
    }
}
