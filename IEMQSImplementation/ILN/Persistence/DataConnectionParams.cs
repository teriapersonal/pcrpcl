﻿using Dapper;
using IEMQSImplementation.ILN.Persistence.Enums;
using System.Collections.Generic;
using System.Data;

namespace IEMQSImplementation.ILN.Persistence
{
    public class DataConnectionParams 
    {
        protected object _Params;
        /// <summary>
        /// DataConnectionParams
        /// </summary>
        /// <param name="queryType"></param>
        public DataConnectionParams(DataConnectionTypeEnum queryType = DataConnectionTypeEnum.Text)
        {
            QueryType = queryType == DataConnectionTypeEnum.Text
                ? CommandType.Text
                : CommandType.StoredProcedure;            
        }

        public virtual object Params { get; set; }
        
        
        /// <summary>
        /// Query
        /// </summary>
        public string Query { get; set; }

        /// <summary>
        /// QueryType
        /// </summary>
        public CommandType QueryType { get; private set; }        
    }    
}
