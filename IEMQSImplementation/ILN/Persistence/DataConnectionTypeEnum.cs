﻿namespace IEMQSImplementation.ILN.Persistence.Enums
{
    /// <summary>
    /// DataConnectionType
    /// </summary>
    public enum DataConnectionTypeEnum
    {
        /// <summary>
        /// StoredProcedure
        /// </summary>
        StoredProcedure,

        /// <summary>
        /// Text
        /// </summary>
        Text
    }
}
