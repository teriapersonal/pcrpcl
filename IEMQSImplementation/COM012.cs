//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class COM012
    {
        public string t_item { get; set; }
        public int t_kitm { get; set; }
        public string t_citg { get; set; }
        public int t_itmt { get; set; }
        public string t_dsca { get; set; }
        public string t_dscb { get; set; }
        public string t_dscc { get; set; }
        public string t_dscd { get; set; }
        public string t_seak { get; set; }
        public string t_seab { get; set; }
        public string t_uset { get; set; }
        public string t_cuni { get; set; }
        public string t_cwun { get; set; }
        public double t_wght { get; set; }
        public string t_ctyp { get; set; }
        public int t_ltct { get; set; }
        public string t_csel { get; set; }
        public string t_csig { get; set; }
        public string t_ctyo { get; set; }
        public string t_cpcl { get; set; }
        public string t_cood { get; set; }
        public int t_eitm { get; set; }
        public int t_umer { get; set; }
        public string t_cpln { get; set; }
        public string t_ccde { get; set; }
        public string t_cmnf { get; set; }
        public string t_cean { get; set; }
        public int t_cont { get; set; }
        public string t_cntr { get; set; }
        public string t_cprj { get; set; }
        public int t_repl { get; set; }
        public int t_cpva { get; set; }
        public string t_dfit { get; set; }
        public int t_stoi { get; set; }
        public string t_cpcp { get; set; }
        public int t_unef { get; set; }
        public int t_ichg { get; set; }
        public int t_uefs { get; set; }
        public int t_seri { get; set; }
        public int t_styp { get; set; }
        public int t_psiu { get; set; }
        public string t_efco { get; set; }
        public System.DateTime t_indt { get; set; }
        public int t_chma { get; set; }
        public int t_edco { get; set; }
        public int t_mcoa { get; set; }
        public int t_opts { get; set; }
        public int t_envc { get; set; }
        public int t_sayn { get; set; }
        public int t_subc { get; set; }
        public int t_srce { get; set; }
        public int t_cnfg { get; set; }
        public System.DateTime t_lmdt { get; set; }
        public string t_exin { get; set; }
        public string t_imag { get; set; }
        public int t_efpr { get; set; }
        public int t_ippg { get; set; }
        public int t_ppeg { get; set; }
        public int t_dpeg { get; set; }
        public int t_dpcr { get; set; }
        public int t_dptp { get; set; }
        public int t_dpuu { get; set; }
        public int t_elrq { get; set; }
        public int t_elcm { get; set; }
        public int t_icsi { get; set; }
        public int t_txta { get; set; }
        public double t_cdf_dens { get; set; }
        public string t_cdf_dsca { get; set; }
        public int t_cdf_fvmi { get; set; }
        public double t_cdf_thic { get; set; }
        public int t_Refcntd { get; set; }
        public int t_Refcntu { get; set; }
    }
}
