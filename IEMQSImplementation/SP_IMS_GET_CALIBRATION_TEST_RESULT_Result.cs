//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    
    public partial class SP_IMS_GET_CALIBRATION_TEST_RESULT_Result
    {
        public Nullable<long> ROW_NO { get; set; }
        public Nullable<int> TotalCount { get; set; }
        public int LineId { get; set; }
        public int HeaderId { get; set; }
        public string Location { get; set; }
        public string InstrumentNumber { get; set; }
        public Nullable<System.DateTime> LastCalibrationDate { get; set; }
        public Nullable<System.DateTime> CalibrationDueDate { get; set; }
        public string CalibrationReportSummary { get; set; }
        public string Unblock { get; set; }
        public string Remarks { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
    }
}
