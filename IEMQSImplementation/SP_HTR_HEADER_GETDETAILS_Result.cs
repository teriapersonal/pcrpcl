//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    
    public partial class SP_HTR_HEADER_GETDETAILS_Result
    {
        public Nullable<long> ROW_NO { get; set; }
        public Nullable<int> TotalCount { get; set; }
        public string Project { get; set; }
        public string Location { get; set; }
        public Nullable<int> RevNo { get; set; }
        public string Status { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public int HeaderId { get; set; }
        public string HTRType { get; set; }
        public string Department { get; set; }
        public string HTRNo { get; set; }
        public string DONo { get; set; }
        public string PoNo { get; set; }
        public string ConcertoMSP { get; set; }
        public string Treatment { get; set; }
        public string HTRReferenceNo { get; set; }
        public string Furnace { get; set; }
        public bool FurnaceCommentsRequired { get; set; }
        public string FurnaceSupervisor { get; set; }
        public bool InspectionCommentsRequired { get; set; }
        public string Inspector { get; set; }
        public string MET { get; set; }
        public string MinLoadingTemp { get; set; }
        public string MaxLoadingTemp { get; set; }
        public string MinRateOfHeating { get; set; }
        public string MaxRateOfHeating { get; set; }
        public string MinSoakingTemperature { get; set; }
        public string MaxSoakingTemperature { get; set; }
        public string SoakingTimeHH { get; set; }
        public string SoakingTimeMM { get; set; }
        public string SoakingTimeTolerance { get; set; }
        public string MinRateOfCooling { get; set; }
        public string MaxRateOfCooling { get; set; }
        public string MinUnloadingTemp { get; set; }
        public string MaxUnloadingTemp { get; set; }
        public string CoolPart { get; set; }
        public string CoolBy { get; set; }
        public Nullable<int> MaxDelay { get; set; }
        public string VolTempOf { get; set; }
        public Nullable<int> TempOfLiquidAfterQuenching { get; set; }
        public string MinVol { get; set; }
        public string HeatingThermocouplesVariation { get; set; }
        public string CoolingThermocouplesVariation { get; set; }
        public string StandardProcedure { get; set; }
        public string FurnaceBedDistance { get; set; }
        public string WPSNo { get; set; }
        public string Comments { get; set; }
        public string FurnaceComments { get; set; }
        public string InspectorComments { get; set; }
        public string ReviewerComments { get; set; }
        public Nullable<System.DateTime> SubmittedOn { get; set; }
        public Nullable<System.DateTime> ReviewdOn { get; set; }
        public Nullable<System.DateTime> FurnaceOn { get; set; }
        public Nullable<System.DateTime> InspectOn { get; set; }
        public string HTRDescription { get; set; }
        public string SubmittedBy { get; set; }
        public string PartDescription { get; set; }
        public string ProjectDesc { get; set; }
        public string CreatedByDesc { get; set; }
        public string EditedByDesc { get; set; }
    }
}
