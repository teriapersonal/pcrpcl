//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class QMS054
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public QMS054()
        {
            this.QMS054_1 = new HashSet<QMS054_1>();
        }
    
        public int PMIId { get; set; }
        public Nullable<int> HeaderId { get; set; }
        public Nullable<int> RefRequestId { get; set; }
        public string Instrument { get; set; }
        public string Element1 { get; set; }
        public string Element2 { get; set; }
        public string Element3 { get; set; }
        public string Element4 { get; set; }
        public string Element5 { get; set; }
        public string Element6 { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public string Inspectedby { get; set; }
        public Nullable<System.DateTime> InspectedOn { get; set; }
    
        public virtual QMS040 QMS040 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<QMS054_1> QMS054_1 { get; set; }
        public virtual QMS050 QMS050 { get; set; }
    }
}
