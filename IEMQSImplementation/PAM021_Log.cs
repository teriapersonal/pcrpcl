//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class PAM021_Log
    {
        public int Id { get; set; }
        public int RefId { get; set; }
        public int LineId { get; set; }
        public int PDHId { get; set; }
        public string Plates_Deviation { get; set; }
        public string Forged_shell_Deviation { get; set; }
        public string Tube_sheet_shell_Deviation { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
    }
}
