﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMQSImplementation.PCR
{   
    public class RETURN_BALANCE_DISTRIBUTION_Result
    {
        [Column(new int[] { (int)clsImplementationEnum.Callers.MaintainReturnDistribution }, "Id", true, true, true, ControlType.Label, 0)]
        public int Id { get; set; }

        [Column(new int[] { (int)clsImplementationEnum.Callers.MaintainReturnDistribution }, "Sequence", false, false, false, ControlType.Label, 1)]
        public int Sequence { get; set; }

        [Column(new int[] { (int)clsImplementationEnum.Callers.MaintainReturnDistribution }, "PCL Number", true, true, true, ControlType.Label, 2)]
        public string PCLNumber { get; set; }

        [Column(new int[] { (int)clsImplementationEnum.Callers.MaintainReturnDistribution }, "Odd Shape", false, false, false, ControlType.AutoComplete, 3)]
        public Nullable<int> OddShape { get; set; }

        [Column(new int[] { (int)clsImplementationEnum.Callers.MaintainReturnDistribution }, "Length", false, false, false, ControlType.Number, 4)]
        public Nullable<double> Length { get; set; }

        [Column(new int[] { (int)clsImplementationEnum.Callers.MaintainReturnDistribution }, "Width", false, false, false, ControlType.Number, 5)]
        public Nullable<double> Width { get; set; }

        [Column(new int[] { (int)clsImplementationEnum.Callers.MaintainReturnDistribution }, "Area", false, false, false, ControlType.Number, 6)]
        public Nullable<double> Area { get; set; }

        [Column(new int[] { (int)clsImplementationEnum.Callers.MaintainReturnDistribution }, "Maximum Return Area", true, true, true, ControlType.Label, 7)]
        public Nullable<double> MaximumReturnArea { get; set; }

        [Column(new int[] { (int)clsImplementationEnum.Callers.MaintainReturnDistribution }, "Return Balance", true, true, true, ControlType.Label, 8)]
        public Nullable<int> ReturnBalance { get; set; }

        [Column(new int[] { (int)clsImplementationEnum.Callers.MaintainReturnDistribution }, "Remark", false, false, false, ControlType.Text, 9)]
        public string Remark { get; set; }

        [Column(new int[] { (int)clsImplementationEnum.Callers.MaintainReturnDistribution }, "Length Original", true, true, true, ControlType.Label, 10)]
        public Nullable<float> LengthOriginal { get; set; }

        [Column(new int[] { (int)clsImplementationEnum.Callers.MaintainReturnDistribution }, "Width Original", true, true, true, ControlType.Label, 11)]
        public Nullable<float> WidthOriginal { get; set; }
        

        [Column(new int[] { (int)clsImplementationEnum.Callers.MaintainReturnDistribution }, "Area Original", true, true, true, ControlType.Label, 12)]
        public Nullable<float> AreaOriginal { get; set; }
        

        [Column(new int[] { (int)clsImplementationEnum.Callers.MaintainReturnDistribution }, "Revision", true, true, true, ControlType.Label, 13)]
        public Nullable<int> Revision { get; set; }

        [Column(new int[] { (int)clsImplementationEnum.Callers.MaintainReturnDistribution }, "Item Type", true, true, true, ControlType.Label, 14)]
        public Nullable<int> ItemType { get; set; }

        [Column(new int[] { (int)clsImplementationEnum.Callers.MaintainReturnDistribution }, "Action", false, true, true, ControlType.ActionRender, 15)]
        public string Action { get; set; }
        
        public string LocationOriginal { get; set; }
        public Nullable<int> OddShapeOriginal { get; set; }
        public Nullable<float> FactorOriginal { get; set; }
        public Nullable<float> ThicknessOriginal { get; set; }
        public Nullable<float> CCD1Original { get; set; }
        public Nullable<float> CDT1Original { get; set; }
        public Nullable<float> CCD2Original { get; set; }
        public Nullable<float> CDT2Original { get; set; }
        public string PCLStatus { get; set; }

    }
}
