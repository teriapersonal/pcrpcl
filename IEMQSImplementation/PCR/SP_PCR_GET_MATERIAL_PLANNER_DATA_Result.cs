﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMQSImplementation.PCR
{
    public class SP_PCR_GET_MATERIAL_PLANNER_DATA_Result
    {
        public Nullable<long> ROW_NO { get; set; }
        public int TotalCount { get; set; }
        public decimal Id { get; set; }
        public string PCRNumber { get; set; }
        public Nullable<int> PCRLineNo { get; set; }
        public Nullable<int> PCRLineRevision { get; set; }
        public Nullable<int> PCRType { get; set; }
        public Nullable<System.DateTime> PCRRequirementDate { get; set; } // PCRRequirementDate
        public Nullable<int> PCRLineStatus { get; set; }
        public Nullable<int> PartNumber { get; set; }
        public string ManufacturingItem { get; set; }
        public string ManufacturingItemDesc { get; set; }
        public string ChildItem { get; set; }
        public string ChildItemDesc { get; set; }
        public Nullable<float> ScrapQuantity { get; set; }
        public Nullable<int> NestedQuantity { get; set; }
        public Nullable<float> AreaConsumed { get; set; }
        public Nullable<float> Length { get; set; }
        public Nullable<float> Width { get; set; }
        public Nullable<int> NoOfPieces { get; set; }
        public string StockNumber { get; set; }
        public string PCLNumber { get; set; }
        public Nullable<int> MovementType { get; set; }
        public string PCRPlannerRemark { get; set; }
        public Nullable<int> StockRevision { get; set; }
        public string MaterialPlanner { get; set; }
        public Nullable<System.DateTime> PCRCreationDate { get; set; }
        public Nullable<System.DateTime> PCRReleaseDate { get; set; }
        public Nullable<System.DateTime> PCLCreationDate { get; set; }
        public Nullable<System.DateTime> PCLReleaseDate { get; set; }
        public string Project { get; set; }
        public string Element { get; set; }
        public Nullable<int> ManufacturingItemPosition { get; set; }
        public string ProjectDesc { get; set; } // from diff. table
        public string ElementDesc { get; set; } // from diff. table
        public string GrainOrientation { get; set; }
        public string ShapeType { get; set; }
        public string ShapeFile { get; set; }
        public Nullable<float> Thickness { get; set; }
        public string WCDeliver { get; set; }
        public string WCDeliverName { get; set; } // from diff. table
        public string MaterialSpecification { get; set; } // from diff. table
        public string PCRHardCopyNo { get; set; }
        public string CuttingLocation { get; set; }
        public Nullable<int> UseAlternateStock { get; set; }
        public string AlternateStockRemark { get; set; }
        public string OriginalItem { get; set; }
        public string EmployeeName { get; set; } // from diff. table
        public string PCRInitiator { get; set; } // from diff. table
        public Nullable<float> LengthModified { get; set; } // from diff. table
        public Nullable<float> WidthModified { get; set; } // from diff. table
        public string CuttingLocationModified { get; set; } // from diff. table
        public string ItemRevision { get; set; }
        public Nullable<int> LinkToSCR { get; set; }
        public string SCRNumber { get; set; }
        public string ARMCode { get; set; } // from diff. table
        public Nullable<int> Itemtype { get; set; }
        public string PCRParameter { get; set; } // from diff. table
        public string LocationAddress { get; set; } // from diff. table
    }
}
