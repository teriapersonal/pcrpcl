﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMQSImplementation.PCR
{
    public class SP_BEFORE_REWRITE_Result
    {
        public int Status { get; set; }
        public string Message { get; set; }
    }
}
