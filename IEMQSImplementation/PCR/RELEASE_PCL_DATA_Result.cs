﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMQSImplementation.PCR
{
    public class RELEASE_PCL_DATA_Result
    {
        public int Id { get; set; }
        public string PCLNumber { get; set; }
        public Nullable<int> Revision { get; set; }
        public Nullable<int> ItemType { get; set; }
        public Nullable<double> TotalConsumedArea { get; set; }
        public Nullable<double> ScrapQuantity { get; set; }
        public Nullable<double> ReturnArea { get; set; }
        public string PCLStatus { get; set; }
        public string ReturnRemark { get; set; }
        public Nullable<double> TotalCuttingLength { get; set; }
        public string StockNumber { get; set; }
        public Nullable<double> TotalArea { get; set; }
        public string WCDeliver { get; set; }
        public string WCDeliverName { get; set; }
        public string LoadPlantMachine { get; set; }
        public string PlannerName { get; set; }
        public string Remark{ get; set; }
        public Nullable<int> MovementType { get; set; }
        public Nullable<System.DateTime> PCLReleaseDate { get; set; }
        public Nullable<long> ReturnBalance { get; set; }
        public string AllowedStage1 { get; set; }
        public Nullable<int> AllowedStage1Name { get; set; }
        public Nullable<bool> DisableSTGI { get; set; }
        public Nullable<bool> DisableReturnBalance { get; set; }
        

    }
}
