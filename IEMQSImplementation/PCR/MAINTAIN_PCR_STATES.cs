﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMQSImplementation.PCR
{
    public class MAINTAIN_PCR_STATES
    {
        public bool FirstEnabled { get; set; }
        public bool LastEnabled { get; set; }
        public bool PreviousEnabled { get; set; }
        public bool NextEnabled { get; set; }
    }
}
