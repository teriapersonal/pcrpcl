﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMQSImplementation.PCR
{
    public class MAINTAIN_PCR_LINE_Result
    {
        public int Id { get; set; }
        public string PCRNumber { get; set; }
        public Nullable<int> PCRLineNo { get; set; }
        public Nullable<int> PCRLineRevision { get; set; }
        public Nullable<System.DateTime> PCRCreationDate { get; set; }
        public Nullable<System.DateTime> PCRReleaseDate { get; set; }
        public Nullable<System.DateTime> PCLCreationDate { get; set; }
        public Nullable<System.DateTime> PCLReleaseDate { get; set; }
        public string Project { get; set; }
        public string ProjectDesc { get; set; }
        public string Element { get; set; }
        public string ElementDesc { get; set; }
        public string PCRHardCopyNo { get; set; }
        public Nullable<double> ScrapQuantity { get; set; }
        public Nullable<long> NestedQuantity { get; set; }
        public Nullable<double> AreaConsumed { get; set; }
        public Nullable<double> Length { get; set; }
        public Nullable<double> Width { get; set; }
        public Nullable<long> NoOfPieces { get; set; }
        public Nullable<double> AreaOfPieces { get; set; }
        public Nullable<double> TotalArea { get; set; }
        public string WCDeliver { get; set; }
        public string StockNumber { get; set; }
        public Nullable<int> MovementType { get; set; }
        public string PCRPlannerRemark { get; set; }
        public Nullable<long> PartNumber { get; set; }
        public Nullable<long> FindNo { get; set; }
        public string ManufacturingItem { get; set; }
        public string ManufacturingItemDesc { get; set; }
        public string ChildItemSegment { get; set; }
        public string ChildItem { get; set; }
        public string ChildItemDesc { get; set; }
        public string ItemMaterialGrade { get; set; }
        public Nullable<int> PCRType { get; set; }
        public Nullable<System.DateTime> PCRRequirementDate { get; set; }
        public string CuttingLocation { get; set; }
        public Nullable<int> PCRLineStatus { get; set; }
        public string PCLNumber { get; set; }
        public Nullable<long> P1 { get; set; }
        public Nullable<long> P2 { get; set; }
        public Nullable<long> P3 { get; set; }
        public Nullable<long> P4 { get; set; }
        public Nullable<long> P5 { get; set; }
        public Nullable<long> P6 { get; set; }
        public Nullable<long> P7 { get; set; }
        public Nullable<long> P8 { get; set; }
        public Nullable<long> P9 { get; set; }
        public Nullable<long> P10 { get; set; }

    }
}
