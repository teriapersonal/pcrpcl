﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMQSImplementation.PCR
{
    public class MAINTAIN_PCR_HEADER_Result
    {
        public decimal Id { get; set; }
        public string PCRNumber { get; set; }
        public Nullable<System.DateTime> CreationDate { get; set; }
        public Nullable<System.DateTime> ReleaseDate { get; set; }
        public string Location { get; set; }
        public string LocationName { get; set; }
        public string Initiator { get; set; }
        public string InitiatorName { get; set; }
        public Nullable<int> Itemtype { get; set; }
        public string ItemTypeName { get; set; }
    }
}
