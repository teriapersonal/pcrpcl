﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMQSImplementation.PCR
{
    public class ColumnAttribute : Attribute
    {
        public bool IsInVisible;
        public bool IsNonSortable;
        public bool IsNonSearchable;
        public ControlType Type;
        public int[] screenNames;
        public string columnName;
        public int Index;
        public ColumnAttribute(
         int[] screenNames,
         string columnName,
         bool IsInVisible,
         bool IsNonSortable,
         bool IsNonSearchable,
         ControlType Type,
         int Index)
        {
            this.screenNames = screenNames;
            this.columnName = columnName;
            this.Index = Index;
            this.IsInVisible = IsInVisible;
            this.IsNonSortable = IsNonSortable;
            this.IsNonSearchable = IsNonSearchable;
            this.Type = Type;
        }

    }

    public class ActionColumnAttribute : ColumnAttribute
    {
        public bool IsApproveAction;
        public bool IsRejectAction;
        
        public ActionColumnAttribute(bool IsApproveAction,
         bool IsRejectAction) : base(new int[] { (int)clsImplementationEnum.Callers.All }, "Action", false, true, true, ControlType.ActionRender, 0)
        {
            this.IsApproveAction = IsApproveAction;
            this.IsRejectAction = IsRejectAction;
        }

    }
    public enum ControlType
    {
        Label = 0,
        Text = 1,
        Number = 2,
        Date = 3,
        ActionRender = 4,
        AutoComplete = 5
    }

}
