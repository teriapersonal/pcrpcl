﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMQSImplementation.PCR
{
    using System;
    public partial class SP_PCR_LOOK_UP
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public string CodeDescription { get; set; }
        public string ParentID { get; set; }
    }

    public class StatusMessage
    {
        public int Status { get; set; }
        public string Message { get; set; }
    }

    public class Stock
    {
        public int Id { get; set; }
        public string Contract { get; set; }
        public string Project { get; set; }
        public string ChildItem { get; set; }
        public string StockNumber { get; set; }
        public int StockRevision { get; set; }
        public float TotalArea { get; set; }
        public float AreaAllocated { get; set; }
        public float FreeArea { get; set; }     
        public bool UseAlternateStock { get; set; }
    }
}
