﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMQSImplementation.PCR
{
    public class MaintainPCRResult
    {
        public List<MAINTAIN_PCR_LINE_Result> LineResult { get; set; }
        public MAINTAIN_PCR_HEADER_Result HeaderResult { get; set; }
        public MAINTAIN_PCR_STATES States { get; set; }
        public List<string> PCRNumbers { get; set; }
    }
}
