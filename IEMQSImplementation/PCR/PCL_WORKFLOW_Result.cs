﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMQSImplementation.PCR
{
    //Needs to Be Reverted back so that appropriate columns are displayed
    public class PCL_WORKFLOW_Result
    {
        public class Columns
        {
            [Column(new int[] { (int)clsImplementationEnum.Callers.All }, "Id", true, true, true, ControlType.Label, 0)]
            public int Id { get; set; }

            [Column(new int[] { (int)clsImplementationEnum.Callers.All }, "PCL Number", false, false, false, ControlType.Label, 0)]
            public string PCLNumber { get; set; }

            [Column(new int[] { (int)clsImplementationEnum.Callers.All }, "Revision", false, false, false, ControlType.Label, 0)]
            public Nullable<int> Revision { get; set; }

            [Column(new int[] { (int)clsImplementationEnum.Callers.All }, "Movement Type", false, false, false, ControlType.Label, 0)]
            public Nullable<int> MovementType { get; set; }

            [Column(new int[] { (int)clsImplementationEnum.Callers.All }, "PCL Status", false, false, false, ControlType.Label, 0)]
            public string PCLStatus { get; set; }

            [Column(new int[] { (int)clsImplementationEnum.Callers.All }, "PCR Requirement Date", false, false, false, ControlType.Label, 0)]
            public Nullable<System.DateTime> PCRRequirementDate { get; set; }

            [Column(new int[] { (int)clsImplementationEnum.Callers.All }, "PCR Number", false, false, false, ControlType.Label, 0)]
            public string PCRNumber { get; set; }

            [Column(new int[] { (int)clsImplementationEnum.Callers.All }, "WC Deliver", false, false, false, ControlType.Label, 0)]
            public string WCDeliver { get; set; }

            [Column(new int[] { (int)clsImplementationEnum.Callers.All }, "WC Deliver Name", true, true, true, ControlType.Label, 0)]
            public string WCDeliverName { get; set; }

            [Column(new int[] { (int)clsImplementationEnum.Callers.All }, "Stock Physical", false, false, false, ControlType.Label, 0)]
            public string StockPhysical { get; set; }

            [Column(new int[] { (int)clsImplementationEnum.Callers.All }, "Stock Number", false, false, false, ControlType.Label, 0)]
            public string StockNumber { get; set; }

            [Column(new int[] { (int)clsImplementationEnum.Callers.All }, "Total Area", false, false, false, ControlType.Label, 0)]
            public Nullable<double> TotalArea { get; set; }

            [Column(new int[] { (int)clsImplementationEnum.Callers.All }, "Return Balance", false, false, false, ControlType.Label, 0)]
            public Nullable<long> ReturnBalance { get; set; }

            [Column(new int[] { (int)clsImplementationEnum.Callers.All }, "Return Area", false, false, false, ControlType.Label, 0)]
            public Nullable<double> ReturnArea { get; set; }

            [Column(new int[] { (int)clsImplementationEnum.Callers.All }, "Project", false, false, false, ControlType.Label, 0)]
            public string Project { get; set; }

            //More Fields to follow...
        }

        public class ActionRender
        {
            [ActionColumn(true, true)]
            public string Action { get; set; }
        }

        public Columns columns;
        public ActionRender actionRender;
    }
}
