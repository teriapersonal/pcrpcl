//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class PRO175
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PRO175()
        {
            this.PRO176 = new HashSet<PRO176>();
            this.PRO177 = new HashSet<PRO177>();
            this.PRO178 = new HashSet<PRO178>();
            this.PRO179_2 = new HashSet<PRO179_2>();
            this.PRO179_3 = new HashSet<PRO179_3>();
            this.PRO179_4 = new HashSet<PRO179_4>();
            this.PRO179_5 = new HashSet<PRO179_5>();
            this.PRO179_6 = new HashSet<PRO179_6>();
            this.PRO179 = new HashSet<PRO179>();
        }
    
        public int HeaderId { get; set; }
        public string ProtocolNo { get; set; }
        public string ReqShellThickness { get; set; }
        public string ActShellThickness1 { get; set; }
        public string ActShellThickness2 { get; set; }
        public string ActShellThickness3 { get; set; }
        public string ActShellThickness4 { get; set; }
        public string ActShellThickness5 { get; set; }
        public string ActShellThickness6 { get; set; }
        public string ActShellThickness7 { get; set; }
        public string ActShellThickness8 { get; set; }
        public string ActShellThickness9 { get; set; }
        public string ReqWEPDimensionRootGap { get; set; }
        public string ReqWEPDimensionRootFace { get; set; }
        public string ReqWEPDimensionInsideWEPAngle { get; set; }
        public string ReqWEPDimensionOutsideWEPAngle { get; set; }
        public string ReqWEPDimensionOffset { get; set; }
        public string CircumferenceMeasurementReqIDCF { get; set; }
        public string CircumferenceMeasurementReqODCF { get; set; }
        public string ReqTotalShellHeight { get; set; }
        public string OutOfRoundnessReqID { get; set; }
        public string OutOfRoundnessOutBy { get; set; }
        public string ProfileMeasurementAllowed { get; set; }
        public string ExternalPressureVessel { get; set; }
        public string PressureVesselReqChordLength { get; set; }
        public string PressureVesselActChordLength { get; set; }
        public string PressureVesselReqRadius { get; set; }
        public string PressureVesselActRadius { get; set; }
        public string PressureVesselAllowableGap { get; set; }
        public string PressureVesselActGap { get; set; }
        public string AdjacentLongSeam { get; set; }
        public string CheckPoint1 { get; set; }
        public string CheckPoint2 { get; set; }
        public string CheckPoint3 { get; set; }
        public string CheckPoint4 { get; set; }
        public string CheckPoint5 { get; set; }
        public string CheckPoint6 { get; set; }
        public string CheckPoint7 { get; set; }
        public string CheckPoint8 { get; set; }
        public string CheckPoint10 { get; set; }
        public Nullable<System.DateTime> CheckPoint10_2 { get; set; }
        public string CheckPoint11 { get; set; }
        public string CheckPoint11_2 { get; set; }
        public string CheckPoint11_3 { get; set; }
        public string CheckPoint12 { get; set; }
        public string QCRemarks { get; set; }
        public string Result { get; set; }
        public string Note1 { get; set; }
        public string Note2 { get; set; }
        public string Note3 { get; set; }
        public string IdentificationOfCleats { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public string ReqWidth { get; set; }
        public string ReqDepth { get; set; }
        public string ReqOffset { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRO176> PRO176 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRO177> PRO177 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRO178> PRO178 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRO179_2> PRO179_2 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRO179_3> PRO179_3 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRO179_4> PRO179_4 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRO179_5> PRO179_5 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRO179_6> PRO179_6 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRO179> PRO179 { get; set; }
    }
}
