//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class CJC011
    {
        public int Id { get; set; }
        public string SeamCategory { get; set; }
        public Nullable<int> NonOverlayThickness { get; set; }
        public Nullable<int> NozzleSize { get; set; }
        public string WeldingProcess { get; set; }
        public string NozzleCategory { get; set; }
        public Nullable<double> Rate { get; set; }
        public Nullable<int> Dia { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public string WeldType { get; set; }
        public string Setup { get; set; }
        public string SeamType { get; set; }
    
        public virtual CJC001 CJC001 { get; set; }
        public virtual CJC002 CJC002 { get; set; }
        public virtual CJC003 CJC003 { get; set; }
    }
}
