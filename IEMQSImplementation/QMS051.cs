//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class QMS051
    {
        public int Id { get; set; }
        public Nullable<int> HeaderId { get; set; }
        public Nullable<int> RefRequestId { get; set; }
        public string QualityProject { get; set; }
        public string Project { get; set; }
        public string BU { get; set; }
        public string Location { get; set; }
        public string SeamNo { get; set; }
        public string StageCode { get; set; }
        public Nullable<int> StageSequence { get; set; }
        public Nullable<int> IterationNo { get; set; }
        public Nullable<long> RequestNo { get; set; }
        public Nullable<int> RequestNoSequence { get; set; }
        public string WeldingProcess { get; set; }
        public Nullable<int> Sample { get; set; }
        public string Position { get; set; }
        public string Welder1 { get; set; }
        public string Welder2 { get; set; }
        public string Welder3 { get; set; }
        public string Welder4 { get; set; }
        public string Welder5 { get; set; }
        public string Welder6 { get; set; }
        public string Welder7 { get; set; }
        public string Welder8 { get; set; }
        public string Welder9 { get; set; }
        public string Welder10 { get; set; }
        public string Welder11 { get; set; }
        public string Welder12 { get; set; }
        public string Welder13 { get; set; }
        public string Welder14 { get; set; }
        public string Welder15 { get; set; }
        public string Welder16 { get; set; }
        public string Welder17 { get; set; }
        public string Welder18 { get; set; }
        public string Welder19 { get; set; }
        public string Welder20 { get; set; }
        public string Welder21 { get; set; }
        public string Welder22 { get; set; }
        public string Welder23 { get; set; }
        public string Welder24 { get; set; }
        public string Element1 { get; set; }
        public Nullable<decimal> ExaminationResult1 { get; set; }
        public string Element2 { get; set; }
        public Nullable<decimal> ExaminationResult2 { get; set; }
        public string Element3 { get; set; }
        public Nullable<decimal> ExaminationResult3 { get; set; }
        public string Element4 { get; set; }
        public Nullable<decimal> ExaminationResult4 { get; set; }
        public string Element5 { get; set; }
        public Nullable<decimal> ExaminationResult5 { get; set; }
        public string Element6 { get; set; }
        public Nullable<decimal> ExaminationResult6 { get; set; }
        public string Element7 { get; set; }
        public Nullable<decimal> ExaminationResult7 { get; set; }
        public string Element8 { get; set; }
        public Nullable<decimal> ExaminationResult8 { get; set; }
        public string Element9 { get; set; }
        public Nullable<decimal> ExaminationResult9 { get; set; }
        public string Element10 { get; set; }
        public Nullable<decimal> ExaminationResult10 { get; set; }
        public string Element11 { get; set; }
        public Nullable<decimal> ExaminationResult11 { get; set; }
        public string Element12 { get; set; }
        public Nullable<decimal> ExaminationResult12 { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public string Inspectedby { get; set; }
        public Nullable<System.DateTime> InspectedOn { get; set; }
        public string Instrument { get; set; }
    
        public virtual QMS040 QMS040 { get; set; }
        public virtual QMS050 QMS050 { get; set; }
    }
}
