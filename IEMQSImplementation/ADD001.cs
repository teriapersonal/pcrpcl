//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class ADD001
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ADD001()
        {
            this.ADD002 = new HashSet<ADD002>();
        }
    
        public int HeaderId { get; set; }
        public string DocumentName { get; set; }
        public Nullable<int> RevNo { get; set; }
        public string Description { get; set; }
        public string Title { get; set; }
        public Nullable<System.DateTime> EffectiveDate { get; set; }
        public string ModelType { get; set; }
        public string AIApprovalTaken { get; set; }
        public string AIApprovalRequired { get; set; }
        public string PEApprovalTaken { get; set; }
        public string PEApprovalRequired { get; set; }
        public Nullable<System.DateTime> ApproveDate { get; set; }
        public string DrawingSize { get; set; }
        public string CustomerComment { get; set; }
        public string PageSize { get; set; }
        public string DOBApprovalTaken { get; set; }
        public string DOBApprovalRequired { get; set; }
        public string PageNo { get; set; }
        public string Remarks { get; set; }
        public string Status { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public string SubmittedBy { get; set; }
        public Nullable<System.DateTime> SubmittedOn { get; set; }
        public string ReviewedBy { get; set; }
        public Nullable<System.DateTime> ReviewedOn { get; set; }
        public string Comments { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ADD002> ADD002 { get; set; }
    }
}
