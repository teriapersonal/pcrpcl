//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class PRO225
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PRO225()
        {
            this.PRO226 = new HashSet<PRO226>();
            this.PRO227 = new HashSet<PRO227>();
            this.PRO228 = new HashSet<PRO228>();
            this.PRO229_2 = new HashSet<PRO229_2>();
            this.PRO229_3 = new HashSet<PRO229_3>();
            this.PRO229 = new HashSet<PRO229>();
        }
    
        public int HeaderId { get; set; }
        public string ProtocolNo { get; set; }
        public string Ovality { get; set; }
        public string OutBy { get; set; }
        public string ReqTotalHeight { get; set; }
        public string ReqCircBottom { get; set; }
        public string ActCircBottom { get; set; }
        public string EvalueTemplateInspection { get; set; }
        public string ReqChordLengthBottom { get; set; }
        public string ActChordLengthBottom { get; set; }
        public string ReqRadiusBottom { get; set; }
        public string ActRadiusBottom { get; set; }
        public string ReqGapBottom { get; set; }
        public string ActGapBottom { get; set; }
        public string ProfileMeasurement { get; set; }
        public string ReqAllowedOffset { get; set; }
        public string MinGapAtKnuckleAreaUsingTemplate { get; set; }
        public string MaxGapAtKnuckleAreaUsingTemplate { get; set; }
        public string ReqOverCrowing { get; set; }
        public string ActOverCrowing { get; set; }
        public string ReqUnderCrowing { get; set; }
        public string ActUnderCrowing { get; set; }
        public string ReqMinThickness { get; set; }
        public string ActMinThickness { get; set; }
        public string ReqStraightFace { get; set; }
        public string ActStraightFace { get; set; }
        public string ReqCrownRadius { get; set; }
        public string ActCrownRadius { get; set; }
        public string ReqKnuckleRadius { get; set; }
        public string ActKnuckleRadius { get; set; }
        public string CheckPoint1 { get; set; }
        public string CheckPoint2 { get; set; }
        public Nullable<System.DateTime> CheckPoint2_2 { get; set; }
        public string CheckPoint4 { get; set; }
        public string CheckPoint5 { get; set; }
        public string CheckPoint6 { get; set; }
        public string QCRemarks { get; set; }
        public string Result { get; set; }
        public string ReqWidth { get; set; }
        public string ReqDepth { get; set; }
        public string AllowedOffset { get; set; }
        public string Note1 { get; set; }
        public string Note2 { get; set; }
        public string Note3 { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRO226> PRO226 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRO227> PRO227 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRO228> PRO228 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRO229_2> PRO229_2 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRO229_3> PRO229_3 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRO229> PRO229 { get; set; }
    }
}
