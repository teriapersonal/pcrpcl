//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation.ESP
{
    using System;
    
    public partial class SP_ESP_FR_GET_FIXTURE_ALLOCATION_IN_ESP_Result
    {
        public Nullable<long> ROW_NO { get; set; }
        public Nullable<int> TotalCount { get; set; }
        public int Id { get; set; }
        public string Project { get; set; }
        public string ProjectDesc { get; set; }
        public int NodeId { get; set; }
        public int RefLineId { get; set; }
        public int RefId { get; set; }
        public string FixtureNo { get; set; }
        public string Status { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public string Remarks { get; set; }
        public string NodeKey { get; set; }
        public string NodeName { get; set; }
        public string FindNo { get; set; }
        public string NodeColor { get; set; }
    }
}
