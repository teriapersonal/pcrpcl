//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation.ESP
{
    using System;
    
    public partial class SP_ESP_GET_SINGLE_WINDOW_PART_DETAILS_Result
    {
        public Nullable<long> ROW_NO { get; set; }
        public Nullable<int> TotalCount { get; set; }
        public string Project { get; set; }
        public Nullable<int> ElementNodeId { get; set; }
        public string Element { get; set; }
        public string ElementDesc { get; set; }
        public int NodeId { get; set; }
        public string NodeName { get; set; }
        public string FindNo { get; set; }
        public string NodeKey { get; set; }
        public string ProductType { get; set; }
        public Nullable<decimal> NoOfPieces { get; set; }
        public string Unit { get; set; }
        public string PPONumber { get; set; }
        public Nullable<int> PONo { get; set; }
        public Nullable<double> PPOQty { get; set; }
        public string PONumber { get; set; }
        public Nullable<double> POOrderQty { get; set; }
        public Nullable<double> QtyOnHand { get; set; }
        public string PCRNumber { get; set; }
        public string PCRStatus { get; set; }
        public Nullable<int> PCRPosition { get; set; }
        public Nullable<int> PCRRevision { get; set; }
        public Nullable<int> PCRStenum { get; set; }
        public string PCLNumber { get; set; }
        public Nullable<int> PCLStEnum { get; set; }
        public string PCLStatus { get; set; }
        public Nullable<int> PCLQty { get; set; }
    }
}
