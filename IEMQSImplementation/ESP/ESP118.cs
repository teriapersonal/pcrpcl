//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation.ESP
{
    using System;
    using System.Collections.Generic;
    
    public partial class ESP118
    {
        public int Id { get; set; }
        public int RefLineId { get; set; }
        public string FixtureNo { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public string DeliverStatus { get; set; }
        public string MaterialStatus { get; set; }
        public string KitLocation { get; set; }
        public Nullable<System.DateTime> FixReqDate { get; set; }
        public string FullkitArea { get; set; }
        public string FullkitAreaStatus { get; set; }
        public string RequestedBy { get; set; }
        public Nullable<System.DateTime> RequestedOn { get; set; }
        public string SubLocation { get; set; }
    
        public virtual ESP111 ESP111 { get; set; }
    }
}
