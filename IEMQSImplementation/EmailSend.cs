﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Mail;
using System.Net.Configuration;
using System.Configuration;
using System.Collections;
using System.Data;
using System.Net;
using System.IO;
using System.Security.Principal;
namespace IEMQSImplementation
{
    public class EmailSend
    {
        string conn = ConfigurationManager.ConnectionStrings["IEMQS"].ToString();
        #region Properties And Accessors

        private String m_ToAdd;
        public String MailToAdd
        {
            get
            {
                return m_ToAdd;
            }
            set
            {
                m_ToAdd = value;
            }
        }
        private String m_FromAdd;
        public String MailFromAdd
        {
            get
            {
                return m_FromAdd;
            }
            set
            {
                m_FromAdd = value;
            }
        }
        private String m_Sub;
        public String MailSub
        {
            get
            {
                return m_Sub;
            }
            set
            {
                m_Sub = value;
            }
        }
        private String m_Body;
        public String MailBody
        {
            get
            {
                return m_Body;
            }
            set
            {
                m_Body = value;
            }
        }
        private String m_Bcc;
        public String MailBcc
        {
            get
            {
                return m_Bcc;
            }
            set
            {
                m_Bcc = value;
            }
        }
        private String m_Cc;
        public String MailCc
        {
            get
            {
                return m_Cc;
            }
            set
            {
                m_Cc = value;
            }
        }
        private SmtpSection m_SmtpSec;
        public SmtpSection MailSmtpSect
        {
            get
            {
                return m_SmtpSec;
            }
            set
            {
                m_SmtpSec = value;
            }
        }
        private String m_Attachment;
        public String MailAttachment
        {
            get
            {
                return m_Attachment;
            }
            set
            {
                m_Attachment = value;
            }
        }
        private AttachmentCollection p_attach = new MailMessage().Attachments;
        public AttachmentCollection MailAttachMentCollection
        {
            get
            {
                return p_attach;
            }
            set
            {
                p_attach = value;
            }

        }
        public bool IsAttachmentDelete
        {
            get;
            set;
        }
        #endregion Properties And Accessors

        public EmailSend()
        {
            this.IsAttachmentDelete = false;
        }

        //public SmtpClient GetSmtpSection(SMTPMaster objSMTPMaster)
        //{
        //    SmtpClient objSmtpClient = new SmtpClient();
        //    NetworkCredential basicCredential =
        //        new NetworkCredential(objSMTPMaster.UserID, objSMTPMaster.PWD);
        //    MailMessage message = new MailMessage();
        //    MailAddress fromAddress = new MailAddress(objSMTPMaster.MailID);
        //    objSmtpClient.Host = objSMTPMaster.Host;
        //    objSmtpClient.Port = Convert.ToInt32(objSMTPMaster.Port);
        //    objSmtpClient.UseDefaultCredentials = false;
        //    objSmtpClient.Credentials = basicCredential;
        //    objSmtpClient.EnableSsl = Convert.ToBoolean(objSMTPMaster.EnableSSL);
        //    return objSmtpClient;
        //}

        #region Methods        
        public void SendMail(EmailSend objMail, Hashtable ht, MAIL001 objMAIL001)
        {
            MailMessage mm = new MailMessage();
            try
            {
                SmtpClient sc = new SmtpClient(ConfigurationManager.AppSettings["SMTPClient"]);
              
                /* HTML Modification start */

                StringBuilder _strBuilder = null;
                StringBuilder _strSubject = null;
                string strBody = string.Empty;
                string strsubject = string.Empty;

                if (objMAIL001 != null)
                {
                    //FROM
                    mm.From = new MailAddress(ConfigurationManager.AppSettings["FromMail"].ToString());
                    //mm.ReplyTo = new MailAddress(tomail);


                    //To
                    string[] _strArr;
                    if (!string.IsNullOrEmpty(objMail.MailToAdd))
                    {
                        _strArr = objMail.MailToAdd.ToString().Split(new char[] { ';', ',' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string _str in _strArr)
                            mm.To.Add(new MailAddress(_str));
                    }

                    //CC
                    string[] _strCC;
                    if (!string.IsNullOrEmpty(objMail.MailCc))
                    {
                        _strCC = objMail.MailCc.ToString().Split(new char[] { ';', ',' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string _str in _strCC)
                            mm.CC.Add(new MailAddress(_str));

                    }
                    if (!string.IsNullOrEmpty(objMAIL001.CCEmail))
                    {
                        _strCC = objMAIL001.CCEmail.ToString().Split(new char[] { ';', ',' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string _str in _strCC)
                            mm.CC.Add(new MailAddress(_str));
                    }


                    //BCC
                    string[] _strBcc;
                    if (!string.IsNullOrEmpty(objMail.MailBcc))
                    {
                        _strBcc = objMail.MailBcc.ToString().Split(new char[] { ';', ',' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string _str in _strBcc)
                            mm.Bcc.Add(new MailAddress(_str));
                    }
                    if (!string.IsNullOrEmpty(objMAIL001.BCCEmail))
                    {
                        _strBcc = objMAIL001.BCCEmail.ToString().Split(new char[] { ';', ',' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string _str in _strBcc)
                            mm.Bcc.Add(new MailAddress(_str));
                    }

                    //Sub
                    if (!string.IsNullOrEmpty(objMail.MailSub))
                        strsubject = objMail.MailSub;
                    else if (!string.IsNullOrEmpty(objMAIL001.Subject))
                    {
                        strsubject = objMAIL001.Subject;
                    }

                    //Body
                    if (!string.IsNullOrEmpty(objMail.MailBody))
                        strBody = objMail.MailBody;
                    else if (!string.IsNullOrEmpty(objMAIL001.Body))
                    {
                        strBody = objMAIL001.Body;
                    }

                    //finding dynamic value from the body and replace it with Hash key value
                    if (strBody != string.Empty)
                    {
                        _strBuilder = new StringBuilder(strBody);
                        _strSubject = new StringBuilder(strsubject);
                        if (ht != null && ht.Count > 0)
                        {
                            foreach (string key in ht.Keys)
                            {
                                if (strBody.Contains(key))
                                {
                                    if (key == "[Subject]" && string.IsNullOrEmpty(ht[key].ToString()))
                                    {
                                        _strBuilder.Replace(key.ToString(), mm.Subject);
                                    }
                                    else if (ht[key] != null && ht[key].ToString() != "")
                                    {
                                        _strBuilder.Replace(key.ToString(), ht[key].ToString());
                                    }
                                    else
                                    {
                                        _strBuilder.Replace(key.ToString(), "---");
                                    }
                                }

                                if (strsubject.Contains(key))
                                {
                                    _strSubject.Replace(key.ToString(), ht[key].ToString());
                                }
                            }
                        }
                    }

                    string sql = "";
                    string ConfigValue = "";
                    
                    using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(conn))
                    {
                        sql = "select Value from CONFIG where [Key] = 'EmailFooter'";
                        using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand(sql, con))
                        {
                            con.Open();
                            System.Data.SqlClient.SqlDataReader sdr = cmd.ExecuteReader();
                            if (sdr.HasRows)
                            {
                                while (sdr.Read())
                                {
                                    ConfigValue = sdr["Value"].ToString();
                                }
                            }
                        }
                        con.Close();
                    }
                    
                    _strBuilder.Append(ConfigValue.ToString());
                    mm.Body = _strBuilder.ToString();
                    mm.Subject = _strSubject.ToString();
                    if (!string.IsNullOrEmpty(objMail.MailAttachment))
                    {
                        Attachment a = new Attachment(objMail.MailAttachment);
                        mm.Attachments.Add(a);
                    }
                    foreach (Attachment at in objMail.MailAttachMentCollection)
                    {
                        mm.Attachments.Add(at);
                    }
                    mm.IsBodyHtml = true;
                    mm.Priority = MailPriority.Normal;
                    sc.Send(mm);

                    mm.To.Clear();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (objMail.IsAttachmentDelete && !string.IsNullOrEmpty(objMail.MailAttachment) && (objMail.MailAttachment.ToUpper().Contains("EMAILFILES") || objMail.MailAttachment.ToUpper().Contains("EXPORTFILES")))
                {
                    System.IO.FileInfo f = new System.IO.FileInfo(objMail.MailAttachment);
                    mm.Dispose();
                    f.Delete();
                }
                mm.Dispose();
            }
        }

        #endregion


    }

}