//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class QMS015
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public QMS015()
        {
            this.QMS016 = new HashSet<QMS016>();
        }
    
        public int HeaderId { get; set; }
        public string QualityProject { get; set; }
        public string Project { get; set; }
        public string BU { get; set; }
        public string Location { get; set; }
        public string QualityId { get; set; }
        public string QualityIdDesc { get; set; }
        public string QualityIdApplicableFor { get; set; }
        public Nullable<int> RevNo { get; set; }
        public string Status { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public string ApprovedBy { get; set; }
        public Nullable<System.DateTime> ApprovedOn { get; set; }
        public string SubmittedBy { get; set; }
        public Nullable<System.DateTime> SubmittedOn { get; set; }
        public string ReturnRemark { get; set; }
        public string ReturnedBy { get; set; }
        public Nullable<System.DateTime> ReturnedOn { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<QMS016> QMS016 { get; set; }
    }
}
