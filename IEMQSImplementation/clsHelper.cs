﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Drawing;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Xml.Serialization;
using IEMQSImplementation;

using System.Linq;
using System.Web;
using System.Web.Mvc;


using IEMQSImplementation.Models;
using System.Collections;
using System.Xml;
using System.Security.Cryptography;

namespace IEMQSImplementation
{
    /// <summary>
    /// Helper class created by Dharmesh 24-06-2017
    /// </summary>

    #region Helper Class
    public class clsHelper
    {
        public class ResponseMsg
        {
            public bool Key;
            public string Value;
            public bool? CheckStage;
            public string CheckStageType;
            public static implicit operator ResponseMsg(string v)
            {
                throw new NotImplementedException();
            }
        }

        public class ResponceMsgWithFileName : ResponseMsg
        {
            public string FileName;
        }
        public class ResponceMsgWithNodeExist : ResponseMsg
        {
            public bool IsNodeExist;
        }
        public class ResponceMsgTCP : ResponseMsg
        {
            public int Id;
            public string Status;
            public string Assignee;
            public string Addressee;
        }
        public class ResponseMsgWCS : ResponseMsg
        {
            public int HeaderId;
            public int LineId;

            public string VolumeCalculate;
            public string InnerVolumeofEquipment;
            public string LocationL;

            public string VolumeCalculateFormula;
            public string InnerVolumeofEquipmentFormula;
            public string LocationLFormula;
        }

        public class Welders : ResponseMsg
        {
            public List<string> WelderStamps;
            public string WeldingProcess;
        }

        public class ResponseMsgWithStatus : ResponseMsg
        {
            public List<AccessRight> listBU { get; set; }
            public List<AccessRight> listLocation { get; set; }
            public List<AccessRight> listRole { get; set; }
            public int NoOfProjects { get; set; }
            public int NoOfRoles { get; set; }
            public int NoOfLocation { get; set; }
            public int NoOfBU { get; set; }
            public int HeaderId { get; set; }
            public string HeaderStatus { get; set; }
            public string RevNo { get; set; }
            public string Status;
            public string WithoutLines { get; set; }
            public string NotDraft { get; set; }
            public string RequestNo { get; set; }
            public int Revision { get; set; }
            public int PDHId { get; set; }
            public string wqtno { get; set; }
            public int PlanningDinID { get; set; }

            public string item { get; set; }
            public string itemdesc { get; set; }
            public string Remarks { get; set; }
            public int ZoneNo { get; set; }
            public string LineStatus { get; set; }
            public string GeneralProject { get; set; }
            public string folderpath { get; set; }
            public string NotApplicableForParallel { get; set; }
            public string RequiredProtocol { get; set; }
            public string OfferedBy { get; set; }
            public string InspectedBy { get; set; }
            public string OfferedtoCusomerBy { get; set; }
            public string dataValue { get; set; }
            public bool dataKey { get; set; }
            public int RequestId { get; set; }
            public bool ActionKey { get; set; }
            public string ActionValue { get; set; }
            public string QtyofFixture { get; set; }
            public DateTime? FixRequiredDate { get; set; }
            public DateTime? MaterialReqDate { get; set; }

            public string tpi { get; set; }
            public string headerid { get; set; }
            public bool IsInformation { get; set; }

            public string TPISkipped { get; set; }
            #region WPP
            public string WeldDesignation { get; set; }
            public string ItemNumber1 { get; set; }
            public string ItemNumber2 { get; set; }
            public string ItemNumber3 { get; set; }

            public string PNumber1 { get; set; }
            public string PNumber2 { get; set; }
            public string PNumber3 { get; set; }

            public string Specification1 { get; set; }
            public string Specification2 { get; set; }
            public string Specification3 { get; set; }
            #endregion

            #region NCR
            public int NewNcrIdDigit { get; set; }
            public string OldNCRNumber { get; set; }
            #endregion

            public string NotDraftswp { get; set; }
        }

        public class ResponceMsgWithObject : ResponseMsg
        {
            public object data;
        }

        public class DashboardData : ResponseMsg
        {
            public int? PendingSeamsToOffer { get; set; }
            public int? PendingPartsToOffer { get; set; }
            public int? TotalSeamsCleared { get; set; }
            public int? TotalPartsCleared { get; set; }
            public int? PendingTCP { get; set; }

            public List<DashboardChartData> ChartSeamsPartsCleared { get; set; }

            public List<ChartData> ChartDataValue { get; set; }
            #region Coding Standard
            public decimal? Salary { get; set; }
            public string UserName { get; set; }

            public List<DashboardChartData> ChartCodeStandard { get; set; }

            public List<DashboardChartData> PieChartCodeStandard { get; set; }
            #endregion


        }

        public class DashboardChartData : ResponseMsg
        {
            public int? SrNO { get; set; }
            public string MonthDisplayName { get; set; }
            public int? TotalSeamCleared { get; set; }
            public int? TotalPartCleared { get; set; }

            #region Coding standard
            public decimal? Salary { get; set; }
            public string UserName { get; set; }
            public int? TotalCount { get; set; }

            #endregion
        }

        public class ProtocolHeaderData
        {
            public string QualityProject { get; set; }
            public string EquipmentNo { get; set; }
            public string DrawingNo { get; set; }
            public int? DrawingRevisionNo { get; set; }
            public string DCRNo { get; set; }
            public DateTime? InspectionDate { get; set; }
            public string SeamNo { get; set; }
            public string InspectionAgency { get; set; }
            public string ManufacturingCode { get; set; }
            public DateTime? OfferDate { get; set; }
            public string Project { get; set; }
            public string BU { get; set; }
            public string Location { get; set; }
            public string StageCode { get; set; }
            public int StageSequence { get; set; }
            public int IterationNo { get; set; }
            public long RequestNo { get; set; }
            public int RequestNoSequence { get; set; }
            public int? ICLRevNo { get; set; }
        }


        public class ChartData
        {
            public int? SrNO { get; set; }
            public int? ChartValue { get; set; }
        }

        public static string EncryptString(string strToEncrypt)
        {
            try
            {
                TripleDESCryptoServiceProvider objDESCrypto =
                    new TripleDESCryptoServiceProvider();
                MD5CryptoServiceProvider objHashMD5 = new MD5CryptoServiceProvider();
                byte[] byteHash, byteBuff;
                //string strTempKey = System.Configuration.ConfigurationManager.AppSettings["CryptKey"];
                string strTempKey = clsBase.CryptKey;
                byteHash = objHashMD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(strTempKey));
                objDESCrypto.Key = byteHash;
                objDESCrypto.Mode = CipherMode.ECB; //CBC, CFB
                byteBuff = ASCIIEncoding.ASCII.GetBytes(strToEncrypt);
                return Convert.ToBase64String(objDESCrypto.CreateEncryptor().TransformFinalBlock(byteBuff, 0, byteBuff.Length));
            }
            catch (Exception ex)
            {
                return "Wrong Input. " + ex.Message;
            }
        }

        public static string DecryptString(string strEncrypted)
        {
            try
            {
                TripleDESCryptoServiceProvider objDESCrypto =
                    new TripleDESCryptoServiceProvider();
                MD5CryptoServiceProvider objHashMD5 = new MD5CryptoServiceProvider();
                byte[] byteHash, byteBuff;
                //string strTempKey = System.Configuration.ConfigurationManager.AppSettings["CryptKey"];
                string strTempKey = clsBase.CryptKey;
                byteHash = objHashMD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(strTempKey));
                objDESCrypto.Key = byteHash;
                objDESCrypto.Mode = CipherMode.ECB; //CBC, CFB
                byteBuff = Convert.FromBase64String(strEncrypted);
                string strDecrypted = ASCIIEncoding.ASCII.GetString(objDESCrypto.CreateDecryptor().TransformFinalBlock(byteBuff, 0, byteBuff.Length));
                return strDecrypted;
            }
            catch (Exception ex)
            {
                return strEncrypted;
            }
        }
    }
    public class ddlValue
    {
        public string id;
        public string text;
        public string Value;
        public string Text;
        public string t_cprj;
        public string t_dsca;
    }
    public class SelectItemList
    {
        public string id { get; set; }
        public string text { get; set; }
    }

    public class AutoCompleteList
    {
        public string id;
        public string value;
        public string label;
    }

    public class AutoCompleteListGetSet
    {
        public string id { get; set; }
        public string value { get; set; }
        public string label { get; set; }
    }
    /// <summary>
    /// Helper class created by Jaydeep Sakariya 11-07-2017
    /// </summary>
    public static class Helper
    {
        public static string WebsiteURL = System.Configuration.ConfigurationManager.AppSettings["WebsiteURL"];

        public const string ExcelPath = "~/Resources/Download/";

        public static string GetSubstring(this string source, int tail_length)
        {
            if (tail_length >= source.Length)
                return source;
            return source.Substring(source.Length - tail_length);
        }
        public static string MakeDatatableSearchCondition(this string[] source, string search)
        {
            string strLikeFormat = string.Empty;
            if (source.Length > 0 && !string.IsNullOrWhiteSpace(search))
            {
                StringBuilder sb = new StringBuilder();

                foreach (var item in search.Trim().Split(' '))
                {
                    int count = 0;

                    sb.Append("and ( ");
                    foreach (var column in source)
                    {                       
                        if (source.Length - 1 == count)
                        {
                            if (column == "RevNo" || column == "SeamListRev" || column == "QualityIdRev")
                            {
                                string sitem = item.ToUpper().Trim('R');
                                sb.Append(" (" + column + " like '%" + sitem + "%'  or ");
                                sb.Append(" " + column + " like '%" + item + "%' ) ");
                            }
                            else
                                sb.Append(" " + column + " like '%" + item + "%' ");
                        }
                        else
                        {
                            if (column == "RevNo" || column == "SeamListRev" || column == "QualityIdRev")
                            {
                                string sitem = item.ToUpper().Trim('R');
                                sb.Append(" (" + column + " like '%" + sitem + "%'  or ");
                                sb.Append(" " + column + " like '%" + item + "%' ) or ");
                            }
                            else
                                sb.Append(" " + column + " like '%" + item + "%' or ");
                        }
                        count++;
                    }
                    sb.Append(") ");

                }
                strLikeFormat = sb.ToString();
            }
            return strLikeFormat;
        }
        public static string ToLiteral(this string input)
        {
            using (var writer = new StringWriter())
            {
                using (var provider = CodeDomProvider.CreateProvider("CSharp"))
                {
                    provider.GenerateCodeFromExpression(new CodePrimitiveExpression(input), writer, null);
                    return writer.ToString();
                }
            }
        }

        public static DateTime? ToNullIfTooEarlyForDb(this DateTime? date)
        {
            return (date >= (DateTime)SqlDateTime.MinValue) ? date : (DateTime?)null;
        }


        [NonAction]
        public static string ToBase64(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        [NonAction]
        public static byte[] FromBase64(string base64EncodedData)
        {
            base64EncodedData = base64EncodedData.Substring(base64EncodedData.IndexOf("base64,") + 7);
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return base64EncodedBytes;
        }

        [NonAction]
        public static string GenerateTextbox(int rowId, string columnName, string columnValue = "", string onBlurMethod = "",
            bool isReadOnly = false, string inputStyle = "", string maxlength = "", string classname = "", bool disabled = false, string onchange = "", string placeholder = "")
        {
            string htmlControl = "";

            string inputID = columnName + "" + (rowId < 0 ? "" : rowId.ToString());
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "form-control " + classname;
            string MaxCharcters = !string.IsNullOrEmpty(maxlength) ? "maxlength='" + maxlength + "'" : "";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";

            htmlControl = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' "
                + MaxCharcters + " value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='"
                + className + "' style='" + inputStyle + "'  " + onBlurEvent + " spara='" + (rowId < 0 ? "0" : rowId.ToString()) + "' " + (!string.IsNullOrEmpty(onchange) ? "onchange='" + onchange + "'" : "")
                + " placeholder='" + placeholder + "' " + (disabled ? "disabled" : "") + " />";

            return htmlControl;
        }

        public static string GenerateLabelFor(int rowId, string columnName, string columnValue = "", string inputStyle = "", string classname = "")
        {
            string htmlControl = "";

            string inputID = columnName + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "form-control " + classname;

            htmlControl = "<label class='control-label +" + className + "' spara='" + rowId + "'  colname ='" + columnName + "' name='" + inputID + "'  id='" + inputID + "' >" + columnValue + "</label>";

            return htmlControl;
        }

        [NonAction]
        public static string GenerateTextArea(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", bool isReadOnly = false, string inputStyle = "", string maxlength = "", string role = "", string className = "")
        {
            string htmlControl = "";

            string inputID = columnName + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            className = "form-control " + className;
            string MaxCharcters = !string.IsNullOrEmpty(maxlength) ? "maxlength='" + maxlength + "'" : "";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onchange='" + onBlurMethod + "'" : "";
            string userRole = role != "" && !string.IsNullOrEmpty(role) ? "role = '" + role + "'" : "";
            htmlControl = "<textarea " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' " + MaxCharcters + " colname='" + columnName + "' name='" + inputID + "' class='" + className + "' spellcheck='true' style='" + inputStyle + "'  " + onBlurEvent + " " + userRole + " spara='" + rowId + "' >" + inputValue + "</textarea>";

            return htmlControl;
        }


        [NonAction]
        public static string GenerateNumericTextbox(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", bool isReadOnly = false, string inputStyle = "", string maxlength = "", string className = "")
        {
            string htmlControl = "";

            string inputID = columnName + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            className = "numeric form-control " + className;
            string MaxCharcters = !string.IsNullOrEmpty(maxlength) ? "maxlength='" + maxlength + "'" : "";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onchange='" + onBlurMethod + "'" : "";

            htmlControl = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' " + MaxCharcters + " value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " spara='" + rowId + "' />";

            return htmlControl;
        }

        [NonAction]
        public static string GenerateHTMLTextbox(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", bool isReadOnly = false, string inputStyle = "",
            bool disabled = false, string maxlength = "", string role = "", string onClick = "", string inlineclassname = "")
        {
            string htmlControl = "";

            string inputID = columnName + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = !(string.IsNullOrWhiteSpace(inlineclassname)) ? "form-control " + inlineclassname : "form-control";
            string MaxCharcters = !string.IsNullOrEmpty(maxlength) ? "maxlength='" + maxlength + "'" : "";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";
            string userRole = role != "" && !string.IsNullOrEmpty(role) ? "role = '" + role + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClick) ? "onclick='" + onClick + "'" : "";
            htmlControl = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' " + MaxCharcters + " value='" + inputValue + "' " + userRole + " colname='" + columnName
                + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + (disabled ? "disabled" : "") + " " + onClickEvent + " />";

            return htmlControl;
        }

        [NonAction]
        public static string GenerateNumericTextbox(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", bool isReadOnly = false, string inputStyle = "", bool disabled = false, string maxlength = "", string role = "", string customClass = "")
        {
            string htmlControl = "";

            string inputID = columnName + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "form-control " + (string.IsNullOrEmpty(customClass) ? "numeric-only" : customClass);
            string MaxCharcters = !string.IsNullOrEmpty(maxlength) ? "maxlength='" + maxlength + "'" : "";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";
            string userRole = role != "" && !string.IsNullOrEmpty(role) ? "role = '" + role + "'" : "";

            htmlControl = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' " + MaxCharcters + " value='" + inputValue + "' " + userRole + " colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + (disabled ? "disabled" : "") + " />";

            return htmlControl;
        }

        [NonAction]
        public static string GenerateHTMLTextboxOnChanged(int rowId, string columnName, string columnValue = "", string onChangeMethod = "", bool isReadOnly = false, string inputStyle = "", bool disabled = false, string maxlength = "")
        {
            string htmlControl = "";

            string inputID = columnName + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "form-control";
            string MaxCharcters = !string.IsNullOrEmpty(maxlength) ? "maxlength='" + maxlength + "'" : "";
            string onBlurEvent = !string.IsNullOrEmpty(onChangeMethod) ? "onchange='" + onChangeMethod + "'" : "";

            htmlControl = "<input type='text' " + (isReadOnly ? "disabled='disabled'" : "") + " id='" + inputID + "' " + MaxCharcters + " value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + (disabled ? "disabled" : "") + " />";

            return htmlControl;
        }

        [NonAction]
        public static string GenerateGridButton(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";


            htmlControl = "<a id='" + inputID + "' name='" + inputID + "' title='" + buttonTooltip + "'  class='btn btn-outline btn-circle btn-sm blue' " + onClickEvent + " ><i class='" + className + "'></i> " + buttonName + "</a>";

            //htmlControl = "<i  data-modal='' id='" + inputID + "' name='" + inputID + "' style='cursor:Pointer;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";

            return htmlControl;
        }
        [NonAction]
        public static string GenerateGridButtonType(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";


            htmlControl = "<button type='button' id='" + inputID + "' name='" + inputID + "' title='" + buttonTooltip + "'  class='btn btn-outline btn-circle btn-sm blue' " + onClickEvent + " ><i class='" + className + "'></i> " + buttonName + "</button>";

            //htmlControl = "<i  data-modal='' id='" + inputID + "' name='" + inputID + "' style='cursor:Pointer;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";

            return htmlControl;
        }
        //used in PROTOCOLS
        [NonAction]
        public static string HTMLActionString(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", string onHrefURL = "", bool isDisable = false, string inputStyle = "")
        {
            string htmlControl = "";
            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? onClickMethod : "";
            string onHref = !string.IsNullOrEmpty(onHrefURL) ? "href=\"" + WebsiteURL + onHrefURL + "\"" : "";

            if (isDisable)
            {
                htmlControl = "<i id=\"" + inputID.ToString() + "\" name=\"" + inputID + "\"    Title=\"" + buttonTooltip + "\" class=\"disabledicon " + className + "\" style=\" " + inputStyle + " \" ></i>";
            }
            else
            {
                htmlControl += "<i id=\"" + inputID.ToString() + "\" name=\"" + inputID + "\"  Title=\"" + buttonTooltip + "\" class=\"iconspace " + className + "\"  onclick=\"" + onClickEvent + "\" style=\" " + inputStyle + " \" ></i>";
            }

            if (!string.IsNullOrEmpty(onHref))
            {
                htmlControl = "<a " + onHref + " >" + htmlControl + "</a>";
            }

            return htmlControl;
        }

        #region Coding Standard
        [NonAction]
        public static string GenerateActionIcon(int? rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", string onHrefURL = "", bool isDisable = false, bool OpenNewWindow = false, string inputstyle = "")
        {
            string htmlControl = "";
            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? onClickMethod : "";
            string onHref = !string.IsNullOrEmpty(onHrefURL) ? "href='" + onHrefURL + "'" : "";
            string newWindow = (OpenNewWindow ? "target='_blank'" : "");
            string customestyle = !string.IsNullOrEmpty(inputstyle) ? "style='" + inputstyle + "'" : "";
            if (isDisable)
            {
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' Title='" + buttonTooltip + "' class='disabledicon " + className + "' " + customestyle + "></i>";
            }
            else
            {
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' Title='" + buttonTooltip + "' class='iconspace " + className + "' onclick =\"" + onClickEvent + "\"  " + customestyle + " ></i>";
                if (!string.IsNullOrEmpty(onHref))
                {
                    htmlControl = "<a " + onHref + " " + newWindow + " >" + htmlControl + "</a>";
                }
            }
            return htmlControl;
        }

        [NonAction]
        public static string MaintainIPISamplingPlanGridButton(int headerid, int lineid, string qProj = "", string Project = "", string bu = "", string loc = "", string qId = "", int? qRev = 0, string stage = "", string stageDesc = "", int? stageseq = 0, bool isEditableFlag = false, bool isShowButton = false, bool ViewMode = false)
        {
            string strButtons = string.Empty;
            if (ViewMode)
            {
                var fn = "LoadIPIMaintainSamplingPlan(" + headerid + "," + lineid + ",'" + qProj + "','" + Project + "','" + bu + "','" + loc + "','" + qId + "'," + qRev + ",'" + stage + "'," + stageseq + ",'true','" + isEditableFlag.ToString().ToLower() + "','Maintain Sampling Plan')";
                //"LoadIPIMaintainSamplingPlan(" + headerid + "," + lineid + "," + revno + ",'" + isEditableFlag.ToString().ToLower() + "','Maintain Sampling Plan -" + stage + "')"
                strButtons = GenerateActionIcon(lineid, "Sampling Plan", "Sampling Plan", "fa fa-flask", fn, "", !isShowButton);
            }
            else
            {
                var fn = "LoadIPISamplingPlan(" + headerid + "," + lineid + ",'" + qProj + "','" + Project + "','" + bu + "','" + loc + "','" + qId + "'," + qRev + ",'" + stage + "'," + stageseq + ",'true','" + isEditableFlag.ToString().ToLower() + "','Maintain Sampling Plan')";
                //"LoadIPISamplingPlan(" + headerid + "," + lineid + "," + revno + ",'" + isEditableFlag.ToString().ToLower() + "','Maintain Sampling Plan -" + stage + "')"
                strButtons = GenerateActionIcon(lineid, "Sampling Plan", "Sampling Plan", "fa fa-flask", fn, "", !isShowButton);
            }
            return strButtons;
        }

        public static string replaceQuote(string str)
        {
            str = (!string.IsNullOrWhiteSpace(str) ? str.Replace("'", "\\'") : str);
            return str;
        }
        [NonAction]
        public static string MaintainIPIStagetypewiseGridButton(int headerid, int lineid, string qProj = "", string Project = "", string bu = "", string loc = "", string qId = "", int? qRev = 0, string stage = "", string stageDesc = "", int? stageseq = 0, bool isEditableFlag = false, bool isShowButton = false, bool ViewMode = false, string stagetype = "")
        {
            string strButtons = string.Empty;
            var qualityid = replaceQuote(qId);
            if (ViewMode)
            {
                strButtons = GenerateActionIcon(lineid, "", "", "fa fa-flask", "", "", true);
                if (stagetype == clsImplementationEnum.SeamStageType.Chemical.GetStringValue() || stagetype == clsImplementationEnum.SeamStageType.PMI.GetStringValue())
                {
                    var fn = "LoadIPIMaintainSamplingPlan(" + headerid + "," + lineid + ",'" + qProj + "','" + Project + "','" + bu + "','" + loc + "','" + qualityid + "'," + qRev + ",'" + stage + "'," + stageseq + ",'true','" + isEditableFlag.ToString().ToLower() + "','Maintain Sampling Plan')";
                    //"LoadIPIMaintainSamplingPlan(" + headerid + "," + lineid + "," + revno + ",'" + isEditableFlag.ToString().ToLower() + "','Maintain Sampling Plan -" + stage + "')"
                    strButtons = GenerateActionIcon(lineid, "Sampling Plan", "Sampling Plan", "fa fa-flask", fn, "", !isShowButton);
                }

                if (stagetype == clsImplementationEnum.SeamStageType.Hardness.GetStringValue())
                {
                    var fn = "LoadHardnessFerritePartial(" + lineid + "," + isEditableFlag.ToString().ToLower() + ",'Maintain hardness required value')";
                    strButtons = GenerateActionIcon(lineid, "Maintain Hardness required value", "Maintain Hardness required value", "fa fa-flask", fn, "", !isShowButton);
                }

                if (stagetype == clsImplementationEnum.SeamStageType.Ferrite.GetStringValue())
                {
                    var fn = "LoadHardnessFerritePartial(" + lineid + "," + isEditableFlag.ToString().ToLower() + ",'Maintain ferrite test required range')";
                    strButtons = GenerateActionIcon(lineid, "Maintain ferrite test required range", "Maintain ferrite test required range", "fa fa-flask", fn, "", !isShowButton);
                }
            }
            else
            {
                strButtons = GenerateActionIcon(lineid, "", "", "fa fa-flask", "", "", true);
                var fn = "LoadIPISamplingPlan(" + headerid + "," + lineid + ",'" + qProj + "','" + Project + "','" + bu + "','" + loc + "','" + qualityid + "'," + qRev + ",'" + stage + "'," + stageseq + ",'true','" + isEditableFlag.ToString().ToLower() + "','Maintain Sampling Plan','" + stagetype + "')";

                if (stagetype == clsImplementationEnum.SeamStageType.Chemical.GetStringValue() || stagetype == clsImplementationEnum.SeamStageType.PMI.GetStringValue())
                {
                    strButtons = GenerateActionIcon(lineid, "Sampling Plan", "Sampling Plan", "fa fa-flask", fn, "", !isShowButton);
                }
                if (stagetype == clsImplementationEnum.SeamStageType.Hardness.GetStringValue())
                {
                    strButtons = GenerateActionIcon(lineid, "Maintain Hardness required value", "Maintain Hardness required value", "fa fa-flask", fn, "", !isShowButton);
                }

                if (stagetype == clsImplementationEnum.SeamStageType.Ferrite.GetStringValue())
                {
                    strButtons = GenerateActionIcon(lineid, "Maintain ferrite test required range", "Maintain ferrite test required range", "fa fa-flask", fn, "", !isShowButton);
                }
            }
            return strButtons;
        }
        public static string MaintainIPICAMActionbutton(int headerid, int lineid, string stage, string stagedesc, int? stageseq, bool isVisible, string module)
        {
            IEMQSEntitiesContext db = new IEMQSEntitiesContext();
            string strButtons = string.Empty;
            if (module == "tp")
            {
                var obj = db.QMS020.Where(x => x.HeaderId == headerid).FirstOrDefault();
                if (obj != null)
                {
                    strButtons = Helper.GenerateActionIcon(lineid, "Sampling Plan", "Sampling Plan", "fa fa-flask",
                                 "LoadIPIMaintainSamplingPlan(" + headerid + "," + lineid + ",'" + obj.QualityProject + "','" + obj.Project + "','" + obj.BU + "','" + obj.Location + "','" + obj.QualityId + "'," + obj.QualityIdRev + ",'" + stage + "','" + stageseq + "','false','false','Maintain Sampling Plan')", "", !isVisible);
                }
                else
                {
                    strButtons = Helper.GenerateActionIcon(lineid, "Sampling Plan", "Sampling Plan", "fa fa-flask",
                               "", "", !isVisible);

                }
            }

            return strButtons;
        }
        [NonAction]
        public static string GenerateActionIcon(string rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", string onHrefURL = "", bool isDisable = false, bool OpenNewWindow = false, string inputstyle = "")
        {
            string htmlControl = "";
            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? onClickMethod : "";
            string onHref = !string.IsNullOrEmpty(onHrefURL) ? "href='" + onHrefURL + "'" : "";
            string newWindow = (OpenNewWindow ? "target='_blank'" : "");
            string customestyle = !string.IsNullOrEmpty(inputstyle) ? "style='" + inputstyle + "'" : "";
            if (isDisable)
            {
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' Title='" + buttonTooltip + "' class='disabledicon " + className + "' " + customestyle + "></i>";
            }
            else
            {
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' Title='" + buttonTooltip + "' class='iconspace " + className + "' onclick =\"" + onClickEvent + "\"  " + customestyle + " ></i>";
                if (!string.IsNullOrEmpty(onHref))
                {
                    htmlControl = "<a " + onHref + " " + newWindow + " >" + htmlControl + "</a>";
                }
            }
            return htmlControl;
        }
        public static string MultiSelectDropdown(List<SelectItemList> list, int rowId, string selectedValue, bool Disabled = false, string OnChangeEvent = "", string OnBlurEvent = "", string columnName = "", string className = "")
        {
            string multipleSelect = string.Empty;
            string[] arrayVal = { };
            string inputID = columnName + "" + rowId.ToString();
            className = "form-control " + className;
            try
            {
                if (!string.IsNullOrWhiteSpace(selectedValue))
                {
                    arrayVal = selectedValue.Split(',');
                }


                multipleSelect += "<select data-name='ddlmultiple' name='" + inputID + "' id='" + inputID + "' class='" + className + "' data-lineid='" + rowId + "' multiple='multiple'  style='width: 100 % ' colname='" + columnName + "' data-value='" + selectedValue + "' class='form-control multiselect-drodown' " + (Disabled ? "disabled " : "") + (OnChangeEvent != string.Empty ? "onchange=" + OnChangeEvent + "" : "") + (OnBlurEvent != string.Empty ? " onblur='" + OnBlurEvent + "'" : "") + " >";
                foreach (var item in list)
                {
                    multipleSelect += "<option value='" + item.id + "' " + ((arrayVal.Contains(item.id)) ? " selected" : "") + " >" + item.text + "</option>";
                }
                multipleSelect += "</select>";
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return multipleSelect;
        }
        [NonAction]
        public static string GenerateActionCheckbox(int rowId, string columnName, bool columnValue = false, string onBlurMethod = "", bool isDisabled = false, string className = "")
        {
            string htmlControl = "";
            string inputID = columnName + "" + rowId.ToString();
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onchange='" + onBlurMethod + "'" : "";
            bool inputValue = columnValue;
            className = " " + className;

            htmlControl = "<label class='mt-checkbox mt-checkbox-outline'><input type='checkbox' id='" + inputID + "' name='" + inputID + "' class='" + className + "' colname='" + columnName + "'  " + onBlurEvent + " " + (isDisabled ? "disabled" : "") + " " + (columnValue == true ? "checked" : "") + " /><span></span></label>";
            return htmlControl;
        }
        public static string HTMLAutoComplete(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false, string maxlength = "", string validate = "", string placeholder = "", string className = "")
        {
            string strAutoComplete = string.Empty;

            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            className = "autocomplete form-control " + className;
            string MaxCharcters = !string.IsNullOrEmpty(maxlength) ? "maxlength='" + maxlength + "'" : "";
            string FieldValidate = !string.IsNullOrEmpty(validate) ? "validate='" + validate + "'" : "";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onchange='" + onBlurMethod + "'" : "";
            string placeHolder = !string.IsNullOrEmpty(placeholder) ? "placeholder='" + placeholder + "'" : "";

            strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " spara='" + rowId + "' id='" + inputID + "' hdElement='" + hdElementId + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + (disabled ? "disabled" : "") + " " + MaxCharcters + " " + FieldValidate + " " + placeHolder + " />";

            return strAutoComplete;
        }

        #endregion
        [NonAction]
        public static string GenerateGridButtonNew(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick=\"" + onClickMethod + "\"" : "";


            htmlControl = "<a id=\"" + inputID + "\" name=\"" + inputID + "\" class=\"btn btn-outline btn-circle btn-sm blue\" " + onClickEvent + " ><i class=\"" + className + "\"></i> " + buttonName + "</a>";

            //htmlControl = "<i  data-modal='' id='" + inputID + "' name='" + inputID + "' style='cursor:Pointer;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";

            return htmlControl;
        }

        [NonAction]
        public static string GenerateHidden(int rowId, string columnName, string columnValue = "")
        {
            string htmlControl = "";

            string inputID = columnName + "" + rowId.ToString();
            string inputValue = columnValue;

            htmlControl = "<input type='hidden' id='" + inputID + "' value='" + inputValue + "' name='" + inputID + "' colname='" + columnName + "'/>";

            return htmlControl;
        }

        [NonAction]
        public static string GenerateCheckbox(int rowId, string columnName, bool columnValue = false)
        {
            string htmlControl = "";
            string inputID = columnName + "" + rowId.ToString();
            bool inputValue = columnValue;

            htmlControl = "<input type='checkbox' id='" + inputID + "' value='" + inputValue + "' name='" + inputID + "' colname='" + columnName + "'/>";
            return htmlControl;
        }

        [NonAction]
        public static string GenerateCheckboxWithEvent(int rowId, string columnName, bool columnValue = false, string onClickMethod = "", bool isEnabled = true, string ClassName = "")
        {
            string htmlControl = "";
            string inputID = columnName + "" + rowId.ToString();
            string inputValue = columnValue ? "Checked='Checked'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onchange='" + onClickMethod + "'" : "";
            string cssClass = !string.IsNullOrEmpty(ClassName) ? "class='" + ClassName + "'" : "";

            if (isEnabled)
            {
                htmlControl = "<input type='checkbox' id='" + inputID + "' " + inputValue + " name='" + inputID + "' colname='" + columnName + "' " + onClickEvent + " " + (!isEnabled ? "disabled='disabled'" : "") + " " + cssClass + " />";
            }
            else
            {
                htmlControl = columnValue ? "Yes" : "No";
            }
            return htmlControl;
        }

        [NonAction]
        public static string GenerateDropdown(int rowId, string columnName, SelectList itemList, string defaultSelectionText = "", string onChangeMethod = "", bool isReadOnly = false, string inputStyle = "")
        {
            string selectOptions = "";
            string inputID = columnName + "" + rowId.ToString();
            string inputName = columnName;
            string className = "form-control col-md-3";
            string onChangedEvent = !string.IsNullOrEmpty(onChangeMethod) ? "onchange='" + onChangeMethod + "'" : "";

            if (defaultSelectionText.Length > 0)
            {
                selectOptions += "<option value=''>" + defaultSelectionText + "</option>";
            }

            foreach (var item in itemList)
            {
                if (item.Selected)
                {
                    selectOptions += "<option selected value=" + item.Value + "  > " + item.Text + "</option>";
                }
                else
                {
                    selectOptions += "<option value=" + item.Value + " >" + item.Text + "</option>";
                }
            }

            return "<select id='" + inputID + "' name='" + inputID + "' " + (isReadOnly ? "disabled='disabled'" : "") + " " + onChangedEvent + " colname='" + columnName + "' class='" + className + "' style='" + inputStyle + "' >" + selectOptions + "</select>";

        }
        //for TVL040/TVL050 (offer inspection/Attend ispection/tpi inspection)
        public static string ViewTVLRefDocument(string projectno, string travelerno, string assemblyno, string bu, string location, decimal? operationno, int headerid, bool? IsHeaderLine = false)
        {
            IEMQSEntitiesContext db = new IEMQSEntitiesContext();
            string button = "";
            bu = bu.Split('-')[0].Trim();
            location = location.Split('-')[0].Trim();
            operationno = Convert.ToDecimal(operationno.Value.ToString("G29"));
            var obj = (from t11 in db.TVL011
                       join t10 in db.TVL010 on t11.HeaderId equals t10.HeaderId
                       where t10.TravelerNo == travelerno
                            && t10.ProjectNo == projectno
                            && t10.BU == bu
                            && t10.Location == location
                            && t10.IdentificationNo == assemblyno
                            && t11.OperationNo == operationno
                       select t11).FirstOrDefault();
            if (obj != null)
            {
                button = GenerateActionIcon(headerid, "Attachment", "Reference Document", "fa fa-paperclip", "ViewTVLRefDocAttachment(" + obj.LineId + "," + obj.OperationRevisionNo + ");", "", Convert.ToBoolean(IsHeaderLine));
            }
            else
            {
                button = GenerateActionIcon(headerid, "Attachment", "Reference Document", "fa fa-paperclip", "", "", true);
            }
            return button;
        }

        [NonAction]
        public static string GenerateDropdownWithSelected(int rowId, string columnName, SelectList itemList, string SelectedValue, string defaultSelectionText = "",
            string onChangeMethod = "", bool isReadOnly = false, string inputStyle = "", string onClick = "")
        {
            string selectOptions = "";
            string inputID = columnName + "" + rowId.ToString();
            string inputName = columnName;
            string className = "form-control";
            string onChangedEvent = !string.IsNullOrEmpty(onChangeMethod) ? "onchange='" + onChangeMethod + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClick) ? "onclick='" + onClick + "'" : "";
            if (defaultSelectionText.Length > 0)
            {
                selectOptions += "<option value=''>" + defaultSelectionText + "</option>";
            }

            foreach (var item in itemList)
            {
                if (item.Value == SelectedValue)
                {
                    selectOptions += "<option selected value=" + item.Value + ">" + item.Text + "</option>";
                }
                else
                {
                    selectOptions += "<option value=" + item.Value + ">" + item.Text + "</option>";
                }
            }

            return "<select id='" + inputID + "' name='" + inputID + "' " + (isReadOnly ? "disabled='disabled'" : "") + " " + onChangedEvent + " colname='" + columnName + "' class='" + className + "' style='" + inputStyle + "' " + onClickEvent + " >" + selectOptions + "</select>";

        }

        public static string MultiSelectDropdown(List<SelectItemList> list, string selectedValue, bool Disabled = false, string OnChangeEvent = "", string ColumnName = "")
        {
            string multipleSelect = string.Empty;
            string[] arrayVal = { };

            try
            {
                if (!string.IsNullOrWhiteSpace(selectedValue))
                {
                    arrayVal = selectedValue.Split(',');
                }


                multipleSelect += "<select name='ddlmultiple' id='ddlmultiple' multiple='multiple'  style='width: 100 % ' colname='" + ColumnName + "' class='form-control' " + (Disabled ? "disabled " : "") + (OnChangeEvent != string.Empty ? "onchange=" + OnChangeEvent + "" : "") + " >";
                foreach (var item in list)
                {
                    multipleSelect += "<option value='" + item.id + "' " + ((arrayVal.Contains(item.id)) ? " selected" : "") + " >" + item.text + "</option>";
                }
                multipleSelect += "</select>";
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return multipleSelect;
        }

        public static SqlException FindSQLException(Exception ex)
        {
            if (ex.InnerException is SqlException)
                return (SqlException)ex.InnerException;
            else if (ex.InnerException == null)
                return null;
            else
                return FindSQLException(ex.InnerException);
        }

        public static string FindSQLExceptionMsg(Exception ex)
        {
            SqlException oEx = FindSQLException(ex);
            string ErrorMsg = string.Empty;
            if (oEx != null)
            {
                switch (oEx.Number)
                {
                    case 547:
                        ErrorMsg = "Foreign refference error";
                        break;
                    case 2627:
                        ErrorMsg = "Record must be unique";
                        break;
                    case 2601:
                        ErrorMsg = "Record must be unique";
                        break;

                    default:
                        if (oEx.InnerException == null)
                        {
                            ErrorMsg = oEx.Message;
                        }
                        else
                        {
                            ErrorMsg = oEx.Message + oEx.InnerException.ToString();
                        }
                        break;
                }
            }
            else
            {
                if (ex.InnerException == null)
                {
                    ErrorMsg = ex.Message;
                }
                else
                {
                    ErrorMsg = ex.Message + ex.InnerException.ToString();
                }
            }
            return "Error : " + ErrorMsg;
        }

        public static object GetPropValue(object src, string propName)
        {
            return src.GetType().GetProperty(propName).GetValue(src, null);
        }

        public static string GetDescription(string strValue)
        {
            if (string.IsNullOrWhiteSpace(strValue))
            {
                return null;
            }
            string[] strArray = strValue.Split('-');
            if (strArray.Count() >= 2)
            {
                return strValue.Split('-')[1].Trim();
            }
            return strValue.Split('-')[0].Trim();
        }
        public static string ExcelFilePath(string userName, out string fileName)
        {
            string folderPath = Path.Combine(System.Web.HttpContext.Current.Server.MapPath(ExcelPath));
            try
            {
                if (!Directory.Exists(folderPath))
                {
                    Directory.CreateDirectory(folderPath);
                }
                else
                {
                    DirectoryInfo directoryInfo = new DirectoryInfo(folderPath);

                    foreach (FileInfo file in directoryInfo.GetFiles())
                    {
                        file.Delete();
                    }
                    foreach (DirectoryInfo dir in directoryInfo.GetDirectories())
                    {
                        dir.Delete(true);
                    }
                }
                fileName = string.Format("{0}_{1}.xlsx", userName, DateTime.Now.ToString("dd-MM-yyyy-hhmmss"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Path.Combine(folderPath, fileName);
        }
        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Defining type of data column gives proper data table 
                var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name, type);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }
        public static string GenerateExcel<T>(List<T> query, string userName)
        {
            //try
            {
                using (ExcelPackage excelPackage = new ExcelPackage())
                {
                    ExcelWorksheet ws = excelPackage.Workbook.Worksheets.Add("Sheet1");

                    //Added by Dharmesh Vasani for Change Excel Column Title
                    ResourceManager rm = new ResourceManager("IEMQSImplementation.HeaderTitle", Assembly.GetExecutingAssembly());
                    //get our column headings
                    var t = typeof(T);
                    var Headings = t.GetProperties();
                    List<string> lstDateCoumnName = ListOfDateColumn().Select(x => x.Name.ToUpper()).ToList();
                    for (int i = 0; i < Headings.Count(); i++)
                    {
                        //Added by Dharmesh Vasani for Change Excel Column Title
                        ws.Cells[1, i + 1].Value = (!string.IsNullOrWhiteSpace(rm.GetString(Headings[i].Name)) ? rm.GetString(Headings[i].Name) : Headings[i].Name);
                        //For Date Format                        
                        if (lstDateCoumnName.Contains(Headings[i].Name.ToUpper()))
                        {
                            var firstRowCell = ws.Cells[ws.Dimension.Start.Row, ws.Dimension.Start.Column, 1, ws.Dimension.End.Column].Where(x => x.Text.ToUpper() == Headings[i].Name.ToUpper()).FirstOrDefault();
                            if (firstRowCell != null)
                            {
                                ws.Column(firstRowCell.Start.Column).Style.Numberformat.Format = "dd-MMM-yyyy hh:mm";
                                ws.Column(firstRowCell.Start.Column).AutoFit();
                            }
                            //ws.Column(ws.Cells[1, i + 1].Columns).Style.Numberformat.Format = "yyyy-mm-dd";
                        }

                    }

                    //populate our Data
                    if (query.Count() > 0)
                    {
                        ws.Cells["A2"].LoadFromCollection(query);
                    }

                    //Format the header                    
                    using (ExcelRange rng = ws.Cells[1, 1, 1, Headings.Count()])
                    {
                        rng.Style.Font.Bold = true;
                        rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        rng.Style.Fill.PatternType = ExcelFillStyle.Solid; //Set Pattern for the background to Solid
                        rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(79, 129, 189)); //Set color to dark blue
                        rng.Style.Font.Color.SetColor(Color.White);
                    }

                    //End
                    string fileName;
                    string excelFilePath = ExcelFilePath(userName, out fileName);

                    ws.Cells.AutoFitColumns();

                    Byte[] bin = excelPackage.GetAsByteArray();
                    File.WriteAllBytes(excelFilePath, bin);

                    return fileName;

                    //var stream = new MemoryStream();
                    //excelPackage.SaveAs(stream);

                    //string fileName = "myfilename.xlsx";
                    //contentType = "application/vnd.openxmlformats- .spreadsheetml.sheet";
                    //contentType = "application/x-ms-excel";

                    //stream.Position = 0;
                    //return stream;
                    //return File(stream, contentType, fileName);
                }
            }
            //catch (Exception) { }

            //return string.Empty;
        }
        /// <summary>  
        /// Return list of date column name from XML.  
        /// </summary>  
        /// <returns>List of date column names</returns>  
        public static List<ExcelDateCoumn> ListOfDateColumn()
        {
            string xmlData = HttpContext.Current.Server.MapPath("~/ExcelSetting.xml");
            DataSet ds = new DataSet();//Using dataset to read xml file  
            ds.ReadXml(xmlData);
            var DataColumns = new List<ExcelDateCoumn>();
            DataColumns = (from rows in ds.Tables[0].AsEnumerable()
                           select new ExcelDateCoumn
                           {
                               Name = (rows[0].ToString()).Trim()
                           }).ToList();
            return DataColumns;
        }
        /// <summary>
        /// Return datatable for dynamic query
        /// </summary>
        /// <param name="sqlQuery"></param>
        /// <returns></returns>
        public static DataTable GetDatableFromSql(string sqlQuery)
        {
            IEMQSEntitiesContext db = new IEMQSEntitiesContext();
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(db.Database.Connection.ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(sqlQuery))
                {
                    cmd.Connection = conn;
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {
                        sda.Fill(dt);
                    }
                }
            }
            return dt;
        }
        /// <summary>
        /// Convert datatable to dynamic list
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<dynamic> ConvertDatatableToDynamicList(DataTable datatable)
        {
            var list = new List<dynamic>();
            foreach (var item in datatable.AsEnumerable())
            {
                // Expando objects are IDictionary<string, object>
                IDictionary<string, object> obj = new ExpandoObject();
                foreach (var column in datatable.Columns.Cast<DataColumn>())
                {
                    obj[column.ColumnName] = item[column] != DBNull.Value ? item[column] : string.Empty;
                }
                list.Add(obj);
            }
            return list;
        }
        public static string GetPropertyName(List<string> propertyKeys, string[] fields)
        {
            string column = "";
            if (propertyKeys != null)
                foreach (var item in fields)
                {
                    if (propertyKeys.Contains(item.ToLower()))
                        column = item.ToLower();
                }
            return column;
        }
        public static List<T> Filter<T>(this List<T> Source, Func<T, string> selector, string CompValue)
        {
            return Source.Where(a => selector(a) == CompValue).ToList();
        }
        public static bool IsChemstage(string stagecode, string bu, string location)
        {
            IEMQSEntitiesContext db = new IEMQSEntitiesContext();
            bool flag = false;
            string chemical = clsImplementationEnum.SeamStageType.Chemical.GetStringValue().ToLower();
            string type = db.QMS002.Where(x => x.StageCode == stagecode && x.BU == bu && x.Location == location).Select(x => x.StageType.ToLower()).FirstOrDefault();
            if (type == chemical)
            {
                flag = true;
            }
            return flag;
        }
        public static bool IsHardnessStage(string stageCode, string bu, string location)
        {
            bool flag = false;
            using (IEMQSEntitiesContext db = new IEMQSEntitiesContext())
            {
                string type = db.QMS002.Where(x => x.StageCode == stageCode && x.BU == bu && x.Location == location).Select(x => x.StageType.ToLower()).FirstOrDefault();
                if (type == "hardness")
                {
                    flag = true;
                }
            }
            return flag;
        }
        public static bool IsFerriteStage(string stageCode, string bu, string location)
        {
            bool flag = false;
            using (IEMQSEntitiesContext db = new IEMQSEntitiesContext())
            {
                string type = db.QMS002.Where(x => x.StageCode == stageCode && x.BU == bu && x.Location == location).Select(x => x.StageType.ToLower()).FirstOrDefault();
                if (type == "ferrite")
                {
                    flag = true;
                }
            }
            return flag;
        }
        public static bool CheckAttachmentUpload(string folderPath, string Type = "")
        {
            var existing = (new clsFileUpload(Type)).GetDocuments(folderPath);

            if (!existing.Any())
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        [NonAction]
        public static string GenerateHTMLDateControl(int rowId, string columnName, string columnValue = "")
        {
            return GenerateHTMLDateControl(rowId, columnName, columnValue, false, "", "", "", "");
        }
        [NonAction]
        public static string GenerateHTMLDateControl(int rowId, string columnName, string columnValue = "", bool disabled = false)
        {
            return GenerateHTMLDateControl(rowId, columnName, columnValue, disabled, "", "", "", "");
        }
        [NonAction]
        public static string GenerateHTMLDateControl(int rowId, string columnName, string columnValue = "", bool disabled = false, string inlineclassname = "")
        {
            return GenerateHTMLDateControl(rowId, columnName, columnValue, disabled, inlineclassname, "", "", "");
        }
        [NonAction]
        public static string GenerateHTMLDateControl(int rowId, string columnName, string columnValue = "", bool disabled = false, string inlineclassname = "", string inlineinputclassname = "")
        {
            return GenerateHTMLDateControl(rowId, columnName, columnValue, disabled, inlineclassname, inlineinputclassname, "", "");
        }
        [NonAction]
        public static string GenerateHTMLDateControl(int rowId, string columnName, string columnValue = "", bool disabled = false, string inlineclassname = "", string inlineinputclassname = "", string inputStyle = "")
        {
            return GenerateHTMLDateControl(rowId, columnName, columnValue, disabled, inlineclassname, inlineinputclassname, inputStyle, "");
        }

        [NonAction]
        public static string GenerateHTMLDateControl(int rowId, string columnName, string columnValue = "", bool disabled = false, string inlineclassname = "", string inlineinputclassname = "", string inputStyle = "", bool addColName = false)
        {
            return GenerateHTMLDateControl(rowId, columnName, columnValue, disabled, inlineclassname, inlineinputclassname, inputStyle, "", addColName);
        }
        [NonAction]
        public static string GenerateHTMLDateControl(int rowId, string columnName, string columnValue = "", bool disabled = false,
                                                    string inlineclassname = "", string inlineinputclassname = "", string inputStyle = "", string role = "", bool addColName = false)
        {
            string htmlControl = "";

            string inputID = columnName + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = !string.IsNullOrEmpty(columnValue) ? "value=\"" + columnValue + "\"" : "";
            string className = (disabled ? "" : "input-group date form_datetime ") + (!(string.IsNullOrWhiteSpace(inlineclassname)) ? inlineclassname : "");
            string userRole = role != "" && !string.IsNullOrEmpty(role) ? "role = '" + role + "'" : "";
            string dateformat = "dd/mm/yyyy";
            htmlControl = "<div class=\"" + className + "\" data-date-format=\"" + dateformat + "\">" +
                          "     <input " + (addColName ? "colname =\"" + columnName + "\"" : "") + " type =\"text\"" + inputValue + "\" size=\"16\" style=\"" + inputStyle + "\" readonly class=\"form-control " + (!string.IsNullOrEmpty(inlineinputclassname) ? inlineinputclassname : "") + "\" id=\"" + inputID + "\" name=\"" + inputID + "\" " + userRole + ">";
            if (!disabled)
            {
                htmlControl += "<span class=\"input-group-btn\">" +
                               "    <button id=\"btn" + columnName + rowId + "\" class=\"btn default date-set\" type=\"button\">" +
                               "        <i class=\"fa fa-calendar\"></i>" +
                               "    </button>" +
                               "</span>";
            }
            htmlControl += "</div>";
            return htmlControl;
        }

        [NonAction]
        public static string GenerateHTMLDateTimeControl(int rowId, string columnName, string columnValue = "")
        {
            return GenerateHTMLDateTimeControl(rowId, columnName, columnValue, false, false, false, "", "", "", "", "");
        }
        [NonAction]
        public static string GenerateHTMLDateTimeControl(int rowId, string columnName, string columnValue = "", bool disabled = false)
        {
            return GenerateHTMLDateTimeControl(rowId, columnName, columnValue, disabled, false, false, "", "", "", "", "");
        }
        [NonAction]
        public static string GenerateHTMLDateTimeControl(int rowId, string columnName, string columnValue = "", bool disabled = false, bool ismeridiandatetime = false)
        {
            return GenerateHTMLDateTimeControl(rowId, columnName, columnValue, disabled, ismeridiandatetime, false, "", "", "", "", "");
        }
        [NonAction]
        public static string GenerateHTMLDateTimeControl(int rowId, string columnName, string columnValue = "", bool disabled = false, bool ismeridiandatetime = false, bool isdisplayclearbutton = false)
        {
            return GenerateHTMLDateTimeControl(rowId, columnName, columnValue, disabled, ismeridiandatetime, isdisplayclearbutton, "", "", "", "", "");
        }
        [NonAction]
        public static string GenerateHTMLDateTimeControl(int rowId, string columnName, string columnValue = "", string format = "",
                                                    string inlineclassname = "", string inlineinputclassname = "", string inputStyle = "", string role = "")
        {
            return GenerateHTMLDateTimeControl(rowId, columnName, columnValue, false, false, false, format, inlineclassname, inlineinputclassname, inputStyle, role);
        }
        [NonAction]
        public static string GenerateHTMLDateTimeControl(int rowId, string columnName, string columnValue = "", bool disabled = false, string format = "",
                                                    string inlineclassname = "", string inlineinputclassname = "", string inputStyle = "", string role = "")
        {
            return GenerateHTMLDateTimeControl(rowId, columnName, columnValue, disabled, false, false, format, inlineclassname, inlineinputclassname, inputStyle, role);
        }
        [NonAction]
        public static string GenerateHTMLDateTimeControl(int rowId, string columnName, string columnValue = "", bool disabled = false, bool ismeridiandatetime = false, string format = "",
                                                    string inlineclassname = "", string inlineinputclassname = "", string inputStyle = "", string role = "")
        {
            return GenerateHTMLDateTimeControl(rowId, columnName, columnValue, disabled, ismeridiandatetime, false, format, inlineclassname, inlineinputclassname, inputStyle, role);
        }
        [NonAction]
        public static string GenerateHTMLDateTimeControl(int rowId, string columnName, string columnValue = "", bool disabled = false, bool ismeridiandatetime = false, bool isdisplayclearbutton = false, string format = "",
                                                    string inlineclassname = "", string inlineinputclassname = "", string inputStyle = "", string role = "")
        {
            string htmlControl = "";

            string inputID = columnName + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = !string.IsNullOrEmpty(columnValue) ? "value=\"" + columnValue + "\"" : "";
            string className = (disabled ? "" : "input-group date " + (ismeridiandatetime ? "form_meridian_datetime" : "") + " datetimecontrol ") + (!(string.IsNullOrWhiteSpace(inlineclassname)) ? inlineclassname : "");
            string userRole = role != "" && !string.IsNullOrEmpty(role) ? "role = '" + role + "'" : "";
            string datetimformat = !string.IsNullOrEmpty(format) ? format : "dd/mm/yyyy hh:ii";
            htmlControl = "<div class=\"" + className + "\" data-date-format=\"" + datetimformat + "\">" +
                          "     <input type=\"text\"" + inputValue + "\" size=\"16\" style=\"" + inputStyle + "\" readonly class=\"form-control " + (!string.IsNullOrEmpty(inlineinputclassname) ? inlineinputclassname : "") + "\" id=\"" + inputID + "\" name=\"" + inputID + "\" " + userRole + ">";
            if (!disabled)
            {
                htmlControl += "<span class=\"input-group-btn\">";
                if (isdisplayclearbutton)
                {
                    htmlControl += "    <button id=\"btnreset" + columnName + rowId + "\" class=\"btn default date-reset\" type=\"button\">" +
                                   "        <i class=\"fa fa-times\"></i>" +
                                   "    </button>";
                }
                htmlControl += "    <button id=\"btn" + columnName + rowId + "\" class=\"btn default date-set\" type=\"button\">" +
                               "        <i class=\"fa fa-calendar\"></i>" +
                               "    </button>";
                htmlControl += "</span>";
            }
            htmlControl += "</div>";
            return htmlControl;
        }

    }
    [Serializable]
    [XmlRoot("Date"), XmlType("Dates")]
    public class ExcelDateCoumn
    {
        public string Name { get; set; }
    }
    #endregion
}
