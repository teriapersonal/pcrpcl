//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    
    public partial class SP_WQ_GET_REQUESTHEADER_Result
    {
        public Nullable<long> ROW_NO { get; set; }
        public Nullable<int> TotalCount { get; set; }
        public int HeaderId { get; set; }
        public string RequestNo { get; set; }
        public string GeneralProject { get; set; }
        public string QualificationProject { get; set; }
        public string BU { get; set; }
        public string Location { get; set; }
        public string Jointtype { get; set; }
        public string WeldingProcess1 { get; set; }
        public string WeldingType1 { get; set; }
        public string WeldingPosition1 { get; set; }
        public string PNo1 { get; set; }
        public string FNo1 { get; set; }
        public string WeldingProcess2 { get; set; }
        public string WeldingType2 { get; set; }
        public string WeldingPosition2 { get; set; }
        public string PNo2 { get; set; }
        public string FNo2 { get; set; }
        public string CouponType { get; set; }
        public Nullable<int> PipeOD { get; set; }
        public Nullable<int> BaseMetalThickness { get; set; }
        public string ShopRemarks { get; set; }
        public string Requeststatus { get; set; }
        public string MaintainBydept { get; set; }
        public string MaintainedBy { get; set; }
        public Nullable<System.DateTime> MaintainedOn { get; set; }
        public string SubmittedBy { get; set; }
        public Nullable<System.DateTime> SubmittedOn { get; set; }
        public string ShopInCharge { get; set; }
        public Nullable<System.DateTime> ShopInChargeOn { get; set; }
        public string ShopInChargeRemarks { get; set; }
        public Nullable<System.DateTime> ShopInChargeReturnOn { get; set; }
        public string WE { get; set; }
        public Nullable<System.DateTime> WEOn { get; set; }
        public string WERemarks { get; set; }
        public Nullable<System.DateTime> WEReturnOn { get; set; }
        public string WQTC { get; set; }
        public Nullable<System.DateTime> WQTCOn { get; set; }
        public string RejectedBy { get; set; }
        public Nullable<System.DateTime> RejectedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
    }
}
