//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    
    public partial class SP_SSS_GETMockupBlock_Result
    {
        public Nullable<long> ROW_NO { get; set; }
        public Nullable<int> TotalCount { get; set; }
        public int SSSId { get; set; }
        public string SSSNo { get; set; }
        public string Contract { get; set; }
        public string Customer { get; set; }
        public Nullable<System.DateTime> CSRMeetingOn { get; set; }
        public int MockUpBlockId { get; set; }
        public string Notes { get; set; }
        public string ToPerson { get; set; }
        public string Status { get; set; }
    }
}
