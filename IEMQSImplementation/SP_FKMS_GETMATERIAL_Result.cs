//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    
    public partial class SP_FKMS_GETMATERIAL_Result
    {
        public Nullable<long> ROW_NO { get; set; }
        public Nullable<int> TotalCount { get; set; }
        public int Id { get; set; }
        public string EnablingMaterial { get; set; }
        public string Remarks { get; set; }
        public string IsReady { get; set; }
        public Nullable<int> MappingId { get; set; }
    }
}
