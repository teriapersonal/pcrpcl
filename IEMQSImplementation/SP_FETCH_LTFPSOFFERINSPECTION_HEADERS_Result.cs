//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    
    public partial class SP_FETCH_LTFPSOFFERINSPECTION_HEADERS_Result
    {
        public Nullable<long> ROW_NO { get; set; }
        public Nullable<int> TotalCount { get; set; }
        public int LineId { get; set; }
        public int HeaderId { get; set; }
        public string QualityProject { get; set; }
        public string LTFPSNo { get; set; }
        public int LTFPSRevNo { get; set; }
        public int OperationNo { get; set; }
        public string RefDocument { get; set; }
        public string DocRevNo { get; set; }
        public string TestFor { get; set; }
        public string SeamNo { get; set; }
        public string PartNo { get; set; }
        public string OperationDescription { get; set; }
        public string TestType { get; set; }
        public string Stage { get; set; }
        public string Agency { get; set; }
        public string TPI { get; set; }
        public bool DocumentRequired { get; set; }
        public string InspectionStatus { get; set; }
        public string LNTStatus { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public string Project { get; set; }
        public string BU { get; set; }
        public string Location { get; set; }
        public Nullable<bool> isNDEStage { get; set; }
    }
}
