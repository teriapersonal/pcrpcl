//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    
    public partial class SP_NDE_MT_CONSUMABLE_HISTORY_DETAILS_Result
    {
        public Nullable<long> ROW_NO { get; set; }
        public Nullable<int> TotalCount { get; set; }
        public int LogId { get; set; }
        public int Id { get; set; }
        public string Project { get; set; }
        public string BU { get; set; }
        public string Location { get; set; }
        public Nullable<int> RevNo { get; set; }
        public string MTConsuNo { get; set; }
        public string Status { get; set; }
        public string Method { get; set; }
        public string Manufacturer { get; set; }
        public string BrandType { get; set; }
        public string BatchNo { get; set; }
        public Nullable<System.DateTime> BatchDate1 { get; set; }
        public Nullable<System.DateTime> BatchDate2 { get; set; }
        public Nullable<System.DateTime> BatchDate3 { get; set; }
        public Nullable<System.DateTime> BatchDate4 { get; set; }
        public string PlannerRemarks { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public string ApprovedBy { get; set; }
        public Nullable<System.DateTime> ApprovedOn { get; set; }
        public string ReturnedBy { get; set; }
        public Nullable<System.DateTime> ReturnedOn { get; set; }
        public string ReturnRemarks { get; set; }
        public string QualityProject { get; set; }
    }
}
