//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    
    public partial class FN_GET_HBOM_DATA_Result
    {
        public string Project { get; set; }
        public string Part { get; set; }
        public string ParentPart { get; set; }
        public Nullable<int> PartLevel { get; set; }
        public string Description { get; set; }
        public string ItemGroup { get; set; }
        public string RevisionNo { get; set; }
        public string FindNo { get; set; }
        public string MaterialSpecification { get; set; }
        public string MaterialDescription { get; set; }
        public string MaterialCode { get; set; }
        public string ProductType { get; set; }
        public Nullable<double> Length { get; set; }
        public Nullable<double> Width { get; set; }
        public Nullable<int> NoOfPieces { get; set; }
        public Nullable<double> Thickness { get; set; }
        public string UOM { get; set; }
        public Nullable<double> BOMQty { get; set; }
        public string Element { get; set; }
        public Nullable<double> Weight { get; set; }
    }
}
