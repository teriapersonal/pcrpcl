//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    
    public partial class SP_IPI_SEAMLISTOFFERINSPECTION_Result
    {
        public Nullable<long> ROW_NO { get; set; }
        public Nullable<int> TotalCount { get; set; }
        public int HeaderId { get; set; }
        public string QualityProject { get; set; }
        public string Project { get; set; }
        public string BU { get; set; }
        public string Location { get; set; }
        public string SeamNo { get; set; }
        public string StageCode { get; set; }
        public string StageCodeDescription { get; set; }
        public Nullable<int> StageSequence { get; set; }
        public Nullable<int> IterationNo { get; set; }
        public string InspectionStatus { get; set; }
        public string LNTInspectorResult { get; set; }
        public string Remarks { get; set; }
        public bool SeamCleared { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public Nullable<bool> isNDEStage { get; set; }
        public Nullable<int> ProtocolId { get; set; }
        public string ProtocolType { get; set; }
        public string AcceptanceStandard { get; set; }
        public string ApplicableSpecification { get; set; }
        public string InspectionExtent { get; set; }
        public string Remark1 { get; set; }
        public string ProtocolTypeDescription { get; set; }
    }
}
