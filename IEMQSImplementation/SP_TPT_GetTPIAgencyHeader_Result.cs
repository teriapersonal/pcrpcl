//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    
    public partial class SP_TPT_GetTPIAgencyHeader_Result
    {
        public Nullable<long> ROW_NO { get; set; }
        public Nullable<int> TotalCount { get; set; }
        public int Id { get; set; }
        public string QualityProject { get; set; }
        public string Stage { get; set; }
        public string StageType { get; set; }
        public string StageDesc { get; set; }
        public string Project { get; set; }
        public string BU { get; set; }
        public string Location { get; set; }
        public string FirstTPIAgency { get; set; }
        public string FirstTPIIntervention { get; set; }
        public string SecondTPIAgency { get; set; }
        public string SecondTPIIntervention { get; set; }
        public string ThirdTPIAgency { get; set; }
        public string ThirdTPIIntervention { get; set; }
        public string ForthTPIAgency { get; set; }
        public string ForthTPIIntervention { get; set; }
        public string FifthTPIAgency { get; set; }
        public string FifthTPIIntervention { get; set; }
        public string SixthTPIAgency { get; set; }
        public string SixthTPIIntervention { get; set; }
        public string Remarks { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public string FirstTPIAgencyDesc { get; set; }
        public string FirstTPIInterventionDesc { get; set; }
        public string SecondTPIAgencyDesc { get; set; }
        public string SecondTPIInterventionDesc { get; set; }
        public string ThirdTPIAgencyDesc { get; set; }
        public string ThirdTPIInterventionDesc { get; set; }
        public string ForthTPIAgencyDesc { get; set; }
        public string ForthTPIInterventionDesc { get; set; }
        public string FifthTPIAgencyDesc { get; set; }
        public string FifthTPIInterventionDesc { get; set; }
        public string SixthTPIAgencyDesc { get; set; }
        public string SixthTPIInterventionDesc { get; set; }
    }
}
