//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQSImplementation
{
    using System;
    using System.Collections.Generic;
    
    public partial class QMS019
    {
        public int LineId { get; set; }
        public Nullable<int> HeaderId { get; set; }
        public Nullable<int> QIDLineId { get; set; }
        public string Project { get; set; }
        public string QualityProject { get; set; }
        public string BU { get; set; }
        public string Location { get; set; }
        public string QualityId { get; set; }
        public Nullable<int> QualityIdRev { get; set; }
        public string StageCode { get; set; }
        public Nullable<int> StageSequence { get; set; }
        public string Element { get; set; }
        public Nullable<double> ReqMinValue { get; set; }
        public Nullable<double> ReqMaxValue { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
    
        public virtual QMS018 QMS018 { get; set; }
        public virtual QMS016 QMS016 { get; set; }
    }
}
