﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace IEMQSImplementation
{
    public class clsReport
    {

        public static HtmlString GetReportData(string ServerRelativeReportURL, List<SSRSParam> ReportParameters, string CustomServerUrl)
        {
            try
            {
                string UrlToHit = GetSSRSReportUrl(ServerRelativeReportURL, ReportParameters, CustomServerUrl);
                var res = "<html><head></head><body style=\"margin:0px\"><iframe sytle=\"max-width:100%; max-height:100%;\" src=\"" + UrlToHit + "\" onload=\"this.width = window.innerWidth; this.height = window.innerHeight;\"></iframe><body>";

                // var res = code(UrlToHit);
                // res = res.Replace("</head>", "<style>.resize100Height{height:100% !important}</style></head>");
                //res = res.Replace("</head>", "<style>div[dir=\"LTR\"]>div{display:inline}</style></head>");
                //res = res.Replace("<div style=\"page-break-after:always\"><hr/></div>", ""); //Replace added to avoid Extra pages in Print
                /*var bodystart = res.IndexOf(">", res.IndexOf("<body")) + 1;
                var body = res.Substring(bodystart, res.IndexOf("</body>") - bodystart);
                ReportDiv.InnerHtml = body;*/
                return new HtmlString(res);
            }
            catch (Exception) { }
            return new HtmlString("Error");
        }

        private static String code(string Url)
        {
            HttpWebRequest myRequest = (HttpWebRequest)WebRequest.Create(Url);
            myRequest.Method = "GET";
            myRequest.Credentials = CredentialCache.DefaultCredentials;
            WebResponse myResponse = myRequest.GetResponse();
            StreamReader sr = new StreamReader(myResponse.GetResponseStream(), System.Text.Encoding.ASCII);
            string result = sr.ReadToEnd();
            sr.Close();
            myResponse.Close();

            return result;
        }

        private static string GetSSRSReportUrl(string serverRelativeUrlPath, List<SSRSParam> ReportParameters, string CustomServerPath = null, int? PageNumber = null)
        {
            string ServerURL = ConfigurationManager.AppSettings["SSRS_Server1"].ToString();
            //"ReportServer_IN_SQL2K16_DV?"
            string URL = (string.IsNullOrEmpty(CustomServerPath) ? (ServerURL + "ReportServer_IN_SQL2K16_DV/Pages/ReportViewer.aspx?") : CustomServerPath) + serverRelativeUrlPath + "&rs:Command=Render&rc:Parameters=False&rc:Toolbar=True";
            //string URL += ServerURL + "ReportServer/Pages/ReportViewer.aspx?"  +serverRelativeUrlPath + "&rs:Command=Render&rc:Parameters=Collapsed&rc:Toolbar=False";
            URL += (PageNumber == null) ? "" : "&rc:Section=" + PageNumber.Value.ToString();
            if (ReportParameters != null)
            {
                foreach (var parameter in ReportParameters)
                {
                    URL += "&" + HttpUtility.UrlEncode(parameter.Param) + "=" + HttpUtility.UrlEncode(parameter.Value);
                }
            }
            return URL;
        }

     

    }


    public class SSRSParam
    {
        public SSRSParam() { }
        public SSRSParam(string Param, string Value)
        {
            this.Param = Param;
            this.Value = Value;
        }

        public string Param { get; set; }
        public string Value { get; set; }
    }
}
