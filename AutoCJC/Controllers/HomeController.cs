﻿using AutoCJC.CJCService;
using AutoCJC.HEDirectoryService;
using AutoCJC.Models;
using IEMQSImplementation;
using IEMQSImplementation.Models;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Mvc;
using System.Configuration;
using System.Security.Principal;

namespace AutoCJC.Controllers
{
    public class HomeController : clsBase
    {
        public ActionResult Login()
        {
            bool isLogin = false;
            _objclsLoginInfo = new clsLoginInfo();
            DirectoryService objService = new DirectoryService();
            if (!string.IsNullOrWhiteSpace(Request["bpid"]))
            {
                try
                {
                    string bpid = objService.Decrypt(Request["bpid"]);
                    COM006 objCOM006 = db.COM006.Where(x => x.t_bpid.Trim() == bpid.Trim()).FirstOrDefault();
                    if (objCOM006 != null)
                    {
                        isLogin = true;
                        Session["userid"] = objCOM006.t_bpid;
                        objClsLoginInfo.UserName = objCOM006.t_bpid;
                        objClsLoginInfo.Name = objCOM006.t_nama;
                        Session[clsImplementationMessage.Session.LoginInfo] = objClsLoginInfo;
                    }
                }
                catch { }
            }
            if (objClsLoginInfo == null || !isLogin)
            {
                return RedirectToAction("AccessDenied", "Home");
            }
            else
            {
                return RedirectToAction("Index", "GenerateCJC");
            }
        }

        public ActionResult OpenSSRSReport(string serverRelativeUrlPath, List<SSRSParam> ReportParameters, bool ShowAttachment = false)
        {
            //var credentials = new WindowsImpersonationCredentials();

            string url = Request.Url.AbsoluteUri;           // http://localhost:1302/TESTERS/Default6.aspx
            string path = Request.Url.AbsolutePath;         // /TESTERS/Default6.aspx
            ViewBag.ReportUrl = serverRelativeUrlPath;
            var ReportParams = new List<ReportParameter>();
            var param = new Dictionary<string, string[]>();
            if (ReportParameters != null)
            {
                foreach (var rp in ReportParameters)
                {
                    if (param.Any(q => q.Key.Equals(rp.Param)))
                    {
                        var oldValue = param.FirstOrDefault(q => q.Key.Equals(rp.Param)).Value.ToList();
                        param.Remove(rp.Param);
                        oldValue.Add(rp.Value);
                        param.Add(rp.Param, oldValue.ToArray());
                    }
                    else
                        param.Add(rp.Param, new string[] { rp.Value });
                }
            }
            foreach (var p in param)
                ReportParams.Add(new ReportParameter(p.Key, p.Value));

            ViewBag.Params = ReportParams;
            //D:/Nikita/Project/IEMQS/IEMQS/AutoCJC/Views/Home/Report.cshtml
            return View("Report");//return clsReport.GetReportData(serverRelativeUrlPath, ReportParameters, CustomServerPath);
        }
        public ReportViewer GetReportServerCreadential(ReportViewer viewer, string ReportPath, List<ReportParameter> ReportParams)
        {
            var credentials = new WindowsImpersonationCredentials(System.Configuration.ConfigurationManager.AppSettings["File_Upload_UserID"], System.Configuration.ConfigurationManager.AppSettings["File_Upload_Password"], System.Configuration.ConfigurationManager.AppSettings["NetworkDomain"]);

            viewer.ProcessingMode = ProcessingMode.Remote;
            viewer.ServerReport.ReportServerCredentials = credentials;
            viewer.ServerReport.ReportPath = System.Configuration.ConfigurationManager.AppSettings["SSRS_Report_Folder"] + ReportPath;
            viewer.ServerReport.ReportServerUrl = new Uri(System.Configuration.ConfigurationManager.AppSettings["SSRS_Server_URL"]);
            viewer.ServerReport.SetParameters((List<ReportParameter>)ReportParams);

            return viewer;
        }
        public ActionResult AccessDenied()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult External(string ReturnURL)
        {
            return Redirect(ReturnURL);
        }
        [Serializable]
        public class WindowsImpersonationCredentials : IReportServerCredentials
        {
            // local variable for network credential.
            private string _UserName;
            private string _PassWord;
            private string _DomainName;
            public WindowsImpersonationCredentials(string UserName, string PassWord, string DomainName)
            {
                _UserName = UserName;
                _PassWord = PassWord;
                _DomainName = DomainName;
            }
            public bool GetFormsCredentials(out Cookie authCookie, out string userName, out string password, out string authority)
            {
                authCookie = null;
                userName = password = authority = null;
                return false;
            }

            public WindowsIdentity ImpersonationUser
            {
                get { return WindowsIdentity.GetCurrent(); }
            }

            public ICredentials NetworkCredentials
            {
                get { return new NetworkCredential(_UserName, _PassWord, _DomainName); }
            }

            public override string ToString()
            {
                return String.Format("WindowsIdentity: {0} ({1})", this.ImpersonationUser.Name, this.ImpersonationUser.User.Value);
            }
        }
    }
}