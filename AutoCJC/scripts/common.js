﻿var ServerSideError = "Error in Server Side";
var ClientSideError = "Error in Script";
//function for load master dropdown

//Develop By Jaydeep Sakariya
function LoadDropDown(url, objControl, lbloptional, data) {
    //$.get(url, data, function (data) {
    //    var option = ((lbloptional === undefined || lbloptional === null) ? "" : "<option value='' selected='selected'>--Select " + lbloptional + "--</option>");
    //    $.each(data, function (index, object) {
    //        option += "<option value='" + object.id + "'>" + object.text + "</option>";
    //    });
    //    $(objControl).html(option);



    //});

    $.ajax({
        url: WebsiteURL + url,
        type: "GET",
        data: data,
        async: false,
        success: function (data) {
            var option = ((lbloptional === undefined || lbloptional === null) ? "" : "<option value='' selected='selected'>--Select " + lbloptional + "--</option>");
            $.each(data, function (index, object) {
                option += "<option value='" + object.id + "'>" + object.text + "</option>";
            });
            $(objControl).html(option);
        }
    })

};
//Develop By Jaydeep Sakariya
function LoadSelect2DropDown(url, objControl, placeHolder, condParam) {
    objControl.select2({
        placeholder: placeHolder,
        delay: 250,
        allowClear: true,
        minimumInputLength: 1,
        ajax: {
            url: WebsiteURL + url,
            type: "POST",
            dataType: 'json',
            data: function (params) {

                var query = {
                    search: params.term,
                    page: params.page,
                    param: condParam
                }

                // Query paramters will be ?search=[term]&page=[page]
                return query;
            },
            processResults: function (data) {
                return {
                    results: data
                };
            }
        }
    });
};

function Select2Selected(objControl, id, text) {
    objControl.select2({
        data: [
            {
                id: id,
                text: text
            }
        ]
    });
};


function Select2Edit(url, objControl, caption, id, text, param) {

    $.ajax({
        url: WebsiteURL + url,
        type: "GET",
        data: param,
        async: false,
        success: function (data) {
            var option = "<option value='" + id + "' selected='selected'>" + text + "</option>";
            $.each(data, function (index, object) {
                option += "<option value='" + object.id + "'>" + object.text + "</option>";
            });
            $(objControl).html(option);

            objControl.select2({
                placeholder: caption,
                delay: 250,
                allowClear: true,
                minimumInputLength: 1
            });
        }
    })

};

function LoadPreValueSelect2(url, objControl, caption, param) {
    $.ajax({
        url: WebsiteURL + url,
        type: "GET",
        data: param,
        async: false,
        success: function (data) {
            var option = "";
            $.each(data, function (index, object) {
                option += "<option value='" + object.id + "'>" + object.text + "</option>";
            });
            $(objControl).html(option);

            objControl.select2({
                placeholder: caption

            });
        }
    })

};

function LoadMultiSelect2OnEdit(strPosition) {
    var arrPositions;
    if (strPosition.length > 0) {
        arrPositions = strPosition.split(',');
    }
    return arrPositions;
}

function FillAutoCompleteForEnum(element, sourceList, hdElement) {
    element.autocomplete({
        source: function (request, response) {
            var array = $.map(sourceList, function (item) {
                return {
                    label: item.Text,
                    value: item.Text,
                    id: item.Value
                }
            });
            response($.ui.autocomplete.filter(array, request.term));
        },
        autoFocus: true,
        minLength: 0,
        select: function (event, ui) {
            $(this).val(ui.item.id);
            $(hdElement).val(ui.item.id);
            //HighlightInValidControls();
        },
        change: function (event, ui) {
            if (ui.item == null) {
                element.val('');
                element.focus();
                //HighlightInValidControls();
            }
        }
    }).focus(function () {
        $(this).data("uiAutocomplete").search($(this).val());
        $(this).autocomplete("search");
    });
}

function FillAutoComplete(element, sourceList, hdElement) {
    element.autocomplete({
        source: function (request, response) {
            var array = $.map(sourceList, function (item) {
                return {
                    label: item.Value,
                    value: item.Value,
                    id: item.Key
                }
            });
            response($.ui.autocomplete.filter(array, request.term));
        },
        autoFocus: true,
        minLength: 0,
        select: function (event, ui) {
            $(this).val(ui.item.id);
            $(hdElement).val(ui.item.id);
            HighlightInValidControls();
        },
        change: function (event, ui) {
            if (ui.item == null) {
                element.val('');
                $(hdElement).val('');
                element.focus();
                HighlightInValidControls();
            }
        }
    }).focus(function () {
        $(this).data("uiAutocomplete").search($(this).val());
        $(this).autocomplete("search");
    });
}

function HighlightInValidControls() {
    $('.error').focus(function () {
        $(this).parent().find('.tooltip').removeClass('hidetooltip').addClass('showtooltip');
    });

    $('.error').blur(function () {
        $(this).parent().find('.tooltip').removeClass('showtooltip').addClass('hidetooltip');
    });

    $('select.error').each(function () {
        //console.log($(this).parent());

        $(this).parent().find('.select2').addClass('error');
    });

    HighlightInValidateSelect2();
}

function HighlightInValidateSelect2() {
    $('.select2-search__field').not("#divLinkedProject *").focus(function () {
        ValidateSearch($(this));
    });

    $('.select2-search__field').not("#divLinkedProject *").blur(function () {
        ValidateSearch($(this));
    });

}
function ValidateSearch(search__field) {
    var $ul = search__field.closest('.select2-selection__rendered');
    var $select2 = search__field.closest('.select2');

    //var hasError = $ul.find('.select2-selection__choice').length;
    //if (hasError == 0) {
    //    $select2.addClass('error');
    //    $select2.parent().find('.tooltip').removeClass('hidetooltip').addClass('showtooltip');
    //}
    //else {
    //    $select2.removeClass('error');
    //    $select2.parent().find('.tooltip').removeClass('showtooltip').addClass('hidetooltip');
    //}
    var notrequired = $('.select2-search__field').closest('.select2').parent().find('.select2-hidden-accessible').hasClass('notrequired');
    if (!notrequired) {
        var selectedItemCnt = $ul.find('.select2-selection__choice').length;
        if (selectedItemCnt == 0) {
            $select2.addClass('error');
            $select2.parent().find('.tooltip').removeClass('hidetooltip').addClass('showtooltip');
        }
        else {
            $select2.removeClass('error');
            $select2.parent().find('.tooltip').removeClass('showtooltip').addClass('hidetooltip');
        }
    }
    else {
        $select2.removeClass('error');
        $select2.parent().find('.tooltip').removeClass('showtooltip').addClass('hidetooltip');
    }
}

function ShowTimeline(url) {
    $.ajax({
        url: WebsiteURL + url,
        dataType: "html",
        type: "GET",
        delay: 250,
        async: false,
        success: function (result) {
            bootbox.dialog({
                title: "Timeline",
                message: result,
                size: 'large'
            });
        }
    });
}


function ReviseHeader(id, path, revElement, hdRevElement, spnStatus, callbackfun) {
    var strHtml = "<scr" + "ipt>$(document).ready(function(){"
                + "$('input[maxlength],textarea[maxlength]').maxlength({alwaysShow: true,});"
                + "$('#frmHeaderRevise').validate({"
                + " rules: {"
                + "txtReviseRemark: {"
                + "required: true,"
                + "},"
                + "messages: {"
                + "txtReviseRemark: {"
                + "required: 'Revise Remark is required'"
                + "}"
                + "}"
                 + "}"
                + "});"
                + "});</scr" + "ipt>"
                + " <div>"
                + "<form method='post' id='frmHeaderRevise' class='form-horizontal' action=''>"
                + "<div class='form-group'>"
                + "<div class='col-md-12'>"
                + "<br /><label class='control-label'><b>Revise Remark</b><span class='required'> * </span></label>"
                + "<div class=''>"
                + "<textarea type='text' value='' style='width:100%' name='txtReviseRemark' id='txtReviseRemark' maxlength='200' class='form-control col-md-3'></textarea>"
                + "</div>"
                + "</div>"
                + "</div>"
                + "</form>"
                + "</div>";

    bootbox.confirm({
        title: 'Revise Document',
        message: 'Do you want to Revise Document?' + strHtml,
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result) {
                //debugger;
                if ($('#frmHeaderRevise').valid()) {
                    var Remarks = $('#txtReviseRemark').val();
                    $.ajax({
                        url: WebsiteURL + path,
                        type: "POST",
                        datatype: 'json',
                        data: { strHeaderId: id, strRemarks: $('#txtReviseRemark').val() },
                        success: function (data) {
                            if (data.Key) {
                                revElement.val("R" + data.rev);
                                hdRevElement.val(data.rev);
                                spnStatus.text(data.Status);
                                DisplayNotiMessage("success", data.Value, "Success");
                                if ($('#ReviseRemark') != null && $('#ReviseRemark') != undefined && $('#ReviseRemark') != 'undefined') {
                                    $('#ReviseRemark').val(Remarks);
                                    $('#ReviseRemark').css({ "display": "inline-block", })
                                }
                                if (callbackfun != null && callbackfun != undefined && callbackfun != 'undefined') {
                                    callbackfun();
                                }
                            }
                            else {
                                DisplayNotiMessage("error", data.Value, "Error");
                                return false;
                            }
                        },
                        error: function () {
                        }
                    });

                }
                else {
                    return false;
                }
            }
            //else { return false; }
        }
    });
}


function ViewHistoryForProjectPLN(HeaderId, Role, URL, Plan) {
    $.ajax({
        type: "POST",
        url: WebsiteURL + URL,
        datatype: 'html',
        async: false,
        beforeSend: function () {
            showLoading(true);
        },
        complete: function () {
            showLoading(false);
        },
        data: { strRole: Role, "HeaderId": HeaderId },
        success: function (result) {
            bootbox.dialog({
                title: 'History of ' + Plan,
                message: result,
                size: 'large',
                buttons: {
                    'cancel': {
                        label: 'Close',
                        className: 'btn btnclose'
                    }
                }
            });
        },
        error: function () {
        }
    });
}


function RevokeDoument(Url, HeaderId, pdinId, RedirectURL) {
    var strHtml = "<scr" + "ipt>$(document).ready(function(){"
                    + "$('#frmRevokeRemarks').validate({"
                    + " rules: {"
                    + "txtRemarks: {"
                    + "required: true,"
                    + "},"
                    + "messages: {"
                    + "txtRemarks: {"
                    + "required: 'Remark is required'"
                    + "}"
                    + "}"
                     + "}"
                    + "});"
                    + "});</scr" + "ipt>"
                    + " <div>"
                    + "<form method='post' id='frmRevokeRemarks' class='form-horizontal' action=''>"
                    + "<div class='form-group'>"
                    + "<div class='col-md-12'>"
                    + "<label class='control-label'>Remarks<span class='required'> * </span></label>"
                    + "<div class=''>"
                    + "<textarea value='' name='txtRemarks' id='txtRemarks' class='form-control col-md-12'></textarea>"
                    + "</div>"
                    + "</div>"
                    + "</div>"
                    + "</form>"
                    + "</div>";


    bootbox.dialog({
        title: "Revoke Confirmation",
        message: 'Do you want to revoke document?' + strHtml,
        size: "medium",
        buttons: {
            Yes: {
                Label: "Yes",
                className: 'btn green',
                callback: function () {
                    if ($('#frmRevokeRemarks').valid()) {
                        $.ajax({
                            type: "POST",
                            url: WebsiteURL + Url,
                            datatype: 'json',
                            async: false,
                            beforeSend: function () {
                                showLoading(true);
                            },
                            complete: function () {
                                showLoading(false);
                            },
                            data: { HeaderId: HeaderId, pdinId: pdinId, Remarks: $("#txtRemarks").val() },
                            success: function (data) {
                                if (data.Key) {
                                    DisplayNotiMessage("success", data.Value, "Success");
                                    //  window.location.href = '/PLN/ApproveReferenceSketch/Index';
                                    window.setTimeout("window.location.href ='" + WebsiteURL + RedirectURL + "'", 2000);
                                }
                                else {
                                    DisplayNotiMessage("error", data.Value, "Error");
                                }
                            },
                            error: function () {
                            }
                        });
                        return true;
                    }
                    else {
                        return false;
                    }
                }
            },
            Cancel: {
                label: 'No',
                className: 'btn red',
                callback: function () { }
            }

        }
    });
}


function DisabledReadOnlyDropdownOpen() {
    $('select[readonly]').on('mousedown', function (e) {
        e.preventDefault();
        this.blur();
        window.focus();
    });
}

var TableDatatablesFixedHeader = function () {
    var oTable;

    function initFixHeaderTable(table) {
        //var table = $('#sample_3');

        var fixedHeaderOffset = 0;
        if (App.getViewPort().width < App.getResponsiveBreakpoint('md')) {
            if ($('.page-header').hasClass('page-header-fixed-mobile')) {
                fixedHeaderOffset = $('.page-header').outerHeight(true);
            }
        } else if ($('.page-header').hasClass('navbar-fixed-top')) {
            fixedHeaderOffset = $('.page-header').outerHeight(true);
        } else if ($('body').hasClass('page-header-fixed')) {
            fixedHeaderOffset = 64; // admin 5 fixed height
        }

        oTable = table.dataTable({
            "bDestroy": true,
            // setup rowreorder extension: http://datatables.net/extensions/fixedheader/
            fixedHeader: {
                header: true,
                headerOffset: fixedHeaderOffset
            },
            searching: false,
            paging: false,
            "bSort": false
        });
    }

    function disabledFixHeader() {
        if (oTable != null && oTable != undefined && oTable != "undefined") {
            oTable.fnDestroy();
        }
    }

    return {
        //main function to initiate the module
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }
        },
        enabled: function (table) {
            initFixHeaderTable(table);
        },
        disabled: function () {
            disabledFixHeader();
        }
        ,
        getoTable: function () {
            console.log(oTable);
        }
    };
}();


var InlineGridEdit = function () {
    var edittbl;
    var btnAddNew;
    //var contentdiv;
    var editetr = {};
    function Initialize(btnid, tblid, dv) {
        edittbl = $(dv).find($(tblid));
        btnAddNew = $(dv).find($(btnid));
        //contentdiv = dv;   
        //console.log(edittbl);
        //console.log(btnAddNew);
        editetr = {};
        btnAddNew.unbind("click");
        btnAddNew.click(function () {
            InsertNewRecordInline($(this), tbl);
        });
        EditLineEnable();
        SetNumericField();
        SetAddNewIntialStage();
    }
    function InsertNewRecordInline(obj, tbl) {
        $('input[maxlength]').maxlength({
            alwaysShow: true,
        });
        tbl.find("tbody tr:first").toggle();
        tbl.find("tbody tr:first select:first").focus();
        $(obj).html('<i class="fa fa-times"></i> Cancel');
        if (tbl.find("tbody tr:first").is(":visible")) {
            $(obj).val("Cancel");
            var delayedFn, blurredFrom;
            tbl.find("tbody tr:first").on('blur', 'a', function (event) {
                blurredFrom = event.delegateTarget;
                delayedFn = setTimeout(function () { SaveLineRecord(0); }, 0);
            });
            tbl.find("tbody tr:first").on('focus', 'a', function (event) {
                if (blurredFrom === event.delegateTarget) {
                    clearTimeout(delayedFn);
                }
            });

            if (editetr.id != null && editetr.id != undefined && editetr.id != "undefined") {
                var $tr = $("[data-id='" + editetr.id + "']");
                $tr.html(editetr.text);
            }
            EditLineEnable();
        }
        else {
            $(obj).val("New");
            $(obj).html('<i class="fa fa-plus"></i> New');
        }
    }

    function EditLineEnable() {
        //remove previous click event
        $(edittbl).find(".editline").unbind("click");

        //add new click event
        $(edittbl).find(".editline").click(function () {

            //If any rows already in edit mode then set as normal row
            edittbl.find('tbody tr').not("tbody tr:first").each(function () {

                $trold = $(this);
                //set readonly or disabled property to all element
                $trold.find('input,select,textarea').attr("readonly", "readonly");
                //$tr.find('input,select,textarea').attr("disabled", "disabled");                
                //find any div edit with editmode. If any then hide update and cancel button and show edit button
                if ($trold.find('.editmode').length > 0) {
                    $trold.find('.editmode').hide();
                    $trold.find('.editline').show();
                }
            })

            var $tr = $(this).closest("tr");

            EditLine($(this), $tr.data("hid"), $tr.data("id"));

        });
    }

    function EditLine(btnEdit, headerId, lineId) {
        //debugger;
        var $tr = $(btnEdit).closest("tr");
        editetr.text = $tr.html();
        editetr.id = $tr.data("id");
        var $td = $(btnEdit).closest("td");
        //console.log($tr);
        //remove readonly attribute for row so user can edit data.
        $tr.find("input,select,textarea").each(function () {
            $(this).removeAttr("readonly");
            $(this).removeAttr("disabled");
        });
        $('input[maxlength]').maxlength({
            alwaysShow: true,
        });


        //check there is any div exist with update and cancel button div. If exist then show div otherwise create new div for update and cancel button.
        if ($td.find('.editmode').length == 0) {
            var editdiv = "<div class='editmode' style='display:inline;'><i id='Update" + lineId + "' name='Update" + lineId + "' style='cursor: pointer;' title='Update Record' class='fa fa-floppy-o iconspace ' onclick='UpdateLine(this," + lineId + ");'></i>";
            editdiv += "<i id='btnCancel" + lineId + "' name='btnCancel" + lineId + "' style='cursor: pointer;' title='Cancel Edit' class='fa fa-ban iconspace canceledit'></i></div> "
            $td.append(editdiv);
        }
        else {
            $td.find('.editmode').show();
        }
        //hide edit button while enable edit mode
        $(btnEdit).hide();

        SetAddNewIntialStage();
        CancelEditEnable();
        SetNumericField();
    }

    function CancelEditEnable() {
        $(edittbl).find(".canceledit").unbind("click");
        $(edittbl).find(".canceledit").click(function () {
            CancelEditLine($(this));
        });
    }
    function CancelEditLine(btnCancel) {
        var $tr = $(btnCancel).closest("tr");
        $tr.html(editetr.text);
        editetr = {};
        SetAddNewIntialStage();
        EditLineEnable();
    }

    function SetAddNewIntialStage() {
        btnAddNew.html('<i class="fa fa-plus"></i> New');
        if (edittbl.find("tbody tr:first").is(":visible")) {
            edittbl.find("tbody tr:first").toggle();
        }
    }

    function SetNumericField() {
        jQuery(function ($) {
            $('.numeric').autoNumeric('init', { aPad: "fase", vMax: "1000000000", aForm: "false" });
            $('.numeric18').autoNumeric('init', { aPad: "fase", vMax: "999999999999999999", aForm: "false" });
            $('.numeric4').autoNumeric('init', { aPad: "fase", vMax: "9999", aForm: "false" });
        });
    }

    return {
        init: function (btnid, tblid, dv) {
            Initialize(btnid, tblid, dv);
        }
    }
}();


function FileUploadEnt(DivId, ControlID) {
    this.ControlID = ControlID;
    this.ActionURL = '';
    this.FolderPath = '';
    this.Type = '';
    this.ShowDeleteAction = false;
    this.MaxChunkSizeInMB = 15;
    this.ShowAttachmentBtn = true;
    this.ChunkIntervalInSec = 1;
    this.IsDisabled = null;
    this.ReloadAfterUpload = null;
    this.IsRequired = false;
    this.AllowFileExtension = '';
    this.MaxFileUploadSizeInMB = null;
    this.SuccessCallbackFunction = '';
    this.LoadCallbackFunction = null;
    this.DivId = DivId;
    this.LoadInPopup = false;
    this.PopupTitle = "File Upload";
    this.ShowDownloadBtn = true;
    this.ShowUploadBtn = true;
    this.IsMultipleUploaded = true;
}

function LoadFileUploadPartialView(objFile) {
    if (objFile.ControlID == undefined || objFile.ControlID == null || objFile.ControlID == "") {
        DisplayNotiMessage("Error", "ControlId not define", "File Upload");
        return;
    }
    //if (folderpath == undefined || folderpath == null || folderpath == "") {
    //    DisplayNotiMessage("Error", "Folder Path Not Define", "File Upload");return;
    //}
    if (objFile.SuccessCallbackFunction === undefined || objFile.SuccessCallbackFunction === null) {
        objFile.SuccessCallbackFunction = "";
    }

    $.ajax({
        url: WebsiteURL + "/Utility/FileUpload/LoadFileUploadPartial",
        type: "POST",
        datatype: "html",
        beforeSend: function () {
            showLoading(true);
        },
        complete: function () {
            showLoading(false);
        },
        data: {
            controlid: objFile.ControlID, folderpath: objFile.FolderPath, maxfileuploadsizeinmb: objFile.MaxFileUploadSizeInMB,
            maxchunksizeinmb: objFile.MaxChunkSizeInMB, chunkintervalinsec: objFile.ChunkIntervalInSec,
            showdeleteaction: objFile.ShowDeleteAction, showattachmentbtn: objFile.ShowAttachmentBtn,
            isdisabled: objFile.IsDisabled, reloadafterupload: objFile.ReloadAfterUpload, isrequired: objFile.IsRequired,
            actionurl: objFile.ActionURL, type: objFile.type, allowfileextension: objFile.AllowFileExtension,
            successcallbackfunction: objFile.SuccessCallbackFunction, showdownloadbtn: objFile.ShowDownloadBtn,
            ismultipleuploaded: objFile.IsMultipleUploaded
        },
        success: function (data) {
            if (objFile.LoadInPopup) {
                bootbox.dialog({
                    title: objFile.PopupTitle,
                    message: data,
                    size: "large",
                    buttons: {
                        Cancel: {
                            label: 'Close',
                            className: 'btn red',
                            callback: function () { }
                        }
                    }
                });
                if (!objFile.ShowUploadBtn) {
                    $('#btnUploadAll' + objFile.ControlID).hide()
                }
            }
            else {
                $("#" + objFile.DivId).html(data);
                if (!objFile.ShowUploadBtn) {
                    $('#btnUploadAll' + objFile.ControlID).hide()
                }
            }

            if (objFile.LoadCallbackFunction != null && objFile.LoadCallbackFunction != undefined && typeof objFile.LoadCallbackFunction === 'function') {
                objFile.LoadCallbackFunction();
            }
        },
        error: function () {
            response = "";
        }
    });
}

//function LoadFileUploadPartialView(divid, loadinpopup, controlid, folderpath, maxfileuploadsizeinmb, maxchunksizeinmb,
//                                    chunkintervalinsec, showdeleteaction, showattachmentbtn,
//                                    isdisabled, reloadafterupload, isrequired, actionurl, type, allowfileextension,
//                                    uploadsuccesscallbackfunction, loadcallbackfunction) {
//    if (controlid == undefined || controlid == null || controlid == "") {
//        DisplayNotiMessage("Error", "ControlId not define", "File Upload");
//        return;
//    }
//    //if (folderpath == undefined || folderpath == null || folderpath == "") {
//    //    DisplayNotiMessage("Error", "Folder Path Not Define", "File Upload");
//    //    return;
//    //}
//    if (uploadsuccesscallbackfunction === undefined || uploadsuccesscallbackfunction === null) {
//        uploadsuccesscallbackfunction = "";
//    }
//    $.ajax({
//        url: WebsiteURL + "/Utility/FileUpload/LoadFileUploadPartial",
//        type: "POST",
//        datatype: "html",
//        beforeSend: function () {
//            showLoading(true);
//        },
//        complete: function () {
//            showLoading(false);
//        },
//        data: {
//            controlid: controlid, folderpath: folderpath, maxfileuploadsizeinmb: maxfileuploadsizeinmb, maxchunksizeinmb: maxchunksizeinmb,
//            chunkintervalinsec: chunkintervalinsec, showdeleteaction: showdeleteaction, showattachmentbtn: showattachmentbtn,
//            isdisabled: isdisabled, reloadafterupload: reloadafterupload, isrequired: isrequired, actionurl: actionurl, type: type,
//            allowfileextension: allowfileextension, successcallbackfunction: uploadsuccesscallbackfunction
//        },
//        success: function (data) {
//            if (loadinpopup) {
//                bootbox.dialog({
//                    title: "File Upload",
//                    message: data,
//                    size: "large",
//                    buttons: {
//                        Cancel: {
//                            label: 'Close',
//                            className: 'btn red',
//                            callback: function () { }
//                        }
//                    }
//                });
//            }
//            else {
//                $("#" + divid).html(data);
//            }
//            if (loadcallbackfunction != null && loadcallbackfunction != undefined && typeof loadcallbackfunction === 'function') {
//                loadcallbackfunction();
//            }
//        },
//        error: function () {
//            response = "";
//        }
//    });
//}


function OpenProtocol(url, protocolType, protocolId) {

    $.ajax({
        type: "POST",
        url: WebsiteURL + "/Utility/General/ViewProtocolByPROD",
        data: { ProtocolType: protocolType, ProtocolId: protocolId },
        success: function (result) {
        },
        error: function () {
        }
    });

    window.open(url, "_blank");
}

function ConvertDDMMYYYYDateFormat(date) {
    // Opera 8.0+
    var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;

    // Firefox 1.0+
    var isFirefox = typeof InstallTrigger !== 'undefined';

    // Safari 3.0+ "[object HTMLElementConstructor]"
    var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification));

    // Internet Explorer 6-11
    var isIE = /*@cc_on!@*/false || !!document.documentMode;

    // Edge 20+
    var isEdge = !isIE && !!window.StyleMedia;

    // Chrome 1+
    var isChrome = !!window.chrome && !!window.chrome.webstore;

    // Blink engine detection
    var isBlink = (isChrome || isOpera) && !!window.CSS;
    var returndate = "";
    if (date.length > 0) {
        var splitDate = date.replace(new RegExp('-', 'g'), "/").split("/");

        if (isFirefox) {
            if (splitDate.length > 0) {
                returndate = splitDate[2] + "/" + splitDate[1] + "/" + splitDate[0];
            }
        }
        else if (isChrome) {
            if (splitDate.length > 0) {
                returndate = splitDate[2] + "/" + splitDate[1] + "/" + splitDate[0];
            }
        }
    }
    console.log(returndate);
    return returndate;
}

function DownloadAllDocuments(folderPath, Type, includeFromSubfolders) {
    bootbox.dialog({
        title: "Download Confirmation",
        message: 'Do you want to download all attachment?',
        size: "medium",
        buttons: {
            Yes: {
                label: 'Yes',
                className: 'btn btn-primary',
                callback: function () {
                    if (Type === undefined || Type == null) {
                        Type = "";
                    }
                    if (includeFromSubfolders === undefined || includeFromSubfolders == null) {
                        includeFromSubfolders = false;
                    }
                    window.open(WebsiteURL + '/Utility/FileUpload/DownloadZipFile?FolderPath=' + folderPath + '&Type=' + Type + '&includeFromSubfolders=' + includeFromSubfolders, "_blank");
                }
            },
            Cancel: {
                label: 'No',
                className: 'btn red',
                callback: function () { }
            }
        }
    });   
}

function DestroyAutoComplete(element) {
        $(element).autocomplete("destroy");
        $(element).removeData('autocomplete');
}

function FillAutoCompleteWithCallBack(element, sourceList, hdElement,fnCallback) {
    element.autocomplete({
        source: function (request, response) {
            var array = $.map(sourceList, function (item) {
                return {
                    label: item.CatDesc,
                    value: item.CatID,
                    id: item.CatID
                }
            });
            response($.ui.autocomplete.filter(array, request.term));
        },
        autoFocus: true,
        minLength: 0,
        select: function (event, ui) {
            event.preventDefault();
            $(this).val(ui.item.label);
            $(hdElement).val(ui.item.value);
            
            //if (fnCallback != null && fnCallback != undefined && fnCallback != 'undefined') {
            //    fnCallback();
            //}
        },
        change: function (event, ui) {
            if (ui.item == null) {
                element.val('');
                element.focus();
                $(hdElement).val('');
            }
            if (fnCallback != null && fnCallback != undefined && fnCallback != 'undefined') {
                fnCallback();
            }
        }
    }).focus(function () {
        $(this).data("uiAutocomplete").search($(this).val());
        $(this).autocomplete("search");
    });
}

function resetLocalStorage(url) {
    localStorage.setItem("selectedmenu", null);
    location.href = WebsiteURL + url;
}

function ConvertDDMMYYYYToYYYYMMDDDateFormat(date) {
    var returndate = "";
    var splitDate = date.replace(new RegExp('-', 'g'), "/").split("/");
    returndate = splitDate[2] + "/" + splitDate[1] + "/" + splitDate[0];
    return returndate;
}

function UpdateAnyTable(TableName, PrimaryId, PrimaryColumnName, UpdateColumnName, UpdateColumnValue, SetNullForBlank) {
    if (SetNullForBlank == undefined || SetNullForBlank == null) {
        SetNullForBlank = true;
    }
    $.ajax({
        url: WebsiteURL + "/Utility/General/UpdateAnyTable",
        type: "POST",
        datatype: "json",
        async: false,
        beforeSend: function () {
            showLoading(true);
        },
        complete: function () {
            showLoading(false);
        },
        data: { TableName: TableName, PrimaryId: parseInt(PrimaryId), PrimaryColumnName: PrimaryColumnName, UpdateColumnName: UpdateColumnName, UpdateColumnValue: UpdateColumnValue, SetNullForBlank: SetNullForBlank },
        success: function (data) {
        },
        error: function () {
            DisplayNotiMessage("error", "Error occured please try again", "Error");
        }
    });
}

function DDMMYYYYDateCompare(FromDate,ToDate) {
    var fdate = ConvertDDMMYYYYToYYYYMMDDDateFormat(FromDate)
    var tdate = ConvertDDMMYYYYToYYYYMMDDDateFormat(ToDate);
    if (fdate != "" && tdate != "") {
        var fnew = new Date(fdate);
        var tnew = new Date(tdate);
        if (fnew > tnew) {
            DisplayNotiMessage("info", "From date must be less than or equal to todate!", "Information")
            return false;
        }
        else {
            return true;
        }
    }
    else {
        return true;
    }
}

function LoadHTMLPartialView(url, divid, status) {
    $("#" + divid).empty();

    $.ajax({
        type: "POST",
        url: WebsiteURL + url,
        data: { status: status },
        datatype: 'html',
        beforeSend: function () {
            showLoading(true);
        },
        complete: function () {
            showLoading(false);
        },
        success: function (result) {
            $("#" + divid).html(result);
        },
        error: function () {
        }
    });
}

function LoadInlineGridTables(tableid, newbtnid, Columns, AjaxSource, parameterArray, extrafn) {
    var table = $('#' + tableid);
    var oTable = table.dataTable({
        "language": {
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "emptyTable": "No data available in table",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "infoEmpty": "No entries found",
            "infoFiltered": "(filtered1 from _MAX_ total entries)",
            "lengthMenu": "_MENU_ entries",
            "search": "Search:",
            "zeroRecords": "No matching records found"
        },
        "bProcessing": false,
        "bServerSide": true,
        "stateSave": false,
        buttons: [],
        "bDestroy": true,
        responsive: true,
        "order": [
            [0, 'asc']
        ],
        "lengthMenu": [
            [5, 10, 25, 50, 100],
            [5, 10, 25, 50, 100]
        ],
        "pageLength": 10,
        'iDisplayLength': 10,
        "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
        "sAjaxSource": WebsiteURL + AjaxSource + GetQueryStringParameterList(parameterArray),
        "fnServerParams": function (aoData) {
            aoData.push({ "name": "type", "value": "sdf" }
                        );
        },
        "fnServerData": function (sSource, aoData, fnCallback) {
            $.ajax({
                'dataType': 'json',
                'type': 'POST',
                'url': sSource,
                'data': aoData,
                'success': function (data) {
                    fnCallback(data);
                },
                'async': false
            });
            $('input[maxlength]').maxlength({
                alwaysShow: true,
            });
            ResetAddRowLine(tableid, newbtnid);
            SetControlNumeric();
            if (extrafn && (typeof extrafn == "function")) {
                extrafn();
            }
        },
        "aoColumns": Columns
    });
}

function LoadGridTables(tableid, Columns, AjaxSource, parameterArray, extrafn, rowDataCallBack) {
    var table = $('#' + tableid);
    var oTable = table.dataTable({
        "language": {
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "emptyTable": "No data available in table",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "infoEmpty": "No entries found",
            "infoFiltered": "(filtered1 from _MAX_ total entries)",
            "lengthMenu": "_MENU_ entries",
            "search": "Search:",
            "zeroRecords": "No matching records found"
        },
        "bProcessing": false,
        "bServerSide": true,
        "stateSave": false,
        buttons: [],
        "bDestroy": true,
        responsive: true,
        "order": [
            [0, 'asc']
        ],
        "lengthMenu": [
            [5, 10, 25, 50, 100],
            [5, 10, 25, 50, 100]
        ],
        "pageLength": 10,
        'iDisplayLength': 10,
        "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
        "sAjaxSource": WebsiteURL + AjaxSource + GetQueryStringParameterList(parameterArray),
        "fnServerParams": function (aoData) {
            aoData.push({ "name": "type", "value": "sdf" }
                        );
        },
        "fnServerData": function (sSource, aoData, fnCallback) {
            $.ajax({
                'dataType': 'json',
                'type': 'POST',
                'url': sSource,
                'data': aoData,
                'success': function (data) {
                    fnCallback(data);
                },
                'async': false
            });
            if (extrafn && (typeof extrafn == "function")) {
                extrafn(aoData);
            }
        },
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            if (rowDataCallBack && (typeof rowDataCallBack == "function")) {
                rowDataCallBack(nRow, aData);
            }
        },
        "aoColumns": Columns
    });
}

function GetQueryStringParameterList(list) {
    var q = "";
    for (var i = 0; i < parameterList.length; i++) {
        for (var j = 0; j < list.length; j++) {
            if (list[j] == parameterList[i]["id"]) {
                if (q.length == 0) {
                    q = "?" + parameterList[i]["id"] + "=" + parameterList[i]["value"];
                }
                else {
                    q = q + "&" + parameterList[i]["id"] + "='" + parameterList[i]["value"] + "'";
                }
            }
        }
    }
    //console.log(q);
    return q;
}

function FillAutoCompleteUsingAjax(element, url, hdElement, autoFocus, parameterArray, extrafn) {
    if (autoFocus == null || autoFocus == undefined || autoFocus == "undefined") {
        autoFocus = false;
    }
    if (parameterArray == null || parameterArray == undefined || parameterArray == "undefined") {
        parameterArray = [];
    }
    element.autocomplete({
        source: function (request, response) {
            $.ajax({
                url: WebsiteURL + url + GetExtraParameters(parameterArray),
                dataType: "json",
                type: "POST",
                delay: 250,
                async: false,
                data: {
                    term: request.term
                },
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item.Text, value: item.Text, id: item.Value };
                    }));
                    if (data.length > 0) {
                        isSelected = true;
                    }
                    else {
                        isSelected = false;
                    }
                }
            });
        },
        autoFocus: autoFocus,
        minLength: 0,
        select: function (event, ui) {
            $(this).val(ui.item.id);
            $(hdElement).val(ui.item.id);
            //HighlightInValidControls();
            if (extrafn && (typeof extrafn == "function")) {
                extrafn();
            }
        },
        change: function (event, ui) {
            if (ui.item == null) {
                $(hdElement).val("");
                element.val('');
                element.focus();
                if (extrafn && (typeof extrafn == "function")) {
                    extrafn();
                }
                //HighlightInValidControls();               
            }
        }
    }).focus(function () {
        $(this).data("uiAutocomplete").search($(this).val());
        $(this).autocomplete("search");
    });
}

function GetExtraParameters(list) {
    var q = "";

    for (var i = 0; i < list.length; i++) {
        if (q.length == 0) {
            q = "?" + list[i]["id"] + "=" + $(list[i]["value"]).val();
        }
        else {
            q = q + "&" + list[i]["id"] + "='" + $(list[i]["value"]).val() + "'";
        }
    }
    //console.log(q);
    return q;
}
function SaveLineData(formdata, URL, table, btnNew) {
    var tabledata = $("#" + table).find("input,select,textarea,hidden").serializeArray();
    if (tabledata != null && tabledata != undefined) {
        $.each(tabledata, function (key, input) {
            formdata.append(input.name, input.value);
        });
    }
    $.ajax({
        url: WebsiteURL + URL,
        type: "POST",
        data: formdata,
        async: false,
        contentType: false,
        processData: false,
        success: function (res) {
            if (res.Key) {
                DisplayNotiMessage("success", res.Value, "Success");
                $('#' + table).DataTable().ajax.reload();
                ShowNewLineRecord($('#' + btnNew));
            }
            else
                DisplayNotiMessage("error", res.Value, "Error");
        },
        error: function (data) { }
    });
}

function SetControlNumeric() {
    $(".numeric-only").autoNumeric('init', { aSep: '', vMax: "9999999.99" });
    $('input[maxlength],textarea[maxlength]').maxlength({
        alwaysShow: true,
    });
}


/*Start : Functions For Utility Grid*/

function SaveUtilityGridData(formdata, URL, table, UtilityParams) {

    $.ajax({
        url: WebsiteURL + URL,
        type: "POST",
        data: formdata,
        async: false,
        contentType: false,
        processData: false,
        success: function (res) {
            if (res.Key) {
                if (UtilityParams != "" && UtilityParams != null && UtilityParams != undefined) {
                    UtilityParams.dtReload();
                }
                DisplayNotiMessage("success", res.Value, "Success");
                if (table != "" && table != null && table != undefined) {
                    if ($('#btnNew_' + table).html() == '<i class="fa fa-times"></i> Cancel')
                        $('#btnNew_' + table).click();
                }
            }
            else {
                DisplayNotiMessage("error", res.Value, "Error");
                if (table != "" && table != null && table != undefined) {
                    $('#' + table).DataTable().columns.adjust();
                }
            }
        },
        error: function (data) { }
    });
}

function DeleteUtilityGridData(Id, URL, UtilityParams) {
    bootbox.dialog({
        message: "Do you want to delete?",
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success',
                callback: function () {
                    $.ajax({
                        url: WebsiteURL + URL,
                        type: "POST",
                        data: { Id: Id },
                        beforeSend: function () {
                            showLoading(true);
                        },
                        complete: function () {
                            showLoading(false);
                        },
                        success: function (data) {

                            if (data.Key == true) {
                                DisplayNotiMessage("success", data.Value, "Success");
                            }
                            else {
                                DisplayNotiMessage("error", data.Value, "Error");
                            }
                            if (UtilityParams != "" && UtilityParams != null && UtilityParams != undefined) {
                                UtilityParams.dtReload();
                            }
                        },
                        error: function (data) {
                            DisplayNotiMessage("error", data.Value, "Error");
                        }
                    });
                    bootbox.hideAll();
                    $(".modal-backdrop").remove()
                }
            },
            cancel: {
                label: 'No',
                className: 'btn-danger',
                callback: function () { }
            },
        }

    });
}
/*End : Functions For Utility Grid*/

function LoadHTMLPartialView(url, divid, status) {
    $("#" + divid).empty();

    $.ajax({
        type: "POST",
        url: WebsiteURL + url,
        data: { status: status },
        datatype: 'html',
        beforeSend: function () {
            showLoading(true);
        },
        complete: function () {
            showLoading(false);
        },
        success: function (result) {
            $("#" + divid).html(result);
        },
        error: function () {
        }
    });
}