﻿// ----------------------------------------------- //
// ---------------- How To Use ------------------- //
/*
 //* below css is only used for UI issue.
    <link href="~/Content/DataTable/fixedColumn.custom.dataTables.css" rel="stylesheet" />

 *  below js must be included after the datatable.min.js
    <script src="~/Scripts/DataTable/dataTables.custom.fixedColumn.js"></script>
    
 *  below function must be execute before datatable initialize
    var settings = {
        sTableID: 'tblTableID',     //Table id (must be pre init with dataTable() ) | Required
        iLeftCols: 3,               //Number of Columns from start to freeze        | Default: 0
        iRightCols: 1,              //Number of Columns from end to freeze          | Default: 0
        bIsWrap:false,              //Is table content wrap                         | Default:true
        bIsWrapHeader:false,        //Is table header wrap                          | Default:true
        bIsFirstRowHidden:true,     //Is table first row hidden                     | Default:false
        bIsFirstColumnCheckbox:true,//Is table first column checkbox                | Default:false
        bIsForceToSet:true,         //Is Ignore Hori. Scrollbar case                | Default:false
        bIsSearchFilter:true,       //Is SearchFilter implement                     | Default:false
        bIsIgnoreHiddenColumns:false,//Is count Hidden Columns for fixed column    | Default:true
        aColumnDefs =[              //Array of Column Name and Min-Width            | Default: []
        {
            sName: 'Column Title',  // Column name or Column Index                  | Required
            iMinWidth: 100          // only accept px                               | Required
        },
        {
            sName: '_DEF',          // Keyword to target All Columns                | Option Feature
            iMinWidth: 100          // only accept px                               | Required
        }, ......]
    }
    initDataTableFixedColumn(settings); 
    or
    $('#tblTableID').dtFixedColumn(settings);
    or 
    setDataTableFixedColumn('tblTableID', 3, 1, false, false, true, true, undefined, aColumnDefs)

    [Note]:
    =======

 *  it will only integrate with datatable table.

 *  To implement dynamically/automatic (without using initDataTableFixedColumn funtion) fixedColumn on exist datatable
    please add scrollX setting {"scrollX":true}
    Default setting { iLeftCols: 2, iRightCols: 1} 

 *  if your implement fixedColumn using initDataTableFixedColumn funtion 
    then there's no need to add {"scrollX":true}

 *  LeftCols & RightCols will only implement first time in datatable 
    if you want to change LeftCols or RightCols Columns 
    then you have to destroy table before re-execute initDataTableFixedColumn(settings)
    Example: 
        $('tblTableID').dataTable().destroy(); 
        initDataTableFixedColumn(settings)
 
 *   if your datatable has used/implemented responsive js 
    then you have to disable responsive setting { "responsive": false }

 *   bIsFirstColumnCheckbox flag only used for 
    If your grid containing first column as checkbox and it's in FixedColumn area
    and replace $("#TableID") to $("#TableID_wrapper") where you fetch checked checkbox value
    do you have checkbox on header for checked all checkbox? so please add this class or id example-select-all
        
 *  bIsFirstRowHidden flag only used for 
    find table first row and make it hide default
    if original table first row is dynamically change visiblity 
    then Fixed first row will change visiblity
    
 *   to set default width on each columns
    add column name "_DEF" and iMinWidth in aColumnDefs:[]
    Example
    settings = { .......,
        aColumnDefs = [              
        {
            sName: '_DEF',          // KeyWord for All Columns                      | Option Feature
            iMinWidth: 100          // only accept px                               | Required
        },......]
    }
    initDataTableFixedColumn(settings); 

 *  bIsForceToSet flag only used for 
    Force/Must to implement FixedColumn on load Datatable
    it will ignore a case of horizantal scrollbar visiblity checking

 *  to show/implement the FixedColumn effect on datatable
    your window screen width must be above 400px.
*/
// ----------------------------------------------- //

//$(document).ready(function () { 
//----- set default settings for each table (scrollX:true is required for FixedColumn) ----- //
// if you have set below setting on you datatable please remove it, so below setting will take a action.
$.extend($.fn.dataTable.defaults, {
    "scrollCollapse": true,
    // "colReorder": true,
    "autoWidth": false,
});
$(document).on('preInit.dt', function (e, settings) {
    // default/auto only implement when 1) table is not responsive, 2) datatable setting is set scrollX:true, 3) datatable is not implemented
    if (!$('#' + settings.sTableId).hasClass('dtr-inline collapsed') && settings.oScroll.sX && $('#' + settings.sTableId + "_wrapper").length == 0)
        initDataTableFixedColumn({ sTableID: settings.sTableId, iLeftCols: 2, iRightCols: 1, bIsSystemGenerated: true });
});
//}); 

// ----- Parameterized Function ----- //
function setDataTableFixedColumn(sTableID, iLeftCols, iRightCols, bIsWrap, bIsWrapHeader, bIsFirstRowHidden, bIsFirstColumnCheckbox, bIsForceToSet, aColumnDefs) {
    initDataTableFixedColumn({ sTableID: sTableID, iLeftCols: iLeftCols, iRightCols: iRightCols, bIsWrap: bIsWrap, bIsWrapHeader: bIsWrapHeader, bIsFirstRowHidden: bIsFirstRowHidden, bIsFirstColumnCheckbox: bIsFirstColumnCheckbox, bIsForceToSet: bIsForceToSet, aColumnDefs: aColumnDefs });
}

function initDataTableFixedColumn(Params) { // sTableID, iLeftCols, iRightCols, bIsWrap, bIsWrapHeader, bIsFirstRowHidden, bIsFirstColumnCheckbox, bIsForceToSet, aColumnDefs, bIsLadyLoading
    //console.log(Params);
    try {
        if (Params.sTableID == undefined || Params.sTableID == "") {
            console.log("dataTables.custom.fixedColumn.js \nTableID is missing in initDataTableFixedColumn({TableID:})")
            return;
        }
        if ($('#' + Params.sTableID).length < 1) {
            console.log("dataTables.custom.fixedColumn.js \n" + Params.sTableID + " table not found!")
            return;
        }
        // ----- Set Default value ----- //
        {
            Params.Wrapper = "#" + Params.sTableID + "_wrapper";
            if (Params.iLeftCols == undefined || Params.iLeftCols == "") {
                Params.iLeftCols = 0;
            }
            if (Params.iRightCols == undefined || Params.iRightCols == "") {
                Params.iRightCols = 0;
            }

            if (Params.aColumnDefs == undefined || Params.aColumnDefs == "") {
                Params.aColumnDefs = [];
            }

            if (Params.bIsIgnoreHiddenColumns == undefined) {
                Params.bIsIgnoreHiddenColumns = true;
            }

            if (Params.bIsWrap == undefined) {
                Params.bIsWrap = true;
            }

            if (Params.bIsWrapHeader == undefined) {
                Params.bIsWrapHeader = true;
            }

            if (Params.bIsFirstRowHidden == undefined) {
                Params.bIsFirstRowHidden = false;
            }

            if (Params.bIsFirstColumnCheckbox == undefined) {
                Params.bIsFirstColumnCheckbox = false;
            }

            if (Params.bIsForceToSet == undefined) {
                Params.bIsForceToSet = false;
            }

            if (Params.bIsSearchFilter == undefined) {
                Params.bIsSearchFilter = false;
            } 
        }

        //----- set default settings for each table (scrollX:true is required for FixedColumn) ----- //
        // if you have set below setting on you datatable please remove it, so below setting will take a action.
        if (Params.bIsSystemGenerated != true) { //skip required settings on auto system generated
            $.extend($.fn.dataTable.defaults, {
                "responsive": false,
                "scrollX": true,
                "scrollCollapse": true,
                //"colReorder": true,
                "autoWidth": false,
            });
        }

        if (IsBrowser("ie,edge,firefox"))
            $("#" + Params.sTableID).css("width", "99.9%");

        // ----- Set Column Min-Width ----- //
        var DefaultWidth = 0; // Params.aColumnDefs[Params.aColumnDefs.findIndex(x => x.sName=="_DEF")].iMinWidth; // findIndex() not supported in IE
        $.each(Params.aColumnDefs, function (idx, colitem) {
            if (colitem.sName == "_DEF") { // find default width and remove element
                DefaultWidth = colitem.iMinWidth;
                Params.aColumnDefs.pop(colitem);
            }
        })
        $("thead th", "#" + Params.sTableID + ", #" + Params.sTableID + "_wrapper").each(function (index, th) {
            for (var i in Params.aColumnDefs) {
                if (Params.aColumnDefs[i].sName == $(th).text().trim() || Params.aColumnDefs[i].sName == index) {
                    $(th).css("min-width", Params.aColumnDefs[i].iMinWidth + "px");
                    break;
                } else if (DefaultWidth > 0) {
                    $(th).css("min-width", DefaultWidth + "px");
                }
            }

            // ----- NoWrap CSS when filter icon is availabled in header ----- //
            if ($(th).find('.btngroup').length > 0) {
                $(th).css("white-space", "nowrap")
            }
        });

        $("#" + Params.sTableID).one('preInit.dt', function (e, settings) {
            showLoading(true);
        });

        $("#" + Params.sTableID).one('init.dt', function (e, settings, json) {
            setTimeout(function () {
                setFixedColumns(Params)
                if (!Params.bIsSearchFilter) {
                    showLoading(false);
                }
            }, 10);
        });
    } catch (e) {
        console.log(e);
    }
}

function setFixedColumns(Params) {
    //console.log(Params);
    try {
        // ----- for resolve dynamic responsive ui issue ----- //
        $(window).resize(function () {
            //setTimeout(setFixedColumns(Params), 1000); 
            //$('#' + Params.sTableID).dataTable().fnAdjustColumnSizing();
            $('#' + Params.sTableID).DataTable().columns.adjust()
        });
        injectFixedColumnCss(Params.sTableID, Params.bIsWrap, Params.bIsWrapHeader)
        // ----- only implement if window width is morethen 400px or table had horizantal scrollbar ----- //
        if (window.innerWidth > 400 && (Params.bIsForceToSet || $("#" + Params.sTableID + "_wrapper .dataTables_scrollBody").hasHScrollBar())) {
            //$('#' + Params.sTableID + "_wrapper").hide();

            // ----- First Column is checkbox ----- //
            if (Params.bIsFirstColumnCheckbox == true) {
                $("#" + Params.sTableID).DataTable().on('draw', function () {
                    setTimeout(function () {
                        // ----- Remove original first td content from table body ----- //
                        $('#' + Params.sTableID + '_wrapper .dataTables_scroll table tbody tr').find('td:first').html('')
                    }, 1);
                });
                setTimeout(function () {
                    $('#example-select-all,.example-select-all').on('click', function () {
                        var rows = $("#" + Params.sTableID + "_wrapper .DTFC_LeftBodyLiner table>tbody tr");
                        $('input[class="clsCheckbox"]', rows).prop('checked', this.checked);
                    });
                }, 1);
            }

            // ----- First Row with default hidden ----- //
            if (Params.bIsFirstRowHidden == true) {
                var DTFCFirstRow = $('#' + Params.sTableID + '_wrapper .DTFC_LeftBodyLiner table tbody tr:first, #' + Params.sTableID + '_wrapper .DTFC_RightBodyLiner table tbody tr:first');

                // hide first fixed row and adjust column size after DrawCallback/ServerResponse
                $("#" + Params.sTableID).DataTable().on('draw', function () {
                    setTimeout(function () {
                        DTFCFirstRow = $('#' + Params.sTableID + '_wrapper .DTFC_LeftBodyLiner table tbody tr:first, #' + Params.sTableID + '_wrapper .DTFC_RightBodyLiner table tbody tr:first');
                        DTFCFirstRow.hide();
                        DTFCFirstRow.css("background-color", "#fbffb2");
                        $('input[type!="hidden"]', DTFCFirstRow).val('');

                        // ----- remove original all td content which is in FixedColumn area ----- //
                        var cntLeftCol = $('#' + Params.sTableID + '_wrapper .DTFC_LeftBodyLiner table tbody tr:first').find("td").length;
                        var cntRightCol = $('#' + Params.sTableID + '_wrapper .DTFC_RightBodyLiner table tbody tr:first').find("td").length;
                        var cntTotalCol = $('#' + Params.sTableID + '_wrapper .dataTables_scroll table tbody tr:first').find("td").length;
                        for (var i = 0; i < cntLeftCol; i++) {
                            $('#' + Params.sTableID + '_wrapper .dataTables_scroll table tbody tr').find('td:eq(' + i + ')').html('');
                        }
                        for (var i = cntTotalCol - 1 ; i >= cntTotalCol - cntRightCol; i--) {
                            $('#' + Params.sTableID + '_wrapper .dataTables_scroll table tbody tr').find('td:eq(' + i + ')').html('');
                        }

                        $("#" + Params.sTableID).DataTable().columns.adjust()
                    }, 1);
                });

                // ----- detect first row is visible and set FixedColumn visiblity ------ //
                setInterval(function () {
                    if ($("#" + Params.sTableID).find("tbody tr:first").is(':visible')) {
                        $(DTFCFirstRow).show();
                        $("#" + Params.sTableID).DataTable().columns.adjust()
                        setTimeout(function () { // this will work for after hide
                            $("#" + Params.sTableID).DataTable().columns.adjust()
                        }, 1000)
                    } else {
                        DTFCFirstRow.hide();
                    }
                }, 100);
            }

            // ----- Find Hidden Columns and increse Cols length ----- //
            if (Params.bIsIgnoreHiddenColumns == true) {
                var tableCols = $('#' + Params.sTableID).DataTable().column;
                var cntTableCols = $('#' + Params.sTableID).DataTable().columns()[0].length;
                for (var i = 0; i < Params.iLeftCols; i++) {
                    if ($(tableCols(i).header()).hasClass('hide') || !tableCols(i).visible())
                        Params.iLeftCols++;
                }
                for (var i = cntTableCols - 1; i >= cntTableCols - Params.iRightCols; i--) {
                    if ($(tableCols(i).header()).hasClass('hide') || !tableCols(i).visible())
                        Params.iRightCols++;
                }
            }

            // ----- Set FixedColumns setting ----- //
            try {
                var fixedColumns = new $.fn.dataTable.FixedColumns($('#' + Params.sTableID), {
                    iLeftColumns: Params.iLeftCols,
                    iRightColumns: Params.iRightCols,
                });
                fixedColumns.fnRedrawLayout();
            } catch (e) {
                //this setting can set only one time if you want change it then you have to destroy your old table
                //console.log(e)
            }

            // ----- remove original search boxs and initalize event ----- //
            FixedTableSearchIntialze(Params.sTableID);

            //$('#' + Params.sTableID).dataTable().fnAdjustColumnSizing();
            $('#' + Params.sTableID).DataTable().columns.adjust();
            //$($.fn.dataTable.tables(true)).DataTable().scroller.measure();

            // ----- Fixed Columns by Name ----- //
            // only work when table data is not fetching by ajax/server 
            /*if (Params.LeftColumns || Params.RightColumns) {
                debugger;
                colreorder = [];
                if (!Params.LeftColumns) {
                    Params.LeftColumns = [];
                }
                if (!Params.RightColumns) {
                    Params.RightColumns = [];
                }
                var tableCols = $('#' + Params.sTableID).DataTable().column;
    
                // define Left Columns
                if (Params.LeftColumns) {
                    $.each(Params.LeftColumns, function (index, item) {
                        colreorder.push( tableCols(item + ":name").index());
                    })
                }
    
                $.each($('#' + Params.sTableID).DataTable().columns()[0], function (index, item) {
                    if (Params.LeftColumns.indexOf($(tableCols(item).header()).text().trim()) == -1 && Params.RightColumns.indexOf($(tableCols(item).header()).text().trim()) == -1) {
                        colreorder.push(tableCols(item).index());
                    }
                })
    
                if (Params.RightColumns) {
                    $.each(Params.RightColumns, function (index, item) {
                        colreorder.push(tableCols(item + ":name").index());
                    })
                }
    
                $('#' + Params.sTableID).DataTable().colReorder.order(colreorder);
                //console.log(colreorder);
    
            }*/
           
        }
        else {
            //$('#' + Params.sTableID + "_wrapper").parent().prepend("<style> " + '#' + Params.sTableID + "_wrapper .table-scrollable {border:unset} </style>");
            $('#' + Params.sTableID + "_wrapper .table-scrollable").css("border", "unset"); // remove extra border
            $('#' + Params.sTableID + "_wrapper .dataTables_scrollBody table.dataTable.no-footer").css("border-bottom", "1px solid #111"); // remove extra border
            $('#' + Params.sTableID + "_wrapper .dataTables_scrollBody table").css("margin", "unset"); // for wide screen column width issue
            if (Params.bIsSearchFilter) {
                Params.iLeftCols = 0
                Params.iRightCols = 0
            }
        }
        if (Params.bIsSearchFilter) {
            setTimeout(function () {
                InitSearchFilterForFixedColumn(Params) 
            }, Params.bIsLadyLoading ? 1000 : 0)
        }
    } catch (e) {
        console.log(e);
    }
    // ----- for dynamic datatable loading ----- //
    setTimeout(function () {
        $('#' + Params.sTableID + "_wrapper .table-scrollable").css("border", "unset"); // remove extra border
        $('#' + Params.sTableID + "_wrapper .dataTables_scrollBody table").css("margin", "unset"); // for wide screen column width issue
        //$('#' + Params.sTableID).dataTable().fnAdjustColumnSizing();
        $('#' + Params.sTableID).DataTable().columns.adjust();
    }, 10)

}

function FixedTableSearchIntialze(tableid) {
    setTimeout(function () {
        // ----- Remove original textbox or any eleements from table header (that column must have id)  ----- //
        $('.DTFC_LeftHeadWrapper,.DTFC_RightHeadWrapper').find("table thead tr:first th").each(function () {
            var id = $(this).children(0).attr("id");
            $('.dataTables_scroll').find('#' + id).first().remove()
        });

        // ----- reduce number of sending filter request on search ----- //
        /*var isTyping = false;
        $('#' + tableid + '_wrapper thead input').keyup(function () {
            isTyping = true;
            setTimeout(function () {
                if (isTyping) {
                    //console.log("typing")
                    isTyping = false; // stop typing for moment
                    setTimeout(function () {
                        if (!isTyping) { // check after moment
                            isTyping = true; // ignor mutiple threads
                            //console.log("filter request send")
                            $('#' + tableid).DataTable().ajax.reload();
                        }
                    }, 1000);
                }
            }, 500);
        });*/

    }, 1000);
}

// ----- only for inline editable grid ----- //
/*function setValueFromFixedColumns(sTableID) {
    $("#" + sTableID + "_wrapper .DTFC_LeftBodyLiner table, #" + sTableID + "_wrapper .DTFC_RightBodyLiner table").find("tbody tr:first td").each(function () {
        var id = $(this).children(0).attr("id");
        var value = $(this).children(0).val();
        $('#' + sTableID).find("tbody tr:first td").each(function () {
            var idMain = $(this).children(0).attr("id");
            if (idMain == id) {
                $(this).children(0).attr("name", "temp_" + idMain);
                $(this).children(0).val(value);
            }
        });
    });
}*/


// ----- add style css for UI issue. ----- //
function injectFixedColumnCss(sTableID, bIsWrap, bIsWrapHeader) {
    var tableStyle = "";
    TableID = "#" + sTableID + "_wrapper";
    if ($("#" + sTableID + "_style").length > 0) {
        return;
    }

    // ----- remove spaces between header and body container ----- //
    tableStyle += TableID + " table.dataTable { margin-top: 0px !important; margin-bottom: 0px !important; } \n";
    // ----- set middle Vertical align ----- */
    tableStyle += TableID + " .DTFC_LeftBodyLiner table td, " + TableID + " .DTFC_RightBodyLiner table td { vertical-align: middle !important; } \n";
    // ----- remove bottom horizantal scrollbar for FixedColumn div ----- //
    tableStyle += TableID + " .DTFC_LeftBodyLiner, " + TableID + " .DTFC_RightBodyLiner {overflow-y: initial !important;  overflow-x: hidden;} \n";
    // ----- Add Right side border line----- //
    tableStyle += TableID + " .DTFC_LeftHeadWrapper table tr:not(.mycustomfilter) th, " + TableID + " .DTFC_RightHeadWrapper table  tr:not(.mycustomfilter) th { background: linear-gradient(rgba(0,0,0,0),rgba(163, 163, 163, 0.42)); } \n";
    tableStyle += TableID + " table.dataTable thead th, " + TableID + " table.dataTable thead td { border-bottom: unset; } \n";

    // ----- remove bottom margin for FixedColumn div ----- //
    tableStyle += TableID + " .DTFC_ScrollWrapper, " + TableID + " .dataTables_scroll { margin-bottom: unset; }";
    // ----- remove sorting icon on Search Filter columns ----- //
    tableStyle += "table.dataTable thead .mycustomfilter .sorting:after,table.dataTable thead .mycustomfilter .sorting_asc:after,table.dataTable thead .mycustomfilter .sorting_desc:after { opacity: 0.0; } ";
    // ----- Set No Wrap Table Content ----- //
    if (bIsWrap == false)
        tableStyle += TableID + " td { white-space:nowrap; } \n";
    if (bIsWrapHeader == false)
        tableStyle += TableID + " th { white-space:nowrap; } \n";
   
    // ----- remove footer extra scrollbar for ms edge/firefox----- //
    //if (IsBrowser("ie,edge,firefox"))
    tableStyle += TableID + " .table-scrollable { overflow:hidden; } \n";
    // inject Css
    $(TableID).parent().prepend("<style id='" + sTableID + "_style'>" + tableStyle + "</style>");
}

// ----- Fetch Parameter value from script src ----- //
/*function getParams(script_name) {
    // Find all script tags
    var scripts = document.getElementsByTagName("script");

    // Look through them trying to find ourselves

    for (var i = 0; i < scripts.length; i++) {
        if (scripts[i].src.indexOf("/" + script_name) > -1) {
            // Get an array of key=value strings of params

            var pa = scripts[i].src.split("?").pop().split("&");

            // Split each key=value into array, the construct js object

            var p = {};
            for (var j = 0; j < pa.length; j++) {
                var kv = pa[j].split("=");
                p[kv[0]] = kv[1];
            }
            return p;
        }
    }

    // No scripts match
    return {};
}*/

// ----- Detect/Check Horiz Scrollbar is visibled ----- //
jQuery.fn.hasHScrollBar = function () {
    try {
        if (IsBrowser("ie,edge,firefox")) //
            return this.get(0).scrollWidth - 1 > this.innerWidth();
        return this.get(0).scrollWidth > this.innerWidth();
    } catch (e) {
        console.log(e);
        return false;
    }
}

jQuery.fn.dtFixedColumn = function (Params) {
    try {
        Params.sTableID = this[0].id;
        initDataTableFixedColumn(Params)
        return this;
    } catch (e) {
        console.log(e);
        return false;
    }
}

// ----- Detect/Check Current Running Browser by Browser Name ----- //
function IsBrowser(sBrowserNames) {
    var isBrowser = false;
    var aBrowserNames = sBrowserNames.toLowerCase().split(',');
    for (var i in aBrowserNames) {
        switch (aBrowserNames[i]) {
            case "opera":
                // Opera 8.0+
                isBrowser = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
                break;
            case "firefox":
                // Firefox 1.0+
                isBrowser = typeof InstallTrigger !== 'undefined';
                break;
            case "safari":
                // Safari 3.0+ "[object HTMLElementConstructor]" 
                isBrowser = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification));
                break;
            case "ie":
                // Internet Explorer 6-11
                isBrowser = /*@cc_on!@*/false || !!document.documentMode;
                break;
            case "edge":
                // Edge 20+
                isBrowser = !(false || !!document.documentMode) && !!window.StyleMedia;
                break;
            case "chrome":
                // Chrome 1+
                isBrowser = !!window.chrome && !!window.chrome.webstore;
                break;
                /*case "blink":
                    // Blink engine detection
                    isBrowser = (isChrome || isOpera) && !!window.CSS;*/
        }
        if (isBrowser)
            break; // true
    };

    return isBrowser; // false
}

function InitSearchFilterForFixedColumn(Params) {
    $('#' + Params.sTableID + "_wrapper .dataTables_scrollHeadInner table").attr('id', Params.sTableID + '_scrollHeadInner');

    $('#' + Params.sTableID).on('preXhr.dt', function (e, settings, data) {
        data.SearchFilter = SetDtServerParameterForFixedColumn(Params.sTableID, []);
    })

    setTimeout(function () {
        $('#' + Params.sTableID + " thead tr:eq(1) th").each(function (idx, th) {
            if (!$(th).is(":visible"))
                $('#' + Params.sTableID + "_wrapper thead tr:first th:eq(" + idx + ")").hide();
        });
        $('#' + Params.sTableID).DataTable().columns.adjust()
    }, 100);

    $('#' + Params.sTableID).on('responsive-resize.dt', function (e, datatable, columns) {
        $.each(columns, function (idx, visiblity) {
            if (visiblity == true)
                $('#' + Params.sTableID + "_wrapper thead tr:first th:eq(" + idx + ")").show();
            else
                $('#' + Params.sTableID + "_wrapper thead tr:first th:eq(" + idx + ")").hide();
        })
        $('#' + Params.sTableID).DataTable().columns.adjust()
    });

    $('#' + Params.sTableID).trigger('resize');
    GenerateGridControlforFixedColumn(Params)
    BindFilterTypeforFixedColumn(Params.sTableID);
    InitEventSearch(Params.sTableID);
    InitClearSearchTextForFixedColumn(Params.sTableID);
    showLoading(false);
}


function GenerateGridControlforFixedColumn(Params) {
    var FltType = "flt";
    var tablegrid = $('#' + Params.sTableID).DataTable();
    var columns = tablegrid.settings().init().columns;
    if (!columns)
        return
    tableTRContent = "";
    tableLFixTRContent = "";
    tableRFixTRContent = "";

    tablegrid.columns().every(function (index) {
        var tableTHContent = "";
        var ColName = (columns[index].name);
        var ColType = (columns[index].attr);
        var ColClass = (columns[index].sClass) == undefined ? "" : (columns[index].sClass);

        if (ColType == undefined) { // Default
            ColType = "string";
            //columns[index].attr = "string";
        }
        if (ColType != "" && ColType != false && ColClass.indexOf("hide") == -1 && ColName != undefined) {
            Fltcross = Fltcross + 1;
            if (ColType == "string") {
                classname = "all";
                tableTHContent = "<th  style = 'min-width:100px; padding: 8px 2px !important;' ><div class='all fontwid' id='fltall" + ColName + "' style='display: inline-flex;'><div class='input-icon right'><i id='fltcloseIcon_img" + Fltcross + "' data-attr='" + ColName + "'  class='fa fa-close hidden clscloseIcon' style='cursor:pointer'></i><input type='text' data-attType='" + ColType + "' id='" + FltType + "" + ColName + "' name=flt" + ColName + "' style = 'padding-left: 2px !important;' class='form-control col-md-3'  data-flttype='" + Contains + "'/></div></div></th>"
            }
            else if (ColType == "numeric") {
                classname = "all";
                tableTHContent = "<th style='min-width:100px; padding: 8px 2px !important;' ><div class='all fontwid' id='fltallnumeric" + ColName + "' style='display: inline-flex;'><div class='input-icon right'><i id='fltcloseIcon_imgother" + Fltcross + "' class='fa fa-close hidden clscloseIcon' style='cursor:pointer'></i><input type='text' data-attType='" + ColType + "' id='" + FltType + "" + ColName + "' name=flt" + ColName + "'style = 'padding-left: 2px !important;' class='numeric form-control col-md-3'  data-flttype='" + Equal + "'/></div></div></th>"
            }
            else if (ColType == "date") {
                classname = "center";
                tableTHContent = "<th style = 'min-width:100px; padding: 8px 2px !important;'  ><div class='all fontwid' id='fltall" + ColName + "' style='display: inline-flex;'><div class='input-icon right'><i id='fltcloseIcon_imgother" + Fltcross + "' class='fa fa-close hidden clscloseIcon' style='cursor:pointer'></i><input type='date' data-attType='" + ColType + "' id='" + FltType + "" + ColName + "' name=flt" + ColName + "' style = 'padding-left: 2px !important;' class='form-control col-md-3'  data-flttype='" + Equal + "'/></div></div></th>"
            }
            else if (ColType == "dropdown") {
                classname = "center";
                tableTHContent = "<th  style = 'min-width:100px; padding: 8px 2px !important;' ><div class='all fontwid' id='fltall" + ColName + "' style='display: inline-flex;'><div class='input-icon right'><i id='fltcloseIcon_imgother" + Fltcross + "' class='fa fa-close hidden clscloseIcon' style='cursor:pointer'></i><select id='" + FltType + "" + ColName + "'  name=flt" + ColName + "' data-attType='" + ColType + "' style = 'padding-left: 2px !important;' class='form-control col-md-3'  data-flttype='" + Equal + "'><option value=''>Select</option> <option value='1'>Yes</option><option value='0'>No</option></select></div></div></th>"
            }
            else if (ColType == "multiple") {
                tableTHContent = "<th style = 'min-width:100px; padding: 8px 2px !important;' ><div class='all fontwid' id='fltall" + ColName + "' style='display: inline-flex;'><div class='input-icon right'><i id='fltcloseIcon_img" + Fltcross + "' class='fa fa-close hidden clscloseIcon' style='cursor:pointer'></i><select name='flt" + ColName + "' data-name='ddlmultiple' data-attType='" + ColType + "' id='" + FltType + "" + ColName + "' style = 'padding-left: 2px !important;'  class='form-control multiselect-drodown'  data-area='Roles' multiple='multiple' data-flttype='" + Equal + "' ></select></div></div></th>"
            }
            else
                tableTHContent = "<th class='min-phone-l'></th>"
        }
        else {
            if (ColClass.indexOf("hide") != -1) {
                tableTHContent = "<th class='min-phone-l hide'></th>"
            }
            else if ((Table_status == "All" && ColClass == "left18") || ColType == "s") {
                tableTHContent = "<th class='min-phone-l' style='display:none;'></th>"
            }
            else {
                classname = "min-phone-l";
                tableTHContent = "<th class='min-phone-l' ></th>"
            }
        }
        if (columns.length - Params.iRightCols -1 < index) {
            tableRFixTRContent += tableTHContent;
            tableTHContent = $(tableTHContent).html('')[0].outerHTML;
        }
        tableTRContent += tableTHContent;
        if (Params.iLeftCols - 1 == index) {
            tableLFixTRContent = tableTRContent;
            temptableTRContent = "";
            $(tableTRContent).html('').each(function (idx, item) {
                temptableTRContent += item.outerHTML;
            });
            tableTRContent = temptableTRContent;
        }
    });

    if ($('#' + Params.sTableID + '_scrollHeadInner').length == 0 && $('#' + Params.sTableID + ' > thead:first tr.mycustomfilter').length == 0) {
        $('#' + Params.sTableID + ' > thead:first ').prepend("<tr class='mycustomfilter'>" + tableTRContent + "</tr>");
    } else if ($('#' + Params.sTableID + '_scrollHeadInner > thead:first tr.mycustomfilter').length == 0) {
        $('#' + Params.sTableID + '_scrollHeadInner  > thead:first ').prepend("<tr class='mycustomfilter'>" + tableTRContent + "</tr>");
        $('.DTFC_LeftHeadWrapper > table  > thead:first', '#' + Params.sTableID + '_wrapper')
            .prepend("<tr class='mycustomfilter'>" + tableLFixTRContent + "</tr>");
        $('.DTFC_RightHeadWrapper > table  > thead:first', '#' + Params.sTableID + '_wrapper')
            .prepend("<tr class='mycustomfilter'>" + tableRFixTRContent + "</tr>");
        
        // ----- on keypress [TAB] from fixed last column, set focus to next searchbox in grid ----- //
        $('.DTFC_LeftHeadWrapper > table  > thead:first > tr.mycustomfilter > th:last', '#' + Params.sTableID + '_wrapper').on("keydown", function (e) {
            if (e.keyCode == 9) {
                e.preventDefault();
                $('#' + Params.sTableID + '_scrollHeadInner  > thead:first > tr.mycustomfilter > th:eq(' + ($(this).index() + 1) + ')').find("input,select").focus();
            }
        })
        //$('.DTFC_RightHeadWrapper > table  > thead:first > th', '#' + Params.sTableID + '_wrapper').removeClass(".sorting")
    }

    $.each($("#" + Params.sTableID).DataTable().settings().init().columns, function (idx, col) {
        if (typeof (col.ColData) === "string") {
            LoadPreValueSelect2(col.ColData, $('#' + FltType + col.name, '#' + Params.sTableID), "Search..", '')
        } else if (col.ColData != undefined) {
            FillInlineAutoComplete($('#' + FltType + col.name, '#' + Params.sTableID), col.ColData)
        }
    });

}
function BindFilterTypeforFixedColumn(table_id) {

    var tablegrid = $('#' + table_id).DataTable();
    var columns = tablegrid.settings().init().columns;
    if (!columns)
        return;
    string = "";
    table_id += "_wrapper";

    tablegrid.columns().every(function (index) {

        var Colvalue = (columns[index].name);
        var Colattr = (columns[index].attr) == undefined ? "string" : (columns[index].attr);
        var Colatts = (columns[index].sClass);
        if (Colvalue != undefined && Colattr != "" && Colattr != false && Colatts != "hide") {
            type = ($(this).find("input").attr("data-atttype"));
            typeothers = ($(this).find("select").attr("data-atttype"));
            if (Colattr == "string") {
                var htmlstring = Filtervalue(Colvalue, table_id);
                if ($('#' + table_id).find('#fltall' + Colvalue + ' > div.btngroup').length == 0) {
                    $("#fltall" + Colvalue, '#' + table_id).prepend(htmlstring);
                }
            }
            else if (Colattr == "date") {
                var htmlstringother = Filtervalueother(Colvalue, table_id);
                if ($('#' + table_id).find('#fltall' + Colvalue + ' > div.btngroup').length == 0) {
                    $("#fltall" + Colvalue, '#' + table_id).prepend(htmlstringother);
                }
            }
            else if (Colattr == "dropdown") {
            }
            else if (Colattr == "numeric") {
                var htmlstringother = Filtervalueother(Colvalue, table_id);
                if ($('#' + table_id).find('#fltallnumeric' + Colvalue + ' > div.btngroup').length == 0) {
                    $("#fltallnumeric" + Colvalue, '#' + table_id).prepend(htmlstringother);
                }
            }
            else if (typeothers == "string" || typeothers == undefined) {
                var htmlstring = Filtervalue(Colvalue, table_id);
                if ($('#' + table_id).find('#fltall' + Colvalue + ' > div.btngroup').length == 0) {
                    $("#fltall" + Colvalue, '#' + table_id).prepend(htmlstring);
                }
            }
        }

    });
    // To set Fixed Column height same as main row hieght
    setTimeout(function () {
        var MainTrHeight = $("thead tr.mycustomfilter:eq(0)", "#" + table_id).css('height');
        $(".DTFC_LeftHeadWrapper", "#" + table_id).find("thead tr.mycustomfilter").css("height", MainTrHeight);
        $(".DTFC_RightHeadWrapper", "#" + table_id).find("thead tr.mycustomfilter").css("height", MainTrHeight);
    },10)
}


function SetDtServerParameterForFixedColumn(tableid, aoData) {

    try {
        SearchFilter = [];
        $.each($("#" + tableid).DataTable().settings().init().columns, function (idx, col) {
            var ColTitle = (col.name);
            var Colattr = (col.attr);
            var Colname = (col.ColName != undefined && col.ColName != "") ? col.ColName : ColTitle;
            var ColValue = $("#flt" + ColTitle, "#" + tableid + "_wrapper").val();
            var FilterType = $("#flt" + ColTitle, "#" + tableid + "_wrapper").attr('data-flttype');
            if (Colname != undefined && Colname != "" && ColValue != undefined && ColValue != "" && Colattr != "" && Colattr != false) {
                if (typeof (ColValue) === "object")
                    aoData.push({ "name": Colname, "value": ColValue });
                else
                    SearchFilter.push({ "ColumnName": Colname, "Value": ColValue, "FilterType": FilterType, "DataType": Colattr == undefined ? "string" : Colattr });
            } else if (Colname != undefined && Colname != "" && (FilterType == "Is Empty" || FilterType == "Is Not Empty") && Colattr != "" && Colattr != false) { // Apply filter without seachbox
                SearchFilter.push({ "ColumnName": Colname, "Value": "-", "FilterType": FilterType, "DataType": Colattr == undefined ? "string" : Colattr });
                $("#flt" + ColTitle, "#" + tableid + "_wrapper").siblings('i').removeClass("hidden");
            }
        });
        aoData.push({ "name": "SearchFilter", "value": JSON.stringify(SearchFilter) });
        return JSON.stringify(SearchFilter);
    } catch (e) {
        console.log(e);
    }
}

function InitClearSearchTextForFixedColumn(table_id) {

    $("#" + table_id + "_wrapper .clscloseIcon").on("click", function () {
        var Atttype = ($(this).siblings("input").attr("data-atttype"));
        var Imageicon = $(this).attr('id').split("_").pop();
        $('#' + Imageicon + '').attr("src", ImagePathurl((Atttype != "numeric" && Atttype != "date") ? "Contains" : "Equal"));

        $(this).siblings("input").attr("data-flttype", '')
        Filterstatus = "";
        FilterTitle = "";
        $(this).addClass('hidden');
        $(this).siblings('input').val('');
        $("#" + table_id).DataTable().ajax.reload();
    });
}