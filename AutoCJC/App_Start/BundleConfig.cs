﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace AutoCJC
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            #region Style Bundle

            bundles.Add(new StyleBundle("~/Content/css").Include(
                                        "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/bootstrap-switch/css").Include(
                                        "~/Content/ThemeCss/bootstrap-switch/css/bootstrap-switch.css"));

            bundles.Add(new StyleBundle("~/Content/select2/css").Include(
                                        "~/Content/ThemeCss/select2/css/select2.min.css",
                                        "~/Content/ThemeCss/select2/css/select2-bootstrap.min.css"));

            bundles.Add(new StyleBundle("~/Content/Theme/css").Include(
                                        "~/Content/ThemeCss/Css/components.min.css",
                                        "~/Content/ThemeCss/Css/plugins.min.css"));

            bundles.Add(new StyleBundle("~/Content/style").Include("~/Content/ThemeCss/style.css", new CssRewriteUrlTransform()));

            bundles.Add(new StyleBundle("~/Content/ThemeCssUI/css").Include("~/Content/jquery-ui.min.css"));

            bundles.Add(new StyleBundle("~/Content/Datatable/css").Include(
                                        "~/Content/ThemeCss/Datatable/datatables.min.css",
                                        "~/Content/ThemeCss/Datatable/datatables.bootstrap.css",
                                        "~/Content/ThemeCss/select2/css/select2-bootstrap.min.css"
                                        ));



            bundles.Add(new StyleBundle("~/Content/Common/css").Include(
                                        "~/Content/ThemeCss/toastr.min.css",
                                        "~/Content/ThemeCss/layoutCss/custom.css",
                                        "~/Content/ThemeCss/layoutCss/custom.min.css"));
            bundles.Add(new StyleBundle("~/Content/datetimepicker/css").Include(
                                      "~/Content/DateTimePicker/clockface.css",
                                      "~/Content/DateTimePicker/bootstrap-datetimepicker.min.css",
                                      "~/Content/DateTimePicker/bootstrap-timepicker.min.css",
                                      "~/Content/DateTimePicker/bootstrap-datepicker3.min.css",
                                      "~/Content/DateTimePicker/daterangepicker.min.css"
                                  ));
            #endregion

            #region Script Bundle

            bundles.Add(new ScriptBundle("~/bundles/datatable/js").Include(
                                     "~/Scripts/ThemeScripts/Datatable/datatable.js",
                                     "~/Scripts/ThemeScripts/Datatable/datatables.min.js",
                                     "~/Scripts/ThemeScripts/Datatable/datatables.bootstrap.js",
                                     "~/Scripts/ThemeScripts/DataTable/dataTables.custom.fixedColumn.js",
                                     "~/Scripts/ThemeScripts/DataTable/dataTables.custom.utility.js",
                                     "~/Scripts/ThemeScripts/DataTable/dataTables.buttons.min.js",
                                     "~/Scripts/ThemeScripts/DataTable/jszip.min.js",
                                     "~/Scripts/ThemeScripts/DataTable/buttons.html5.min.js",
                                     "~/Scripts/ThemeScripts/DataTable/Column-Filter.js"
                                     ));

        bundles.Add(new ScriptBundle("~/bundles/datetimepicker/js").Include(
                                    "~/Scripts/DatetimePicker/moment.min.js",
                                    "~/Scripts/DatetimePicker/bootstrap-datepicker.min.js",
                                    "~/Scripts/DatetimePicker/bootstrap-timepicker.min.js",
                                    "~/Scripts/DatetimePicker/bootstrap-datetimepicker.min.js",
                                    "~/Scripts/DatetimePicker/clockface.js",
                                    "~/Scripts/DatetimePicker/components-date-time-pickers.min.js"
                                    ));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include("~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include("~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include("~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                                        "~/Scripts/bootstrap.js",
                                        "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap/js").Include(
                                       "~/Scripts/ThemeScripts/Plugins/bootstrap/bootstrap.min.js",
                                       "~/Scripts/bootstrap-maxlength.js"));

            bundles.Add(new ScriptBundle("~/bundles/ThemeJs/js").Include("~/Scripts/ThemeScripts/Plugins/jquery.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/ThemeJsUI/js").Include("~/Scripts/jquery-ui.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jquery-validation/js").Include(
                                         "~/Scripts/ThemeScripts/Plugins/jquery-validation/jquery.validate.min.js",
                                         "~/Scripts/ThemeScripts/Plugins/jquery-validation/additional-methods.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/Common/js").Include(
                                        "~/Scripts/ThemeScripts/toastr.min.js",
                                        "~/Scripts/Main.js",
                                        "~/Scripts/common.js",
                                        "~/Scripts/autoNumeric-1.9.41.js"));

            bundles.Add(new ScriptBundle("~/bundles/CommonFooter/js").Include(
                                        "~/Scripts/ThemeScripts/app.min.js",
                                        "~/Scripts/ThemeScripts/layout.js",
                                        "~/Scripts/ThemeScripts/demo.js",
                                        "~/Scripts/ThemeScripts/quick-sidebar.js",
                                        "~/Scripts/ThemeScripts/quick-nav.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrapmultiselect/js").Include(
                "~/Scripts/ThemeScripts/app.min.js",
                "~/Scripts/bootbox.js",
                "~/Scripts/ThemeScripts/jquery-validate.bootstrap-tooltip.js"));

            #endregion
        }
    }
}