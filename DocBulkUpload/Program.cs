﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.IO;
using System.Net.Mail;
using System.Configuration;
using System.Net;

namespace DocBulkUpload
{
    class Program
    {
        SqlConnection cn;
        string strDocPathInsert = "", strDocInsert = "", strUpdateFlag = "", strInsertPolicy = "", strUpdateDocumentIdInJEP = "";
        static void Main(string[] args)
        {
            Program p = new Program();
            p.ExecuteUploadProcedure();
        }
        class JEPModel
        {
            public string JepNo { get; set; }
            public string Message { get; set; }
            public string DocNo { get; set; }
            public string CreatedBy { get; set; }
        }


        public bool FileTransfer(string NetworkPath, string FileName, string DestFolder, out string DestPath, out string ErrorMsg)
        {
            try
            {

                var v = new Uri(NetworkPath);
                string strTemp = "Select UserId,UserPassword from DES104 WHERE ConfigValues='" + v.Host + "'";
                SqlDataAdapter daTempData = new SqlDataAdapter(SQLfunctions.GetCommandFromSQL(strTemp, SQLfunctions.GetConnection()));
                DataTable dtTempData = new DataTable();
                daTempData.Fill(dtTempData);
                if (dtTempData != null && dtTempData.Rows.Count > 0)
                {
                    string UserId = dtTempData.Rows[0]["UserId"].ToString();
                    string UserPassword = dtTempData.Rows[0]["UserPassword"].ToString();


                    NetworkCredential credentials = new NetworkCredential(UserId, UserPassword);
                    using (new ConnectToSharedFolder(NetworkPath, credentials))
                    {
                        ErrorMsg = "";
                        string srcPath = Path.Combine(NetworkPath, FileName);
                        var srcDirectory = new DirectoryInfo(Path.GetDirectoryName(srcPath));
                        if (srcDirectory.Exists)
                        {

                            DestPath = Path.Combine(DestFolder, DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + FileName);
                            var destinationDirectory = new DirectoryInfo(Path.GetDirectoryName(DestPath));
                            if (!destinationDirectory.Exists)
                            {
                                destinationDirectory.Create();
                            }
                            string myNetworkPath = string.Empty;

                            if (File.Exists(srcPath))
                            {
                                File.Copy(srcPath, DestPath);
                                return true;
                            }
                            else
                            {
                                ErrorMsg = "File path not found.";
                                return false;
                            }



                        }
                        else
                        {
                            ErrorMsg = "File is not exists to source folder.";
                            DestPath = "";
                            return false;
                        }
                    }
                }
                else
                {
                    ErrorMsg = "Shared folder is not accessed on File server.";
                    DestPath = "";
                    return false;
                }
            }
            catch (Exception)
            {
                ErrorMsg = "File is not copy to destination folder.";
                DestPath = "";
                return false;
            }
        }

        private void ExecuteUploadProcedure()
        {
            try
            {
                cn = new SqlConnection(SQLfunctions.GetConnectionString);
                if (cn.State == ConnectionState.Closed)
                    cn.Open();

                string FCS = ConfigurationSettings.AppSettings["FCS"].ToString();
                string FCSPath = ConfigurationSettings.AppSettings["FCSPath"].ToString();

                List<JEPModel> listJEP = new List<JEPModel>();

                string FCS_SERVER = "Select ConfigName,ConfigValues,UserId,UserPassword from DES104 WHERE ConfigName='" + FCSPath + "'";
                SqlDataAdapter daFCS_SERVER = new SqlDataAdapter(SQLfunctions.GetCommandFromSQL(FCS_SERVER, SQLfunctions.GetConnection()));
                DataTable dtFCS_SERVER = new DataTable();
                daFCS_SERVER.Fill(dtFCS_SERVER);

                if (dtFCS_SERVER != null && dtFCS_SERVER.Rows.Count > 0)
                {
                    string FCSServer = Convert.ToString(dtFCS_SERVER.Rows[0]["ConfigValues"]);
                    if (!string.IsNullOrEmpty(FCSServer))
                    {


                        string strTemp = "Select * from DES109 WHERE isnull(IsUploaded,0)=0 and Location='" + FCS + "'";
                        SqlDataAdapter daTempData = new SqlDataAdapter(SQLfunctions.GetCommandFromSQL(strTemp, SQLfunctions.GetConnection()));
                        DataTable dtTempData = new DataTable();
                        daTempData.Fill(dtTempData);

                        //Temp table data loop
                        if (dtTempData != null && dtTempData.Rows.Count > 0)
                            for (int i = 0; i < dtTempData.Rows.Count; i++)
                            {

                                string DestFolder = Path.Combine(FCSServer, dtTempData.Rows[i]["Project"].ToString() + "-" + dtTempData.Rows[i]["DocNo"].ToString() + "-R" + dtTempData.Rows[i]["DocRevision"].ToString());

                                // string DestFolder = dtTempData.Rows[i]["LocationIP"].ToString() + "//FCSFiles//" + dtTempData.Rows[i]["Project"].ToString() + "-" + dtTempData.Rows[i]["DocNo"].ToString() + "-R" + dtTempData.Rows[i]["DocRevision"].ToString() + "//";
                                string errormsg = "", DestPath = "";
                                bool status = FileTransfer(dtTempData.Rows[i]["SrcFolder"].ToString(), dtTempData.Rows[i]["FileName"].ToString(), DestFolder, out DestPath, out errormsg);
                                if (status)
                                {
                                    string DocCheck = "SELECT Top 1 DocumentId FROM DES059 WHERE DocumentNo='" + dtTempData.Rows[i]["DocNo"].ToString() + "' Order by DocumentRevision Desc";

                                    Int64? DocID = SQLfunctions.ExecuteSqlCommandReturnInt(DocCheck);
                                    if (DocID <= 0)
                                    {
                                        DataSet ds = new DataSet();
                                        SqlCommand StoredProcedureCommand = new SqlCommand("SP_DES_GET_OBJECT_POLICY_BY_OBJECTNAME", cn);
                                        StoredProcedureCommand.CommandType = CommandType.StoredProcedure;
                                        StoredProcedureCommand.Parameters.Add("@ObjectName", SqlDbType.NVarChar, -1).Value = "DOC";
                                        StoredProcedureCommand.Parameters.Add("@BUId", SqlDbType.BigInt).Value = dtTempData.Rows[i]["BUId"];

                                        var adapter = new SqlDataAdapter(StoredProcedureCommand);
                                        adapter.Fill(ds);
                                        if (ds.Tables[0].Rows.Count > 0)
                                        {
                                            #region Add Data in Document
                                            strDocInsert = "Insert into DES059 ([Project],[DocumentTypeId],[DocumentNo],[DocumentTitle],[DocumentRevision]," +
                                                                        "[ClientDocumentNumber],[MilestoneDocument],[PolicyId],[IsJEP],[Location],[Status],[FolderId],[ClientVersion]," +
                                                                        "[CreatedBy],[CreatedOn],[Department],[RoleGroup],[IsLatestRevision],[ReturnNotes]) OUTPUT INSERTED.DocumentId values " + "(@Project,@DocumentTypeId,@DocumentNo,@DocumentTitle,@DocumentRevision,@ClientDocumentNumber,@MilestoneDocument,@PolicyId," +
                                                                        "@IsJEP,@Location,@Status,@FolderId,@ClientVersion,@CreatedBy,@CreatedOn,@Department," +
                                                                        "@RoleGroup,@IsLatestRevision,@ReturnNotes)";

                                            SqlCommand cmdDocInsert = new SqlCommand(strDocInsert, cn);
                                            cmdDocInsert.Parameters.AddWithValue("@Project", dtTempData.Rows[i]["Project"].ToString());
                                            cmdDocInsert.Parameters.AddWithValue("@DocumentTypeId", dtTempData.Rows[i]["DocTypeId"]);
                                            cmdDocInsert.Parameters.AddWithValue("@DocumentNo", dtTempData.Rows[i]["DocNo"].ToString());
                                            cmdDocInsert.Parameters.AddWithValue("@DocumentTitle", dtTempData.Rows[i]["DocDescription"].ToString());
                                            cmdDocInsert.Parameters.AddWithValue("@DocumentRevision", dtTempData.Rows[i]["DocRevision"]);
                                            cmdDocInsert.Parameters.AddWithValue("@ClientDocumentNumber", dtTempData.Rows[i]["CustomerDocNo"].ToString());
                                            cmdDocInsert.Parameters.AddWithValue("@MilestoneDocument", dtTempData.Rows[i]["Milestone"]);
                                            cmdDocInsert.Parameters.AddWithValue("@PolicyId", ds.Tables[0].Rows[0].ItemArray[0]);
                                            cmdDocInsert.Parameters.AddWithValue("@IsJEP", true);
                                            cmdDocInsert.Parameters.AddWithValue("@Location", dtTempData.Rows[i]["Location"]);
                                            cmdDocInsert.Parameters.AddWithValue("@Status", "Draft");
                                            cmdDocInsert.Parameters.AddWithValue("@FolderId", dtTempData.Rows[i]["FolderId"]);
                                            cmdDocInsert.Parameters.AddWithValue("@ClientVersion", dtTempData.Rows[i]["CustomerRevNo"].ToString());
                                            cmdDocInsert.Parameters.AddWithValue("@CreatedBy", dtTempData.Rows[i]["CreatedBy"].ToString());
                                            cmdDocInsert.Parameters.AddWithValue("@CreatedOn", dtTempData.Rows[i]["CreatedOn"]);
                                            cmdDocInsert.Parameters.AddWithValue("@Department", dtTempData.Rows[i]["Department"]);
                                            cmdDocInsert.Parameters.AddWithValue("@RoleGroup", dtTempData.Rows[i]["RoleGroup"].ToString());
                                            cmdDocInsert.Parameters.AddWithValue("@IsLatestRevision", true);
                                            cmdDocInsert.Parameters.AddWithValue("@ReturnNotes", dtTempData.Rows[i]["ReturnNotes"].ToString());

                                            DocID = (Int64)cmdDocInsert.ExecuteScalar();

                                            listJEP.Add(new JEPModel
                                            {
                                                JepNo = dtTempData.Rows[i]["JEPNumber"].ToString(),
                                                DocNo = dtTempData.Rows[i]["DocNo"].ToString(),
                                                Message = "Document created successfully",
                                                CreatedBy = dtTempData.Rows[i]["CreatedBy"].ToString()
                                            });

                                            #endregion


                                            #region Update New Created DocumetId in JEP
                                            strUpdateDocumentIdInJEP = "Update DES075 SET DocumentId=" + Convert.ToInt64(DocID) + " Where DocNo='" + dtTempData.Rows[i]["DocNo"].ToString() + "' and RevisionNo=" + dtTempData.Rows[i]["DocRevision"];
                                            SqlCommand cmdUpdateFlag = new SqlCommand(strUpdateDocumentIdInJEP, cn);
                                            cmdUpdateFlag.ExecuteNonQuery();
                                            #endregion

                                            #region Insert DOC in Policy


                                            for (int iii = 0; iii <= ds.Tables[0].Rows.Count - 1; iii++)
                                            {
                                                strInsertPolicy = "Insert into DES062 ([DocumentId],[PolicyID],[PolicyStep],[PolicyOrderID],[ColorCode]," +
                                                     "[DesignationLevels],[IsCompleted]) values" + "(@DocID,@PolicyID,@PolicyStep,@PolicyOrderID,@ColorCode,@DesignationLevels,@IsCompleted)";

                                                SqlCommand cmdPolicyInsert = new SqlCommand(strInsertPolicy, cn);
                                                cmdPolicyInsert.Parameters.AddWithValue("@DocID", Convert.ToInt64(DocID));
                                                cmdPolicyInsert.Parameters.AddWithValue("@PolicyID", ds.Tables[0].Rows[iii].ItemArray[0]);
                                                cmdPolicyInsert.Parameters.AddWithValue("@PolicyStep", ds.Tables[0].Rows[iii].ItemArray[3]);
                                                cmdPolicyInsert.Parameters.AddWithValue("@PolicyOrderID", ds.Tables[0].Rows[iii].ItemArray[4]);
                                                cmdPolicyInsert.Parameters.AddWithValue("@ColorCode", ds.Tables[0].Rows[iii].ItemArray[5]);
                                                cmdPolicyInsert.Parameters.AddWithValue("@DesignationLevels", ds.Tables[0].Rows[iii].ItemArray[6]);
                                                cmdPolicyInsert.Parameters.AddWithValue("@IsCompleted", 0);
                                                cmdPolicyInsert.ExecuteNonQuery();
                                            }


                                            //#region Update Policy ID
                                            //string DocPolicyCheck = "SELECT Top 1 PolicyID FROM DES062 WHERE DocumentId='" + Convert.ToInt64(DocID) + "'";
                                            //Int64? PolicyId = SQLfunctions.ExecuteSqlCommandReturnInt(DocPolicyCheck);

                                            //string strUpdatePolicyId = "Update DES059 SET PolicyId=" + PolicyId + " Where DocumentId=" + Convert.ToInt64(DocID) + "";
                                            //SqlCommand cmdUpdatePlicyID = new SqlCommand(strUpdatePolicyId, cn);
                                            //cmdUpdatePlicyID.ExecuteNonQuery();
                                            //#endregion

                                            #endregion
                                        }
                                    }
                                    if (DocID > 0 && (!string.IsNullOrEmpty(DestPath)))
                                    {
                                        //Check file exists or not
                                        //if (!File.Exists(dtTempData.Rows[i]["SrcFolder"].ToString()))
                                        //{
                                        //Move file from source to destination
                                        try
                                        {
                                            #region Add Data in Document Mapping  
                                            //var DestFolder = "\\\\10.1.1.9\\dot net\\Test\\2\\";
                                            //var DestFolder = dtTempData.Rows[i]["LocationIP"].ToString() + "//FCSFiles//" + dtTempData.Rows[i]["Project"].ToString() + "-" + dtTempData.Rows[i]["DocNo"].ToString() + "-R" + dtTempData.Rows[i]["DocRevision"].ToString() + "//";
                                            //var destinationDirectory = new DirectoryInfo(Path.GetDirectoryName(DestFolder));

                                            //if (!destinationDirectory.Exists)
                                            //{
                                            //    destinationDirectory.Create();
                                            //}

                                            //System.IO.File.Copy(dtTempData.Rows[i]["SrcFolder"].ToString() + "\\" + dtTempData.Rows[i]["FileName"].ToString(), DestFolder + "//" + Guid.NewGuid().ToString() + "_" + dtTempData.Rows[i]["FileName"].ToString());

                                            //Insert document path record
                                            strDocPathInsert = "Insert into DES060 ([DocumentId],[IsMainDocument],[MainDocumentPath],[Document_name],[FileFormate]," +
                                                          "[Remark],[FileVersion],[IsLatest],[CreatedBy],[CreatedOn],[VersionGUID],[FileLocation]) values" + "(@DocID,@IsMainDocument,@MainDocumentPath,@Document_name,@FileFormate,@Remark,@FileVersion," +
                                                          "@IsLatest,@CreatedBy,@CreatedOn,@VersionGUID,@FileLocation)";
                                            SqlCommand cmdPathInsert = new SqlCommand(strDocPathInsert, cn);
                                            cmdPathInsert.Parameters.AddWithValue("@DocID", Convert.ToInt64(DocID));
                                            cmdPathInsert.Parameters.AddWithValue("@IsMainDocument", 0);
                                            cmdPathInsert.Parameters.AddWithValue("@MainDocumentPath", DestPath);
                                            cmdPathInsert.Parameters.AddWithValue("@Document_name", dtTempData.Rows[i]["FileName"].ToString());
                                            cmdPathInsert.Parameters.AddWithValue("@FileFormate", Path.GetExtension(dtTempData.Rows[i]["SrcFolder"].ToString() + "\\" + dtTempData.Rows[i]["FileName"].ToString()));
                                            cmdPathInsert.Parameters.AddWithValue("@Remark", dtTempData.Rows[i]["ReturnNotes"].ToString());
                                            cmdPathInsert.Parameters.AddWithValue("@FileVersion", 1);
                                            cmdPathInsert.Parameters.AddWithValue("@IsLatest", true);
                                            cmdPathInsert.Parameters.AddWithValue("@CreatedBy", dtTempData.Rows[i]["CreatedBy"].ToString());
                                            cmdPathInsert.Parameters.AddWithValue("@CreatedOn", DateTime.Now);
                                            cmdPathInsert.Parameters.AddWithValue("@VersionGUID", dtTempData.Rows[i]["VersionGUID"].ToString());
                                            cmdPathInsert.Parameters.AddWithValue("@FileLocation", dtTempData.Rows[i]["FileLocation"].ToString());
                                            cmdPathInsert.ExecuteNonQuery();

                                            //Update upload flag
                                            strUpdateFlag = "Update DES109 SET IsUploaded=1 Where TempDocId=" + dtTempData.Rows[i]["TempDocId"] + "";
                                            SqlCommand cmdUpdateFlag = new SqlCommand(strUpdateFlag, cn);
                                            cmdUpdateFlag.ExecuteNonQuery();

                                            listJEP.Add(new JEPModel
                                            {
                                                JepNo = dtTempData.Rows[i]["JEPNumber"].ToString(),
                                                DocNo = dtTempData.Rows[i]["DocNo"].ToString(),
                                                Message = "Document " + dtTempData.Rows[i]["FileName"].ToString() + " uploaded successfully.",
                                                CreatedBy = dtTempData.Rows[i]["CreatedBy"].ToString()
                                            });

                                            #endregion


                                        }
                                        catch (Exception)
                                        {
                                        }
                                    }

                                }
                                else
                                {
                                    strUpdateFlag = "Update DES109 SET IsUploaded=2 Where TempDocId=" + dtTempData.Rows[i]["TempDocId"].ToString() + "";
                                    SqlCommand cmdUpdateFlag = new SqlCommand(strUpdateFlag, cn);
                                    cmdUpdateFlag.ExecuteNonQuery();
                                    listJEP.Add(new JEPModel
                                    {
                                        JepNo = dtTempData.Rows[i]["JEPNumber"].ToString(),
                                        DocNo = dtTempData.Rows[i]["DocNo"].ToString(),
                                        Message = "Document " + dtTempData.Rows[i]["FileName"].ToString() + " System Info:" + errormsg,
                                        CreatedBy = dtTempData.Rows[i]["CreatedBy"].ToString()
                                    });
                                }
                            }

                    }
                }
                if (listJEP.Count() > 0)
                {
                    List<String> mailList = new List<string>();
                    mailList = listJEP.Select(x => x.JepNo).Distinct().ToList();
                    foreach (var mailJEP in mailList)
                    {
                        var data = listJEP.Where(y => y.JepNo == mailJEP).ToList();
                        string body = "<h4><b>JEP NO.:</b>" + mailJEP + "</h4>";
                        string subject = "JEP " + mailJEP + " bulk document upload notification";
                        string MailTo = "";
                        bool Flag = true;
                        foreach (var JEP in data)
                        {
                            if (Flag)
                            {
                                Flag = false;
                                string email = "select t_mail from COM007 where t_emno='" + JEP.CreatedBy + "'";
                                SqlDataAdapter daemail = new SqlDataAdapter(SQLfunctions.GetCommandFromSQL(email, SQLfunctions.GetConnection()));
                                DataTable dtemail = new DataTable();
                                daemail.Fill(dtemail);
                                if (dtemail != null && dtemail.Rows.Count > 0)
                                    MailTo = Convert.ToString(dtemail.Rows[0]["t_mail"]);
                            }
                            body += "<p>" + JEP.Message + "</p>";
                        }
                        if (!string.IsNullOrEmpty(MailTo))
                            EmailService.SendEmail(new EmailParamModel { ToEmailAddress = MailTo, Subject = subject, Body = body });
                    }
                }


            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            finally
            {
                cn.Close();
            }
        }


    }
}