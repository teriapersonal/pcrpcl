﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using System;
using System.Collections.Generic;

namespace IEMQS.DESServices
{
    public interface IARMService
    {
        List<SP_DES_GET_ARMSet_Result> GetARMSet(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal);

        List<SP_DES_GET_ARMFILEBY_ARMCODEID_Result> GetARMFileByArmCodeId(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection,Int64? ArmCodeId, out int recordsTotal);

        
        string AddEdit(ARMModel model, out string errorMsg);
        Int64 DeleteArmSet(string Arms, out string errorMsg);
        List<ARMModel> GetAllArmCode();
        List<SP_DES_GET_ARMCode_Result> GetARMCode(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal);
        Int64 AddEditARMCode(ARMCodeModal model, out string errorMsg);
        Int64 DeleteArmCode(Int64 ARMCodeId, out string errorMsg);
        bool UpdateArmSetStatus(string Arms, bool Status);

        Int64? AddARMCodeFile(ARMCodeModal model,int fileLocation);

        Int64 DeleteARMAttach(Int64 Id, out string errorMsg);
        bool GetRoleGroup(string PsNo);
    }
}
