﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace IEMQS.DESServices
{
    public class DOCFileService : IDOCFileService
    {
        public Int64 AddEdit(DOCFileModel model, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";

                    var temp = db.DES060.Find(model.DocumentMappingId);
                    

                    List<DES060> List_DES060 = new List<DES060>();

                    foreach (var item in model.FileFormatData)
                    {
                        DES060 _DES060 = temp == null ? new DES060() : temp;
                        string VersionGuid = Guid.NewGuid().ToString();
                        _DES060.DocumentId = model.DocumentId;
                        _DES060.IsMainDocument = model.IsMainDocument;
                        _DES060.MainDocumentPath = item.destination + "/" + item.filename;
                        _DES060.Status = model.Status;
                        _DES060.Document_name = item.originalname;
                        _DES060.FileFormate = Path.GetExtension(item.originalname);
                        _DES060.Remark = model.Remark;
                        _DES060.FileVersion = 1;
                        _DES060.VersionGUID = VersionGuid;
                        _DES060.IsLatest = true;

                        if (temp == null || (model.IsRevisionForDocMapping && temp != null))
                        {
                            _DES060.FileLocation = model.FileLocation;
                            if (model.FileLocation == (int)LocationType.Hazira)
                            {
                                _DES060.Central = true; //By Default Centrall will be false.
                            }
                            else
                            {
                                _DES060.Central = false; //By Default Centrall will be false.
                            }

                            _DES060.CreatedBy = CommonService.objClsLoginInfo.UserName;
                            _DES060.CreatedOn = DateTime.Now;

                        }
                        else
                        {
                            _DES060.EditedBy = model.EditedBy;
                            _DES060.EditedOn = DateTime.Now;
                            AddDocumentHistory(model.DocumentId, null, null, null, model.DocumentName + " file has been edited.", CommonService.objClsLoginInfo.UserName);
                        }
                        List_DES060.Add(_DES060);
                        AddDocumentHistory(model.DocumentId, null, null, null, model.DocumentName + " file has been added.", CommonService.objClsLoginInfo.UserName);

                    }
                    db.DES060.AddRange(List_DES060);
                    db.SaveChanges();

                    return 1;

                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_DOCUMENTMAPPING_Result> GetDocumentMappingList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, Int64? DocId, bool IsLatest, string VersionGUID, string currentUser, string Roles, out int recordsTotal, out string errorMsg)
        {
            errorMsg = "";
            using (IEMQSDESEntities db = new IEMQSDESEntities())
            {
                try
                {
                    var data = db.SP_DES_GET_DOCUMENTMAPPING(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, DocId, IsLatest, VersionGUID, currentUser, Roles).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    errorMsg = "All document loded successfully";
                    return data;
                }
                catch (Exception ex)
                {
                    CommonService.SendErrorToText(ex);
                    recordsTotal = 10;
                    var data = db.SP_DES_GET_DOCUMENTMAPPING(0, 10, "", "", 1, 0, IsLatest, null, currentUser, Roles).ToList();
                    errorMsg = "Document does not loded successfully";
                    return data;
                }
            }
        }

        public Int64 UpdateIsMainFileStatus(DOCFileModel model, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    var data = db.DES060.FirstOrDefault(x => x.DocumentMappingId == model.DocumentMappingId);
                    data.IsMainDocument = model.IsMainDocument;
                    db.Entry(data).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    AddDocumentHistory(data.DocumentId, null, null, null, data.IsMainDocument != true ? data.Document_name + " is unmarked as main file." : data.Document_name + " is marked as main file", CommonService.objClsLoginInfo.UserName);
                    return data.DocumentMappingId;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public Int64? UpdateDOCLifeCycle(int DocumentID, string Project, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    var data = db.DES059.Where(yy => yy.DocumentId == DocumentID).FirstOrDefault();
                    var getDocFromMapping = db.DES060.Where(xx => xx.DocumentId == DocumentID).Select(x => x.DocumentMappingId).ToList();
                    if (data != null)
                    {
                        if (data.DES060 != null && data.DES060.Count > 0)
                        {
                            if (!(data.DES060.Any(x => (x.IsMainDocument ?? false))))
                            {
                                errorMsg = "Atleast one document should be selected as a main document.";
                                return 0;
                            }
                        }
                        else
                        {
                            errorMsg = "Upload at least one document";
                            return 0;
                        }

                        if (getDocFromMapping != null && getDocFromMapping.Count > 0)
                        {
                            List<DES061> des061 = db.DES061.ToList();
                            foreach (var item in getDocFromMapping)
                            {
                                var check = des061.Where(x => x.DocumentMappingId == item).Select(x => x.CheckInStatus).FirstOrDefault();
                                if (check == true)
                                {
                                    errorMsg = "You cannot submit due to check in file is pendig";
                                    return 0;
                                }
                            }
                        }

                        data.CreatedBy = CommonService.objClsLoginInfo.UserName;
                        data.CreatedOn = DateTime.Now;
                        data.Status = ObjectStatus.InProcess.ToString();
                        data.IsDOCCheckIn = false;
                        data.DOCCheckInBy = CommonService.objClsLoginInfo.UserName;
                        data.DOCCheckInOn = DateTime.Now;
                        db.Entry(data).State = System.Data.Entity.EntityState.Modified;

                        var dataOfLifeCycle = db.DES062.Where(yy => yy.DocumentId == DocumentID).OrderBy(x => x.PolicyOrderID).FirstOrDefault();
                        if (dataOfLifeCycle != null)
                        {
                            dataOfLifeCycle.CreatedBy = CommonService.objClsLoginInfo.UserName;
                            dataOfLifeCycle.CreatedOn = DateTime.Now;
                            dataOfLifeCycle.IsCompleted = true;
                            db.Entry(dataOfLifeCycle).State = System.Data.Entity.EntityState.Modified;
                        }
                        db.SaveChanges();

                        #region Notification  
                        try
                        {
                            db.SP_DES_DOC_PUSH_NOTIFICATION(Project, CommonService.objClsLoginInfo.UserName, NotificationObject.Create.ToString(), data.DocumentId);
                        }
                        catch (Exception ex)
                        {
                            CommonService.SendErrorToText(ex);
                        }
                        #endregion

                        AddDocumentHistory(data.DocumentId, null, null, null, "Document " + data.DocumentNo + " is in process.", CommonService.objClsLoginInfo.UserName);
                        return data.DocumentId;
                    }
                    else
                    {
                        errorMsg = "Document not found";
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }

        }

        public Int64 AddCheckedInOUT(CheckeINOUT model, out string infoMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    #region CheckIn/Out
                    var getDOcId = db.DES060.Where(x => x.DocumentMappingId == model.DocumentMappingId).FirstOrDefault();
                    var temp = db.DES061.Where(yy => yy.DocumentMappingId == model.DocumentMappingId).FirstOrDefault();
                    DES061 _DES061 = temp == null ? new DES061() : temp;
                    infoMsg = "";
                    if (temp == null)
                    {
                        _DES061.DocumentMappingId = model.DocumentMappingId;
                        _DES061.CheckedBy = CommonService.objClsLoginInfo.UserName;
                        _DES061.CheckIn_Remark = model.CheckedIn_Remark;
                        _DES061.CheckInOn = DateTime.Now;
                        _DES061.CheckInStatus = true;
                        _DES061.CheckOutStatus = false;
                        db.DES061.Add(_DES061);
                        db.SaveChanges();
                        if (model.CheckedIn_Remark != null)
                        {
                            AddDocumentHistory(getDOcId.DocumentId, null, null, _DES061.CheckInCheckOutId, getDOcId.Document_name + " file is CheckedOut with remark: " + model.CheckedIn_Remark, CommonService.objClsLoginInfo.UserName);
                        }
                        else
                        {
                            AddDocumentHistory(getDOcId.DocumentId, null, null, _DES061.CheckInCheckOutId, getDOcId.Document_name + " file is CheckedOut ", CommonService.objClsLoginInfo.UserName);
                        }
                        infoMsg = "true";
                        return _DES061.CheckInCheckOutId;
                    }
                    else
                    {
                        var data60 = db.DES060.Where(x => x.DocumentMappingId == model.DocumentMappingId).FirstOrDefault();
                        var data = db.DES061.Where(yy => yy.DocumentMappingId == model.DocumentMappingId).FirstOrDefault();
                        if (model.CheckedIn_Remark != null)
                        {
                            data.CheckIn_Remark = model.CheckedIn_Remark;
                            data.CheckInOn = DateTime.Now;
                            data.CheckInStatus = true;
                            data.CheckOutStatus = false;
                            AddDocumentHistory(getDOcId.DocumentId, null, null, data.CheckInCheckOutId, data60.Document_name + " is CheckedOut with remark " + model.CheckedIn_Remark, CommonService.objClsLoginInfo.UserName);
                            infoMsg = "true";
                        }
                        else
                        {
                            data.CheckOut_Remark = model.CheckedOut_Remark;
                            data.CheckOutOn = DateTime.Now;
                            data.CheckInStatus = false;
                            data.CheckOutStatus = true;
                            //if (model.CheckedOut_Remark != null)
                            //{
                            //    AddDocumentHistory(getDOcId.DocumentId, null, null, data.CheckInCheckOutId, data60.Document_name + " is checkedOut with remark " + model.CheckedOut_Remark, CommonService.objClsLoginInfo.UserName);
                            //}
                            //else
                            //{
                            //    AddDocumentHistory(getDOcId.DocumentId, null, null, data.CheckInCheckOutId, data60.Document_name + " is checkedOut ", CommonService.objClsLoginInfo.UserName);
                            //}
                            infoMsg = "false";
                        }
                        db.Entry(data).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        return data.CheckInCheckOutId;
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public Int64 UpdateFileOnCheckOutMark(DOCFileModel model)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    bool UpdateFileOnCheckOutMark = false;
                    var temp = db.DES060.Find(model.DocumentMappingId);

                    if (temp != null && temp.DocumentMappingId > 0)
                    {
                        UpdateFileOnCheckOutMark = temp.IsMainDocument ?? false;
                        temp.IsLatest = false;
                        db.Entry(temp).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                    DES060 _DES060 = new DES060();

                    _DES060.DocumentId = model.DocumentId;
                    _DES060.IsMainDocument = UpdateFileOnCheckOutMark;
                    _DES060.MainDocumentPath = model.MainDocumentPath;
                    _DES060.Status = temp.Status;
                    _DES060.Document_name = model.DocumentName;
                    _DES060.FileLocation = model.FileLocation;
                    if (model.FileLocation == (int)LocationType.Hazira)
                    {
                        _DES060.Central = true;
                    }
                    else
                    {
                        _DES060.Central = false; //By Default Centrall will be false.
                    }
                    _DES060.FileFormate = model.FileFormate;
                    _DES060.Remark = temp.Remark;
                    _DES060.IsLatest = true;
                    _DES060.FileVersion = ((temp.FileVersion ?? 0) + 1);
                    _DES060.VersionGUID = temp.VersionGUID;
                    _DES060.CreatedBy = CommonService.objClsLoginInfo.UserName;
                    _DES060.CreatedOn = DateTime.Now;
                    db.DES060.Add(_DES060);
                    db.SaveChanges();
                    if (temp.Document_name != null)
                    {
                        if (model.Remark != null)
                        {
                            AddDocumentHistory(temp.DocumentId, null, null, null, model.DocumentName + " updated file on CheckedIn of previous file: " + temp.Document_name + " with remark: " + model.Remark, CommonService.objClsLoginInfo.UserName);
                        }
                        else
                        {
                            AddDocumentHistory(temp.DocumentId, null, null, null, model.DocumentName + " updated file on CheckedIn of previous file: " + temp.Document_name, CommonService.objClsLoginInfo.UserName);
                        }
                    }
                    return _DES060.DocumentMappingId;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public Int64 DeleteDocFile(Int64 Id, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";

                    var checkFromChild = db.DES061.Where(yy => yy.DocumentMappingId == Id).FirstOrDefault();
                    if (checkFromChild != null)
                    {
                        db.DES061.Remove(checkFromChild);
                        db.SaveChanges();
                    }

                    var check = db.DES060.Find(Id);
                    var GetDocId = check.DocumentId;
                    var GetDocName = check.Document_name;
                    if (check != null)
                    {
                        db.DES060.Remove(check);
                        db.SaveChanges();
                    }
                    AddDocumentHistory(GetDocId, null, null, null, GetDocName + " file has been deleted ", CommonService.objClsLoginInfo.UserName);

                    return check.DocumentMappingId;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<DOCFileModel> GetDocumentListByDocId(Int64? DocID)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES060.Where(x => x.DocumentId == DocID && x.IsLatest == true).Select(x => new DOCFileModel
                    {
                        DocumentMappingId = x.DocumentMappingId,
                        DocumentId = x.DocumentId,
                        IsMainDocument = x.IsMainDocument ?? false,
                        MainDocumentPath = x.MainDocumentPath,
                        Status = x.Status,
                        DocumentName = x.Document_name,
                        FileFormate = x.FileFormate,
                        FileVersion = x.FileVersion,
                        Remark = x.Remark,
                        IsLatest = x.IsLatest ?? false,
                        CreatedBy = x.CreatedBy,
                        CreatedOn = x.CreatedOn,
                        EditedBy = x.EditedBy,
                        EditedOn = x.EditedOn,
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public void AddDocumentHistory(Int64? DocID, Int64? DocFileMappingId, Int64? DocLifCycleId, Int64? CheckINOut, string Discription, string CreatedBy)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    DES063 _DES063 = new DES063();
                    _DES063.DocumentId = DocID;
                    _DES063.DocumentMappingId = DocFileMappingId;
                    _DES063.DocumentLifeCycleId = DocLifCycleId;
                    _DES063.CheckInCheckOutId = CheckINOut;
                    _DES063.Description = Discription;
                    _DES063.CreatedBy = CommonService.objClsLoginInfo.UserName;
                    _DES063.CreatedOn = DateTime.Now;
                    db.DES063.Add(_DES063);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }


        public Int64? UpdateDOCPolicy(Int64 DocumentLifeCycleId, string Project, out string errorMsg, out string infoMsg, out string armMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    infoMsg = "";
                    armMsg = "";

                    bool IsCompleteDCR = false;
                    string strDocNo = string.Empty;

                    var data = db.DES062.Where(yy => yy.DocumentLifeCycleId == DocumentLifeCycleId).FirstOrDefault();
                    var checkDocumentIsExist = db.DES060.Where(x => x.DocumentId == data.DocumentId).ToList();
                    string getLastStep = string.Empty;        
                    var DocData1 = db.DES059.Where(x => x.DocumentId == data.DocumentId).FirstOrDefault();
                    var updateDCR1 = CommonService.CheckDCR( DocData1.DocumentNo);
                    if (updateDCR1 == "")
                    {
                        if (checkDocumentIsExist.Count() > 0)
                        {
                            if (checkDocumentIsExist.Any(x => x.IsMainDocument ?? false))
                            {
                                if (data != null)
                                {
                                    if (data.IsCompleted != true)
                                    {
                                        //var getDocData = db.DES060.Where(xx => xx.DocumentId == data.DocumentId).FirstOrDefault();
                                        var datalist = db.DES062.Where(yy => yy.DocumentId == data.DocumentId).ToList().Count;
                                        var list = db.DES062.Where(x => x.DocumentId == data.DocumentId && x.IsCompleted != true).OrderBy(x => x.PolicyOrderID).FirstOrDefault();
                                        if (list.DocumentLifeCycleId == data.DocumentLifeCycleId)
                                        {
                                            if (list.PolicyOrderID > 1 && datalist != list.PolicyOrderID)
                                            {
                                                var DocData = db.DES059.Where(x => x.DocumentId == data.DocumentId).FirstOrDefault();
                                                DocData.Status = ObjectStatus.InProcess.ToString();
                                                DocData.EditBy = CommonService.objClsLoginInfo.UserName;
                                                DocData.EditedOn = DateTime.Now;
                                                db.Entry(DocData).State = System.Data.Entity.EntityState.Modified;
                                            }
                                            if (datalist == list.PolicyOrderID)
                                            {
                                                var DocData = db.DES059.Where(x => x.DocumentId == data.DocumentId).FirstOrDefault();

                                                // var getData = db.DES026.Where(x => x.Id == DocData.DocumentTypeId).Select(x => x.Lookup).FirstOrDefault();
                                                if (DocData != null && DocData.DocumentTypeId == (int)DOCType.ARM)
                                                {
                                                    try
                                                    {
                                                        DES037 _DES037 = new DES037();
                                                        var dataDES037 = db.DES037.Where(x => x.Arms == DocData.DocumentNo).FirstOrDefault();
                                                        if (dataDES037 != null)
                                                        {
                                                            dataDES037.Vrsn = "R" + DocData.DocumentRevision;
                                                            db.Entry(dataDES037).State = System.Data.Entity.EntityState.Modified;
                                                            db.SaveChanges();
                                                            armMsg = dataDES037.Arms + " ARM set updated succesfully";
                                                        }
                                                        else
                                                        {
                                                            _DES037.Arms = DocData.DocumentNo;
                                                            _DES037.Desc = DocData.DocumentTitle;
                                                            _DES037.Vrsn = "R" + DocData.DocumentRevision;
                                                            _DES037.Fnam = DocData.DocumentNo;
                                                            _DES037.ArmCode = null;
                                                            _DES037.Type = "Customized";
                                                            _DES037.Status = true;
                                                            _DES037.CreatedBy = CommonService.objClsLoginInfo.UserName;
                                                            _DES037.CreatedOn = DateTime.Now;
                                                            _DES037.IsFromDOC = true;
                                                            db.DES037.Add(_DES037);
                                                            db.SaveChanges();
                                                            armMsg = _DES037.Arms + " ARM set created succesfully";
                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        errorMsg = "Error while adding data in ARM !";
                                                        CommonService.SendErrorToText(ex);
                                                    }
                                                }
                                                DocData.Status = ObjectStatus.Completed.ToString();
                                                DocData.EditBy = CommonService.objClsLoginInfo.UserName;
                                                DocData.EditedOn = DateTime.Now;
                                                db.Entry(DocData).State = System.Data.Entity.EntityState.Modified;

                                                IsCompleteDCR = true;
                                                strDocNo = DocData.DocumentNo;

                                                getLastStep = list.PolicyStep;
                                            }

                                            if (list.PolicyOrderID == 1)
                                            {
                                                var getDocStatus = data.DES059.Status;
                                                if (getDocStatus == ObjectStatus.Draft.ToString())
                                                {


                                                    var DocData = db.DES059.Where(x => x.DocumentId == data.DocumentId).FirstOrDefault();
                                                    if (DocData.IsDOCCheckIn == true)
                                                    {
                                                        if (DocData.DOCCheckInBy == CommonService.objClsLoginInfo.UserName)
                                                        {
                                                            DocData.Status = ObjectStatus.InProcess.ToString();
                                                            DocData.IsDOCCheckIn = false;
                                                            DocData.EditBy = CommonService.objClsLoginInfo.UserName;
                                                            DocData.EditedOn = DateTime.Now;
                                                            db.Entry(DocData).State = System.Data.Entity.EntityState.Modified;
                                                        }
                                                        else
                                                        {
                                                            errorMsg = "This document is alredy in use by " + new clsManager().GetUserNameFromPsNo(DocData.DOCCheckInBy);
                                                            return data.DocumentId;
                                                        }
                                                    }

                                                }
                                            }

                                            AddDocumentHistory(data.DES059.DocumentId, null, null, null, data.DES059.DocumentNo + " document policy step " + data.PolicyStep + " has been completed.", CommonService.objClsLoginInfo.UserName);
                                            data.CreatedBy = CommonService.objClsLoginInfo.UserName;
                                            data.CreatedOn = DateTime.Now;
                                            data.IsCompleted = true;
                                            db.Entry(data).State = System.Data.Entity.EntityState.Modified;
                                            db.SaveChanges();

                                            if (IsCompleteDCR)
                                            {
                                                var updateDCR = CommonService.CompleteDCR(null, strDocNo);
                                            }

                                            infoMsg = data.DES059.DocumentNo + " " + data.PolicyStep + " successfully";
                                        }
                                        else
                                        {
                                            errorMsg = "You Must Complete Before Steps !";
                                        }
                                        #region Notification 
                                        try

                                        {
                                            db.SP_DES_DOC_PUSH_NOTIFICATION(Project, CommonService.objClsLoginInfo.UserName, NotificationObject.LifeCycle.ToString(), data.DocumentId);
                                        }
                                        catch (Exception ex)
                                        {
                                            CommonService.SendErrorToText(ex);
                                        }

                                        #endregion
                                    }
                                    else
                                    {
                                        var PsName = new clsManager().GetUserNameFromPsNo(data.CreatedBy);
                                        infoMsg = "DOC has been already" + data.PolicyStep + " by " + PsName;
                                    }
                                }
                            }
                            else
                            {
                                errorMsg = "Atleast one document should be selected as a main document in " + data.DES059.DocumentNo;
                            }
                        }
                        else
                        {
                            errorMsg = "Upload at least one document in " + data.DES059.DocumentNo;
                        }
                    }
                    else
                    {
                        errorMsg = "Document is already in use DCR : " + updateDCR1;
                    }
                    return data.DocumentId;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public Int64? ReviseDOCPolicy(string ReviseDOCPolicy, Int64? DocumentId, string Project, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    var data = db.DES062.Where(yy => yy.DocumentId == DocumentId).ToList();
                    var docdata = db.DES059.Where(yy => yy.DocumentId == DocumentId).FirstOrDefault();

                    if (docdata.Status != ObjectStatus.Completed.ToString())
                    {
                        if (data != null)
                        {
                            foreach (var item in data)
                            {
                                item.IsCompleted = false;
                                item.CreatedBy = "";
                                item.CreatedOn = null;
                                db.Entry(item).State = System.Data.Entity.EntityState.Modified;
                            }
                        }
                        if (!string.IsNullOrEmpty(ReviseDOCPolicy) && docdata != null)
                        {
                            docdata.ReturnNotes = ReviseDOCPolicy;
                            db.Entry(docdata).State = System.Data.Entity.EntityState.Modified;
                        }
                        var DocData = db.DES059.Where(yy => yy.DocumentId == DocumentId).FirstOrDefault();
                        if (DocData != null)
                        {
                            DocData.Status = ObjectStatus.Draft.ToString();
                            db.Entry(DocData).State = System.Data.Entity.EntityState.Modified;

                            #region Notification 
                            try
                            {
                                db.SP_DES_DOC_PUSH_NOTIFICATION(Project, CommonService.objClsLoginInfo.UserName, NotificationObject.Return.ToString(), DocData.DocumentId);
                            }

                            catch (Exception ex)
                            {
                                CommonService.SendErrorToText(ex);
                            }

                            #endregion
                        }

                        AddDocumentHistory(DocumentId, null, null, null, docdata.DocumentNo + " document policy has been revised", CommonService.objClsLoginInfo.UserName);
                        db.SaveChanges();
                    }
                    else
                    {
                        var getLastPolicy = data.Last();
                        var PsName = new clsManager().GetUserNameFromPsNo(getLastPolicy.CreatedBy);
                        errorMsg = "Document is already " + getLastPolicy.PolicyStep + " by " + PsName;
                    }
                    return DocumentId;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }


        public Int64 UpdateDOCRemark(DOCFileModel model)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.DES060.Where(yy => yy.DocumentMappingId == model.DocumentMappingId).FirstOrDefault();
                    data.Remark = model.Remark;
                    db.Entry(data).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    if (!(string.IsNullOrEmpty(data.Remark)))
                    {
                        var dataDES063 = db.DES063.Where(yy => yy.DocumentMappingId == model.DocumentMappingId && yy.DocumentId == data.DES059.DocumentId).FirstOrDefault();
                        if (dataDES063 != null)
                        {
                            dataDES063.Description = data.Document_name + "'s remark- " + data.Remark;
                            dataDES063.CreatedBy = CommonService.objClsLoginInfo.UserName;
                            dataDES063.CreatedOn = DateTime.Now;
                            db.Entry(data).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                        }
                        else
                        {
                            AddDocumentHistory(data.DocumentId, data.DocumentMappingId, null, null, data.Document_name + "'s remark- " + data.Remark, CommonService.objClsLoginInfo.UserName);
                        }
                    }
                    return data.DocumentMappingId;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public bool AddRevision(DOCFileModel model, out string errorMsg)
        {
            try
            {
                UpdateRevisionFlag(model.OldDocIDForRevision);
                var getDocMappingData = GetDocumentListByDocId(model.OldDocIDForRevision);
                int i = 0;
                errorMsg = "";
                foreach (DOCFileModel item in getDocMappingData)
                {
                    //item.DocumentId = model.DocumentId;
                    //item.FileLocation = model.FileLocation;
                    //item.MainDocumentPath = model.newPathAfterRevise[i].ToString();
                    //item.IsMainDocument = model.IsMainDocument;
                    item.IsRevisionForDocMapping = true;
                    //item.DocumentName = model.DocumentName;
                    //item.Remark = model.Remark;
                    try
                    {
                        using (IEMQSDESEntities db = new IEMQSDESEntities())
                        {
                            errorMsg = "";
                            string VersionGuid = Guid.NewGuid().ToString();
                            var temp = db.DES060.Find(item.DocumentMappingId);
                            DES060 _DES060 = temp == null ? new DES060() : temp;

                            _DES060.DocumentId = model.DocumentId;
                            _DES060.IsMainDocument = item.IsMainDocument;
                            _DES060.MainDocumentPath = model.newPathAfterRevise[i].ToString();
                            _DES060.Status = item.Status;
                            _DES060.Document_name = item.DocumentName;
                            _DES060.FileFormate = item.FileFormate;
                            _DES060.Remark = item.Remark;
                            _DES060.FileVersion = 1;
                            _DES060.VersionGUID = VersionGuid;
                            _DES060.IsLatest = true;



                            if (temp == null || (item.IsRevisionForDocMapping && temp != null))
                            {
                                _DES060.FileLocation = model.FileLocation;
                                if (model.FileLocation == (int)LocationType.Hazira)
                                {
                                    _DES060.Central = true; //By Default Centrall will be false.
                                }
                                else
                                {
                                    _DES060.Central = false; //By Default Centrall will be false.
                                }

                                _DES060.CreatedBy = CommonService.objClsLoginInfo.UserName;
                                _DES060.CreatedOn = DateTime.Now;
                                db.DES060.Add(_DES060);
                            }
                            else
                            {
                                _DES060.EditedBy = model.EditedBy;
                                _DES060.EditedOn = DateTime.Now;
                                AddDocumentHistory(model.DocumentId, null, null, null, model.DocumentName + "  has been edited.", CommonService.objClsLoginInfo.UserName);
                            }
                            db.SaveChanges();
                            AddDocumentHistory(model.DocumentId, null, null, null, model.DocumentName + " has been added.", CommonService.objClsLoginInfo.UserName);
                        }
                    }
                    catch (Exception ex)
                    {
                        CommonService.SendErrorToText(ex);
                        throw;
                    }
                    i++;
                }
                return true;
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }



        }

        public bool UpdateRevisionFlag(Int64? DocId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.DES059.FirstOrDefault(x => x.DocumentId == DocId);
                    data.IsLatestRevision = false;
                    db.Entry(data).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    AddDocumentHistory(data.DocumentId, null, null, null, data.DocumentNo + " document with revision R" + data.DocumentRevision + "'s new revision added", CommonService.objClsLoginInfo.UserName);
                    return true;

                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }
    }
}
