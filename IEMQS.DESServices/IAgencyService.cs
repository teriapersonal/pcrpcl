﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using System;
using System.Collections.Generic;

namespace IEMQS.DESServices
{
    public interface IAgencyService
    {
        Int64 AddEdit(AgencyModel model, out string errorMsg);
        List<AgencyModel> GetAgencybyBU(Int64 BUId);
        List<AgencyModel> GetAllAgency(Int64? BUId);
        List<AgencyModel> GetAllAgency();
        List<SP_DES_GET_AGENCY_Result> GetAgencyList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal);
        Int64 DeleteAgency(Int64 Id, out string errorMsg);

        bool UpdateIsCheckInAgency(Int64? AgencyId, bool IsAgencyCheckIn);
        bool IsCheckInAgency(string PsNo, Int64? AgencyId);
    }
}
