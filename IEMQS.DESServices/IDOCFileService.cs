﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using System;
using System.Collections.Generic;

namespace IEMQS.DESServices
{
    public interface IDOCFileService
    {
        Int64 AddEdit(DOCFileModel model, out string errorMsg);

        List<SP_DES_GET_DOCUMENTMAPPING_Result> GetDocumentMappingList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, Int64? DocId, bool IsLatest, string VersionGUID, string currentUser, string Roles, out int recordsTotal, out string errorMsg);

        Int64 UpdateIsMainFileStatus(DOCFileModel model, out string errorMsg);

        Int64? UpdateDOCLifeCycle(int DocumentID, string Project, out string errorMsg);

        Int64 AddCheckedInOUT(CheckeINOUT CheckIn, out string infoMsg);

        Int64 DeleteDocFile(Int64 Id, out string errorMsg);

        Int64 UpdateFileOnCheckOutMark(DOCFileModel model);

        List<DOCFileModel> GetDocumentListByDocId(Int64? DocID);

        Int64? UpdateDOCPolicy(Int64 DocumentLifeCycleId, string Project, out string errorMsg, out string infoMsg, out string armMsg);

        Int64? ReviseDOCPolicy(string ReturnNotes, Int64? DocumentId,string Project, out string errorMsg);

        Int64 UpdateDOCRemark(DOCFileModel model);

        bool AddRevision(DOCFileModel model, out string errorMsg);
    }
}
