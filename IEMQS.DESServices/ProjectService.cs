﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IEMQS.DESServices
{
    public class ProjectService : IProjectService
    {
        public Int64 AddEdit(FavoriteProjectModel model, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";

                    string psno = CommonService.objClsLoginInfo.UserName;
                    var temp = db.DES052.Where(x => x.PSNumber == psno && x.Project == model.Project).FirstOrDefault();
                    DES052 _DES052 = temp == null ? new DES052() : temp;
                    _DES052.Project = model.Project;
                    _DES052.PSNumber = psno;
                    _DES052.IsFavorite = model.IsFavorite;


                    if (temp == null)
                    {
                        _DES052.CreatedBy = psno;
                        _DES052.CreatedOn = DateTime.Now;
                        db.DES052.Add(_DES052);
                    }
                    else
                    {
                        _DES052.EditedBy = psno;
                        _DES052.EditedOn = DateTime.Now;
                    }
                    db.SaveChanges();
                    return _DES052.FavoriteId;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<FavoriteProjectModel> GetAllFavoriteProject(string PsNo)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES052.Where(w => w.PSNumber == PsNo && w.IsFavorite == true).OrderByDescending(x => x.FavoriteId).Select(x => new FavoriteProjectModel
                    {
                        FavoriteId = x.FavoriteId,
                        Project = x.Project,
                        PSNumber = x.PSNumber,
                        IsFavorite = x.IsFavorite ?? false
                    }).Take(12).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_LANDING_PAGE_PROJECT_Result> GetFavoriteList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    string PSNo = CommonService.objClsLoginInfo.UserName;
                    string Roles = CommonService.objClsLoginInfo.UserRoles;
                    var data = db.SP_DES_GET_LANDING_PAGE_PROJECT(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, PSNo, Roles).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_PROJECTLIST_Result> GetProjectList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, string PSNo, out int recordsTotal)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_PROJECTLIST(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, PSNo).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public ProjectModel GetProjectByProjectNo(string ProjectNo)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var _DES051 = db.DES051.Where(x => x.Project == ProjectNo).FirstOrDefault();
                    ProjectModel _ProjectModel = new ProjectModel();
                    if (_DES051 != null)
                    {
                        _ProjectModel.Project = _DES051.Project;
                        _ProjectModel.MainProject = _DES051.MainProject;
                        _ProjectModel.Contract = _DES051.Contract;
                        _ProjectModel.ContractName = db.SP_DES_GET_CONTRACT_NAME_BY_CODE(_DES051.Contract).FirstOrDefault();
                        _ProjectModel.ProjectDescription = _DES051.ProjectDescription;

                        _ProjectModel.BUCode = _DES051.BUCode;
                        _ProjectModel.BU = GetBUNameFromBUCode(_DES051.BUCode);
                        _ProjectModel.PBU = _DES051.PBU;
                        _ProjectModel.Customer = db.SP_DES_GET_CUSTOMERNAMEBYCODE(_DES051.Customer).FirstOrDefault();
                        _ProjectModel.CustomerLOINumber = _DES051.CustomerLOINumber;
                        _ProjectModel.CustomerPONumber = _DES051.CustomerPONumber;
                        _ProjectModel.Owner = new clsManager().GetUserNameFromPsNo((_DES051.Owner));
                        _ProjectModel.ProductType = _DES051.ProductType;
                        _ProjectModel.Consultant = _DES051.Consultant;
                        _ProjectModel.Licensor = _DES051.Licensor;
                        _ProjectModel.DesignCode = _DES051.DesignCode;
                        _ProjectModel.CodeStamp = _DES051.CodeStamp;
                        _ProjectModel.RegulatoryRequirement = _DES051.RegulatoryRequirement;
                        _ProjectModel.MajorMaterial = _DES051.MajorMaterial;
                        _ProjectModel.EquipmentDiameter = _DES051.EquipmentDiameter;
                        _ProjectModel.TLtoTL = _DES051.TLtoTL;
                        _ProjectModel.FabricatedWeight = _DES051.FabricatedWeight;
                        _ProjectModel.LOIDate = _DES051.LOIDate;
                        _ProjectModel.PIMDate = _DES051.PIMDate;
                        _ProjectModel.PODate = _DES051.PODate;
                        _ProjectModel.ContractualDeliveryDate = _DES051.ContractualDeliveryDate;
                        _ProjectModel.SiteWorkRequire = _DES051.SiteWorkRequire;
                        _ProjectModel.DeliveryCondition = _DES051.DeliveryCondition;
                        _ProjectModel.DeliveryTerms = _DES051.DeliveryTerms;
                        _ProjectModel.ProductEndUser = _DES051.ProductEndUser;
                        _ProjectModel.ZeroDate = _DES051.ZeroDate;
                        _ProjectModel.EnquiryName = _DES051.EnquiryName;
                        _ProjectModel.OrderCategory = _DES051.OrderCategory;
                        _ProjectModel.Status = _DES051.Status;
                        _ProjectModel.CreatedOn = _DES051.CreatedOn;
                        _ProjectModel.JEPNumber = db.SP_DES_GET_PROJECT_JEPNO(ProjectNo).FirstOrDefault();
                        _ProjectModel.WarrantyExpiryDate = _DES051.WarrantyExpiryDate;
                        _ProjectModel.WarrantyTerms = _DES051.WarrantyTerms;
                        _ProjectModel.Edition = _DES051.Edition;
                        _ProjectModel.Location = _DES051.location;
                        if (!string.IsNullOrEmpty(_DES051.CompletedBy))
                        {
                            _ProjectModel.CompletedBy = new clsManager().GetUserNameFromPsNo((_DES051.CompletedBy).ToString());
                        }
                        _ProjectModel.CompletedOn = _DES051.CompletedOn;
                        if (!string.IsNullOrEmpty(_DES051.CreatedBy))
                        {
                            _ProjectModel.CreatedBy = new clsManager().GetUserNameFromPsNo((_DES051.CreatedBy).ToString());
                        }
                        if (!string.IsNullOrEmpty(_DES051.InProcessBy))
                        {
                            _ProjectModel.InProcessBy = new clsManager().GetUserNameFromPsNo((_DES051.InProcessBy).ToString());
                        }
                        _ProjectModel.InProcessOn = _DES051.InProcessOn;
                        _ProjectModel.CustomerForDINReport = _DES051.CustomerForDINReport;
                        _ProjectModel.Description = _DES051.Description;
                        _ProjectModel.DesignGroup = _DES051.DesignGroup;
                        if (!string.IsNullOrEmpty(_DES051.EditedBy))
                        {
                            _ProjectModel.EditedBy = new clsManager().GetUserNameFromPsNo((_DES051.EditedBy).ToString());
                        }
                        _ProjectModel.EditedOn = _DES051.EditedOn;
                        _ProjectModel.EquipmentName = _DES051.EquipmentName;
                        _ProjectModel.EquipmentNo = _DES051.EquipmentNo;
                        _ProjectModel.ProductionCenter = _DES051.ProductionCenter;
                        _ProjectModel.ProjectPMG = _DES051.ProjectPMG;
                        _ProjectModel.CreatedByPsno = _DES051.CreatedBy;
                        if (!string.IsNullOrEmpty(_ProjectModel.ProjectPMG))
                        {
                            _ProjectModel.ProjectPMGPsNo = _ProjectModel.ProjectPMG;
                            _ProjectModel.ProjectPMG = new clsManager().GetUserNameFromPsNo(_ProjectModel.ProjectPMG);
                        }
                        else
                        {
                            _ProjectModel.ProjectPMG = new clsManager().GetUserNameFromPsNo(_ProjectModel.Owner);
                        }
                        _ProjectModel.DesignCode = GetDesignCodeByProjectNo(ProjectNo, "DesignCode", _DES051.BUCode, CommonService.objClsLoginInfo.Location);
                    }
                    return _ProjectModel;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        private string GetBUNameFromBUCode(string bUCode)
        {
            try
            {
                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    string BU = string.Empty;
                    if (!string.IsNullOrEmpty(bUCode))
                    {
                        BU = dbContext.DES001.Where(d => d.BUCode == bUCode).FirstOrDefault().BU;
                    }
                    return BU;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public ProjectModel EditProject(ProjectModel model)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    string psNo = CommonService.objClsLoginInfo.UserName;
                    var _DES051 = db.DES051.Find(model.Project);
                    _DES051.Consultant = model.Consultant;
                    _DES051.DesignCode = model.DesignCode;
                    _DES051.CodeStamp = model.CodeStamp;
                    _DES051.RegulatoryRequirement = model.RegulatoryRequirement;
                    _DES051.MajorMaterial = model.MajorMaterial;
                    _DES051.EquipmentDiameter = model.EquipmentDiameter;
                    _DES051.TLtoTL = model.TLtoTL;
                    _DES051.FabricatedWeight = model.FabricatedWeight;
                    _DES051.SiteWorkRequire = model.SiteWorkRequire;
                    _DES051.DeliveryCondition = model.DeliveryCondition;
                    _DES051.DeliveryTerms = model.DeliveryTerms;
                    _DES051.ProductEndUser = model.ProductEndUser;
                    _DES051.OrderCategory = model.OrderCategory;
                    _DES051.EditedOn = DateTime.Now;
                    _DES051.EditedBy = psNo;
                    _DES051.JEPNumber = model.JEPNumber;
                    _DES051.WarrantyTerms = model.WarrantyTerms;
                    _DES051.WarrantyExpiryDate = model.WarrantyExpiryDate;
                    _DES051.ProjectPMG = model.ProjectPMGPsNo;
                    _DES051.Edition = model.Edition;
                    _DES051.Licensor = model.Licensor;
                    _DES051.location = model.Location;
                    _DES051.CustomerLOINumber = model.CustomerLOINumber;
                    _DES051.LOIDate = model.LOIDate;
                    db.SaveChanges();
                    return model;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public bool UpdateProjectStatus(string Project, string Status, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    string psNo = CommonService.objClsLoginInfo.UserName;
                    errorMsg = string.Empty;
                    var _DES051 = db.DES051.Find(Project);

                    var check = db.DES008.Where(x => x.DES001.BUCode == _DES051.BUCode && x.DES001.PBU == _DES051.PBU).ToList();
                    if (check != null && check.Count > 0)
                    {
                        if (Status == ObjectStatus.InProcess.ToString())
                        {
                            var IsFolderCreated = db.DES054.Where(n => n.ProjectNo == Project).ToList();
                            if (IsFolderCreated.Count == 0)
                            {
                                foreach (var D8 in check.Where(x => !(x.ParentFolderId > 0)).ToList())
                                {
                                    DES054 _DES054 = new DES054();
                                    _DES054.ProjectNo = Project;
                                    _DES054.FolderName = D8.FolderName;
                                    _DES054.FolderOrderNo = D8.FolderOrderNo;
                                    _DES054.Description = D8.Description;
                                    _DES054.CreatedBy = psNo;
                                    _DES054.CreatedOn = DateTime.Now;
                                    _DES054.IsSystemGenerated = true;

                                    _DES054.DES055 = D8.DES009.Select(z => new DES055
                                    {
                                        RoleGroup = z.RoleGroup,
                                    }).ToList();

                                    _DES054.DES057 = D8.DES010.Select(z => new DES057
                                    {
                                        MappingId = z.MappingId,
                                        TypeId = z.TypeId
                                    }).ToList();

                                    db.DES054.Add(_DES054);
                                    db.SaveChanges();
                                    foreach (var D8_1 in check.Where(x => x.ParentFolderId == D8.FolderId).ToList())
                                    {
                                        DES054 _DES054_1 = new DES054();
                                        _DES054_1.ProjectNo = Project;
                                        _DES054_1.ParentProjectFolderId = _DES054.ProjectFolderId;
                                        _DES054_1.FolderName = D8_1.FolderName;
                                        _DES054_1.FolderOrderNo = D8_1.FolderOrderNo;
                                        _DES054_1.Description = D8_1.Description;
                                        _DES054_1.CreatedBy = psNo;
                                        _DES054_1.CreatedOn = DateTime.Now;
                                        _DES054_1.IsSystemGenerated = true;
                                        _DES054_1.DES057 = D8_1.DES010.Select(z => new DES057
                                        {
                                            MappingId = z.MappingId,
                                            TypeId = z.TypeId
                                        }).ToList();
                                        db.DES054.Add(_DES054_1);
                                        db.SaveChanges();
                                        foreach (var D8_2 in check.Where(x => x.ParentFolderId == D8_1.FolderId).ToList())
                                        {
                                            DES054 _DES054_2 = new DES054();
                                            _DES054_2.ProjectNo = Project;
                                            _DES054_2.ParentProjectFolderId = _DES054_1.ProjectFolderId;
                                            _DES054_2.FolderName = D8_2.FolderName;
                                            _DES054_2.FolderOrderNo = D8_2.FolderOrderNo;
                                            _DES054_2.Description = D8_2.Description;
                                            _DES054_2.CreatedBy = psNo;
                                            _DES054_2.CreatedOn = DateTime.Now;
                                            _DES054_2.IsSystemGenerated = true;
                                            _DES054_2.DES057 = D8_2.DES010.Select(z => new DES057
                                            {
                                                MappingId = z.MappingId,
                                                TypeId = z.TypeId
                                            }).ToList();
                                            db.DES054.Add(_DES054_2);
                                            db.SaveChanges();
                                            foreach (var D8_3 in check.Where(x => x.ParentFolderId == D8_2.FolderId).ToList())
                                            {
                                                DES054 _DES054_3 = new DES054();
                                                _DES054_3.ProjectNo = Project;
                                                _DES054_3.ParentProjectFolderId = _DES054_2.ProjectFolderId;
                                                _DES054_3.FolderName = D8_3.FolderName;
                                                _DES054_3.FolderOrderNo = D8_3.FolderOrderNo;
                                                _DES054_3.Description = D8_3.Description;
                                                _DES054_3.CreatedBy = psNo;
                                                _DES054_3.CreatedOn = DateTime.Now;
                                                _DES054_3.IsSystemGenerated = true;
                                                _DES054_3.DES057 = D8_3.DES010.Select(z => new DES057
                                                {
                                                    MappingId = z.MappingId,
                                                    TypeId = z.TypeId
                                                }).ToList();
                                                db.DES054.Add(_DES054_3);
                                                db.SaveChanges();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        DES053 _DES053 = new DES053();
                        _DES053.Project = Project;
                        _DES053.Description = "Project Status updated from " + _DES051.Status + " To " + Status;
                        _DES053.CreatedBy = psNo;
                        _DES053.CreatedOn = DateTime.Now;
                        _DES051.DES053.Add(_DES053);
                        _DES051.Status = Status;
                        if (Status == ObjectStatus.InProcess.ToString())
                        {
                            _DES051.InProcessOn = DateTime.Now;
                            _DES051.InProcessBy = psNo;
                        }
                        else if (Status == ObjectStatus.Completed.ToString())
                        {
                            _DES051.CompletedOn = DateTime.Now;
                            _DES051.CompletedBy = psNo;
                        }
                        db.SaveChanges();

                        return true;
                    }
                    else
                    {
                        errorMsg = "Folder Structure not exist for this BU.";
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public ProjectFolderModel ProjectFolderList(string ProjectNo, string PSNo, string Department)
        {
            try
            {
                ProjectFolderModel _ProjectFolderModel = new ProjectFolderModel();
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var _DES054 = db.DES054.Where(x => x.ProjectNo == ProjectNo).ToList();
                    var _DES051 = db.DES051.Where(x => x.Project == ProjectNo).FirstOrDefault();
                    if (_DES054 != null && _DES051 != null)
                    {
                        var _DES001 = db.DES001.Where(x => x.BUCode == _DES051.BUCode && x.PBU == _DES051.PBU).FirstOrDefault();
                        if (_DES001 != null)
                        {
                            _ProjectFolderModel.Contract = _DES051.Contract;
                            _ProjectFolderModel.ProjectNo = ProjectNo;
                            _ProjectFolderModel.BUCode = _DES051.BUCode;
                            _ProjectFolderModel.BUId = _DES001.BUId;

                            var getLoginInfo = CommonService.objClsLoginInfo;
                            _ProjectFolderModel.FolderList = db.SP_DES_GET_PROJECT_FOLDERLIST(ProjectNo, Department, getLoginInfo.GetUserRole(), PSNo, _DES051.Contract, _DES051.BUCode, getLoginInfo.Location).ToList();
                        }
                    }
                }
                return _ProjectFolderModel;
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_PROJECTDETAILS_ACCESS_Result> GetProjectAccessList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, string ProjectNo, string ContractNo, string BUCode)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_PROJECTDETAILS_ACCESS(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, BUCode, ProjectNo, ContractNo).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public FolderListModel GetCategoryActionByProjectNoParentId(string ProjectNo, Int64? ParentProjectFolderId, bool EditModeOnFlag)
        {
            try
            {
                FolderListModel model = new FolderListModel();
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var DES054GetAll = db.DES054.ToList();
                    if (ParentProjectFolderId > 0)
                    {
                        var _DES054 = DES054GetAll.Where(x => x.ProjectFolderId == ParentProjectFolderId && x.ProjectNo == ProjectNo).FirstOrDefault();
                        if (_DES054 != null && _DES054.ProjectFolderId > 0)
                        {
                            if (EditModeOnFlag)
                            {
                                model.ProjectFolderId = _DES054.ProjectFolderId;
                                model.FolderName = _DES054.FolderName;
                                model.Description = _DES054.Description;
                                model.ParentProjectFolderId = _DES054.ParentProjectFolderId;
                                model.ProjectNo = _DES054.ProjectNo;
                                model.IsSystemGenerated = Convert.ToBoolean(_DES054.IsSystemGenerated);
                            }
                            else
                            {
                                model.ParentProjectFolderId = ParentProjectFolderId;
                            }
                        }
                    }
                    else
                    {
                        model.ParentProjectFolderId = ParentProjectFolderId;
                    }

                    var ListCatAct = db.SP_DES_GET_CATEGORY_ACTION_BY_PROJECTNO_PARENTFOLDERID(model.ParentProjectFolderId, ProjectNo).ToList();

                    var _D54 = db.DES054.Where(x => x.ProjectFolderId == model.ProjectFolderId && x.ProjectNo == ProjectNo).FirstOrDefault();
                    model.ListCategory = ListCatAct.Where(x => x.TypeId == (int)MenuType.Category).Select(x => new CategoryActionModel
                    {
                        MappingId = x.MappingId,
                        TypeId = x.TypeId,
                        Name = x.Name,
                        IsChecked = (_D54 != null) ? (_D54.DES057.Select(y => y.MappingId).ToList()).Contains(x.MappingId) : false
                    }).ToList();

                    model.ListAction = ListCatAct.Where(x => x.TypeId == (int)MenuType.Action).Select(x => new CategoryActionModel
                    {
                        MappingId = x.MappingId,
                        TypeId = x.TypeId,
                        Name = x.Name,
                        IsChecked = (_D54 != null) ? (_D54.DES057.Select(y => y.MappingId).ToList()).Contains(x.MappingId) : false
                    }).ToList();
                }
                return model;
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public bool IsAlreadyExist(string FolderName, Int64? ParentProjectFolderId, string ProjectNo)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.DES054.Where(x => x.ProjectNo == ProjectNo && x.FolderName.ToLower().Trim() == FolderName.ToLower().Trim() && (x.ParentProjectFolderId == ParentProjectFolderId)).ToList();
                    if (data != null && data.Count > 0)
                    {
                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public Int64 DeleteFolder(Int64 Id, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    var check = db.DES054.Find(Id);

                    if (check != null)
                    {
                        var checkchildobjects = db.SP_DES_GET_ALL_CHILD_BY_PROJECT_FOLDERID(check.ProjectNo, Id).ToList();
                        if (checkchildobjects.Count > 0)
                        {
                            errorMsg = "The folder is not empty or any object of this folder is created";
                            return 0;
                        }
                        else
                        {
                            List<DES054> _DES054List = db.DES054.Where(x => x.ParentProjectFolderId == Id).ToList();
                            if (_DES054List.Count > 0)
                            {
                                _DES054List.ForEach(x =>
                                {
                                    db.DES057.RemoveRange(x.DES057);
                                });
                                db.DES054.RemoveRange(_DES054List);
                            }

                            db.DES057.RemoveRange(check.DES057);
                            db.DES054.Remove(check);
                            db.SaveChanges();
                        }
                        return check.ProjectFolderId;
                    }
                    return 0;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public long AddEditProjectFolder(FolderListModel model, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = string.Empty;
                    var temp = db.DES054.Find(model.ProjectFolderId);
                    DES054 _DES054 = temp == null ? new DES054() : temp;

                    _DES054.FolderName = model.FolderName;
                    _DES054.Description = model.Description;
                    _DES054.IsSystemGenerated = false;
                    _DES054.ProjectNo = model.ProjectNo;

                    if (_DES054 != null && _DES054.ProjectFolderId > 0)
                    {
                        _DES054.EditedBy = model.EditedBy;
                        _DES054.EditedOn = DateTime.Now;
                    }
                    else
                    {
                        _DES054.ParentProjectFolderId = model.ParentProjectFolderId;
                        _DES054.CreatedBy = CommonService.objClsLoginInfo.UserName;
                        _DES054.CreatedOn = DateTime.Now;

                        var temp1 = db.DES054.Find(model.ParentProjectFolderId);
                        DES054 _DES0541 = temp1 == null ? new DES054() : temp1;

                        List<DES057> _DES057List = new List<DES057>();
                        foreach (var categoryitem in _DES0541.DES057)
                        {
                            DES057 _DES057 = new DES057();
                            _DES057.MappingId = Convert.ToInt64(categoryitem.MappingId);
                            _DES057.TypeId = categoryitem.TypeId;
                            _DES057List.Add(_DES057);
                        }
                        _DES054.DES057 = _DES057List;
                        db.DES054.Add(_DES054);
                    }

                    db.SaveChanges();
                    return _DES054.ProjectFolderId;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<string> ProjectSearch(string Status, string Key)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    Key = string.IsNullOrEmpty(Key) ? Key : Key.Trim().ToLower();
                    if (Status == "All")
                        return db.DES051.Where(x => x.Project.ToLower().Contains(Key.ToLower())).Select(x => x.Project).ToList();
                    else
                        return db.DES051.Where(x => x.Status == Status && x.Project.ToLower().Contains(Key.ToLower())).Select(x => x.Project).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public ProjectFolderModel GetFolder(Int64 Id, Int64? BUId, List<string> UserRole, string Project)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    ProjectFolderModel _ProjectFolderModel = new ProjectFolderModel();
                    var temp = db.DES054.Find(Id);
                    var getDefault = db.DES002.Where(x => x.IsDefault == true && x.UserRoles != null).ToList().Where(x => x.UserRoles == "All" || x.UserRoles.Split(',').Any(y => UserRole.Contains(y))).Select(b => new CategoryActionModel
                    {
                        TypeId = b.TypeId,
                        MappingId = b.MappingId,
                        Name = b.Name,
                        URL = b.URL.Replace("#ProjectNo#", Project),
                        ParentId = b.ParentId,
                        IsTarget = b.IsTarget ?? false
                    });

                    if (temp != null)
                    {
                        if (temp.DES055 != null)
                            _ProjectFolderModel.RoleGroup = string.Join(",", temp.DES055.Select(x => x.RoleGroup).ToList());

                        var AcList = temp.DES057.Where(x => x.TypeId == (int)MenuType.Action).Select(b => new CategoryActionModel
                        {
                            MappingId = b.MappingId,
                            Name = b.DES002.Name,
                            URL = b.DES002.URL,
                            ParentId = b.DES002.ParentId,
                            IsTarget = b.DES002.IsTarget ?? false
                        });

                        var Aclist1 = getDefault.Where(x => x.TypeId == (int)MenuType.Action);
                        _ProjectFolderModel.ListAction = AcList.Union(Aclist1).ToList();


                        var CList = temp.DES057.Where(x => x.TypeId == (int)MenuType.Category).Select(b => new CategoryActionModel
                        {
                            MappingId = b.MappingId,
                            Name = b.DES002.Name,
                            URL = b.DES002.URL,
                            ParentId = b.DES002.ParentId,
                            IsTarget = b.DES002.IsTarget ?? false
                        });
                        var CList1 = getDefault.Where(x => x.TypeId == (int)MenuType.Category);
                        _ProjectFolderModel.ListCategory = CList.Union(CList1).ToList();
                    }
                    else
                    {
                        var list = db.SP_DES_GET_CATEGORY_ACTION_BY_BUID_PARENTFOLDERID(BUId, Id, 0).ToList();

                        var CList = list.Where(x => x.TypeId == (int)MenuType.Category).Select(b => new CategoryActionModel
                        {
                            MappingId = b.MappingId,
                            Name = b.Name,
                            URL = b.URL
                        }).ToList();
                        var CList1 = getDefault.Where(x => x.TypeId == (int)MenuType.Category);
                        _ProjectFolderModel.ListCategory = CList.Union(CList1).ToList();
                    }
                    return _ProjectFolderModel;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public Int64 AddProjectUserFolderAuthorize(ProjectUserFolderAuthorizeModel model)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    string psno = CommonService.objClsLoginInfo.UserName;
                    var temp = db.DES056.Where(u => u.ProjectFolderId == model.ProjectFolderId && u.Project == model.Project && u.RequestStatus == "InProcess" && u.PSNumber == psno).FirstOrDefault();

                    if (!(temp != null))
                    {
                        DES056 _DES056 = new DES056();
                        _DES056.ProjectFolderId = model.ProjectFolderId;
                        _DES056.PSNumber = psno;
                        _DES056.Project = model.Project;
                        _DES056.EmailText = model.EmailText;
                        _DES056.RequestStatus = ObjectStatus.InProcess.ToString();
                        _DES056.Location = model.Location;
                        _DES056.FunctionId = model.FunctionId;
                        _DES056.BUID = model.BUID;
                        _DES056.CreatedBy = psno;
                        _DES056.CreatedOn = DateTime.Now;
                        db.DES056.Add(_DES056);
                        db.SaveChanges();


                        var listPSNo = new DepartmentGroupService().GetPSNumberList(model.FunctionId).Select(x => new { id = x.t_psno, text = x.t_namb }).ToList();

                        foreach (var item in listPSNo)
                        {
                            var userroles = db.SP_DES_GET_USER_ROLE_ROLEGROUP(item.id).Select(x => x.Role).ToList();
                            var userrolegroup = db.SP_DES_GET_USER_ROLE_ROLEGROUP(item.id).Select(x => x.RoleGroup).ToList();
                            string strroles = string.Join(",", userroles);
                            string strrolegroup = string.Join(",", userrolegroup);
                            var level = new CommonService().GetUserLevel(strrolegroup, strroles);
                            if (level != null && level.Value < 3)
                                db.SP_DES_PROJECT_ACCESS_PUSH_NOTIFICATION(model.Project, CommonService.objClsLoginInfo.UserName, strroles, item.id, psno);
                        }
                        return _DES056.ProjectUserFolderAuthorizeId;
                    }
                    return 0;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<RecentProjectModel> GetAllRecentProject(string PsNo)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES058.Where(x => x.PSNumber == PsNo).OrderByDescending(x => x.CreatedOn).Select(x => new RecentProjectModel
                    {
                        RecentId = x.RecentId,
                        Project = x.Project,
                        PSNumber = x.PSNumber,
                        CreatedBy = x.CreatedBy,
                        CreatedOn = x.CreatedOn,
                    }).Take(12).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public Int64 AddRecentProject(string PsNo, string project)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    DES058 data = db.DES058.Where(x => x.PSNumber == PsNo && x.Project == project).FirstOrDefault();
                    if (data != null)
                    {
                        data.CreatedBy = PsNo;
                        data.CreatedOn = DateTime.Now;
                        db.SaveChanges();
                        return data.RecentId;
                    }
                    List<DES058> list = db.DES058.Where(x => x.PSNumber == PsNo).OrderBy(x => x.CreatedOn).ToList();
                    if (list != null && list.Count >= 12)
                    {
                        DES058 data1 = list.FirstOrDefault();
                        data1.Project = project;
                        data1.CreatedBy = PsNo;
                        data1.CreatedOn = DateTime.Now;
                        db.SaveChanges();
                        return data1.RecentId;
                    }
                    else
                    {
                        DES058 data1 = new DES058();
                        data1.Project = project;
                        data1.PSNumber = PsNo;
                        data1.CreatedBy = PsNo;
                        data1.CreatedOn = DateTime.Now;
                        db.DES058.Add(data1);
                        db.SaveChanges();
                        return data1.RecentId;
                    }
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        #region MAINTAIN PROJECT DETAILS

        /// <summary>
        /// Maintain Project Details | Get Project by Project No from Session
        /// </summary>
        /// <param name="ProjectNo"></param>
        /// <returns></returns>
        public ProjectModel GetJEPProjectByProjectNo(string ProjectNo)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    ProjectModel objProjectModel = new ProjectModel();
                    objProjectModel = db.DES051.Where(x => x.Project == ProjectNo).Select(x => new ProjectModel
                    {
                        Project = x.Project,
                        MainProject = x.MainProject,
                        Contract = x.Contract,
                        ProjectDescription = x.ProjectDescription,
                        BUCode = x.BUCode,
                        PBU = x.PBU,
                        Customer = x.Customer,
                        CustomerLOINumber = x.CustomerLOINumber,
                        CustomerPONumber = x.CustomerPONumber,
                        Owner = x.Owner,
                        ProductType = x.ProductType,
                        Consultant = x.Consultant,
                        Licensor = x.Licensor,
                        DesignCode = x.DesignCode,
                        CodeStamp = x.CodeStamp,
                        RegulatoryRequirement = x.RegulatoryRequirement,
                        MajorMaterial = x.MajorMaterial,
                        EquipmentDiameter = x.EquipmentDiameter,
                        TLtoTL = x.TLtoTL,
                        FabricatedWeight = x.FabricatedWeight,
                        LOIDate = x.LOIDate,
                        PIMDate = x.PIMDate,
                        PODate = x.PODate,
                        ContractualDeliveryDate = x.ContractualDeliveryDate,
                        SiteWorkRequire = x.SiteWorkRequire,
                        DeliveryCondition = x.DeliveryCondition,
                        DeliveryTerms = x.DeliveryTerms,
                        ProductEndUser = x.ProductEndUser,
                        ZeroDate = x.ZeroDate,
                        EnquiryName = x.EnquiryName,
                        OrderCategory = x.OrderCategory,
                        Status = x.Status,
                        CreatedOn = x.CreatedOn,
                        Description = x.ProjectDescription,
                        EquipmentNo = x.EquipmentNo,
                        EquipmentName = x.EquipmentName,
                        ASMETag = x.ASMETag,
                        ProductionCenter = x.ProductionCenter,
                        DesignGroup = x.DesignGroup,
                        CustomerForDINReport = x.CustomerForDINReport,
                        DrawingSeriesNo = x.DrawingSeriesNo,
                        ProjectPMG = x.ProjectPMG,
                        Edition = x.Edition,
                        Location = x.location

                    }).FirstOrDefault();

                    if (!string.IsNullOrEmpty(objProjectModel.ProjectPMG))
                    {
                        objProjectModel.ProjectPMGPsNo = objProjectModel.ProjectPMG;
                        objProjectModel.ProjectPMG = new clsManager().GetUserNameFromPsNo(objProjectModel.ProjectPMG);
                    }
                    else
                    {
                        objProjectModel.ProjectPMG = new clsManager().GetUserNameFromPsNo(objProjectModel.Owner);
                    }
                    objProjectModel.ISJEPCreated = db.DES074.Any(x => x.Project == objProjectModel.Project);

                    if (objProjectModel.Customer != null)
                    {
                        var Customer = db.SP_DES_GET_CUSTOMERNAMEBYCODE(objProjectModel.Customer).FirstOrDefault();
                        objProjectModel.Customer = Customer;
                    }
                    else
                    {
                        objProjectModel.Customer = "";
                    }
                    return objProjectModel;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        /// <summary>
        /// Maintain Project Details | Edit data save in table DES051
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public string EditJEPProject(ProjectModel model)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    string psNo = CommonService.objClsLoginInfo.UserName;
                    var _DES051 = db.DES051.Find(model.Project);
                    _DES051.Description = model.Description;
                    _DES051.EquipmentNo = model.EquipmentNo;
                    _DES051.EquipmentName = model.EquipmentName;
                    _DES051.CodeStamp = model.CodeStamp;
                    _DES051.ProductionCenter = model.ProductionCenter;
                    _DES051.DesignGroup = model.DesignGroup;
                    _DES051.CustomerForDINReport = model.CustomerForDINReport;
                    _DES051.DrawingSeriesNo = model.DrawingSeriesNo;
                    _DES051.EditedBy = psNo;
                    _DES051.EditedOn = DateTime.Now;
                    _DES051.ProjectPMG = model.ProjectPMGPsNo;
                    db.SaveChanges();
                    return _DES051.Project;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<DINGroupModel> GetAllDesignGroup()
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES033.Select(x => new DINGroupModel
                    {
                        DINGroupId = x.DINGroupId,
                        GroupName = x.GroupName
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        #endregion


        public List<SP_DES_GET_ALL_OBJECTS_BY_FOLDERID_PROJECT_Result> GetAllObjectsList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, string project, string searchObjectType, string searchObjectStatus, string searchObjectName, long? folderId, string Roles, out int recordsTotal)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_ALL_OBJECTS_BY_FOLDERID_PROJECT(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, project, folderId, searchObjectType, searchObjectStatus, searchObjectName, Roles).ToList();
                    recordsTotal = 0;
                    int? rt = 0;
                    data.ForEach(x =>
                    {
                        rt = x.TableCount;
                        x.QString = CommonService.Encrypt("Id=" + x.ObjectId);
                        if (x.Type == ObjectName.JEP.ToString())
                        {
                            x.GString = CommonService.Encrypt("JEPId=" + x.ObjectId);
                        }

                        if (x.Type == ObjectName.Part.ToString())
                        {
                            x.GString = CommonService.Encrypt("ItemId=" + x.ObjectId);
                        }
                    });
                    recordsTotal = rt ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public FindObjectModel FindObject(string Id, String Type)
        {
            try
            {
                FindObjectModel objFindObjectModel = new FindObjectModel();
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    if (Type.ToLower() == ObjectName.DIN.ToString().ToLower())
                    {
                        var dindata = db.DES081.Where(t => t.DINNo == Id).OrderByDescending(t => t.IssueNo).FirstOrDefault();
                        objFindObjectModel.IsExist = (dindata != null) ? true : false;
                        objFindObjectModel.Id = Id;
                        objFindObjectModel.ProjectNo = (dindata != null) ? dindata.ProjectNo : "";
                        objFindObjectModel.PKId = (dindata != null) ? dindata.DINId : 0;

                    }
                    else if (Type.ToLower() == ObjectName.DOC.ToString().ToLower())
                    {
                        var docdata = db.DES059.Where(t => t.DocumentNo == Id).OrderByDescending(t => t.DocumentRevision).FirstOrDefault();
                        objFindObjectModel.IsExist = (docdata != null) ? true : false;
                        objFindObjectModel.Id = Id;
                        objFindObjectModel.ProjectNo = (docdata != null) ? docdata.Project : "";
                        objFindObjectModel.PKId = (docdata != null) ? docdata.DocumentId : 0;
                    }
                    else if (Type.ToLower() == ObjectName.Part.ToString().ToLower())
                    {
                        var partdata = db.DES066.Where(t => t.ItemKey == Id).OrderByDescending(t => t.RevNo).FirstOrDefault();
                        objFindObjectModel.IsExist = (partdata != null) ? true : false;
                        objFindObjectModel.Id = Id;
                        objFindObjectModel.ProjectNo = (partdata != null) ? partdata.Project : "";
                        objFindObjectModel.PKId = (partdata != null) ? partdata.ItemId : 0;
                    }
                    else if (Type.ToLower() == ObjectName.JEP.ToString().ToLower())
                    {
                        var JEPdata = db.DES074.Where(t => t.JEPNumber == Id).FirstOrDefault();
                        objFindObjectModel.IsExist = (JEPdata != null) ? true : false;
                        objFindObjectModel.Id = Id;
                        objFindObjectModel.ProjectNo = (JEPdata != null) ? JEPdata.Project : "";
                        objFindObjectModel.PKId = (JEPdata != null) ? JEPdata.JEPId : 0;
                    }
                    else if (Type.ToLower() == ObjectName.DCR.ToString().ToLower())
                    {
                        var DCRdata = db.DES089.Where(t => t.DCRNo == Id).FirstOrDefault();
                        objFindObjectModel.IsExist = (DCRdata != null) ? true : false;
                        objFindObjectModel.Id = Id;
                        objFindObjectModel.ProjectNo = (DCRdata != null) ? DCRdata.Project : "";
                        objFindObjectModel.PKId = (DCRdata != null) ? DCRdata.DCRId : 0;
                    }
                    else if (Type.ToLower() == "jep-customerfeedback")
                    {
                        string[] custids = Id.Split('-');
                        long JEPCustomerFeedbackDocumentId = Convert.ToInt64(custids[custids.Count() - 1]);
                        var CustomerFeedbackData = db.DES086.Where(d => d.JEPCustFeedbackDocumentId == JEPCustomerFeedbackDocumentId).FirstOrDefault();
                        objFindObjectModel.IsExist = (CustomerFeedbackData != null) ? true : false;
                        objFindObjectModel.Id = (CustomerFeedbackData != null) ? CustomerFeedbackData.DES085.DES075.DES074.JEPNumber : "";
                        objFindObjectModel.ProjectNo = (CustomerFeedbackData != null) ? CustomerFeedbackData.DES085.DES075.DES074.Project : "";
                        objFindObjectModel.PKId = (CustomerFeedbackData != null) ? (long)CustomerFeedbackData.JEPCustomerFeedbackId : 0;
                        objFindObjectModel.JEPId = (CustomerFeedbackData != null) ? (long)CustomerFeedbackData.DES085.DES075.JEPId : 0;
                    }
                    else if (Type.ToLower() == "roc")
                    {
                        string[] custids = Id.Split('-');
                        long ROCCommentedDocId = Convert.ToInt64(custids[custids.Count() - 1]);
                        var ROCData = db.DES070.Where(d => d.ROCCommentedDocId == ROCCommentedDocId).FirstOrDefault();
                        objFindObjectModel.IsExist = (ROCData != null) ? true : false;
                        objFindObjectModel.Id = (ROCData.DES069 != null) ? ROCData.DES069.ROCRefNo : "";
                        objFindObjectModel.ProjectNo = ROCData != null ? ((ROCData.DES069 != null) ? ROCData.DES069.Project : "") : "";
                        objFindObjectModel.PKId = (ROCData != null) ? (long)ROCData.ROCId : 0;
                    }
                    else if (Type.ToLower() == "roc comment")
                    {
                        string[] custids = Id.Split('-');
                        long ROCId = Convert.ToInt64(custids.Last());
                        var ROCData = db.DES069.Where(d => d.ROCId == ROCId).FirstOrDefault();
                        objFindObjectModel.IsExist = (ROCData != null) ? true : false;
                        objFindObjectModel.Id = (ROCData != null) ? ROCData.ROCRefNo : "";
                        objFindObjectModel.ProjectNo = ROCData != null ? ((ROCData != null) ? ROCData.Project : "") : "";
                        objFindObjectModel.PKId = (ROCData != null) ? (long)ROCData.ROCId : 0;
                    }
                    else if (Type.ToLower() == "roc-comment-attached")
                    {
                        string[] custids = Id.Split('-');
                        long ROCCommentsAttachId = Convert.ToInt64(custids[custids.Count() - 1]);
                        var ROCData = db.DES072.Where(d => d.ROCCommentsAttachId == ROCCommentsAttachId).FirstOrDefault();
                        objFindObjectModel.IsExist = (ROCData != null) ? true : false;
                        if (ROCData != null)
                        {
                            var des069s = db.DES069.Where(d => d.ROCId == ROCData.DES071.ROCId).FirstOrDefault();
                            if (des069s != null)
                                objFindObjectModel.ProjectNo = des069s.Project;
                        }
                        objFindObjectModel.PKId = (ROCData != null) ? (ROCData.DES071 != null ? (long)ROCData.DES071.ROCId : 0) : 0;
                    }
                    return objFindObjectModel;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public bool IsAlreadyExistDrawingSeriesNo(string DrawingSeriesNo, string Project)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.DES051.Where(x => x.DrawingSeriesNo == DrawingSeriesNo && x.Project != Project).FirstOrDefault();
                    if (data != null)
                    {
                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public string GetDesignCodeByProjectNo(string Project, string category, string bUCode, string location)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {

                    return string.Join(", ", db.SP_DES_DESIGNCODE_BY_PROJECT(Project, category, bUCode, location));
                    //if (!string.IsNullOrWhiteSpace(DesignCodeByProject))
                    //{
                    //    List<string> lstdn = new List<string>();
                    //    List<string> lst = DesignCodeByProject.Split(',').ToList();
                    //    foreach (var item in lst)
                    //    {
                    //        lstdn.Add(lstDesignCode.Where(a => a.Value == item).Select(a => a.Text).FirstOrDefault());
                    //    }
                    //    ViewBag.DesignCodeDesc = string.Join(", ", lstdn.Where(a => a.ToString() != null).ToList());
                    //}
                    //var objPAM002 = db.PAM002.Where(x => x.Project == ProjectNo).OrderByDescending(x => x.HeaderId).FirstOrDefault();
                    //if (objPAM002 != null)
                    //{
                    //    var objPAM011 = db.PAM011.Where(a => a.PAMHeaderId == objPAM002.HeaderId).FirstOrDefault();
                    //    string designcode = objPAM011?.DesignCode;
                    //    if (!string.IsNullOrWhiteSpace(designcode))
                    //    {
                    //        List<string> lstdn = new List<string>();
                    //        List<string> lst = designcode.Split(',').ToList();
                    //        foreach (var item in lst)
                    //        {
                    //            lstdn.Add(lstDesignCode.Where(a => a.Value == item).Select(a => a.Text).FirstOrDefault());
                    //        }
                    //        ViewBag.DesignCodeDesc = string.Join(", ", lstdn.Where(a => a.ToString() != null).ToList());
                    //    }
                    //    ViewBag.DesignCode = objPAM011?.DesignCode;

                    //    model.DesignCode = objPAM011?.DesignCode;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }


        public string GetObjectNameByObjectId(Int64 ObjectId)
        {
            if (ObjectId > 0)
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var objDES003 = db.DES003.FirstOrDefault(i => i.ObjectId == ObjectId);
                    if (objDES003 != null)
                    {
                        return objDES003.ObjectName;
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            else
            {
                return "";
            }
        }
    }
}
