﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using System;
using System.Collections.Generic;

namespace IEMQS.DESServices
{
    public interface ISizeCodeService
    {
        string AddEdit(SizeCodeModel model, out string errorMsg);
        SizeCodeModel GetSizeCodebyId(long Id);
        List<SizeCodeModel> GetAllSizeCode();
        List<SP_DES_GET_SIZE_CODE_Result> GetSizeCodeList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal);
        Int64 DeleteSizeCode(string Id, out string errorMsg);
    }
}
