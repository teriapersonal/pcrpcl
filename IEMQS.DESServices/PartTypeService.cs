﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IEMQS.DESServices
{
    public class PartTypeService : IPartTypeService
    {
        public Int64 AddEdit(PartTypeModel model, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";

                    #region check Validation
                   
                    var checkdublicate = db.DES040.Where(x => x.ProductForm == model.ProductForm && x.ItemTypeId != model.ItemTypeId).FirstOrDefault();
                    if (checkdublicate != null)
                    {
                        errorMsg = "Product form is already exist";
                        return 0;
                    }
                    
                    #endregion       

                    var temp = db.DES040.Find(model.ItemTypeId);
                    DES040 _DES040 = temp == null ? new DES040() : temp;
                  
                    _DES040.ProductForm = model.ProductForm;
                    _DES040.ItemType = model.ItemType;

                    if (temp == null)
                    {
                        _DES040.CreatedBy = CommonService.objClsLoginInfo.UserName;
                        _DES040.CreatedOn = DateTime.Now;
                        db.DES040.Add(_DES040);
                    }
                    else
                    {
                        _DES040.EditedBy = CommonService.objClsLoginInfo.UserName;
                        _DES040.EditedOn = DateTime.Now;
                    }
                    db.SaveChanges();
                    return _DES040.ItemTypeId;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }       
        
        public List<SP_DES_GET_PART_TYPE_Result> GetPartTypeList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_PART_TYPE(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public Int64 DeletePartType(Int64 Id, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    var check = db.DES040.Find(Id);
                    if (check != null)
                    {                        
                        db.DES040.Remove(check);
                        db.SaveChanges();
                    }
                    return check.ItemTypeId;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<string> GetProductFormList()
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    List<string> uvwMaterials = new List<string>();
                    uvwMaterials = db.uvwMaterial.Select(u => u.t_prod).Distinct().ToList();
                    return uvwMaterials;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

    }
}
