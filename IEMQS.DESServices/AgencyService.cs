﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IEMQS.DESServices
{
    public class AgencyService : IAgencyService
    {
        public Int64 AddEdit(AgencyModel model, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";

                    #region Check Validation                   

                    if (model.AgencyId > 0)
                    {
                        var checkchild = db.DES027.Find(model.AgencyId);
                        if (checkchild != null)
                        {
                            if (checkchild.DES068 != null && checkchild.DES068.Count > 0)
                            {
                                errorMsg = "Agency is already in used. you cannot edit";
                                return 0;
                            }
                            if (checkchild.DES076 != null && checkchild.DES076.Count > 0)
                            {
                                errorMsg = "Agency is already in used. you cannot edit";
                                return 0;
                            }
                        }

                    }

                    var checkdublicate = db.DES027.Where(x => x.AgencyName == model.AgencyName && x.BUId == model.BUId && x.AgencyId != model.AgencyId).FirstOrDefault();
                    if (checkdublicate != null)
                    {
                        errorMsg = "Agency with BU already exist";
                        return 0;
                    }

                    #endregion

                    var temp = db.DES027.Find(model.AgencyId);
                    DES027 _DES027 = temp == null ? new DES027() : temp;

                    _DES027.AgencyName = model.AgencyName;
                    _DES027.AgencyDescription = model.AgencyDescription;
                    _DES027.BUId = model.BUId;

                    if (temp == null)
                    {
                        _DES027.CreatedBy = CommonService.objClsLoginInfo.UserName;
                        _DES027.CreatedOn = DateTime.Now;
                        db.DES027.Add(_DES027);
                    }
                    else
                    {
                        _DES027.EditedBy = CommonService.objClsLoginInfo.UserName;
                        _DES027.EditedOn = DateTime.Now;
                    }
                    db.SaveChanges();
                    return _DES027.AgencyId;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<AgencyModel> GetAllAgency(Int64? BUId)
        {
            try
            {

                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES027.OrderBy(x => x.AgencyId).Where(x=>x.BUId==BUId).Select(x => new AgencyModel
                    {
                        AgencyId = x.AgencyId,
                        AgencyName = x.AgencyName,
                        AgencyDescription = x.AgencyDescription,
                        BUId = x.BUId,
                        AgencyCheckInBy = x.AgencyCheckInBy,
                        AgencyCheckInOn = x.AgencyCheckInOn,
                        IsAgencyCheckIn = x.IsAgencyCheckIn ?? false
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<AgencyModel> GetAllAgency()
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES027.OrderBy(x => x.AgencyId).Select(x => new AgencyModel
                    {
                        AgencyId = x.AgencyId,
                        AgencyName = x.AgencyName,
                        AgencyDescription = x.AgencyDescription,
                        BUId = x.BUId,
                        AgencyCheckInBy = x.AgencyCheckInBy,
                        AgencyCheckInOn = x.AgencyCheckInOn,
                        IsAgencyCheckIn = x.IsAgencyCheckIn ?? false
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<AgencyModel> GetAgencybyBU(Int64 BUId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES027.Where(x => x.BUId == BUId).Select(x => new AgencyModel
                    {
                        AgencyId = x.AgencyId,
                        AgencyName = x.AgencyName,
                        AgencyDescription = x.AgencyDescription,
                        BUId = x.BUId
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_AGENCY_Result> GetAgencyList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_AGENCY(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public Int64 DeleteAgency(Int64 Id, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    var check = db.DES027.Find(Id);
                    if (check != null)
                    {
                        if (check.DES068 != null && check.DES068.Count > 0)
                        {
                            errorMsg = "Agency is already in used. you cannot delete";
                            return 0;
                        }
                        if (check.DES076 != null && check.DES076.Count > 0)
                        {
                            errorMsg = "Agency is already in used. you cannot delete";
                            return 0;
                        }

                        db.DES027.Remove(check);
                        db.SaveChanges();
                        return check.AgencyId;
                    }
                    return 0;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public bool UpdateIsCheckInAgency(Int64? AgencyId, bool IsAgencyCheckIn)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var temp = db.DES027.Find(AgencyId);
                    if (temp != null)
                    {
                        temp.AgencyCheckInBy = CommonService.objClsLoginInfo.UserName;
                        temp.AgencyCheckInOn = DateTime.Now;
                        if (!IsAgencyCheckIn)
                        {
                            temp.AgencyCheckInBy = temp.AgencyCheckInBy;
                            temp.AgencyCheckInOn = null;
                        }
                        temp.IsAgencyCheckIn = IsAgencyCheckIn;
                        db.Entry(temp).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        return true;
                    }
                    return false;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public bool IsCheckInAgency(string PsNo, Int64? AgencyId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var temp = db.DES027.Find(AgencyId);
                    if (temp != null && temp.IsAgencyCheckIn == true)
                    {
                        if (temp.AgencyCheckInBy != PsNo)
                        {
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }
    }
}
