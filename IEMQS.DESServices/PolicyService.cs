﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IEMQS.DESServices
{
    public class PolicyService : IPolicyService
    {
        public Int64 AddEdit(PolicyModel model, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    var temp = db.DES005.Find(model.PolicyId);
                    #region check Validation  
                    if (model.PolicyStepList == null || model.PolicyStepList.Count(x => x.IsDelete != true) == 0) //x.IsDelete==false
                    {
                        errorMsg = "Policy step(s) are required";
                        return 0;
                    }
                    if (temp != null)
                    {
                        if (temp.DES007 != null && temp.DES007.Count > 0)
                        {
                            errorMsg = "Policy is already in used. you cannot edit";
                            return 0;
                        }
                    }
                    #endregion

                    DES005 _DES005 = temp == null ? new DES005() : temp;
                    if (temp != null)
                    {
                        db.DES006.RemoveRange(_DES005.DES006);
                    }

                    _DES005.Name = model.Name;
                    _DES005.Description = model.Description;
                    List<DES006> _DES006List = new List<DES006>();
                    foreach (var d6 in model.PolicyStepList)
                    {
                        if (!d6.IsDelete) //d6.IsDelete == false
                            _DES006List.Add(new DES006
                            {
                                StepName = d6.StepName,
                                OrderNo = d6.OrderNo,
                                ColorCode = d6.ColorCode,
                                DesignationLevels = d6.DesignationLevels
                            });
                    }
                    _DES005.DES006 = _DES006List;
                    if (temp == null)
                    {
                        _DES005.CreatedBy = CommonService.objClsLoginInfo.UserName;
                        _DES005.CreatedOn = DateTime.Now;
                        db.DES005.Add(_DES005);
                    }
                    else
                    {
                        _DES005.EditedBy = CommonService.objClsLoginInfo.UserName;
                        _DES005.EditedOn = DateTime.Now;
                    }
                    db.SaveChanges();
                    return _DES005.PolicyId;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<PolicyModel> GetAllPolicy()
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES005.OrderBy(x => x.PolicyId).Select(x => new PolicyModel
                    {
                        PolicyId = x.PolicyId,
                        Name = x.Name
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public PolicyModel GetPolicybyId(Int64 Id)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES005.Where(x => x.PolicyId == Id).Select(x => new PolicyModel
                    {
                        PolicyId = x.PolicyId,
                        Name = x.Name,
                        Description = x.Description,
                        PolicyStepList = x.DES006.Select(y => new PolicyStepModel
                        {
                            PolicyStepId = y.PolicyStepId,
                            PolicyId = y.PolicyId,
                            StepName = y.StepName,
                            OrderNo = y.OrderNo,
                            ColorCode = y.ColorCode,
                            DesignationLevels = y.DesignationLevels
                        }).ToList(),
                    }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_POLICY_Result> GetPolicyList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_POLICY(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    return data;

                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public Int64 DeletePolicy(Int64 Id, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    var check = db.DES005.Find(Id);
                    if (check != null)
                    {
                        if (check.DES007 != null && check.DES007.Count > 0)
                        {
                            errorMsg = "Policy is already in used. you cannot delete";
                            return 0;
                        }
                        db.DES006.RemoveRange(db.DES006.Where(x => x.PolicyId == check.PolicyId).ToList());
                        db.DES005.Remove(check);
                        db.SaveChanges();
                    }
                    return check.PolicyId;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public bool CheckPolicyNameAlreadyExist(string name, Int64? policyId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    name = name.Trim();
                    var data = db.DES005.Where(x => x.Name == name && (x.PolicyId != policyId || policyId == null)).ToList();
                    if (data != null && data.Count > 0)
                    {
                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                return false;
            }
        }

    }
}
