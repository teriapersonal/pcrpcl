﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace IEMQS.DESServices
{
    public class DESApiPartDeliverablesService
    {
        public static bool Add(ApiPartDeliverablesModel model, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    Int64? iPolicyId = 0;
                    string strType = model.partType;
                    #region check Validation
                    var Project = db.DES051.Where(x => x.Project == model.relatedProjectName).FirstOrDefault();
                    if (Project == null)
                    {
                        errorMsg = "Project does not exist";
                        return false;
                    }

                    var check = db.DES066.Where(x => x.Project == model.relatedProjectName && x.ItemKey == model.partName).FirstOrDefault();
                    if (check != null)
                    {                       
                        return true;
                    }

                    //CHECK PART NAME AND POLICY EXITS OR NOT
                    var _objPartDetails = db.DES003.Where(w => w.ObjectName.ToLower() == model.partType.ToLower().Trim()).FirstOrDefault();
                    if (_objPartDetails != null)
                    {
                        strType = _objPartDetails.ObjectName;
                        var _objPolicyDetails = db.DES007.Where(w => w.ObjectId == _objPartDetails.ObjectId).FirstOrDefault();
                        if (_objPolicyDetails != null)
                            iPolicyId = _objPolicyDetails.PolicyId;
                        else
                        {
                            errorMsg = "Policy does not exits.";
                            return false;
                        }
                    }
                    else
                    {
                        errorMsg = "Part type does not exits.";
                        return false;
                    }
                    #endregion


                    DES066 _DES066 = new DES066();
                    _DES066.Project = model.relatedProjectName;
                    _DES066.Type = strType;
                    _DES066.ItemKey = model.partName;
                    _DES066.RoleGroup = "ENGG,DE";
                    _DES066.ItemKeyType = ItemKeyType.UserDefined.ToString();// "AutoGenerate";
                    _DES066.OrderPolicy = "Customised";
                    _DES066.ManufactureItem = 2;
                    _DES066.RevNo = 0;
                    _DES066.PolicyId = iPolicyId;
                    _DES066.ItemGroup = model.itemGroup;
                    _DES066.ItemName = model.description;
                    _DES066.CreatedBy = model.owner;
                    _DES066.CreatedOn = DateTime.Now;
                    _DES066.Status = ObjectStatus.Created.ToString();
                    _DES066.IsFromLN = true;
                    _DES066.UOM = model.UOM;

                    List<DES095> _DES095List = new List<DES095>();
                    if (_objPartDetails != null)
                    {
                        _objPartDetails.DES007.ToList().ForEach(x =>
                            {
                                x.DES005.DES006.ToList().ForEach(Y =>
                                {
                                    DES095 _DES095 = new DES095();
                                    _DES095.PolicyId = Y.PolicyId;
                                    _DES095.PolicyStep = Y.StepName;
                                    _DES095.PolicyStepId = Y.PolicyStepId;
                                    _DES095.ColorCode = Y.ColorCode;
                                    _DES095.DesignationLevels = Y.DesignationLevels;
                                    _DES095.ItemId = _DES066.ItemId;
                                    _DES095.PolicyOrderId = Y.OrderNo;

                                    if (Y.OrderNo == 1)
                                    {
                                        _DES095.IsCompleted = true;
                                        _DES095.CreatedBy = model.owner;
                                        _DES095.CreatedOn = DateTime.Now;
                                    }
                                    _DES095List.Add(_DES095);
                                });
                            });
                    }
                    _DES066.DES095 = _DES095List;
                    DES096 _DES096 = new DES096();
                    _DES096.Description = "Item " + model.partName + " Created from LN";
                    _DES096.CreatedBy = model.owner;
                    _DES096.CreatedOn = DateTime.Now;
                 //   _DES066.DES096.Add(_DES096); kishan
                    db.DES066.Add(_DES066);
                    db.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                errorMsg = "";
                return false;
            }
        }
    }

    public class ApiPartDeliverablesModel
    {
        [Required(ErrorMessage = "Description is required")]
        [StringLength(30, ErrorMessage = "Description length can't be more than 30")]
        public string description { get; set; }


        public string isTopPart { get; set; }

        [Required(ErrorMessage = "Item Group is required")]
        [StringLength(6, ErrorMessage = "Item Group length can't be more than 200")]
        public string itemGroup { get; set; }

        [Required(ErrorMessage = "Owner is required")]
        [StringLength(9, ErrorMessage = "Owner length can't be more than 9")]
        public string owner { get; set; }

        [Required(ErrorMessage = "Part Name is required")]
        [StringLength(50, ErrorMessage = "Part Name length can't be more than 50")]
        public string partName { get; set; }

        [Required(ErrorMessage = "Part Type is required")]
        [StringLength(47, ErrorMessage = "Part Type length can't be more than 47")]
        public string partType { get; set; }


        public string policy { get; set; }

        [Required(ErrorMessage = "Project Name is required")]
        [StringLength(9, ErrorMessage = "Related Project Name length can't be more than 9")]
        public string relatedProjectName { get; set; }

        [StringLength(6, ErrorMessage = "Related Project Name length can't be more than 6")]
        public string UOM { get; set; }
    }
}
