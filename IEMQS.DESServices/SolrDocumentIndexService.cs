﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IEMQS.DESCore.Data;
using SolrNet.Commands.Parameters;
using IEMQS.DESCore;

namespace IEMQS.DESServices
{
    public class SolrDocumentIndexService : ISolrDocumentIndexService
    {
        public List<DES060> GetNonIndexedFiles()
        {
            try
            {
                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    List<DES060> listofFiles = new List<DES060>();
                    listofFiles = dbContext.SP_DES_GET_NonIndexedFilesForIndexing().Select(d => new DES060
                    {
                        DocumentMappingId = d.DocumentMappingId,
                        DocumentId = d.DocumentId,
                        MainDocumentPath = d.MainDocumentPath
                    }).ToList();
                    //DES060.Where(d => d.isIndexed == false).ToList();
                    return listofFiles;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw ex;
            }
        }

        public List<SP_DES_GET_ADVANCE_SEARCH_DOC_Result> GetAdvanceSerchDoc(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, string DocMapId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_ADVANCE_SEARCH_DOC(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, DocMapId).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    return data;

                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_FileLogs_Result> GetFilelogStatus(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_FileLogs(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    return data;

                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<DocumentIndexModel> Search(string query, int pageNumber, int resultsPerPage,int SortColumn,string Sortdirection, out int recordsTotal)
        {
            try
            {
                //string solrUrl = "http://10.7.66.55:8983/solr/SlrCore";
                string SolrUrl = new CommonService().GetConfigvalues("Solr_URL");
                //string solrUrl = "http://10.7.66.55:8983/solr/QACore";
                IEnumerable<string> documentMappingids = new List<string>();
                string DocumentMappingIds = string.Empty;

                var solrWorker = SolrOperationsCache<DESCore.DocumentIndexModel>.GetSolrOperations(SolrUrl);
                //var sortby = new SortedDictionary<string, string>();
                //sortby.Add("Type", "desc");
                List<SolrNet.SortOrder> sortOrders=new List<SolrNet.SortOrder>();
                string SortClnName = GetColumnNameFromcolumnId(SortColumn);
                sortOrders.Add(new SolrNet.SortOrder(SortClnName, Sortdirection=="desc"? SolrNet.Order.DESC: SolrNet.Order.ASC));
                var p = new Dictionary<string, string>();
                p.Add("wt", "xml");
                QueryOptions options = new QueryOptions
                {
                    Rows = resultsPerPage,
                    Start = pageNumber, //- 1) * resultsPerPage,
                    ExtraParams = p,
                    OrderBy= sortOrders//new SolrNet.SortOrder("Type",SolrNet.Order.DESC)
                };

                List<DocumentIndexModel> results1 = new List<DocumentIndexModel>();

                var results = solrWorker.Query(query, options);

                results1 = results.ToList();
                recordsTotal = results.NumFound;

                //List<SolrSearchResultModel> searchResult = new List<SolrSearchResultModel>();
                //foreach (SolrSearchResultModel indexModel in results)
                //{
                //    searchResult.Add(new SolrSearchResultModel
                //    {
                //        Name=indexModel.Name,

                //    });
                //}

                //DocumentIndexer repository = new DocumentIndexer(solrUrl);

                //documentMappingids = results.Select(tf => tf.ObjectId);

                //foreach (string documentids in documentMappingids)
                //{
                //    DocumentMappingIds += documentids + ",";
                //}
                //var searchResults = new SearchResults();
                //searchResults.Results = results.;
                //searchResults.TotalResults = results.NumFound;
                //searchResults.QueryTime = results.Header.QTime;

                return results1.ToList();
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw ex;
            }
        }

        private string GetColumnNameFromcolumnId(int sortColumn)
        {
            switch (sortColumn)
            {
                case 1:
                    return "Name";
                case 2:
                    return "Revision";
                case 3:
                    return "Type";
                case 4:
                    return "FileName";
                case 5:
                    return "ProjectNumber";
                case 6:
                    return "Status";
                default:
                    return "Name";
            }
        }

        public string UpdateIndexFlag(long DocumentMappingId)
        {
            try
            {
                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    DES060 dES060 = new DES060();
                    dES060 = dbContext.DES060.Where(d => d.DocumentMappingId == DocumentMappingId).FirstOrDefault();
                    if (dES060 != null)
                    {
                        dES060.isIndexed = true;
                        dES060.FileIndexedOn = DateTime.Now;
                        //dES060.EditedBy = "1";
                        //dES060.EditedOn = DateTime.Now;
                        dbContext.SaveChanges();
                        return "File index flag has been updated successfully.";
                    }
                    return "File is not found for update the flag.";
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw ex;
            }
        }

        public string UpdateIndexFlagForCustomerFeedbackfiles(long JEPCustFeedbackDocumentId, string Type, string SolrObjectId, string Project = "")
        {
            try
            {
                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    switch (Type)
                    {
                        case "DOC":
                            DES106 desdoc = new DES106();
                            desdoc = dbContext.DES106.Where(d => d.DocumentMappingId == JEPCustFeedbackDocumentId).FirstOrDefault();
                            if (desdoc != null)
                            {
                                if (string.IsNullOrEmpty(desdoc.SolrObjectId))
                                {
                                    desdoc.SolrObjectId = SolrObjectId;
                                }
                                desdoc.IsIndex = true;
                                desdoc.EditedOn = DateTime.Now;
                                desdoc.EditedBy = 1;
                                //dES060.EditedOn = DateTime.Now;
                                dbContext.SaveChanges();
                                return "File index flag has been updated successfully.";
                            }
                            return "File is not found for update the flag.";

                        case "JEP-CustomerFeedback":
                            DES106 dES106 = new DES106();
                            dES106 = dbContext.DES106.Where(d => d.JEPCustFeedbackDocumentId == JEPCustFeedbackDocumentId).FirstOrDefault();
                            if (dES106 != null)
                            {
                                if (string.IsNullOrEmpty(dES106.SolrObjectId))
                                {
                                    dES106.SolrObjectId = SolrObjectId;
                                }
                                dES106.IsIndex = true;
                                dES106.EditedOn = DateTime.Now;
                                dES106.EditedBy = 1;
                                //dES060.EditedOn = DateTime.Now;
                                dbContext.SaveChanges();
                                return "File index flag has been updated successfully.";
                            }
                            return "File is not found for update the flag.";
                        case "DCR":
                            DES106 dEdcr = new DES106();
                            dEdcr = dbContext.DES106.Where(d => d.DCRSupportedDocId == JEPCustFeedbackDocumentId).FirstOrDefault();
                            if (dEdcr != null)
                            {
                                if (string.IsNullOrEmpty(dEdcr.SolrObjectId))
                                {
                                    dEdcr.SolrObjectId = SolrObjectId;
                                }
                                dEdcr.IsIndex = true;
                                dEdcr.EditedOn = DateTime.Now;
                                dEdcr.EditedBy = 1;
                                //dES060.EditedOn = DateTime.Now;
                                dbContext.SaveChanges();
                                return "File index flag has been updated successfully.";
                            }
                            return "File is not found for update the flag.";
                        case "Part":
                            DES106 dEPart = new DES106();
                            dEPart = dbContext.DES106.Where(d => d.ItemId == JEPCustFeedbackDocumentId).FirstOrDefault();
                            if (dEPart != null)
                            {
                                dEPart.IsIndex = true;
                                dEPart.EditedOn = DateTime.Now;
                                dEPart.EditedBy = 1;
                                //dES060.EditedOn = DateTime.Now;
                                dbContext.SaveChanges();
                                return "File index flag has been updated successfully.";
                            }
                            else
                            {
                                DES106 objDES106New = new DES106();
                                objDES106New.SolrObjectId = SolrObjectId;
                                objDES106New.ItemId = JEPCustFeedbackDocumentId;
                                objDES106New.Central = true;
                                objDES106New.IsIndex = true;
                                objDES106New.CreatedBy = 1;
                                objDES106New.CreatedOn = DateTime.Now;
                                objDES106New.EditedBy = 1;
                                objDES106New.EditedOn = DateTime.Now;
                                objDES106New.isDeleted = false;
                                dbContext.DES106.Add(objDES106New);
                                dbContext.SaveChanges();
                                return "File index flag has been updated successfully.";
                            }
                        case "DIN":
                            DES106 deDIN = new DES106();
                            deDIN = dbContext.DES106.Where(d => d.DINId == JEPCustFeedbackDocumentId).FirstOrDefault();
                            if (deDIN != null)
                            {
                                deDIN.IsIndex = true;
                                deDIN.EditedOn = DateTime.Now;
                                deDIN.EditedBy = 1;
                                //dES060.EditedOn = DateTime.Now;
                                dbContext.SaveChanges();
                                return "File index flag has been updated successfully.";
                            }
                            else
                            {
                                DES106 objDES106New = new DES106();
                                objDES106New.SolrObjectId = SolrObjectId;
                                objDES106New.DINId = JEPCustFeedbackDocumentId;
                                objDES106New.Central = true;
                                objDES106New.IsIndex = true;
                                objDES106New.CreatedBy = 1;
                                objDES106New.CreatedOn = DateTime.Now;
                                objDES106New.EditedBy = 1;
                                objDES106New.EditedOn = DateTime.Now;
                                objDES106New.isDeleted = false;
                                dbContext.DES106.Add(objDES106New);
                                dbContext.SaveChanges();
                                return "File index flag has been updated successfully.";
                            }
                        case "JEP":
                            DES106 dEJEP = new DES106();
                            dEJEP = dbContext.DES106.Where(d => d.JEPID == JEPCustFeedbackDocumentId).FirstOrDefault();
                            if (dEJEP != null)
                            {
                                if (string.IsNullOrEmpty(dEJEP.SolrObjectId))
                                {
                                    dEJEP.SolrObjectId = SolrObjectId;
                                }
                                dEJEP.IsIndex = true;
                                dEJEP.EditedOn = DateTime.Now;
                                dEJEP.EditedBy = 1;
                                //dES060.EditedOn = DateTime.Now;
                                dbContext.SaveChanges();
                                return "File index flag has been updated successfully.";
                            }
                            else
                            {
                                DES106 objDES106New = new DES106();
                                objDES106New.SolrObjectId = SolrObjectId;
                                objDES106New.JEPID = JEPCustFeedbackDocumentId;
                                objDES106New.Central = true;
                                objDES106New.IsIndex = true;
                                objDES106New.CreatedBy = 1;
                                objDES106New.CreatedOn = DateTime.Now;
                                objDES106New.EditedBy = 1;
                                objDES106New.EditedOn = DateTime.Now;
                                objDES106New.isDeleted = false;
                                dbContext.DES106.Add(objDES106New);
                                dbContext.SaveChanges();
                                return "File index flag has been updated successfully.";
                            }
                        case "ROC":
                            DES106 dEROC = new DES106();
                            dEROC = dbContext.DES106.Where(d => d.ROCCommentedDocId == JEPCustFeedbackDocumentId).FirstOrDefault();
                            if (dEROC != null)
                            {
                                if (string.IsNullOrEmpty(dEROC.SolrObjectId))
                                {
                                    dEROC.SolrObjectId = SolrObjectId;
                                }
                                dEROC.IsIndex = true;
                                dEROC.EditedOn = DateTime.Now;
                                dEROC.EditedBy = 1;
                                //dES060.EditedOn = DateTime.Now;
                                dbContext.SaveChanges();
                                return "File index flag has been updated successfully.";
                            }
                            return "File is not found for update the flag.";
                        case "ROC-Attached":
                            DES106 dEROCAttached = new DES106();
                            dEROCAttached = dbContext.DES106.Where(d => d.ROCCommentsAttachId == JEPCustFeedbackDocumentId).FirstOrDefault();
                            if (dEROCAttached != null)
                            {
                                if (string.IsNullOrEmpty(dEROCAttached.SolrObjectId))
                                {
                                    dEROCAttached.SolrObjectId = SolrObjectId;
                                }
                                dEROCAttached.IsIndex = true;
                                dEROCAttached.EditedOn = DateTime.Now;
                                dEROCAttached.EditedBy = 1;
                                //dES060.EditedOn = DateTime.Now;
                                dbContext.SaveChanges();
                                return "File index flag has been updated successfully.";
                            }
                            return "File is not found for update the flag.";
                        case "ROC Comment":
                            DES106 dEROCComment = new DES106();
                            dEROCComment = dbContext.DES106.Where(d => d.ROCCommentsId == JEPCustFeedbackDocumentId).FirstOrDefault();
                            if (dEROCComment != null)
                            {
                                dEROCComment.IsIndex = true;
                                dEROCComment.EditedOn = DateTime.Now;
                                dEROCComment.EditedBy = 1;
                                //dES060.EditedOn = DateTime.Now;
                                dbContext.SaveChanges();
                                return "ROC Comment index flag has been updated successfully.";
                            }
                            else
                            {
                                DES106 objDES106New = new DES106();
                                objDES106New.SolrObjectId = SolrObjectId;
                                objDES106New.ROCCommentsId = JEPCustFeedbackDocumentId;
                                objDES106New.Central = true;
                                objDES106New.IsIndex = true;
                                objDES106New.CreatedBy = 1;
                                objDES106New.CreatedOn = DateTime.Now;
                                objDES106New.EditedBy = 1;
                                objDES106New.EditedOn = DateTime.Now;
                                objDES106New.isDeleted = false;
                                dbContext.DES106.Add(objDES106New);
                                dbContext.SaveChanges();
                                return "ROC Comment index flag has been updated successfully.";
                            }
                        case "Project":
                            DES106 dEProject = new DES106();
                            dEProject = dbContext.DES106.Where(d => d.Project == Project).FirstOrDefault();
                            if (dEProject != null)
                            {
                                dEProject.IsIndex = true;
                                dEProject.EditedOn = DateTime.Now;
                                dEProject.EditedBy = 1;
                                //dES060.EditedOn = DateTime.Now;
                                dbContext.SaveChanges();
                                return "Project index flag has been updated successfully.";
                            }
                            else
                            {
                                DES106 objDES106New = new DES106();
                                objDES106New.SolrObjectId = SolrObjectId;
                                objDES106New.Project = Project;
                                objDES106New.Central = true;
                                objDES106New.IsIndex = true;
                                objDES106New.CreatedBy = 1;
                                objDES106New.CreatedOn = DateTime.Now;
                                objDES106New.EditedBy = 1;
                                objDES106New.EditedOn = DateTime.Now;
                                objDES106New.isDeleted = false;
                                dbContext.DES106.Add(objDES106New);
                                dbContext.SaveChanges();
                                return "Project index flag has been updated successfully.";
                            }
                        default:
                            return "File is not found for update the flag.";
                    }
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw ex;
            }
        }

        public string UpdateIndexFlagForProject(string Project, string SolrObjectId)
        {
            try
            {
                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    DES106 dEProject = new DES106();
                    dEProject = dbContext.DES106.Where(d => d.Project == Project).FirstOrDefault();
                    if (dEProject != null)
                    {
                        dEProject.IsIndex = true;
                        dEProject.EditedOn = DateTime.Now;
                        dEProject.EditedBy = 1;
                        //dES060.EditedOn = DateTime.Now;
                        dbContext.SaveChanges();
                        return "File index flag has been updated successfully.";
                    }
                    else
                    {
                        DES106 desAddProjectId = new DES106();
                        desAddProjectId.SolrObjectId = SolrObjectId;
                        desAddProjectId.Project = Project;
                        desAddProjectId.Central = true;
                        desAddProjectId.IsIndex = true;
                        desAddProjectId.CreatedBy = 1;
                        desAddProjectId.CreatedOn = DateTime.Now;
                        desAddProjectId.EditedBy = 1;
                        desAddProjectId.EditedOn = DateTime.Now;
                        desAddProjectId.isDeleted = false;
                        dbContext.DES106.Add(desAddProjectId);
                        dbContext.SaveChanges();
                        return "File index flag has been updated successfully.";
                    }
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<string> GetCustomerNameasperTerm(string Term)
        {
            try
            {
                List<string> customerlist = new List<string>();
                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    customerlist = dbContext.SP_DES_GET_CUSTOMER_NAME_EXIST_IN_PROJECT(Term).ToList();

                    return customerlist;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw ex;
            }
        }

        public List<AdvanceSearchModel> GetBUList(string projectno)
        {
            try
            {
                List<AdvanceSearchModel> bulist = new List<AdvanceSearchModel>();
                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    //bulist = dbContext.DES001.Select(b => new AdvanceSearchModel
                    //{
                    //    BUId = b.BUId,
                    //    BU = b.BU + " - " + b.PBU
                    //}).ToList();

                    bulist = dbContext.SP_DES_GET_BU_LIST_AS_PER_PROJECT_AND_WITHIN_PROJECT(projectno).Select(b => new AdvanceSearchModel
                    {
                        BUId = b.BUId,
                        BU = b.BU
                    }).ToList();
                    return bulist;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw ex;
            }
        }

        public List<AdvanceSearchModel> GetStatusList(string projectno)
        {
            try
            {
                List<AdvanceSearchModel> statuslist = new List<AdvanceSearchModel>();
                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    //statuslist = dbContext.DES006.Select(b => new AdvanceSearchModel
                    //{
                    //    PolicyStepId = b.PolicyStepId,
                    //    StepName = b.StepName
                    //}).Distinct().ToList();

                    statuslist = dbContext.SP_DES_GET_STATUS_LIST_AS_PER_PROJECT_AND_WITHIN_PROJECT(projectno).Select(b => new AdvanceSearchModel
                    {
                        PolicyStepId = b.PolicyStepId,
                        StepName = b.StepName
                    }).ToList();
                    return statuslist;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw ex;
            }
        }

        public List<AdvanceSearchModel> GetObjectList(string projectno)
        {
            try
            {
                List<AdvanceSearchModel> bulist = new List<AdvanceSearchModel>();
                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    //bulist = dbContext.DES003.Select(b => new AdvanceSearchModel
                    //{
                    //    ObjectId = b.ObjectId,
                    //    ObjectName = b.ObjectName
                    //}).Distinct().ToList();

                    bulist = dbContext.SP_DES_GET_OBJECT_LIST_AS_PER_PROJECT_AND_WITHIN_PROJECT(projectno).Select(b => new AdvanceSearchModel
                    {
                        ObjectId = b.ObjectId,
                        ObjectName = b.ObjectName
                    }).ToList();
                    return bulist;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw ex;
            }
        }

        public List<AdvanceSearchModel> GetObjectListBasedonBuId(string BUId)
        {
            try
            {
                List<AdvanceSearchModel> bulist = new List<AdvanceSearchModel>();
                long BUid = Convert.ToInt64(BUId);
                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    bulist = dbContext.DES003.Where(x => x.BUId == BUid).Select(b => new AdvanceSearchModel
                    {
                        ObjectId = b.ObjectId,
                        ObjectName = b.ObjectName
                    }).Distinct().ToList();
                    return bulist;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw ex;
            }
        }

        public List<string> GetOwnerName(string ownerInitials)
        {
            try
            {
                List<string> bulist = new List<string>();
                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    bulist = dbContext.SP_DES_GET_ProjectOwnerName(ownerInitials).ToList();

                    return bulist;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw ex;
            }
        }

        public List<string> GetDepartmentlist(string department)
        {
            try
            {
                List<string> departmentlist = new List<string>();
                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    departmentlist = dbContext.SP_DES_GET_DepartmentListAssignedtoRole(department).ToList();

                    return departmentlist;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw ex;
            }
        }

        public List<SP_DES_GET_StatusAsPerObjectId_Result> GetStatusListAsperObjectId(string ObjectId)
        {
            try
            {
                List<SP_DES_GET_StatusAsPerObjectId_Result> bulist = new List<SP_DES_GET_StatusAsPerObjectId_Result>();
                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    bulist = dbContext.SP_DES_GET_StatusAsPerObjectId(ObjectId).ToList();

                    return bulist;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw ex;
            }
        }

        public List<SP_DES_GET_NON_INDEX_DOC_OBJECT_FILES_FOR_INDEX_Result> GetNonIndexDocObject()
        {
            try
            {
                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    List<SP_DES_GET_NON_INDEX_DOC_OBJECT_FILES_FOR_INDEX_Result> documentIndexModels = new List<SP_DES_GET_NON_INDEX_DOC_OBJECT_FILES_FOR_INDEX_Result>();
                    documentIndexModels = dbContext.SP_DES_GET_NON_INDEX_DOC_OBJECT_FILES_FOR_INDEX().ToList();
                    return documentIndexModels;

                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw ex;
            }
        }

        public List<SP_DES_GET_NON_INDEX_JEP_OBJECT_DATA_FOR_INDEX_Result> GetNonIndexJEPObject()
        {
            try
            {
                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    List<SP_DES_GET_NON_INDEX_JEP_OBJECT_DATA_FOR_INDEX_Result> documentIndexModels = new List<SP_DES_GET_NON_INDEX_JEP_OBJECT_DATA_FOR_INDEX_Result>();
                    documentIndexModels = dbContext.SP_DES_GET_NON_INDEX_JEP_OBJECT_DATA_FOR_INDEX().ToList();
                    return documentIndexModels;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw ex;
            }
        }

        public List<SP_DES_GET_NON_INDEX_DIN_OBJECT_DATA_FOR_INDEX_Result> GetNonIndexDINObject()
        {
            try
            {
                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    List<SP_DES_GET_NON_INDEX_DIN_OBJECT_DATA_FOR_INDEX_Result> documentIndexModels = new List<SP_DES_GET_NON_INDEX_DIN_OBJECT_DATA_FOR_INDEX_Result>();
                    documentIndexModels = dbContext.SP_DES_GET_NON_INDEX_DIN_OBJECT_DATA_FOR_INDEX().ToList();
                    return documentIndexModels;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw ex;
            }
        }

        public void UpdateSolrSchedularRunDate(DateTime dateTime)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    DES104 dES104 = new DES104();
                    dES104 = db.DES104.Where(d => d.ConfigName == "LastSOLRSchedularRunDate").FirstOrDefault();
                    if (dES104 != null)
                    {
                        dES104.ConfigValues = dateTime.ToString();
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                //AddExceptionLogs(GetExceptionData(ex, "From UpdateCentralFlag method.", location));
                throw ex;
            }
        }

        public List<SP_DES_GET_NON_INDEX_CUSTOMER_FEEDBACK_FILES_Result> GetNonIndexCustomerFeedbackFiles()
        {
            try
            {
                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    List<SP_DES_GET_NON_INDEX_CUSTOMER_FEEDBACK_FILES_Result> documentIndexModels = new List<SP_DES_GET_NON_INDEX_CUSTOMER_FEEDBACK_FILES_Result>();
                    documentIndexModels = dbContext.SP_DES_GET_NON_INDEX_CUSTOMER_FEEDBACK_FILES().ToList();
                    return documentIndexModels;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw ex;
            }
        }

        public List<SP_DES_GET_NON_INDEX_DCR_FILES_Result> GetNonIndexDCRFiles()
        {
            try
            {
                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    List<SP_DES_GET_NON_INDEX_DCR_FILES_Result> documentIndexModels = new List<SP_DES_GET_NON_INDEX_DCR_FILES_Result>();
                    documentIndexModels = dbContext.SP_DES_GET_NON_INDEX_DCR_FILES().ToList();
                    return documentIndexModels;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw ex;
            }
        }

        public List<SP_DES_GET_NON_INDEX_PART_OBJECT_Result> GetNonIndexParts()
        {
            try
            {
                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    List<SP_DES_GET_NON_INDEX_PART_OBJECT_Result> documentIndexModels = new List<SP_DES_GET_NON_INDEX_PART_OBJECT_Result>();
                    documentIndexModels = dbContext.SP_DES_GET_NON_INDEX_PART_OBJECT().ToList();
                    return documentIndexModels;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw ex;
            }
        }

        public List<SP_DES_GET_NON_INDEX_PROJECT_Result> GetNonindexProjects()
        {
            try
            {
                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    List<SP_DES_GET_NON_INDEX_PROJECT_Result> documentIndexModels = new List<SP_DES_GET_NON_INDEX_PROJECT_Result>();
                    documentIndexModels = dbContext.SP_DES_GET_NON_INDEX_PROJECT().ToList();
                    return documentIndexModels;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw ex;
            }
        }

        public void AddSearchResult(List<DocumentIndexModel> documentIndexModels)
        {
            try
            {
                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    foreach (DocumentIndexModel indexModel in documentIndexModels)
                    {
                        DES107 dES107 = new DES107();
                        dES107 = dbContext.DES107.Where(d => d.ObjectId == indexModel.ObjectId).FirstOrDefault();
                        if (dES107 != null)
                        {
                            dES107.NumofTimeSearched += 1;
                            dES107.EditedBy = 1;
                            dES107.EditedOn = DateTime.Now;
                            dbContext.SaveChanges();
                        }
                        else
                        {
                            DES107 desObje = new DES107()
                            {
                                ObjectId = indexModel.ObjectId,
                                Customer = indexModel.Customer,
                                BU = indexModel.BU,
                                Department = indexModel.Department,
                                ObjectType = indexModel.Type,
                                ObjectStatus = indexModel.Status,
                                ObjectName = indexModel.Name,
                                Revision = indexModel.Revision,
                                Originator = indexModel.Originator,
                                OwnerName = indexModel.OwnerName,
                                DocumentPath = indexModel.MainDocumentPath,
                                Descripion = indexModel.Descripion,
                                FileFormat = indexModel.FileFormat,
                                version = indexModel.version,
                                FileName = indexModel.FileName,
                                NumofTimeSearched = 1,
                                CreatedBy = 1,
                                CreatedOn = DateTime.Now,
                                EditedBy = 1,
                                EditedOn = DateTime.Now
                            };

                            dbContext.DES107.Add(desObje);
                            dbContext.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_FREQUENT_SEARCHED_DOCUMENTS_Result> GetMostSearchedDocuments(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_FREQUENT_SEARCHED_DOCUMENTS(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex).ToList();
                    //recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    recordsTotal = data.Count();
                    return data;

                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_NON_INDEX_ROC_FILES_Result> GetNonIndex_ROC_Files()
        {
            try
            {
                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    List<SP_DES_GET_NON_INDEX_ROC_FILES_Result> documentIndexModels = new List<SP_DES_GET_NON_INDEX_ROC_FILES_Result>();
                    documentIndexModels = dbContext.SP_DES_GET_NON_INDEX_ROC_FILES().ToList();
                    return documentIndexModels;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw ex;
            }
        }

        public List<SP_DES_GET_NON_INDEX_ROC_COMMENT_ATTACHED_FILES_Result> GetNonIndex_ROC_Attached_Files()
        {
            try
            {
                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    List<SP_DES_GET_NON_INDEX_ROC_COMMENT_ATTACHED_FILES_Result> documentIndexModels = new List<SP_DES_GET_NON_INDEX_ROC_COMMENT_ATTACHED_FILES_Result>();
                    documentIndexModels = dbContext.SP_DES_GET_NON_INDEX_ROC_COMMENT_ATTACHED_FILES().ToList();
                    return documentIndexModels;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw ex;
            }
        }

        public List<SP_DES_GET_NON_INDEX_ROC_COMMENTS_Result> GetNonIndex_ROC_Comments()
        {
            try
            {
                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    List<SP_DES_GET_NON_INDEX_ROC_COMMENTS_Result> documentIndexModels = new List<SP_DES_GET_NON_INDEX_ROC_COMMENTS_Result>();
                    documentIndexModels = dbContext.SP_DES_GET_NON_INDEX_ROC_COMMENTS().ToList();
                    return documentIndexModels;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw ex;
            }
        }

        public string SaveFilterData(string CustomerName, string BUPBU, string Department, string Object, string Status, string Name, string Revision, string Originator, string Owner, string SearchKeyword, string SearchName, string ProjectNo, int? FilterId, string SubObject)
        {
            Int64 SearchFilterId = 0;
            try
            {
                SearchFilterId = (FilterId.HasValue ? FilterId.Value : 0);
                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    bool isFilternameExist = false;

                    isFilternameExist = CheckFilterNameIsExistorNot(SearchName, SearchFilterId);
                    if (!isFilternameExist)
                    {
                        if (SearchFilterId == 0)
                        {
                            DES110 objdes110 = new DES110()
                            {
                                FilterName = SearchName,
                                CustomerName = CustomerName,
                                BUPBU = BUPBU,
                                Department = Department,
                                Object = Object,
                                Status = Status,
                                ObjectName = Name,
                                Revision = Revision,
                                Originator = Originator,
                                Owner = Owner,
                                SearchKeyword = SearchKeyword,
                                ProjectNumber = ProjectNo,
                                CreatedBy = Convert.ToInt64(CommonService.objClsLoginInfo.UserName),
                                CreatedOn = DateTime.Now,
                                SubObject = SubObject
                            };
                            dbContext.DES110.Add(objdes110);
                        }
                        else
                        {
                            var objdes110 = dbContext.DES110.FirstOrDefault(i => i.PK_SearchFilterId == SearchFilterId);
                            if (objdes110 != null)
                            {
                                objdes110.FilterName = SearchName;
                                objdes110.CustomerName = CustomerName;
                                objdes110.BUPBU = BUPBU;
                                objdes110.Department = Department;
                                objdes110.Object = Object;
                                objdes110.Status = Status;
                                objdes110.ObjectName = Name;
                                objdes110.Revision = Revision;
                                objdes110.Originator = Originator;
                                objdes110.Owner = Owner;
                                objdes110.SearchKeyword = SearchKeyword;
                                objdes110.ProjectNumber = ProjectNo;
                                objdes110.SubObject = SubObject;
                            }
                            else
                            {
                                return "Search filter does not exist.";
                            }
                        }
                        dbContext.SaveChanges();
                        if (SearchFilterId == 0)
                        {
                            return "Filter Data has been saved.";
                        }
                        else
                        {
                            return "Filter Data has been updated.";
                        }
                    }
                    else
                    {
                        return "Filter name is already exist, Please choose another name.";
                    }
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        private bool CheckFilterNameIsExistorNot(string searchName, Int64 FilterId)
        {
            try
            {
                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    long Userid = Convert.ToInt64(CommonService.objClsLoginInfo.UserName);
                    DES110 dES110 = new DES110();
                    dES110 = dbContext.DES110.Where(d => d.FilterName == searchName && d.CreatedBy == Userid && d.PK_SearchFilterId != FilterId).FirstOrDefault();
                    if (dES110 != null)
                    {
                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<DES110> BindFilterdata(long PsNo)
        {
            try
            {
                List<DES110> listdes110 = new List<DES110>();
                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    listdes110 = dbContext.DES110.Where(d => d.CreatedBy == PsNo).ToList();
                }
                return listdes110;
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<DES110> FetchSavedFilterData(long FilterId, long PsNo)
        {
            try
            {
                List<DES110> savedfilterlist = new List<DES110>();
                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    savedfilterlist = dbContext.DES110.Where(d => d.PK_SearchFilterId == FilterId && d.CreatedBy == PsNo).ToList();
                    return savedfilterlist;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public string DeleteFilterData(long filterid)
        {
            try
            {
                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    DES110 dES110 = new DES110();
                    dES110 = dbContext.DES110.Where(d => d.PK_SearchFilterId == filterid).FirstOrDefault();
                    if (dES110.PK_SearchFilterId > 0)
                    {
                        dbContext.DES110.Remove(dES110);
                        dbContext.SaveChanges();
                        return "Filter Data has been removed successfully.";
                    }
                    else
                    {
                        return "Filter Data not found.";
                    }
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<string> GetProjectListName(string proj)
        {
            try
            {
                List<string> Projectlist = new List<string>();
                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    Projectlist = dbContext.DES051.Where(p => p.Project.Contains(proj)).Select(d => d.Project).ToList();

                    return Projectlist;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw ex;
            }
        }

        public List<SP_DES_GET_NON_INDEX_DOC_OBJECT_FILES_FOR_INDEX_Result> GetFilesforUVSearchfileCreation()
        {
            try
            {
                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    List<SP_DES_GET_NON_INDEX_DOC_OBJECT_FILES_FOR_INDEX_Result> documentIndexModels = new List<SP_DES_GET_NON_INDEX_DOC_OBJECT_FILES_FOR_INDEX_Result>();
                    documentIndexModels = dbContext.SP_DES_GET_NON_INDEX_DOC_OBJECT_FILES_FOR_INDEX().ToList();
                    return documentIndexModels;

                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw ex;
            }
        }

        public List<SP_DES_DEL_REMOVE_DATA_INDEX_Result> GetRemoveIndexData()
        {
            try
            {
                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    List<SP_DES_DEL_REMOVE_DATA_INDEX_Result> documentIndexModels = new List<SP_DES_DEL_REMOVE_DATA_INDEX_Result>();
                    documentIndexModels = dbContext.SP_DES_DEL_REMOVE_DATA_INDEX().ToList();
                    return documentIndexModels;

                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw ex;
            }
        }
        public void RemoveIndexData(List<Int64> Ids)
        {
            using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
            {
                var list = dbContext.DES106.Where(i => Ids.Contains(i.Central_IndexStatusId)).ToList();
                dbContext.DES106.RemoveRange(list);
                dbContext.SaveChanges();
            }
        }
    }
}
