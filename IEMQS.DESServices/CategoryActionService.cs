﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IEMQS.DESServices
{
    public class CategoryActionService : ICategoryActionService
    {
        public Int64 AddEdit(CategoryActionModel model, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";

                    #region Check Validation
                    if (model.MappingId > 0)
                    {                        
                        var checkchild = db.DES002.Find(model.MappingId);
                        if (checkchild != null && model.TypeId == 1)
                        {
                            if (checkchild.DES004 != null && checkchild.DES004.Count > 0)
                            {
                                errorMsg = "Category is already in used. you cannot edit";
                                return 0;
                            }
                        }

                        var checkchild1 = db.DES002.Find(model.MappingId);
                        if (checkchild1 != null && model.TypeId == 2)
                        {
                            if (checkchild1.DES004 != null && checkchild1.DES004.Count > 0)
                            {
                                errorMsg = "Action is already in used. you cannot edit";
                                return 0;
                            }
                        }
                    }                   

                    var checkdublicate = db.DES002.Where(x => x.TypeId == model.TypeId && x.Name == model.Name && x.URL == model.URL && x.MappingId != model.MappingId).FirstOrDefault();
                    if (checkdublicate != null)
                    {
                        errorMsg = "Category & action is already exist";
                        return 0;
                    }
                    #endregion

                    var temp = db.DES002.Find(model.MappingId);
                    DES002 _DES002 = temp == null ? new DES002() : temp;

                    _DES002.TypeId = model.TypeId;
                    _DES002.ParentId = model.ParentId;
                    _DES002.Name = model.Name;
                    _DES002.URL = model.URL;
                    _DES002.IsTarget = model.IsTarget;

                    if (temp == null)
                    {
                        _DES002.CreatedBy = CommonService.objClsLoginInfo.UserName;
                        _DES002.CreatedOn = DateTime.Now;
                        db.DES002.Add(_DES002);
                    }
                    else
                    {
                        _DES002.EditedBy = CommonService.objClsLoginInfo.UserName;
                        _DES002.EditedOn = DateTime.Now;
                    }
                    db.SaveChanges();
                    return _DES002.MappingId;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<CategoryActionModel> GetAllCategory()
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES002.OrderBy(x => x.MappingId).Select(x => new CategoryActionModel
                    {
                        MappingId = x.MappingId,
                        TypeId = x.TypeId,
                        ParentId = x.ParentId,
                        Name = x.Name,
                        URL = x.URL,
                        IsTarget = x.IsTarget ?? false
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<CategoryActionModel> GetAllNameByType(int TypeId)
        {
            try
            {              
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {                    
                    return db.DES002.Where(x => x.TypeId == TypeId).Select(x => new CategoryActionModel
                    {
                        MappingId = x.MappingId,
                        TypeId = x.TypeId,
                        ParentId = x.ParentId,
                        Name = x.Name,
                        URL = x.URL
                    }).ToList();                   
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public CategoryActionModel GetCategorybyId(long Id)
        {
            throw new NotImplementedException();
        }

        public List<SP_DES_GET_CATEGORY_ACTION_Result> GetCategoryActionList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_CATEGORY_ACTION(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }
       
        public Int64 DeleteCategory(Int64 Id, out int TypeId, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    TypeId = 0;
                    var check = db.DES002.Find(Id);
                    if (check != null)
                    {
                        if (check.DES004 != null && check.DES004.Count > 0)
                        {
                            if (check.TypeId == 1)
                                errorMsg = "Category is already in used. you cannot delete";
                            else
                                errorMsg = "Action is already in used. you cannot delete";
                            return 0;
                        }
                        db.DES002.Remove(check);
                        db.SaveChanges();
                        TypeId = check.TypeId ?? 0;

                        return check.MappingId;
                    }

                    return 0;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }
    }
}
