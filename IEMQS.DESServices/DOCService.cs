﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IEMQS.DESServices
{
    public class DOCService : IDOCService
    {
        public Int64 AddEdit(DOCModel model, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    var temp = db.DES059.Find(model.DocumentId);
                    DES059 _DES059 = temp == null ? new DES059() : temp;

                    _DES059.DocumentTypeId = model.DocumentTypeId;
                    _DES059.DocumentTitle = model.DocumentTitle;
                    _DES059.ClientDocumentNumber = model.ClientDocumentNumber;
                    _DES059.ClientVersion = model.ClientVersion;
                    _DES059.MilestoneDocument = model.MilestoneDocument;


                    if (temp == null || (model.IsRevision && temp != null))
                    {
                        _DES059.IsDOCCheckIn = true;
                        _DES059.DOCCheckInBy = CommonService.objClsLoginInfo.UserName;
                        _DES059.DOCCheckInOn = DateTime.Now;
                        _DES059.DocumentRevision = model.DocumentRevision;
                        _DES059.IsLatestRevision = true;
                        _DES059.IsJEP = model.IsJEP;
                        _DES059.Status = ObjectStatus.Draft.ToString();
                        _DES059.Project = model.Project;
                        _DES059.FolderId = model.FolderId;
                        _DES059.Department = model.Department;
                        _DES059.RoleGroup = model.RoleGroup;
                        if (model.IsJEP)
                        {
                            if (model.IsEdit)
                            {
                                _DES059.DocumentNo = model.DocumentNo;
                            }
                            else if (model.IsRevision)
                            {
                                _DES059.DocumentNo = model.DocumentNo;
                            }
                            else
                            {
                                var jep = db.DES075.Where(x => x.JEPDocumentDetailsId == model.JEPDocumentDetailsId).FirstOrDefault();
                                if (jep != null)
                                {
                                    _DES059.DocumentNo = jep.DocNo;
                                    model.DocumentNo = jep.DocNo;
                                }
                                else
                                {
                                    errorMsg = "Jep not found";
                                    return 0;
                                }
                            }
                        }
                        else
                        {
                            _DES059.DocumentNo = model.DocumentNo;
                        }


                        #region getData From LifeCycle
                        var policy = GetPolicySteps(ObjectName.DOC.ToString(), model.BUId, null); // Get Polocy Steps
                        List<DES062> _DES062List = new List<DES062>();
                        if (policy != null)
                        {
                            foreach (var item in policy)
                            {
                                _DES059.PolicyId = item.PolicyId;
                                _DES062List.Add(new DES062
                                {
                                    PolicyID = item.PolicyId,
                                    PolicyStep = item.PolicyStep,
                                    PolicyOrderID = item.PolicyOrderID,
                                    IsCompleted = false,
                                    DesignationLevels = item.DesignationLevels,
                                    ColorCode = item.ColorCode
                                });
                            }
                        }
                        #endregion

                        _DES059.DES062 = _DES062List;
                        _DES059.CreatedBy = CommonService.objClsLoginInfo.UserName;
                        _DES059.CreatedOn = DateTime.Now;
                        _DES059.Location = model.Location;
                        db.DES059.Add(_DES059);
                        db.SaveChanges();
                        if (model.IsJEP)
                        {
                            var jep = db.DES075.Find(model.JEPDocumentDetailsId);
                            if (jep != null)
                            {
                                jep.DocumentId = _DES059.DocumentId;
                                db.SaveChanges();
                            }
                        }
                        AddDocumentHistory(_DES059.DocumentId, null, null, null, model.DocumentNo + " has been created successfully", CommonService.objClsLoginInfo.UserName);
                    }
                    else
                    {
                        if (temp.IsDOCCheckIn != true)
                        {
                            _DES059.IsDOCCheckIn = true;
                            _DES059.DOCCheckInBy = CommonService.objClsLoginInfo.UserName;
                            _DES059.DOCCheckInOn = DateTime.Now;
                        }
                        _DES059.EditBy = CommonService.objClsLoginInfo.UserName;
                        _DES059.EditedOn = DateTime.Now;
                        AddDocumentHistory(model.DocumentId, null, null, null, model.DocumentNo + " has been edited successfully", CommonService.objClsLoginInfo.UserName);
                        db.SaveChanges();
                    }
                    return _DES059.DocumentId;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public DOCModel GetDocumentByID(Int64 Id)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.DES059.Where(x => x.DocumentId == Id).Select(x => new DOCModel
                    {
                        DocumentId = x.DocumentId,
                        Project = x.Project,
                        DocumentTypeId = x.DocumentTypeId,
                        DocumentNo = x.DocumentNo,
                        DocumentTitle = x.DocumentTitle,
                        DocumentRevision = x.DocumentRevision,
                        IsLatestRevision = x.IsLatestRevision ?? false,
                        DocumentRevisionStr = "R" + x.DocumentRevision,
                        ClientDocumentNumber = x.ClientDocumentNumber,
                        MilestoneDocument = x.MilestoneDocument ?? false,
                        PolicyId = x.PolicyId,
                        ClientVersion = x.ClientVersion,
                        IsJEP = x.IsJEP ?? false,
                        FolderId = x.FolderId,
                        CreatedBy = x.CreatedBy,
                        CreatedOn = x.CreatedOn,
                        EditedBy = x.EditBy,
                        EditedOn = x.EditedOn,
                        Status = x.Status,
                        Department = x.Department,
                        RoleGroup = x.RoleGroup,
                        DocumentNoToGetInHidden = x.DocumentNo,
                        IsDOCCheckIn = x.IsDOCCheckIn ?? false,
                        ReturnNotes = x.ReturnNotes
                    }).FirstOrDefault();
                    data.PolicySteps = GetPolicySteps(null, null, data.DocumentId);
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }


        public void GetDocumentNoFromJEP(string DocNo, Int64? DocId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.DES075.FirstOrDefault(x => x.DocNo == DocNo);
                    data.DocumentId = DocId;
                    db.Entry(data).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }

        }

        public Int64 UpdateIsMainFileStatus(DOCFileModel model, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    var data = db.DES060.FirstOrDefault(x => x.DocumentMappingId == model.DocumentMappingId);
                    data.IsMainDocument = model.IsMainDocument;
                    db.Entry(data).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    AddDocumentHistory(model.DocumentId, data.DocumentMappingId, null, null, data.Document_name + " is marked as main file", CommonService.objClsLoginInfo.UserName);
                    return data.DocumentMappingId;

                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }

        }

        public Int64 Delete(Int64 Id, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    var check = db.DES059.Find(Id);
                    if (check != null)
                    {

                        if (check.DES060.Select(x => x.DocumentId).FirstOrDefault() > 0)
                        {
                            errorMsg = "Document Type is already in used. you cannot delete";
                            return 0;
                        }

                        if (check.DES062.Select(x => x.DocumentId).ToList() != null)
                        {
                            var checkPolicy = db.DES062.Where(x => x.DocumentId == check.DocumentId).ToList();
                            foreach (var DocId in checkPolicy)
                            {
                                db.DES062.Remove(DocId);
                            }

                        }
                        if (check.DocumentNo != null)
                        {
                            var getDOCNoFromJEP = db.DES075.Where(x => x.DocNo == check.DocumentNo).FirstOrDefault();
                            if (getDOCNoFromJEP != null)
                            {
                                getDOCNoFromJEP.DocNo = null;
                            }
                        }

                        db.DES059.Remove(check);

                        db.SaveChanges();
                        AddDocumentHistory(Id, null, null, null, check.DocumentNo + " has beend deleted", check.CreatedBy);
                    }
                    return check.DocumentId;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<ObjectPolicyStepsModel> GetPolicySteps(string Type, Int64? BUId, Int64? DocumentId)
        {
            try
            {

                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    if (DocumentId > 0)
                    {
                        var PoliicyData = db.DES062.Where(x => x.DocumentId == DocumentId).ToList();

                        List<ObjectPolicyStepsModel> listObjectPolicyStepsModel = new List<ObjectPolicyStepsModel>();

                        foreach (var x in PoliicyData)
                        {
                            ObjectPolicyStepsModel objectPolicyStepsModel = new ObjectPolicyStepsModel();
                            objectPolicyStepsModel.DocumentLifeCycleId = x.DocumentLifeCycleId;
                            objectPolicyStepsModel.PolicyId = x.PolicyID;
                            objectPolicyStepsModel.PolicyStep = x.PolicyStep;
                            objectPolicyStepsModel.PolicyOrderID = x.PolicyOrderID;
                            objectPolicyStepsModel.IsCompleted = x.IsCompleted ?? false;
                            objectPolicyStepsModel.CreatedBy = new clsManager().GetUserNameFromPsNo((x.CreatedBy));
                            objectPolicyStepsModel.CreatedOn = x.CreatedOn;
                            objectPolicyStepsModel.ColorCode = x.ColorCode;
                            objectPolicyStepsModel.DesignationLevels = x.DesignationLevels;
                            listObjectPolicyStepsModel.Add(objectPolicyStepsModel);
                        }
                        return listObjectPolicyStepsModel.OrderBy(x => x.PolicyOrderID).ToList();
                    }
                    else
                    {
                        return db.SP_DES_GET_OBJECT_POLICY_BY_OBJECTNAME(Type, BUId).Select(x => new ObjectPolicyStepsModel
                        {
                            PolicyId = x.PolicyId,
                            PolicyStep = x.StepName,
                            PolicyOrderID = x.OrderNo,
                            ColorCode = x.ColorCode,
                            DesignationLevels = x.DesignationLevels
                        }).ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_DOCUMENT_Result> GetDocumentList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, string project, Int64? folderId, string Roles, out int recordsTotal)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_DOCUMENT(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, project, folderId, Roles).ToList();
                    recordsTotal = 0;
                    int? rt = 0;
                    data.ForEach(x =>
                    {
                        rt = x.TableCount;
                        x.QString = CommonService.Encrypt("Id=" + x.DocumentId);
                    });
                    recordsTotal = rt ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }


        public List<SP_DES_GET_DOCUMENT_BY_DOCNO_Result> GetDocumentListByDOCNO(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, string project, string DocumentNo, out int recordsTotal)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_DOCUMENT_BY_DOCNO(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, project, DocumentNo).ToList();
                    recordsTotal = 0;
                    int? rt = 0;
                    data.ForEach(x =>
                    {
                        rt = x.TableCount;
                        x.QString = CommonService.Encrypt("Id=" + x.DocumentId);
                    });
                    recordsTotal = rt ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_DOCUMENT_HISTORY_Result> GetDocumentHistory(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, Int64? DocId, out int recordsTotal)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_DOCUMENT_HISTORY(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, DocId).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }


        public bool CheckDocumentNo(string documentNo, Int64? documentId, string project, int? DocumentRevision)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    documentNo = documentNo.Trim();
                    var data = db.DES059.Where(x => x.DocumentNo == documentNo && x.DocumentId != documentId && x.DocumentRevision == DocumentRevision).FirstOrDefault();
                    if (data != null)
                    {
                        return true;
                    }
                    else
                    {
                        if (!(documentId > 0))
                        {
                            var data1 = db.DES075.Where(x => x.DocNo == documentNo).ToList();
                            if (data1 != null && data1.Count > 0)
                            {
                                return true;
                            }
                        }

                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                return false;
            }
        }

        public void AddDocumentHistory(Int64? DocID, Int64? DocFileMappingId, Int64? DocLifCycleId, Int64? CheckINOut, string Discription, string CreatedBy)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    DES063 _DES063 = new DES063();
                    _DES063.DocumentId = DocID;
                    _DES063.DocumentMappingId = DocFileMappingId;
                    _DES063.DocumentLifeCycleId = DocLifCycleId;
                    _DES063.CheckInCheckOutId = CheckINOut;
                    _DES063.Description = Discription;
                    _DES063.CreatedBy = CommonService.objClsLoginInfo.UserName;
                    _DES063.CreatedOn = DateTime.Now;
                    db.DES063.Add(_DES063);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }

        }

        public List<JEPDocumentModel> GetJEPDocumentNo(string Department, string Project)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES075.Where(x => x.Department == Department && x.DocumentId == null && x.DES074.Project == Project && x.DES074.Status == ObjectStatus.Completed.ToString()).Select(x => new JEPDocumentModel
                    {
                        JEPDocumentDetailsId = x.JEPDocumentDetailsId,
                        DocNo = x.DocNo
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_USER_POLICY_BYLEVEL_Result> GetUserLeavel(string RoleGroup, string Roles, Int64 DocumentId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_USER_POLICY_BYLEVEL(RoleGroup, Roles, DocumentId).ToList();
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_DOC_USER_POLICY_MASS_PROMOTE_Result> GetMassPromoteUserPolicy(string psno, string Roles, string FilterValue)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_DOC_USER_POLICY_MASS_PROMOTE(psno, Roles, FilterValue).ToList();
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public bool UpdateLatestRevision(Int64? DocId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {

                    var data = db.DES059.FirstOrDefault(x => x.DocumentId == DocId);
                    data.IsLatestRevision = false;
                    db.Entry(data).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    AddDocumentHistory(data.DocumentId, null, null, null, data.DocumentNo + " updated with latest revision", CommonService.objClsLoginInfo.UserName);
                    return true;

                }

            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }

        }

        public List<DOCModel> GetALLRevisionByDocId(Int64? Id, out string errorMsg)
        {
            errorMsg = "";
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.DES059.Where(x => x.DocumentId == Id && x.IsLatestRevision == true).Select(x => new DOCModel
                    {
                        DocumentId = x.DocumentId,
                        Project = x.Project,
                        DocumentTypeId = x.DocumentTypeId,
                        DocumentNo = x.DocumentNo,
                        DocumentTitle = x.DocumentTitle,
                        DocumentRevision = x.DocumentRevision,
                        IsLatestRevision = x.IsLatestRevision ?? false,
                        DocumentRevisionStr = "R" + x.DocumentRevision,
                        ClientDocumentNumber = x.ClientDocumentNumber,
                        MilestoneDocument = x.MilestoneDocument ?? false,
                        PolicyId = x.PolicyId,
                        IsJEP = x.IsJEP ?? false,
                        FolderId = x.FolderId,
                        CreatedBy = x.CreatedBy,
                        CreatedOn = x.CreatedOn,
                        EditedBy = x.EditBy,
                        EditedOn = x.EditedOn,
                        Status = x.Status,
                        Department = x.Department,
                        RoleGroup = x.RoleGroup,
                    }).ToList();
                    errorMsg = "Revision Data Found";
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                errorMsg = "Revision Data Not Found";
                throw;
            }
        }

        public DOCModel GetJEPData(Int64? JEPDocId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    DOCModel dOCModel = new DOCModel();
                    var data = db.DES075.FirstOrDefault(x => x.JEPDocumentDetailsId == JEPDocId);

                    dOCModel.DocumentTitle = data.DocDescription;
                    dOCModel.DocumentRevision = data.RevisionNo;
                    dOCModel.DocumentRevisionStr = "R" + data.RevisionNo;
                    dOCModel.DocumentTypeId = data.DocumentTypeId;
                    dOCModel.ClientDocumentNumber = data.CustomerDocNo;
                    dOCModel.MilestoneDocument = data.MilestoneDocument ?? false;
                    dOCModel.ClientVersion = data.CustomerDocRevision;
                    return dOCModel;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }

        }


        public List<SP_DES_GET_PENDING_DOCUMENT_Result> GetPendingDocumentList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, string psno, string Roles, out int recordsTotal, string Project)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_PENDING_DOCUMENT(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, psno, Roles, Project).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    return data;

                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }



        public bool IsExistINJEP(DOCFileModel addDocMapp)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.DES075.Where(x => x.DocumentId == addDocMapp.OldDocIDForRevision).FirstOrDefault();
                    var model = db.DES059.Find(addDocMapp.DocumentId);

                    if (data != null)
                    {
                        var dataDES085 = db.DES085.Where(x => x.JEPDocumentDetailsId == data.JEPDocumentDetailsId).FirstOrDefault();
                        DES075 DES075 = new DES075();
                        DES075.JEPId = data.JEPId;
                        DES075.DocNo = model.DocumentNo;
                        DES075.DocDescription = model.DocumentTitle;
                        DES075.RevisionNo = model.DocumentRevision;
                        DES075.DocumentTypeId = model.DocumentTypeId;
                        DES075.CustomerDocRevision = model.ClientVersion;
                        DES075.CustomerDocNo = model.ClientDocumentNumber;
                        DES075.Department = model.Department;
                        if (dataDES085 != null)
                        {
                            if (dataDES085.ReceiptDate != null)
                            {
                                DES075.PlannedDate = dataDES085.ReceiptDate.Value.AddDays(15);
                            }
                            else
                            {
                                DES075.PlannedDate = DateTime.Now;
                            }
                        }
                        else
                        {
                            DES075.PlannedDate = DateTime.Now;
                        }
                        DES075.Weightage = "0";
                        DES075.DocumentId = model.DocumentId;
                        DES075.CreatedBy = CommonService.objClsLoginInfo.UserName;
                        DES075.CreatedOn = DateTime.Now;
                        DES075.MilestoneDocument = model.MilestoneDocument;
                        DES075.IsApproved = data.IsApproved;
                        db.DES075.Add(DES075);
                        db.SaveChanges();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }


        public bool IsExistINDCR(Int64 Id)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var getJEPDetailID = db.DES075.Where(x => x.DocumentId == Id).FirstOrDefault();
                    if (getJEPDetailID != null)
                    {
                        List<bool> addData = new List<bool>();
                        var getDCRAffectedData = db.DES090.Where(x => x.JEPDocumentDetailsId == getJEPDetailID.JEPDocumentDetailsId).Select(x => x.DCRId).ToList();
                        if (getDCRAffectedData.Count > 0)
                        {
                            foreach (var item in getDCRAffectedData)
                            {
                                var getStatus = db.DES089.Where(x => x.DCRId == item).FirstOrDefault();
                                var getCurrentPolicyStatus = db.DES102.Where(x => x.DCRId == item).FirstOrDefault();
                                //if (getStatus.ReleasedStepId == (int)DCRReleasedSteps.Step1 || getStatus.ReleasedStepId == (int)DCRReleasedSteps.Step3)
                                //{
                                //    if (getStatus.Status == ObjectStatus.Completed.ToString() || getStatus.Status == ObjectStatus.Cancelled.ToString())
                                //    {
                                //        addData.Add(true);
                                //    }
                                //    else
                                //    {
                                //        addData.Add(false);
                                //    }
                                //}
                                if (getCurrentPolicyStatus.PolicyStep==DCRLifeCyclePolicyStep.InCorporate.ToString() && getCurrentPolicyStatus.IsCompleted==true)
                                {
                                    addData.Add(true);
                                }
                                else
                                {
                                    addData.Add(false);
                                }
                            }
                        }
                        if (addData.Contains(false))
                        {
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public bool IsCheckInDOC(string PsNo, Int64? DocId, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    var getDocID = db.DES059.Where(x => x.DocumentId == DocId).FirstOrDefault();

                    if (getDocID != null && getDocID.IsDOCCheckIn == true)
                    {
                        if (getDocID.DOCCheckInBy != PsNo)
                        {
                            errorMsg = "Document is already in use by " + new clsManager().GetUserNameFromPsNo((getDocID.DOCCheckInBy));
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }
        public bool UpdateIsCheckInDOC(Int64? DocId, bool HasPsNo)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var getDocID = db.DES059.Where(x => x.DocumentId == DocId).FirstOrDefault();
                    if (HasPsNo != true)
                    {
                        getDocID.IsDOCCheckIn = false;
                        getDocID.DOCCheckInBy = null;
                    }
                    else
                    {
                        getDocID.IsDOCCheckIn = true;
                        getDocID.DOCCheckInBy = CommonService.objClsLoginInfo.UserName;
                        getDocID.DOCCheckInOn = DateTime.Now;
                    }
                    db.Entry(getDocID).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public bool GetDownloadRigts(string Role, string RoleGroup, Int64 DocId, string CurrentPSNo)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var getCreatedDocPsNo = db.DES059.Where(x => x.DocumentId == DocId).Select(x => x.CreatedBy).FirstOrDefault();
                    var getDesigLevelByID = db.DES062.Where(x => x.DocumentId == DocId).Select(x => x.DesignationLevels).ToList();//this will get designation leavel related to DocID
                    getDesigLevelByID.RemoveAt(0); // this will remove very 1st leavel due to all has rights to create.
                    var getUserLevel = db.SP_DES_GET_USER_LEVEL(RoleGroup, Role).FirstOrDefault(); // this will get current user level

                    if (getCreatedDocPsNo == CurrentPSNo)
                    {
                        return true;
                    }
                    if (getDesigLevelByID.Contains(getUserLevel)) //If Level By DocId contains CurentUser Level it will send true
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public string GetRoleGroupByPSNo(string PSNo)
        {

            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {

                    var rolegroup = db.SP_DES_GET_USER_ROLE_ROLEGROUP(PSNo).Select(x => x.RoleGroup).ToList();
                    string roleGroup = string.Join(",", rolegroup);
                    return roleGroup;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }
    }
}
