﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;

namespace IEMQS.DESServices
{
    public interface IPartService
    {
        List<SP_DES_GET_PARTLIST_BYPROJECT_Result> GetPartListByProject(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, string Project, string Roles);
        List<SP_DES_GET_RELATEDPARTLIST_BYPROJECT_Result> GetRelatedPartListByProject(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, string Project, string Roles);

        List<SP_DES_GET_PARTBOMLIST_BYPROJECT_Result> GetBOMPartListByProject(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, string Project, string Roles);

        bool AddEditItem(PartModel model, string PSNo, out Int64 ItemId, out string ItemKey, out string errorMsg, out string SuccessMsg, out string ItemKeyConflict, out string ExistingItemkey, out bool ConfirmExistingItemkey, out bool ConfirmProductFormCode, out string RevisionConfirmMsg, out bool RevisionConfirm);

        PartModel GetPartByID(Int64 ItemId, Int64? BOMId);

        Int64 Delete(Int64 Id, out string errorMsg);


        List<SelectListItem> GetItemGroup();
        List<ARMListModel> GetARMSet();

        List<SizeCodeModel> GetAllSizeCode();

        string GetARMSetLatestVrs(string Arms);

        List<MaterialModel> GetMaterial();

        List<PartModel> GetParentData(string ProjectNo);

        ProductFormCodeModel GetProductFormDataById(Int64? ProductFormId);
        List<SelectListItem> GetCountractDOCbyType(int type, string Contract);
        List<SelectListItem> GetProjectManufacturedPartList(string project);
        List<ProductFormCodeModel> GetProductFormCodeList(Int64? BUId);
        List<PartPolicyModel> GetPartPolicyList(Int64? BUId);
        bool AddEditPartSheet(TempPartModel model, out string errormsg);
        List<TempPartModel> GetTempPartListById(long? itemSheetId, string project, string Contract, Int64? BUId, out string errorMsg);
        List<TempPartModel> Add5Row(int TNo, int Addrow, long? itemSheetId, string project, string Contract, Int64? BUId, out string errorMsg);

        List<PowerViewModel> GetPowerViewList(string Project, long? ItemId = null);
        TreeViewModel GetTreeViewList(string Project, long? ItemId = null);


        List<SP_DES_GET_PART_HISTORY_Result> GetPartHistory(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, string itemid, out int recordsTotal);

        List<SP_DES_GET_BOM_HISTORY_Result> GetBOMHistory(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, string itemid, out int recordsTotal);

        

        List<SP_DES_GET_PART_REVISION_Result> GetPartRevision(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, string project, string itemkey, out int recordsTotal);

        List<SP_DES_GET_ALL_MATERIAL_Result> GetAllMaterialList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, Int64 ProductFormId, out int recordsTotal);

        List<SP_DES_GET_ALL_ITEM_GROUP_Result> GetAllItemGroupList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, string MaterialId, out int recordsTotal);

        List<SP_DES_GET_ALL_SIZE_Result> GetAllSizeList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal);
        SizeCodeModel GetSizeById(string Id);
        List<SP_DES_GET_ALL_ARMSET_Result> GetAllARMSetList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal);
        ARMModel GetARMSetById(string Id);


        Int64 SavePartColumn(PartColumnModel model);
        PartColumnModel GetSavePartColumn();

        PartModel GetUserDefinedItemKey(string ItemKey, string Contract);

        BOMModel GetDataByItemAndParentItem(long ItemId, long ParentItemId, string Project, int? FindNumber);

        List<ItemGroupUOMModel> GetItemGroupUOMModelList();

        List<SP_DES_MAINTAIN_ITEMSHEET_Result> MaintainTimesheet(long Id, string psno, string Department, string RoleGroup, long? FolderId);

        List<SP_DES_GET_CHILDITEM_BOM_Result> GetChildItemBOM(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, string Project, out int recordsTotal, Int64 ItemId, string ItemKey);

        List<SP_DES_GET_PARENTITEM_BOM_Result> GetParentItemBOM(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, string Project, out int recordsTotal, Int64 ItemId);

        string CreateXMLFile(string Project,string CurrentPsNo, out string errorMsg);

        ItemMatrialModel GetItemGroupFromMaterial(string Material, string ProductForm,string ItemGroup);
        List<SelectListItem> GetCountractJepDOCbyType(int type, string contract);
        List<SP_DES_GET_PART_POLICY_BYLEVEL_Result> GetUserLevel(string roleGroup, string roles, long itemId);
        List<PartPolicyStepsModel> GetPolicySteps(string Type, Int64? BUId, Int64? ItemId);

        bool MassPromote(string project,GetListOfItmeID model, string PsNo, out List<string> errorMsg, out List<string> infoMsg);

        Int64? ReviseItemPolicy(Int64? ItemId, string Project, out string errorMsg);
        Int64? UpdateItemPolicy(Int64 PartLifeCycleId, string Project, out string errorMsg, out string infoMsg);
        bool CheckItemKeyIsvalid(string ItemKey, string Contract);

        List<SP_DES_GET_ITEM_BOMLIST_Result> GetItemBOMList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, long ItemId);

        List<SP_DES_GET_PENDING_PARTLIST_Result> GetPendingPartList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, string psno, string Roles, string Project, out int recordsTotal);

        List<SP_DES_GET_PART_USER_POLICY_MASS_PROMOTE_Result> GetMassPromoteUserPolicy(string psno, string Roles, string FilterValue);



        List<DDLManufactureItem> GetManufacturingItembyProject(string Project);
        List<DDLProject> GetProjects();

        int PartCopyToSheet(Int64 ManufacturingItem, Int64 ItemSheetId);

        List<SelectItemList> GetContractProjects(string Project);
        List<SelectItemList> GetProjectsManufactureUniqueItem(List<string> Projects);
        List<List<SP_DES_SOURCE_TO_DEST_COPYPART_CHILD_ITEM_Result>> Insert_CopyPart_Source_To_destination(PartGroupModel model, out string errorMsg);

        void ExportToExcel(Int64? ItemSheetId);

        bool NFPartCreate(string Project, Int64? ItemId);
        DataTable ImportPartAndBOMItem(string Project, string Contract, string UserId, long? FolderId, string Department, string RoleGroup, DataTable dtPart);
        DataTable ImportPartItem(string Project, string Contract, string UserId, long? FolderId, string Department, string RoleGroup, DataTable dtPart);
        DataTable ImportBOMItem(string Project, string Contract, string UserId, long? FolderId, string Department, string RoleGroup, DataTable dtBOM);

        bool ClearCache();
        List<LookupModel> GetItemListForBOM(string Contract, string item);
        List<LookupModel> GetParentItemListForBOM(string project, string item);
        List<LookupModel> GetJEPDOCbyType(int type, string Contract, string DocNo);
        List<LookupModel> GetDOCbyType(int type, string Contract, string DocNo);
        PartModel GetItemDetailByID(Int64 Id);
        PartModel GetItemBOMDetailByID(Int64 Id, Int64 BOMId);
        bool AddEditBOM(PartModel model, string PSNo, string Department, string RoleGroup, out Int64 BOMId, out string errorMsg, out string SuccessMsg, out string RevisionConfirmMsg, out bool RevisionConfirm);
        List<MaterialModel> GetMaterialByProductFormCode(string material, string ProductForm);
        List<SizeCodeModel> GetSizeCode(string SizeCode);
        ItemGroupUOMModel GetItemGroupDetail(string ItemGroup);
        List<SP_DES_GET_PARTLIST_BYITEMKEYS_Result> GetPartListByItemkeys(string Itemkeys);
        PartModel GetPartDetailsForEdit(Int64 ItemId);
        List<SP_DES_GET_ALL_CHILD_PART_Result> GetChildPart(Int64 ManufacturingItem);
        List<SP_DES_GET_ALL_CHILD_BOM_Result> GetChildBom(Int64 ManufacturingItem);
    }
}
