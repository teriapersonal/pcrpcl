﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IEMQS.DESServices
{
    public class SizeCodeService : ISizeCodeService
    {
        public string AddEdit(SizeCodeModel model, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";

                    #region Check Validation
                    if (model.SizeCode != null)
                    {
                        var checkdublicateCode = db.DES039.Where(x => x.SizeCode != model.SizeCode).FirstOrDefault();
                        if (checkdublicateCode != null)
                        {
                            errorMsg = "Size Code is already exist";
                            return "0";
                        }
                    }
                    #endregion

                    var temp = db.DES039.Find(model.SizeCode);
                    DES039 _DES039 = temp == null ? new DES039() : temp;

                    _DES039.SizeCode = model.SizeCode;
                    _DES039.Description1 = model.Description1;
                    _DES039.Description2 = model.Description2;
                    _DES039.Description3 = model.Description3;
                    _DES039.WeightFactor = model.WeightFactor;

                    if (temp == null)
                    {
                        _DES039.CreatedBy = CommonService.objClsLoginInfo.UserName;
                        _DES039.CreatedOn = DateTime.Now;
                        db.DES039.Add(_DES039);
                        errorMsg = "Size Code added successfully";
                    }
                    else
                    {
                        _DES039.EditedBy = CommonService.objClsLoginInfo.UserName;
                        _DES039.EditedOn = DateTime.Now;
                        errorMsg = "Size Code updated successfully";
                    }
                    db.SaveChanges();
                    return _DES039.SizeCode;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SizeCodeModel> GetAllSizeCode()
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES039.OrderBy(x => x.SizeCode).Select(x => new SizeCodeModel
                    {
                        SizeCode = x.SizeCode,
                        Description1 = x.Description1,
                        Description2 = x.Description2,
                        Description3 = x.Description3,
                        WeightFactor = x.WeightFactor
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public SizeCodeModel GetSizeCodebyId(long Id)
        {
            throw new NotImplementedException();
        }

        public List<SP_DES_GET_SIZE_CODE_Result> GetSizeCodeList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_SIZE_CODE(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public Int64 DeleteSizeCode(string Id, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    var check = db.DES039.Find(Id);
                    if (check != null)
                    {
                        db.DES039.Remove(check);
                        db.SaveChanges();
                    }
                    return 1;  //check.SizeCode
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

    }
}
