﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IEMQS.DESServices
{
    public class FolderStructureService : IFolderStructureService
    {
        public long AddEdit(FolderStructureModel model, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = string.Empty;
                    var temp = db.DES008.Find(model.FolderId);
                    DES008 _DES008 = temp == null ? new DES008() : temp;

                    _DES008.FolderName = model.FolderName;
                    _DES008.FolderOrderNo = model.FolderOrderNo;
                    _DES008.Description = model.Description;

                    if (_DES008 != null && _DES008.FolderId > 0)
                    {
                        _DES008.EditedBy = model.EditedBy;
                        _DES008.EditedOn = DateTime.Now;
                        db.DES009.RemoveRange(_DES008.DES009);
                        db.DES010.RemoveRange(_DES008.DES010);

                        List<DES009> _DES009List = new List<DES009>();
                        if (model.RoleGroupList != null && model.RoleGroupList.Count > 0)
                        {
                            foreach (var Role in model.RoleGroupList)
                            {
                                DES009 _DES009 = new DES009();
                                _DES009.FolderId = model.FolderId;
                                _DES009.RoleGroup = Role;
                                _DES009List.Add(_DES009);
                            }
                        }

                        string[] _Lst = new string[1];
                        _Lst = model.CategoryIds.Split(',');
                        List<DES010> _DES010List = new List<DES010>();
                        foreach (var categoryitem in _Lst)
                        {
                            DES010 _DES010 = new DES010();
                            _DES010.MappingId = Convert.ToInt64(categoryitem);
                            _DES010.TypeId = (int)MenuType.Category;
                            _DES010List.Add(_DES010);
                        }

                        _Lst = new string[1];
                        _Lst = model.ActionIds.Split(',');
                        foreach (var actionitem in _Lst)
                        {
                            DES010 _DES010 = new DES010();
                            _DES010.MappingId = Convert.ToInt64(actionitem);
                            _DES010.TypeId = (int)MenuType.Action;
                            _DES010List.Add(_DES010);
                        }

                        _DES008.DES009 = _DES009List;
                        _DES008.DES010 = _DES010List;
                    }
                    else
                    {
                        _DES008.BUId = model.BUId;
                        _DES008.ParentFolderId = model.ParentFolderId;
                        _DES008.CreatedBy = CommonService.objClsLoginInfo.UserName;
                        _DES008.CreatedOn = DateTime.Now;

                        List<DES009> _DES009List = new List<DES009>();
                        if (model.RoleGroupList != null && model.RoleGroupList.Count > 0)
                        {
                            foreach (var Role in model.RoleGroupList)
                            {
                                DES009 _DES009 = new DES009();
                                _DES009.FolderId = model.FolderId;
                                _DES009.RoleGroup = Role;
                                _DES009List.Add(_DES009);
                            }
                        }
                        string[] _Lst = new string[1];
                        _Lst = model.CategoryIds.Split(',');
                        List<DES010> _DES010List = new List<DES010>();
                        foreach (var categoryitem in _Lst)
                        {
                            DES010 _DES010 = new DES010();
                            _DES010.MappingId = Convert.ToInt64(categoryitem);
                            _DES010.TypeId = (int)MenuType.Category;
                            _DES010List.Add(_DES010);
                        }

                        _Lst = new string[1];
                        _Lst = model.ActionIds.Split(',');
                        foreach (var actionitem in _Lst)
                        {
                            DES010 _DES010 = new DES010();
                            _DES010.MappingId = Convert.ToInt64(actionitem);
                            _DES010.TypeId = (int)MenuType.Action;
                            _DES010List.Add(_DES010);
                        }

                        _DES008.DES009 = _DES009List;
                        _DES008.DES010 = _DES010List;
                        db.DES008.Add(_DES008);
                    }


                    db.SaveChanges();
                    return _DES008.FolderId;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw
;
            }
        }

        public List<FolderStructureModel> GetAllFolderStructure()
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES008.OrderBy(x => x.FolderOrderNo==null).ThenBy(x=>x.FolderOrderNo).Select(x => new FolderStructureModel
                    {
                        FolderId = x.FolderId,
                        BUId = x.BUId,
                        ParentFolderId = x.ParentFolderId,
                        FolderName = x.FolderName,
                        FolderOrderNo = x.FolderOrderNo,
                        Description = x.Description
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<FolderStructureModel> GetAllFolderStructureByBUId(Int64 BUId)
        {
            try
            {
                return GetAllFolderStructure().Where(x => x.BUId == BUId).ToList();
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public FolderStructureModel GetCategoryActionByBUIdParentId(Int64 BUId, Int64 ParentFolderId, bool EditModeOnFlag)
        {
            try
            {
                FolderStructureModel model = new FolderStructureModel();
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    if (ParentFolderId > 0)
                    {
                        var _DES008 = GetAllFolderStructure().Where(x => x.FolderId == ParentFolderId).FirstOrDefault();
                        if (_DES008 != null && _DES008.FolderId > 0)
                        {
                            if (EditModeOnFlag)
                            {
                                model.FolderId = _DES008.FolderId;
                                model.BUId = _DES008.BUId;
                                model.FolderName = _DES008.FolderName;
                                model.FolderOrderNo = _DES008.FolderOrderNo;
                                model.Description = _DES008.Description;
                                model.ParentFolderId = _DES008.ParentFolderId;
                            }
                            else
                            {
                                model.ParentFolderId = ParentFolderId;
                            }
                        }
                    }
                    else
                    {
                        var _DES008 = GetAllFolderStructure().Where(x => x.FolderId == ParentFolderId).FirstOrDefault();
                        model.ParentFolderId = ParentFolderId;
                    }

                    var ListCatAct = db.SP_DES_GET_CATEGORY_ACTION_BY_BUID_PARENTFOLDERID(BUId, model.ParentFolderId, model.FolderId).ToList();

                    var _D8 = db.DES008.Where(x => x.FolderId == model.FolderId).FirstOrDefault();
                    model.ListCategory = ListCatAct.Where(x => x.TypeId == (int)MenuType.Category).Select(x => new CategoryActionModel
                    {
                        MappingId = x.MappingId,
                        TypeId = x.TypeId,
                        Name = x.Name,
                        IsChecked = (_D8 != null) ? (_D8.DES010.Select(y => y.MappingId).ToList()).Contains(x.MappingId) : false
                    }).ToList();

                    model.ListAction = ListCatAct.Where(x => x.TypeId == (int)MenuType.Action).Select(x => new CategoryActionModel
                    {
                        MappingId = x.MappingId,
                        TypeId = x.TypeId,
                        Name = x.Name,
                        IsChecked = (_D8 != null) ? (_D8.DES010.Select(y => y.MappingId).ToList()).Contains(x.MappingId) : false
                    }).ToList();

                    if (_D8 != null)
                    {
                        model.RoleGroupList = _D8.DES009.Where(x => x.FolderId == model.FolderId).Select(x => x.RoleGroup).ToList();
                        model.FunctionIds = string.Join(", ", model.RoleGroupList);
                    }
                }
                return model;
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public bool IsAlreadyExist(string FolderName, long? ParentFolderId, long? BUId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    List<DES008> _DES008 = new List<DES008>();
                    _DES008 = db.DES008.Where(x => x.BUId == BUId && x.FolderName.ToLower().Trim() == FolderName.ToLower().Trim() && (x.ParentFolderId == ParentFolderId)).ToList();

                    if (_DES008 != null && _DES008.Count > 0)
                    {
                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public Int64 DeleteFolder(Int64 Id, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    var check = db.DES008.Find(Id);

                    if (check != null)
                    {
                        List<DES008> _DES008List = db.DES008.Where(x => x.ParentFolderId == Id).ToList();
                        if (_DES008List.Count > 0)
                        {
                            _DES008List.ForEach(x =>
                            {
                                db.DES010.RemoveRange(x.DES010);
                                db.DES009.RemoveRange(x.DES009);
                            });
                            db.DES008.RemoveRange(_DES008List);
                        }

                        db.DES010.RemoveRange(check.DES010);
                        db.DES009.RemoveRange(check.DES009);
                        db.DES008.Remove(check);
                        db.SaveChanges();
                        return check.FolderId;
                    }
                    return 0;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<string> GetRoleGroup()
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.SP_DES_GET_ROLE().Select(x => x.RoleGroup).Distinct().ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

    }
}
