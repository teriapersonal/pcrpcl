﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using System;
using System.Collections.Generic;

namespace IEMQS.DESServices
{
    public interface IDOCService
    {
        Int64 AddEdit(DOCModel model, out string errorMsg);

        DOCModel GetDocumentByID(Int64 ID);

        Int64 Delete(Int64 ID, out string errorMsg);
        
        List<ObjectPolicyStepsModel> GetPolicySteps(string type,Int64? BUId, Int64? DocumentId);

        List<JEPDocumentModel> GetJEPDocumentNo(string Department,string Project);       

        List<SP_DES_GET_DOCUMENT_Result> GetDocumentList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, string project, Int64? folderId,string Roles, out int recordsTotal);
    
        List<SP_DES_GET_DOCUMENT_BY_DOCNO_Result> GetDocumentListByDOCNO(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, string project,string DocumentNo, out int recordsTotal);
        bool CheckDocumentNo(string documentNo, Int64? documentId, string project,int? DocumentRevision);

        void GetDocumentNoFromJEP(string DocNo,Int64? DocId);

        List<SP_DES_GET_USER_POLICY_BYLEVEL_Result> GetUserLeavel(string RoleGroup, string Roles, Int64 DocumentId);

        bool UpdateLatestRevision(Int64? DocId);

        List<DOCModel> GetALLRevisionByDocId(Int64? DocId,out string errorMsg);

        List<SP_DES_GET_DOCUMENT_HISTORY_Result> GetDocumentHistory(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, Int64? DocId, out int recordsTotal);

        DOCModel GetJEPData(Int64? JEPDocumentDetailsId);

        List<SP_DES_GET_PENDING_DOCUMENT_Result> GetPendingDocumentList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, string psno, string Roles, out int recordsTotal, string Project);

        List<SP_DES_GET_DOC_USER_POLICY_MASS_PROMOTE_Result> GetMassPromoteUserPolicy(string psno, string Roles,string FilterValue);

        bool IsExistINJEP(DOCFileModel addDocMapp);

        bool IsExistINDCR(Int64 Id);

        bool IsCheckInDOC(string PsNo, Int64? DocId,out string errorMsg);

        bool UpdateIsCheckInDOC(Int64? DocId, bool HasPsNo);

        bool GetDownloadRigts(string CurrentRole,string CurrentRoleGroup,Int64 DocId,string CurrentPsNo);

        string GetRoleGroupByPSNo(string PSNo);
    }
}
