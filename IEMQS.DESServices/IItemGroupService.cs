﻿using IEMQS.DESCore.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMQS.DESServices
{
    public interface IItemGroupService
    {
        List<SP_DES_GET_ALLITEM_GROUP_Result> GetItemGroupList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal);
    }
}
