﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using System;
using System.Collections.Generic;

namespace IEMQS.DESServices
{
    public interface IPolicyService
    {
        Int64 AddEdit(PolicyModel model, out string errorMsg);
        PolicyModel GetPolicybyId(Int64 Id);
        List<PolicyModel> GetAllPolicy();
        List<SP_DES_GET_POLICY_Result> GetPolicyList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal);
        Int64 DeletePolicy(Int64 Id, out string errorMsg);
        bool CheckPolicyNameAlreadyExist(string name, Int64? policyId);
    }
}
