﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IEMQS.DESServices
{
    public class ProjectAuthorizationService : IProjectAuthorizationService
    {
        public List<SP_DES_GET_ProjectAuthorization_Result> GetProjectAuthorizationList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, string Project, string Department, string UserRoles)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    ProjectModel prjdata = new ProjectModel();
                    if (!string.IsNullOrEmpty(Project))
                    {
                        prjdata = new ProjectService().GetProjectByProjectNo(Project);
                    }
                    var data = db.SP_DES_GET_ProjectAuthorization(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, Project, Department, UserRoles, CommonService.objClsLoginInfo.UserName, prjdata.Contract, prjdata.BUCode, prjdata.Location).ToList();
                    if (data.Count() > 0)
                    {
                        data = data.Where(x => x.IsProjectRight == true).ToList();
                    }
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public Int64 Approve(Int64 Id, string UserName)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var _DES056 = db.DES056.Find(Id);
                    if (_DES056 != null)
                    {
                        db.SP_DES_INSERT_PROJECTAUTHORIZATION_APPROVE(_DES056.ProjectUserFolderAuthorizeId, _DES056.BUID, _DES056.Location, _DES056.Project, _DES056.PSNumber, _DES056.FunctionId, UserName, _DES056.ProjectFolderId);
                        _DES056.RequestStatus = ObjectStatus.Completed.ToString();
                        db.SaveChanges();
                        return _DES056.ProjectUserFolderAuthorizeId;
                    }
                    return 0;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public Int64 Reject(Int64 Id)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var _DES056 = db.DES056.Find(Id);
                    if (_DES056 != null)
                    {
                        _DES056.RequestStatus = ObjectStatus.Rejected.ToString();
                        db.SaveChanges();
                        return _DES056.ProjectUserFolderAuthorizeId;
                    }
                    return 0;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }
    }
}