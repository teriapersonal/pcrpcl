﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.SqlServer;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.DESServices
{
    public class PartService : CreateXML, IPartService

    {
        private static MemoryCache _cache = MemoryCache.Default;
        public List<SP_DES_GET_PARTLIST_BYPROJECT_Result> GetPartListByProject(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, string Project, string Roles)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_PARTLIST_BYPROJECT(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, Project, Roles).ToList();
                    int? rt = 0;
                    data.ForEach(x =>
                    {
                        rt = x.TableCount;
                        string qstring = CommonService.Encrypt("ItemId=" + x.ItemId);
                        x.QString = qstring;
                        x.BString = qstring;
                    });

                    recordsTotal = rt ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_RELATEDPARTLIST_BYPROJECT_Result> GetRelatedPartListByProject(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, string Project, string Roles)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_RELATEDPARTLIST_BYPROJECT(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, Project, Roles).ToList();
                    int? rt = 0;
                    data.ForEach(x =>
                    {
                        rt = x.TableCount;
                        string qstring = CommonService.Encrypt("ItemId=" + x.ItemId);
                        x.QString = qstring;
                        x.BString = qstring;
                    });

                    recordsTotal = rt ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }


        public List<SP_DES_GET_PARTBOMLIST_BYPROJECT_Result> GetBOMPartListByProject(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, string Project, string Roles)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_PARTBOMLIST_BYPROJECT(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, Project, Roles).ToList();
                    int? rt = 0;
                    data.ForEach(x =>
                    {
                        rt = x.TableCount;
                        x.QString = CommonService.Encrypt("ItemId=" + x.ItemId);
                        if (x.ItemId > 0 && x.BOMId > 0)
                            x.BString = CommonService.Encrypt("ItemId=" + x.ItemId + "&BOMId=" + x.BOMId);
                        else
                            x.BString = CommonService.Encrypt("ItemId=" + x.ItemId);
                    });

                    recordsTotal = rt ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public bool AddEditItem(PartModel model, string PSNo, out Int64 ItemId, out string ItemKey, out string errorMsg, out string SuccessMsg, out string ItemKeyConflict, out string ExistingItemkey, out bool ConfirmExistingItemkey, out bool ConfirmProductFormCode, out string RevisionConfirmMsg, out bool RevisionConfirm)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    if (model.ItemId > 0)
                    {
                        var data = GetItemDetailByID(model.ItemId.Value);
                        if (data != null)
                        {
                            model.ItemKeyType = data.ItemKeyType;
                            model.OrderPolicy = data.OrderPolicy;
                            model.Type = data.Type;
                            model.ProductFormCodeId = data.ProductFormCodeId;
                            model.ItemKey = data.ItemKey;
                        }
                        else
                        {
                            ItemId = 0;
                            ItemKey = "";
                            errorMsg = "";
                            SuccessMsg = "";
                            ItemKeyConflict = "";
                            ExistingItemkey = "";
                            ConfirmExistingItemkey = false;
                            ConfirmProductFormCode = false;
                            RevisionConfirmMsg = "";
                            RevisionConfirm = false;
                            return false;
                        }
                    }
                    var out_ItemId = new ObjectParameter("out_ItemId", typeof(long?));
                    var out_ItemKey = new ObjectParameter("out_ItemKey", typeof(string));
                    var out_ErrorMsg = new ObjectParameter("out_ErrorMsg", typeof(string));
                    var out_SuccessMsg = new ObjectParameter("out_SuccessMsg", typeof(string));
                    var out_ItemKeyConflict = new ObjectParameter("out_ItemKeyConflict", typeof(string));
                    var out_ExistingItemkey = new ObjectParameter("out_ExistingItemkey", typeof(string));
                    var out_ConfirmExistingItemkey = new ObjectParameter("out_ConfirmExistingItemkey", typeof(bool));
                    var out_ConfirmProductFormCode = new ObjectParameter("out_ConfirmProductFormCode", typeof(bool));
                    var out_RevisionConfirmMsg = new ObjectParameter("out_RevisionConfirmMsg", typeof(string));
                    var out_RevisionConfirm = new ObjectParameter("out_RevisionConfirm", typeof(bool));
                    db.SP_DES_ADD_ITEM(model.ItemId, model.ItemKeyType, model.OrderPolicy, model.ProductFormCodeId, model.ItemKey
                         , model.Type, model.PolicyId, model.RevNo, model.Material, model.ItemGroup, model.ProcurementDrgDocumentId, model.ItemName, model.String2
                         , model.String3, model.String4, model.ARMSet, model.ARMRev, model.SizeCode, model.Thickness, model.CladPlatePartThickness1, model.CladSpecificGravity1, model.IsDoubleClad, model.CladPlatePartThickness2
                         , model.CladSpecificGravity2, model.ItemWeight, model.UOM, model.CRSYN, model.Density, model.WeightFactor, model.ManufactureItem, model.HdnString2, model.HdnString3, model.HdnString4, model.DoNotReferExistingItemkey, model.DoNotCheckProductFormCode, model.ConfirmRevision, model.Project, PSNo, model.FolderId, model.Department
                         , model.RoleGroup, model.ActionForPart
                         , out_ItemId, out_ItemKey, out_ErrorMsg, out_SuccessMsg, out_ItemKeyConflict, out_ExistingItemkey, out_ConfirmExistingItemkey, out_ConfirmProductFormCode, out_RevisionConfirmMsg, out_RevisionConfirm);


                    Int64.TryParse(Convert.ToString(out_ItemId.Value), out ItemId);
                    ItemKey = Convert.ToString(out_ItemKey.Value);
                    errorMsg = Convert.ToString(out_ErrorMsg.Value);
                    SuccessMsg = Convert.ToString(out_SuccessMsg.Value);
                    ItemKeyConflict = Convert.ToString(out_ItemKeyConflict.Value);
                    ExistingItemkey = Convert.ToString(out_ExistingItemkey.Value);
                    bool.TryParse(Convert.ToString(out_ConfirmExistingItemkey.Value), out ConfirmExistingItemkey);
                    bool.TryParse(Convert.ToString(out_ConfirmProductFormCode.Value), out ConfirmProductFormCode);
                    RevisionConfirmMsg = Convert.ToString(out_RevisionConfirmMsg.Value);
                    bool.TryParse(Convert.ToString(out_RevisionConfirm.Value), out RevisionConfirm);
                    return true;

                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                ItemId = 0;
                ItemKey = "";
                errorMsg = "";
                SuccessMsg = "";
                ItemKeyConflict = "";
                ExistingItemkey = "";
                ConfirmExistingItemkey = false;
                ConfirmProductFormCode = false;
                RevisionConfirmMsg = "";
                RevisionConfirm = false;
                return false;
            }
        }

        public bool AddEditBOM(PartModel model, string PSNo, string Department, string RoleGroup, out Int64 BOMId, out string errorMsg, out string SuccessMsg, out string RevisionConfirmMsg, out bool RevisionConfirm)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var out_BOMId = new ObjectParameter("out_BOMId", typeof(long?));
                    var out_ErrorMsg = new ObjectParameter("out_ErrorMsg", typeof(string));
                    var out_SuccessMsg = new ObjectParameter("out_SuccessMsg", typeof(string));
                    var out_RevisionConfirmMsg = new ObjectParameter("out_RevisionConfirmMsg", typeof(string));
                    var out_RevisionConfirm = new ObjectParameter("out_RevisionConfirm", typeof(bool));
                    db.SP_DES_ADD_BOM(model.ItemId, model.BOMModel.ParentItemId, model.BOMModel.ActionForPCRelation, model.BOMModel.DRGNoDocumentId, model.BOMModel.FindNumber, model.BOMModel.ExtFindNumber, model.BOMModel.FindNumberDescription, model.BOMModel.JobQty, model.BOMModel.CommissioningSpareQty, model.BOMModel.MandatorySpareQty, model.BOMModel.OperationSpareQty, model.BOMModel.ExtraQty, model.BOMModel.Quantity, model.BOMModel.Length, model.BOMModel.Width, model.BOMModel.NumberOfPieces, model.BOMModel.Weight, model.BOMModel.BomReportSize, model.BOMModel.DrawingBomSize, model.BOMModel.HdnBomReportSize, model.BOMModel.HdnDrawingBomSize, model.BOMModel.Remarks, PSNo, Department, RoleGroup, model.Project, model.PolicyId, model.BOMModel.ConfirmRevision, out_BOMId, out_ErrorMsg, out_SuccessMsg, out_RevisionConfirmMsg, out_RevisionConfirm);

                    Int64.TryParse(Convert.ToString(out_BOMId.Value), out BOMId);
                    errorMsg = Convert.ToString(out_ErrorMsg.Value);
                    SuccessMsg = Convert.ToString(out_SuccessMsg.Value);
                    RevisionConfirmMsg = Convert.ToString(out_RevisionConfirmMsg.Value);
                    bool.TryParse(Convert.ToString(out_RevisionConfirm.Value), out RevisionConfirm);
                    return true;

                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                BOMId = 0;
                errorMsg = "";
                SuccessMsg = "";
                RevisionConfirmMsg = "";
                RevisionConfirm = false;
                return false;
            }
        }

        public PartModel GetPartByID(Int64 Id, Int64? BOMId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {

                    var data = db.DES066.Where(x => x.ItemId == Id).Select(x => new PartModel
                    {
                        ItemId = x.ItemId,
                        ItemList = new List<SelectListItem> { new SelectListItem { Text = x.ItemKey, Value = SqlFunctions.StringConvert((double?)x.ItemId).Trim(), Selected = true } },
                        Project = x.Project,
                        Contract = x.DES051.Contract,
                        ItemKeyType = x.ItemKeyType,
                        OrderPolicy = x.OrderPolicy,
                        ProductFormCodeId = x.ProductFormCodeId,
                        ProductForm = x.DES028.ProductForm,
                        Type = x.Type,
                        ItemKey = x.ItemKey,
                        PolicyId = x.PolicyId,
                        Policy = x.DES005.Name,
                        ReviseWithBOM = x.ReviseWithBOM ?? false,
                        Material = x.Material,
                        ItemGroup = x.ItemGroup,
                        ItemName = x.ItemName,
                        String2 = x.String2,
                        String3 = x.String3,
                        String4 = x.String4,
                        ARMSet = x.ARMSet,
                        SizeCode = x.SizeCode,
                        CRSYN = x.CRSYN,
                        RevNo = x.RevNo,
                        Department = x.Department,
                        RoleGroup = x.RoleGroup,
                        Status = x.Status,
                        CreatedBy = x.CreatedBy,
                        CreatedOn = x.CreatedOn,
                        EditedBy = x.EditedBy,
                        EditedOn = x.EditedOn,
                        FolderId = x.FolderId,
                        Thickness = x.Thickness,
                        CladPlatePartThickness1 = x.CladPlatePartThickness1,
                        IsDoubleClad = x.IsDoubleClad ?? "No",
                        CladPlatePartThickness2 = x.CladPlatePartThickness2,
                        CladSpecificGravity2 = x.CladSpecificGravity2,
                        CladSpecificGravity1 = x.CladSpecificGravity1,
                        ItemWeight = x.ItemWeight,
                        IsFromLN = x.IsFromLN ?? false,
                        IsFromFKMS = x.IsFromFKMS ?? false,
                        ManufactureItem = x.ManufactureItem,
                        ARMRev = x.ARMRev,
                        Density = x.Density,
                        WeightFactor = x.WeightFactor,
                        UOM = x.UOM,
                        HdnString2 = x.HdnString2,
                        HdnString3 = x.HdnString3,
                        HdnString4 = x.HdnString4,
                        ProcurementDrgDocumentId = x.ProcurementDrgDocumentId,
                        ProcurementDrgDocumentNo = x.DES059.DocumentNo,
                    }).FirstOrDefault();

                    //if (data != null && data.ProductFormCodeId > 0)
                    //{
                    //    data.ProductForm = GetProductFormDataById(data.ProductFormCodeId).ProductForm;
                    //}

                    if (BOMId > 0)
                    {
                        data.BOMModel = db.DES067.Where(y => y.BOMId == BOMId).Select(x => new BOMModel
                        {
                            BOMId = x.BOMId,
                            ItemId = x.ItemId,
                            ParentItemId = x.ParentItemId,
                            ParentItemList = new List<SelectListItem> { new SelectListItem { Text = x.DES0661.ItemKey, Value = SqlFunctions.StringConvert((double?)x.ParentItemId).Trim(), Selected = true } },
                            DRGNoDocumentId = x.DRGNoDocumentId,
                            DRGNoList = new List<SelectListItem> { new SelectListItem { Text = x.DES059.DocumentNo, Value = SqlFunctions.StringConvert((double?)x.DRGNoDocumentId).Trim(), Selected = true } },
                            FindNumber = x.FindNumber,
                            ExtFindNumber = x.ExtFindNumber,
                            FindNumberDescription = x.FindNumberDescription,
                            ActionForPCRelation = "Update",
                            Weight = x.Weight,
                            JobQty = x.JobQty,
                            CommissioningSpareQty = x.CommissioningSpareQty,
                            MandatorySpareQty = x.MandatorySpareQty,
                            OperationSpareQty = x.OperationSpareQty,
                            ExtraQty = x.ExtraQty,
                            Remarks = x.Remarks,
                            Quantity = x.Quantity,
                            Length = x.Length,
                            Width = x.Width,
                            NumberOfPieces = x.NumberOfPieces,
                            BomReportSize = x.BomReportSize,
                            DrawingBomSize = x.DrawingBomSize,
                            CreatedBy = x.CreatedBy,
                            CreatedOn = x.CreatedOn,
                            EditedBy = x.EditedBy,
                            EditedOn = x.EditedOn,
                            HdnBomReportSize = x.HdnBomReportSize,
                            HdnDrawingBomSize = x.HdnDrawingBomSize,
                        }).FirstOrDefault();
                    }
                    return data;

                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public PartModel GetItemDetailByID(Int64 Id)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.DES066.Where(x => x.ItemId == Id).Select(x => new PartModel
                    {
                        ItemId = x.ItemId,
                        Project = x.Project,
                        Contract = x.DES051.Contract,
                        ItemKeyType = x.ItemKeyType,
                        OrderPolicy = x.OrderPolicy,
                        ProductFormCodeId = x.ProductFormCodeId,
                        ProductFormCodeName = x.DES028.ProductForm + "-" + x.DES028.SubProductForm,
                        ProductForm = x.DES028.ProductForm,
                        Type = x.Type,
                        ItemKey = x.ItemKey,
                        PolicyId = x.PolicyId,
                        Policy = x.DES005.Name,
                        ReviseWithBOM = x.ReviseWithBOM ?? false,
                        Material = x.Material,
                        ItemGroup = x.ItemGroup,
                        ItemName = x.ItemName,
                        String2 = x.String2,
                        String3 = x.String3,
                        String4 = x.String4,
                        ARMSet = x.ARMSet,
                        SizeCode = x.SizeCode,
                        CRSYN = x.CRSYN,
                        RevNo = x.RevNo,
                        Department = x.Department,
                        RoleGroup = x.RoleGroup,
                        Status = x.Status,
                        CreatedBy = x.CreatedBy,
                        CreatedOn = x.CreatedOn,
                        EditedBy = x.EditedBy,
                        EditedOn = x.EditedOn,
                        FolderId = x.FolderId,
                        Thickness = x.Thickness,
                        CladPlatePartThickness1 = x.CladPlatePartThickness1,
                        IsDoubleClad = x.IsDoubleClad ?? "No",
                        CladPlatePartThickness2 = x.CladPlatePartThickness2,
                        CladSpecificGravity2 = x.CladSpecificGravity2,
                        CladSpecificGravity1 = x.CladSpecificGravity1,
                        ItemWeight = x.ItemWeight,
                        IsFromLN = x.IsFromLN ?? false,
                        IsFromFKMS = x.IsFromFKMS ?? false,
                        ManufactureItem = x.ManufactureItem,
                        ARMRev = x.ARMRev,
                        Density = x.Density,
                        WeightFactor = x.WeightFactor,
                        UOM = x.UOM,
                        HdnString2 = x.HdnString2,
                        HdnString3 = x.HdnString3,
                        HdnString4 = x.HdnString4,
                        ProcurementDrgDocumentId = x.ProcurementDrgDocumentId,
                        ProcurementDrgDocumentNo = x.DES059.DocumentNo,
                        ProductFormBOMReportSize = x.DES028.BOMReportSize,
                        ProductFormDrawingBOMSize = x.DES028.DrawingBOMSize,
                    }).FirstOrDefault();
                    data.MaterialDesc = GetMaterial().Where(x => x.Value == data.Material).Select(x => x.Text).FirstOrDefault();
                    data.ItemGroupDesc = (from IG in db.uvwItem_Group
                                          where IG.t_citg == data.ItemGroup
                                          select IG.t_citg + " (" + IG.t_dsca + ")"
                                          ).FirstOrDefault();
                    data.ARMSetDesc = GetARMSet().Where(x => x.Arms == data.ARMSet).Select(x => x.Desc).FirstOrDefault();

                    return data;

                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public PartModel GetItemBOMDetailByID(Int64 Id, Int64 BOMId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.DES066.Where(x => x.ItemId == Id).Select(x => new PartModel
                    {
                        ItemId = x.ItemId,
                        Project = x.Project,
                        Contract = x.DES051.Contract,
                        ItemKeyType = x.ItemKeyType,
                        OrderPolicy = x.OrderPolicy,
                        ProductFormCodeId = x.ProductFormCodeId,
                        ProductFormCodeName = x.DES028.ProductForm + "-" + x.DES028.SubProductForm,
                        ProductForm = x.DES028.ProductForm,
                        Type = x.Type,
                        ItemKey = x.ItemKey,
                        PolicyId = x.PolicyId,
                        Policy = x.DES005.Name,
                        ReviseWithBOM = x.ReviseWithBOM ?? false,
                        Material = x.Material,
                        ItemGroup = x.ItemGroup,
                        ItemName = x.ItemName,
                        String2 = x.String2,
                        String3 = x.String3,
                        String4 = x.String4,
                        ARMSet = x.ARMSet,
                        SizeCode = x.SizeCode,
                        CRSYN = x.CRSYN,
                        RevNo = x.RevNo,
                        Department = x.Department,
                        RoleGroup = x.RoleGroup,
                        Status = x.Status,
                        CreatedBy = x.CreatedBy,
                        CreatedOn = x.CreatedOn,
                        EditedBy = x.EditedBy,
                        EditedOn = x.EditedOn,
                        FolderId = x.FolderId,
                        Thickness = x.Thickness,
                        CladPlatePartThickness1 = x.CladPlatePartThickness1,
                        IsDoubleClad = x.IsDoubleClad ?? "No",
                        CladPlatePartThickness2 = x.CladPlatePartThickness2,
                        CladSpecificGravity2 = x.CladSpecificGravity2,
                        CladSpecificGravity1 = x.CladSpecificGravity1,
                        ItemWeight = x.ItemWeight,
                        IsFromLN = x.IsFromLN ?? false,
                        IsFromFKMS = x.IsFromFKMS ?? false,
                        ManufactureItem = x.ManufactureItem,
                        ARMRev = x.ARMRev,
                        Density = x.Density,
                        WeightFactor = x.WeightFactor,
                        UOM = x.UOM,
                        HdnString2 = x.HdnString2,
                        HdnString3 = x.HdnString3,
                        HdnString4 = x.HdnString4,
                        ProcurementDrgDocumentId = x.ProcurementDrgDocumentId,
                        ProcurementDrgDocumentNo = x.DES059.DocumentNo,
                        ProductFormBOMReportSize = x.DES028.BOMReportSize,
                        ProductFormDrawingBOMSize = x.DES028.DrawingBOMSize,
                    }).FirstOrDefault();
                    data.MaterialDesc = GetMaterial().Where(x => x.Value == data.Material).Select(x => x.Text).FirstOrDefault();
                    if (BOMId > 0)
                    {
                        data.BOMModel = db.DES067.Where(y => y.BOMId == BOMId).Select(x => new BOMModel
                        {
                            BOMId = x.BOMId,
                            ItemId = x.ItemId,
                            ParentItemId = x.ParentItemId,
                            ParentItemKey = x.DES0661.ItemKey,
                            DRGNoDocumentId = x.DRGNoDocumentId,
                            DRGNoDocumentNo = x.DES059.DocumentNo,
                            FindNumber = x.FindNumber,
                            ExtFindNumber = x.ExtFindNumber,
                            FindNumberDescription = x.FindNumberDescription,
                            ActionForPCRelation = "Update",
                            Weight = x.Weight,
                            JobQty = x.JobQty,
                            CommissioningSpareQty = x.CommissioningSpareQty,
                            MandatorySpareQty = x.MandatorySpareQty,
                            OperationSpareQty = x.OperationSpareQty,
                            ExtraQty = x.ExtraQty,
                            Remarks = x.Remarks,
                            Quantity = x.Quantity,
                            Length = x.Length,
                            Width = x.Width,
                            NumberOfPieces = x.NumberOfPieces,
                            BomReportSize = x.BomReportSize,
                            DrawingBomSize = x.DrawingBomSize,
                            CreatedBy = x.CreatedBy,
                            CreatedOn = x.CreatedOn,
                            EditedBy = x.EditedBy,
                            EditedOn = x.EditedOn,
                            HdnBomReportSize = x.HdnBomReportSize,
                            HdnDrawingBomSize = x.HdnDrawingBomSize,
                        }).FirstOrDefault();
                    }
                    return data;

                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public Int64 Delete(Int64 Id, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    var check = db.DES066.Find(Id);
                    if (check != null)
                    {
                        if (check.ManufactureItem == (int)ItemMakeBuy.Make)
                        {
                            errorMsg = "Manufacture Item cannot delete";
                            return 0;
                        }
                        if (check.RevNo > 0)
                        {
                            errorMsg = "Revised Item cannot delete";
                            return 0;
                        }
                        if (check.Status == ObjectStatus.Completed.ToString())
                        {
                            errorMsg = "Released Item cannot delete";
                            return 0;
                        }
                        db.DES038.RemoveRange(db.DES038.Where(x => x.ItemKey == check.ItemKey).ToList());
                        db.DES067.RemoveRange(check.DES067);
                        db.DES094.RemoveRange(check.DES094);
                        db.DES095.RemoveRange(check.DES095);
                        db.DES066.Remove(check);
                        db.SaveChanges();
                    }
                    return check.ItemId;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                errorMsg = "Child relation exist cannot delete this item";
                return 0;
            }
        }

        public List<SelectListItem> GetItemGroup()
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.SP_DES_GET_Item_Group().Select(x => new SelectListItem
                    {
                        Text = x.t_dsca,
                        Value = x.t_citg,
                    }).ToList();

                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<ARMListModel> GetARMSet()
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    if (!_cache.Contains("ARMSet"))
                    {
                        CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
                        cacheItemPolicy.AbsoluteExpiration = DateTime.Now.AddDays(1);
                        db.Database.CommandTimeout = 3 * 60;
                        var armlist = db.DES037.Where(x => x.Status == true).GroupBy(x => new { x.Arms, x.DES036.Description }).Select(x => new ARMListModel
                        {
                            Desc = x.Key.Arms + " (" + x.Key.Description + ")",
                            Arms = x.Key.Arms,
                            Vrsn = x.Max(y => y.Vrsn)
                        }).OrderBy(x => x.Arms).ToList();

                        _cache.Add("ARMSet", armlist, cacheItemPolicy);
                        return armlist;
                    }
                    else
                        return _cache.Get("ARMSet") as List<ARMListModel>;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }

        }

        public List<SizeCodeModel> GetAllSizeCode()
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    if (!_cache.Contains("SizeCode"))
                    {
                        CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
                        cacheItemPolicy.AbsoluteExpiration = DateTime.Now.AddDays(1);
                        db.Database.CommandTimeout = 3 * 60;
                        var SizeCodelist = db.DES039.OrderBy(x => x.SizeCode).Select(x => new SizeCodeModel
                        {
                            SizeCode = x.SizeCode,
                            Description1 = x.Description1,
                            Description2 = x.Description2,
                            Description3 = x.Description3,
                            WeightFactor = x.WeightFactor
                        }).OrderBy(x => x.SizeCode).ToList();
                        _cache.Add("SizeCode", SizeCodelist, cacheItemPolicy);
                        return SizeCodelist;
                    }
                    else
                        return _cache.Get("SizeCode") as List<SizeCodeModel>;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }


        public string GetARMSetLatestVrs(string Arms)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    string s = db.DES037.Where(x => x.Arms == Arms).Max(x => x.Vrsn);
                    return s;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }

        }

        public List<MaterialModel> GetMaterial()
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    if (!_cache.Contains("Material"))
                    {
                        CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
                        cacheItemPolicy.AbsoluteExpiration = DateTime.Now.AddDays(1);
                        db.Database.CommandTimeout = 3 * 60;
                        var materialset = db.uvwMaterial.Select(y => new MaterialModel
                        {
                            Text = (y.t_mtrl.StartsWith("---")) ? y.t_mtrl : y.t_desc + " (" + y.t_mtrl + ")",
                            Value = y.t_mtrl,
                            ProductForm = y.t_prod,
                            ItemGroup = y.t_itgr
                        }).ToList();
                        _cache.Add("Material", materialset, cacheItemPolicy);
                        return materialset;
                    }
                    else
                        return _cache.Get("Material") as List<MaterialModel>;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }

        }

        public List<PartModel> GetParentData(string ProjectNumber)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES066.Where(x => x.Project == ProjectNumber).Select(x => new PartModel
                    {
                        Project = x.Project
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }

        }

        //Get All Product

        public List<SelectListItem> GetCountractDOCbyType(int type, string Contract)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES059.Where(x => x.DocumentTypeId == type && x.DES051.Contract == Contract && x.Status == ObjectStatus.Completed.ToString()).GroupBy(x => x.DocumentNo).ToList().Select(x => new SelectListItem
                    {
                        Text = x.Key,
                        Value = Convert.ToString(x.Max(y => y.DocumentId))
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SelectListItem> GetCountractJepDOCbyType(int type, string Contract)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES059.Where(x => db.DES075.Any(y => y.DocumentId == x.DocumentId) && x.DocumentTypeId == type && x.DES051.Contract == Contract && x.Status == ObjectStatus.Completed.ToString()).GroupBy(x => x.DocumentNo).ToList().Select(x => new SelectListItem
                    {
                        Text = x.Key,
                        Value = Convert.ToString(x.Max(y => y.DocumentId))
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SelectListItem> GetProjectManufacturedPartList(string project)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES066.Where(x => x.Project == project && x.ManufactureItem == 2).GroupBy(x => x.ItemKey).ToList().Select(x => new SelectListItem
                    {
                        Text = x.Key,// + " (R" + x.Max(y => y.RevNo) + ")",
                        Value = Convert.ToString(x.Max(y => y.ItemId))
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<ProductFormCodeModel> GetProductFormCodeList(Int64? BUId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES028.Where(x => x.BUId == BUId).OrderBy(x => x.ProductForm).Select(x => new ProductFormCodeModel
                    {
                        ProductFormCodeId = x.ProductFormCodeId,
                        ProductForm = x.ProductForm + " - " + x.SubProductForm,
                        ProductFormCode = x.ProductFormCode,
                        Description1 = x.Description1,
                        Description2 = x.Description2,
                        Description3 = x.Description3,
                        BOMReportSize = x.BOMReportSize,
                        BUId = x.BUId,
                        DrawingBOMSize = x.DrawingBOMSize,
                        SubProductForm = x.SubProductForm,
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<PartPolicyModel> GetPartPolicyList(Int64? BUId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    List<string> Objname = Enum.GetValues(typeof(ItemType)).Cast<ItemType>().Select(v => v.ToString().Replace("_", " ")).ToList();
                    return db.DES003.Where(x => x.BUId == BUId && Objname.Contains(x.ObjectName)).ToList().Select(x => new PartPolicyModel
                    {
                        ObjectName = x.ObjectName,
                        PolicyName = x.DES007.Select(y => y.DES005.Name).FirstOrDefault(),
                        PolicyId = x.DES007.Select(y => y.DES005.PolicyId).FirstOrDefault()
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public bool AddEditPartSheet(TempPartModel model, out string errormsg)
        {
            try
            {
                errormsg = "";
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    string psno = CommonService.objClsLoginInfo.UserName;
                    if (model != null && model.PartList != null)
                        foreach (var part in model.PartList)
                        {
                            DES065 _DES065 = db.DES065.Find(part.TempItemId);
                            if (_DES065 != null)
                            {
                                if (part.IsDeleted)
                                {
                                    db.DES065.Remove(_DES065);
                                }
                                else
                                {
                                    _DES065.Project = model.Project;
                                    _DES065.ItemSheetId = model.ItemSheetId;
                                    _DES065.ItemKeyType = part.ItemKeyType;
                                    _DES065.OrderPolicy = part.OrderPolicy;
                                    _DES065.ProductFormCodeId = part.ProductFormCodeId;
                                    _DES065.Type = part.Type;
                                    if (part.ItemKeyType == ItemKeyType.UserDefined.ToString())
                                        _DES065.ItemKey = part.ItemKey;
                                    _DES065.PolicyId = part.PolicyId;
                                    _DES065.ReviseWithBOM = part.ReviseWithBOM;
                                    _DES065.DRGNoDocumentId = part.DRGNoDocumentId;
                                    if (part.ParentPartId > 0)
                                    {
                                        var pdata = db.DES066.Find(part.ParentPartId);
                                        if (pdata != null)
                                        {
                                            var latestdata = db.DES066.Where(x => x.ItemKey == pdata.ItemKey).OrderByDescending(x => x.RevNo).FirstOrDefault();
                                            _DES065.ParentPartId = latestdata.ItemId;
                                        }

                                    }
                                    _DES065.FindNumber = part.FindNumber;
                                    _DES065.ExtFindNumber = part.ExtFindNumber;
                                    _DES065.FindNumberDescription = part.FindNumberDescription;
                                    _DES065.ActionForPCRelation = part.ActionForPCRelation;
                                    _DES065.Material = part.Material;
                                    _DES065.ItemGroup = part.ItemGroup;
                                    _DES065.ItemName = part.ItemName;
                                    _DES065.String2 = part.String2;
                                    _DES065.String3 = part.String3;
                                    _DES065.String4 = part.String4;
                                    _DES065.ARMSet = part.ARMSet;
                                    _DES065.ProcurementDrgDocumentId = part.ProcurementDrgDocumentId;
                                    _DES065.Weight = part.Weight;
                                    _DES065.SizeCode = part.SizeCode;
                                    _DES065.Thickness = part.Thickness;
                                    _DES065.CladPlatePartThickness1 = part.CladPlatePartThickness1;
                                    _DES065.IsDoubleClad = part.IsDoubleClad;
                                    _DES065.CladPlatePartThickness2 = part.CladPlatePartThickness2;
                                    _DES065.CladSpecificGravity2 = part.CladSpecificGravity2;
                                    _DES065.CladSpecificGravity1 = part.CladSpecificGravity1;
                                    _DES065.CRSYN = part.CRSYN;
                                    _DES065.JobQty = part.JobQty;
                                    _DES065.CommissioningSpareQty = part.CommissioningSpareQty;
                                    _DES065.MandatorySpareQty = part.MandatorySpareQty;
                                    _DES065.OperationSpareQty = part.OperationSpareQty;
                                    _DES065.ExtraQty = part.ExtraQty;
                                    _DES065.Remarks = part.Remarks;
                                    _DES065.Quantity = part.Quantity;
                                    _DES065.Length = part.Length;
                                    _DES065.Width = part.Width;
                                    _DES065.NumberOfPieces = part.NumberOfPieces;
                                    _DES065.BomReportSize = part.BomReportSize;
                                    _DES065.DrawingBomSize = part.DrawingBomSize;
                                    _DES065.RevNo = part.RevNo;
                                    _DES065.SizeCode = part.SizeCode;
                                    _DES065.ManufactureItem = part.ManufactureItem;
                                    _DES065.ARMRev = part.ARMRev;
                                    _DES065.Density = part.Density;
                                    _DES065.WeightFactor = part.WeightFactor;
                                    _DES065.UOM = part.UOM;
                                    _DES065.HdnBomReportSize = part.HdnBomReportSize;
                                    _DES065.HdnDrawingBomSize = part.HdnDrawingBomSize;
                                    _DES065.ItemWeight = part.ItemWeight;
                                    _DES065.HdnString2 = part.HdnString2;
                                    _DES065.HdnString3 = part.HdnString3;
                                    _DES065.HdnString4 = part.HdnString4;

                                    _DES065.EditedBy = psno;
                                    _DES065.EditedOn = DateTime.Now;
                                }

                            }
                        }
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<TempPartModel> GetTempPartListById(long? itemSheetId, string project, string Contract, Int64? BUId, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    List<DES065> _DES065List = db.DES065.Where(x => x.ItemSheetId == itemSheetId).ToList();
                    if (_DES065List != null && _DES065List.Count() > 0)
                    {
                        string psno = CommonService.objClsLoginInfo.UserName;
                        errorMsg = "";
                        var DRGNoList = GetCountractJepDOCbyType((int)DOCType.Manufacturing_Drawing, Contract);
                        var ManufacturedPartList = GetProjectManufacturedPartList(project);
                        var ProductForm = GetProductFormCodeList(BUId);
                        //ProductFormList = ProductForm;

                        //ItemGroupUOMList = db.uvwSize_Code.Select(y => new ItemGroupUOMModel
                        //{
                        //    t_citg = y.t_citg,
                        //    t_cuni = y.t_cuni,
                        //}).ToList();

                        var SizeCodeList = GetAllSizeCode();
                        var ARMSetList = GetARMSet();
                        var MaterialList = GetMaterial();
                        List<string> ItemGroupNotVisible = db.DES026.Where(x => x.Type == "ItemGroupNotVisible").Select(x => x.Lookup).ToList();
                        var ItemGroupList = db.uvwItem_Group.Where(x => (!ItemGroupNotVisible.Contains(x.t_citg)));//.ToList();


                        var ProcurementDrawingList = GetCountractDOCbyType((int)DOCType.Procurement_Drawing, Contract);
                        // PolicyList = GetPartPolicyList(BUId);
                        var data = _DES065List.Select(x => new TempPartModel
                        {
                            TempItemId = x.TempItemId,
                            Project = x.Project,
                            ItemSheetId = x.ItemSheetId,
                            ItemKeyType = x.ItemKeyType,
                            OrderPolicy = x.OrderPolicy,
                            ProductFormCodeId = x.ProductFormCodeId,
                            Type = x.Type,
                            ItemKey = x.ItemKey,
                            PolicyId = x.PolicyId,
                            Policy = x.DES005?.Name,
                            ReviseWithBOM = x.ReviseWithBOM ?? false,
                            DRGNoDocumentId = x.DRGNoDocumentId,
                            ParentPartId = x.ParentPartId,
                            FindNumber = x.FindNumber,
                            ExtFindNumber = x.ExtFindNumber,
                            FindNumberDescription = x.FindNumberDescription,
                            ActionForPCRelation = x.ActionForPCRelation,
                            Material = x.Material,
                            ItemGroup = x.ItemGroup,
                            ItemName = x.ItemName,
                            String2 = x.String2,
                            String3 = x.String3,
                            String4 = x.String4,
                            ARMSet = x.ARMSet,
                            ProcurementDrgDocumentId = x.ProcurementDrgDocumentId,
                            Weight = x.Weight,
                            SizeCode = x.SizeCode,
                            Thickness = x.Thickness,
                            CladPlatePartThickness1 = x.CladPlatePartThickness1,
                            IsDoubleClad = x.IsDoubleClad,
                            CladPlatePartThickness2 = x.CladPlatePartThickness2,
                            CladSpecificGravity1 = x.CladSpecificGravity1,
                            CladSpecificGravity2 = x.CladSpecificGravity2,
                            CRSYN = x.CRSYN,
                            JobQty = x.JobQty,
                            CommissioningSpareQty = x.CommissioningSpareQty,
                            MandatorySpareQty = x.MandatorySpareQty,
                            OperationSpareQty = x.OperationSpareQty,
                            ExtraQty = x.ExtraQty,
                            Remarks = x.Remarks,
                            Quantity = x.Quantity,
                            Length = x.Length,
                            Width = x.Width,
                            NumberOfPieces = x.NumberOfPieces,
                            BomReportSize = x.BomReportSize,
                            DrawingBomSize = x.DrawingBomSize,
                            RevNo = x.RevNo,
                            CreatedBy = x.CreatedBy,
                            CreatedOn = x.CreatedOn,
                            EditedBy = x.EditedBy,
                            EditedOn = x.EditedOn,


                            BOMId = x.BOMId,
                            ItemId = x.ItemId,
                            ManufactureItem = x.ManufactureItem,
                            ARMRev = x.ARMRev,
                            Density = x.Density,
                            WeightFactor = x.WeightFactor,
                            UOM = x.UOM,
                            HdnBomReportSize = x.HdnBomReportSize,
                            HdnDrawingBomSize = x.HdnDrawingBomSize,
                            ItemWeight = x.ItemWeight,
                            ErrorMsg = x.ErrorMsg,
                            SuccessMsg = x.SuccessMsg,
                            ItemKeyConflict = x.ItemKeyConflict,
                            MultiFindNoItemKey = x.MultiFindNoItemKey ?? false,

                            HdnString2 = x.HdnString2,
                            HdnString3 = x.HdnString3,
                            HdnString4 = x.HdnString4,
                        }).ToList();
                        data.ForEach(x =>
                        {
                            x.ItemKeyTypeList = new CommonService().GetItemKeyType(x.ItemKeyType);
                            x.OrderPolicyList = new CommonService().GetOrderPolicy(x.OrderPolicy);
                            x.ItemTypeList = new CommonService().GetItemType(x.ProductFormCodeId, x.Type);
                            x.ActionforPCRelationList = new CommonService().GetActionforPCRelation(x.ActionForPCRelation);
                            x.DRGNoList = DRGNoList.Select(y => new SelectListItem
                            {
                                Text = y.Text,
                                Value = y.Value,
                                Selected = y.Value == Convert.ToString(x.DRGNoDocumentId)
                            }).ToList();
                            x.ManufacturedPartList = ManufacturedPartList.Select(y => new SelectListItem
                            {
                                Text = y.Text,
                                Value = y.Value,
                                Selected = y.Value == Convert.ToString(x.ParentPartId)
                            }).ToList();
                            x.ProductFormCodeList = ProductForm.Select(y => new SelectListItem
                            {
                                Text = y.ProductForm,
                                Value = Convert.ToString(y.ProductFormCodeId),
                                Selected = y.ProductFormCodeId == x.ProductFormCodeId
                            }).ToList();

                            if (x.ProductFormCodeId > 0)
                            {
                                var pdata = db.DES028.Find(x.ProductFormCodeId);
                                x.MaterialList = MaterialList.Where(t => t.ProductForm == pdata.ProductForm).Select(y => new SelectListItem
                                {
                                    Text = y.Text,
                                    Value = y.Value,
                                    Selected = y.Value == x.Material
                                }).ToList();
                            }
                            else
                            {
                                x.MaterialList = new List<SelectListItem>();
                            }

                            if (!string.IsNullOrEmpty(x.Material))
                            {
                                var mdata = db.uvwMaterial.Where(t => t.t_mtrl == x.Material).FirstOrDefault();

                                string itemgroup = mdata.t_itgr.Substring(0, 4);
                                x.ItemGroupList = ItemGroupList.Where(y => y.t_citg.StartsWith(itemgroup)).Select(y => new SelectListItem
                                {
                                    Text = y.t_citg + " (" + y.t_dsca + ")",
                                    Value = y.t_citg,
                                    Selected = y.t_citg == x.ItemGroup
                                }).ToList();
                            }
                            else
                            {
                                x.ItemGroupList = new List<SelectListItem>();
                            }

                            x.ARMSetList = ARMSetList.Where(y => y.Arms == x.ARMSet).Select(y => new SelectListItem
                            {
                                Text = y.Desc,
                                Value = y.Arms,
                                Selected = y.Arms == x.ARMSet
                            }).ToList();//.ToList();
                            x.ProcurementDrawingList = ProcurementDrawingList.Select(y => new SelectListItem
                            {
                                Text = y.Text,
                                Value = y.Value,
                                Selected = y.Value == Convert.ToString(x.ProcurementDrgDocumentId)
                            }).ToList();
                            if (!string.IsNullOrEmpty(x.SizeCode))
                            {
                                x.SizeCodeList = SizeCodeList.Where(y => y.SizeCode == x.SizeCode).Select(y => new SelectListItem
                                {
                                    Text = y.SizeCode,
                                    Value = y.SizeCode,
                                    Selected = y.SizeCode == Convert.ToString(x.SizeCode)
                                }).ToList();
                            }
                            else
                            {
                                x.SizeCodeList = new List<SelectListItem>();
                            }
                        });
                        return data;
                    }
                    else
                    {
                        return Add5Row(0, 15, itemSheetId, project, Contract, BUId, out errorMsg);
                    }


                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<TempPartModel> Add5Row(int TNo, int Addrow, long? itemSheetId, string project, string Contract, Int64? BUId, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    string psno = CommonService.objClsLoginInfo.UserName;
                    errorMsg = "";
                    var DRGNoList = GetCountractJepDOCbyType((int)DOCType.Manufacturing_Drawing, Contract);
                    var ManufacturedPartList = GetProjectManufacturedPartList(project);
                    var ProductForm = GetProductFormCodeList(BUId);
                    // ProductFormList = ProductForm;
                    //var MaterialList = db.uvwMaterial.ToList();
                    //List<string> ItemGroupNotVisible = db.DES026.Where(x => x.Type == "ItemGroupNotVisible").Select(x => x.Lookup).ToList();
                    //var ItemGroupList = db.uvwItem_Group.Where(x => (!ItemGroupNotVisible.Contains(x.t_citg))).ToList();

                    //ItemGroupUOMList = db.uvwSize_Code.Select(y => new ItemGroupUOMModel
                    //{
                    //    t_citg = y.t_citg,
                    //    t_cuni = y.t_cuni,
                    //}).ToList();

                    //var ARMSetList = GetARMSet();
                    var ProcurementDrawingList = GetCountractDOCbyType((int)DOCType.Procurement_Drawing, Contract);
                    // PolicyList = GetPartPolicyList(BUId);

                    var ItemKeyTypeList = new CommonService().GetItemKeyType();
                    var OrderPolicyList = new CommonService().GetOrderPolicy();
                    var ActionforPCRelationList = new CommonService().GetActionforPCRelation();

                    var ProductFormSelectList = ProductForm.Select(y => new SelectListItem
                    {
                        Text = y.ProductForm,
                        Value = Convert.ToString(y.ProductFormCodeId)
                    }).ToList();
                    //var MaterialSelectList = MaterialList.Select(x => new SelectListItem
                    //{
                    //    Text = x.t_mtrl,
                    //    Value = x.t_mtrl
                    //}).ToList();
                    //var ItemGroupSelectList = ItemGroupList.Select(y => new SelectListItem
                    //{
                    //    Text = y.t_dsca,
                    //    Value = y.t_citg,
                    //}).ToList();
                    //var ARMSetSelectList = ARMSetList.Select(y => new SelectListItem
                    //{
                    //    Text = y.Desc,
                    //    Value = y.Arms,
                    //}).ToList();
                    //var SizeCodeList = GetAllSizeCode();
                    //var SizeCodeSelectList = SizeCodeList.Select(y => new SelectListItem
                    //{
                    //    Text = y.SizeCode,
                    //    Value = y.SizeCode
                    //}).ToList();
                    List<DES065> list = new List<DES065>();
                    var pt = GetPartPolicyList(BUId).Where(x => x.ObjectName == ItemType.Part.ToString()).FirstOrDefault();
                    if (pt == null)
                    {
                        errorMsg = "Policy is not exist";
                        return null;
                    }
                    for (int i = 0; i < Addrow; i++)
                    {
                        list.Add(new DES065
                        {
                            ItemSheetId = itemSheetId,
                            Project = project,
                            Type = pt.ObjectName,
                            PolicyId = pt.PolicyId,
                            CreatedBy = psno,
                            Quantity = 0,
                            RevNo = 0,
                            NumberOfPieces = 0,
                            BomReportSize = "0",
                            DrawingBomSize = "0",
                            CreatedOn = DateTime.Now,
                        });
                    }
                    db.DES065.AddRange(list);
                    db.SaveChanges();
                    List<TempPartModel> TempPartList = new List<TempPartModel>();
                    for (int i = 0; i < TNo; i++)
                    {
                        TempPartList.Add(new TempPartModel());
                    }

                    TempPartList.AddRange(list.Select(x => new TempPartModel
                    {
                        TempItemId = x.TempItemId,
                        Project = x.Project,
                        ItemSheetId = x.ItemSheetId,
                        Type = x.Type,
                        PolicyId = x.PolicyId,
                        Policy = pt.PolicyName,
                        ItemKeyTypeList = ItemKeyTypeList,
                        OrderPolicyList = OrderPolicyList,
                        ItemTypeList = new CommonService().GetItemType(x.ProductFormCodeId, x.Type),
                        ActionforPCRelationList = ActionforPCRelationList,
                        DRGNoList = DRGNoList,
                        ManufacturedPartList = ManufacturedPartList,
                        ProductFormCodeList = ProductFormSelectList,
                        MaterialList = new List<SelectListItem>(),
                        ItemGroupList = new List<SelectListItem>(),
                        ARMSetList = new List<SelectListItem>(),
                        ProcurementDrawingList = ProcurementDrawingList,
                        SizeCodeList = new List<SelectListItem>(),
                        Quantity = x.Quantity,
                        RevNo = x.RevNo,
                        NumberOfPieces = x.NumberOfPieces,
                        BomReportSize = x.BomReportSize,
                        DrawingBomSize = x.DrawingBomSize,
                    }).ToList());
                    return TempPartList;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<PowerViewModel> GetPowerViewList(string Project, long? ItemId = null)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_PART_POWERVIEW(Project, ItemId).Select(x => new PowerViewModel
                    {
                        BOMId = x.BOMId,
                        ItemId = x.ItemId,
                        ParentItemId = x.ParentItemId,
                        ItemKey = x.ItemKey,
                        Type = x.Type,
                        Rev = x.RevNo,
                        policy = x.policy,
                        FindNumber = x.FindNumber,
                        ExtFindNumber = x.ExtFindNumber,
                        ItemName = x.ItemName,
                        Quantity = x.Quantity,
                        Length = x.Length,
                        Width = x.Width,
                        NumberOfPieces = x.NumberOfPieces,
                        UOM = x.UOM,
                        Material = x.Material,
                        State = x.State,
                        ItemGroup = x.ItemGroup,
                        //TableCount = x.TableCount,

                    }).ToList();

                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        // List<TreeViewModel> TreeView = new List<TreeViewModel>();
        public TreeViewModel GetTreeViewList(string Project, long? ItemId = null)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var list = db.SP_DES_GET_PART_POWERVIEW(Project, ItemId).ToList();
                    var data = list.Where(x => x.ItemId == ItemId).Select(x => new TreeViewModel
                    {
                        //id = x.ItemId,
                        name = x.ItemKey,
                        title = ""
                    }).FirstOrDefault();
                    TreeViewModel TreeView = new TreeViewModel();
                    TreeView = (data);
                    GetChildItems(list, TreeView, ItemId);
                    return TreeView;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public void GetChildItems(List<SP_DES_GET_PART_POWERVIEW_Result> treeViewList, TreeViewModel TreeView, Int64? ParentId)
        {
            var listChilds = treeViewList.Where(i => i.ParentItemId == ParentId).ToList();
            TreeView.children = new List<TreeViewModel>();
            if (listChilds != null)
                foreach (var item in listChilds)
                {
                    var data = new TreeViewModel
                    {
                        // id = item.ItemId,
                        name = item.ItemKey,
                        title = "Quantity: " + item.Quantity + ", Find No:" + item.FindNumber
                    };
                    GetChildItems(treeViewList, data, item.ItemId);
                    TreeView.children.Add(data);
                }

        }

        public List<SP_DES_GET_Material_Result> GetAllMaterialList()
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_Material().ToList();
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GENERATE_XML_Result> GetPartBOMDataByProject(string Project)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var ItemGroupUOMList = GetItemGroupUOMModelList().ToList();

                    var MainTopItem = db.DES066.Where(x => x.Project == Project && x.ItemKey.Contains("M1TOP")).OrderByDescending(x => x.RevNo).FirstOrDefault();

                    List<SP_DES_GENERATE_XML_Result> lstPart = new List<SP_DES_GENERATE_XML_Result>();
                    List<SP_DES_GENERATE_XML_Result> FinalPartList = new List<SP_DES_GENERATE_XML_Result>();

                    if (MainTopItem != null)
                    {
                        lstPart = db.SP_DES_GENERATE_XML(Project, MainTopItem.ItemId).ToList();
                        List<Int64?> itemIdlist = lstPart.Select(x => x.ItemId).Distinct().ToList();
                        foreach (var itemid in itemIdlist)
                        {
                            var item = lstPart.Where(x => x.ItemId == itemid).FirstOrDefault();
                            if (item != null)
                            {
                                var _ItemGroup = item.ItemGroup != null ? ItemGroupUOMList.Where(aa => aa.t_citg == item.ItemGroup).FirstOrDefault() : null;
                                if (_ItemGroup != null)
                                {
                                    item.UOM = _ItemGroup.t_cuni != null ? _ItemGroup.t_cuni : string.Empty;
                                    item.SignalCode = _ItemGroup.t_csig != null ? _ItemGroup.t_csig : string.Empty;
                                    item.SelectionCode = _ItemGroup.t_csel != null ? _ItemGroup.t_csel : string.Empty;
                                    item.ProductClass = _ItemGroup.t_cpcl != null ? _ItemGroup.t_cpcl : string.Empty;
                                    item.ProductLine = _ItemGroup.t_cpln != null ? _ItemGroup.t_cpln : string.Empty;
                                }

                                var _ARMData = item.ARMSet != null ? GetARMSetById(item.ARMSet) : null;
                                if (_ARMData != null)
                                {
                                    item.ARMDescription = _ARMData.Desc;
                                    item.ARMText = _ARMData.Arms;
                                }

                                item.ListBOMModel = lstPart.Where(x => x.ItemId == item.ItemId).Select(x => new BOMModel
                                {
                                    BOMId = x.BOMId,
                                    ItemId = x.ItemId,
                                    ParentItemId = x.ParentItemId,
                                    DRGNoDocumentId = x.DRGNoDocumentId,
                                    FindNumber = x.FindNumber,
                                    ExtFindNumber = x.ExtFindNumber,
                                    FindNumberDescription = x.FindNumberDescription,
                                    ActionForPCRelation = x.ActionForPCRelation,
                                    Weight = x.Weight,
                                    JobQty = x.JobQty,
                                    CommissioningSpareQty = x.CommissioningSpareQty,
                                    MandatorySpareQty = x.MandatorySpareQty,
                                    OperationSpareQty = x.OperationSpareQty,
                                    ExtraQty = x.ExtraQty,
                                    Remarks = x.Remarks,
                                    Quantity = x.Quantity,
                                    Length = x.Length,
                                    Width = x.Width,
                                    NumberOfPieces = x.NumberOfPieces,
                                    BomReportSize = x.BomReportSize,
                                    DrawingBomSize = x.DrawingBomSize,
                                    CreatedBy = x.CreatedBy,
                                    CreatedOn = x.CreatedOn,
                                    EditedBy = x.EditedBy,
                                    EditedOn = x.EditedOn,
                                    BOMLevel = x.BOMLevel,
                                    HdnBomReportSize = x.HdnBomReportSize,
                                    HdnDrawingBomSize = x.HdnDrawingBomSize,
                                    DOCModel = db.DES059.Where(z => z.DocumentId == x.DRGNoDocumentId).Select(z => new DOCModel
                                    {
                                        //AddToLocationIp = z.AddToLocationIp,
                                        //BUId = z.BUId,
                                        //Central = z.Central,
                                        ClientDocumentNumber = z.ClientDocumentNumber,
                                        //CurrentLocationIp = z.CurrentLocationIp,
                                        DocumentId = z.DocumentId,
                                        //DocumentIdForGrid = z.DocumentIdForGrid,
                                        CreatedBy = z.CreatedBy,
                                        CreatedOn = z.CreatedOn,
                                        DocumentNo = z.DocumentNo,
                                        DocumentRevision = z.DocumentRevision,
                                        //EditedBy = z.EditedBy,
                                        EditedOn = z.EditedOn,
                                        //DocumentRevisionStr = z.DocumentRevisionStr,
                                        DocumentTitle = z.DocumentTitle,
                                        DocumentTypeId = z.DocumentTypeId,
                                        DocumentType = db.DES026.Where(k => k.Id == z.DocumentTypeId).Select(k => k.Lookup).FirstOrDefault(),
                                        FolderId = z.FolderId,
                                        //IsEdit = z.IsEdit,
                                        //IsJEP = z.IsJEP,
                                        //IsRevision = z.IsRevision,
                                        //JEPDocumentNo = z.JEPDocumentNo,
                                        //Location = z.Location,
                                        //MilestoneDocument = z.MilestoneDocument,
                                        PolicyId = z.PolicyId,
                                        //PolicySteps = z.PolicySteps,
                                        //PreFixDocumentNo = z.PreFixDocumentNo,
                                        Project = z.Project,
                                        Status = z.Status,

                                        ListDOCFileModel = z.DES060.Where(a => a.DocumentId == z.DocumentId).Select(a => new DOCFileModel
                                        {
                                            DocumentId = a.DocumentId,
                                            CreatedBy = a.CreatedBy,
                                            Status = a.Status,
                                            CreatedOn = a.CreatedOn,
                                            DocumentMappingId = a.DocumentMappingId,
                                            DocumentName = a.Document_name,
                                            EditedBy = a.EditedBy,
                                            EditedOn = a.EditedOn,
                                            FileFormate = a.FileFormate,
                                            //IsMainDocument = a.IsMainDocument,
                                            //IsRevisionForDocMapping = a.IsRevisionForDocMapping,
                                            MainDocumentPath = a.MainDocumentPath
                                        }).ToList()
                                    }).FirstOrDefault()
                                }).ToList();

                                FinalPartList.Add(item);
                            }
                        }


                    }
                    return FinalPartList;

                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }

        }

        public Int64 SavePartColumn(PartColumnModel model)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {

                    var psno = CommonService.objClsLoginInfo.UserName;

                    var temp = db.DES092.Where(x => x.PSNO == psno).FirstOrDefault();
                    DES092 _DES092 = temp == null ? new DES092() : temp;

                    _DES092.ItemState = model.ItemState;
                    _DES092.RevNo = model.RevNo;
                    _DES092.FindNumber = model.FindNumber;
                    _DES092.ReExtFindNumbervNo = model.ReExtFindNumbervNo;
                    _DES092.FindNumberDescription = model.FindNumberDescription;
                    _DES092.ARMSet = model.ARMSet;
                    _DES092.ARMRev = model.ARMRev;
                    _DES092.OrderPolicy = model.OrderPolicy;
                    _DES092.ProductForm = model.ProductForm;
                    _DES092.Material = model.Material;
                    _DES092.ItemGroup = model.ItemGroup;
                    _DES092.ItemName = model.ItemName;
                    _DES092.String2 = model.String2;
                    _DES092.String3 = model.String3;
                    _DES092.String4 = model.String4;
                    _DES092.ProcurementDrgDocumentNo = model.ProcurementDrgDocumentNo;
                    _DES092.ItemWeight = model.ItemWeight;
                    _DES092.BOMWeight = model.BOMWeight;
                    _DES092.Thickness = model.Thickness;
                    _DES092.CladPlatePartThickness1 = model.CladPlatePartThickness1;
                    _DES092.IsDoubleClad = model.IsDoubleClad;
                    _DES092.CladPlatePartThickness2 = model.CladPlatePartThickness2;
                    _DES092.CladSpecificGravity2 = model.CladSpecificGravity2;
                    _DES092.CRSYN = model.CRSYN;
                    _DES092.JobQty = model.JobQty;
                    _DES092.CommissioningSpareQty = model.CommissioningSpareQty;
                    _DES092.MandatorySpareQty = model.MandatorySpareQty;
                    _DES092.ExtraQty = model.ExtraQty;
                    _DES092.Remarks = model.Remarks;
                    _DES092.Quantity = model.Quantity;
                    _DES092.Length = model.Length;
                    _DES092.Width = model.Width;
                    _DES092.NumberOfPieces = model.NumberOfPieces;
                    _DES092.BomReportSize = model.BomReportSize;
                    _DES092.DrawingBomSize = model.DrawingBomSize;

                    _DES092.Createdbycolumn = model.Createdbycolumn;
                    _DES092.CreatedOncolumn = model.CreatedOncolumn;
                    _DES092.editedbycolumn = model.editedbycolumn;
                    _DES092.EditedOncolumn = model.EditedOncolumn;
                    if (temp == null)
                    {
                        _DES092.PSNO = psno;
                        _DES092.CreatedBy = psno;
                        _DES092.CreatedOn = DateTime.Now;
                        db.DES092.Add(_DES092);
                    }
                    else
                    {
                        _DES092.EditedBy = psno;
                        _DES092.EditedOn = DateTime.Now;
                    }
                    db.SaveChanges();
                    return _DES092.Id;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public PartColumnModel GetSavePartColumn()
        {
            try
            {
                PartColumnModel obj = new PartColumnModel();
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {

                    var psno = CommonService.objClsLoginInfo.UserName;

                    var data = db.DES092.Where(x => x.PSNO == psno).FirstOrDefault();
                    if (data != null)
                    {
                        obj = db.DES092.Where(x => x.PSNO == psno).Select(x => new PartColumnModel
                        {
                            ItemState = x.ItemState ?? false,
                            RevNo = x.RevNo ?? false,
                            FindNumber = x.FindNumber ?? false,
                            ReExtFindNumbervNo = x.ReExtFindNumbervNo ?? false,
                            FindNumberDescription = x.FindNumberDescription ?? false,
                            ARMSet = x.ARMSet ?? false,
                            ARMRev = x.ARMRev ?? false,
                            OrderPolicy = x.OrderPolicy ?? false,
                            ProductForm = x.ProductForm ?? false,
                            Material = x.Material ?? false,
                            ItemGroup = x.ItemGroup ?? false,
                            ItemName = x.ItemName ?? false,
                            String2 = x.String2 ?? false,
                            String3 = x.String3 ?? false,
                            String4 = x.String4 ?? false,
                            ProcurementDrgDocumentNo = x.ProcurementDrgDocumentNo ?? false,
                            ItemWeight = x.ItemWeight ?? false,
                            BOMWeight = x.BOMWeight ?? false,
                            Thickness = x.Thickness ?? false,
                            CladPlatePartThickness1 = x.CladPlatePartThickness1 ?? false,
                            IsDoubleClad = x.IsDoubleClad ?? false,
                            CladPlatePartThickness2 = x.CladPlatePartThickness2 ?? false,
                            CladSpecificGravity2 = x.CladSpecificGravity2 ?? false,
                            CRSYN = x.CRSYN ?? false,
                            JobQty = x.JobQty ?? false,
                            CommissioningSpareQty = x.CommissioningSpareQty ?? false,
                            MandatorySpareQty = x.MandatorySpareQty ?? false,
                            ExtraQty = x.ExtraQty ?? false,
                            Remarks = x.Remarks ?? false,
                            Quantity = x.Quantity ?? false,
                            Length = x.Length ?? false,
                            Width = x.Width ?? false,
                            NumberOfPieces = x.NumberOfPieces ?? false,
                            BomReportSize = x.BomReportSize ?? false,
                            DrawingBomSize = x.DrawingBomSize ?? false,
                            Createdbycolumn = x.Createdbycolumn ?? false,
                            CreatedOncolumn = x.CreatedOncolumn ?? false,
                            editedbycolumn = x.editedbycolumn ?? false,
                            EditedOncolumn = x.EditedOncolumn ?? false

                        }).FirstOrDefault();
                    }
                    else
                    {
                        obj.ItemState = true;
                        obj.RevNo = true;
                        obj.FindNumber = true;
                        obj.ReExtFindNumbervNo = true;
                        obj.FindNumberDescription = true;
                        obj.ARMSet = true;
                        obj.ARMRev = true;
                        obj.OrderPolicy = true;
                        obj.ProductForm = true;
                        obj.Material = true;
                        obj.ItemGroup = true;
                        obj.ItemName = true;
                        obj.String2 = true;
                        obj.String3 = true;
                        obj.String4 = true;
                        obj.ProcurementDrgDocumentNo = true;
                        obj.ItemWeight = true;
                        obj.BOMWeight = true;
                        obj.Thickness = true;
                        obj.CladPlatePartThickness1 = true;
                        obj.IsDoubleClad = true;
                        obj.CladPlatePartThickness2 = true;
                        obj.CladSpecificGravity2 = true;
                        obj.CRSYN = true;
                        obj.JobQty = true;
                        obj.CommissioningSpareQty = true;
                        obj.MandatorySpareQty = true;
                        obj.ExtraQty = true;
                        obj.Remarks = true;
                        obj.Quantity = true;
                        obj.Length = true;
                        obj.Width = true;
                        obj.NumberOfPieces = true;
                        obj.BomReportSize = true;
                        obj.DrawingBomSize = true;
                        obj.Createdbycolumn = true;
                        obj.CreatedOncolumn = true;
                        obj.editedbycolumn = true;
                        obj.EditedOncolumn = true;
                    }
                }
                return obj;
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_PART_HISTORY_Result> GetPartHistory(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, string itemid, out int recordsTotal)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_PART_HISTORY(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, itemid).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_BOM_HISTORY_Result> GetBOMHistory(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, string itemid, out int recordsTotal)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_BOM_HISTORY(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, itemid).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_PART_REVISION_Result> GetPartRevision(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, string project, string itemkey, out int recordsTotal)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_PART_REVISION(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, project, itemkey).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    data.ForEach(x =>
                    {
                        x.QString = CommonService.Encrypt("ItemId=" + x.ItemId);
                    });
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_ALL_MATERIAL_Result> GetAllMaterialList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, Int64 ProductFormId, out int recordsTotal)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_ALL_MATERIAL(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, ProductFormId).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        //public SP_DES_GET_MATERIAL_BY_ID_Result GetMaterialById(string Id)
        //{
        //    try
        //    {
        //        using (IEMQSDESEntities db = new IEMQSDESEntities())
        //        {
        //            var data = db.SP_DES_GET_MATERIAL_BY_ID(Id).FirstOrDefault();
        //            return data;
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}

        public List<SP_DES_GET_ALL_ITEM_GROUP_Result> GetAllItemGroupList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, string MaterialId, out int recordsTotal)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_ALL_ITEM_GROUP(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, MaterialId).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_ALL_SIZE_Result> GetAllSizeList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_ALL_SIZE(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public SizeCodeModel GetSizeById(string Id)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES039.Where(x => x.SizeCode == Id).Select(x => new SizeCodeModel
                    {
                        SizeCode = x.SizeCode,
                        Description1 = x.Description1,
                        Description2 = x.Description2,
                        Description3 = x.Description3,
                        WeightFactor = x.WeightFactor
                    }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_ALL_ARMSET_Result> GetAllARMSetList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_ALL_ARMSET(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public ARMModel GetARMSetById(string Id)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.DES037.Where(x => x.Arms == Id).OrderByDescending(x => x.Vrsn).Select(x => new ARMModel
                    {
                        Arms = x.Arms,
                        Vrsn = x.Vrsn,
                        Fnam = x.Fnam,
                        Desc = x.Desc,
                        ArmCode = x.ArmCode,
                    }).FirstOrDefault();
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<ItemGroupUOMModel> GetItemGroupUOMModelList()
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    if (!_cache.Contains("LNSizeCode"))
                    {
                        CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
                        cacheItemPolicy.AbsoluteExpiration = DateTime.Now.AddDays(1);
                        db.Database.CommandTimeout = 3 * 60;
                        var LNSizeCodelist = db.uvwSize_Code.Select(y => new ItemGroupUOMModel
                        {
                            t_cuni = y.t_cuni,
                            t_kitm = y.t_kitm,
                            t_citg = y.t_citg,
                            t_csel = y.t_csel,
                            t_csig = y.t_csig,
                            t_cpcl = y.t_cpcl,
                            t_cpln = y.t_cpln,
                        }).ToList();
                        _cache.Add("LNSizeCode", LNSizeCodelist, cacheItemPolicy);
                        return LNSizeCodelist;
                    }
                    else
                        return _cache.Get("LNSizeCode") as List<ItemGroupUOMModel>;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public PartModel GetPartDetailsForEdit(Int64 ItemId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.DES066.Where(x => x.ItemId == ItemId).Select(x => new PartModel
                    {
                        ItemId = x.ItemId,
                        Project = x.Project,
                        Contract = x.DES051.Contract,
                        ItemKeyType = x.ItemKeyType,
                        OrderPolicy = x.OrderPolicy,
                        ProductFormCodeId = x.ProductFormCodeId,
                        Type = x.Type,
                        ItemKey = x.ItemKey,
                        PolicyId = x.PolicyId,
                        Policy = x.DES005.Name,
                        ReviseWithBOM = x.ReviseWithBOM ?? false,
                        Material = x.Material,
                        ItemGroup = x.ItemGroup,
                        ItemName = x.ItemName,
                        String2 = x.String2,
                        String3 = x.String3,
                        String4 = x.String4,
                        ARMSet = x.ARMSet,
                        SizeCode = x.SizeCode,
                        CRSYN = x.CRSYN,
                        RevNo = x.RevNo,
                        Department = x.Department,
                        RoleGroup = x.RoleGroup,
                        Status = x.Status,
                        CreatedBy = x.CreatedBy,
                        CreatedOn = x.CreatedOn,
                        EditedBy = x.EditedBy,
                        EditedOn = x.EditedOn,
                        FolderId = x.FolderId,
                        Thickness = x.Thickness,
                        CladPlatePartThickness1 = x.CladPlatePartThickness1,
                        IsDoubleClad = x.IsDoubleClad,
                        CladPlatePartThickness2 = x.CladPlatePartThickness2,
                        CladSpecificGravity2 = x.CladSpecificGravity2,
                        CladSpecificGravity1 = x.CladSpecificGravity1,
                        ItemWeight = x.ItemWeight,
                        IsFromLN = x.IsFromLN ?? false,
                        IsFromFKMS = x.IsFromFKMS ?? false,
                        ManufactureItem = x.ManufactureItem,
                        ARMRev = x.ARMRev,
                        Density = x.Density,
                        WeightFactor = x.WeightFactor,
                        UOM = x.UOM,
                        HdnString2 = x.HdnString2,
                        HdnString3 = x.HdnString3,
                        HdnString4 = x.HdnString4,
                        ProcurementDrgDocumentId = x.ProcurementDrgDocumentId,
                        ActionForPart = x.Status == ObjectStatus.Completed.ToString() ? PartAction.Revise.ToString() : PartAction.Update.ToString(),
                        ProcurementDrgDocumentNo = x.DES059.DocumentNo,
                        ProductForm = x.DES028.ProductForm,
                        //MaterialList = new List<SelectListItem> { new SelectListItem { Text = x.Material, Value = x.Material, Selected = true } },
                        //ItemGroupList = new List<SelectListItem> { new SelectListItem { Text = x.ItemGroup, Value = x.ItemGroup, Selected = true } },
                        //ARMSetList = new List<SelectListItem> { new SelectListItem { Text = x.ARMSet, Value = x.ARMSet, Selected = true } },
                        SizeCodeList = new List<SelectListItem> { new SelectListItem { Text = x.SizeCode, Value = x.SizeCode, Selected = true } },
                        ProcurementDRGNoList = new List<SelectListItem> { new SelectListItem { Text = x.DES059.DocumentNo, Value = SqlFunctions.StringConvert((double?)x.ProcurementDrgDocumentId).Trim(), Selected = true } },

                    }).FirstOrDefault();
                    data.MaterialList = GetMaterial().Where(x => x.Value == data.Material).Select(x => new SelectListItem { Text = x.Text, Value = x.Value, Selected = true }).ToList();
                    data.ItemGroupList = (from IG in db.uvwItem_Group
                                          where IG.t_citg == data.ItemGroup
                                          select new SelectListItem
                                          {
                                              Value = IG.t_citg,
                                              Text = IG.t_citg + " (" + IG.t_dsca + ")",
                                              Selected = true
                                          }).ToList();
                    data.ARMSetList = GetARMSet().Where(x => x.Arms == data.ARMSet).Select(x => new SelectListItem { Text = x.Desc, Value = x.Arms, Selected = true }).ToList();

                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }
        public PartModel GetUserDefinedItemKey(string ItemKey, string Contract)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES066.Where(x => x.ItemKey == ItemKey && (x.DES051.Contract == Contract || x.OrderPolicy == OrderPolicy.Standard.ToString())).OrderByDescending(x => x.RevNo).Select(x => new PartModel
                    {
                        ItemId = x.ItemId,
                        Project = x.Project,
                        Contract = x.DES051.Contract,
                        ItemKeyType = x.ItemKeyType,
                        OrderPolicy = x.OrderPolicy,
                        ProductFormCodeId = x.ProductFormCodeId,
                        Type = x.Type,
                        ItemKey = x.ItemKey,
                        PolicyId = x.PolicyId,
                        Policy = x.DES005.Name,
                        ReviseWithBOM = x.ReviseWithBOM ?? false,
                        Material = x.Material,
                        ItemGroup = x.ItemGroup,
                        ItemName = x.ItemName,
                        String2 = x.String2,
                        String3 = x.String3,
                        String4 = x.String4,
                        ARMSet = x.ARMSet,
                        SizeCode = x.SizeCode,
                        CRSYN = x.CRSYN,
                        RevNo = x.RevNo,
                        Department = x.Department,
                        RoleGroup = x.RoleGroup,
                        Status = x.Status,
                        CreatedBy = x.CreatedBy,
                        CreatedOn = x.CreatedOn,
                        EditedBy = x.EditedBy,
                        EditedOn = x.EditedOn,
                        FolderId = x.FolderId,
                        Thickness = x.Thickness,
                        CladPlatePartThickness1 = x.CladPlatePartThickness1,
                        IsDoubleClad = x.IsDoubleClad,
                        CladPlatePartThickness2 = x.CladPlatePartThickness2,
                        CladSpecificGravity2 = x.CladSpecificGravity2,
                        CladSpecificGravity1 = x.CladSpecificGravity1,
                        ItemWeight = x.ItemWeight,
                        IsFromLN = x.IsFromLN ?? false,
                        IsFromFKMS = x.IsFromFKMS ?? false,
                        ManufactureItem = x.ManufactureItem,
                        ARMRev = x.ARMRev,
                        Density = x.Density,
                        WeightFactor = x.WeightFactor,
                        UOM = x.UOM,
                        HdnString2 = x.HdnString2,
                        HdnString3 = x.HdnString3,
                        HdnString4 = x.HdnString4,
                        ProcurementDrgDocumentId = x.ProcurementDrgDocumentId,
                        ActionForPart = x.Status == ObjectStatus.Completed.ToString() ? PartAction.Revise.ToString() : PartAction.Update.ToString(),
                        ProcurementDrgDocumentNo = x.DES059.DocumentNo,
                        ProductForm = x.DES028.ProductForm,
                    }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<DDLProject> GetProjects()
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES051.Where(w => w.Status != ObjectStatus.Created.ToString()).OrderBy(x => x.Project).Select(x => new DDLProject
                    {
                        Project = x.Project,
                        ProjectDesc = x.Project
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SelectItemList> GetContractProjects(string Project)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    string contract = db.DES051.Where(x => x.Project == Project).Select(x => x.Contract).FirstOrDefault();
                    return db.DES051.Where(w => w.Contract == contract).OrderBy(x => x.Project).Select(x => new SelectItemList
                    {
                        id = x.Project,
                        text = x.Project
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<DDLManufactureItem> GetManufacturingItembyProject(string Project)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES066.Where(w => w.Project == Project && w.ManufactureItem == 2).GroupBy(x => x.ItemKey).Select(x => new DDLManufactureItem
                    {
                        ItemId = x.Max(z => z.ItemId),
                        ItemKey = x.Key
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public BOMModel GetDataByItemAndParentItem(long ItemId, long ParentItemId, string Project, int? FindNumber)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    long? pid = null;
                    var pdata = db.DES066.Find(ParentItemId);
                    if (pdata != null)
                    {
                        var latestdata = db.DES066.Where(x => x.ItemKey == pdata.ItemKey).OrderByDescending(x => x.RevNo).FirstOrDefault();
                        pid = latestdata.ItemId;
                    }


                    var data = db.DES067.Where(y => y.ItemId == ItemId && y.ParentItemId == pid && y.FindNumber == FindNumber && y.DES066.Project == Project).Select(x => new BOMModel
                    {
                        BOMId = x.BOMId,
                        ItemId = x.ItemId,
                        ParentItemId = x.ParentItemId,
                        DRGNoDocumentId = x.DRGNoDocumentId,
                        FindNumber = x.FindNumber,
                        ExtFindNumber = x.ExtFindNumber,
                        FindNumberDescription = x.FindNumberDescription,
                        ActionForPCRelation = x.ActionForPCRelation,
                        Weight = x.Weight,
                        JobQty = x.JobQty,
                        CommissioningSpareQty = x.CommissioningSpareQty,
                        MandatorySpareQty = x.MandatorySpareQty,
                        OperationSpareQty = x.OperationSpareQty,
                        ExtraQty = x.ExtraQty,
                        Remarks = x.Remarks,
                        Quantity = x.Quantity,
                        Length = x.Length,
                        Width = x.Width,
                        NumberOfPieces = x.NumberOfPieces,
                        BomReportSize = x.BomReportSize,
                        DrawingBomSize = x.DrawingBomSize,
                        CreatedBy = x.CreatedBy,
                        CreatedOn = x.CreatedOn,
                        EditedBy = x.EditedBy,
                        EditedOn = x.EditedOn,
                        HdnBomReportSize = x.HdnBomReportSize,
                        HdnDrawingBomSize = x.HdnDrawingBomSize,
                    }).FirstOrDefault();
                    if (data != null && data.DRGNoDocumentId > 0)
                    {
                        data.DRGNoDocumentNo = db.DES059.Where(x => x.DocumentId == data.DRGNoDocumentId).Select(x => x.DocumentNo).FirstOrDefault();
                    }
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_MAINTAIN_ITEMSHEET_Result> MaintainTimesheet(long Id, string psno, string Department, string RoleGroup, long? FolderId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var validation = db.SP_DES_MAINTAIN_ITEMSHEET_VALIDATE(Id).ToList();
                    if (validation != null && validation.Count > 0)
                    {
                        return validation.Select(x => new SP_DES_MAINTAIN_ITEMSHEET_Result
                        {
                            TempItemId = x.TempItemId,
                            ErrorMsg = x.ErrorMsg
                        }).ToList();
                    }
                    var data = db.SP_DES_MAINTAIN_ITEMSHEET(Id, psno, FolderId, Department, RoleGroup).ToList();
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public ProductFormCodeModel GetProductFormDataById(long? ProductFormId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.DES028.Where(x => x.ProductFormCodeId == ProductFormId).Select(x => new ProductFormCodeModel
                    {
                        ProductFormCodeId = x.ProductFormCodeId,
                        ProductForm = x.ProductForm,
                        ProductFormCode = x.ProductFormCode,
                        Description1 = x.Description1,
                        Description2 = x.Description2,
                        Description3 = x.Description3,
                        BOMReportSize = x.BOMReportSize,
                        BUId = x.BUId,
                        DrawingBOMSize = x.DrawingBOMSize,
                        SubProductForm = x.SubProductForm,
                    }).FirstOrDefault();
                    data.TypeList = new CommonService().GetItemType(data.ProductFormCodeId);
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public ItemMatrialModel GetItemGroupFromMaterial(string Material, string ProductForm, string ItemGroup)
        {

            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    List<string> ItemGroupNotVisible = db.DES026.Where(x => x.Type == "ItemGroupNotVisible").Select(x => x.Lookup).ToList();
                    var data = db.uvwMaterial.Where(t => t.t_mtrl == Material && t.t_prod == ProductForm && t.t_itgr == ItemGroup).FirstOrDefault();
                    if (data != null)
                    {
                        string itemgroup = data.t_itgr.Substring(0, 4);
                        return new ItemMatrialModel
                        {
                            Density = data.t_spgr,
                            CladSpecificGravity1 = data.t_dens,
                            ItemGroupUOM = (from IG in db.uvwItem_Group
                                            where IG.t_citg.StartsWith(itemgroup) && (!ItemGroupNotVisible.Contains(IG.t_citg))
                                            select new ItemGroupUOMModel
                                            {
                                                t_citg = IG.t_citg,
                                                t_deca = IG.t_citg + " (" + IG.t_dsca + ")",
                                                Selected = (IG.t_citg == data.t_itgr)
                                            }).ToList()
                        };
                    }
                    else
                        return new ItemMatrialModel { ItemGroupUOM = new List<ItemGroupUOMModel>() };

                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_CHILDITEM_BOM_Result> GetChildItemBOM(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, string Project, out int recordsTotal, long ItemId, string ItemKey)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_CHILDITEM_BOM(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, ItemId, Project, ItemKey).ToList();
                    //recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    int? rt = 0;
                    data.ForEach(x =>
                    {
                        rt = x.TableCount;
                        string qstring = CommonService.Encrypt("ItemId=" + x.ItemId);
                        x.QString = qstring;
                    });

                    recordsTotal = rt ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_PARENTITEM_BOM_Result> GetParentItemBOM(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, string Project, out int recordsTotal, long ItemId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_PARENTITEM_BOM(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, ItemId, Project).ToList();
                    //recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    int? rt = 0;
                    data.ForEach(x =>
                    {
                        rt = x.TableCount;
                        string qstring = CommonService.Encrypt("ItemId=" + x.ItemId);
                        x.QString = qstring;
                    });

                    recordsTotal = rt ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_ITEM_BOMLIST_Result> GetItemBOMList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, long ItemId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_ITEM_BOMLIST(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, ItemId).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;

                    int? rt = 0;
                    data.ForEach(x =>
                    {
                        rt = x.TableCount;
                        x.QString = CommonService.Encrypt("ItemId=" + x.ItemId);
                        if (x.ItemId > 0 && x.BOMId > 0)
                            x.BString = CommonService.Encrypt("ItemId=" + x.ItemId + "&BOMId=" + x.BOMId);
                        else
                            x.BString = CommonService.Encrypt("ItemId=" + x.ItemId);
                    });

                    recordsTotal = rt ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_PART_POLICY_BYLEVEL_Result> GetUserLevel(string RoleGroup, string Roles, Int64 ItemId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_PART_POLICY_BYLEVEL(RoleGroup, Roles, ItemId).ToList();
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                return null;
            }
        }

        public List<PartPolicyStepsModel> GetPolicySteps(string Type, Int64? BUId, Int64? ItemId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    if (ItemId > 0)
                    {
                        var PoliicyData = db.DES095.Where(x => x.ItemId == ItemId).ToList();

                        List<PartPolicyStepsModel> listObjectPolicyStepsModel = new List<PartPolicyStepsModel>();

                        foreach (var x in PoliicyData)
                        {
                            PartPolicyStepsModel objectPolicyStepsModel = new PartPolicyStepsModel();
                            objectPolicyStepsModel.PartLifeCycleId = x.PartLifeCycleId;
                            objectPolicyStepsModel.PolicyId = x.PolicyId;
                            objectPolicyStepsModel.PolicyStep = x.PolicyStep;
                            objectPolicyStepsModel.PolicyOrderID = x.PolicyOrderId;
                            objectPolicyStepsModel.IsCompleted = x.IsCompleted ?? false;
                            objectPolicyStepsModel.CreatedBy = new clsManager().GetUserNameFromPsNo((x.CreatedBy));
                            objectPolicyStepsModel.CreatedOn = x.CreatedOn;
                            objectPolicyStepsModel.ColorCode = x.ColorCode;
                            objectPolicyStepsModel.DesignationLevels = x.DesignationLevels;
                            listObjectPolicyStepsModel.Add(objectPolicyStepsModel);
                        }
                        return listObjectPolicyStepsModel.OrderBy(x => x.PolicyOrderID).ToList();
                    }
                    else
                    {
                        return db.SP_DES_GET_OBJECT_POLICY_BY_OBJECTNAME(Type, BUId).Select(x => new PartPolicyStepsModel
                        {
                            PolicyId = x.PolicyId,
                            PolicyStep = x.StepName,
                            PolicyOrderID = x.OrderNo,
                            ColorCode = x.ColorCode,
                            DesignationLevels = x.DesignationLevels
                        }).ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public bool MassPromote(string project, GetListOfItmeID model, string PsNo, out List<string> errorMsg, out List<string> infoMsg)
        {
            errorMsg = new List<string>();
            infoMsg = new List<string>();
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    List<SP_DES_CHANGE_ITEM_POLICY_Result> data = new List<SP_DES_CHANGE_ITEM_POLICY_Result>();
                    if (model != null && model.PolicyOrderId > 0 && model.ItemId != null && model.ItemId.Count > 0)
                    {
                        string ItemIds = string.Join(",", model.ItemId);
                        data = db.SP_DES_CHANGE_ITEM_POLICY(ItemIds, model.PolicyOrderId, PsNo).ToList();
                        if (data != null && data.Count > 0)
                            foreach (var item in data)
                            {
                                infoMsg.Add(item.ItemKey + " " + model.BtnText + " successfully");

                                //foreach (var items in model.ItemId)
                                {
                                    //if (model.PolicyOrderId == 1)
                                    {
                                        #region forNotification
                                        try
                                        {
                                            db.SP_DES_PART_PUSH_NOTIFICATION(project, CommonService.objClsLoginInfo.UserName, NotificationObject.LifeCycle.ToString(), item.ItemId);
                                        }

                                        catch
                                        {
                                        }
                                        #endregion
                                    }
                                }
                                //#region Notification 


                                //#endregion
                            }
                        List<Int64?> Ritemid = model.ItemId.Where(x => (!data.Select(y => y.ItemId).ToList().Contains(x))).ToList();
                        if (Ritemid != null)
                            foreach (var itemid in Ritemid)
                            {
                                var itemdetails = db.DES066.Find(itemid);
                                if (itemdetails != null)
                                    errorMsg.Add(itemdetails.ItemKey + " item not in " + model.BtnText + " state");

                            }
                    }


                    return true;
                    //var data = GetMassPromoteUserPolicy(PsNo, Roles, model.BtnText.Trim());
                    //if (data != null)
                    //{
                    //    var list = data.Where(x => model.ItemId.Contains(x.ItemId)).ToList();
                    //    if (list != null)
                    //        foreach (var item in list)
                    //        {
                    //            string emsg, imsg;
                    //            UpdateItemPolicy(item.PartLifeCycleId, Project, out emsg, out imsg);
                    //            if (!string.IsNullOrEmpty(emsg))
                    //                errorMsg.Add(emsg);
                    //            if (!string.IsNullOrEmpty(imsg))
                    //                infoMsg.Add(imsg);
                    //        }

                    //    List<Int64?> Ritemid = model.ItemId.Where(x => (!data.Select(y => y.ItemId).ToList().Contains(x))).ToList();
                    //    if (Ritemid != null)
                    //        foreach (var itemid in Ritemid)
                    //        {
                    //            var itemdetails = db.DES066.Find(itemid);
                    //            if (itemdetails != null)
                    //                errorMsg.Add(itemdetails.ItemKey + " item not in " + model.BtnText + " state");

                    //        }
                    //    return true;
                    //}
                    //else return false;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }
        public Int64? UpdateItemPolicy(Int64 PartLifeCycleId, string Project, out string errorMsg, out string infoMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    infoMsg = "";
                    var data = db.DES095.Where(yy => yy.PartLifeCycleId == PartLifeCycleId).FirstOrDefault();

                    if (data != null)
                    {
                        var datalist = db.DES095.Where(yy => yy.ItemId == data.ItemId).ToList().Count;
                        var list = db.DES095.Where(x => x.ItemId == data.ItemId && x.IsCompleted != true).OrderBy(x => x.PolicyOrderId).FirstOrDefault();
                        if (list.PartLifeCycleId == data.PartLifeCycleId)
                        {
                            if (list.PolicyOrderId > 1 && datalist != list.PolicyOrderId)
                            {
                                var PartData = db.DES066.Where(x => x.ItemId == data.ItemId).FirstOrDefault();
                                PartData.Status = ObjectStatus.InProcess.ToString();
                                PartData.EditedBy = CommonService.objClsLoginInfo.UserName;
                                PartData.EditedOn = DateTime.Now;
                                db.Entry(PartData).State = System.Data.Entity.EntityState.Modified;
                            }
                            if (datalist == list.PolicyOrderId)
                            {
                                var PartData = db.DES066.Where(x => x.ItemId == data.ItemId).FirstOrDefault();
                                PartData.Status = ObjectStatus.Completed.ToString();
                                PartData.EditedBy = CommonService.objClsLoginInfo.UserName;
                                PartData.EditedOn = DateTime.Now;
                                db.Entry(PartData).State = System.Data.Entity.EntityState.Modified;
                                var updateDCR = CommonService.CompleteDCR(PartData.ItemKey, null);
                            }

                            if (list.PolicyOrderId == 1)
                            {
                                var getPartStatus = data.DES066.Status;
                                if (getPartStatus == ObjectStatus.Draft.ToString())
                                {
                                    var PartData = db.DES066.Where(x => x.ItemId == data.ItemId).FirstOrDefault();
                                    PartData.Status = ObjectStatus.InProcess.ToString();
                                    PartData.EditedBy = CommonService.objClsLoginInfo.UserName;
                                    PartData.EditedOn = DateTime.Now;
                                    db.Entry(PartData).State = System.Data.Entity.EntityState.Modified;
                                }
                            }

                            // AddPartHistory(data.DES066.ItemId, null, data.DES066.ItemKey + " Updated Part Policy ", CommonService.objClsLoginInfo.UserName);
                            data.CreatedBy = CommonService.objClsLoginInfo.UserName;
                            data.CreatedOn = DateTime.Now;
                            data.IsCompleted = true;
                            db.Entry(data).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                            // infoMsg = "Part " + data.PolicyStep + " Successfully";
                            infoMsg = data.DES066.ItemKey + " " + data.PolicyStep + " successfully";

                            #region Notification 

                            try
                            {
                                db.SP_DES_PART_PUSH_NOTIFICATION(Project, CommonService.objClsLoginInfo.UserName, NotificationObject.LifeCycle.ToString(), data.ItemId);
                            }
                            catch
                            {
                            }
                            #endregion
                        }
                        else
                        {
                            infoMsg = "You Must Complete Before Steps !";
                        }
                    }
                    return data.ItemId;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public Int64? ReviseItemPolicy(Int64? ItemId, string Project, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    var data = db.DES095.Where(yy => yy.ItemId == ItemId).ToList();
                    var partdata = db.DES066.Where(yy => yy.ItemId == ItemId).FirstOrDefault();
                    if (data != null)
                    {
                        foreach (var item in data)
                        {
                            item.IsCompleted = false;
                            item.CreatedBy = "";
                            item.CreatedOn = null;
                            db.Entry(item).State = System.Data.Entity.EntityState.Modified;
                        }
                    }
                    var itemData = db.DES066.Where(yy => yy.ItemId == ItemId).FirstOrDefault();
                    if (itemData != null)
                    {
                        itemData.Status = ObjectStatus.Draft.ToString();
                        db.Entry(itemData).State = System.Data.Entity.EntityState.Modified;
                    }

                    #region Notification
                    try
                    {
                        try
                        {
                            db.SP_DES_PART_PUSH_NOTIFICATION(Project, CommonService.objClsLoginInfo.UserName, NotificationObject.Return.ToString(), partdata.ItemId);
                        }

                        catch
                        {
                        }
                    }
                    catch
                    {
                    }

                    #endregion

                    //AddPartHistory(ItemId, null, "Item Policy has been revised. ", CommonService.objClsLoginInfo.UserName);
                    db.SaveChanges();
                    return ItemId;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        //public void AddPartHistory(Int64? ItemID, Int64? ItemLifCycleId, string Discription, string CreatedBy)
        //{
        //    try
        //    {
        //        using (IEMQSDESEntities db = new IEMQSDESEntities())
        //        {
        //            DES096 _DES096 = new DES096();
        //            _DES096.ItemId = ItemID.Value;
        //            _DES096.PartLifeCycleId = ItemLifCycleId;
        //            _DES096.Description = Discription;
        //            _DES096.CreatedBy = CommonService.objClsLoginInfo.UserName;
        //            _DES096.CreatedOn = DateTime.Now;
        //            db.DES096.Add(_DES096);
        //            db.SaveChanges();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        CommonService.SendErrorToText(ex);
        //        //throw;
        //    }
        //}

        public bool CheckItemKeyIsvalid(string ItemKey, string Contract)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    if (!string.IsNullOrEmpty(ItemKey))
                    {
                        ItemKey = ItemKey.Trim();
                        var data = db.DES066.Where(x => x.ItemKey == ItemKey).FirstOrDefault();
                        if (data != null)
                        {
                            if (data.OrderPolicy == OrderPolicy.Standard.ToString())
                                return false;
                            else if (data.IsFromFKMS == true || data.IsFromLN == true)
                                return true;
                            else if (data.DES051.Contract != Contract)
                                return true;
                        }
                    }
                    return false;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                return false;
            }
        }

        public int PartCopyToSheet(Int64 ManufacturingItem, Int64 ItemSheetId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_PART_COPY_TO_WEBSHEET(ItemSheetId, ManufacturingItem);
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_PENDING_PARTLIST_Result> GetPendingPartList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, string psno, string Roles, string Project, out int recordsTotal)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_PENDING_PARTLIST(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, psno, Roles, Project).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    return data;

                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_PART_USER_POLICY_MASS_PROMOTE_Result> GetMassPromoteUserPolicy(string psno, string Roles, string FilterValue)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_PART_USER_POLICY_MASS_PROMOTE(psno, Roles, FilterValue).ToList();
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SelectItemList> GetProjectsManufactureUniqueItem(List<string> Projects)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    List<string> items = new List<string>();
                    if (Projects != null)
                        for (int i = 0; i < Projects.Count(); i++)
                        {
                            string p = Projects[i];
                            if (i > 0)
                            {
                                items = db.DES066.Where(x => x.Project == p && x.ManufactureItem == 2)
                               .Select(x => x.ItemKey.Replace(x.Project + "-", "")).Where(x => items.Contains(x)).ToList();
                            }
                            else
                            {

                                items = db.DES066.Where(x => x.Project == p && x.ManufactureItem == 2)
                                .Select(x => x.ItemKey.Replace(x.Project + "-", "")).ToList();
                            }
                        }
                    return items.Select(x => new SelectItemList { id = x, text = x }).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<List<SP_DES_SOURCE_TO_DEST_COPYPART_CHILD_ITEM_Result>> Insert_CopyPart_Source_To_destination(PartGroupModel model, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var result = new List<List<SP_DES_SOURCE_TO_DEST_COPYPART_CHILD_ITEM_Result>>();
                    errorMsg = "";
                    if (model.CopyPartGroupList != null)
                    {
                        foreach (var CopyPartGroup in model.CopyPartGroupList)
                        {
                            if (CopyPartGroup != null && (!string.IsNullOrEmpty(CopyPartGroup.ProjectId)) && CopyPartGroup.ManufactureItemId > 0)
                            {
                                var data = db.SP_DES_SOURCE_TO_DEST_COPYPART_CHILD_ITEM(model.Project, model.ManufactureItem, CopyPartGroup.ProjectId, CopyPartGroup.ManufactureItemId, model.PsNo, null, model.Department, model.RoleGroup);
                                result.Add(data.ToList());
                            }
                        }
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public void ExportToExcel(Int64? ItemSheetId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    List<TempPartModel> list = new List<TempPartModel>();
                    List<DES065> _DES065List = db.DES065.Where(x => x.ItemSheetId == ItemSheetId).ToList();

                    if (_DES065List != null && _DES065List.Count() > 0)
                    {
                        var data = _DES065List.Where(x => x.ProductFormCodeId > 0).Select(x => new PrintPartModel
                        {
                            Action_for_PC_Relation = x.ActionForPCRelation,
                            ARM_Rev = x.ARMRev,
                            ARM_Set = x.ARMSet,
                            Item_Key_Type = x.ItemKeyType,
                            Order_Policy = x.OrderPolicy,
                            BOM_Report_Size = x.BomReportSize,
                            Type = x.Type,
                            BOM_Weight = x.Weight,
                            Clad_PlatePart_Thickness_1 = x.CladPlatePartThickness1,
                            Policy = x.DES005.Name,
                            ReviseWithBOM = x.ReviseWithBOM ?? false,
                            Clad_PlatePart_Thickness_2 = x.CladPlatePartThickness2,
                            Clad_Specific_Gravity_1 = x.CladSpecificGravity1,
                            Clad_Specific_Gravity_2 = x.CladSpecificGravity2,
                            Commissioning_Spare_Qty = x.CommissioningSpareQty,
                            CRS = x.CRSYN,
                            Drawing_BOM_Size = x.DrawingBomSize,
                            Material = x.Material,
                            Item_Group = x.ItemGroup,
                            Drg_No = ((x.DRGNoDocumentId != null) ? db.DES059.Find(x.DRGNoDocumentId).DocumentNo : string.Empty),
                            String2 = x.String2,
                            String3 = x.String3,
                            String4 = x.String4,
                            Extra_Qty = x.ExtraQty,
                            Ext_Find_No = x.ExtFindNumber,
                            Item_Weight = x.ItemWeight,
                            Size_Code = x.SizeCode,
                            Thickness = x.Thickness,
                            Find_No = x.FindNumber,
                            IsDoubleClad = x.IsDoubleClad,
                            Find_No_Desc = x.FindNumberDescription,
                            Item_Description = x.ItemName,
                            Item_Id = x.ItemKey,
                            Job_Qty = x.JobQty,
                            Mandatory_Spare_Qty = x.MandatorySpareQty,
                            Number_Of_Pieces = x.NumberOfPieces,
                            Operation_Spare_Qty = x.OperationSpareQty,
                            Parent_Item = ((x.ParentPartId != null) ? db.DES066.Where(u => u.ItemId == x.ParentPartId).Select(u => u.ItemKey).FirstOrDefault() : string.Empty),
                            Remarks = x.Remarks,
                            Quantity = x.Quantity,
                            Length = x.Length,
                            Width = x.Width,
                            Procurement_Drg = ((x.ProcurementDrgDocumentId != null) ? db.DES059.Find(x.ProcurementDrgDocumentId)?.DocumentNo : string.Empty),
                            Product_Form = ((x.ProductFormCodeId != null) ? db.DES028.Find(x.ProductFormCodeId)?.ProductForm : string.Empty),
                            Rev_No = "R" + Convert.ToString(x.RevNo),
                            Specific_Gravity = x.Density,
                            Weight_Factor = x.WeightFactor,
                            UOM = x.UOM,
                        }).ToList();

                        var itemsheet = db.DES064.Find(ItemSheetId);

                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.Buffer = true;
                        HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + itemsheet.Project + "_" + itemsheet.SheetName + ".xls");
                        HttpContext.Current.Response.Charset = "";
                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";

                        //style to format numbers to string
                        string style = @"<style> .textmode { mso-number-format:\@; } </style>";
                        HttpContext.Current.Response.Write(style);
                        HttpContext.Current.Response.Output.Write(ConvertListToHtmlTable(data).ToString());
                        HttpContext.Current.Response.Flush();
                        HttpContext.Current.Response.End();
                    }
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        private StringBuilder ConvertListToHtmlTable<T>(List<T> list)
        {
            StringBuilder sb = new StringBuilder();
            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            sb.Append("<Table border='1'><thead bgcolor='gray'><tr>");
            foreach (PropertyInfo prop in Props)
            {
                sb.Append("<th align='center'>" + prop.Name.Replace("_", " ") + "</th>");
            }
            sb.Append("</tr></thead><tbody>");
            foreach (T item in list)
            {
                sb.Append("<tr>");
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    sb.Append("<td>" + Props[i].GetValue(item, null) + "</td>");
                }
                sb.Append("</tr>");
            }
            sb.Append("</tbody></table>");
            //put a breakpoint here and check datatable
            return sb;
        }
        public bool NFPartCreate(string Project, Int64? ItemId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    #region forNotification

                    try
                    {
                        db.SP_DES_PART_PUSH_NOTIFICATION(Project, CommonService.objClsLoginInfo.UserName, NotificationObject.Create.ToString(), ItemId);
                    }

                    catch
                    {
                    }
                    #endregion
                }
            }
            catch
            {
            }
            return true;
        }
        public DataTable ImportPartAndBOMItem(string Project, string Contract, string UserId, long? FolderId, string Department, string RoleGroup, DataTable dtPart)
        {
            var dtPartReturn = new DataTable();
            try
            {

                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {

                    var conn = db.Database.Connection;
                    var connectionState = conn.State;
                    try
                    {
                        if (connectionState != ConnectionState.Open) conn.Open();
                        using (var cmd = conn.CreateCommand())
                        {
                            var parameter0 = new SqlParameter("@tblDES_PART_AND_BOM_ITEM_IMPORT", SqlDbType.Structured);
                            parameter0.Value = dtPart;
                            parameter0.TypeName = "dbo.DES_PART_AND_BOM_ITEM_IMPORT1";

                            var parameter1 = new SqlParameter("@Project", SqlDbType.VarChar);
                            parameter1.Value = Project;

                            var parameter2 = new SqlParameter("@Contract", SqlDbType.VarChar);
                            parameter2.Value = Contract;

                            var parameter3 = new SqlParameter("@psno", SqlDbType.VarChar);
                            parameter3.Value = UserId;

                            var parameter4 = new SqlParameter("@FolderId", SqlDbType.Int);
                            parameter4.Value = FolderId;

                            var parameter5 = new SqlParameter("@Department", SqlDbType.VarChar);
                            parameter5.Value = Department;

                            var parameter6 = new SqlParameter("@RoleGroup", SqlDbType.VarChar);
                            parameter6.Value = RoleGroup;


                            cmd.CommandText = "SP_DES_IMPORT_PART_AND_BOM_ITEM";
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(parameter0);
                            cmd.Parameters.Add(parameter1);
                            cmd.Parameters.Add(parameter2);
                            cmd.Parameters.Add(parameter3);
                            cmd.Parameters.Add(parameter4);
                            cmd.Parameters.Add(parameter5);
                            cmd.Parameters.Add(parameter6);

                            using (var reader = cmd.ExecuteReader())
                            {
                                dtPartReturn.Load(reader);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        // error handling
                        throw;
                    }
                    finally
                    {
                        if (connectionState != ConnectionState.Closed) conn.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
            return dtPartReturn;
        }
        public DataTable ImportPartItem(string Project, string Contract, string UserId, long? FolderId, string Department, string RoleGroup, DataTable dtPart)
        {
            var dtPartReturn = new DataTable();
            try
            {
                dtPart.Columns["Procurement Drg"].SetOrdinal(10);
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {

                    var conn = db.Database.Connection;
                    var connectionState = conn.State;
                    try
                    {
                        if (connectionState != ConnectionState.Open) conn.Open();
                        using (var cmd = conn.CreateCommand())
                        {
                            var parameter0 = new SqlParameter("@tblDES_PART_ITEM_IMPORT", SqlDbType.Structured);
                            parameter0.Value = dtPart;
                            parameter0.TypeName = "dbo.DES_PART_ITEM_IMPORT";

                            var parameter1 = new SqlParameter("@Project", SqlDbType.VarChar);
                            parameter1.Value = Project;

                            var parameter2 = new SqlParameter("@Contract", SqlDbType.VarChar);
                            parameter2.Value = Contract;

                            var parameter3 = new SqlParameter("@psno", SqlDbType.VarChar);
                            parameter3.Value = UserId;

                            var parameter4 = new SqlParameter("@FolderId", SqlDbType.Int);
                            parameter4.Value = FolderId;

                            var parameter5 = new SqlParameter("@Department", SqlDbType.VarChar);
                            parameter5.Value = Department;

                            var parameter6 = new SqlParameter("@RoleGroup", SqlDbType.VarChar);
                            parameter6.Value = RoleGroup;


                            cmd.CommandText = "SP_DES_IMPORT_PART_ITEM";
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(parameter0);
                            cmd.Parameters.Add(parameter1);
                            cmd.Parameters.Add(parameter2);
                            cmd.Parameters.Add(parameter3);
                            cmd.Parameters.Add(parameter4);
                            cmd.Parameters.Add(parameter5);
                            cmd.Parameters.Add(parameter6);

                            using (var reader = cmd.ExecuteReader())
                            {
                                dtPartReturn.Load(reader);
                                dtPartReturn.Columns["Procurement Drg"].SetOrdinal(14);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        // error handling
                        throw;
                    }
                    finally
                    {
                        if (connectionState != ConnectionState.Closed) conn.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
            return dtPartReturn;
        }
        public DataTable ImportBOMItem(string Project, string Contract, string UserId, long? FolderId, string Department, string RoleGroup, DataTable dtPart)
        {
            var dtPartReturn = new DataTable();
            try
            {

                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {

                    var conn = db.Database.Connection;
                    var connectionState = conn.State;
                    try
                    {
                        if (connectionState != ConnectionState.Open) conn.Open();
                        using (var cmd = conn.CreateCommand())
                        {
                            var parameter0 = new SqlParameter("@tblDES_BOM_ITEM_IMPORT", SqlDbType.Structured);
                            parameter0.Value = dtPart;
                            parameter0.TypeName = "dbo.DES_BOM_ITEM_IMPORT1";

                            var parameter1 = new SqlParameter("@Project", SqlDbType.VarChar);
                            parameter1.Value = Project;

                            var parameter2 = new SqlParameter("@Contract", SqlDbType.VarChar);
                            parameter2.Value = Contract;

                            var parameter3 = new SqlParameter("@psno", SqlDbType.VarChar);
                            parameter3.Value = UserId;

                            var parameter4 = new SqlParameter("@FolderId", SqlDbType.Int);
                            parameter4.Value = FolderId;

                            var parameter5 = new SqlParameter("@Department", SqlDbType.VarChar);
                            parameter5.Value = Department;

                            var parameter6 = new SqlParameter("@RoleGroup", SqlDbType.VarChar);
                            parameter6.Value = RoleGroup;


                            cmd.CommandText = "SP_DES_IMPORT_BOM_ITEM";
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(parameter0);
                            cmd.Parameters.Add(parameter1);
                            cmd.Parameters.Add(parameter2);
                            cmd.Parameters.Add(parameter3);
                            cmd.Parameters.Add(parameter4);
                            cmd.Parameters.Add(parameter5);
                            cmd.Parameters.Add(parameter6);

                            using (var reader = cmd.ExecuteReader())
                            {
                                dtPartReturn.Load(reader);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        // error handling
                        throw;
                    }
                    finally
                    {
                        if (connectionState != ConnectionState.Closed) conn.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
            return dtPartReturn;
        }

        public bool ClearCache()
        {
            try
            {
                if (_cache.Contains("ARMSet"))
                    _cache.Remove("ARMSet");
                if (_cache.Contains("SizeCode"))
                    _cache.Remove("SizeCode");
                if (_cache.Contains("Material"))
                    _cache.Remove("Material");
                if (_cache.Contains("LNSizeCode"))
                    _cache.Remove("LNSizeCode");
                GetARMSet();
                GetAllSizeCode();
                GetMaterial();
                GetItemGroupUOMModelList();
                return true;
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                return false;
            }
        }

        public List<LookupModel> GetItemListForBOM(string Contract, string item)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES066.Where(x => (x.DES051.Contract == Contract || x.OrderPolicy == OrderPolicy.Standard.ToString()) && x.IsFromFKMS != true && x.IsFromLN != true && x.ItemKey.Contains(item)).GroupBy(x => x.ItemKey).Take(10).Select(x => new LookupModel { Lookup = x.Key, Id = x.Max(y => y.ItemId) }).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                return null;
            }
        }

        public List<LookupModel> GetParentItemListForBOM(string project, string item)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES066.Where(x => x.Project == project && x.ManufactureItem == 2 && x.ItemKey.Contains(item)).GroupBy(x => x.ItemKey).Take(10).Select(x => new LookupModel
                    {
                        Lookup = x.Key,
                        Id = x.Max(y => y.ItemId)
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                return null;
            }
        }

        public List<LookupModel> GetJEPDOCbyType(int type, string Contract, string DocNo)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES059.Where(x => db.DES075.Any(y => y.DocumentId == x.DocumentId) && x.DocumentTypeId == type && x.DES051.Contract == Contract && x.Status == ObjectStatus.Completed.ToString() && x.DocumentNo.Contains(DocNo)).GroupBy(x => x.DocumentNo).Take(10).Select(x => new LookupModel
                    {
                        Lookup = x.Key,
                        Id = x.Max(y => y.DocumentId)
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<LookupModel> GetDOCbyType(int type, string Contract, string DocNo)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES059.Where(x => x.DocumentTypeId == type && x.DES051.Contract == Contract && x.Status == ObjectStatus.Completed.ToString() && x.DocumentNo.Contains(DocNo)).GroupBy(x => x.DocumentNo).Take(10).Select(x => new LookupModel
                    {
                        Lookup = x.Key,
                        Id = x.Max(y => y.DocumentId)
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<MaterialModel> GetMaterialByProductFormCode(string material, string ProductForm)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    material = material.ToLower();
                    ProductForm = ProductForm.ToLower();
                    return GetMaterial().Where(t => (t.ProductForm != null && t.ProductForm.ToLower() == ProductForm) && t.Text.ToLower().Contains(material)).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SizeCodeModel> GetSizeCode(string SizeCode)
        {
            try
            {

                return GetAllSizeCode().Where(t => t.SizeCode.Contains(SizeCode)).ToList();

            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public ItemGroupUOMModel GetItemGroupDetail(string ItemGroup)
        {
            try
            {
                return GetItemGroupUOMModelList().Where(x => x.t_citg == ItemGroup).FirstOrDefault();
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_PARTLIST_BYITEMKEYS_Result> GetPartListByItemkeys(string Itemkeys)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_PARTLIST_BYITEMKEYS(Itemkeys).ToList();
                    data.ForEach(x =>
                    {
                        string qstring = CommonService.Encrypt("ItemId=" + x.ItemId);
                        x.QString = qstring;
                        x.BString = qstring;
                    });
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_ALL_CHILD_PART_Result> GetChildPart(Int64 ManufacturingItem)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_ALL_CHILD_PART(ManufacturingItem).ToList();
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_ALL_CHILD_BOM_Result> GetChildBom(Int64 ManufacturingItem)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_ALL_CHILD_BOM(ManufacturingItem).ToList();
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

    }
}
