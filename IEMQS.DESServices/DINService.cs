﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using IEMQSImplementation;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.DESServices
{
    public class DINService : IDINService
    {
        //Get Distribution Group Data for while Add
        public List<DINGroupModel> GetDistributionGroup()
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES033.Select(x => new DINGroupModel
                    {
                        DINGroupId = x.DINGroupId,
                        GroupName = x.GroupName
                    }).Where(x => x.GroupName != null && x.DINGroupId != null).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        //GET DOCList of JEP
        public List<SP_DES_GET_DIN_JEP_DOC_Result> GetJEPDocList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, string Project, Int64 DINId, out int recordsTotal)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_DIN_JEP_DOC(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, Project, DINId).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    return data;

                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        //GET Global DOCList of JEP
        public List<SP_DES_GET_DIN_GLOBAL_JEP_DOC_Result> GetGlobalJEPDocList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, string Project, string DocumentNo, string CurrentProject, Int64 DINId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_DIN_GLOBAL_JEP_DOC(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, Project, DocumentNo, CurrentProject, DINId).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    return data;

                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        //GET Doucument Details of JEP
        public List<SP_DES_GET_DIN_JepDocumentDetails_Result> GetJepDocumentDetails(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, Int64? DINId, string Roles, out int recordsTotal)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_DIN_JepDocumentDetails(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, DINId, Roles).ToList();
                    //recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    recordsTotal = 0;
                    int? rt = 0;
                    data.ForEach(x =>
                    {
                        rt = x.TableCount;
                        x.QString = CommonService.Encrypt("Id=" + x.DocumentId);
                    });
                    recordsTotal = rt ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        //GET DIN
        public List<SP_DES_GET_DIN_Result> GetDINList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, Int64? folderId, string Roles, string Project, out int recordsTotal)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_DIN(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, folderId, Roles, Project).ToList();
                    recordsTotal = 0;
                    int? rt = 0;
                    data.ForEach(x =>
                    {
                        rt = x.TableCount;
                        x.QString = CommonService.Encrypt("Id=" + x.DINId);
                    });
                    recordsTotal = rt ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        //Get Project Details by Project
        public ProjectModel GetProjectDetails(string Project)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES051.Where(x => x.Project == Project).Select(x => new ProjectModel
                    {
                        ProjectDescription = x.ProjectDescription,
                        Contract = x.Contract,
                        Customer = x.Customer,
                        CodeStamp = x.CodeStamp
                        //CustomerForDINReport = x.CustomerForDINReport
                    }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        //Get Policy Steps 
        public List<DINPolicyStepsModel> GetPolicySteps(string type, Int64? BUId, Int64? DINId)
        {

            try
            {
                List<DINPolicyStepsModel> listObjectPolicyStepsModel = new List<DINPolicyStepsModel>();
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    if (DINId > 0)

                    {
                        var PoliicyDatas = db.DES083.Where(x => x.DINId == DINId).ToList();
                        var PoliicyData = PoliicyDatas.OrderBy(x => x.PolicyOrderID).ToList();

                        foreach (var x in PoliicyData)
                        {
                            DINPolicyStepsModel objectPolicyStepsModel = new DINPolicyStepsModel();
                            objectPolicyStepsModel.DINLifeCycleId = x.DINLifeCycleId;
                            objectPolicyStepsModel.PolicyId = x.PolicyID;
                            objectPolicyStepsModel.PolicyStep = x.PolicyStep;
                            objectPolicyStepsModel.PolicyOrderID = x.PolicyOrderID;
                            objectPolicyStepsModel.IsCompleted = x.IsCompleted ?? false;
                            objectPolicyStepsModel.CreatedBy = new clsManager().GetUserNameFromPsNo((x.CreatedBy));
                            objectPolicyStepsModel.CreatedOn = x.CreatedOn;
                            objectPolicyStepsModel.ColorCode = x.ColorCode;
                            objectPolicyStepsModel.DesignationLevels = x.DesignationLevels;
                            listObjectPolicyStepsModel.Add(objectPolicyStepsModel);
                        }

                    }
                    else
                    {
                        listObjectPolicyStepsModel = db.SP_DES_GET_OBJECT_POLICY_BY_OBJECTNAME(type, BUId).Select(x => new DINPolicyStepsModel
                        {
                            PolicyId = x.PolicyId,
                            PolicyStep = x.StepName,
                            PolicyOrderID = x.OrderNo,
                            ColorCode = x.ColorCode,
                            DesignationLevels = x.DesignationLevels
                        }).ToList();
                    }
                }
                return listObjectPolicyStepsModel;
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        //Get Document By Id
        public DINModel GetDocumentByID(Int64? ID)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES081.Where(x => x.DINId == ID).Select(x => new DINModel
                    {
                        DINId = x.DINId,
                        DINNo = x.DINNo,
                        ProjectNo = x.ProjectNo,
                        ContractNo = x.ContractNo,
                        ProjectDescription = x.ProjectDescription,
                        Customer = x.Customer,
                        ASMETag = x.ASMETag,
                        NuclearASME = x.NuclearASME,
                        CustomerForDINReport = x.CustomerForDINReport,
                        IssueNo = x.IssueNo,
                        DistributionGroupId = x.DistributionGroupId,
                        Description = x.Description,
                        Status = x.Status,
                        RoleGroup = x.RoleGroup,
                        EditedBy = x.EditedBy,
                        EditedOn = x.EditedOn,
                    }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        //Add Edit DIN
        public Int64? AddEdit(DINModel model, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";

                    var temp = db.DES081.Find(model.DINId);
                    DES081 _DES081 = temp == null ? new DES081() : temp;

                    _DES081.DINNo = model.DINNoGenratorPrefix + model.DINNo;
                    _DES081.ProjectNo = model.ProjectNo;
                    _DES081.ContractNo = model.ContractNo;
                    _DES081.ProjectDescription = model.ProjectDescription;
                    _DES081.Customer = model.Customer;
                    _DES081.ASMETag = model.ASMETag;
                    _DES081.NuclearASME = model.NuclearASME;
                    _DES081.IssueNo = model.IssueNo;
                    _DES081.DistributionGroupId = model.DistributionGroupId;
                    _DES081.Description = model.Description;
                    _DES081.CustomerForDINReport = model.CustomerForDINReport;
                    if (model.FolderId != null)
                    {
                        _DES081.FolderId = model.FolderId;
                    }
                    else
                    {
                        _DES081.FolderId = temp.FolderId;
                    }
                    _DES081.RoleGroup = model.RoleGroup.Trim();

                    if (temp == null)
                    {
                        _DES081.IsDINCheckIn = true;
                        _DES081.DINCheckInBy = CommonService.objClsLoginInfo.UserName;
                        _DES081.DINCheckInOn = DateTime.Now;
                        _DES081.Department = model.Department;
                        _DES081.IsLatestIssue = true;
                        _DES081.Status = ObjectStatus.Draft.ToString();
                        #region getData From LifeCycle
                        var policy = GetPolicySteps(ObjectName.DIN.ToString(), model.BUId, null); // Get Polocy Steps
                        List<DES083> _DES083List = new List<DES083>();

                        if (policy != null)
                        {
                            foreach (var item in policy)
                            {
                                _DES081.PolicyId = item.PolicyId;
                                _DES083List.Add(new DES083
                                {
                                    PolicyID = item.PolicyId,
                                    PolicyStep = item.PolicyStep,
                                    PolicyOrderID = item.PolicyOrderID,
                                    IsCompleted = false,
                                    DesignationLevels = item.DesignationLevels,
                                    ColorCode = item.ColorCode
                                });
                            }

                        }
                        #endregion

                        _DES081.DES083 = _DES083List;
                        _DES081.CreatedBy = CommonService.objClsLoginInfo.UserName;
                        _DES081.CreatedOn = DateTime.Now;
                        db.DES081.Add(_DES081);
                        db.SaveChanges();
                        AddDINHistory(_DES081.DINId, null, null, _DES081.DINNo + " (Issue No: " + _DES081.IssueNo + ") has been created  successfully", CommonService.objClsLoginInfo.UserName);

                        UpdateDINLifeCycle(_DES081.DINId, out errorMsg);
                    }
                    else
                    {
                        if (temp.IsDINCheckIn != true)
                        {
                            _DES081.IsDINCheckIn = true;
                            _DES081.DINCheckInBy = CommonService.objClsLoginInfo.UserName;
                            _DES081.DINCheckInOn = DateTime.Now;
                        }
                        _DES081.EditedBy = CommonService.objClsLoginInfo.UserName;
                        _DES081.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        AddDINHistory(_DES081.DINId, null, null, _DES081.DINNo + " (Issue No: " + _DES081.IssueNo + ") has been edited successfully", CommonService.objClsLoginInfo.UserName);
                    }
                    Int64? DinId = AddAck(_DES081.DINId, model.ProjectNo);
                    return DinId;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        //Add Ref.Document In DIN
        public Int64 AddDinRefDocument(DINRefDocumentModel model, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";

                    var temp = db.DES082.Find(model.DINRefDocumentId);

                    DES082 _DES082 = temp == null ? new DES082() : temp;

                    _DES082.DINId = model.DINId;

                    var getDINIssue = db.DES081.Where(x => x.DINId == model.DINId).Select(x => x.IssueNo).FirstOrDefault();
                    if (temp == null)
                    {

                        var JEPData = db.DES075.Find(model.DocumentId);
                        if (JEPData != null)
                        {
                            if (JEPData.DocumentId > 0)
                            {
                                var DocData = db.DES059.Find(JEPData.DocumentId);
                                if (DocData != null)
                                {
                                    if (DocData.Status == ObjectStatus.Completed.ToString())
                                    {
                                        _DES082.Status = DINStatus.Added.ToString();
                                    }
                                    else
                                    {
                                        _DES082.Status = DINStatus.NotReleased.ToString();
                                    }
                                }
                                else
                                {
                                    _DES082.Status = DINStatus.NotReleased.ToString();
                                }
                            }
                            else
                            {
                                _DES082.Status = DINStatus.NotReleased.ToString();
                            }
                        }
                        else
                        {
                            _DES082.Status = DINStatus.NotReleased.ToString();
                        }



                        _DES082.JEPDocumentDetailsId = model.DocumentId;
                        _DES082.CreatedBy = CommonService.objClsLoginInfo.UserName;
                        _DES082.CreatedOn = DateTime.Now;
                        db.DES082.Add(_DES082);
                        db.SaveChanges();
                        if (_DES082.Status == DINStatus.NotReleased.ToString())
                        {
                            AddDINHistory(model.DINId, null, null, JEPData.DocNo + " (" + ("R" + JEPData.RevisionNo) + ") - (Status: Not Released) document is added for Issue No: " + getDINIssue, CommonService.objClsLoginInfo.UserName);
                        }
                        else
                        {
                            AddDINHistory(model.DINId, null, null, JEPData.DocNo + " (" + ("R" + JEPData.RevisionNo) + ") - (Status: " + _DES082.Status + ") document is added for Issue No: " + getDINIssue, CommonService.objClsLoginInfo.UserName);
                        }

                    }
                    else
                    {
                        _DES082.EditedBy = CommonService.objClsLoginInfo.UserName;
                        _DES082.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        //AddDINHistory(model.DINId, null, null, model.DocumentNo + " document is added", CommonService.objClsLoginInfo.UserName);
                    }

                    return _DES082.DINRefDocumentId;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        //Add DIN Policy 
        public Int64 AddPolicy(List<ObjectPolicyStepsModel> ObjectPolicyStepsModel, Int64 DINId, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    DES083 _DES083 = new DES083();
                    foreach (var item in ObjectPolicyStepsModel)
                    {
                        var temp = db.DES083.Find(DINId);
                        _DES083 = temp == null ? new DES083() : temp;
                        _DES083.DINId = DINId;
                        _DES083.PolicyID = item.PolicyId;
                        _DES083.PolicyStep = item.PolicyStep;
                        _DES083.PolicyOrderID = item.PolicyOrderID;
                        _DES083.ColorCode = item.ColorCode;
                        _DES083.DesignationLevels = item.DesignationLevels;

                        if (temp == null)
                        {
                            _DES083.CreatedBy = CommonService.objClsLoginInfo.UserName;
                            _DES083.CreatedOn = DateTime.Now;
                            db.DES083.Add(_DES083);
                            db.SaveChanges();
                            // AddDINHistory(DINId, null, null, " DIN Policy For " + _DES083.DES081.DINNo + " has been Created", CommonService.objClsLoginInfo.UserName);
                        }
                        else
                        {
                            _DES083.EditedBy = CommonService.objClsLoginInfo.UserName;
                            _DES083.EditedOn = DateTime.Now;
                            db.SaveChanges();
                            // AddDINHistory(DINId, null, _DES083.PolicyID, " DIN Policy For " + _DES083.DES081.DINNo + " has been Updated", CommonService.objClsLoginInfo.UserName);
                        }

                    }
                    return _DES083.DINLifeCycleId;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }


        public Int64 UpdateDINLifeCycle(Int64? DINID, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    var data = db.DES081.Where(yy => yy.DINId == DINID).FirstOrDefault();
                    if (data != null)
                    {
                        data.CreatedBy = CommonService.objClsLoginInfo.UserName;
                        data.CreatedOn = DateTime.Now;
                        data.Status = ObjectStatus.Created.ToString();
                        data.IsDINCheckIn = false;
                        data.DINCheckInBy = CommonService.objClsLoginInfo.UserName;
                        data.DINCheckInOn = DateTime.Now;
                        db.Entry(data).State = System.Data.Entity.EntityState.Modified;

                        var dataOfLifeCycle = db.DES083.Where(yy => yy.DINId == DINID).OrderBy(x => x.PolicyOrderID).FirstOrDefault();
                        if (dataOfLifeCycle != null)
                        {
                            dataOfLifeCycle.CreatedBy = CommonService.objClsLoginInfo.UserName;
                            dataOfLifeCycle.CreatedOn = DateTime.Now;
                            dataOfLifeCycle.IsCompleted = true;
                            db.Entry(dataOfLifeCycle).State = System.Data.Entity.EntityState.Modified;
                        }

                        db.SaveChanges();
                        //AddDINHistory(data.DINId, null, null, data.DINNo + " DIN is InProcess", CommonService.objClsLoginInfo.UserName);

                        //This is related to Status in DES082

                        var getData = db.DES082.Where(x => x.DINId == DINID).ToList();

                        foreach (var item in getData)
                        {
                            string docStatus = item.Status;
                            var JEPData = db.DES075.Find(item.JEPDocumentDetailsId);
                            if (item.Status == DINStatus.Added.ToString() && JEPData != null)
                            {
                                if (JEPData.DocumentId > 0)
                                {
                                    var DocData = db.DES059.Find(JEPData.DocumentId);
                                    if (DocData != null)
                                    {
                                        if (DocData.Status != ObjectStatus.Completed.ToString())
                                        {
                                            docStatus = DINStatus.NotReleased.ToString();
                                        }
                                    }
                                    else
                                    {
                                        docStatus = DINStatus.NotReleased.ToString();
                                    }
                                }
                                else
                                {
                                    docStatus = DINStatus.NotReleased.ToString();
                                }
                            }

                            var datas = db.DES082.Where(yy => yy.DINRefDocumentId == item.DINRefDocumentId).FirstOrDefault();
                            datas.Status = docStatus;
                            db.Entry(datas).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                        }

                        return data.DINId;
                    }
                    else
                    {
                        errorMsg = "DIN Life Cycle has not been updated.";
                        return 0;
                    }



                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }

        }

        public Int64 UpdateGenralRemark(DINRefDocumentModel model)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.DES082.Where(yy => yy.DINRefDocumentId == model.DINRefDocumentId).FirstOrDefault();
                    data.GeneralRemarks = model.GeneralRemarks;
                    db.Entry(data).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    AddDINHistory(data.DINId, null, null, data.DES075.DocNo + "'s remark added ", CommonService.objClsLoginInfo.UserName);
                    return data.DINRefDocumentId;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }

        }

        public Int64 DeleteDINRefDocument(Int64 Id, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";

                    var check = db.DES082.Find(Id);

                    if (check != null)
                    {
                        var DocNo = check.DES075.DocNo;
                        var DocRev = check.DES075.RevisionNo;
                        var DINIssueNo = check.DES081.IssueNo;
                        string DocStatus = string.Empty;
                        if (check.Status == DINStatus.NotReleased.ToString())
                        {
                            DocStatus = "Not Released";
                        }
                        else
                        {
                            DocStatus = check.Status;
                        }

                        var DINId = check.DES081.DINId;
                        var refID = check.DINRefDocumentId;

                        db.DES082.Remove(check);
                        db.SaveChanges();


                        AddDINHistory(DINId, null, null, DocNo + " (R" + DocRev + ")" + " - (Status: " + DocStatus + ") document is deleted for Issue No: " + DINIssueNo, CommonService.objClsLoginInfo.UserName);
                        return refID;
                    }
                    return 0;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public Int64 DeleteDIN(Int64 DINId, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    var check = db.DES081.Find(DINId);

                    if (check != null)
                    {
                        var DINNO = check.DINNo;
                        var ID = check.DINId;

                        if (check.DES082 != null)
                        {
                            var allDIN = check.DES082.Where(x => x.DINId == DINId).ToList();
                            if (allDIN.Count > 0)
                            {
                                foreach (var item in allDIN)
                                {
                                    var checkChild = db.DES082.Find(item.DINRefDocumentId);
                                    var cID = checkChild.DES081.DINId;
                                    var getChildDOC = checkChild.DES075.DocNo;
                                    db.DES082.Remove(checkChild);
                                    AddDINHistory(cID, null, null, getChildDOC + " is deleted", CommonService.objClsLoginInfo.UserName);
                                }
                                db.SaveChanges();
                            }
                        }
                        if (check.DES083 != null)
                        {
                            var allDIN = check.DES083.Where(x => x.DINId == DINId).ToList();
                            if (allDIN.Count > 0)
                            {
                                foreach (var item in allDIN)
                                {
                                    var checkChild = db.DES083.Find(item.DINLifeCycleId);
                                    var cID = checkChild.DES081.DINId;
                                    db.DES083.Remove(checkChild);
                                    // AddDINHistory(cID, null, null, "DIN child Has Been Deleted ", CommonService.objClsLoginInfo.UserName);
                                }
                                db.SaveChanges();
                            }
                        }

                        if (check.DES099 != null)
                        {
                            var allDIN = check.DES099.Where(x => x.DINId == DINId).ToList();
                            if (allDIN.Count > 0)
                            {
                                foreach (var item in allDIN)
                                {

                                    var allDINAckId = db.DES113.Where(x => x.DINAckID == item.DINAckID).ToList();
                                    if (allDINAckId.Count > 0)
                                    {
                                        db.DES113.RemoveRange(allDINAckId);
                                        db.SaveChanges();
                                    }

                                    var checkChild = db.DES099.Find(item.DINAckID);
                                    if (checkChild != null)
                                    {
                                        db.DES099.Remove(checkChild);
                                        db.SaveChanges();

                                    }
                                }
                            }
                        }
                        db.DES081.Remove(check);
                        db.SaveChanges();

                        AddDINHistory(ID, null, null, DINNO + " is deleted ", CommonService.objClsLoginInfo.UserName);


                        if (DINNO != null)
                        {

                            var Data = db.DES081.Where(x => x.DINNo == DINNO).OrderByDescending(x => x.IssueNo).FirstOrDefault();
                            var DINData = db.DES081.Where(x => x.DINId == Data.DINId).FirstOrDefault();
                            DINData.IsLatestIssue = true;
                            db.Entry(DINData).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                        }

                        return check.DINId;
                    }
                    return 0;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public bool CheckDINNo(string DINNo, long? DINId, string project, int IssueNo)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    DINNo = DINNo.Trim();
                    var data = db.DES081.Where(x => x.DINNo == DINNo && (x.DINId != DINId || DINId == null) && x.IssueNo == IssueNo).ToList();
                    if (data != null && data.Count > 0)
                    {
                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                return false;
            }
        }

        public int GenerateIssueNo(string ProjectNo)
        {
            try
            {
                int nextIssueNo = 0;
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var listOfIssueNo = db.DES081.Where(yy => yy.ProjectNo == ProjectNo).Select(x => x.IssueNo).ToList();

                    if (listOfIssueNo.Count > 0)
                    {
                        int findMax = Convert.ToInt32(listOfIssueNo.Max());
                        nextIssueNo = findMax + 1;
                    }
                    else
                    {
                        nextIssueNo = 1;
                    }
                }
                return nextIssueNo;
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public int GenerateDINNo(string ProjectNo)
        {
            try
            {
                int nextDOCNo = 0;
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var listOfDINNo = db.DES081.Where(yy => yy.ProjectNo == ProjectNo).Select(x => x.DINNo).ToList();
                    List<int> listOfIssueNo = new List<int>();
                    int val = 0;

                    foreach (var item in listOfDINNo)
                    {

                        var getDIN = item.Split('-').LastOrDefault();
                        if (getDIN != "")
                        {
                            Int32.TryParse(getDIN, out val);
                            listOfIssueNo.Add(Convert.ToInt32(val));
                        }
                    }

                    if (listOfIssueNo.Count > 0)
                    {

                        int findMax = Convert.ToInt32(listOfIssueNo.Max());
                        nextDOCNo = findMax + 1;
                    }
                    else
                    {
                        nextDOCNo = 1;
                    }
                }
                return nextDOCNo;
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public void AddDINHistory(Int64? DINId, Int64? DINRefDocumentId, Int64? DINLifeCycleId, string Discription, string CreatedBy)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    DES084 _DES084 = new DES084();
                    _DES084.DINId = DINId;
                    _DES084.DINRefDocumentId = DINRefDocumentId;
                    _DES084.DINLifeCycleId = DINLifeCycleId;
                    _DES084.Description = Discription;
                    _DES084.CreatedBy = CreatedBy;
                    _DES084.CreatedOn = DateTime.Now;
                    db.DES084.Add(_DES084);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public Int64 AddAcknowledgeNote(Int64? DINId, string AcknowledgeNote)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.DES081.Where(yy => yy.DINId == DINId).FirstOrDefault();
                    data.AcknowledgeNote = AcknowledgeNote;
                    db.Entry(data).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    AddDINHistory(data.DINId, null, null, AcknowledgeNote != "" ? "Acknowledged note : " + AcknowledgeNote : "Acknowledged note :-", CommonService.objClsLoginInfo.UserName);
                    return data.DINId;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public bool UpdateGird(DINRefDocumentModel DocDetail, out string errorMsg, out string infoMsg)
        {
            errorMsg = "";
            infoMsg = "";
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var getAllRevision = db.DES059.Where(xx => xx.DocumentNo == DocDetail.DocumentNo && xx.Status == ObjectStatus.Completed.ToString()).OrderByDescending(x => x.DocumentRevision).FirstOrDefault();


                    if (getAllRevision != null)
                    {
                        // var getAllRevision1 = db.DES059.Where(xx => xx.DocumentId == DocDetail.DocumentId).FirstOrDefault();
                        if ((DocDetail.DocumentId != getAllRevision.DocumentId))
                        {
                            var getLasxtRevisionStatus = db.DES059.Where(xx => xx.DocumentNo == DocDetail.DocumentNo).OrderByDescending(x => x.DocumentRevision).FirstOrDefault();
                            if (getAllRevision.DocumentRevision > 0 && getLasxtRevisionStatus.Status == ObjectStatus.Completed.ToString())
                            {
                                var data = db.DES082.Find(DocDetail.DINRefDocumentId);

                                var dataFromJEP = db.DES075.Where(x => x.DocumentId == getAllRevision.DocumentId && x.JEPId == data.DES075.JEPId).FirstOrDefault();
                                if (dataFromJEP != null && dataFromJEP.JEPDocumentDetailsId > 0)
                                {

                                    #region If Revision Found
                                    // var dcrData = db.DES090.Where(x => x.JEPDocumentDetailsId == dataFromJEP.JEPDocumentDetailsId && (x.DES089.Status == ObjectStatus.Created.ToString() || x.DES089.Status == ObjectStatus.InProcess.ToString())).Select(x => x.DES089.DCRNo).ToList(); ;
                                    //if (dcrData != null && dcrData.Count > 0)
                                    //{
                                    //     infoMsg = "Document is use in " + string.Join(",", dcrData.Distinct()) + " DCR user can not take latest revision in DIN";
                                    //  }
                                    //  else
                                    //  {

                                    Int64? OldJEPId = data.JEPDocumentDetailsId;
                                    data.JEPDocumentDetailsId = dataFromJEP.JEPDocumentDetailsId;
                                    data.EditedBy = CommonService.objClsLoginInfo.UserName;
                                    data.EditedOn = DateTime.Now;
                                    data.Status = DINStatus.Revised.ToString();
                                    db.Entry(data).State = System.Data.Entity.EntityState.Modified;
                                    db.SaveChanges();
                                    AddDINHistory(data.DES081.DINId, null, null, data.DES075.DocNo + " is revised", CommonService.objClsLoginInfo.UserName);

                                    var getDES099data = db.DES099.Where(x => x.DINId == data.DINId).ToList();
                                    foreach (var dES099Data in getDES099data)
                                    {

                                        var getGES113data = db.DES113.Where(x => x.DINAckID == dES099Data.DINAckID).ToList();
                                        foreach (var dES113data in getGES113data)
                                        {
                                            if (dES113data.JEPDocumentDetailsId == OldJEPId)
                                            {
                                                dES113data.JEPDocumentDetailsId = dataFromJEP.JEPDocumentDetailsId;
                                                db.Entry(dES113data).State = System.Data.Entity.EntityState.Modified;
                                                db.SaveChanges();
                                            }
                                        }

                                    }
                                    errorMsg = "Document latest revision updated successfully";
                                    #endregion
                                }
                                else if (getLasxtRevisionStatus != null && getLasxtRevisionStatus.DocumentRevision > 0)
                                {
                                    var getLastBeforeRevision = db.DES059.Where(xx => xx.DocumentNo == DocDetail.DocumentNo).ToList();

                                    if (getLastBeforeRevision != null)
                                    {
                                        var IsExistInJEP = db.DES075.Where(x => x.DocNo == DocDetail.DocumentNo).ToList();
                                        #region Add Data in JEP
                                        if (IsExistInJEP != null)
                                        {
                                            foreach (var item in getLastBeforeRevision)
                                            {
                                                bool getData = IsExistInJEP.Any(x => x.RevisionNo == item.DocumentRevision);
                                                if (!getData)
                                                {
                                                    var dataDES085 = db.DES085.Where(x => x.JEPDocumentDetailsId == data.JEPDocumentDetailsId).FirstOrDefault();
                                                    DES075 DES075 = new DES075();
                                                    DES075.JEPId = data.DES075.JEPId;
                                                    DES075.DocNo = item.DocumentNo;
                                                    DES075.DocDescription = item.DocumentTitle;
                                                    DES075.RevisionNo = item.DocumentRevision;
                                                    DES075.DocumentTypeId = item.DocumentTypeId;
                                                    DES075.CustomerDocRevision = item.ClientVersion;
                                                    DES075.CustomerDocNo = item.ClientDocumentNumber;
                                                    DES075.Department = item.Department;
                                                    if (dataDES085 != null)
                                                    {
                                                        if (dataDES085.ReceiptDate != null)
                                                        {
                                                            DES075.PlannedDate = dataDES085.ReceiptDate.Value.AddDays(15);
                                                        }
                                                        else
                                                        {
                                                            DES075.PlannedDate = DateTime.Now;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        DES075.PlannedDate = DateTime.Now;
                                                    }
                                                    DES075.Weightage = "0";
                                                    DES075.DocumentId = item.DocumentId;
                                                    DES075.CreatedBy = CommonService.objClsLoginInfo.UserName;
                                                    DES075.CreatedOn = DateTime.Now;
                                                    DES075.MilestoneDocument = item.MilestoneDocument;
                                                    //DES075.IsApproved = data.IsApproved;
                                                    db.DES075.Add(DES075);

                                                    db.SaveChanges();

                                                }
                                            }
                                        }
                                        #endregion


                                        var dataFromNewJEP = db.DES075.Where(x => x.DocumentId == getAllRevision.DocumentId && x.JEPId == data.DES075.JEPId).FirstOrDefault();


                                        if (dataFromNewJEP != null && dataFromNewJEP.JEPDocumentDetailsId > 0)
                                        {

                                            Int64? OldJEPId = data.JEPDocumentDetailsId;
                                            data.JEPDocumentDetailsId = dataFromNewJEP.JEPDocumentDetailsId;
                                            data.EditedBy = CommonService.objClsLoginInfo.UserName;
                                            data.EditedOn = DateTime.Now;
                                            data.Status = DINStatus.Revised.ToString();
                                            db.Entry(data).State = System.Data.Entity.EntityState.Modified;
                                            db.SaveChanges();
                                            AddDINHistory(data.DES081.DINId, null, null, data.DES075.DocNo + " is revised", CommonService.objClsLoginInfo.UserName);

                                            var getDES099data = db.DES099.Where(x => x.DINId == data.DINId).ToList();
                                            foreach (var dES099Data in getDES099data)
                                            {

                                                var getGES113data = db.DES113.Where(x => x.DINAckID == dES099Data.DINAckID).ToList();
                                                foreach (var dES113data in getGES113data)
                                                {
                                                    if (dES113data.JEPDocumentDetailsId == OldJEPId)
                                                    {
                                                        dES113data.JEPDocumentDetailsId = dataFromNewJEP.JEPDocumentDetailsId;
                                                        db.Entry(dES113data).State = System.Data.Entity.EntityState.Modified;
                                                        db.SaveChanges();
                                                    }
                                                }

                                            }
                                            errorMsg = "Document latest revision updated successfully";
                                        }


                                    }
                                }
                                else
                                {
                                    infoMsg = "Latest revision not found in " + data?.DES075?.DES074?.JEPNumber + " JEP.";
                                }
                            }
                            else if (getAllRevision.DocumentRevision >= 0 && getLasxtRevisionStatus.Status != ObjectStatus.Completed.ToString())
                            {
                                infoMsg = "Latest revision is in Process.";
                            }
                            else
                            {
                                infoMsg = "This is latest revision.";
                            }
                        }
                        else
                        {
                            var getLasxtRevisionStatus = db.DES059.Where(xx => xx.DocumentNo == DocDetail.DocumentNo).OrderByDescending(x => x.DocumentRevision).FirstOrDefault();
                            if (getAllRevision.DocumentRevision >= 0 && getLasxtRevisionStatus.Status != ObjectStatus.Completed.ToString())
                            {
                                infoMsg = "Latest revision is in Process.";
                            }
                            else
                            {
                                infoMsg = "This is latest revision.";
                            }
                        }
                    }
                    else
                    {
                        infoMsg = "This is latest revision.";
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                errorMsg = "Something went wrong while getting latest revision.";
                return false;
            }

        }

        public List<SP_DES_GET_DIN_POLICY_BYLEVEL_Result> GetDINUserLevel(string RoleGroup, string Roles, Int64? DINId, string Department)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_DIN_POLICY_BYLEVEL(RoleGroup, Roles, DINId, Department).ToList();
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                return null;
            }
        }

        public Int64? ReviseDINPolicy(Int64 DINId, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    var data = db.DES083.Where(yy => yy.DINId == DINId).ToList();
                    if (data != null)
                    {
                        foreach (var item in data)
                        {
                            item.IsCompleted = false;
                            item.EditedBy = CommonService.objClsLoginInfo.UserName;
                            item.EditedOn = DateTime.Now;
                            db.Entry(item).State = System.Data.Entity.EntityState.Modified;
                        }
                    }
                    db.SaveChanges();
                    return DINId;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }

        }

        public Int64? UpdateDINPolicy(Int64 DINLifeCycleId, Int64 DinId, out string errorMsg, out string infoMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    infoMsg = "";
                    var data = db.DES083.Where(yy => yy.DINLifeCycleId == DINLifeCycleId).FirstOrDefault();
                    var datalist = db.DES083.Where(yy => yy.DINId == data.DINId).ToList().Count;
                    var lists = db.DES083.Where(x => x.DINId == data.DINId).OrderBy(x => x.PolicyOrderID).ToList();
                    var list = lists[1];
                    if (data != null)
                    {
                        if (list.DINLifeCycleId == data.DINLifeCycleId)
                        {
                            if (list.PolicyOrderID > 1 && datalist != list.PolicyOrderID)
                            {
                                var DINData = db.DES081.Where(x => x.DINId == data.DINId).FirstOrDefault();
                                DINData.Status = ObjectStatus.Created.ToString();
                                DINData.EditedBy = CommonService.objClsLoginInfo.UserName;
                                DINData.EditedOn = DateTime.Now;
                                db.Entry(DINData).State = System.Data.Entity.EntityState.Modified;
                            }

                            data.CreatedBy = CommonService.objClsLoginInfo.UserName;
                            data.CreatedOn = DateTime.Now;
                            data.IsCompleted = true;
                            db.Entry(data).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                            infoMsg = "DIN " + data.PolicyStep + " successful";

                        }
                        else
                        {
                            errorMsg = "You Must Complete Before Steps !";
                            return 0;
                        }
                    }
                    return data.DINId;

                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }

        }

        public List<SP_DES_GET_DIN_HISTORY_Result> GetDINHistory(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, string DINNo, out int recordsTotal)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_DIN_HISTORY(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, DINNo).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    return data;

                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public Int64? AddAck(Int64? DINId, string Project)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    string errorMsg = "";
                    var _DES081 = db.DES081.Where(x => x.DINId == DINId).FirstOrDefault();
                    var _DES099Data = db.DES099.Where(xx => xx.DINId == DINId).ToList();

                    if (_DES099Data.Count() > 0)
                    {
                        #region CheckWether Department is Change or Not
                        List<DINModel> _DINModelList = new List<DINModel>();
                        foreach (var _DES099Datas in _DES099Data)
                        {
                            DINModel _DINModel = new DINModel();
                            _DINModel._lstPsNameSplit = new List<string>();
                            _DINModel.DINAckID = _DES099Datas.DINAckID;
                            _DINModel.DINId = _DES099Datas.DINId;
                            _DINModel.Department = _DES099Datas.Department;
                            var getPsNo = _DES099Datas.PsNoRelatedDept.Split(',').ToList();
                            foreach (var SinglePsNo in getPsNo)
                            {
                                _DINModel._lstPsNameSplit.Add(SinglePsNo);
                            }
                            _DINModelList.Add(_DINModel);
                        }


                        var GetGroupIdDes99 = _DES099Data.Select(x => x.DINGroupId).Distinct().FirstOrDefault();
                        if (_DES081.DistributionGroupId != GetGroupIdDes99)
                        {
                            // First Remove Old Data
                            var IsExistDINId = db.DES099.Where(xx => xx.DINId == DINId).ToList();

                            if (IsExistDINId != null && IsExistDINId.Count > 0)
                            {
                                foreach (var item in IsExistDINId)
                                {
                                    var _DES113data = db.DES113.Where(x => x.DINAckID == item.DINAckID).ToList();

                                    db.DES113.RemoveRange(_DES113data);
                                    db.SaveChanges();

                                    db.DES099.Remove(item);
                                    db.SaveChanges();
                                }
                            }
                            AddDataINDES099(_DES081.DistributionGroupId, _DES081.DINId);
                            //Add New Data
                        }
                        #endregion
                        else
                        {
                            //first check department are same 
                            List<DistDepartment> getDES034DeptAndPsNo = getDepartmentIdByGroupID(_DES081.DistributionGroupId);
                            foreach (var item in getDES034DeptAndPsNo)
                            {

                                var IsExistINDES099 = _DINModelList.Where(x => x.Department == item.DepartmentId && x._lstPsNameSplit.All(item._lstPsName.Contains)).ToList();

                                if (IsExistINDES099.Count() > 0)
                                {
                                    foreach (var items in IsExistINDES099)
                                    {
                                        var checIsChangeINPsNo = item._lstPsName.Except(items._lstPsNameSplit).ToList();
                                        var checIsChangeINPsNo1 = items._lstPsNameSplit.Except(item._lstPsName).ToList();
                                        if (checIsChangeINPsNo.Count > 0 || checIsChangeINPsNo1.Count > 0)
                                        {
                                            var getData = db.DES099.Where(x => x.DINAckID == items.DINAckID).FirstOrDefault();
                                            var getDataIfExistInDES113 = db.DES113.Where(x => x.DINAckID == items.DINAckID).ToList();
                                            if (getDataIfExistInDES113.Count() > 0)
                                            {
                                                db.DES113.RemoveRange(getDataIfExistInDES113);
                                                db.SaveChanges();
                                            }

                                            db.DES099.Remove(getData);
                                            db.SaveChanges();

                                            DES099 _DES099 = new DES099();
                                            _DES099.DINId = DINId;
                                            _DES099.DINGroupId = _DES081.DistributionGroupId;
                                            _DES099.Department = item.DepartmentId;
                                            var getPSNumberByDept = db.DES035.Where(xx => xx.DINGroupDeptId == item.DINGroupDeptId).ToList();
                                            string joinedPsNo = string.Join(",", getPSNumberByDept.Select(x => x.PSNumber).ToList());
                                            _DES099.PsNoRelatedDept = joinedPsNo;
                                            _DES099.IsCompleted = false;
                                            _DES099.CreatedBy = null;
                                            db.DES099.Add(_DES099);
                                            db.SaveChanges();
                                        }
                                    }
                                }
                                else
                                {
                                    try
                                    {
                                        DES099 _DES099 = new DES099();
                                        _DES099.DINId = DINId;
                                        _DES099.DINGroupId = _DES081.DistributionGroupId;
                                        _DES099.Department = item.DepartmentId;
                                        var getPSNumberByDept = db.DES035.Where(xx => xx.DINGroupDeptId == item.DINGroupDeptId).ToList();
                                        string joinedPsNo = string.Join(",", getPSNumberByDept.Select(x => x.PSNumber).ToList());
                                        _DES099.PsNoRelatedDept = joinedPsNo;
                                        _DES099.IsCompleted = false;
                                        _DES099.CreatedBy = null;
                                        db.DES099.Add(_DES099);
                                        db.SaveChanges();
                                    }
                                    catch (Exception ex)
                                    {
                                        CommonService.SendErrorToText(ex);
                                        throw;
                                    }

                                }

                            }

                            //AfterDone this if there are missmatch department then remove.

                            var getAgainDataOfDES99 = db.DES099.Where(x => x.DINId == DINId).ToList();

                            List<DINModel> _DINModelLst = new List<DINModel>();
                            foreach (var _DES099Datas in _DES099Data)
                            {
                                DINModel _DINModel = new DINModel();
                                _DINModel._lstPsNameSplit = new List<string>();
                                _DINModel.DINAckID = _DES099Datas.DINAckID;
                                _DINModel.DINId = _DES099Datas.DINId;
                                _DINModel.Department = _DES099Datas.Department;
                                var getPsNOList = _DES099Datas.PsNoRelatedDept.Split(',').ToList();
                                foreach (var getPsNo in getPsNOList)
                                {
                                    _DINModel._lstPsNameSplit.Add(getPsNo);
                                }
                                _DINModelLst.Add(_DINModel);
                            }

                            foreach (var againItem in _DINModelLst)
                            {
                                var IsExistINDES034 = getDES034DeptAndPsNo.Where(x => x.DepartmentId == againItem.Department && x._lstPsName.All(againItem._lstPsNameSplit.Contains)).ToList();

                                if (IsExistINDES034.Count() > 0)
                                {
                                    foreach (var itemIsExistINDES034 in IsExistINDES034)
                                    {

                                        var checIsChangeINPsNo = itemIsExistINDES034._lstPsName.Except(againItem._lstPsNameSplit).ToList();
                                        var checIsChangeINPsNo1 = againItem._lstPsNameSplit.ToList().Except(itemIsExistINDES034._lstPsName).ToList();
                                        if (checIsChangeINPsNo.Count > 0 || checIsChangeINPsNo1.Count > 0)
                                        {
                                            var getDataIfExistInDES113 = db.DES113.Where(x => x.DINAckID == againItem.DINAckID).ToList();
                                            if (getDataIfExistInDES113.Count() > 0)
                                            {
                                                db.DES113.RemoveRange(getDataIfExistInDES113);
                                                db.SaveChanges();
                                            }

                                            var getData = db.DES099.Where(x => x.DINAckID == againItem.DINAckID).FirstOrDefault();
                                            db.DES099.Remove(getData);
                                            db.SaveChanges();
                                        }
                                    }
                                }
                                else
                                {
                                    var getDataIfExistInDES113 = db.DES113.Where(x => x.DINAckID == againItem.DINAckID).ToList();
                                    if (getDataIfExistInDES113.Count() > 0)
                                    {
                                        db.DES113.RemoveRange(getDataIfExistInDES113);
                                        db.SaveChanges();
                                    }


                                    var getData = db.DES099.Where(x => x.DINAckID == againItem.DINAckID).FirstOrDefault();
                                    db.DES099.Remove(getData);
                                    db.SaveChanges();
                                }
                            }

                        }
                    }
                    else
                    {
                        AddDataINDES099(_DES081.DistributionGroupId, _DES081.DINId);
                    }


                    return DINId;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public Int64? AddStatus(DINModel prms, string Project, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    var DINData = db.DES081.Where(x => x.DINId == prms.DINId).FirstOrDefault();
                    var _DES082 = db.DES082.Where(x => x.DINId == prms.DINId).ToList();
                    DINData.Status = ObjectStatus.Completed.ToString();
                    DINData.EditedBy = CommonService.objClsLoginInfo.UserName;
                    DINData.EditedOn = DateTime.Now;
                    db.Entry(DINData).State = System.Data.Entity.EntityState.Modified;
                    AddDINHistory(DINData.DINId, null, null, DINData.DINNo + " (Issue No: " + DINData.IssueNo + ")  is Released", CommonService.objClsLoginInfo.UserName);
                    db.SaveChanges();
                    #region Notification 
                    var ExistDINId = db.DES099.Where(xx => xx.DINId == prms.DINId).ToList();
                    List<string> PsNo = new List<string>();
                    if (ExistDINId != null && ExistDINId.Count > 0)
                    {
                        foreach (var item in ExistDINId)
                        {
                            PsNo.Add(item.PsNoRelatedDept);
                            try
                            {
                                db.SP_DES_DIN_PUSH_NOTIFICATION(Project, CommonService.objClsLoginInfo.UserName, NotificationObject.Create.ToString(), item.DINAckID);
                            }
                            catch
                            {
                            }
                        }
                    }

                    #endregion

                    List<string> emailFromPSNo = new List<string>();
                    foreach (var emailPSNo in PsNo)
                    {
                        //string[] EmailToPsNo = emailPSNo.Split(',');
                        if (emailPSNo.Contains(','))
                        {
                            string[] EmailToPsNo = emailPSNo.Split(',');
                            foreach (var _EmailToPsNo in EmailToPsNo)
                            {
                                string getEmial = new clsManager().Manager.GetMailIdFromPsNo(_EmailToPsNo);
                                if (getEmial != "")
                                {
                                    emailFromPSNo.Add(getEmial);
                                }
                            }
                        }
                        else
                        {
                            string getEmial = new clsManager().Manager.GetMailIdFromPsNo(emailPSNo);
                            if (getEmial != "")
                            {
                                emailFromPSNo.Add(getEmial);
                            }

                        }

                    }
                    #region Email
                    //List<string> DocumentData = new List<string>();
                    //if(_DES082.Count)
                    //foreach (var getDocumentData in _DES082)
                    //{

                    //    var GetData = db.DES075.Where(x => x.JEPDocumentDetailsId == getDocumentData.JEPDocumentDetailsId).FirstOrDefault();

                    //    DocumentData.Add("<b>DocumentNo: </b> " + GetData.DocNo + " ,<b>Document Revision: <b>R" + GetData.RevisionNo + " ,<b>Document Description: <b>" + GetData.DocDescription + "<br>");
                    //}

                    string EMailFrom = "IEMQS@larsentoubro.com"; //new clsManager().Manager.GetMailIdFromPsNo(DINData.CreatedBy);
                    string EMailTo = new clsManager().Manager.GetMailIdFromPsNo(DINData.CreatedBy);

                    try
                    {
                        Hashtable _ht = new Hashtable();
                        EmailSend _objEmail = new EmailSend();

                        _ht["[FromEmail]"] = EMailFrom;
                        _ht["[ProjectNo]"] = DINData.ProjectNo;
                        //CURRENTLY WE FIX THE CONDITION FOR HZW | WE HAVE TO PUT CONDITION FOR LOCATION
                        var UserName = new clsManager().Manager.GetConfigValue("FCS_HZW_UserName").ToString();
                        var Password = new clsManager().Manager.GetConfigValue("FCS_HZW_Password").ToString();
                        var Domain = new clsManager().Manager.GetConfigValue("FCS_HZW_Domain").ToString();
                        IpLocation ipLocation = CommonService.GetUseIPConfig;
                        string[] ipArray = ipLocation.File_Path.Split('/');
                        var NetworkDomain = "\\\\" + ipArray[0];
                        var NetCre = new NetworkCredential(UserName, Password, Domain);
                        var GetURL = HttpContext.Current.Request.Url.OriginalString.Substring(0, HttpContext.Current.Request.Url.OriginalString.LastIndexOf('/'));
                        string qstring = CommonService.Encrypt("Id=" + DINData.DINId);
                        var CreatedDCRDetailURL = GetURL + "\\Detail?q=" + qstring;
                        var GetURLAuthority = HttpContext.Current.Request.Url.Authority;
                        MAIL001 objTemplateMaster = new MAIL001();
                        objTemplateMaster.Subject = "Issue No." + DINData.IssueNo + " of DIN no. (" + DINData.DINNo + ") for project no. " + DINData.ProjectNo + " is Released";
                        //objTemplateMaster.Body = "<br><p>Dear All,</p><br><p>" + "Business Object: <b>" + DINData.DINNo + " </b>/p><br><p>Project: " + DINData.ProjectNo + "</p><br><p>Issue No." + DINData.IssueNo + " of DIN no. (" + DINData.DINNo + ") for project no. " + DINData.ProjectNo + " is Released</p><br><p>Attached Document Details:</p><br><p>" + DocumentData + "<p><p>Link: <a  href=\"https://" + GetURLAuthority + "/DES/Dashboard/FindObject?Id=" + DINData.DINNo + "&Type=DIN\">" + DINData.DINNo + "</a></p><br><p>It requires your attention.</p>";
                        objTemplateMaster.Body = "<br><p>Dear All,</p><br><p>" + "Business Object: <b>" + DINData.DINNo + " </b>/p><br><p>Project: " + DINData.ProjectNo + "</p><br><p>Issue No." + DINData.IssueNo + " of DIN no. (" + DINData.DINNo + ") for project no. " + DINData.ProjectNo + " is Released</p><br><p>Link: <a  href=\"https://" + GetURLAuthority + "/DES/Dashboard/FindObject?Id=" + DINData.DINNo + "&Type=DIN\">" + DINData.DINNo + "</a></p><br><p>It requires your attention.</p>";
                        //objTemplateMaster.Description = "test";
                        objTemplateMaster.IsActive = true;
                        _objEmail.MailToAdd = String.Join("; ", emailFromPSNo.ToList()) ;
                        _objEmail.MailCc = EMailTo;
                        _objEmail.SendMail(_objEmail, _ht, objTemplateMaster);
                    }
                    catch (Exception ex)
                    {
                        //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    }

                    #endregion


                }
                return 1;
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }

        }

        public void AddDataINDES099(Int64? DistributionGroupId, Int64? DINId)
        {
            using (IEMQSDESEntities db = new IEMQSDESEntities())
            {
                List<DES099> _DES099List = new List<DES099>();
                var getDepartment = db.DES034.Where(yy => yy.DINGroupId == DistributionGroupId).ToList();
                if (getDepartment != null)
                    foreach (var item in getDepartment)
                    {
                        DES099 _DES099 = new DES099();
                        _DES099.DINId = DINId;
                        _DES099.DINGroupId = DistributionGroupId;
                        _DES099.Department = item.DeptId;
                        var getPSNumberByDept = db.DES035.Where(xx => xx.DINGroupDeptId == item.DINGroupDeptId).ToList();
                        string joinedPsNo = string.Join(",", getPSNumberByDept.Select(x => x.PSNumber).ToList());
                        _DES099.PsNoRelatedDept = joinedPsNo;
                        _DES099.IsCompleted = false;
                        _DES099.CreatedBy = null;
                        _DES099List.Add(_DES099);

                    }
                db.DES099.AddRange(_DES099List);
                db.SaveChanges();
            }
        }


        public Int64? IsAllowACk(string Department)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var datalist = db.DES099.Where(yy => yy.Department == Department).FirstOrDefault();
                    if (datalist != null)
                    {
                        return datalist.DINAckID;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }

        }

        public Int64? IsAllowACkTOUser(Int64? DINId, string UserDepartment, string PSNumber)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var datalist = db.DES099.Where(yy => yy.DINId == DINId && yy.Department == UserDepartment && yy.IsCompleted != true).ToList();
                    if (datalist != null)
                    {
                        foreach (var item in datalist)
                        {
                            if (item.PsNoRelatedDept != null)
                            {
                                if (item.PsNoRelatedDept.Count() > 0)
                                {
                                    foreach (var items in item.PsNoRelatedDept.Split(',').ToList())
                                    {
                                        if (items == PSNumber)
                                        {
                                            return item.DINAckID;
                                        }
                                    }
                                }
                            }
                        }
                        return 0;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public Int64? UpdateAckStatus(Int64? DINACKId, Int64? DINId, string Project, out string errorMsg)
        {
            errorMsg = "";
            using (IEMQSDESEntities db = new IEMQSDESEntities())
            {
                try
                {
                    var data = db.DES099.Where(yy => yy.DINAckID == DINACKId).FirstOrDefault();
                    data.IsCompleted = true;
                    data.CreatedBy = CommonService.objClsLoginInfo.UserName;
                    data.CreatedOn = DateTime.Now;
                    db.Entry(data).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();

                    #region Notification
                    try
                    {
                        db.SP_DES_DIN_PUSH_NOTIFICATION(Project, CommonService.objClsLoginInfo.UserName, NotificationObject.Acknowledge.ToString(), data.DINAckID);
                    }
                    catch
                    {
                    }

                    #endregion

                    //var getDataToAllowAck = db.DES099.Where(xx => xx.DINId == DINId).Select(xx => xx.IsCompleted).ToList();

                    //if (!getDataToAllowAck.Contains(false))
                    //{
                    //    var DINData = db.DES081.Where(x => x.DINId == data.DINId).FirstOrDefault();
                    //    DINData.Status = ObjectStatus.Completed.ToString();
                    //    DINData.EditedBy = CommonService.objClsLoginInfo.UserName;
                    //    DINData.EditedOn = DateTime.Now;
                    //    db.Entry(DINData).State = System.Data.Entity.EntityState.Modified;
                    //    db.SaveChanges();
                    //}

                    return data.DINAckID;
                }
                catch (Exception ex)
                {
                    CommonService.SendErrorToText(ex);
                    errorMsg = "Something went wrong";
                    return 0;
                }
            }
        }

        public List<SP_DES_GET_DIN_ACKNOWLEDGED_Result> ViewACKData(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, Int64? DINId, out int recordsTotal)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_DIN_ACKNOWLEDGED(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, DINId).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    return data;

                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<DepartmentName> getDataByGroupID(Int64? GroupId)
        {
            try
            {
                List<DepartmentName> xx = new List<DepartmentName>();
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    DepartmentName _objDN;
                    PsNameDeptWise obj;
                    var getDepartmentIdByGoupd = db.DES034.Where(x => x.DINGroupId == GroupId).Select(x => x.DINGroupDeptId).ToList();

                    List<PsNameDeptWise> lstDeptName;
                    foreach (var departmentId in getDepartmentIdByGoupd)
                    {
                        _objDN = new DepartmentName();

                        lstDeptName = new List<PsNameDeptWise>();
                        var PSNumberByDepartment = db.DES035.Where(y => y.DINGroupDeptId == departmentId).Select(y => y.PSNumber).ToList();
                        var DeparmentByDepartmentId = db.DES034.Where(x => x.DINGroupDeptId == departmentId).Select(x => x.DeptId).First();
                        var DepartmentName = new clsManager().getDepartmentDescription(DeparmentByDepartmentId);
                        _objDN.deptName = DepartmentName;

                        foreach (var item in PSNumberByDepartment)
                        {
                            obj = new PsNameDeptWise();
                            var PSName = new clsManager().GetUserNameFromPsNoForDIN(item);
                            if (PSName != null)
                            {
                                obj.PsName = PSName;
                            }
                            else
                            {
                                obj.PsName = "-";
                            }
                            lstDeptName.Add(obj);
                        }
                        _objDN._lstPsName = lstDeptName;
                        xx.Add(_objDN);
                    }

                }
                return xx.ToList();
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                return null;
            }
        }

        public string GetCustomerName(string CustomerCode)
        {
            try
            {
                string Name = string.Empty;
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    Name = db.SP_DES_GET_CUSTOMERNAMEBYCODE(CustomerCode).FirstOrDefault();
                }
                return Name;
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public Int64? DinRevision(Int64? DinId, Int64? BUId, string DinRevision, string Department)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data1 = db.DES081.Find(DinId);
                    var data2 = db.DES082.Where(x => x.DINId == DinId).ToList();
                    var data3 = db.DES099.Where(x => x.DINId == DinId).ToList();

                    #region Add DES0081 and DES083
                    DES081 _DES081 = new DES081();

                    if (data1 != null)
                    {
                        data1.IsLatestIssue = false;
                        db.Entry(data1).State = System.Data.Entity.EntityState.Modified;

                        _DES081.DINNo = data1.DINNo;
                        _DES081.ProjectNo = data1.ProjectNo;
                        _DES081.ContractNo = data1.ContractNo;
                        _DES081.ProjectDescription = data1.ProjectDescription;
                        _DES081.Customer = data1.Customer;
                        _DES081.ASMETag = data1.ASMETag;
                        _DES081.NuclearASME = data1.NuclearASME;
                        _DES081.IssueNo = data1.IssueNo + 1;
                        _DES081.DistributionGroupId = data1.DistributionGroupId;
                        _DES081.Description = data1.Description;
                        _DES081.CustomerForDINReport = data1.CustomerForDINReport;
                        _DES081.FolderId = data1.FolderId;
                        _DES081.RoleGroup = data1.RoleGroup.Trim();
                        _DES081.IsLatestIssue = true;
                        _DES081.Status = ObjectStatus.Created.ToString();
                        _DES081.Department = Department;
                        _DES081.IsDINCheckIn = true;
                        _DES081.DINCheckInBy = CommonService.objClsLoginInfo.UserName;
                        _DES081.DINCheckInOn = DateTime.Now;
                    }
                    #region getData From LifeCycle
                    var policy = GetPolicySteps(ObjectName.DIN.ToString(), BUId, null); // Get Polocy Steps
                    List<DES083> _DES083List = new List<DES083>();

                    if (policy != null)
                    {
                        foreach (var item in policy)
                        {
                            _DES081.PolicyId = item.PolicyId;
                            _DES083List.Add(new DES083
                            {
                                PolicyID = item.PolicyId,
                                PolicyStep = item.PolicyStep,
                                PolicyOrderID = item.PolicyOrderID,
                                IsCompleted = false,
                                DesignationLevels = item.DesignationLevels,
                                ColorCode = item.ColorCode
                            });
                        }
                    }
                    #endregion

                    _DES081.DES083 = _DES083List;
                    _DES081.CreatedBy = CommonService.objClsLoginInfo.UserName;
                    _DES081.CreatedOn = DateTime.Now;
                    db.DES081.Add(_DES081);
                    db.SaveChanges();
                    AddDINHistory(_DES081.DINId, null, null, "New issue of " + _DES081.DINNo + " (Issue No: " + _DES081.IssueNo + ") has been created", CommonService.objClsLoginInfo.UserName);
                    Int64? NewCreatedDinNo = _DES081.DINId;
                    #endregion

                    #region DES082
                    if (data2 != null)
                    {
                        foreach (var item in data2)
                        {
                            DES082 _DES082 = new DES082();
                            _DES082.DINId = NewCreatedDinNo;
                            _DES082.GeneralRemarks = item.GeneralRemarks;
                            //_DES082.DocumentId = item.DocumentId;
                            var data = db.DES075.Find(item.JEPDocumentDetailsId);
                            if (item.Status != DINStatus.NotReleased.ToString() && data != null && data.DocumentId > 0)
                            {
                                _DES082.Status = DINStatus.CarryForward.ToString();
                            }
                            else
                            {
                                _DES082.Status = DINStatus.Added.ToString();
                            }

                            _DES082.JEPDocumentDetailsId = item.JEPDocumentDetailsId;
                            _DES082.CreatedBy = CommonService.objClsLoginInfo.UserName;
                            _DES082.CreatedOn = DateTime.Now;
                            db.DES082.Add(_DES082);
                            db.SaveChanges();
                        }
                    }
                    #endregion

                    #region DES099
                    if (data3 != null)
                    {
                        foreach (var item in data3)
                        {
                            DES099 _DES099 = new DES099();
                            _DES099.DINId = NewCreatedDinNo;
                            _DES099.DINGroupId = item.DINGroupId;
                            _DES099.Department = item.Department;
                            _DES099.PsNoRelatedDept = item.PsNoRelatedDept;
                            _DES099.IsCompleted = false;
                            db.DES099.Add(_DES099);
                            db.SaveChanges();

                            //getOldDataDes113

                            var Olddes113Data = db.DES113.Where(x => x.DINAckID == item.DINAckID).ToList();
                            List<DES113> _ListDES133data = new List<DES113>();
                            foreach (var desData113 in Olddes113Data)
                            {
                                DES113 D113 = new DES113();
                                D113.DINAckID = _DES099.DINAckID;
                                D113.JEPDocumentDetailsId = desData113.JEPDocumentDetailsId;
                                D113.DOCCopys = desData113.DOCCopys;
                                D113.IsElectronicCopys = desData113.IsElectronicCopys;
                                _ListDES133data.Add(D113);
                            }
                            db.DES113.AddRange(_ListDES133data);
                            db.SaveChanges();
                        }
                    }
                    #endregion

                    string errorMsg = "";
                    UpdateDINLifeCycle(_DES081.DINId, out errorMsg);

                    return NewCreatedDinNo;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }

        }

        public List<SP_DES_GET_PENDING_DIN_Result> GetPendingDINList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, string PsNo, string Roles, out int recordsTotal, string Project)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_PENDING_DIN(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, Roles, PsNo, Project).ToList();
                    int? rt = 0;
                    data.ForEach(x =>
                    {
                        rt = x.TableCount;
                        x.QString = CommonService.Encrypt("Id=" + x.DINId);
                    });

                    recordsTotal = rt ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SelectListItem> GetContractProjectList(string projectNo)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var _DES051 = db.DES051.Where(w => w.Project == projectNo).FirstOrDefault();
                    var ContractNo = _DES051.Contract;
                    //return db.DES051.Where(x => x.Contract == ContractNo && x.Status != ObjectStatus.Created.ToString()).Select(x => new SelectListItem
                    //{
                    //    Text = x.Project,
                    //    Value = x.Project
                    //}).ToList();

                    return db.DES051.Where(x => x.Status != ObjectStatus.Created.ToString()).Select(x => new SelectListItem
                    {
                        Text = x.Project,
                        Value = x.Project
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public bool DINSaveAsDraft(Int64? DINId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var _DES081 = db.DES081.Find(DINId);
                    if (_DES081 != null)
                    {
                        _DES081.IsDINCheckIn = true;
                        _DES081.DINCheckInBy = CommonService.objClsLoginInfo.UserName;
                        _DES081.DINCheckInOn = DateTime.Now;
                        if (_DES081.IsDraft != true)
                        {
                            AddDINHistory(_DES081.DINId, null, null, _DES081.DINNo + "(Issue No: " + _DES081.IssueNo + ") is in created stage", CommonService.objClsLoginInfo.UserName);
                        }
                        _DES081.IsDraft = true;
                        db.SaveChanges();

                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public bool UpdateIsCheckInDIN(Int64? DINId, bool HasPsNo)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var getDinID = db.DES081.Where(x => x.DINId == DINId).FirstOrDefault();
                    if (HasPsNo != true)
                    {
                        getDinID.IsDINCheckIn = false;
                        getDinID.DINCheckInBy = null;
                    }
                    else
                    {
                        getDinID.IsDINCheckIn = true;
                        getDinID.DINCheckInBy = CommonService.objClsLoginInfo.UserName;
                        getDinID.DINCheckInOn = DateTime.Now;
                    }
                    db.Entry(getDinID).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }


        public List<DINDistributeDocument> GetDistributeDOCData(Int64? DINId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    List<DINDistributeDocument> Data = new List<DINDistributeDocument>();
                    var GetDataDES099 = db.DES099.Where(xx => xx.DINId == DINId).ToList();

                    // CheckData exist in DES113 
                    if (GetDataDES099.Count() > 0)
                    {
                        foreach (var data99 in GetDataDES099)
                        {

                            // CheckData exist in DES113 
                            List<DES113> _Des113List = new List<DES113>();
                            var GetDataDES113 = db.DES113.Where(xx => xx.DINAckID == data99.DINAckID).ToList();

                            if (GetDataDES113.Count() > 0)
                            {
                                // If exist.
                            }
                            else
                            {
                                var GetDataDES082 = db.DES082.Where(x => x.DINId == DINId).ToList();
                                if (GetDataDES082.Count > 0)
                                {
                                    foreach (var dataDES082 in GetDataDES082)
                                    {
                                        DES113 _DES113 = new DES113();
                                        _DES113.JEPDocumentDetailsId = dataDES082.JEPDocumentDetailsId;
                                        _DES113.DINAckID = data99.DINAckID;
                                        _Des113List.Add(_DES113);
                                    }
                                    db.DES113.AddRange(_Des113List);
                                    db.SaveChanges();
                                }
                                else
                                {
                                    return Data;
                                }
                            }
                        }
                    }


                    CheckDOCAddORRemove(DINId);

                    Data = GetDistributeDataByDINID(DINId);

                    return Data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public void CheckDOCAddORRemove(Int64? DINId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var GetDataDES082 = db.DES082.Where(xx => xx.DINId == DINId).Select(x => x.JEPDocumentDetailsId).ToList();
                    var GetDataDES099 = db.DES099.Where(xx => xx.DINId == DINId).ToList();

                    List<DES113> _Des113ListForRemove = new List<DES113>();
                    foreach (var data99 in GetDataDES099)
                    {
                        var ListJEPDOCDES0113 = db.DES113.Where(x => x.DINAckID == data99.DINAckID).ToList();
                        foreach (var DES113JepID in ListJEPDOCDES0113)
                        {
                            if (GetDataDES082.Contains(DES113JepID.JEPDocumentDetailsId))
                            {

                            }
                            else
                            {
                                var getDES0113DataToRemove = db.DES113.Where(x => x.DINDistributeDOCId == DES113JepID.DINDistributeDOCId).FirstOrDefault();
                                _Des113ListForRemove.Add(getDES0113DataToRemove);

                            }
                        }

                    }
                    db.DES113.RemoveRange(_Des113ListForRemove);
                    db.SaveChanges();

                    List<DES113> _Des113List = new List<DES113>();
                    foreach (var item in GetDataDES082)
                    {
                        foreach (var data99 in GetDataDES099)
                        {
                            var ListJEPDOCDES0113 = db.DES113.Where(x => x.DINAckID == data99.DINAckID).ToList();
                            if (ListJEPDOCDES0113.Select(x => x.JEPDocumentDetailsId).Contains(item))
                            {

                            }
                            else
                            {
                                DES113 _DES113 = new DES113();
                                _DES113.JEPDocumentDetailsId = item;
                                _DES113.DINAckID = data99.DINAckID;
                                _DES113.DOCCopys = 0;
                                _DES113.IsElectronicCopys = false;
                                _Des113List.Add(_DES113);
                            }
                        }

                    }
                    db.DES113.AddRange(_Des113List);
                    db.SaveChanges();
                }


            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }

        }
        public List<DistDepartment> getDepartmentIdByGroupID(Int64? GroupId)
        {
            try
            {
                List<DistDepartment> xx = new List<DistDepartment>();
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    DistDepartment _objDN;
                    var getDepartmentIdByGoupd = db.DES034.Where(x => x.DINGroupId == GroupId).Select(x => x.DINGroupDeptId).ToList();
                    foreach (var departmentId in getDepartmentIdByGoupd)
                    {
                        _objDN = new DistDepartment();
                        var DeparmentByDepartmentId = db.DES034.Where(x => x.DINGroupDeptId == departmentId).Select(x => x.DeptId).First();
                        var PSNumberByDepartment = db.DES035.Where(y => y.DINGroupDeptId == departmentId).Select(y => y.PSNumber).ToList();

                        var DepartmentName = new clsManager().getDepartmentDescription(DeparmentByDepartmentId);
                        _objDN.DINGroupDeptId = departmentId;
                        _objDN.Department = DepartmentName;
                        _objDN.DepartmentId = DeparmentByDepartmentId;
                        _objDN._lstPsName = PSNumberByDepartment;

                        xx.Add(_objDN);
                    }
                }
                return xx.ToList();
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                return null;
            }
        }


        public List<DINDistributeDocument> GetDistributeDataByDINID(Int64? DINId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    List<DINDistributeDocument> DINDisDocumentList = new List<DINDistributeDocument>();

                    var _DESData99 = db.DES099.Where(x => x.DINId == DINId).ToList();

                    foreach (var data99 in _DESData99)
                    {
                        var _DESData113 = db.DES113.Where(x => x.DINAckID == data99.DINAckID).ToList();
                        foreach (var data113 in _DESData113)
                        {
                            DINDistributeDocument _DINDistributeDocument = new DINDistributeDocument();
                            _DINDistributeDocument.DINDistributeDOCId = data113.DINDistributeDOCId;
                            _DINDistributeDocument.DINId = data99.DINId;
                            _DINDistributeDocument.JEPDocumentDetailsId = data113.JEPDocumentDetailsId;
                            _DINDistributeDocument.Department = data99.Department;
                            _DINDistributeDocument.DeptWiseDOCCopys = data113.DOCCopys ?? 0;
                            _DINDistributeDocument.IsElectronicCopys = data113.IsElectronicCopys ?? false;
                            _DINDistributeDocument.DepartmentName = new clsManager().getDepartmentDescription(data99.Department);
                            _DINDistributeDocument.GetFirstPsNumber = new clsManager().GetShortNameByPsno(data99.PsNoRelatedDept.Split(',').FirstOrDefault());
                            if (data113.JEPDocumentDetailsId != null)
                            {
                                var _DES075 = db.DES075.Where(x => x.JEPDocumentDetailsId == data113.JEPDocumentDetailsId).FirstOrDefault();
                                if (_DES075 != null)
                                {
                                    _DINDistributeDocument.DocuemntNo = _DES075.DocNo;
                                    _DINDistributeDocument.DocumentRev = _DES075.RevisionNo;
                                }
                            }
                            DINDisDocumentList.Add(_DINDistributeDocument);
                        }
                    }
                    return DINDisDocumentList;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }




        public bool AddDocDistribute(DINDistributeDocument model)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var DES113 = db.DES113.Where(x => x.DINDistributeDOCId == model.DINDistributeDOCId).FirstOrDefault();
                    if (DES113 != null)
                    {
                        //Kishan
                        DES113.DOCCopys = model.DeptWiseDOCCopys;
                        db.Entry(DES113).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public bool AddDocElectronicDistribute(DINDistributeDocument model)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var DES113 = db.DES113.Where(x => x.DINDistributeDOCId == model.DINDistributeDOCId).FirstOrDefault();
                    if (DES113 != null)
                    {
                        //Kishan
                        if (model.IsElectronicCopys == true)
                        {
                            DES113.DOCCopys = 0;
                        }
                        DES113.IsElectronicCopys = model.IsElectronicCopys;
                        db.Entry(DES113).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public bool IsContainDocument(Int64? DinID)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var DES082 = db.DES082.Where(x => x.DINId == DinID).ToList();
                    if (DES082.Count() > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public string DocumentData(string DocumentNo, Int64? DinID, string Project, string CurrentUser, string Roles)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    string URL = string.Empty;
                    var getDES081Data = db.DES081.Where(x => x.DINId == DinID).FirstOrDefault();

                    var GetDES059Data = db.DES059.Where(x => x.DocumentNo == DocumentNo).OrderByDescending(x => x.DocumentRevision).FirstOrDefault();
                    if (getDES081Data.Status != ObjectStatus.Completed.ToString())
                    {

                        if (GetDES059Data.Status == ObjectStatus.Completed.ToString())
                        {
                            string GetUserRoles = CommonService.GetUserRoleGroupByPsNo(Roles, CommonService.objClsLoginInfo.UserName);
                            List<string> UserRoleGroup = GetUserRoles.Split(',').ToList();
                            if (UserRoleGroup.Contains(GetDES059Data.RoleGroup.Trim()))
                            {
                                ObjectParameter returnId = new ObjectParameter("newDocumentId", typeof(long?));
                                db.SP_DES_COPY_DOCDATA(GetDES059Data.DocumentId, Project, CommonService.objClsLoginInfo.UserName, CommonService.GetUseIPConfig.Location, returnId);
                                Int64 newDocumentId;
                                Int64.TryParse(Convert.ToString(returnId.Value), out newDocumentId);
                                URL = "/DES/DOC/Edit/?q=" + CommonService.Encrypt("Id=" + newDocumentId);
                            }
                        }
                        else
                        {
                            if (GetDES059Data.IsDOCCheckIn == true && GetDES059Data.DOCCheckInBy == CurrentUser)
                            {
                                URL = "/DES/DOC/Edit/?q=" + CommonService.Encrypt("Id=" + GetDES059Data.DocumentId);
                            }
                            else
                            {
                                URL = "/DES/DOC/Detail/?q=" + CommonService.Encrypt("Id=" + GetDES059Data.DocumentId); ;
                            }
                        }
                    }

                    else
                    {
                        URL = "/DES/DOC/Detail/?q=" + CommonService.Encrypt("Id=" + GetDES059Data.DocumentId); ;
                    }
                    return URL;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }

        }
        public bool EditProjectDetails(string Project, string CustomerForDINReport)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.DES051.Where(yy => yy.Project == Project).FirstOrDefault();
                    data.CustomerForDINReport = CustomerForDINReport;
                    db.Entry(data).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
                return false;
            }
        }


        public string IsDocumentComplete(string DocumentNo, Int64? DinID, string Project, string CurrentUser, string Roles)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    string URL = string.Empty;
                    var getDES081Data = db.DES081.Where(x => x.DINId == DinID).FirstOrDefault();

                    var GetDES059Data = db.DES059.Where(x => x.DocumentNo == DocumentNo).OrderByDescending(x => x.DocumentRevision).FirstOrDefault();
                    if (getDES081Data.Status != ObjectStatus.Completed.ToString())
                    {

                        if (GetDES059Data.Status == ObjectStatus.Completed.ToString())
                        {
                            string GetUserRoles = CommonService.GetUserRoleGroupByPsNo(Roles, CommonService.objClsLoginInfo.UserName);
                            List<string> UserRoleGroup = GetUserRoles.Split(',').ToList();
                            if (UserRoleGroup.Contains(GetDES059Data.RoleGroup.Trim()))
                            {

                                URL = "True";
                            }
                            else
                            {
                                URL = "/DES/DOC/Detail/?q=" + CommonService.Encrypt("Id=" + GetDES059Data.DocumentId);
                            }
                        }
                        else
                        {
                            if (GetDES059Data.IsDOCCheckIn == true && GetDES059Data.DOCCheckInBy == CurrentUser)
                            {
                                URL = "/DES/DOC/Edit/?q=" + CommonService.Encrypt("Id=" + GetDES059Data.DocumentId);
                            }
                            else
                            {
                                URL = "/DES/DOC/Detail/?q=" + CommonService.Encrypt("Id=" + GetDES059Data.DocumentId);
                            }
                        }
                    }

                    else
                    {
                        URL = "/DES/DOC/Detail/?q=" + CommonService.Encrypt("Id=" + GetDES059Data.DocumentId); ;
                    }
                    return URL;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }

        }
    }
}