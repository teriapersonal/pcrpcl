﻿using IEMQS.DESCore.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IEMQS.DESServices
{
    public class FCSFileTransferManagementService : IFCSFileTransferManagementService
    {
        public List<DES060> GetNotTransferredFilelist(int location)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {                    
                    return db.DES060.Where(d => d.Central == false && d.FileLocation == location).ToList();                    
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                AddExceptionLogs(GetExceptionData(ex, "From GetNotTransferredFilelist method.", location));
                throw;
            }
        }

        private DES088 GetExceptionData(Exception ex, string sourcePoint, int location)
        {
            try
            {
                DES088 ObjdES088 = new DES088()
                {
                    ErrorMessage = ex.Message,
                    SourcePoint = sourcePoint,
                    CreatedOn = DateTime.Now,
                    IsDeleted = false,
                    Location = location
                };
                return ObjdES088;
            }
            catch (Exception exe)
            {
                CommonService.SendErrorToText(exe);
                throw;
            }
        }

        public void UpdateCentralFlag(long DocumentMappingId, int location)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {                   
                    var dES060s = db.DES060.Where(d => d.Central == false && d.DocumentMappingId == DocumentMappingId).FirstOrDefault();
                    if (dES060s !=null && dES060s.DocumentMappingId > 0)
                    {
                        dES060s.Central = true; 
                        dES060s.isIndexed = false;
                        dES060s.FileTransfredOn = DateTime.Now;
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                AddExceptionLogs(GetExceptionData(ex, "From UpdateCentralFlag method.", location));
                throw;
            }
        }

        public void UpdateFCSSchedularRunDate(DateTime dateTime, int location)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {                    
                    var dES104 = db.DES104.Where(d => d.ConfigName == "LastFCSSchedularRunDate").FirstOrDefault();
                    if (dES104 != null)
                    {
                        dES104.ConfigValues = dateTime.ToString();
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                AddExceptionLogs(GetExceptionData(ex, "From UpdateCentralFlag method.", location));
                throw;
            }
        }

        public bool AddTransferFileLog(DES078 _des078, int location)
        {
            try
            {
                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    DES078 ObjdES078 = new DES078()
                    {
                        TransfredFilename = _des078.TransfredFilename,
                        SourcePlace = _des078.SourcePlace,
                        DestinationPlace = _des078.DestinationPlace,
                        DocumentMappingId = _des078.DocumentMappingId,
                        isDeleted = _des078.isDeleted,
                        Createdon = _des078.Createdon,
                        CreatedBy = _des078.CreatedBy,
                        EditedOn = _des078.EditedOn,
                        EditedBy = _des078.EditedBy
                    };

                    dbContext.DES078.Add(ObjdES078);
                    dbContext.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                AddExceptionLogs(GetExceptionData(ex, "From AddTransferFileLog method.", location));
                throw;
            }
        }

        public void AddExceptionLogs(DES088 _dES088)
        {
            try
            {
                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    DES088 ObjdES088 = new DES088()
                    {
                        ErrorMessage = _dES088.ErrorMessage,
                        SourcePoint = _dES088.SourcePoint,
                        CreatedOn = _dES088.CreatedOn,
                        IsDeleted = _dES088.IsDeleted,
                        Location = _dES088.Location
                    };
                    dbContext.DES088.Add(ObjdES088);
                    dbContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                AddExceptionLogs(GetExceptionData(ex, "From AddExceptionLogs method.", 1));
                throw;
            }
        }

        public List<SP_DES_GET_CUSTOMER_FEEDBACK_FILE_FOR_TRANSFER_Result> GetCustomerFeedbackFileForTransfer(int Location)
        {
            try
            {
                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {                   
                    return dbContext.SP_DES_GET_CUSTOMER_FEEDBACK_FILE_FOR_TRANSFER(Location).ToList();                    
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public void AddLogForTransfrredFiles(long OjbectId, int location, string Type)
        {
            try
            {
                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    switch (Type)
                    {
                        case "DOC":
                            DES106 dESdoc = new DES106()
                            {
                                DocumentMappingId = OjbectId,
                                Central = true,
                                IsIndex = false,
                                CreatedBy = 1,
                                CreatedOn = DateTime.Now,
                                EditedBy = 1,
                                EditedOn = DateTime.Now,
                                isDeleted = false
                            };

                            dbContext.DES106.Add(dESdoc);
                            dbContext.SaveChanges();
                            return;
                        case "JEP-CustomerFeedback":
                            DES106 dES106 = new DES106()
                            {
                                JEPCustFeedbackDocumentId = OjbectId,
                                Central = true,
                                IsIndex = false,
                                CreatedBy = 1,
                                CreatedOn = DateTime.Now,
                                EditedBy = 1,
                                EditedOn = DateTime.Now,
                                isDeleted = false
                            };

                            dbContext.DES106.Add(dES106);
                            dbContext.SaveChanges();
                            return;
                        case "DCR":
                            DES106 dEsDCR = new DES106()
                            {
                                DCRSupportedDocId = OjbectId,
                                Central = true,
                                IsIndex = false,
                                CreatedBy = 1,
                                CreatedOn = DateTime.Now,
                                EditedBy = 1,
                                EditedOn = DateTime.Now,
                                isDeleted = false
                            };

                            dbContext.DES106.Add(dEsDCR);
                            dbContext.SaveChanges();
                            return;
                        case "ROC":
                            DES106 dEsROC = new DES106()
                            {
                                ROCCommentedDocId = OjbectId,
                                Central = true,
                                IsIndex = false,
                                CreatedBy = 1,
                                CreatedOn = DateTime.Now,
                                EditedBy = 1,
                                EditedOn = DateTime.Now,
                                isDeleted = false
                            };

                            dbContext.DES106.Add(dEsROC);
                            dbContext.SaveChanges();
                            return;
                        case "ROC-Attached":
                            DES106 dEsROCA = new DES106()
                            {
                                ROCCommentsAttachId = OjbectId,
                                Central = true,
                                IsIndex = false,
                                CreatedBy = 1,
                                CreatedOn = DateTime.Now,
                                EditedBy = 1,
                                EditedOn = DateTime.Now,
                                isDeleted = false
                            };

                            dbContext.DES106.Add(dEsROCA);
                            dbContext.SaveChanges();
                            return;
                    }

                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                AddExceptionLogs(GetExceptionData(ex, "From AddLogForTransfrredFiles method.", location));
                throw;
            }
        }

        public List<SP_DES_GET_DCR_OBJECT_FILE_FOR_TRANSFER_Result> GetDCRFileForTransfer(int Location)
        {
            try
            {
                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    return dbContext.SP_DES_GET_DCR_OBJECT_FILE_FOR_TRANSFER(Location).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<DES086> GetCustomerFeedbackRecordofHazira(int location)
        {
            try
            {
                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {                    
                    List<long?> listofJEPFeebackIds = new List<long?>();
                    listofJEPFeebackIds = dbContext.DES106.Where(x => x.isDeleted == false && x.JEPCustFeedbackDocumentId != null).Select(d => d.JEPCustFeedbackDocumentId).ToList();

                    return dbContext.DES086.Where(x => x.FileLocation == location && !listofJEPFeebackIds.Contains(x.JEPCustFeedbackDocumentId)).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<DES091> GetDCRRecordofHazira(int location)
        {
            try
            {
                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    List<long?> listofJEPFeebackIds = new List<long?>();
                    listofJEPFeebackIds = dbContext.DES106.Where(x => x.isDeleted == false && x.DCRSupportedDocId != null).Select(d => d.DCRSupportedDocId).ToList();

                    return dbContext.DES091.Where(x => x.FileLocation == location && !listofJEPFeebackIds.Contains(x.DCRSupportedDocId)).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_ROC_COMMENTED_FILE_FOR_TRANSFER_Result> GetROCFileForTransfer(int Location)
        {
            try
            {
                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                { 
                    return dbContext.SP_DES_GET_ROC_COMMENTED_FILE_FOR_TRANSFER(Location).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_ROC_COMMENTED_ATTACHED_FILE_FOR_TRANSFER_Result> GetROC_ATTACHED_FileForTransfer(int Location)
        {
            try
            {
                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                { 
                    return dbContext.SP_DES_GET_ROC_COMMENTED_ATTACHED_FILE_FOR_TRANSFER(Location).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<DES070> GetROCFileRecordofHazira(int location)
        {
            try
            {
                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    List<long?> listofJEPFeebackIds = new List<long?>();
                    listofJEPFeebackIds = dbContext.DES106.Where(x => x.isDeleted == false && x.ROCCommentedDocId != null).Select(d => d.ROCCommentedDocId).ToList();

                    return dbContext.DES070.Where(x => x.FileLocation == location && !listofJEPFeebackIds.Contains(x.ROCCommentedDocId)).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<DES072> GetROCCommentedFileRecordofHazira(int location)
        {
            try
            {
                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    List<long?> listofJEPFeebackIds = new List<long?>();
                    listofJEPFeebackIds = dbContext.DES106.Where(x => x.isDeleted == false && x.ROCCommentsAttachId != null).Select(d => d.ROCCommentsAttachId).ToList();

                    return dbContext.DES072.Where(x => x.FileLocation == location && !listofJEPFeebackIds.Contains(x.ROCCommentsAttachId)).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_PLM_DOC_OBJECT_FILE_FOR_TRANSFER_TO_IEMQS_FCS_Result> GetPLMDOCObjectFilesforTransfer(int Location)
        {
            try
            {
                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    return dbContext.SP_DES_GET_PLM_DOC_OBJECT_FILE_FOR_TRANSFER_TO_IEMQS_FCS(Location).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public void UpdateCentralANDMaindocumentPathFORPLMFiles(string MaindocumentPath, long DocumentMappingId)
        {
            try
            {
                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                   var dES060 = dbContext.DES060.Where(d => d.DocumentMappingId == DocumentMappingId).FirstOrDefault();
                    if (dES060 != null)
                    {
                        dES060.Central = false;
                        dES060.MainDocumentPath = MaindocumentPath;
                    }
                    dbContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }
        public void UpdateDCRDocumentPathFORPLMFiles(string DocumentPath, long DCRSupportedDocId)
        {
            try
            {
                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    var _objDES091 = dbContext.DES091.Where(d => d.DCRSupportedDocId == DCRSupportedDocId).FirstOrDefault();
                    if (_objDES091 != null)
                    {
                        _objDES091.DocumentPath = DocumentPath;
                    }
                    dbContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_DOC_OBJECT_FILE_FOR_TRANSFER_Result> GetDOCObjectFileForTransfer(int Location)
        {
            try
            {
                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    return dbContext.SP_DES_GET_DOC_OBJECT_FILE_FOR_TRANSFER(Location).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }
    }
}
