﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using System;
using System.Collections.Generic;

namespace IEMQS.DESServices
{
    public interface ITransmittalService
    {
        Int64 AddEdit(AgencyDocumentModel model, out string errorMsg);
        List<AgencyModel> AgnecywithEmailTemplate(string Project);

        List<SP_DES_GET_TRANSMITTAL_AGENCY_DOC_Result> GetAgencyDocList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, Int64? AgencyID, out int recordsTotal, string Project);

        List<SP_DES_GET_TRANSMITTAL_AGENCY_Result> GetTransmittalAgencyList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, string Project, long? FolderId,string Roles);
        List<SP_DES_GET_TRANSMITTAL_JEP_DOC_Result> GetJEPDocList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, string Project, Int64? AgencyID, out int recordsTotal);
        List<SP_DES_GET_TRANSMITTAL_GLOBAL_JEP_DOC_Result> GetGlobalJEPDocList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, string Project, string DocumentNo, string CurrentProject, Int64? AgencyID);
        AgencyDocumentModel GetAgencyDocumentById(long id);
        List<AgencyDocModel> GetTransDocumentById(long id);
        Int64 DeleteAgencyDoc(Int64 Id, out string errorMsg);

        Int64 DeleteAgencyWiseAllDoc(Int64 Id, out string errorMsg);
        Int64 DeleteTransmittalWiseDoc(Int64 Id, out string errorMsg);
        bool UpdateAgencyDoc(AgencyDocumentModel model, out string errorMsg);
        bool UpdateEmailFlag(AgencyDocumentModel model);
        bool CheckAgencyAlreadyExist(long? agencyId, string project, Int64? AgencyDocId);
        AgencyDocModel AgencyDetail(Int64 AgencyID);
        List<Int64> GetTransmittalDocList(Int64 TransmittalID);
        AgencyDocModel TransmittalEditDetail(Int64 TransmittalID);

        List<SP_DES_GET_TRANSMITTAL_AGENCY__WISE_DOC_Result> BindAgencywiseDocs(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, Int64? AgencyID, Int64? TransmittalId, out int recordsTotal, string Project);
        Int64 AddTransmittal(TransmittalModel model, out List<string> DocPathList, out string newGeneratedTransmittalNo, out string errorMsg);

        List<SP_DES_GET_GENERATE_TRANSMITTAL_Result> GetGenerateTransmittalList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, string Project, long? FolderId,string Roles);

        List<SP_DES_GET_AGENCY_EMAIL_TEMPLATE_Result> GetEmailTemplateByAgency(Int64? TransmittalId);

        Int64 AddEmailHistory(TransmittalEmailModel model);
        List<SP_DES_GET_TRANSMITTAL_EMAIL_HISTORY_Result> BindEmailHistory(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, Int64? TransmittalId, out int recordsTotal);

        List<SP_DES_GET_TRANSMITTAL_Detail_Result> BindTransmittlDetail(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, Int64? TransmittalId, out int recordsTotal);

        List<string> GetDownloadFiles(Int64? TrnamittalId);

        List<Tuple<Int64, double>> GetWeightage(Int64? AgencyId,string Project);

        bool CheckWeight(Int64? AgencyID,string project, out string errorMSG);
    }
}
