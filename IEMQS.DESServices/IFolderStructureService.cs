﻿using IEMQS.DESCore;
using System;
using System.Collections.Generic;

namespace IEMQS.DESServices
{
    public interface IFolderStructureService
    {
        List<FolderStructureModel> GetAllFolderStructure();
        List<FolderStructureModel> GetAllFolderStructureByBUId(Int64 Id);
        Int64 AddEdit(FolderStructureModel model, out string errorMsg);

        FolderStructureModel GetCategoryActionByBUIdParentId(Int64 BUId, Int64 ParentFolderId, bool EditModeOnFlag);
        bool IsAlreadyExist(string FolderName, long? ParentFolderId, long? BUId);

        Int64 DeleteFolder(Int64 Id, out string errorMsg);
        List<String> GetRoleGroup();
    }
}
