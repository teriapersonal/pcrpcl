﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using System;
using System.Collections.Generic;

namespace IEMQS.DESServices
{
    public interface IProjectService
    {
        Int64 AddEdit(FavoriteProjectModel model, out string errorMsg);
        List<FavoriteProjectModel> GetAllFavoriteProject(string PsNo);
        List<SP_DES_GET_LANDING_PAGE_PROJECT_Result> GetFavoriteList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal);

        List<SP_DES_GET_PROJECTLIST_Result> GetProjectList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, string PSNo, out int recordsTotal);
        ProjectModel GetProjectByProjectNo(string ProjectNo);
        ProjectModel EditProject(ProjectModel model);

        bool UpdateProjectStatus(string Project, string Status, out string errorMsg);

        ProjectFolderModel ProjectFolderList(string ProjectNo, string PSNo, string Department);

        List<SP_DES_GET_PROJECTDETAILS_ACCESS_Result> GetProjectAccessList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, string ProjectNo, string ContractNo, string BUCode);

        FolderListModel GetCategoryActionByProjectNoParentId(string ProjectNo, Int64? ParentProjectFolderId, bool EditModeOnFlag);
        bool IsAlreadyExist(string FolderName, long? ParentProjectFolderId, string ProjectNo);
        Int64 DeleteFolder(Int64 Id, out string errorMsg);

        long AddEditProjectFolder(FolderListModel model, out string errorMsg);
        List<string> ProjectSearch(string Status, string Key);

        ProjectFolderModel GetFolder(Int64 Id, Int64? BUCode,List<string> ListRoles,string Project);

        Int64 AddProjectUserFolderAuthorize(ProjectUserFolderAuthorizeModel model);

        List<RecentProjectModel> GetAllRecentProject(string PsNo);
        Int64 AddRecentProject(string PsNo, string project);

        ProjectModel GetJEPProjectByProjectNo(string ProjectNo);

        string EditJEPProject(ProjectModel model);

        List<DINGroupModel> GetAllDesignGroup();

        List<SP_DES_GET_ALL_OBJECTS_BY_FOLDERID_PROJECT_Result> GetAllObjectsList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, string project, string searchObjectType, string searchObjectStatus, string searchObjectName, long? folderId,string Roles, out int recordsTotal);

        FindObjectModel FindObject(string Id, String Type);
        bool IsAlreadyExistDrawingSeriesNo(string DrawingSeriesNo, string Project);

        string GetDesignCodeByProjectNo(string Project, string category, string bUCode, string location);

        string GetObjectNameByObjectId(Int64 ObjectId);
    }
}
