﻿using IEMQS.DESCore.Data;
using System;
using System.Collections.Generic;

namespace IEMQS.DESServices
{
    public interface IProjectAuthorizationService
    {
        List<SP_DES_GET_ProjectAuthorization_Result> GetProjectAuthorizationList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, string Project, string Department,string UserRoles);

        Int64 Approve(Int64 Id , string UserName);
        Int64 Reject(Int64 Id);



    }
}
