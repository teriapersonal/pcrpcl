﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IEMQS.DESServices
{
    public class BUService : IBUService
    {
        public Int64 AddEdit(BUModel model, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    #region check Validation
                    if (model.BUId > 0)
                    {
                        var checkchild = db.DES001.Find(model.BUId);
                        if (checkchild != null)
                        {
                            if (checkchild.DES003 != null && checkchild.DES003.Count > 0)
                            {
                                errorMsg = "BU-PBU is already in used. you cannot edit";
                                return 0;
                            }
                            if (checkchild.DES008 != null && checkchild.DES008.Count > 0)
                            {
                                errorMsg = "BU-PBU is already in used. you cannot edit";
                                return 0;
                            }
                        }

                    }
                    var checkdublicate = db.DES001.Where(x => x.BU == model.BU && x.PBU == model.PBU && x.BUId != model.BUId).FirstOrDefault();
                    if (checkdublicate != null)
                    {
                        errorMsg = "BU-PBU is already exist";
                        return 0;
                    }

                    var checkdublicate1 = db.DES001.Where(x => (x.BU == model.BU && x.BUCode != model.BUCode) || (x.BUCode == model.BUCode && x.BU != model.BU)).FirstOrDefault();
                    if (checkdublicate1 != null)
                    {
                        errorMsg = "BU code not match with BU";
                        return 0;
                    }

                    var checkdublicate2 = db.DES001.Where(x => (x.PBU == model.PBU && x.PBUCode != model.PBUCode) || (x.PBUCode == model.PBUCode && x.PBU != model.PBU)).FirstOrDefault();
                    if (checkdublicate2 != null)
                    {
                        errorMsg = "PBU code not match with PBU";
                        return 0;
                    }

                    var checkdublicateCode = db.DES001.Where(x => (x.PBU==model.PBU && x.BUId != model.BUId) && ((x.BU == model.BU && x.BUCode == model.BUCode && x.BU != model.BU) || (x.BU != model.BU && x.BUCode == model.BUCode) || (x.BU == model.BU && x.BUCode != model.BUCode))).FirstOrDefault();
                    if (checkdublicateCode != null)
                    {
                        errorMsg = "BU code is already in used";
                        return 0;
                    }
                    var checkdublicatepbuCode = db.DES001.Where(x => (x.BU == model.BU && x.BUId != model.BUId) && ((x.PBU == model.PBU && x.PBUCode == model.PBUCode && x.PBU != model.PBU) || (x.PBU != model.PBU && x.PBUCode == model.PBUCode) || (x.PBU == model.PBU && x.PBUCode != model.PBUCode))).FirstOrDefault();
                    if (checkdublicatepbuCode != null)
                    {
                        errorMsg = "PBU code is already in used";
                        return 0;
                    }
                    
                    #endregion
                    var temp = db.DES001.Find(model.BUId);
                    DES001 _DES001 = temp == null ? new DES001() : temp;

                    _DES001.BU = model.BU;
                    _DES001.BUCode = model.BUCode;
                    _DES001.PBU = model.PBU;
                    _DES001.PBUCode = model.PBUCode;

                    if (temp == null)
                    {
                        _DES001.CreatedBy = CommonService.objClsLoginInfo.UserName;
                        _DES001.CreatedOn = DateTime.Now;
                        db.DES001.Add(_DES001);
                    }
                    else
                    {
                        _DES001.EditedBy = CommonService.objClsLoginInfo.UserName;
                        _DES001.EditedOn = DateTime.Now;
                    }
                    db.SaveChanges();
                    return _DES001.BUId;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<BUModel> GetAllBU()
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES001.Select(x => new BUModel
                    {
                        BUId = x.BUId,
                        BU = x.BU + "-" + x.PBU,
                        PBU = x.PBU,
                        BUCode = x.BUCode,
                        PBUCode = x.PBUCode
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public BUModel GetBUbyId(Int64 Id)
        {
            throw new NotImplementedException();
        }

        public List<SP_DES_GET_BU_Result> GetBUPBUList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_BU(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    return data;

                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public Int64 DeleteBU(Int64 Id, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    var check = db.DES001.Find(Id);
                    if (check != null)
                    {
                        if (check.DES003 != null && check.DES003.Count > 0)
                        {
                            errorMsg = "BU-PBU is already in used. you cannot delete";
                            return 0;
                        }
                        if (check.DES008 != null && check.DES008.Count > 0)
                        {
                            errorMsg = "BU-PBU is already in used. you cannot delete";
                            return 0;
                        }
                        db.DES001.Remove(check);
                        db.SaveChanges();

                        return check.BUId;
                    }
                    return 0;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }
    }
}
