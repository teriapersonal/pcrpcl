﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IEMQS.DESServices
{
    public class PartSheetService : IPartSheetService
    {
        public long AddEdit(PartSheetModel model, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";

                    var temp = db.DES064.Find(model.ItemSheetId);
                    DES064 _DES064 = temp == null ? new DES064() : temp;

                    _DES064.Project = model.Project;
                    _DES064.FolderId = model.FolderId;
                    _DES064.SheetName = model.SheetName;
                    _DES064.Description = model.Description;

                    if (temp == null)
                    {
                        _DES064.IsSheetCheckIn = true;
                        _DES064.SheetCheckInBy = CommonService.objClsLoginInfo.UserName;
                        _DES064.SheetCheckInOn = DateTime.Now;
                        _DES064.CreatedBy = CommonService.objClsLoginInfo.UserName;
                        _DES064.CreatedOn = DateTime.Now;
                        db.DES064.Add(_DES064);
                    }
                    else
                    {
                        if (temp.IsSheetCheckIn != true)
                        {
                            _DES064.IsSheetCheckIn = true;
                            _DES064.SheetCheckInBy = CommonService.objClsLoginInfo.UserName;
                            _DES064.SheetCheckInOn = DateTime.Now;
                        }
                        _DES064.EditedBy = CommonService.objClsLoginInfo.UserName;
                        _DES064.EditedOn = DateTime.Now;
                    }
                    db.SaveChanges();
                    return _DES064.ItemSheetId;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public bool CheckSheetNameAlreadyExist(string sheetName, string project, long? itemSheetId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    sheetName = sheetName.Trim();
                    var data = db.DES064.Where(x => x.SheetName == sheetName && x.Project == project && (x.ItemSheetId != itemSheetId || itemSheetId == null)).ToList();
                    if (data != null && data.Count > 0)
                    {
                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                return false;
            }
        }

        public long DeletePartSheet(long Id)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    DES064 _DES064 = db.DES064.Find(Id);
                    if (_DES064 != null)
                    {
                        db.DES065.RemoveRange(_DES064.DES065);
                        db.DES064.Remove(_DES064);
                        db.SaveChanges();
                        return _DES064.ItemSheetId;
                    }
                    return 0;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public PartSheetModel GetPartSheetById(long? Id)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES064.Where(x => x.ItemSheetId == Id).Select(x => new PartSheetModel
                    {
                        ItemSheetId = x.ItemSheetId,
                        Project = x.Project,
                        SheetName = x.SheetName,
                        Description = x.Description,
                        CreatedBy = x.CreatedBy,
                        CreatedOn = x.CreatedOn,
                        EditedBy = x.EditedBy,
                        EditedOn = x.EditedOn,
                        Status = x.Status
                    }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_PART_SHEET_Result> GetPartSheetList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, string Project, long FolderId, out int recordsTotal)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_PART_SHEET(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, Project, FolderId).ToList();
                    recordsTotal = 0;
                    int? rt = 0;
                    data.ForEach(x =>
                    {
                        rt = x.TableCount;
                        x.QString = CommonService.Encrypt("Id=" + x.ItemSheetId);
                    });
                    recordsTotal = rt ?? 0;
                    return data;

                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public bool UpdateIsCheckInSheet(Int64? ItemSheedId, bool UpdateIsCheckInSheet)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var getSheetID = db.DES064.Where(x => x.ItemSheetId == ItemSheedId).FirstOrDefault();
                    if (UpdateIsCheckInSheet != true)
                    {
                        getSheetID.IsSheetCheckIn = true;
                        getSheetID.SheetCheckInBy = CommonService.objClsLoginInfo.UserName;
                        getSheetID.SheetCheckInOn = DateTime.Now;
                    }
                    else
                    {
                        getSheetID.IsSheetCheckIn = false;
                        getSheetID.SheetCheckInBy = null;
                    }
                    db.Entry(getSheetID).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

    }
}
