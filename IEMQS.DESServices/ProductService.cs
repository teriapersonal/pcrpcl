﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IEMQS.DESServices
{
    public class ProductService : IProductService
    {
        public Int64 AddEdit(ProductModel model, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";

                    #region Check Validation

                    if (model.ProductId > 0)
                    {
                        var checkchild = db.DES029.Find(model.ProductId);
                        if (checkchild != null)
                        {                            
                            if (checkchild.DES030 != null && checkchild.DES030.Count > 0)
                            {
                                errorMsg = "Product is already in used. you cannot edit";
                                return 0;
                            }
                        }
                    }

                    var checkdublicate = db.DES029.Where(x => x.Product == model.Product && x.BUId == model.BUId && x.Description == model.Description && x.ProductId != model.ProductId).FirstOrDefault();
                    if (checkdublicate != null)
                    {
                        errorMsg = "Product is already exist";
                        return 0;
                    }

                    #endregion

                    var temp = db.DES029.Find(model.ProductId);
                    DES029 _DES029 = temp == null ? new DES029() : temp;

                    _DES029.Product = model.Product;
                    _DES029.Description = model.Description;
                    _DES029.BUId = model.BUId;

                    if (temp == null)
                    {
                        _DES029.CreatedBy = CommonService.objClsLoginInfo.UserName;
                        _DES029.CreatedOn = DateTime.Now;                        
                        db.DES029.Add(_DES029);
                    }
                    else
                    {
                        _DES029.EditedBy = CommonService.objClsLoginInfo.UserName;
                        _DES029.EditedOn = DateTime.Now;
                    }
                    db.SaveChanges();
                    return _DES029.ProductId;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<ProductModel> GetAllProduct()
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES029.OrderBy(x => x.ProductId).Select(x => new ProductModel
                    {
                        ProductId = x.ProductId,
                        BUId = x.BUId,
                        Product = x.Product + " - " + x.Description,
                        Description = x.Description
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public ProductModel GetProductbyId(long Id)
        {
            throw new NotImplementedException();
        }

        public List<SP_DES_GET_PRODUCT_Result> GetProductList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_PRODUCT(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public Int64 DeleteProduct(Int64 Id, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    var check = db.DES029.Find(Id);
                    if (check != null)
                    {
                        if (check.DES030 != null && check.DES030.Count > 0)
                        {
                            errorMsg = "Product is already in used. you cannot delete";
                            return 0;
                        }

                        db.DES029.Remove(check);
                        db.SaveChanges();
                        return check.ProductId;
                    }
                    return 0;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }
    }
}