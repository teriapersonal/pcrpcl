﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IEMQS.DESServices
{
    public class AgencyEmailService : IAgencyEmailService
    {
        public Int64 AddEdit(AgencyEmailModel model)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {

                    var temp = db.DES068.Find(model.TransmittalEmailId);
                    DES068 _DES068 = temp == null ? new DES068() : temp;

                    _DES068.Project = model.Project;
                    _DES068.AgencyId = model.AgencyId;
                    _DES068.AgencyTo = model.AgencyTo;
                    _DES068.AgencyCC = model.AgencyCC;
                    _DES068.LandTTo = model.LandTTo;
                    _DES068.LandTCC = model.LandTCC;
                    _DES068.Subject = model.Subject;
                    _DES068.EmailBody = model.EmailBody;

                    if (temp == null)
                    {
                        _DES068.CreatedBy = CommonService.objClsLoginInfo.UserName;
                        _DES068.CreatedOn = DateTime.Now;
                        db.DES068.Add(_DES068);
                    }
                    else
                    {
                        _DES068.EditedBy = CommonService.objClsLoginInfo.UserName;
                        _DES068.EditedOn = DateTime.Now;
                    }
                    db.SaveChanges();
                    return _DES068.TransmittalEmailId;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<AgencyEmailModel> GetAllAgencyEmail()
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES068.OrderBy(x => x.TransmittalEmailId).Select(x => new AgencyEmailModel
                    {
                        Project = x.Project,
                        AgencyId = x.AgencyId,
                        AgencyTo = x.AgencyTo,
                        AgencyCC = x.AgencyCC,
                        LandTTo = x.LandTTo,
                        LandTCC = x.LandTCC,
                        Subject = x.Subject,
                        EmailBody = x.EmailBody
                }).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public AgencyEmailModel GetAgencyEmailbyId(Int64 Id)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES068.Where(x => x.TransmittalEmailId == Id).Select(x => new AgencyEmailModel
                    {
                        TransmittalEmailId = x.TransmittalEmailId,
                        Project = x.Project,
                        AgencyId = x.AgencyId,
                        AgencyTo = x.AgencyTo,
                        AgencyCC = x.AgencyCC,
                        LandTTo = x.LandTTo,
                        LandTCC = x.LandTCC,
                        Subject = x.Subject,
                        EmailBody = x.EmailBody
                    }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_TRANSMITTAL_EMAIL_CONFIGURATION_Result> GetAgencyEmailList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal,string Project)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_TRANSMITTAL_EMAIL_CONFIGURATION(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, Project).ToList();                    
                    recordsTotal = 0; 
                    int? rt = 0;
                    data.ForEach(x =>
                    {
                        rt = x.TableCount;
                        x.QString = CommonService.Encrypt("Id=" + x.TransmittalEmailId);
                    });
                    recordsTotal = rt ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public Int64 DeleteAgencyEmail(Int64 Id, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    var check = db.DES068.Find(Id);
                    if (check != null)
                    {
                        db.DES068.Remove(check);
                        db.SaveChanges();

                        return check.TransmittalEmailId;
                    }
                    return 0;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public bool CheckAgencyEmailAlreadyExist(Int64? agencyId, string Project, Int64? transmittalemailId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                { 
                    var data = db.DES068.Where(x => x.AgencyId == agencyId && x.Project == Project && (x.TransmittalEmailId != transmittalemailId || transmittalemailId == null)).ToList();
                    if (data != null && data.Count > 0)
                    {
                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                return false;
            }
        }
    }
}
