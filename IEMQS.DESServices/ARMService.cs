﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IEMQS.DESServices
{
    public class ARMService : IARMService
    {

        #region ARMSet

        public List<SP_DES_GET_ARMSet_Result> GetARMSet(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_ARMSet(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    return data;

                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }


        public List<SP_DES_GET_ARMFILEBY_ARMCODEID_Result> GetARMFileByArmCodeId(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, Int64? ArmCodeID, out int recordsTotal)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_ARMFILEBY_ARMCODEID(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, ArmCodeID).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    return data;

                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }



        public string AddEdit(ARMModel model, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    var temp = db.DES037.Find(model.Arms);
                    DES037 _DES037 = temp == null ? new DES037() : temp;

                    _DES037.Arms = model.Arms;
                    _DES037.Desc = model.Desc;
                    _DES037.Vrsn = model.Vrsn;
                    _DES037.Fnam = model.Fnam;
                    _DES037.ArmCode = model.ArmCode;
                    _DES037.Type = "Customized";
                    _DES037.IsFromDOC = false;
                    if (temp == null)
                    {
                        _DES037.Status = true; //true //_DES037.Status
                        _DES037.CreatedBy = CommonService.objClsLoginInfo.UserName;
                        _DES037.CreatedOn = DateTime.Now;
                        db.DES037.Add(_DES037);
                        errorMsg = "ArmSet added successfully";
                    }
                    else
                    {
                        _DES037.EditedBy = CommonService.objClsLoginInfo.UserName;
                        _DES037.EditedOn = DateTime.Now;
                        errorMsg = "ArmSet updated successfully";
                    }
                    db.SaveChanges();
                    return _DES037.Arms;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public Int64 DeleteArmSet(string Arms, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    var check = db.DES037.Find(Arms);
                    if (check != null)
                    {
                        db.DES037.Remove(check);
                        db.SaveChanges();
                    }
                    return 1;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<ARMModel> GetAllArmCode()
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES036.OrderBy(x => x.ARMCodeId).Select(x => new ARMModel
                    {
                        ARMCodeId = x.ARMCodeId,
                        Name = x.Name
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        #endregion

        #region ARMCode

        public List<SP_DES_GET_ARMCode_Result> GetARMCode(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_ARMCode(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    return data;

                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public Int64 AddEditARMCode(ARMCodeModal model, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";

                    #region Check Validation                   

                    if (model.ARMCodeId > 0)
                    {
                        var checkchild = db.DES036.Find(model.ARMCodeId);
                        if (checkchild != null)
                        {
                            if (checkchild.DES037 != null && checkchild.DES037.Count > 0)
                            {
                                errorMsg = "ARMCode is already in used. you cannot edit";
                                return 0;
                            }
                        }
                    }

                    var checkdublicate = db.DES036.Where(x => x.Name == model.Name && x.ARMCodeId != model.ARMCodeId).FirstOrDefault();
                    if (checkdublicate != null)
                    {
                        errorMsg = "ARMCode is already exist";
                        return 0;
                    }

                    #endregion

                    var temp = db.DES036.Find(model.ARMCodeId);
                    DES036 _DES036 = temp == null ? new DES036() : temp;

                    _DES036.Name = model.Name;
                    _DES036.Description = model.Description;

                    if (temp == null)
                    {
                        _DES036.CreatedBy = CommonService.objClsLoginInfo.UserName;
                        _DES036.CreatedOn = DateTime.Now;
                        db.DES036.Add(_DES036);
                    }
                    else
                    {
                        _DES036.EditedBy = CommonService.objClsLoginInfo.UserName;
                        _DES036.EditedOn = DateTime.Now;
                    }
                    db.SaveChanges();
                    return _DES036.ARMCodeId;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public Int64 DeleteArmCode(Int64 ARMCodeId, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    var check = db.DES036.Find(ARMCodeId);
                    if (check != null)
                    {
                        if (check.DES037 != null && check.DES037.Count > 0)
                        {
                            errorMsg = "ARMCode is already in used. you cannot delete";
                            return 0;
                        }

                        db.DES036.Remove(check);
                        db.SaveChanges();

                        return check.ARMCodeId;
                    }
                    return 0;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public bool UpdateArmSetStatus(string Arms, bool Status)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var _DES037 = db.DES037.Find(Arms);
                    _DES037.Status = Status;
                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }


        public Int64? AddARMCodeFile(ARMCodeModal model, int fileLocation)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var temp = db.DES036.Find(model.ARMCodeId);
                    DES112 _DES112 = new DES112();
                    _DES112.ARMCodeId = model.ARMCodeId;
                    _DES112.FileName = model.FileName;
                    _DES112.FilePath = model.FilePath;
                    _DES112.FileLocation = fileLocation;
                    _DES112.CreatedBy = CommonService.objClsLoginInfo.UserName;
                    _DES112.CreatedOn = DateTime.Now;
                    db.DES112.Add(_DES112);
                    db.SaveChanges();
                    return _DES112.ARMCodeFileId;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }


        public Int64 DeleteARMAttach(Int64 Id, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";

                    var objDES112 = db.DES112.Find(Id);
                    var getId = objDES112.ARMCodeFileId;
                    if (objDES112 != null)
                    {
                        db.DES112.Remove(objDES112);
                        db.SaveChanges();
                      //  AddEditCreateROCCommentsHistory(getId, null, null, "ROC " + title + "'s has been deleted");
                        return getId;
                    }
                    return 0;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }


        public bool GetRoleGroup(string PsNo)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    string strrolegroup = string.Empty;
                    var data = db.SP_DES_GET_USER_ROLE_ROLEGROUP(PsNo).Select(x => x.RoleGroup).ToList();
                    if (data.Contains("ITA"))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        #endregion
    }
}
