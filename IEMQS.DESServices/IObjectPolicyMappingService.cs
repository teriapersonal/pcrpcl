﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using System;
using System.Collections.Generic;

namespace IEMQS.DESServices
{
    public interface IObjectPolicyMappingService
    {
        Int64 AddEdit(ObjectPolicyMappingModel model, out string errorMsg);
        ObjectPolicyMappingModel GetObjectPolicyMappingbyId(Int64 Id);
        List<ObjectPolicyMappingModel> GetAllObjectPolicyMapping();
        List<SP_DES_GET_OBJECT_POLICY_MAPPING_Result> GetObjectPolicyMappingList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal);
        Int64 DeleteObjectPolicyMapping(Int64 Id, out string errorMsg);
    }
}
