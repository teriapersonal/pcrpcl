﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IEMQS.DESServices
{
    public class ObjectService : IObjectService
    {
        public long AddEdit(ObjectModel model, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = string.Empty;
                    #region check Validation

                    foreach (var item in model.BUIdList)
                    {
                        var check = db.DES003.Where(x => x.ObjectName == model.ObjectName && x.BUId == item && x.ObjectId != model.ObjectId).FirstOrDefault();
                        if (check != null)
                        {
                            errorMsg = "ObjectName with this BU is already exist";
                            return 0;
                        }
                    }                    
                    #endregion

                    if (model.ObjectId > 0)
                    {
                        DES003 _DES003 = db.DES003.Find(model.ObjectId);

                        _DES003.ObjectName = model.ObjectName;
                        _DES003.EditedBy = CommonService.objClsLoginInfo.UserName;
                        _DES003.EditedOn = DateTime.Now;

                        db.DES004.RemoveRange(_DES003.DES004);

                        List<DES004> _DES004List = new List<DES004>();
                        foreach (var categoryitem in model.CategoryIdList)
                        {
                            DES004 _DES004 = new DES004();
                            _DES004.ObjectId = model.ObjectId;
                            _DES004.MappingId = categoryitem;
                            _DES004.TypeId = (int)MenuType.Category;
                            _DES004List.Add(_DES004);

                        }
                        foreach (var actionitem in model.ActionIdList)
                        {
                            DES004 _DES004 = new DES004();
                            _DES004.ObjectId = model.ObjectId;
                            _DES004.MappingId = actionitem;
                            _DES004.TypeId = (int)MenuType.Action;
                            _DES004List.Add(_DES004);
                        }
                        _DES003.DES004 = _DES004List;
                        db.SaveChanges();
                    }
                    else
                    {
                        List<DES003> _DES003List = new List<DES003>();
                        var _lstBU = new BUService().GetAllBU();
                        foreach (var item in _lstBU)
                        {
                            DES003 _DES003 = new DES003();
                            _DES003.ObjectName = model.ObjectName;
                            _DES003.ApplyBU = false;
                            if ((model.BUIdList != null && model.BUIdList.Count > 0) && model.BUIdList.Contains(Convert.ToInt64(item.BUId)))
                            {
                                _DES003.ApplyBU = true;
                            }

                            _DES003.BUId = item.BUId;
                            _DES003.CreatedBy = CommonService.objClsLoginInfo.UserName;
                            _DES003.CreatedOn = DateTime.Now;
                            List<DES004> _DES004List = new List<DES004>();
                            foreach (var categoryitem in model.CategoryIdList)
                            {
                                DES004 _DES004 = new DES004();
                                _DES004.MappingId = categoryitem;
                                _DES004.TypeId = (int)MenuType.Category;
                                _DES004List.Add(_DES004);

                            }
                            foreach (var actionitem in model.ActionIdList)
                            {
                                DES004 _DES004 = new DES004();
                                _DES004.MappingId = actionitem;
                                _DES004.TypeId = (int)MenuType.Action;
                                _DES004List.Add(_DES004);
                            }
                            _DES003.DES004 = _DES004List;
                            _DES003List.Add(_DES003);
                        }
                        db.DES003.AddRange(_DES003List);
                        db.SaveChanges();
                    }
                    return 1;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<ObjectModel> GetAllObject()
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES003.OrderBy(x => x.ObjectId).Select(x => new ObjectModel
                    {
                        ObjectId = x.ObjectId,
                        ObjectName = x.ObjectName,
                        ApplyBU = x.ApplyBU,
                        BUId = x.BUId,
                        BU = x.DES001.BU +" - "+ x.DES001.PBU
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public ObjectModel GetObjectbyId(long Id)
        {
            throw new NotImplementedException();
        }

        public List<SP_DES_GET_OBJECT_Result> GetObjectList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_OBJECT(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public Int64 DeleteObject(Int64 Id, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    var check = db.DES003.Find(Id);

                    if (check != null)
                    {
                        if (check.DES007 != null && check.DES007.Count > 0)
                        {
                            errorMsg = "Object is already in used. you cannot delete";
                            return 0;
                        }

                        db.DES004.RemoveRange(check.DES004);
                        db.DES003.Remove(check);
                        db.SaveChanges();
                        return check.ObjectId;
                    }
                    return 0;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public bool UpdateApplyBU(Int64 Id, bool ApplyBU)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var _DES003 = db.DES003.Find(Id);
                    _DES003.ApplyBU = ApplyBU;
                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

    }
}
