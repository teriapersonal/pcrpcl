﻿using IEMQS.DESCore.Data;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace IEMQS.DESServices
{
    public class DESApiCreateBOMService
    {
        public static bool Add(ApiCreateBOMModel model, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    
                    var parentpart = db.DES066.Where(x => x.ItemKey == model.parentPartName).FirstOrDefault();
                    if (parentpart != null)
                    {
                        var childpart = db.DES066.Where(x => x.ItemKey == model.childPartName).FirstOrDefault();
                        if (childpart != null)
                        {
                            var check = db.DES067.Where(x => x.ItemId == childpart.ItemId && x.ParentItemId == parentpart.ItemId).FirstOrDefault();

                            if (model.action.Trim() == "ADD")
                            {
                                if (check != null)
                                {
                                    errorMsg = "";
                                    return true;
                                }

                                DES067 _DES067 = new DES067();
                                _DES067.ItemId = childpart.ItemId;
                                _DES067.ParentItemId = parentpart.ItemId;
                                _DES067.FindNumber = model.findNumber;
                                _DES067.Quantity = model.quantity;
                                _DES067.Length = model.length;
                                _DES067.Width = model.width;
                                _DES067.NumberOfPieces = model.numberOfPieces;
                                _DES067.CreatedBy = model.owner;
                                _DES067.CreatedOn = DateTime.Now;
                                db.DES067.Add(_DES067);
                            }
                            else
                            {
                                DES067 _DES067 = db.DES067.Find(check.BOMId);
                                _DES067.FindNumber = model.findNumber;
                                _DES067.Quantity = model.quantity;
                                _DES067.Length = model.length;
                                _DES067.Width = model.width;
                                _DES067.NumberOfPieces = model.numberOfPieces;
                                _DES067.EditedBy = model.owner;
                                _DES067.EditedOn = DateTime.Now;
                            }
                            db.SaveChanges();
                            return true;
                        }
                        else
                        {
                            errorMsg = "Child part does not exist.";
                            return false;
                        }
                    }
                    else
                    {
                        errorMsg = "Parent part does not exist.";
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                errorMsg = "";
                return false;
            }
        }
    }

    public class ApiCreateBOMModel
    {
        [Required(ErrorMessage = "Action is required")]
        public string action { get; set; }

        [Required(ErrorMessage = "Child part name is required")]
        [StringLength(30, ErrorMessage = "Child Part Name length can't be more than 30")]
        public string childPartName { get; set; }

        [Required(ErrorMessage = "Child part type is required")]
        [StringLength(12, ErrorMessage = "Child Part Type length can't be more than 12")]
        public string childPartType { get; set; }

        [Required(ErrorMessage = "Find number is required")]
        public int? findNumber { get; set; }

        [Required(ErrorMessage = "Owner is required")]
        [StringLength(9, ErrorMessage = "Owner length can't be more than 9")]
        public string owner { get; set; }
        public double? length { get; set; }
        public int? numberOfPieces { get; set; }

        [Required(ErrorMessage = "Parent part name is required")]
        [StringLength(30, ErrorMessage = "Parent Part Name length can't be more than 30")]
        public string parentPartName { get; set; }

        [Required(ErrorMessage = "Parent part type is required")]
        [StringLength(12, ErrorMessage = "Parent Part Type length can't be more than 12")]
        public string parentPartType { get; set; }

        [Required(ErrorMessage = "Quantity is required")]
        public double? quantity { get; set; }
        public double? width { get; set; }
    }
}
