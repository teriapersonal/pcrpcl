﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using System;
using System.Collections.Generic;

namespace IEMQS.DESServices
{
    public interface IPartTypeService
    {
        Int64 AddEdit(PartTypeModel model, out string errorMsg);
        List<SP_DES_GET_PART_TYPE_Result> GetPartTypeList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal);
        Int64 DeletePartType(Int64 Id, out string errorMsg);
        List<string> GetProductFormList();
    }
}
