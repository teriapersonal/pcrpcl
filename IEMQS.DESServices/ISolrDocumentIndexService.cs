﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using System;
using System.Collections.Generic;

namespace IEMQS.DESServices
{
    public interface ISolrDocumentIndexService
    {
        List<DES060> GetNonIndexedFiles();

        List<SP_DES_GET_NON_INDEX_DOC_OBJECT_FILES_FOR_INDEX_Result> GetNonIndexDocObject();

        List<SP_DES_GET_NON_INDEX_JEP_OBJECT_DATA_FOR_INDEX_Result> GetNonIndexJEPObject();

        List<SP_DES_GET_NON_INDEX_DIN_OBJECT_DATA_FOR_INDEX_Result> GetNonIndexDINObject();

        List<SP_DES_GET_NON_INDEX_CUSTOMER_FEEDBACK_FILES_Result> GetNonIndexCustomerFeedbackFiles();
        List<SP_DES_GET_NON_INDEX_DCR_FILES_Result> GetNonIndexDCRFiles();

        List<SP_DES_GET_NON_INDEX_PART_OBJECT_Result> GetNonIndexParts();

        List<SP_DES_GET_NON_INDEX_PROJECT_Result> GetNonindexProjects();

        List<SP_DES_GET_NON_INDEX_ROC_FILES_Result> GetNonIndex_ROC_Files();

        List<SP_DES_GET_NON_INDEX_ROC_COMMENT_ATTACHED_FILES_Result> GetNonIndex_ROC_Attached_Files();

        List<SP_DES_GET_ADVANCE_SEARCH_DOC_Result> GetAdvanceSerchDoc(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, string DocMapId);

        List<SP_DES_GET_FileLogs_Result> GetFilelogStatus(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal);

        List<SP_DES_GET_FREQUENT_SEARCHED_DOCUMENTS_Result> GetMostSearchedDocuments(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal);

        string UpdateIndexFlag(long DocumentMappingId);

        List<string> GetCustomerNameasperTerm(string Term);

        List<AdvanceSearchModel> GetBUList(string projectno);

        List<AdvanceSearchModel> GetObjectList(string projectno);

        List<AdvanceSearchModel> GetObjectListBasedonBuId(string BUId);

        List<AdvanceSearchModel> GetStatusList(string projectno);

        List<string> GetOwnerName(string ownerInitials);

        List<string> GetDepartmentlist(string department);

        List<string> GetProjectListName(string proj);

        List<SP_DES_GET_StatusAsPerObjectId_Result> GetStatusListAsperObjectId(string ObjectId);

        void UpdateSolrSchedularRunDate(DateTime dateTime);

        void AddSearchResult(List<DocumentIndexModel> documentIndexModels);

        string SaveFilterData(string CustomerName, string BUPBU, string Department, string Object, string Status, string Name, string Revision, string Originator, string Owner, string SearchKeyword, string SearchName, string ProjectNo, int? FilterId, string SubObject);

        string DeleteFilterData(long filterid);

        List<DES110> BindFilterdata(long PsNo);

        List<DES110> FetchSavedFilterData(long FilterId, long PsNo);
        List<SP_DES_DEL_REMOVE_DATA_INDEX_Result> GetRemoveIndexData();
        void RemoveIndexData(List<Int64> Ids);

    }
}
