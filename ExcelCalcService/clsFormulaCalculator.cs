﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.InteropServices;
using Excel = Microsoft.Office.Interop.Excel;
using ExcelCalcService.Models;

namespace ExcelCalcService
{
    public class clsFormulaCalculator : IDisposable
    {
        public ResponseModel CalculateFormula(string formula)
        {
            ResponseModel objResponse = new ResponseModel();
            objResponse.formula = formula;

            if (!formula.StartsWith("="))
                formula = "=" + formula;

            Excel.Application xlApp = new Excel.Application();
            Excel.Workbook xlWB = xlApp.Workbooks.Add();
            Excel.Worksheet xlWS = (Excel.Worksheet)xlWB.Worksheets.get_Item(1);

            try
            {
                //xlWS.Cells[1, 1].Formula = formula;
                //xlWS.Calculate();
                //result = xlWS.Cells[1, 1].Value.ToString();               
                xlWS.Cells[1, 1] = formula;
                Excel.Range range = (Excel.Range)xlWS.Cells[1, 1];

                objResponse.key = true;
                objResponse.value = range.Value.ToString();

                if (range != null)
                    Marshal.FinalReleaseComObject(range);
            }
            catch (Exception ex)
            {
                objResponse.key = false;
                objResponse.value = ex.Message;
            }
            finally
            {
                xlWB.Close(0);
                xlApp.Quit();

                if (xlWS != null)
                    Marshal.FinalReleaseComObject(xlWS);

                if (xlWB != null)
                    Marshal.FinalReleaseComObject(xlWB);

                if (xlApp != null)
                    Marshal.FinalReleaseComObject(xlApp);
            }
            return objResponse;
        }

        public void Dispose() // helper finalize function
        {
            // here you can free the resources you allocated explicitly
            GC.SuppressFinalize(this);

            GC.Collect();
            GC.WaitForPendingFinalizers();
        }
    }
}