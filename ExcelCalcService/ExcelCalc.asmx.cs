﻿using ExcelCalcService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;

namespace ExcelCalcService
{
    /// <summary>
    /// Summary description for ExcelCalcService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class ExcelCalc : System.Web.Services.WebService
    {
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public ResponseModel CalculateFormula(string formula)
        {
            ResponseModel objResponse = new ResponseModel();
            objResponse.key = false;
            objResponse.formula = formula;

            clsFormulaCalculator obj1 = new clsFormulaCalculator();
            try
            {
                //clsCalculateFormula objTest = new clsCalculateFormula();
                //objResponse.value = objTest.CalculateFormula(formula);

                //objTest.DisposeObject();

                //objResponse.key = true;
                //objResponse.formula = formula;

                objResponse = obj1.CalculateFormula(formula);
            }
            catch (Exception ex)
            {
                objResponse.key = false;
                objResponse.value = ex.Message;
            }
            finally
            {
                obj1.Dispose();
            }
            return objResponse;
        }
    }
}
