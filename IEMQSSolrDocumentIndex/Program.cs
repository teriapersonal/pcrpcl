﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using GemBox.Document;
using IEMQS.DESCore.Data;
using IEMQS.DESServices;
using IEMQSSolrDocumentIndex.DAL;
using IEMQSSolrDocumentIndex.Indexing;
using IEMQSSolrDocumentIndex.Searching;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;
using AcApplication = Autodesk.AutoCAD.ApplicationServices.Application;
using Autodesk.AutoCAD.EditorInput;
using CADImport;
using CADImport.DWG;
using CADImport.DXF;
using CADImport.RasterImage;
using CADImport.CAD3D;
using System.Threading.Tasks;
using Tesseract;
using GemBox.Presentation;
using System.Windows.Xps.Packaging;
using System.Net;
using System.Configuration;

namespace IEMQSSolrDocumentIndex
{
    class Program
    {
        //public  Document acDoc
        //{
        //    get
        //    {
        //        return Autodesk.AutoCAD.ApplicationServices.Application.DocumentManager.MdiActiveDocument;
        //    }
        //}

        static void Main(string[] args)
        {
            var SolrIndexFor = ConfigurationManager.AppSettings["SolrIndexFor"] != null ? Convert.ToInt32(ConfigurationManager.AppSettings["SolrIndexFor"].ToString()) : 2;
            if (SolrIndexFor == 1)
            {
                FetchFileAndIndexFiles();
            }
            else if (SolrIndexFor == 2)
            {
                FetchFileforUVSearchFileCreation();
            }
        }

        private static void FetchFileAndIndexFiles()
        {
            try
            {
                var ApplicationRunFor = ConfigurationManager.AppSettings["ApplicationRunFor"] != null ? Convert.ToInt32(ConfigurationManager.AppSettings["ApplicationRunFor"].ToString()) : 0;
                var RecordsToBeIndexLoop = ConfigurationManager.AppSettings["RecordsToBeIndexLoop"] != null ? Convert.ToInt32(ConfigurationManager.AppSettings["RecordsToBeIndexLoop"].ToString()) : 500;
                var StartIndexForDocument = ConfigurationManager.AppSettings["StartIndexForDocument"] != null ? Convert.ToInt64(ConfigurationManager.AppSettings["StartIndexForDocument"].ToString()) : 1;
                var EndIndexForDocument = ConfigurationManager.AppSettings["EndIndexForDocument"] != null ? Convert.ToInt64(ConfigurationManager.AppSettings["EndIndexForDocument"].ToString()) : Int64.MaxValue;

                //string SolrUrl = "http://10.7.66.55:8983/solr/SlrCore";
                string SolrUrl = new CommonService().GetConfigvalues("Solr_URL"); //"http://10.7.66.55:8983/solr/QACore";
                List<DocumentModel> _des060s = new List<DocumentModel>();
                SolrDocumentIndexService ObjsolrDocumentIndexService = new SolrDocumentIndexService();
                DocumentIndexer documentIndexer = new DocumentIndexer(SolrUrl);

                Console.WriteLine("Ready for fetch files.");

                #region Remove All Documents
                if (ApplicationRunFor == 100)
                {
                    var recordsTotal = 0;
                    var listAllDocuments = ObjsolrDocumentIndexService.Search("BU:*", 0, int.MaxValue, 0, "", out recordsTotal);
                    var listObjectIdIds = listAllDocuments.Select(i => i.ObjectId).ToList();
                    documentIndexer.RemoveFiles(listObjectIdIds);
                }
                #endregion

                if (ApplicationRunFor == 0 || ApplicationRunFor == 1)
                {
                    #region Index DOC Object
                    try
                    {

                        Console.WriteLine("Ready for fetch Non Index DOC objects.");
                        _des060s = ObjsolrDocumentIndexService.GetNonIndexDocObject().Where(i => i.RowNo >= StartIndexForDocument && i.RowNo <= EndIndexForDocument).Select(d => new DocumentModel
                        {
                            ObjectId = d.ObjectId,
                            Customer = d.Customer,
                            BU = d.BU,
                            Department = d.Department,
                            Type = d.Type,
                            Status = d.Status,
                            Name = d.Name,
                            Revision = d.Revision,
                            Originator = d.Originator,
                            OwnerName = d.OwnerName,
                            MainDocumentPath = d.MainDocumentPath,
                            Descripion = d.Descripion,
                            FileFormat = d.FileFormate,
                            version = d.FileVersion,
                            FileName = d.Document_name,
                            DocumentMappingId = d.DocumentMappingId.ToString(),
                            ProjectNumber = d.Project,
                            SubObject = d.SubObject,
                            RowNo=d.RowNo.Value
                        }).ToList();

                        Console.WriteLine("Total file found :: " + _des060s.Count());

                        //documentIndexer.IndexFile(_des060s);

                        //Console.WriteLine("Updating index flag for files.");
                        //foreach (DocumentModel documentModel in _des060s)
                        //{
                        //    ObjsolrDocumentIndexService.UpdateIndexFlag(Convert.ToInt64(documentModel.DocumentMappingId));
                        //    ObjsolrDocumentIndexService.UpdateIndexFlagForCustomerFeedbackfiles(Convert.ToInt64(documentModel.DocumentMappingId), "DOC", documentModel.ObjectId);
                        //}
                        var CurrentIndex = 0;
                        while (CurrentIndex < _des060s.Count)
                        {
                            //System.Threading.Thread.Sleep(100);
                            try
                            {
                                var templist = _des060s.Skip(CurrentIndex).Take(RecordsToBeIndexLoop).ToList();
                                Console.WriteLine("DOC objects Solr Index Start For :" + (CurrentIndex + 1) + " To " + (CurrentIndex + RecordsToBeIndexLoop));
                                documentIndexer.IndexFile(templist);
                                Console.WriteLine("DOC objects Solr Index End For :" + (CurrentIndex + 1) + " To " + (CurrentIndex + RecordsToBeIndexLoop));

                                Console.WriteLine("Updating index flag for files.");
                                foreach (DocumentModel documentModel in templist)
                                {
                                    ObjsolrDocumentIndexService.UpdateIndexFlag(Convert.ToInt64(documentModel.DocumentMappingId));
                                    ObjsolrDocumentIndexService.UpdateIndexFlagForCustomerFeedbackfiles(Convert.ToInt64(documentModel.DocumentMappingId), "DOC", documentModel.ObjectId);
                                }
                            }
                            catch (System.Exception ex)
                            {
                                Console.WriteLine("DOC objects Solr Index For :" + (CurrentIndex + 1) + " To " + (CurrentIndex + RecordsToBeIndexLoop) + " Error :" + ex.Message.ToString());
                            }
                            if (CurrentIndex + RecordsToBeIndexLoop > _des060s.Count)
                            {
                                CurrentIndex = _des060s.Count;
                            }
                            else
                            {
                                CurrentIndex = CurrentIndex + RecordsToBeIndexLoop;
                            }
                        }

                    }
                    catch (System.Exception)
                    {
                    }
                    #endregion
                }
                if (ApplicationRunFor == 0 || ApplicationRunFor == 2)
                {
                    #region Index JEP Object
                    try
                    {
                        Console.WriteLine("Ready for fetch Non Index JEP objects.");

                        _des060s = ObjsolrDocumentIndexService.GetNonIndexJEPObject().Select(d => new DocumentModel
                        {
                            ObjectId = d.ObjectId,
                            Customer = d.Customer,
                            BU = d.BU,
                            Department = d.Department,
                            Type = d.Type,
                            Status = d.Status,
                            Name = d.Name,
                            Revision = d.Revision,
                            Originator = d.Originator,
                            OwnerName = d.OwnerName,
                            MainDocumentPath = d.MainDocumentPath,
                            Descripion = d.Descripion,
                            JEPCustFeedbackDocumentId = d.JEPId.ToString(),
                            ProjectNumber = d.Project,
                            SubObject = d.SubObject
                        }).ToList();

                        Console.WriteLine("Total file found :: " + _des060s.Count());

                        // DocumentIndexer documentIndexer = new DocumentIndexer(SolrUrl);

                        documentIndexer.IndexFile(_des060s);

                        Console.WriteLine("Updating index flag for JEP.");
                        foreach (DocumentModel documentModel in _des060s)
                        {
                            ObjsolrDocumentIndexService.UpdateIndexFlagForCustomerFeedbackfiles(Convert.ToInt64(documentModel.JEPCustFeedbackDocumentId), "JEP", documentModel.ObjectId);
                        }
                    }
                    catch (System.Exception ex)
                    {
                    }
                    #endregion
                }
                if (ApplicationRunFor == 0 || ApplicationRunFor == 3)
                {
                    #region Index Customer Feedback Files
                    try
                    {
                        Console.WriteLine("Ready for fetch Non Index Customer Feedback files.");
                        _des060s = ObjsolrDocumentIndexService.GetNonIndexCustomerFeedbackFiles().Select(d => new DocumentModel
                        {
                            ObjectId = d.ObjectId,
                            Customer = d.Customer,
                            BU = d.BU,
                            Department = d.Department,
                            Type = d.Type,
                            Status = d.Status,
                            Name = d.Name,
                            Revision = d.Revision,
                            Originator = d.Originator,
                            OwnerName = d.OwnerName,
                            MainDocumentPath = d.MainDocumentPath,
                            Descripion = d.Descripion,
                            FileFormat = d.FileFormate,
                            version = d.FileVersion,
                            FileName = d.Document_name,
                            JEPCustFeedbackDocumentId = d.JEPCustFeedbackDocumentId.ToString(),
                            ProjectNumber = d.Project,
                            SubObject = d.SubObject
                        }).ToList();

                        Console.WriteLine("Total file found :: " + _des060s.Count());

                        //DocumentIndexer documentIndexer = new DocumentIndexer(SolrUrl);

                        documentIndexer.IndexFile(_des060s);

                        Console.WriteLine("Updating index flag for customer feedback files.");
                        foreach (DocumentModel documentModel in _des060s)
                        {
                            ObjsolrDocumentIndexService.UpdateIndexFlagForCustomerFeedbackfiles(Convert.ToInt64(documentModel.JEPCustFeedbackDocumentId), "JEP-CustomerFeedback", documentModel.ObjectId);
                        }
                    }
                    catch (System.Exception ex)
                    {
                    }
                    #endregion
                }
                if (ApplicationRunFor == 0 || ApplicationRunFor == 4)
                {
                    #region Index DCR Files
                    try
                    {
                        Console.WriteLine("Ready for fetch Non Index DCR files.");
                        _des060s = ObjsolrDocumentIndexService.GetNonIndexDCRFiles().Select(d => new DocumentModel
                        {
                            ObjectId = d.ObjectId,
                            Customer = d.Customer,
                            BU = d.BU,
                            Department = d.Department,
                            Type = d.Type,
                            Status = d.Status,
                            Name = d.Name,
                            Revision = d.Revision,
                            Originator = d.Originator,
                            OwnerName = d.OwnerName,
                            MainDocumentPath = d.MainDocumentPath,
                            Descripion = d.Descripion,
                            FileFormat = d.FileFormate,
                            version = d.FileVersion,
                            FileName = d.Document_name,
                            JEPCustFeedbackDocumentId = d.DCRSupportedDocId.ToString(),
                            ProjectNumber = d.Project,
                            SubObject = d.SubObject
                        }).ToList();

                        Console.WriteLine("Total file found :: " + _des060s.Count());

                        //DocumentIndexer documentIndexer = new DocumentIndexer(SolrUrl);

                        documentIndexer.IndexFile(_des060s);

                        Console.WriteLine("Updating index flag for DCR files.");
                        foreach (DocumentModel documentModel in _des060s)
                        {
                            ObjsolrDocumentIndexService.UpdateIndexFlagForCustomerFeedbackfiles(Convert.ToInt64(documentModel.JEPCustFeedbackDocumentId), "DCR", documentModel.ObjectId);
                        }
                    }
                    catch (System.Exception ex)
                    {
                    }
                    #endregion
                }
                if (ApplicationRunFor == 0 || ApplicationRunFor == 5)
                {
                    #region Index Parts
                    try
                    {
                        Console.WriteLine("Ready for fetch Non Index parts.");
                        _des060s = ObjsolrDocumentIndexService.GetNonIndexParts().Select(d => new DocumentModel
                        {
                            ObjectId = d.ObjectId,
                            Customer = d.Customer,
                            BU = d.BU,
                            Department = d.Department,
                            Type = d.Type,
                            Status = d.Status,
                            Name = d.Name,
                            Revision = d.Revision,
                            Originator = d.Originator,
                            OwnerName = d.OwnerName,
                            MainDocumentPath = d.MainDocumentPath,
                            Descripion = d.Descripion,
                            FileFormat = d.FileFormate,
                            version = d.FileVersion,
                            FileName = d.Document_name,
                            JEPCustFeedbackDocumentId = d.ItemId.ToString(),
                            ProjectNumber = d.Project,
                            SubObject = d.SubObject
                        }).ToList();

                        Console.WriteLine("Total file found :: " + _des060s.Count());

                        //DocumentIndexer documentIndexer = new DocumentIndexer(SolrUrl);

                        //documentIndexer.IndexFile(_des060s);

                        //Console.WriteLine("Updating index flag for parts.");
                        //foreach (DocumentModel documentModel in _des060s)
                        //{
                        //    ObjsolrDocumentIndexService.UpdateIndexFlagForCustomerFeedbackfiles(Convert.ToInt64(documentModel.JEPCustFeedbackDocumentId), "Part", documentModel.ObjectId);
                        //}
                        var CurrentIndex = 0;
                        while (CurrentIndex < _des060s.Count)
                        {
                            var templist = _des060s.Skip(CurrentIndex).Take(RecordsToBeIndexLoop).ToList();
                            Console.WriteLine("Part objects Solr Index Start For :" + (CurrentIndex + 1) + " To " + (CurrentIndex + RecordsToBeIndexLoop));
                            documentIndexer.IndexFile(templist);
                            Console.WriteLine("Part objects Solr Index End For :" + (CurrentIndex + 1) + " To " + (CurrentIndex + RecordsToBeIndexLoop));

                            Console.WriteLine("Updating index flag for parts.");
                            foreach (DocumentModel documentModel in templist)
                            {
                                ObjsolrDocumentIndexService.UpdateIndexFlagForCustomerFeedbackfiles(Convert.ToInt64(documentModel.JEPCustFeedbackDocumentId), "Part", documentModel.ObjectId);
                            }
                            if (CurrentIndex + RecordsToBeIndexLoop > _des060s.Count)
                            {
                                CurrentIndex = _des060s.Count;
                            }
                            else
                            {
                                CurrentIndex = CurrentIndex + RecordsToBeIndexLoop;
                            }
                        }
                    }
                    catch (System.Exception ex)
                    {
                    }
                    #endregion
                }
                if (ApplicationRunFor == 0 || ApplicationRunFor == 6)
                {
                    #region Index Projects
                    try
                    {
                        Console.WriteLine("Ready for fetch Non Index Projects.");
                        _des060s = ObjsolrDocumentIndexService.GetNonindexProjects().Select(d => new DocumentModel
                        {
                            ObjectId = d.ObjectId,
                            Customer = d.Customer,
                            BU = d.BU,
                            Department = d.Department,
                            Type = d.Type,
                            Status = d.Status,
                            Name = d.Name,
                            Revision = d.Revision,
                            Originator = d.Originator,
                            OwnerName = d.OwnerName,
                            MainDocumentPath = d.MainDocumentPath,
                            Descripion = d.Descripion,
                            FileFormat = d.FileFormate,
                            version = d.FileVersion,
                            FileName = d.Document_name,
                            JEPCustFeedbackDocumentId = d.Project.ToString(),
                            ProjectNumber = d.Project,
                            SubObject = d.SubObject
                        }).ToList();

                        Console.WriteLine("Total file found :: " + _des060s.Count());

                        //DocumentIndexer documentIndexer = new DocumentIndexer(SolrUrl);

                        documentIndexer.IndexFile(_des060s);

                        Console.WriteLine("Updating index flag for Project.");
                        foreach (DocumentModel documentModel in _des060s)
                        {
                            ObjsolrDocumentIndexService.UpdateIndexFlagForProject(documentModel.JEPCustFeedbackDocumentId, documentModel.ObjectId);
                        }
                    }
                    catch (System.Exception ex)
                    {
                    }
                    #endregion
                }

                #region Commected Code
                //#region Index ROC Files  //Currently not in use as this functionality is absolute as pe Bhavin R P on 3/04/2020
                //Console.WriteLine("Ready for fetch Non Index ROC Files.");
                //_des060s = ObjsolrDocumentIndexService.GetNonIndex_ROC_Files().Select(d => new DocumentModel
                //{
                //    ObjectId = d.ObjectId,
                //    Customer = d.Customer,
                //    BU = d.BU,
                //    Department = d.Department,
                //    Type = d.Type,
                //    Status = d.Status,
                //    Name = d.Name,
                //    Revision = d.Revision,
                //    Originator = d.Originator,
                //    OwnerName = d.OwnerName,
                //    MainDocumentPath = d.MainDocumentPath,
                //    Descripion = d.Descripion,
                //    FileFormat = d.FileFormate,
                //    version = d.FileVersion,
                //    FileName = d.Document_name,
                //    JEPCustFeedbackDocumentId = d.ROCCommentedDocId.ToString()
                //}).ToList();

                //Console.WriteLine("Total file found :: " + _des060s.Count());

                ////DocumentIndexer documentIndexer = new DocumentIndexer(SolrUrl);

                //documentIndexer.IndexFile(_des060s);

                //Console.WriteLine("Updating index flag for ROC Files.");
                //foreach (DocumentModel documentModel in _des060s)
                //{
                //    ObjsolrDocumentIndexService.UpdateIndexFlagForCustomerFeedbackfiles(Convert.ToInt64(documentModel.JEPCustFeedbackDocumentId),"ROC");
                //}

                //#endregion
                #endregion

                if (ApplicationRunFor == 0 || ApplicationRunFor == 7)
                {
                    #region Index ROC Attached Files
                    try
                    {
                        Console.WriteLine("Ready for fetch Non Index ROC Attached Files.");
                        _des060s = ObjsolrDocumentIndexService.GetNonIndex_ROC_Attached_Files().Select(d => new DocumentModel
                        {
                            ObjectId = d.ObjectId,
                            Customer = d.Customer,
                            BU = d.BU,
                            Department = d.Department,
                            Type = d.Type,
                            Status = d.Status,
                            Name = d.Name,
                            Revision = d.Revision,
                            Originator = d.Originator,
                            OwnerName = d.OwnerName,
                            MainDocumentPath = d.MainDocumentPath,
                            Descripion = d.Descripion,
                            FileFormat = d.FileFormate,
                            version = d.FileVersion,
                            FileName = d.Document_name,
                            JEPCustFeedbackDocumentId = d.ROCCommentsAttachId.ToString(),
                            ProjectNumber = d.Project,
                            SubObject = d.SubObject
                        }).ToList();

                        Console.WriteLine("Total file found :: " + _des060s.Count());

                        //DocumentIndexer documentIndexer = new DocumentIndexer(SolrUrl);

                        documentIndexer.IndexFile(_des060s);

                        Console.WriteLine("Updating index flag for ROC Attached  Files.");
                        foreach (DocumentModel documentModel in _des060s)
                        {
                            ObjsolrDocumentIndexService.UpdateIndexFlagForCustomerFeedbackfiles(Convert.ToInt64(documentModel.JEPCustFeedbackDocumentId), "ROC-Attached", documentModel.ObjectId);
                        }
                    }
                    catch (System.Exception ex)
                    {
                    }
                    #endregion
                }
                if (ApplicationRunFor == 0 || ApplicationRunFor == 8)
                {
                    #region Index ROC Comments
                    try
                    {
                        Console.WriteLine("Ready for fetch Non Index ROC Agency Comments.");
                        _des060s = ObjsolrDocumentIndexService.GetNonIndex_ROC_Comments().Select(d => new DocumentModel
                        {
                            ObjectId = d.ObjectId,
                            Customer = d.Customer,
                            BU = d.BU,
                            Department = d.Department,
                            Type = d.Type,
                            Status = d.Status,
                            Name = d.Name,
                            Revision = d.Revision,
                            Originator = d.Originator,
                            OwnerName = d.OwnerName,
                            MainDocumentPath = d.MainDocumentPath,
                            Descripion = d.Descripion,
                            FileFormat = d.FileFormate,
                            version = d.FileVersion,
                            FileName = d.Document_name,
                            JEPCustFeedbackDocumentId = d.ItemId.ToString(),
                            ProjectNumber = d.Project,
                            SubObject = d.SubObject
                        }).ToList();

                        Console.WriteLine("Total file found :: " + _des060s.Count());

                        //DocumentIndexer documentIndexer = new DocumentIndexer(SolrUrl);

                        documentIndexer.IndexFile(_des060s);

                        Console.WriteLine("Updating index flag for ROC Comment.");
                        foreach (DocumentModel documentModel in _des060s)
                        {
                            ObjsolrDocumentIndexService.UpdateIndexFlagForCustomerFeedbackfiles(Convert.ToInt64(documentModel.JEPCustFeedbackDocumentId), "ROC Comment", documentModel.ObjectId);
                        }
                    }
                    catch (System.Exception ex)
                    {
                    }
                    #endregion
                }
                if (ApplicationRunFor == 0 || ApplicationRunFor == 9)
                {
                    #region Index DIN Object
                    try
                    {
                        Console.WriteLine("Ready for fetch Non Index DIN objects.");

                        _des060s = ObjsolrDocumentIndexService.GetNonIndexDINObject().Select(d => new DocumentModel
                        {
                            ObjectId = d.ObjectId,
                            Customer = d.Customer,
                            BU = d.BU,
                            Department = d.Department,
                            Type = d.Type,
                            Status = d.Status,
                            Name = d.Name,
                            Revision = d.Revision,
                            Originator = d.Originator,
                            OwnerName = d.OwnerName,
                            MainDocumentPath = d.MainDocumentPath,
                            Descripion = d.Descripion,
                            JEPCustFeedbackDocumentId = d.DINId.ToString(),
                            ProjectNumber = d.ProjectNo,
                            SubObject = d.SubObject
                        }).ToList();

                        Console.WriteLine("Total file found :: " + _des060s.Count());

                        // DocumentIndexer documentIndexer = new DocumentIndexer(SolrUrl);

                        //documentIndexer.IndexFile(_des060s);

                        //Console.WriteLine("Updating index flag for DIN.");
                        //foreach (DocumentModel documentModel in _des060s)
                        //{
                        //    ObjsolrDocumentIndexService.UpdateIndexFlagForCustomerFeedbackfiles(Convert.ToInt64(documentModel.JEPCustFeedbackDocumentId), "DIN", documentModel.ObjectId);
                        //}

                        var CurrentIndex = 0;
                        while (CurrentIndex < _des060s.Count)
                        {
                            var templist = _des060s.Skip(CurrentIndex).Take(RecordsToBeIndexLoop).ToList();
                            Console.WriteLine("DIN objects Solr Index Start For :" + (CurrentIndex + 1) + " To " + (CurrentIndex + RecordsToBeIndexLoop));
                            documentIndexer.IndexFile(templist);
                            Console.WriteLine("DIN objects Solr Index End For :" + (CurrentIndex + 1) + " To " + (CurrentIndex + RecordsToBeIndexLoop));

                            Console.WriteLine("Updating index flag for DIN.");
                            foreach (DocumentModel documentModel in templist)
                            {
                                ObjsolrDocumentIndexService.UpdateIndexFlagForCustomerFeedbackfiles(Convert.ToInt64(documentModel.JEPCustFeedbackDocumentId), "DIN", documentModel.ObjectId);
                            }
                            if (CurrentIndex + RecordsToBeIndexLoop > _des060s.Count)
                            {
                                CurrentIndex = _des060s.Count;
                            }
                            else
                            {
                                CurrentIndex = CurrentIndex + RecordsToBeIndexLoop;
                            }
                        }
                    }
                    catch (System.Exception ex)
                    {
                    }
                    #endregion
                }
                #region Remove Index Data From Solr
                Console.WriteLine("Remove Index Data From Solr");
                var listRemoveIndexData = ObjsolrDocumentIndexService.GetRemoveIndexData();
                if (listRemoveIndexData.Count > 0)
                {
                    var listIds = listRemoveIndexData.Select(i => i.SolrObjectId).ToList();
                    documentIndexer.RemoveFiles(listIds);

                    var listDES106Ids = listRemoveIndexData.Select(i => i.Central_IndexStatusId).ToList();
                    ObjsolrDocumentIndexService.RemoveIndexData(listDES106Ids);
                }
                Console.WriteLine("Remove Index Data From Solr Completed");
                #endregion

                Console.WriteLine("Indexing completed.");

                Console.WriteLine("Index flag has been updated for file.");

                ObjsolrDocumentIndexService.UpdateSolrSchedularRunDate(DateTime.Now);
            }
            catch (System.Exception ex)
            {
                Console.WriteLine("There is error: " + ex.Message);
                //Console.ReadLine();

                throw ex;
            }
        }

        private static void FetchFileforUVSearchFileCreation()
        {
            try
            {
                List<DocumentModel> _des060s_DOC_UV_Search = new List<DocumentModel>();
                List<DocumentModel> _des060s_DCR_UV_Search = new List<DocumentModel>();
                List<DocumentModel> _des060s_JEP_Cust_Feedback_UV_Search = new List<DocumentModel>();
                List<DocumentModel> _des060s_ROC_UV_Search = new List<DocumentModel>();
                SolrDocumentIndexService ObjsolrDocumentIndexService = new SolrDocumentIndexService();

                //Create DOC UV Search file
                #region Create DOC UV Search file
                Console.WriteLine("DOC UV Search file creation started.");
                _des060s_DOC_UV_Search = ObjsolrDocumentIndexService.GetNonIndexDocObject().Select(d => new DocumentModel
                {
                    ObjectId = d.ObjectId,
                    Customer = d.Customer,
                    BU = d.BU,
                    Department = d.Department,
                    Type = d.Type,
                    Status = d.Status,
                    Name = d.Name,
                    Revision = d.Revision,
                    Originator = d.Originator,
                    OwnerName = d.OwnerName,
                    MainDocumentPath = d.MainDocumentPath,
                    Descripion = d.Descripion,
                    FileFormat = d.FileFormate,
                    version = d.FileVersion,
                    FileName = d.Document_name,
                    DocumentMappingId = d.DocumentMappingId.ToString(),
                    ProjectNumber = d.Project
                }).ToList();

                foreach (DocumentModel documentModel in _des060s_DOC_UV_Search)
                {
                    //Console.WriteLine("Search file process start: " + documentModel.MainDocumentPath);
                    string result = TestSeachFileGenearte(documentModel.MainDocumentPath);
                    //Console.WriteLine("Output:" + result);
                }
                Console.WriteLine("DOC UV Search file creation completed.");
                #endregion

                //Create DCR UV Search file
                #region Create DCR UV Search file
                Console.WriteLine("DCR UV Search file creation started.");
                _des060s_DCR_UV_Search = ObjsolrDocumentIndexService.GetNonIndexDCRFiles().Select(d => new DocumentModel
                {
                    ObjectId = d.ObjectId,
                    Customer = d.Customer,
                    BU = d.BU,
                    Department = d.Department,
                    Type = d.Type,
                    Status = d.Status,
                    Name = d.Name,
                    Revision = d.Revision,
                    Originator = d.Originator,
                    OwnerName = d.OwnerName,
                    MainDocumentPath = d.MainDocumentPath,
                    Descripion = d.Descripion,
                    FileFormat = d.FileFormate,
                    version = d.FileVersion,
                    FileName = d.Document_name,
                    JEPCustFeedbackDocumentId = d.DCRSupportedDocId.ToString(),
                    ProjectNumber = d.Project
                }).ToList();

                foreach (DocumentModel documentModel in _des060s_DCR_UV_Search)
                {
                    //Console.WriteLine("Search file process start: " + documentModel.MainDocumentPath);
                    string result = TestSeachFileGenearte(documentModel.MainDocumentPath);
                    //Console.WriteLine("Output:" + result);
                }
                Console.WriteLine("DCR UV Search file creation completed.");
                #endregion

                //Create JEP customer feedback UV Search file
                #region Create JEP customer feedbackUV Search file
                Console.WriteLine("JEP customer feedback UV Search file creation started.");
                _des060s_JEP_Cust_Feedback_UV_Search = ObjsolrDocumentIndexService.GetNonIndexCustomerFeedbackFiles().Select(d => new DocumentModel
                {
                    ObjectId = d.ObjectId,
                    Customer = d.Customer,
                    BU = d.BU,
                    Department = d.Department,
                    Type = d.Type,
                    Status = d.Status,
                    Name = d.Name,
                    Revision = d.Revision,
                    Originator = d.Originator,
                    OwnerName = d.OwnerName,
                    MainDocumentPath = d.MainDocumentPath,
                    Descripion = d.Descripion,
                    FileFormat = d.FileFormate,
                    version = d.FileVersion,
                    FileName = d.Document_name,
                    JEPCustFeedbackDocumentId = d.JEPCustFeedbackDocumentId.ToString(),
                    ProjectNumber = d.Project
                }).ToList();
                foreach (DocumentModel documentModel in _des060s_JEP_Cust_Feedback_UV_Search)
                {
                    //Console.WriteLine("Search file process start: " + documentModel.MainDocumentPath);
                    string result = TestSeachFileGenearte(documentModel.MainDocumentPath);
                    //Console.WriteLine("Output:" + result);
                }
                Console.WriteLine("JEP customer feedback UV Search file creation completed.");
                #endregion

                //Create ROC UV Search file
                #region Create ROC UV Search file
                Console.WriteLine("ROC UV Search file creation started.");
                _des060s_ROC_UV_Search = ObjsolrDocumentIndexService.GetNonIndex_ROC_Attached_Files().Select(d => new DocumentModel
                {
                    ObjectId = d.ObjectId,
                    Customer = d.Customer,
                    BU = d.BU,
                    Department = d.Department,
                    Type = d.Type,
                    Status = d.Status,
                    Name = d.Name,
                    Revision = d.Revision,
                    Originator = d.Originator,
                    OwnerName = d.OwnerName,
                    MainDocumentPath = d.MainDocumentPath,
                    Descripion = d.Descripion,
                    FileFormat = d.FileFormate,
                    version = d.FileVersion,
                    FileName = d.Document_name,
                    JEPCustFeedbackDocumentId = d.ROCCommentsAttachId.ToString(),
                    ProjectNumber = d.Project
                }).ToList();
                foreach (DocumentModel documentModel in _des060s_ROC_UV_Search)
                {
                    //Console.WriteLine("Search file process start: " + documentModel.MainDocumentPath);
                    string result = TestSeachFileGenearte(documentModel.MainDocumentPath);
                    //Console.WriteLine("Output:" + result);
                }
                Console.WriteLine("ROC UV Search file creation completed.");
                #endregion

            }
            catch (System.Exception ex)
            {
                Console.WriteLine("There is error: " + ex.Message);
                //Console.ReadLine();

                throw ex;
            }
        }

        private static void GetTextFromFile(CADImage vDrawing, StringBuilder S)
        {
            for (int i = 0; i < vDrawing.CurrentLayout.Count; i++)
            {
                if (vDrawing.CurrentLayout.Entities[i].EntType == EntityType.Text)
                {
                    CADText vText = (CADText)vDrawing.CurrentLayout.Entities[i];
                    //MessageBox.Show("Text: " + vText.Text);
                    if (vText.Text != "" && vText.Text != " ")
                    {
                        S.Append(vText.Text + " ");
                    }
                    //S.AppendLine();
                    //S += vText.Text;
                }
                if (vDrawing.CurrentLayout.Entities[i].EntType == EntityType.MText)
                {
                    CADMText vMText = (CADMText)vDrawing.CurrentLayout.Entities[i];
                    for (int j = 0; j < vMText.Block.Count; j++)
                    {
                        S.Append(((CADText)vMText.Block.Entities[j]).Text + " ");
                        //S.AppendLine();
                        //S = S + ((CADText)vMText.Block.Entities[j]).Text + "\n";
                    }
                    // MessageBox.Show("MText:\n" + S);
                    // S = "";
                }
            }
        }
        public static System.Data.DataTable xlsxToDT(string fileName)
        {
            System.Data.DataTable table = new System.Data.DataTable();
            XSSFWorkbook workbook = new XSSFWorkbook(new FileStream(fileName, FileMode.Open, FileAccess.Read));
            ISheet sheet = workbook.GetSheetAt(0);
            IRow headerRow = sheet.GetRow(0);
            int cellCount = headerRow.LastCellNum;
            for (int i = headerRow.FirstCellNum; i < cellCount; i++)
            {
                System.Data.DataColumn column = new System.Data.DataColumn(headerRow.GetCell(i).StringCellValue);
                table.Columns.Add(column);
            }
            int rowCount = sheet.LastRowNum;
            for (int i = (sheet.FirstRowNum + 1); i <= sheet.LastRowNum; i++)
            {
                IRow row = sheet.GetRow(i);
                DataRow dataRow = table.NewRow();
                for (int j = row.FirstCellNum; j < cellCount; j++)
                {
                    if (j >= 0)
                    {
                        if (row.GetCell(j) != null)
                        {
                            //EXCEPTION GENERATING IN THIS CODE
                            //row.GetCell(j).SetCellType(CellType.STRING);
                            //dataRow[j] = row.GetCell(j).ToString();
                            dataRow[j] = row.GetCell(j).ToString();
                            ////////////////////////////
                        }
                    }
                }
                table.Rows.Add(dataRow);
            }
            workbook = null;
            sheet = null;
            return table;
        }

        public static System.Data.DataTable xlsToDT(string fileName)
        {
            System.Data.DataTable table = new System.Data.DataTable();
            //xss
            XSSFWorkbook workbook = new XSSFWorkbook(new FileStream(fileName, FileMode.Open, FileAccess.Read));
            ISheet sheet = workbook.GetSheetAt(0);
            IRow headerRow = sheet.GetRow(0);
            int cellCount = headerRow.LastCellNum;
            for (int i = headerRow.FirstCellNum; i < cellCount; i++)
            {
                System.Data.DataColumn column = new System.Data.DataColumn(headerRow.GetCell(i).StringCellValue);
                table.Columns.Add(column);
            }
            int rowCount = sheet.LastRowNum;
            for (int i = (sheet.FirstRowNum + 1); i <= sheet.LastRowNum; i++)
            {
                IRow row = sheet.GetRow(i);
                if (row == null)
                {
                    continue;
                }
                DataRow dataRow = table.NewRow();
                for (int j = row.FirstCellNum; j < cellCount; j++)
                {
                    if (j >= 0)
                    {
                        if (row.GetCell(j) != null)
                        {
                            //EXCEPTION GENERATING IN THIS CODE
                            //row.GetCell(j).SetCellType(CellType.STRING);
                            //dataRow[j] = row.GetCell(j).ToString();
                            dataRow[j] = row.GetCell(j).ToString();
                            ////////////////////////////
                        }
                    }
                }
                table.Rows.Add(dataRow);
            }
            workbook = null;
            sheet = null;
            return table;
        }

        private static string GetSearchParameterFormat(string column, string SearchParameter)
        {
            if (!string.IsNullOrEmpty(SearchParameter))
            {
                string[] temp = SearchParameter.Split(' ');
                if (temp.Length > 0)
                {
                    SearchParameter = "(" + column;
                    foreach (string worditerate in temp)
                    {
                        if (!string.IsNullOrEmpty(worditerate) && worditerate != "-")
                            SearchParameter += worditerate + "*" + " OR ";
                    }
                    SearchParameter = SearchParameter.Substring(0, SearchParameter.LastIndexOf("OR")).Trim();
                    SearchParameter += ")";
                }
            }
            return SearchParameter + " AND ";
        }
        public static string TestSeachFileGenearte(string DocPath)
        {
            string strResult = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(DocPath))
                {
                    //var srhFile = Server.MapPath($"~/SrhConverter/Search_Out/{filename}.dcn.srh");
                    var srhFCSFilePath = System.IO.Path.GetDirectoryName(DocPath) + "\\" + System.IO.Path.GetFileName(DocPath) + ".dcn.srh";
                    if (!System.IO.File.Exists(srhFCSFilePath))
                    {
                        //DocPath = "http://10.7.66.83:100/Home/FileGenerate?DocPath=//10.7.66.185/FCSFiles/S010308A-Doc5566-R0/1579157028941_Document%20Object.xlsx";
                        string URL = "http://10.7.66.83:100/Home/FileGenerate?DocPath=" + DocPath;
                        System.Net.ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                        WebRequest request = HttpWebRequest.Create(URL);
                        //WebResponse response = request.GetResponse();
                        using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                        {
                            StreamReader reader = new StreamReader(response.GetResponseStream());
                            strResult = reader.ReadToEnd(); // it takes the response from your url. now you can use as your need  

                        }
                    }
                    else
                    {
                        strResult = "Search file already exists.!";
                    }
                }
                else
                {
                    strResult = "File path is empty.! ";
                }
                return strResult;
            }
            catch (System.Exception ex)
            {
                return ex.Message;
            }
        }
    }
}
