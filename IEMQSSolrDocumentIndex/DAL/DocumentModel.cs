﻿using CADImport;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using SolrNet.Attributes;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tesseract;

namespace IEMQSSolrDocumentIndex.DAL
{
    public class DocumentModel
    {
        #region Members

        private string documentText;

        private string cntserach;

        #endregion

        //[SolrUniqueKey("id")]
        //public long DocumentMappingId { get; internal set; }

        //[SolrField("filepath")]
        //public string MainDocumentPath { get; internal set; }

        //[SolrField("doctext")]
        //public string DocumentText
        //{
        //    get
        //    {
        //        if (this.documentText == null)
        //        {
        //            this.documentText = File.ReadAllText(MainDocumentPath);
        //        }
        //        return this.documentText;
        //    }

        //}

        //[SolrField("contentsearch")]
        //public string ContentSearch
        //{
        //    get
        //    {
        //        if (this.cntserach == null)
        //        {
        //            this.cntserach = File.ReadAllText(MainDocumentPath);
        //        }
        //        return this.cntserach;
        //    }

        //}

        [SolrUniqueKey("id")]
        public string ObjectId { get; internal set; }


        [SolrField("Customer")]
        public string Customer { get; internal set; }

        [SolrField("BU")]
        public string BU { get; internal set; }

        [SolrField("Department")]
        public string Department { get; internal set; }

        [SolrField("Type")]
        public string Type { get; internal set; }

        [SolrField("Status")]
        public string Status { get; internal set; }

        [SolrField("Name")]
        public string Name { get; internal set; }

        [SolrField("Revision")]
        public string Revision { get; internal set; }

        [SolrField("Originator")]
        public string Originator { get; internal set; }

        [SolrField("OwnerName")]
        public string OwnerName { get; internal set; }

        [SolrField("filepath")]
        public string MainDocumentPath { get; internal set; }

        [SolrField("FileExtension")]
        public string FileFormat { get; internal set; }

        [SolrField("Descripion")]
        public string Descripion { get; internal set; }

        [SolrField("version")]
        public string version { get; internal set; }

        [SolrField("FileName")]
        public string FileName { get; internal set; }

        [SolrField("ProjectNumber")]
        public string ProjectNumber { get; internal set; }

        [SolrField("SubObject")]
        public string SubObject { get; internal set; }

        public string DocumentMappingId { get; internal set; }
        public string JEPCustFeedbackDocumentId { get; internal set; }
        public Int64 RowNo { get; set; }

        [SolrField("contentsearch")]
        public string ContentSearch
        {
            get
            {
                if (this.cntserach == null)
                {
                    if (!string.IsNullOrEmpty(MainDocumentPath))
                        this.cntserach = GetFileContent(MainDocumentPath, FileFormat); //File.ReadAllText(MainDocumentPath);
                }
                return this.cntserach;
            }

        }

        private string GetFileContent(string mainDocumentPath, string FileFormat)
        {
            try
            {
                List<string> fileformates = new List<string> { "html", "HTML", "json", "txt", "csv", "CSV", "xml", "XML", "MHTML", "mhtml", "mht", "MHT", "htm", "HTM", "WEB", "web" };
                List<string> AutocadFormats = new List<string> { "dwg", "DWG", "DXF", "dxf", "STL", "stl", "CGM", "cgm" };
                List<string> ImageFormats = new List<string> { "png", "PNG", "jpg", "JPG", "bmp", "BMP", "tif", "TIF", "tiff", "TIFF" };
                if (fileformates.Contains(FileFormat))
                {
                    return File.ReadAllText(MainDocumentPath);
                }
                else if (FileFormat == "PDF" || FileFormat == "pdf")
                {
                    StringBuilder text = new StringBuilder();
                    using (PdfReader reader = new PdfReader(mainDocumentPath))
                    {
                        for (int i = 1; i <= reader.NumberOfPages; i++)
                        {
                            text.Append(PdfTextExtractor.GetTextFromPage(reader, i));
                        }
                    }

                    return text.ToString();
                }
                else if (FileFormat.ToLower() == "docx" || FileFormat.ToLower() == "doc" || FileFormat.ToLower() == "dot" || FileFormat.ToLower() == "DOT")
                {
                    try
                    {
                        // If using Professional version, put your serial key below.
                        GemBox.Document.ComponentInfo.SetLicense("FREE-LIMITED-KEY");

                        GemBox.Document.ComponentInfo.FreeLimitReached += (sender, e) => e.FreeLimitReachedAction = GemBox.Document.FreeLimitReachedAction.ContinueAsTrial;
                        //Load Word document from file's path.
                        var document = GemBox.Document.DocumentModel.Load(mainDocumentPath);



                        //Get Word document's plain text.
                        string text = document.Content.ToString();
                        return text;
                    }
                    catch (Exception ex)
                    {
                        return "";
                    }
                    return "";
                }
                else if (FileFormat.ToLower() == "xlsx" || FileFormat.ToLower() == "xls" || FileFormat.ToLower() == "xlsm")
                {
                    // If using Professional version, put your serial key below.
                    GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("FREE-LIMITED-KEY");
                    GemBox.Spreadsheet.SpreadsheetInfo.FreeLimitReached += (sender, e) => e.FreeLimitReachedAction = GemBox.Spreadsheet.FreeLimitReachedAction.ContinueAsTrial;
                    var workbook = GemBox.Spreadsheet.ExcelFile.Load(mainDocumentPath);

                    StringBuilder filetext = new StringBuilder();

                    // Iterate through all worksheets in an Excel workbook.
                    foreach (var worksheet in workbook.Worksheets)
                    {
                        filetext.AppendLine();
                        filetext.AppendFormat("{0} {1} {0}", new string('-', 25), worksheet.Name);

                        // Iterate through all rows in an Excel worksheet.
                        foreach (var row in worksheet.Rows)
                        {
                            filetext.AppendLine();

                            // Iterate through all allocated cells in an Excel row.
                            foreach (var cell in row.AllocatedCells)
                                if (cell.ValueType != GemBox.Spreadsheet.CellValueType.Null)
                                    filetext.Append(string.Format("{0}", cell.Value).PadRight(25));
                                else
                                    filetext.Append(new string(' ', 25));
                        }
                    }
                    return filetext.ToString();
                    //StringBuilder text = new StringBuilder();
                    //DataTable dt = new DataTable();
                    //if (FileFormat.ToLower() == "xlsx")
                    //{ 
                    //dt = xlsxToDT(mainDocumentPath);
                    //}
                    //else if (FileFormat.ToLower() == "xls")
                    //{
                    //    dt = xlsToDT(mainDocumentPath);
                    //}
                    //for (int i = 0; i <= dt.Rows.Count - 1; i++)
                    //{
                    //    for (int j = 0; j <= dt.Columns.Count - 1; j++)
                    //    {
                    //        text.Append(" \r\n "+dt.Rows[i][j].ToString());
                    //        //xlWorkSheet.Cells[i + 1, j + 1] = cell;
                    //    }
                    //}
                    //return  text.ToString();

                }
                else if (AutocadFormats.Contains(FileFormat))
                {
                    if (FileFormat.ToLower() == "dwg" || FileFormat.ToLower() == "dxf")
                    {
                        CADImage vDrawing = CADImage.CreateImageByExtension(mainDocumentPath);
                        vDrawing.LoadFromFile(mainDocumentPath);
                        StringBuilder S = new StringBuilder();
                        GetTextFromFile(vDrawing, S);
                        return S.ToString();
                    }
                    else if (FileFormat.ToLower() == "stl")
                    {
                        CADImport.CAD3D.STLImage vDrawing = new CADImport.CAD3D.STLImage();
                        vDrawing.LoadFromFile(mainDocumentPath);
                        StringBuilder S = new StringBuilder();
                        GetTextFromFile(vDrawing, S);
                        return S.ToString();
                    }
                    else if (FileFormat.ToLower() == "cgm")
                    {
                        CADImport.CGM.CGMImage vDrawing = new CADImport.CGM.CGMImage();
                        vDrawing.LoadFromFile(mainDocumentPath);
                        StringBuilder S = new StringBuilder();
                        GetTextFromFile(vDrawing, S);
                        return S.ToString();
                    }
                }
                else if (ImageFormats.Contains(FileFormat))
                {
                    var dataPath = @"\\10.7.66.185\TesseractLNG\tessdata";

                    using (var tEngine = new TesseractEngine(dataPath, "eng", EngineMode.Default)) //creating the tesseract OCR engine with English as the language
                    {
                        using (var img = Pix.LoadFromFile(mainDocumentPath)) // Load of the image file from the Pix object which is a wrapper for Leptonica PIX structure
                        {
                            using (var page = tEngine.Process(img)) //process the specified image
                            {
                                var text = page.GetText(); //Gets the image's content as plain text.
                                return text;
                                //Console.WriteLine(text); //display the text
                                //Console.WriteLine(page.GetMeanConfidence()); //Get's the mean confidence that as a percentage of the recognized text.
                                //Console.ReadKey();
                            }
                        }
                    }

                }
                else if (FileFormat.ToLower() == "pptx" || FileFormat.ToLower() == "ppt")
                {
                    StringBuilder S = new StringBuilder();
                    GemBox.Presentation.ComponentInfo.SetLicense("FREE-LIMITED-KEY");
                    GemBox.Presentation.ComponentInfo.FreeLimitReached += (sender, e) => e.FreeLimitReachedAction = GemBox.Presentation.FreeLimitReachedAction.ContinueAsTrial;
                    var presentation = GemBox.Presentation.PresentationDocument.Load(mainDocumentPath);

                    var sb = new StringBuilder();

                    foreach (var slides in presentation.Slides)
                    {
                        S.Append(slides.TextContent + " ");
                    }
                    return S.ToString();
                }
                else if (FileFormat.ToLower() == "odt" || FileFormat.ToLower() == "ODT")
                {
                    StringBuilder S = new StringBuilder();

                    Independentsoft.Office.Odf.TextDocument doc = new Independentsoft.Office.Odf.TextDocument(mainDocumentPath);

                    IList<Independentsoft.Office.Odf.IContentElement> elements = doc.GetContentElements();
                    foreach (Independentsoft.Office.Odf.IContentElement element in elements)
                    {
                        try
                        {
                            S.Append(element + " ");
                            S.AppendLine();
                        }
                        catch (System.Exception ex)
                        {
                            continue;
                        }

                    }
                    return S.ToString();
                }
                else if(FileFormat.ToLower()=="XPS" || FileFormat.ToLower()=="xps")
                {
                    var xpsDocument = new System.Windows.Xps.Packaging.XpsDocument(mainDocumentPath, FileAccess.Read);
                    var fixedDocSeqReader = xpsDocument.FixedDocumentSequenceReader;
                    if (fixedDocSeqReader == null)
                    {
                        return "";
                        //return null;
                    }

                    const string UnicodeString = "UnicodeString";
                    const string GlyphsString = "Glyphs";

                    var textLists = new List<List<string>>();
                    foreach (System.Windows.Xps.Packaging.IXpsFixedDocumentReader fixedDocumentReader in fixedDocSeqReader.FixedDocuments)
                    {
                        foreach (System.Windows.Xps.Packaging.IXpsFixedPageReader pageReader in fixedDocumentReader.FixedPages)
                        {
                            var pageContentReader = pageReader.XmlReader;
                            if (pageContentReader == null)
                                continue;

                            var texts = new List<string>();
                            while (pageContentReader.Read())
                            {
                                if (pageContentReader.Name != GlyphsString)
                                    continue;
                                if (!pageContentReader.HasAttributes)
                                    continue;
                                if (pageContentReader.GetAttribute(UnicodeString) != null)
                                    texts.Add(pageContentReader.GetAttribute(UnicodeString));
                            }
                            textLists.Add(texts);
                        }
                    }
                    xpsDocument.Close();
                    //string _fullPageText = textLists.ToString();
                    StringBuilder filetext = new StringBuilder();
                    //int pageIdx = 0;
                    foreach (List<string> txtList in textLists)
                    {
                        //pageIdx++;
                        //Console.WriteLine("== Page {0} ==", pageIdx);
                        foreach (string txt in txtList)
                            filetext.Append(" " + txt);
                        //Console.WriteLine(" " + txt);
                        //Console.WriteLine();
                    }
                    return filetext.ToString();
                }
                return "";
            }
            catch (Exception ex)
            {
                Console.WriteLine("Maindocument Path :" + mainDocumentPath);
                Console.WriteLine("Maindocument Path Error ::" + ex.Message.ToString());
                //Console.ReadLine();
                return "";
            }
        }

        private static void GetTextFromFile(CADImage vDrawing, StringBuilder S)
        {
            for (int i = 0; i < vDrawing.CurrentLayout.Count; i++)
            {
                if (vDrawing.CurrentLayout.Entities[i].EntType == EntityType.Text)
                {
                    CADText vText = (CADText)vDrawing.CurrentLayout.Entities[i];
                    //MessageBox.Show("Text: " + vText.Text);
                    if (vText.Text != "" && vText.Text != " ")
                    {
                        S.Append(vText.Text + " ");
                    }
                    //S.AppendLine();
                    //S += vText.Text;
                }
                if (vDrawing.CurrentLayout.Entities[i].EntType == EntityType.MText)
                {
                    CADMText vMText = (CADMText)vDrawing.CurrentLayout.Entities[i];
                    for (int j = 0; j < vMText.Block.Count; j++)
                    {
                        S.Append(((CADText)vMText.Block.Entities[j]).Text + " ");
                        //S.AppendLine();
                        //S = S + ((CADText)vMText.Block.Entities[j]).Text + "\n";
                    }
                    // MessageBox.Show("MText:\n" + S);
                    // S = "";
                }
            }
        }

        public static DataTable xlsxToDT(string fileName)
        {
            DataTable table = new DataTable();
            XSSFWorkbook workbook = new XSSFWorkbook(new FileStream(fileName, FileMode.Open, FileAccess.Read));
            ISheet sheet = workbook.GetSheetAt(0);
            IRow headerRow = sheet.GetRow(0);
            int cellCount = headerRow.LastCellNum;
            for (int i = headerRow.FirstCellNum; i < cellCount; i++)
            {
                DataColumn column = new DataColumn(headerRow.GetCell(i).StringCellValue);
                table.Columns.Add(column);
            }
            int rowCount = sheet.LastRowNum;
            for (int i = (sheet.FirstRowNum + 1); i <= sheet.LastRowNum; i++)
            {
                IRow row = sheet.GetRow(i);
                DataRow dataRow = table.NewRow();
                for (int j = row.FirstCellNum; j < cellCount; j++)
                {
                    if (j >= 0)
                    {
                        if (row.GetCell(j) != null)
                        {
                            //EXCEPTION GENERATING IN THIS CODE
                            //row.GetCell(j).SetCellType(CellType.STRING);
                            //dataRow[j] = row.GetCell(j).ToString();
                            dataRow[j] = row.GetCell(j).ToString();
                            ////////////////////////////
                        }
                    }
                }
                table.Rows.Add(dataRow);
            }
            workbook = null;
            sheet = null;
            return table;
        }

        public static DataTable xlsToDT(string fileName)
        {
            DataTable table = new DataTable();
            HSSFWorkbook workbook = new HSSFWorkbook(new FileStream(fileName, FileMode.Open, FileAccess.Read));
            ISheet sheet = workbook.GetSheetAt(0);
            IRow headerRow = sheet.GetRow(0);
            int cellCount = headerRow.LastCellNum;
            for (int i = headerRow.FirstCellNum; i < cellCount; i++)
            {
                DataColumn column = new DataColumn(headerRow.GetCell(i).StringCellValue);
                table.Columns.Add(column);
            }
            int rowCount = sheet.LastRowNum;
            for (int i = (sheet.FirstRowNum + 1); i <= sheet.LastRowNum; i++)
            {
                IRow row = sheet.GetRow(i);
                if (row == null)
                {
                    continue;
                }
                DataRow dataRow = table.NewRow();
                for (int j = row.FirstCellNum; j < cellCount; j++)
                {
                    if (j >= 0)
                    {
                        if (row.GetCell(j) != null)
                        {
                            //EXCEPTION GENERATING IN THIS CODE
                            //row.GetCell(j).SetCellType(CellType.STRING);
                            //dataRow[j] = row.GetCell(j).ToString();
                            dataRow[j] = row.GetCell(j).ToString();
                            ////////////////////////////
                        }
                    }
                }
                table.Rows.Add(dataRow);
            }
            workbook = null;
            sheet = null;
            return table;
        }
    }

    public class SearchModel
    {
        [SolrField("id")]
        public string ObjectId { get; set; }
    }
}
