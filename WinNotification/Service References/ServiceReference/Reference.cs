﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WinNotification.ServiceReference {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="ServiceReference.IIEMQSService")]
    public interface IIEMQSService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IIEMQSService/DoWork", ReplyAction="http://tempuri.org/IIEMQSService/DoWorkResponse")]
        string DoWork();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IIEMQSService/DoWork", ReplyAction="http://tempuri.org/IIEMQSService/DoWorkResponse")]
        System.Threading.Tasks.Task<string> DoWorkAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IIEMQSService/GetWinNotification", ReplyAction="http://tempuri.org/IIEMQSService/GetWinNotificationResponse")]
        string GetWinNotification(string psno);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IIEMQSService/GetWinNotification", ReplyAction="http://tempuri.org/IIEMQSService/GetWinNotificationResponse")]
        System.Threading.Tasks.Task<string> GetWinNotificationAsync(string psno);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IIEMQSServiceChannel : WinNotification.ServiceReference.IIEMQSService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class IEMQSServiceClient : System.ServiceModel.ClientBase<WinNotification.ServiceReference.IIEMQSService>, WinNotification.ServiceReference.IIEMQSService {
        
        public IEMQSServiceClient() {
        }
        
        public IEMQSServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public IEMQSServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public IEMQSServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public IEMQSServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public string DoWork() {
            return base.Channel.DoWork();
        }
        
        public System.Threading.Tasks.Task<string> DoWorkAsync() {
            return base.Channel.DoWorkAsync();
        }
        
        public string GetWinNotification(string psno) {
            return base.Channel.GetWinNotification(psno);
        }
        
        public System.Threading.Tasks.Task<string> GetWinNotificationAsync(string psno) {
            return base.Channel.GetWinNotificationAsync(psno);
        }
    }
}
