﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Security;
using System.Security.Cryptography;
using System.Configuration;

using System.Diagnostics;
using Org.BouncyCastle.X509;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.Pkcs;
using iTextSharp.text.pdf.security;
using iTextSharp.text.pdf;
using System.IO;
using iTextSharp.text;
using System.Security.Cryptography.X509Certificates;
using System.Net;

using System.Security.Principal;
using System.Runtime.InteropServices;
using System.Net.Mail;
using com.lnthed.utility;
using com.lnthed.utility.ODMWEB;
using com.lnthed.utility.ODEWEBLIST;
using System.Xml;
namespace DigiSign_FormsAppl
{
    public partial class Form1 : Form
    {
        string FileName = string.Empty;

        com.lnthed.utility.ODMWEB.Copy copyService = new com.lnthed.utility.ODMWEB.Copy();
        DateTime dt = DateTime.Now;

        public Form1()
        {
            InitializeComponent();
        }


        public PdfStamper pdfStamper { get; set; }

        private string signPDF(string _filepath)
        {
            string new_FileName = Path.GetFileNameWithoutExtension(_filepath).ToString() + ".pdf";
            //string new_Path = Path.GetDirectoryName(_filepath).ToString() + @"\" + new_FileName;
            string new_Path = ConfigurationManager.AppSettings["fileout"].ToString() + new_FileName;
            string pass = ConfigurationManager.AppSettings["certPin"].ToString();
            string location = ConfigurationManager.AppSettings["certLocation"].ToString();
            string reason = ConfigurationManager.AppSettings["certReason"].ToString();
            string certname = ConfigurationManager.AppSettings["certName"].ToString();
            certname = certname.Replace('\'', '"');
            //SecureString password = DecryptString(val);
            //string pass = ToInsecureString(password);
            try
            {

                //flag=false;
                //X509Store store = new X509Store(StoreName.My, StoreLocation.CurrentUser);
                //store.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);

                X509Certificate2 cert = GetCertificateFromStore(certname);

                X509Certificate2Extension.SetPinForPrivateKey(cert, ConfigurationManager.AppSettings["certPin"].ToString());
                X509CertificateParser cp = new X509CertificateParser();
                Org.BouncyCastle.X509.X509Certificate[] chain = new Org.BouncyCastle.X509.X509Certificate[] {
                    cp.ReadCertificate(cert.RawData)};
                IExternalSignature externalSignature = new X509Certificate2Signature(cert, "SHA-1");

                PdfReader pdfReader = new PdfReader(_filepath);
                Stream signedPdf;

                if (File.Exists(new_Path))
                    File.Delete(new_Path);

                signedPdf = new System.IO.FileStream(new_Path, FileMode.Create);
                pdfStamper = PdfStamper.CreateSignature(pdfReader, signedPdf, '\0');
                PdfSignatureAppearance signatureAppearance = pdfStamper.SignatureAppearance;
                //new iTextSharp.text.Rectangle(50, 405, 225, 475)
                signatureAppearance.Reason = reason;
                signatureAppearance.Location = location;
                signatureAppearance.Contact = cert.GetNameInfo(X509NameType.SimpleName, false);
                // signatureAppearance.SignatureGraphic = iTextSharp.text.Image.GetInstance(pathToSignatureImage);

                //pdfStamper.Writer.SetPdfVersion(PdfWriter.PDF_VERSION_1_4);
                //pdfStamper.Writer.PDFXConformance = PdfWriter.PDFA1A;
                //pdfStamper.Writer.PDFXConformance = PdfWriter.ALLOW_MODIFY_CONTENTS;
                iTextSharp.text.Rectangle cropBox = pdfReader.GetCropBox(1);
                string a = ConfigurationManager.AppSettings["A"].ToString();
                string b = ConfigurationManager.AppSettings["B"].ToString();
                string c = ConfigurationManager.AppSettings["C"].ToString();
                string d = ConfigurationManager.AppSettings["D"].ToString();

                signatureAppearance.SetVisibleSignature(new iTextSharp.text.Rectangle(cropBox.GetLeft(float.Parse(a)), cropBox.GetBottom(float.Parse(b)), cropBox.GetLeft(float.Parse(c)), cropBox.GetBottom(float.Parse(d))), 1, "Signature_" + Convert.ToString(DateTime.Now.Ticks));
                signatureAppearance.SignatureRenderingMode = PdfSignatureAppearance.RenderingMode.DESCRIPTION;

                MakeSignature.SignDetached(signatureAppearance, externalSignature, chain, null, null, null, 0, CryptoStandard.CMS);
                //UploadFile(new_Path,new_FileName);
                WriteToFile(new_FileName + " file has been Successfully Signed.");
                toolStripStatusLabel1.Text = "Document Signing is started..";
                statusStrip1.Refresh();

            }
            catch (Exception ex)
            {
                WriteToFile("Error while Signing the Document." + ex.Message);
                toolStripStatusLabel1.Text = "Error Occurred while Signing the document...Time: " + dt.ToString();
                statusStrip1.Refresh();
                label2.Text = "Error Occurred while Siginig the document...Time: " + dt.ToString();
                //SendMail("nilesh.panchal@larsentoubro.com", "nilesh.panchal@larsentoubro.com", "Error In DigiSign System", "There is an Error Occured in Digital Sigining Process.Please Resolve the Issue ASAP.Thank you"+ ex.Message);
                // CreateLog ("signPDF", ex.Message) ;
                string errorfile = ConfigurationManager.AppSettings["error"].ToString() + Path.GetFileNameWithoutExtension(_filepath).ToString() + ".pdf";
                new_Path = _filepath;
                if (File.Exists(errorfile))
                {
                    File.Delete(errorfile);
                }
                else
                {
                    File.Move(_filepath, errorfile);
                }
            }
            finally
            {
                if (File.Exists(_filepath))
                    File.Delete(_filepath);
            }
            return new_Path;
        }

        private static X509Certificate2 GetCertificateFromStore(string certName)
        {

            // Get the certificate store for the current user.
            X509Store store = new X509Store(StoreName.My, StoreLocation.CurrentUser);
            try
            {
                store.Open(OpenFlags.ReadOnly);

                // Place all certificates in an X509Certificate2Collection object.
                X509Certificate2Collection certCollection = store.Certificates;
                // If using a certificate with a trusted root you do not need to FindByTimeValid, instead: 
                // currentCerts.Find(X509FindType.FindBySubjectDistinguishedName, certName, true);
                X509Certificate2Collection currentCerts = certCollection.Find(X509FindType.FindByTimeValid, DateTime.Now, false);
                //X509Certificate2Collection signingCert = currentCerts.Find(X509FindType.FindBySubjectDistinguishedName, certName, false);
                X509Certificate2Collection signingCert = currentCerts.Find(X509FindType.FindBySubjectDistinguishedName, certName, false);
                if (signingCert.Count == 0)
                    return null;
                // Return the first certificate in the collection, has the right name and is current. 
                return signingCert[0];
            }
            finally
            {
                store.Close();
            }
        }

        private void UploadFile(string signedPdf, string fileName)
        {
            string username = string.Empty;
            string pwd = string.Empty;
            string domain = string.Empty;
            string DocNo = "0";
            string rootFolder = string.Empty;
            string folder = string.Empty;
            string subFolder = string.Empty;
            string library = string.Empty;
            string signoutput = string.Empty;
            try
            {
                string tmpFileName = Path.GetFileNameWithoutExtension(fileName).ToString();
                rootFolder = ConfigurationManager.AppSettings["DestinationPath"].ToString();
                folder = ConfigurationManager.AppSettings["Folder"].ToString();
                string[] tmparr = tmpFileName.Split(Convert.ToChar("_"));
                subFolder = "@" + tmparr[0] + "@" + tmparr[1];
                //string postatus = tmparr[2];
                //postatus == "0"
                if (false)
                {
                    //PO is draft hence no digital signature required
                    //WriteToFile("PO Status Found:" + postatus + " PO is Draft hence Digital Signature is Not Required ");
                }
                else
                {
                    //PO is approved hence call digital signature function
                    //WriteToFile("PO Status Found:" + postatus + " PO is Approved hence Digital Signature is Required ");

                    fileName = signPDF(fileName);


                    library = ConfigurationManager.AppSettings["library"].ToString();
                    UpdateListItemCreateFolder(rootFolder + "/" + folder, subFolder, library);
                    string filename = Path.GetFileNameWithoutExtension(fileName).ToString();
                    filename = filename.Substring(0, (filename.Length - 2));
                    string[] destinationUrl = { rootFolder + "/" + folder + "/" + subFolder + "/" + filename + ".pdf" };
                    WriteToFile("Destination URL is:" + destinationUrl[0]);
                    Byte[] docBytes = File.ReadAllBytes(fileName);
                    try
                    {
                        username = ConfigurationManager.AppSettings["SPUserName"].ToString();
                        pwd = ConfigurationManager.AppSettings["SPPassword"].ToString();
                        domain = ConfigurationManager.AppSettings["SPDomain"].ToString();
                    }
                    catch (Exception ex)
                    {
                        WriteToFile("Fetching Credentials from App.config is failed Exception:" + ex.Message);
                    }
                    copyService.Credentials = new System.Net.NetworkCredential(username, pwd, domain);
                    string[] output = new string[2];
                    //Create File in Sub Folder  --SP web service
                    if (!username.Equals("") && !pwd.Equals("") && !domain.Equals(""))
                    {
                        com.lnthed.utility.ODMWEB.CopyResult cResult1 = new com.lnthed.utility.ODMWEB.CopyResult();
                        com.lnthed.utility.ODMWEB.CopyResult cResult2 = new com.lnthed.utility.ODMWEB.CopyResult();
                        com.lnthed.utility.ODMWEB.CopyResult[] cResultArray = { cResult1, cResult2 };
                        com.lnthed.utility.ODMWEB.FieldInformation fFiledInfo = new com.lnthed.utility.ODMWEB.FieldInformation();

                        fFiledInfo.DisplayName = DocNo;
                        fFiledInfo.Type = com.lnthed.utility.ODMWEB.FieldType.Text;
                        fFiledInfo.Value = DocNo;
                        com.lnthed.utility.ODMWEB.FieldInformation[] fFiledInfoArray = { fFiledInfo };

                        uint copyresult = copyService.CopyIntoItems("dummy", destinationUrl, fFiledInfoArray, docBytes, out cResultArray);

                        if (File.Exists(fileName))
                            File.Delete(fileName);
                        //Derive SP web service output. ErrorCode and ErrorDescription
                        output[0] = cResultArray[0].ErrorCode.ToString();
                        output[1] = cResultArray[0].ErrorCode.ToString() == "Success" ? "" : cResultArray[0].ErrorMessage.ToString();
                        if (output[0].Equals("Success"))
                        {
                            WriteToFile(filename + " is Successfully Uploaded to the Destination Url");
                        }
                        else
                        { WriteToFile("Error Occured while Uploading. Error: " + output[0] + " And Error Description: " + output[1]); }
                    }
                    else
                    {
                        output[0] = "Error";
                        output[1] = "Blank Credentials";
                        WriteToFile("Inside UploadFile Method. Credentials are blank.");
                    }
                }
            }
            catch (Exception ex)
            {
                WriteToFile("Exception Occured.Location: UploadFile. " + ex.Message);
            }
        }

        public static void UpdateListItemCreateFolder(string rootfolder, string folderName, string docLibrary)
        {

            try
            {
                //SP Web service:  http://vpwtsheion/_vti_bin/lists.asmx      
                com.lnthed.utility.ODEWEBLIST.Lists listService = new com.lnthed.utility.ODEWEBLIST.Lists();
                string tempurl = listService.Url.ToString();
                string username = string.Empty;
                string pwd = string.Empty;
                string domain = string.Empty;
                try
                {
                    username = ConfigurationManager.AppSettings["UserName"].ToString();
                    pwd = ConfigurationManager.AppSettings["Password"].ToString();
                    domain = ConfigurationManager.AppSettings["Domain"].ToString();
                }
                catch (Exception ex)
                {
                    WriteToFile("Fetching Credentials from App.config is failed. Exception: " + ex.Message);
                }
                listService.Credentials = new System.Net.NetworkCredential(username, pwd, domain);

                if (!username.Equals("") && !pwd.Equals("") && !domain.Equals(""))
                {
                    string xmlconst = "<Batch OnError='Continue' RootFolder='" + rootfolder + "'><Method ID='1' Cmd='New'><Field Name='ID'>New</Field><Field Name='FSObjType'>1</Field><Field Name='BaseName'>!@foldername</Field></Method></Batch>";
                    XmlDocument doc = new XmlDocument();
                    string xmlFolder = xmlconst.Replace("!@foldername", folderName);
                    doc.LoadXml(xmlFolder);
                    XmlNode batchNode = doc.SelectSingleNode("//Batch");
                    //XmlNode resultNode = splist.UpdateListItems(@"VHZDVLNAPP1:D:\ERPLN\bse:510", batchNode);
                    XmlNode resultNode = listService.UpdateListItems(docLibrary, batchNode);
                    WriteToFile(folderName + " Folder is Created Successfully.");
                }
            }
            catch (Exception ex)
            {
                //Console.Write("Error in creating folder. " + ex.Message + "StackTrace" + ex.StackTrace);
                WriteToFile("Error Occured inside UpdateListItemCreateFolder. Exception: " + ex.Message);
            }
        }

        public void CreateLog(string strFunctionName, string exMessage)
        {
            try
            {
                string strFilePath = ConfigurationManager.AppSettings["error"].ToString() + "DigitalSigningUtility_Error_" + DateTime.Now.ToString("ddMMyyyy") + ".log";
                FileStream fs = null;
                StreamWriter sw = null;
                if (!File.Exists(strFilePath))
                    fs = File.Create(strFilePath);
                else
                    sw = File.AppendText(strFilePath);
                sw = sw == null ? new StreamWriter(fs) : sw;
                sw.WriteLine("Location : " + strFunctionName);
                sw.WriteLine("DateTime : " + DateTime.Now);
                sw.WriteLine("Exception :" + exMessage);

                sw.WriteLine("------------------------------------------------------------------------------------------------------------------------------");
                sw.Close();
                if (fs != null)
                    fs.Close();
            }
            catch (Exception ex1)
            {

            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            string input_path = "";
            string scanmode = ConfigurationManager.AppSettings["scanfolder"].ToString();
            var credentials = new NetworkCredential(ConfigurationManager.AppSettings["UserName"].ToString(), ConfigurationManager.AppSettings["Password"].ToString(), ConfigurationManager.AppSettings["Domain"].ToString());

            try
            {
                if (scanmode.Equals("0"))
                {
                    input_path = ConfigurationManager.AppSettings["filein"].ToString();
                    FileProcessing(input_path);
                }
                else
                {
                    input_path = ConfigurationManager.AppSettings["fileinremote"].ToString();
                    //using (new NetworkConnection(input_path, credentials))
                    //{
                    FileProcessing(input_path);
                    //}
                }
            }
            catch (Exception ex)
            {
                WriteToFile("There is an Error in Directory. " + ex.Message);
                toolStripStatusLabel1.Text = "Directory is not found....";
                statusStrip1.Refresh();
            }
        }

        private void FileProcessing(string input_path)
        {
            string[] filePaths = Directory.GetFiles(input_path, "*.pdf");
            WriteToFile("No. of files = " + filePaths.Length);
            if (filePaths.Length > 0)
            {
                for (int i = 0; i < filePaths.Length; i++)
                {
                    WriteToFile(filePaths[i] + " file has been Selected For Signing");
                    UploadFile("", filePaths[i]);
                    WriteToFile("");
                }
            }
            else
            {
                toolStripStatusLabel1.Text = "No Files Found....";
                statusStrip1.Refresh();
                WriteToFile("NO files found.");
                WriteToFile("");
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //Configuration configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            //string val = ConfigurationManager.AppSettings["certPin"].ToString();
            //string[] keys = ConfigurationManager.AppSettings.AllKeys;
            // string ecncrptval = EncryptString(ToSecureString(val));
            //configuration.AppSettings.Settings["pin"].Value = ecncrptval;
            //configuration.Save(ConfigurationSaveMode.Modified);
            //ConfigurationManager.RefreshSection("appSettings");
            // SecureString password = DecryptString(val);
            // string pass = ToInsecureString(password);
            //MessageBox.Show("Encrypted Val:" + ConfigurationManager.AppSettings["pin"].ToString() + "" + "Decrypted Val:" + pass);

            toolStripStatusLabel1.Text = "Initializing...";
            statusStrip1.Refresh();
            timer1.Enabled = true;
            label2.Text = "#";
            //SecureString ss = ToSecureString("12345678");
            //string enc = EncryptString(ss);
        }


        private void writetolistConsole(string s)
        {
            /*int cnt = consoleList.Items.Count;
            consoleList.Items.Add(DateTime.Now + ":"+   s);*/

        }

        public static void WriteToFile(string txt)
        {
            string logpath = ("DigiSignLog_" + (DateTime.Now.Day.ToString() + ("_" + (DateTime.Now.Month.ToString() + ("_" + (DateTime.Now.Year.ToString() + ".log"))))));
            StreamWriter sw = new StreamWriter(logpath, true);
            if ((txt != ""))
            {
                sw.WriteLine((DateTime.Now.ToString() + (" : " + txt)));
            }
            else
            {
                sw.WriteLine("-------------------------------------------------------------------------------------------------");
            }
            sw.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Opens the Log File.
            string logpath = ("DigiSignLog_" + (DateTime.Now.Day.ToString() + ("_" + (DateTime.Now.Month.ToString() + ("_" + (DateTime.Now.Year.ToString() + ".log"))))));
            string path = Path.Combine(Directory.GetCurrentDirectory(), logpath);
            System.Diagnostics.Process.Start(path);
        }
        public void SendMail(string _from, string _to, string _subject, string _body)
        {
            string rval = "";
            _body = ("<div style=\'font-family:Segoe UI Light; font-size:12px;\'>" + (_body + "</div>"));
            try
            {
                MailMessage mm = new MailMessage(_from, _to);
                mm.Subject = _subject;
                if ((_to.Trim() != ""))
                {
                    string[] ccmails = _to.Split(new char[] { ',' });
                    //string ccmail = " ";
                    foreach (string ccmail in ccmails)
                    {
                        mm.To.Add(new MailAddress(ccmail));
                    }
                }
                mm.Body = _body;
                mm.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "ssawf";
                smtp.Port = 25;
                smtp.Send(mm);

                WriteToFile("EMail Sent to " + _to);
            }
            catch (Exception ex)
            {
                rval = "Error sending Mail: " + ex.ToString();
                WriteToFile(rval);
            }
        }

        static byte[] entropy = System.Text.Encoding.Unicode.GetBytes("Salt Is Not A Password");
        public static string EncryptString(System.Security.SecureString input)
        {
            byte[] encryptedData = System.Security.Cryptography.ProtectedData.Protect(
                System.Text.Encoding.Unicode.GetBytes(ToInsecureString(input)),
                entropy,
                System.Security.Cryptography.DataProtectionScope.CurrentUser);
            return Convert.ToBase64String(encryptedData);
        }

        public static SecureString DecryptString(string encryptedData)
        {
            try
            {
                byte[] decryptedData = System.Security.Cryptography.ProtectedData.Unprotect(
                    Convert.FromBase64String(encryptedData),
                    entropy,
                    System.Security.Cryptography.DataProtectionScope.CurrentUser);
                return ToSecureString(System.Text.Encoding.Unicode.GetString(decryptedData));
            }
            catch
            {
                return new SecureString();
            }
        }

        public static SecureString ToSecureString(string input)
        {
            SecureString secure = new SecureString();
            foreach (char c in input)
            {
                secure.AppendChar(c);
            }
            secure.MakeReadOnly();
            return secure;
        }

        public static string ToInsecureString(SecureString input)
        {
            string returnValue = string.Empty;
            IntPtr ptr = System.Runtime.InteropServices.Marshal.SecureStringToBSTR(input);
            try
            {
                returnValue = System.Runtime.InteropServices.Marshal.PtrToStringBSTR(ptr);
            }
            finally
            {
                System.Runtime.InteropServices.Marshal.ZeroFreeBSTR(ptr);
            }
            return returnValue;
        }
    }


    static class X509Certificate2Extension
    {
        public static void SetPinForPrivateKey(this X509Certificate2 certificate, string pin)
        {
            if (certificate == null) throw new ArgumentNullException("certificate");
            var key = (RSACryptoServiceProvider)certificate.PrivateKey;

            var providerHandle = IntPtr.Zero;
            var pinBuffer = Encoding.ASCII.GetBytes(pin);

            // provider handle is implicitly released when the certificate handle is released.
            SafeNativeMethods.Execute(() => SafeNativeMethods.CryptAcquireContext(ref providerHandle,
                key.CspKeyContainerInfo.KeyContainerName,
                key.CspKeyContainerInfo.ProviderName,
                key.CspKeyContainerInfo.ProviderType,
                SafeNativeMethods.CryptContextFlags.Silent));
            SafeNativeMethods.Execute(() => SafeNativeMethods.CryptSetProvParam(providerHandle,
                SafeNativeMethods.CryptParameter.KeyExchangePin,
                pinBuffer, 0));

            SafeNativeMethods.Execute(() => SafeNativeMethods.CertSetCertificateContextProperty(
                certificate.Handle,
                SafeNativeMethods.CertificateProperty.CryptoProviderHandle,
                0, providerHandle));
        }


        internal static class SafeNativeMethods
        {
            internal enum CryptContextFlags
            {
                None = 0,
                Silent = 0x40
            }

            internal enum CertificateProperty
            {
                None = 0,
                CryptoProviderHandle = 0x1
            }

            internal enum CryptParameter
            {
                None = 0,
                KeyExchangePin = 0x20
            }

            [DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
            public static extern bool CryptAcquireContext(
                ref IntPtr hProv,
                string containerName,
                string providerName,
                int providerType,
                CryptContextFlags flags
                );

            [DllImport("advapi32.dll", SetLastError = true, CharSet = CharSet.Auto)]
            public static extern bool CryptSetProvParam(
                IntPtr hProv,
                CryptParameter dwParam,
                [In] byte[] pbData,
                uint dwFlags);

            [DllImport("CRYPT32.DLL", SetLastError = true)]
            internal static extern bool CertSetCertificateContextProperty(
                IntPtr pCertContext,
                CertificateProperty propertyId,
                uint dwFlags,
                IntPtr pvData
                );

            public static void Execute(Func<bool> action)
            {
                if (!action())
                {
                    throw new Win32Exception(Marshal.GetLastWin32Error());
                }
            }
        }
    }
}
